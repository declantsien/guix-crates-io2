(define-module (crates-io se rd serde_json_canonicalizer) #:use-module (crates-io))

(define-public crate-serde_json_canonicalizer-0.1.0 (c (n "serde_json_canonicalizer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu-js") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "0i9iqbkzxa8x79w13njilacclc1xmmvmna1jdiy0r50rjjkrmz98") (y #t)))

(define-public crate-serde_json_canonicalizer-0.2.0 (c (n "serde_json_canonicalizer") (v "0.2.0") (d (list (d (n "ryu-js") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "189nmgcgjg986ixs9dfsv4xmy08y0svrj4mx0cn3s79yjbmc595q")))

