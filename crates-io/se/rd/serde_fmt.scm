(define-module (crates-io se rd serde_fmt) #:use-module (crates-io))

(define-public crate-serde_fmt-0.0.0 (c (n "serde_fmt") (v "0.0.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0x4awfk1hpdap06bzc5r8a02bm4fgv44dmy1rgymm4p50k05pjrm") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-serde_fmt-0.0.1 (c (n "serde_fmt") (v "0.0.1") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1iv3y6i1yb323q232z7089hqkly0w2j9vpfhv3y7l073zz21wj72") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-serde_fmt-0.1.0 (c (n "serde_fmt") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0sxil33hlv1g4p2xl7idfpgni2hc4m4c50m33h01ml8idkz1h5wr") (f (quote (("std" "serde/std"))))))

(define-public crate-serde_fmt-1.0.0 (c (n "serde_fmt") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.104") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0r9y64icg4sgix9yasbfq0aqk3p1bhgxc19zq38gqpmr4ybk2xfy") (f (quote (("std" "serde/std"))))))

(define-public crate-serde_fmt-1.0.1 (c (n "serde_fmt") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.104") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "195sp3zxbdgrnaqxwn9z3pij04fd7yyihnm4fpfc261r5fdacqr9") (f (quote (("std" "serde/std"))))))

(define-public crate-serde_fmt-1.0.2 (c (n "serde_fmt") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.104") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "01xl8vlbgq4qjl0va8wzcrnxi4lip4rvl3sblvz6axfsvpsxx031") (f (quote (("std" "serde/std"))))))

(define-public crate-serde_fmt-1.0.3 (c (n "serde_fmt") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.104") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "190h61yfnzprzb1cvkzmyzcarj52f6xggiz8542xck0h2k5dvm71") (f (quote (("std" "serde/std"))))))

