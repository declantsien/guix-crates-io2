(define-module (crates-io se rd serdeval) #:use-module (crates-io))

(define-public crate-serdeval-0.1.0 (c (n "serdeval") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1l1bwsz58ldn9cqj1ax9qy071aza8jgw028zkayshj6msggkl0ll")))

