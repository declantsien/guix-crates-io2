(define-module (crates-io se rd serde_int_map_traits) #:use-module (crates-io))

(define-public crate-serde_int_map_traits-0.2.0 (c (n "serde_int_map_traits") (v "0.2.0") (h "19hmrspv426spd6ax4a3dmbzis3zx3vyb8majlycwrpiwwfhyzvh")))

(define-public crate-serde_int_map_traits-0.3.0 (c (n "serde_int_map_traits") (v "0.3.0") (h "0r22shbdccxa6l4cjaychdd3ss9lqq3fsm346lpg9ldnh7r4j4wf")))

