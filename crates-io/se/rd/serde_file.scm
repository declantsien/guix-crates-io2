(define-module (crates-io se rd serde_file) #:use-module (crates-io))

(define-public crate-serde_file-0.1.0 (c (n "serde_file") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "scopeguard") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0gcayz0l65c0sm8r19il7nyfrypf3fxvimr7phlq1xw1x1llrrlg")))

