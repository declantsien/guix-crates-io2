(define-module (crates-io se rd serde-mobile) #:use-module (crates-io))

(define-public crate-serde-mobile-1.0.0 (c (n "serde-mobile") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 2)))) (h "1asngvbyvlsmmfbmrgdkki9ar5nrnvb9mrwb655nh04vphmcrm8c")))

(define-public crate-serde-mobile-1.0.2 (c (n "serde-mobile") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 2)))) (h "11bz5sx5ncfvwdnzqibxwpgi1ibc41w40xk8n2xr88q9akdjqldh")))

(define-public crate-serde-mobile-2.0.0 (c (n "serde-mobile") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 2)))) (h "02bhxgphgfb9gr974r2axj425j6d1k0nh59fi2qzzgwrza8f3f31")))

(define-public crate-serde-mobile-3.0.0 (c (n "serde-mobile") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std" "derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 2)))) (h "1911rw3xdhrlidll1b1dcarbnw6bqvycr7jbnkizvwgl96n14bn3")))

