(define-module (crates-io se rd serde_gura) #:use-module (crates-io))

(define-public crate-serde_gura-0.1.0 (c (n "serde_gura") (v "0.1.0") (d (list (d (n "gura") (r "^0.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "041l60hymlbpsxcd1j9wlf4ynq8l95wzgphjfzx8iqrw25z9z1n3")))

(define-public crate-serde_gura-0.1.1 (c (n "serde_gura") (v "0.1.1") (d (list (d (n "gura") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wfaf6xbma24n5s8040xl6yga9g8v57k80rjrsq78mpc5c6nynsc")))

(define-public crate-serde_gura-0.1.2 (c (n "serde_gura") (v "0.1.2") (d (list (d (n "gura") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xy2izzlcx7g779bb3wmv4lr6nkfx9q6qpq1vjzlj60sfrmhj5xp")))

(define-public crate-serde_gura-0.1.3 (c (n "serde_gura") (v "0.1.3") (d (list (d (n "gura") (r "^0.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zvnkxxfjvp81jzd01l259bszc1w59qkqaz8vi5rbixyp3rkzq6q")))

(define-public crate-serde_gura-0.1.4 (c (n "serde_gura") (v "0.1.4") (d (list (d (n "gura") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "035ghplz5fi5n8w8fb7acfzf8acimqzmri4qp6qni4i2ppbvppkg")))

(define-public crate-serde_gura-0.1.5 (c (n "serde_gura") (v "0.1.5") (d (list (d (n "gura") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0d5h4ykadhpc7m1c516iiyycn3g0w50vp28awar912a3id8ghwdd")))

(define-public crate-serde_gura-0.1.6 (c (n "serde_gura") (v "0.1.6") (d (list (d (n "gura") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04hgcigscz6szfnp8f54wsnpp6zd60ng47x3i1fmnv0g62gmqsd2")))

(define-public crate-serde_gura-0.1.7 (c (n "serde_gura") (v "0.1.7") (d (list (d (n "gura") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17fn0magxh592jyg2f6jh4sp6d19qxbqx5f8r1v1pcipd6m1yliq")))

(define-public crate-serde_gura-0.1.8 (c (n "serde_gura") (v "0.1.8") (d (list (d (n "gura") (r "^0.5.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13gzhp6kwf5drwl0s2v3m0q0965lb0dmi3yf1lyg9b3fkgwza42x")))

