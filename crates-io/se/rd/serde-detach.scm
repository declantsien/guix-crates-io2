(define-module (crates-io se rd serde-detach) #:use-module (crates-io))

(define-public crate-serde-detach-0.0.1 (c (n "serde-detach") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-object") (r "^0.0.0-alpha.0") (d #t) (k 2)) (d (n "serde_taml") (r "^0.0.1") (d #t) (k 2)) (d (n "wyz") (r "^0.2.0") (d #t) (k 0)))) (h "123j0pnqs824sxpz1qd3369f2kfdgcs33i33pdaaidj2lh6ia8f6")))

