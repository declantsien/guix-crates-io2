(define-module (crates-io se rd serde_struct_wrapper) #:use-module (crates-io))

(define-public crate-serde_struct_wrapper-0.3.0 (c (n "serde_struct_wrapper") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.71") (d #t) (k 0)))) (h "1vr4v8mrank4hvmp1zf3bdmvl6wdf1anc3klml9y22hxgdj5xlqq")))

(define-public crate-serde_struct_wrapper-0.3.1 (c (n "serde_struct_wrapper") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.71") (d #t) (k 0)))) (h "1p23y3qvdsjzhxxpl2ndq75ji2djbns1kswjcfis3xcp4pgm6nan")))

(define-public crate-serde_struct_wrapper-0.3.2 (c (n "serde_struct_wrapper") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.71") (d #t) (k 0)))) (h "1xzkvc7kc1jq5jqsbsifb7f5ycn6lnn1qcqscdglcrl3bknsn76w")))

