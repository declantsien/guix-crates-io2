(define-module (crates-io se rd serde_yaml2) #:use-module (crates-io))

(define-public crate-serde_yaml2-0.1.0 (c (n "serde_yaml2") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.8.0") (d #t) (k 0)))) (h "1s8slggdik8g77gz5rq60h62s8m0a27639jinj6mw0arj3nmlddl")))

(define-public crate-serde_yaml2-0.1.1 (c (n "serde_yaml2") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.8.0") (d #t) (k 0)))) (h "1i5j8hbf2ngj25azqaf4b46nfh0p361wv0kb23pxy28pzxlnl58l")))

