(define-module (crates-io se rd serde_libconfig) #:use-module (crates-io))

(define-public crate-serde_libconfig-0.0.1 (c (n "serde_libconfig") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "11qj5a0qakbgshp14ws72dszzj7qa33zyfjy43cgwg38lsxlaz0i")))

