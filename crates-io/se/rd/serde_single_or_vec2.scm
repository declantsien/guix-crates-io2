(define-module (crates-io se rd serde_single_or_vec2) #:use-module (crates-io))

(define-public crate-serde_single_or_vec2-1.0.0 (c (n "serde_single_or_vec2") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("serde"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s6g4mk7ilbi2w0vr0ixi921lfa6yiq7dzqs59pkws705y4s95i3") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

