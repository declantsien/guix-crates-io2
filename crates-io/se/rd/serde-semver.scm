(define-module (crates-io se rd serde-semver) #:use-module (crates-io))

(define-public crate-serde-semver-0.1.0 (c (n "serde-semver") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1h0h6wphrl6hbx4623l28hp0vyn4nn3mjinvk0v0rvp1i02cklag")))

(define-public crate-serde-semver-0.2.0 (c (n "serde-semver") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-semver-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0bhwklcix07v3qznlcanhrsr8jyaypf9h4qvk6x7aagb5ygzbd2v")))

(define-public crate-serde-semver-0.2.1 (c (n "serde-semver") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-semver-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "00f3hwy6vs0c6qpjqqabq8l9s2is6a2igva57qc7isl98x5jc64l")))

