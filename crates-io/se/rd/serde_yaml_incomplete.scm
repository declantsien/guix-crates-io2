(define-module (crates-io se rd serde_yaml_incomplete) #:use-module (crates-io))

(define-public crate-serde_yaml_incomplete-0.0.0 (c (n "serde_yaml_incomplete") (v "0.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.4") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.4") (d #t) (k 1)) (d (n "syntex") (r "^0.29") (d #t) (k 1)))) (h "1gfzsnjki2ljisw9kdqfd9ljjkkfmh985xxx956cjjfxvl450fl6") (f (quote (("foo")))) (y #t)))

