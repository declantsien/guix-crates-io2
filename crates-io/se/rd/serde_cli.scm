(define-module (crates-io se rd serde_cli) #:use-module (crates-io))

(define-public crate-serde_cli-0.0.0 (c (n "serde_cli") (v "0.0.0") (d (list (d (n "erased-serde") (r "^0.3.17") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)))) (h "12gw1qp96qmabj8sa2ngmg7wv3cszbqpxxg3xgw9jyzsn354qn1n")))

(define-public crate-serde_cli-0.1.0 (c (n "serde_cli") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3.17") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "1nnxw4fy601znvmi1hzmfg55389q83ffbk52h1v9msq3l037zhdz")))

