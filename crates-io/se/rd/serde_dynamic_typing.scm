(define-module (crates-io se rd serde_dynamic_typing) #:use-module (crates-io))

(define-public crate-serde_dynamic_typing-0.1.0 (c (n "serde_dynamic_typing") (v "0.1.0") (d (list (d (n "ebacktrace") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1d1fhdjj0ksrk0dy8j4vybfs9q056c4z19min41w7sfz2psywppl")))

(define-public crate-serde_dynamic_typing-0.1.1 (c (n "serde_dynamic_typing") (v "0.1.1") (d (list (d (n "ebacktrace") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1kam6hwnnj6b8c23917yaffn8mrzfa1c36ncg51agr4hf7jm4v5x")))

