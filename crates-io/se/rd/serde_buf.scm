(define-module (crates-io se rd serde_buf) #:use-module (crates-io))

(define-public crate-serde_buf-0.1.0 (c (n "serde_buf") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0hfnwfbig8qdpjd6p1ba78sbi8zvnph43xag6322skndk33y0g0a")))

(define-public crate-serde_buf-0.1.1 (c (n "serde_buf") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1k2nc3pa7rbzyhhnjakw6nkx2wa6da6nrxf65s6p2d3xdjfvx1is")))

