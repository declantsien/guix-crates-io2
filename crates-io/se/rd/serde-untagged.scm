(define-module (crates-io se rd serde-untagged) #:use-module (crates-io))

(define-public crate-serde-untagged-0.0.0 (c (n "serde-untagged") (v "0.0.0") (h "11fb7zvvzsqvhjm9llb1416clfpiavfgmyz977sn7mx2vmvn0fqw") (y #t)))

(define-public crate-serde-untagged-0.1.0 (c (n "serde-untagged") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3.30") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)))) (h "0swkmy16s1m5i7gdk3ygbrxmm34s21hzqk55d0spkh06xmy9jhp3") (r "1.56")))

(define-public crate-serde-untagged-0.1.1 (c (n "serde-untagged") (v "0.1.1") (d (list (d (n "erased-serde") (r "^0.3.30") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)))) (h "18sabbkrlizg44120g62bkzq2jny5q5q9n7sdddbfl9gqrcsr8qb") (r "1.56")))

(define-public crate-serde-untagged-0.1.2 (c (n "serde-untagged") (v "0.1.2") (d (list (d (n "erased-serde") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1rxa0ixdd6c7xg59s3mjlh5h5782yvwardw3hm3kiw6qv718b263") (r "1.56")))

(define-public crate-serde-untagged-0.1.3 (c (n "serde-untagged") (v "0.1.3") (d (list (d (n "erased-serde") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1454yfjnzn3ga31b6rhds68ygqfyn95836l9wc8h75vl747mbqpf") (r "1.56")))

(define-public crate-serde-untagged-0.1.4 (c (n "serde-untagged") (v "0.1.4") (d (list (d (n "erased-serde") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "1b2x30zczv16q6xakjlh1mhn6y1m2ww994szxfhlnryqfc1y13jc") (r "1.56")))

(define-public crate-serde-untagged-0.1.5 (c (n "serde-untagged") (v "0.1.5") (d (list (d (n "erased-serde") (r "^0.4.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "0riav1qd7xpm4nbj9aa51k30imbb2ncs5sp790rkbz4d6qsha5ka") (r "1.56")))

(define-public crate-serde-untagged-0.1.6 (c (n "serde-untagged") (v "0.1.6") (d (list (d (n "erased-serde") (r "^0.4.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)) (d (n "typeid") (r "^1") (d #t) (k 0)))) (h "1dn5nmkmbpc0x50ai3lp307pdf50dzd8wb5xbjp5rxw2pncvlxi6") (r "1.61")))

