(define-module (crates-io se rd serde_arrays) #:use-module (crates-io))

(define-public crate-serde_arrays-0.1.0 (c (n "serde_arrays") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zghprf1ddjxckzxbi3la6skrwradlb23c9ybwyyqs3zhlr62qrq")))

