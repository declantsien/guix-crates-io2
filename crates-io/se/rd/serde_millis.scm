(define-module (crates-io se rd serde_millis) #:use-module (crates-io))

(define-public crate-serde_millis-0.1.0 (c (n "serde_millis") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0s6bbyhm2cmj5z2j0qp98d6507hxbkgj570hvg08xip74bz8fvhz")))

(define-public crate-serde_millis-0.1.1 (c (n "serde_millis") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "002d8dk3fwidh5j44aixjbzw80q24w0d208xklv2rvm51iwdrqp6")))

