(define-module (crates-io se rd serde_bare) #:use-module (crates-io))

(define-public crate-serde_bare-0.1.0 (c (n "serde_bare") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1q46wzbpksh3f22a3mbicns32fkn67s6j49gbzhzzdkmmrpck2m8")))

(define-public crate-serde_bare-0.1.1 (c (n "serde_bare") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gsckh6g4mrd67hm1m93j8brsblm2j0y4gxh7886n011b52c3qm0")))

(define-public crate-serde_bare-0.2.0 (c (n "serde_bare") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0zhzwqpzfc1897p2cvajr18yhy3xwki136j5dds95xc74jqk9hfg")))

(define-public crate-serde_bare-0.3.0 (c (n "serde_bare") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ck4f494iwcw8fwl9s9kwxh1i1fldwp8nb97fjnr7ywqm9aj5nq1")))

(define-public crate-serde_bare-0.4.0 (c (n "serde_bare") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1yhm6b1q0mwpd4y9c0agp9312iab4bsa2kiy2syaspxlw2qhkgdd")))

(define-public crate-serde_bare-0.5.0 (c (n "serde_bare") (v "0.5.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "0q6nvsgvdwnmwg5p9ivr1jv8gwi2h75c4789gfasxwfhxs357iai") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

