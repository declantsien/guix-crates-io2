(define-module (crates-io se rd serde-json-core-fmt) #:use-module (crates-io))

(define-public crate-serde-json-core-fmt-0.1.0 (c (n "serde-json-core-fmt") (v "0.1.0") (d (list (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1aa0kjvrr327pr2nn3h37dwrn761f3cf12c64c81js51jw6dd8jr") (f (quote (("custom-error-messages")))) (y #t)))

(define-public crate-serde-json-core-fmt-0.1.1 (c (n "serde-json-core-fmt") (v "0.1.1") (d (list (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hxlpmll5gf2kbplsy5q9q0x279cvdiddgrgkdxwaslk8kk9c3yn") (f (quote (("custom-error-messages"))))))

