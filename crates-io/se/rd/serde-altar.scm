(define-module (crates-io se rd serde-altar) #:use-module (crates-io))

(define-public crate-serde-altar-0.1.0 (c (n "serde-altar") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "03z6hwxz7vwf4iy2cxlx6vwfg0ycm0ya29x7w9v13hisai6crz9x") (y #t)))

(define-public crate-serde-altar-0.2.0 (c (n "serde-altar") (v "0.2.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1vbqsiqfggcjb8fpk7wj09l4gqa2n00hjj76473jx4h39vgzs9r6") (y #t)))

(define-public crate-serde-altar-0.3.0 (c (n "serde-altar") (v "0.3.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1ys6yzmna1hlhr3rkg07dyx0ybmapby469kx1h5f05fh4rr82kb6") (y #t)))

(define-public crate-serde-altar-0.4.0 (c (n "serde-altar") (v "0.4.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0jvabbsygj9x6xfmf11nh52j50mqwmqy8w5vpkcgnd4rxf93by93") (y #t)))

(define-public crate-serde-altar-0.5.0 (c (n "serde-altar") (v "0.5.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1blcgwbz5srqmfi6j78ga1g2gr0zhnh890ifdlmxik30dajp14n8") (y #t) (r "1.56")))

(define-public crate-serde-altar-0.5.1 (c (n "serde-altar") (v "0.5.1") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "14xsfmwqzb7b0m08prlvwl236yz4jb81bz6522j3lcmjiyyqv74l") (y #t) (r "1.56")))

