(define-module (crates-io se rd serde_derive) #:use-module (crates-io))

(define-public crate-serde_derive-0.8.4 (c (n "serde_derive") (v "0.8.4") (d (list (d (n "serde_codegen") (r "= 0.8.4") (d #t) (k 0)))) (h "13vc99rd9c4s59nlz2wgvgzdarwqdw3859gshkdyl7d2jy09bpia")))

(define-public crate-serde_derive-0.8.6 (c (n "serde_derive") (v "0.8.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.6") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.6") (d #t) (k 0)) (d (n "serde_test") (r "^0.8.6") (d #t) (k 2)))) (h "053pk2lmf3z2wpgsyb31xrzpssms1g2xibxia4m5yg7bkhpwlid6")))

(define-public crate-serde_derive-0.8.7 (c (n "serde_derive") (v "0.8.7") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.7") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.7") (d #t) (k 0)) (d (n "serde_test") (r "^0.8.7") (d #t) (k 2)))) (h "01mk4k8sc2nh02nq93avq92nfsvs6sjlf146f3pxksgcb6jwcp1x")))

(define-public crate-serde_derive-0.8.8 (c (n "serde_derive") (v "0.8.8") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.8") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.8") (d #t) (k 0)) (d (n "serde_test") (r "^0.8.8") (d #t) (k 2)))) (h "1cm0lzvkkii3bm1lcz7y7lqkzasfnn5jl8li9w809lmbgwh18yyp")))

(define-public crate-serde_derive-0.8.9 (c (n "serde_derive") (v "0.8.9") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.9") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.9") (d #t) (k 0)) (d (n "serde_test") (r "^0.8.9") (d #t) (k 2)))) (h "0rfs85wvnwchnld8q39xkin1lcr7lr9spi8d8rh63mghfhwry5ii")))

(define-public crate-serde_derive-0.8.10-rc1 (c (n "serde_derive") (v "0.8.10-rc1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.9") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.10-rc1") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.9") (d #t) (k 2)))) (h "06ik7dgmd19jcifwz8j4pn8ywsf8bg5fvmgsn7458mn9a14msqci")))

(define-public crate-serde_derive-0.8.10 (c (n "serde_derive") (v "0.8.10") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.10") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.10") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.10") (d #t) (k 2)))) (h "1f8371aigb477nddqgwlwmfqvq0hwa25pw35vrs8aam9c7m0x3v5")))

(define-public crate-serde_derive-0.8.11 (c (n "serde_derive") (v "0.8.11") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.11") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.11") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.11") (d #t) (k 2)))) (h "0azjnx7mwgsxq6wzm9h35ghrd9pqy0sjkrjqykbkf762dkc83krr")))

(define-public crate-serde_derive-0.8.12 (c (n "serde_derive") (v "0.8.12") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.12") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.12") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.12") (d #t) (k 2)))) (h "0gs4gi2hwlb6qcxvvc100fcbzd5qhmp5ych8lh5k78awx00jnz1c")))

(define-public crate-serde_derive-0.8.13 (c (n "serde_derive") (v "0.8.13") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.13") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.13") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.13") (d #t) (k 2)))) (h "0ahs84plzmnbiqfy43rg0yi99l6c092jw6nfmly6493w6vrlv7h1")))

(define-public crate-serde_derive-0.8.14 (c (n "serde_derive") (v "0.8.14") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.14") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.14") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.14") (d #t) (k 2)))) (h "1j8c4rzb1dszbrbphqpkxwpr8qg5flr7sxiqi63aqjg2yh68n0ha")))

(define-public crate-serde_derive-0.8.15 (c (n "serde_derive") (v "0.8.15") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "post-expansion") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.15") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.15") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.15") (d #t) (k 2)))) (h "0vhlqmxw8m2pj10jdzs5vk6v4syh45jyh5di4ncmggqk8rd3m96q")))

(define-public crate-serde_derive-0.8.16 (c (n "serde_derive") (v "0.8.16") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "post-expansion") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.16") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.16") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.16") (d #t) (k 2)))) (h "1l1nb31gd6kgqv747pxqmqs507l88drl4502pcmvln4168yxy3ny")))

(define-public crate-serde_derive-0.8.17 (c (n "serde_derive") (v "0.8.17") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "post-expansion") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.17") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.17") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.17") (d #t) (k 2)))) (h "1373n8asryskdrl75py8wr3ayqvh77w4nw9aza56d4y1bw73z9y6")))

(define-public crate-serde_derive-0.8.18 (c (n "serde_derive") (v "0.8.18") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.18") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.18") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.18") (d #t) (k 2)))) (h "0jmaj3krl8ga38ix2sqw02ix12pwj1ms71j90wx8s4mrvsnfh6yr")))

(define-public crate-serde_derive-0.8.19 (c (n "serde_derive") (v "0.8.19") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.19") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.19") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.19") (d #t) (k 2)))) (h "1d2lf0cj1fq3q62ynr22cx8igsaj2biw7g4s5ihd61s2kia43dd4")))

(define-public crate-serde_derive-0.8.20 (c (n "serde_derive") (v "0.8.20") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.20") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.20") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.20") (d #t) (k 2)))) (h "0c4zgjb39jx9qsr0jfdsyn6v1x3pfdam0hga0xvymayai370n0qq")))

(define-public crate-serde_derive-0.8.21 (c (n "serde_derive") (v "0.8.21") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.21") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.21") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.21") (d #t) (k 2)))) (h "0gc4q4vphbzbhk898pbw6gglca2y2bm63hrrr8q2gfbr8vkx2ykf")))

(define-public crate-serde_derive-0.8.22 (c (n "serde_derive") (v "0.8.22") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.22") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.22") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.22") (d #t) (k 2)))) (h "0hidjrdbv39gcg9ysaw2mnjdwk0xv6h9i8qliprbqs7gdlyibr5j")))

(define-public crate-serde_derive-0.9.0-rc1 (c (n "serde_derive") (v "0.9.0-rc1") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0-rc1") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9.0-rc1") (d #t) (k 2)))) (h "022cfqhrmm7z5lfjhlmj3gw8l56wllxinl1yv89wb33bph3xa4yw")))

(define-public crate-serde_derive-0.8.23 (c (n "serde_derive") (v "0.8.23") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.8.23") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.8.23") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.8.23") (d #t) (k 2)))) (h "01jkmw0a9zs4bvxjpqmrgfbgpg9yrjvmflw7kk9fjfab4vsfai6f")))

(define-public crate-serde_derive-0.9.0-rc2 (c (n "serde_derive") (v "0.9.0-rc2") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9.0-rc2") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0-rc2") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9.0-rc2") (d #t) (k 2)))) (h "0hknn469ay7d4lwzpdxbpd66b40prarrdb6xlbmpp8wjxbc92h1g")))

(define-public crate-serde_derive-0.9.0-rc3 (c (n "serde_derive") (v "0.9.0-rc3") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9.0-rc3") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0-rc3") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9.0-rc3") (d #t) (k 2)))) (h "1m2cw80bn3xj32plpkn4gfkznkz73zi1myda3w7cjvz3h60rfl8n")))

(define-public crate-serde_derive-0.9.0-rc4 (c (n "serde_derive") (v "0.9.0-rc4") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9.0-rc4") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0-rc4") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9.0-rc4") (d #t) (k 2)))) (h "0mi13wpjkm4pnq4fhr0ny858zp04002zm6k5w0vbbcv4k50pxc8x")))

(define-public crate-serde_derive-0.9.0 (c (n "serde_derive") (v "0.9.0") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9.0") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9.0") (d #t) (k 2)))) (h "1c7899m1mlygwfjnc09g0h77l5yphwskh6lz2q5a3z667c4vdxai")))

(define-public crate-serde_derive-0.9.1 (c (n "serde_derive") (v "0.9.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 2)) (d (n "serde_codegen") (r "= 0.9.0") (f (quote ("with-syn"))) (k 0)) (d (n "serde_test") (r "^0.9") (d #t) (k 2)))) (h "17zmffpsaihi02vrz9nbbj70n4rygxclip5mp2k3wdrnqjg26pbk")))

(define-public crate-serde_derive-0.9.2 (c (n "serde_derive") (v "0.9.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.12.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "0bgkszzaj59c5f9jl9qfyar9z7h26910x3984ly0cas0mxpd8y3j") (f (quote (("unstable-testing" "clippy" "serde_codegen_internals/unstable-testing") ("unstable"))))))

(define-public crate-serde_derive-0.9.3 (c (n "serde_derive") (v "0.9.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.12.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "1ljn8pwk5fl71by25hdi3ppclavykvdvd3hvi9li3f6sj06kmwi0") (f (quote (("unstable-testing" "clippy" "serde_codegen_internals/unstable-testing") ("unstable"))))))

(define-public crate-serde_derive-0.9.4 (c (n "serde_derive") (v "0.9.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.12.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "15jfqmkbqalfdvzcphs1gp0a8f7vkbxr0j1a4ya2nsqad4fzn8rq") (f (quote (("unstable-testing" "clippy" "serde_codegen_internals/unstable-testing") ("unstable"))))))

(define-public crate-serde_derive-0.9.5 (c (n "serde_derive") (v "0.9.5") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.12.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "1lgwnja6wkbxj28jcj0r7g52h8k4i3y0dk2hgy8lv79z298fyvpv") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.6 (c (n "serde_derive") (v "0.9.6") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.13.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "1gy3rfan2lfwq5bmhlzx72znzw238h1x7lh2hgjyqcx9khvy1ipc") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.7 (c (n "serde_derive") (v "0.9.7") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.13.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "174qrvh4qv71s0imi748gszb6glh5g306j5c18hfn78nqljh9wva") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.8 (c (n "serde_derive") (v "0.9.8") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0bfka0d0s1rcw6rar9ks96dr3ab5sq2s6i304zbbzgicl1ic13p8") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.9 (c (n "serde_derive") (v "0.9.9") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "029pjcyflxjnqv6x14gayhsza5knmn89gxh8z7jp6hahfkbifl58") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.10 (c (n "serde_derive") (v "0.9.10") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1kr6yxc3macqf06j7gxggns20lkz2hh104llifa51j3qrpryk7kq") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.11 (c (n "serde_derive") (v "0.9.11") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0lhl5chknm55jnsrzcppwdkbwjf6k7x39ndl8r3ddcips15s4ppi") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.12 (c (n "serde_derive") (v "0.9.12") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.2") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "049s6jjw4qxc7vj52a5caciqdxfryk50qcydrcdblmbdkxim7dzb") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.13 (c (n "serde_derive") (v "0.9.13") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.2") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0l7djs7hg8ap1yc3g2xdkrn9k294zs83idsjnsgdi4yi9ppp4p6p") (f (quote (("unstable"))))))

(define-public crate-serde_derive-0.9.14 (c (n "serde_derive") (v "0.9.14") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.2") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1bzi3dx3pm5qyz6349k4x22nlmrqm22jb4yg6xw2qlhn53vjyiry") (f (quote (("unstable"))))))

(define-public crate-serde_derive-1.0.0 (c (n "serde_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1f59gyph2mv47gbfh58fqq7445wh0ndb962g8a0z7bqsa6wfqqba")))

(define-public crate-serde_derive-0.9.15 (c (n "serde_derive") (v "0.9.15") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "= 0.14.2") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1fkldf0lnl6pwxs00qpyp79m30qmfpi3bk0wm22211ylyikdi3wp") (f (quote (("unstable"))))))

(define-public crate-serde_derive-1.0.1 (c (n "serde_derive") (v "1.0.1") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0q7dzks97j1hq8cvsnbxam87n24x6vnxlc7kw6kgszyjbkyw7wwl")))

(define-public crate-serde_derive-1.0.2 (c (n "serde_derive") (v "1.0.2") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "16my4hcflbi8yqbwglhmf3kvnjxy90nibq4aic01hdcnj23vc1kc")))

(define-public crate-serde_derive-1.0.3 (c (n "serde_derive") (v "1.0.3") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1z4wjax4glj15lcw24y27wm6s72fi5ck32x03g8djdwbbhisxx65")))

(define-public crate-serde_derive-1.0.4 (c (n "serde_derive") (v "1.0.4") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1q1mvwlibvjxb0kxs4hvb22ysa2bd9dk758hn50w6khbkzpixn4z")))

(define-public crate-serde_derive-1.0.5 (c (n "serde_derive") (v "1.0.5") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1na1xf4c3wffbkncfd72byx7lz4hqfw487nvpp3v1980kag871bw")))

(define-public crate-serde_derive-1.0.6 (c (n "serde_derive") (v "1.0.6") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "10hxybb4qlz5y9npmxvvk2ja77dmpdp9mrqk8nd2g901w0ggfvp4")))

(define-public crate-serde_derive-1.0.7 (c (n "serde_derive") (v "1.0.7") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1vkaszlpc6f76vy61wppb1f3436nlbli5aa5w6ha2a85kxdws680")))

(define-public crate-serde_derive-1.0.8 (c (n "serde_derive") (v "1.0.8") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1lq613c89gxzzgca0yqvh4mr90aqghcbmifh08wiyxq0annjym8h")))

(define-public crate-serde_derive-1.0.9 (c (n "serde_derive") (v "1.0.9") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0fb6q14ja8fx3fdn9maig46jrfcjmkrx99m8hnrc7lzb0n4sav1g")))

(define-public crate-serde_derive-1.0.10 (c (n "serde_derive") (v "1.0.10") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "096nxl07spj8aa8dk2fsgx3bnyc8y3g5hc2p9w455f6askq7qw3v")))

(define-public crate-serde_derive-1.0.11 (c (n "serde_derive") (v "1.0.11") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1pbn79y09rapzdg127x39fmk7xn866m4gca7fwzffs72ddq3x0ng")))

(define-public crate-serde_derive-1.0.12 (c (n "serde_derive") (v "1.0.12") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.15.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "13y6mwdb77vwngf40pl77jh520c3h4mrkwx93pd1lxad72jry5ia")))

(define-public crate-serde_derive-1.0.13 (c (n "serde_derive") (v "1.0.13") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1s9s5zdmi0pqn1phamgn72d37y71kjpyvl863vv5wi55vk675kf0")))

(define-public crate-serde_derive-1.0.14 (c (n "serde_derive") (v "1.0.14") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0v7asm02r4ssw1g4nz8f6jccyqh87cj195qigidrm6pxarhgcbw1")))

(define-public crate-serde_derive-1.0.15 (c (n "serde_derive") (v "1.0.15") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1absixl1rvsp7fh7vssq4cc7z5f9dcr64l1i5aalc77xhghamz0s")))

(define-public crate-serde_derive-1.0.16 (c (n "serde_derive") (v "1.0.16") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "03h0s2alj3mbi244dgkn5639acgdx98vml83nq98xg2zh16dal8s")))

(define-public crate-serde_derive-1.0.17 (c (n "serde_derive") (v "1.0.17") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0n45a80lnrnz36l283b952xi8xjaiwhd450pl9jsz7v1b1i7jlb7")))

(define-public crate-serde_derive-1.0.18 (c (n "serde_derive") (v "1.0.18") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.16.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1a3c5vhflsdggd63w66gk97vlzlhvi9lbpblffgmlbgyg7v6183z")))

(define-public crate-serde_derive-1.0.19 (c (n "serde_derive") (v "1.0.19") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.17.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0gj09a0cqv2qbnpdhcqdmma2g35ilp51cnbk36lk20bif4zgxniv")))

(define-public crate-serde_derive-1.0.20 (c (n "serde_derive") (v "1.0.20") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.17.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0jnsdv4ss7mkm3nra9lgq6q174jlhq4qhxbzjxyvkaqqxhb3rkii")))

(define-public crate-serde_derive-1.0.21 (c (n "serde_derive") (v "1.0.21") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.17.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "121pzwpgw06bd6c920jjmrq8wv8mj34ccpkj55c95p4lsqiw6av5")))

(define-public crate-serde_derive-1.0.22 (c (n "serde_derive") (v "1.0.22") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.17.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "0w68x7ky580dyhxvrvfrfgw00xky0rr90fknz305la6b7dzk5a6h")))

(define-public crate-serde_derive-1.0.23 (c (n "serde_derive") (v "1.0.23") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.17.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "18hqflk1s8qlyy09xd0czsliwjiwh7mgmy496qzsqaxh01rxwwh6")))

(define-public crate-serde_derive-1.0.24 (c (n "serde_derive") (v "1.0.24") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.18.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1kk65bj2js2lm74p84lz8ly2vndzr7mh8628q5cvjjbfgfh2xj82")))

(define-public crate-serde_derive-1.0.25 (c (n "serde_derive") (v "1.0.25") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.18.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "16d3hf5qa8svfnz6hgjca6jixm5s3l7dlj24a48d3rw4axngl2zc") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.26 (c (n "serde_derive") (v "1.0.26") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.18.1") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1hab9y54yymrkfi9s1642ymfssnyx88dgzd6s6a4rzwdgn9ymd4m") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.27 (c (n "serde_derive") (v "1.0.27") (d (list (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.19.0") (k 0)) (d (n "syn") (r "^0.11") (f (quote ("visit"))) (d #t) (k 0)))) (h "1l1zpl1jm769mvk50002kf128ml8d33brkgfkvl5adz9ry8pbfpl") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.28 (c (n "serde_derive") (v "1.0.28") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.20.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "1qpgyrswqai9lp5jvbawk9bqa9sk2d5v25gah13wx1vd6ni6dxlm") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.29 (c (n "serde_derive") (v "1.0.29") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.20.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "12lla7awadbas73091kn3kd25hb154q5wv8r7ijbnx2mmq01zcwa") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.30 (c (n "serde_derive") (v "1.0.30") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.20.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "0nnpv56202g352j84rkcirmliqlmp04f4mab52irgcrvhfly5ix5") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.31 (c (n "serde_derive") (v "1.0.31") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.20.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "165610lbir8kv4hd850fq4ap4vjp44lbp7888v9ql74xz1aqgqv1") (f (quote (("deserialize_in_place") ("default")))) (y #t)))

(define-public crate-serde_derive-1.0.32 (c (n "serde_derive") (v "1.0.32") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.20.1") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "0fsgm14md1gy155z148lnnjy236sy63zisil2ybi3dp5ifdyincw") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.33 (c (n "serde_derive") (v "1.0.33") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.21.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "0mm3lw0plf1imqkpgm9f6janz2hzpm08x7czj6bsh6kyrsk67c93") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.34 (c (n "serde_derive") (v "1.0.34") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.22.0") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "1i8j7xbqri8mbkj5b6040dhxcp22y82j2vw8kl38p55ajpcypnl6") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.35 (c (n "serde_derive") (v "1.0.35") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.22.1") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "1gdl3j7glyw9y5w12vfczc8i1whqgh4wagdpnlflclj4g3vziwch") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.36 (c (n "serde_derive") (v "1.0.36") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.22.2") (k 0)) (d (n "syn") (r "^0.12") (f (quote ("visit"))) (d #t) (k 0)))) (h "0234ymdx9lk6v6hfgxwjjarjwkyl2i3hqf1cbfnwxpfv84ny5zvg") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.37 (c (n "serde_derive") (v "1.0.37") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.0") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "13v6z55324frbyxx4907nrxz543xaxm5yhh0vsl1ym08naw1lwgi") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.38 (c (n "serde_derive") (v "1.0.38") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "0l2z43wpk0bcj01zaf6m0i4q6kkav51zl4wfr9dnyf1ryzppyizh") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.39 (c (n "serde_derive") (v "1.0.39") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1agjlyxrby3pn3yy1jfjnddmx0sk395y5kg0hc0vrnmjqn6pzs8n") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.40 (c (n "serde_derive") (v "1.0.40") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1ivxyqdjdjn67a430ym92nlq8rn30gimrl9sxzchwf8l6b4z727v") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.41 (c (n "serde_derive") (v "1.0.41") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1hw94yizxi1a11zx1zy5srqw762p2zw17afm19ssckar2b5ldv41") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.42 (c (n "serde_derive") (v "1.0.42") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "02ywsw41ss1dv9vs4kd4n6z0zzig1vk5072kaha1565vrshcb4mk") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.43 (c (n "serde_derive") (v "1.0.43") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "077dj4yx18p58frgfybzcryrijan1crl3g9bp8kac25hqigkw4da") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.44 (c (n "serde_derive") (v "1.0.44") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "03ip5p4s4avvf2c9c7ksaffydi78nm5gs2frb5wqacajfxwh31sx") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.45 (c (n "serde_derive") (v "1.0.45") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive_internals") (r "= 0.23.1") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1k9zf2ll1ddhv49adykr1llx3i52sraiziyl0x7481lv1q9za284") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.46 (c (n "serde_derive") (v "1.0.46") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1ldckl8xa1mwv1r6iw6d61dzkf5y4bwjvakhj1hcn7hb9g63qbrq") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.47 (c (n "serde_derive") (v "1.0.47") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1wjh3isd0wj1fvymf3njnc2hgni3qpb24wqlwxvgjy9vfq5cgfd0") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.48 (c (n "serde_derive") (v "1.0.48") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1c78414207k8pk2p97gd0dac18v8rhsifnf9a6j05q4y26hnffm4") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.49 (c (n "serde_derive") (v "1.0.49") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "0mwys2lryb57pd7qsgw9r18s3ng06n84ldd8s6k8qnqw1hgywmwd") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.50 (c (n "serde_derive") (v "1.0.50") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "0ybfk4zd2as772bh290674skx0s50z9w6aakisf4r3462qxhjxpf") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.51 (c (n "serde_derive") (v "1.0.51") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1qm3r9241p24wgc7crvcrlj6phkj3kvplp971p39pzn6ps84lqlw") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.52 (c (n "serde_derive") (v "1.0.52") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1m6i5dafhh8j73hrsa4bv4fzy8ixnycvnhcs18xs97bsnmhv4i9y") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.53 (c (n "serde_derive") (v "1.0.53") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1g2nwaqvsgwxn1f4sq3zlqlqmw348bbiz2zkgl79xc07yrxfyjbi") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.54 (c (n "serde_derive") (v "1.0.54") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "0551cx5wvrs551y4d0d9wwxf612gdybhf3ac3gsyd8pmfqdpa8ai") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.55 (c (n "serde_derive") (v "1.0.55") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "1c7dxkk24nj7d4vdfbfqgadp66kmz2hksmffgfkl32ng6pwhw6zm") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.56 (c (n "serde_derive") (v "1.0.56") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "0qw60h0ifixzyzcllbmnc331whw27fbqirwx09ckcxppazc2dsi0") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.57 (c (n "serde_derive") (v "1.0.57") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "11lwnsr16fdni7fnprnf8glhsy21l62hdzz2hdzgzbpi0p1sknqv") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.58 (c (n "serde_derive") (v "1.0.58") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("visit"))) (d #t) (k 0)))) (h "00arsfkz8l7nbsgkkmk3zaiv2pw86say562pahbwsmm5a8dgaf5c") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.59 (c (n "serde_derive") (v "1.0.59") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "08b03jjvw5kd7ycs1396dbid7z5jqlfi36wy11mhqhs6zs563fwl") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.60 (c (n "serde_derive") (v "1.0.60") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1pas8vvc0d6ld22zdcy7s4cx7vhq5s4r2mxplrsz2s5klwl7zkaz") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.61 (c (n "serde_derive") (v "1.0.61") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1aa9da0m9j9aqr3ci7n81p8h46r500ffki8pa4zh92jl1pmahjyh") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.62 (c (n "serde_derive") (v "1.0.62") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "0xsp59pdxca712qrl8zb30kzi74vkizj5np1grzqsff43n2y00wm") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.63 (c (n "serde_derive") (v "1.0.63") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "153pmr70b1hfks1p7yfvdw2lmknlq3jw4f266m45q37j3v90706n") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.64 (c (n "serde_derive") (v "1.0.64") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1n4w8yrhy5lmnraqv8axsmj3b0h58bwbdyh6fpy53q7vlq5n5r3r") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.65 (c (n "serde_derive") (v "1.0.65") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1jzd3i08c14fg6j6nry9mwacpap4kc3lpgrb5a82wshbyzsz1vrm") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.66 (c (n "serde_derive") (v "1.0.66") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "0ygvlf8knzlcsqxk1irc0lazb2h8dgzxbqmgyz1ymxg0lwzj340a") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.67 (c (n "serde_derive") (v "1.0.67") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "082hajg2cmy5awfnjmbsrrq42dwlrwr8pzkz370g6x4hy6nnvks6") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.68 (c (n "serde_derive") (v "1.0.68") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1znbcdmd3lx4ls5a827di5ndxlcqj7idpbc91hc2vv8qz05ss9ba") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.69 (c (n "serde_derive") (v "1.0.69") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "14xmw9ihyn7lmx21v0nizxc23b9wvcplpvh06vkagqvs05l4swnx") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.70 (c (n "serde_derive") (v "1.0.70") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "0911ay4nfjd7x6pqy20j73yirmqwx06zpv5q64q6j21bhdwsf99m") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.71 (c (n "serde_derive") (v "1.0.71") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1fz7535lpn6fd8y8gf0plv541w1k4f2xaii2i4vvqgzpx7awc6dp") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.72 (c (n "serde_derive") (v "1.0.72") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "07y49nqcaj698cx0540yh7pk9pswlpxpy8dm61aj2ghpmlj0gd6c") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.73 (c (n "serde_derive") (v "1.0.73") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1s8yr88zyjz3awn1dxsxjkp1rrdqrga7cz09rwl5ymn0bi6cn9xz") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.74 (c (n "serde_derive") (v "1.0.74") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "060wz41f5q6ik8wszdlk4zhqjxpa7nnw3f1wqpwad3vj09dkgqs7") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.75 (c (n "serde_derive") (v "1.0.75") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "06ci9ac0shx3jg5l79fsrzdf9xchcgy7alb2rn618yvk6yvwhkr3") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.76 (c (n "serde_derive") (v "1.0.76") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("visit"))) (d #t) (k 0)))) (h "1samiradg857ygp0srpk5jxfwdzfwvfl3szfqr7i3dw30qv890vx") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.77 (c (n "serde_derive") (v "1.0.77") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "1vha1f7q46r4piiav064razhil3fnpxmrk72pad1pqp3m8pwasam") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.78 (c (n "serde_derive") (v "1.0.78") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "1zy0k8g227wswyga3fz1bz9sa7ym9z9b422gglrdbba5abc25dhg") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.79 (c (n "serde_derive") (v "1.0.79") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "1zk7yp2vnsggi57sn47zka0mk4pyfwqpk7s7m7vvzbs52289smii") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.80 (c (n "serde_derive") (v "1.0.80") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "17146b0373s4wv75z17hgrls39wlzh1a6765k0wfqarhqq3y6p92") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.81 (c (n "serde_derive") (v "1.0.81") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "119qja4pb8dr8in6yijgx0f184hdalxzrvavr5pvbdgm8sv16ys7") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.82 (c (n "serde_derive") (v "1.0.82") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0m311ndh887dfj48mr7vvh451z7qaixhiylsjasjsnn6d94zk9wn") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.83 (c (n "serde_derive") (v "1.0.83") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0lb9szwfhhqd2s33rxr0k5qjz9rz1h9hw69wmwnsyza90abq4scl") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.84 (c (n "serde_derive") (v "1.0.84") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0nwslcdnyzaq14mw85dwmamdb80nzid351ci81724p527id13mml") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.85 (c (n "serde_derive") (v "1.0.85") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1d37cp63imhw8h0qmqn2phvampmy4g14hwb9jxvn1i8s1xmk05d9") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.86 (c (n "serde_derive") (v "1.0.86") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "00mp2ngm0rp7rs86kp4d3lp9qnpmya11h8jmfqrny93i24gcgsji") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.87 (c (n "serde_derive") (v "1.0.87") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1shlh5zbzci7ib9jvwy7v68msk7xlk3xfapvkysiirb7cn2rfgk3") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.88 (c (n "serde_derive") (v "1.0.88") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1m0djw7cix5z0ika4qnra39786rvxxh7rrbhlqxyynhpypk1ivdy") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.89 (c (n "serde_derive") (v "1.0.89") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0v40dcyfqx8r70frd3dypg0wp8prr6vhn97a9vi8hklinpsanvmv") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.90 (c (n "serde_derive") (v "1.0.90") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0yfysbp1l27rcyh4dld92nd6wwpmidfb8qqr7nr6iwa4qaz85z2q") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.91 (c (n "serde_derive") (v "1.0.91") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "17rzl9gcs2z8bhf845dsnvgpqlli4gv4rr6bqjl3qgls21dlj6qh") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.92 (c (n "serde_derive") (v "1.0.92") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0gmfigimvhmkx0k6vl35ipxxn5rjkrakwbhd3jv3dacv1hyj58s6") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.93 (c (n "serde_derive") (v "1.0.93") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1190h1fxv8hm74zjvx9kdhajc4ky4sh39gwh5qkkigcn6rkfdk64") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.94 (c (n "serde_derive") (v "1.0.94") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0yzy4wki4v4l39fc42q5lrdigh5pk0vjhvg1z7sj4fs6srwynigg") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.95 (c (n "serde_derive") (v "1.0.95") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "1kkv5zh8cs0v5g9m46pbwm8cwqmx62xmh37pmrsr51cxaj8ypa2y") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.96 (c (n "serde_derive") (v "1.0.96") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0g75cczs4r6vvad5na08jy9g93zqrldrfj542xiymmh1pnrlpxpv") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.97 (c (n "serde_derive") (v "1.0.97") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "0zq2qm2gabmpa57wxfxb09jl41nxccsk454715xjabzymlh0han2") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.98 (c (n "serde_derive") (v "1.0.98") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (f (quote ("visit"))) (d #t) (k 0)))) (h "172jgdv5csxmmsycs16pqr9qd0bvfn65n9zfcxa287v3i8drxrh1") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.99 (c (n "serde_derive") (v "1.0.99") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "09dll6gd5fnma52mgf71w4vl6br3l3x9hv11k3f0hsr0c66c2kfb") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.100 (c (n "serde_derive") (v "1.0.100") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1fqdf5rnbhxsdx13cy6lzp88lxs0r5mx5462kxw7s59ywkyi1r0i") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.101 (c (n "serde_derive") (v "1.0.101") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0bn0wz3j48248187mfmypyqnh73mq734snxxhr05vmgcl51kl4sb") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.102 (c (n "serde_derive") (v "1.0.102") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1y1b1bx786xd65fq73w98i2g9wx9mfrzn8w948i36y9ghcdgq4ya") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.103 (c (n "serde_derive") (v "1.0.103") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1l2icqq548dmq5bn278zb2vj725znj4h4ms89w3b0r1fkbpzmim8") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.104 (c (n "serde_derive") (v "1.0.104") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0r7gjlwfry44b4ylz524ynjp9v3qiwdj4c588lh94aas78q9x3qj") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.105 (c (n "serde_derive") (v "1.0.105") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1y5gzwpy8yjv9pwh1js11vr18nfz4gg1g2kmyr6p58hvavy00pdc") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.106 (c (n "serde_derive") (v "1.0.106") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0v7a2rkpx9hi70pv2wr2h0h07rgmr7gi37v0s4dn5f2gpwx9wm4y") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.107 (c (n "serde_derive") (v "1.0.107") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1wrrgyrx5f428nms6y0cpw2ddp7sgffizizwiawd95sm5vi4bghh") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.108 (c (n "serde_derive") (v "1.0.108") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1430jwjy9m7a69ibfm5liwlqdnpyz5s4mq77hxp9ifk1iax6wcba") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.109 (c (n "serde_derive") (v "1.0.109") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0zl1jb6isx1g1rh8355znawvfy233i6dgg12bffy644rp1dy8cbi") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.110 (c (n "serde_derive") (v "1.0.110") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "110rfxlcli1dm9bif7f7pqavkiq0m93qa56arazx6hlsz9mvz3w1") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.111 (c (n "serde_derive") (v "1.0.111") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0l5j3v7g4lj18w4iifzz7jp83gv206a2645yp209q7nawv43lb1z") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.112 (c (n "serde_derive") (v "1.0.112") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0mrz1720qfgnc3v3739wadmy1vy9x741lffrmzbd7h1a477460xz") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.113 (c (n "serde_derive") (v "1.0.113") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0cdf4r6bwxw6cwg2fj93kwd7mypwhjfzxzyw3i4cnm09gnhymick") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.114 (c (n "serde_derive") (v "1.0.114") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "13lgjxsc617yhblm779jwg43gxab2dfgrpyd6znvl3v90i5yj2ra") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.115 (c (n "serde_derive") (v "1.0.115") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0j6w64m3z7kaagq6j0qmn7q84jkvnfll1a4205mc6g57s38yx7v0") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.116 (c (n "serde_derive") (v "1.0.116") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "1s4sbl32lk1afxryfax73clvq22lwzdgzljb7f3mgr6q1wvscc7n") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.117 (c (n "serde_derive") (v "1.0.117") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0kn7ais3zv9ajbyc216qm14r61zwlm229815yd4anjmlmmraxlfb") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.118 (c (n "serde_derive") (v "1.0.118") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "1pvj4v8k107ichsnm7jgm9kxyi2lf971x52bmxhm5mcwd4k3akf8") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.119 (c (n "serde_derive") (v "1.0.119") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.58") (f (quote ("visit"))) (d #t) (k 0)))) (h "1kg1zyp1h0801hw70j97nb51bgbl4dcp3hk8zpaxsnd0g7758aam") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.120 (c (n "serde_derive") (v "1.0.120") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.58") (f (quote ("visit"))) (d #t) (k 0)))) (h "0xaprf386crlji08v4rdfv6cwlxmnxjkfpj3jlxrxkh5b35si8hc") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.121 (c (n "serde_derive") (v "1.0.121") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.58") (f (quote ("visit"))) (d #t) (k 0)))) (h "0ikwybhifif9g6rjy03l5f8kqvwrckmy9cpsbk31nrfwg23spz7k") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.122 (c (n "serde_derive") (v "1.0.122") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "05wf294rrz1xvcmjk1vylr7lcwn478mi3c7c831qm3431wq1zvld") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.123 (c (n "serde_derive") (v "1.0.123") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0ccg4m7ww6mfs5vjdbdifri2kf1wyd4difjnqnraph2gssaw54ck") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.124 (c (n "serde_derive") (v "1.0.124") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "16q6k7zmc7pcbl4pmngpmw4aj3apw4d2ja2swbsqdqcl7rlzf00q") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.125 (c (n "serde_derive") (v "1.0.125") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0vfhndim4sa1i1x38dyvrxyq5v8zxjs0av05ldfkn82qpfibg4xh") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.126 (c (n "serde_derive") (v "1.0.126") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0hsdh39qj0g187nwndfzg67q4qajbm5g6x0fr5xarblmk2y7sfln") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.127 (c (n "serde_derive") (v "1.0.127") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1k2ywp5qg490x5kxv7xljfj5a4x54krf15w56836cl9j6inr4950") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.128 (c (n "serde_derive") (v "1.0.128") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1i52jm4sqnvlx5ahrqf8ghv71262jlh6v9bjdh6rba30ifxjzbqk") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.129 (c (n "serde_derive") (v "1.0.129") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1ws9lbqbn36wn22vyyil9rhq7hns19nm32sm4xjabn9ksmxfhyp5") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.130 (c (n "serde_derive") (v "1.0.130") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "12shxhi47db54i4j44ic2nl299x5p89ngna0w3m6854nn4d1mg6p") (f (quote (("deserialize_in_place") ("default"))))))

(define-public crate-serde_derive-1.0.131 (c (n "serde_derive") (v "1.0.131") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1ljqafasqrm8zmipnjdfsgjsk75dfi9bjihra4ynmzqd9qyah45p") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.132 (c (n "serde_derive") (v "1.0.132") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0xk2c38j9421wnbgcv7n7zl72i5cyvfbpnc7i1ahwv2mn9fdph7c") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.133 (c (n "serde_derive") (v "1.0.133") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0dym0l8a0pch0mkqnhrf89n4wngzwf0d1z88hb8dhs456acic87d") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.134 (c (n "serde_derive") (v "1.0.134") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0gb5rdbn98wac3d32j70rw1yklcaxiq0syskfw81kzhkzbxx2kkq") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.135 (c (n "serde_derive") (v "1.0.135") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "139jv2fya2180d1vv1y4934b2pdky3cffjg29g077jflhwyy1kcd") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.136 (c (n "serde_derive") (v "1.0.136") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1yb28smlymba4qbj2bn4c4myvblypqvkxv9q33s0dlzwa9qpwn88") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.137 (c (n "serde_derive") (v "1.0.137") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "1gkqhpw86zvppd0lwa8ljzpglwczxq3d7cnkfwirfn9r1jxgl9hz") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.138 (c (n "serde_derive") (v "1.0.138") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0z7jns3j63dvavizqap2yw781addf0c629cgp07s3y5fcwa9ngh2") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.139 (c (n "serde_derive") (v "1.0.139") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "1yrxaj1jviv99z14mn59sppmbgc4szhzp3xdb2pk4yfyq4q347fw") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.140 (c (n "serde_derive") (v "1.0.140") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "1nh59a45hcbidkal0nfln76l5xfkyby9jl5j3hgviqwzddij48bg") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.141 (c (n "serde_derive") (v "1.0.141") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0gs1lf982wsfjlg2yqsk5h8g4wqdmny0kj0xnmixhg801laklx3m") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.142 (c (n "serde_derive") (v "1.0.142") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0bnciicgb929l10s19p5q020il8y5ipyrky273sh5gms17cbid9l") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.143 (c (n "serde_derive") (v "1.0.143") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "14c3p1d87mswxfnz6ly7ah20sscvwpslgfc5dg167vksapgfin6k") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.144 (c (n "serde_derive") (v "1.0.144") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "003sx1ihjcfwj2az357qj7i38b1ji3w8krw35y0h3ldidy0kmvcl") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.145 (c (n "serde_derive") (v "1.0.145") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0k1baapz2qbd9i6x08saryngdc7m47z0syi79kcarg6isf21byl1") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.146 (c (n "serde_derive") (v "1.0.146") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "01b2nd3wmzscrh9xwiwhix9jv4p917cangfmwxy08rhxp8rgs557") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.147 (c (n "serde_derive") (v "1.0.147") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0ln8rqbybpxmk4fvh6lgm75acs1d8x90fi44fhx3x77wm0n3c7ag") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.148 (c (n "serde_derive") (v "1.0.148") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "031cp7vd2zq479zjrzpwpfzhphilgng30dv1pyx22dd5b9194m55") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.149 (c (n "serde_derive") (v "1.0.149") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "1r4l1ww0hka44y5h0z8j6p4nh6vaphrysqml1razvzdz9jqfksml") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.150 (c (n "serde_derive") (v "1.0.150") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "0zk2vclg2qq3nlj518a8bbbiy7ysp4xddnpaim334dvin0jxz8s2") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.151 (c (n "serde_derive") (v "1.0.151") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "1n4sf4n7i80vq84fciclp7qb3y8j1hr7ns0d342mr62s2adbwni5") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.152 (c (n "serde_derive") (v "1.0.152") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "07jlbk3khspawlqayr5lhzgqirv031ap4p8asw199l7ciq8psj5g") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.153 (c (n "serde_derive") (v "1.0.153") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "0kwmh3c8jna1vz3n8gi833ahws33bmp6nwk6pkn6s3qgg6jpdx0y") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.154 (c (n "serde_derive") (v "1.0.154") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "05rjwflrzyabv0sn0d7h70cxri7ssqyagjf2zp53ni9m55r0vj2g") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.155 (c (n "serde_derive") (v "1.0.155") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "0c3ndnvvc8iqjnqr0lcc7x4g6h1zwc8z99r3s1lzyjmc7x5ajwfh") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.156 (c (n "serde_derive") (v "1.0.156") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.104") (d #t) (k 0)))) (h "0z88gj1imji06pwll6il2qvcvx4mwzf2hci29b3wwsz30539rqnp") (f (quote (("deserialize_in_place") ("default")))) (r "1.31")))

(define-public crate-serde_derive-1.0.157 (c (n "serde_derive") (v "1.0.157") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1rg1jrifh3byywcxwmpxk9hp0699c6kc8h2545qpjan2am2pz6bq") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.158 (c (n "serde_derive") (v "1.0.158") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1b9cqqbrg796hi9l4f4kw0qbnkm31hgcfsk9ny15ais85xqw20g8") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.159 (c (n "serde_derive") (v "1.0.159") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "11c59pxvq01mv12ixflzymgg0jz4wwwi7da7n7s3s2avh0blsqac") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.160 (c (n "serde_derive") (v "1.0.160") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1pvpiml328jhsz5h3pdc7x7wcyaagab6l5hb2q07wjfqcdy0j6i9") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.161 (c (n "serde_derive") (v "1.0.161") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1n3rxm5hdfiqdpg4jlh1lvs01r13m51k8qj4gq3whv1rwh7pi8vf") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.162 (c (n "serde_derive") (v "1.0.162") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1diwx4c86b63mgmzbd5nvj8imjwhipm48jlhi62bar7xa91q3852") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.163 (c (n "serde_derive") (v "1.0.163") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0gm97qsj056dhzphafdn733vq922l8aj6q4w721qh34kwdvmg04c") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.164 (c (n "serde_derive") (v "1.0.164") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0s0xccdxvz8ilr4jqqgw8iqkfnvjx6i90kciys5w4lfciiimnwyr") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.165 (c (n "serde_derive") (v "1.0.165") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)))) (h "1ms3scznmgcvhjslq86xb4g0jwhfcvknbzvmkc0y9zszw8hfkakf") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.166 (c (n "serde_derive") (v "1.0.166") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)))) (h "1mn6hcs8p04mcfn5wwsgx7gdw5l8rsnd3n8ldr32ssrbvrnkvn2x") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.167 (c (n "serde_derive") (v "1.0.167") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)))) (h "1ff4h8zqb93fca1cl5cjryjzhh20k0chvrvlx7q5905wd1mi16xn") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.168 (c (n "serde_derive") (v "1.0.168") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.21") (d #t) (k 0)))) (h "0hfnd7c6i7vbbcfpzp3if9sr1lg25qajfkysfx0y9266g2b5iznl") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.169 (c (n "serde_derive") (v "1.0.169") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.24") (d #t) (k 0)))) (h "1zblx80lq3k3rr01whx1z0m35i1yybaz60gdqdrani6rl3z8qwr7") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.170 (c (n "serde_derive") (v "1.0.170") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "1f5pi2bh1f5kmf4rrz8ppialb1mdb9ypg5xk1nxas8vbis27gm3p") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.171 (c (n "serde_derive") (v "1.0.171") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)))) (h "10j6s97fk7fgjiqhhrx6a44rqxr7v3w985i3avx4d36i7dh9961q") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.172-alpha.0 (c (n "serde_derive") (v "1.0.172-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "00liqqgzl2ai6g4qgyqf0akrqn4r2lrm0ks2wr4nm01xjz2n0y61") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.172 (c (n "serde_derive") (v "1.0.172") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "128zz5dx17jqrfg48v2518y6091in3nk461jjahf8mb5n93w2zyk") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.173 (c (n "serde_derive") (v "1.0.173") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0jfygh09mn80pkfrppg5lfsww2m4gbds775y68i27q22hgg0s9d6") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.174 (c (n "serde_derive") (v "1.0.174") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0bp6xmmdmbx9b41imww20man8jjcvhxsd5bgad9qx5vzihlklp3f") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.175 (c (n "serde_derive") (v "1.0.175") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "1d0ar34z5gbincb8mdgaa3sl242cp6wdsn1qsv0161hidzg7lgxj") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.176 (c (n "serde_derive") (v "1.0.176") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0vrhg9713nipmzz09pknmpji9k8rhj0d7w8zcn83ngl2vk2virx4") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.177 (c (n "serde_derive") (v "1.0.177") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "1lvgph51y9pxibmjcbvpk6dys3kwwv5vzinyzq4j3mrkg3z9f5s0") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.178 (c (n "serde_derive") (v "1.0.178") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0sspxpbvv7nqbhbcdz52npk1nbcj8y31bnkkn964ahb6ilqq517j") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.179 (c (n "serde_derive") (v "1.0.179") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0b7cl13dql0phgrjrwdvyhkkbzqb6agzfhxh0c6fdiw5ai7i47kl") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.180 (c (n "serde_derive") (v "1.0.180") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0dlh47jnacckz53kzvdvb87cqnbid42yyrrjnyrnls1bg3bl9rr4") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.181 (c (n "serde_derive") (v "1.0.181") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "1vbpp686q8vcdbjc4bc8l2qxvnjpkxsbvg6gpchfr9fk1k5zc0my") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.182 (c (n "serde_derive") (v "1.0.182") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "0w4asf95p2khl3rwh9nn368ivvzwn36sybh1372ba2dwlip2qk3g") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.183 (c (n "serde_derive") (v "1.0.183") (d (list (d (n "proc-macro2") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "quote") (r "^1") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (t "cfg(not(all(target_arch = \"x86_64\", target_os = \"linux\", target_env = \"gnu\")))") (k 0)))) (h "05j8kbch6wpjk6m7kcgnx3bwkcsg5pp2zfci3bkvxfdhc0nrgzma") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.184 (c (n "serde_derive") (v "1.0.184") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "17p06z9bw7f33ikpliaa22xnskxkgzvy5ch9cshas1llnbsjgpy1") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.185 (c (n "serde_derive") (v "1.0.185") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1v4ql8v4axj7mx3gr7vdl7ha545hlkz6f0sy91rpfhyipbfdynfw") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.186 (c (n "serde_derive") (v "1.0.186") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0w76p0brqqqkgrmbylb6qg9yhmxksmpgbs2c7acg8nmnw3vrgmjs") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.187 (c (n "serde_derive") (v "1.0.187") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1f2yc8yvqbf1c72sg8c2kpbhc839sj7gf4hva3ag3cvqlmn2lsz4") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.188 (c (n "serde_derive") (v "1.0.188") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1wjaclvsfxgqnnnykllvb5gffsxynk66x6h4c1ds6anq8b37mjjf") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.189 (c (n "serde_derive") (v "1.0.189") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1m9j5f5dd010arr75mipj4ykngk1ipv8qdqialaf770033wx2j0y") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.190 (c (n "serde_derive") (v "1.0.190") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1qy0697y6rbsqvaq7sgy8bpq1sh4h13xmvsizkbjnp2f76gn1ib7") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.191 (c (n "serde_derive") (v "1.0.191") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1zdwpz7i5jh0imy2wkmvv8wfmlj9q2qyb7qqh2v93kkbckam5yj6") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.192 (c (n "serde_derive") (v "1.0.192") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1hgvm47ffd748sx22z1da7mgcfjmpr60gqzkff0a9yn9przj1iyn") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.193 (c (n "serde_derive") (v "1.0.193") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1lwlx2k7wxr1v160kpyqjfabs37gm1yxqg65383rnyrm06jnqms3") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.194 (c (n "serde_derive") (v "1.0.194") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1q6l0ycrykdg960c350fgnac6d653q1v608g84qrk3rf692mwf53") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.195 (c (n "serde_derive") (v "1.0.195") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0b7ag1qm9q3fgwlmyk2ap5gjbqa9vyf2wfmj4xish6yq0f38zzj6") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.196 (c (n "serde_derive") (v "1.0.196") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0rybziqrfaxkaxrybkhrps7zv3ibxnjdk0fwais16zayr5h57j1k") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.197 (c (n "serde_derive") (v "1.0.197") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "02v1x0sdv8qy06lpr6by4ar1n3jz3hmab15cgimpzhgd895v7c3y") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.198 (c (n "serde_derive") (v "1.0.198") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1nb6hn1xpvhih00x6jkfm42na6pwz59h2zayj2x865xhd6wdm3p8") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.199 (c (n "serde_derive") (v "1.0.199") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1z5psgjcf14i14wsg6p88xxqh53pdzi4mlm65kj43qa1cmx2bg8i") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.200 (c (n "serde_derive") (v "1.0.200") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1jrb95pssal86p6kbh188p14893mvgn75mafr74f7kh0jimh8vw5") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.201 (c (n "serde_derive") (v "1.0.201") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0r98v8h47s7zhml7gz0sl6wv82vyzh1hv27f1g0g35lp1f9hbr65") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.202 (c (n "serde_derive") (v "1.0.202") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0f28ghhyilpfn8bggs9vpm9z2015ld0fswnr9h4nkzxw0j08aj30") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

(define-public crate-serde_derive-1.0.203 (c (n "serde_derive") (v "1.0.203") (d (list (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1fmmqmfza3mwxb1v80737dj01gznrh8mhgqgylkndx5npq7bq32h") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

