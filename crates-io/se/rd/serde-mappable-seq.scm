(define-module (crates-io se rd serde-mappable-seq) #:use-module (crates-io))

(define-public crate-serde-mappable-seq-0.1.0 (c (n "serde-mappable-seq") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1zmfzhmfaxlylx98qlddq0ndlmm26j0c9blx41psdvbbry2z2nn4")))

