(define-module (crates-io se rd serde_version_derive) #:use-module (crates-io))

(define-public crate-serde_version_derive-0.1.0 (c (n "serde_version_derive") (v "0.1.0") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1x6sf81xi5rmc780h9a26b271j30abg8sglxh6xr2l3jlxkrv6yp")))

(define-public crate-serde_version_derive-0.2.0 (c (n "serde_version_derive") (v "0.2.0") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0hd1scwk7n79dmhv134vmhq8i44v14dnzg5lmbgdf3n22fvl0g2w")))

(define-public crate-serde_version_derive-0.2.1 (c (n "serde_version_derive") (v "0.2.1") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1nsmr2z8jzs2l64n29si4i0ppp0yws3xw343cdijqx0dfkaa0bi0")))

(define-public crate-serde_version_derive-0.2.3 (c (n "serde_version_derive") (v "0.2.3") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1vn0vblmc7clbw7w8v81nz5nfrydfa6c7wyz9fq92npxvn99lcwh")))

(define-public crate-serde_version_derive-0.3.0 (c (n "serde_version_derive") (v "0.3.0") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "10bm0ka16321k9446xp38g3q8g1bw3r06kcd6hcp4hr3ppigymrl")))

(define-public crate-serde_version_derive-0.3.1 (c (n "serde_version_derive") (v "0.3.1") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1b2n8wiq3gg8hjx2yl6hglf3zrljpzr3pzzkj3wxh4dmq8w7y4q2")))

(define-public crate-serde_version_derive-0.4.0 (c (n "serde_version_derive") (v "0.4.0") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "192wn7kbqawa3f30gic5ijng09wdkn4nw7sw6xal29v3815sfd57")))

(define-public crate-serde_version_derive-0.4.1 (c (n "serde_version_derive") (v "0.4.1") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "16s276dq92m0q7zv299aw09sfiswx5bpvz0ynmh9qimmdw1q9ra7")))

(define-public crate-serde_version_derive-0.4.2 (c (n "serde_version_derive") (v "0.4.2") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0dyncg8zqq33bgcyndavw76a2jq869q33a7zzkdchjwyphc2m6bp")))

(define-public crate-serde_version_derive-0.5.0 (c (n "serde_version_derive") (v "0.5.0") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0sqrs89cd3m8j9sqw27l1qvnr4pq87gywj0qfmh762pnb5bi0x5i")))

(define-public crate-serde_version_derive-0.5.1 (c (n "serde_version_derive") (v "0.5.1") (d (list (d (n "proc-macro-util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1bk14mkd71ra8prcigj6nkhbv8v5z93vv4z0d8ia76ijv9sccf4q")))

