(define-module (crates-io se rd serde_yang) #:use-module (crates-io))

(define-public crate-serde_yang-0.1.0 (c (n "serde_yang") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)))) (h "0f2dzssaamf6is2clh4w93h9rjzzrf7lcsn22par78r8g4qgk9ls")))

(define-public crate-serde_yang-0.2.0 (c (n "serde_yang") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)))) (h "1qrqrgdmykzxkhb9wlai881incf8lmvgrrmf9av908p8jk3368s8")))

