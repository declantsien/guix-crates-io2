(define-module (crates-io se rd serde-json-core) #:use-module (crates-io))

(define-public crate-serde-json-core-0.0.1 (c (n "serde-json-core") (v "0.0.1") (d (list (d (n "heapless") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 2)))) (h "1v7npy9xmjgmc5ci1nhxahv93lz0542jx9gr87frw59216cf11z2") (f (quote (("std" "serde/std"))))))

(define-public crate-serde-json-core-0.1.0 (c (n "serde-json-core") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "09cqsbpcl25y61ha2j7w93rlz6b6mhj76rvqr8kg77nsb900dx6b") (f (quote (("std" "serde/std"))))))

(define-public crate-serde-json-core-0.2.0 (c (n "serde-json-core") (v "0.2.0") (d (list (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0lr1pbigc07kxyv9f304379i14lhdpvh39vdys2v8j81l0b61zc9") (f (quote (("std" "serde/std") ("custom-error-messages"))))))

(define-public crate-serde-json-core-0.3.0 (c (n "serde-json-core") (v "0.3.0") (d (list (d (n "heapless") (r "^0.6.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "11pi3mv6j2017xfav7hhdz6583x1r3fn71lilz4x4a1a1ks1gbrr") (f (quote (("std" "serde/std") ("custom-error-messages"))))))

(define-public crate-serde-json-core-0.4.0 (c (n "serde-json-core") (v "0.4.0") (d (list (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "10a7k6ly9ly4xkk30nqq2js7acrg7wjl73bpg43hzjib4zmaw540") (f (quote (("std" "serde/std") ("default" "heapless") ("custom-error-messages" "heapless"))))))

(define-public crate-serde-json-core-0.5.0 (c (n "serde-json-core") (v "0.5.0") (d (list (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 2)))) (h "1w2g6ylmxbj9fbz4f86i0nnyk2mbv7qbksx0mbimxx17wj7krv2q") (f (quote (("std" "serde/std") ("default" "heapless") ("custom-error-messages" "heapless")))) (r "1.55.0")))

(define-public crate-serde-json-core-0.5.1 (c (n "serde-json-core") (v "0.5.1") (d (list (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (k 0)) (d (n "serde_derive") (r "^1.0.100") (d #t) (k 2)))) (h "0cxv1cp244f7mahw3snmdq91s44py3jwf3lj6i643g606fsim7iw") (f (quote (("std" "serde/std") ("default" "heapless") ("custom-error-messages" "heapless")))) (r "1.56.0")))

