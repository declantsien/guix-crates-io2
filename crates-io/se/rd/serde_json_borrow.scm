(define-module (crates-io se rd serde_json_borrow) #:use-module (crates-io))

(define-public crate-serde_json_borrow-0.1.0 (c (n "serde_json_borrow") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "05zprm5lbirf3n0b2p7730sapnmjs5l2y4cl5rs0wsbkh1ihqnam")))

(define-public crate-serde_json_borrow-0.1.1 (c (n "serde_json_borrow") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0d2g9hljz1s8kacminklfv4ykm7g00nr2p9jnf6zdm26a5y41gv1")))

(define-public crate-serde_json_borrow-0.1.2 (c (n "serde_json_borrow") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0chbbbjj633c3lz7gwcbvq2950jaawszf5ik6nbjmak6b600fnn7")))

(define-public crate-serde_json_borrow-0.1.3 (c (n "serde_json_borrow") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0aa152147rsymxcmr73pdw9ppp607vny0sdy3grpn1jn7ifylcsq")))

(define-public crate-serde_json_borrow-0.1.4 (c (n "serde_json_borrow") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "1yacs7kpgh3n97nzl3map9psxg3iigxynv24vmx679dyl804xrzx")))

(define-public crate-serde_json_borrow-0.2.0 (c (n "serde_json_borrow") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "0xnccmfhh8d8djpyvqn0c3vi3wsdvk00p2yml7w3kwxs1igy7rzj")))

(define-public crate-serde_json_borrow-0.3.0 (c (n "serde_json_borrow") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "18jsc5n98c81x7nv9r1311dviipmxk8rrpnqy4j8z3q6b88xwxxi")))

(define-public crate-serde_json_borrow-0.4.0 (c (n "serde_json_borrow") (v "0.4.0") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "15jy0b5n0xrdpfvli1bxwxra8721ib4xbf8prkpmkmrj4qihyw13")))

(define-public crate-serde_json_borrow-0.4.1 (c (n "serde_json_borrow") (v "0.4.1") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "1b4z3rmqzjlnhq50fk8prvjj31qyq9s77bn93r6zcfrhkxjk3xjv")))

(define-public crate-serde_json_borrow-0.4.2 (c (n "serde_json_borrow") (v "0.4.2") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "1mfnnj5hhkz41a8yp9vnrf5n5s55y4fajplqchs9b3mr54qw3bnq")))

(define-public crate-serde_json_borrow-0.4.3 (c (n "serde_json_borrow") (v "0.4.3") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "12gycmh84rv00jxnxn6p318sbysarvmchcd0d44sg4jlkxqc4ikw")))

(define-public crate-serde_json_borrow-0.4.4 (c (n "serde_json_borrow") (v "0.4.4") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "1in5irm8djfwppssqaakdqhhv8y0gdiqd26vh4pddf71abm9h85g")))

(define-public crate-serde_json_borrow-0.4.5 (c (n "serde_json_borrow") (v "0.4.5") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "0sbjw0r8kzm862015wi90abppq0w4ikww6bznqfm4ymfvllyf36n")))

(define-public crate-serde_json_borrow-0.4.6 (c (n "serde_json_borrow") (v "0.4.6") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "1bx7h4dh07j2dpscs9qp02ylgkpfgjzxfp4wvjdcfxgzjlg202rp")))

(define-public crate-serde_json_borrow-0.4.7 (c (n "serde_json_borrow") (v "0.4.7") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "10hipy8y2sm7ccc2cdxlfdnfqixi3vm7nxw392xi84xlaxcc0pwg")))

(define-public crate-serde_json_borrow-0.5.0 (c (n "serde_json_borrow") (v "0.5.0") (d (list (d (n "binggan") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 2)))) (h "0g0zjzy8dcq3yd5p07cy371krb9cz8mmmjq2ajby2k35y5zhjmm2") (f (quote (("default" "cowkeys") ("cowkeys"))))))

