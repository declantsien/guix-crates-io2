(define-module (crates-io se rd serde-lite-derive) #:use-module (crates-io))

(define-public crate-serde-lite-derive-0.1.0 (c (n "serde-lite-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03i94fxxlrrah9vxkgi2p1hr23cq2185f93qzza33iwxpa5d5rph")))

(define-public crate-serde-lite-derive-0.1.1 (c (n "serde-lite-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m8dpaqn9xnjdq4swqz96f0q24m6k8141kbwsms3p98wwfbsl2fd")))

(define-public crate-serde-lite-derive-0.2.0 (c (n "serde-lite-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xgs1g3qphxsr4z3f7ncmkxl3sk1jvlxm27xincnh5lmawwvy893")))

(define-public crate-serde-lite-derive-0.3.0 (c (n "serde-lite-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nmsqj8wdmgc8n47gndbdrncl98vdlgy0sbhk8m2hviymxmg013l")))

(define-public crate-serde-lite-derive-0.3.1 (c (n "serde-lite-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1q6ax6v9cymb7jrhpbxyilbhjlvma6x5yn65yvsam9fxbp208md6")))

(define-public crate-serde-lite-derive-0.3.2 (c (n "serde-lite-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rfrdf3y3hs1a35g66afyswgv9c68isgl70512v63fqla4ai86ia")))

(define-public crate-serde-lite-derive-0.4.0 (c (n "serde-lite-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jm0k4nrwq4kzqv2zz9nbrbhm17vjs0ckj7i5f1p3yhnjiibrayx")))

(define-public crate-serde-lite-derive-0.5.0 (c (n "serde-lite-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "02vnsrc7gpd8ksa6bvlgbrfqyaqkb1fagxhw619hilfqwf26mqkw")))

