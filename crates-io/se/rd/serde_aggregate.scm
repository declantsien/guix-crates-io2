(define-module (crates-io se rd serde_aggregate) #:use-module (crates-io))

(define-public crate-serde_aggregate-0.1.0 (c (n "serde_aggregate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "039p2s90c2f4hgwp96hsp2q2xryxwr3kz9ir5a3r6f8aaw29hyvr") (y #t) (r "1.65")))

(define-public crate-serde_aggregate-0.1.1 (c (n "serde_aggregate") (v "0.1.1") (h "161jlmrd3qc98fvbz1yfnb7apfci0lafz9iv2za7294g6mpilw2q") (y #t)))

(define-public crate-serde_aggregate-0.1.2 (c (n "serde_aggregate") (v "0.1.2") (h "12ihix9ng1fr6qgj3lxw9ljfk3jvdig1alkx8xcnv2nz902zhjgv") (y #t)))

