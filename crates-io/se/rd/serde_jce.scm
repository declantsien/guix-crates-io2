(define-module (crates-io se rd serde_jce) #:use-module (crates-io))

(define-public crate-serde_jce-0.1.0 (c (n "serde_jce") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0pp4lj3mxnfqq6wvs9dv46d0a7sspmxzryy1sym26rhjg9wr4y0k") (r "1.56")))

