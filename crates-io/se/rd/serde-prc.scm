(define-module (crates-io se rd serde-prc) #:use-module (crates-io))

(define-public crate-serde-prc-0.1.0 (c (n "serde-prc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "hash40") (r "^1.3.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0b6c2a1n7gj7sfgif1sfpg8ldx9svck4vaflwwwv7ifdghmxm8as")))

(define-public crate-serde-prc-0.1.1 (c (n "serde-prc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "hash40") (r "^1.3.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1ndvpcnjalark5df6xsn43024lx4yc9y8chzdizwmwym9zai65p0")))

