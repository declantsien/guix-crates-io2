(define-module (crates-io se rd serde_derive_state_internals) #:use-module (crates-io))

(define-public crate-serde_derive_state_internals-0.1.0 (c (n "serde_derive_state_internals") (v "0.1.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1p406yzdv7jx8f43xss8myj4hd2giwbzdnjp8wic4dqf4h82r9bd")))

(define-public crate-serde_derive_state_internals-0.2.0 (c (n "serde_derive_state_internals") (v "0.2.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "060m616i28ay301g91gplv3b2c75ky0svs6416mzz9hcqfzdlc9a")))

(define-public crate-serde_derive_state_internals-0.3.0 (c (n "serde_derive_state_internals") (v "0.3.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "0snphjdf7i5305mgdlwd2b9khjiymhzsdi5jdqd4c31p1zcmygxz")))

(define-public crate-serde_derive_state_internals-0.4.0 (c (n "serde_derive_state_internals") (v "0.4.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1yabb3fj8hg6d55kgzdnwblg92bq1mbqid041yd8i9r488zyblv1")))

(define-public crate-serde_derive_state_internals-0.4.1 (c (n "serde_derive_state_internals") (v "0.4.1") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "0li2r14yv7a8l1j0q4j2jf8v105mw5irq7c6pvq7j53xscx8kayz")))

(define-public crate-serde_derive_state_internals-0.4.2 (c (n "serde_derive_state_internals") (v "0.4.2") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1alsywrlagh67f8m2h39b9f9jila4j4wssdp4di7qm70b2wp5cbw")))

(define-public crate-serde_derive_state_internals-0.4.3 (c (n "serde_derive_state_internals") (v "0.4.3") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "0x79kfl8wss71rfvbz9x4jnp311sn997ns73hrvpzm5l0hjddjgm")))

