(define-module (crates-io se rd serde_spanned) #:use-module (crates-io))

(define-public crate-serde_spanned-0.6.0 (c (n "serde_spanned") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1s1qvxk2h1i3p3b47p1vc7cr3f694zfvlajwjaw42f7mrqhyjs1c") (r "1.60.0")))

(define-public crate-serde_spanned-0.6.1 (c (n "serde_spanned") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1x7wqhqay4bgkaq8dmvb9z01mk2z0j0g9jl5nb9ynv3aanpqrz8f") (r "1.60.0")))

(define-public crate-serde_spanned-0.6.2 (c (n "serde_spanned") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0z803jfccflna5wwd0m6wfhzz5fg6h8f3cnwnzijfq2g313pc44k") (r "1.64.0")))

(define-public crate-serde_spanned-0.6.3 (c (n "serde_spanned") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "11p1l83r5g3k18pi88cqri2r9ai03pm8b4azj4j02ypx6scnqhln") (r "1.64.0")))

(define-public crate-serde_spanned-0.6.4 (c (n "serde_spanned") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "102ym47sr1y48ml42wjv6aq8y77bij1qckx1j0gb3rbka21jn0hj") (r "1.67")))

(define-public crate-serde_spanned-0.6.5 (c (n "serde_spanned") (v "0.6.5") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1hgh6s3jjwyzhfk3xwb6pnnr1misq9nflwq0f026jafi37s24dpb") (r "1.67")))

(define-public crate-serde_spanned-0.6.6 (c (n "serde_spanned") (v "0.6.6") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1839b6m5p9ijjmcwamiya2r612ks2vg6w2pp95yg76lr3zh79rkr") (r "1.65")))

