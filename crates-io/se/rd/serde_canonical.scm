(define-module (crates-io se rd serde_canonical) #:use-module (crates-io))

(define-public crate-serde_canonical-0.1.0 (c (n "serde_canonical") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qih32agkrw1mjn7qgsi3hys4diwss1536r8bg1vym04p5ic9bfc")))

