(define-module (crates-io se rd serde-lexpr) #:use-module (crates-io))

(define-public crate-serde-lexpr-0.1.0 (c (n "serde-lexpr") (v "0.1.0") (d (list (d (n "lexpr") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "1phwp5740rfrgy43ai2jq5v1j2zb6k6xgngmn0iyb74h860m58zx")))

(define-public crate-serde-lexpr-0.1.1 (c (n "serde-lexpr") (v "0.1.1") (d (list (d (n "lexpr") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "0hcx4d999cza4yzck1iv552d2kdpr9ar6nr8vx5kg9y3i9jmwghl")))

(define-public crate-serde-lexpr-0.1.2 (c (n "serde-lexpr") (v "0.1.2") (d (list (d (n "lexpr") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "17whqffzp97gbimzpfkg7s27xxnbxsd0naqj47vypm9sxwr5sc2b")))

(define-public crate-serde-lexpr-0.1.3 (c (n "serde-lexpr") (v "0.1.3") (d (list (d (n "lexpr") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "0vxd9rpm8zhlyjbv2xngnz7yxbdyq3d8q4a6g6gganb1749xlk5v")))

