(define-module (crates-io se rd serde-nothing) #:use-module (crates-io))

(define-public crate-serde-nothing-0.1.0 (c (n "serde-nothing") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16y6cypgmwh87zq9vr8yznpzc55didz2xg3ihhsia148ba3kg5lp") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-serde-nothing-0.1.1 (c (n "serde-nothing") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jvdnwdls8d798sazi34d0ni20y6k12wpy0mzi7p7d9zy5j4gycs") (f (quote (("std" "serde/std") ("default" "std"))))))

