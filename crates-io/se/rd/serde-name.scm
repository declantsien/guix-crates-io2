(define-module (crates-io se rd serde-name) #:use-module (crates-io))

(define-public crate-serde-name-0.0.0 (c (n "serde-name") (v "0.0.0") (h "02qgigpbsgsdh6gjvpwd2v46nhj383w967vymw9y4ylk3898jw2p")))

(define-public crate-serde-name-0.1.0 (c (n "serde-name") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-reflection") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17kh3khnpj0r5p71ngdg7dw0pixjmirybd80glfjjl6zw7dk5fwj")))

(define-public crate-serde-name-0.1.1 (c (n "serde-name") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n4b08zlc309fgg38g7iv76l3kz2n1n1bdswhzlcdvds61x2ni47")))

(define-public crate-serde-name-0.1.2 (c (n "serde-name") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "19hdj6hmypf05502d66laimq2bd2mqv3srxbrp8q3hlf063p1i0j")))

(define-public crate-serde-name-0.2.0 (c (n "serde-name") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xvj9pdj8l1wf2rwbv3fhmgqxabzajqkdyi708mhzb3ya8bnnv81")))

(define-public crate-serde-name-0.2.1 (c (n "serde-name") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.6") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1w0h2xfmd7zk178wf21kvk8minj9hqw9ra9gcjrz5r64pkmi8nrv")))

