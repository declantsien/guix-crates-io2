(define-module (crates-io se rd serde_trim) #:use-module (crates-io))

(define-public crate-serde_trim-0.1.0 (c (n "serde_trim") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "00iwrqkn4rg8nrcg7r1lqxawnahjsmnq67q7bqxbr004g361428a")))

(define-public crate-serde_trim-0.2.0 (c (n "serde_trim") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "185lyjzgxj4px12q48b0cbrdbs4rvfc3l8gzpdf8r77j54i4i0xf")))

(define-public crate-serde_trim-0.3.0 (c (n "serde_trim") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "1c8sf6nm1zz31w5b071smn67nn49vrnm0pzg5m0rvkc84k80n488")))

(define-public crate-serde_trim-0.3.1 (c (n "serde_trim") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "0q6krl3lw4xgys1r11ia6s13zw0kihsjbfhkp3pknbqc35d4fbh0")))

(define-public crate-serde_trim-0.4.0 (c (n "serde_trim") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "0ph7q5j2x9yz2ckd68j7mhp3bnydy9f9pkbvdf3zid63h6hlzacd")))

(define-public crate-serde_trim-1.0.0 (c (n "serde_trim") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "1fs0zn5pmj50crnhiicvnx6h9sbkq8dxf85vji42xsxkxz9zl954")))

(define-public crate-serde_trim-1.1.0 (c (n "serde_trim") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)))) (h "10hy1glwyfg702dw5f181hdz79zlki02mgx0f2zpcjh0616k5lbh")))

