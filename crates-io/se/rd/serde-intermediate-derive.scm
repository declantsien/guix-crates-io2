(define-module (crates-io se rd serde-intermediate-derive) #:use-module (crates-io))

(define-public crate-serde-intermediate-derive-1.3.0 (c (n "serde-intermediate-derive") (v "1.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wg25v42j6jjcsdnwzy3k6ssz8akpxs0pmwbkbrzqyczba8bp0iw")))

(define-public crate-serde-intermediate-derive-1.3.1 (c (n "serde-intermediate-derive") (v "1.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rr0bmq4myf39g81kk7zgyl5sj59scdzpmnccqn42s2ybdkp38ab")))

(define-public crate-serde-intermediate-derive-1.3.2 (c (n "serde-intermediate-derive") (v "1.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11qi4yrhwj2p9kgssqrkw0ki235av2jjjhdcfvqbx94vnh4lbyzz")))

(define-public crate-serde-intermediate-derive-1.4.0 (c (n "serde-intermediate-derive") (v "1.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1l2n95f979mryknb622cs2dg7mv7z8lk95wxlgy1gsf4wzibzs6a")))

(define-public crate-serde-intermediate-derive-1.4.1 (c (n "serde-intermediate-derive") (v "1.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pzhwhx7jr0x9fjqqqn64b7564lp3a83fq3ll2vzarxcamayrzss")))

(define-public crate-serde-intermediate-derive-1.4.2 (c (n "serde-intermediate-derive") (v "1.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nmza579ryvgz92c26dbjkldn80d2jnkx157vmzw4sp8dkwlv6q9")))

(define-public crate-serde-intermediate-derive-1.5.0 (c (n "serde-intermediate-derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10i4va7vpp8sc3d1s6vimly4bm7rd6scy9hcw9hayqb9w53w7hlh")))

(define-public crate-serde-intermediate-derive-1.5.1 (c (n "serde-intermediate-derive") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x9kk9bziqa4l6hyb45f863w7h4vv713q4zd8q54vgcps9hc2f5w")))

(define-public crate-serde-intermediate-derive-1.5.2 (c (n "serde-intermediate-derive") (v "1.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17abwcp6wn8ka10rw133va4k0w864whq3fdfvcdcl8akcj1cc44q")))

(define-public crate-serde-intermediate-derive-1.5.3 (c (n "serde-intermediate-derive") (v "1.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pr6fhdcys71j52yf2zbd00zgq8kpn4z4rw1wr89crblp11lqygl")))

(define-public crate-serde-intermediate-derive-1.5.4 (c (n "serde-intermediate-derive") (v "1.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12hf0a30a2c4w30vc3pma5ixprwa1wvm5jj5n9gi4s8z3h0sxw8m")))

(define-public crate-serde-intermediate-derive-1.5.5 (c (n "serde-intermediate-derive") (v "1.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vn2fcnxj283mijh9fdch9h66by2vmfb59a5gs4slsh3gc85p07a")))

(define-public crate-serde-intermediate-derive-1.6.0 (c (n "serde-intermediate-derive") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02pgk3bn2z75jv15hp25m0jsxklj31q77l4jl9w2s8ad0v524a3q")))

