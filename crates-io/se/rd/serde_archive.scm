(define-module (crates-io se rd serde_archive) #:use-module (crates-io))

(define-public crate-serde_archive-0.1.2 (c (n "serde_archive") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0wxp01dfd47vnsps3nd76ir0bk2lhkycnlw044981f3bfb9b7x89") (f (quote (("with-zip" "zip") ("with-tar" "tar")))) (y #t)))

(define-public crate-serde_archive-0.1.3 (c (n "serde_archive") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "123wjzcd91mqpfysjwxn8r15qicmm6xnxhj83vaqbnfjq6n8fafc") (f (quote (("with-zip" "zip") ("with-tar" "tar")))) (y #t)))

(define-public crate-serde_archive-0.1.4 (c (n "serde_archive") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0lm04wkpzyqdn2mjcj89a52k67hnp1xky4zmwdzd5f70f1j28v0a") (f (quote (("with-zip" "zip") ("with-tar" "tar")))) (y #t)))

(define-public crate-serde_archive-0.1.5 (c (n "serde_archive") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1q1qpm27xlky3c6wagyk8ngd0qapnpp58n7xzwvczg09lqnx38lf") (f (quote (("with-zip" "zip") ("with-tar" "tar"))))))

