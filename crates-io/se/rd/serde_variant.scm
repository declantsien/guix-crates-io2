(define-module (crates-io se rd serde_variant) #:use-module (crates-io))

(define-public crate-serde_variant-0.1.0 (c (n "serde_variant") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)))) (h "1kx5rg8whzav21i5rvdz7sqkcmzs8mhkkhr2nka8gfqd8203vib0")))

(define-public crate-serde_variant-0.1.1 (c (n "serde_variant") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)))) (h "15644l910kvy0wh7ij2nca8262rgcq8347ppbjs61fb8z5qqv62z")))

(define-public crate-serde_variant-0.1.2 (c (n "serde_variant") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)))) (h "0xh0jzbd8fwb2nkakdcq0j63wbpbwg09k5ld6j864l6h5w5yra27")))

(define-public crate-serde_variant-0.1.3 (c (n "serde_variant") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)))) (h "0cm4cl13hiziv7ljrq18lgg2qlhqr3qx7ppxi1j9p7cz87gnh00a")))

