(define-module (crates-io se rd serde-value-flatten) #:use-module (crates-io))

(define-public crate-serde-value-flatten-0.1.0 (c (n "serde-value-flatten") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)))) (h "1w7n8h8hc13p5l32x93msr2jcddidck7iv01m593wn832f94p7ya") (f (quote (("ovh-ldp"))))))

