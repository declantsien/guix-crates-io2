(define-module (crates-io se rd serde_amp) #:use-module (crates-io))

(define-public crate-serde_amp-0.1.0 (c (n "serde_amp") (v "0.1.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 0)) (d (n "serde") (r ">= 1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "0jy9wqrx7jhgvcpb85ikkalnb2klrannbbc82lxhk35sbjnxhld7")))

(define-public crate-serde_amp-0.2.0 (c (n "serde_amp") (v "0.2.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 0)) (d (n "serde") (r ">=1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bjh8zapbbbjkhv0s386i19r4n8fyq6gfvamxkavhczwxjwrb8sq")))

