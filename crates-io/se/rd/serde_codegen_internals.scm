(define-module (crates-io se rd serde_codegen_internals) #:use-module (crates-io))

(define-public crate-serde_codegen_internals-0.1.0 (c (n "serde_codegen_internals") (v "0.1.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.36.0") (o #t) (d #t) (k 0)))) (h "1dk19aarx4yma5alpv0z7b7sr88yjllpkq0hgqa8lz1d6i8m7pb3") (f (quote (("with-syntex" "syntex_syntax") ("nightly-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.2.0 (c (n "serde_codegen_internals") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.37.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.37.0") (o #t) (d #t) (k 0)))) (h "0ibm07lj31mixpqqsz2bm4wk9vmy4b0igqlqfdw5srhc6x79d03x") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("nightly-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.3.0 (c (n "serde_codegen_internals") (v "0.3.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.38.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (o #t) (d #t) (k 0)))) (h "0wf4v94kxmplmg91hl670y4h49q9pwmsm9cs4fhd1sh3vv4ribxz") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("nightly-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.4.0-rc1 (c (n "serde_codegen_internals") (v "0.4.0-rc1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.38.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (o #t) (d #t) (k 0)))) (h "187acjrwa9hh33wplsrsxavacxx76bdp5jjbckc6khfc61hf9rlx") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.4.0 (c (n "serde_codegen_internals") (v "0.4.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "0jgqgrcxavy7r5g6qn3cmj59am8nr977pis4bc12a0j0lwdz3mpf") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("nightly-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.5.0 (c (n "serde_codegen_internals") (v "0.5.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "1dq3b2h0jh0kw6hm15l4xdfbyi853bkr1aw3zcq48x59vc3rkra4") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.6.0 (c (n "serde_codegen_internals") (v "0.6.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.41.0") (o #t) (d #t) (k 0)))) (h "1dy2yrvkz9585ylyg4932041jllcjhs4gadcm7aa0af93ms64f1l") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.6.1 (c (n "serde_codegen_internals") (v "0.6.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.41.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.41.0") (o #t) (d #t) (k 0)))) (h "1h5iaci1dhwbqhifv07hwm2yxw6sip2a07345h35m03n9zm6rwm0") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.7.0 (c (n "serde_codegen_internals") (v "0.7.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.42.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (o #t) (d #t) (k 0)))) (h "089x2yrriywdhjqlgz492jhva5v1alk0x7qwbllj78yh3rwf4xzq") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.8.0 (c (n "serde_codegen_internals") (v "0.8.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.43.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.43.0") (o #t) (d #t) (k 0)))) (h "0qfv1fqpzxzw3hy0ylp7wjs1p9lizbwnci670pmki95agh5n9244") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.8.9 (c (n "serde_codegen_internals") (v "0.8.9") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.44.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "101ccxah2nbxkfsv404ips6lyqbalw7hzyw7nxhirs02qv2ia08v") (f (quote (("with-syntex" "syntex_syntax" "syntex_errors") ("unstable-testing" "clippy") ("default" "with-syntex"))))))

(define-public crate-serde_codegen_internals-0.9.0-rc1 (c (n "serde_codegen_internals") (v "0.9.0-rc1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "0pc7ynap7rzg2cax2drcbqgkxpjjfc4k2x7sw5cs70gllkkkizj0") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.9.0 (c (n "serde_codegen_internals") (v "0.9.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "17lcfa66gv2r7lfgyaq0bzw00m5cxpvhkllkcx2fgvpr4kd3pyxp") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.10.0 (c (n "serde_codegen_internals") (v "0.10.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1v6wzxvpifair4a395jyw866pxc944ymbx5afhfkk1sim9vpx3ri") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.11.1 (c (n "serde_codegen_internals") (v "0.11.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0ch7ji411jqf9gcxy7kkqpknp964y1j52g37sa8ccj25ami3m4sr") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.11.2 (c (n "serde_codegen_internals") (v "0.11.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1q454pqlf6bphriq6m2x6mdd4d663a5mrp1jqfhq88827xqly8jm") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.11.3 (c (n "serde_codegen_internals") (v "0.11.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "01p83ar87nd4fjnwwps3csnc4id816jy78p4h3rmky09l0j7kbdg") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.12.0 (c (n "serde_codegen_internals") (v "0.12.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0j8lir9ylizrh7x85kw69awc11jvkx3n2ymnizj78a1ab6x5rjpv") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_codegen_internals-0.12.1 (c (n "serde_codegen_internals") (v "0.12.1") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0zw7c26bgz7h3cspf0xfkd83wppz0a78qmqz7rd9hiaix1qc50nm")))

(define-public crate-serde_codegen_internals-0.13.0 (c (n "serde_codegen_internals") (v "0.13.0") (d (list (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "15ps4rfdn59nkmxgikkcyka0fi0a3i8jmd3a9w75r5qbjkr2n5y3")))

(define-public crate-serde_codegen_internals-0.14.0 (c (n "serde_codegen_internals") (v "0.14.0") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing"))) (k 0)))) (h "0jn7mhxdf150yh8kpqwmidjxjyl7wh7lydrvh21v2wb4s5dks4d5")))

(define-public crate-serde_codegen_internals-0.14.1 (c (n "serde_codegen_internals") (v "0.14.1") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing"))) (k 0)))) (h "0vhb97avmxfc0wbrs4186a7ndzkkg5r5n7k32255447rk5l00ljd")))

(define-public crate-serde_codegen_internals-0.14.2 (c (n "serde_codegen_internals") (v "0.14.2") (d (list (d (n "syn") (r "^0.11") (f (quote ("parsing"))) (k 0)))) (h "0004s3wlc85vi6hq62hq84cv5b6qbbin1n6hdaqj095xhg98p25w")))

(define-public crate-serde_codegen_internals-0.11.0 (c (n "serde_codegen_internals") (v "0.11.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0qzlwn34lqrzn1vdwlq4l0jxwvm2w6aywmiwmxirl3g7m69zyr21") (f (quote (("unstable-testing" "clippy"))))))

