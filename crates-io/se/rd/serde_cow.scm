(define-module (crates-io se rd serde_cow) #:use-module (crates-io))

(define-public crate-serde_cow-0.1.0 (c (n "serde_cow") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "12q514rijil7kiwc43bv4apmwbf2z07s2n87qv2g0wkab7jlrs34")))

