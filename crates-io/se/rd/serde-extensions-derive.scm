(define-module (crates-io se rd serde-extensions-derive) #:use-module (crates-io))

(define-public crate-serde-extensions-derive-0.1.0 (c (n "serde-extensions-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1w435d1lvw09nwchwakm34m8nd72bqlv62k34wcanxn4flynpzwi")))

