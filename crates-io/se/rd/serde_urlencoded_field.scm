(define-module (crates-io se rd serde_urlencoded_field) #:use-module (crates-io))

(define-public crate-serde_urlencoded_field-0.1.0 (c (n "serde_urlencoded_field") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0vdzm5zg2390395wm9jxvbc01rmsibr8phnh0i86bx4rzxgqk20g")))

(define-public crate-serde_urlencoded_field-1.0.0 (c (n "serde_urlencoded_field") (v "1.0.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "09i6lrjyqc7h5kj2id6xlixmpxz5wx7wnm21n0v6ymiwjzf7hpp2")))

(define-public crate-serde_urlencoded_field-1.1.0 (c (n "serde_urlencoded_field") (v "1.1.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0jp0y74pxp6risrck07b9hv7h73lr029pc3wx8m25rpss3vgkp22")))

(define-public crate-serde_urlencoded_field-1.2.0 (c (n "serde_urlencoded_field") (v "1.2.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0llgb5c67kpnchik84484wfw176p9qqk5slra2phz3yfn7fm9a8j")))

(define-public crate-serde_urlencoded_field-1.3.0 (c (n "serde_urlencoded_field") (v "1.3.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "03727cxlaj9k04dajw7j2qy0znchr4ih83fqyxphlxigs9zknlv9")))

(define-public crate-serde_urlencoded_field-1.4.0 (c (n "serde_urlencoded_field") (v "1.4.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1758a3v82iqnzq52m344lpvzh82fg6by4cwn8ay3r56f7pwny8kg")))

(define-public crate-serde_urlencoded_field-1.6.0 (c (n "serde_urlencoded_field") (v "1.6.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "09p16p9q6179fqz15w562mks2i2j7nkz2l3lyfxv44bnrcyazg4j")))

(define-public crate-serde_urlencoded_field-1.8.0 (c (n "serde_urlencoded_field") (v "1.8.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0w4mgfsbfclpb2z2g566rif7c3vwi35p5sx8vkkwlfvvkap7bfsa")))

(define-public crate-serde_urlencoded_field-1.10.0 (c (n "serde_urlencoded_field") (v "1.10.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1hx7nspsdwz6k7kx9hkrfzvwk7hw0ms5w8vx8x4k0n64asxgsnf3")))

(define-public crate-serde_urlencoded_field-1.12.0 (c (n "serde_urlencoded_field") (v "1.12.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0xh89h9s06268iq2diq9crgpb6871isqlz4cwdh4x0ww12g5lyj7")))

(define-public crate-serde_urlencoded_field-1.14.0 (c (n "serde_urlencoded_field") (v "1.14.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.128") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.128") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0192ai5828xvkxmzjiszhwvrzq5fpkgrahxc49d21m6b8zsnc45d")))

(define-public crate-serde_urlencoded_field-1.16.0 (c (n "serde_urlencoded_field") (v "1.16.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.128") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0xzrdi6a34ndya6mwfha3566kizad06xxwwdi3k64c9rg1i2nf81")))

(define-public crate-serde_urlencoded_field-1.18.0 (c (n "serde_urlencoded_field") (v "1.18.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "11pcdw9rdx00y8m1hm4qcjdj3f5dbicazny184fyjgk6275v0b1m")))

(define-public crate-serde_urlencoded_field-1.20.0 (c (n "serde_urlencoded_field") (v "1.20.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.129") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1g3akcf1vqv14kk1y3f0xldbrc5932mq0nghvv8pr64dwypbwkf7")))

(define-public crate-serde_urlencoded_field-1.22.0 (c (n "serde_urlencoded_field") (v "1.22.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "02wq8xvmqvrchg37mpz9mixn0idd0k0xf70dx9vck8gzhbsaqmg8")))

(define-public crate-serde_urlencoded_field-1.24.0 (c (n "serde_urlencoded_field") (v "1.24.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "1qv6lfcvjgq51am9vq33h7y7hhqw7hy1y37im0vmnc2j09fjf7zv")))

(define-public crate-serde_urlencoded_field-1.26.0 (c (n "serde_urlencoded_field") (v "1.26.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0q8865rcsil67mj093w6lwrz45ja6vwyki18y6hprxssz75b2q48")))

(define-public crate-serde_urlencoded_field-1.28.0 (c (n "serde_urlencoded_field") (v "1.28.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "1ncs8jns5zzk2fd6mxc4gasz2j976wyv6180n45wl0wzabajgcbm")))

(define-public crate-serde_urlencoded_field-1.30.0 (c (n "serde_urlencoded_field") (v "1.30.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 0)))) (h "09na220hjr8by2hrna86jx056pc164r2h2w52babyzk4kl29hq6k")))

(define-public crate-serde_urlencoded_field-1.32.0 (c (n "serde_urlencoded_field") (v "1.32.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)))) (h "0f2rh0msyg81nj06y7zl2s1mcq6b1izv3d276ymiz925fqd1b93d")))

(define-public crate-serde_urlencoded_field-1.34.0 (c (n "serde_urlencoded_field") (v "1.34.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1ziir11npsb9f0i27aah0rk3s2xg1a8pzy2zkvn32lniif1xv4yq")))

(define-public crate-serde_urlencoded_field-1.38.0 (c (n "serde_urlencoded_field") (v "1.38.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0jk2xy9ld8x8vr98r88fmrfi0j85i7fdv1a5ymg6i5wgbd36qlpw")))

(define-public crate-serde_urlencoded_field-1.36.0 (c (n "serde_urlencoded_field") (v "1.36.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "092dsyms6713ddmgzmyw7m02qi4qzhd2nkkbsdmhxaaa0zv1ww72")))

(define-public crate-serde_urlencoded_field-1.40.0 (c (n "serde_urlencoded_field") (v "1.40.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.131") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "001iwydd77v4vhq5f83k4p5pvn45sk03ix4ac1kg2sc0d8d7szp3")))

(define-public crate-serde_urlencoded_field-1.42.0 (c (n "serde_urlencoded_field") (v "1.42.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "0f9wcxdi9fzw68s1i4wpf09c502saf4nczzxm8g2v1xzai6psdql")))

(define-public crate-serde_urlencoded_field-1.44.0 (c (n "serde_urlencoded_field") (v "1.44.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "0yg008nrfx5mkhi3ryj4dymld8xi49jqr06syy4wj0sw1lid1qp0")))

(define-public crate-serde_urlencoded_field-1.46.0 (c (n "serde_urlencoded_field") (v "1.46.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)))) (h "12cxr1jhps59x0vyi5wrh20rbkryg0zp53sihdq7yfvvwd8n0bn5")))

(define-public crate-serde_urlencoded_field-1.48.0 (c (n "serde_urlencoded_field") (v "1.48.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.134") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)))) (h "1mvphlkakmi3hwkxx02anv34104phifqgg6z3iafxj7bk72gqayg")))

(define-public crate-serde_urlencoded_field-1.50.0 (c (n "serde_urlencoded_field") (v "1.50.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.135") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1n07x2gh68nvvs2iils6mzkdvn2d1y354y1zw6skbkwwdyy27i4y")))

(define-public crate-serde_urlencoded_field-1.52.0 (c (n "serde_urlencoded_field") (v "1.52.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "14v9r78461jh0nnik0wqlljn8mc2ndf7lv3gw84qlc1zla1crwqv")))

(define-public crate-serde_urlencoded_field-1.54.0 (c (n "serde_urlencoded_field") (v "1.54.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0nh8h02ib8qvzz2vlm6ylaca9zivbl18rrrg9b563axqbbcnm7pj")))

(define-public crate-serde_urlencoded_field-1.56.0 (c (n "serde_urlencoded_field") (v "1.56.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "01pjsps0x0s87h6hq19xfx6i4fh0q9jn3nz6wg6b4vpb4kjmjilg")))

(define-public crate-serde_urlencoded_field-1.58.0 (c (n "serde_urlencoded_field") (v "1.58.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1bbjs6gp67kjl6jx0bl1r9wsfv29nqxkkxj1lqv3ajqhw8j5dg27")))

(define-public crate-serde_urlencoded_field-1.60.0 (c (n "serde_urlencoded_field") (v "1.60.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)))) (h "0b8x2kvfw3ikzmq6pc8k06fcx8sjp5skb3lii2vwngijrk7ldcnx")))

(define-public crate-serde_urlencoded_field-1.62.0 (c (n "serde_urlencoded_field") (v "1.62.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1c5ap7g68m2b3ifnsrdxccinyyhbqp5lrnx82400npc0b0yjyll4")))

(define-public crate-serde_urlencoded_field-1.64.0 (c (n "serde_urlencoded_field") (v "1.64.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1v1ki090qp44nqxy54z4rdc9vxwavfhvqlzlh5qpb52fi3vsmr3j")))

(define-public crate-serde_urlencoded_field-1.66.0 (c (n "serde_urlencoded_field") (v "1.66.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1m0liz7k5ydg74svd105my80f6y78n8k5s3zckimpcvmrlz2shv3")))

(define-public crate-serde_urlencoded_field-1.69.0 (c (n "serde_urlencoded_field") (v "1.69.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1q4x8np4wqnggwya316ywfqjs1s8dh6z91df3rgabric1671h9nx")))

(define-public crate-serde_urlencoded_field-1.71.0 (c (n "serde_urlencoded_field") (v "1.71.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1vcyhlp22mi7rd41y11p82bgzj7igzsnqqpfhfiniv3wvq3c7q5m")))

(define-public crate-serde_urlencoded_field-1.73.0 (c (n "serde_urlencoded_field") (v "1.73.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.141") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0w1ppcz991m6n1wb2f6r898vi3ghdbdd576x16s7siqrnr9ddkzh")))

(define-public crate-serde_urlencoded_field-1.75.0 (c (n "serde_urlencoded_field") (v "1.75.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.142") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1qmc1px24f7kr8qp82za1r8yji7241nd30ismrzy9hkkpv7bbi99")))

(define-public crate-serde_urlencoded_field-1.77.0 (c (n "serde_urlencoded_field") (v "1.77.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0ddwf3n3vsx47lsabin8p64igvfvq662657ikqm7jjnbq81yi8g0")))

(define-public crate-serde_urlencoded_field-1.79.0 (c (n "serde_urlencoded_field") (v "1.79.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "14nk91i8gb3wdmiv3aj5qbjwk6yp9g644yc16g6safqhvp0by4n5")))

(define-public crate-serde_urlencoded_field-1.81.0 (c (n "serde_urlencoded_field") (v "1.81.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0k1gg4kdl4bc5p2ynsbmfiyahy8i8449kh1rxg2vqzpyvxxd95sw")))

(define-public crate-serde_urlencoded_field-1.83.0 (c (n "serde_urlencoded_field") (v "1.83.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0jgh0gph5g8z18ipipzgjk01mq983vwlwd2f931qddjh1pwp1c29")))

(define-public crate-serde_urlencoded_field-1.85.0 (c (n "serde_urlencoded_field") (v "1.85.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)))) (h "14ml0x0l5yf26yx0760p042ai5jw1cgg6cjcva7qsvwjmlqsj51f")))

(define-public crate-serde_urlencoded_field-1.87.0 (c (n "serde_urlencoded_field") (v "1.87.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0m8ifiyk14nar44g39n5yjzd31ldl46a1ia4dmj041vygbvqz8gx")))

(define-public crate-serde_urlencoded_field-1.89.0 (c (n "serde_urlencoded_field") (v "1.89.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0jmhvlyvv1hjv6kfrdyvyyg15xr78my6j3x91ya8i86rkgk3788j")))

(define-public crate-serde_urlencoded_field-1.91.0 (c (n "serde_urlencoded_field") (v "1.91.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1zy3bh2mzxcd3z0v2lsc37djcgvsa81ci2dxzjypfpcaav558gzb")))

(define-public crate-serde_urlencoded_field-1.93.0 (c (n "serde_urlencoded_field") (v "1.93.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "004dj8iy4jkzj6i8g0xchsdvbmnm7lp13ima4zyn3z146clcjka2")))

(define-public crate-serde_urlencoded_field-1.95.0 (c (n "serde_urlencoded_field") (v "1.95.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.149") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0gdhal59ha6p9iacgxypqw8kscr7n8shi3svc886qzl8rf5ymfp9")))

(define-public crate-serde_urlencoded_field-1.97.0 (c (n "serde_urlencoded_field") (v "1.97.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.150") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1grgxy9m5i9ymrhkni6f0gyp03k09sim5hx2ph6lwr4vvn6nrj0y")))

(define-public crate-serde_urlencoded_field-1.99.0 (c (n "serde_urlencoded_field") (v "1.99.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0r6338q1xqrmrs5hysypksxhzf0mwqjg9km6vgj7m29y5wnf5yfv")))

(define-public crate-serde_urlencoded_field-1.101.0 (c (n "serde_urlencoded_field") (v "1.101.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "13zkdx5cnvqh5w3xjgq03xrh4kpxihsjpp0l42zaj70k1cafz1yf")))

(define-public crate-serde_urlencoded_field-1.103.0 (c (n "serde_urlencoded_field") (v "1.103.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1nii4i2dlxg192msjyk96040s5cfd161n34wkn9swpcj55s8krck")))

(define-public crate-serde_urlencoded_field-1.105.0 (c (n "serde_urlencoded_field") (v "1.105.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0xjz7bcs30rvpql4pw6qcrkkcqj1sasy841dkajsfb6qvf83hc6d")))

(define-public crate-serde_urlencoded_field-1.107.0 (c (n "serde_urlencoded_field") (v "1.107.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "1z6xhjlhd5q20iz5m7f534g392dha5lgshrjwigwrwiw6sd6z1df")))

(define-public crate-serde_urlencoded_field-1.109.0 (c (n "serde_urlencoded_field") (v "1.109.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1fbaxqk77lj8yavb3mdrnn3682nrjf61mi8qy2d5yqcw7ig7kaas")))

(define-public crate-serde_urlencoded_field-1.111.0 (c (n "serde_urlencoded_field") (v "1.111.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1m34ddkjmvvcslcfinxd5pbj6zwlnvk9xz0j0cqf2ch97n9mpbr0")))

(define-public crate-serde_urlencoded_field-1.113.0 (c (n "serde_urlencoded_field") (v "1.113.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.153") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0kwmdmmldj6ljysi7dh94fqwsf5s7ri1n0jmmazk3xy6s8177rnz")))

(define-public crate-serde_urlencoded_field-1.115.0 (c (n "serde_urlencoded_field") (v "1.115.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0s0c0j34f194hbm2s0hvb1ypwl4ppd71f0lvj1g08hfbwbbwabp4")))

(define-public crate-serde_urlencoded_field-1.117.0 (c (n "serde_urlencoded_field") (v "1.117.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0jalrjl0wqc8xfhf34nxnfq2srmlb1wbscsczz2ka1r8f36dah81")))

(define-public crate-serde_urlencoded_field-1.119.0 (c (n "serde_urlencoded_field") (v "1.119.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0pdnax73m2vg69jf5zvp7mcg5n3l876vk3x6rddlr5ksrq3wkix0")))

