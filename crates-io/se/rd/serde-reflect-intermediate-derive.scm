(define-module (crates-io se rd serde-reflect-intermediate-derive) #:use-module (crates-io))

(define-public crate-serde-reflect-intermediate-derive-1.2.0 (c (n "serde-reflect-intermediate-derive") (v "1.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vq4y9vi70h2w069hz93spmjgq016s5ram23lj9jawm1gryfrb1v")))

(define-public crate-serde-reflect-intermediate-derive-1.2.2 (c (n "serde-reflect-intermediate-derive") (v "1.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hglygh2zgzhv575y7dwva473bczz5j3dzsmby1hsrs4y816ihn2")))

(define-public crate-serde-reflect-intermediate-derive-1.2.3 (c (n "serde-reflect-intermediate-derive") (v "1.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02ajl69jhy7dawiiqgbb7g4crvdl0kpwv7qiqb6g9riy8f26zgsj")))

(define-public crate-serde-reflect-intermediate-derive-1.2.4 (c (n "serde-reflect-intermediate-derive") (v "1.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pw477wbzvx34x8rqpvan8k4z9k0kfy63a4i865spicg510x3hz8")))

