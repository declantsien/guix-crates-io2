(define-module (crates-io se rd serde_json_any_key) #:use-module (crates-io))

(define-public crate-serde_json_any_key-1.0.0 (c (n "serde_json_any_key") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rj2rnir0d6ibq4b2rnrrd3ifqihxlxdwmy6w45wl0wkfq37mmgj")))

(define-public crate-serde_json_any_key-1.0.1 (c (n "serde_json_any_key") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1aa4zpd5j7nyp6dcajqqjazkdmksyhsvxn8j900sb8jw55fifb1i")))

(define-public crate-serde_json_any_key-1.1.0 (c (n "serde_json_any_key") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "100vnr26150y85r26q44h82l3gi92jj4sl60zxbpkfca6j8ila7q")))

(define-public crate-serde_json_any_key-2.0.0 (c (n "serde_json_any_key") (v "2.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cvl59n4h7j7yaxgy9ggza7qcg4pdpjy3f9821sc9xh92b50ki5j")))

