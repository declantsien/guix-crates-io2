(define-module (crates-io se rd serde-dot-case) #:use-module (crates-io))

(define-public crate-serde-dot-case-0.1.0 (c (n "serde-dot-case") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0qsjc6ydz1pw5r7kl48inds47iw2isy111h2h5pci9g3bqvdksd0")))

