(define-module (crates-io se rd serde_var_export) #:use-module (crates-io))

(define-public crate-serde_var_export-0.0.0 (c (n "serde_var_export") (v "0.0.0") (h "0hjdxy8vywprih26vy7v9ajqjq0vn2nynbdwqmvnvdfrgz3wv4ix") (y #t)))

(define-public crate-serde_var_export-0.1.0 (c (n "serde_var_export") (v "0.1.0") (d (list (d (n "dtoa") (r "^0.4.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0qkf851zpblp1aald4djxjqmwb61kwyjzk6fiyrcsqi7vj5s9cw5")))

