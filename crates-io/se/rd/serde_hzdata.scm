(define-module (crates-io se rd serde_hzdata) #:use-module (crates-io))

(define-public crate-serde_hzdata-0.1.0 (c (n "serde_hzdata") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mqy97z8yqcdc2gmsww0jpwjzi1ppvvqj3bynpq4fzw4nmqxy208") (r "1.56.0")))

(define-public crate-serde_hzdata-0.1.1 (c (n "serde_hzdata") (v "0.1.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0cq4nbr81xr61b5bh5b7h4hqs9cd49fxxspi0xq0c30bcx6dzfg5") (r "1.56.0")))

