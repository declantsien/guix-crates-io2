(define-module (crates-io se rd serde-gettext) #:use-module (crates-io))

(define-public crate-serde-gettext-0.1.0 (c (n "serde-gettext") (v "0.1.0") (d (list (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "dynfmt") (r "^0.1") (f (quote ("python"))) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.4.3") (d #t) (k 0)) (d (n "libc-strftime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1wh35wspks8s2z6an31irhw38xh99hcn8pn2gr231rjkaqmghxg7")))

(define-public crate-serde-gettext-0.1.1 (c (n "serde-gettext") (v "0.1.1") (d (list (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "dynfmt") (r "^0.1") (f (quote ("python"))) (d #t) (k 0)) (d (n "gettext-rs") (r "^0.4.3") (d #t) (k 0)) (d (n "libc-strftime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "12h9b1dxrp7aji1ksi3ix555zj0mwnz4dh5d9f8d54djcwn9mh1g")))

