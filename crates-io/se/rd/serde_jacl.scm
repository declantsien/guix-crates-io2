(define-module (crates-io se rd serde_jacl) #:use-module (crates-io))

(define-public crate-serde_jacl-0.1.0 (c (n "serde_jacl") (v "0.1.0") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mrqks1nrxhlnrg3jpmdgka1xvylphxa05c954sxlggsiqv34cjw")))

(define-public crate-serde_jacl-0.1.1 (c (n "serde_jacl") (v "0.1.1") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "017zx8imliwiw42mv722d33d77vfxkvwriw63wg8vklqrwaf6i1z")))

(define-public crate-serde_jacl-0.1.2 (c (n "serde_jacl") (v "0.1.2") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nfjl6fgxyllhj2m2hpiwckxli993bdsvd3jvbblpnsznlhw0ynw")))

(define-public crate-serde_jacl-0.2.0 (c (n "serde_jacl") (v "0.2.0") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f454ql46d4sik8w8hakah0nv9rkcvb817q1vjkgf9s2xi10yc5c")))

(define-public crate-serde_jacl-0.2.1 (c (n "serde_jacl") (v "0.2.1") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "08cmrby4fg6d3hwqyjrs4kaaafdz9px8gfadd804z9wh95r5gz88")))

(define-public crate-serde_jacl-0.2.2 (c (n "serde_jacl") (v "0.2.2") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q6lysik94afy57qiq57y3zs5z8k8wa7nxk73354xcc5n6hqa9pg")))

(define-public crate-serde_jacl-0.2.3 (c (n "serde_jacl") (v "0.2.3") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "15xi0s71m2j57bmw119p1gbd2714hyib0qbbdwg85vg07scglvfp")))

(define-public crate-serde_jacl-0.3.0 (c (n "serde_jacl") (v "0.3.0") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k0ac0xgia82f4bprirnfhlx084pdsnq5hyyqdhpq5bz7n4l4s7h")))

(define-public crate-serde_jacl-0.3.1 (c (n "serde_jacl") (v "0.3.1") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "13van46fa3vc69ifwpxfwflpwhbgghsrxa45gm84fhsjj5q6m49a")))

(define-public crate-serde_jacl-0.3.2 (c (n "serde_jacl") (v "0.3.2") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "09c2ny49wg0bjfbqr56fwqw3r78i5bsy5kr8dzs428vyxcq0i8sg")))

(define-public crate-serde_jacl-0.3.3 (c (n "serde_jacl") (v "0.3.3") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rjvwziswiagz4pdw68wrn5byzv5nmfnfpg33lsynaxcbvfjq8g0")))

(define-public crate-serde_jacl-0.3.4 (c (n "serde_jacl") (v "0.3.4") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "003k90iy8cvgjj48xkxrv6b4fy2n30pcvcawzvwbjaf20rwbpi6d")))

(define-public crate-serde_jacl-0.3.5 (c (n "serde_jacl") (v "0.3.5") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wh09azja44w2g6lj5zmqvs5zwi42lp59dp43prcp0blad1hqar4")))

(define-public crate-serde_jacl-0.3.6 (c (n "serde_jacl") (v "0.3.6") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "17w7cl0abwck81zg4x94d98m7bb4xnadi88hibbp4ndv4abgsgms")))

(define-public crate-serde_jacl-0.4.0 (c (n "serde_jacl") (v "0.4.0") (d (list (d (n "escape8259") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "08sr9kr5rdy2x1398xibj3klx1a1xvqc26qlrhazfi7wibrspsdd")))

