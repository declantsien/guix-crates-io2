(define-module (crates-io se rd serde-matcher) #:use-module (crates-io))

(define-public crate-serde-matcher-0.1.0 (c (n "serde-matcher") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "16m12xak1qgajz2df8hpb4dfkwdg77xpbsvbs9i2qqr1zqb7644y") (y #t)))

(define-public crate-serde-matcher-0.1.1 (c (n "serde-matcher") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "08pydq96zgddjzvw9nnnm2ndc3b5kpxxb7b5w5s2y2zy7qcvv9xp") (y #t)))

(define-public crate-serde-matcher-0.1.2 (c (n "serde-matcher") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "1qnyh60kqdghqninr60d16x9fba258vy6h78mf6mjvbj1r7pfx3j") (y #t)))

