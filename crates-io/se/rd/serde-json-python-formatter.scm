(define-module (crates-io se rd serde-json-python-formatter) #:use-module (crates-io))

(define-public crate-serde-json-python-formatter-0.1.0 (c (n "serde-json-python-formatter") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pda475qvybg2q6zpjkd5f6vfg0bbwbikjaqyb7shrvw0xafwqnv")))

