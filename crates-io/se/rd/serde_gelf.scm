(define-module (crates-io se rd serde_gelf) #:use-module (crates-io))

(define-public crate-serde_gelf-0.1.2 (c (n "serde_gelf") (v "0.1.2") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m3v93c7m4nin0mmnk1k2lyrd7mc8z0xkn4z4yp0xqymap4jhq0n") (f (quote (("ovh-ldp"))))))

(define-public crate-serde_gelf-0.1.3 (c (n "serde_gelf") (v "0.1.3") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16agg75ghqnjgh3nc1gdr001l2dkx31mlxdm82n6rfn7gx3s0cha") (f (quote (("ovh-ldp"))))))

(define-public crate-serde_gelf-0.1.4 (c (n "serde_gelf") (v "0.1.4") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pmk8h5a8if01c9nq5nl246krspccv1b5v2j7x1lv2rfm5ix319h") (f (quote (("ovh-ldp"))))))

(define-public crate-serde_gelf-0.1.5 (c (n "serde_gelf") (v "0.1.5") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde-value-flatten") (r "^0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kgzzgayp9wwwz3bs9ny05vrkpc05ndfkv4rd1blq771fiyd8lkj") (f (quote (("ovh-ldp" "serde-value-flatten/ovh-ldp"))))))

(define-public crate-serde_gelf-0.1.6 (c (n "serde_gelf") (v "0.1.6") (d (list (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde-value-utils") (r "^0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "089smaaibfzj07izslk89kzhgaaifgklljr272a3c5mw0bjj67rx") (f (quote (("ovh-ldp" "serde-value-utils/with-schema"))))))

