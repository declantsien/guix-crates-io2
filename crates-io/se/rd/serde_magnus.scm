(define-module (crates-io se rd serde_magnus) #:use-module (crates-io))

(define-public crate-serde_magnus-0.1.0 (c (n "serde_magnus") (v "0.1.0") (d (list (d (n "magnus") (r "^0.4") (d #t) (k 0)) (d (n "magnus") (r "^0.4") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1z4qvk3vagzmfna4p42n4cnjz9y4f04w068fq2098wlbwij5xn31") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.1.1 (c (n "serde_magnus") (v "0.1.1") (d (list (d (n "magnus") (r "^0.4") (d #t) (k 0)) (d (n "magnus") (r "^0.4") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1d630nn4n56qd5sc25kyr0l5np6ykyz4svp5jnn3cynys0bh3gz0") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.1.2 (c (n "serde_magnus") (v "0.1.2") (d (list (d (n "magnus") (r "^0.4") (d #t) (k 0)) (d (n "magnus") (r "^0.4") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "187f53vjqrhwrq8yg7amqnymz5csjw90has891rxajjnqg3zdsmn") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.2.0 (c (n "serde_magnus") (v "0.2.0") (d (list (d (n "magnus") (r "^0.5") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "15ncgzx7vv9zzm4rnmc2fha6kfldynga3ldvmajhivh98nyi28mq") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.2.1 (c (n "serde_magnus") (v "0.2.1") (d (list (d (n "magnus") (r "^0.5") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "0pr93hi3xd8zdw9ii1vvr0rgwaprlxcmf0aq64a2hsz1x76wh0qg") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.2.2 (c (n "serde_magnus") (v "0.2.2") (d (list (d (n "magnus") (r "^0.5") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0, <=1.0.156") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "1371ybwrdkjnf7rhyrwyj66qivl4gs41bb97xyirg0vqqq5dd6px") (y #t) (r "1.51")))

(define-public crate-serde_magnus-0.7.0 (c (n "serde_magnus") (v "0.7.0") (h "04b7m8iqk94ps83niz21khm7fgnm6irwgz1pp8qgisghagyj4gkx")))

(define-public crate-serde_magnus-0.8.0 (c (n "serde_magnus") (v "0.8.0") (d (list (d (n "magnus") (r "^0.6.2") (d #t) (k 0)) (d (n "magnus") (r "^0.6.2") (f (quote ("embed"))) (d #t) (k 2)) (d (n "rb-sys") (r "^0.9, <=0.9.81") (d #t) (k 0)) (d (n "rb-sys-build") (r "^0.9, <=0.9.81") (d #t) (k 0)) (d (n "serde") (r "^1.0, <=1.0.156") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "14nixlswrj4cv3x8baiafyv1c1j9qhbr2dlgmhm7jbch5ih5d6ja") (r "1.65")))

(define-public crate-serde_magnus-0.8.1 (c (n "serde_magnus") (v "0.8.1") (d (list (d (n "magnus") (r "^0.6.2") (d #t) (k 0)) (d (n "magnus") (r "^0.6.2") (f (quote ("embed"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 0)))) (h "1g9d4qvjgcgn00k26z9hafrd3a7pc0r5zvwrj5p03qdmhfjhvhkn") (r "1.65")))

