(define-module (crates-io se rd serde-duration-ext) #:use-module (crates-io))

(define-public crate-serde-duration-ext-0.1.0 (c (n "serde-duration-ext") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "166x6rr3liyzr5mgfdz7f2b66naphnkfj1cz8a2xmzywz6n2h75f") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

