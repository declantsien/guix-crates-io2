(define-module (crates-io se rd serde-roxmltree) #:use-module (crates-io))

(define-public crate-serde-roxmltree-0.1.0 (c (n "serde-roxmltree") (v "0.1.0") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0mkjkayib8a3jqmldgck9adgzlmhj063hif1397rvp321dry1dj3")))

(define-public crate-serde-roxmltree-0.1.1 (c (n "serde-roxmltree") (v "0.1.1") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rg0z953ch7cpmx0df9cgkpvph1dc13xccmp0hkv8hasd23k6jy2")))

(define-public crate-serde-roxmltree-0.2.0 (c (n "serde-roxmltree") (v "0.2.0") (d (list (d (n "roxmltree") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1jj0180l2i356lv93daikwbdsnw97y1m2af3g4z0mhhz1afgglz1")))

(define-public crate-serde-roxmltree-0.3.0 (c (n "serde-roxmltree") (v "0.3.0") (d (list (d (n "roxmltree") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "01ivlk7xqmirpbizpi63998zqzv0laxsf1lrs0cn2zvq39ajzvv7")))

(define-public crate-serde-roxmltree-0.3.1 (c (n "serde-roxmltree") (v "0.3.1") (d (list (d (n "roxmltree") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0hx7xdibws5c9r26xx4k0dlbyhyr2cmb7l8hv7nirx6fd98fwf0z")))

(define-public crate-serde-roxmltree-0.3.2 (c (n "serde-roxmltree") (v "0.3.2") (d (list (d (n "roxmltree") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "18cwvavabvwil02fk5qfg8h8rmziadghn5qr2267wnc00hdm3b7d")))

(define-public crate-serde-roxmltree-0.3.3 (c (n "serde-roxmltree") (v "0.3.3") (d (list (d (n "roxmltree") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "15azg412yfz78r34mc6f0vm5r139v40bh4723airfzcaxp8j46qb")))

(define-public crate-serde-roxmltree-0.3.4 (c (n "serde-roxmltree") (v "0.3.4") (d (list (d (n "roxmltree") (r "^0.15") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1p9cy9cp40bji52lqn4bysx4b5m60bb4dxcyiab3cgijbsbibkfy")))

(define-public crate-serde-roxmltree-0.4.0 (c (n "serde-roxmltree") (v "0.4.0") (d (list (d (n "roxmltree") (r "^0.16") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "17s5sb46imziwcq2mkyz1zxxqv0wry0a39hvg9f633825xd4x8m9")))

(define-public crate-serde-roxmltree-0.5.0 (c (n "serde-roxmltree") (v "0.5.0") (d (list (d (n "roxmltree") (r "^0.17") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "115lcrwynz2mzhkm5l236y08rfihs0axxp3chaf572xxhyl6h2km")))

(define-public crate-serde-roxmltree-0.6.0 (c (n "serde-roxmltree") (v "0.6.0") (d (list (d (n "roxmltree") (r "^0.18") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "03qfcz3dkk9cd3laky1hvryfvdfjcv1pnf6a5hcs8ynf1khfngk1")))

(define-public crate-serde-roxmltree-0.6.1 (c (n "serde-roxmltree") (v "0.6.1") (d (list (d (n "roxmltree") (r "^0.18") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "02gbf5bjczk3xi5piykb7wrwk6xgy3ci5i02wh6j5ws0dahgvz29")))

(define-public crate-serde-roxmltree-0.7.0 (c (n "serde-roxmltree") (v "0.7.0") (d (list (d (n "roxmltree") (r "^0.19") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1489jrp9xzcpnnad1wfbzrw27q5b14gihw7p8i47012cj8qf3z1z")))

(define-public crate-serde-roxmltree-0.7.1 (c (n "serde-roxmltree") (v "0.7.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1q13nvfmywr0lnyzk4r3rp914m0cfaddvwila07ldjfmpp7zgwsh")))

(define-public crate-serde-roxmltree-0.7.2 (c (n "serde-roxmltree") (v "0.7.2") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0s0rpl2dcwqpx7bfi73xnbbcgqz28qfg4ap402gpwhjz9spylscc")))

(define-public crate-serde-roxmltree-0.8.0 (c (n "serde-roxmltree") (v "0.8.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rs9mlm15planh6aglshqmc6g60db5alv1axi67vajfwwmjfqxdn")))

(define-public crate-serde-roxmltree-0.8.1 (c (n "serde-roxmltree") (v "0.8.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1zx3cmhs8gik8w993hqflzl4pg94w8ls2r4ib8asbrhi0jq3vg34")))

