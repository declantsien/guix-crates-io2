(define-module (crates-io se rd serde-avro-bytes) #:use-module (crates-io))

(define-public crate-serde-avro-bytes-0.1.0 (c (n "serde-avro-bytes") (v "0.1.0") (d (list (d (n "apache-avro") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1ai4rc36gzfzmippgz1k7b7gbvss4k2l453ah4g5z2kn02j8kzhi")))

(define-public crate-serde-avro-bytes-0.2.0 (c (n "serde-avro-bytes") (v "0.2.0") (d (list (d (n "apache-avro") (r "^0.16.0") (d #t) (k 0)) (d (n "bstr") (r "^1.9.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0z2l3k4yp01h5xr5az5h1qxy9i6kq4gcnchzp1i28sh0hf04wcm5") (f (quote (("default")))) (s 2) (e (quote (("bstr" "dep:bstr"))))))

