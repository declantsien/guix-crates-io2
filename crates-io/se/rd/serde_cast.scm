(define-module (crates-io se rd serde_cast) #:use-module (crates-io))

(define-public crate-serde_cast-1.0.0 (c (n "serde_cast") (v "1.0.0") (d (list (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rxdd0fj7llbh8g7ziv309n764bdhc2wx9aymq0lnfk65hms8f4i") (y #t)))

(define-public crate-serde_cast-1.0.1 (c (n "serde_cast") (v "1.0.1") (d (list (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zcvfhwyksqcayx6wqyj667swd92kmr0p5n4781qlrzfdgjhrqj0")))

