(define-module (crates-io se rd serde-hessian) #:use-module (crates-io))

(define-public crate-serde-hessian-0.1.0-rc1 (c (n "serde-hessian") (v "0.1.0-rc1") (d (list (d (n "hessian_rs") (r "^0.0.4-rc3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0ch2m025q49kkh6jf6d4d4p8aqv0s64rkiy2qgzh0i15mkh7mcc9")))

