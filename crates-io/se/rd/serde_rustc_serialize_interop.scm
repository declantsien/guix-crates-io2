(define-module (crates-io se rd serde_rustc_serialize_interop) #:use-module (crates-io))

(define-public crate-serde_rustc_serialize_interop-1.0.0 (c (n "serde_rustc_serialize_interop") (v "1.0.0") (d (list (d (n "bincode") (r "^0.5.0") (d #t) (k 0)) (d (n "either") (r "^0.1.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (d #t) (k 0)))) (h "0dldk5aqx360xc00hsj2cg4j8jp086qjgdgxphs1apmbm1khyywh")))

