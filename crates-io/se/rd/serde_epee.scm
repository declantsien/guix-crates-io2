(define-module (crates-io se rd serde_epee) #:use-module (crates-io))

(define-public crate-serde_epee-0.1.0 (c (n "serde_epee") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1xbzxcs09mxqv1d1xvj74fb2c8v9br0vbiiwfnrmh93dn1k1r8j8")))

(define-public crate-serde_epee-0.1.1 (c (n "serde_epee") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1v4cpcdz4wa2w3ik40mg66zahlnqp2n8hjbszbrmc9l300smd00a")))

(define-public crate-serde_epee-0.1.2 (c (n "serde_epee") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1jk2y4m4x999nrr8w1wxxzkp75djmp7852i7z9pbng7pi88460pr")))

(define-public crate-serde_epee-0.1.3 (c (n "serde_epee") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "0xjrg9m2bds60qvpfjf7w2hlm1d3ifv956a9qjgz743vcn85k528")))

(define-public crate-serde_epee-0.2.0 (c (n "serde_epee") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1k7jf13l1giq5hbwlhpf8js5js1ji4zy7iqg8smzrwyqmznzbjb7")))

(define-public crate-serde_epee-0.3.0 (c (n "serde_epee") (v "0.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1pl9srr5j6lfrvgwz2kd99xvwlr7hpkawlq0h8f1znk8d2rib02p")))

