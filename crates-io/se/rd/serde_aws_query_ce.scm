(define-module (crates-io se rd serde_aws_query_ce) #:use-module (crates-io))

(define-public crate-serde_aws_query_ce-0.1.0 (c (n "serde_aws_query_ce") (v "0.1.0") (d (list (d (n "aws-smithy-types") (r "^1.0") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "serde_with") (r "^3.4") (d #t) (k 2)))) (h "10rapf383hy6mvc4h0indly13c47d6d94ldv2pzw0hng3h3f0m9a")))

(define-public crate-serde_aws_query_ce-0.1.1 (c (n "serde_aws_query_ce") (v "0.1.1") (d (list (d (n "aws-smithy-types") (r "^1.0") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "serde_with") (r "^3.4") (d #t) (k 2)))) (h "154f02ln6sk6vyz63gmf4svnjyx4pcla9gxfw5v65grmjmy4lp9r")))

