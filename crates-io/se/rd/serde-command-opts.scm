(define-module (crates-io se rd serde-command-opts) #:use-module (crates-io))

(define-public crate-serde-command-opts-0.1.0 (c (n "serde-command-opts") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.2") (d #t) (k 0)))) (h "1g06zb283srbrra1apqpga75k062xxd88fs69wp5v37brgpdissl")))

(define-public crate-serde-command-opts-0.1.1 (c (n "serde-command-opts") (v "0.1.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.2") (d #t) (k 0)))) (h "00d7yrgk0h9sjrmc1lnk2800rh2nlgwpqm407lvm5g5lmmc543dh")))

