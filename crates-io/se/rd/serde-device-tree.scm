(define-module (crates-io se rd serde-device-tree) #:use-module (crates-io))

(define-public crate-serde-device-tree-0.0.1 (c (n "serde-device-tree") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15b9ixdd4ajjzsi2vw069jj95lf9ycdz31c7yk4lw97anhhlf5d2") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

