(define-module (crates-io se rd serde_osc) #:use-module (crates-io))

(define-public crate-serde_osc-0.1.0 (c (n "serde_osc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1ra5n14klnw96svx15kmni2cnk87cknihzf7qmz9kqa5qlp9mxf2")))

(define-public crate-serde_osc-0.1.1 (c (n "serde_osc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "12ixxdpa2h0a3mcrj55g5kk4gj2prscqbcrg2xra8j5a0bfnwrkq")))

(define-public crate-serde_osc-0.1.2 (c (n "serde_osc") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "19pkmdg3qfv03w57aigkk6c62f1jxylmhgb9ww133m11aqrh1y88")))

(define-public crate-serde_osc-0.2.0 (c (n "serde_osc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15cyzmgiz653v0acsbz68m5q9zx7mvy5dgjl3k1lirsf70x6hdw7")))

(define-public crate-serde_osc-0.3.0 (c (n "serde_osc") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gqby9r1469lzp73ciiww7bjbdad0ywdn1mmmaa9ss3r4nkh9bi6")))

(define-public crate-serde_osc-0.4.0 (c (n "serde_osc") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04afy0k27q5ki1pansqqc8dd78zzf64ihp3ch9fr64yyv64igdfk")))

(define-public crate-serde_osc-0.4.1 (c (n "serde_osc") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "14bmp5qrhk5p5f9m3l33x2l3ywdv8pcr2h5l7wkswqwkwfcc7qvi")))

(define-public crate-serde_osc-0.4.2 (c (n "serde_osc") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pwam2inq0jwd25kd2667xa4dj82ab989i9ydzskx548461awxfm")))

