(define-module (crates-io se rd serde_sexpr) #:use-module (crates-io))

(define-public crate-serde_sexpr-0.1.0 (c (n "serde_sexpr") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)))) (h "1nwblq8qwybxjls6n75y440c0kfm5r382z1irrsl1ikrszpby62k")))

