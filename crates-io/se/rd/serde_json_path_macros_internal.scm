(define-module (crates-io se rd serde_json_path_macros_internal) #:use-module (crates-io))

(define-public crate-serde_json_path_macros_internal-0.1.0 (c (n "serde_json_path_macros_internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bgmw73qbwsq5jbfr6axqw2k7j8rpmv2rqipfb0j0c541lrqg4xv")))

(define-public crate-serde_json_path_macros_internal-0.1.1 (c (n "serde_json_path_macros_internal") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p1ijw024gfb7rl56csda8v48sfkf8pmjigw272dyy7dsahybpbm")))

