(define-module (crates-io se rd serde-tcod-config-parser) #:use-module (crates-io))

(define-public crate-serde-tcod-config-parser-0.1.0 (c (n "serde-tcod-config-parser") (v "0.1.0") (d (list (d (n "logos") (r "^0.10.0-rc2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1wnyl6bvjxpzfk7131jmfja45bvcyi327arj219i40bd8jw27s3j") (y #t)))

(define-public crate-serde-tcod-config-parser-0.1.1 (c (n "serde-tcod-config-parser") (v "0.1.1") (d (list (d (n "logos") (r "^0.10.0-rc2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "051v1yzfsf1037529pqbnbsndrbr8drfqix5n44fz5vs0xndskv4")))

