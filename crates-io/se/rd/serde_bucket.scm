(define-module (crates-io se rd serde_bucket) #:use-module (crates-io))

(define-public crate-serde_bucket-0.1.0 (c (n "serde_bucket") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1lx6f9wyyynmi7rlx8pl8aql4jnwizmmv36v6sj3j0jf3lii9g6a") (f (quote (("error")))) (y #t)))

(define-public crate-serde_bucket-0.1.1 (c (n "serde_bucket") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "101dfzmacb0xj06fs2drcp8rlq77khnf3566j06nzxd15mlw18z9") (f (quote (("error"))))))

(define-public crate-serde_bucket-0.1.2 (c (n "serde_bucket") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0s23vfb4yljngbh6jhq3886j5hkd5gdc4iwpbc8zgpxs1wba1pj8") (f (quote (("error"))))))

(define-public crate-serde_bucket-0.1.3 (c (n "serde_bucket") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "18ivp7fmmhv75zxsv0g7cpaqvid6mhskv2j8vs3x2bv9mj4l77zd") (f (quote (("error") ("deserializer"))))))

(define-public crate-serde_bucket-0.1.4 (c (n "serde_bucket") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0fv1c5phjin3gxgpwjz55bjmsfi35pxc4ki04gad4kbpdg1qid1v") (f (quote (("error") ("deserializer"))))))

(define-public crate-serde_bucket-0.1.5 (c (n "serde_bucket") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "15k7q29rphg9bx5gi90nadbqlqialjvwnz8s01hmnj8rg5gmqnam") (f (quote (("error") ("deserializer"))))))

