(define-module (crates-io se rd serde_krds) #:use-module (crates-io))

(define-public crate-serde_krds-0.1.0 (c (n "serde_krds") (v "0.1.0") (d (list (d (n "kindle_formats") (r "^0.1.1") (f (quote ("linked_hash_maps"))) (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0h58b25fqwlq2grgzm4hm7g64x4p4a93wfv7a697ig4755hvcppw")))

(define-public crate-serde_krds-0.1.1 (c (n "serde_krds") (v "0.1.1") (d (list (d (n "kindle_formats") (r "^0.1.1") (f (quote ("linked_hash_maps"))) (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0xps9vjhvn7dk4803q7wcp36zgxkc7g0yqxyi8d5q173w5mg2gcr")))

