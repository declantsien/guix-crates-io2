(define-module (crates-io se rd serde-helpers) #:use-module (crates-io))

(define-public crate-serde-helpers-0.1.0 (c (n "serde-helpers") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (o #t) (d #t) (k 0)))) (h "161jy1s664acp8x9vakcfd94d4s4xby118ik0xn8khxak2ilxhw3") (f (quote (("yaml" "serde_yaml") ("json" "serde_json") ("default" "json" "yaml"))))))

