(define-module (crates-io se rd serdata) #:use-module (crates-io))

(define-public crate-serdata-0.5.0 (c (n "serdata") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "rt-multi-thread" "macros" "fs" "io-std" "io-util" "sync"))) (d #t) (k 0)))) (h "0cgwynqxyr7xil0c1xwnxwn4x8z7bjljljymzz4gd9vsbw6h3ljp") (y #t)))

