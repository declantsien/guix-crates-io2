(define-module (crates-io se rd serde_json_path_to_error) #:use-module (crates-io))

(define-public crate-serde_json_path_to_error-0.0.2 (c (n "serde_json_path_to_error") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "082pjmsmm5m30k3dvlwrr0sfndjy909p6vd9gfys5ib0k9r0cl30")))

(define-public crate-serde_json_path_to_error-0.1.0 (c (n "serde_json_path_to_error") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "0f25pb42iwc9a4i9dckl705abc3pl343mq8lhnxhc6qrwaab6s8d") (r "1.56")))

(define-public crate-serde_json_path_to_error-0.1.1 (c (n "serde_json_path_to_error") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "1yfrnbc8hrz4ld7qi5fmzchk5sqdrkacwd89wy5w9v0d26zqpvp0") (r "1.56")))

(define-public crate-serde_json_path_to_error-0.1.2 (c (n "serde_json_path_to_error") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "17ncwbl3avd2x57bmc9kiphcy4c2b1s1r3yifmqqrpxq06hjyx5v") (r "1.56")))

(define-public crate-serde_json_path_to_error-0.1.3 (c (n "serde_json_path_to_error") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "1vm85v9m03lgh355khb71ra0arj23pz24p71sgs0gn17kb713ikr") (r "1.56")))

(define-public crate-serde_json_path_to_error-0.1.4 (c (n "serde_json_path_to_error") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)))) (h "0lvxvc91arwr2v665zsagp5aw6g6smfd9xgv6l38hkdh5r1w5wnp") (r "1.56")))

