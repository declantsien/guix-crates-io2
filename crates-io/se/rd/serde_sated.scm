(define-module (crates-io se rd serde_sated) #:use-module (crates-io))

(define-public crate-serde_sated-0.1.0 (c (n "serde_sated") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0rllilssggx9f6g5air32x16a018qlp4rwkg775g0665pnd4zw9q")))

(define-public crate-serde_sated-0.1.1 (c (n "serde_sated") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "187a7fa2979aim7vjjvgpqh686l31cqxahcz9kgrs3rd43gyfdx4")))

(define-public crate-serde_sated-0.1.2 (c (n "serde_sated") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1ilmlwp9h8ms6qdlvsk3wcx4z9s1rgy3m6gn9l5nn4vl2a3wyl0i")))

(define-public crate-serde_sated-0.1.3 (c (n "serde_sated") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0qz5i5hvd4dlkq6xffrnwgkg7p2kyzykxw09wy46qj3dm01nhknv")))

