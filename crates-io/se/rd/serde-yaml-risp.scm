(define-module (crates-io se rd serde-yaml-risp) #:use-module (crates-io))

(define-public crate-serde-yaml-risp-0.1.0 (c (n "serde-yaml-risp") (v "0.1.0") (d (list (d (n "risp") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1z1hw4lzi8qdfcaj511bij47aymm7m6khsnmc0yfvljkf7r1xlrs")))

