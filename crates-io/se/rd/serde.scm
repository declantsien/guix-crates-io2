(define-module (crates-io se rd serde) #:use-module (crates-io))

(define-public crate-serde-0.0.0 (c (n "serde") (v "0.0.0") (h "05n2qvvqd1jghn4ld7bcp4gkpzqiwdixdvf83zjiigdr4scjvfyi")))

(define-public crate-serde-0.2.0 (c (n "serde") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "^0.2.0") (d #t) (k 2)))) (h "10ba9qp270p677rjb5zxd4c5aib4gv592ihx7aag7l28z9lra1gj")))

(define-public crate-serde-0.2.1 (c (n "serde") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "^0.2.1") (d #t) (k 2)))) (h "0am3a1xlb5swgkl6m7712fb523qar8vplbs161lrkhd8bqpdp94b")))

(define-public crate-serde-0.3.0 (c (n "serde") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "serde_macros") (r "^0.3.0") (d #t) (k 2)))) (h "159yrgfd681kdsmj10lqc3cyxhh18qhngg9jlm1nz4ksd89qk1an")))

(define-public crate-serde-0.3.1 (c (n "serde") (v "0.3.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "serde_macros") (r "^0.3.1") (d #t) (k 2)))) (h "1bgw9xy33mzn5cbng0k3iimdnm45xyc9mxb35fnndwj3dqnnn2fj")))

(define-public crate-serde-0.3.2 (c (n "serde") (v "0.3.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "serde_macros") (r "^0.3.2") (d #t) (k 2)))) (h "0zf8fi2wf8x6dmjlyql4da1b9v69r773aln47hhf9qh29qhjq4zq")))

(define-public crate-serde-0.3.3 (c (n "serde") (v "0.3.3") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "serde_macros") (r "*") (d #t) (k 2)))) (h "1igcc27sfdksbybwnrcnlwww0394v6ska0gblhbdfy177bfv33xc")))

(define-public crate-serde-0.4.0 (c (n "serde") (v "0.4.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1praf3ysiqs2r1jflq5i34g9pwb1r11i9qy9644cjyj9rv10xbjg")))

(define-public crate-serde-0.4.1 (c (n "serde") (v "0.4.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0fa5dbfi4387vh51si9y8ky2gmls922hksm1g202h9hv2nqmfvmv")))

(define-public crate-serde-0.4.2 (c (n "serde") (v "0.4.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0q8xa6dyz3fmkfg1fnkw3axnkvhwf7gnpf4rdb06z4pmvzifmkv2")))

(define-public crate-serde-0.4.3 (c (n "serde") (v "0.4.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "06s2ayx1p5zzj4q7bfld60c9iprsk1256pnh8qj6h794mjinw11b") (f (quote (("nightly"))))))

(define-public crate-serde-0.5.0 (c (n "serde") (v "0.5.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "15lngxma3mvh12lghlizsp3dp9qafip2p0vzyx9q6685qarmfaw3") (f (quote (("nightly"))))))

(define-public crate-serde-0.5.1 (c (n "serde") (v "0.5.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1s9cyvvmqva5srz0gx093gk2bibnbs90f5hzd8rxb3sw4xppqmld") (f (quote (("nightly"))))))

(define-public crate-serde-0.5.2 (c (n "serde") (v "0.5.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "10iy2ss4y9awnj7ds8ggd07zxic04022wa6lzfwxa9k8j89q2x1b") (f (quote (("nightly"))))))

(define-public crate-serde-0.5.3 (c (n "serde") (v "0.5.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "13g78avicgaii15j1gxc4vwik13f5acfmshrjxxn1ccr9mk6ri4h") (f (quote (("nightly"))))))

(define-public crate-serde-0.6.0 (c (n "serde") (v "0.6.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "174033dhqd56k95nd8fmnrkdqdx6fhs375qjxwb94nzbjhav63k8") (f (quote (("nightly"))))))

(define-public crate-serde-0.6.1 (c (n "serde") (v "0.6.1") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "00yc7vf0dvnq38qmhwazs35bv12hf8ih1wx406s5zx6bq55q9d3g") (f (quote (("nightly"))))))

(define-public crate-serde-0.6.6 (c (n "serde") (v "0.6.6") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "09dgfvawa3ahfaiw5yklxq41wjqlggnydh886sd2f31l5z346syw") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly"))))))

(define-public crate-serde-0.6.7 (c (n "serde") (v "0.6.7") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "19d059wa3qvgc082m6mg6pyfb43ybvp7minr5d7k2zvppckh21df") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly"))))))

(define-public crate-serde-0.6.10 (c (n "serde") (v "0.6.10") (d (list (d (n "clippy") (r "^0.0.36") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "1d2kcx3n22gkz6axmrwim97qkh5zrv8ffbm1hv2wkzlmknajkc6j") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly" "clippy"))))))

(define-public crate-serde-0.6.11 (c (n "serde") (v "0.6.11") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "07nbs5brpib95n5v8lrjxjn78m1x3cs4j3lq3yvyivm7z2mvkazr") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly" "clippy"))))))

(define-public crate-serde-0.6.12 (c (n "serde") (v "0.6.12") (d (list (d (n "clippy") (r "^0.0.39") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "0b1wvp36scbkb7ahjb7awpd1viwp7fhv1h6xmyhj1x5zl8q1a5gw") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly-testing" "clippy"))))))

(define-public crate-serde-0.6.13 (c (n "serde") (v "0.6.13") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "1r7fdmh06g87z5rsxw76x581cy86kqmvvhznxjaf4xbya1rw2yis") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly-testing" "clippy"))))))

(define-public crate-serde-0.6.14 (c (n "serde") (v "0.6.14") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "0zw1rc34ppqlvy6h9na89k8nccc46fdndlgrhyp12dngicp9j8i8") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly-testing" "clippy"))))))

(define-public crate-serde-0.6.15 (c (n "serde") (v "0.6.15") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)))) (h "1ylid5gr7r5bfki391wwrgq3kszaqpb5fws93vql3r9xwplihyy9") (f (quote (("num-rational" "num/rational") ("num-impls" "num-bigint" "num-complex" "num-rational") ("num-complex" "num/complex") ("num-bigint" "num/bigint") ("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-serde-0.7.0 (c (n "serde") (v "0.7.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0wwv95da38g2bjkbancz0n91s3hq94iqx9xirl40cbzzmfs7lllh") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-serde-0.7.4 (c (n "serde") (v "0.7.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1na2q06jnjdcync6qkavfzfgq173b9s8xj5s59yfrr3wg39cx6jm") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-serde-0.7.5 (c (n "serde") (v "0.7.5") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1m183bbhfzakg8vw2wdy4ln8b0f9nl1id1484k5zn5hicf2jv721") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-serde-0.7.6 (c (n "serde") (v "0.7.6") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1wm52nvqjj4xdy80zx8svygbc03v68awmc7dv28dc5ljs0w39p1c") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly")))) (y #t)))

(define-public crate-serde-0.7.7 (c (n "serde") (v "0.7.7") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "17z9zl3gxlxmw220xv55p2vhp6sgfm2124l3hgn6ywygfd9b4sgz") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.8 (c (n "serde") (v "0.7.8") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "09xhr8cysdh7rf5qqi19g716cafz7ib2gpihdac0vx1d45sq0i3l") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.9 (c (n "serde") (v "0.7.9") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "06yvm9qsbnqsjf00wcjib9b04hymnfmfr8mr7yynw71gl2l36qdp") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.10 (c (n "serde") (v "0.7.10") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0zjki93n39y0r66acmh6dh1xnk1nkaplw73nkm44p6m5y1xys7kq") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.11 (c (n "serde") (v "0.7.11") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0iavhsq2s6v4srgjs3d1fc0crbdwxind2pjb4vr2dka9d4ykmj4w") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.12 (c (n "serde") (v "0.7.12") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0c7ijrdnazvq7k7jvbg4cyfbnmh4fif7v6qrf7p64bfjaxaykp1b") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.13 (c (n "serde") (v "0.7.13") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "05y20cly5vjrn2z853q9lcs1r9rgbry85xljj8biy7ynqnkfzm5v") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.7.14 (c (n "serde") (v "0.7.14") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1in9g0pfi88xnhk15kky852hb6sdr6rddk5m4238ffh7hlydf9na") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.8.0-rc1 (c (n "serde") (v "0.8.0-rc1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1lc3h4rgvg7lm3bz6x51613mlkh2jjg57x2mr60s29nvsjv096iz") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.8.0-rc2 (c (n "serde") (v "0.8.0-rc2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1hdzqc68hlz3bc55b5iaag7m7d75wbr6rqxxkg0b540vasxvl9w8") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.0-rc3 (c (n "serde") (v "0.8.0-rc3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "16plx1d96gxqd4c7d862s5hzp5bl50sqk7cijnq89skgzkqcz48g") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.7.15 (c (n "serde") (v "0.7.15") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "14haziy946c4m8h5bgg3c98558rbjim3jnl1c5zjdi4fm8r0f3hv") (f (quote (("std") ("nightly-testing" "clippy" "nightly" "std") ("nightly") ("default" "std") ("collections" "alloc") ("alloc" "nightly"))))))

(define-public crate-serde-0.8.0 (c (n "serde") (v "0.8.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0i9syhj1c50qwxwwxwpjcc27wqzdry8025y1p5xc46id8429ml3s") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.1 (c (n "serde") (v "0.8.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0wm1m0i60wb69aichl4204jwc1wl3n17g03jl7k9vhci1d8ss83p") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.2 (c (n "serde") (v "0.8.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "17cn2j604n7mcxfn9v9bkkzw7fmjg7vdrgc7ycy6f5q8akx2f049") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.3 (c (n "serde") (v "0.8.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "11nzrl9izqr4l145659rwbwf9787l24g3wx7mlhqww77bjcdinmz") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.4 (c (n "serde") (v "0.8.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0xapyixqd60gldlf43sz0vndra46381v6zjdp6l9zliipfgdmpxi") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.5 (c (n "serde") (v "0.8.5") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0zmg6bspfwf5pqy1b6xxinxfb1f5rflb8fhaclrhxj4n6f0r0llm") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.6 (c (n "serde") (v "0.8.6") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1cc8wj1pizj868zmi6i4kfkwn4fh3vsvgkhpcs7gd1yqx2qb1r8n") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.7 (c (n "serde") (v "0.8.7") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1x0nxzm1fzrmmr3mdjnrhl8jdn6fkk37kkd5cgpl0wn83crpj6cy") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.8 (c (n "serde") (v "0.8.8") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1gkpcn517gq3k58wlwizy3nhxm09jyghnnyjmfswbnlra18zs8w5") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.9 (c (n "serde") (v "0.8.9") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0vp98v3wy057byzndki913wl3nyb93y9k1ldlwbf1d0ir2wcmpxd") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.10 (c (n "serde") (v "0.8.10") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0rj0br3b8wr9h8qpl35dzm2db2gjm1ddclnb2xhsldcb4pkqf5ks") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.11 (c (n "serde") (v "0.8.11") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1n7nhmlwmdvwb24n48v3jy80z8445mrzwlhwfg2sldxqwhn6dnqm") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.12 (c (n "serde") (v "0.8.12") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0205qzan53d4va369327z08a1nj2r6x1swlx3fi0hixvzqmgbcpn") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.13 (c (n "serde") (v "0.8.13") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1vdhik4b430vdgabmkblhrvnlrlp5xmj78lv43605vxij583inql") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.14 (c (n "serde") (v "0.8.14") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1fp58hk1s9gkiy53s0nrrqkk7yma4914myg0ylhypxmkz24iizwf") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.15 (c (n "serde") (v "0.8.15") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1rd7pl1c7vkkw9x8ls55c6fclm02586ghyqkcfw4b70zyyhk2plc") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.16 (c (n "serde") (v "0.8.16") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0gllmk2f8f8skxz4km4a3r8dqamsd97mm2swfcnjs88b19fyc18i") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.17 (c (n "serde") (v "0.8.17") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1xj3ksb8ai9wj39in5fbb9zzd1nsxa5f8bqyxg56ahn846928kkq") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.18 (c (n "serde") (v "0.8.18") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0msaw0k7z84c8pailb95s6rb1p0r5isabwx76rfg8in2zai29dfr") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.19 (c (n "serde") (v "0.8.19") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1ran2c1l0xb4jm89034c8yjizmawd2288cb8ddz89662f449r8aq") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.20 (c (n "serde") (v "0.8.20") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "08dxcmxjgzwj5ki0vysvp8kq8qvyjp6ri2gqpdl6mr3plzaahfkr") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.21 (c (n "serde") (v "0.8.21") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0rls4w0gc1ngfx0c4bq8a0gz702fpkiv8gnml4z4frpp3kqnnz3v") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.22 (c (n "serde") (v "0.8.22") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "07yibxjq4xqi5g69q60vbnhhczn3lz55anlxr74hmf9gnsssmr7i") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.0-rc1 (c (n "serde") (v "0.9.0-rc1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0bl36gai9jqgwc87nfp58whmrf1amvsyhc619vaxqrn9wl4mrl6k") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.8.23 (c (n "serde") (v "0.8.23") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1j4ajipn0sf4ya0crgcb94s848qp7mfc35n6d0q2rf8rk5skzbcx") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.0-rc2 (c (n "serde") (v "0.9.0-rc2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0bq2rha5x1r1g8yk9s1n9g3jvbhfp44l1gmcd3v70qnnddxwgzsd") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.0-rc3 (c (n "serde") (v "0.9.0-rc3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0k6hg9mfi0g9nbfkzbyda9h8lqcd7mzishsrd7cnai6vspfxzvmz") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.0-rc4 (c (n "serde") (v "0.9.0-rc4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc3") (d #t) (k 2)))) (h "0sz3q89mpq7dxixiljkdjfmyh3948wc58a9fcvdgdlfzycds3izy") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.0 (c (n "serde") (v "0.9.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0-rc4") (d #t) (k 2)))) (h "1rr65dcjip3m5jl1iyv0smabwgfbvjvvrkq9vnih12cj2b3hvm3k") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.1 (c (n "serde") (v "0.9.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "0qpxvg4xgvd81bdkdswyx3ms5bx9siamcls1y25g12z5kf0hfh09") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.2 (c (n "serde") (v "0.9.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "15v24pplwiif9zc16kpg627n9idzy5wbqjlkcqahn53p8h2pgqnc") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.3 (c (n "serde") (v "0.9.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1w7556a99j6pwflaq4l3ybxk3c64ssnpfzll62v6p4wqly0nh97z") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.4 (c (n "serde") (v "0.9.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "19d43sywqmwhb6d09a9l9bbjanrfslgm2xq03nxpwd1gpbm08njp") (f (quote (("unstable-testing" "clippy" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.5 (c (n "serde") (v "0.9.5") (d (list (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "06f1ghcz7gmmsgbqr1wyi13sjd4ghypnxha9m979rl794l0833sd") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.6 (c (n "serde") (v "0.9.6") (d (list (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1yb2v2vgkblsh94iw96j3039lqlcl5ansj1263jdn2bwn34a7s8a") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.7 (c (n "serde") (v "0.9.7") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1fv9z950pfs3ryi9l4fgsz2m1psa50sykyzwcy2sg40gjrrxf3hy") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.8 (c (n "serde") (v "0.9.8") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "05p10clpfmm280qnf708xmd0iljnzlr32ifl7wqyfnrklprb0k90") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.9 (c (n "serde") (v "0.9.9") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1dzwrcxxd9l88kz4grvl4j3gj6aq0c33h67dwfvg91gqaf57p9h5") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.10 (c (n "serde") (v "0.9.10") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1pwsbn11c0y77g2gc0bnybx6i8yc36gljrq1gzmhbsr8m0ryz3d7") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.11 (c (n "serde") (v "0.9.11") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "13s5if46nhxqsqvy9da4szfsyihbdxywgrbjyr8nw0bwh2f320m7") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.12 (c (n "serde") (v "0.9.12") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "13c9aqw652ip5xssspr179v37ga86rngdird69wwcy0qgs7868zh") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.13 (c (n "serde") (v "0.9.13") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "16qp1zhcgxcfvw13vasnvp1rgv4bmyscy9kk8fg7c04lj1azs793") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.14 (c (n "serde") (v "0.9.14") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "14fxzdbi1wj1fmxzcgm5hzf9dxsrnrnl8lzg9lwk313gal6s9jd4") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.0 (c (n "serde") (v "1.0.0") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0dxf74s7lj479p8vx0gr65m5hilx69d6ryrpq3gy3gghw37k75in") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-0.9.15 (c (n "serde") (v "0.9.15") (d (list (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "1bsla8l5xr9pp5sirkal6mngxcq6q961km88jvf339j5ff8j7dil") (f (quote (("unstable-testing" "unstable" "std") ("unstable") ("std") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.1 (c (n "serde") (v "1.0.1") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1piybmhrvc6agrqfmwcdpmp7azzda5pjl59ajfwxzdqbya94kqhv") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.2 (c (n "serde") (v "1.0.2") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qy6zkzhqi789zpj4p4bv28909zk2f2m720xvc7h2c9rssfsaiiv") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.3 (c (n "serde") (v "1.0.3") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ryhg4ya13qsscdh0w8d8v5j5x4jk15212xm9w1703f5995dgk9n") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.4 (c (n "serde") (v "1.0.4") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0irdgr75rgbdy7kvwpf64lc8bkjnc7c03rwyrd37lfws82zgc7lr") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.5 (c (n "serde") (v "1.0.5") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15ggi0sa5s3mhlgiah843bzrn7q1zzr55m6mh3fam4k3hi6040g1") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.6 (c (n "serde") (v "1.0.6") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12jh1zasvy03i5ph2b642ybzpa9yha9vfr57mdlq1xjpawxdp8rq") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.7 (c (n "serde") (v "1.0.7") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11whxqkz9p53661pmir3i270hi9xi7pzc12mj8qi41d62s9xghy0") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.8 (c (n "serde") (v "1.0.8") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1d0v5kw7b11rv5gq6a10i7sdnv023y43cs8lny7w8kmqdz9k1xf2") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("collections" "alloc") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.9 (c (n "serde") (v "1.0.9") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0yjgbyxk1l8dchfk9wcgwagfxan89xdipxszgbsmv39f39snnz3a") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.10 (c (n "serde") (v "1.0.10") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bybr8ykcyf6jbshpxq492i44bix4jk743jymlwskm9hhngpsga3") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.11 (c (n "serde") (v "1.0.11") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1s93i1475m9x9zg7jgs3d6fisf1c6qg4cg0izwbinwzrvllnywpp") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.12 (c (n "serde") (v "1.0.12") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05i2j1vvqxcxx36znak5gybqpwp1vnylsyazfd65vv2qvm9vfqbz") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.13 (c (n "serde") (v "1.0.13") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1yq41vmpjvxnsr9scfsw7vr60xgcyf4si2s954i64fzcxgl3pzjw") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.14 (c (n "serde") (v "1.0.14") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0d6zdjhjggy6i2y2lb2ks9zg4lbqs2g166irfgh3wrj7g9isgdmw") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.15 (c (n "serde") (v "1.0.15") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16gpd93npkxzq7c48h77w0kz5gp2xfdqy29d1g8j5if6sk4lcw3a") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.16 (c (n "serde") (v "1.0.16") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02b8wb0hlhsv3628h1dh8bpywr8xzdsj189ff5r6akjdjqgn66p1") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.17 (c (n "serde") (v "1.0.17") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02nim05s829kczw50ca40wr2njdwwg1cfvh5bzrp9kxbbwhj7l6i") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.18 (c (n "serde") (v "1.0.18") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jwygjnqa8in9qlvbw7r7m2g8m9z7cnsk9kh3dy9qng3qk596n9r") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.19 (c (n "serde") (v "1.0.19") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0aj4m0nwz0dl39mqdqicwd7s6sibz4lkrlcbkdy72db8w5lsp70c") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.20 (c (n "serde") (v "1.0.20") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "173da6dad3lrsdg1z2iqpknd6qd7d5r80i4fh8z3b9mls8fihass") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.21 (c (n "serde") (v "1.0.21") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17nxx9dn7scyjqc69d5b1l5kw99sxgva74887dwfw5smhqz6dnkf") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.22 (c (n "serde") (v "1.0.22") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0i7igwszdm75jn0hnh2n86ra8fxq955ys5rcxdd06hjp2ak5vlzw") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.23 (c (n "serde") (v "1.0.23") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "018g2h2b2jas8b21y1f9q0dsf8vrf4vfj4x6x10h1w4jy7bkfz3a") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.24 (c (n "serde") (v "1.0.24") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "18dnbicmykdq2c8v4ic9frkdvfyrk5c29ncfmy5d11gsqm7anmqw") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.25 (c (n "serde") (v "1.0.25") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07wh2x1qwl7rnhjm9a838n4r138y845hqzjq8jf5j562d2x24q9q") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.26 (c (n "serde") (v "1.0.26") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1y4l3c6ffzy1zz677qykpffzay3x5sw4wbs4irkz5wi9vd4szmni") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.27 (c (n "serde") (v "1.0.27") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "09l53dkxlqg73jdi61jskc0qb1hx0gsmfq4rn8dsbyi0ks8z76fv") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.28 (c (n "serde") (v "1.0.28") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16370p7nlni2g45pmz3215rlwml36dii5843dz4hirhgn36zwa79") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.29 (c (n "serde") (v "1.0.29") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01mlbkrkg9b9728wf8alz30v4ws7s0m4v0ayc5954jcfjxrvfqs7") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.30 (c (n "serde") (v "1.0.30") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1maw12p5sfc3znr6ddzz8nmcvcbry0gm38qs194srkazm9066k8z") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.31 (c (n "serde") (v "1.0.31") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0iwrx41q3g1ald0nphlqh0dyp80f753vx207372ywj9if8avq4kq") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable")))) (y #t)))

(define-public crate-serde-1.0.32 (c (n "serde") (v "1.0.32") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1rqcv3rkqj4frvry9xjavhz3mg3yi44v0r9kzffybxikigh66gy7") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.33 (c (n "serde") (v "1.0.33") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01vq6m27f4pilay5wwmjs1719mpc2i0wvgc879fcw13gsjh5msag") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.34 (c (n "serde") (v "1.0.34") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pvmplyajirr70p1ncp37mhzl174a1j46yl5dgqmnnlqzc00s40f") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.35 (c (n "serde") (v "1.0.35") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13k461xis06riiyxyv0fshnfrn35by55l0vh757rjwj5i45dn3w0") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.36 (c (n "serde") (v "1.0.36") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mjbjz0jy9crdq9nxgwhn1wh1sp2md4nlg660c7cfhjahyp440f7") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.37 (c (n "serde") (v "1.0.37") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0icn72q551sspnq8wwqa8wsvzd1nxsjrrpb56wngbs6d1mkfxg6k") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.38 (c (n "serde") (v "1.0.38") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16bk7j76dr18z2xmjqhzvj4g0l6nrrv3691an06y08x8q6d3adjc") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.39 (c (n "serde") (v "1.0.39") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0c036arrf5yr7dy706bm4yr042g78g75jij153snrx4mjf2vdrjk") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.40 (c (n "serde") (v "1.0.40") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0j02by1hkx7ldh7pb9vm979ibghl696dygdy8k5x0rxpr595aii9") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.41 (c (n "serde") (v "1.0.41") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "053wvvv92vzbrlnqaihska75gnbfj57xick28cp50hmxayfiwxaz") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.42 (c (n "serde") (v "1.0.42") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1krdzws67r8cfp7i3ny8qh52dkk74jrj5kk52gnk5jaj2f376fd7") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.43 (c (n "serde") (v "1.0.43") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1l62isd6siljaz5r69fhsrzn9wavbr8hdg9308a0vwknha45v18c") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.44 (c (n "serde") (v "1.0.44") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "199mf80v0s7923qrf03b4npxgmlgwhnjg1w6r2zkh8l08y0a8ryf") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.45 (c (n "serde") (v "1.0.45") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "181442lx6mx8i3c69gr1b7813v343irb7ap7xz6smk9v283dhjba") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.46 (c (n "serde") (v "1.0.46") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00gh51pg19fd7wkzjshv74xa942c9scdwl2gfidkqwlk5l9nz4a0") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.47 (c (n "serde") (v "1.0.47") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07p276kypknp94djf5jplmkl8v8fcn0mz23qpbl47g2lsgammg69") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.48 (c (n "serde") (v "1.0.48") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0377qs2znxdamkwp6xgmxjhqqnkfqdfy4a8z9n6f741s88y07h66") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.49 (c (n "serde") (v "1.0.49") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0p8xiylrlzxhrbz7xzgcvq6rr05f8hazmmd0ys5i1ygcaah5ilhs") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.50 (c (n "serde") (v "1.0.50") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "14kgf6844qln47gk6pc2n4cjwmdj1lhg5x3vl4vy0kmrij0kv3a2") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.51 (c (n "serde") (v "1.0.51") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1nww2v5jnr51jr4w46gh5js34xy9z9ah613wl4rg50ajiv0lr4i1") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.52 (c (n "serde") (v "1.0.52") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17zppi7xhk1fnzwr4kpqairaiprczngysgsym3sj3wbvk73d088r") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.53 (c (n "serde") (v "1.0.53") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08lg7kwdpcbfglj55d532szziivyjs6sqv66h0nxknif28xywkfy") (f (quote (("unstable") ("std") ("rc") ("playground" "serde_derive") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.54 (c (n "serde") (v "1.0.54") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16cn2lwpjfiafkfwv5qb3g7i53h65rkjhw7viandgbpbplk1g76v") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.55 (c (n "serde") (v "1.0.55") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0jyqrz7giclbrxhmk2dxq7ilqh1n21wk2cxmhgwgh35srb1sdxlp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.56 (c (n "serde") (v "1.0.56") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sahrlqa5lyw241pafii9f8jxh2dibapnp1vc7rfcls9r6a362j9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.57 (c (n "serde") (v "1.0.57") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1a4cvwq65crr95wknmi9vkyvqnr7fyfm5r75dhaklwbvjm3z2y4l") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.58 (c (n "serde") (v "1.0.58") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0m6m6djd2jc90ahg89g3i96kgqvasm28gz61xqnc38p7zf7dzs9l") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.59 (c (n "serde") (v "1.0.59") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15fyz40qna1chk1r34va960d9m6j04a89krq3kbalg51c9irfk9a") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.60 (c (n "serde") (v "1.0.60") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00z85mnilpm5hhw4xcg2k95162dkgq4y7lfhnbgzymam03hkbjlz") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.61 (c (n "serde") (v "1.0.61") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "19hywk5y8pysv6dr1ipkz0wfix6cc04f98vz511bix2a4fc4q4yp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.62 (c (n "serde") (v "1.0.62") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0whwx0z2jvwkx5qyyb8xhkb4vdclsz67fnvzfnl8wfk7c8jmbna4") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.63 (c (n "serde") (v "1.0.63") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lm4bxsvc2ymn7qspji8k3q4fn2r4xpjzrm991a899nr9l0hfjkg") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.64 (c (n "serde") (v "1.0.64") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "14cg9pds0nfk1d16xhbigisfh2aapb6a934ckhj00lkc6h3bx9gv") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.65 (c (n "serde") (v "1.0.65") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0hy3dx1q18ciryhgxans3f086phlll6nh8nssh0qgzlqy2flcisx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.66 (c (n "serde") (v "1.0.66") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15ffq9lqh81g2rn627cxvhlwkvnniass4740d3vy082imjlxk8p9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.67 (c (n "serde") (v "1.0.67") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0q0zpv9adyifpj02lwhvhvw9yfxdn9113m2ca3k5j9nq61rmyf2d") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.68 (c (n "serde") (v "1.0.68") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bjnlh5krxbqjj37wij82xn2gfns4kksrhi2ahdk84caz97cr7s2") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.69 (c (n "serde") (v "1.0.69") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1j3l5rnvbk6vykz7ifrhqgyf1wkkpr22xcp94xsnsmlw2lxml3i1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.70 (c (n "serde") (v "1.0.70") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "080rr5ah8mij5xzvyv55gk85619vp0kqkbhxv63d3xksq0cxyfhc") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.71 (c (n "serde") (v "1.0.71") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1nqb0rgwkm521lpmgl1zkkv3m86zxj2r71gvf9glwn2li1fd1ykd") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.72 (c (n "serde") (v "1.0.72") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1w8wzvzm4in98mxhczm83gyr4411r6m82hi5i43sb41g3nqjrdwq") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.73 (c (n "serde") (v "1.0.73") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0z8k5v5pcics49arm1xbn3vr7dxxxf9kgfzqfaifxan8ak04fbb7") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.74 (c (n "serde") (v "1.0.74") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f33krfmjkabn1rqmprx5bfdqyg15ndwn17qgqlj9pai1p6vw67j") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.75 (c (n "serde") (v "1.0.75") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11zwmam91nkja1avm4sx0v3mkagy05hif2lh69k7w2zagi841lr2") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.76 (c (n "serde") (v "1.0.76") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1z8pr61i32mbnly0c600v3143gr17dp5adfjvz6pd58876p6j36h") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.77 (c (n "serde") (v "1.0.77") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0zxbai367kzgmx22q6dnxpl2sfcrmwc8kmaf53cy8g2jsxvpkrn6") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.78 (c (n "serde") (v "1.0.78") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05a4l3s9qjisr4bblk2pynic5b6k3dwmbxf4pgfsv6a6fpi99v4j") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.79 (c (n "serde") (v "1.0.79") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1adsrvdbz40s1h1k9rp21zfg1nz29mxmij184m3m3hsd0p6pq9c4") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.80 (c (n "serde") (v "1.0.80") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1vzpsfqf5jd0a57czfr8sb244azn9j3bz4608xd2dp97f3y43h8m") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.81 (c (n "serde") (v "1.0.81") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06wn9566q5960igjdzimd4cccfpd7dpbmjrr5r77ps0a36qba7n9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.82 (c (n "serde") (v "1.0.82") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1dim357nfh65g2bxzjh7hzwxhaqjb42a1j8impal2971mqcjz9bg") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.83 (c (n "serde") (v "1.0.83") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hiyxgs3rfxpp50rl05jr8xx04qfad2risjxly6rd7l58spi4zhm") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.84 (c (n "serde") (v "1.0.84") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1dxigsxgccazcmxqnhffa26zkfl555aknpjmc7cifb2rlpajwwqf") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.85 (c (n "serde") (v "1.0.85") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0llpa5hbsqrvhypr3314si4a13am5msj8n7dlfy723sym68qnjsk") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.86 (c (n "serde") (v "1.0.86") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gf6kizvfmp5nacklvpnj97sn4ganf0p3vvyi1xw54dh4xy4basj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.87 (c (n "serde") (v "1.0.87") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1vjqd99y48drlfif1ab7zg8s838zp39ypi1d3a4krs01g3izs81f") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.88 (c (n "serde") (v "1.0.88") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0l6q9gmxsjy0jjz6350x30blb2jf1dxz140wd6kwk51bixr1sc4z") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.89 (c (n "serde") (v "1.0.89") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0q1mcab7l5czglfnvsm54x65ch3dbh7h4bg34r8rn34hbywlylcj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.90 (c (n "serde") (v "1.0.90") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1x40hvjd60jcgdl3ishnbv0k2yn7z2sklz075jyvlx84h8h7qpxa") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.91 (c (n "serde") (v "1.0.91") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1za3x9mxv3x9539vq9casan61zfcvxc3ini3pjj25kj5zab9nbm7") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.92 (c (n "serde") (v "1.0.92") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gn9i38s47c7jg377mnq0qfk8hgnhhcsml7hdbq55avfybq6nx1j") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.93 (c (n "serde") (v "1.0.93") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1abrf67fa8q8jh3292m02z0crcqf820jj065bzkbdcq4f37jj3ln") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.94 (c (n "serde") (v "1.0.94") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ns7k7mxza1dcj9wgd8l8h367mpnp1v2aipdpb9ik758vrpnjsh7") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc" "unstable"))))))

(define-public crate-serde-1.0.95 (c (n "serde") (v "1.0.95") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0rq5qzwjwdihpg3qizancg6ml2hpi8jfbcxh345k7lnjnbb9yyp4") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-serde-1.0.96 (c (n "serde") (v "1.0.96") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jjy1h4qd3xy0yfhxy301dk9qg17ahl72d99y2l3h3mcsd4mwfvr") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.97 (c (n "serde") (v "1.0.97") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wlvfs82flb3di86m3nzf1m4vkc78vqcwrk865s0ldhrvgz3ssyl") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.98 (c (n "serde") (v "1.0.98") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04w1wsijcpim3jxh27hmpl6lhnhxl8ambbs8khnjznhpqrm65rbz") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.99 (c (n "serde") (v "1.0.99") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17ysh94r1443sf4ixhb0mf5m5rkmxr9wm28vlb4hs0bdnlg8bhpy") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.100 (c (n "serde") (v "1.0.100") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0nnrl6b053v0qpky46cj6ikcq7d5iys76406y87p64xj0s2kwizl") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.101 (c (n "serde") (v "1.0.101") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1p8r24hagcsrl92w5z32nfrg9040qkgqf8iwwnf7mzigpavwk5lp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.102 (c (n "serde") (v "1.0.102") (d (list (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1q0v1cd6810iia6m28i33nh4cyp0wdcjqy9s0627c20bkfykjjqc") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.103 (c (n "serde") (v "1.0.103") (d (list (d (n "serde_derive") (r "= 1.0.103") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00ip3xy09nk6c2b47ky1m5379yjmwk6n3sr2vmblp478p1xgj5qj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.104 (c (n "serde") (v "1.0.104") (d (list (d (n "serde_derive") (r "= 1.0.104") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ja4mgw4p42syjk7jkzwhj2yg6llfrfm7vn8rvy7v3c1bzr1aha1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.105 (c (n "serde") (v "1.0.105") (d (list (d (n "serde_derive") (r "= 1.0.105") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zrj157dxvmymp5ii60anap2qqks4pkr3fwsp71wi3sv4nzzn1z7") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.106 (c (n "serde") (v "1.0.106") (d (list (d (n "serde_derive") (r "= 1.0.106") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "169kg1px2k0hlxziiikdwy42wnrs2gjbvsv7yxygcwi08736mprn") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.107 (c (n "serde") (v "1.0.107") (d (list (d (n "serde_derive") (r "= 1.0.107") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kg02pp595lvb1zbb28gih53jv286dqn1wdh7b1gz26z5h7mb9zb") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.108 (c (n "serde") (v "1.0.108") (d (list (d (n "serde_derive") (r "= 1.0.108") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0hd7ksscspjyp6jhmfvsjbhq9az2m09jzr2qwz0r1zshvgxfkd4q") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.109 (c (n "serde") (v "1.0.109") (d (list (d (n "serde_derive") (r "= 1.0.109") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1p4scm0hnbn1cm3llcfil3jwkfrwqyj6b67gwl06i6kk8g7i7b60") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.110 (c (n "serde") (v "1.0.110") (d (list (d (n "serde_derive") (r "= 1.0.110") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0k10135zxqc67y6dhq5bc1vq3viy5aby8r4rp9pba5jd8q4b7rwr") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.111 (c (n "serde") (v "1.0.111") (d (list (d (n "serde_derive") (r "=1.0.111") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0zg3i95c663yqyfmqpwr1l3s6h60jjw6mk5jh003ig8cnksls4n9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.112 (c (n "serde") (v "1.0.112") (d (list (d (n "serde_derive") (r "=1.0.112") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0hy25vll5l297k5ia2d4bc0jdx8dkgcw7lfic9cqxzpas5raqskk") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.113 (c (n "serde") (v "1.0.113") (d (list (d (n "serde_derive") (r "=1.0.113") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0smc5pj00nci5g9a9hrz9w7rs7f5cir7gvsqf54pj74qc62cfdb1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.114 (c (n "serde") (v "1.0.114") (d (list (d (n "serde_derive") (r "=1.0.114") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lwcxlh8c09cs6qmwr6w68hl989mczwmwrzgc3p7hl0aixcgf5sk") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.115 (c (n "serde") (v "1.0.115") (d (list (d (n "serde_derive") (r "=1.0.115") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mamxl1ijys9r6jk8pj5vjxs7l60y11i845mhjpkhwnsya49lk75") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.116 (c (n "serde") (v "1.0.116") (d (list (d (n "serde_derive") (r "=1.0.116") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "19bhld1qyjs4qr3rd6dmzmy13imb6a1qbinb2fjqd0yjh6pmgzln") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.117 (c (n "serde") (v "1.0.117") (d (list (d (n "serde_derive") (r "=1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06nwyyma9hch1abjqj0y9cb09m1y6lbzbsc7jff6483pvs1sk3xq") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.118 (c (n "serde") (v "1.0.118") (d (list (d (n "serde_derive") (r "=1.0.118") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0028kv3dh3ix5g7jfws22zb9hcqq4cnpwn2lnlpam1wxhmil5ih6") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.119 (c (n "serde") (v "1.0.119") (d (list (d (n "serde_derive") (r "=1.0.119") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hwwx4a7lk4sxsrd11i2zmhmjagxd307zanqxx4ivdimkvs3dpcv") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.120 (c (n "serde") (v "1.0.120") (d (list (d (n "serde_derive") (r "=1.0.120") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1aqaqfm148wyi6c739gay87fp7rni4yb3r2q9bsvm08k0r4j6sqn") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.121 (c (n "serde") (v "1.0.121") (d (list (d (n "serde_derive") (r "=1.0.121") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ca09hmdrnmd41vahpb8rl09azp76lxx8i328sygc1mbdk3y6nb1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.122 (c (n "serde") (v "1.0.122") (d (list (d (n "serde_derive") (r "=1.0.122") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0b56fgnw1h23fspr0j4vb6lnizw1812rar9kkdchg9fq5ayz2klp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.123 (c (n "serde") (v "1.0.123") (d (list (d (n "serde_derive") (r "=1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bk9733mgiv5sg8yb19y8mc85fb2aaqp1k02v10alavj688idmcj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.124 (c (n "serde") (v "1.0.124") (d (list (d (n "serde_derive") (r "=1.0.124") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17yjk4g886jzq8ihn5k6523mbpp95m8scgdbp7xlaanbazwiyxmx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.125 (c (n "serde") (v "1.0.125") (d (list (d (n "serde_derive") (r "=1.0.125") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0w8i0f4wsq4zd9vz1k6lq00066rjrgzlxkm25h8sfpss387cb3am") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.126 (c (n "serde") (v "1.0.126") (d (list (d (n "serde_derive") (r "=1.0.126") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00vdk7y3j8h2xv28a2i2ad1d19g5iwrdknbq8yp79v6axamhaxgc") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.127 (c (n "serde") (v "1.0.127") (d (list (d (n "serde_derive") (r "=1.0.127") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1s7s8vqwf6lgmddcqjpcrx907afgn83lzwnkg5klxlgnmdw9hfzh") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.128 (c (n "serde") (v "1.0.128") (d (list (d (n "serde_derive") (r "=1.0.128") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0d3834if88xc5kr4dqwxq70ks57ryrvsdz74yvqdpsbq37ds0mhh") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.129 (c (n "serde") (v "1.0.129") (d (list (d (n "serde_derive") (r "=1.0.129") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1wgqi7j6szk736wj61gjvbplx3lx7cx4gnlf2x9khxdas8v2ixyi") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.130 (c (n "serde") (v "1.0.130") (d (list (d (n "serde_derive") (r "=1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04y9s1mxcxakg9bhfdiff9w4zzprk6m6dazcpmpi8nfg6zg0cbgi") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-serde-1.0.131 (c (n "serde") (v "1.0.131") (d (list (d (n "serde_derive") (r "=1.0.131") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hb10pb4m2qj5liiqaqs03xwsr9diisfcr6c6a8kci9yppgnkbdl") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.132 (c (n "serde") (v "1.0.132") (d (list (d (n "serde_derive") (r "=1.0.132") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "027hwywf8pyvqsjkgikjdbm22pvhpd626xzbswgws1gk7k17b64b") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.133 (c (n "serde") (v "1.0.133") (d (list (d (n "serde_derive") (r "=1.0.133") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16lq33l09nkm0hxdhfjjmh3yjv83rrcqw9lbxb8y4q3va5km0mlp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.134 (c (n "serde") (v "1.0.134") (d (list (d (n "serde_derive") (r "=1.0.134") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0smwnf9adwzjfpgwdn308j55cgxhd8rrla2v9wbzivch2r6c7cwn") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.135 (c (n "serde") (v "1.0.135") (d (list (d (n "serde_derive") (r "=1.0.135") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0axdrddc78biwmv7pn0r7z2q2iw1c5a6d55prpfs4kj96daj7y9c") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.136 (c (n "serde") (v "1.0.136") (d (list (d (n "serde_derive") (r "=1.0.136") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12a791cbdd3gi08536i4frrqsps0ak8gvhpijvgj9rg1055y4cff") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.15")))

(define-public crate-serde-1.0.137 (c (n "serde") (v "1.0.137") (d (list (d (n "serde_derive") (r "=1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1l8pynxnmld179a33l044yvkigq3fhiwgx0518a1b0vzqxa8vsk1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.138 (c (n "serde") (v "1.0.138") (d (list (d (n "serde_derive") (r "=1.0.138") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0irai0sf2jrrrzyni7bclly5fmk8p77ylzj4qmld3fc6awjccy0m") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.139 (c (n "serde") (v "1.0.139") (d (list (d (n "serde_derive") (r "=1.0.139") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mna8q52l2qc2ipqw8jwbxplrs7d7sdqbq5f8j5scnp4i6wfnw81") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.140 (c (n "serde") (v "1.0.140") (d (list (d (n "serde_derive") (r "=1.0.140") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00rvf8c4wq1a3wac2z0cqa0ny7zgciqhz1mmkqv7qywnqx15m1gw") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.141 (c (n "serde") (v "1.0.141") (d (list (d (n "serde_derive") (r "=1.0.141") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0015l8rqif2cijlfp31whxc4cl997sj25rhgpnqgr6avr7r77y3s") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.142 (c (n "serde") (v "1.0.142") (d (list (d (n "serde_derive") (r "=1.0.142") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qpyympfvmkmqi0jbsd71kbwqgsg57izc3fhw4hv5dkgj4vw9475") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.143 (c (n "serde") (v "1.0.143") (d (list (d (n "serde_derive") (r "=1.0.143") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ls54n526mbqw324h3y62whl54fdbad4vmn6ym7zf909nzaybs2k") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.144 (c (n "serde") (v "1.0.144") (d (list (d (n "serde_derive") (r "=1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0q3qy4cv4jjmwa23nrgrppfxh2g8ahr7fs4iijw47k9xvq87fx0g") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.145 (c (n "serde") (v "1.0.145") (d (list (d (n "serde_derive") (r "=1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12snfm7vwzmj377aifjs570wr49glz2zzpv06scwpg1h2hsvd3kj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.146 (c (n "serde") (v "1.0.146") (d (list (d (n "serde_derive") (r "=1.0.146") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07rn75a3vrml2ywlvcczc5fpizy8x9rq7crfnkhlibd0c1x0pxbd") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.147 (c (n "serde") (v "1.0.147") (d (list (d (n "serde_derive") (r "=1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0rc9jj8bbhf3lkf07ln8kyljigyzc4kk90nzg4dc2gwqmsdxd4yi") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.148 (c (n "serde") (v "1.0.148") (d (list (d (n "serde_derive") (r "=1.0.148") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1p62a9n1527bv3k0fmjgny1ps12mr90v3qbn0rnis6d09fxn8gz5") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.149 (c (n "serde") (v "1.0.149") (d (list (d (n "serde_derive") (r "=1.0.149") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0mg0x35hdc8wrzxjsqg61r6h5jgpq4y6wmwlgdq0wn8c68r9jsr5") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.150 (c (n "serde") (v "1.0.150") (d (list (d (n "serde_derive") (r "=1.0.150") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "14azw28cg03cdrdj2m81xydjrggzx4vqlb156gdbbwa2h3ncj9p3") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.151 (c (n "serde") (v "1.0.151") (d (list (d (n "serde_derive") (r "=1.0.151") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "187bgyl6zclk4js5xhaj7cas24cmwqsnknz68k898jd2q4gx9zlp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.152 (c (n "serde") (v "1.0.152") (d (list (d (n "serde_derive") (r "=1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ysykpc4a9f1yn7zikdwhyfs0bpa7mlc8vsm7sl4glr1606iyzdv") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.13")))

(define-public crate-serde-1.0.153 (c (n "serde") (v "1.0.153") (d (list (d (n "serde_derive") (r "=1.0.153") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "084dr6f3fkap4ndkvwgba58dbmiwjss0nhw7w4k8a4dsnir2qf1s") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.154 (c (n "serde") (v "1.0.154") (d (list (d (n "serde_derive") (r "=1.0.154") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17n4c5lx3g67gi5ifjvqk43x0sri56qvz6ssnhd7yplj2c91bpcc") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.155 (c (n "serde") (v "1.0.155") (d (list (d (n "serde_derive") (r "=1.0.155") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1f46kqy02ldnznlf3l5l9j82s6w0yky7n77y1chx9ihmfj0v9wki") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.156 (c (n "serde") (v "1.0.156") (d (list (d (n "serde_derive") (r "=1.0.156") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "19394in28sb9gh1v2153rqkyq46irr81x5a20701gpha5h4mnjri") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.157 (c (n "serde") (v "1.0.157") (d (list (d (n "serde_derive") (r "=1.0.157") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ji57yw36a26zkg0857gnzcjzg4hnkmdg3d9zj45fayzypyfazbh") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.158 (c (n "serde") (v "1.0.158") (d (list (d (n "serde_derive") (r "=1.0.158") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jfr4na9isxr3g5h61dyx15z9r35sc6p2b710n417vk386f4s7bp") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.159 (c (n "serde") (v "1.0.159") (d (list (d (n "serde_derive") (r "=1.0.159") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0r9hd539dzp9b7h13w9akwh33pv8f3bvk449ym0yrbix7hsfh11w") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.160 (c (n "serde") (v "1.0.160") (d (list (d (n "serde_derive") (r "=1.0.160") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0v11q6pjdjivw24cv98zv9dkdx50d6h9748lgvdbrqxwr1q3fbxv") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.161 (c (n "serde") (v "1.0.161") (d (list (d (n "serde_derive") (r "=1.0.161") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1vw59ccp377sxq5zpgvcyafyp4kqjsl9njx6f06zadh1y36rfxw5") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.162 (c (n "serde") (v "1.0.162") (d (list (d (n "serde_derive") (r "=1.0.162") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1dksgs0zi9wdh3bm3gzzsvmgg39fn8vb4d8gbz09haswmghzdcki") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.163 (c (n "serde") (v "1.0.163") (d (list (d (n "serde_derive") (r "=1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hmyrqi2gszlngycz0rdznda7zr0q0nw1imm13h9llvsp18sn4r1") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.164 (c (n "serde") (v "1.0.164") (d (list (d (n "serde_derive") (r "=1.0.164") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0z82r42ayqb48vw4cbsja4byl779vh33p4k4q6dpd3z973wqr34y") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.165 (c (n "serde") (v "1.0.165") (d (list (d (n "serde_derive") (r "=1.0.165") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1z281mx0pgf26bqgl509ngfc4hqm2rz2j0sgrrdwq33xpc1gjff9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.166 (c (n "serde") (v "1.0.166") (d (list (d (n "serde_derive") (r "=1.0.166") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1f0dy7sc5p1f1r2a4kxyplm7sdrcg2kkd9p682nx6hflz42786yh") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.167 (c (n "serde") (v "1.0.167") (d (list (d (n "serde_derive") (r "=1.0.167") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0dxj485c99av9zx9l2q2kx12ns2dqbhggkwl3nm44fs6aqs53bvx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.168 (c (n "serde") (v "1.0.168") (d (list (d (n "serde_derive") (r "=1.0.168") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1br697rmgkfm1p578midw90s7wwkpr1wicq8s7g6f0vj92azh56n") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.169 (c (n "serde") (v "1.0.169") (d (list (d (n "serde_derive") (r "=1.0.169") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1c14k1nqpfydqfxaxx8pj4z16gfi9byx0bf1wqqxa04mizdw6ldx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.170 (c (n "serde") (v "1.0.170") (d (list (d (n "serde_derive") (r "=1.0.170") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0fg4yssammjqdvj2cg275hcczbjjh75gjhh5hj7wmaxs2bsmfrm5") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.171 (c (n "serde") (v "1.0.171") (d (list (d (n "serde_derive") (r "=1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1a9lvibgi42mhmgafp747mvshsq6ybx6rzcjqh398rfp9wg7vqih") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.172-alpha.0 (c (n "serde") (v "1.0.172-alpha.0") (d (list (d (n "serde_derive") (r "=1.0.172-alpha.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.172-alpha.0") (d #t) (k 2)))) (h "05kdfcnqil7yvzy5f3mn5lirwa8az3snwm7s5c1397003sf14k8c") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.172 (c (n "serde") (v "1.0.172") (d (list (d (n "serde_derive") (r "=1.0.172") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "055yfr7apklkr7cgimnpf9q47sp2kmm58yjypddg5ia90mk9wj1w") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.173 (c (n "serde") (v "1.0.173") (d (list (d (n "serde_derive") (r "=1.0.173") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0gym73xg28rkw7pwhr8fd0sbc78zr4px4msa99qvq837dn4p07z9") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.174 (c (n "serde") (v "1.0.174") (d (list (d (n "serde_derive") (r "=1.0.174") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1cd95wcy3x5qwi1rxdc5mdj5gc4ln5qalfymjm9mxgd5jdj7b21v") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.175 (c (n "serde") (v "1.0.175") (d (list (d (n "serde_derive") (r "=1.0.175") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "02zzxg8hlg2b06yrgl6n0fvmvs5m68jgx9j84x708z9rsyf469ax") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.176 (c (n "serde") (v "1.0.176") (d (list (d (n "serde_derive") (r "=1.0.176") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1p34f7a9xh9qsc1akjnf4zxrr428dnw3cc9rds0p0p9wab4jip3n") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.177 (c (n "serde") (v "1.0.177") (d (list (d (n "serde_derive") (r "=1.0.177") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "02jn1kfmy8hmcfpvvy9wbm2mipsjkl0m12ya345jxy3bm8b2bfk3") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.178 (c (n "serde") (v "1.0.178") (d (list (d (n "serde_derive") (r "=1.0.178") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0j0kk1md1ihf3zh032mhhzwx4hcjvigv5ni0lmk05gm777fkndk0") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.179 (c (n "serde") (v "1.0.179") (d (list (d (n "serde_derive") (r "=1.0.179") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1h75vjpymizr52xybm3wa6510ag20a3b1pd172zllz92ilmz8nqa") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.19")))

(define-public crate-serde-1.0.180 (c (n "serde") (v "1.0.180") (d (list (d (n "serde_derive") (r "=1.0.180") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1vdkcc4d1vdbkiaw9fli7f4l0f00irwf5iiy9s5fi3q57wc7z9hf") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.181 (c (n "serde") (v "1.0.181") (d (list (d (n "serde_derive") (r "=1.0.181") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "144q7qs3zcwbr8vkx348kb318cb3iqlkkhk3l2yw0h1j7k4p6gkd") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.182 (c (n "serde") (v "1.0.182") (d (list (d (n "serde_derive") (r "=1.0.182") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0c74669l3z5mn9jm9ybd25hsz4xyy55hpx4rl8gplnqz8xs0mcxx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.183 (c (n "serde") (v "1.0.183") (d (list (d (n "serde_derive") (r "=1.0.183") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0g7bv28rp0h0bdy116r63aldyvlfkpy318a54x9nv1vp4sh8vb1j") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.184 (c (n "serde") (v "1.0.184") (d (list (d (n "serde_derive") (r "=1.0.184") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1w7zyjbyfscb8kd6y17sf3i4zgq3b7zlwyj06n85qf6p0i5iz49c") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.185 (c (n "serde") (v "1.0.185") (d (list (d (n "serde_derive") (r "=1.0.185") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0cdwyn6g7jjsa77l1za8a5g3m5yn231lbylgaqxlrmfzy5lnz6xy") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.186 (c (n "serde") (v "1.0.186") (d (list (d (n "serde_derive") (r "=1.0.186") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1hdphc7iicvpwd631abahpgx5dpl70lznsaysjdxw2f0411b4pcz") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.187 (c (n "serde") (v "1.0.187") (d (list (d (n "serde_derive") (r "=1.0.187") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0k5f3g233plznjk0q41v0782zq0gl1grzwcaawgbsm964lagx9rh") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.188 (c (n "serde") (v "1.0.188") (d (list (d (n "serde_derive") (r "=1.0.188") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "17jlqzfhimsk8w37ifjwnm86nwjzawlbgwmwc7nhwdwslv5hz7ng") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.189 (c (n "serde") (v "1.0.189") (d (list (d (n "serde_derive") (r "=1.0.189") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0dq542wki7rn2inhg70f35qjzd8aayjfkvcfvhdh1m2awx22lhlf") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.190 (c (n "serde") (v "1.0.190") (d (list (d (n "serde_derive") (r "=1.0.190") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1xwndn6n8pb8y0vd84sba1nvfdf4x27nkbgnqsi99s0yr8sc7lwi") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.191 (c (n "serde") (v "1.0.191") (d (list (d (n "serde_derive") (r "=1.0.191") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1jc0l0wjsmqnr116962iwwa5vnfl80i5as0lh8j850qr221c8d58") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.192 (c (n "serde") (v "1.0.192") (d (list (d (n "serde_derive") (r "=1.0.192") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "00ghhaabyrnr2cn504lckyqzh3fwr8k7pxnhhardr1djhj2a18mw") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.193 (c (n "serde") (v "1.0.193") (d (list (d (n "serde_derive") (r "=1.0.193") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "129b0j67594f8qg5cbyi3nyk31y97wrqihi026mba34dwrsrkp95") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.194 (c (n "serde") (v "1.0.194") (d (list (d (n "serde_derive") (r "=1.0.194") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0wxplk1ayrsb81bdwh8zmkldw1b0xigs3qc90r5ck6374nc4848b") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.195 (c (n "serde") (v "1.0.195") (d (list (d (n "serde_derive") (r "=1.0.195") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "00kbc86kgaihpza0zdglcd2qq5468yg0dvvdmkli2y660bs1s9k3") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.196 (c (n "serde") (v "1.0.196") (d (list (d (n "serde_derive") (r "=1.0.196") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0civrvhbwwk442xhlkfdkkdn478by486qxmackq6k3501zk2c047") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.197 (c (n "serde") (v "1.0.197") (d (list (d (n "serde_derive") (r "=1.0.197") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1qjcxqd3p4yh5cmmax9q4ics1zy34j5ij32cvjj5dc5rw5rwic9z") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.198 (c (n "serde") (v "1.0.198") (d (list (d (n "serde_derive") (r "=1.0.198") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1k0z8mwkkl46bwfk16z7v8xidi5pwnj4a9fsf42k8cchjw6a8ilq") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.199 (c (n "serde") (v "1.0.199") (d (list (d (n "serde_derive") (r "=1.0.199") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0slnp99f9in676pvkl3d15zii66v83xp2rwrjk6pfv03vxv6x7qc") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.200 (c (n "serde") (v "1.0.200") (d (list (d (n "serde_derive") (r "=1.0.200") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0kyfkis7jg71wak6l2bfpy7gffpx22ha7vgpm8hhwz6njk6gkinx") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.201 (c (n "serde") (v "1.0.201") (d (list (d (n "serde_derive") (r "=1.0.201") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0g1nrz2s6l36na6gdbph8k07xf9h5p3s6f0s79sy8a8nxpmiq3vq") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.202 (c (n "serde") (v "1.0.202") (d (list (d (n "serde_derive") (r "=1.0.202") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "15d3if1151db1z89qibk0f8bpy64d93kmxypyrgvmchisjh62sr2") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

(define-public crate-serde-1.0.203 (c (n "serde") (v "1.0.203") (d (list (d (n "serde_derive") (r "=1.0.203") (d #t) (t "cfg(any())") (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1500ghq198n6py5anvz5qbqagd9h1hq04f4qpsvjzrvix56snlvj") (f (quote (("unstable") ("std") ("rc") ("derive" "serde_derive") ("default" "std") ("alloc")))) (r "1.31")))

