(define-module (crates-io se rd serde_plain) #:use-module (crates-io))

(define-public crate-serde_plain-0.1.0 (c (n "serde_plain") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "1zxlav0f8l6hbh7pb2vq42j32lv39azx5i699jibz2vrqd9dx811")))

(define-public crate-serde_plain-0.2.0 (c (n "serde_plain") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "0sz5cq48dxn7zj27h6q0988cpwqjpfmc9ni5w83bzx6q7vn1ram4")))

(define-public crate-serde_plain-0.3.0 (c (n "serde_plain") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "1nlfwfgc54wzd8k7pckvq8p2zidy24bcqjm94ss94q005gdb0pv2")))

(define-public crate-serde_plain-1.0.0 (c (n "serde_plain") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "0n1q2gdryp9jfmw89vixld68ldmydwiayw11wx921nps55z5wicm")))

(define-public crate-serde_plain-1.0.1 (c (n "serde_plain") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "1bil7ixlgky9d3wbx6ygr4l00am1c95zxcaprv87kcax660q00fn")))

(define-public crate-serde_plain-1.0.2 (c (n "serde_plain") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.29") (d #t) (k 2)))) (h "0l4d4nbw00pz6n43icrc605bhgynfmlyq39sn8i10qasnrnzrqcw")))

