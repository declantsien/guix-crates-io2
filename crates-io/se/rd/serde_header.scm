(define-module (crates-io se rd serde_header) #:use-module (crates-io))

(define-public crate-serde_header-0.1.0 (c (n "serde_header") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0r2xgdw7wp866gpibmgk3shcmdxmify5dzbif7myllfc379ck9w5")))

(define-public crate-serde_header-0.1.1 (c (n "serde_header") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1i8ccb13038dhszw63w1bys3cibx4faqzaq5gxbnmpycszkdk0j5")))

(define-public crate-serde_header-0.2.0 (c (n "serde_header") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1k8yqw7yxwnv8kv00vhjkc2g64p1p90wmiz36m2w1sn7afab9p6h") (f (quote (("crate_http" "http"))))))

