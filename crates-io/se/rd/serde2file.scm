(define-module (crates-io se rd serde2file) #:use-module (crates-io))

(define-public crate-serde2file-0.1.0 (c (n "serde2file") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1h3ikamrb3p99653k6cra69q174r4mbf4mz2mb0phbvmsni36jdn")))

(define-public crate-serde2file-0.2.0 (c (n "serde2file") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0xg73g24h3w2cc0y1alw0vw9zwhqrc88gckbz3bhm87i78ax9b2z")))

(define-public crate-serde2file-0.2.2 (c (n "serde2file") (v "0.2.2") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0l0lqb191aknyylbcll3va63zy71q76a5rhwcsw4l86md7dr6vfr")))

(define-public crate-serde2file-0.2.3 (c (n "serde2file") (v "0.2.3") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "02ly6skq3gcf1y4zbc5p1s8r8mhq2wm00y71grpfihzxydlg75bz")))

(define-public crate-serde2file-0.2.4 (c (n "serde2file") (v "0.2.4") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0wcpnnfkwp84qjf321ga6xglypwm69bxvk0hsgfvwg5vmjdjagzs")))

(define-public crate-serde2file-0.3.0 (c (n "serde2file") (v "0.3.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1l6p3bw0gszbia03ciiy8l049nb4rpg1vsrqxa2bhi8aw2pgbnh8")))

(define-public crate-serde2file-0.4.0 (c (n "serde2file") (v "0.4.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "06wxcn7slhd9hmlz37fgpdkdi4lib9zp68j88ykgmp6s2pzvdzxn")))

(define-public crate-serde2file-0.6.0 (c (n "serde2file") (v "0.6.0") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1wk9zhgyjjxgw4m5bmv9388jrv8hiwi5xp188yd9niciv7c8skxz")))

(define-public crate-serde2file-0.6.1 (c (n "serde2file") (v "0.6.1") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "12yfdwzf28m2ldswx0rrz57l98zc1qzkji4nwkgibcjs4m2y38s5")))

(define-public crate-serde2file-0.6.2 (c (n "serde2file") (v "0.6.2") (d (list (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0iydybrdpdn816a5ap6y3nhpn4lkmmxrihlmi0skq32nrd0w238i")))

(define-public crate-serde2file-0.6.3 (c (n "serde2file") (v "0.6.3") (d (list (d (n "aes-gcm") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1kpf9f5vdpx7vrjcvj6d040n5hlf8sxfpgzlyp90s0qfph8073d7") (f (quote (("default" "encrypt_tool" "anyhow") ("core")))) (s 2) (e (quote (("encrypt_tool" "dep:aes-gcm") ("anyhow" "dep:anyhow"))))))

(define-public crate-serde2file-0.6.4 (c (n "serde2file") (v "0.6.4") (d (list (d (n "aes-gcm") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "17x2qcp5az2bqpmbqzyj7x4dzsw0w8yc4zrq766lfvg2s3yz2m6x") (f (quote (("default" "encrypt_tool") ("core")))) (s 2) (e (quote (("encrypt_tool" "dep:aes-gcm"))))))

(define-public crate-serde2file-0.6.5 (c (n "serde2file") (v "0.6.5") (d (list (d (n "aes-gcm") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1ym7y6z90fd4aa1afsd07x62q4np8pxssihvq5fvcgfdksrryx95") (f (quote (("default" "encrypt_tool") ("core")))) (s 2) (e (quote (("encrypt_tool" "dep:aes-gcm"))))))

(define-public crate-serde2file-0.6.6 (c (n "serde2file") (v "0.6.6") (d (list (d (n "aes-gcm") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0hyxm0frnwb0w1iyphid4n28c6ba7rizwajsmcnpkxglk2ifpj9f") (f (quote (("default" "encrypt_tool") ("core")))) (s 2) (e (quote (("encrypt_tool" "dep:aes-gcm"))))))

(define-public crate-serde2file-0.6.7 (c (n "serde2file") (v "0.6.7") (d (list (d (n "aes-gcm") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde2file_macro_derive") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1szvm77cmim54rydcdihqjfkl53d2fhlh5jv66j1zrif0cjdys5y") (f (quote (("default" "encrypt_tool") ("core")))) (s 2) (e (quote (("encrypt_tool" "dep:aes-gcm"))))))

