(define-module (crates-io se rd serde_bencode) #:use-module (crates-io))

(define-public crate-serde_bencode-0.1.0 (c (n "serde_bencode") (v "0.1.0") (d (list (d (n "serde") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.19") (d #t) (k 0)))) (h "0ss90khg3lc0hxjs7dcspicnpgarc23szyxq5v40zxkk1bryrswg")))

(define-public crate-serde_bencode-0.1.1 (c (n "serde_bencode") (v "0.1.1") (d (list (d (n "serde") (r "^0.8.19") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.19") (d #t) (k 0)))) (h "06cxnxqnmp1c5m18aplzdlmppcp1bi2fj9h7b9qk8pbn8606wr5j")))

(define-public crate-serde_bencode-0.1.2 (c (n "serde_bencode") (v "0.1.2") (d (list (d (n "serde") (r "^0.8.22") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.22") (d #t) (k 0)))) (h "1358ay91kgyg8dcwycfr9b9ywp35m82zvkyq2l37r7k6fa9qj47w")))

(define-public crate-serde_bencode-0.1.3 (c (n "serde_bencode") (v "0.1.3") (d (list (d (n "serde") (r "^0.8.22") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.22") (d #t) (k 0)))) (h "1jhrdndr3bch2p6w6jxx267ahyh8w51z4wpza7x20inda1pzw66z")))

(define-public crate-serde_bencode-0.2.0 (c (n "serde_bencode") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0hvlclmia9g9mic3v9dpw6swa13366807q08x8psyf4n6q8wwycv")))

(define-public crate-serde_bencode-0.2.1 (c (n "serde_bencode") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0i8db2ymwwrbpyy2j3nd3s8pp5bhxrbvfxfz171aq43b3g0ljp1i")))

(define-public crate-serde_bencode-0.2.2 (c (n "serde_bencode") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04ycvfv8mn081rabhin7f9c55ha78kyhzlqgpjhb3rxzp5kr32q0")))

(define-public crate-serde_bencode-0.2.3 (c (n "serde_bencode") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "03f3l6f390mpddn885vpi5hrc7919913724sxbxdl9h1mbdqnkck")))

(define-public crate-serde_bencode-0.2.4 (c (n "serde_bencode") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.176") (d #t) (k 2)))) (h "0gj17p1w5hyi69fngv55dai4nb4fmdij76gqwyb9if9qfixzq3d7")))

