(define-module (crates-io se rd serde_with_extension) #:use-module (crates-io))

(define-public crate-serde_with_extension-0.1.0 (c (n "serde_with_extension") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1") (f (quote ("serde-with-float"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.11.0") (d #t) (k 0)))) (h "03s30i69cz24kwkkiz031n24bdcq23697w4xgmks7kvm8k5i15rg") (f (quote (("default") ("decimal" "rust_decimal")))) (r "1.56")))

(define-public crate-serde_with_extension-2.0.0 (c (n "serde_with_extension") (v "2.0.0") (d (list (d (n "rust_decimal") (r "^1") (f (quote ("serde-with-float"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0rkls3wqd5maqslvks2ak06dcp79f2hmrs2ja8zg6022w5f6a4kw") (f (quote (("default") ("decimal" "rust_decimal") ("all" "decimal")))) (r "1.66")))

(define-public crate-serde_with_extension-3.0.0 (c (n "serde_with_extension") (v "3.0.0") (d (list (d (n "rust_decimal") (r "^1") (f (quote ("serde-with-float"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_with") (r "^3") (d #t) (k 0)))) (h "09w9xh6m7wqhvii88302hgnqvr10i7gkg2mj8k3qcnn2bvh7lbix") (f (quote (("default") ("decimal" "rust_decimal") ("all" "decimal")))) (r "1.66")))

