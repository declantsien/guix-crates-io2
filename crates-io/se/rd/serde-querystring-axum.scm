(define-module (crates-io se rd serde-querystring-axum) #:use-module (crates-io))

(define-public crate-serde-querystring-axum-0.2.0 (c (n "serde-querystring-axum") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "axum-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-querystring") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 2)))) (h "0yyprgh621016421xhmm71s6fd7fyag80bndncywcaq1rsp74l52") (r "1.56")))

