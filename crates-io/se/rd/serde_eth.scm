(define-module (crates-io se rd serde_eth) #:use-module (crates-io))

(define-public crate-serde_eth-0.1.0 (c (n "serde_eth") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1irz49kr9rcn5p7imqp7grcz16x152j6asg6nyqbl5fshln5dvpw")))

(define-public crate-serde_eth-0.1.1 (c (n "serde_eth") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "10r6f8jr75chdzzyb1344y4cx8p5giah77m1rc1c35lgf64qqzch")))

