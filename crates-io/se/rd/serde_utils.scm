(define-module (crates-io se rd serde_utils) #:use-module (crates-io))

(define-public crate-serde_utils-0.1.0 (c (n "serde_utils") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "04x9ydi9vyr3y424jjq1a6ijq5j7hb2nxvj9qf39w4w77vza2qnw")))

(define-public crate-serde_utils-0.2.0 (c (n "serde_utils") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)))) (h "09mkrb6dybp79acsm607fvvvqd4ghkywyq197mzqq167p6523qwh")))

(define-public crate-serde_utils-0.2.1 (c (n "serde_utils") (v "0.2.1") (d (list (d (n "rmp-serde") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)))) (h "0yby45bp84r9392dscbq98861k3p9qvgsfb95bdrpg4ad9b58fwn")))

(define-public crate-serde_utils-0.3.0 (c (n "serde_utils") (v "0.3.0") (d (list (d (n "rmp-serde") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)))) (h "1srrfz1359f9glmyyalang7i25dkpb1s7rmi4nargn18bnnz62pp")))

(define-public crate-serde_utils-0.4.0 (c (n "serde_utils") (v "0.4.0") (d (list (d (n "rmp-serde") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "0aab3k1lwnhn9cf3li9kh7i9d2as4hw28p9nknmy4jdk1wm862wf")))

(define-public crate-serde_utils-0.4.1 (c (n "serde_utils") (v "0.4.1") (d (list (d (n "rmp-serde") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "06fl403drmlpy529aw6h6n0620nv6rnfp8p9ghyzas2qk7iydzc7")))

(define-public crate-serde_utils-0.5.0 (c (n "serde_utils") (v "0.5.0") (d (list (d (n "rmp-serde") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0j0ljk3h3455zkfw84rn6i12ar5zkizyn1fdf3g7c5h43qzhqqmf")))

(define-public crate-serde_utils-0.5.1 (c (n "serde_utils") (v "0.5.1") (d (list (d (n "rmp-serde") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1rhv0dm9j56kmm3xhk689z48ip4mka6530jb4572bh3zkjb54jmk")))

(define-public crate-serde_utils-0.5.2 (c (n "serde_utils") (v "0.5.2") (d (list (d (n "rmp-serde") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1nzh394rv4fcm8czfawp0f9qx46nbqqnginzh57nadq3p1qzxyy6")))

(define-public crate-serde_utils-0.6.0 (c (n "serde_utils") (v "0.6.0") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)))) (h "1g74ras1q3cr95rva7izzcjmqz5whs89dpq071ilcdn9cjryvq7n")))

(define-public crate-serde_utils-0.6.1 (c (n "serde_utils") (v "0.6.1") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)))) (h "18ya9j34jpwbxxy6zkf5r6r764nhdvnn7sq21i31fckbfjyv860z")))

(define-public crate-serde_utils-0.6.2 (c (n "serde_utils") (v "0.6.2") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)))) (h "0sgazslywccpfbqmb3yxkr6vh21c1735q07kpinm1sxyb2a1lqlf")))

