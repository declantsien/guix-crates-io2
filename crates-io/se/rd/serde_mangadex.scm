(define-module (crates-io se rd serde_mangadex) #:use-module (crates-io))

(define-public crate-serde_mangadex-1.0.0 (c (n "serde_mangadex") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "06wbl07b1w94ahkraajfbqj1ns1ia0rw7lakbcxlxy71xjrmvghl") (y #t)))

(define-public crate-serde_mangadex-1.0.1 (c (n "serde_mangadex") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1pccyhhf6m0pixil9891a7a5xxss803acms9ix8wx7h9sliq94yn") (y #t)))

(define-public crate-serde_mangadex-1.1.0 (c (n "serde_mangadex") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0yl4086fl3mbcaz130d2clncgi6vrzxg20b1bjh9qlv0c5jrj9s1") (f (quote (("default")))) (y #t)))

(define-public crate-serde_mangadex-1.2.0 (c (n "serde_mangadex") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1z8yy8989fbyc0kgv2q7qkrayyh7wy28jzhpaf3pnzjslj4hhmsb") (f (quote (("time" "chrono") ("display" "derive_more") ("default")))) (y #t)))

