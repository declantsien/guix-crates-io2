(define-module (crates-io se rd serde-hashkey) #:use-module (crates-io))

(define-public crate-serde-hashkey-0.1.0 (c (n "serde-hashkey") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.14") (k 0)) (d (n "serde_derive") (r "^1.0.98") (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0kmka41cxm9i2zmlv1xwrwgwdrz52vl77wx5g8qn4r7yp1jlcvjg") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-serde-hashkey-0.2.0 (c (n "serde-hashkey") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1ig75sm814gsrg26wpaxhs9wgahhxhd4i9glphypy0wsw6si2yfg")))

(define-public crate-serde-hashkey-0.2.1 (c (n "serde-hashkey") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "12fcmq87v5djqcn0rk193n3hb0zk1mir8hdn19vxy8cq5waz013x")))

(define-public crate-serde-hashkey-0.2.2 (c (n "serde-hashkey") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "02sklhgq6cp0djz171vdi62qvxi97jn8gyx0rfavrdfcyn71wkkr")))

(define-public crate-serde-hashkey-0.2.3 (c (n "serde-hashkey") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "091ka27xxzjnix6rj1b6qmhcqkl9q5wx92fbrvbaz2i125rd6hbr")))

(define-public crate-serde-hashkey-0.2.4 (c (n "serde-hashkey") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "14klmj0vw1ajsm2fgdyi8xzizr6r2fkibvx3bfcnvzga0as7dlsb")))

(define-public crate-serde-hashkey-0.3.0 (c (n "serde-hashkey") (v "0.3.0") (d (list (d (n "num-traits02") (r ">=0.2.1, <0.3.0") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float2") (r ">=2.0.0, <3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r ">=1.0.117, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.117, <2.0.0") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 2)))) (h "1ic5arr43pkhlsx81b46ympm2lq6sk68kfwmc4wng3bjay24nf6j") (f (quote (("ordered-float" "ordered-float2" "num-traits02") ("default"))))))

(define-public crate-serde-hashkey-0.4.0 (c (n "serde-hashkey") (v "0.4.0") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float2") (r "^2.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "09vsqfr0i85ljxri7s2461p1rwxzkn7c1v5av61sxggzshbj0lmq") (f (quote (("ordered-float" "ordered-float2" "num-traits02") ("default"))))))

(define-public crate-serde-hashkey-0.4.1 (c (n "serde-hashkey") (v "0.4.1") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float3") (r "^3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0gsicsnh8l97c6vvgsy11s4shyid40dvwsz61knghn0izm516x97") (f (quote (("ordered-float" "ordered-float3" "num-traits02") ("default")))) (y #t)))

(define-public crate-serde-hashkey-0.4.2 (c (n "serde-hashkey") (v "0.4.2") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float3") (r "^3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "150542948f7krmv049fjnwqh353jf8391aaga5z6pj5fa8l801c9") (f (quote (("ordered-float" "ordered-float3" "num-traits02") ("default")))) (y #t)))

(define-public crate-serde-hashkey-0.4.3 (c (n "serde-hashkey") (v "0.4.3") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float3") (r "^3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0vcywdkj3c1p492g2hsdbf121xylpf62vjgwid2jj5rw8ps9rh75") (f (quote (("ordered-float" "ordered-float3" "num-traits02") ("default"))))))

(define-public crate-serde-hashkey-0.4.4 (c (n "serde-hashkey") (v "0.4.4") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float3") (r "^3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1zalm1z4hpbilfpjaiwvx1bx9rkdmsg93rdmfbhfzsy1pjpclssa") (f (quote (("ordered-float" "ordered-float3" "num-traits02") ("default"))))))

(define-public crate-serde-hashkey-0.4.5 (c (n "serde-hashkey") (v "0.4.5") (d (list (d (n "num-traits02") (r "^0.2.1") (o #t) (d #t) (k 0) (p "num-traits")) (d (n "ordered-float3") (r "^3.0.0") (o #t) (d #t) (k 0) (p "ordered-float")) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0kp0ml39gqbr9qvan33s3974rv43f08wh046wf1hpg8yqg9r0fn1") (f (quote (("ordered-float" "ordered-float3" "num-traits02") ("default"))))))

