(define-module (crates-io se rd serde_with_object_id) #:use-module (crates-io))

(define-public crate-serde_with_object_id-0.1.0 (c (n "serde_with_object_id") (v "0.1.0") (d (list (d (n "mongodb") (r "^2.4") (f (quote ("bson-serde_with"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2.3") (d #t) (k 0)))) (h "11j3yrsbqw528pdskf7yaigmxbcc11fxczm24v7yyyyv1d7r3xwc")))

