(define-module (crates-io se rd serde_dbgfmt) #:use-module (crates-io))

(define-public crate-serde_dbgfmt-0.1.0 (c (n "serde_dbgfmt") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_path_to_error") (r "^0.1.15") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0lw8865dw6lshbx3jrq0g91nmc6ld5zqvjyyajqdf2xa7rya1qq5") (r "1.67")))

