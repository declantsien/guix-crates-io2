(define-module (crates-io se rd serde_dyn) #:use-module (crates-io))

(define-public crate-serde_dyn-0.1.0 (c (n "serde_dyn") (v "0.1.0") (d (list (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1s4cm6hqkzqsbgzw0l305xi79j8cwrmmvsqpwh8w3pwhp2xlx9j9")))

(define-public crate-serde_dyn-0.2.0 (c (n "serde_dyn") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "1085k24nigid8i98pf82ggs1chnrbsgrbs1jgsizr60yymjh9czy")))

(define-public crate-serde_dyn-0.2.1 (c (n "serde_dyn") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jzjgg67b0m3f2l40xk26l6g3r30fxj1mlvnx5yf8yal3d0y2dg1")))

(define-public crate-serde_dyn-0.2.2 (c (n "serde_dyn") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "16vhxwfn9cvjfyzk62xxhjpkqmr7z1fj0s8rldd5qq3cbra7djpb")))

(define-public crate-serde_dyn-0.3.0 (c (n "serde_dyn") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "09hm9gd0bwlxq8ippzka7pcg5s1w00p7xfydak3lnk3l4p98big4")))

(define-public crate-serde_dyn-0.3.1 (c (n "serde_dyn") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "177hn50fyk74gmqgawbrwmkb82l4y4blwldnbag6y79ri13mvy3y")))

(define-public crate-serde_dyn-0.4.0 (c (n "serde_dyn") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "type-uuid") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "03wgv53f7208ppmv6z8d6873wc4542nalbj2knj68fi9266k94wg")))

