(define-module (crates-io se rd serde_view) #:use-module (crates-io))

(define-public crate-serde_view-0.1.0 (c (n "serde_view") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1") (d #t) (k 0)))) (h "0mydkmdlkrs9w9q9khxr0prj4f2pvwlpwibz224l568g7hmi7xaa")))

(define-public crate-serde_view-0.1.1 (c (n "serde_view") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1") (d #t) (k 0)))) (h "0nv0m8g3wvbmvp7drs7jw4h6px3laxwvvrpb9y98x0cwrpr97y9b")))

(define-public crate-serde_view-0.1.2 (c (n "serde_view") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1rvlk0xcicqi3cm37d6ncv8bh3mn0m8qm4y75pvw5pazn8dq6wly")))

(define-public crate-serde_view-0.1.3 (c (n "serde_view") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1.3") (d #t) (k 0)))) (h "1dplc7fpkghn5vjf1swjbjsrnh4km2ypqs4r71qcgc2ibzwsa1vv")))

(define-public crate-serde_view-0.1.4 (c (n "serde_view") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1.3") (d #t) (k 0)))) (h "1dii97svq6hbkkq63yznfwldsn05ijj4xfxrjaj5h04bk85z9l3r") (y #t)))

(define-public crate-serde_view-0.1.5 (c (n "serde_view") (v "0.1.5") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0ljkkpk5wn8c1hxzaw3arrq621cw5i62fi06cdnb3f15az7hf6j8")))

(define-public crate-serde_view-0.1.6 (c (n "serde_view") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_view_macros") (r "^0.1.6") (d #t) (k 0)))) (h "1g2hcwkfhnbcmyrr86zvhw2cg48b3pkirwxnkyw00n4is81wyc26")))

