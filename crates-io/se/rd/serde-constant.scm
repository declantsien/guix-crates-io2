(define-module (crates-io se rd serde-constant) #:use-module (crates-io))

(define-public crate-serde-constant-0.1.0 (c (n "serde-constant") (v "0.1.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0zmlk5jr0sz1bd8ankpv2pknyx9b8mij7rhyxaql8r75bzlhs79h")))

