(define-module (crates-io se rd serde_watson) #:use-module (crates-io))

(define-public crate-serde_watson-0.1.0 (c (n "serde_watson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)) (d (n "watson_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1lk1sp0hvwx1jnca3gsmarvc6zsz64hphyjhrgkd7hfb49s643md")))

