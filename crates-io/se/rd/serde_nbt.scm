(define-module (crates-io se rd serde_nbt) #:use-module (crates-io))

(define-public crate-serde_nbt-0.1.0 (c (n "serde_nbt") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (f (quote ("union"))) (d #t) (k 0)))) (h "0sg08637j52pfc8v7ld45ql89wmzqs24fvzvc3s9wxf5vhsf8fj6")))

(define-public crate-serde_nbt-0.1.1 (c (n "serde_nbt") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (f (quote ("union"))) (d #t) (k 0)))) (h "0a1ja488m7027d5dbr1nya550pwlgmfkkvmi4cd952cxfrnc5q5l")))

