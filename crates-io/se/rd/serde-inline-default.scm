(define-module (crates-io se rd serde-inline-default) #:use-module (crates-io))

(define-public crate-serde-inline-default-0.1.0 (c (n "serde-inline-default") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y6nm9gxrxh039pcxjk0bjh0y1mv1bswz31y1i04qyj89s8wlxll")))

(define-public crate-serde-inline-default-0.1.1 (c (n "serde-inline-default") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i78dc9i09hgmnflalqzv1ics1ra2pc18lcmibr1zw5ma3g4r0ps")))

(define-public crate-serde-inline-default-0.2.0 (c (n "serde-inline-default") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "042r94mriaiy1qj4ck0aqj851913a8lq9czkinq2ml1lqlyi704r")))

