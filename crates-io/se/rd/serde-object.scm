(define-module (crates-io se rd serde-object) #:use-module (crates-io))

(define-public crate-serde-object-0.0.0-alpha.0 (c (n "serde-object") (v "0.0.0-alpha.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linkme") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "try_match") (r "^0.2.2") (d #t) (k 0)) (d (n "wyz") (r "^0.2.0") (d #t) (k 0)))) (h "1m0v1l0xqpx66r2kvinvcn5p4l1glcgp5vwqbivsn9d0vr9cjrbs") (f (quote (("assistant-extra" "linkme"))))))

