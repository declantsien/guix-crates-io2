(define-module (crates-io se rd serde-clap-deserialize) #:use-module (crates-io))

(define-public crate-serde-clap-deserialize-0.1.0 (c (n "serde-clap-deserialize") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "04xy0dcfnp6bli44s1pppgw2zknvvlk3lx6glnjbswkc1mbngz23")))

