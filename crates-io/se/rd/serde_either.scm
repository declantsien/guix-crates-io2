(define-module (crates-io se rd serde_either) #:use-module (crates-io))

(define-public crate-serde_either-0.1.0 (c (n "serde_either") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1zc1ynbmbfsrr14w1xghydwz1lagmm4qpkmh09l6fc48hq58rr1d")))

(define-public crate-serde_either-0.1.1 (c (n "serde_either") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "03k2fqrw4cngvp6s3fabs1252dv18y6zjg5dhz4zwciv3xig118n")))

(define-public crate-serde_either-0.2.0 (c (n "serde_either") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0kwakhbzpb0220231ps5id0ij00wp2cgpnqg1hbasvh8gnxl5fhh")))

(define-public crate-serde_either-0.2.1 (c (n "serde_either") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1yzja6inn7jkjgarzzhz3589g1pmzmmidk6j4z9gqvw2wzs475k8")))

