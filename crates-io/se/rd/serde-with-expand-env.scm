(define-module (crates-io se rd serde-with-expand-env) #:use-module (crates-io))

(define-public crate-serde-with-expand-env-0.1.0 (c (n "serde-with-expand-env") (v "0.1.0") (d (list (d (n "envmnt") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)))) (h "0zrwzwx7qvxsah9xls94dzzmk9cgwnz3piasr9fa7ls8c1xaf5ck")))

(define-public crate-serde-with-expand-env-1.0.0 (c (n "serde-with-expand-env") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)) (d (n "shellexpand") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "0jdc41kwaahixpjrppih69n9m356lh0vk4ys3majz50h5r4n9vc9")))

(define-public crate-serde-with-expand-env-1.1.0 (c (n "serde-with-expand-env") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 2)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 2)))) (h "163y9v8qszgaafg2adxq7pln13ziiy8z2rhbilq0k8p37d58i3c8")))

