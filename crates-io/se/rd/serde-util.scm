(define-module (crates-io se rd serde-util) #:use-module (crates-io))

(define-public crate-serde-util-0.1.0 (c (n "serde-util") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "199wf3n0skcyj5z53i4d5dfh0w2wm1b019h3hr9adyzi00jn9rf6")))

(define-public crate-serde-util-0.2.0 (c (n "serde-util") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wkkh0n2mda3wxbcw0zygj5gw0zglf1dfxc1pazdjcaajpxlkk3y")))

(define-public crate-serde-util-0.3.0 (c (n "serde-util") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sraxqd3rrsd4300f7flsp7imza660rdyr9dk47cg9rdjyajs1j3")))

(define-public crate-serde-util-0.3.1 (c (n "serde-util") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lrl357wqan8sw43xa8in194703mpbiy9d316jiyd9pxc8qflikq")))

