(define-module (crates-io se rd serde_unit_struct) #:use-module (crates-io))

(define-public crate-serde_unit_struct-0.1.0 (c (n "serde_unit_struct") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_unit_struct_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0wyvkyl2gs97y4nmxwgycr39pv5ycp8bsbcp4l7d14sdsddrmzgj")))

(define-public crate-serde_unit_struct-0.1.1 (c (n "serde_unit_struct") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_unit_struct_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0njakgkrjmjznzf3mh5z985krbdmz8qm449fq3b5mni995yfzy5w")))

(define-public crate-serde_unit_struct-0.1.2 (c (n "serde_unit_struct") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_unit_struct_derive") (r "^0.1.2") (d #t) (k 0)))) (h "181dc7dd89ayvnly0nrfv9s4y2k6ylfsp17c3kvxkw4cfbm6bszn") (y #t)))

(define-public crate-serde_unit_struct-0.1.3 (c (n "serde_unit_struct") (v "0.1.3") (d (list (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "postcard") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-pickle") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_unit_struct_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 2)))) (h "11wr4pdgiksfa9da87n5zcbdz32v21d7b788srgaal4r0mwp9s6f")))

