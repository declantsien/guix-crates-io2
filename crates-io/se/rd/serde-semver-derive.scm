(define-module (crates-io se rd serde-semver-derive) #:use-module (crates-io))

(define-public crate-serde-semver-derive-0.1.0 (c (n "serde-semver-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0cqfn7a3dj94vscjp1cmpnid6rmz31c0l6hwvckra2fgsp5zcd89")))

(define-public crate-serde-semver-derive-0.1.1 (c (n "serde-semver-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "06fvq0lrdpsyfdps83cxk2fg9740myww7nyhv9yq27nc0m09rddv")))

