(define-module (crates-io se rd serde-key-value-vec-map) #:use-module (crates-io))

(define-public crate-serde-key-value-vec-map-0.0.1 (c (n "serde-key-value-vec-map") (v "0.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "16ciqia2a51dhkwn53629dvalgdzlx727ignd1yfjzmvqs34f17y")))

(define-public crate-serde-key-value-vec-map-0.1.0 (c (n "serde-key-value-vec-map") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1j33zpr56gfnlm9xvr6wnai2fdyfi99f5504mgij3slcnzqhqwfa")))

