(define-module (crates-io se rd serde-cmd) #:use-module (crates-io))

(define-public crate-serde-cmd-0.1.0 (c (n "serde-cmd") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "18kzchk8i39q2mnk5gfjf99dk5x5jyyb4ngscpwy2y4hvg6wmzk8") (f (quote (("default" "serde"))))))

(define-public crate-serde-cmd-0.1.1 (c (n "serde-cmd") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1xkjysc8wj9wsx0dc9n0x3nc9b0mz22lmsswjrph0kcvmadvds8m") (f (quote (("default" "serde"))))))

(define-public crate-serde-cmd-0.1.2 (c (n "serde-cmd") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1xzf2y8pcwd9r4pxvhmh4qkigy8k3y7hg30vvr4scvkbxqd1a7ks") (f (quote (("default" "serde")))) (y #t)))

(define-public crate-serde-cmd-0.1.3 (c (n "serde-cmd") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0rf83inn5qbrgd9csch35h6dybf6n54dy6l24vkzai7gv3hd2a50") (f (quote (("default" "serde"))))))

