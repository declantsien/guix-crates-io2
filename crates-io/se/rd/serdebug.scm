(define-module (crates-io se rd serdebug) #:use-module (crates-io))

(define-public crate-serdebug-1.0.0 (c (n "serdebug") (v "1.0.0") (d (list (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serdebug_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0hjyw12qd2i9pjks3snd74y845xpq9rmqya5f7aa6kshvp6zsnwj") (f (quote (("nightly"))))))

(define-public crate-serdebug-1.0.1 (c (n "serdebug") (v "1.0.1") (d (list (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serdebug_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0gckksl5k00gx2vcc9636j5a0458dysivq5id0lcdxiy2r5ycj89")))

(define-public crate-serdebug-1.0.2 (c (n "serdebug") (v "1.0.2") (d (list (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serdebug_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1f0v39bcinzy9nm6gl8i8s555djkbhwgp9z9h3lvg01isy911r7i")))

(define-public crate-serdebug-1.0.4 (c (n "serdebug") (v "1.0.4") (d (list (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serdebug_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0861fdph5k9i1ix5fz9lrbpnyff5ndrzcb1s2aw5lm37shllxzj8")))

(define-public crate-serdebug-1.0.5 (c (n "serdebug") (v "1.0.5") (d (list (d (n "rustc_version") (r "^0.2.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 2)) (d (n "serdebug_derive") (r "^1.0.0") (d #t) (k 0)))) (h "05hav0ynhf3nfgdvvq6f3h9yi795zrxqlcgl1gv4y2ld7p440m8r")))

