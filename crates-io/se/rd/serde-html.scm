(define-module (crates-io se rd serde-html) #:use-module (crates-io))

(define-public crate-serde-html-0.0.0 (c (n "serde-html") (v "0.0.0") (d (list (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ksal2hwxi7mg9fl7bkh5w3djmzfwkjgasdbklwprsc5rj92j3cv") (f (quote (("default"))))))

(define-public crate-serde-html-0.0.1 (c (n "serde-html") (v "0.0.1") (d (list (d (n "indexmap") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "012gxyd5rf2ljjhxkflyizzdsd1jhvf5fq0i6v5y9cjqk71zf0r0") (f (quote (("default"))))))

