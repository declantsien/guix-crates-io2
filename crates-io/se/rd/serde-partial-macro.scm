(define-module (crates-io se rd serde-partial-macro) #:use-module (crates-io))

(define-public crate-serde-partial-macro-0.1.0 (c (n "serde-partial-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde_derive_internals") (r "~0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro" "derive"))) (k 0)))) (h "1dyxk0yqlg1l2blaaywn9x1kgi9dnfk8jm43qigmzbdlasgl4ab0") (r "1.56.0")))

(define-public crate-serde-partial-macro-0.2.0 (c (n "serde-partial-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde_derive_internals") (r "~0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro" "derive"))) (k 0)))) (h "0apg8ynrfmcph30plslalpyhwzc5br5m9s5avscwwdn1sb9drr8x") (r "1.56.0")))

(define-public crate-serde-partial-macro-0.3.0 (c (n "serde-partial-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde_derive_internals") (r "~0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro" "derive"))) (k 0)))) (h "02w1psa94qlz5gwhvixnxnrns21kfzy7b6ihfg5h32s12wwzdz88") (r "1.56.0")))

(define-public crate-serde-partial-macro-0.3.1 (c (n "serde-partial-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde_derive_internals") (r "~0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro" "derive"))) (k 0)))) (h "15y7cq32jschnq4wlnqa5ym4xfdyll77znki8vdhsm8mj09cy8ys") (r "1.56.0")))

