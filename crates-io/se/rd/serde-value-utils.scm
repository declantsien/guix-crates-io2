(define-module (crates-io se rd serde-value-utils) #:use-module (crates-io))

(define-public crate-serde-value-utils-0.1.0 (c (n "serde-value-utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)))) (h "1jgbfd81gns2srdi3anlq4v6cnis89qwp4fj3bz4yilks8237rzm") (f (quote (("with-schema"))))))

