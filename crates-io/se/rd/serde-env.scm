(define-module (crates-io se rd serde-env) #:use-module (crates-io))

(define-public crate-serde-env-0.0.0 (c (n "serde-env") (v "0.0.0") (h "0lypjwr13ykdiqdla97fh5gin3c546914iylz4398b47irm0sapa")))

(define-public crate-serde-env-0.0.1 (c (n "serde-env") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.2.0") (d #t) (k 2)))) (h "1gkqr9ylsw1xvj8p3i72hwphj53f22a9ifz2bawg5gbqvg1cn9cy")))

(define-public crate-serde-env-0.0.2 (c (n "serde-env") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.2.0") (d #t) (k 2)))) (h "1h56xjmnwzkzbr9imkksx498f704a8sk8al946y4dpvimarcy0sf")))

(define-public crate-serde-env-0.0.3 (c (n "serde-env") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.2") (d #t) (k 2)))) (h "080alc70dn212l2fhg8klmlzc887i2d9a6n34rmi50pj2hib2gaf")))

(define-public crate-serde-env-0.1.0 (c (n "serde-env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "1nrmmh31d8vk2qy34a4q4s4vphlf9qgnbfpfzkig4m208dhfsccz")))

(define-public crate-serde-env-0.1.1 (c (n "serde-env") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "0n6f802db9xwac94y1zakzyw9nr78x5inmiq9dpzsjb2hjh1k0f6")))

