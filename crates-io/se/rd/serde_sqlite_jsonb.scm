(define-module (crates-io se rd serde_sqlite_jsonb) #:use-module (crates-io))

(define-public crate-serde_sqlite_jsonb-0.0.1-alpha (c (n "serde_sqlite_jsonb") (v "0.0.1-alpha") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json5") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1pgm7mvss5kvj4mx5zpn1msrmljl6kza3jsdsbdkmm2xh4mhlb9f") (f (quote (("default" "serde_json")))) (r "1.63")))

(define-public crate-serde_sqlite_jsonb-0.1.0 (c (n "serde_sqlite_jsonb") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled" "blob"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json5") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0z4ywrhxrq0xr439nkc5gjcc5fij29wmm84yla3s12hk60nrdsr8") (f (quote (("default" "serde_json")))) (r "1.63")))

