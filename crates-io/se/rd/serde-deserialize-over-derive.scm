(define-module (crates-io se rd serde-deserialize-over-derive) #:use-module (crates-io))

(define-public crate-serde-deserialize-over-derive-0.1.0 (c (n "serde-deserialize-over-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1psx4l7knq7dvsw4v917i715izdwvyj3n6afwzqcg6272xj2axj1")))

(define-public crate-serde-deserialize-over-derive-0.1.1 (c (n "serde-deserialize-over-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cf74l0mcm0a9djri1h53626rglcawwa9nzjfj7f58r068aaw5ma")))

