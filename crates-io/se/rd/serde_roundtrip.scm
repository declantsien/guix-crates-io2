(define-module (crates-io se rd serde_roundtrip) #:use-module (crates-io))

(define-public crate-serde_roundtrip-0.1.0 (c (n "serde_roundtrip") (v "0.1.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)) (d (n "serde_roundtrip_derive") (r "^0.1") (d #t) (k 2)))) (h "0p0rqsi2xkyqf0v2zidr3balx99011y6ln058xag82n069rc0h6v")))

