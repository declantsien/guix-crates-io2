(define-module (crates-io se rd serde_merge) #:use-module (crates-io))

(define-public crate-serde_merge-0.1.0 (c (n "serde_merge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rk7fqaknmy2drky99ww4zhhxn5cmcyv4mdrrg62xz5kn21q6x6k")))

(define-public crate-serde_merge-0.1.1 (c (n "serde_merge") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12cqzjf6b8bl0gkd2nv6zfcxmm8pa82ahxxxhxfkavd730dcsszv")))

(define-public crate-serde_merge-0.1.2 (c (n "serde_merge") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10j8m0bcklcx1x244181vjx6bhimakx7ja6ib5q4xy1x9inbi9rs")))

(define-public crate-serde_merge-0.1.3 (c (n "serde_merge") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16s0bxn35wm12kpplznp2qppd3a6siiy04kcn71jl8qnhn3r2vk0")))

