(define-module (crates-io se rd serde-dson) #:use-module (crates-io))

(define-public crate-serde-dson-0.0.0 (c (n "serde-dson") (v "0.0.0") (d (list (d (n "dson") (r "^0.0.0") (d #t) (k 0) (p "dson")) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sghlw2qwh8ryb4d3xh4vdp95pfjq8pskgacdn1dd4fzwcv6xa14")))

