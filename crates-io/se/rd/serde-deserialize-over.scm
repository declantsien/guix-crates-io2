(define-module (crates-io se rd serde-deserialize-over) #:use-module (crates-io))

(define-public crate-serde-deserialize-over-0.1.0 (c (n "serde-deserialize-over") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-deserialize-over-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1parrhlhm964qwxvvgqi2i3lgbgvirkryvmgwwb70n3bhg5wzblj")))

(define-public crate-serde-deserialize-over-0.1.1 (c (n "serde-deserialize-over") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-deserialize-over-derive") (r "=0.1.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jq6vwa14l8s6z1lmca60slnsd9pspys3j289d210k6r9vfhq4l1")))

