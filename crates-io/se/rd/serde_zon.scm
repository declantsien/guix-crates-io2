(define-module (crates-io se rd serde_zon) #:use-module (crates-io))

(define-public crate-serde_zon-0.0.1 (c (n "serde_zon") (v "0.0.1") (h "0w26jlgh6di2qffckf6pnvss1pw85h27xz15bhwbxfs5zhxi4ldc")))

(define-public crate-serde_zon-0.0.2 (c (n "serde_zon") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hlk15iwa9j62jammfbvp9xnk4ahdr16xhqmndiwzd77jxjwlr6n")))

