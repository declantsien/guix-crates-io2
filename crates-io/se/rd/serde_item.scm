(define-module (crates-io se rd serde_item) #:use-module (crates-io))

(define-public crate-serde_item-0.1.0 (c (n "serde_item") (v "0.1.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.35.0") (o #t) (d #t) (k 0)))) (h "1bg7r00p7lf7llfqkpjgznx3ff307m72f8ar15bkl48qwi7yjnkv") (f (quote (("with-syntex" "syntex_syntax") ("nightly-testing" "clippy") ("default" "with-syntex")))) (y #t)))

(define-public crate-serde_item-0.2.0 (c (n "serde_item") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.36.0") (o #t) (d #t) (k 0)))) (h "05kp0lbybal2idkw93rb67yv8xj3vwsmbaiy1iqigqjfg19s3k0f") (f (quote (("with-syntex" "syntex_syntax") ("nightly-testing" "clippy") ("default" "with-syntex")))) (y #t)))

