(define-module (crates-io se rd serde_keyed_derive) #:use-module (crates-io))

(define-public crate-serde_keyed_derive-1.0.196 (c (n "serde_keyed_derive") (v "1.0.196") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (d #t) (k 0)))) (h "1mhmvqi7aq22hnz5x7835z85hvk40pg5bbkl51mmlrjbp1xjak0p") (f (quote (("deserialize_in_place") ("default")))) (r "1.56")))

