(define-module (crates-io se rd serde_prometheus_labels) #:use-module (crates-io))

(define-public crate-serde_prometheus_labels-0.2.0 (c (n "serde_prometheus_labels") (v "0.2.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "prometheus-client") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (k 2)))) (h "0apciivkwyipcca2jhblldvp4qp12iaqbrhngqk0hflzqac9av4l") (s 2) (e (quote (("bridge" "dep:parking_lot" "dep:prometheus-client"))))))

