(define-module (crates-io se rd serde_shon) #:use-module (crates-io))

(define-public crate-serde_shon-0.1.0 (c (n "serde_shon") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)))) (h "11ny62dj8yb3kdik2qsda78df8smgr5wxsh39xbkxyx1q5mylzb0")))

