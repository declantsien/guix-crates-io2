(define-module (crates-io se rd serde_array_query) #:use-module (crates-io))

(define-public crate-serde_array_query-0.1.0 (c (n "serde_array_query") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "19rlay9scadd2ff6yqzmgsvdcnhfh2b71i5vnbak6nq0n616x76q") (f (quote (("from_str" "serde_urlencoded") ("default" "serde/derive" "from_str"))))))

(define-public crate-serde_array_query-0.2.0 (c (n "serde_array_query") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "1b9af8p1wdfn81k9ipdxxc2vzj6r29y81agsyprnvwld9a5amwqi") (f (quote (("from_str" "serde_urlencoded") ("default" "serde/derive" "from_str"))))))

(define-public crate-serde_array_query-0.3.0 (c (n "serde_array_query") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "0grz0pdlbnr2abhip32z89y9psghyb6rg5ip0qhi1cnx13amg1qx") (f (quote (("from_str" "serde_urlencoded") ("default" "serde/derive" "from_str"))))))

