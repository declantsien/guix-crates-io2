(define-module (crates-io se rd serde-yaml-olidacombe) #:use-module (crates-io))

(define-public crate-serde-yaml-olidacombe-0.8.26 (c (n "serde-yaml-olidacombe") (v "0.8.26") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.5.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "yaml_rust") (r "^0.6.0") (d #t) (k 0) (p "yaml-rust-davvid")))) (h "1adhnr0pj55n52h1aljzxr0f7l977qjhyhljnahajvp49g8cpcq7") (r "1.56")))

(define-public crate-serde-yaml-olidacombe-0.8.27 (c (n "serde-yaml-olidacombe") (v "0.8.27") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.5.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "yaml_rust") (r "^0.6.0") (d #t) (k 0) (p "yaml-rust-olidacombe")))) (h "161dg5sm37407fnf3vpdmwj7h4sgbaw1nyag1xb6k7jxfjdp78lk") (r "1.56")))

