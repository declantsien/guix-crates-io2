(define-module (crates-io se rd serde_builder) #:use-module (crates-io))

(define-public crate-serde_builder-0.1.0 (c (n "serde_builder") (v "0.1.0") (d (list (d (n "concat-arrays") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15sj5rlapad73b4yl84w9icr018x3kz1hn856bmgwm9q78b92bph") (f (quote (("leaking"))))))

(define-public crate-serde_builder-0.1.1 (c (n "serde_builder") (v "0.1.1") (d (list (d (n "concat-arrays") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mlmyns1lwbgvbl98jial44m38rdlf6bn3s8953sai2xda3vnk5g") (f (quote (("leaking"))))))

(define-public crate-serde_builder-0.2.0 (c (n "serde_builder") (v "0.2.0") (d (list (d (n "concat-arrays") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a52g0f5518lmsxwhs6xpw6xypk2si606f4p4rlc6rmafzc20pig") (f (quote (("leaking"))))))

