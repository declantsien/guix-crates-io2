(define-module (crates-io se rd serde-humantime) #:use-module (crates-io))

(define-public crate-serde-humantime-0.1.0 (c (n "serde-humantime") (v "0.1.0") (d (list (d (n "humantime") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0swnm0apgb6s2w2m9lld8vsdv34ix327awnlf8mi1fhy6scidihf")))

(define-public crate-serde-humantime-0.1.1 (c (n "serde-humantime") (v "0.1.1") (d (list (d (n "humantime") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zcg0s40zc4qmphq0azamklxb46ymh8dnf26anfg3khjzbdbary3")))

