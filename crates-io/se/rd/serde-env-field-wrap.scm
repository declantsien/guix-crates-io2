(define-module (crates-io se rd serde-env-field-wrap) #:use-module (crates-io))

(define-public crate-serde-env-field-wrap-0.1.0 (c (n "serde-env-field-wrap") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1l68cc2kg5f5whd0fv3wdl9jd7kcx33zrmv8c7ab7a9gj761jysm")))

(define-public crate-serde-env-field-wrap-0.1.1 (c (n "serde-env-field-wrap") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1niblfsgx4y0lb9pl3yrhpkz0cc8w12p086ijry7i4lls8dwkzgx") (y #t)))

(define-public crate-serde-env-field-wrap-0.3.0 (c (n "serde-env-field-wrap") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0bn5674s4anlfwjlcl8yb8vcnaqdax547xxikagxwpns6rp2n1r3")))

