(define-module (crates-io se rd serde_bytes_ng) #:use-module (crates-io))

(define-public crate-serde_bytes_ng-0.1.0 (c (n "serde_bytes_ng") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "0ppn5iqqkig6w8lc11ylganmjadww9h7hbfsw57iq2dxi1cl1ysb") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes_ng-0.1.1 (c (n "serde_bytes_ng") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "0d0mpn4nhbl79qbdbzlnh78g1j6acpmfnmcff3msr0z0jdqwimcr") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes_ng-0.1.2 (c (n "serde_bytes_ng") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "1dj4cpd8n248cx5gnj5lpdk6dk0c3zjnr2sfjqzjbql4hv7fpc5x") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

