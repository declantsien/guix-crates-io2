(define-module (crates-io se rd serdeconv) #:use-module (crates-io))

(define-public crate-serdeconv-0.2.0 (c (n "serdeconv") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "194nsr1wprimlayiq8spqn9hrq0ady71a72halwq5chdl3gcds8f")))

(define-public crate-serdeconv-0.2.1 (c (n "serdeconv") (v "0.2.1") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.1") (d #t) (k 0)))) (h "15y138gi56k6gb4dq1p1l71xp025b19m99is80i8ghjzxfw08kiw")))

(define-public crate-serdeconv-0.3.0 (c (n "serdeconv") (v "0.3.0") (d (list (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0qg7wrkm84kq3vmn84agbgd7pl7374lzqn2004zq09fijf5dp7p7")))

(define-public crate-serdeconv-0.4.0 (c (n "serdeconv") (v "0.4.0") (d (list (d (n "rmp-serde") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "0cal6qkzps92g7csycqij4ik1df3ccxn5sxnjvfvm473phnwbvbi")))

(define-public crate-serdeconv-0.4.1 (c (n "serdeconv") (v "0.4.1") (d (list (d (n "rmp-serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (f (quote ("parse"))) (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "08bjyh38pr9ssg9vivk66r461j3piq78w5jb55afa98xxxnnk5w8")))

