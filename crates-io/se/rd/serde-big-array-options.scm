(define-module (crates-io se rd serde-big-array-options) #:use-module (crates-io))

(define-public crate-serde-big-array-options-0.1.0 (c (n "serde-big-array-options") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.189") (d #t) (k 0)))) (h "01cq5wx588hrkipwxpvq3npy6njmbbxwazn2vqvzihjkgapjf9zw")))

(define-public crate-serde-big-array-options-0.1.1 (c (n "serde-big-array-options") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.189") (d #t) (k 0)))) (h "04r50xa18sywvqlny23dxdmvfs975s1x5qwqyzwx57k190d4ycwb")))

