(define-module (crates-io se rd serde-ext-macros) #:use-module (crates-io))

(define-public crate-serde-ext-macros-0.1.1 (c (n "serde-ext-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n3b5pxq7k9rdkrri63rhpzviakb9slfcakm39z710l86ja8slb0")))

