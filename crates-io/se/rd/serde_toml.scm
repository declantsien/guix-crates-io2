(define-module (crates-io se rd serde_toml) #:use-module (crates-io))

(define-public crate-serde_toml-0.0.0 (c (n "serde_toml") (v "0.0.0") (h "1bzrb7rmjq5y46lzwrwz2npdjarcvcx79jvkp9gnv8p2r01wgbx8") (y #t)))

(define-public crate-serde_toml-0.0.1 (c (n "serde_toml") (v "0.0.1") (h "0hb5nqg5jdhizzq3d0pj091vyvj52n7dxdfqnszn6a3rd50ihs0f") (r "1.60.0")))

