(define-module (crates-io se rd serde_kicad_sexpr) #:use-module (crates-io))

(define-public crate-serde_kicad_sexpr-0.1.0 (c (n "serde_kicad_sexpr") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.63") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16pqb7227v2m29amcw6734z1w0whmhapihpms1fmfjmlkrjppicg")))

