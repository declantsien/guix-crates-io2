(define-module (crates-io se rd serde-redis) #:use-module (crates-io))

(define-public crate-serde-redis-0.1.0 (c (n "serde-redis") (v "0.1.0") (d (list (d (n "redis") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1bram4vn7shlznc611vhaka5j0bpwfn7l3aw1g12frx0bxf2l618")))

(define-public crate-serde-redis-0.1.1 (c (n "serde-redis") (v "0.1.1") (d (list (d (n "redis") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1lhf3z3z5cfkvc7c4jp7bhfd99dzm19a1rkxg1jvh13s3ih5z3r6")))

(define-public crate-serde-redis-0.2.0 (c (n "serde-redis") (v "0.2.0") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1cljq096y05vrbmn8mnld7k1rlalanj1m57d9zaapcdjdzx7ly5q")))

(define-public crate-serde-redis-0.3.0 (c (n "serde-redis") (v "0.3.0") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "1rdqn6x3ppiqn3k2sibchqnmw7nhs1lsm7syj4x05mmhnh6syprj")))

(define-public crate-serde-redis-0.3.1 (c (n "serde-redis") (v "0.3.1") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "1iww30fnawwgsfm9dl5qz6w49fb2rxamb5zy776zah5kcasjswa9") (y #t)))

(define-public crate-serde-redis-0.4.0 (c (n "serde-redis") (v "0.4.0") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "0b1p4xnwcdc5fk83p99v5l2x5265imypa128zcqkkb61dd1agan9")))

(define-public crate-serde-redis-0.5.0 (c (n "serde-redis") (v "0.5.0") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "0rdwpqdg9b98i4a131bp1vwg0xnxf5mzfspjm0y092cf7aa4kwqz")))

(define-public crate-serde-redis-0.5.1 (c (n "serde-redis") (v "0.5.1") (d (list (d (n "redis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 2)))) (h "0wk6fn1iw5i7gppdbr139mzkmkcly9kv20b9ja3m59aaawl2pxzk")))

(define-public crate-serde-redis-0.6.0 (c (n "serde-redis") (v "0.6.0") (d (list (d (n "redis") (r "> 0.5, < 0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)))) (h "0186aqdr421mzkq7b8rliqqfj53kmqijclxy8g3pygq4sjwz493k")))

(define-public crate-serde-redis-0.7.0 (c (n "serde-redis") (v "0.7.0") (d (list (d (n "redis") (r "> 0.5, < 0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1syvi31b4qi3n78g2najhmnq8jc1m1pdk5j1shcmi60vpm30h6hn")))

(define-public crate-serde-redis-0.7.1 (c (n "serde-redis") (v "0.7.1") (d (list (d (n "redis") (r "> 0.5, < 0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0rdpgl98jjjqrzijklc76f8hamflgivwjd2fz989jkrh0qrsini0")))

(define-public crate-serde-redis-0.7.2 (c (n "serde-redis") (v "0.7.2") (d (list (d (n "redis") (r "> 0.5, < 0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06dicb7dxnq84ycsv19c2dq7mwg7q44j8f7kgxkkmgkhbj0ys4i6")))

(define-public crate-serde-redis-0.7.3 (c (n "serde-redis") (v "0.7.3") (d (list (d (n "redis") (r "> 0.5, <= 0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0g3p615mgcqx8s44jm1bj8xlcjm6305y51vkgg4rdsj6ynmk55i6")))

(define-public crate-serde-redis-0.8.0 (c (n "serde-redis") (v "0.8.0") (d (list (d (n "redis") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05pkc4p9a6wyl9zg98zs6dj0lsbcb0yysdl7slnb2bdwipmbryh7")))

(define-public crate-serde-redis-0.9.0 (c (n "serde-redis") (v "0.9.0") (d (list (d (n "redis") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1rikxn068bhsn1qck11vy1mxbc4xc7arnv7pj8s37ra17apqj5h8")))

(define-public crate-serde-redis-0.10.0 (c (n "serde-redis") (v "0.10.0") (d (list (d (n "redis") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01myb21vr5v7884jbxys9hywsy3nrj54f8zfac15yxq6fxg63cr3")))

(define-public crate-serde-redis-0.11.0 (c (n "serde-redis") (v "0.11.0") (d (list (d (n "redis") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15ggw2gzkfm2025a5dvl96na9ml3xar8rpymbg3qawm6jn3fi7sb")))

(define-public crate-serde-redis-0.12.0 (c (n "serde-redis") (v "0.12.0") (d (list (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "03awc091ij4zn9pfs4n924v8l632jqm8b5y50q3ajka8k11pg9kq")))

(define-public crate-serde-redis-0.13.0 (c (n "serde-redis") (v "0.13.0") (d (list (d (n "redis") (r "^0.22.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xlm15wmkkx0dbp83qq3s49vzrnvg1zj1xg0qpjry1wb55hllksm")))

