(define-module (crates-io se rd serde_flow_derive) #:use-module (crates-io))

(define-public crate-serde_flow_derive-1.0.0 (c (n "serde_flow_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0gs6kg75ijgfv91k7ypgr4v5hhwajj3skrcbw7x6c3sphyd7151p") (f (quote (("tokio") ("default") ("async-std"))))))

(define-public crate-serde_flow_derive-1.0.1 (c (n "serde_flow_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1davr39mjsmly5b50241l6h6hwacpfyhwr7x637m1mdx4rcf23qv") (f (quote (("tokio") ("default") ("async-std"))))))

(define-public crate-serde_flow_derive-1.1.0 (c (n "serde_flow_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1f8m4xx49gxhnsjigx1xic0zx50n6rq7168hdh22z101j5iv0nan") (f (quote (("tokio") ("default") ("async-std"))))))

(define-public crate-serde_flow_derive-1.1.1 (c (n "serde_flow_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1lqi62ikp099nwjz8hr6cykm147ik12yf7v9m8z7b8q7ka5qyk1d") (f (quote (("tokio") ("default") ("async-std"))))))

