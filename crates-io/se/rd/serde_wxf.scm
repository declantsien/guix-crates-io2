(define-module (crates-io se rd serde_wxf) #:use-module (crates-io))

(define-public crate-serde_wxf-0.0.1 (c (n "serde_wxf") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.5") (d #t) (k 0)))) (h "1alpb1y5jzy2z9h0kirnfpkn54ga8xznyx76w8ky79jsq2qpin2p")))

