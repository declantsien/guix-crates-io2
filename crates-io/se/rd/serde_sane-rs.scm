(define-module (crates-io se rd serde_sane-rs) #:use-module (crates-io))

(define-public crate-serde_sane-rs-0.1.0 (c (n "serde_sane-rs") (v "0.1.0") (h "0945kxlwmp82lpvmi4b1ja1pwv0d79giq53ycxs92nzssl3m22hw")))

(define-public crate-serde_sane-rs-0.1.1 (c (n "serde_sane-rs") (v "0.1.1") (h "0nay2r5jx3m2na0r4xnw9kb6fyhw5w8aajmh2irg5sh2y21q91f9")))

(define-public crate-serde_sane-rs-0.2.0 (c (n "serde_sane-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "14bzi7z7m6rqq9iw4bb2sdp58jf102hzvzjv5957iyg4bj2ri82l")))

