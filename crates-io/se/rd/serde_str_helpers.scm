(define-module (crates-io se rd serde_str_helpers) #:use-module (crates-io))

(define-public crate-serde_str_helpers-0.1.0 (c (n "serde_str_helpers") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "0jnc8k4rmji6g089sxc959j3mmqrvsm7p2kv55n858y43qjhjq0b")))

(define-public crate-serde_str_helpers-0.1.2 (c (n "serde_str_helpers") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "0hf815gv8nww7scqxhr53r90rz3xhn9hsfpkd94qadrg9z4sfi5p")))

