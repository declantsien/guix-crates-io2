(define-module (crates-io se rd serde_ipld_dagjson) #:use-module (crates-io))

(define-public crate-serde_ipld_dagjson-0.1.0 (c (n "serde_ipld_dagjson") (v "0.1.0") (d (list (d (n "cid") (r "^0.11.0") (f (quote ("serde-codec"))) (d #t) (k 0)) (d (n "ipld-core") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0i8n1148g4497v501klkqwi5bw6rlkaa774ydkn35hjpnn5z7d72")))

(define-public crate-serde_ipld_dagjson-0.1.1 (c (n "serde_ipld_dagjson") (v "0.1.1") (d (list (d (n "cid") (r "^0.11.0") (f (quote ("serde-codec"))) (d #t) (k 0)) (d (n "ipld-core") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "0q6404wmv3jff0mhlmnf2iggq5ps86ad44bzznfn8ikmdi5c2x8h")))

(define-public crate-serde_ipld_dagjson-0.1.2 (c (n "serde_ipld_dagjson") (v "0.1.2") (d (list (d (n "ipld-core") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "19aivbhv8vd7wxkbrvg7pkazzqkqg2yqsxz59x7750wvdqbz2h1g")))

(define-public crate-serde_ipld_dagjson-0.2.0 (c (n "serde_ipld_dagjson") (v "0.2.0") (d (list (d (n "ipld-core") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "0dnsxj720cym6ya4iyks46bjw8cyac85wrl4b7phd8zllxxv8n9k")))

