(define-module (crates-io se rd serde_token) #:use-module (crates-io))

(define-public crate-serde_token-0.0.1 (c (n "serde_token") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v9jvgm8ajp8pc0qihjy3vky3zdv79vrxaild8k50fqnfjxf9kfq")))

(define-public crate-serde_token-0.0.2 (c (n "serde_token") (v "0.0.2") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q5v6bgsi7jf2cgyml27w0v3ryzxidy6x0imvyh1f4k1086kjyh0")))

