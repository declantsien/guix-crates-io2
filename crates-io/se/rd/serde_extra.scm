(define-module (crates-io se rd serde_extra) #:use-module (crates-io))

(define-public crate-serde_extra-0.1.0 (c (n "serde_extra") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "1qwhdmwwbai2m87h2by83p87qf18cqf6hrr1rn7nrqqmac57s22y") (f (quote (("use_128") ("defaults" "use_128"))))))

(define-public crate-serde_extra-0.1.1 (c (n "serde_extra") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0aby0hl9b7p15zijw5rkgr5wsd2vr5256ns7rkw77b18v16105fg") (f (quote (("use_128") ("default" "use_128"))))))

(define-public crate-serde_extra-0.2.0 (c (n "serde_extra") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0yglgyhhyr51gpzf34yax7klxaa40n64kax4yqz2maa1kfanrg8x") (f (quote (("use_128") ("default" "use_128"))))))

(define-public crate-serde_extra-0.2.1 (c (n "serde_extra") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0mw6vvw28x88i7pmn7dzfv0wjpi8gb0zr5ahsmqh4619jn7yzh5h") (f (quote (("use_128") ("default" "use_128"))))))

(define-public crate-serde_extra-0.3.0 (c (n "serde_extra") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "1npdp6mh9d8ml0jfn2qbw8mdgvsqjpbndfa0wkcgbzdcaby791pa") (f (quote (("use_128") ("default" "use_128"))))))

(define-public crate-serde_extra-0.3.1 (c (n "serde_extra") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)))) (h "0n9w86dmlvj8qdxz93gj76inv2jgy9sbhrz0fdm0vdzz4008rgbp") (f (quote (("use_128") ("default" "use_128"))))))

