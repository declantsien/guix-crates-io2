(define-module (crates-io se rd serde_wormhole) #:use-module (crates-io))

(define-public crate-serde_wormhole-0.0.0-alpha.1 (c (n "serde_wormhole") (v "0.0.0-alpha.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a2kqzimib0400igjza7fds9cwx6swm9pfb6nhmykchrs8axxy5l")))

(define-public crate-serde_wormhole-0.1.0 (c (n "serde_wormhole") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "110xgpcxps25blyik4aijzclajg6sqzkqigx85is0y1mh6zj5c14")))

