(define-module (crates-io se rd serde_bytes_base64) #:use-module (crates-io))

(define-public crate-serde_bytes_base64-0.1.0 (c (n "serde_bytes_base64") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08xdrgbc8qi4sqbifwbz9hrp5i3wlmpp1afaiqrr02lhkiajf78f")))

(define-public crate-serde_bytes_base64-0.1.1 (c (n "serde_bytes_base64") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "145anfk01yx0v7a717rqlqs8bf34lcmffbzb92favkglqgfy66a8")))

