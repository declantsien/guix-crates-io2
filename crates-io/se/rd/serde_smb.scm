(define-module (crates-io se rd serde_smb) #:use-module (crates-io))

(define-public crate-serde_smb-0.1.0 (c (n "serde_smb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "count-write") (r "^0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smb3") (r "^0.1") (d #t) (k 2)))) (h "1l3xxizhjsqbrgkkh9y5knw4g87lcsrzr4nya2whmb2h628dvgas")))

