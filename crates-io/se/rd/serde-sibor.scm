(define-module (crates-io se rd serde-sibor) #:use-module (crates-io))

(define-public crate-serde-sibor-0.1.0 (c (n "serde-sibor") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "057f7rw8ayqhyzr918z6nsh5xyyyg85irc4j7v0b5813a77pg645")))

