(define-module (crates-io se rd serde07) #:use-module (crates-io))

(define-public crate-serde07-0.7.0 (c (n "serde07") (v "0.7.0") (d (list (d (n "serde") (r "^0.7") (k 0)))) (h "1m50sim4yy11wv3hz3cqlz4kkk3v8paank4cnxbxma5s2w2dwh4p") (f (quote (("std" "serde/std") ("nightly-testing" "serde/nightly-testing") ("nightly" "serde/nightly") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

