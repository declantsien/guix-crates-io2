(define-module (crates-io se rd serde-query-derive) #:use-module (crates-io))

(define-public crate-serde-query-derive-0.1.0 (c (n "serde-query-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "15iz5zc433pvnpqgk0yix15smn7qzxz86k54hg5ja2s42rhrsyzm")))

(define-public crate-serde-query-derive-0.1.1 (c (n "serde-query-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0n85fsmwhcwlkz6l1992q8i20jv1inyv56ii7qisskrc6jkjm0c0")))

(define-public crate-serde-query-derive-0.1.2 (c (n "serde-query-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "024m40jw21p2zvxb73ps7b776pv21v37qx9kvh1yik9p2fq2230r")))

(define-public crate-serde-query-derive-0.1.3 (c (n "serde-query-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0jcx821m81za3kg4ichzyif46fdc4igm6475zlnsgg8b3s5fcixh")))

(define-public crate-serde-query-derive-0.1.4 (c (n "serde-query-derive") (v "0.1.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "17xs4rhw9g6qkcsj66pjhw9xh379l2h06ppzyvwqrfv84mhgn6nn")))

(define-public crate-serde-query-derive-0.1.5 (c (n "serde-query-derive") (v "0.1.5") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "0f2155dx9ysih165ks6ayvriyh50ynd1sp0ib4xcdai8wg6kn5id")))

(define-public crate-serde-query-derive-0.2.0 (c (n "serde-query-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde-query-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "13xv0hfypnnkscjhsm60ziinz69b7qqmlk8kkj7w686sq8vcav65")))

