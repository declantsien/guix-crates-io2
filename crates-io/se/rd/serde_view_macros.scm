(define-module (crates-io se rd serde_view_macros) #:use-module (crates-io))

(define-public crate-serde_view_macros-0.1.0 (c (n "serde_view_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0shb6lx90vybr8vx5s21iqsis435mb3bxsv1hbzkvdg2mzs85d7m")))

(define-public crate-serde_view_macros-0.1.2 (c (n "serde_view_macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1365a5ik0f1dripd0dbc8ryvx52rzmcqaw8jgrn4nixifzz7a0m6")))

(define-public crate-serde_view_macros-0.1.3 (c (n "serde_view_macros") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vd2ln7icwmzw7pfnhhq2dcznhjxig3ifqnm6y53iqj776s44ggp")))

(define-public crate-serde_view_macros-0.1.5 (c (n "serde_view_macros") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hpz7a4bjq5v6qlz7h58y63bw1zxmwqiw5gj7arjjzc7ymj0zj3d")))

(define-public crate-serde_view_macros-0.1.6 (c (n "serde_view_macros") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1h1ij1vzckb9x14dbcbffypwfybm1rwv7xk7yqlgrmdaqpzrkr26")))

