(define-module (crates-io se rd serde-capture) #:use-module (crates-io))

(define-public crate-serde-capture-0.1.0 (c (n "serde-capture") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07wp0dcpa9vph8sr4yfbasy8izgjwnf24ipil5gi88d9kr0rcwfh")))

