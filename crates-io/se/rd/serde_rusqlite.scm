(define-module (crates-io se rd serde_rusqlite) #:use-module (crates-io))

(define-public crate-serde_rusqlite-0.10.0 (c (n "serde_rusqlite") (v "0.10.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13hci9p9crmdrqz83kngza6ndgky0csmg6pj0dlkqahby2xkrqpn")))

(define-public crate-serde_rusqlite-0.10.1 (c (n "serde_rusqlite") (v "0.10.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1amakbqggvvrgfnli0ncg3qd1xmzycajv0kq96jy1m4h3gr08a92")))

(define-public crate-serde_rusqlite-0.11.0 (c (n "serde_rusqlite") (v "0.11.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01x8qvw0wvppmi1785f7y31d5n9hb4h4v6fg5372yaj4zkgwzzaj")))

(define-public crate-serde_rusqlite-0.11.2 (c (n "serde_rusqlite") (v "0.11.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xbvybcys4x4rarn8gkj2znn02xiyxq13m9377qm4wjhbkzhlzns")))

(define-public crate-serde_rusqlite-0.12.1 (c (n "serde_rusqlite") (v "0.12.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0v3pjcwac0k0zjdmjrh4xs7jwsqw981sjl32p0dizhxslhlgkzwn")))

(define-public crate-serde_rusqlite-0.13.0 (c (n "serde_rusqlite") (v "0.13.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0iwbpcrgkm60g50yai3nv5rabkb2sn0kv3sj1z48gkk1ibxbwd4a")))

(define-public crate-serde_rusqlite-0.14.0 (c (n "serde_rusqlite") (v "0.14.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13gimxjabq4x6kclxi5hmpsckd4ldz9hdy06kga48df9dh3rl94g")))

(define-public crate-serde_rusqlite-0.15.0 (c (n "serde_rusqlite") (v "0.15.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02pvv924jq5sn4hmw2q2wrbngr05krpvwflhdgx7brkbaq963gz3")))

(define-public crate-serde_rusqlite-0.15.1 (c (n "serde_rusqlite") (v "0.15.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "032m7z46c3wz2qffq5nh95y0lkapsbrii6j7caxacwdd71gbsg69")))

(define-public crate-serde_rusqlite-0.16.0 (c (n "serde_rusqlite") (v "0.16.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bmhqqpzasxqcrcpj3yw65swb2minvd6nk97jrsnjp73lfjpbg83")))

(define-public crate-serde_rusqlite-0.17.0 (c (n "serde_rusqlite") (v "0.17.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1s51br5yci8knwph1nnpr79nlmm2hmmcfb8vqlfwjzdq5s89k8qp")))

(define-public crate-serde_rusqlite-0.18.0 (c (n "serde_rusqlite") (v "0.18.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sl09w9paz3xbinf5px7inxx10vczw9fj903kpppyddqa9d476in")))

(define-public crate-serde_rusqlite-0.19.0 (c (n "serde_rusqlite") (v "0.19.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0zwhf4j4x6r8hzjx7mpbqhxf3dvy86kqyhji4zxvvgcar2vkknm3")))

(define-public crate-serde_rusqlite-0.19.1 (c (n "serde_rusqlite") (v "0.19.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11smllzik08akj0wx6qa9cf6hnz72bmmk4wknbf82yb4nypcmfl7")))

(define-public crate-serde_rusqlite-0.20.0 (c (n "serde_rusqlite") (v "0.20.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07s17i2svn2mmnaiajzcxhgqga4a6jxh74xgg2wgba5xydmy71j3")))

(define-public crate-serde_rusqlite-0.21.0 (c (n "serde_rusqlite") (v "0.21.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16rc5lf8gm5zdv3kw72nhvf9plf9p33yz20dvx92cndlpxxfnf4v")))

(define-public crate-serde_rusqlite-0.22.0 (c (n "serde_rusqlite") (v "0.22.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0y2hcclixl7a4rhqkbzg5dals1m85yz4r4hjh4p24sbn7zsgd9fk")))

(define-public crate-serde_rusqlite-0.23.0 (c (n "serde_rusqlite") (v "0.23.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1vdwv0bgy96d1n94nqdryjl1cvd4qyd30l0niwqw3cds1i78z1gc")))

(define-public crate-serde_rusqlite-0.24.0 (c (n "serde_rusqlite") (v "0.24.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02711xzh426wmvc9iw906adrdny6vppsji21wbv3c8n8d5vrmw33")))

(define-public crate-serde_rusqlite-0.25.0 (c (n "serde_rusqlite") (v "0.25.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0adpv0c8991w25hhjy8mv079s6m6v1p2yngkbk34c3881i29mj30")))

(define-public crate-serde_rusqlite-0.26.0 (c (n "serde_rusqlite") (v "0.26.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "051v2fn5wgqzd8fis3gh2sa4jzkxca0hzs0h4fkxmhlx83ib88qh")))

(define-public crate-serde_rusqlite-0.27.0 (c (n "serde_rusqlite") (v "0.27.0") (d (list (d (n "rusqlite") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f3v4bgwp7prcy7zs1jjyw0vn1gkfqgdh1y41lcsa41zgcn7if21")))

(define-public crate-serde_rusqlite-0.28.0 (c (n "serde_rusqlite") (v "0.28.0") (d (list (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1h4i9gnhypklwcpcpal0kj3j9rr4g3yr6m1g2763bii2zra6xvn4")))

(define-public crate-serde_rusqlite-0.29.0 (c (n "serde_rusqlite") (v "0.29.0") (d (list (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15sx2p9a88prakj28krhkf8h6x1rs2v9av8h34vy4calh6rixs50")))

(define-public crate-serde_rusqlite-0.30.0 (c (n "serde_rusqlite") (v "0.30.0") (d (list (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0zi8dg8dpkv5q0rn7hskrsvxn6fxy409cdm2mik2r3abxvg5xzna")))

(define-public crate-serde_rusqlite-0.30.1 (c (n "serde_rusqlite") (v "0.30.1") (d (list (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1v83lllqw542c5kp02mqp8h5hyj1gjnmfd4ylv9b4jq53n1hjdg7")))

(define-public crate-serde_rusqlite-0.31.0 (c (n "serde_rusqlite") (v "0.31.0") (d (list (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ji4fjh95nj8mv59gnr1391syl1ydvx09alwpmf3fwg21vqm32sk")))

(define-public crate-serde_rusqlite-0.32.0 (c (n "serde_rusqlite") (v "0.32.0") (d (list (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0jjcp4fbsly8v4p9bd8sk0zybmwb1dag0gxjfp9znry2pv050kh9")))

(define-public crate-serde_rusqlite-0.33.0 (c (n "serde_rusqlite") (v "0.33.0") (d (list (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1nf3ra23zhp9v9lcph62l71q08saspf8x9yd6bwi1f5hr81v8jpl")))

(define-public crate-serde_rusqlite-0.33.1 (c (n "serde_rusqlite") (v "0.33.1") (d (list (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bxqgk3mdrpfakagi3a1rfla98kqxpg74yd034bsk4r68ixbyynl")))

(define-public crate-serde_rusqlite-0.34.0 (c (n "serde_rusqlite") (v "0.34.0") (d (list (d (n "rusqlite") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12q2bamgpfwq2kxw1i4v619j2dsf2sm7ncjdb32n995d9b0xl026")))

(define-public crate-serde_rusqlite-0.35.0 (c (n "serde_rusqlite") (v "0.35.0") (d (list (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ml36934k9rfmzgqk94fhx5pgwz3w1p5nx3mgg6wpki4jlyr0vc3")))

