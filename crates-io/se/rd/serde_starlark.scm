(define-module (crates-io se rd serde_starlark) #:use-module (crates-io))

(define-public crate-serde_starlark-0.0.0 (c (n "serde_starlark") (v "0.0.0") (h "0604dalzp7aw7p66dngdyqfjpzkwy2s4hnkkhbnv993v6hhbk0yi") (y #t)))

(define-public crate-serde_starlark-0.1.0 (c (n "serde_starlark") (v "0.1.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1zravyscc7ky0hnxm0krq3xphzf92wm2ss0bnkmph4b6h3n3zg0z") (r "1.64")))

(define-public crate-serde_starlark-0.1.1 (c (n "serde_starlark") (v "0.1.1") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1l9dzz8v2q9c3vw8ylc7zg2l25mgn0k3q3hvdkjpbl07hdjqh9yy") (r "1.64")))

(define-public crate-serde_starlark-0.1.2 (c (n "serde_starlark") (v "0.1.2") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1p6wj90azvmcaa3ap4j8v5b8bpm9c7xhw1gv8yx71fj491bw1af1") (r "1.64")))

(define-public crate-serde_starlark-0.1.3 (c (n "serde_starlark") (v "0.1.3") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0nm4da7byl9kw5ia4ikll6nng6rj99h2gcbrgdjvfqw697hap1rn") (r "1.64")))

(define-public crate-serde_starlark-0.1.4 (c (n "serde_starlark") (v "0.1.4") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "02z4507z3j05xmb7jqik11fclp5iqs4jc63ba9rsc98nk0mmrghx") (r "1.64")))

(define-public crate-serde_starlark-0.1.5 (c (n "serde_starlark") (v "0.1.5") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "042ndaiflm5xl310smr01wcvwkh31q45ln02bkylgs6kf0781daf") (r "1.64")))

(define-public crate-serde_starlark-0.1.6 (c (n "serde_starlark") (v "0.1.6") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1zancy228s1b5jzd3gswcy2in6qns5p7pq5460qchvp835hf6wai") (r "1.56")))

(define-public crate-serde_starlark-0.1.7 (c (n "serde_starlark") (v "0.1.7") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0wx14hvc7a2v804s8h3kfa78mi8yds0l4rcdsaxn5mw2a8s5smsa") (r "1.56")))

(define-public crate-serde_starlark-0.1.8 (c (n "serde_starlark") (v "0.1.8") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0z5kpbpq0m41vy77zz10s8j78w411kms2c4qxh5jlgv105gzqzkl") (r "1.56")))

(define-public crate-serde_starlark-0.1.9 (c (n "serde_starlark") (v "0.1.9") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "028p59fng9c1dzvj6f1c3ihxarh74mkcy8h5jz3akmfha5i5yyin") (r "1.56")))

(define-public crate-serde_starlark-0.1.10 (c (n "serde_starlark") (v "0.1.10") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0ljlszxcxsg23c5j3666bb21zw6ip491wpf4qz7lhhkf2hmwf9dx") (r "1.56")))

(define-public crate-serde_starlark-0.1.11 (c (n "serde_starlark") (v "0.1.11") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1crbz8dn69h2818n2156jzirpngp836z3x4l6l2gd3pxb8nw7szc") (r "1.56")))

(define-public crate-serde_starlark-0.1.12 (c (n "serde_starlark") (v "0.1.12") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "semver") (r "^1") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1kn406h5awv71sj4grrvb21qsky2dlhrkrcbvlcfhi19p4kmy71f") (r "1.56")))

(define-public crate-serde_starlark-0.1.13 (c (n "serde_starlark") (v "0.1.13") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)))) (h "0p7mbzbzf3f3kisdc48mr6znz5a0b6again2fyfiwfayjdxqkm5a") (r "1.56")))

(define-public crate-serde_starlark-0.1.14 (c (n "serde_starlark") (v "0.1.14") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)))) (h "0mvrxjg6p73p6i28nih825rqrvg9vvv723p0290apiyldl8mnrr9") (r "1.56")))

(define-public crate-serde_starlark-0.1.15 (c (n "serde_starlark") (v "0.1.15") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)))) (h "139yfw340sls7g4fl5mqv4fs50fl7drnlpn9gz5b4p0cxsacglz1") (r "1.56")))

(define-public crate-serde_starlark-0.1.16 (c (n "serde_starlark") (v "0.1.16") (d (list (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)))) (h "0jpdd49qplr049wmyavmkx7qrmha9rrw2qmq2rq68ly8q4k5zwj3") (r "1.56")))

