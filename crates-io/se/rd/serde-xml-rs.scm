(define-module (crates-io se rd serde-xml-rs) #:use-module (crates-io))

(define-public crate-serde-xml-rs-0.1.0 (c (n "serde-xml-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "04dg8lrvpv2ar8cv4x6zmsa9gwfm2ssm1kwlsc36a0hsn9fs3h3c")))

(define-public crate-serde-xml-rs-0.1.1 (c (n "serde-xml-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "0p82z7rp8168bs2ghaiyxq1ixbcam8qiz7wnxg3adz0qvq6qddyv")))

(define-public crate-serde-xml-rs-0.1.2 (c (n "serde-xml-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "0a83km6zfvlryxw5spxngdzcg9w6z7qg26119j5cqb986f0gyr3l")))

(define-public crate-serde-xml-rs-0.2.0 (c (n "serde-xml-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "0vkgz0998lnq8davi81gbdzf6qaxxsfscvcykdpfwi0rd44ikkiz")))

(define-public crate-serde-xml-rs-0.2.1 (c (n "serde-xml-rs") (v "0.2.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "0s3zb6fw4x5frv9injyyq37l4q4z6ca8xkfg9rywdvhk8cgqh1hc")))

(define-public crate-serde-xml-rs-0.3.0 (c (n "serde-xml-rs") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0f5a5mmsdhkmn528ba20ad15gn3b36sdnhd0zw2n7rj4c4nmg528")))

(define-public crate-serde-xml-rs-0.3.1 (c (n "serde-xml-rs") (v "0.3.1") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1c9kndxgpcpyjvpi1pa3k8jnl9zldgjpv0vcsqr8bajg4gy8vn97") (f (quote (("with-backtrace" "error-chain/default") ("legacy-support" "error-chain/example_generated") ("default" "with-backtrace"))))))

(define-public crate-serde-xml-rs-0.4.0 (c (n "serde-xml-rs") (v "0.4.0") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0v44g8y2kk4vldl6912nblfgbvh3rrb9pl27zjrbpl7kbj91br7g")))

(define-public crate-serde-xml-rs-0.4.1 (c (n "serde-xml-rs") (v "0.4.1") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1ykx1xkfd59gf0ijnp93xhpd457xy4zi8xv2hrr0ikvcd6h1pgzh")))

(define-public crate-serde-xml-rs-0.5.0 (c (n "serde-xml-rs") (v "0.5.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1vnl8vqsmda4gi63r1788xxa5l32fiyrwdg04nx82vvm96155nrs")))

(define-public crate-serde-xml-rs-0.5.1 (c (n "serde-xml-rs") (v "0.5.1") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1ypdy4cry8y6jbia0l0cqvkxkdvl7vplzfzb44s6lbxyb682w5k5")))

(define-public crate-serde-xml-rs-0.6.0 (c (n "serde-xml-rs") (v "0.6.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "10i7dvd0c1clj4jbljd08qs8466nlymx7ma7k3ncksx1rn7affpv")))

