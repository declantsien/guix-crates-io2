(define-module (crates-io se rd serde_derive_internals) #:use-module (crates-io))

(define-public crate-serde_derive_internals-0.15.0 (c (n "serde_derive_internals") (v "0.15.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1lc0l6klq1zfc4bbmm4zvaflk630rf4f7dvagaahzqy74a6k6702")))

(define-public crate-serde_derive_internals-0.15.1 (c (n "serde_derive_internals") (v "0.15.1") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "0fjxwrp6yslj4zj2d5rk28hi99zdn6g23k60zfn03n2jvbhf9bip")))

(define-public crate-serde_derive_internals-0.16.0 (c (n "serde_derive_internals") (v "0.16.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "0n3gswnav351d23yjx59dd2inxmpaga9412km3dnqqd605niyf5x")))

(define-public crate-serde_derive_internals-0.17.0 (c (n "serde_derive_internals") (v "0.17.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1aqknmk7hqr30d1ydbbygbxdr7dn9smd4qzjbnsjsfsjhmi95w9j")))

(define-public crate-serde_derive_internals-0.18.0 (c (n "serde_derive_internals") (v "0.18.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "00q6mhf2fzv7lhr1w8xzdzkidfp4szq7n2xlnmns20cqp73smikm")))

(define-public crate-serde_derive_internals-0.18.1 (c (n "serde_derive_internals") (v "0.18.1") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1bl95pb4jgm4axsv5mhy0f97g86b8rp42zw306k6knz8kzrfj3vk")))

(define-public crate-serde_derive_internals-0.19.0 (c (n "serde_derive_internals") (v "0.19.0") (d (list (d (n "syn") (r "^0.11.10") (f (quote ("parsing"))) (k 0)) (d (n "synom") (r "^0.11") (d #t) (k 0)))) (h "1damvp27z4dc2bspapcrmqhmjsi4v6yjdf69lnhb0gqcag4z20vf")))

(define-public crate-serde_derive_internals-0.20.0 (c (n "serde_derive_internals") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1s5dvniqc8pc0zg138f4lk0knhqxxa3nbh404fccscmyfg84ij0z")))

(define-public crate-serde_derive_internals-0.20.1 (c (n "serde_derive_internals") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "0361z79am1a8w3ksyq330hkjk84y96d291d421x079y4wlgpi6x7")))

(define-public crate-serde_derive_internals-0.21.0 (c (n "serde_derive_internals") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "135byy01rg8garx51phiqc7549s94ghv1l0lr4yj8xbr55vs82ip")))

(define-public crate-serde_derive_internals-0.22.0 (c (n "serde_derive_internals") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1z1wpa5ch04hsx26pixy949r4q5drsprs7hlbqf3f7m4a97p2gvb")))

(define-public crate-serde_derive_internals-0.22.1 (c (n "serde_derive_internals") (v "0.22.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1r1k6mnm1m4qvbfmcvf9q1pzzzrmy0xny7lbkcdfw941l2njb5cz")))

(define-public crate-serde_derive_internals-0.22.2 (c (n "serde_derive_internals") (v "0.22.2") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "0igrriggirjic4xywagqzchsfl3fyfm59xf8lnay2v38j794xw6j")))

(define-public crate-serde_derive_internals-0.23.0 (c (n "serde_derive_internals") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1khw33m7mixaljnapbf6xn8dy66114bzzl1ipbfh7g25haj41cw9")))

(define-public crate-serde_derive_internals-0.23.1 (c (n "serde_derive_internals") (v "0.23.1") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1577rbhxf000p6fq3slk06n7k6izd1cmbwcylyyppzahcicw8c4x")))

(define-public crate-serde_derive_internals-0.24.0 (c (n "serde_derive_internals") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "0gpk52vamgbs4kqn4n3hwsjhlp4kldiw2ff8m91v03s7rmz72i3g")))

(define-public crate-serde_derive_internals-0.24.1 (c (n "serde_derive_internals") (v "0.24.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "clone-impls"))) (k 0)))) (h "1ywgn5917kczly5yrbzi2xfl4yhrzbly5sf7q97fmg7bn70cd04a")))

(define-public crate-serde_derive_internals-0.25.0 (c (n "serde_derive_internals") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "1ihqfkpplqqiwmh87s8p9jsv27ibkz1z7gc0abqs2mrhlr6b7fhx")))

(define-public crate-serde_derive_internals-0.26.0 (c (n "serde_derive_internals") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "0g2zdr6s8i0r29yy7pdl6ahimq8w6ck70hvrciiry2ljwwlq5gw5")))

(define-public crate-serde_derive_internals-0.27.0 (c (n "serde_derive_internals") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "0nsrzsx806vzs68j7jk0hjsrjvnidrnzihwqzznbfwx2lfq36rak") (r "1.56")))

(define-public crate-serde_derive_internals-0.28.0 (c (n "serde_derive_internals") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "clone-impls"))) (k 0)))) (h "02cmyjzfyqxx2i7cc163sxdxlmsgd3bm3fkbssj8jh8bsi1shy75") (r "1.56")))

(define-public crate-serde_derive_internals-0.29.0 (c (n "serde_derive_internals") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("clone-impls" "derive" "parsing" "printing"))) (k 0)))) (h "1qsl3pkk9yg57wivsxg8lsw77d94l34z70hclrczx9d3cp7023rk") (r "1.56")))

(define-public crate-serde_derive_internals-0.29.1 (c (n "serde_derive_internals") (v "0.29.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing"))) (k 0)))) (h "04g7macx819vbnxhi52cx0nhxi56xlhrybgwybyy7fb9m4h6mlhq") (r "1.56")))

