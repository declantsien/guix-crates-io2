(define-module (crates-io se rd serde2file_macro_derive) #:use-module (crates-io))

(define-public crate-serde2file_macro_derive-0.1.0 (c (n "serde2file_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1650chpbgps9g1rihacsb8hq00pcwxq15azkfp5wvcgw3fbkhvfa")))

(define-public crate-serde2file_macro_derive-0.1.1 (c (n "serde2file_macro_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b3ad4blbg00zrsaswl03mdfnlwz5nk78diijm6yfjmrywr5nsn8")))

(define-public crate-serde2file_macro_derive-0.2.0 (c (n "serde2file_macro_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yc9sdw017svhzn9x4nik357haaaafi8065df58h8cjfjwkhg0wv")))

(define-public crate-serde2file_macro_derive-0.2.1 (c (n "serde2file_macro_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rsw1a29skp5cb82idpfm9xgi2752l1l0yhr4amzd9id3i967ik2")))

(define-public crate-serde2file_macro_derive-0.2.2 (c (n "serde2file_macro_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qjdv707k669a5m0wkr6nimcdhl5415kxczwa80p695qqvg3iq1n")))

(define-public crate-serde2file_macro_derive-0.3.0 (c (n "serde2file_macro_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x7fyd1vc4hxb2h4bsz83r6v6ahqdkkzrwyf6d4qsgjn8ypl1cdr")))

(define-public crate-serde2file_macro_derive-0.4.0 (c (n "serde2file_macro_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zfbwp2zn45xwsdly4qgxljc71dzal2rv7qjr2s891hkdj3mhiwl")))

(define-public crate-serde2file_macro_derive-0.5.0 (c (n "serde2file_macro_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qj0rdq05dlwvfwwgjikiba6y2pq7k9gbyvykmd3hajk7ryr0am9")))

(define-public crate-serde2file_macro_derive-0.6.0 (c (n "serde2file_macro_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "081c854nhbjmbk57f46yapqqlsahydw8fmmyj4h8c3jkxq3x8sgn")))

(define-public crate-serde2file_macro_derive-0.6.1 (c (n "serde2file_macro_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m7xvw5gh4yncafpilx4yg6jsbw9yq27km1vjzhp44ccbndvxzzf")))

