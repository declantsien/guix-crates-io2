(define-module (crates-io se rd serde_webgpu) #:use-module (crates-io))

(define-public crate-serde_webgpu-0.1.0 (c (n "serde_webgpu") (v "0.1.0") (d (list (d (n "half") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0m5vj398hc5bgmk8d0nmr05g6din1ww82883a3xsg751p44mjrjh")))

(define-public crate-serde_webgpu-0.1.1 (c (n "serde_webgpu") (v "0.1.1") (d (list (d (n "half") (r "^2.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1na3kcq512nddbvn186dg4yaikqsb8c9x9d2n74w2sdy769jvnw0")))

(define-public crate-serde_webgpu-0.2.0 (c (n "serde_webgpu") (v "0.2.0") (d (list (d (n "half") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zscfcfd3yyq548g982gcwjya3lm4f1ywj22dnkbp4y2byq4d1ad")))

(define-public crate-serde_webgpu-0.2.1 (c (n "serde_webgpu") (v "0.2.1") (d (list (d (n "half") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1sl53qf619ricsv8bqk6ljbb25g8hf3aamlk54m4pf6ci9hgnniq")))

