(define-module (crates-io se rd serde_scala) #:use-module (crates-io))

(define-public crate-serde_scala-0.1.0 (c (n "serde_scala") (v "0.1.0") (d (list (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)))) (h "1n1nyzkikdwfp6vk48sbxw3lc97c8n6j03wcarr4h89qqd9fy8gn")))

(define-public crate-serde_scala-0.1.1 (c (n "serde_scala") (v "0.1.1") (d (list (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)))) (h "1ph1xc85ahhylkc63ylfydk74bidz5mg9k16mnkp9cp28iqsjayc")))

(define-public crate-serde_scala-0.1.2 (c (n "serde_scala") (v "0.1.2") (d (list (d (n "num-rational") (r "^0.4.1") (d #t) (k 0)))) (h "1jq3ldxyh716zyxl363kd23hnqw7sk6fqc41f039kqkgcdwda39l")))

