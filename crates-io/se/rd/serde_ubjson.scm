(define-module (crates-io se rd serde_ubjson) #:use-module (crates-io))

(define-public crate-serde_ubjson-0.2.0 (c (n "serde_ubjson") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gc2ipfdrv35bxhfp7w1861gb1jx0r9bci3v2kmcxkmni1kfy0j4")))

(define-public crate-serde_ubjson-0.2.1 (c (n "serde_ubjson") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1wr27d2zx7hxx3x82x9fvsl72hhpry54xinb54rgcjmayngibnf6")))

