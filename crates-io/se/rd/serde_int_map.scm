(define-module (crates-io se rd serde_int_map) #:use-module (crates-io))

(define-public crate-serde_int_map-0.1.0 (c (n "serde_int_map") (v "0.1.0") (h "0ndjchzyj4sij5jgxldcc48rnrjmr962xqxkdw38axqp98mnc8wb")))

(define-public crate-serde_int_map-0.1.1 (c (n "serde_int_map") (v "0.1.1") (d (list (d (n "serde_int_map_derive") (r "^0.1.1") (d #t) (k 0)))) (h "122n5ijshpq8zwbqj5pgyvwznkqcphq5dyapwmjpk7nl7l35y9zf")))

(define-public crate-serde_int_map-0.1.2 (c (n "serde_int_map") (v "0.1.2") (h "0686lkimqrgb9raaxbjn22j0zfws9mfj8m2z5m7vqf5zrgcdzbi6")))

(define-public crate-serde_int_map-0.2.0 (c (n "serde_int_map") (v "0.2.0") (d (list (d (n "serde_int_map_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde_int_map_traits") (r "^0.2") (d #t) (k 0)))) (h "19va3317im5mf8vyk1b738j59h3fxc1p0il9n5r1d87ylnwgxs0d")))

(define-public crate-serde_int_map-0.3.0 (c (n "serde_int_map") (v "0.3.0") (d (list (d (n "serde_int_map_derive") (r "^0.3") (d #t) (k 0)) (d (n "serde_int_map_traits") (r "^0.3") (d #t) (k 0)))) (h "0wixfiaf38nmgb8y7vvgz5cas8cffd8f6z501njlqg82gz9i7myz")))

