(define-module (crates-io se rd serde_amf) #:use-module (crates-io))

(define-public crate-serde_amf-0.1.0 (c (n "serde_amf") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "18rqz97ha5l33rixgxrsmmag21gdih11qhpv5cjs1kjxcq76x4m1")))

