(define-module (crates-io se rd serde_ignored) #:use-module (crates-io))

(define-public crate-serde_ignored-0.0.1 (c (n "serde_ignored") (v "0.0.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "1x55x7c8fqn93chsjb8ph3rmjvv9f26n9p2ifpind6z4zkh56v5p")))

(define-public crate-serde_ignored-0.0.2 (c (n "serde_ignored") (v "0.0.2") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0r4gpn1420igrz0gvxp25fjp4vi8xpqfjmq6d4a1swkld1bzbcyl")))

(define-public crate-serde_ignored-0.0.3 (c (n "serde_ignored") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iigx0bl7n7bsn212z7dm4qgf1k7xqsrx62q6vndrmq58j77j3n1")))

(define-public crate-serde_ignored-0.0.4 (c (n "serde_ignored") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hmi3z05scaadrrh98qnl1qv1qy70sh962bf7gk6pdgdvijrf3hr")))

(define-public crate-serde_ignored-0.1.0 (c (n "serde_ignored") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0naf6s0x17ai62z4yzyzdnhgphimh9ljhgnd33v3865qyjwbn91w")))

(define-public crate-serde_ignored-0.1.1 (c (n "serde_ignored") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03kavbv3h2lzdlczr5dzaazsgna4r3nd00iazh22cgvxs75zsj3j")))

(define-public crate-serde_ignored-0.1.2 (c (n "serde_ignored") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bzz3546g3p01hgwh6jh0gyqdwc28xcp3pir4al2wbsgs4wpsb0w")))

(define-public crate-serde_ignored-0.1.3 (c (n "serde_ignored") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cx63zhnkd8gfl1il6dj2wlfdpw74039s00j02j525j1l9n06h0r") (r "1.31")))

(define-public crate-serde_ignored-0.1.4 (c (n "serde_ignored") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rncnm9jzcav6d85gzv02wdvsv1nc3xncrlzgi7143lhqgg0fyfy") (r "1.36")))

(define-public crate-serde_ignored-0.1.5 (c (n "serde_ignored") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08bdbicajiicrkljc4ajqkbq9lbr3jfq4rvghrx68ryrxmzdmcw2") (r "1.36")))

(define-public crate-serde_ignored-0.1.6 (c (n "serde_ignored") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "192l9msvbyqlvk97b4x60dzw36p2zzyn0n7gg0ykay0p2yv2w8ai") (r "1.36")))

(define-public crate-serde_ignored-0.1.7 (c (n "serde_ignored") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19j5mrngznhxa7yfvxwmc4dc0mdzvm7w92i0m4adz2xshx04mswl") (r "1.36")))

(define-public crate-serde_ignored-0.1.8 (c (n "serde_ignored") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "09xqh88hbnxz1h04rvbn8l5z9rpikn1qb5q9100ab6wsa81x3sbd") (r "1.36")))

(define-public crate-serde_ignored-0.1.9 (c (n "serde_ignored") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.166") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "199c91ddk6p132xavn6hm3idw55j1c5a5xbhww4g4fgxadf1vhw0") (r "1.36")))

(define-public crate-serde_ignored-0.1.10 (c (n "serde_ignored") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.194") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 2)))) (h "1psdv0ahmxgw4l3dg341j5q2k09d7glj93v01mm14lhvdniikqx8") (r "1.36")))

