(define-module (crates-io se rd serde_jcs) #:use-module (crates-io))

(define-public crate-serde_jcs-0.1.0 (c (n "serde_jcs") (v "0.1.0") (d (list (d (n "ryu-js") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std" "float_roundtrip"))) (k 0)))) (h "1a6rx9qmnwq1xsqr6aysmcb839y6fwwq3k4rwbqcb9y1kdjczkna")))

