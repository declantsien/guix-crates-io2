(define-module (crates-io se rd serde-indexed) #:use-module (crates-io))

(define-public crate-serde-indexed-0.0.1 (c (n "serde-indexed") (v "0.0.1") (d (list (d (n "heapless") (r "^0.5.1") (f (quote ("serde"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 2)) (d (n "serde_cbor") (r "^0.11.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dga7jaamdyz7ay3i6mkr3406i17wyi1g3xa3my7ra1w04yqvajx")))

(define-public crate-serde-indexed-0.0.2 (c (n "serde-indexed") (v "0.0.2") (d (list (d (n "heapless") (r "^0.5.1") (f (quote ("serde"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 2)) (d (n "serde_cbor") (r "^0.11.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "093lvva53g69vfjnac7smcilhd2kngpjrcykvdrlhk7a4zdw1lhr")))

(define-public crate-serde-indexed-0.0.3 (c (n "serde-indexed") (v "0.0.3") (d (list (d (n "heapless") (r "^0.5.1") (f (quote ("serde"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 2)) (d (n "serde_cbor") (r "^0.11.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gzxcwmix7f3d90mqsxxaw9avi4kwfnp5zsv8x13844pj959jkvv")))

(define-public crate-serde-indexed-0.0.4 (c (n "serde-indexed") (v "0.0.4") (d (list (d (n "heapless") (r "^0.5.1") (f (quote ("serde"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 2)) (d (n "serde_cbor") (r "^0.11.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kfcv3s4if9aw7nkjh8243dzglk7hxja7ayllz0jbvj4jwkxlzhd")))

(define-public crate-serde-indexed-0.1.0 (c (n "serde-indexed") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.6") (f (quote ("serde"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 2)) (d (n "serde_cbor") (r "^0.11.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hjzjsskv3vbqbdbb4pxb84dwwy73r4kpcnsmfywfxdpv82bc7s3")))

