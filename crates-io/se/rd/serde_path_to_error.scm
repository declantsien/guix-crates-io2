(define-module (crates-io se rd serde_path_to_error) #:use-module (crates-io))

(define-public crate-serde_path_to_error-0.1.0 (c (n "serde_path_to_error") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "188nbnn1w7gydfrkkkmjyrcg1syqh9kn7z8a0pjqdb8bxa0xcpaf")))

(define-public crate-serde_path_to_error-0.1.1 (c (n "serde_path_to_error") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13jgnnj32dkdf47s67yjd1c996g1vddlamv265m71wkxm1xbksdf")))

(define-public crate-serde_path_to_error-0.1.2 (c (n "serde_path_to_error") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qhbdhlivnhkymrwyq941klcqrw5r4n2fkd2g0y1c66q0m88k6rm")))

(define-public crate-serde_path_to_error-0.1.3 (c (n "serde_path_to_error") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14rmizb52ly1gcyhdi16pbh9l0g0ffd3z7f415vya0ffqiyfafwi")))

(define-public crate-serde_path_to_error-0.1.4 (c (n "serde_path_to_error") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n5ilbsxvi174m2fd506ivd43kws0yh523li1xz0zqh60ngi1xj2")))

(define-public crate-serde_path_to_error-0.1.5 (c (n "serde_path_to_error") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12qi29nvlq22f27dghm59jcqpqzsaxnz7avb5pbq5arz2x7ishnh")))

(define-public crate-serde_path_to_error-0.1.6 (c (n "serde_path_to_error") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gf9pw2f3s32d6vmvw8bvbn38n3zsyfpi1ifi9wv4j0p9vfqdlcv") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.7 (c (n "serde_path_to_error") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xx3n62nyjvrjzg4ypm5419ywy22285j5a4rx858lshrp39qm1np") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.8 (c (n "serde_path_to_error") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zflkys658wkmc0wfywa52r2rwd6lncfyk0hb7mpq3vq8hq68k0q") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.9 (c (n "serde_path_to_error") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hbkdhmz82hwx5bxasym776f74jlvnivsx00l4qi7jb3nli4zc16") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.10 (c (n "serde_path_to_error") (v "0.1.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12fi5rdkq7jdkpbdzsj9h253snic20fqpq4jvnm6p5rkypznj2fv") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.11 (c (n "serde_path_to_error") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q5vb8pfjan001qp529h3npy7jn35zsy3k7srppns1knahfmrw7p") (r "1.31")))

(define-public crate-serde_path_to_error-0.1.12 (c (n "serde_path_to_error") (v "0.1.12") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0maf9xakczp457rlclbghr3vk21zlw18i5gi0cg0ass9sxqn86qb") (r "1.56")))

(define-public crate-serde_path_to_error-0.1.13 (c (n "serde_path_to_error") (v "0.1.13") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "0fjdy9qb7ni1cyvh14bcy1kh9hpzrcfijz0irdv7mn4xjli49k4a") (r "1.56")))

(define-public crate-serde_path_to_error-0.1.14 (c (n "serde_path_to_error") (v "0.1.14") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "0dc31z4bg0jwn69gcqsczbmcy5y4w6r0vdcc4c38vma9x2ycivjb") (r "1.56")))

(define-public crate-serde_path_to_error-0.1.15 (c (n "serde_path_to_error") (v "0.1.15") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "0z1k6kkwi4v4psbwd91rrw9pqk90bikx4xaprzmzsffy82i59lgb") (r "1.56")))

(define-public crate-serde_path_to_error-0.1.16 (c (n "serde_path_to_error") (v "0.1.16") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 2)))) (h "19hlz2359l37ifirskpcds7sxg0gzpqvfilibs7whdys0128i6dg") (r "1.56")))

