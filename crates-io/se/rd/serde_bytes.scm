(define-module (crates-io se rd serde_bytes) #:use-module (crates-io))

(define-public crate-serde_bytes-0.9.0 (c (n "serde_bytes") (v "0.9.0") (d (list (d (n "bincode") (r "= 1.0.0-alpha6") (d #t) (k 2)) (d (n "serde") (r "^0.9") (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_test") (r "^0.9") (d #t) (k 2)))) (h "0m4hqc5d7s04dkl0szipi0s9mvvsgs8fcvr6k4i6jvibzqsh0pjs") (f (quote (("std" "serde/std") ("default" "std") ("collections" "serde/collections"))))))

(define-public crate-serde_bytes-0.1.0 (c (n "serde_bytes") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0hdvq77snryfh2hlsb2l7l2mp1y5rjhlqw9ck7glap9vw78m6rp9") (f (quote (("std" "serde/std") ("default" "std") ("collections" "serde/collections")))) (y #t)))

(define-public crate-serde_bytes-0.10.0 (c (n "serde_bytes") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0man49mrp2yvgmjw0bs4q7izznpgyias7islf901wgmqkfnzawvs") (f (quote (("std" "serde/std") ("default" "std") ("collections" "serde/collections"))))))

(define-public crate-serde_bytes-0.10.1 (c (n "serde_bytes") (v "0.10.1") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "068yqp02mjr4h5f6l4qqmibb2hvrcsaynvahwx6q9s9dpxiaxf0j") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.10.2 (c (n "serde_bytes") (v "0.10.2") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mbyyw6mj2w70w6w0npnzvdwsggicdjj6dmigv7l8m196z3sqdas") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.10.3 (c (n "serde_bytes") (v "0.10.3") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0nba0kbfnkrwlwzv7vc8i5cpvsvlyfdn6bf2hi2brbm3j2ppidjj") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.10.4 (c (n "serde_bytes") (v "0.10.4") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1crprfp0plm6jphpq7dm3735fi4bi5gph792ph0v75indcdfbdmd") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.10.5 (c (n "serde_bytes") (v "0.10.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "127c9br02ygajs4z3bw850i48nc25f4yn7kmh21wqd3z7nlbiyyy") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.0 (c (n "serde_bytes") (v "0.11.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mwnrpac4k9l444hl2af8bpgp7p37q0ls9lgaq72a6xxx4211nyc") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.1 (c (n "serde_bytes") (v "0.11.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0sc5n336i7q4fiij4l8f892zcirgybrbxzl8bp51qxzqdvdlgzxa") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.2 (c (n "serde_bytes") (v "0.11.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1z7gwq92wrav9ln68gnmqmvaa99qv9kynd8257maxav4zy103bs5") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.3 (c (n "serde_bytes") (v "0.11.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1bl45kf3c71xclv7wzk5525nswm4bgsnjd3s1s15f4k2a8whfnij") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.4 (c (n "serde_bytes") (v "0.11.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "043fq19qghfys7p2l70d12sx8741dg5i12zzl9z9s8y6ypxqgx1v") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.5 (c (n "serde_bytes") (v "0.11.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1fcb6sw8wkrj4ylm118wkb31hw124nkjnqyhbgqnd8w85zfhgbhn") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-serde_bytes-0.11.6 (c (n "serde_bytes") (v "0.11.6") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0m1sbwa4w6rzycqxycmpm4wgybm61ckyn0ma4gblipmw9r376bi1") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.7 (c (n "serde_bytes") (v "0.11.7") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0yykjghylayiv2skibi3a19n6785m0sawrxiviw63szfhf0hxifg") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.8 (c (n "serde_bytes") (v "0.11.8") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "06a8lv3x1zm1ynzq6xri4k46zklnzh62i6y47w4rjvxkypzwb3bi") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.9 (c (n "serde_bytes") (v "0.11.9") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1542ajxpmn8ywl7k8vhbm773klyx2ndd844f5kh95awsdx1xlss1") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.10 (c (n "serde_bytes") (v "0.11.10") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03pg7rx69sv8506z8v0bg580ysrygw32sd47cp4s38z48cr13igk") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.11 (c (n "serde_bytes") (v "0.11.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "0yj2kj2hcphabzrydpa4dndfm9clh8cy6iv4fc4dw2ijwm7vw5js") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.12 (c (n "serde_bytes") (v "0.11.12") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "1zv8sfkcgjzjjzaidgzx9vjxsnipw8x5jrccv3v5ln3pys9fqcxb") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.31")))

(define-public crate-serde_bytes-0.11.13 (c (n "serde_bytes") (v "0.11.13") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "041x29xr6q6pb33lsbjzwzbiz41yv9a2s7h3962vff1mm6g8gccb") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.53")))

(define-public crate-serde_bytes-0.11.14 (c (n "serde_bytes") (v "0.11.14") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.166") (k 0)) (d (n "serde_derive") (r "^1.0.166") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.166") (d #t) (k 2)))) (h "0d0pb7wsq2nszxvg2dmzbj9wsvrzchbq2m4742csnhzx2g1rg14b") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (r "1.53")))

