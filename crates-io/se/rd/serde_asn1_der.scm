(define-module (crates-io se rd serde_asn1_der) #:use-module (crates-io))

(define-public crate-serde_asn1_der-0.1.0 (c (n "serde_asn1_der") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.5") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 2)))) (h "0h0jdy7yrp1jnlfl1fknlbaqc5ix76m7cvrdp9rsvddq684g53l2")))

(define-public crate-serde_asn1_der-0.1.1 (c (n "serde_asn1_der") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xrg23h83xfvywh8gqbw6flcfzq59lr2i2wqd4f4m96lac3a5l76")))

(define-public crate-serde_asn1_der-0.7.0 (c (n "serde_asn1_der") (v "0.7.0") (d (list (d (n "asn1_der") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12xgm9hjnimxfybxvb3hq0jrdn2nc4i1hrhpqn56xjssswcx0axr") (y #t)))

(define-public crate-serde_asn1_der-0.7.1 (c (n "serde_asn1_der") (v "0.7.1") (d (list (d (n "asn1_der") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "088bjs3sfnp41smfm0824b2k8qrdvql21i0iypqzxgkcbcyg5w17")))

(define-public crate-serde_asn1_der-0.7.2 (c (n "serde_asn1_der") (v "0.7.2") (d (list (d (n "asn1_der") (r "^0.7.1") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lyrz4mrm2883ccpk0wypxirsk5fs2ibf8jwvb8qr8xlynfcicbi") (f (quote (("default") ("any" "erased-serde"))))))

(define-public crate-serde_asn1_der-0.7.3 (c (n "serde_asn1_der") (v "0.7.3") (d (list (d (n "asn1_der") (r "^0.7") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0day1c7rdy17hwrnq7cbw9xz0ymhi92pidnphg8xkdv7ji7i4bm7") (f (quote (("default") ("any" "erased-serde"))))))

(define-public crate-serde_asn1_der-0.7.4 (c (n "serde_asn1_der") (v "0.7.4") (d (list (d (n "asn1_der") (r "^0.7") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pfxz6frm6livpswyrkzp372gffxm5c7cdm8lrk9bcrk10a11h54") (f (quote (("default") ("any" "erased-serde"))))))

(define-public crate-serde_asn1_der-0.8.0 (c (n "serde_asn1_der") (v "0.8.0") (d (list (d (n "asn1_der") (r "^0.7") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1df5mbmwdgy90hkdq8lbfc8lfspnjvmq5nf6fpkdl7shvirngb3a") (f (quote (("default") ("any" "erased-serde"))))))

