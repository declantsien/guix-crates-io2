(define-module (crates-io se rd serde_postgres) #:use-module (crates-io))

(define-public crate-serde_postgres-0.1.0 (c (n "serde_postgres") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0z5zkxv5mxx4a0h5bndrc58yh4n4gpz1m8ry144s67v5ajkzfjnd")))

(define-public crate-serde_postgres-0.1.1 (c (n "serde_postgres") (v "0.1.1") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "1n7wfi6irmmgbzqfn2bk84xdc7wk10ramxjw9ip2vmkj3iby2sd7")))

(define-public crate-serde_postgres-0.1.2 (c (n "serde_postgres") (v "0.1.2") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0cc8av1gpqzsd5c4jy27fgq5774rkl0ryn5pjdw9idn1qia35cdf")))

(define-public crate-serde_postgres-0.1.3 (c (n "serde_postgres") (v "0.1.3") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "1rdm8zxm1adsiasgns2ssz8yrc5yxd8sirz4bzp3mvr3jfinx5ds")))

(define-public crate-serde_postgres-0.2.0 (c (n "serde_postgres") (v "0.2.0") (d (list (d (n "postgres") (r "^0.17.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.5.2") (k 0)) (d (n "tokio-postgres") (r "^0.5.2") (d #t) (k 2)))) (h "0zzy0vgzhg5a8b7s8m3276v4knv6jnym2hfi7jsbgwgdvyf2l9q5")))

