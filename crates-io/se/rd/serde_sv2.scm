(define-module (crates-io se rd serde_sv2) #:use-module (crates-io))

(define-public crate-serde_sv2-0.1.0 (c (n "serde_sv2") (v "0.1.0") (d (list (d (n "buffer_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)))) (h "17q174ms2jh68ppk5xsknxzqyrjmbdrzmwjpc3r12ja62472la6i")))

(define-public crate-serde_sv2-0.1.1 (c (n "serde_sv2") (v "0.1.1") (d (list (d (n "buffer_sv2") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)))) (h "10c1lnrcs2zzh1mc1mnr9sy91m63gax08nmihfs63gjpdbwmjrvg")))

(define-public crate-serde_sv2-0.1.3 (c (n "serde_sv2") (v "0.1.3") (d (list (d (n "buffer_sv2") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)))) (h "1m60iijhmcdkcsfkjv2g882pbibxy0hd9ls83r7n92rjz376g7ql")))

(define-public crate-serde_sv2-1.0.0 (c (n "serde_sv2") (v "1.0.0") (d (list (d (n "buffer_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)))) (h "019f2m6lfr1i019hlvyq732bpsl9sp1ig9xyskdkiskgd65kpf74")))

