(define-module (crates-io se rd serde_schema) #:use-module (crates-io))

(define-public crate-serde_schema-0.0.1 (c (n "serde_schema") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (o #t) (d #t) (k 0)))) (h "1q8y4k3cjg91ilc28jmrbsbb4ln3plx6scm30k1kq7w70f1wm518") (f (quote (("bytes" "serde_bytes"))))))

