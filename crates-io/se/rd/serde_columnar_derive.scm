(define-module (crates-io se rd serde_columnar_derive) #:use-module (crates-io))

(define-public crate-serde_columnar_derive-0.1.0 (c (n "serde_columnar_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ql96yiwbf2hd60l32rimwlyyzjfnb33lxm2myi105b88qsybz7f") (y #t)))

(define-public crate-serde_columnar_derive-0.1.1 (c (n "serde_columnar_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fg0nldzl36nvdjhddp70bgwvva0fjmqyf98pa5rkrfxjgvmbg6v")))

(define-public crate-serde_columnar_derive-0.1.2 (c (n "serde_columnar_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00fzbwsl2dpw47939ykyrjd5fbpicharm8ljqs895h09zsx02cvs") (y #t)))

(define-public crate-serde_columnar_derive-0.2.0 (c (n "serde_columnar_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06jkam07br50nrhgwhnvlwhm0idw2bmn3y0p1gryhalnw4c4s8vf")))

(define-public crate-serde_columnar_derive-0.2.1 (c (n "serde_columnar_derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lpc6zyyc0bi9m97696ncbydgvnp1l71mxf47af5ig0wx7iw4a0j") (f (quote (("compress"))))))

(define-public crate-serde_columnar_derive-0.2.2 (c (n "serde_columnar_derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fxbyg29sq3j21z2b25gj2x6dnzqwimqjnhswys6cihy8aw4r2n7") (f (quote (("compress"))))))

(define-public crate-serde_columnar_derive-0.2.3 (c (n "serde_columnar_derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i43xyj8w244yxgnvnb6nmqjr7665i0kwbz5xh0yls1lh97xbcxl") (f (quote (("compress") ("analyze"))))))

(define-public crate-serde_columnar_derive-0.3.0 (c (n "serde_columnar_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikqwkiwc2qqgl6gvkbbi2gr2m78mhx936fcxyabgv3lysmnj1v3") (f (quote (("compress") ("analyze"))))))

(define-public crate-serde_columnar_derive-0.3.1 (c (n "serde_columnar_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ly2pfykcwmr5h8xzyvfy6q6ksx9z3wbkdwcks63bjwx3jz144a7") (f (quote (("compress") ("analyze"))))))

(define-public crate-serde_columnar_derive-0.3.2 (c (n "serde_columnar_derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08450435d1zwhlz2h8zc8nc5irvipzh2wd452lxwzd711h9hmma6") (f (quote (("compress") ("analyze"))))))

(define-public crate-serde_columnar_derive-0.3.3 (c (n "serde_columnar_derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r7dckbl8di9mr8zg4jzl34qlclkqbx0900zgq79g4lv5anppxx0") (f (quote (("analyze"))))))

(define-public crate-serde_columnar_derive-0.3.4 (c (n "serde_columnar_derive") (v "0.3.4") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lgd01y8n3b337dxka58zvg6zy207x935vmizdzkjnn5pg5alpiy") (f (quote (("analyze"))))))

