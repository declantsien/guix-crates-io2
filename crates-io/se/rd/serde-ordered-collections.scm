(define-module (crates-io se rd serde-ordered-collections) #:use-module (crates-io))

(define-public crate-serde-ordered-collections-2.0.0 (c (n "serde-ordered-collections") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0dj93clciay4807d3rl908vdhisbzdh8sv8mwnp384587rsisc6x")))

