(define-module (crates-io se rd serdeio) #:use-module (crates-io))

(define-public crate-serdeio-0.1.0 (c (n "serdeio") (v "0.1.0") (d (list (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0va4c0xgw31zz0jxa7dr9810lhzb7dvb9q8z90ka86bb16dlg3zd") (s 2) (e (quote (("yaml" "dep:serde_yaml") ("csv" "dep:csv"))))))

(define-public crate-serdeio-0.2.0 (c (n "serdeio") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1n3ij674ymq7szfbhvk7iqaq2ldbnbgyj0hyrvnjdxv8h1b5apdn") (s 2) (e (quote (("yaml" "dep:serde_yaml") ("csv" "dep:csv"))))))

(define-public crate-serdeio-0.3.0 (c (n "serdeio") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0vjv94ymrr3nmg6p6ylqaksy57m8zvrlpbiblvw5h7l446n8imgx") (s 2) (e (quote (("yaml" "dep:serde_yaml") ("csv" "dep:csv"))))))

(define-public crate-serdeio-0.4.0 (c (n "serdeio") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0kaah5k0w8xxnzpj557ddarisihzj994zjsyvsgvh7dbpd3f98kd") (s 2) (e (quote (("yaml" "dep:serde_yaml") ("csv" "dep:csv"))))))

