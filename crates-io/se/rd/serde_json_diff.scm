(define-module (crates-io se rd serde_json_diff) #:use-module (crates-io))

(define-public crate-serde_json_diff-0.1.0 (c (n "serde_json_diff") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "14hcx6b7ra0ka4h3fry6pv039k1xmk7w8sry4qavrl17kl68kcbh") (f (quote (("cli" "clap" "thiserror"))))))

(define-public crate-serde_json_diff-0.1.1 (c (n "serde_json_diff") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0hp6l46v7a6g38jkgvlribdbpnsqa7hx47p8psxp7nk3jkm9a2cg") (f (quote (("cli" "clap" "thiserror"))))))

(define-public crate-serde_json_diff-0.2.0 (c (n "serde_json_diff") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1v80a9mjnvdm9fr6vg9zhd7pdla1wjx4m4nmr67dfmlmvvr1bipa") (f (quote (("cli" "clap" "thiserror"))))))

