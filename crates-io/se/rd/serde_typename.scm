(define-module (crates-io se rd serde_typename) #:use-module (crates-io))

(define-public crate-serde_typename-0.1.0 (c (n "serde_typename") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)))) (h "1gdzg8kvpisz0gn25hqp4xynxm8dscrd7gc54rq2d8lfpa9gyc95")))

