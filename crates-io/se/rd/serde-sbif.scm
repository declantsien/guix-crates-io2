(define-module (crates-io se rd serde-sbif) #:use-module (crates-io))

(define-public crate-serde-sbif-0.1.0 (c (n "serde-sbif") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "peekread") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kvygi5j2bbz7hkvxzxljbazx0apnn1yg4xnki03k9q2a1jxrjs0")))

