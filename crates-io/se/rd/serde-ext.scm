(define-module (crates-io se rd serde-ext) #:use-module (crates-io))

(define-public crate-serde-ext-0.1.0 (c (n "serde-ext") (v "0.1.0") (h "0406ikywrhai1jkz800gq1iv74nb7dnj4andnq9mgmiflp3wvh3n")))

(define-public crate-serde-ext-0.1.1 (c (n "serde-ext") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-ext-macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "11hv51ywa9jh997ylvx0lgjxz2qkrlzdnbrix267kbhy1rga52ly") (f (quote (("default" "base64"))))))

