(define-module (crates-io se rd serde_type_name) #:use-module (crates-io))

(define-public crate-serde_type_name-0.1.0 (c (n "serde_type_name") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hn8jbiwsss980cxvm92i57nm638d5b6zfq637pz0d9xijgs1xry")))

(define-public crate-serde_type_name-0.2.0 (c (n "serde_type_name") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05479la47wlg762wdsypk6g4szsfc9ss1wbc9p2nf1z2db6w9b4j")))

