(define-module (crates-io se rd serde_single_or_vec) #:use-module (crates-io))

(define-public crate-serde_single_or_vec-1.0.0 (c (n "serde_single_or_vec") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("serde"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (k 2)))) (h "1jpgndibcakicji1pwz32q0a5ak9fb2yqk6gc88pgnj5dgvqhzfh") (f (quote (("std" "serde/std" "serde_json/std") ("default" "std") ("alloc" "serde/alloc" "serde_json/alloc"))))))

(define-public crate-serde_single_or_vec-1.0.1 (c (n "serde_single_or_vec") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("serde"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nd0ibxgvnfxkn0lah7prb5gq93m35vpi14rwh1w1dxl5j7c1alb") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

