(define-module (crates-io se rd serde_python) #:use-module (crates-io))

(define-public crate-serde_python-0.1.0 (c (n "serde_python") (v "0.1.0") (d (list (d (n "cpython") (r "^0.2.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_python_derive") (r "^0.1") (d #t) (k 0)))) (h "0cvh5i9rw0w6cv42jip1ilsrk7bn9nfqlqmkgs2vvc4gr8wcis4r")))

(define-public crate-serde_python-0.1.1 (c (n "serde_python") (v "0.1.1") (d (list (d (n "cpython") (r "^0.2.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_python_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1yqrq0my5s3s59rnxb3bgavaljl29wlpcsvi211cmykx4k9raivn")))

