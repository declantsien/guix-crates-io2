(define-module (crates-io se rd serde_json_path_macros) #:use-module (crates-io))

(define-public crate-serde_json_path_macros-0.1.0 (c (n "serde_json_path_macros") (v "0.1.0") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_json_path_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json_path_macros_internal") (r "^0.1.0") (d #t) (k 0)))) (h "1dsgzjgl87xk8gcn6wiwkyxj8rj2l8p1vnb4d3fkbya37fhx5a2z")))

(define-public crate-serde_json_path_macros-0.1.1 (c (n "serde_json_path_macros") (v "0.1.1") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_json_path_core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json_path_macros_internal") (r "^0.1.0") (d #t) (k 0)))) (h "1ajlrbd7fl7z0h638ij6njwdp86jwp1fnrhgifvb0yhzii62viib")))

(define-public crate-serde_json_path_macros-0.1.2 (c (n "serde_json_path_macros") (v "0.1.2") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_json_path_core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json_path_macros_internal") (r "^0.1.1") (d #t) (k 0)))) (h "1ncb67s40vhakmk5c79lnjp6blnvr1l7ms2bvw9xfa4ibg442ynh")))

(define-public crate-serde_json_path_macros-0.1.3 (c (n "serde_json_path_macros") (v "0.1.3") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_json_path_core") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json_path_macros_internal") (r "^0.1.1") (d #t) (k 0)))) (h "1sm44x353cxz0784n14yqm8zk4x7ysi270rxp2v2bqsl8x7l6rc5")))

(define-public crate-serde_json_path_macros-0.1.4 (c (n "serde_json_path_macros") (v "0.1.4") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_json_path_core") (r "^0.1.6") (d #t) (k 0)) (d (n "serde_json_path_macros_internal") (r "^0.1.1") (d #t) (k 0)))) (h "08xpphpj0pcckgkwbm76asvdz4bqmr32kw8p97lx6gs4g8byhc9a")))

