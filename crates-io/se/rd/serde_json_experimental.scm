(define-module (crates-io se rd serde_json_experimental) #:use-module (crates-io))

(define-public crate-serde_json_experimental-1.0.29-rc1 (c (n "serde_json_experimental") (v "1.0.29-rc1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0l980l9rg7xjmqbsrjv2qf8c02bj7k1dqr3kd3j9lpxvbhscwg6h") (f (quote (("raw_value") ("preserve_order" "indexmap") ("default") ("arbitrary_precision")))) (y #t)))

