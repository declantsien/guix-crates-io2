(define-module (crates-io se rd serde_tokenstream) #:use-module (crates-io))

(define-public crate-serde_tokenstream-0.1.0 (c (n "serde_tokenstream") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19dafvzbp13j9a51zvapzqm8phi0dh6x533072h7x7prxllgs68k")))

(define-public crate-serde_tokenstream-0.1.1 (c (n "serde_tokenstream") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1vivqnc79dgxswnnzyib1b4d1111xh3bvn9zigv6wmm01fqwrcz8")))

(define-public crate-serde_tokenstream-0.1.2 (c (n "serde_tokenstream") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.32") (d #t) (k 2)))) (h "1axhz8jgackzc3pwaaagdsmcbym9as6v53amdxjjm97vax9fjg0c")))

(define-public crate-serde_tokenstream-0.1.3 (c (n "serde_tokenstream") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0gb8kw9dw77k8yqsn0pma8b754cd2s81s48hh51q2pjk79fb3pnn")))

(define-public crate-serde_tokenstream-0.1.4 (c (n "serde_tokenstream") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vvidjp211kh91as869m0h177f34bfsnsvw104q353zx4cmxx06d")))

(define-public crate-serde_tokenstream-0.1.5 (c (n "serde_tokenstream") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17b3mihg190w1cxzqjqnw1mk1y7shrfrgn9h5i1cma3r06xg1517")))

(define-public crate-serde_tokenstream-0.1.6 (c (n "serde_tokenstream") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zyya6ir61zgh2lp1f52k0mdbjdjg8qb9rfdrdkix828cwnm2kr7")))

(define-public crate-serde_tokenstream-0.1.7 (c (n "serde_tokenstream") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1sflql03j268sxydzd4xfihsa9aq5v8vb2n6mbrn9clr0bca2yvr")))

(define-public crate-serde_tokenstream-0.2.0 (c (n "serde_tokenstream") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "07v2q4fhbvj9w0kn1s18hqr9lcp8klmaxjlzy2bd10nq7z9gy04a")))

