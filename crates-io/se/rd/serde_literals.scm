(define-module (crates-io se rd serde_literals) #:use-module (crates-io))

(define-public crate-serde_literals-0.1.0 (c (n "serde_literals") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)))) (h "0ssy51qs7jakxlm4v53kk3irfqwmpplrzaral8s56qv8n3sbizvw")))

(define-public crate-serde_literals-0.1.1 (c (n "serde_literals") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)))) (h "1w9n32d99mlmviyfmdman353v3q90qh88zmd50hkyvid4qvi33vx")))

