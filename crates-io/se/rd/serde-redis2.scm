(define-module (crates-io se rd serde-redis2) #:use-module (crates-io))

(define-public crate-serde-redis2-0.12.0 (c (n "serde-redis2") (v "0.12.0") (d (list (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ya0kki499i0g14va7n742famgnc9vhyywj9hng3gvrp2c7gmf18") (y #t)))

