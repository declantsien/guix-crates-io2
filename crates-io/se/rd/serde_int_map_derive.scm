(define-module (crates-io se rd serde_int_map_derive) #:use-module (crates-io))

(define-public crate-serde_int_map_derive-0.1.0 (c (n "serde_int_map_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_int_map") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f3akd7i2bcxyi1375p4mxgcb1pgm5wgm6wg0m83cf34hq1s8ymp") (f (quote (("print_tokenstreams"))))))

(define-public crate-serde_int_map_derive-0.1.1 (c (n "serde_int_map_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_int_map") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nl8rzjfnghl20dyy8p0czy9xfnyf4sp9b22fmij4ag5jdbm2rrs") (f (quote (("print_tokenstreams"))))))

(define-public crate-serde_int_map_derive-0.1.2 (c (n "serde_int_map_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_int_map") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "185p790micm7zs228qpihyk2cffbri4kj4m8wz4p3d97sw0f1jz2") (f (quote (("print_tokenstreams"))))))

(define-public crate-serde_int_map_derive-0.2.0 (c (n "serde_int_map_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_int_map_traits") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1csfjz9ixzqncmqj35ql06g6y3s4gbwafzimf3gzp755a9pp4q8a") (f (quote (("print_tokenstreams"))))))

(define-public crate-serde_int_map_derive-0.3.0 (c (n "serde_int_map_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_int_map_traits") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hw0hjss5vjhpglaml6sm835pd2ijjx864cq0jykx9yj3na8vhvx") (f (quote (("print_tokenstreams"))))))

