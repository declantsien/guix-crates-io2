(define-module (crates-io se rd serde_graphql_input) #:use-module (crates-io))

(define-public crate-serde_graphql_input-0.1.0 (c (n "serde_graphql_input") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "02vy9085nr3vfrc3i2acak4kbb8vnsf1xx15k2mm7pzyp7bjqsm6")))

(define-public crate-serde_graphql_input-0.1.1 (c (n "serde_graphql_input") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "itoa") (r "^1.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0f6nbsvk7vyv6i3ram5c87krziczmwqv2vpalqhgic35xvsgvvbl")))

