(define-module (crates-io se rd serde-big-array) #:use-module (crates-io))

(define-public crate-serde-big-array-0.1.0 (c (n "serde-big-array") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wjzfnkigvznxlx6si901wj6kydighm1x6ccwbqmi4x6pa2a7nc0")))

(define-public crate-serde-big-array-0.1.1 (c (n "serde-big-array") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "155r01x43ycfp62crmg76rsmncd5rvfh62g73w9yrcy0hkb8x5zr")))

(define-public crate-serde-big-array-0.1.2 (c (n "serde-big-array") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l6yvd7zph102n2j1r5hxpxn3a9sy4gb9pqcwnlmqhqca8phmi7a")))

(define-public crate-serde-big-array-0.1.3 (c (n "serde-big-array") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dn56pssd2kvqnmms5ixy394yl1ljd0nw4z8k53z29bc1wkp6i6n")))

(define-public crate-serde-big-array-0.1.4 (c (n "serde-big-array") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jirawfds8b1ahz2d9p0vfjciaav9hrr5aijjsn7s073k8j2kmnv")))

(define-public crate-serde-big-array-0.1.5 (c (n "serde-big-array") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gkyqxk760mp1lfcg6lhjk95ajc89nr0qdd0vl4ic0g8pyxcy9mr")))

(define-public crate-serde-big-array-0.2.0 (c (n "serde-big-array") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kj0h99y7ma9nsayv87fj2n680bcrwv2mrcbmc774lgak18ywgl8") (f (quote (("const-generics"))))))

(define-public crate-serde-big-array-0.3.0 (c (n "serde-big-array") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mfwh5pw50354ixcxdyg7gpzy1z36y8chg7ppic8w9db69wryc2j") (f (quote (("const-generics"))))))

(define-public crate-serde-big-array-0.3.1 (c (n "serde-big-array") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1591qrwfpgxdcyz511bc2si6axkhc6bz4hnd943k1sihba2r3xql") (f (quote (("const-generics"))))))

(define-public crate-serde-big-array-0.3.2 (c (n "serde-big-array") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qpy3nk3dpxrrmcfmcnsijad10yi0jxy1x6gc6bbwywma9vhxchq") (f (quote (("const-generics"))))))

(define-public crate-serde-big-array-0.4.0 (c (n "serde-big-array") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "106wk4bysdyifsy6m7z99w1azbk1gfalgj1v5ysgp9z9arl0m281") (y #t)))

(define-public crate-serde-big-array-0.3.3 (c (n "serde-big-array") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "190hrlbilvarn5almh3n2s4di9qagxnz3chv6xaw1c72dygzacfd") (f (quote (("const-generics"))))))

(define-public crate-serde-big-array-0.4.1 (c (n "serde-big-array") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rwhbrffdxy87bxbyx8p68cg30gf0dlflx14vk1qiwlafjdg08rk")))

(define-public crate-serde-big-array-0.5.0 (c (n "serde-big-array") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d50knh2qwnynk8lfplkgx6vpalqifvang4lphfc50a4mzhndfzi")))

(define-public crate-serde-big-array-0.5.1 (c (n "serde-big-array") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zsb9s9rcca3408kg20c6xpx917c9vbbnap5gvrf0wvdqz17rz0i")))

