(define-module (crates-io se rd serde-strings) #:use-module (crates-io))

(define-public crate-serde-strings-0.1.0 (c (n "serde-strings") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jd4xplqs8ncb4rsw7hl0wg9zfg2arzpi1biayac6myy4v0qkwcq")))

