(define-module (crates-io se rd serde_dhall_typegen) #:use-module (crates-io))

(define-public crate-serde_dhall_typegen-0.1.0 (c (n "serde_dhall_typegen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.56") (d #t) (k 2)))) (h "0vc5l6f2g8644zi5r860fj83whm9x8v9fg1gi627nqyhy6lrqhxv")))

