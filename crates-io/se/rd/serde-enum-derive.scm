(define-module (crates-io se rd serde-enum-derive) #:use-module (crates-io))

(define-public crate-serde-enum-derive-0.0.1 (c (n "serde-enum-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0g9d5wdhcs6rji0wa96gh6lybhgfipk8x9bmv4avxmv6qw1r0yaz")))

(define-public crate-serde-enum-derive-0.0.2 (c (n "serde-enum-derive") (v "0.0.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0jbi83bn5k26j17inxw6zmnf5izsldvjnrll8hil34kjdb5nmlfh")))

