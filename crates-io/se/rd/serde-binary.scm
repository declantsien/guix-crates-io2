(define-module (crates-io se rd serde-binary) #:use-module (crates-io))

(define-public crate-serde-binary-0.1.0 (c (n "serde-binary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lllbhnn8qzdxv6vd3ch05j3bahz8jgzib28k1p2kzbw7k745lbs")))

(define-public crate-serde-binary-0.2.0 (c (n "serde-binary") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nczkyz4fic2y0xm0cq0c5ggrgm6w8b5f1b7gr4f741l590picnq")))

(define-public crate-serde-binary-0.2.1 (c (n "serde-binary") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03p7ip0q0wny6c7akw5dxiw5nby5bkdvw5fadp72jh2399r2zqx2")))

(define-public crate-serde-binary-0.2.2 (c (n "serde-binary") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zkz2p8mz54yz6gay9dklzzjix5ph2n4xvx7wwmjfk2i1hc8bv92")))

(define-public crate-serde-binary-0.3.0 (c (n "serde-binary") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03llfiz5gy5m3pq92825w73vhrfb0za11v9j2jl7587hh1s5vmcl")))

(define-public crate-serde-binary-0.3.1 (c (n "serde-binary") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (f (quote ("wasm32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g492fyyiw0gqvqmcprf7dmdk3vnlk123bfas7vym3pjkclhpdnh")))

(define-public crate-serde-binary-0.3.2 (c (n "serde-binary") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2") (f (quote ("wasm32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "048wchpjrdznxfjgr4hqj5wha2x1axplf59jj1zwg8v3sfn9a760")))

(define-public crate-serde-binary-0.3.3 (c (n "serde-binary") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^2.0.3") (f (quote ("wasm32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ipja2f66a92cjs2y49fhf6nqwkvjwsm90y77hg7vr5ivdb1w3p6")))

(define-public crate-serde-binary-0.3.6 (c (n "serde-binary") (v "0.3.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary_rw") (r "^3.0.0") (f (quote ("wasm32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1n4lmf18bj51a03a3mmyv2p14hlh1vwlf60zky6d1p2yldi3wr21")))

(define-public crate-serde-binary-0.4.0 (c (n "serde-binary") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary-stream") (r "^3.0.0") (f (quote ("wasm32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fgdcv6xyb451dbbilv4wdnrk77qkilk963l6vy89fdbjdh6jgaz")))

(define-public crate-serde-binary-0.5.0 (c (n "serde-binary") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "binary-stream") (r "^3.3.2") (f (quote ("32bit"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01h5jj3a50bpnh7y51n0qamqbdj1fkyqp23gyyj57vc3gd0dnl5m")))

