(define-module (crates-io se rd serde-json-fmt) #:use-module (crates-io))

(define-public crate-serde-json-fmt-0.1.0 (c (n "serde-json-fmt") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (k 2)) (d (n "serde") (r "^1.0.160") (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)))) (h "06g4nniaz35iq664q6fsb8paz2y483frlcwr0196v8jjbxx3p8x4") (r "1.60")))

