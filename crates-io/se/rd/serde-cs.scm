(define-module (crates-io se rd serde-cs) #:use-module (crates-io))

(define-public crate-serde-cs-0.1.0 (c (n "serde-cs") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bcnd3wlrrm4185k0vc0v7b4liz170fp81kh70dqnf4fan6i86vp")))

(define-public crate-serde-cs-0.2.0 (c (n "serde-cs") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1kglw5fjw0b2n2375wcxgiw3dmaahji49ma1v469fx4rr3g57df4")))

(define-public crate-serde-cs-0.2.1 (c (n "serde-cs") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0f4yz82nbiv7dzzynmhzn4wh802iwj0v3jwsid392nh8rzvgkwl3")))

(define-public crate-serde-cs-0.2.2 (c (n "serde-cs") (v "0.2.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bbjrvxli5jp6bj2mp7si3ggpg1l68lb3gjspqgpcfcibi1v1m8q")))

(define-public crate-serde-cs-0.2.3 (c (n "serde-cs") (v "0.2.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09d9ck6cm39cjp0mspppcib5nqhz3y5gz417jmsd4ql7yprwj0l2")))

(define-public crate-serde-cs-0.2.4 (c (n "serde-cs") (v "0.2.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jap0njgxwf90wsaxb9a8dslp5h1vjp6hc8z5krjzzpdcmi73l87")))

