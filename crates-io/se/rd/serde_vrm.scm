(define-module (crates-io se rd serde_vrm) #:use-module (crates-io))

(define-public crate-serde_vrm-0.0.9 (c (n "serde_vrm") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qda69gq56ask32bfj960g3w3rbm7sf4l429anrh7sc46lgriw20")))

(define-public crate-serde_vrm-0.0.10 (c (n "serde_vrm") (v "0.0.10") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ja5ix9bbab8qw4yb28g7jl83z7m5k5zq1xvkrw4z9hifwy3a4p")))

