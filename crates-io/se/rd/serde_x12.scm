(define-module (crates-io se rd serde_x12) #:use-module (crates-io))

(define-public crate-serde_x12-0.1.0 (c (n "serde_x12") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vgd957ii2b07mz3d9idgqadgq6dvj72m8wcvl82mkvmdzi7ppv2")))

(define-public crate-serde_x12-0.2.0 (c (n "serde_x12") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ykrgyw684gbmi96a8rpinkyxa9zll66s32n4xgf3kyfn8nbzc2p") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.3.0 (c (n "serde_x12") (v "0.3.0") (d (list (d (n "json_dotpath") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nnlyy8d9a7lrf6szf6sfxqvfgzinkxag1hk4ysps2gxxa9lh3wf") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.4.0 (c (n "serde_x12") (v "0.4.0") (d (list (d (n "json_dotpath") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1z5psygxbaj2iba91plb52rw6nfkw2032iy4rg7h0zs90rxv6rqy") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.5.0 (c (n "serde_x12") (v "0.5.0") (d (list (d (n "json_dotpath") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vbhkpgdnsjgy0zsnvxbv0cy1z3dghxrk1svpxwpp81i22p7zm4g") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.6.0 (c (n "serde_x12") (v "0.6.0") (d (list (d (n "json_dotpath") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03mz84vna12y5zsjygdicv5l4h9f38yj7y7ir32zfzd64las2jsa") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.7.0 (c (n "serde_x12") (v "0.7.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12nnhfra4bhd1xccw6z8rgvklnrkmdrb93biav2ln1iwk12bnc7y") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.7.1 (c (n "serde_x12") (v "0.7.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x12-types") (r "^0.7") (d #t) (k 0)))) (h "0w1hgs8g1ja0jnprn5fdwk8g4x1l9drvx03c210hayklh13r2a1z") (f (quote (("debug"))))))

(define-public crate-serde_x12-0.7.2 (c (n "serde_x12") (v "0.7.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "x12-types") (r "^0.8") (d #t) (k 0)))) (h "16w42rffmrv1a3samz2ykagy4xij06zr8vck0m3jd0l3zv6jln7z") (f (quote (("debug"))))))

