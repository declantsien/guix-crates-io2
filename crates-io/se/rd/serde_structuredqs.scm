(define-module (crates-io se rd serde_structuredqs) #:use-module (crates-io))

(define-public crate-serde_structuredqs-0.1.0 (c (n "serde_structuredqs") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rhk6yb966y4s1g2xckx7yk2q3rvmpyh4k5i7miy1hh2rvb2sf9l")))

(define-public crate-serde_structuredqs-0.1.1 (c (n "serde_structuredqs") (v "0.1.1") (d (list (d (n "form_urlencoded") (r "^1.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kbnagn20n2yi8zgpyn783zxsfjfkjpviqdjkghq3sxgkd7vq8fm")))

(define-public crate-serde_structuredqs-0.2.0 (c (n "serde_structuredqs") (v "0.2.0") (d (list (d (n "form_urlencoded") (r "^1.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0hy4p95idi1m9g3if401xdaidz77cbldxrwiwpwmivyczn34k714")))

