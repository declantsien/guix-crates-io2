(define-module (crates-io se rd serde08) #:use-module (crates-io))

(define-public crate-serde08-0.8.0 (c (n "serde08") (v "0.8.0") (d (list (d (n "serde") (r "^0.8") (k 0)))) (h "179sfzjm09ymqy16vi60h40hb8x01ci4pgg8hcc885a4gaxql9i2") (f (quote (("unstable-testing" "serde/unstable-testing") ("unstable" "serde/unstable") ("std" "serde/std") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

