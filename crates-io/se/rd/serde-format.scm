(define-module (crates-io se rd serde-format) #:use-module (crates-io))

(define-public crate-serde-format-0.1.0 (c (n "serde-format") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0icpr3mqvzhbdw8lkqc22aclvcd0wz0dg57ww02hvwyv5qd78gy0")))

(define-public crate-serde-format-0.2.0 (c (n "serde-format") (v "0.2.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m7lmvdmxd7daxqhn9dpz28g8by1abn4wkb61vcfga27w8jy4kla")))

