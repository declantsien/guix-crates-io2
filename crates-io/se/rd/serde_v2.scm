(define-module (crates-io se rd serde_v2) #:use-module (crates-io))

(define-public crate-serde_v2-0.1.0 (c (n "serde_v2") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wk97w0piqyyjbsn7nxkds1vmsp473wzpdqri8042jydfm4igys9")))

