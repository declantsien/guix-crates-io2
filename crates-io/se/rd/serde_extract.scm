(define-module (crates-io se rd serde_extract) #:use-module (crates-io))

(define-public crate-serde_extract-0.1.0 (c (n "serde_extract") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_serializer_quick_unsupported") (r "^0.1.2") (d #t) (k 0)))) (h "0l4c8dlf7iw8al1i995m7gn7ai0kykxfjazcr8zwblpl53mcjj3c")))

