(define-module (crates-io se rd serde-odbc) #:use-module (crates-io))

(define-public crate-serde-odbc-0.1.0 (c (n "serde-odbc") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "odbc-sys") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1n2699mfg59lwmp3sgw59n50v4j1zcmhgb8gqlp6bf11901zndb1")))

(define-public crate-serde-odbc-0.1.1 (c (n "serde-odbc") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "odbc-sys") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0ap9zwdwnb6ycm89214jwiv75ggw93k0zjj5c48qsxlpmwjk6sgg")))

(define-public crate-serde-odbc-0.1.2 (c (n "serde-odbc") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "odbc-sys") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1kwfzhahxka0bdbbbajm8b5kg1z0j70zxw60sl25s4674bbyws3w")))

(define-public crate-serde-odbc-0.1.3 (c (n "serde-odbc") (v "0.1.3") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "odbc-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1sndyd9yc2c2881761z4byc3pvzvfzhr2m2lnwwlvmmpvz0sfj7q")))

(define-public crate-serde-odbc-0.1.4 (c (n "serde-odbc") (v "0.1.4") (d (list (d (n "actix-web") (r "^0.7") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "odbc-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1d3c6qbgrq3fcjycb45gz68dpas8whzw99f4syb2djkk6fza7mb4")))

(define-public crate-serde-odbc-0.2.0 (c (n "serde-odbc") (v "0.2.0") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "odbc-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dgq1y81h9sl2w8z3jd5h2ia9hcailp0jrd2zqxdy9wjfynkj98m")))

(define-public crate-serde-odbc-0.3.0 (c (n "serde-odbc") (v "0.3.0") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "odbc-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01l85083k2c71xa0zinbcf3jabg2bh95r5has571y3gaj3l96440")))

(define-public crate-serde-odbc-0.3.1 (c (n "serde-odbc") (v "0.3.1") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "odbc-sys") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivn2zry2w66449bzzscrrd4y7q9fq0pcis5xagp4z0z3yi10xgd")))

