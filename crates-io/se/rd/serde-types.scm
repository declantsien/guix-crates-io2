(define-module (crates-io se rd serde-types) #:use-module (crates-io))

(define-public crate-serde-types-0.0.0 (c (n "serde-types") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "0ajm63jlqmacvilxjqkk3jmh11pdss42gxayxykvg6y1c0a3vgw0") (f (quote (("default") ("csharp"))))))

(define-public crate-serde-types-0.1.0 (c (n "serde-types") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 2)))) (h "0ck4hh6v6dihrh63jf2yqjz4fqzg4bf03yc0cc0fqmildpzd6lzg") (f (quote (("default") ("csharp"))))))

(define-public crate-serde-types-0.1.1 (c (n "serde-types") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 2)))) (h "0br18h08rnk6h7a1sxbmz293rh01nr9934xkp37yk3f52h0v7ig7") (f (quote (("default") ("csharp"))))))

