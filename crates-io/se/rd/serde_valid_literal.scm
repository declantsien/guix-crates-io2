(define-module (crates-io se rd serde_valid_literal) #:use-module (crates-io))

(define-public crate-serde_valid_literal-0.4.0 (c (n "serde_valid_literal") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1s7lhx93mw7y46sqma6nbda75aw78xsv9yzl6bzmw8pa752rz84h")))

(define-public crate-serde_valid_literal-0.5.0 (c (n "serde_valid_literal") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "19ks7p3923x483yz9p7b2imwcsdca5nwwb5bk3flsc7v3wgzwg9d")))

(define-public crate-serde_valid_literal-0.5.1 (c (n "serde_valid_literal") (v "0.5.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1rlbh75jjf4f7lxn383276l2k6xnz9lqdw7x22s0jpcw5i4sbm57")))

(define-public crate-serde_valid_literal-0.5.2 (c (n "serde_valid_literal") (v "0.5.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "08imcc4b9dkkac0vfxdwmqiwvbwy9a7ljhdq68fa84an2wkj1yp6")))

(define-public crate-serde_valid_literal-0.5.3 (c (n "serde_valid_literal") (v "0.5.3") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0lv9540yfwh8cxi6qvcg25bg3d4j8pmj9i3yqxcn4kkfv8345vh4")))

(define-public crate-serde_valid_literal-0.5.4 (c (n "serde_valid_literal") (v "0.5.4") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "078935315jzaz57g5sqmk28qgaz2zqmzq7kaif298mmxkxazc88s")))

(define-public crate-serde_valid_literal-0.6.0 (c (n "serde_valid_literal") (v "0.6.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "11dp50kcpf3npnsd85hpvl8yj44bbl3b466nz5hh64f7spx472qq")))

(define-public crate-serde_valid_literal-0.6.1 (c (n "serde_valid_literal") (v "0.6.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "18vff36rhazjcllm89dhi8b3hvp8qdmy7znl922zs65zfdh04ir3")))

(define-public crate-serde_valid_literal-0.6.2 (c (n "serde_valid_literal") (v "0.6.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0hzzppa760v138lk9y5mhgmi61fnmrz5incl76jbbjdrq753hn6z")))

(define-public crate-serde_valid_literal-0.7.0 (c (n "serde_valid_literal") (v "0.7.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0spahjwpybxcn8jr4rwilrbq7yih2246xxiyxnnaqdy8isczg85c")))

(define-public crate-serde_valid_literal-0.8.0 (c (n "serde_valid_literal") (v "0.8.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1y2kfvc5pyq1dl8vi1bm17ffg6kkknbnc2nmbkjda7kbbyvxq43f")))

(define-public crate-serde_valid_literal-0.9.0 (c (n "serde_valid_literal") (v "0.9.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1vz9axrrb8ix9vy76gvd2gnsjrcj58v12n15yrmn17b03f2ip2ig")))

(define-public crate-serde_valid_literal-0.10.0 (c (n "serde_valid_literal") (v "0.10.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0y44zph2qf7lv042iz3v5pjw60w05nagfjf0jvykl00i80qbmdyd")))

(define-public crate-serde_valid_literal-0.10.1 (c (n "serde_valid_literal") (v "0.10.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0qirs1g1v34fzyxmw8jnqjvm9nywr6pv7xs7pd9svl29ia2rw6nc")))

(define-public crate-serde_valid_literal-0.11.0 (c (n "serde_valid_literal") (v "0.11.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1n7z61f62p4vwcp5x8s3ysgh37r5jvr928wdpjllqxlnyvv5a4ff")))

(define-public crate-serde_valid_literal-0.11.1 (c (n "serde_valid_literal") (v "0.11.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1r5n34m6jn6qbxvdkaj42i9d60fz9iphi3d6iyq5i769z9y7pz6c")))

(define-public crate-serde_valid_literal-0.11.2 (c (n "serde_valid_literal") (v "0.11.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0ngd033vqk4mw9gf1ys6nl2n4glpcyylz7z82dj0mqg1h66zk4mh")))

(define-public crate-serde_valid_literal-0.11.3 (c (n "serde_valid_literal") (v "0.11.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "130d60lgcr7yypfp0iswskcma7w6qmv4b2h5c5fd6c3rd3707y58")))

(define-public crate-serde_valid_literal-0.12.0-rc.1 (c (n "serde_valid_literal") (v "0.12.0-rc.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0jlzgy4gdny9lvv10vym815bnhzlpb2dzf1svd52yg3qdvdnlbl4")))

(define-public crate-serde_valid_literal-0.12.0-beta.1 (c (n "serde_valid_literal") (v "0.12.0-beta.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "12d36k4mx48b5zkcpir0mgxp47ld1s29zrl2klixzxbv2cjqipgh")))

(define-public crate-serde_valid_literal-0.12.0-beta.2 (c (n "serde_valid_literal") (v "0.12.0-beta.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1074fydacf95dj0vi7jhmkl011kqsmm77nixf62pqdswky5091v7")))

(define-public crate-serde_valid_literal-0.12.0-beta.3 (c (n "serde_valid_literal") (v "0.12.0-beta.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0n15vm70j2bliwxf4xbzq57vvwbbipcs0lzsjk5sv5nl715f308a")))

(define-public crate-serde_valid_literal-0.12.0-beta.4 (c (n "serde_valid_literal") (v "0.12.0-beta.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "09ym23p9gv81snfamrb044hfji0w5dd38q97pl3zi9rwck8a186x")))

(define-public crate-serde_valid_literal-0.12.0-beta.5 (c (n "serde_valid_literal") (v "0.12.0-beta.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0ci9271h52jss8yif4fjpgj8d18nb4izks7ml4ld7sngkgy0bafr")))

(define-public crate-serde_valid_literal-0.12.0-beta.6 (c (n "serde_valid_literal") (v "0.12.0-beta.6") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0wifjlmr5f619lqcbm8k1y0y7qwhiy8h1fq4pdn22a1gcng71zzq")))

(define-public crate-serde_valid_literal-0.12.0-beta.7 (c (n "serde_valid_literal") (v "0.12.0-beta.7") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "12idy123zjq6jfdjabqlp968h9g5j239pp2k3n34yizl6mi2vpdb")))

(define-public crate-serde_valid_literal-0.12.0-beta.8 (c (n "serde_valid_literal") (v "0.12.0-beta.8") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1nl1850axclibdr9mjmm2rlrx5056d2w9pwxvpsd899skp517b73")))

(define-public crate-serde_valid_literal-0.12.0-beta.9 (c (n "serde_valid_literal") (v "0.12.0-beta.9") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1qcnllhzdc72ym5b5kxivlvhz79dx3rjpmbl95xkh4v3xxz7yp8j")))

(define-public crate-serde_valid_literal-0.12.0-beta.10 (c (n "serde_valid_literal") (v "0.12.0-beta.10") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "09ika4jkz1drkxcmq75x7d4nr2y3krkhma4hq85qks1xmqrpdc31")))

(define-public crate-serde_valid_literal-0.12.0 (c (n "serde_valid_literal") (v "0.12.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0ci5l64c4pcp3x5cpgxlqmm8z1l1453hvjry4bma55hg310j2xj6")))

(define-public crate-serde_valid_literal-0.13.0 (c (n "serde_valid_literal") (v "0.13.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1w2skxh8w2yyfa0brphwzgnaa5b3icnzh901bkgyvqaar64w90ii")))

(define-public crate-serde_valid_literal-0.14.0 (c (n "serde_valid_literal") (v "0.14.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0jm29sj7dyng7iplxgchcljdsr3awnz0pga6mfzv3vcc49im38rp")))

(define-public crate-serde_valid_literal-0.15.0 (c (n "serde_valid_literal") (v "0.15.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0m0gil52jfhkg46h0yg6g3j8x70342mygv3hrp2klfipzi6gl6n5") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.16.0-alpha (c (n "serde_valid_literal") (v "0.16.0-alpha") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0bgj69daxkmydhm3fk6dn9rgpmk0jczcx2dl8jk1q7rjp2cmcvgd") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.16.0 (c (n "serde_valid_literal") (v "0.16.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "17qd3k19mhf95xvhwh214svhdgd2qnrmn9hrd83svk51b4k8lb9p") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.16.1 (c (n "serde_valid_literal") (v "0.16.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1z9fh4g9ymqlp194m448zk79pirdc6izyxw5gs3alzg5h64l44dd") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.16.2 (c (n "serde_valid_literal") (v "0.16.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "17f9vvjdli8bvzjgm81dknj1yzvcf28yhr2wp0k8nb0id8q0drzx") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.16.3 (c (n "serde_valid_literal") (v "0.16.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1d1npsds4634lij3wjciran222yaxxkpmz4l0zwlji6nn69g4zgm") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.17.0 (c (n "serde_valid_literal") (v "0.17.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1c21ixmq2xqgk3n7j0jqdsb8cp4qxp8c18blcighadwn6ykir0dy") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.18.0 (c (n "serde_valid_literal") (v "0.18.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0bni2mcvf6nls7p1gvcx7qk86rfrrd6yrwmcxrajnnk064dlzvdc") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.19.0 (c (n "serde_valid_literal") (v "0.19.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "04zbcs48zsgp197qqsr71zl3l3wmlypn8dirm8cmd8f27xhd7sif") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.20.0 (c (n "serde_valid_literal") (v "0.20.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0gx5n5c8qyy14rdfp4v0r9pmn2i4iyiv0087yfasajlahv0mm57l") (f (quote (("i128") ("default"))))))

(define-public crate-serde_valid_literal-0.21.0 (c (n "serde_valid_literal") (v "0.21.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0crb2hqkl0mxnbx27j3kbizjdfrlz7aangs149yqxjmk5q42xglj") (f (quote (("i128") ("default"))))))

