(define-module (crates-io se rd serde-value) #:use-module (crates-io))

(define-public crate-serde-value-0.0.1 (c (n "serde-value") (v "0.0.1") (d (list (d (n "ordered-float") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "0d29m8iwj0nlna11cn5nw2dyff6i5q80n0skvbgnnh6b38xj0w3x")))

(define-public crate-serde-value-0.0.2 (c (n "serde-value") (v "0.0.2") (d (list (d (n "ordered-float") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "0jvxlhjp8xw0knbzcybq5hgmj5ws492v7gswvcrnw377vnhp5f3x")))

(define-public crate-serde-value-0.1.0 (c (n "serde-value") (v "0.1.0") (d (list (d (n "ordered-float") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)))) (h "1s7x9qyl0y84anzrv5amr424niw7j1gz49psqvhnb8ddlghm04wc")))

(define-public crate-serde-value-0.2.0 (c (n "serde-value") (v "0.2.0") (d (list (d (n "ordered-float") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)))) (h "0r0r54pg256qs5zn7lmpnb1nr8pg71l34wsmcjq7zdmvbm3493s0")))

(define-public crate-serde-value-0.2.1 (c (n "serde-value") (v "0.2.1") (d (list (d (n "ordered-float") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)))) (h "0350na6gxza8axpgdg6c3ndbwwpki6vly0mapqcaynp0qv37ch6r")))

(define-public crate-serde-value-0.3.0 (c (n "serde-value") (v "0.3.0") (d (list (d (n "ordered-float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)))) (h "1qv5piqr09raz8ckk766rv32yp6ly73d9vlgfbjkmhn21khmkvn1")))

(define-public crate-serde-value-0.3.1 (c (n "serde-value") (v "0.3.1") (d (list (d (n "ordered-float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)))) (h "1ggynky3q7xa36w1glnapmcgdpd5q4dm5ykhr9a7vwr42xnia75z")))

(define-public crate-serde-value-0.4.0 (c (n "serde-value") (v "0.4.0") (d (list (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.0") (d #t) (k 0)))) (h "175rigwaap0ksq0b56qgb4vjig4511ngbf3gj8gg2nshfkqkcpdw")))

(define-public crate-serde-value-0.4.1 (c (n "serde-value") (v "0.4.1") (d (list (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.9") (d #t) (k 2)))) (h "1368isy32xyamfx4kdsrhb22bic1c4nw6wzr0kwjp7rdcisp34rn")))

(define-public crate-serde-value-0.5.0 (c (n "serde-value") (v "0.5.0") (d (list (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0c7cayrrcb6r2j38l8wxv8n3f7qzip62g4hiniqfhk1gpilyk8mf")))

(define-public crate-serde-value-0.5.1 (c (n "serde-value") (v "0.5.1") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0v9m0zrf00yxaibsj5jgfbbi18lv94b9sx2gsrw5qi0r13wpq63i")))

(define-public crate-serde-value-0.5.2 (c (n "serde-value") (v "0.5.2") (d (list (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "03gv25qjkfxsm1j7kp9xdqjbzki6iwk6m9ip14dddjwh4bg3m42j")))

(define-public crate-serde-value-0.5.3 (c (n "serde-value") (v "0.5.3") (d (list (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0kz4i4a2if0la082ww64bbf38b0b1nycdm2rlp0ymi7d7n3kyrks")))

(define-public crate-serde-value-0.6.0 (c (n "serde-value") (v "0.6.0") (d (list (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1swh6870pr1cxr6ha769rv4wdnyfxdvsc42cmvf8lmla38lsfras")))

(define-public crate-serde-value-0.7.0 (c (n "serde-value") (v "0.7.0") (d (list (d (n "ordered-float") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "0b18ngk7n4f9zmwsfdkhgsp31192smzyl5z143qmx1qi28sa78gk")))

