(define-module (crates-io se rd serde-toml-merge) #:use-module (crates-io))

(define-public crate-serde-toml-merge-0.1.0 (c (n "serde-toml-merge") (v "0.1.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15wpqgv1s5kc5ilrfq0r494rr3f2zz274dbh076xd343hqcifqiq")))

(define-public crate-serde-toml-merge-0.2.0 (c (n "serde-toml-merge") (v "0.2.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hl3bq1syckqdp34skkh57ph31xjmzacpzc0c7ypqrpbprggyggb")))

(define-public crate-serde-toml-merge-0.3.0 (c (n "serde-toml-merge") (v "0.3.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19c2divss2ybmwysi8wjz4zkkrmv5aidimrxjsg8h06s8hjs4hnq")))

(define-public crate-serde-toml-merge-0.3.1 (c (n "serde-toml-merge") (v "0.3.1") (d (list (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1gz3dmz8p43dviyp1d5j4mxxpjq9i3i4nf6im720php5a2sp5057")))

(define-public crate-serde-toml-merge-0.3.2 (c (n "serde-toml-merge") (v "0.3.2") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0j7np8kr72rz6lhkilh1sf160dhdqpkhjjflgy3rl9qw9jahh315")))

(define-public crate-serde-toml-merge-0.3.3 (c (n "serde-toml-merge") (v "0.3.3") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0k16vwqfsb3fj6dhnpfawq96afqxbkbszm75i84hpmhn5ksfanmg")))

(define-public crate-serde-toml-merge-0.3.4 (c (n "serde-toml-merge") (v "0.3.4") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "09pvxpa130khjvxx7j5c00ah3l7mwf293z2cmlwdi6zj9zbl8qpw")))

(define-public crate-serde-toml-merge-0.3.5 (c (n "serde-toml-merge") (v "0.3.5") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0zyja7rl6mlv2914snzr6vsxb2n5czs9q2srswcvradfh6w16wii")))

(define-public crate-serde-toml-merge-0.3.6 (c (n "serde-toml-merge") (v "0.3.6") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "13vr40djk0nxsmz3q30c7hk79ijj6lj8ihdiaha3110kn1smw1w8")))

(define-public crate-serde-toml-merge-0.3.7 (c (n "serde-toml-merge") (v "0.3.7") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "09m24wm4n6xnj3jl5i20865mlhrw97xi6kms60fic5bjaxn6nnp7")))

