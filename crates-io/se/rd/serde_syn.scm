(define-module (crates-io se rd serde_syn) #:use-module (crates-io))

(define-public crate-serde_syn-0.1.0 (c (n "serde_syn") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0718b2qhxv456657qxn8152qx66y9v2z1xsr9396jb6hrfnsh30f")))

