(define-module (crates-io se rd serde-versions-derive) #:use-module (crates-io))

(define-public crate-serde-versions-derive-0.0.1 (c (n "serde-versions-derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16f6s7iflk2lrjjk9mfvif6znf3h3bh6hb81r2kmpn63mg7hq85y")))

(define-public crate-serde-versions-derive-0.0.2 (c (n "serde-versions-derive") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zrgvbri8wrasr7z0br78xqvw276bjkwiyyirba4f9aynq6ajvwq")))

(define-public crate-serde-versions-derive-0.0.3 (c (n "serde-versions-derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g01k3cydfnhfhp2srb40hdqsfvj1j8msk96cb31mipacg2wd1b0")))

(define-public crate-serde-versions-derive-0.0.4 (c (n "serde-versions-derive") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hlqgi5v9brfa436khw63ryql5l7flnngqx52g31mwcjl2r40lvq")))

(define-public crate-serde-versions-derive-0.0.5 (c (n "serde-versions-derive") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xff9wlvfhv87b3ls9vrikv5qdcj4ywby1isk01f6k9gnsrs9n1v")))

