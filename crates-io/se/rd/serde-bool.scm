(define-module (crates-io se rd serde-bool) #:use-module (crates-io))

(define-public crate-serde-bool-0.0.0 (c (n "serde-bool") (v "0.0.0") (h "17a5g6nh06kl1cxkidzzj2hqjlc2v9m1sc9mc4facsx01lc8qn3q")))

(define-public crate-serde-bool-0.0.2 (c (n "serde-bool") (v "0.0.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "051n5zv89g4gidpg9p3p0c6hcin806lg6dphi9j9s18ljb6k03sd")))

(define-public crate-serde-bool-0.0.3 (c (n "serde-bool") (v "0.0.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03wprdhg4xh3kj1f9bj01bcrbbyhsw90l99ld0kxll7zvsl9b658")))

(define-public crate-serde-bool-0.1.0 (c (n "serde-bool") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0zminagwzpv82jxi25xss6lsy8nww87cajyv7da0iqk2pwkafcr2")))

(define-public crate-serde-bool-0.1.1 (c (n "serde-bool") (v "0.1.1") (d (list (d (n "serde") (r "^1") (k 0)))) (h "1nz8scmdk01yw7i2z5618ycckns6sl4zx729nghbydy2vanfb25j") (r "1.56")))

(define-public crate-serde-bool-0.1.2 (c (n "serde-bool") (v "0.1.2") (d (list (d (n "serde") (r "^1") (k 0)))) (h "14zfy3kzls0jq9kzzarilc7m8rg99kz01v08nq11231qjic679jd") (r "1.56")))

(define-public crate-serde-bool-0.1.3 (c (n "serde-bool") (v "0.1.3") (d (list (d (n "serde") (r "^1") (k 0)))) (h "14ksbf9akp196if44423fqdc206nnbz63wbwfl9yrgmh8a94zw9a") (r "1.56")))

