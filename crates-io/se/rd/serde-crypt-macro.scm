(define-module (crates-io se rd serde-crypt-macro) #:use-module (crates-io))

(define-public crate-serde-crypt-macro-0.1.0 (c (n "serde-crypt-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "14dflldv183gx4ifswc0dzpz4kfq1p7cwvk2lh2k83kxlwsjnrdd")))

(define-public crate-serde-crypt-macro-0.1.1 (c (n "serde-crypt-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1v20j7w6j76qnj322wzc25dmn0165jplbx134ap22ziv200g8827")))

(define-public crate-serde-crypt-macro-0.1.2 (c (n "serde-crypt-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0knj08scvnw27ysdxjbgs8qa14lhaxy08r2m1782l7fx9y8aflb9")))

(define-public crate-serde-crypt-macro-0.1.3 (c (n "serde-crypt-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1nc8iq2rpa1fhr8i6mph89s3piawask8a671klckl0fdc7wqzrgc")))

(define-public crate-serde-crypt-macro-0.1.4 (c (n "serde-crypt-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "143xszyp4qb7ir5v5gv1d4r8mwpiarpmdbpa2c71977qk5r56mdf")))

(define-public crate-serde-crypt-macro-0.1.5 (c (n "serde-crypt-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-crypt") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "13j79a4hgz4rqh46wipdn79x0svqy8h143bbbjnh04izcphibd1x")))

