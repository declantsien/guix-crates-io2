(define-module (crates-io se rd serde_apply_macros) #:use-module (crates-io))

(define-public crate-serde_apply_macros-0.1.0 (c (n "serde_apply_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14fpyw6y6pzyvialrgpgbgx4s8s2xb1zm17vflb0j89h79ppfml8")))

(define-public crate-serde_apply_macros-0.1.1 (c (n "serde_apply_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "042hr04fnradxi8wq836l1276ljn0r5nv9gdx2j3kzzg5yp7kk6v")))

