(define-module (crates-io se rd serde_tuple_macros) #:use-module (crates-io))

(define-public crate-serde_tuple_macros-0.5.0 (c (n "serde_tuple_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zhdcfzb52lb4j5rydnhf2w1hvn66dwrjdpjm8jqws1b38fiaxj0")))

