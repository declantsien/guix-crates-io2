(define-module (crates-io se rd serde-json-utils) #:use-module (crates-io))

(define-public crate-serde-json-utils-0.1.0 (c (n "serde-json-utils") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15glw1aa096wkvz4p48wgyapfhjnplzlkkagnkqx04bgdvqmcpsh") (r "1.56")))

(define-public crate-serde-json-utils-0.2.0 (c (n "serde-json-utils") (v "0.2.0") (d (list (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b8kfzffm3mp91qw02wn84kmdfc2l3rdsr3yyhi33cfigznh2v4d") (r "1.56")))

(define-public crate-serde-json-utils-0.2.1 (c (n "serde-json-utils") (v "0.2.1") (d (list (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03x6r9gjvrqvxxpp12aa5pnrly7kkvd61mkqc894sqs6bd3q00wx") (r "1.56")))

