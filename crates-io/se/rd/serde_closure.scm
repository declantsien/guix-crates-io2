(define-module (crates-io se rd serde_closure) #:use-module (crates-io))

(define-public crate-serde_closure-0.1.0 (c (n "serde_closure") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yffpwv6j35csq0gv3rphk8089z5kcjipbb8xcdn6cr5kgij1y2h")))

(define-public crate-serde_closure-0.1.1 (c (n "serde_closure") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "relative") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17885ydrbz3q257d4j0yj9flwrp3rcr68yxhzwz65ar294rqkwpy")))

(define-public crate-serde_closure-0.1.2 (c (n "serde_closure") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "relative") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04v9sma317wcfrpwyj10ivq9hc3vwx391qgmdpyi8dcsnrk01vc0")))

(define-public crate-serde_closure-0.1.3 (c (n "serde_closure") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "relative") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zngr6axgwxxx2jfmj2imchr365b7qlxnc71zxlli6qgm8bf0c8b")))

(define-public crate-serde_closure-0.1.4 (c (n "serde_closure") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "relative") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lgmvmzdpd7knwf67in4p7ivxpqq72rzavvy9qy0fflc9jqrxkvm")))

(define-public crate-serde_closure-0.1.5 (c (n "serde_closure") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "relative") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bbgd71r48i2q27mmqk6vfnq0ip6l7d56nm4gy8z8li72sj3m4nx")))

(define-public crate-serde_closure-0.2.0 (c (n "serde_closure") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1i6jf2x07rhhkssl5g2cpgq2rxfndy3zksl9gmpjym9rpngrcmcm")))

(define-public crate-serde_closure-0.2.1 (c (n "serde_closure") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1k1l3kpw0j6lmd9pwl1zgk6ma24sd73if54i4hr735qmsmgx4grd")))

(define-public crate-serde_closure-0.2.2 (c (n "serde_closure") (v "0.2.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fd88bcmfbnhp9bsjb6ccg81sfvpahc10d2w7nwnb7l17fqy86gd")))

(define-public crate-serde_closure-0.2.3 (c (n "serde_closure") (v "0.2.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1znzqgn33ydsg0qgq075ajiz8acfv6s57ba31pq00fj77mr9mdbr")))

(define-public crate-serde_closure-0.2.4 (c (n "serde_closure") (v "0.2.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0s7fb4mlz1ha5rw3kdy8k27m01qyb9jr1mqrf67zvj9b5xph17w6")))

(define-public crate-serde_closure-0.2.5 (c (n "serde_closure") (v "0.2.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bqs9x5rd38rpp39l4qrclal5fa3fii5wkrmlzn3q9ji65blcsgm")))

(define-public crate-serde_closure-0.2.6 (c (n "serde_closure") (v "0.2.6") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zznadhb0v4d689s31ial63gj1pshhhqjz9hvq84xnmkzllc04id")))

(define-public crate-serde_closure-0.2.7 (c (n "serde_closure") (v "0.2.7") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vdzn7haqvj43qf92zsckm3v7i5hvjx40587h8wj0bzdc1aqx1mm")))

(define-public crate-serde_closure-0.2.8 (c (n "serde_closure") (v "0.2.8") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "185lzhs0ms6r7ixl3khpz34rhvnyz4j12aac6m3cn4c579dlgq4h")))

(define-public crate-serde_closure-0.2.9 (c (n "serde_closure") (v "0.2.9") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qjl20fnn51mpj6nn0fs6rfvf9aifpi7svyx2ih26igy8hjmkfm0")))

(define-public crate-serde_closure-0.2.10 (c (n "serde_closure") (v "0.2.10") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jhv474w7vw2rgqdl94gn3mms2yjr2kd0g72s23j0mavb3m1jrvb")))

(define-public crate-serde_closure-0.2.11 (c (n "serde_closure") (v "0.2.11") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "= 0.2.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c4fp9q602s69vyijwiy7zx188ydqmk97w1blgz6lm9v11sc2n6i")))

(define-public crate-serde_closure-0.2.12 (c (n "serde_closure") (v "0.2.12") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.2.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14mapz5ng4jjzwb1n73zzvry3v1gwkhg47v8pcznyjszcypnb7iv")))

(define-public crate-serde_closure-0.2.13 (c (n "serde_closure") (v "0.2.13") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.2.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nzxhxdb77g0gmdk2b42qwsczyn45nixv5n3h0aqf5jc1jmhd563")))

(define-public crate-serde_closure-0.2.14 (c (n "serde_closure") (v "0.2.14") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.2.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rv6lkrfdgx3sxhi18wxigi8dzzmvji0dra0gg3h7w23dvvhrb3j") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-serde_closure-0.3.0 (c (n "serde_closure") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04njh8vd02709xx0gih75py38cjlg4wql6hlcg80rhg6mzlj3br1") (f (quote (("nightly"))))))

(define-public crate-serde_closure-0.3.1 (c (n "serde_closure") (v "0.3.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qh0ldyzhnrk2l1la5vgkdcd9q542vgcj178dgn6m32hm1kbrzq5")))

(define-public crate-serde_closure-0.3.2 (c (n "serde_closure") (v "0.3.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "162ng7lcy8wnc5zfvlyfq7mpsmgs8748v7a8r85nw94zy3gszz4d")))

(define-public crate-serde_closure-0.3.3 (c (n "serde_closure") (v "0.3.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_closure_derive") (r "=0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "181dg4kkmz7ffzjpq7vq9nrca2lr291rkxz1lmhx0kdzrxxl6ncn")))

