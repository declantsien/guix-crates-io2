(define-module (crates-io se rd serde-rlp) #:use-module (crates-io))

(define-public crate-serde-rlp-0.1.0 (c (n "serde-rlp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1h7m5axq7ql9f048qsmb8ysh9akghixlbw94a52dxz8qgynqg20d")))

(define-public crate-serde-rlp-0.1.1 (c (n "serde-rlp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "08aal5l8698ki7crgbj70y80ywxfl5rjs762abrr44i33xypcv8g")))

(define-public crate-serde-rlp-0.1.2 (c (n "serde-rlp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1w7mvp3y14njdi8c8lhkxm1vk8k591jlqyksp79ms28al3wyl35j")))

(define-public crate-serde-rlp-0.1.3 (c (n "serde-rlp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1fm9bkqahlr2mb4bpad8c6jgy93jck3z8cqcln76jy5p6wzj1fqs")))

(define-public crate-serde-rlp-0.1.4 (c (n "serde-rlp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "error") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1a9w2g81pffgdlnvflrr3hvkrdzp4l33q8w2y8jh4w3pfnb2yiv9")))

