(define-module (crates-io se rd serde_shade_nbt) #:use-module (crates-io))

(define-public crate-serde_shade_nbt-0.0.1 (c (n "serde_shade_nbt") (v "0.0.1") (d (list (d (n "mutf8") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vwsya8km1c9pyg0k64nfy0qbkj77hb9a93088y130c2a8rjzrwi")))

(define-public crate-serde_shade_nbt-0.0.2 (c (n "serde_shade_nbt") (v "0.0.2") (d (list (d (n "mutf8") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14mj556dk981gs773a76a5qs1b5wchvif94i2z3va9pplxy1hsmy")))

(define-public crate-serde_shade_nbt-0.0.3 (c (n "serde_shade_nbt") (v "0.0.3") (d (list (d (n "mutf8") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pj8wr6vvhazdp0537j7p4n2g3np7blxn6pb0hkkjxv16z3s2a4j")))

(define-public crate-serde_shade_nbt-0.0.4 (c (n "serde_shade_nbt") (v "0.0.4") (d (list (d (n "mutf8") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bbg8xbpv1dc15l7040gggjlg2z274yzgfr6zmaannlwjiwly0zw")))

(define-public crate-serde_shade_nbt-0.0.5 (c (n "serde_shade_nbt") (v "0.0.5") (d (list (d (n "mutf8") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vbgh8zb4q1w39zmmyfs5kmwrli8jj9msrf7dzrc0qrix4snqdz4")))

