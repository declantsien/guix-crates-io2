(define-module (crates-io se rd serde-bridge) #:use-module (crates-io))

(define-public crate-serde-bridge-0.0.0 (c (n "serde-bridge") (v "0.0.0") (d (list (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "044xvdscwr9rq30xfz945dkprs14gyfxjx8x3ikmv143xpcplsqh")))

(define-public crate-serde-bridge-0.0.1 (c (n "serde-bridge") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1pjy4fb8vww2dh6i7k9yi0vdpc068mbml3fgssc52c6pqg9mb4yg")))

(define-public crate-serde-bridge-0.0.2 (c (n "serde-bridge") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1c7gcsnby8ngw5fm9jhsw1zynsgn87c6z27qvhbriy5nyqgx5166")))

(define-public crate-serde-bridge-0.0.3 (c (n "serde-bridge") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "114daxpgs3gfzarrxvqs7bicqgh3gw0hgjb5qm8hl3vyz18y8s2z")))

