(define-module (crates-io se rd serde_default_utils) #:use-module (crates-io))

(define-public crate-serde_default_utils-0.1.0 (c (n "serde_default_utils") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "1y4y8dk75304npils5g141a501w5hpddmr0b1y605b2m5bys5s9x")))

(define-public crate-serde_default_utils-0.2.0 (c (n "serde_default_utils") (v "0.2.0") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "0flv5ylr05pa28msab43hg9kzpr4maw7bsvqd636sk9qdfr67kgb") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde_default_utils-0.2.1 (c (n "serde_default_utils") (v "0.2.1") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "0sib7wnhy7rpxns24nj2gjf5a70xm0l3vkgjfiarqh56ir71ha5m") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde_default_utils-0.2.2 (c (n "serde_default_utils") (v "0.2.2") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-inline-default") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "033pbwv4bhr2mhphfz2czp9ra0wjkcdjjjn3rvjd3mq8wx6fpkq8") (f (quote (("std") ("inline-derive" "serde-inline-default") ("default" "std" "inline-derive"))))))

