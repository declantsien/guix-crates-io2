(define-module (crates-io se rd serde-json-schema) #:use-module (crates-io))

(define-public crate-serde-json-schema-0.1.0 (c (n "serde-json-schema") (v "0.1.0") (d (list (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0m35bb62xmdk9l3qb1q75k56zz8791r3nnx3i02790rl4qxh86r2")))

(define-public crate-serde-json-schema-0.1.1 (c (n "serde-json-schema") (v "0.1.1") (d (list (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0hm18aq08cqn3h6qr8phzsci6laqbp8401f77blv59rcqhlwf7s8")))

