(define-module (crates-io se rd serde-envfile) #:use-module (crates-io))

(define-public crate-serde-envfile-0.1.0 (c (n "serde-envfile") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)))) (h "0xc8c7qfq6j5xswssr5ay2xjwqb5g2f52hxnzwsm9fb2mr87wsbl") (f (quote (("debug"))))))

