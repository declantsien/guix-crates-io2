(define-module (crates-io se rd serde_avro_derive) #:use-module (crates-io))

(define-public crate-serde_avro_derive-0.1.0 (c (n "serde_avro_derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_avro_derive_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_avro_fast") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rvynpqgrm0cicqdd4nxm515vzjx2s2xwwcrkyanf26nspn7j4wv")))

(define-public crate-serde_avro_derive-0.1.1 (c (n "serde_avro_derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_avro_derive_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_avro_fast") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0j9fi7rzmidlajk8ykaz6gf176jj4ypw6nydj9274b1ag4gmq8ya")))

(define-public crate-serde_avro_derive-0.1.2 (c (n "serde_avro_derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_avro_derive_macros") (r "^0.1") (d #t) (k 0)) (d (n "serde_avro_fast") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01d75ik0vq5ij7x2hr2d31c25zvggdj2miilf4qmi644yz49061k")))

(define-public crate-serde_avro_derive-0.2.0 (c (n "serde_avro_derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_avro_derive_macros") (r "^0.2") (d #t) (k 0)) (d (n "serde_avro_fast") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "062wj1wrw9nxwfcyjd2x6birsaanx7y1h2y277qsnbdy29a45jk5")))

