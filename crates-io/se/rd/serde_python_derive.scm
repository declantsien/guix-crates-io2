(define-module (crates-io se rd serde_python_derive) #:use-module (crates-io))

(define-public crate-serde_python_derive-0.1.0 (c (n "serde_python_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (d #t) (k 0)))) (h "1rsym52gi6zf0d5084aq4rnb2sf9pq19swdc65zj8mxs5hn62vgc")))

(define-public crate-serde_python_derive-0.1.1 (c (n "serde_python_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.2") (d #t) (k 0)))) (h "1c7nz6ack4riz4k56c45nxjl4s8ni7s1bn2wi6hv6jdd1vb83ava")))

