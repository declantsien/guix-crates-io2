(define-module (crates-io se rd serde_json_matcher) #:use-module (crates-io))

(define-public crate-serde_json_matcher-0.1.1 (c (n "serde_json_matcher") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "0bfsfnnxywydb16s7jj3x6zj3xf3kl2dxlfx4rf0p8r61468ka0a")))

(define-public crate-serde_json_matcher-0.1.2 (c (n "serde_json_matcher") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "0gd1ygsl7ij2lx7y9v0n5r8r4h9ssallrvjhdvpn7rmrmjl3lmz0")))

(define-public crate-serde_json_matcher-0.1.3 (c (n "serde_json_matcher") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "1xjxkmq66zkb86dk48869pj215w9vr1kr8lpicpakkiba4sgna2c")))

(define-public crate-serde_json_matcher-0.1.4 (c (n "serde_json_matcher") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "03wwdz64w6a01rlrl5yvwbxvs6pivibmfzwi3dggmmmf0wnhhyzn") (r "1.36")))

(define-public crate-serde_json_matcher-0.1.5 (c (n "serde_json_matcher") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.90") (d #t) (k 0)))) (h "1mm1ifvrlha53v9x8xv2vkn6ylxq2n0m9zx99winjxdmjn7r2m4l") (r "1.36")))

