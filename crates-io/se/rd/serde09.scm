(define-module (crates-io se rd serde09) #:use-module (crates-io))

(define-public crate-serde09-0.9.0 (c (n "serde09") (v "0.9.0") (d (list (d (n "serde") (r "^0.9") (k 0)))) (h "0szdvfw4jinjzab4ryplvwxnmcx12jhhpsn8yy3x6wp965a3lrkw") (f (quote (("unstable-testing" "serde/unstable-testing") ("unstable" "serde/unstable") ("std" "serde/std") ("playground" "serde/playground") ("derive" "serde/derive") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

