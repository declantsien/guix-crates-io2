(define-module (crates-io se rd serde-scale-wrap) #:use-module (crates-io))

(define-public crate-serde-scale-wrap-0.1.0 (c (n "serde-scale-wrap") (v "0.1.0") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("alloc"))) (k 0)) (d (n "serde-scale") (r "^0.1.0") (f (quote ("alloc"))) (k 0)))) (h "1cjmvmmmp02gwk83911y4igk3cirfqh8hmy6843xjb9wzmm5i295") (f (quote (("std" "parity-scale-codec/std" "serde/std" "serde-scale/std") ("default" "std"))))))

(define-public crate-serde-scale-wrap-0.2.0 (c (n "serde-scale-wrap") (v "0.2.0") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("alloc"))) (k 0)) (d (n "serde-scale") (r "^0.2.0") (f (quote ("alloc"))) (k 0)))) (h "0cx7wm1cpcnsh425kmggkmd0dd1m2rwymsi10f1vkppxkz9d7fmg") (f (quote (("std" "parity-scale-codec/std" "serde/std" "serde-scale/std") ("default" "std"))))))

(define-public crate-serde-scale-wrap-0.2.1 (c (n "serde-scale-wrap") (v "0.2.1") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("alloc"))) (k 0)) (d (n "serde-scale") (r "^0.2.1") (f (quote ("alloc"))) (k 0)))) (h "0qwxd5m2bdlcgxs4vm9vqh1cn1h7p16x78j7i7xxgfdydx0yzc2y") (f (quote (("std" "parity-scale-codec/std" "serde/std" "serde-scale/std") ("default" "std"))))))

(define-public crate-serde-scale-wrap-0.2.2 (c (n "serde-scale-wrap") (v "0.2.2") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("alloc"))) (k 0)) (d (n "serde-scale") (r "^0.2.2") (f (quote ("alloc"))) (k 0)))) (h "0v1nk3bqvycm2abmc1sndyhgyzsinzgwdi2m3jhci1rd6fqvkm2z") (f (quote (("std" "parity-scale-codec/std" "serde/std" "serde-scale/std") ("default" "std"))))))

(define-public crate-serde-scale-wrap-0.3.0 (c (n "serde-scale-wrap") (v "0.3.0") (d (list (d (n "parity-scale-codec") (r "^2.1.3") (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("alloc"))) (k 0)) (d (n "serde-scale") (r "^0.2.2") (f (quote ("alloc"))) (k 0)))) (h "1ds771pp4k540vl04vby0klpkk9vyihcn65ljjs7nrm5xar9hixd") (f (quote (("std" "parity-scale-codec/std" "serde/std" "serde-scale/std") ("default" "std"))))))

