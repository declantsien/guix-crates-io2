(define-module (crates-io se rd serde-hex) #:use-module (crates-io))

(define-public crate-serde-hex-0.1.0 (c (n "serde-hex") (v "0.1.0") (d (list (d (n "array-init") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0vf2dplp1i27z9xphha8vzysv1dfxx3ykr0yy5zzv6mks7jf6dya")))

