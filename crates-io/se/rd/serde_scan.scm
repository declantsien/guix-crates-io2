(define-module (crates-io se rd serde_scan) #:use-module (crates-io))

(define-public crate-serde_scan-0.1.0 (c (n "serde_scan") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0naiigz2q0xs2skl9ddkcb0d2rll9r2va69sfph2bn3wvzlvsila")))

(define-public crate-serde_scan-0.2.0 (c (n "serde_scan") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "16n269ys08acb6mfrdhrjd8hbyq0s8fcy9ri5pdyhgq0wkzdy5zp")))

(define-public crate-serde_scan-0.3.0 (c (n "serde_scan") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1m4bkvsxjpdw984vs0i14w0pfwfsy7iy667bnqhlkljryifcgb39")))

(define-public crate-serde_scan-0.3.1 (c (n "serde_scan") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1afkrjlsbxp7wzaz08a4rwjz6wx28dpsbdz19ka1nyvpnvms01i7")))

(define-public crate-serde_scan-0.3.2 (c (n "serde_scan") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1i56k6n39bd3vcv1hy2f4ff2cykc0n4m71v46k2z5c01dsizhv2d")))

(define-public crate-serde_scan-0.4.0 (c (n "serde_scan") (v "0.4.0") (d (list (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0q7vvy4rwgwir5mfh9v0s4qmky8di9b3rx1vk03a7wjqr6vqx6xq")))

(define-public crate-serde_scan-0.4.1 (c (n "serde_scan") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15smmk8bvih1i2i7pnrf8q9p51zb4i5v20dj718fyk37ap9swa63")))

