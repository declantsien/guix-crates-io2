(define-module (crates-io se rd serde_test) #:use-module (crates-io))

(define-public crate-serde_test-0.7.11 (c (n "serde_test") (v "0.7.11") (d (list (d (n "serde") (r "^0.7.11") (d #t) (k 0)))) (h "0awb0xqlfr0v4qihfv3db4wh0n6kc20khsjqbzi1mz1sy5y483ak")))

(define-public crate-serde_test-0.7.12 (c (n "serde_test") (v "0.7.12") (d (list (d (n "serde") (r "^0.7.12") (d #t) (k 0)))) (h "15ifsgm0j486fvxgbvzizh0zsw5sr808grxh2r32ri20bas10aab")))

(define-public crate-serde_test-0.7.13 (c (n "serde_test") (v "0.7.13") (d (list (d (n "serde") (r "^0.7.13") (d #t) (k 0)))) (h "110wd28bz81kg5c7m5bnq4xki53ixr6ibp8p9jzphr409i3srlgh")))

(define-public crate-serde_test-0.7.14 (c (n "serde_test") (v "0.7.14") (d (list (d (n "serde") (r "^0.7.14") (d #t) (k 0)))) (h "1mbg5q3yclk7nwghadn2vb2g3ja7q5d3wvilbilzqfhqs1cgz4hy")))

(define-public crate-serde_test-0.8.0-rc1 (c (n "serde_test") (v "0.8.0-rc1") (d (list (d (n "serde") (r "^0.8.0-rc1") (d #t) (k 0)))) (h "0qrlp7kra1pzxhcr0bvhidylkaqlflxd8r39zc4fxlam8cdjdkbq")))

(define-public crate-serde_test-0.8.0-rc2 (c (n "serde_test") (v "0.8.0-rc2") (d (list (d (n "serde") (r "^0.8.0-rc2") (d #t) (k 0)))) (h "14h9g3mh548p17j75mj6n4c5gqlpgnp0512b9cdq6kp55ld5izmm")))

(define-public crate-serde_test-0.8.0-rc3 (c (n "serde_test") (v "0.8.0-rc3") (d (list (d (n "serde") (r "^0.8.0-rc3") (d #t) (k 0)))) (h "0rmwc506wmwjpmpyqcvs8yrfr7b2gymnza5bp75kqgpjlzq6sr7v")))

(define-public crate-serde_test-0.7.15 (c (n "serde_test") (v "0.7.15") (d (list (d (n "serde") (r "^0.7.15") (d #t) (k 0)))) (h "0y2vbrrblv0pnx27yxlm5przd78ldgf3cbkkbfgn6pw5hghnmgmj")))

(define-public crate-serde_test-0.8.0 (c (n "serde_test") (v "0.8.0") (d (list (d (n "serde") (r "^0.8.0") (d #t) (k 0)))) (h "1vjnbl4m7az3f3r5gb7g6vpxil9z6mrgh91g2mhz69jkw228abp0")))

(define-public crate-serde_test-0.8.1 (c (n "serde_test") (v "0.8.1") (d (list (d (n "serde") (r "^0.8.1") (d #t) (k 0)))) (h "1icdw06jiwwchh43ar4k3fjcycfp1245hvhglzb3irkxamg20yn2")))

(define-public crate-serde_test-0.8.2 (c (n "serde_test") (v "0.8.2") (d (list (d (n "serde") (r "^0.8.2") (d #t) (k 0)))) (h "0cdmpk5iqkd9pw9m906h2h6pzdsrkx2gj5q2za35rahcyp1y509z")))

(define-public crate-serde_test-0.8.3 (c (n "serde_test") (v "0.8.3") (d (list (d (n "serde") (r "^0.8.3") (d #t) (k 0)))) (h "1w0a2l4f8pm6kkqcml5q2mglzv7i1w7flmhddpmzfh7vjdi0crgb")))

(define-public crate-serde_test-0.8.4 (c (n "serde_test") (v "0.8.4") (d (list (d (n "serde") (r "^0.8.4") (d #t) (k 0)))) (h "17zsn14zr5caqi6b3m1h033x84x1wxab4b8ys1hgg83l7sl85f0d")))

(define-public crate-serde_test-0.8.5 (c (n "serde_test") (v "0.8.5") (d (list (d (n "serde") (r "^0.8.5") (d #t) (k 0)))) (h "15qpsvbzvq0fcidpbd8wx6bv2jqjgs4qgvj63xc70v3jakz03xl1")))

(define-public crate-serde_test-0.8.6 (c (n "serde_test") (v "0.8.6") (d (list (d (n "serde") (r "^0.8.6") (d #t) (k 0)))) (h "1v4pgdi7wk1vlnjmvlhnrl0zza83aqp9mdlnqkbkka7l29qxbaib")))

(define-public crate-serde_test-0.8.7 (c (n "serde_test") (v "0.8.7") (d (list (d (n "serde") (r "^0.8.7") (d #t) (k 0)))) (h "0skmpqsq8nisp61a1gp29inxah2h8jqxgfh3z6597crn4ac4p0aj")))

(define-public crate-serde_test-0.8.8 (c (n "serde_test") (v "0.8.8") (d (list (d (n "serde") (r "^0.8.8") (d #t) (k 0)))) (h "148ns0yqxa5yl5ba3l55skr0127prz0hkdqj4l3r5c1ca716zrwi")))

(define-public crate-serde_test-0.8.9 (c (n "serde_test") (v "0.8.9") (d (list (d (n "serde") (r "^0.8.9") (d #t) (k 0)))) (h "1lmh94y3n26ni0mri0jhkr7i2klwjci90hapahx5whgzmhfxfczc")))

(define-public crate-serde_test-0.8.10 (c (n "serde_test") (v "0.8.10") (d (list (d (n "serde") (r "^0.8.10") (d #t) (k 0)))) (h "1bwzz4aw65pifz9bkfzd3kdpz64v1iqj80jr29v7zarr08l8dsa3")))

(define-public crate-serde_test-0.8.11 (c (n "serde_test") (v "0.8.11") (d (list (d (n "serde") (r "^0.8.11") (d #t) (k 0)))) (h "1rsgjb6insyb5m67qgwk8q2a8c9mnkj5y48is3dqpq46x9vikf8l")))

(define-public crate-serde_test-0.8.12 (c (n "serde_test") (v "0.8.12") (d (list (d (n "serde") (r "^0.8.12") (d #t) (k 0)))) (h "1ixryjkipm8x3y7mdz1w34xjm0hsz5ybs7b0w1rm1jx66qb321rd")))

(define-public crate-serde_test-0.8.13 (c (n "serde_test") (v "0.8.13") (d (list (d (n "serde") (r "^0.8.13") (d #t) (k 0)))) (h "0v7bwnfwnqm4h1qh5ckbx0h3nb7xvj3ky9z4fqvm26wqydfzanmf")))

(define-public crate-serde_test-0.8.14 (c (n "serde_test") (v "0.8.14") (d (list (d (n "serde") (r "^0.8.14") (d #t) (k 0)))) (h "15ag6bvrl3agmlprfqwv2spn5ir4zlqbdvi1583micprba62n81p")))

(define-public crate-serde_test-0.8.15 (c (n "serde_test") (v "0.8.15") (d (list (d (n "serde") (r "^0.8.15") (d #t) (k 0)))) (h "16576zkvccbmvq784bgs14nc0qhl450yjs5fcd8x0jn0492cy5kz")))

(define-public crate-serde_test-0.8.16 (c (n "serde_test") (v "0.8.16") (d (list (d (n "serde") (r "^0.8.16") (d #t) (k 0)))) (h "0cqj18vywgfb5b7kjmk80sqn2p0q7gmrr362mp7djx2pvfbmfzqf")))

(define-public crate-serde_test-0.8.17 (c (n "serde_test") (v "0.8.17") (d (list (d (n "serde") (r "^0.8.17") (d #t) (k 0)))) (h "0b8cqfvj7fc9r0l7k1vhappm3l0pf3w4d18xis74m1rwlz7js79m")))

(define-public crate-serde_test-0.8.18 (c (n "serde_test") (v "0.8.18") (d (list (d (n "serde") (r "^0.8.18") (d #t) (k 0)))) (h "0z6s5p8cjanlihpxk9f37jnzpykmb6fzbw7qmn2qc5q6jr9248bc")))

(define-public crate-serde_test-0.8.19 (c (n "serde_test") (v "0.8.19") (d (list (d (n "serde") (r "^0.8.19") (d #t) (k 0)))) (h "025k3ksvwnpnkdwq4dd5fm3dy8jv1ng9mlq7xj2zq12qwzvh39sx")))

(define-public crate-serde_test-0.8.20 (c (n "serde_test") (v "0.8.20") (d (list (d (n "serde") (r "^0.8.20") (d #t) (k 0)))) (h "0vvq2hg4sg1zx8qjkgcblf5xhkys6675hd2pvwl7igclbyhiww7f")))

(define-public crate-serde_test-0.8.21 (c (n "serde_test") (v "0.8.21") (d (list (d (n "serde") (r "^0.8.21") (d #t) (k 0)))) (h "1jjmd48754qgmwpva7zq06jp51irccf3d7aq0zbrzjlaf7ahxd2k")))

(define-public crate-serde_test-0.8.22 (c (n "serde_test") (v "0.8.22") (d (list (d (n "serde") (r "^0.8.22") (d #t) (k 0)))) (h "15hvwafz4az4di5p9bazs18gh57c5ay3hczdhpy8xx60p3r6hh55")))

(define-public crate-serde_test-0.9.0-rc1 (c (n "serde_test") (v "0.9.0-rc1") (d (list (d (n "serde") (r "^0.9.0-rc1") (d #t) (k 0)))) (h "0w8p7dgx3phmd6v1f7hrasbf612y6ykv21ssawj8h3hks0cwb2x3")))

(define-public crate-serde_test-0.8.23 (c (n "serde_test") (v "0.8.23") (d (list (d (n "serde") (r "^0.8.23") (d #t) (k 0)))) (h "1m939j7cgs7i58r6vxf0ffp3nbr8advr8p9dqa9w8zk0z2yks2qi")))

(define-public crate-serde_test-0.9.0-rc2 (c (n "serde_test") (v "0.9.0-rc2") (d (list (d (n "serde") (r "^0.9.0-rc2") (d #t) (k 0)))) (h "12vyl3cwgfmqvr6kf3id47vsm9pnlgfyiarm67b2vss2wd3slsf8")))

(define-public crate-serde_test-0.9.0-rc3 (c (n "serde_test") (v "0.9.0-rc3") (d (list (d (n "serde") (r "^0.9.0-rc3") (d #t) (k 0)))) (h "05j02pbnfvcxy0x3nws2sndliws3ym7sdigalyr17swad465sgb6")))

(define-public crate-serde_test-0.9.0-rc4 (c (n "serde_test") (v "0.9.0-rc4") (d (list (d (n "serde") (r "^0.9.0-rc4") (d #t) (k 0)))) (h "1zc41kkq962lw7dc0gswls66qfvv0m9gjpfax3653iqf04i0504j")))

(define-public crate-serde_test-0.9.0 (c (n "serde_test") (v "0.9.0") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 0)))) (h "0fx4khpk4qmz6zvpmaxqp6w2kj5xq2hbcwhxriv9dg0psqz5p98a")))

(define-public crate-serde_test-0.9.1 (c (n "serde_test") (v "0.9.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1f9b619hkhqlfqc8c48fka8zqb9gi5mgxhbps0nqcn5l8lyrzrrp")))

(define-public crate-serde_test-0.9.2 (c (n "serde_test") (v "0.9.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1zihwkg2viinbi4dkzfl84p9fcan5gayjm7dild1pj74bccjzfyj") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_test-0.9.3 (c (n "serde_test") (v "0.9.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0hng3hqbpqf7fxs53r85p6d4q0npgx9kzsib8xws5789fw3sgaqq") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_test-0.9.4 (c (n "serde_test") (v "0.9.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "10ajlmacd33jwj4jdmf527f41d2c50synn1gkm457ih8qbrwhrnq") (f (quote (("unstable-testing" "clippy"))))))

(define-public crate-serde_test-0.9.5 (c (n "serde_test") (v "0.9.5") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1y06db7r0yvk4s1pws3jfzncqhx99h8pb1dis4qsvv1hflf2ba71")))

(define-public crate-serde_test-0.9.6 (c (n "serde_test") (v "0.9.6") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "05ibjg1imp5hf4zarmdwpz9i9nr2gmq4p7sfb8nm1grqs7qzic49")))

(define-public crate-serde_test-0.9.7 (c (n "serde_test") (v "0.9.7") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "07znwl21aw50g46q9cqdhv1rjbcihxrwpl665xsp9cxlfvkj1hh2")))

(define-public crate-serde_test-0.9.8 (c (n "serde_test") (v "0.9.8") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "02lwsky6lfxldiwjnn9cr8czi8b752crqs6w2phply48cal652ic")))

(define-public crate-serde_test-0.9.9 (c (n "serde_test") (v "0.9.9") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0l8warakvwhn6ag53qrhimwgy62vxs9ibmxm6m01b3f7x49b9sv2")))

(define-public crate-serde_test-0.9.10 (c (n "serde_test") (v "0.9.10") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "00w16m04jjpbr5smp2i33s18rrcfl8x3fqimqg8amh1ppwfkni4i")))

(define-public crate-serde_test-0.9.11 (c (n "serde_test") (v "0.9.11") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1vkcp5lfjwgf8l6adsf3lnh02g2wwlj6lnwz1lj4zf6shvvhk6v6")))

(define-public crate-serde_test-0.9.12 (c (n "serde_test") (v "0.9.12") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0wy8d99ilmw5aak1flijdk6jq0i54x5pzj9kxajnj6ki3q962b2p")))

(define-public crate-serde_test-0.9.13 (c (n "serde_test") (v "0.9.13") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "1iijhxyz9hzshlg66h2rdnjy4hgfygh9lkmx23m1dh3d6bng9f2m")))

(define-public crate-serde_test-0.9.14 (c (n "serde_test") (v "0.9.14") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "0wy102frpzwr2fhsvchh2ad2fi24q0xpz3m0794iabvwnq0dvk40")))

(define-public crate-serde_test-1.0.0 (c (n "serde_test") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0lsxqn4hyzzm1q88hl50qpd5dbisflr03y1wrmbnnh7l4h190cpj")))

(define-public crate-serde_test-0.9.15 (c (n "serde_test") (v "0.9.15") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "193mf0qkhvjywd06x6hhmkixlqcyfbpfwfmr75dp2b8xwzpsvxwf")))

(define-public crate-serde_test-1.0.1 (c (n "serde_test") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0jzjp8v6rxw3kfx4a3knrlp9viwy6pk4zjhwa9sz3v5sv12jxvxk")))

(define-public crate-serde_test-1.0.2 (c (n "serde_test") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "03fqakws84pzl5c1dyn5n1am9vi4r5iss344k058h656yz46b7xi")))

(define-public crate-serde_test-1.0.3 (c (n "serde_test") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wl049wzn03xblrvg94snnj86i8mlw42m5y113hsms34g6qc4s76")))

(define-public crate-serde_test-1.0.4 (c (n "serde_test") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1l4pgbmp1gls3z8dyki2mf1qx6yk4q1w4vy4gzlbpi9y14a193yx")))

(define-public crate-serde_test-1.0.5 (c (n "serde_test") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0lyfrzghfajyjrizbc0pa4q00dkfag7347cnw0qdczqjsb0gb23i")))

(define-public crate-serde_test-1.0.6 (c (n "serde_test") (v "1.0.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00ri094088c69mq21bwvwr1ahlx44m9jsay9xzmh3v458cvcbfy3")))

(define-public crate-serde_test-1.0.7 (c (n "serde_test") (v "1.0.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0snl19isfi7kjivva6ixhzhhvh97sfy0ccxpw30jr1pcw0i8133x")))

(define-public crate-serde_test-1.0.8 (c (n "serde_test") (v "1.0.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "19kgqybqydwghblm4wacn24y9sbgnwc875my35wcx1gjflh1v6bj")))

(define-public crate-serde_test-1.0.9 (c (n "serde_test") (v "1.0.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1f4kshkk6nhjx06yximawm8jppl5c5d17wzqmv0cmz49n0ansz2p")))

(define-public crate-serde_test-1.0.10 (c (n "serde_test") (v "1.0.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0781fis89mjn6l8fcn8p5mxyh5jad5yxwhgq5pimhzbldm6jc0ql")))

(define-public crate-serde_test-1.0.11 (c (n "serde_test") (v "1.0.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1873gkxmbzrmaj62nlgwqvrwbxb1v4ysimhll40amds9pabvr78h")))

(define-public crate-serde_test-1.0.12 (c (n "serde_test") (v "1.0.12") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0df2csfg5q5hkcbwwfw7a47f3xs8dia0nqhjpphp381fm3f5kw0r")))

(define-public crate-serde_test-1.0.13 (c (n "serde_test") (v "1.0.13") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0pihjw9a0xvq4wh48h4q2hhws20s9jk0dmnb16hdqmz801a2hdsr")))

(define-public crate-serde_test-1.0.14 (c (n "serde_test") (v "1.0.14") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0wssynilbiw32kq1qqzh0270vnr0091j3ihxiz1syi6kfyshzx0g")))

(define-public crate-serde_test-1.0.15 (c (n "serde_test") (v "1.0.15") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "075h6jw2yi07nilj981lhb30pcbrrdpfv0acs1hkg1yj6gm46la5")))

(define-public crate-serde_test-1.0.16 (c (n "serde_test") (v "1.0.16") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1nyg9q0624m5hr33y48w5jllk7mz1aj55h3556h9imm84a8j6x1d")))

(define-public crate-serde_test-1.0.17 (c (n "serde_test") (v "1.0.17") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08lwaxl4bj2awd25v743n160dq7clfwkqmwy6pavp1yd8k8sbnrp")))

(define-public crate-serde_test-1.0.18 (c (n "serde_test") (v "1.0.18") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0xll6gifnp2m0d25kjrwgvfig99xfynl3mfj1icvn6j52lba05vh")))

(define-public crate-serde_test-1.0.19 (c (n "serde_test") (v "1.0.19") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0h7sypz6sh8cb8q7wxz000n735pd156nf4d6fhnlw5sm3vhna9s8")))

(define-public crate-serde_test-1.0.20 (c (n "serde_test") (v "1.0.20") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "04ap9li0m6ic8q11is79gvidgdp9hp1dx7jm11igaxd6z8n68pbd")))

(define-public crate-serde_test-1.0.21 (c (n "serde_test") (v "1.0.21") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gfmxswpb23dd9c45ylna5ywx5zygch3nlqd84adxys3g2kmyjh3")))

(define-public crate-serde_test-1.0.22 (c (n "serde_test") (v "1.0.22") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1wvprb8yn1vi9b2nxk6dilcaafk4b5hymszvx8cw7mcccg5wqchs")))

(define-public crate-serde_test-1.0.23 (c (n "serde_test") (v "1.0.23") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11ph983hid72pbisnx9zzr5vxmhwilfvflm88433x58x3zwbh94n")))

(define-public crate-serde_test-1.0.24 (c (n "serde_test") (v "1.0.24") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11jfhixkdi571lk6bzjiv82xpq41zk9xqbvjs0b5i6wili2fpifi")))

(define-public crate-serde_test-1.0.25 (c (n "serde_test") (v "1.0.25") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1n1k86wc1wwcf6bilx1cjmpvxzgw562jf2bgn44wn42k84jp4spi")))

(define-public crate-serde_test-1.0.26 (c (n "serde_test") (v "1.0.26") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qai4p3jc8qfv5d52bngd3j5j1zmp1nz9rncx399hd9x37jhfxwx")))

(define-public crate-serde_test-1.0.27 (c (n "serde_test") (v "1.0.27") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jwgap1sh3d4lagcxwqjihnscgp2qginhz1hn5zk04lb72w7sq7b")))

(define-public crate-serde_test-1.0.28 (c (n "serde_test") (v "1.0.28") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0rky01k9v3lxvlpvh32nxq009vb84bbr0cpasqlx1mgg9y9x4a94")))

(define-public crate-serde_test-1.0.29 (c (n "serde_test") (v "1.0.29") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sk205kzk7wmd1n5sqw5z9b1f9fp7q68x6pdr6dqsbxg33rb39ym")))

(define-public crate-serde_test-1.0.30 (c (n "serde_test") (v "1.0.30") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bil2bhp2xmkq8mqxjia7sjx994bq3qv0ixrvd3i3m14k2ml5dmh")))

(define-public crate-serde_test-1.0.31 (c (n "serde_test") (v "1.0.31") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0vizrslyv11njjsmpaa1j6zcf02p9vi77q0pc81n0cd2mhbhm5cm") (y #t)))

(define-public crate-serde_test-1.0.32 (c (n "serde_test") (v "1.0.32") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f75g1hrdjvwbhlnkq5b6zwqvbr77820g0kmfyqwqd48gz55f6xs")))

(define-public crate-serde_test-1.0.33 (c (n "serde_test") (v "1.0.33") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1phnga671n8zbxh0qvgxx0a2rbpxh6y5f1hg7zi8j5ang4ads33z")))

(define-public crate-serde_test-1.0.34 (c (n "serde_test") (v "1.0.34") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05lgchhldqyypiq6v7y50av7mfaf9ikra15yhzsv1184d9bzk4di")))

(define-public crate-serde_test-1.0.35 (c (n "serde_test") (v "1.0.35") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "18hyvhw8zicwfqybrnxfrz3drbq4gxvazvxlfdgbqsq3gnwpiry6")))

(define-public crate-serde_test-1.0.36 (c (n "serde_test") (v "1.0.36") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00s60mdbzcpxb1r6h30kb2kxbs7m088qv8skjnbi1g49323lfhy9")))

(define-public crate-serde_test-1.0.37 (c (n "serde_test") (v "1.0.37") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0q38f0p507pv7dvi8i8srwjhci7j3mavyc8ygwy94y72ddjvv150")))

(define-public crate-serde_test-1.0.38 (c (n "serde_test") (v "1.0.38") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15a5773kdh7fhj2mvks554jzrriz6yw6sf6b6lljdkl6xsgc5bip")))

(define-public crate-serde_test-1.0.39 (c (n "serde_test") (v "1.0.39") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jxpyh6fvsgg7hjajrzmih4f9jz3jc7fln4ls493g9sr3hkxsngh")))

(define-public crate-serde_test-1.0.40 (c (n "serde_test") (v "1.0.40") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "144d2mylzx3rqnx2hc3z05bsdcbrbri0nnr182x6knnplg7kmw9f")))

(define-public crate-serde_test-1.0.41 (c (n "serde_test") (v "1.0.41") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15bwlq6qchy0xp8ybpx2g739x5m8zngp5v50cj00sli9qbqm8wj3")))

(define-public crate-serde_test-1.0.42 (c (n "serde_test") (v "1.0.42") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0w44r91y8kpr9sbn4xdsw3iggrmp6ng0mydhbfk2clhn7v1djrxz")))

(define-public crate-serde_test-1.0.43 (c (n "serde_test") (v "1.0.43") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "12kzpf872hi16rz6s3a26770h597pamwf5yy0sqgig3c1g5dpdg8")))

(define-public crate-serde_test-1.0.44 (c (n "serde_test") (v "1.0.44") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08hcr43xg7vgf1p9nq94mh543krlggz5y6r1h0lpy7dcrlnjldq5")))

(define-public crate-serde_test-1.0.45 (c (n "serde_test") (v "1.0.45") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0xkc9qdfac10679136hc8frfclpqcnr919fpv0i71scs72sz71y1")))

(define-public crate-serde_test-1.0.46 (c (n "serde_test") (v "1.0.46") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ivccqddkf2vakw7x65qrd1kgk0rvmbmq076ivv1abglgp0myl5p")))

(define-public crate-serde_test-1.0.47 (c (n "serde_test") (v "1.0.47") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0lbk2dsrxjhvpvsj1pd7nf88mi49l5i9qfslfv8x81q4cyqgngjk")))

(define-public crate-serde_test-1.0.48 (c (n "serde_test") (v "1.0.48") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ppzpwrm5ncwc3qgfpnchvsm6c211nr1a98jlxzanf2wqnvaz1wf")))

(define-public crate-serde_test-1.0.49 (c (n "serde_test") (v "1.0.49") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bpnv3aqx9ihpfja0ps4gkyz19i9f09xah7cm4zaj37y1zrfvr9w")))

(define-public crate-serde_test-1.0.50 (c (n "serde_test") (v "1.0.50") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "091zryqbl0mx265zs599dsxaych69h64hq4brma9m2lbxk61738i")))

(define-public crate-serde_test-1.0.51 (c (n "serde_test") (v "1.0.51") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qs93yn8hywfq5x07nj77c1i70fgmmgzvxdjmg0l0r9fm7nj6ad2")))

(define-public crate-serde_test-1.0.52 (c (n "serde_test") (v "1.0.52") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1klbagcdz6wbz16gxyfx7d4d59r24ra5zj2ghpl87a5i52ydbs1l")))

(define-public crate-serde_test-1.0.53 (c (n "serde_test") (v "1.0.53") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lnm60r1rsjfgryn4h9w88gywn8y5zw24ib7kqp27hg3pdwz2ky1")))

(define-public crate-serde_test-1.0.54 (c (n "serde_test") (v "1.0.54") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1chkzz904xkhy54h42jkinrhbk05999jd0bf22gma3vddccl9d8d")))

(define-public crate-serde_test-1.0.55 (c (n "serde_test") (v "1.0.55") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zdwvfqngk5017nbycdd69falj1k9z5nmmhyasbfjffnkaqsg3ay")))

(define-public crate-serde_test-1.0.56 (c (n "serde_test") (v "1.0.56") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "14wch2684hhkawap23djn3h96qs1bg9hq8gbw10x163ivk645i6w")))

(define-public crate-serde_test-1.0.57 (c (n "serde_test") (v "1.0.57") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15hpcydlwmk9akgd0lk6ikqzhc44r039amvq140fmin96kl3ql9i")))

(define-public crate-serde_test-1.0.58 (c (n "serde_test") (v "1.0.58") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0prhvln9y9y8vlr8nrgyq5zym8gr9wi57p3k0k6nmwjipmvf2wgh")))

(define-public crate-serde_test-1.0.59 (c (n "serde_test") (v "1.0.59") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0xhgcrgmwklm3728w9h93p24q0n7n07cicz336qwg1i0xd8lnfzg")))

(define-public crate-serde_test-1.0.60 (c (n "serde_test") (v "1.0.60") (d (list (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "10z99mirpg8jf42f9h5zh9jjp3a1d9s62pdjsfv1iwzg22vhkzws")))

(define-public crate-serde_test-1.0.61 (c (n "serde_test") (v "1.0.61") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "014ih7jnq8rnjhiv67spg1rzjaj66vszsjl9q88yp4p7kwb4f7jz")))

(define-public crate-serde_test-1.0.62 (c (n "serde_test") (v "1.0.62") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "043awnnh3fqrsc9f0dssqldh1s7jmnd93k5fhgssbajrpry7bz1k")))

(define-public crate-serde_test-1.0.63 (c (n "serde_test") (v "1.0.63") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0yfq73q8aigk7l9a5503mxvcknp48m6dhgg96g1jypl13gqdwgns")))

(define-public crate-serde_test-1.0.64 (c (n "serde_test") (v "1.0.64") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f00f7knl52fh133bq51mfa5v0ikpmkrd7vv9vpzjr6psg4i17xg")))

(define-public crate-serde_test-1.0.65 (c (n "serde_test") (v "1.0.65") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08agnfqq7r5fabkb8z1bh0nac5wqvphb4a18l6dygy6w0g4jpn9z")))

(define-public crate-serde_test-1.0.66 (c (n "serde_test") (v "1.0.66") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1fky7iwwc0k1hzcdbap762jykqq5f3dgp1cqjpndfggrqlfqb7ip")))

(define-public crate-serde_test-1.0.67 (c (n "serde_test") (v "1.0.67") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1dlrij8biqmms1yrz4jcmpb1b8g4cy11799fi8gna4mzgh4knjg1")))

(define-public crate-serde_test-1.0.68 (c (n "serde_test") (v "1.0.68") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qlwjg5n5vb6f5pjncypf8fm9kf3cf1r3hg6ki4md3frkd33bkkw")))

(define-public crate-serde_test-1.0.69 (c (n "serde_test") (v "1.0.69") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1jyncacmaiwvcs3di62d9vfavx6p59np5v747iw7lflm14mca25r")))

(define-public crate-serde_test-1.0.70 (c (n "serde_test") (v "1.0.70") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1g4yaav1sdidmaadakdqlwvfgnb6wf1ik7kh23igas8yc42m9m8i")))

(define-public crate-serde_test-1.0.71 (c (n "serde_test") (v "1.0.71") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0c33k1k8gzvnqlnkd3p0abkvlpsxs73lpmsliwz61w7lw1xlxc10")))

(define-public crate-serde_test-1.0.72 (c (n "serde_test") (v "1.0.72") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0y8mf7krzfg4hn9i56351iz2vw7w0di8sjvadgfw9pj37aijf9ik")))

(define-public crate-serde_test-1.0.73 (c (n "serde_test") (v "1.0.73") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bvy821siwwxjq7g4zf715mq6wzs4cbqcnz6vh8r58iw53w6hlsm")))

(define-public crate-serde_test-1.0.74 (c (n "serde_test") (v "1.0.74") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qk0bcplh60svm4qlmv3vn3psdi324h9ch2gzrfbwgypdcccssd1")))

(define-public crate-serde_test-1.0.75 (c (n "serde_test") (v "1.0.75") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "159j1pa4lb0yp2wr3njl8grz9rni72if6d23f455ydbppfj0d8fs")))

(define-public crate-serde_test-1.0.76 (c (n "serde_test") (v "1.0.76") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mxfhj0dwbqk2i38fm7jircnm3ijhcc11fnr1kbqmybnz2ycl0ib")))

(define-public crate-serde_test-1.0.77 (c (n "serde_test") (v "1.0.77") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pifddv30inqi8nv076qnq4s0gcp46lac2myh6r7yikklimsh08k")))

(define-public crate-serde_test-1.0.78 (c (n "serde_test") (v "1.0.78") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1d950a5brlz850bqax55xvgxfw3427w01w0jdrs625f6s6vsp7nv")))

(define-public crate-serde_test-1.0.79 (c (n "serde_test") (v "1.0.79") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kl20g7k1vgs786yj9z2j9qb3l9zh0hync8mpg7q82yvhx933l0l")))

(define-public crate-serde_test-1.0.80 (c (n "serde_test") (v "1.0.80") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zisym19qv0anhrs54flxzmq781d7ky0dq0rd5nf1g3whj4lrsam")))

(define-public crate-serde_test-1.0.81 (c (n "serde_test") (v "1.0.81") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qd29k3j9nshqfr6vvk2jizfmaxj5l4kr9bdpq02lfk5nrcx38p5")))

(define-public crate-serde_test-1.0.82 (c (n "serde_test") (v "1.0.82") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06wry26nah98wdx6g8lbbw51b7kh4573pwn75x021b3y7g7mbfql")))

(define-public crate-serde_test-1.0.83 (c (n "serde_test") (v "1.0.83") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07i5ry57x2d7nfqy2ljhhhjqq2h62vb4d9fxmldxy270i16pk707")))

(define-public crate-serde_test-1.0.84 (c (n "serde_test") (v "1.0.84") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0mri4yxay3zvqzzfvj5hq5fsgby8ssxsqslipffq9a6axka2aw0a")))

(define-public crate-serde_test-1.0.85 (c (n "serde_test") (v "1.0.85") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kgv9zm5fd2fanb9drfchhln8va4hg430x46h6k98xhx8184783i")))

(define-public crate-serde_test-1.0.86 (c (n "serde_test") (v "1.0.86") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13cca7y604z87j36fw598ai0jzdais5plsqk5mp5nrajyvzw1g3v")))

(define-public crate-serde_test-1.0.87 (c (n "serde_test") (v "1.0.87") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15c9hvh9r64jxmp2rlc5d8w4w8bmyzihy87s46lma5vkgs7rflbc")))

(define-public crate-serde_test-1.0.88 (c (n "serde_test") (v "1.0.88") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ql9yrrsy44w32sz2kpdx2ppr5k1rmllfpvamn3zgpg09vjlmd7d")))

(define-public crate-serde_test-1.0.89 (c (n "serde_test") (v "1.0.89") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "10msbllyvz1m1xnwk1rhfx6j01qxvd1x6m8gnwy2bdaqfla7x03h")))

(define-public crate-serde_test-1.0.90 (c (n "serde_test") (v "1.0.90") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02r4vlil53w0j85zba593i24ma0x25n8lxqsf3sffqgs0z97xqbc")))

(define-public crate-serde_test-1.0.91 (c (n "serde_test") (v "1.0.91") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1irprr38kdzh83s3inrm5ndf6fv0lpflfzn2fw5j78nq54g3ajqn")))

(define-public crate-serde_test-1.0.92 (c (n "serde_test") (v "1.0.92") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "15qfhjyz26vwdnzq9h6783bpr33h1ga22dmj07rai2bkwsw5fdym")))

(define-public crate-serde_test-1.0.93 (c (n "serde_test") (v "1.0.93") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lil41lax3a64gd2jqnxqjhhjbrfdl208sf71in06sw60lj4ysw9")))

(define-public crate-serde_test-1.0.94 (c (n "serde_test") (v "1.0.94") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ii81dkyd3qmh8q3wjiwlpq6xfby2b0m5wf1f7clg93a7h4qjn3f")))

(define-public crate-serde_test-1.0.95 (c (n "serde_test") (v "1.0.95") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0fdhjwl4a4c0rpj8i6zjf3f15pq9sr4rhm695fyndp57nyxl4k6g")))

(define-public crate-serde_test-1.0.96 (c (n "serde_test") (v "1.0.96") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sbkyin13jclaqpg5rwa6fisb0a77m7l3w3qsxv0mfr79gn1f6af")))

(define-public crate-serde_test-1.0.97 (c (n "serde_test") (v "1.0.97") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0i17jdgy18rcp19v6h4shim27l99q3vc7pzr2xaa2g0rjq9p6lr2")))

(define-public crate-serde_test-1.0.98 (c (n "serde_test") (v "1.0.98") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ahy6m18dlsmnbgcrw85f8lbxll9gh13p5k8by2mk53lqyq8zram")))

(define-public crate-serde_test-1.0.99 (c (n "serde_test") (v "1.0.99") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1kd1gm98wcjhx5d9sdfrk24xqln060zrn6aj5h017xyi4s9rkn7j")))

(define-public crate-serde_test-1.0.100 (c (n "serde_test") (v "1.0.100") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hqziqbqxa9n0ir7ms2fqc6y5v8yxq8jbwq2jv8x4bdm3q5rhvig")))

(define-public crate-serde_test-1.0.101 (c (n "serde_test") (v "1.0.101") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0070ycbh47yhxb5vxwa15vi2wpdkw3v1m14v4mjryz1568fqkbsa")))

(define-public crate-serde_test-1.0.102 (c (n "serde_test") (v "1.0.102") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0g6qg65dmyq277z3aq63hx9xyrwjr3ps48dd4qjxw98s7d2dkn80")))

(define-public crate-serde_test-1.0.103 (c (n "serde_test") (v "1.0.103") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06z3x4sbwlq6dl95pz15n6zwvs3s9qspg0f4ajmp3yscnpdj8by5")))

(define-public crate-serde_test-1.0.104 (c (n "serde_test") (v "1.0.104") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "01276bwlamw0bg4aj7nc3mwv0ws0hcza6kj0adxkhi1pikznvy9k")))

(define-public crate-serde_test-1.0.105 (c (n "serde_test") (v "1.0.105") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1vnp0wld20z1wjr8qp2hxcy6yh2zhicg1mfb0qrzxgwq2a4n6raa")))

(define-public crate-serde_test-1.0.106 (c (n "serde_test") (v "1.0.106") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0liq1w07p2vgcl047wg0avnxfwwf0w6j9wwfwrsnq9cwq6a0dy28")))

(define-public crate-serde_test-1.0.107 (c (n "serde_test") (v "1.0.107") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1snlvarfwmydfbs9l6a1014yz1rkkp9k9g4rfr2zwz06c9p80bip")))

(define-public crate-serde_test-1.0.108 (c (n "serde_test") (v "1.0.108") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ck0h2v1qks3rl3lyc01vhz1fw13q51v9bvx0i9a19634v9y88ik")))

(define-public crate-serde_test-1.0.109 (c (n "serde_test") (v "1.0.109") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ydj7c596zi90cxfqph17y6vjh7k9lnywxw0zr51gyj4fxmi8dxr")))

(define-public crate-serde_test-1.0.110 (c (n "serde_test") (v "1.0.110") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1rm5yb4jyjni578wgpac08iifz5sqka8a3fvdvd3qxn91x0rk88z")))

(define-public crate-serde_test-1.0.111 (c (n "serde_test") (v "1.0.111") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1a67f3i12if22akkkz99gw6s7rgl829df9qkb20cczxhzga88flx")))

(define-public crate-serde_test-1.0.112 (c (n "serde_test") (v "1.0.112") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1wzlgandkyxh4cfiqrxjll2m8dq23pgvj4a9hhb5apdjfwxdj9nk")))

(define-public crate-serde_test-1.0.113 (c (n "serde_test") (v "1.0.113") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02s7zjs12m5abk13j5farc00rzissk1anpl015vawpzz914jsan3")))

(define-public crate-serde_test-1.0.114 (c (n "serde_test") (v "1.0.114") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13cv5a48zgffh92inldsq9mm1gddmw4dn0w3254bs5xgfk8910gm")))

(define-public crate-serde_test-1.0.115 (c (n "serde_test") (v "1.0.115") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ix8ifsdxp61d1rlkcnkgdspc4pzrxn6bsv5r2zjbc0rr050h3jf")))

(define-public crate-serde_test-1.0.116 (c (n "serde_test") (v "1.0.116") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0sswvms89fkwf4l9n64xgabzs9pqqiyi2kiqkx42yjmby71xwglj")))

(define-public crate-serde_test-1.0.117 (c (n "serde_test") (v "1.0.117") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0g3x856rp7idqn1i42gg2a6kl77qrg9vp33mbvyxs3vwg0prx96r")))

(define-public crate-serde_test-1.0.118 (c (n "serde_test") (v "1.0.118") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1028gfy1nszk3n0arfx57n8bdwadg2y6ba8fprh9zlhi8mqziwzp")))

(define-public crate-serde_test-1.0.119 (c (n "serde_test") (v "1.0.119") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1av6ad00brvvcirxha57si3m7pjd19w4i3p9467h0k17ahagpwv0")))

(define-public crate-serde_test-1.0.120 (c (n "serde_test") (v "1.0.120") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0jyiskd75pid7abfxanrxb8j703w26n9kqlvm3sa4kxii5jdkmrx")))

(define-public crate-serde_test-1.0.121 (c (n "serde_test") (v "1.0.121") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0qg4nxiyc87cba47hkzxskcjh51i4dvxch1k5wm3a8lhw784r7pr")))

(define-public crate-serde_test-1.0.122 (c (n "serde_test") (v "1.0.122") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0y3yqc44g70n3rnwf9cagki7a7gxglx7d1d9cprw5m985m3d0sd4")))

(define-public crate-serde_test-1.0.123 (c (n "serde_test") (v "1.0.123") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0sg1qwnz5g5x521n893zwx388ls981k5gsyfijd1vxxx222ml51q")))

(define-public crate-serde_test-1.0.124 (c (n "serde_test") (v "1.0.124") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1fj0zc1ivi8xr7ybjx329zcq1mb15qd5zv66w2pfhqh7zhw95ib1")))

(define-public crate-serde_test-1.0.125 (c (n "serde_test") (v "1.0.125") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zkrx363gf931y9j6l61a6sca922mh3kc637fn8rfnmggvpmzfxl")))

(define-public crate-serde_test-1.0.126 (c (n "serde_test") (v "1.0.126") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bfsp7jg2kxs37mjk3ixazag89blwb40w155kl5hhch5qb8ma45x")))

(define-public crate-serde_test-1.0.127 (c (n "serde_test") (v "1.0.127") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0mnv1zv5y0sg5hihc8vhmhx6255mhlw6v2v146hhh9iyz3r557ny")))

(define-public crate-serde_test-1.0.128 (c (n "serde_test") (v "1.0.128") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0kd0j5ln5xscvzk7bdkzq681rfy883dpyc7r4cc5cffyvkg3s1b4")))

(define-public crate-serde_test-1.0.129 (c (n "serde_test") (v "1.0.129") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0c3qhjc7ir8q79q4455gimgnf6z0b7bsjw1a17j70b7z7sfvykna")))

(define-public crate-serde_test-1.0.130 (c (n "serde_test") (v "1.0.130") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0k1y6nm2aqqlwcma4kfjqqr3lsnvhy969s4hs7ay5smxbli7h8fq")))

(define-public crate-serde_test-1.0.131 (c (n "serde_test") (v "1.0.131") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1i659yysg3b7vmnb4s4bqsm9s85fkqg7h3ljnl7k1fjlcba03anz") (r "1.15")))

(define-public crate-serde_test-1.0.132 (c (n "serde_test") (v "1.0.132") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "000k2a0mfidm57xf8rvbbm2xw43yk8n0mgc9qi941zrm4v7b0f5i") (r "1.15")))

(define-public crate-serde_test-1.0.133 (c (n "serde_test") (v "1.0.133") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lsjrf4pxln39jk2yvsj9fb6gc7zgqqqx3zg70x4msknymr7hlxq") (r "1.15")))

(define-public crate-serde_test-1.0.134 (c (n "serde_test") (v "1.0.134") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1x53rfsnyhspv4s9055d33191bmh30wg051nkkc65rc327hdn5i6") (r "1.15")))

(define-public crate-serde_test-1.0.135 (c (n "serde_test") (v "1.0.135") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1bwdqr37br4ca5fdi44s2arpihb9jwpawk4h3lf4z28qvf2j90jg") (r "1.15")))

(define-public crate-serde_test-1.0.136 (c (n "serde_test") (v "1.0.136") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sdhwz0713y7lzcb83rm3ibix8yhsy6rvrzf03612xyrz6k5nrr1") (r "1.15")))

(define-public crate-serde_test-1.0.137 (c (n "serde_test") (v "1.0.137") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13b24m6xardz1ldsgpc0bqi2k880xm4xvw6j2k1l4hm3mqknh6gy") (r "1.13")))

(define-public crate-serde_test-1.0.138 (c (n "serde_test") (v "1.0.138") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1fbx652jqs8nk2lfv3ifddxfaa16815jibxpvlpybcw5v1rgfkc4") (r "1.13")))

(define-public crate-serde_test-1.0.139 (c (n "serde_test") (v "1.0.139") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pm5ci2mi2p5qqcvszxfra5hgzfw8q39z0jl1nbxs038mi9kd4yc") (r "1.13")))

(define-public crate-serde_test-1.0.140 (c (n "serde_test") (v "1.0.140") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0xd92ric0gkn8g8l8csh3cnccdnalmx98n6lfi6llzj25wd6bc1q") (r "1.13")))

(define-public crate-serde_test-1.0.141 (c (n "serde_test") (v "1.0.141") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mkpfkv4rrh6wihlanxkhgfq92lzxji4av5jlmr7n8bv5lmz5p02") (r "1.13")))

(define-public crate-serde_test-1.0.142 (c (n "serde_test") (v "1.0.142") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1xaw36fnwsd9ii0gyy6p2fnprxh7yld4kd0kc289ah3hs73mby5v") (r "1.13")))

(define-public crate-serde_test-1.0.143 (c (n "serde_test") (v "1.0.143") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1q22dzjalvmhwglil3wrkwkx8r2s63d08n4bil7wgkdwq43v1dfl") (r "1.13")))

(define-public crate-serde_test-1.0.144 (c (n "serde_test") (v "1.0.144") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "09cys8vj85w8j0shbbrh1c6jqckzq5922z3a41vn298z94hkczvc") (r "1.13")))

(define-public crate-serde_test-1.0.145 (c (n "serde_test") (v "1.0.145") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1v9q26vlbw0qix3bfw4d5xdbfvv79wkrwff5nih2c4sr448x45ww") (r "1.13")))

(define-public crate-serde_test-1.0.146 (c (n "serde_test") (v "1.0.146") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1hibbvvjh798iplxvc0rw3vi6j9p1bq61b59znypivx3ay103rqm") (r "1.13")))

(define-public crate-serde_test-1.0.147 (c (n "serde_test") (v "1.0.147") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "155dgiwh5f49j8w3hg5bd2a5flybfnim2dmpj5xvlvsf1r86c5k4") (r "1.13")))

(define-public crate-serde_test-1.0.148 (c (n "serde_test") (v "1.0.148") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0n3m165lk00nl6fa2iv2qzks58rhs57ddb0kjlc1vjl964i7i0av") (r "1.13")))

(define-public crate-serde_test-1.0.149 (c (n "serde_test") (v "1.0.149") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1appk5zqcfircrg6by9d7fcgi4a0wgk03fzz6w48qz6dgmwc8d1l") (r "1.13")))

(define-public crate-serde_test-1.0.150 (c (n "serde_test") (v "1.0.150") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "05vaplwpxi4xakhpia0sbzn3h8gh6675lqb3nnbkjxj0l2991mnj") (r "1.13")))

(define-public crate-serde_test-1.0.151 (c (n "serde_test") (v "1.0.151") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1cxcdmwcm95n8rbx6xl150y5ycbn892gglli10jlzb2x63kppxzq") (r "1.13")))

(define-public crate-serde_test-1.0.152 (c (n "serde_test") (v "1.0.152") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17pdigm0w1wvch7vpnk13199wn3gmkb0883l0hr53qv75l6j249n") (r "1.13")))

(define-public crate-serde_test-1.0.153 (c (n "serde_test") (v "1.0.153") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0f4bgwp8xy9f8436lwy83ggszabh20yd2ighclw7f1zxvs4p7gic") (r "1.19")))

(define-public crate-serde_test-1.0.154 (c (n "serde_test") (v "1.0.154") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "045hryc3y9p3rfpwacx4rx82z3di9nkdljjkf3jhxbphn9n2sbfg") (r "1.19")))

(define-public crate-serde_test-1.0.155 (c (n "serde_test") (v "1.0.155") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0acfr5q7glgbqrl8x5mijil7hapc5k72dvpp51wa6ix3lnbw22wx") (r "1.19")))

(define-public crate-serde_test-1.0.156 (c (n "serde_test") (v "1.0.156") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qn8cmd0wa8a0y3a7pwc9v1yh2lswrfy7agszhzcjrbd8lw31jb4") (r "1.19")))

(define-public crate-serde_test-1.0.157 (c (n "serde_test") (v "1.0.157") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ifx8dy6v0dqr13wk5n0w7qj6zbglcm45csxf36b60lxnbxccca2") (r "1.19")))

(define-public crate-serde_test-1.0.158 (c (n "serde_test") (v "1.0.158") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0vqzr0phk2jykw0jf29z8qdz7mf02vg4jh97iv4mxv0nwlr8z4zn") (r "1.19")))

(define-public crate-serde_test-1.0.159 (c (n "serde_test") (v "1.0.159") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1g22mxzirh20pd629dwxmv1pz7qhrg59f7x1zsjgbylfwijalngj") (r "1.19")))

(define-public crate-serde_test-1.0.160 (c (n "serde_test") (v "1.0.160") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1s3mcs9b3qw59lc6w2d1bkpklh2fjclvy5iszkvmhcljwc0ab59w") (r "1.19")))

(define-public crate-serde_test-1.0.161 (c (n "serde_test") (v "1.0.161") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qv2k8p4z7rzj74ap8a4wrqlr6qz982g9nkj6hsz5qdr00bkkm2x") (r "1.19")))

(define-public crate-serde_test-1.0.162 (c (n "serde_test") (v "1.0.162") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "06vj07bp5435shy87xp1a6n2z0ivspnk1k0rwsxxd02xiy0k2a9b") (r "1.19")))

(define-public crate-serde_test-1.0.163 (c (n "serde_test") (v "1.0.163") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0hfbv2w3lbf8kk04fwcshbgcpkx8jmyqb3gbrd5zv2bv06l6h08h") (r "1.19")))

(define-public crate-serde_test-1.0.164 (c (n "serde_test") (v "1.0.164") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13v4v9qg04iay25gsgv7d6396zjr75j4jf9l3rpaaii51qb3hz3r") (r "1.19")))

(define-public crate-serde_test-1.0.165 (c (n "serde_test") (v "1.0.165") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0al1kxarknggv0naih4jddhmcxbmxy7fd0vm13pcfdi0fkhaqis7") (r "1.19")))

(define-public crate-serde_test-1.0.166 (c (n "serde_test") (v "1.0.166") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1k0jxrs9q4w0mz9warqglxjh2hmhhbxad0jifhx68ljqns2mbiv5") (r "1.19")))

(define-public crate-serde_test-1.0.167 (c (n "serde_test") (v "1.0.167") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "11f0sfzcrwy497kjz4vc4xlc1lrjdh2d1g5pg2jyfp6ifg6jczni") (r "1.19")))

(define-public crate-serde_test-1.0.168 (c (n "serde_test") (v "1.0.168") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0zbgjybjj2h0grjhp52b14jmsgnszzccaacm4l1s9j8dxraciid9") (r "1.19")))

(define-public crate-serde_test-1.0.169 (c (n "serde_test") (v "1.0.169") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1kxjzppjmf5v05p3ynzrdvwnjxl7qzdpadbxjs1r60rfq48vg8n4") (r "1.19")))

(define-public crate-serde_test-1.0.170 (c (n "serde_test") (v "1.0.170") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0nmp64q9fa9mfcn24vrzf6p0rpf1lg5c28ma7s8ksqj5lxmj3426") (r "1.19")))

(define-public crate-serde_test-1.0.171 (c (n "serde_test") (v "1.0.171") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0fjsdcxx1ig10qf73dqwgyy7ksjwmmi3657aaybyqj8l9qphlj5n") (r "1.19")))

(define-public crate-serde_test-1.0.172-alpha.0 (c (n "serde_test") (v "1.0.172-alpha.0") (d (list (d (n "serde") (r "=1.0.172-alpha.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.172-alpha.0") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "=1.0.172-alpha.0") (d #t) (k 2)))) (h "1w1zff9y209xl1x3yhhd1s7c7cb13ij3gkwmcip3zznff567swl7") (r "1.19")))

(define-public crate-serde_test-1.0.172 (c (n "serde_test") (v "1.0.172") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1vy731lm4qyxh93lvnfnjazwga784wm56n34l513fh1l93kx4c3d") (r "1.19")))

(define-public crate-serde_test-1.0.173 (c (n "serde_test") (v "1.0.173") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1nxwv6ly22n53n2bqk0xw00i1k66ig74vns8r5aha4xh70shqs5d") (r "1.19")))

(define-public crate-serde_test-1.0.174 (c (n "serde_test") (v "1.0.174") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0k26z6gzm3szj7z84c2w9n4kzdgcrkfvlicjq7w5bjkr3lgma4wf") (r "1.19")))

(define-public crate-serde_test-1.0.175 (c (n "serde_test") (v "1.0.175") (d (list (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1958cmxbsdkdgx78hrnd9ccbac0gbs5l06vfsip9rbd9gkvz1fi9") (r "1.19")))

(define-public crate-serde_test-1.0.176 (c (n "serde_test") (v "1.0.176") (d (list (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1ar0sqlahx33l0qzw7alk61dp14ma52q42vy9z8i51j9w6n4jbss") (r "1.56")))

