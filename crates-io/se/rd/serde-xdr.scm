(define-module (crates-io se rd serde-xdr) #:use-module (crates-io))

(define-public crate-serde-xdr-0.1.0 (c (n "serde-xdr") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1awij3dd5wm6922khy71n6x82br7l8cnxaz4lsyhxr8zrhzalf1q")))

(define-public crate-serde-xdr-0.2.0 (c (n "serde-xdr") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0380n55nipgj4vwwc9cnzyhl7fhdfyj88jknhzj1r113vb3whzjr")))

(define-public crate-serde-xdr-0.3.0 (c (n "serde-xdr") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0x5c20vdly293v84ksf7f8misjig01dw057ralaz0h3pj7rllb5z")))

(define-public crate-serde-xdr-0.4.0 (c (n "serde-xdr") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1mkfrgf5qiaysxqsha4ycgcngpyrbr43ip7x9qlgmfa5vb7qlwpa")))

(define-public crate-serde-xdr-0.5.0 (c (n "serde-xdr") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1f6mcv48y44g85jcrqgcx9kp8iibwhs5ix5sfbbls3xif7pz2rs9")))

(define-public crate-serde-xdr-0.5.1 (c (n "serde-xdr") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0xf6xsq8268vhrrvmm5dkd0kg4l6qg5bqrc9n7ynwasx7iy9p7hl")))

(define-public crate-serde-xdr-0.6.0 (c (n "serde-xdr") (v "0.6.0") (d (list (d (n "byteorder") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "failure") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "failure_derive") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "ordered-float") (r ">=0.5.0, <0.6.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_bytes") (r ">=0.10.0, <0.11.0") (d #t) (k 2)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0pdhk4bzx6briykdri56871v9zvfp9pf2zb734iykcawcrdkask0") (f (quote (("ignore-enum-variant-names"))))))

