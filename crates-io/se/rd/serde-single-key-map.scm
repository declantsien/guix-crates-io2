(define-module (crates-io se rd serde-single-key-map) #:use-module (crates-io))

(define-public crate-serde-single-key-map-0.1.0 (c (n "serde-single-key-map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kkxr875sd1y56p6nvlkcmri08bx0wb67iz598in19bgrsqsj7lq")))

