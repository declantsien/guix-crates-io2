(define-module (crates-io se rd serde_piecewise_default_derive) #:use-module (crates-io))

(define-public crate-serde_piecewise_default_derive-0.1.0 (c (n "serde_piecewise_default_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1820ndclji143y2kxsf3w28qjpsi8rkfmbi6lqdvywi4lqin7hjj")))

(define-public crate-serde_piecewise_default_derive-0.2.0 (c (n "serde_piecewise_default_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0apz63796hd9rlqdn4vmxgs1472bq0wfkvbr7k1448xjwx9nji0r")))

