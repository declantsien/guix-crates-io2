(define-module (crates-io se rd serde-error) #:use-module (crates-io))

(define-public crate-serde-error-0.1.0 (c (n "serde-error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0adr8ynfjsxf70fwivwb7pl7csqc5smxfc4d6lhalzdkg8x3vjw8")))

(define-public crate-serde-error-0.1.1 (c (n "serde-error") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bycblj86vvk0fhbrzg3kmfz35m15zigb1zcf5zl1h9p16rwhcnw")))

(define-public crate-serde-error-0.1.2 (c (n "serde-error") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gyimf74y9pfd7449zjz921p8h6nys3a3g48k9hnmvdf2ckii279")))

