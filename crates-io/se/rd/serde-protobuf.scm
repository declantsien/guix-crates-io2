(define-module (crates-io se rd serde-protobuf) #:use-module (crates-io))

(define-public crate-serde-protobuf-0.3.0 (c (n "serde-protobuf") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "serde") (r "^0.8.5") (d #t) (k 0)) (d (n "serde-value") (r "^0.3.0") (d #t) (k 2)))) (h "1j0ij6qxj1k78gicsjnclpp77ih3jsv4zxhfgfyg14gz1g7l7pw4")))

(define-public crate-serde-protobuf-0.4.0 (c (n "serde-protobuf") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)) (d (n "serde") (r "^0.8.5") (d #t) (k 0)) (d (n "serde-value") (r "^0.3.0") (d #t) (k 2)))) (h "0hpmh6ya20iv56dl0knb7l2vx67xn2k0clzi0chdyvsngiink4kw")))

(define-public crate-serde-protobuf-0.5.0 (c (n "serde-protobuf") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-value") (r "^0.4.0") (d #t) (k 2)))) (h "1i2vhwbjh0srxdg7p6h3aifb26337j89i2ycpxy2lj30xjlnymbw")))

(define-public crate-serde-protobuf-0.6.0 (c (n "serde-protobuf") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde-value") (r "^0.5.0") (d #t) (k 2)))) (h "0shpc9lgb5hs65slni1z8qrvsrd28z70sgm21nz2dk3rgxmmajrw")))

(define-public crate-serde-protobuf-0.7.0 (c (n "serde-protobuf") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde-value") (r "^0.5.0") (d #t) (k 2)))) (h "00i65dp214sg5hzazw5axdsdlxvn5rbaz78c67b5h61fjrahmfq1")))

(define-public crate-serde-protobuf-0.8.0 (c (n "serde-protobuf") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.86") (d #t) (k 0)) (d (n "serde-value") (r "^0.5.3") (d #t) (k 2)))) (h "06mhb0cabg8nzh4147zd46n441k1nv08h8ja7f6nj84bnp6qbym3")))

(define-public crate-serde-protobuf-0.8.1 (c (n "serde-protobuf") (v "0.8.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.86") (d #t) (k 0)) (d (n "serde-value") (r "^0.5.3") (d #t) (k 2)))) (h "1d20sd4vmihgybcs5b1qvnda73dhh75sbvnlm8139g2qji4bmxh7")))

(define-public crate-serde-protobuf-0.8.2 (c (n "serde-protobuf") (v "0.8.2") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "protobuf") (r "^2.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1r1j5iqv35kn7cqjf35byj2r531wg9sw4njrdcp86cm7z69n8viq")))

