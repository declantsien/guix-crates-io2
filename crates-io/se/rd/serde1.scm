(define-module (crates-io se rd serde1) #:use-module (crates-io))

(define-public crate-serde1-1.0.0 (c (n "serde1") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (k 0)))) (h "0zdv92ir29yqi2d19ycyrpjpg2b50yfzn0cxid5bcgcz9z3gcxsq") (f (quote (("unstable" "serde/unstable") ("std" "serde/std") ("rc" "serde/rc") ("playground" "serde/playground") ("derive" "serde/derive") ("default" "std") ("collections" "serde/collections") ("alloc" "serde/alloc"))))))

(define-public crate-serde1-1.0.1 (c (n "serde1") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (k 0)))) (h "04pjsx9kz7432hrlcqhb6amsxrf5vk21ja7kilhzqw4l9ccsnd7b") (f (quote (("unstable" "serde/unstable") ("std" "serde/std") ("rc" "serde/rc") ("derive" "serde/derive") ("default" "std") ("alloc" "serde/alloc"))))))

