(define-module (crates-io se rd serde-generate-bin) #:use-module (crates-io))

(define-public crate-serde-generate-bin-0.1.0 (c (n "serde-generate-bin") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-generate") (r "^0.25.0") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.6") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.6") (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0sjdm3xq1dvlrhwaylxk7hll71rkw0sc50a9jkssvkd8z713na0d") (r "1.60")))

