(define-module (crates-io se rd serde_mtproto_derive) #:use-module (crates-io))

(define-public crate-serde_mtproto_derive-0.1.0 (c (n "serde_mtproto_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1z85z8ib2q41xsmlq3ab3kgxpn6jgag8npvpv7p3w24n0swrrnzq")))

(define-public crate-serde_mtproto_derive-0.2.0 (c (n "serde_mtproto_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "11g7g8rnmfg7l5ql1ijpnx3vq6rdjdp42j1n8w85r6jc7ryfmjg7")))

(define-public crate-serde_mtproto_derive-0.3.0 (c (n "serde_mtproto_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "071c3b0fmwvpkpkmav1l167mdyzl6mmnw9yyp41s47h26dh31ghr")))

(define-public crate-serde_mtproto_derive-0.3.1 (c (n "serde_mtproto_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "073q2ddmwmgjspkjjapmmz7al9krl7ymcqzr0jaicchwvp3kq0zb")))

