(define-module (crates-io se rd serde-filter) #:use-module (crates-io))

(define-public crate-serde-filter-0.1.0 (c (n "serde-filter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0x7wlydc9ghrcss31nfiljan2218dr46vcjfbakgq33m476sffng")))

(define-public crate-serde-filter-0.1.1 (c (n "serde-filter") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "10ckfj8a8pwq6vihadjmal15l57mv6j5f4rh7a1658973y6f7ijq")))

