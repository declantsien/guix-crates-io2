(define-module (crates-io se rd serde_fs) #:use-module (crates-io))

(define-public crate-serde_fs-0.1.0 (c (n "serde_fs") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "derive-error") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1alqzpqh4flg0jxgzrikd223hpm74calygvka4dr4xvm2qccf26z")))

(define-public crate-serde_fs-0.2.0 (c (n "serde_fs") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "derive-error") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1nn6b2qin7bpfihi4rn4zjvcsldcib2gznpyjyq3jc01c6k0d22b")))

