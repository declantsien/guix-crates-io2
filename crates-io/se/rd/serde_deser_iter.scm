(define-module (crates-io se rd serde_deser_iter) #:use-module (crates-io))

(define-public crate-serde_deser_iter-0.1.0 (c (n "serde_deser_iter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0kijbjzw3jfpqqiirgwccknxpmq1cdd9ydqnfc0q6r7w8adg1n0k") (r "1.65")))

