(define-module (crates-io se rd serde-lite) #:use-module (crates-io))

(define-public crate-serde-lite-0.1.0 (c (n "serde-lite") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00dzrnk21a89vgr8y6dsqsvayx5g3inhm5hzijkbxn7ppg65m0sh") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.1.1 (c (n "serde-lite") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zkg5hl4lll7ivdz9j3qz112ra3xsnal21ghl8lq4pd7x4jrn44x") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.2.0 (c (n "serde-lite") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08fd9g520494f4cjw0838a7v8vgxcdmi33bdzp0ij73z2rz3lq59") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.3.0 (c (n "serde-lite") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d2rakvl29fynl5kckzh3gk12658s3w2x10xj8pagy2izh3kpr2b") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.3.1 (c (n "serde-lite") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.3.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19hwkmxmp0qy6lj0hj2qma6vfsflhcji43fcnvcpv26r76j278mw") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.3.2 (c (n "serde-lite") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0892w7c1c2k67dgmjdvinjn0pjpxcycfh8pvqydzmngb3g0fxp82") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.4.0 (c (n "serde-lite") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16bn8fvb5h7m9qw78k6x8s59vi5lwcxq0v4c26d66p8dhdcmah44") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

(define-public crate-serde-lite-0.5.0 (c (n "serde-lite") (v "0.5.0") (d (list (d (n "indexmap") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde-lite-derive") (r "=0.5.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0v01mq8z1a1nbnrjsr5zrhmkfmml31gnf9nnm2dhv7qsp5x2gs7r") (f (quote (("preserve-order" "indexmap") ("derive" "serde-lite-derive") ("default"))))))

