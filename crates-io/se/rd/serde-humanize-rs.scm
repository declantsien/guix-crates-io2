(define-module (crates-io se rd serde-humanize-rs) #:use-module (crates-io))

(define-public crate-serde-humanize-rs-0.1.0 (c (n "serde-humanize-rs") (v "0.1.0") (d (list (d (n "humanize-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18k85kgjx4882g8r1ax4m7k2bjk0s17wpzzh8mvdhbw8a3xzcwqi")))

(define-public crate-serde-humanize-rs-0.1.1 (c (n "serde-humanize-rs") (v "0.1.1") (d (list (d (n "humanize-rs") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kf20c1rm3wbf29bgjca9jjlglijvi022v1ay0m74wnxacidlbfk")))

