(define-module (crates-io se rd serde-json-wasm) #:use-module (crates-io))

(define-public crate-serde-json-wasm-0.1.0 (c (n "serde-json-wasm") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "1ydj488z65f1rb12kz1mkvpbr2m313dgii5k2v2qcbgdcf3pbrx1")))

(define-public crate-serde-json-wasm-0.1.1 (c (n "serde-json-wasm") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "1fsd7nsml0kws0l0b0w813id2sfz0lgnk5w1ykncnnvj53dpnvnr")))

(define-public crate-serde-json-wasm-0.1.2 (c (n "serde-json-wasm") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "1dfy2z9q5mbpwxwbblsni33nwi9p54ra1srk0q9d8gci15wyfddi")))

(define-public crate-serde-json-wasm-0.1.3 (c (n "serde-json-wasm") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0hzl6jcp5s29fvv5jhi3xw8zwhsqsjk3h4sv34m2b5z2hja51kvp")))

(define-public crate-serde-json-wasm-0.2.1 (c (n "serde-json-wasm") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "0gw257nzqlpp3mpsl81ddrklq1ypj6jmw1kwd4s2678g756xk53j")))

(define-public crate-serde-json-wasm-0.2.2 (c (n "serde-json-wasm") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0ln7h2dnw1p9v5zz56ccc53pcabqlcm6ylsv11k5223rnqz37105")))

(define-public crate-serde-json-wasm-0.2.3 (c (n "serde-json-wasm") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0dqyyxykczyy2g43rvzyrx8jl0wnp8idbvkwrldfj5k661rss2qj")))

(define-public crate-serde-json-wasm-0.3.0 (c (n "serde-json-wasm") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0ghj064k0psy9l7qzqj3jkxpk29y9y7rd0qny212niw3hs977gvf")))

(define-public crate-serde-json-wasm-0.3.1 (c (n "serde-json-wasm") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0jflvv0ifqcp460595d8hikph5qi2fx27m3z8prlbyn85rkz7vjh")))

(define-public crate-serde-json-wasm-0.3.2 (c (n "serde-json-wasm") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0lqqa3s7ycd2dn3g3f3y1mvr4wb13nnrn4rlkla8an3yv6bc8ah4")))

(define-public crate-serde-json-wasm-0.4.0 (c (n "serde-json-wasm") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "01vv9pd5rixl6cp4p042f2i25fq9iv4nh4ymgxqlif560jyf5l8v")))

(define-public crate-serde-json-wasm-0.4.1 (c (n "serde-json-wasm") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "1989psk4gq2918vnag2g9i052cl96jw52a4hrvl3x88w82y4v6s7")))

(define-public crate-serde-json-wasm-0.5.0 (c (n "serde-json-wasm") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0n1s5jzjjj4wz7fyi29a0b189s6x5nc2h9hl9qzmq5nx0jdywnx1")))

(define-public crate-serde-json-wasm-0.5.1 (c (n "serde-json-wasm") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0dy1z2qyzvxdpirfdhn3p3kss7cpd2sg5j6a9ar2h60ymlgjm9hn")))

(define-public crate-serde-json-wasm-1.0.0 (c (n "serde-json-wasm") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "0yx61f8035h6nrxyy2z5hfpwkq9dfxy1xbqigkvvbxmhyc1pvhw3") (f (quote (("unstable" "serde/unstable") ("std" "serde/std") ("default" "std"))))))

(define-public crate-serde-json-wasm-0.5.2 (c (n "serde-json-wasm") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.80") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "1mzx9dvnlmzcfnmrkgmrzm1a52419ajngrw1vnwa1yjkgnh174ly")))

(define-public crate-serde-json-wasm-1.0.1 (c (n "serde-json-wasm") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 2)))) (h "19f9z0m31xvz8f9ynj76bqh5nhnfx709s2fmzyyraifxag8s0pgh") (f (quote (("unstable" "serde/unstable") ("std" "serde/std") ("default" "std"))))))

