(define-module (crates-io se rd serde-reflection-aptos) #:use-module (crates-io))

(define-public crate-serde-reflection-aptos-0.3.5 (c (n "serde-reflection-aptos") (v "0.3.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "05aqlym69s5lx33z0b2dzhikybnvyizm98x9q2v19z5yddh68j60") (y #t)))

