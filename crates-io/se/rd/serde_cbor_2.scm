(define-module (crates-io se rd serde_cbor_2) #:use-module (crates-io))

(define-public crate-serde_cbor_2-0.12.0-dev (c (n "serde_cbor_2") (v "0.12.0-dev") (d (list (d (n "half") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.14") (k 0)) (d (n "serde_derive") (r "^1.0.14") (k 2)))) (h "0mfxl2b08f5w1xyq7740kf4vvyqnsqrg804vpvfiw7z097s7avdl") (f (quote (("unsealed_read_write") ("tags") ("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

