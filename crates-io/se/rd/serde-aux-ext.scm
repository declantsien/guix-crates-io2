(define-module (crates-io se rd serde-aux-ext) #:use-module (crates-io))

(define-public crate-serde-aux-ext-0.1.0 (c (n "serde-aux-ext") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^3.1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rfpm85fi0qkqrvraqva3fgm1rrw3krrf782fvhsfz2drf4pffwj")))

(define-public crate-serde-aux-ext-0.1.1 (c (n "serde-aux-ext") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0584rs88q7c6l57aqr90gkgk4l1xdpx3s81w6qkby1s0lxayhllz")))

(define-public crate-serde-aux-ext-0.2.0 (c (n "serde-aux-ext") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bf0746dawimi4hj2xhdskkvl87vd8mjifp21jp54n69g7nfl8zh") (r "1.58.0")))

