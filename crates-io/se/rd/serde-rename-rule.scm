(define-module (crates-io se rd serde-rename-rule) #:use-module (crates-io))

(define-public crate-serde-rename-rule-0.1.0 (c (n "serde-rename-rule") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (o #t) (k 0)))) (h "1i6gszq27yk663w37fc9hdy38xzvf3zpp19pg37izb4i8ds0hgl2") (f (quote (("_rustversion_before_1_26_0" "rustversion"))))))

(define-public crate-serde-rename-rule-0.1.1 (c (n "serde-rename-rule") (v "0.1.1") (d (list (d (n "rustversion") (r "^1.0") (o #t) (k 0)))) (h "00f3rg69rrhpvxscjz1pd4xxny0q55n971vpakr1lpj0ban51d94") (f (quote (("_rustversion_before_1_26_0" "rustversion"))))))

(define-public crate-serde-rename-rule-0.1.2 (c (n "serde-rename-rule") (v "0.1.2") (d (list (d (n "rustversion") (r "^1.0") (o #t) (k 0)))) (h "0cvw16syahdx2i3lc0n8lrif9kzxnvrabdp53x7yzk0l6w830agx") (f (quote (("_rustversion_before_1_26_0" "rustversion"))))))

(define-public crate-serde-rename-rule-0.2.0 (c (n "serde-rename-rule") (v "0.2.0") (h "02b48ykx6ld33qfkksw3d5zbqrgcpm8b5prw7pagi59xqhq4b0j7") (f (quote (("std") ("default" "std")))) (r "1.58.0")))

(define-public crate-serde-rename-rule-0.2.1 (c (n "serde-rename-rule") (v "0.2.1") (h "1qb8myj5zfp6njzndpihb24xkgqzhvrkh57q8bw64z0xw5fq2sma") (f (quote (("std") ("default" "std")))) (r "1.58.0")))

(define-public crate-serde-rename-rule-0.2.2 (c (n "serde-rename-rule") (v "0.2.2") (h "0w5ysc7sjjswvpsgcwzvczy3i44knzz53ip57cg71z1689bl8kkr") (f (quote (("std") ("default" "std")))) (r "1.58.0")))

