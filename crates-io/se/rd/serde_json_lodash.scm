(define-module (crates-io se rd serde_json_lodash) #:use-module (crates-io))

(define-public crate-serde_json_lodash-0.1.0 (c (n "serde_json_lodash") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "065737z2k4fwwr7j6kqlz7d2bwc0kzf7mvcivnlzvyfda15h3jal")))

(define-public crate-serde_json_lodash-0.1.1 (c (n "serde_json_lodash") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18gwc3laczfcxdn5s2v5ff2r47wgr5cnck58g7pmm451lmvn2sas")))

(define-public crate-serde_json_lodash-0.1.5 (c (n "serde_json_lodash") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00wk4ghvfc3q6pdnpg7pv8pr1lp503m7m4llxqs2xg14q1w60mr0")))

(define-public crate-serde_json_lodash-0.1.11 (c (n "serde_json_lodash") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bbr765pcmc19a9hmq3cnqjc0v39m020nsnqddn2r1jkkg3kqaz6") (f (quote (("camel" "paste"))))))

(define-public crate-serde_json_lodash-0.1.13 (c (n "serde_json_lodash") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "052c5hpjic0hiqhqjfgjjc9d8jch46h57clg1ghcarz9m7b7kfsm") (f (quote (("camel" "paste") ("all" "camel" "lazy_static"))))))

(define-public crate-serde_json_lodash-0.1.14 (c (n "serde_json_lodash") (v "0.1.14") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11gnm3hj6a464mxril8hmlxg1q6fris4jsrmkp23ikfpfd5z761k") (f (quote (("camel" "paste") ("all" "camel" "lazy_static"))))))

(define-public crate-serde_json_lodash-0.1.16 (c (n "serde_json_lodash") (v "0.1.16") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kjfqhx8lxljgj8gp8ph01jrnagnjgrdbjc6h2j4r9ww6f22xldc") (f (quote (("camel" "paste") ("all" "camel" "lazy_static"))))))

