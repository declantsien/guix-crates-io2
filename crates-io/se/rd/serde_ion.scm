(define-module (crates-io se rd serde_ion) #:use-module (crates-io))

(define-public crate-serde_ion-0.1.0 (c (n "serde_ion") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_stacker") (r "^0.1") (d #t) (k 0)))) (h "03whjz0pq8h7mk495jpyqknpfyaqnb6ii0bg1mc20krc7zmkbvlr")))

