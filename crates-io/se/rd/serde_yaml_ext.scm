(define-module (crates-io se rd serde_yaml_ext) #:use-module (crates-io))

(define-public crate-serde_yaml_ext-0.1.0 (c (n "serde_yaml_ext") (v "0.1.0") (d (list (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "0d2izr8zq0dfjqwsamldwrciq5axrvvkj8km6z8bnyrdn03fnn09")))

(define-public crate-serde_yaml_ext-0.1.1 (c (n "serde_yaml_ext") (v "0.1.1") (d (list (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "1a2f00kjgsv6h6hrxd5zav0x87h98bjpail7ml3k04pd1xpkbxa4")))

