(define-module (crates-io se rd serde_ethabi) #:use-module (crates-io))

(define-public crate-serde_ethabi-0.2.1 (c (n "serde_ethabi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c6gvfiq5h707samjnh9s29vdzdf4glh9bck483qw6wwyd8bqdkp")))

(define-public crate-serde_ethabi-0.2.2 (c (n "serde_ethabi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06ch4m6nrk9v64i7hwz6q3asj8jxlsgifh9ffx2z076v8bdnyyfj")))

