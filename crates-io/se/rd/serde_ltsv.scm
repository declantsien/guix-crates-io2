(define-module (crates-io se rd serde_ltsv) #:use-module (crates-io))

(define-public crate-serde_ltsv-0.1.0 (c (n "serde_ltsv") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1slkyqvm7jggpi47jhzq7q9wfnqzmrxvk8hzjvsidxf84qnfnsn6")))

