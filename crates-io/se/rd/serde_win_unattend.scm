(define-module (crates-io se rd serde_win_unattend) #:use-module (crates-io))

(define-public crate-serde_win_unattend-0.1.0 (c (n "serde_win_unattend") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "13b084zpf0rl17kwn6lfcyvxgbygj4g2i2n35863w5hnw69hhj6b")))

(define-public crate-serde_win_unattend-0.2.0 (c (n "serde_win_unattend") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yx87j4j0fmyh8m1hbibh1zh5aazvrx6qx66vbfs8c2hz6z9kva6")))

(define-public crate-serde_win_unattend-0.3.0 (c (n "serde_win_unattend") (v "0.3.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "18fyc24qy4bpmj4zdhis3x7z2hsswpkn59kgjznxw5qjn4r0ldii")))

(define-public crate-serde_win_unattend-0.3.1 (c (n "serde_win_unattend") (v "0.3.1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dxzijqx8cmbszffmm275c987nbi4j6snb33j2mdg3j5lnsh6xfb")))

(define-public crate-serde_win_unattend-0.3.2 (c (n "serde_win_unattend") (v "0.3.2") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "07nphl509az5ql2c9qp6637yhzchwpzl11rvy2x81lp60nd3ihwi")))

(define-public crate-serde_win_unattend-0.3.3 (c (n "serde_win_unattend") (v "0.3.3") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qq1vgvfw01aadf01h75lxg5zpsh4drl0lysyrsdwsr2327ap2dr")))

