(define-module (crates-io se rd serde_ini) #:use-module (crates-io))

(define-public crate-serde_ini-0.0.1 (c (n "serde_ini") (v "0.0.1") (d (list (d (n "result") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1kd4bdpa1lh2rnpkcsllx3kcqsf22xlk1gdn6fv2y6nzr3zzchak")))

(define-public crate-serde_ini-0.1.0 (c (n "serde_ini") (v "0.1.0") (d (list (d (n "result") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1y9hxpsdxgawzb01r5qs84ihxv9igyp1mmffi508pabgw98iy2d7")))

(define-public crate-serde_ini-0.1.1 (c (n "serde_ini") (v "0.1.1") (d (list (d (n "result") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "061la6h7wmjw5haxx5nz8dwng23mahzkimgz5a0z0hcn5gmvhydk")))

(define-public crate-serde_ini-0.1.2 (c (n "serde_ini") (v "0.1.2") (d (list (d (n "result") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1rf31i09svza15bl1bhrfqxl9c0hp46kdvb6s52jmr65ljzmg20a")))

(define-public crate-serde_ini-0.1.3 (c (n "serde_ini") (v "0.1.3") (d (list (d (n "result") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1vgvgrcda3f5w2gyanvnraalgjrwxf07m7n76b7nm2imi2a54zmw")))

(define-public crate-serde_ini-0.2.0 (c (n "serde_ini") (v "0.2.0") (d (list (d (n "result") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0f8ir1bbcdyad50aj1c53dkiwr24x6dr88f045skl1xvwa3nc8zb")))

