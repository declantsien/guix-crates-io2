(define-module (crates-io se rd serde_yaml_with_quirks) #:use-module (crates-io))

(define-public crate-serde_yaml_with_quirks-0.8.17 (c (n "serde_yaml_with_quirks") (v "0.8.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0dsx6rxpa2qn8llfsnb5jrjxs3jzb6mqxvr5abmg7c2rfj3awy5b")))

(define-public crate-serde_yaml_with_quirks-0.8.24 (c (n "serde_yaml_with_quirks") (v "0.8.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "indexmap") (r "^1.5.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "06ijk200hscj0rqmi5pqywxb5zyccl8gndcc0p8f5sl6p8z9iia7") (r "1.46")))

