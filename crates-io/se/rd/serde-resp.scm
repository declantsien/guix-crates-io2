(define-module (crates-io se rd serde-resp) #:use-module (crates-io))

(define-public crate-serde-resp-0.1.0 (c (n "serde-resp") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0cbmpmms7gdmmwm05l01grjxpq7p5jdaiw2lhp0vldil8ms1yzc6")))

(define-public crate-serde-resp-0.2.0 (c (n "serde-resp") (v "0.2.0") (d (list (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0mq07yhwhyds0yk0icz8vcpbb676cmhas2yx75sc2843hxlk4bmd")))

(define-public crate-serde-resp-0.3.0 (c (n "serde-resp") (v "0.3.0") (d (list (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "08ndw2vc6wyq1afvqrj815arh4yng6hid87lb2l7vn4wrw2xsbm5")))

(define-public crate-serde-resp-0.3.1 (c (n "serde-resp") (v "0.3.1") (d (list (d (n "itoa") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0dc2fpzmlj1rhvd08ywscf4l70aigh0j9rjjws6lrkm32k1ms7yf")))

