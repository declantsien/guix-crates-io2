(define-module (crates-io se rd serde-automerge) #:use-module (crates-io))

(define-public crate-serde-automerge-0.1.0 (c (n "serde-automerge") (v "0.1.0") (d (list (d (n "automerge") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0i92y8ga5rh3z7m3vfh6cqz0x0hridxrwk4q6h7q3rpqxf7q081c")))

