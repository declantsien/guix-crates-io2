(define-module (crates-io se rd serde_serializer_quick_unsupported) #:use-module (crates-io))

(define-public crate-serde_serializer_quick_unsupported-0.1.0 (c (n "serde_serializer_quick_unsupported") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)))) (h "0qsciz1yx5hc31q4njlah23g73qn56kc1amz9f0fhia38ain12ci")))

(define-public crate-serde_serializer_quick_unsupported-0.1.1 (c (n "serde_serializer_quick_unsupported") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)))) (h "13mfgkj9lpvfpai2l5c4ap4pagnn3ga6z8kqw7i2wz12sh8g8681")))

(define-public crate-serde_serializer_quick_unsupported-0.1.2 (c (n "serde_serializer_quick_unsupported") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 2)))) (h "15jkmlcqxx6pab555wdpjli4xkic26sb28117n8wcychpifmrdny")))

