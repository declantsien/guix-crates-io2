(define-module (crates-io se rd serde-enum) #:use-module (crates-io))

(define-public crate-serde-enum-0.1.0 (c (n "serde-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qaag8z1f9hd3fr23s53jg22w5z9jb3w489aasv3mdsnyy9whm73")))

(define-public crate-serde-enum-0.1.1 (c (n "serde-enum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07nin9cgcrpyyil5qy4wrclyx7bf5p3as609wl45yc9zxrdh36z9")))

(define-public crate-serde-enum-0.1.2 (c (n "serde-enum") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04ahpqxazny0n444v38qnsmdwdiv57q7ccxz1b0f8708263rz105")))

(define-public crate-serde-enum-0.1.3 (c (n "serde-enum") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yl7af3vgy2c8288x1rda7g8i6qvyhqx0ywm0xbwv77lq8xjx3hw")))

(define-public crate-serde-enum-0.1.4 (c (n "serde-enum") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02wp3qf4m5qc2llgl1089pv81h9wlakybvivnnrzx9dj5gsj96zp")))

