(define-module (crates-io se rd serde_enabled) #:use-module (crates-io))

(define-public crate-serde_enabled-0.1.0 (c (n "serde_enabled") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 2)))) (h "1xw6091pjmgf1yacdbgiplq01qw1z9w9ii1smzfh7rdyjk675phk")))

