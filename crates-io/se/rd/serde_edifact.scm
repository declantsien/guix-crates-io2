(define-module (crates-io se rd serde_edifact) #:use-module (crates-io))

(define-public crate-serde_edifact-0.1.0 (c (n "serde_edifact") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y9srgvkx7g7yff33vr4qp3s3afd4cnxifw08iyqb12ck9bjkif2") (f (quote (("debug"))))))

(define-public crate-serde_edifact-0.2.0 (c (n "serde_edifact") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18lbdyag0rsymjgy5pqx5608yzwjcbbicrq7w96437zlzbipxhj6") (f (quote (("debug"))))))

(define-public crate-serde_edifact-0.3.0 (c (n "serde_edifact") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19rcn3k24h27jnkvqnc4mspa8y1rd4agrlqx34r4znqfs2gb5cj5") (f (quote (("debug"))))))

(define-public crate-serde_edifact-0.4.0 (c (n "serde_edifact") (v "0.4.0") (d (list (d (n "edifact-types") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wnlgamwkhp0jrircwb3jkxa7z9y9rfbyg2f2z7pzc2ay5x6dmbp") (f (quote (("debug"))))))

