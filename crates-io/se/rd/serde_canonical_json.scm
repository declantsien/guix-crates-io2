(define-module (crates-io se rd serde_canonical_json) #:use-module (crates-io))

(define-public crate-serde_canonical_json-1.0.0 (c (n "serde_canonical_json") (v "1.0.0") (d (list (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (f (quote ("std" "perf"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 2)))) (h "0r847bi6jh522r8150as5ji5y5827lwmhqlkz8jfjg0zcvi4xy9y")))

