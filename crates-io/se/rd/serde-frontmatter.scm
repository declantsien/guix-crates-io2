(define-module (crates-io se rd serde-frontmatter) #:use-module (crates-io))

(define-public crate-serde-frontmatter-0.1.0 (c (n "serde-frontmatter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "04yyfa4k172khb67diwk5lgx29rm94q65gmhx4glf548ffcgzwm5")))

