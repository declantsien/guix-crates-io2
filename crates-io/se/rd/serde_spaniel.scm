(define-module (crates-io se rd serde_spaniel) #:use-module (crates-io))

(define-public crate-serde_spaniel-0.1.0 (c (n "serde_spaniel") (v "0.1.0") (d (list (d (n "rustyline") (r "^6.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1bjppww8pxh20lqvcdxlg7hv6g8r5rh37hhrm9ww9jzb59a8mah4") (f (quote (("u8i8_variants") ("stdio") ("default" "stdio" "rustyline" "u8i8_variants"))))))

(define-public crate-serde_spaniel-0.2.0 (c (n "serde_spaniel") (v "0.2.0") (d (list (d (n "rustyline") (r "^7.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1wzrpyrkwwqdbn5jdrw74pvm3qkmkla8kx3lsqcwabvf6kii40c8") (f (quote (("u8i8_variants") ("stdio") ("default" "stdio" "rustyline" "u8i8_variants"))))))

(define-public crate-serde_spaniel-0.3.0 (c (n "serde_spaniel") (v "0.3.0") (d (list (d (n "rustyline") (r "^8.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "04lbbrmdwmqmiqbcbaxy6r48a7rgmn1gqh20c3khmcxvhd7j2pkz") (f (quote (("u8i8_variants") ("stdio") ("default" "stdio" "rustyline" "u8i8_variants"))))))

(define-public crate-serde_spaniel-0.4.0 (c (n "serde_spaniel") (v "0.4.0") (d (list (d (n "rustyline") (r "^10.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0pvimwaalhkgnnysmi5sqh8v7f0y0py5dkbisraxnglwnqns5ghq") (f (quote (("u8i8_variants") ("stdio") ("default" "stdio" "rustyline" "u8i8_variants"))))))

