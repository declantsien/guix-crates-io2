(define-module (crates-io se rd serde-aco) #:use-module (crates-io))

(define-public crate-serde-aco-0.2.0 (c (n "serde-aco") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dq8636n95khswv8a2rnynjhnrjg8lrvsyxypzn2m6vn4jjmmypd")))

