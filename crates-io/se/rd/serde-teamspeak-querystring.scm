(define-module (crates-io se rd serde-teamspeak-querystring) #:use-module (crates-io))

(define-public crate-serde-teamspeak-querystring-0.2.0 (c (n "serde-teamspeak-querystring") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lexical") (r "^6.0") (f (quote ("std" "parse-integers" "parse-floats"))) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "1aylpa5calsrnbgq6g6i4b5pf1gc55qlgz4nsgnq5sdri2vdx0jn")))

(define-public crate-serde-teamspeak-querystring-0.2.1 (c (n "serde-teamspeak-querystring") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lexical") (r "^6.0") (f (quote ("std" "parse-integers" "parse-floats"))) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xlszgskqq4xlrf0phnmm3cpn4wqznsbhpxdffznrixhgdnv691n")))

(define-public crate-serde-teamspeak-querystring-0.3.1 (c (n "serde-teamspeak-querystring") (v "0.3.1") (d (list (d (n "atoi") (r "^2.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ssq84vi800ri7hlif9p42z2mwsxwlp9xsigw58c6c6kl5mr4d9n")))

