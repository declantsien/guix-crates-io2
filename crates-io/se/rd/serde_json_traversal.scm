(define-module (crates-io se rd serde_json_traversal) #:use-module (crates-io))

(define-public crate-serde_json_traversal-0.1.0 (c (n "serde_json_traversal") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mxb7lwwh18q0gb4q3g26kg8si473vqzpzd9cnaiav7k7ipxgadz")))

(define-public crate-serde_json_traversal-0.2.0 (c (n "serde_json_traversal") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0g1jg60ypkxi5x8dsvvmdxx34x0hkkjxv3v4r45zrcpkl6dg2qw3")))

