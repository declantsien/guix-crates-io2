(define-module (crates-io se rd serde_stacker) #:use-module (crates-io))

(define-public crate-serde_stacker-0.1.0 (c (n "serde_stacker") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1jn54i5m1mlc6nm47f96k85fgjs9mhpbbqa4dvd5xjbivkdw55ic")))

(define-public crate-serde_stacker-0.1.1 (c (n "serde_stacker") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "065r7lx10ivxximw2ln3xgi2kry3ffd5k328h3wc2c3v2k1298dw")))

(define-public crate-serde_stacker-0.1.2 (c (n "serde_stacker") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1lap5i2vp0j2pwf7wbrj3xfbw5w1b2qja5c2mcafs23c84dx1iq9")))

(define-public crate-serde_stacker-0.1.3 (c (n "serde_stacker") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "09df7w3ra7g7gdjdjh6dzlgm6g6r56gxknxkahr0x1x9m8ycxl1y")))

(define-public crate-serde_stacker-0.1.4 (c (n "serde_stacker") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1qlfpy0nmxrvahz4hs9p1y84rb0vy6mbxn1lfgvq6fryls8j7jgl")))

(define-public crate-serde_stacker-0.1.5 (c (n "serde_stacker") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "04lc95zm7hr9p08raqy0y2rsnwa31q99c3kz3jkh4mjbzfsh4bhl") (r "1.36")))

(define-public crate-serde_stacker-0.1.6 (c (n "serde_stacker") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1216slxay1m3vh606wxlji8335c6kpid5s13cirikhz90gpjfwh3") (r "1.36")))

(define-public crate-serde_stacker-0.1.7 (c (n "serde_stacker") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1zgwd22cswfsjsxmnpf97nw5fzyv0s6mif5blbb948q7qgskvxrm") (r "1.36")))

(define-public crate-serde_stacker-0.1.8 (c (n "serde_stacker") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "0pasl4wgfyv2xs4nal4a5n8qj2v75nr7m89rwv8fqg0hq7s5fm9g") (r "1.36")))

(define-public crate-serde_stacker-0.1.9 (c (n "serde_stacker") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)))) (h "13kz9x2f1bhv6n997ydkykkhz479ki25wm6jxmr5rfsj1dxh2njp") (r "1.36")))

(define-public crate-serde_stacker-0.1.10 (c (n "serde_stacker") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.166") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)))) (h "0c0wiapmxgb7xq562c50mcwlv0baz27p0y6czif7nzpph3k22lvz") (r "1.56")))

(define-public crate-serde_stacker-0.1.11 (c (n "serde_stacker") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (f (quote ("unbounded_depth"))) (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)))) (h "0vi72mq3xdrnif8w12b3n4pdqssighy5bkqfgxjq1zvkazzwrgxs") (r "1.56")))

