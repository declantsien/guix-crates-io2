(define-module (crates-io se rd serde-strz) #:use-module (crates-io))

(define-public crate-serde-strz-1.0.0 (c (n "serde-strz") (v "1.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ri7h0pfp5gz0h2x1slp2d5hlvvsvvc442vkx3n1afsinz22ildl")))

(define-public crate-serde-strz-1.1.0 (c (n "serde-strz") (v "1.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0a1frz9zjfj7qqnz8rga26yvxmwbqdbz7gmdk7rxibzi28c2a9yv")))

(define-public crate-serde-strz-1.1.1 (c (n "serde-strz") (v "1.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1l5j0jlz5cy1xnlcviabp8i99b606lrnbvv6z7m9yr0xwhijl01b")))

