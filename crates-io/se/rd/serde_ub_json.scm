(define-module (crates-io se rd serde_ub_json) #:use-module (crates-io))

(define-public crate-serde_ub_json-0.1.0 (c (n "serde_ub_json") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jhqxh3x36mihyv9n2xci0bfks0yl9ql16akqki72xpqw9bgmd4c")))

