(define-module (crates-io se rd serde_roundtrip_derive) #:use-module (crates-io))

(define-public crate-serde_roundtrip_derive-0.1.0 (c (n "serde_roundtrip_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("fold"))) (d #t) (k 0)))) (h "1wz53qmllgywmld37ppr97wvdsrzfc79ayh4xb38bqjghgm5ry6j")))

(define-public crate-serde_roundtrip_derive-0.1.1 (c (n "serde_roundtrip_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("fold"))) (d #t) (k 0)))) (h "1z9msh1c5xrj35c050g6x2jsvvr9m99mni1vy7pd9kpbslbbnfl3")))

(define-public crate-serde_roundtrip_derive-0.1.2 (c (n "serde_roundtrip_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("fold"))) (d #t) (k 0)))) (h "1d15r7grrc72s4qxs7l6v4rp3mvw290jcqxc9gc1lf962pbwvdmf")))

