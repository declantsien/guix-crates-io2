(define-module (crates-io se rd serde_flat_path) #:use-module (crates-io))

(define-public crate-serde_flat_path-0.1.0 (c (n "serde_flat_path") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1r8shpjaqzcjd057ksix9hjhc6wjnx92nqxvh7d3k0has5h1nan7") (f (quote (("default") ("allow_overlap"))))))

(define-public crate-serde_flat_path-0.1.1 (c (n "serde_flat_path") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11fglj0py5ywi3fkaq7rj2mc78nqn27jhphwzy2rbsv87hp4j6z5") (f (quote (("default") ("allow_overlap"))))))

(define-public crate-serde_flat_path-0.1.2 (c (n "serde_flat_path") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g8swhs4a2m2ldljmivcab3l2cwif1ljm8p77jh9y7x0yvjlg3vc") (f (quote (("default") ("allow_overlap"))))))

