(define-module (crates-io se rd serde_sciter) #:use-module (crates-io))

(define-public crate-serde_sciter-0.2.0 (c (n "serde_sciter") (v "0.2.0") (d (list (d (n "sciter-rs") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0c336r5l72jscr99y62b3xxkwi60mipccfvpq3z59aq4r1m01jni") (y #t)))

