(define-module (crates-io se rd serde-extensions) #:use-module (crates-io))

(define-public crate-serde-extensions-0.1.0 (c (n "serde-extensions") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-extensions-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "0qxi9wwixrls0yc3wbj9j30pmlnj7m5his3zcmlgyzwdryz7g5j0")))

(define-public crate-serde-extensions-0.1.1 (c (n "serde-extensions") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-extensions-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "17q6fx26si5qkpqfkin9li5xbnhrpnavww6f4shg3i6lwhxxypmk")))

