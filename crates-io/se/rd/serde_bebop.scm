(define-module (crates-io se rd serde_bebop) #:use-module (crates-io))

(define-public crate-serde_bebop-0.1.0 (c (n "serde_bebop") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "018774j3pqhmxfma0pzb4r0vc0i0ni3cwypyb1shn45ji7vamn7p")))

