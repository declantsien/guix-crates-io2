(define-module (crates-io se rd serde_rustler) #:use-module (crates-io))

(define-public crate-serde_rustler-0.0.1 (c (n "serde_rustler") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rustler") (r "^0.20.0") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0v0m1vfkk04ljazs27x7rrqfqzbpl97i2l6k256mbmr42vvmh24x")))

(define-public crate-serde_rustler-0.0.2 (c (n "serde_rustler") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rustler") (r "^0.20.0") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06xwd050rhzlqml9cnn9cqgx8fp3h6axw0wynv2iqdy3rnvzcr4x")))

(define-public crate-serde_rustler-0.0.3 (c (n "serde_rustler") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rustler") (r "^0.20.0") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0a8p9axsibbh4q1gxvlhqz4y8zvy917153gvm741qqnvv9rz3y6p")))

(define-public crate-serde_rustler-0.1.0 (c (n "serde_rustler") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rustler") (r "^0.21.0") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0l8vfawrl5nil641lh67vl8wkq6qh4k6xk7k3h8lvr0sklbclq17")))

