(define-module (crates-io se rd serde_from_file) #:use-module (crates-io))

(define-public crate-serde_from_file-0.1.0 (c (n "serde_from_file") (v "0.1.0") (d (list (d (n "from_file") (r "^0.1.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29.0") (f (quote ("serialize"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (o #t) (d #t) (k 0)))) (h "01rfzxw5v4xh44vcfgkjspm4igb6b0zjawm65ybvyb63sxfnapag") (y #t) (s 2) (e (quote (("yaml" "dep:serde_yaml") ("xml" "dep:quick-xml") ("json" "dep:serde_json"))))))

(define-public crate-serde_from_file-0.1.2 (c (n "serde_from_file") (v "0.1.2") (h "136qgxahk098bkd40970z6b2qwhdlnm5ydnawy70vk5v70mj2dwb") (y #t)))

