(define-module (crates-io se rd serde-diff-derive) #:use-module (crates-io))

(define-public crate-serde-diff-derive-0.1.0 (c (n "serde-diff-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11lg32mqg552w1a5bxy6alag5x42wy6r3hd5nilxr88hjnxw98bl")))

(define-public crate-serde-diff-derive-0.1.1 (c (n "serde-diff-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qig4qcvvj1gf45syf3466bhrkjm3hvs1wn3yjc1a3dsfs0gyz4y")))

(define-public crate-serde-diff-derive-0.1.2 (c (n "serde-diff-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r2awq1q370gflkyjkh08ddj0dplqagnir3rhj0lmikr0qz9sgy5")))

(define-public crate-serde-diff-derive-0.1.3 (c (n "serde-diff-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jzd7vdlq83ix9kqr268lgiri54x6ln6il8r7sp92l6b9j2iwklw")))

(define-public crate-serde-diff-derive-0.2.0 (c (n "serde-diff-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07sl07vwch5sk5fpygz9vqkp2x4ddhjyp149i37vms4c5fx41jw6")))

(define-public crate-serde-diff-derive-0.3.0 (c (n "serde-diff-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fmpymhj2ibdclz97vawf7xj5cw8s0h6q7x7iqaf6yp5qxpqxvn3")))

(define-public crate-serde-diff-derive-0.3.1 (c (n "serde-diff-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n27zh49bx71a1bx8k3gipdliclh3vkfw00dgwbbg2j8fkqz6bn1")))

(define-public crate-serde-diff-derive-0.4.0 (c (n "serde-diff-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1psnqk2wpm82nyk33zb363qrajxbhmiplk48w9l1bbi42fv669b9")))

