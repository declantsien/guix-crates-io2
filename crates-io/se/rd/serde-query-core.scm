(define-module (crates-io se rd serde-query-core) #:use-module (crates-io))

(define-public crate-serde-query-core-0.2.0 (c (n "serde-query-core") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "k9") (r "^0.11.6") (d #t) (k 2)) (d (n "prettyplease") (r "^0.1.23") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 2)))) (h "08k1g602xcqgmj70bbawbrrpf5ncssapmi5n8741mvv47sw2429m")))

