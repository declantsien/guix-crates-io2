(define-module (crates-io se rd serde_json5) #:use-module (crates-io))

(define-public crate-serde_json5-0.1.0 (c (n "serde_json5") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11lm79d24jd1biz60659nc6chvsjxvnfmvf2kzbpn6jya5abg9ij")))

