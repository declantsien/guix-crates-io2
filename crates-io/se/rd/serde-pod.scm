(define-module (crates-io se rd serde-pod) #:use-module (crates-io))

(define-public crate-serde-pod-0.1.0 (c (n "serde-pod") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sa3i69x3h31igxkz028pj8c29qapm3020gqprbw0amsym0mdxfh")))

(define-public crate-serde-pod-0.1.1 (c (n "serde-pod") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1d1h2lmcyygicpdkcmqx2lpcyzh6rs2wbjr6knidqx2cfgjnza64")))

(define-public crate-serde-pod-0.2.0 (c (n "serde-pod") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (f (quote ("i128"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0klnsxd6blh6vp601zbjnpvfl9sbprspisyahp4w6rja015b58pv")))

