(define-module (crates-io se rd serde_indextree) #:use-module (crates-io))

(define-public crate-serde_indextree-0.1.0 (c (n "serde_indextree") (v "0.1.0") (d (list (d (n "indextree") (r "^3.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "1bhsx15fh68nf5ay5rvc9r4lyxnl0dhqsa8b3hm929ja8rn88k0b")))

(define-public crate-serde_indextree-0.2.0 (c (n "serde_indextree") (v "0.2.0") (d (list (d (n "indextree") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0kzp3dk6smfy0509fd1sih9ai4a31kgpb1rshsxb0m2l01c7s3m7")))

