(define-module (crates-io se rd serde-tagged-intermediate) #:use-module (crates-io))

(define-public crate-serde-tagged-intermediate-1.0.0 (c (n "serde-tagged-intermediate") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0dglz0f2ii5j25ipaph3s58kpgbssyxmy2gkxmvl4wv1xlpdd9vw")))

(define-public crate-serde-tagged-intermediate-1.1.0 (c (n "serde-tagged-intermediate") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0fl4rsrg0s9zz8ky3fzqsxvddl82dh18ksgvlf2c2al9j6gyiz9n")))

(define-public crate-serde-tagged-intermediate-1.2.0 (c (n "serde-tagged-intermediate") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d7b1qml8gdiac790i2k4yv2rxa9yavwcqiy4nlgmgngw13xfaa4")))

(define-public crate-serde-tagged-intermediate-1.2.2 (c (n "serde-tagged-intermediate") (v "1.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09dy3a0dj1clj0qjqsv8n24yhdhkzzv2ry8f38ffl0vab7arvmhw")))

(define-public crate-serde-tagged-intermediate-1.2.3 (c (n "serde-tagged-intermediate") (v "1.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mjyh2yqck9hjm1bmsdf5vxnpn1c2a0xk4c65g3j5rvv6xbk7baw")))

(define-public crate-serde-tagged-intermediate-1.2.4 (c (n "serde-tagged-intermediate") (v "1.2.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "14cvfkd4g58gp3sll791wlprka6cvk5kshckil1x7640xamgmnnw")))

(define-public crate-serde-tagged-intermediate-1.3.0 (c (n "serde-tagged-intermediate") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "11661fbgnb1jzsxgl63rk0nyf3f45pqh6fx4346iashcz0lsxm17")))

(define-public crate-serde-tagged-intermediate-1.3.1 (c (n "serde-tagged-intermediate") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "11bcpvf897zk58x6gx8qn6h1zyimgbbrq87qk9ibiz1lm4cxag6w")))

(define-public crate-serde-tagged-intermediate-1.3.2 (c (n "serde-tagged-intermediate") (v "1.3.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "14sp3by8hs661ffz747qvpx7wvqk6sajmzn6qa711s92p4vnkvph")))

(define-public crate-serde-tagged-intermediate-1.4.0 (c (n "serde-tagged-intermediate") (v "1.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1bi820nvn1qfn3d8fkx7rq69qpyxv9v960q2dvm7qbyq5wyrqi8z")))

(define-public crate-serde-tagged-intermediate-1.4.1 (c (n "serde-tagged-intermediate") (v "1.4.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.4") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pph1gragqss947azsfrwqvdfz8vbsv9jysxmdscwzwqkfknnqbd") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.4.2 (c (n "serde-tagged-intermediate") (v "1.4.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.4") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1irlrjqhgp35ffc38n09v46qhbs43kggrj5b2ggbmvsqcvaz892p") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.0 (c (n "serde-tagged-intermediate") (v "1.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "199y2ak002mm0n7axax41fsfgcq58pwksf5xxah8w75lvmf1989n") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.1 (c (n "serde-tagged-intermediate") (v "1.5.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12d94w9jg1mkr6kqbj8iqqg5al7aj0hmnc969avxs74df2830kb4") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.2 (c (n "serde-tagged-intermediate") (v "1.5.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1p9qnymwn9w9b3yc7lzwibnpa9d51syh5imnbfcllz9vrn81b6as") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.3 (c (n "serde-tagged-intermediate") (v "1.5.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f152057n8na0sp3pn1ws50d4vbddparwmpnjks45wj6gyxm9q1n") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.4 (c (n "serde-tagged-intermediate") (v "1.5.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1sn544363rgjbnld9zic23lanpsm56ga3a4ldgkwfy2z3khmppa0") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.5.5 (c (n "serde-tagged-intermediate") (v "1.5.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.5") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0778apisc8iz76hdfwidv6agalgbxnmckrfiscnwzhy5iva7bia9") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

(define-public crate-serde-tagged-intermediate-1.6.0 (c (n "serde-tagged-intermediate") (v "1.6.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-intermediate") (r "^1.6") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0k5qnc4111wk5fa0illblam5v3a8ih1psl6vz2h6pvqqr9yik3zq") (f (quote (("derive" "serde-intermediate/derive") ("default" "derive"))))))

