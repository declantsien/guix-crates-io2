(define-module (crates-io se rd serde_prefix) #:use-module (crates-io))

(define-public crate-serde_prefix-0.1.0 (c (n "serde_prefix") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1dq83rfmsv366wh4j2ymy1pmlvcw0irqzcvfqz9i8vk7vkml1r8g")))

