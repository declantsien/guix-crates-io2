(define-module (crates-io se rd serde_sheets) #:use-module (crates-io))

(define-public crate-serde_sheets-0.1.2 (c (n "serde_sheets") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "google-sheets4") (r "^3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.23") (f (quote ("rustls-native-certs"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "yup-oauth2") (r "^6.6") (d #t) (k 0)))) (h "0phrsbsc9mn73w278amzdgq692w9pb7as5z2qqf09mkvzhs9j11n")))

