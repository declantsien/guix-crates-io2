(define-module (crates-io se rd serde-chrono-str) #:use-module (crates-io))

(define-public crate-serde-chrono-str-0.1.0 (c (n "serde-chrono-str") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "10lczzm89vzhd9s37nmrjm4y62kkrsm3gx7wj1pkzq08mj1npm49")))

