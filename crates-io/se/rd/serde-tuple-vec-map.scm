(define-module (crates-io se rd serde-tuple-vec-map) #:use-module (crates-io))

(define-public crate-serde-tuple-vec-map-0.1.0 (c (n "serde-tuple-vec-map") (v "0.1.0") (d (list (d (n "serde") (r "^0.9") (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "1cr9va0yiczsdgrkd8xm85fgplphrsvcixdx3jvr9597db0wjpfy") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde-tuple-vec-map-0.2.0 (c (n "serde-tuple-vec-map") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n2wga30ki15gzwbvqqw2vmyc85q0z3sph590bckz6cygsyvjb3y") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde-tuple-vec-map-0.2.1 (c (n "serde-tuple-vec-map") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "111bkiznbwz3mi920rzdynmwdk4sq1z5m1x06jf4c2pymkrajv8p") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde-tuple-vec-map-0.2.2 (c (n "serde-tuple-vec-map") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hdapfqymp2p9ydjxm60iwd74hmhhfqaxrf8byxsvj2hchmbaq4m") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde-tuple-vec-map-1.0.0 (c (n "serde-tuple-vec-map") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xlw7gnz1nbnaim9qx03ldnpw0nkhax9x1dvgybpq31z84nrxc8g") (f (quote (("std") ("default" "std"))))))

(define-public crate-serde-tuple-vec-map-1.0.1 (c (n "serde-tuple-vec-map") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10lcazf1rr8c0dgshz169f2qi8mhvjashadpbd27szg71nz0wkd0") (f (quote (("std") ("default" "std"))))))

