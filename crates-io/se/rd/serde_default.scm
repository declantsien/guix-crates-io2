(define-module (crates-io se rd serde_default) #:use-module (crates-io))

(define-public crate-serde_default-0.1.0 (c (n "serde_default") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "16bv9srszzyl5rq0nybcvvgxhhrhvjk7qq2j7qv11yyrhrxwgm4z")))

