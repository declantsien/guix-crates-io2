(define-module (crates-io se rd serde-datetime) #:use-module (crates-io))

(define-public crate-serde-datetime-0.1.0 (c (n "serde-datetime") (v "0.1.0") (d (list (d (n "chrono") (r "<=0.4.19") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1773cn4za1p0hn8pr9flrgsbdpn3wa31y4dxawdga988wd1bg1li")))

(define-public crate-serde-datetime-0.1.1 (c (n "serde-datetime") (v "0.1.1") (d (list (d (n "chrono") (r "<=0.4.19") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i75nhxbcypcx9dm0309k0g4cnk522jf1c2b0hfyxxwxv9jaxa5f")))

(define-public crate-serde-datetime-0.2.0 (c (n "serde-datetime") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jykqr9326x4x7i417rwk8b23dx2zsg5flbljskda1z8zgv0jzf0") (y #t)))

(define-public crate-serde-datetime-0.2.1 (c (n "serde-datetime") (v "0.2.1") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iri5cm6biw2bx5rs6bnmn04wzadqfjqjdh9wvlhzbd4nqd95ycm")))

(define-public crate-serde-datetime-0.2.2 (c (n "serde-datetime") (v "0.2.2") (d (list (d (n "chrono") (r ">=0.4.20") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1xnc7s3apbi7jdby50cx1fbcqcbff5z0j8hqfmzmily7zmfysnq8")))

