(define-module (crates-io se rd serde_nanos) #:use-module (crates-io))

(define-public crate-serde_nanos-0.1.0 (c (n "serde_nanos") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0k4kwfs7s6yl2h22d1g2il89s8hl05yp3yhm1dc627jfp0bdvrvq")))

(define-public crate-serde_nanos-0.1.1 (c (n "serde_nanos") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1xgzclq16ka34l3lpnjwvxlqna45sh8pnrr4ss8sxd6l4sk31wvy")))

(define-public crate-serde_nanos-0.1.2 (c (n "serde_nanos") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "03nya22gljca7m6841jdj9x40fzvds0rgzs21bi6ncax3yk6jjg4")))

(define-public crate-serde_nanos-0.1.3 (c (n "serde_nanos") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0wdwsvw41g66bl2kd2jv1xm84dkzyscvxpl0nnidda1wffvh3s4a")))

(define-public crate-serde_nanos-0.1.4 (c (n "serde_nanos") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1199n6aapb3cclbxgp6kzb6bx1cylg6ipbgyw0xcak3s6vq44cd9")))

