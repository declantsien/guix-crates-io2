(define-module (crates-io se rd serdebin) #:use-module (crates-io))

(define-public crate-serdebin-0.1.0 (c (n "serdebin") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1qp9bp6v3pvgbsvqnagz6r16kjllaby28x2wc6bj3nvg984lmlyn")))

