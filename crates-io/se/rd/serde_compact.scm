(define-module (crates-io se rd serde_compact) #:use-module (crates-io))

(define-public crate-serde_compact-1.0.0-rc.1 (c (n "serde_compact") (v "1.0.0-rc.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "04kvdpdqcyz6khc6280inkq39zw3mck4097a4ib3pfffsgmfif8d") (r "1.61")))

(define-public crate-serde_compact-1.0.0-rc.2 (c (n "serde_compact") (v "1.0.0-rc.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0qrpqyyw93m5b79ad975zy5nc97fjvcl5g67fx2ymd23z1qvzp8r") (r "1.61")))

(define-public crate-serde_compact-1.0.0-rc.3 (c (n "serde_compact") (v "1.0.0-rc.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "visit"))) (d #t) (k 0)))) (h "0x2sgii127yi13kh5cvnf03k4lld3p83kp5297inl5l6yiib9vvp") (r "1.61")))

