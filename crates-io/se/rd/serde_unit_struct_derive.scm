(define-module (crates-io se rd serde_unit_struct_derive) #:use-module (crates-io))

(define-public crate-serde_unit_struct_derive-0.1.0 (c (n "serde_unit_struct_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1slf2ld6f4m1g8bqq7scgm7g1fx1pymrwsaz1x00jf166jsja3p0")))

(define-public crate-serde_unit_struct_derive-0.1.1 (c (n "serde_unit_struct_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bxqdhwfnillpw083clz8d79xfn4x3cgkb74l1l0f69kvirkrnp9")))

(define-public crate-serde_unit_struct_derive-0.1.2 (c (n "serde_unit_struct_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rf2h6fd70a0srzsci9j68ipxbfisw4wl30xal3avbr00b9rf5qd") (y #t)))

(define-public crate-serde_unit_struct_derive-0.1.3 (c (n "serde_unit_struct_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1v4djpdhq3j0l2xfhxf12vwlibgzwczh6q7ygyv8zcrlhn1ia6rz")))

