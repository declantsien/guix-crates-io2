(define-module (crates-io se rd serde-csv-extra) #:use-module (crates-io))

(define-public crate-serde-csv-extra-0.1.0 (c (n "serde-csv-extra") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08kc3qgvsm3jnqydn50yc4lqjsxxzp1638sszgxs39hkza0dn8zg")))

(define-public crate-serde-csv-extra-0.2.0 (c (n "serde-csv-extra") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xwarcjib830245i30zdbz0ki19qs20597z2jw57vyi5fig49m6f")))

