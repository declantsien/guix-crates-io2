(define-module (crates-io se rd serde-xml-rust) #:use-module (crates-io))

(define-public crate-serde-xml-rust-0.6.0 (c (n "serde-xml-rust") (v "0.6.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1m303ia33k5pvcff5dn2nwrjzjpv4yvnhrqj27a12vpw9g11fav6") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

