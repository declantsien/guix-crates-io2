(define-module (crates-io se rd serde-serialize-seed) #:use-module (crates-io))

(define-public crate-serde-serialize-seed-0.0.1 (c (n "serde-serialize-seed") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1q0av1d1zzz7yzhd32p9f62gzpkj5kj2khjkrhxjyxm5ww491q26") (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.2 (c (n "serde-serialize-seed") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0zccan71izlg3flgfkmq2kclnb8d2scczfjc9ysgggzzrnm9z7sn") (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.3 (c (n "serde-serialize-seed") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0mqc4115ycdgbcpbvjs5l0s202hkdnmgcpf11ycfk2ym3ci87grb") (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.4 (c (n "serde-serialize-seed") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0m0jh51w0svhackw411m3iiwjrq2hf6mja2fhpw8fjb9qam8yhja") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.5 (c (n "serde-serialize-seed") (v "0.0.5") (d (list (d (n "phantom-type") (r "^0.4.2") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0xbkm8rcdqfw58d076jlvf2nc4lipyi7y4jg1ikvnd2d9n30727p") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.6 (c (n "serde-serialize-seed") (v "0.0.6") (d (list (d (n "phantom-type") (r "^0.4.2") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0yr3crg5hxkvsjmbx8dfz6v6cwkwnfhnn36vvxd35rmhyafvzpyg") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.7 (c (n "serde-serialize-seed") (v "0.0.7") (d (list (d (n "phantom-type") (r "^0.4.2") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "071cc9qycc63iigi8digzyi19w48xpispdp92nwccp2c3m6br2ii") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

(define-public crate-serde-serialize-seed-0.0.8 (c (n "serde-serialize-seed") (v "0.0.8") (d (list (d (n "phantom-type") (r "^0.4.2") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1cqmflq8pz0br6rcal94angwkjqsz0hipbzc948c2dwzy1jhymfh") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-serde-serialize-seed-0.0.9 (c (n "serde-serialize-seed") (v "0.0.9") (d (list (d (n "phantom-type") (r "^0.4.2") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "01gmg953bjn1pimi6m1wy685l2gd4yl5sh14pdxjv8949dbva7nk") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-serde-serialize-seed-0.0.10 (c (n "serde-serialize-seed") (v "0.0.10") (d (list (d (n "phantom-type") (r "^0.5.0") (k 0)) (d (n "serde") (r "^1.0.151") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0f1mw1n1ydfzl6ixvsz922gi97r9wb39gjbaskh4hs7m03kw3jz3") (f (quote (("default" "alloc") ("alloc")))) (r "1.70")))

