(define-module (crates-io se rd serde-nu) #:use-module (crates-io))

(define-public crate-serde-nu-0.1.0 (c (n "serde-nu") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "nu-protocol") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hjszk4k4x54psxgmj0x5616y5sqv9i3dphdmc3lcshhcm8jg3x2")))

(define-public crate-serde-nu-0.1.1 (c (n "serde-nu") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "nu-protocol") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14q5xfam9f83bp0q0pxs2nzzq8g4b1sfac73gg1bdbr2sa9a8gkj")))

(define-public crate-serde-nu-0.2.0 (c (n "serde-nu") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "nu-protocol") (r "^0.34") (d #t) (k 0)) (d (n "nu-source") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bk5f1fqd9v9sqwm0lbrwjgdz0y8biv6k8rglx0wb1c59r8y5gpx")))

