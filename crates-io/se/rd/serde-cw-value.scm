(define-module (crates-io se rd serde-cw-value) #:use-module (crates-io))

(define-public crate-serde-cw-value-0.7.0 (c (n "serde-cw-value") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)))) (h "1m4riskk8gp6jq921jzk9ldgaz8xiz0c7djhv2vmimwfdgd34pd7")))

