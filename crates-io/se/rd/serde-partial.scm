(define-module (crates-io se rd serde-partial) #:use-module (crates-io))

(define-public crate-serde-partial-0.1.0 (c (n "serde-partial") (v "0.1.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-partial-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0f3sshb2399kvja54swdr3cy60nj1zmv9253gvyn2yjpxc5kx68y") (r "1.56.0")))

(define-public crate-serde-partial-0.2.0 (c (n "serde-partial") (v "0.2.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-partial-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01s8wj7kkbnjbxi0mxz0kl024619by7pz4sq9835xh2q70i0q669") (f (quote (("std" "serde/std") ("default" "alloc" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

(define-public crate-serde-partial-0.2.1 (c (n "serde-partial") (v "0.2.1") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-partial-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "15vax3fvbi70f72v85simdva27g431xj75xyj1bs25phb33zl32l") (f (quote (("std" "serde/std") ("default" "alloc" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

(define-public crate-serde-partial-0.3.0 (c (n "serde-partial") (v "0.3.0") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-partial-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0xygjcir0scvw9kph69sh1qaf5hds601qjaa4bq4ikpfwnby2gi5") (f (quote (("std" "serde/std") ("default" "alloc" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

(define-public crate-serde-partial-0.3.1 (c (n "serde-partial") (v "0.3.1") (d (list (d (n "serde") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-partial-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "17v28l4nk4kvkqsmh254rdnsiyc35g85df0xkgn44y351grrxgrx") (f (quote (("std" "serde/std") ("default" "alloc" "std") ("alloc" "serde/alloc")))) (r "1.56.0")))

