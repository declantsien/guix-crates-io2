(define-module (crates-io se -l se-logger) #:use-module (crates-io))

(define-public crate-se-logger-0.1.0 (c (n "se-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "004l6yah5pl9z85c3qf7n1lsyzrdqd39m72qb707r59w8kw345yd")))

(define-public crate-se-logger-0.1.1 (c (n "se-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "1zqwmcgzyag0ch20dmw4isg1rqgk00spd8ypl21df2wwz7xx4xrd")))

(define-public crate-se-logger-0.1.2 (c (n "se-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "1zh55dgxvjcqzm33sw1qh993kh37npqc3ngg2qgda7zzfx7pm5li")))

