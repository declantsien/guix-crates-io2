(define-module (crates-io se lv selva) #:use-module (crates-io))

(define-public crate-selva-0.2.0 (c (n "selva") (v "0.2.0") (d (list (d (n "glium") (r "^0.21.0") (d #t) (k 0)) (d (n "glsl-include") (r "^0.2") (d #t) (k 0)) (d (n "glslwatch") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0cacb9pf2lvw7h6nd9ljy6wlg9cjmv0zvf1zbngpdlj5fmqank12")))

