(define-module (crates-io se ra serai-signals-primitives) #:use-module (crates-io))

(define-public crate-serai-signals-primitives-0.0.1 (c (n "serai-signals-primitives") (v "0.0.1") (h "17n2qkyk541jgsm4m0x1s840kzz7zlp5bq69nrvl9lvvxng6q8wn")))

(define-public crate-serai-signals-primitives-0.1.0 (c (n "serai-signals-primitives") (v "0.1.0") (d (list (d (n "borsh") (r "^1") (f (quote ("derive" "de_strict_order"))) (o #t) (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "serai-primitives") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mcz1qvagg6l24x3a9pipyg0h8zgszr2yb8r9sh64dbxkrba66wd") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "zeroize" "scale/std" "scale-info/std" "borsh?/std" "serde?/std" "serai-primitives/std") ("serde" "dep:serde") ("borsh" "dep:borsh")))) (r "1.74")))

