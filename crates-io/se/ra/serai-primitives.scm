(define-module (crates-io se ra serai-primitives) #:use-module (crates-io))

(define-public crate-serai-primitives-0.1.0 (c (n "serai-primitives") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0567bhbsbhsfzadckfxmlbh357pxklhf8kp7px4hmxc8nxlbpfcf")))

