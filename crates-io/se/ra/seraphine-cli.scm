(define-module (crates-io se ra seraphine-cli) #:use-module (crates-io))

(define-public crate-seraphine-cli-0.1.0 (c (n "seraphine-cli") (v "0.1.0") (d (list (d (n "seraphine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "05z75pvpgd3b1gm85d67h4nvv07c4gjpmbmwzkgsv627j44zcv8k")))

(define-public crate-seraphine-cli-0.1.1 (c (n "seraphine-cli") (v "0.1.1") (d (list (d (n "seraphine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "17x4hkmzb9r8hjam8a7rjrcijphy50x4crax6n6wwllw82l4j4fb")))

