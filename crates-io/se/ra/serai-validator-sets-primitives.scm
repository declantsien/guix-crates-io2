(define-module (crates-io se ra serai-validator-sets-primitives) #:use-module (crates-io))

(define-public crate-serai-validator-sets-primitives-0.1.0 (c (n "serai-validator-sets-primitives") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dkg30sx0inaakm70rl7yfzm38dxgcx0ldpr4ndl9jcr6hxch4bz")))

