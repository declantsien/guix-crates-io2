(define-module (crates-io se ra serai-processor) #:use-module (crates-io))

(define-public crate-serai-processor-0.1.0 (c (n "serai-processor") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "group") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "scale") (r "^3") (d #t) (k 0) (p "parity-scale-codec")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1zpnhwjgfnaw4lqmsp641688nqg3zma6mwqbri25vqaaxpib2f3l")))

