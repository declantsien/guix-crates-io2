(define-module (crates-io se ra serai-runtime) #:use-module (crates-io))

(define-public crate-serai-runtime-0.1.0 (c (n "serai-runtime") (v "0.1.0") (d (list (d (n "codec") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)))) (h "0p089vk7wrbfblqrr6yz7l5jpih9wcq28w8b8z040hjqrva4hkai")))

