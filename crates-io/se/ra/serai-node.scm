(define-module (crates-io se ra serai-node) #:use-module (crates-io))

(define-public crate-serai-node-0.1.0 (c (n "serai-node") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.16") (f (quote ("server"))) (d #t) (k 0)))) (h "03if5rw0f52dhqrzfhdnrcwfyam74k3gkrpxa80g1gb9p5h3grlc")))

