(define-module (crates-io se ra serai-tokens-pallet) #:use-module (crates-io))

(define-public crate-serai-tokens-pallet-0.1.0 (c (n "serai-tokens-pallet") (v "0.1.0") (d (list (d (n "scale") (r "^3") (f (quote ("derive" "max-encoded-len"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)))) (h "1s006qayfhdifkq5apy6sjzcn8gab792mr5r0j2yl8lp21vb8y0h")))

