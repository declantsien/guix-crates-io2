(define-module (crates-io se ra serai-client) #:use-module (crates-io))

(define-public crate-serai-client-0.1.0 (c (n "serai-client") (v "0.1.0") (d (list (d (n "scale") (r "^3") (d #t) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1q8s7fg0md0vim42iry417x87ks71avgxq2a30nr1jz9qs8lh8w4")))

