(define-module (crates-io se ra serai-message-queue) #:use-module (crates-io))

(define-public crate-serai-message-queue-0.1.0 (c (n "serai-message-queue") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.16") (f (quote ("server"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01y9cj6bik169yj1jsz0p3482if0wl63lzqakh5cbiq30hhk017q")))

