(define-module (crates-io se ra seraphine-core) #:use-module (crates-io))

(define-public crate-seraphine-core-0.1.0 (c (n "seraphine-core") (v "0.1.0") (h "1q8v33w5q64nqn95fw60yjch8jvra2pk362a350ap5g83if599wc")))

(define-public crate-seraphine-core-0.1.1 (c (n "seraphine-core") (v "0.1.1") (h "0nfgk031pp71s6k3wx1m6l39rs27y35l52vk8i6bnj940nry43df")))

