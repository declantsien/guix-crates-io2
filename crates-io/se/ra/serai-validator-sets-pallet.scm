(define-module (crates-io se ra serai-validator-sets-pallet) #:use-module (crates-io))

(define-public crate-serai-validator-sets-pallet-0.1.0 (c (n "serai-validator-sets-pallet") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)))) (h "1xl141k7ydl641bdbrkpyifinkzb6wfk83l5nac49qpq84p165ai")))

