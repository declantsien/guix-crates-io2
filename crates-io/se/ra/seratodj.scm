(define-module (crates-io se ra seratodj) #:use-module (crates-io))

(define-public crate-seratodj-0.0.1 (c (n "seratodj") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 2)) (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.13.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i5jwlds29kdimv41n6v049d4j6wwp46lwlw9r2xcnrkhfs3im50") (y #t)))

