(define-module (crates-io se ra serai-in-instructions-primitives) #:use-module (crates-io))

(define-public crate-serai-in-instructions-primitives-0.1.0 (c (n "serai-in-instructions-primitives") (v "0.1.0") (d (list (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "020pqrs9rhsnm91na6j3x6jk2vjyg57w9dvb6lklylpp6kyi18dk")))

