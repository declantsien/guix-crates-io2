(define-module (crates-io se ra seraphite) #:use-module (crates-io))

(define-public crate-seraphite-1.0.0 (c (n "seraphite") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 0)))) (h "0m12rh1zss2m74jhn3sn1szd3gfqi5b1l1ziznkq26yqcwnikqxq") (y #t)))

(define-public crate-seraphite-1.0.1 (c (n "seraphite") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 0)))) (h "1s90b7v59qn8a3w2dxbfbq3kkmxykwaw3fah5p5gw44qxl1cilcc")))

