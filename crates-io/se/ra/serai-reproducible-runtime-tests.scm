(define-module (crates-io se ra serai-reproducible-runtime-tests) #:use-module (crates-io))

(define-public crate-serai-reproducible-runtime-tests-0.1.0 (c (n "serai-reproducible-runtime-tests") (v "0.1.0") (d (list (d (n "dockertest") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)))) (h "1frxvzg63n05zflajhcp0kjwwn1yynykc2g80i7w4gpva63im9d9")))

