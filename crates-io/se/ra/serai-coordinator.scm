(define-module (crates-io se ra serai-coordinator) #:use-module (crates-io))

(define-public crate-serai-coordinator-0.1.0 (c (n "serai-coordinator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1w25n4fn1qcw00z0vjq2sqipahsf3f2qksq9snwhzq1zf2fbn2a5")))

