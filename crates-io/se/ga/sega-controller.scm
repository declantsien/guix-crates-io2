(define-module (crates-io se ga sega-controller) #:use-module (crates-io))

(define-public crate-sega-controller-0.1.0 (c (n "sega-controller") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1mdjvdrzid6rablsck927j010fzvzg4sy8fcpr68qr25g0rr5x1p") (f (quote (("mega-drive") ("all" "mega-drive"))))))

