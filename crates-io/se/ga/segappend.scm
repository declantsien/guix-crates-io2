(define-module (crates-io se ga segappend) #:use-module (crates-io))

(define-public crate-segappend-0.1.0 (c (n "segappend") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1l1wfh4lhcli9602vdpywba8rxj24zlqvb0pxla6yzygwkr12k2i")))

(define-public crate-segappend-0.2.0 (c (n "segappend") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1ansk8ram1pr4bvc6v0628x1z5r64r9qb3mjd7mj3d3968dww7b1")))

