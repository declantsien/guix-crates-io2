(define-module (crates-io se ga sega_cmp) #:use-module (crates-io))

(define-public crate-sega_cmp-0.1.0 (c (n "sega_cmp") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0gkpcmcqyqnx09ghxzk623g71xskyxayik45yvqp2i8sjh8d3wnl")))

(define-public crate-sega_cmp-0.2.0 (c (n "sega_cmp") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1v1c5rdrv3562zzq0mkklis5jmw1gc70pwifwgmmy41r49y5cdsg")))

