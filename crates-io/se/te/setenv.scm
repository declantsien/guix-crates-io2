(define-module (crates-io se te setenv) #:use-module (crates-io))

(define-public crate-setenv-0.1.0 (c (n "setenv") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0gm1pj4xd1adhafvmxvlf0d466g2djvqm8mz0h9n8xlnk88s14zx")))

(define-public crate-setenv-0.1.1 (c (n "setenv") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0b6r45xy17d9ghfsa9kpwsz8jfcflg4myxk9zl9a7fiqwcz1l01c")))

(define-public crate-setenv-0.1.2 (c (n "setenv") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17mj7ijq09ig4slj6xqsii31v9sr7yq71mbk5lgpn3i6fnwq3vym")))

