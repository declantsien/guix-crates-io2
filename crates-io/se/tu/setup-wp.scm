(define-module (crates-io se tu setup-wp) #:use-module (crates-io))

(define-public crate-setup-wp-1.0.1 (c (n "setup-wp") (v "1.0.1") (d (list (d (n "mysql") (r "^21.0.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1933abpdy2g2pqs87kxap426fmwsd0s2gs21pf4hx1mgxjvshsbk")))

