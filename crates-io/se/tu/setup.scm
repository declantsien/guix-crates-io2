(define-module (crates-io se tu setup) #:use-module (crates-io))

(define-public crate-setup-0.1.0 (c (n "setup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "aptos-cli-config") (r "^0.1.0") (d #t) (k 0)) (d (n "coinlist") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)) (d (n "move-deps") (r "^0.1.6") (f (quote ("address32"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0y8ap86wh7cjxvf9ij4bgcmz9488pgckfr3jkqrybffii73ahgz0")))

