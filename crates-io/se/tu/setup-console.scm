(define-module (crates-io se tu setup-console) #:use-module (crates-io))

(define-public crate-setup-console-0.1.0 (c (n "setup-console") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1awcpxhyalgvczvc8z39lx11cwhh9j849i08j62bpgfx4j7380z8")))

(define-public crate-setup-console-0.1.1 (c (n "setup-console") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nibaisfcf561cj25d575vpacfqxr09qz1czqws5prf1gxjdxrcy")))

(define-public crate-setup-console-0.2.0 (c (n "setup-console") (v "0.2.0") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hac7542v413rci77s202yyq48lww5z0xflv5bwdqr9pl4i1fgy1") (y #t)))

(define-public crate-setup-console-0.2.1 (c (n "setup-console") (v "0.2.1") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b064lq7b1g8n8c7jcasbimk3gdy954i8gb77nc17jr3r1k6ryx5")))

