(define-module (crates-io se fa sefar) #:use-module (crates-io))

(define-public crate-sefar-0.1.0 (c (n "sefar") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "095mr7yh7rpwnhgsj4j00shs5nkqgw0l6fsybd4586m3d824zzqb") (f (quote (("report") ("parallel") ("binary")))) (y #t)))

(define-public crate-sefar-0.1.1 (c (n "sefar") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sp86bv85qwii00rjbdik36dn53hsxdq7blmdpjvyvidpljm8av9") (f (quote (("report") ("parallel") ("binary")))) (y #t)))

(define-public crate-sefar-0.1.2 (c (n "sefar") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16pqynxrc8r9yl1ipmj4qcqlgxrmj2fg93prcv2zaglxd5i4d76g") (f (quote (("report") ("parallel") ("binary")))) (y #t)))

(define-public crate-sefar-0.1.3 (c (n "sefar") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rqqxymb4dlpysdsz8g9zkh2192p08g41vb45aqvhq8yk073v0p7") (f (quote (("report") ("parallel") ("binary")))) (y #t)))

(define-public crate-sefar-0.1.4 (c (n "sefar") (v "0.1.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ad4cmjn6izcrjf712mqlhjy6b12d0v28rcgxn3204v5a2ii8181") (f (quote (("report") ("parallel") ("binary"))))))

