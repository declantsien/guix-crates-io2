(define-module (crates-io se cp secp256k1-test) #:use-module (crates-io))

(define-public crate-secp256k1-test-0.7.1 (c (n "secp256k1-test") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zaziqqg7p0mhxcnlybisi7f1kyiza63b0i8grdyhvkg0b85p0wj") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

(define-public crate-secp256k1-test-0.7.2 (c (n "secp256k1-test") (v "0.7.2") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hjpl9myb81j759ksll01p6pi9kid2n109yz35d9papqya5dg2r7") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

