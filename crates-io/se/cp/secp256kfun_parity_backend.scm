(define-module (crates-io se cp secp256kfun_parity_backend) #:use-module (crates-io))

(define-public crate-secp256kfun_parity_backend-0.1.0 (c (n "secp256kfun_parity_backend") (v "0.1.0") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2.2") (k 0)))) (h "0lpgx4z25zk4h46b77ky1p2klmr6m417mw5r2mbnsmc9qkd1kqfw") (f (quote (("default") ("alloc"))))))

(define-public crate-secp256kfun_parity_backend-0.1.1 (c (n "secp256kfun_parity_backend") (v "0.1.1") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2.2") (k 0)))) (h "1nfahjym922d7cdlk1fxaj5la6c8bdjxlzacx9cfpqbizvah03vd") (f (quote (("default") ("alloc"))))))

(define-public crate-secp256kfun_parity_backend-0.1.2 (c (n "secp256kfun_parity_backend") (v "0.1.2") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2.2") (k 0)))) (h "16aggddn6pnq2hh9hmyw0kb7lr47p6lhmh7fliqb3qgacvzi36cm") (f (quote (("default") ("alloc"))))))

(define-public crate-secp256kfun_parity_backend-0.1.3 (c (n "secp256kfun_parity_backend") (v "0.1.3") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2.2") (k 0)))) (h "0waj169r9yw5rv3psfs0xv74h1av4csmaa2rhk0wbr7lgfap8z0r") (f (quote (("default") ("alloc"))))))

(define-public crate-secp256kfun_parity_backend-0.1.5 (c (n "secp256kfun_parity_backend") (v "0.1.5") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0) (p "subtle-ng")))) (h "02aq7sr3lms0i5fjqjdwc25iwzmdvinlh56gsjzic1xzznqsh4k0") (f (quote (("default") ("alloc"))))))

(define-public crate-secp256kfun_parity_backend-0.1.6 (c (n "secp256kfun_parity_backend") (v "0.1.6") (d (list (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "1wpqlyw99i8gv59daqin997p68l45bvph6l2yg2aylj9av3k3r3v") (f (quote (("default") ("alloc")))) (y #t)))

