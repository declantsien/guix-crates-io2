(define-module (crates-io se cp secp256k1-zkp-sys) #:use-module (crates-io))

(define-public crate-secp256k1-zkp-sys-0.1.0 (c (n "secp256k1-zkp-sys") (v "0.1.0") (d (list (d (n "cc") (r ">= 1.0.28, <= 1.0.35") (d #t) (k 1)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "secp256k1") (r "^0.13") (d #t) (k 0)) (d (n "secp256k1-zkp-dev") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0b73mdjavxg63zs2zq6r761dldh87qc5qh0595n56an8g0ml8b8n")))

(define-public crate-secp256k1-zkp-sys-0.2.0 (c (n "secp256k1-zkp-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.4") (d #t) (k 0)))) (h "10jhgzjazcdasjlla8rqn7w6lzp2chwaix9g3d3wn9gvzdkbz2l2") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_2_0")))

(define-public crate-secp256k1-zkp-sys-0.3.0 (c (n "secp256k1-zkp-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.4") (d #t) (k 0)))) (h "0rw3dq4bsh7npmj0spky0lgfrnjf06nc63mjvga6iziqsrap8137") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_3_0")))

(define-public crate-secp256k1-zkp-sys-0.4.0 (c (n "secp256k1-zkp-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.4") (d #t) (k 0)))) (h "1w46kyrfsbwxyj2a64sbd3p53gc8xmjghw4pvybm764i2973qnml") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_4_0")))

(define-public crate-secp256k1-zkp-sys-0.5.0 (c (n "secp256k1-zkp-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.4") (d #t) (k 0)))) (h "1vvdscsni2pyhgs6ml0wmcws7q7absrw6snvk0a8gpags1q8xi4h") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_5_0")))

(define-public crate-secp256k1-zkp-sys-0.6.0 (c (n "secp256k1-zkp-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.5") (d #t) (k 0)))) (h "1qw24nz4ql2ivxq0yzzv1rbpc3hyx9852d8prv9pjzk2590q1y76") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_6_0")))

(define-public crate-secp256k1-zkp-sys-0.7.0 (c (n "secp256k1-zkp-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0d0098d0c2fl30arrbalhq9jzzj3y2vs660hsai1sz08mjza5rv4") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_7_0")))

(define-public crate-secp256k1-zkp-sys-0.8.0 (c (n "secp256k1-zkp-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.8.0") (d #t) (k 0)))) (h "09lwnh1spgbmkssapfndvb87dpx7f3nnqxjbfvxm7cdmgj9csds2") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_8_0")))

(define-public crate-secp256k1-zkp-sys-0.8.1 (c (n "secp256k1-zkp-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.8.0") (d #t) (k 0)))) (h "08y7jcr5864pfrb5kv1gnkf1vblqb65x5f6rx2cii3m1fp5b2fnh") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_8_0")))

(define-public crate-secp256k1-zkp-sys-0.9.0 (c (n "secp256k1-zkp-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1l2jvfbi71cy9zjw3ld6fg283sl7wb0k94d701cbg12c6f1cmg1b") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_8_0")))

(define-public crate-secp256k1-zkp-sys-0.9.1 (c (n "secp256k1-zkp-sys") (v "0.9.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "secp256k1-sys") (r "^0.9.0") (d #t) (k 0)))) (h "0abfng87nggk2scymx8pc7kka08pddpfdq6ivcnswjq78cpxbsml") (f (quote (("std") ("recovery" "secp256k1-sys/recovery") ("lowmemory" "secp256k1-sys/lowmemory") ("default" "std")))) (l "rustsecp256k1zkp_v0_8_0")))

