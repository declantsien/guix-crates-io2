(define-module (crates-io se cp secp256k1-plus) #:use-module (crates-io))

(define-public crate-secp256k1-plus-0.5.7 (c (n "secp256k1-plus") (v "0.5.7") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qpkdgc2an6yxjdjna9ha0y0rd0n54zmd301knm14zg39pda08kj") (f (quote (("unstable") ("singleton" "lazy_static") ("dev" "clippy") ("default" "singleton"))))))

