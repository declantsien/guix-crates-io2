(define-module (crates-io se cp secp256k1-abc-sys) #:use-module (crates-io))

(define-public crate-secp256k1-abc-sys-0.1.0 (c (n "secp256k1-abc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "1dwc1n4r9nafx80gwabl3mr7xlqq7ayb34zabxj6hv4w22y4xq2y")))

(define-public crate-secp256k1-abc-sys-0.1.1 (c (n "secp256k1-abc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "1q0skhjj8rgjhxnb34wh9zcxz02j64vrj2m8x0fkrin111pvfv9v")))

(define-public crate-secp256k1-abc-sys-0.1.2 (c (n "secp256k1-abc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "04fnn5cmlbkfhjl52x02c4zgynas9f54sfplyb02zf0brki7p6fr")))

