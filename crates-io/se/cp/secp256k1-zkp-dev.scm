(define-module (crates-io se cp secp256k1-zkp-dev) #:use-module (crates-io))

(define-public crate-secp256k1-zkp-dev-0.1.0 (c (n "secp256k1-zkp-dev") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secp256k1") (r "^0.13") (d #t) (k 0)))) (h "15q2bdzyrs20zpj659fyfbf3qaihnlvhh2ha9aglwsnh55sddiir")))

