(define-module (crates-io se cp secp256k1-ecdsa) #:use-module (crates-io))

(define-public crate-secp256k1-ecdsa-0.0.0 (c (n "secp256k1-ecdsa") (v "0.0.0") (h "0wmh1lb61m9s8bnar0k0lsyhwklmsbqazm9i1j87jwk18ys538yg") (y #t)))

(define-public crate-secp256k1-ecdsa-0.1.0 (c (n "secp256k1-ecdsa") (v "0.1.0") (h "1m4nwqmp82ssv7jmhd8xgwllq99zz812jx6v44c7w20w3733kjym") (y #t)))

(define-public crate-secp256k1-ecdsa-0.2.0 (c (n "secp256k1-ecdsa") (v "0.2.0") (h "10pvpa3xlllpikgh0qx2si90f93yar0zjc0x3n0bmxw30kfldm9x") (y #t)))

