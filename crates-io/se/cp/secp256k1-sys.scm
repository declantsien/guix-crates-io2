(define-module (crates-io se cp secp256k1-sys) #:use-module (crates-io))

(define-public crate-secp256k1-sys-0.1.0 (c (n "secp256k1-sys") (v "0.1.0") (d (list (d (n "cc") (r ">= 1.0.28, <= 1.0.41") (d #t) (k 1)))) (h "0i1ky1jvw34yg1qc3zld9r8kriys9lzm8lsb7g1zs3w3iddpln8f") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "secp256k1")))

(define-public crate-secp256k1-sys-0.1.1 (c (n "secp256k1-sys") (v "0.1.1") (d (list (d (n "cc") (r ">= 1.0.28, <= 1.0.41") (d #t) (k 1)))) (h "1frwhlhkvsxfsdqn8dl1723dcfk6q6831ljfrwy0h82lxn8q0hx8") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_1_1")))

(define-public crate-secp256k1-sys-0.1.2 (c (n "secp256k1-sys") (v "0.1.2") (d (list (d (n "cc") (r ">= 1.0.28, <= 1.0.41") (d #t) (k 1)))) (h "087j362x72f2qrgjzqrgqnwksbpw9akfi6k35vqs0lim1mpw5cks") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_1_1")))

(define-public crate-secp256k1-sys-0.2.0 (c (n "secp256k1-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "0lg3mvlnpjxylk629w4xk7c1xcbnm358xn03m11sw6cb12dksibi") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_2_0")))

(define-public crate-secp256k1-sys-0.3.0 (c (n "secp256k1-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1lldqabffd9ax824bqdcjgvbnw0zncis3gij8h7r745h1lhksm8i") (f (quote (("std") ("recovery") ("lowmemory") ("fuzztarget") ("external-symbols") ("endomorphism") ("default" "std")))) (l "rustsecp256k1_v0_2_0")))

(define-public crate-secp256k1-sys-0.4.0 (c (n "secp256k1-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0addgncbv2287yhgaqb61bcy385hizwqhnwqr00mk7z4br2vdr37") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_4_0")))

(define-public crate-secp256k1-sys-0.4.1 (c (n "secp256k1-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yrs8rjwwzz7icnfy0m78mdi52zizf11pi9gg6f84cr5wk6bfz42") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_4_1")))

(define-public crate-secp256k1-sys-0.4.2 (c (n "secp256k1-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0dk0as7qdlvg5vkcsihndzg1jgqb9amhwmz3xiip94fy7ibs4zcm") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_4_1")))

(define-public crate-secp256k1-sys-0.5.0 (c (n "secp256k1-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0d9s202a16ffqn131dyv8lv9ny47rrl8c87dmvry2wi56bbvkd87") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-secp256k1-sys-0.5.1 (c (n "secp256k1-sys") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "150qcgkaxifs2yvmxn9dy0spfhxvaiflip81hn48wjfd19r72vxa") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-secp256k1-sys-0.5.2 (c (n "secp256k1-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0471zw9rz8yl1bb92aix4yabg87q79k09d1sqh7kj685znh20bhm") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-secp256k1-sys-0.6.0 (c (n "secp256k1-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0sr2fjzrn94f4wcqwa3diwl5mcm0rl535046hbbi0a1zmy7dqn3h") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-secp256k1-sys-0.6.1 (c (n "secp256k1-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0jxsdclaykc1ap67yqcbazcpyfx4nqg5sbp8bdi6w0615wn0w243") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_6_1")))

(define-public crate-secp256k1-sys-0.7.0 (c (n "secp256k1-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "08lg7nqzsf6azj6f5r64jvkid1mm220zgh652jngbnk4wj5f4n40") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_7_0")))

(define-public crate-secp256k1-sys-0.8.0 (c (n "secp256k1-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1zsifvn06nxs48733yxr6d7cg2iw8lz2gsx03p3xizc2crrn4ak4") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_8_0")))

(define-public crate-secp256k1-sys-0.8.1 (c (n "secp256k1-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0zm0kw4v8nn2p87h2zwwzpvwy4rvmg26749vfliv5gzgx6wjk8bh") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_8_1")))

(define-public crate-secp256k1-sys-0.9.0 (c (n "secp256k1-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1dz39ym5f3g2wfbr2m7f29f5fc9v30c9mp4rajyj9z9qgi37rrh9") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (y #t) (l "rustsecp256k1_v0_9_0")))

(define-public crate-secp256k1-sys-0.9.1 (c (n "secp256k1-sys") (v "0.9.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "10xyk168qab5by10hdnxh6xjapa6jzq4cp7xac0f6dy7dq47mnad") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (y #t) (l "rustsecp256k1_v0_9_1")))

(define-public crate-secp256k1-sys-0.9.2 (c (n "secp256k1-sys") (v "0.9.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1sxpzg4w1qzjlx8wal90ir4i1ngsddj8q6iw7ic9vha2mrm79lg5") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_9_2")))

(define-public crate-secp256k1-sys-0.10.0 (c (n "secp256k1-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "06xwfjw0gam2rchcwgrjg4bi5lrd105p40yn2hzl8qv22mkvscql") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_10_0") (r "1.56.1")))

