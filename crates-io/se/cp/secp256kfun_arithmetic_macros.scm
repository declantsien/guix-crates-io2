(define-module (crates-io se cp secp256kfun_arithmetic_macros) #:use-module (crates-io))

(define-public crate-secp256kfun_arithmetic_macros-0.1.0 (c (n "secp256kfun_arithmetic_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "07hy93qhddrb0mha621lnczp8dg2v5m4gzqhc3zfnc15ly2w7dwi")))

