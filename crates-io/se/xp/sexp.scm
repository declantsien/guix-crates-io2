(define-module (crates-io se xp sexp) #:use-module (crates-io))

(define-public crate-sexp-0.1.0 (c (n "sexp") (v "0.1.0") (h "16vhv41f2cr2yv4j63czld5781zy81ffv4z6jn769y2xv0v7q3cc")))

(define-public crate-sexp-0.1.1 (c (n "sexp") (v "0.1.1") (h "1ms9h4gb61jvhn1azk1hizk0aw7lsrjw3xbszpwsl2jqzj56w178")))

(define-public crate-sexp-1.0.0 (c (n "sexp") (v "1.0.0") (h "1k05dpadqzqm0b4mpbn83lxqykawvsbh1lqwn09a20ynwydqf1kv")))

(define-public crate-sexp-1.0.1 (c (n "sexp") (v "1.0.1") (h "10p581c5hjb4iqxdi7qr9wznnds28c335i3gpgh0dpvqm1p93r3l")))

(define-public crate-sexp-1.1.0 (c (n "sexp") (v "1.1.0") (h "0l3l4fsn8ahav2lvfg52j3xzsan819isd417g662kvw5k5fnf911")))

(define-public crate-sexp-1.1.1 (c (n "sexp") (v "1.1.1") (h "1c1k2i2yk9gbfvnvwhp8wxgbyiszrxqm0ns4f4jl6cfxfqkr13db")))

(define-public crate-sexp-1.1.2 (c (n "sexp") (v "1.1.2") (h "16150m91wgsgagbwpyxclnxjnnn60gina3acxz27y16wawild3lq")))

(define-public crate-sexp-1.1.3 (c (n "sexp") (v "1.1.3") (h "1wwj86gma5lzm2c35n3nnakkk307gvlw4wny1z0abhq6ag01wn4p")))

(define-public crate-sexp-1.1.4 (c (n "sexp") (v "1.1.4") (h "07spb15kp06jg7qqqz9azz1blmih5p5rgx4c4fq00h7qknnag3ww")))

