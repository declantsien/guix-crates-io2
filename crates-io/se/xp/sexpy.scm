(define-module (crates-io se xp sexpy) #:use-module (crates-io))

(define-public crate-sexpy-0.1.0 (c (n "sexpy") (v "0.1.0") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1q8yackz8a25im49w83qyfwds650kr8h92vddxw3g1dyif17b4k0")))

(define-public crate-sexpy-0.1.1 (c (n "sexpy") (v "0.1.1") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.1") (d #t) (k 0)))) (h "0xb7hxg27lr6zpcz82dr4z39c35kd6yr1b6rlmvhf2kpzkf4c9qs")))

(define-public crate-sexpy-0.1.2 (c (n "sexpy") (v "0.1.2") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.1") (d #t) (k 0)))) (h "0gk3mv04v5qij75jagg0zb4mrywanxcxgbdgppz3gz1fi7g8lams")))

(define-public crate-sexpy-0.2.0 (c (n "sexpy") (v "0.2.0") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.2") (d #t) (k 0)))) (h "02f2r3q7hz70m1vgpyq6bdrlrs4zy42hxabyi8cdn6vbn2mnqggg")))

(define-public crate-sexpy-0.3.0 (c (n "sexpy") (v "0.3.0") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.3") (d #t) (k 0)))) (h "1hfizgl4aa67ck5ackcs40mask38v21qk7c8763w2cgjvqnsncly")))

(define-public crate-sexpy-0.4.0 (c (n "sexpy") (v "0.4.0") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.4") (d #t) (k 0)))) (h "0g3qbjkg6g6cacirgjgcfmriy24qaq4k9yh63rbh092ffz0pk9m5")))

(define-public crate-sexpy-0.5.0 (c (n "sexpy") (v "0.5.0") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.5") (d #t) (k 0)))) (h "1viswmj1dzjyz7cw1fxl40rkl84kc4vmcb02r1w27yb1940k5qs1")))

(define-public crate-sexpy-0.5.1 (c (n "sexpy") (v "0.5.1") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.5") (d #t) (k 0)))) (h "05iw0140lh0sz2y8napmmkffhix1dw6pw9kg08pw57y0pmqgl7z8")))

(define-public crate-sexpy-0.5.2 (c (n "sexpy") (v "0.5.2") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "~0.5") (d #t) (k 0)))) (h "0z3x9qwc7lkk2w0wzbfln4z1x3fy7lmxil9b55w83p49y6lvy1zj")))

(define-public crate-sexpy-0.5.3 (c (n "sexpy") (v "0.5.3") (d (list (d (n "nom") (r "~5.0") (d #t) (k 0)) (d (n "sexpy_derive") (r "^0.5.2") (d #t) (k 0)))) (h "0hgrnkq3n6gmfhwd9y46nf48nzv6irnircjhn6hhx32jaskndcsc")))

(define-public crate-sexpy-0.5.4 (c (n "sexpy") (v "0.5.4") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "sexpy_derive") (r "^0.5.2") (d #t) (k 0)))) (h "0h0qwribi57jbgk9ia1dcbkmhf6wjl5lc6kbv8dx7b9af4g5mn8x")))

(define-public crate-sexpy-0.5.5 (c (n "sexpy") (v "0.5.5") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "sexpy_derive") (r "^0.5.3") (d #t) (k 0)))) (h "1slzggy856nnypl7qq01fgnqw86gh2jib3swyxafqsa2gqqhsj3j")))

(define-public crate-sexpy-0.5.6 (c (n "sexpy") (v "0.5.6") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "sexpy_derive") (r "^0.5.3") (d #t) (k 0)))) (h "0lichh0ccdfxqviw1g3ldw1z5ym9x3dc442nzygf4d2n1lvsaqks")))

