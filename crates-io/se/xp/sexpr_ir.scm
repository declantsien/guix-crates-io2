(define-module (crates-io se xp sexpr_ir) #:use-module (crates-io))

(define-public crate-sexpr_ir-0.1.0 (c (n "sexpr_ir") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "057jd46kcsq5icilinydkmjjrm68hbnldqfz171ih791inbsrd96")))

(define-public crate-sexpr_ir-0.2.0 (c (n "sexpr_ir") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0s5xwjaf8bpkmwkyg8pg97rj4i3m1950s5nixais1q61125pn64v")))

(define-public crate-sexpr_ir-0.2.1 (c (n "sexpr_ir") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "04ds3kxpg3iaqqhy4bpxbzwn4bjk6iicniqxai1ckw1zj4zd6ip7")))

(define-public crate-sexpr_ir-0.3.1 (c (n "sexpr_ir") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0asc9pib2gq46s971kyhi789l12ym6x0pq8j7rwkc3aqfr1izjq3")))

(define-public crate-sexpr_ir-0.3.2 (c (n "sexpr_ir") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "03nz2my44d54llf47glqnl1i58k5z8q44d65926f3ky4vrqvjjfm")))

(define-public crate-sexpr_ir-0.4.0 (c (n "sexpr_ir") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "19l4na1fravywh3va756m23hszc4bzlb7048lf0fls3lb3ybyd6l")))

(define-public crate-sexpr_ir-0.4.1 (c (n "sexpr_ir") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "14zqwxl124xl3cnvw8wvrkcw57921xx4qfsr4rbwc0rph6sh7gx2")))

(define-public crate-sexpr_ir-0.4.2 (c (n "sexpr_ir") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1nqms2gv29bh9zvdii5k2lcq6wxafvl1pk2mp0adm3rxs9dyc5g7")))

(define-public crate-sexpr_ir-0.4.3 (c (n "sexpr_ir") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "18vxryfwn56z6hddrz95pq6p5gh6hix8cb626rpkima6cyzm5330")))

(define-public crate-sexpr_ir-0.4.4 (c (n "sexpr_ir") (v "0.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1dlvpwsdxwck9zidykpvvmmxgp5bq0acli3ijdihbclg87d1hx82")))

(define-public crate-sexpr_ir-0.5.0 (c (n "sexpr_ir") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "09w5bq3flrrny0g0k5n88igd2xhiz3yac63q8k522qx2c9xk3vpm")))

(define-public crate-sexpr_ir-0.6.0 (c (n "sexpr_ir") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "18iasgf47fvfx6fm7xi1lpzbjjm8hdvl58054faa2mqky3hg13y6")))

(define-public crate-sexpr_ir-0.6.1 (c (n "sexpr_ir") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0sqshd66ryx99403lg3ap7m8p7lmx5mn5nsb46hqibw8qjz3cpf9")))

(define-public crate-sexpr_ir-0.7.0 (c (n "sexpr_ir") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0f0vhh0chbbq5rm6bq5q7427mc0dlqkas90va0lnwcwgzzsaj0lf")))

(define-public crate-sexpr_ir-0.7.1 (c (n "sexpr_ir") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "13d5bv9fd3s4kq68h8gzisicj06f18k7i64nb50bfnxxc01r1m5v")))

(define-public crate-sexpr_ir-0.7.2 (c (n "sexpr_ir") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "15lsih1z9cjf4frddzap6500z4mc4l806567v51k3xfm7dg1zmi1")))

