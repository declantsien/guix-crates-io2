(define-module (crates-io se xp sexpr_parser) #:use-module (crates-io))

(define-public crate-sexpr_parser-0.1.0 (c (n "sexpr_parser") (v "0.1.0") (h "0raslpwyjqsjjibp6pvhq1d7lbgnziyh850gnng0xih4c2pagnlv")))

(define-public crate-sexpr_parser-0.1.1 (c (n "sexpr_parser") (v "0.1.1") (h "03433w77pyz5mdyqvldbm9k6lqjivhwdsfja9a0flfwrw4mim0a4")))

(define-public crate-sexpr_parser-0.1.2 (c (n "sexpr_parser") (v "0.1.2") (h "0aj4913m2jlryihbwh0ma16gdmpkd1lvzdsh2pqcdh2kjz4scz8b")))

(define-public crate-sexpr_parser-0.2.0 (c (n "sexpr_parser") (v "0.2.0") (h "1rrv2b3g3q0mcclbmzykr07vclr0ln7mljgl6k0gfdgyzd60a2ql")))

(define-public crate-sexpr_parser-0.2.1 (c (n "sexpr_parser") (v "0.2.1") (h "0h7fcw1w15hz6qz936f4vz21bnlxbjqbl506k0sm4qhqi1ypap79")))

(define-public crate-sexpr_parser-0.3.0 (c (n "sexpr_parser") (v "0.3.0") (h "12n6xcqfdhnfb5890jz0yfl18byxn5l0s17szjhc4r6fdk55x3yy")))

