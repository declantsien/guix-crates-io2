(define-module (crates-io se xp sexpr_process) #:use-module (crates-io))

(define-public crate-sexpr_process-0.1.0 (c (n "sexpr_process") (v "0.1.0") (d (list (d (n "sexpr_ir") (r "0.4.*") (d #t) (k 0)))) (h "1ial0f7c7syyylbdhf3bbzacxhsx3a2lgq74l4g2l3rsh68rlvi0")))

(define-public crate-sexpr_process-0.1.1 (c (n "sexpr_process") (v "0.1.1") (d (list (d (n "sexpr_ir") (r "^0.5.0") (d #t) (k 0)))) (h "1wgvyp4yjrlq4ch5ynqmz7im2cca0nbiyxajqrhn9arf8qqdx6df")))

