(define-module (crates-io se xp sexpy_derive) #:use-module (crates-io))

(define-public crate-sexpy_derive-0.1.0 (c (n "sexpy_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cnp92xaqd36yqxcal49cxb2mjwxcmkm8jxdw9qv17dj0vijldmf")))

(define-public crate-sexpy_derive-0.1.1 (c (n "sexpy_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0951lh3gl4phkgsgw2xk21ykakb0k9gvkplijx4xswfr7a4d7pw4")))

(define-public crate-sexpy_derive-0.2.0 (c (n "sexpy_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "150sb66dvndxndzmsz3sw9ikxdvqckhjvvlffjlqz9ak4bmvl9nr")))

(define-public crate-sexpy_derive-0.3.0 (c (n "sexpy_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06wda77d4b0qjm9nkznnw03ddiy4qjc4sy1wc8y870rfzip6vavi")))

(define-public crate-sexpy_derive-0.4.0 (c (n "sexpy_derive") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "115k2l6x619j9x8plf3x7nqyywsxgb7rnxaxw218v05cj2blfkq9")))

(define-public crate-sexpy_derive-0.5.0 (c (n "sexpy_derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n6z9yaxqbhlk1lwy24c34gih922mb7529mccsjj75r6192s4ihn")))

(define-public crate-sexpy_derive-0.5.2 (c (n "sexpy_derive") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08wgcw7rym89p5sl49xj24zg6xx12ym3ga8y9j15vj0iq56v5zf0")))

(define-public crate-sexpy_derive-0.5.3 (c (n "sexpy_derive") (v "0.5.3") (d (list (d (n "proc-macro-error") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mkxdqgknr428q77khia4fqv8n9wdk8w471jqdhfxid8hx1wpy6f")))

