(define-module (crates-io se jo sejong) #:use-module (crates-io))

(define-public crate-sejong-0.1.0 (c (n "sejong") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0lv7v1za9xa3nzafr118g2720dzfdyl05z8lls2bx11q9gj4rcc8") (f (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1.1 (c (n "sejong") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1c5mig1yq6rhd6vfz18s7d6rmaif0r68yirikk6vvkdzvqsfwrjr") (f (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1.2 (c (n "sejong") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0vaga104by14b4gcv6xrra68b1fxddlfy371jd7rcn9lpnffya58") (f (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1.3 (c (n "sejong") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1sqnh3vbkgp2y8frbj4d908h1pz0gpmv84nxll06phgg1qjjklil") (f (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1.4 (c (n "sejong") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0f3l7lds81xmdzyq7123q53c9icplfrrk2apv63fva4mki7ghs80") (f (quote (("wasm" "wasm-bindgen" "lazy_static" "wee_alloc"))))))

(define-public crate-sejong-0.1.5 (c (n "sejong") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ddljvq91qi1m50sc9ba0ys5qypql3hxxnzpavd9xiwl6322aqrr") (f (quote (("wasm" "wasm-bindgen" "lazy_static" "wee_alloc"))))))

