(define-module (crates-io se re serenity-commands-macros) #:use-module (crates-io))

(define-public crate-serenity-commands-macros-0.1.0 (c (n "serenity-commands-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1a2m5akfwg4pg0ixdl3xp3dzk11wp2mqqbdl5dj0yraph9yvcbm4")))

(define-public crate-serenity-commands-macros-0.2.0 (c (n "serenity-commands-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wpnwvb6yqkybxlhnrsygygg2akrq6d3sbp6bwwp69cn32y4c2gn")))

(define-public crate-serenity-commands-macros-0.3.0 (c (n "serenity-commands-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mpwl4v0hx48i68i905k98grji8l87rkiwzkdhn1w60js42kdamn")))

(define-public crate-serenity-commands-macros-0.4.0 (c (n "serenity-commands-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k21k9xhsk7ls3pqy02spp5chy3wg96l4fnfpzn2wmdv0q26yq7g")))

(define-public crate-serenity-commands-macros-0.4.1 (c (n "serenity-commands-macros") (v "0.4.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1qmmvzw1xzjlvk2lm968sycs1m1gd5cj4a9daz7hddnmjsmf7mxf")))

(define-public crate-serenity-commands-macros-0.4.2 (c (n "serenity-commands-macros") (v "0.4.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05r2i0snqgj7y6g0yjcapfqpxi1nv24h6nn62hp81k6jc68q55bk")))

(define-public crate-serenity-commands-macros-0.4.3 (c (n "serenity-commands-macros") (v "0.4.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0vnqsjldhmiry0ywwsr9pf64qm05326dc1pl1yc6cax7l35gs61k")))

