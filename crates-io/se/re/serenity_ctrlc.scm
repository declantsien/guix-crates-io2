(define-module (crates-io se re serenity_ctrlc) #:use-module (crates-io))

(define-public crate-serenity_ctrlc-0.1.0 (c (n "serenity_ctrlc") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "serenity") (r "^0.10.10") (f (quote ("rustls_backend" "gateway" "client" "model"))) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dr4k80dhr4k6qn2n7510rmj3p166irvr1fk22xr5hrgfka7vk0d")))

(define-public crate-serenity_ctrlc-0.1.1 (c (n "serenity_ctrlc") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "serenity") (r "^0.10.10") (f (quote ("rustls_backend" "gateway" "client" "model"))) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13mbsrp0fw81lv9m4pxa9yads6r8x39acnn1vlrrc8az4a885k2b")))

(define-public crate-serenity_ctrlc-0.2.0 (c (n "serenity_ctrlc") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "serenity") (r "^0.10.10") (f (quote ("rustls_backend" "gateway" "client" "model"))) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0w9ba8r185zvg2blw9ns1i8nr3svki078zrd59fbh3l353lnaqlm")))

(define-public crate-serenity_ctrlc-0.3.0 (c (n "serenity_ctrlc") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "serenity") (r ">=0.10.10, <0.12.0") (f (quote ("rustls_backend" "gateway" "client" "model"))) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yv2h25vqmhk7iz5kdc9mfkfg4bp7bg1ryb24j0bp4l07jw5zgyc")))

