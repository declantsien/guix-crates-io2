(define-module (crates-io se re serenity-slash-decode) #:use-module (crates-io))

(define-public crate-serenity-slash-decode-0.1.0 (c (n "serenity-slash-decode") (v "0.1.0") (d (list (d (n "serenity") (r "^0.10") (f (quote ("unstable_discord_api"))) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("unstable_discord_api" "client" "gateway" "rustls_backend" "builder" "model"))) (k 2)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0qla06qb8dxn9a1ydm5z7m50s0cqaicx4j2gd03c8hpx2r4lg4lh")))

(define-public crate-serenity-slash-decode-0.1.1 (c (n "serenity-slash-decode") (v "0.1.1") (d (list (d (n "serenity") (r "^0.10") (f (quote ("unstable_discord_api"))) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("unstable_discord_api" "client" "gateway" "rustls_backend" "builder" "model"))) (k 2)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "00kfvhpcnz1s58m2h9lpbwh0v7kjh8gsynqsyg2n1i3rsz1dwvb4")))

(define-public crate-serenity-slash-decode-0.1.2 (c (n "serenity-slash-decode") (v "0.1.2") (d (list (d (n "serenity") (r "^0.10.9") (f (quote ("unstable_discord_api"))) (k 0)) (d (n "serenity") (r "^0.10.9") (f (quote ("unstable_discord_api" "client" "gateway" "rustls_backend" "builder" "model"))) (k 2)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0yyymnnfmypfxgq94p1ljmrsfx4zw69xl4sdw5bq3x7if3wlq0br")))

