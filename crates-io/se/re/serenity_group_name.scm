(define-module (crates-io se re serenity_group_name) #:use-module (crates-io))

(define-public crate-serenity_group_name-0.1.0 (c (n "serenity_group_name") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "00viwhsi94w4r9d0ccmkyr24hgvm4njmf20il9pjpbw9j4fnczi3")))

