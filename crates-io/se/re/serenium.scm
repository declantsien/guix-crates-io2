(define-module (crates-io se re serenium) #:use-module (crates-io))

(define-public crate-serenium-0.1.0 (c (n "serenium") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0cj6bnyzdsq1cmhvrxdxjyacxvwczbpsvgfp29m53w55cvx0w0ib")))

