(define-module (crates-io se qn seqname) #:use-module (crates-io))

(define-public crate-seqname-0.1.0 (c (n "seqname") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1l49bvz293k7j3gkifjfv5l33r8v615h1znbnzcl3j6qqqwcxs97")))

(define-public crate-seqname-0.2.1 (c (n "seqname") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "197bscj3s4mcqm3ln7l8xamm7ncln3w5x6h25a6ns837binsnmdg")))

