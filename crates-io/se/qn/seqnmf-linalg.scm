(define-module (crates-io se qn seqnmf-linalg) #:use-module (crates-io))

(define-public crate-seqnmf-linalg-0.1.0 (c (n "seqnmf-linalg") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0c1h5i5rpxsn74ch70n1ngw5qzzmk40c7hamhxnj98y9whyg92d0")))

(define-public crate-seqnmf-linalg-0.1.1 (c (n "seqnmf-linalg") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00gwq73x2qscdrwsxyd2v4irskzhdq5v6fpvrsysk4gz8d6zdh2z")))

(define-public crate-seqnmf-linalg-0.1.2 (c (n "seqnmf-linalg") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ql17dhmsg1flw9kqbk4q9a2xg2pbs7a85cxpwlwp11yi5l7b8ik")))

(define-public crate-seqnmf-linalg-0.1.3 (c (n "seqnmf-linalg") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1wi8v101h71g1lj6sxpix783z37lydhbil2hacln33apvddkmr9x")))

(define-public crate-seqnmf-linalg-0.1.4 (c (n "seqnmf-linalg") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0psw779y8rybnw3i3xmwg4z3qaj59kw4srxwds6mh9zq5naa7inq")))

