(define-module (crates-io se t_ set_builder) #:use-module (crates-io))

(define-public crate-set_builder-1.0.0 (c (n "set_builder") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0a8g94sxbkn3sn4xbjdkffdk0pdx33jhg6lbxpji40930nrnnjhi") (y #t)))

(define-public crate-set_builder-1.0.1 (c (n "set_builder") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "131lrb9m173yan54qxwjwpdp4z1ny12a9ljwfkhigfab9c8pb92p") (y #t)))

(define-public crate-set_builder-2.0.0 (c (n "set_builder") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1i8c4ljz5jaslywmhbgwxzha7v5ji2c8qa4mcy54hyik6n88jfd3") (y #t)))

(define-public crate-set_builder-2.0.1 (c (n "set_builder") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nm6w20h4r9rrigxkayj0cp67i3bzdfg6z94xs55rj644rsq12yz") (y #t)))

(define-public crate-set_builder-3.0.0 (c (n "set_builder") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s2yyj0m2k6q6arx50kdhd0pyz53w11nz5bf111pk0213ysh0ixb") (y #t)))

(define-public crate-set_builder-3.0.1 (c (n "set_builder") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0c9wjrdhc0lvq6qsj197smn6amy73gwhvdicj9f8g44nm9qlkqlx") (y #t)))

(define-public crate-set_builder-4.0.0 (c (n "set_builder") (v "4.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0gbprnf3lpgqanwiskwgckjd1s2rgh27n854j2164an9x8hanl2j")))

(define-public crate-set_builder-5.0.0 (c (n "set_builder") (v "5.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ghckdqz31p7hq49wfracm46mp39bpa595mqffmf3md3l2y6291q")))

(define-public crate-set_builder-5.0.1 (c (n "set_builder") (v "5.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09pys6j655ag4zs62xsk9dfzvkyggxdg4dy39nbwwfjzx3i3vy83")))

