(define-module (crates-io se t_ set_slice) #:use-module (crates-io))

(define-public crate-set_slice-0.2.1 (c (n "set_slice") (v "0.2.1") (h "0ir3zjdzlxwkiqlwga2ablmb6ngxsfl0qqgy1rqvj97p02fjxbgh")))

(define-public crate-set_slice-0.2.2 (c (n "set_slice") (v "0.2.2") (h "1r855ixf9ma2cghvhlypvrkn2lxjwfpap5z6byc64ck4qfwk4dnw")))

(define-public crate-set_slice-0.2.3 (c (n "set_slice") (v "0.2.3") (h "0ayzzidwybg17xddykzkvz6p02zj5w1x8p8llcr44i5sm98sk6xi") (y #t)))

(define-public crate-set_slice-0.2.3-1 (c (n "set_slice") (v "0.2.3-1") (h "0xdnx5js3szvxqbwp19p8dbzrxm8a2192wgfwwli588ral0mi1x2") (y #t)))

(define-public crate-set_slice-0.2.4 (c (n "set_slice") (v "0.2.4") (h "17kwfmn8h3lm342dmissqcsi9x9r595gs65l30j4qyi9hg8xjz4v") (y #t)))

(define-public crate-set_slice-0.2.5 (c (n "set_slice") (v "0.2.5") (h "0d0la8qzhnmpssd87i2q8qvr04lgsfw75if703j8ixkh09gji296")))

(define-public crate-set_slice-0.2.6 (c (n "set_slice") (v "0.2.6") (h "1fm7kcf6dvhllq48ggh2ba0yqhpq5ghv8bkprcqfhl507hllj8gm")))

(define-public crate-set_slice-0.2.7-1 (c (n "set_slice") (v "0.2.7-1") (h "03wmsm0mghpb76bkskv2i4glc79i5l1m9d6pg60yiyrxmyi4jgbx")))

(define-public crate-set_slice-0.2.8 (c (n "set_slice") (v "0.2.8") (h "15g5np8hq0kbrj3fl6mbk328mvpl7lqmndccsq5hrpgr3z6h1ax5")))

(define-public crate-set_slice-0.2.9 (c (n "set_slice") (v "0.2.9") (h "1fs3b79nzlw31k5bzg41xcclals5sw8iljqinwjwy1lbhwgrf8c4")))

(define-public crate-set_slice-0.2.9-1 (c (n "set_slice") (v "0.2.9-1") (h "1f54ba31jprqm046as9c6lnip3ifzia7cbkj2jy21cmpla3sr60r")))

(define-public crate-set_slice-0.2.9-2 (c (n "set_slice") (v "0.2.9-2") (h "063f9baqzy0nyx98j0pj02smvkbn2i6knz78f2d8rxgqn2s3w11y")))

(define-public crate-set_slice-0.2.9-3 (c (n "set_slice") (v "0.2.9-3") (h "08lif5q38xjlbl11ll96bwp1hfnqyaagijn4hjd4vs6kw2a7mggb")))

(define-public crate-set_slice-0.2.10 (c (n "set_slice") (v "0.2.10") (h "0cmjgpb0icdqpxr0lnhkm0n2lsh014q8r0jgd0pnvqvv12pbimz6")))

(define-public crate-set_slice-0.3.0 (c (n "set_slice") (v "0.3.0") (h "1fcrxixlsv3hq61g6i4ji3yjz1lm4ab5x3iryr619hxsxgkyzb2w")))

