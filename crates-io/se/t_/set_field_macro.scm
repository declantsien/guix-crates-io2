(define-module (crates-io se t_ set_field_macro) #:use-module (crates-io))

(define-public crate-set_field_macro-0.1.0 (c (n "set_field_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j4w90pm6ghshg9r0fpy8m5hhd6nj4sys5sr5cxjzl2mnrfqx7l3")))

(define-public crate-set_field_macro-0.1.1 (c (n "set_field_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1y43j6kly6l1i08xvmvv0c5626bm0gjz47gavh54skvsk2yrsvq4")))

