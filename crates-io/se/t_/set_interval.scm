(define-module (crates-io se t_ set_interval) #:use-module (crates-io))

(define-public crate-set_interval-0.1.0 (c (n "set_interval") (v "0.1.0") (h "19qm75psizqnypqlyaflk966hr4hv3b8j2wamcrqs6r0hkkdab6m") (y #t)))

(define-public crate-set_interval-0.1.1 (c (n "set_interval") (v "0.1.1") (h "0jca43mgq0ryg05jwmfprjwn1njwvzn9r2irk68fjalc353nndcm") (y #t)))

