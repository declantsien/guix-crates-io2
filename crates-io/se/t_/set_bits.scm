(define-module (crates-io se t_ set_bits) #:use-module (crates-io))

(define-public crate-set_bits-0.1.0 (c (n "set_bits") (v "0.1.0") (h "09xljg3gi7lzfw68l54s5p8ncr75vvzx3q1d08hfdnh60l6a5lbx")))

(define-public crate-set_bits-1.0.0 (c (n "set_bits") (v "1.0.0") (h "007icwazbnqa8f3qy4gvbqwr1ym0w3rrhl9n6lmnfbn28k4s35rj")))

(define-public crate-set_bits-1.0.1 (c (n "set_bits") (v "1.0.1") (h "07qvqyfvinr6695f1qx6a0dh5jcz54f9ah7qfzwck56p8xxcd9sr")))

(define-public crate-set_bits-1.0.2 (c (n "set_bits") (v "1.0.2") (h "1c5h75rxysa5y7gf2dfbpp59jblkhqh7qqn4grh26avinz18l95v")))

(define-public crate-set_bits-1.0.3 (c (n "set_bits") (v "1.0.3") (h "0q0yzl41sxs33nl7c3m9w9jb1myrildkk0sshddblcg390s8p9ag")))

(define-public crate-set_bits-1.1.0 (c (n "set_bits") (v "1.1.0") (h "19lwixp04v0d937j1bychh3gy9qxq3mqc5mmvqviykh97ly7rd56")))

(define-public crate-set_bits-1.1.1 (c (n "set_bits") (v "1.1.1") (h "13gjkvkw5g09ac9m3cka97w1b83pvcy4dsgflbxyz5fzv3s1p0df")))

(define-public crate-set_bits-1.2.0 (c (n "set_bits") (v "1.2.0") (h "0p55ajwx8vrscmv2v1w88hxq7dz3rk2fkz2s48w7fqzb04rbfdfw")))

