(define-module (crates-io se t_ set_enum_fields) #:use-module (crates-io))

(define-public crate-set_enum_fields-0.1.5 (c (n "set_enum_fields") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "executors") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k110053qjyb6g8gm45l2vd3ghd2rfh4bj013v9r25fbbshf3350")))

