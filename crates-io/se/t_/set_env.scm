(define-module (crates-io se t_ set_env) #:use-module (crates-io))

(define-public crate-set_env-1.0.0 (c (n "set_env") (v "1.0.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Registry" "Win32_Foundation" "alloc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14mjcaznl1h8f550fiqn24vafr0nf2d6s79jpvzyjl1i1x0jsmnq")))

(define-public crate-set_env-1.0.1 (c (n "set_env") (v "1.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Registry" "Win32_Foundation" "alloc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11vxan1p1q6x9kawkjybjd6g61aag61zhjd9b7xz6iigpknfmhv6")))

(define-public crate-set_env-1.0.2 (c (n "set_env") (v "1.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Registry" "Win32_Foundation" "alloc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wdcx5jcj8mmagjwl5vg58fc3jkjddqg87r56rhxpc3mb0gwgr2m")))

(define-public crate-set_env-1.1.0 (c (n "set_env") (v "1.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Registry" "Win32_Foundation" "alloc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05c6vf21xwld74lbx10pgna04nrwmr43mryjfb7zdsxl83ffirzr")))

(define-public crate-set_env-1.1.1 (c (n "set_env") (v "1.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Registry" "Win32_Foundation" "alloc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xj9zc8rn2phf8d3i0fpaffm58j3360q93vvqcyjlvnvqdwwrpwb")))

(define-public crate-set_env-1.2.0 (c (n "set_env") (v "1.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "1sxlrdx36j4xxjy500bqvzfwnpnrr6y0kl4w50zbgli2c2ay380b")))

(define-public crate-set_env-1.2.1 (c (n "set_env") (v "1.2.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "09sry7a2saz0kkmqmssjplaj9iaz0g0rdq1lr17iw1n6rgsvcvqc")))

(define-public crate-set_env-1.2.2 (c (n "set_env") (v "1.2.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "04p6jniadsly7a3ylrghpp7agxidwxxy34mywcsmngknprm8ll7n")))

(define-public crate-set_env-1.3.0 (c (n "set_env") (v "1.3.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "16nc4cq7k392r1xrsskvgslhmwdr6l9r9fpkxssfpmjp06vbklmd")))

(define-public crate-set_env-1.3.1 (c (n "set_env") (v "1.3.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "0dqqpa13m8q40h9vbr8nik1flhh0jkjz37y1ywqw92h6x2blmn7l")))

(define-public crate-set_env-1.3.2 (c (n "set_env") (v "1.3.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "14kbj2kbilh3w92jxi0ww0r1l2ij0jpivf3n96x5kz8gphxyik19")))

(define-public crate-set_env-1.3.3 (c (n "set_env") (v "1.3.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "19q9kgm7g90wi359ivz2v0pdly5787kvk9r99f6dqcv0f34q7sha")))

(define-public crate-set_env-1.3.4 (c (n "set_env") (v "1.3.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1ssqbja6rj2bnl2cxjfg4ms2k9dxzcmq648sjdyqm6yh5yv4z1ji")))

