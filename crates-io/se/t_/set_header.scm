(define-module (crates-io se t_ set_header) #:use-module (crates-io))

(define-public crate-set_header-0.1.1 (c (n "set_header") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "axummid") (r "^0.1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1q0qniby9q9r357f5ck0m69xlibllnzsfhazgixv7jscjddrcj8s")))

(define-public crate-set_header-0.1.2 (c (n "set_header") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "axummid") (r "^0.1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0jvlimg90rv0cvfgda72456p2ii2qjg2rv2gwydw8gii12jm6irf")))

(define-public crate-set_header-0.1.3 (c (n "set_header") (v "0.1.3") (d (list (d (n "amid") (r "^0.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "13il2v51cz2glxb3kwnigjj7ckph7wqdm62054ign6dr9zflmwid")))

(define-public crate-set_header-0.1.4 (c (n "set_header") (v "0.1.4") (d (list (d (n "amid") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1rq2j7aa6v3hzkwswsq86097pk17hin7lixc4x6gmq44hbwc3k3c")))

(define-public crate-set_header-0.1.5 (c (n "set_header") (v "0.1.5") (d (list (d (n "amid") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "17rilhkyq5gjszqd5rramvf7qsfnjy5pwp08vb88nvm613mvkak2")))

(define-public crate-set_header-0.1.6 (c (n "set_header") (v "0.1.6") (d (list (d (n "amid") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "01vwqqmz52z8j5p51fg1siyazlxqyzsbaa74f5i9p3dxqqxkmz9d")))

(define-public crate-set_header-0.1.7 (c (n "set_header") (v "0.1.7") (d (list (d (n "amid") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "10blnv5m1fwsszd1dcs0m5kl7r03vdgbxsahy2l6zkqlds3lr2h9")))

(define-public crate-set_header-0.1.8 (c (n "set_header") (v "0.1.8") (d (list (d (n "amid") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04azr8879rabi6h5vimg6b4j8svda1d5hcsa4y0jdwl145h3xzhd")))

