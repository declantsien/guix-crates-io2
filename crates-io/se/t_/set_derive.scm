(define-module (crates-io se t_ set_derive) #:use-module (crates-io))

(define-public crate-set_derive-0.1.0 (c (n "set_derive") (v "0.1.0") (h "1kjdbd1fk6fwq71mw1mrzdka0gbw75clh60my8zbr7cssbshry2p")))

(define-public crate-set_derive-0.1.1 (c (n "set_derive") (v "0.1.1") (h "1d738a2d578g08a5lwc8zv22q2cvcfap4da703zyyxnbihmxk2j9")))

(define-public crate-set_derive-0.1.2 (c (n "set_derive") (v "0.1.2") (h "1r8pq0ix0lg3sdrpbwhshqh956f5av1qnvljlp471zkri4k0rjgq")))

(define-public crate-set_derive-0.1.3 (c (n "set_derive") (v "0.1.3") (h "1gypixysi71pzjlpj9bh0cgxfdwcyrsk40bb2hd2hbj2zwp9rkw9")))

(define-public crate-set_derive-0.1.4 (c (n "set_derive") (v "0.1.4") (h "1mwg4jpylhwyalgdv2x68ncg2gk661nf32ll0x0i9jialpfgn056")))

(define-public crate-set_derive-1.0.0 (c (n "set_derive") (v "1.0.0") (h "00wrafpv2fs2z23n47xizd12dia533lgig36xvydkkm123p69dy0")))

(define-public crate-set_derive-1.1.0 (c (n "set_derive") (v "1.1.0") (h "1l3vm4hnchran1j4c2cjqg2v9gmlzzbffz930ah2khlxzpjvlqar")))

