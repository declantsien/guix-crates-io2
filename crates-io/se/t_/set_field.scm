(define-module (crates-io se t_ set_field) #:use-module (crates-io))

(define-public crate-set_field-0.1.0 (c (n "set_field") (v "0.1.0") (d (list (d (n "set_field_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0n2zb0b5sdncbrn85yai4cfpgszp9pmqbd0cn4fxdp1n8aa09fwn")))

(define-public crate-set_field-0.1.1 (c (n "set_field") (v "0.1.1") (d (list (d (n "set_field_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1bky0bp8wgikcw1ny3zw16hpllckg7k1zq0bxd22shpbky88zfvw")))

