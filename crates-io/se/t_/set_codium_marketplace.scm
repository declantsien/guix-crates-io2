(define-module (crates-io se t_ set_codium_marketplace) #:use-module (crates-io))

(define-public crate-set_codium_marketplace-0.1.0 (c (n "set_codium_marketplace") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1xkr8jjymrfgi7d98iznkk1skzb9sa3hz623qr72jwb8qpz3dqrv")))

(define-public crate-set_codium_marketplace-0.1.1 (c (n "set_codium_marketplace") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1vd56qby1qp410awl806cd9wv092l617r4nlp83r3ni20dapz88l")))

