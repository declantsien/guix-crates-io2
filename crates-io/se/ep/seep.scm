(define-module (crates-io se ep seep) #:use-module (crates-io))

(define-public crate-seep-0.1.0 (c (n "seep") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c0brzsavbz4sandda2bqwp5vfapcxb3sljhvf68zlk048vaqn7r")))

(define-public crate-seep-0.1.1 (c (n "seep") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w98qwhlld9s8v2ly6fc08ky9rfr5anx9z032wkvk8ph0a20cds0")))

