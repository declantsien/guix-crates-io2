(define-module (crates-io se ep seeport) #:use-module (crates-io))

(define-public crate-seeport-0.1.0 (c (n "seeport") (v "0.1.0") (d (list (d (n "coingecko") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1c7qmkzl5cmrnwnwcnnaxh8dmx6cali9j5if7mcbfbhfzbbwd7p9")))

(define-public crate-seeport-0.1.1 (c (n "seeport") (v "0.1.1") (d (list (d (n "coingecko") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "10lnvr82ac6scz33pcbiwhnx9csdfrzbva1wychx4v9sbsycpf7y")))

(define-public crate-seeport-0.1.2 (c (n "seeport") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "coingecko") (r "^1.0.0") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "108xj962fid3vsq23rn0lxf75lqg2zmdwjnr3pap2a4hll3cy8l4")))

