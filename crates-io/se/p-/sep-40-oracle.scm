(define-module (crates-io se p- sep-40-oracle) #:use-module (crates-io))

(define-public crate-sep-40-oracle-0.1.0 (c (n "sep-40-oracle") (v "0.1.0") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0aylx6bxa66hqgnkskvf7p0ffpblvk7f84w5gyx4qsffgi3n2yvv") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-0.1.1 (c (n "sep-40-oracle") (v "0.1.1") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1lilbcikdysfl9aaamw9cb2nr92cnvm0h2grg9x7qf8fns6rwn7s") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-0.2.0 (c (n "sep-40-oracle") (v "0.2.0") (d (list (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "10d175ks28rv6jrggkf1kmvy73n7h5xplvmy68xag8p2cyc3wfga") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-1.0.0 (c (n "sep-40-oracle") (v "1.0.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "14zvly8n9qacvzscdjp3fj3hmh0gw2wa9jy6cw2la7cp483nl6w4") (f (quote (("testutils" "soroban-sdk/testutils"))))))

