(define-module (crates-io se p- sep-sys) #:use-module (crates-io))

(define-public crate-sep-sys-1.1.1+1.0.0 (c (n "sep-sys") (v "1.1.1+1.0.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1fs92qwqzj1qyi89n0qd7azg66xwr0dq82d27km70xb6jwc7d64y") (y #t) (l "sep")))

(define-public crate-sep-sys-1.1.1+1.0.1 (c (n "sep-sys") (v "1.1.1+1.0.1") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1qi5lkl2qv779jrbvvqdzl2gv1p60a8nz8gb5jqh2fh28lcdg9d7") (l "sep")))

(define-public crate-sep-sys-1.2.0 (c (n "sep-sys") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1hsx56a53h163sgr1wsa9q94lqha1zikp7ghy2g2zlfwp51kfkig") (l "sep")))

