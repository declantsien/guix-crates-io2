(define-module (crates-io se p- sep-41-token) #:use-module (crates-io))

(define-public crate-sep-41-token-0.2.0 (c (n "sep-41-token") (v "0.2.0") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0yc20jf6ld6ck0zigynwxif8fi7qn6w5sz4r997ldnk27f3y5xji") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-0.2.1 (c (n "sep-41-token") (v "0.2.1") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0y4nbgbiq5p48rnv93jib3l9mx54jxzx41gp9cv87xys2jhvdvg0") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-0.3.0 (c (n "sep-41-token") (v "0.3.0") (d (list (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1zv74lpz8jfzazynj8qfrbvv8yk7hjyk915cz4qivbfjsrywacl8") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-1.0.0 (c (n "sep-41-token") (v "1.0.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "130nqdph8l3kwf8qsmpc2mljg25zgc9wi5bf0ylsjd6v56c2yld0") (f (quote (("testutils" "soroban-sdk/testutils"))))))

