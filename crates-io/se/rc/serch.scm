(define-module (crates-io se rc serch) #:use-module (crates-io))

(define-public crate-serch-0.1.0 (c (n "serch") (v "0.1.0") (d (list (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "14i88q0vsk2ays5z38anrjdjv2ncpg0n36cxcfdf493fjgwx9wgx")))

(define-public crate-serch-0.1.1 (c (n "serch") (v "0.1.1") (d (list (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "152nw36cc5vjcf7klkc89v2r2grpwmwhj19prnn1jvvgj5vvg5zb")))

