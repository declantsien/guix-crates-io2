(define-module (crates-io se qu sequelite) #:use-module (crates-io))

(define-public crate-sequelite-0.2.0 (c (n "sequelite") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "sequelite_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0mdilhvyp6nkzvrzr566c4shsx3bx6rvddwk30ikfh6hj2q0f1zk") (f (quote (("default" "chrono") ("chrono" "rusqlite/chrono") ("bundled" "rusqlite/bundled"))))))

(define-public crate-sequelite-0.2.1 (c (n "sequelite") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "sequelite_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1z11bvvahy6x406a089wx1l8dxxn5v3gwpzb20qw8c0df90zza3h") (f (quote (("default" "chrono") ("chrono" "rusqlite/chrono") ("bundled" "rusqlite/bundled"))))))

(define-public crate-sequelite-0.2.2 (c (n "sequelite") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "sequelite_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0kdz7p2b165bj19q59js516pndp0z4dfl71j6cyklwkn5md34yvm") (f (quote (("default" "chrono") ("chrono" "rusqlite/chrono") ("bundled" "rusqlite/bundled"))))))

(define-public crate-sequelite-0.2.3 (c (n "sequelite") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "sequelite_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1v64wiap3yl2v9f2mwdx6mdyghqy30nxn432ysirh01fhcwxsx4k") (f (quote (("default" "chrono") ("chrono" "rusqlite/chrono") ("bundled" "rusqlite/bundled"))))))

