(define-module (crates-io se qu sequence-generator-rust) #:use-module (crates-io))

(define-public crate-sequence-generator-rust-0.1.0 (c (n "sequence-generator-rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1vwcbgh93myjz1irfibm3jga1qvmg42nz1pc942gzi780ln7w79i") (y #t)))

(define-public crate-sequence-generator-rust-0.1.1 (c (n "sequence-generator-rust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "11ihdpg06bkbljhhpzqgd6a4dir7apr6dq5rm308x5h535nzkwxa") (y #t)))

(define-public crate-sequence-generator-rust-0.1.2 (c (n "sequence-generator-rust") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1yh1l1ihryh3fisjja24qjm1glxnvnchn6q6kjs14y4582zydv9s") (y #t)))

(define-public crate-sequence-generator-rust-0.2.0 (c (n "sequence-generator-rust") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "04kx5f591q97prwainyhaim2vxl0cnhpllpk670mblb0qzgxmhyz") (y #t)))

(define-public crate-sequence-generator-rust-0.2.1 (c (n "sequence-generator-rust") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0wdc3iyhajf4m32lrgcsaq9q861lhi8b4s0vfrbcr2ikv7ij37g7")))

(define-public crate-sequence-generator-rust-0.2.2 (c (n "sequence-generator-rust") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hgjy136hqv1cl0cb0zm9a5spb3q19v2pbdsmw2xxj8sirslw205")))

(define-public crate-sequence-generator-rust-0.3.0 (c (n "sequence-generator-rust") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1x0qpqvjj2p77hgfqw0dw7qyzk1gbcwdr64lz12y593mhiqdv4c6")))

(define-public crate-sequence-generator-rust-0.3.1 (c (n "sequence-generator-rust") (v "0.3.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0997jr09mhs06a98mlv0m0nsahn7y3qjlarg8hc5wn2j2hh3bb62")))

(define-public crate-sequence-generator-rust-0.3.2 (c (n "sequence-generator-rust") (v "0.3.2") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 0)))) (h "119c6365lhxz0mjnbia0qyzjfw0rjz4whx2awbbmsww6l3ln9rff")))

(define-public crate-sequence-generator-rust-0.4.0 (c (n "sequence-generator-rust") (v "0.4.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 0)))) (h "05ljmmvx53ihl43k067bil25h8gl7nwh0l391xg6ngcnazkf6jqw")))

(define-public crate-sequence-generator-rust-0.4.1 (c (n "sequence-generator-rust") (v "0.4.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1fnjc8r286fz8x3zcgbq8likxyg6g9knnzcqw426vyiykv8y45c6")))

