(define-module (crates-io se qu sequelite_macro) #:use-module (crates-io))

(define-public crate-sequelite_macro-0.2.0 (c (n "sequelite_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ixmm0xzmr509cvzyvrzz2pn8imbii6c79c2jhskxssrjbk5m6b2")))

(define-public crate-sequelite_macro-0.2.1 (c (n "sequelite_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "087cw0zn5azl29mgx2mag0nsqc4yinv4kl03y7rr46m84w66xjvr")))

