(define-module (crates-io se qu sequent-repl) #:use-module (crates-io))

(define-public crate-sequent-repl-0.1.0 (c (n "sequent-repl") (v "0.1.0") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "revolver") (r "^0.1.0") (d #t) (k 0)) (d (n "sequent") (r "^0.1.0") (d #t) (k 0) (p "sequent")) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0i6sic82dwjchg96myhxc5jy96n73mm51h11jzjpl28xc2cacnzz")))

(define-public crate-sequent-repl-0.2.0 (c (n "sequent-repl") (v "0.2.0") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "revolver") (r "^0.1.0") (d #t) (k 0)) (d (n "sequent") (r "^0.2.0") (d #t) (k 0) (p "sequent")) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "12xfpndc1rhknah3h9isb0fl9qwwm7z133zi5msjbf5z3dk528l4")))

(define-public crate-sequent-repl-0.2.1 (c (n "sequent-repl") (v "0.2.1") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "revolver") (r "^0.1.0") (d #t) (k 0)) (d (n "sequent") (r "^0.2.1") (d #t) (k 0) (p "sequent")) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1fliqnb77acaqvn0y293lycwsx6blb1qwbknk9vqynq68y1vr3pw")))

(define-public crate-sequent-repl-0.3.0 (c (n "sequent-repl") (v "0.3.0") (d (list (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "revolver") (r "^0.2.0") (d #t) (k 0)) (d (n "sequent") (r "^0.3.0") (d #t) (k 0) (p "sequent")) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "stanza") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1prw8qakc3zlfydhlzgi67b2by1d8j9lh1g5zdnm79iaw5gypwi4")))

