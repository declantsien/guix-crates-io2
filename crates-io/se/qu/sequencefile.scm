(define-module (crates-io se qu sequencefile) #:use-module (crates-io))

(define-public crate-sequencefile-0.1.0 (c (n "sequencefile") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1df0zza0xz7m81yfgh3kv8bbsgwcqd2l4lg208pw1jf7hxpzh729")))

(define-public crate-sequencefile-0.1.1 (c (n "sequencefile") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "19fm34lb9kc6a8rw4hv7h5liz6hvvvszsqwsamqzifmgpy2djr39")))

(define-public crate-sequencefile-0.1.2 (c (n "sequencefile") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "bzip2") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0sin5v97qns18ij695ia0iaszmjh24pxlmrca8lb5yaz0akhn0ly")))

(define-public crate-sequencefile-0.1.3 (c (n "sequencefile") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "bzip2") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0jlp21978mb19igqx8bv9kx76j8c613vfrddnp7610ymr4c9iq0y")))

(define-public crate-sequencefile-0.1.4 (c (n "sequencefile") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "bzip2") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0hbcnzih4vhs0ybyx54226hq6dm8fdhlzx4y9dvg31kyni8r28lq")))

(define-public crate-sequencefile-0.2.0 (c (n "sequencefile") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "bzip2") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0ddjgfqyrkx64i1x8riq4ygfgvj5nclrh1wwdn1ydnjyih8x1apa")))

