(define-module (crates-io se qu sequence_trie) #:use-module (crates-io))

(define-public crate-sequence_trie-0.0.1 (c (n "sequence_trie") (v "0.0.1") (h "04385xn21x1wbpwf64zkm1qaazigs967zhcgmnhjkvncirfahbni")))

(define-public crate-sequence_trie-0.0.2 (c (n "sequence_trie") (v "0.0.2") (h "0419zs52918caf79mwggkag32qm4b7jkshfgwgcqxs9jvb9nnbv3")))

(define-public crate-sequence_trie-0.0.3 (c (n "sequence_trie") (v "0.0.3") (h "1wh204vq6afvyyqwrib4rancpx149yvxhdy1dmad7q0l758cbbnz")))

(define-public crate-sequence_trie-0.0.4 (c (n "sequence_trie") (v "0.0.4") (h "1p03w0nlir2x98djvkpz391gcj7v1zd44916kqmmwp06igl8wyhl")))

(define-public crate-sequence_trie-0.0.5 (c (n "sequence_trie") (v "0.0.5") (h "1lwspkqkhjszd5vm7wy99ydwq1vz52k80qs7hw6jnx253bbrlyg8")))

(define-public crate-sequence_trie-0.0.6 (c (n "sequence_trie") (v "0.0.6") (h "0n6a0rmdlibqff219q4k5y37izbwfc0fliryzlv0mw8ksqjksl8h")))

(define-public crate-sequence_trie-0.0.7 (c (n "sequence_trie") (v "0.0.7") (h "17yackp527xdzyym4q3kz2dy8qc7rzyrwzkkk1849y9hvalyapjw")))

(define-public crate-sequence_trie-0.0.8 (c (n "sequence_trie") (v "0.0.8") (h "01ml3ai66k164ny77wzdaayb4klzzsxxr0a2ra3b6ximwmh4q2xc")))

(define-public crate-sequence_trie-0.0.9 (c (n "sequence_trie") (v "0.0.9") (h "194m78csww6354fwsnz1g5wjs6rjc4mhfqavg1by4rlafcg5byv7")))

(define-public crate-sequence_trie-0.0.10 (c (n "sequence_trie") (v "0.0.10") (h "0p8akhgadjj42y096x48s9sws1ldy88rxc66f7s1bar83jr9dvkg")))

(define-public crate-sequence_trie-0.0.11 (c (n "sequence_trie") (v "0.0.11") (h "154g4w6x880rmgbpspsjplk3lxfzll5miz4q4xr884sb8mlj5ql1")))

(define-public crate-sequence_trie-0.0.12 (c (n "sequence_trie") (v "0.0.12") (h "1g6m1p60g8lg43ngsy4lck06rv2q1assgwfs3237x408midbis5l")))

(define-public crate-sequence_trie-0.0.13 (c (n "sequence_trie") (v "0.0.13") (h "1s0vr9hgm3wqw6qaasaj09jd8pbhyd1zb3wbdmkbky8zgl7ypd6m")))

(define-public crate-sequence_trie-0.1.0 (c (n "sequence_trie") (v "0.1.0") (h "1di32bvgq3diiwp5nc4fxjrwkcpnmb33pfdyak13dj8jcpk3s7qb")))

(define-public crate-sequence_trie-0.2.0 (c (n "sequence_trie") (v "0.2.0") (h "1a887df1332zws8ygx254g54wiw2rjixlbpl5sxb9jb5v158ndbz")))

(define-public crate-sequence_trie-0.2.1 (c (n "sequence_trie") (v "0.2.1") (h "1vby6b3bx0cwyx71i38ani2zwcpn7wmdgagnp3bd9c9km16725f9")))

(define-public crate-sequence_trie-0.3.0 (c (n "sequence_trie") (v "0.3.0") (h "1br6mpsbbgryqa977rsmvxbyialaki7qd3psf3r8i8r712wxz0ga")))

(define-public crate-sequence_trie-0.3.1 (c (n "sequence_trie") (v "0.3.1") (h "009026v2m9rzgm8slyivyf3nwd7fb6jp6499k1xrd3psdssa4m0i")))

(define-public crate-sequence_trie-0.3.2 (c (n "sequence_trie") (v "0.3.2") (h "1ddn5587knxhcwdzmp6hzid1l2z26c1gwail00ssw7nv3rjwsgyy")))

(define-public crate-sequence_trie-0.3.3 (c (n "sequence_trie") (v "0.3.3") (h "0cwldaacxfcqzvy0y5iqa4pjvyk8k7c5n3lf787z01wqz56y23c8")))

(define-public crate-sequence_trie-0.3.4 (c (n "sequence_trie") (v "0.3.4") (h "0c8q997fq5v2ikvapigqsf4iralypp377pmr2rb21x5ilazzw4zc")))

(define-public crate-sequence_trie-0.3.5 (c (n "sequence_trie") (v "0.3.5") (h "0lf3fqdqc0z4n62w08wf1v73axlqdvlmczmx0x0c1ly9wl27459j")))

(define-public crate-sequence_trie-0.3.6 (c (n "sequence_trie") (v "0.3.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hz0h12ihsk793yrbs291p6n2fxkw73bjm24nvp75l6cnxkj1qhy") (f (quote (("btreemap"))))))

