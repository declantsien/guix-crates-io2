(define-module (crates-io se qu sequeda-password-generator) #:use-module (crates-io))

(define-public crate-sequeda-password-generator-0.1.0 (c (n "sequeda-password-generator") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 0)))) (h "0zwg4jdaz63l1sizn0y0012hwzdq66r2nvqa3jp542r44m8wkndl")))

