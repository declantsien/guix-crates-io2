(define-module (crates-io se qu sequel) #:use-module (crates-io))

(define-public crate-sequel-0.1.0 (c (n "sequel") (v "0.1.0") (d (list (d (n "postgres") (r "*") (f (quote ("uuid" "rustc-serialize" "time"))) (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "07ja95kx2k5jdmznklqpddbdjr2pwhxxkvl4kdylz1463vfdxfin") (f (quote (("default" "postgres"))))))

