(define-module (crates-io se qu sequoia-directories) #:use-module (crates-io))

(define-public crate-sequoia-directories-0.1.0 (c (n "sequoia-directories") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "same-file") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m9plvzm61571y1vzsp3jkba2mgbxgwckrbpmcbqdky5c24x87dh") (r "1.70")))

