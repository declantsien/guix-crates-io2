(define-module (crates-io se qu sequence-map) #:use-module (crates-io))

(define-public crate-sequence-map-0.1.0 (c (n "sequence-map") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "0qvin7bhc1mgx7y75sczjbr0w01zbrcyx3s5dakmpr8c5y2a4mm2")))

