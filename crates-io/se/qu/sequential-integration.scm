(define-module (crates-io se qu sequential-integration) #:use-module (crates-io))

(define-public crate-sequential-integration-0.0.1 (c (n "sequential-integration") (v "0.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "mexprp") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "12cki5cry4bnaslkpmmvwm66q290rxqgmghc6ralrjkr854db00k")))

(define-public crate-sequential-integration-0.0.2 (c (n "sequential-integration") (v "0.0.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "mexprp") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "1xmw5gbp360bcgh1r594ik6pjv1xqaq0ppfky4bak6x3pz9d30h0")))

(define-public crate-sequential-integration-0.0.3 (c (n "sequential-integration") (v "0.0.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "mexprp") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "1j4s7689lb0cxqszk5nhj9iab5fkni9xwq5hcyrh7n66c1ias985")))

(define-public crate-sequential-integration-0.0.4 (c (n "sequential-integration") (v "0.0.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "mexprp") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "1nxwsg4xggi5b99pzgm3w60gwa93fjmf6k8cinr22gv6pqj4krzm")))

(define-public crate-sequential-integration-1.0.0 (c (n "sequential-integration") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "0yc6igrh8d4li4piv89kyclaj7dqs93qcx4zw822lglzbyzxsjyh")))

(define-public crate-sequential-integration-1.0.1 (c (n "sequential-integration") (v "1.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "19v9xkbxkqcjnn0zawrbg8wkwhvfcxl59wvdhrcg06pmjfslirlc")))

(define-public crate-sequential-integration-1.0.2 (c (n "sequential-integration") (v "1.0.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces" "backtraces-impl-backtrace-crate"))) (d #t) (k 0)))) (h "066qlx25wkk5hmm1djljc861qchiqr1dpgj58pi8xcy37fha5ra1")))

