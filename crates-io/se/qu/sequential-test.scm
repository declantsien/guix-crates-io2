(define-module (crates-io se qu sequential-test) #:use-module (crates-io))

(define-public crate-sequential-test-0.1.0 (c (n "sequential-test") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1q474q0w0ys6fpfp4krbglgjwy244rnnlwb3n1l4gc9ijh78ryhr")))

(define-public crate-sequential-test-0.1.1 (c (n "sequential-test") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sjynqlqfnhwi6dd81izj9mfw5ffkym61jsqn0lymnf2i6z8khl3")))

(define-public crate-sequential-test-0.1.2 (c (n "sequential-test") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sequential-macro") (r "^0.1.0") (d #t) (k 0)))) (h "12byghw4bfkcw95sxisx47ggsh5raj1h9r5bsvb1550ccbxbnx4y")))

(define-public crate-sequential-test-0.1.3 (c (n "sequential-test") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sequential-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0cii05qvr4rh84nir8yla8amkc1lals34svk4y4c1vpxa1g9r3ji")))

(define-public crate-sequential-test-0.1.4 (c (n "sequential-test") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sequential-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0s2md3m69psvpd7255i5vp8kh5q66ix9dk4x11a74pfs0li87zmv")))

(define-public crate-sequential-test-0.2.4 (c (n "sequential-test") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sequential-macro") (r "^0.1.4") (d #t) (k 0)))) (h "1sbrzf6yz7kln16jimnx44253ch0z9fhwijg4qrpfzmwfgbw1ngh")))

