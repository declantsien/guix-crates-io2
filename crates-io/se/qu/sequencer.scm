(define-module (crates-io se qu sequencer) #:use-module (crates-io))

(define-public crate-sequencer-0.1.0 (c (n "sequencer") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1rl8d06kz3ky39dfckl4c0jfg3fk05k5l2ncxd2384kqbgz2gs23")))

(define-public crate-sequencer-0.1.1 (c (n "sequencer") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1r75zr5rpqlapx25gk009jc431bhqjmsp29hfck430a4b2dd1xli")))

(define-public crate-sequencer-0.7.0 (c (n "sequencer") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1rr5bljpbymwg1yvidladhb6lzpdrcavjrfw5bmgcpvnd3qhn47z") (y #t)))

