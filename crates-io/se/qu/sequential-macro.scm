(define-module (crates-io se qu sequential-macro) #:use-module (crates-io))

(define-public crate-sequential-macro-0.1.0 (c (n "sequential-macro") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1sqzik89wqh5bmrig0zknf700kzbxfgzklkk3gg1pv2v94fpar3l")))

(define-public crate-sequential-macro-0.1.1 (c (n "sequential-macro") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1n6v35p0cff1k1f9ag1ibxjpfbrgxifkxq0v5yr98i4vsqfna2b0")))

(define-public crate-sequential-macro-0.1.4 (c (n "sequential-macro") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01sdjh8xfp9z0ibnfckx163i23m008s8a717pwjmv989yk2sqpzb")))

(define-public crate-sequential-macro-0.2.4 (c (n "sequential-macro") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0chxw769s4f8anjw9ps0basbld9zlgawqmb5m4ri5cz759b7aq5s")))

