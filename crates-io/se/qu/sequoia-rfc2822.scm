(define-module (crates-io se qu sequoia-rfc2822) #:use-module (crates-io))

(define-public crate-sequoia-rfc2822-0.6.0 (c (n "sequoia-rfc2822") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)))) (h "0nhqs66g3bw77z6dh2yh9d3hzq2v5zdgj7f4df6pd9x4ixkqdkr7") (y #t)))

(define-public crate-sequoia-rfc2822-0.7.0 (c (n "sequoia-rfc2822") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1m6wwi5av5d5nwa5k0fyv2c32g7024w2nry4506prib50bplq3jd") (y #t)))

(define-public crate-sequoia-rfc2822-0.8.0 (c (n "sequoia-rfc2822") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0phy1sxnpvvh8lwk2cgcc2c0rhr37avrsanvi2g0sz0lljxb5yrn") (y #t)))

(define-public crate-sequoia-rfc2822-0.9.0 (c (n "sequoia-rfc2822") (v "0.9.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1aj34i6862718m162rqfv69fkmvdw063s6ws7hbp42n73gb08p5c") (y #t)))

(define-public crate-sequoia-rfc2822-0.10.0 (c (n "sequoia-rfc2822") (v "0.10.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (k 2)))) (h "0qbk61lrbm1nngj4wb8yjafq431aywi67y94vk4hzk72gxi1c397") (y #t)))

