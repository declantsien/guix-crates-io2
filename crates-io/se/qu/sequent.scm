(define-module (crates-io se qu sequent) #:use-module (crates-io))

(define-public crate-sequent-0.1.0 (c (n "sequent") (v "0.1.0") (d (list (d (n "flanker-assert-str") (r "^0.5.0") (d #t) (k 2)) (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05nyc1aspzqf8vg8br2p4jvrk342ny8x4lyq39ig20dsnnbvwzbk")))

(define-public crate-sequent-0.2.0 (c (n "sequent") (v "0.2.0") (d (list (d (n "flanker-assert-str") (r "^0.5.0") (d #t) (k 2)) (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05hdin8x8f7wgcad3inmd19l9v322ajfa3b4j3ncjasqzzknfnnd")))

(define-public crate-sequent-0.2.1 (c (n "sequent") (v "0.2.1") (d (list (d (n "flanker-assert-str") (r "^0.5.0") (d #t) (k 2)) (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0y5qwkxv88rv9wj1rzpd9hw4xv5ikhh0lgf9gaa5spypb3yh7m29")))

(define-public crate-sequent-0.3.0 (c (n "sequent") (v "0.3.0") (d (list (d (n "flanker-assert-str") (r "^0.5.0") (d #t) (k 2)) (d (n "flanker-temp") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0f0s5m903kp9jh5ihd8vpvzkhrc5wsd9ymq2zy5y408s7dscpplq")))

