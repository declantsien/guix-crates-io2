(define-module (crates-io se qu sequoia-openpgp-mt) #:use-module (crates-io))

(define-public crate-sequoia-openpgp-mt-0.1.0 (c (n "sequoia-openpgp-mt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "buffered-reader") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "sequoia-openpgp") (r "^1") (k 0)) (d (n "sequoia-openpgp") (r "^1") (f (quote ("crypto-cng"))) (t "cfg(windows)") (k 2)) (d (n "sequoia-openpgp") (r "^1") (d #t) (t "cfg(not(windows))") (k 2)))) (h "0ia49v1jza3qx291a8jclfscw91idjxbm89nxdii59x5gc26pa02") (r "1.56")))

