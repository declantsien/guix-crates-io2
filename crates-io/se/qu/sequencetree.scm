(define-module (crates-io se qu sequencetree) #:use-module (crates-io))

(define-public crate-sequencetree-0.1.0 (c (n "sequencetree") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0j09mni4ndipsgyfpy58izn3xm81rczs57ibfyqspgyv07izi9d6")))

(define-public crate-sequencetree-0.1.2 (c (n "sequencetree") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "182bmvlf2jm4cqz1qp3a12kn76305qr8szz0nhyzd2c2pmiqzg2a")))

(define-public crate-sequencetree-0.1.3 (c (n "sequencetree") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1k67wm34fs3zd6y8r3yxjd5yzdfpf10wr3ng1ckk5iz15giz9jb3")))

(define-public crate-sequencetree-0.1.4 (c (n "sequencetree") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "193c7xa6gylksnajpa9q7vldwf18znfixabahfn0ixqhsfvjwxi6")))

