(define-module (crates-io se rb serbuffer) #:use-module (crates-io))

(define-public crate-serbuffer-0.1.0 (c (n "serbuffer") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0nbhk0ygrgq5fz2f8kspkmhn9rb53qz13s84fwwgl9xcgkwv88hh")))

(define-public crate-serbuffer-0.1.1 (c (n "serbuffer") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "19jahq6bppgvj1fxy1kzd5m5gish1mx5s8hrzkwbf93c5wdfcd01")))

(define-public crate-serbuffer-1.0.0 (c (n "serbuffer") (v "1.0.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0bdi4jcv8h03c1bhqyf83mxjb6n2nns0ind6r8mvrn0kd1jzdv3x")))

(define-public crate-serbuffer-1.1.0 (c (n "serbuffer") (v "1.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "11i1j21yacbw62azcc3py559wakffajlxvwyvs26n2f4zh7mvxpq")))

(define-public crate-serbuffer-1.1.1 (c (n "serbuffer") (v "1.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1gfl18v66ph232yd5s33f8qib59y7zf35l8kyxlb4w3k1h7h7k7r")))

(define-public crate-serbuffer-1.2.0 (c (n "serbuffer") (v "1.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1b4svbmiqlkbvsxg9b2n47hhmlkxxhbi4cgl7nc20z02mzvhlzq1")))

(define-public crate-serbuffer-1.2.1 (c (n "serbuffer") (v "1.2.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0zh3gc90ibb6i5xh7147acviy4jadan0dq1br04bfpqh58l3n05q")))

(define-public crate-serbuffer-1.2.2 (c (n "serbuffer") (v "1.2.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "074jp8dgxq632pbdl9dmr17b6w32b5p935kpqwgqhncnyldf3lq4")))

(define-public crate-serbuffer-1.2.3 (c (n "serbuffer") (v "1.2.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0ppvkcm8skapicqnm6ajhgxxbm500cx9500qi54wxayspkgyfln4")))

(define-public crate-serbuffer-1.2.4 (c (n "serbuffer") (v "1.2.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "18zwpl5dcha8rpghd674vxprpp77bxx6vpm91yvjjx9lzq0dsb8b")))

(define-public crate-serbuffer-1.2.5 (c (n "serbuffer") (v "1.2.5") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0kz75sf4s54an6jxmjr3q4i2c7sznzfknyzr5jlcq5l0i44kfamx")))

(define-public crate-serbuffer-1.3.0 (c (n "serbuffer") (v "1.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0vjs8j6ywchhyjaq7dx1s28b16hsm49r96h3i5sgmnzydscizrf2")))

(define-public crate-serbuffer-1.3.1 (c (n "serbuffer") (v "1.3.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1z54kj6z3wgm9qn1ld3nmx373xh9arq1hjmrgbqiizd9yc3iapi7")))

(define-public crate-serbuffer-1.3.2 (c (n "serbuffer") (v "1.3.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1v0baa02c070ilbbjiwpkv9wrs4i6fg93zh5v0b3dzgms9psz1ia")))

(define-public crate-serbuffer-1.3.3 (c (n "serbuffer") (v "1.3.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1hksfpg7pgpjb29lcyp6sw3swmrznvyp6pycadr8s0nkbk48b6jj")))

