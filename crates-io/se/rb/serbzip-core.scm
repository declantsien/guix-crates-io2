(define-module (crates-io se rb serbzip-core) #:use-module (crates-io))

(define-public crate-serbzip-core-0.5.0 (c (n "serbzip-core") (v "0.5.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zgh8dchjbzxr2gbfsp0bq1n9zrnabbh8h0g9i1k0wvlwbjwkrnd")))

(define-public crate-serbzip-core-0.6.0 (c (n "serbzip-core") (v "0.6.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i35drgafx5n3lgrvikzdjjf0wh4jh0y1q5zlj0c190wwdvjj0bk")))

(define-public crate-serbzip-core-0.7.0 (c (n "serbzip-core") (v "0.7.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12b27m4adjkjlikdh4lkk2qv289nqajwvd9w3myvfvhma99gypjc")))

(define-public crate-serbzip-core-0.8.0 (c (n "serbzip-core") (v "0.8.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0llx3z5wd94cwky0biiibmxznhfxnzxx5zmg7mwvg0b1dxrs6jkp")))

(define-public crate-serbzip-core-0.9.0 (c (n "serbzip-core") (v "0.9.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "039flh0wv38141l691wb2gq3z53v74dkazlaipn8z5g32wb1qnjl")))

(define-public crate-serbzip-core-0.10.0 (c (n "serbzip-core") (v "0.10.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fw1jqpffr452gznk450ly0vdgqmdywxmqyx8dkz102704636imj")))

