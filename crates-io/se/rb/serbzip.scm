(define-module (crates-io se rb serbzip) #:use-module (crates-io))

(define-public crate-serbzip-0.1.0 (c (n "serbzip") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "1n9gwb6j9qihla72ivi650908k86n0yrfjfq7icrv3fqlxwalvck") (f (quote (("build-binary" "clap" "home" "reqwest"))))))

(define-public crate-serbzip-0.2.0 (c (n "serbzip") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1ayg2gs7yi6lvy9y7vyc51ahpr3bfc46rz9n1564dmpyvscmgzig")))

(define-public crate-serbzip-0.3.0 (c (n "serbzip") (v "0.3.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1nlwsmsl5gpy2xnyd31lqzb9n8x93v79xi8zyjij46dr8x6alklz")))

(define-public crate-serbzip-0.4.0 (c (n "serbzip") (v "0.4.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1fa5fy8a5mvxkxajcrlmgcs0014gb5l685cg0295mswn3fch184s")))

(define-public crate-serbzip-0.5.0 (c (n "serbzip") (v "0.5.0") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1rf4pax7bfi5d5qy916fba72i9ip7gmpxrn3r2m9dn0xnwl649kv")))

(define-public crate-serbzip-0.6.0 (c (n "serbzip") (v "0.6.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serbzip-core") (r "^0.6.0") (d #t) (k 0)))) (h "098nm3wfri3xc7cddiyjjxdmja4dg3qg3shlz48k647ml7p3sgpc")))

(define-public crate-serbzip-0.7.0 (c (n "serbzip") (v "0.7.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serbzip-core") (r "^0.7.0") (d #t) (k 0)))) (h "0940hkh8q3kfxy7zybgqkpwjdwh2z2h87hkx6in3waplkkklrw7g")))

(define-public crate-serbzip-0.8.0 (c (n "serbzip") (v "0.8.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serbzip-core") (r "^0.8.0") (d #t) (k 0)))) (h "1wdgw7h090jnpyqxxm3rps4nx9fw7xp8ims9jkgkh8laqcnipcb6")))

(define-public crate-serbzip-0.9.0 (c (n "serbzip") (v "0.9.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serbzip-core") (r "^0.9.0") (d #t) (k 0)))) (h "1kwryyfrcp0cslnyrwdjdrs9p7cf0jl0wv1gif8h42vn6zn0v6z1")))

(define-public crate-serbzip-0.10.0 (c (n "serbzip") (v "0.10.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serbzip-core") (r "^0.10.0") (d #t) (k 0)))) (h "1c7awqi7krrq8whk48fvk135jcpgyb4rin38q796qjhi59qncw2g")))

