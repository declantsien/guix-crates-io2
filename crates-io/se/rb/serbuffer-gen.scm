(define-module (crates-io se rb serbuffer-gen) #:use-module (crates-io))

(define-public crate-serbuffer-gen-0.1.0 (c (n "serbuffer-gen") (v "0.1.0") (h "16qxnc69lz1gi66xrv948ygah7a3vkl74x2jqgzgpalsj68nmhvl")))

(define-public crate-serbuffer-gen-0.1.1 (c (n "serbuffer-gen") (v "0.1.1") (h "01507dvcmjr99agraqiasvb29420s5zny2pcgapwcq8n7pypbjzs")))

(define-public crate-serbuffer-gen-0.1.2 (c (n "serbuffer-gen") (v "0.1.2") (h "0df9knz5mwhrz6k6ffjp8z099wfbi4z5vc9529y7hza7dxnxs1c3")))

(define-public crate-serbuffer-gen-1.0.0 (c (n "serbuffer-gen") (v "1.0.0") (h "0pm95vdgm3lk6i5bh40m7jz59cc1qqgyk7dv5vfcl8n09v3vb563")))

(define-public crate-serbuffer-gen-1.0.1 (c (n "serbuffer-gen") (v "1.0.1") (h "0jlj90dmamzk4fd3pkm0w6nhvjnaxflb5wxplc2q4gvca7p01cx7")))

(define-public crate-serbuffer-gen-1.2.0 (c (n "serbuffer-gen") (v "1.2.0") (h "09pikwipxd9q9dwimpn9m86r0w3ms6zq0zamyfmgzw1rw11ndmdl")))

(define-public crate-serbuffer-gen-1.2.1 (c (n "serbuffer-gen") (v "1.2.1") (h "1959ra5i0m5pvkij6fvvy1yfd5xsh8mvaf665gm6278lvnj0zdn4")))

(define-public crate-serbuffer-gen-1.2.2 (c (n "serbuffer-gen") (v "1.2.2") (h "0cfjk7xxiazl06f6r3b3ylvasm1bacvx01fbr5xwx810zilmslp4")))

(define-public crate-serbuffer-gen-1.2.3 (c (n "serbuffer-gen") (v "1.2.3") (h "13hwviwfvfdirgj311jvvpiwf1rk88mmhs5na9zpa6f0jk2w25f8")))

(define-public crate-serbuffer-gen-1.2.4 (c (n "serbuffer-gen") (v "1.2.4") (h "1fvhhb0wc921lrp77z13n5nfhgaasvnh3l8kdp39llhfkqsw5sr6")))

(define-public crate-serbuffer-gen-1.2.5 (c (n "serbuffer-gen") (v "1.2.5") (h "1fc5rg627fjr78wlr9qssi8pd17301vx6sg5jgfmwvdhg80fhi1j")))

(define-public crate-serbuffer-gen-1.2.6 (c (n "serbuffer-gen") (v "1.2.6") (h "11ffqyxfi4pv0h0c5hxq04s87kkn9lycwmj7ah3vnkqj9n9rh3ng")))

(define-public crate-serbuffer-gen-1.3.0 (c (n "serbuffer-gen") (v "1.3.0") (h "1059c9hbjn0lh6wy9fpim75w9fj3qp2vh58n1wf14rqh5cdbnq39")))

(define-public crate-serbuffer-gen-1.3.1 (c (n "serbuffer-gen") (v "1.3.1") (h "10asnfwh3vravdrnqc189wdx2qwfy36nyvqifqarc46m8fy1ajc9")))

