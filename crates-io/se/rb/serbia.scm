(define-module (crates-io se rb serbia) #:use-module (crates-io))

(define-public crate-serbia-0.1.0 (c (n "serbia") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1.0.62") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "07iz72qsw53v57j8f7gb5v7066igvin1jnch6395wcsrhrhqhj7d")))

(define-public crate-serbia-0.1.1 (c (n "serbia") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0xj1baxymcna1jdz04dnmdflarjrw8l6l0xsihbkxv9zhs29av4m")))

(define-public crate-serbia-0.2.0 (c (n "serbia") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1chjnrahb65bb3rxks0cjf29vhf10mayyws8b2bca420gq3z6446") (y #t)))

(define-public crate-serbia-0.3.0 (c (n "serbia") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "19jf12041dv4gx7bkrb63ry2c8bsr08i54ll8ghbrhidbxhc0bqq") (y #t)))

(define-public crate-serbia-0.4.0 (c (n "serbia") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "13vmsa6b6qaj5xsncrp18vxmj39blfsaa13n57in7vwnhsdmr0hq") (y #t)))

(define-public crate-serbia-0.4.1 (c (n "serbia") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1sxfxjxj64ini708imfn9hz9yj4idyyh8c38mijpnminmikfigs2")))

(define-public crate-serbia-0.4.2 (c (n "serbia") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0fgwva5q44g34wn15557i1j86xdhs3nriqkinrysdflz24a60my1")))

(define-public crate-serbia-0.4.3 (c (n "serbia") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "05cd3zzzsqp06b574bsphp4kk09br31jdbk6n9cq1ssjrwv4hfcx")))

