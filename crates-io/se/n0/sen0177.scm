(define-module (crates-io se n0 sen0177) #:use-module (crates-io))

(define-public crate-sen0177-0.1.0 (c (n "sen0177") (v "0.1.0") (d (list (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "serial-core") (r "^0.4") (d #t) (k 0)))) (h "1ra7jiq786njm7r5pj00kc87pssfh4ka70fyls879g6rg43k6s4i")))

(define-public crate-sen0177-0.1.1 (c (n "sen0177") (v "0.1.1") (d (list (d (n "serial") (r "^0.4") (d #t) (k 0)) (d (n "serial-core") (r "^0.4") (d #t) (k 0)))) (h "17bfxcnciy4m1mwda9xz0jlascyvdlspl970a1jjbwmcz87phhix")))

(define-public crate-sen0177-0.2.0 (c (n "sen0177") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 2)))) (h "130y8gr568i4qilzgqrfaqs9h6icr8qyx8h50fwv2x4s03dj41fq") (f (quote (("std") ("default" "std"))))))

(define-public crate-sen0177-0.3.0-alpha.1 (c (n "sen0177") (v "0.3.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "serial") (r "^0.4") (d #t) (k 2)))) (h "0awlr0qjp07y6vl9pc65nszgxz7jv1alf1hqcgj12gxizph6kl9f") (f (quote (("std") ("default" "std"))))))

(define-public crate-sen0177-0.4.0-alpha.1 (c (n "sen0177") (v "0.4.0-alpha.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "serial") (r "^0.4") (d #t) (k 2)))) (h "1anzjawm6j4yx478pnigamjdc2djzwvvbba00mhcxc2azniz5ccm") (f (quote (("std") ("default"))))))

(define-public crate-sen0177-0.5.0 (c (n "sen0177") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "embedded-hal") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "embedded-hal-nb") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 2)))) (h "0sqsbmsa0p9dyar5hj81gv71lbc2srmg3yd999gjdxxfpz0q5yvm") (f (quote (("std") ("default"))))))

(define-public crate-sen0177-0.6.0 (c (n "sen0177") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-nb") (r "^1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 2)))) (h "19g78995k6waffa72622af58pqqqrr66f5l1ngqvk9sj9pdkwdwc") (f (quote (("std") ("default"))))))

