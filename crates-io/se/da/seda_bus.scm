(define-module (crates-io se da seda_bus) #:use-module (crates-io))

(define-public crate-seda_bus-0.0.1 (c (n "seda_bus") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "task_runner") (r "^0.0.1") (d #t) (k 0)))) (h "1msz8b9jcfwkk9cj4wdnabxpdkp9k3fmppbc154nqn1xji6baf63")))

(define-public crate-seda_bus-0.0.2 (c (n "seda_bus") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.17") (d #t) (k 0)) (d (n "scheduled-thread-pool") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (d #t) (k 0)))) (h "0djh8pm42hdhyl9mhll8mr4h64g6zfswgdhvbddm6n65lyvmvmf4")))

(define-public crate-seda_bus-0.0.6 (c (n "seda_bus") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1mbpv3fg9vqz8ih2lwhbzxyvjkbmcyjqsm5835j01m22b38ayvpp")))

(define-public crate-seda_bus-0.0.7 (c (n "seda_bus") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "120kkjsd8sdchswjzvb48z75ypjf7a70js8ld2p60lgilgnjifx0")))

(define-public crate-seda_bus-0.0.8 (c (n "seda_bus") (v "0.0.8") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1qb873bv99ymih1xdni98dx9v9z33s7lh7sxyib0fhv3yix0f5by")))

(define-public crate-seda_bus-0.0.9 (c (n "seda_bus") (v "0.0.9") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.25") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0q9j2zzs68h1kq9ikf0ymsjxj75rpq8l0b5s5i3kjdcmmlmflpjk")))

(define-public crate-seda_bus-0.0.10 (c (n "seda_bus") (v "0.0.10") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.25") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0gyn91d7yhrkjjb2ii6i5hlpcp8wmfwf06jan4lmy8yrs8ssladj")))

(define-public crate-seda_bus-0.0.11 (c (n "seda_bus") (v "0.0.11") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.28") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1jxbskd69kw6as8qb57k0rjbsfkyxxn6xvm8lbhj8cbfj35aqyhg")))

(define-public crate-seda_bus-0.0.12 (c (n "seda_bus") (v "0.0.12") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.28") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1fwrr26sy3bpkkg6sc6ads0xd1vynkxd77dh9f8w99cfng7n6vrc")))

(define-public crate-seda_bus-0.0.13 (c (n "seda_bus") (v "0.0.13") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.28") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0sa8k052wrri1b626xizq5sgd48j9dyh4nkw6p82g6c502can1mz")))

(define-public crate-seda_bus-0.0.15 (c (n "seda_bus") (v "0.0.15") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.32") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1cr9l440swvi6xkkilr1sp6z5yigy5s7v2a0x5hzdmjhwgp9wx3r")))

(define-public crate-seda_bus-0.1.0 (c (n "seda_bus") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1vib85aapkp545lb74cw8i9piydfxill8wyn469sfklzh2azkj1r")))

(define-public crate-seda_bus-0.1.1 (c (n "seda_bus") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1cgdnsygsf0s87pcq182659q0vjyrmjviqx4c2s1zdjil66l4xgb")))

(define-public crate-seda_bus-0.1.2 (c (n "seda_bus") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "05gmn2910cc73dkqpc09cn1rl07ifzh24vzpmlzsfg8grbqkfifj")))

(define-public crate-seda_bus-0.1.3 (c (n "seda_bus") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0rx1cjbzq3m03qihd3vjjqfval3mgslnb8qvkv6dl9br1c0gjia4")))

(define-public crate-seda_bus-0.1.4 (c (n "seda_bus") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0ypisd0h4s7h5ihyv93d8m6sjdj8gbfq2lf7ilncpmh6hf09mc3j")))

(define-public crate-seda_bus-0.1.5 (c (n "seda_bus") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.44") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1xlfwkjwaqfk5i0rr5ivkxmg1l55a56lh87f8k1y44g5101l43z0")))

(define-public crate-seda_bus-0.1.6 (c (n "seda_bus") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.44") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0n0b2csw1y2xzhi00px07a09pwvqjll55z9h8jv107z1k0lgzbam")))

(define-public crate-seda_bus-0.1.7 (c (n "seda_bus") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1j9hy6kagpivpsvyvhj04chyyvlgnqa2d40yd1nwxnscm0cl06vl")))

(define-public crate-seda_bus-0.1.8 (c (n "seda_bus") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0sr1cz00m9ph9r8sybd6n8b6yby086a0nfrg0avw8xvgizfycjav")))

(define-public crate-seda_bus-0.1.9 (c (n "seda_bus") (v "0.1.9") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0plyyldzp091962jjpsrxkaw1hqfp1b08smbzs86s7z492wfyiaq")))

(define-public crate-seda_bus-0.1.10 (c (n "seda_bus") (v "0.1.10") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0s1ry480ak0hk93ghaqqnzkiws1b2b9k0galgphjdwnzps4cxlf1")))

(define-public crate-seda_bus-0.1.11 (c (n "seda_bus") (v "0.1.11") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "19iyvb1p3dpxjdjx16zk17pp54gc0xifhl9kz2ivcpbpxcxpg7d8")))

(define-public crate-seda_bus-0.1.12 (c (n "seda_bus") (v "0.1.12") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1qgng07i7ijjc84bmj55glf3pac9yfsy8hcvrm0q455w91f5d7pf")))

(define-public crate-seda_bus-0.1.13 (c (n "seda_bus") (v "0.1.13") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "02fyrfyvs73hfkzhr34wl92h6w8akr3y3vl219zh3g8kb556p888")))

(define-public crate-seda_bus-0.2.0 (c (n "seda_bus") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0w9i2c967l0rais0bsz7x6ikb88880pdm7xw0va1ayl7c3a05z23")))

