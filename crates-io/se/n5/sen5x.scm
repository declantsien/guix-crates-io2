(define-module (crates-io se n5 sen5x) #:use-module (crates-io))

(define-public crate-sen5x-0.0.1 (c (n "sen5x") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.2") (d #t) (k 0)))) (h "1pjarrm66l68j6smh99dw9y8rf6jllvzy3mvv18whhrszx4rxhb8")))

