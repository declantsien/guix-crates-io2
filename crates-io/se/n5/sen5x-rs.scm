(define-module (crates-io se n5 sen5x-rs) #:use-module (crates-io))

(define-public crate-sen5x-rs-0.0.2 (c (n "sen5x-rs") (v "0.0.2") (d (list (d (n "embedded-hal") (r "=1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "=0.10.0") (f (quote ("eh1"))) (d #t) (k 2)))) (h "19ks9ph25n9y46cspgvvkpmb99ibmp82wwa7r1bxh99lfxr7yl9g")))

(define-public crate-sen5x-rs-0.1.0 (c (n "sen5x-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "=0.10.0") (f (quote ("eh1"))) (d #t) (k 2)))) (h "1j3qrzxqsl4246a4slnndc3awf910krrk3jn5xh5ikg2ij2lrrwf")))

(define-public crate-sen5x-rs-0.2.0 (c (n "sen5x-rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "=1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "=0.10.0") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.3.0") (d #t) (k 0)))) (h "0ryp0l82yxzdnkr7y9kdnzir5r8na52g5jhqf70821v7s0g1z2rx")))

