(define-module (crates-io se ct section_testing) #:use-module (crates-io))

(define-public crate-section_testing-0.0.1 (c (n "section_testing") (v "0.0.1") (h "0awf9j4dar3450x36za6ghzf9s06908jq42kqyh7h6az2yj90549")))

(define-public crate-section_testing-0.0.2 (c (n "section_testing") (v "0.0.2") (h "11zy6jnp3b18wg5k17zwzkkyknc3nkwiaphabfqzll8yqal6cxs7")))

(define-public crate-section_testing-0.0.3 (c (n "section_testing") (v "0.0.3") (h "1n26mcbqn7pzzb5s5ikkdq3j7p3abn4b335bg4i4ghz9dwc4zxga")))

(define-public crate-section_testing-0.0.4 (c (n "section_testing") (v "0.0.4") (h "0a1zwpcs2dqhky2wd8y82cm25l3s9i5dbyn4ypgmvdysizcxgr7c")))

(define-public crate-section_testing-0.0.5 (c (n "section_testing") (v "0.0.5") (h "18dfl8nacwcd1z2y4sgdx7751czzpl6282q6cd49ys5gd0xlkljz")))

