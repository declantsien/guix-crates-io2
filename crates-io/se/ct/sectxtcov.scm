(define-module (crates-io se ct sectxtcov) #:use-module (crates-io))

(define-public crate-sectxtcov-0.0.1 (c (n "sectxtcov") (v "0.0.1") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.5") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.6") (d #t) (k 0)) (d (n "tokio") (r ">=0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "11vmpjd3w9rwcfgznhfzfgm07wqf71ny5m0isxyicicjib0qzyij") (y #t)))

(define-public crate-sectxtcov-0.0.2 (c (n "sectxtcov") (v "0.0.2") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.5") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.6") (d #t) (k 0)) (d (n "tokio") (r ">=0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0502hqlp2x80kfwa6hha41fykqyx2y2g5a8c8dklarqly918jxi3") (y #t)))

(define-public crate-sectxtcov-0.0.3 (c (n "sectxtcov") (v "0.0.3") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.5") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.6") (d #t) (k 0)) (d (n "tokio") (r ">=0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1yplsmhnsc33bmyyd9nn9hkd1i1wi67y520ys4wyi818pwd1pw0k") (y #t)))

(define-public crate-sectxtcov-0.0.4 (c (n "sectxtcov") (v "0.0.4") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.5") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.6") (d #t) (k 0)) (d (n "tokio") (r ">=0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1v73j75cx5yfdkcxdra2s1bkxfsv5ig400k6bmcnlk26qla1b3yx") (y #t)))

