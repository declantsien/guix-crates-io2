(define-module (crates-io se ct sectxtlib) #:use-module (crates-io))

(define-public crate-sectxtlib-0.0.1 (c (n "sectxtlib") (v "0.0.1") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iref") (r ">=1.1") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "1ksxby5vbcvydq01jbib3gkmy9ngcfpa127dg0bxaa1yy3xhdjij")))

(define-public crate-sectxtlib-0.1.0 (c (n "sectxtlib") (v "0.1.0") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iref") (r ">=1.1") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "0dw6bzqxij465flgyzyhqpblr7w13n53pjvhi4731zl8z3nal40p")))

(define-public crate-sectxtlib-0.1.1 (c (n "sectxtlib") (v "0.1.1") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iref") (r ">=1.1") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "0vzz70jglmk6w1hnmy0jrbzl8j919086zv2yk1sx4wnrh9n6d09r")))

(define-public crate-sectxtlib-0.1.2 (c (n "sectxtlib") (v "0.1.2") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iref") (r ">=1.1") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "0sqac5fjdzqzp7ibj18zvw2w6dsm34q9725h2dji99dacysa6xsp")))

(define-public crate-sectxtlib-0.1.3 (c (n "sectxtlib") (v "0.1.3") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "0chh5j83k0ihb3d6in8bmz8ibmn4npz3g4xcw893iy797x7bi647")))

(define-public crate-sectxtlib-0.2.0 (c (n "sectxtlib") (v "0.2.0") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)))) (h "04a50g370v61pyyjhci0b2q5xnnmh6pwp0h299q819yvyq4djixz")))

(define-public crate-sectxtlib-0.2.1 (c (n "sectxtlib") (v "0.2.1") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07nrc91lc0klf313ck61p6qh0mh01jc9adb8wax297l0ii804x9z")))

(define-public crate-sectxtlib-0.2.2 (c (n "sectxtlib") (v "0.2.2") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ia7xisy5flcv7xwhky5m33ar068g85a3fsixrk86yzq4b1rn8bn")))

(define-public crate-sectxtlib-0.2.3 (c (n "sectxtlib") (v "0.2.3") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ibs1xfmqc9jp9k3zhb90zfa2gppwykaj5bnhjqi912klgn9gcip")))

(define-public crate-sectxtlib-0.2.4 (c (n "sectxtlib") (v "0.2.4") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ckh4d0zzvif49pc045zvrly2mva8yqpqfiwjky588qxng2pjn8p")))

(define-public crate-sectxtlib-0.2.5 (c (n "sectxtlib") (v "0.2.5") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pd8a4qha9a27il3275mglj6rnja3a639g7aw0xw5s953j81pkr5")))

(define-public crate-sectxtlib-0.2.6 (c (n "sectxtlib") (v "0.2.6") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sfhbp2jkr13k057d36kxl43fs3bw5zzih3k4bhfnwdi2vwj506v")))

(define-public crate-sectxtlib-0.3.0 (c (n "sectxtlib") (v "0.3.0") (d (list (d (n "chrono") (r ">=0.4.10") (d #t) (k 0)) (d (n "iri-string") (r ">=0.7.0") (d #t) (k 0)) (d (n "nom") (r ">=5.1.2") (d #t) (k 0)) (d (n "oxilangtag") (r ">=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "valuable") (r ">=0.1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vblz9gqc886yy557sic2fnfsfcrm82iawy947c9ay881l0ck7p6")))

