(define-module (crates-io se ct sectok) #:use-module (crates-io))

(define-public crate-sectok-0.1.0 (c (n "sectok") (v "0.1.0") (d (list (d (n "percent-encoding") (r "2.*") (d #t) (k 0)))) (h "0pqkc3jgfap76c0p8gf0hm6zl07pw0b67p9rywiqp36yzmnjgqd3")))

(define-public crate-sectok-0.2.0 (c (n "sectok") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "percent-encoding") (r "2.*") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "027sd0glw6bs3bn22y2b0dabhjj4ja216ggzmncm04kd0662hj3s")))

