(define-module (crates-io se ct section_parser_derive) #:use-module (crates-io))

(define-public crate-section_parser_derive-0.1.1 (c (n "section_parser_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "066m697g5ixg8rym67fl9wh6807fjippsq2fqmanf8xfy9z8fmln")))

