(define-module (crates-io se an sean_test_eth2_ssz) #:use-module (crates-io))

(define-public crate-sean_test_eth2_ssz-0.1.2 (c (n "sean_test_eth2_ssz") (v "0.1.2") (d (list (d (n "eth2_ssz_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "ethereum-types") (r "^0.9.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1k0g1slk0fblw94j848jdq3qc8m9sm9l2k4d27z19g5x7gl0k3c2") (f (quote (("arbitrary" "ethereum-types/arbitrary")))) (y #t)))

