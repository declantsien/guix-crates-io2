(define-module (crates-io se an sean_test_serde_utils) #:use-module (crates-io))

(define-public crate-sean_test_serde_utils-0.1.0 (c (n "sean_test_serde_utils") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 2)))) (h "06rdwnpv62vrvvgsgdya6494y1rpblq18i74x03zd8qw02fi68jh") (y #t)))

