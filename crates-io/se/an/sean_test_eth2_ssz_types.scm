(define-module (crates-io se an sean_test_eth2_ssz_types) #:use-module (crates-io))

(define-public crate-sean_test_eth2_ssz_types-0.2.0 (c (n "sean_test_eth2_ssz_types") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sean_test_eth2_ssz") (r "^0.1.2") (d #t) (k 0)) (d (n "sean_test_serde_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "sean_test_tree_hash") (r "^0.1.1") (d #t) (k 0)) (d (n "sean_test_tree_hash_derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 2)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1mkyzm2yfb9gnmscd3ixbq2yllwg48yfva1x4jz240pxxlxv1n8y") (y #t)))

