(define-module (crates-io se an sean_test_eth2_hashing) #:use-module (crates-io))

(define-public crate-sean_test_eth2_hashing-0.1.1 (c (n "sean_test_eth2_hashing") (v "0.1.1") (d (list (d (n "cpufeatures") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.19") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0lszrvk5mnnfc3pzgl8rj4rja39jmdlvps3247gscs59vmnxdcjq") (f (quote (("zero_hash_cache" "lazy_static") ("default" "zero_hash_cache")))) (y #t)))

