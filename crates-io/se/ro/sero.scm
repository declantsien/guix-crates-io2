(define-module (crates-io se ro sero) #:use-module (crates-io))

(define-public crate-sero-0.1.0 (c (n "sero") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4") (d #t) (k 0)))) (h "0d1j4dmg9s0z90mg4klsmf0wjxfpiym899rlwcg2p2775ivaaqs8")))

(define-public crate-sero-0.1.1 (c (n "sero") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.0") (d #t) (k 0)))) (h "04x24ykzkgkn9bzcmil0amwhkjhj96v1704ihdcimvcddzi2rrmf")))

(define-public crate-sero-0.1.2 (c (n "sero") (v "0.1.2") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "07x9krvz0wlvv7bzz3zpg217zq04jp3qzg4xvfcybipk547dw5pv")))

(define-public crate-sero-0.1.3 (c (n "sero") (v "0.1.3") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "05im09rrqxdrrmjajc5jwvaxzzjjkxl9qp218mc82qywc12vf2l1")))

(define-public crate-sero-0.1.4 (c (n "sero") (v "0.1.4") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "1x5msx61xv6cpk97ha7mp90ckx4bnwbq9gh6s8p6x7wj78wmdrp8")))

(define-public crate-sero-0.1.5 (c (n "sero") (v "0.1.5") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "02y5nfwsdcgbhqjl4aming4hwzwmdj6snbqhqza8pp0ig878zngv")))

(define-public crate-sero-0.1.6 (c (n "sero") (v "0.1.6") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "101093cr9kkmm5m0ng8qk2wnnn4jycqv4vnp5zi3bpczdqp9xgld")))

(define-public crate-sero-0.1.7 (c (n "sero") (v "0.1.7") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "0ysdgbl2y138qipi5m2ih6p3fgf41cqmack6ckbv9n1js1ksia40")))

(define-public crate-sero-0.1.8 (c (n "sero") (v "0.1.8") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "03qmf2nz85p5wq7414fniffwnhcf157mhsc0isfwscp2p30r9ay6")))

(define-public crate-sero-0.1.9 (c (n "sero") (v "0.1.9") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "06zwzfhvfpvy50gzr7i5mqr66ma09qv76m0pnkgwjr11gallvdbi")))

(define-public crate-sero-0.1.10 (c (n "sero") (v "0.1.10") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "0v5p8bk73h8ar86blbfsbi0f9shp66k0nkh868zh7z4gbvdif663")))

(define-public crate-sero-0.1.11 (c (n "sero") (v "0.1.11") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "0fk5arsqwfjj3x8vxihrzplgwfhi31mnsnr481rrslr09k718k42")))

(define-public crate-sero-0.1.12 (c (n "sero") (v "0.1.12") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "0dj42ypjhhgialq20my79p3lxcm68n28gz1489zhdg2dmz8327jb")))

(define-public crate-sero-0.1.13 (c (n "sero") (v "0.1.13") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "1v3z51d39xsmjic432vanrb7n4zallxrjhggzf52yqi2h45nsqal")))

(define-public crate-sero-0.1.14 (c (n "sero") (v "0.1.14") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "0ck8jhqvxv5n63nf3ihc1dmprppxnb00apmfcjmkwig6ps5602pz")))

(define-public crate-sero-0.1.15 (c (n "sero") (v "0.1.15") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "17d2iqi6v8rw2ypxl11ff61h2g1yha705rcrk5fqc2xjjdl39cbs")))

(define-public crate-sero-0.1.16 (c (n "sero") (v "0.1.16") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "1pv5bmrzh8x1da8x2b68vjybsvlm44y2kvrl77rmcv2z340307cp")))

(define-public crate-sero-0.1.17 (c (n "sero") (v "0.1.17") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)))) (h "11inzqwvwvdlqva2gb2r7xaqrlldw0ljby7r57xg1mrv5mn4mfdn")))

