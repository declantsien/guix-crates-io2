(define-module (crates-io se np senpy) #:use-module (crates-io))

(define-public crate-senpy-0.1.0 (c (n "senpy") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "0pxrs27iprz3hzws1aj9a81p259v4alg3xr312bka8v9zhdwjc76")))

(define-public crate-senpy-0.1.1 (c (n "senpy") (v "0.1.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1jrxvi238pg9b3bkds8xdff1hmgsl6lsvx5bggik5jjghdx466cp")))

(define-public crate-senpy-0.1.2 (c (n "senpy") (v "0.1.2") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1q0kwz3bk6j1xgf04pg5xnhn656khkli1pjgbdcx75mg2lwp7np7")))

