(define-module (crates-io se np senpy-ffi) #:use-module (crates-io))

(define-public crate-senpy-ffi-0.1.0 (c (n "senpy-ffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "senpy") (r "^0.1.2") (d #t) (k 0)))) (h "1v4r3h4ym81nr51f1s9xfn3c6grwr9lshjpd5nzvxb3j5hh4cqay")))

(define-public crate-senpy-ffi-0.1.1 (c (n "senpy-ffi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "senpy") (r "^0.1.2") (d #t) (k 0)))) (h "0dknbcljryann53j8d5k879bqb2qllmw6h16bm9r955zsi19rffa")))

