(define-module (crates-io se np senpy-cli) #:use-module (crates-io))

(define-public crate-senpy-cli-0.1.0 (c (n "senpy-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 1)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "senpy") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "vergen") (r "^7.0.0") (d #t) (k 1)))) (h "0r7pbimdn8hb4xfvvcsy0k9x7k8ybmqcrjvvpdbhzzwv26g0fjmz") (y #t)))

(define-public crate-senpy-cli-0.1.1 (c (n "senpy-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "senpy") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_dhall") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "1bnkyvqpsrynhwkrg3iq4jzhl5h6my19bdb2z2m8al1xyqq60w5m")))

