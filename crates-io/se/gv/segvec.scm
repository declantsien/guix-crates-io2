(define-module (crates-io se gv segvec) #:use-module (crates-io))

(define-public crate-segvec-0.1.0 (c (n "segvec") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "03mm2fsx0vzba32v09krf8gi5a6qbcyg6qd6qbcb4qgkyw7sqln6")))

(define-public crate-segvec-0.1.1 (c (n "segvec") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1hcljmb5bajyf5az7hrs5j5swv8pw85a05b9zn4jjwrqf67gf83g")))

(define-public crate-segvec-0.1.2 (c (n "segvec") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1q2fsydcvrvdc6041hmir1kyypanflh0fzg20sslrvqisriavly9") (f (quote (("thin-segments" "thin-vec") ("small-vec" "smallvec"))))))

(define-public crate-segvec-0.1.3 (c (n "segvec") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1jy5b9d8z0r59smi6wk0jp3c62jlyd4y7f38qyn548k7zdqyqvvy") (f (quote (("thin-segments" "thin-vec") ("small-vec" "smallvec"))))))

(define-public crate-segvec-0.1.4 (c (n "segvec") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0gqq5hh6577inidaww7fvdvh5h9911nw4l7d9rjcq5zlipdc26a3") (f (quote (("thin-segments" "thin-vec") ("small-vec" "smallvec"))))))

(define-public crate-segvec-0.1.5 (c (n "segvec") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("const_generics"))) (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "07ihpp7mwf5fyn865wnzqi3yzgask5pcvwhnk2p7xl4p9g9yak6r") (f (quote (("thin-segments" "thin-vec") ("small-vec" "smallvec"))))))

(define-public crate-segvec-0.2.0 (c (n "segvec") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (f (quote ("const_generics" "union"))) (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0xzq2qc8pn2k47723sx0j140njp9rv1ak6h1s0p4h48dpkcv6fkw") (f (quote (("thin-segments" "thin-vec") ("small-vec" "smallvec"))))))

