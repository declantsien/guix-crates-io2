(define-module (crates-io se iz seize) #:use-module (crates-io))

(define-public crate-seize-0.0.0 (c (n "seize") (v "0.0.0") (d (list (d (n "crossbeam-epoch") (r "^0.9.5") (d #t) (k 0)) (d (n "lock_api") (r "^0.4.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "17lyjaqa33hwva8yyr90maqjan2hba2jk3h27yjr1q62jm40qqri")))

(define-public crate-seize-0.1.0 (c (n "seize") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "0inz632974hpnyhf19xvlrcb6r014z9jh09m78jbksi6amj1cdn9") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.0 (c (n "seize") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "1v4rrbhsc362pk93agf681hppd3v5j0nd8rmfx8bvxwbpwyfz66z") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.1 (c (n "seize") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "0wb9sblk1gchh25vdv6r2mj8vq9szls7w703w0k91qv3agaci4yf") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.2 (c (n "seize") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "1vlcfkyyiiqcar6dacv9g3ajq3h461xf70kz1a1ymbfpzh1r7p0j") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.3 (c (n "seize") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "0wjphg0gxfjwxmwr7jzfyps9hd1mn9k4prlr5pignh13ym3wdz4a") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.4 (c (n "seize") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (o #t) (d #t) (k 0)))) (h "04xs1mv29frmg7aqsijqvbnr0c8a2yvs3s0xxd2kmbnrhph3vv2y") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.2.5 (c (n "seize") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "0nmqqm3n2g9dqnbsgxl6qm5z5jhpryciaf6sb459n4ivcpg3jmqf") (f (quote (("log" "tracing") ("default"))))))

(define-public crate-seize-0.3.0 (c (n "seize") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "05j2q4x7yy3yai6ks0jw6v76bi1a7sp9j2x9ycsh8pbv607jpapj")))

(define-public crate-seize-0.3.1 (c (n "seize") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "08qgg48zscmw5xdn8lqcrc6dgc33ah1ymfb72cjmjs9v2qba7lyy")))

(define-public crate-seize-0.3.2 (c (n "seize") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "0w8qcd9f0jbidijdsfxad240b4fqwsp2bab5jnvh4pir088pbcah")))

(define-public crate-seize-0.3.3 (c (n "seize") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "1lrl38vzfbqd1k8lqp34jrh3kyzpyjiwd0mlr6y4x413cp8294k8")))

(define-public crate-seize-0.4.0 (c (n "seize") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "1r8lbi3wrg4qqij3j6mq7p3xvp9sqgap36939r64q7w98cnw3zxv")))

(define-public crate-seize-0.4.1 (c (n "seize") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)))) (h "16ddkfr3mi6w3c7c49x2qwkahafvqbjxjxa4ys0wnraf8lrln5d0")))

