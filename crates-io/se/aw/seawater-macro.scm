(define-module (crates-io se aw seawater-macro) #:use-module (crates-io))

(define-public crate-seawater-macro-0.1.0 (c (n "seawater-macro") (v "0.1.0") (d (list (d (n "iron-ingot") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full"))) (d #t) (k 0)))) (h "1j2xyy6xa77g44dg1i5clkr93bn0rdvijl1s7kcq36m813mwkkds")))

(define-public crate-seawater-macro-0.2.0 (c (n "seawater-macro") (v "0.2.0") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1dqlvq1xgr8qhy1yzwyiknzksmq3jjg170lhx69qrmadp7j5grlj")))

(define-public crate-seawater-macro-0.2.1 (c (n "seawater-macro") (v "0.2.1") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "17hi293y19h7crrjc7xmrfyl5ck34wa9ahqxykm0rkh9czi32g5m")))

(define-public crate-seawater-macro-0.2.2 (c (n "seawater-macro") (v "0.2.2") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0jpls1lwkwlzf5jw044fflv5cvp62zn4rdhxn5cvadk68hvlralk")))

