(define-module (crates-io se aw seaweedfs) #:use-module (crates-io))

(define-public crate-seaweedfs-0.1.0 (c (n "seaweedfs") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("alloc"))) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "prost") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1.0-alpha.5") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0-alpha.5") (d #t) (k 1)) (d (n "tower") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "00cv3bknahs5dvflga01516m9xnm1gfsnd0s03lsf7six5p04gfk")))

