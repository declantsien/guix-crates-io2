(define-module (crates-io se aw seaward) #:use-module (crates-io))

(define-public crate-seaward-1.0.0 (c (n "seaward") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0k6cy5fcy1hxfgl3z0mksax6ysbw0c6v949pxq6c2fv01bfz5218")))

(define-public crate-seaward-1.0.1 (c (n "seaward") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0cxi284wjc7zrwl7jx4kwwwkb56fc4y52n0gzh3xgixnhrkbkdc8")))

(define-public crate-seaward-1.0.2 (c (n "seaward") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ciyrwnb31s1fkl8lidwa49ymgmjh7s0ix7hpx2r0cx3h7sg0044")))

(define-public crate-seaward-1.0.3 (c (n "seaward") (v "1.0.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt" "signal" "rt-multi-thread"))) (d #t) (k 0)))) (h "1l0ridiykxg5n4viwbs4plr2a6mlrrxwrncfzr3xa9cv12wi9wqi")))

