(define-module (crates-io se aw seawater) #:use-module (crates-io))

(define-public crate-seawater-0.1.0 (c (n "seawater") (v "0.1.0") (d (list (d (n "iron-ingot") (r "^0.2.0") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0pd1wka2dap15wp828mqxl0h5q571dwmmvmiiffnblyi9d521lck")))

(define-public crate-seawater-0.1.1 (c (n "seawater") (v "0.1.1") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.1.0") (d #t) (k 0)))) (h "183bllzzs8hzzsdvbaljv9gd97jxy4xb52dwnppzjdnr2n8gi721")))

(define-public crate-seawater-0.1.2 (c (n "seawater") (v "0.1.2") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.1.0") (d #t) (k 0)))) (h "10iawnzw6ljvag8a6igk93g9y0ji2z42clg8xdc5pivkhgd5292z")))

(define-public crate-seawater-0.2.0 (c (n "seawater") (v "0.2.0") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0k68x5f70556lwpxs3c7z1qmhavnz74fj6a1icvxz6jagbvyz1p5")))

(define-public crate-seawater-0.2.1 (c (n "seawater") (v "0.2.1") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.2.1") (d #t) (k 0)))) (h "09c8kzsrcl9in7rr86pida8fv2zbjnafwip61viv6zlg2yhf642n")))

(define-public crate-seawater-0.2.2 (c (n "seawater") (v "0.2.2") (d (list (d (n "iron-ingot") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "seawater-macro") (r "^0.2.2") (d #t) (k 0)))) (h "1darc7gs4brm433i6jlam7c9wdjqi1kg1r20b7jvam67k5ykz3lf")))

