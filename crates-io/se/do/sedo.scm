(define-module (crates-io se do sedo) #:use-module (crates-io))

(define-public crate-sedo-0.1.0 (c (n "sedo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0z8acrfl0bl811h7wqnhvw4y94kx497xx3w1azfbkwhgb866snbz")))

(define-public crate-sedo-0.2.0 (c (n "sedo") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0r3dgzsas8jaxpvyzsi7785nkhpxa97smqvibxnj6d7jk50zbcdd")))

(define-public crate-sedo-0.2.1 (c (n "sedo") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0y7pfww7i6piirnnq2izgvhxw4qgd02l1hgyyfrmzjwkxay8xdv9")))

(define-public crate-sedo-0.2.2 (c (n "sedo") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1ahl6k2kp9i7iz0kbq1rwxvypzf3wnasbpwccf9in5qnh8ik4vwf")))

(define-public crate-sedo-0.3.0 (c (n "sedo") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1gsgfqlcdnrgnzgrzwvqm95wwi7dfnc6x1psm4rs0a71dczznysj")))

