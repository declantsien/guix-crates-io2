(define-module (crates-io se v_ sev_attest_tool) #:use-module (crates-io))

(define-public crate-sev_attest_tool-0.1.0 (c (n "sev_attest_tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "sev") (r "^1.2.1") (f (quote ("openssl"))) (d #t) (k 0)))) (h "02hzfiplss99cw6skqz8jwmavxw3h5v6affnad2kkybxk99zc4zh") (r "1.70.0")))

