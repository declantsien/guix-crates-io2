(define-module (crates-io se t- set-trie) #:use-module (crates-io))

(define-public crate-set-trie-0.1.0 (c (n "set-trie") (v "0.1.0") (h "1mlpalr9b8ah480a9y97hn4m9am8bibmpw4ph220n7wr9l0ipb32")))

(define-public crate-set-trie-0.2.0 (c (n "set-trie") (v "0.2.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0mnlxf3k2hcczndd5a9acml0zmqacw5fafgkfwpdi61k2jfpcrx7")))

(define-public crate-set-trie-0.2.1 (c (n "set-trie") (v "0.2.1") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "08ir8ddvgpycq58nxfa1pnwl2f1l4vdw8sifgybb6n3ipni6jdrb")))

(define-public crate-set-trie-0.2.2 (c (n "set-trie") (v "0.2.2") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1xsff8h2jrw7bv3zdyvd85svrv5iw16cxbsp0xg9q6qk4anbgbb3")))

(define-public crate-set-trie-0.2.3 (c (n "set-trie") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "01smzrq8bgbf23nys31jcq5rhc5gy111jrnhnlcciaijrayz6y8z")))

