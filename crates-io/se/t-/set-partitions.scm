(define-module (crates-io se t- set-partitions) #:use-module (crates-io))

(define-public crate-set-partitions-1.0.0 (c (n "set-partitions") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1gvvqrs4hay587san6haq6clmf8av4s0fzil6p6y2q0iixm7xlsp") (y #t)))

(define-public crate-set-partitions-1.0.1 (c (n "set-partitions") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "15lha3xmv1b0nakdpvhf5mcsbl8y88q19brx056qsf587hhllib1")))

