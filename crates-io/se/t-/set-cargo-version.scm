(define-module (crates-io se t- set-cargo-version) #:use-module (crates-io))

(define-public crate-set-cargo-version-1.0.0 (c (n "set-cargo-version") (v "1.0.0") (d (list (d (n "toml_edit") (r "^0.2") (d #t) (k 0)))) (h "15j05r7fn44lqmjv1r71kn92zfn31hlzvd6mpal6lsww3dm7wc8x")))

(define-public crate-set-cargo-version-1.2.0 (c (n "set-cargo-version") (v "1.2.0") (d (list (d (n "toml_edit") (r "^0.13") (d #t) (k 0)))) (h "1i9l0jw4cyf0qfb16zrij1qxlw9y10v8vv78izzhl3ipx6j1880i")))

(define-public crate-set-cargo-version-1.2.1 (c (n "set-cargo-version") (v "1.2.1") (d (list (d (n "toml_edit") (r "^0.14") (d #t) (k 0)))) (h "1bl9d2qxmdsxmwc4asmq6rs62nprjqfi42k6ygbcx28i0nc9c6i3")))

