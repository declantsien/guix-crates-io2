(define-module (crates-io se t- set-encoding) #:use-module (crates-io))

(define-public crate-set-encoding-0.1.0 (c (n "set-encoding") (v "0.1.0") (d (list (d (n "bitrw") (r "^0.7.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)) (d (n "tbe") (r "^0.2.0") (d #t) (k 0)))) (h "05vdkdrczjj9iqhq3igp9gxg8hbjzrc275gi129gg1a4fl0vbjca")))

(define-public crate-set-encoding-0.2.0 (c (n "set-encoding") (v "0.2.0") (d (list (d (n "bitrw") (r "^0.8.3") (d #t) (k 0)) (d (n "int") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "tbe") (r "^0.3.3") (d #t) (k 0)))) (h "00f5kb22cx41c06xwb1mi6jsaa7sn6ligjngp24bds252swdplc1")))

