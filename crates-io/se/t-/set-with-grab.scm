(define-module (crates-io se t- set-with-grab) #:use-module (crates-io))

(define-public crate-set-with-grab-1.0.0 (c (n "set-with-grab") (v "1.0.0") (d (list (d (n "set-with-grab-macro") (r "^1.0") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "00jr13jpnkdxgsj20s05dsc2klxfi2aqy0z65dgqf8r20w49b1jy")))

