(define-module (crates-io se t- set-user-tasks) #:use-module (crates-io))

(define-public crate-set-user-tasks-0.1.0 (c (n "set-user-tasks") (v "0.1.0") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (d #t) (k 0)))) (h "1v67d3x1aiiiqzbpml5am4a02pkawj0592bf1h689asvbjii29bk")))

(define-public crate-set-user-tasks-0.1.1 (c (n "set-user-tasks") (v "0.1.1") (d (list (d (n "windows") (r ">=0.36.1") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (d #t) (k 0)))) (h "0lxhhrmsryzbwd8gni916y86s9wbjl5wncbn8wjpcnl790lwj3vi")))

(define-public crate-set-user-tasks-0.36.1 (c (n "set-user-tasks") (v "0.36.1") (d (list (d (n "windows") (r "^0.36.1") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (d #t) (k 0)))) (h "0ld8xmwsnnangylmni9kml3krghs2ywzbv8arxr7r3r5gna6mjv3")))

(define-public crate-set-user-tasks-0.34.0 (c (n "set-user-tasks") (v "0.34.0") (d (list (d (n "windows") (r "^0.34.0") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (d #t) (k 0)))) (h "0d9lv84spg862nkijy4bjhkqap40wv9bkhrk6s6mii646lmfkak3")))

(define-public crate-set-user-tasks-0.48.0 (c (n "set-user-tasks") (v "0.48.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (d #t) (k 0)))) (h "1zajw7aais9vhh7zwzp3llb91qbx45y2vjnh6ssw9z1wiyckxzvb")))

