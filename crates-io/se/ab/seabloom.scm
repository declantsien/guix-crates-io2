(define-module (crates-io se ab seabloom) #:use-module (crates-io))

(define-public crate-seabloom-0.1.0 (c (n "seabloom") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "seahash") (r "^3.0.1") (d #t) (k 0)))) (h "1h8wza6myi5i6448vylkcygxgpcqsv6h4i1211bhshzzwwgqfxcp")))

