(define-module (crates-io se ee seeed-studio-relay-board) #:use-module (crates-io))

(define-public crate-seeed-studio-relay-board-0.1.0 (c (n "seeed-studio-relay-board") (v "0.1.0") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "02g568s7a3s5rnkxcgq9vpjxhpbabxsjnbb9m1zmx8dqqfwwaa4n")))

(define-public crate-seeed-studio-relay-board-0.1.1 (c (n "seeed-studio-relay-board") (v "0.1.1") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "0cwniv127dnhk6xs9b3r1q2af0ivscgj9kzdjbp0vykrdxr5md14")))

(define-public crate-seeed-studio-relay-board-0.1.2 (c (n "seeed-studio-relay-board") (v "0.1.2") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "0ldrvb8h8gcnqxhfxrz7bv86l2n51h5iq40nh6fxzvalv3yr2yhj")))

(define-public crate-seeed-studio-relay-board-0.1.3 (c (n "seeed-studio-relay-board") (v "0.1.3") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "11222pdjv7rddd7ri9py9cmpr9rryn3l3xvzra9b0vq9g52mxqf2")))

(define-public crate-seeed-studio-relay-board-0.1.4 (c (n "seeed-studio-relay-board") (v "0.1.4") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "1q5y0a4bsf70qmzzgqzza48xgyasxaqkjwp482pdkljxcv690h3b")))

(define-public crate-seeed-studio-relay-board-0.1.5 (c (n "seeed-studio-relay-board") (v "0.1.5") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "0z0n2k4gr1sqx89m17k8yx3z3q90l0cljgaj9x50ibdci4hnm1mz")))

(define-public crate-seeed-studio-relay-board-0.1.6 (c (n "seeed-studio-relay-board") (v "0.1.6") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "0lpjrag7frdvf36rc5mvvwmwc5vzcmysxjzga2nxph4lq8y6xkx0")))

(define-public crate-seeed-studio-relay-board-0.1.7 (c (n "seeed-studio-relay-board") (v "0.1.7") (d (list (d (n "i2c-linux") (r "^0.1.2") (d #t) (k 0)))) (h "1fxjyzima4a1gcjqn01rb2ngn8x737llcxcb49m4a7fznwycmzql")))

