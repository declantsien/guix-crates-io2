(define-module (crates-io se ee seeed-erpc) #:use-module (crates-io))

(define-public crate-seeed-erpc-0.0.1 (c (n "seeed-erpc") (v "0.0.1") (d (list (d (n "bbqueue") (r "^0.4.11") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^6.0") (k 0)))) (h "03si1vbb4ckvnl4h3090cxcli0c9w945pskilmgk29r9ygy3c40b")))

(define-public crate-seeed-erpc-0.0.2 (c (n "seeed-erpc") (v "0.0.2") (d (list (d (n "bbqueue") (r "^0.4.11") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^6.0") (k 0)))) (h "0r5f9cca5q0c2vxsv8z0za78x7j6m6slmanh42db25iw3fw53w1y")))

(define-public crate-seeed-erpc-0.1.0 (c (n "seeed-erpc") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.4.11") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^6.0") (k 0)))) (h "1j192n78g0qi9ppd0cwbsimhb7maz7zppzkj211adx70vm6ly5yy")))

(define-public crate-seeed-erpc-0.1.1 (c (n "seeed-erpc") (v "0.1.1") (d (list (d (n "bbqueue") (r "^0.4.11") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "no-std-net") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^6.0") (k 0)))) (h "0y7mfff7d4kkr6dqy6vhgr318qg2shhsi1pyh7240ariff615ffm")))

