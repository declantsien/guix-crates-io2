(define-module (crates-io se ei seeip) #:use-module (crates-io))

(define-public crate-seeip-1.0.0 (c (n "seeip") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "117yaanbglczkg9n3p4d9fm84kjvdnvy1bvxkafv6rs7j37028j6")))

(define-public crate-seeip-2.0.0 (c (n "seeip") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrqbhq862q9d3wlrg21a6r3frd030vav0xnlgsphvw05c0jashw")))

(define-public crate-seeip-3.0.0 (c (n "seeip") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hd6wjdz2qk43s95d92z15h48y3v4cwbyg6kvwl0lfiq8ma53nx9")))

