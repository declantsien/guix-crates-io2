(define-module (crates-io se xt sext) #:use-module (crates-io))

(define-public crate-sext-0.1.0 (c (n "sext") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mvcz12g1mdkknpbkkplf8h77ix7srhh4l16qyfrsk9cjx1fww38") (y #t)))

(define-public crate-sext-0.1.1 (c (n "sext") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "136kmmhps8hklgh3xbb1z24qb2f9bwcsbr88qd41dx04ga3rx8n5")))

