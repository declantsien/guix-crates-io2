(define-module (crates-io se qi seqio) #:use-module (crates-io))

(define-public crate-seqio-0.1.0 (c (n "seqio") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "02ap9pdf2j2si3inb0xzar34b97khivn0s9mf87vgz0sy701l0sd") (r "1.65.0")))

(define-public crate-seqio-0.1.1 (c (n "seqio") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)))) (h "03bnhcjx9d8xl1f68nvlmfmgjc6hcgs4wy6ik63r13csrnn2iql8") (r "1.65.0")))

