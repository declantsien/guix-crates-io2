(define-module (crates-io se qi seqid) #:use-module (crates-io))

(define-public crate-seqid-0.1.0 (c (n "seqid") (v "0.1.0") (h "1v6qa57547h33gz2sybkg5xy7vhbk5fndvfvz2ympg4gp435rilj")))

(define-public crate-seqid-0.1.1 (c (n "seqid") (v "0.1.1") (h "0yvpwfkgpr9v7hd534c38ayw9431pjfbk212s608p6mv5960dx7a") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-seqid-0.1.2 (c (n "seqid") (v "0.1.2") (h "10vhp8q5cmmwyy032v1sz5gixpcav08lblplpgrsv5by1wabjn14") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-seqid-0.1.3 (c (n "seqid") (v "0.1.3") (h "117g8rn72r4ww00h7n2h85f04lg99npahjdwrc40ggah7bhb6yrn") (f (quote (("default" "alloc") ("alloc"))))))

