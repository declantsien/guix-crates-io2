(define-module (crates-io se l4 sel4-start) #:use-module (crates-io))

(define-public crate-sel4-start-0.0.4 (c (n "sel4-start") (v "0.0.4") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1nmxc2wnxs5i68vj1gn12kmz1m709w9s9r1d6cgzzdh7cy3s7x5s") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.5 (c (n "sel4-start") (v "0.0.5") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1ninmxkjsziqpyh33z1yn2f4j37s9p9b7q29ncmib82cgx5j7xab") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.6 (c (n "sel4-start") (v "0.0.6") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.6") (d #t) (k 0)))) (h "06bc5l2pzysp9slv0xpdccdz8i62h3rfpr1i6356k84nls6b2syc") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.15 (c (n "sel4-start") (v "0.0.15") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.15") (d #t) (k 0)))) (h "1x55p7bf2z6v6vcmwfvq5dzshvqv77f08dxv3p7d0fnpw3yz1224") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.18 (c (n "sel4-start") (v "0.0.18") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.18") (d #t) (k 0)))) (h "17vxzll9md69v1iqsa83j4gqkpxcbmslfzkl99w7fi6xm2gdhb8p") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.19 (c (n "sel4-start") (v "0.0.19") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.19") (d #t) (k 0)))) (h "1wmp5sij8phqlxc705690iydmkg184psq4azj0rrl0dayy9p884i") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.20 (c (n "sel4-start") (v "0.0.20") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4-sys") (r "^0.0.20") (d #t) (k 0)))) (h "0kj3b241350sql4k6vq5lxrfv1lj16lnwxmmvlyswj860g2v62gk") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.21 (c (n "sel4-start") (v "0.0.21") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4") (r "^0.0.2") (d #t) (k 0)) (d (n "sel4-sys") (r "^0.0.20") (d #t) (k 0)))) (h "0adh382s8c38s4l3a75gr61ps02xnyyfkq7ksj0hv6l9csw7wpgv") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.23 (c (n "sel4-start") (v "0.0.23") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4") (r "^0.0.7") (d #t) (k 0)) (d (n "sel4-sys") (r "^0.0.23") (d #t) (k 0)))) (h "0hl8d87nvbynzhnkg1ag5qxkpacf5lhf1m5z26j863132pqs1j85") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.26 (c (n "sel4-start") (v "0.0.26") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4") (r "^0.0.9") (d #t) (k 0)) (d (n "sel4-sys") (r "^0.0.26") (d #t) (k 0)))) (h "19fhlkkqd4lnhzp4b3pi0afgxw3p3fbh6ii6j8aq602j2z4hmgxp") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.27 (c (n "sel4-start") (v "0.0.27") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4") (r "^0.0.11") (d #t) (k 0)) (d (n "sel4-sys") (r "^0.0.27") (d #t) (k 0)))) (h "08xhbmn4i4gj4r1r0zrsaibbc3yl7k6rhq8isznng4bvmz78jl2j") (f (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.28 (c (n "sel4-start") (v "0.0.28") (d (list (d (n "maplit") (r "^0.1.3") (d #t) (k 1)) (d (n "sel4") (r "^0.0.12") (d #t) (k 0)) (d (n "sel4-sys") (r "^0.0.28") (d #t) (k 0)))) (h "1as3cn5v9aymgy20wcvn8mi4vg5yij7sy8lk4vhalkcl1rhxhkq1") (f (quote (("unstable"))))))

