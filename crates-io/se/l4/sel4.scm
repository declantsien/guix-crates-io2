(define-module (crates-io se l4 sel4) #:use-module (crates-io))

(define-public crate-sel4-0.0.1 (c (n "sel4") (v "0.0.1") (d (list (d (n "sel4-sys") (r "^0.0.15") (d #t) (k 0)))) (h "0grxs1wvv164fbk88dc0p06w8c1p3q86xr8xsvpqw8cj81bvxfqp") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.2 (c (n "sel4") (v "0.0.2") (d (list (d (n "sel4-sys") (r "^0.0.20") (d #t) (k 0)))) (h "11r3lnbxsipwvl4v5lfkgc5423br0i0h7m64qbh74bdawv3zbl52") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.4 (c (n "sel4") (v "0.0.4") (d (list (d (n "sel4-sys") (r "^0.0.22") (d #t) (k 0)))) (h "0gj9s5qd5dndv65baa8ayzjibx77iaxc2jgigc3b9fwh0a7qa90v") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.5 (c (n "sel4") (v "0.0.5") (d (list (d (n "sel4-sys") (r "^0.0.22") (d #t) (k 0)))) (h "0kvi55bwyp6zjap7phj7rjjg3v30w4nm0im1wa33kxwbby76p5sk") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.6 (c (n "sel4") (v "0.0.6") (d (list (d (n "sel4-sys") (r "^0.0.22") (d #t) (k 0)))) (h "0ic3gwwvqczalmwrzf6wdlc43n55ab4f7i1kwdr6z5vc3p1z5zaa") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.7 (c (n "sel4") (v "0.0.7") (d (list (d (n "sel4-sys") (r "^0.0.23") (d #t) (k 0)))) (h "0ijkcbfl8yga1kvd2sqzl7pbwvwxrfizl37m66x8l2flprvbanl4") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.8 (c (n "sel4") (v "0.0.8") (d (list (d (n "sel4-sys") (r "^0.0.25") (d #t) (k 0)))) (h "1g9yhp3yawadhc0p3mlw4jzggg34qmh4idcqmf0i6ifdc9k50zv6") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.9 (c (n "sel4") (v "0.0.9") (d (list (d (n "sel4-sys") (r "^0.0.26") (d #t) (k 0)))) (h "0l30g5vsqfnirpb72ikcrz8ga7x123g66w6sav0jp6yry03ba9nc") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.10 (c (n "sel4") (v "0.0.10") (d (list (d (n "sel4-sys") (r "^0.0.26") (d #t) (k 0)))) (h "0g7bsj1srgrpa2v8a6yix3c7khjp1hlar9xmwhmd4vsfz47ckbqr") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.11 (c (n "sel4") (v "0.0.11") (d (list (d (n "sel4-sys") (r "^0.0.27") (d #t) (k 0)))) (h "0nb7qhrl3rz9mh4hp1jyvnyar6kr0478g3ybw1xixw8741m5nbkv") (f (quote (("unstable"))))))

(define-public crate-sel4-0.0.12 (c (n "sel4") (v "0.0.12") (d (list (d (n "sel4-sys") (r "^0.0.28") (d #t) (k 0)))) (h "1yrglwzfxydzkg221c9wswrbxi3z8p6nbycfig2pc3vn4sps4idr") (f (quote (("unstable"))))))

