(define-module (crates-io se l4 sel4-sys) #:use-module (crates-io))

(define-public crate-sel4-sys-0.0.1 (c (n "sel4-sys") (v "0.0.1") (h "08f363csrdic6ixxbiydhl51al4i8hvdwglxk61dijj3fhhyqayl") (f (quote (("unstable"))))))

(define-public crate-sel4-sys-0.0.2 (c (n "sel4-sys") (v "0.0.2") (h "1scjg9ppfmv9vbd60pr647k63m6r5y07pbx4m58qwqhag882hiyb") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.3 (c (n "sel4-sys") (v "0.0.3") (h "13z202inr4zqwl898nz7hxa9h84s5lc52c8m2q6ix14fcfvc59f9") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.4 (c (n "sel4-sys") (v "0.0.4") (h "03zdhwv8z5dkixa1halxq7r4isjnc86vhdzavq257jrwwdfsg09l") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.5 (c (n "sel4-sys") (v "0.0.5") (h "0czcl09x6zqxg00bi3b5ik3cdsds48kw58n8c2nmw9p6afdidn6y") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.6 (c (n "sel4-sys") (v "0.0.6") (d (list (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1jr9bfi9g6yj5466gz4pxsiii0si6li7gd5prn6fkfmrjgp0r3nj") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.8 (c (n "sel4-sys") (v "0.0.8") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "15ynxdpch0fkn38g6nfj6sy9139iprbx6ik1vslr3yp3qrkflnm0") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.9 (c (n "sel4-sys") (v "0.0.9") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "070fh281pkb2bjdbplb8w5940zsp04maqsngsmwysz3zqzv3g129") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.10 (c (n "sel4-sys") (v "0.0.10") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1wm823b12z8l85m8r19jdngmcpwrhfaq0hqysrligmf8cx2j505h") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.11 (c (n "sel4-sys") (v "0.0.11") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "0lvxim415zsqsk3d0r7i7f3sjc8029ln37g24qg60gmjfkbvn3wh") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.12 (c (n "sel4-sys") (v "0.0.12") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1fcl4s0m6x4zv6b8qlsnd8aws0s1gyhnh5vmji35qk4n4b3ljrqv") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK")))) (y #t)))

(define-public crate-sel4-sys-0.0.13 (c (n "sel4-sys") (v "0.0.13") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "0pf25h6hh4926a2rsmq3myqbc49h7j1wfdlcv9h0h7srplnixpw8") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.14 (c (n "sel4-sys") (v "0.0.14") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1xvs39a4b81mz42a6n7mhxbm0iv3rzqy1wl0h52qks4zn6q1nnmr") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.15 (c (n "sel4-sys") (v "0.0.15") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1xfsq10jlq916dcfvkyv8qlj11zlxn3v4yay2zp42nz153sp81ad") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.16 (c (n "sel4-sys") (v "0.0.16") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "0qqhfibdamhh1js2glcs7rflr5ilv76xqx753p7riv5chaj3xdmb") (f (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.17 (c (n "sel4-sys") (v "0.0.17") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "0pmjpjpf3ra90j9is8r9wvh0da7bb848dr2l95yr2gv1jd778ka2") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.18 (c (n "sel4-sys") (v "0.0.18") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "19cff5psijzya61mym00dwshpx5fni48xsrnks66p3c6mra58nhj") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.19 (c (n "sel4-sys") (v "0.0.19") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "1qywgbsga580q34pql7s7shyx5ks7m6ppzsl1vx2i7aaxnx91xm5") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.20 (c (n "sel4-sys") (v "0.0.20") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "023l8fs8jmja0hmhw8k0kjkgz43fbbwhs53kh59rn3jmpas1wr19") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.21 (c (n "sel4-sys") (v "0.0.21") (d (list (d (n "bitflags-core") (r "^0.3.4") (d #t) (k 0)) (d (n "rlibc") (r "^0.1.4") (d #t) (k 0)))) (h "174aj6qvgkrj6kar2ynffdp8nvyd0rjnpiidazsfy1fv8hfnk2d5") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK")))) (y #t)))

(define-public crate-sel4-sys-0.0.22 (c (n "sel4-sys") (v "0.0.22") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "1g5i2w470xjf0ynn6hcqygbqbpw3vjdqsbg48sh9mma95y293mj6") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.23 (c (n "sel4-sys") (v "0.0.23") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "1230cvskzyivwy2kdp7pr1hvc2ba4b6dwmhsdasxgsmcbxiq3i8x") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.24 (c (n "sel4-sys") (v "0.0.24") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "0qmik8vq1clraczl0svhwg0ql53aiv9qkpmm9qd7ak7j2bl3bbfz") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.25 (c (n "sel4-sys") (v "0.0.25") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "0cyks3f9pbzd23na5v5k5wz35b3g1kcknihkpibn5zsjn99i4nm8") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.26 (c (n "sel4-sys") (v "0.0.26") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "129byf6x0zrxsy1apf67mg434w00h9wnp5ilsczgrxz6r9iyg4fj") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.27 (c (n "sel4-sys") (v "0.0.27") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "06yl3gvkg3vwmj2fcrmygm9kqmqlz2wbhv61f8s7q2n3bv9x3qdg") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.28 (c (n "sel4-sys") (v "0.0.28") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)))) (h "08j80gv72ys526k947na9slw66jlhv6bxamyay74df2f9jpph9dl") (f (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

