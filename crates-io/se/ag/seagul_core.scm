(define-module (crates-io se ag seagul_core) #:use-module (crates-io))

(define-public crate-seagul_core-0.1.0 (c (n "seagul_core") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "09fq9l64jdfnj12j50dd1siw910jcrrc9hnas3nv680vdzvr3gs8")))

