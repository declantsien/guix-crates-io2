(define-module (crates-io se ag seagulldoro) #:use-module (crates-io))

(define-public crate-seagulldoro-0.1.0 (c (n "seagulldoro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "breadx") (r "^0.2.3") (f (quote ("async"))) (d #t) (k 0)) (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)))) (h "0gi23l0hlsnxsbqlx6q9xmq1m34wgryvbgn2ac8v62v2wlih5jc4")))

