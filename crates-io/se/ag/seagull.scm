(define-module (crates-io se ag seagull) #:use-module (crates-io))

(define-public crate-seagull-0.1.0 (c (n "seagull") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "postgres") (r "^0.17.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1c13nfzhcl9hh1snw5x2phszn9ivkjcl3hkjsyzrd35c5mnx3si8")))

