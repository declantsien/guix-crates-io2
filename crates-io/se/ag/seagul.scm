(define-module (crates-io se ag seagul) #:use-module (crates-io))

(define-public crate-seagul-0.1.0 (c (n "seagul") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "seagul_core") (r "^0.1.0") (d #t) (k 0)))) (h "0lkd2xcvzdrmmg0wzzsswzj95npm26m9crrwj704k4ba6b0y00y7") (y #t)))

(define-public crate-seagul-0.1.1 (c (n "seagul") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "seagul_core") (r "^0.1.0") (d #t) (k 0)))) (h "1pgclpi1dxkls2f9lz91kn7bg7bq4pd7xjry8w53v3vn154l7a42")))

(define-public crate-seagul-0.2.0 (c (n "seagul") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "seagul_core") (r "^0.1.0") (d #t) (k 0)))) (h "12am3av88y2cf7fbmwc3ml184fdqvn1ram0jkyb9j6l1sazb7j9v")))

