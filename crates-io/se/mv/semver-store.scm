(define-module (crates-io se mv semver-store) #:use-module (crates-io))

(define-public crate-semver-store-0.1.0 (c (n "semver-store") (v "0.1.0") (h "0i855ghdxpwsvxxcpyn5n0yihh4q8kk1yv4j7b1hvvnahhm79h6b")))

(define-public crate-semver-store-0.2.0 (c (n "semver-store") (v "0.2.0") (h "1nbk0rw7rplqcg218sh2f3ih101cwlrmmay0a6pz6bk36bk80hfg")))

