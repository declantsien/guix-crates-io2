(define-module (crates-io se mv semver_sort) #:use-module (crates-io))

(define-public crate-semver_sort-0.1.0 (c (n "semver_sort") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17yf6z92as4nc68hwhykgf3mn5s3gqk2va8awm0mxr7ni64hsqvl")))

(define-public crate-semver_sort-0.2.0 (c (n "semver_sort") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ayb6hw50y2afdli7v2vkgijr377w3iib08l7b7f33cf6hqdb304")))

(define-public crate-semver_sort-1.0.0 (c (n "semver_sort") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0czj637k55y9zrvgzllldz1f5fjaz7w3495ywfpsra6h7zc85lsx")))

