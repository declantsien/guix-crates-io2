(define-module (crates-io se mv semverq) #:use-module (crates-io))

(define-public crate-semverq-0.1.0 (c (n "semverq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05mhalq8r8dhd8l6n6x92zli92h2darmjrsa5cavr1byfcsxrldl")))

