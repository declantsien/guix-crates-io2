(define-module (crates-io se mv semver-trick) #:use-module (crates-io))

(define-public crate-semver-trick-0.2.0 (c (n "semver-trick") (v "0.2.0") (h "0hbwgzpbyw6cbwfba0w1r23icy1k2i5wpdh7jm495cv3adav38fd")))

(define-public crate-semver-trick-0.3.0 (c (n "semver-trick") (v "0.3.0") (h "0rrykgzymspaham85l5d18csaixs0004hyyhpyjy57p6cmbnv2kp")))

(define-public crate-semver-trick-0.2.1 (c (n "semver-trick") (v "0.2.1") (d (list (d (n "semver-trick") (r "^0.3") (d #t) (k 0)))) (h "19c7sg7y5lhz1b0k8dnvikgd5d33s224ld1ap5lfrlllw1i2lhv0")))

