(define-module (crates-io se mv semver-eq) #:use-module (crates-io))

(define-public crate-semver-eq-1.0.22 (c (n "semver-eq") (v "1.0.22") (d (list (d (n "serde") (r "^1.0.194") (o #t) (k 0)))) (h "0rmgjphzrhvnpi16k55dk3dyr3yjcznr56ac25mxm12bsfnpmszb") (f (quote (("std") ("default" "std")))) (r "1.31")))

