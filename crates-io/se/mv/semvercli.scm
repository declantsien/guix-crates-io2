(define-module (crates-io se mv semvercli) #:use-module (crates-io))

(define-public crate-semvercli-0.0.1 (c (n "semvercli") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "0pcc1zwdyldkd512f54h3lmj5rkqqchgijch7n3bqqc1mwbxmz83")))

