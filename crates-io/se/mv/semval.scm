(define-module (crates-io se mv semval) #:use-module (crates-io))

(define-public crate-semval-0.0.0 (c (n "semval") (v "0.0.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0mhxqk1m8sirhbdy089ahra0iskh8xdx41igcyrrl0b7inxw39db") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.1 (c (n "semval") (v "0.0.1") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0aya7iqfysqqrc6cr4sf36fpzvj7ky272nr20in3vq8nhlhpjql1") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.2 (c (n "semval") (v "0.0.2") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "07kl5z4cscb66bg1hiykvfi692sqw684v33f49v5y8pl8f2d0zpz") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.3 (c (n "semval") (v "0.0.3") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1w0i123787z01zwmyh9468az06cwjl9fl1m46ccs0273yfblkpgv") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.4 (c (n "semval") (v "0.0.4") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "11cjr9426h9jsv6i42vh1qcg55sip5jidhdrwm780rgf0hn010ak") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.5 (c (n "semval") (v "0.0.5") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0f5gv3rdc7mdzi9gs95j4l5257x7lpgm8rsndjngl0gf3zq0w79p") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.6 (c (n "semval") (v "0.0.6") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "18xq94iy2hz4z82r1rcg7bq44dpd97qc3q7f7f1a1lv64jk5jbxf") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.7 (c (n "semval") (v "0.0.7") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0jb7dy6pa3468a9br3998jn7dwi75mykkp4mk4ykxxqa436cnvkj") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.0.8 (c (n "semval") (v "0.0.8") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "09bn7v3zzfnsc8djx5009b2gcdi8dv6fbqa8sraa0pkfpa9fckr6") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.0 (c (n "semval") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1x2nzfb2bwg8jbcr2hvkx5fq4jgwnpbqq0r3bvf675q0av9cq5a6") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.1 (c (n "semval") (v "0.1.1") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "05a5zyhlwyb7kx0n54c88kz49m9jvdfpfb1avbayn9j7x2dd2hp6") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.2 (c (n "semval") (v "0.1.2") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "14jppjw9vmf856nz3y8ggs2app89dxrlv6ys4lf7dzw91vs6zfaa") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.3 (c (n "semval") (v "0.1.3") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "02chm18yddg301w2wxqbc72yl8wxjir4bhpdcz1k5bgrr44w5vph") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.5 (c (n "semval") (v "0.1.5") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1alk0kksbjrr69ma3chwqj46y2c40cpdihrs656bdalz8dv2r2gw") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.6 (c (n "semval") (v "0.1.6") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1mpl2pvkp6jnq904pd20y5qafcmhpdvw08g9xjb3dx23j5z2vnv3") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.7 (c (n "semval") (v "0.1.7") (d (list (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1wd1w0nfxak990ydrwzpniwn7w367xcyhx1x6shyy2gras0617ss") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.1.8 (c (n "semval") (v "0.1.8") (d (list (d (n "smallvec") (r ">=1.7") (d #t) (k 0)))) (h "15hz76zqx54231jpgv0hn3lr2w5icivi6fgrpapi9nss4d7n32ff") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.2.0 (c (n "semval") (v "0.2.0") (d (list (d (n "smallvec") (r ">=1.7") (d #t) (k 0)))) (h "0aa9npc6jxk1m4hl552vik3fh6ach625pvj1aw9rsc6dbpkrqfc7") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.3.0 (c (n "semval") (v "0.3.0") (d (list (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0hchg0qws1ydqak5mn85w7nmpw7ksq0dcxayx8azd75npk0620k6") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.4.0 (c (n "semval") (v "0.4.0") (d (list (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "0pw3hnfhi3c5s565zfbvn5s53wvfa48fyxvvask2r1jh060jfv7z") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.4.1 (c (n "semval") (v "0.4.1") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)))) (h "13vdc3c5v9cl44xq7xrk15rc66ibw7l41ws943jzkwqxyqss48gf") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.5.0 (c (n "semval") (v "0.5.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)))) (h "03qh3axv3vxrzl0xsflvad9dnyn65h42rgj3fmbpa3c7i4rgawgp") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.5.1 (c (n "semval") (v "0.5.1") (d (list (d (n "smallvec") (r "^1.11.2") (f (quote ("union"))) (d #t) (k 0)))) (h "07s44pdcnhng5115wr9zdx4vhr2vi5mv303wlapyvvabw4p07gxj") (f (quote (("std") ("default" "std"))))))

(define-public crate-semval-0.5.2 (c (n "semval") (v "0.5.2") (d (list (d (n "smallvec") (r "^1.13.1") (f (quote ("union"))) (d #t) (k 0)))) (h "1jid96q2xij0ailn01chc725mb9p1h5n2kk3agpdyysbanifny9q") (f (quote (("std") ("default" "std"))))))

