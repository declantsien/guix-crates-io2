(define-module (crates-io se mv semverator) #:use-module (crates-io))

(define-public crate-semverator-0.1.1 (c (n "semverator") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0wy48v5qkj0i2jm19dbjrj2k2p3i1h053dfrli5f3y17y95809xb")))

(define-public crate-semverator-0.1.2 (c (n "semverator") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0nla9c1c7kw2vrgrs0gg2l0x1fnb7sjqyrp8d43fdnjmzfpfb5ip")))

(define-public crate-semverator-0.2.0 (c (n "semverator") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "056qzbjn8440qykynpfncfyf3rv8d63w2785inrwhv9k1yb0aglj")))

(define-public crate-semverator-0.2.1 (c (n "semverator") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "053q94xv34885nbfbfhawfgx6dj7ipls2hmap78vg3bzvq8mz2gz")))

(define-public crate-semverator-0.2.2 (c (n "semverator") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0bfdkdgwawb7yll2qdvxwq08cyi3k1acdpl9djyfzbwbgxq29919")))

(define-public crate-semverator-0.3.0 (c (n "semverator") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "03g8ignx4hl42fjq3j5r8qm6k0miwrh6v0rqlbq5km9wx9nh3z5a")))

(define-public crate-semverator-0.3.1 (c (n "semverator") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1p13jcgqbkw9fcwc88mayv314xcnh30ichzhbj0crjd48ian333p")))

(define-public crate-semverator-0.4.0 (c (n "semverator") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1x589zcv69fnzcyss5q3g4b6bhs5gkl2217wx0c725sxwnf2v6v3")))

(define-public crate-semverator-0.4.1 (c (n "semverator") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "156cm6i8958x75k5lcsi3zn3z2xgxz8p2c6s8ybmby6zssqsxs5s")))

(define-public crate-semverator-0.4.2 (c (n "semverator") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1jx0v02dawxm23m5kdmpbw4fsh5w4ysnlqwxvh0xs2n1sb5syrk8")))

(define-public crate-semverator-0.4.3 (c (n "semverator") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0qwmv7g06ml2z29md1q67w694jhm6n6if4r3pmzssrj216nb10x2")))

(define-public crate-semverator-0.5.0 (c (n "semverator") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0jsp0mr8bhxpi2jljhcs3ha31bzy9ssc4j3ady8m16l7ddhgnlds")))

(define-public crate-semverator-0.5.1 (c (n "semverator") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0wfb833j0q5z1xd8zks938z2mf7amfxwwxxh9z2xjjm392zfzary")))

