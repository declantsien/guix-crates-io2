(define-module (crates-io se mv semver-tool) #:use-module (crates-io))

(define-public crate-semver-tool-0.1.0 (c (n "semver-tool") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "semver") (r "^1.0.4") (d #t) (k 0)))) (h "1cl9na1abyifikjk8ighdni79c2mh30zxa3pd2hvriginf56pk67")))

