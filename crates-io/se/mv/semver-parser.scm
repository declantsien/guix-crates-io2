(define-module (crates-io se mv semver-parser) #:use-module (crates-io))

(define-public crate-semver-parser-0.1.0 (c (n "semver-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "1a8qv0whvmz7fcvqq21y6sg2q28sidj58wzrlzfdbq9yb6s54pal")))

(define-public crate-semver-parser-0.2.0 (c (n "semver-parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "00a7rdbgaxwzpb1sxks6bri8bfysfpmi4jh3vjd58vxqx76msl9k")))

(define-public crate-semver-parser-0.3.0 (c (n "semver-parser") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "1m2ihr0yca51y9zv075baxwnz52qcimnn9kn40xzsfii563wza2z")))

(define-public crate-semver-parser-0.4.0 (c (n "semver-parser") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "1wljnifg62fbd66hjzrzd06hgw9sz5yrf4yjfv9pm019cfz85704")))

(define-public crate-semver-parser-0.4.1 (c (n "semver-parser") (v "0.4.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "0xsamdk1bigyy8b4s4ywr29gd5pzlkw36cf3zhmpclg67r4kjh7s")))

(define-public crate-semver-parser-0.5.0 (c (n "semver-parser") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "1s779pj2c8zfp83kshyf8xdp70m1pn7p2rkl37adw0229f700qsx")))

(define-public crate-semver-parser-0.5.1 (c (n "semver-parser") (v "0.5.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "16wcnda802chx57m9lx0wl9zw2yq0y2ll95hdzxf0wmx07ffzd6k")))

(define-public crate-semver-parser-0.6.0 (c (n "semver-parser") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1wm6vxn563xxlcg2h9qphna6pp4cfngp2ppn3p7s06zjjlrxi73m")))

(define-public crate-semver-parser-0.6.1 (c (n "semver-parser") (v "0.6.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1s8s7a7yg8xhgci17y0xhyyncg229byivhpr0wbs3ljdlyjl73p8")))

(define-public crate-semver-parser-0.6.2 (c (n "semver-parser") (v "0.6.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0rrsf0giv2pdvid0pmwcxfqlrq16k4sc3n5cjnmkcim5qp4z7zyq")))

(define-public crate-semver-parser-0.7.0 (c (n "semver-parser") (v "0.7.0") (h "18vhypw6zgccnrlm5ps1pwa0khz7ry927iznpr88b87cagr1v2iq")))

(define-public crate-semver-parser-0.9.0 (c (n "semver-parser") (v "0.9.0") (h "1ahqhvgpzhcsd28id7xnrjv4419i9yyalhm7d7zi430qx0hi2vml")))

(define-public crate-semver-parser-0.10.0 (c (n "semver-parser") (v "0.10.0") (d (list (d (n "pest") (r "^2.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.0") (d #t) (k 0)))) (h "1mc3l1l0vyh33ldgwhlxhr764pjn1wd2dfd7gf4r3yw0adn2q08f")))

(define-public crate-semver-parser-0.10.1 (c (n "semver-parser") (v "0.10.1") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0a0lgmnd7jga3c6090lsn4lifh3mnzmy4v6d6yqg9rfm59n19vs2")))

(define-public crate-semver-parser-0.10.2 (c (n "semver-parser") (v "0.10.2") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1xqijhqhx3bn77xnl1mlcp032hz8nv7n2fbdacbdzq7rnzsvxc00")))

