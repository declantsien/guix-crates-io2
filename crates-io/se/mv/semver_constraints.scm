(define-module (crates-io se mv semver_constraints) #:use-module (crates-io))

(define-public crate-semver_constraints-0.1.0 (c (n "semver_constraints") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03m29jawrgj4898mg5v7appr252x482cm4c7wl816g8qcv1l68gr") (f (quote (("default"))))))

