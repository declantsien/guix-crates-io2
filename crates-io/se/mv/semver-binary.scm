(define-module (crates-io se mv semver-binary) #:use-module (crates-io))

(define-public crate-semver-binary-0.1.0 (c (n "semver-binary") (v "0.1.0") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)))) (h "0cmi7yasnqm47kmb70gmxjwzhhm97i8hpsk5kdzz4ixpa38h12ws") (f (quote (("default"))))))

