(define-module (crates-io se mv semver-cli2) #:use-module (crates-io))

(define-public crate-semver-cli2-0.1.0 (c (n "semver-cli2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "18l90gmg45qwyn45am7gkawf39rd1b9md9rwvx76nnp3ma1zwlrk")))

