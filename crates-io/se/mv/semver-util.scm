(define-module (crates-io se mv semver-util) #:use-module (crates-io))

(define-public crate-semver-util-0.1.0 (c (n "semver-util") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver_pub") (r "^1.0.14") (d #t) (k 0) (p "semver")) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "170fiyzqichfrnlxxi878khqbvcvls77s3wkdaiqdy1rdvnpyvyd") (r "1.60")))

(define-public crate-semver-util-0.1.1 (c (n "semver-util") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver_pub") (r "^1.0.14") (d #t) (k 0) (p "semver")) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "066ny1ck6ar4ydfbbgkpffnhqg8llr4ra5cz7waixsfjb4ks24nk") (r "1.60")))

(define-public crate-semver-util-0.1.2 (c (n "semver-util") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver_pub") (r "^1.0.14") (d #t) (k 0) (p "semver")) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kdgcyyzj9jw7q9nkvbqispp6kmsvhzdr5iw8a8cazmky8sf97d9") (r "1.60")))

(define-public crate-semver-util-0.2.0 (c (n "semver-util") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver_pub") (r "^1.0.14") (d #t) (k 0) (p "semver")) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pmy5i0f21v79gzfz6jfirzfadby9mvk3jv1hkral0saygh5aqs3") (r "1.60")))

(define-public crate-semver-util-0.2.1 (c (n "semver-util") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver_pub") (r "^1.0.14") (d #t) (k 0) (p "semver")) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0acsyfyv8b2b902j1wqiik0v7ncck1vzprzj0lhszyivq1lv528d") (r "1.64")))

