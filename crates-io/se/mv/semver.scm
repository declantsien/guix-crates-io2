(define-module (crates-io se mv semver) #:use-module (crates-io))

(define-public crate-semver-0.1.0 (c (n "semver") (v "0.1.0") (h "00b56gkxal340rnypw4rph7ddqvcahr7cqcbxi04xlb0v1nq2aqm") (y #t)))

(define-public crate-semver-0.1.1 (c (n "semver") (v "0.1.1") (h "1g6frwiprlsgazayidjzixm1vhg9p7f76is1gi0nlhcafzgdrlgs") (y #t)))

(define-public crate-semver-0.1.2 (c (n "semver") (v "0.1.2") (h "0d5v7kp3fw8w7wwzxfwwh2shw9yivh0k0r5xhcqcwwddrhvprnyx") (y #t)))

(define-public crate-semver-0.1.3 (c (n "semver") (v "0.1.3") (h "0ysb3wqjlrax5wf92piwbkc147ssmnamlx8gffylf1jfivi25whv") (y #t)))

(define-public crate-semver-0.1.4 (c (n "semver") (v "0.1.4") (h "1x54nmjjgjz9nxgwn41mxy29qlid9cjcxvg5kw1rqx88b5plpjsw") (y #t)))

(define-public crate-semver-0.1.5 (c (n "semver") (v "0.1.5") (h "1grd8vh5d2cckhh1cc4niwig60sjqcrap0g9v0zwxmv0v8z32nhm") (y #t)))

(define-public crate-semver-0.1.6 (c (n "semver") (v "0.1.6") (h "1x5qb6vmqr3caa6l4lgqggxqld6h7999mnbraghf7wcvih1br7bs") (y #t)))

(define-public crate-semver-0.1.7 (c (n "semver") (v "0.1.7") (h "1vn8x4gc5imikmip4s84a9ypvm3clawa2flg45101fwxwrchkvwj") (y #t)))

(define-public crate-semver-0.1.8 (c (n "semver") (v "0.1.8") (h "19rc0bqyzhmwznz47xsdd0lkc45zvvffsxvdbl83jcx87nbv5nsw") (y #t)))

(define-public crate-semver-0.1.9 (c (n "semver") (v "0.1.9") (h "1l786jglm6zfyzl0r4zlbdflpdvgmla8gkmvyi8p5r8m7d9p4ixx") (y #t)))

(define-public crate-semver-0.1.10 (c (n "semver") (v "0.1.10") (h "0x56331g9fdhkbncnvkbmiqi9pf3fb8sw7m6nrmgxghy5ga43mdq") (y #t)))

(define-public crate-semver-0.1.11 (c (n "semver") (v "0.1.11") (h "1yy6z5jdapgrsx12axhwv2928las2s80i74c96dc6xma5ka06zi9") (y #t)))

(define-public crate-semver-0.1.12 (c (n "semver") (v "0.1.12") (h "1dgh144130xg7l5pwnvyb16whdfqk5xc6mcmg0yzbdsrj6wxpdmd") (y #t)))

(define-public crate-semver-0.1.13 (c (n "semver") (v "0.1.13") (h "19fibjyi433l1z3x8gj9fpfr3859y835l64w0p3wh6shrvgsb6h0") (y #t)))

(define-public crate-semver-0.1.14 (c (n "semver") (v "0.1.14") (h "0fjqgsg1lhmg5j7ykhl3ci5y3j5x92ky6nmvik80xrpbr52jcf6x") (y #t)))

(define-public crate-semver-0.1.15 (c (n "semver") (v "0.1.15") (h "1lxiyblk9b9h07vhbi85m4i473ysv4lgjmkp5hmvx29pb4asv5r9") (y #t)))

(define-public crate-semver-0.1.16 (c (n "semver") (v "0.1.16") (h "0fry30rlj9lv5ycbm97nnanx28flld426psbyidfzgnr3sjbf1iv") (y #t)))

(define-public crate-semver-0.1.17 (c (n "semver") (v "0.1.17") (h "1c0n88s61ychbwsc0gn14hjsj3j7hv1sf1h9vic5i0cin8bryxni") (y #t)))

(define-public crate-semver-0.1.18 (c (n "semver") (v "0.1.18") (h "0znr9wibmgmiw8igzp1z26qjbjaqgw4vq23y37wvqaq36y4231m7") (y #t)))

(define-public crate-semver-0.1.19 (c (n "semver") (v "0.1.19") (h "1fhirn6k6l34qmp36qmj9xkipf1zxx5x4d4l7gjps17pl93kw0mk") (y #t)))

(define-public crate-semver-0.1.20 (c (n "semver") (v "0.1.20") (h "1b10m0hxrr947gp41lj9vnmgl5ddwx3d41vnblsg06ppvkz11x6l")))

(define-public crate-semver-0.2.0 (c (n "semver") (v "0.2.0") (h "1jfgnaws4zi5bm8lms8gwnkw3varyy390za461v3ajvxcaz8vdib")))

(define-public crate-semver-0.2.1 (c (n "semver") (v "0.2.1") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^1.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "07cjg24jlr7qkcy2prxxlpygianbpxa0vbdg6fqimh6z1k3wvrlp") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.2.2 (c (n "semver") (v "0.2.2") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^1.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1wiz89fy6g4z9vgl2br1j5va295qjyh6gkdg4qh001yrjslk7b70") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.2.3 (c (n "semver") (v "0.2.3") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "nom") (r "^1.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0gv28l5ggain32flkvw3sss0szz810y3pjw89vciaf7hl4w7cnrd") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.4.0 (c (n "semver") (v "0.4.0") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "04fxlln4ssnjwrrv3s5wh50vj9x84m71wzap4lgy7qkxpxn72gjs") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.4.1 (c (n "semver") (v "0.4.1") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.6.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0kirdvbj06x6hffrab86raqy3dyf9i903ny1qlmj12iyq2phc76a") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.5.0 (c (n "semver") (v "0.5.0") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.6.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0m6rfaah36lzn1lsig4gf7wb87ms7ljkih7274ir86307yv0lpx1") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.5.1 (c (n "semver") (v "0.5.1") (d (list (d (n "crates-index") (r "^0.4.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.6.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1xbiv8l72rmngb3lgbmk3vd4lalcbzxcnrn085c2b75irl7gcbxf") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.6.0 (c (n "semver") (v "0.6.0") (d (list (d (n "crates-index") (r "^0.5.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0drmjiwkq0znj33q9x9hw7ld8f28n9diyjs3jlh1l1v5kvn8ccbs") (f (quote (("default") ("ci"))))))

(define-public crate-semver-0.7.0 (c (n "semver") (v "0.7.0") (d (list (d (n "crates-index") (r "^0.5.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "11bac3qzsvppsy2cx18k1l8ip55ji68lnibwzgvpg9qgbaw63p9z") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-semver-0.8.0 (c (n "semver") (v "0.8.0") (d (list (d (n "crates-index") (r "^0.5.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0js5nqq9v2xp4yvdr8038sqrbmr5bgccms16mc6xdn5jka8brqmy") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-semver-0.9.0 (c (n "semver") (v "0.9.0") (d (list (d (n "crates-index") (r "^0.5.0") (d #t) (k 2)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "00q4lkcj0rrgbhviv9sd4p6qmdsipkwkbra7rh11jrhq5kpvjzhx") (f (quote (("default") ("ci" "serde"))))))

(define-public crate-semver-0.10.0 (c (n "semver") (v "0.10.0") (d (list (d (n "diesel") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1401i88135h2paxwvf0b51hf585rdzxa8yxg7j800gk2z8lfqk1r") (f (quote (("default") ("ci" "serde" "diesel/sqlite"))))))

(define-public crate-semver-0.11.0 (c (n "semver") (v "0.11.0") (d (list (d (n "diesel") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "semver-parser") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dn6064fipjymnmjccyjhb70miyvqvp08gvw1wbg8vbg4c8ay0gk") (f (quote (("default") ("ci" "serde" "diesel/sqlite"))))))

(define-public crate-semver-1.0.0-rc.1 (c (n "semver") (v "1.0.0-rc.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0qa85nplv5p3521v1h77vh76q20pcg51a1gfzmzypymv7plf7q8b") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.0-rc.2 (c (n "semver") (v "1.0.0-rc.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "01lcpgc88sc1aimpvva7a0gxs1ph4lc44z8axinwjsb0jg35qq8z") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.0 (c (n "semver") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1wxbg1wn35dq12mnf2ir5x9l5h1alayxpabnjahvx6zbh4p89dbn") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.1 (c (n "semver") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "08x74j2hkijm2j2s4xqhj49g83ivgpcqadjfqrdcnp8x06zxl8yh") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.2 (c (n "semver") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0c1j6rfadv0446v3iq2mzm64llkngbjf588sj8j71lk69anlamln") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.3 (c (n "semver") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1gna1p10i86sf1pqfqndkwl0wks35x84yvjw77c74ckzxrbsqfjz") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.4 (c (n "semver") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "04l00sn8y7lv1a8j11a6r7vwcm5qmlsdh7zqb0rw2cxab1i8x2jn") (f (quote (("std") ("default" "std"))))))

(define-public crate-semver-1.0.5 (c (n "semver") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1ivj7z9yp6v46ml40nsr8aqh64fphzv5xfvkxpxni6pcja7731h4") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.6 (c (n "semver") (v "1.0.6") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "13gkqi8szcqgn3k6agndw9kfggfa41pm6ir02y3l5lpd0cg3i8x4") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.7 (c (n "semver") (v "1.0.7") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1i1ca3q9y08kfm4hhkk0r9xd4j3z511r4nyr4b99cwdy927x4nyn") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.8 (c (n "semver") (v "1.0.8") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "07albvmqbwa3vq01d0g51db92mvpah3cj2bskxpypwlc38p7q9fh") (f (quote (("std") ("default" "std")))) (y #t) (r "1.31")))

(define-public crate-semver-1.0.9 (c (n "semver") (v "1.0.9") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1za0l0syrm1pfqjbqly7rm84baqjknhn4my4qf6nr4xmznyl7clc") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.10 (c (n "semver") (v "1.0.10") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "072yn9w4bm3vpiashjrlvi64809nwxjkf1i7qmx945d0zqg0c7d4") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.11 (c (n "semver") (v "1.0.11") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1pf6y017l563csl061zz4p0x9bm6hx0yax1gprwk8x8pnbmbx4ix") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.12 (c (n "semver") (v "1.0.12") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1qb4hmpsm0c0j6pi0g2hdr55yk1b7j2rz0kl34mqynfnyrnkwcx2") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.13 (c (n "semver") (v "1.0.13") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "049ppz71ayvgwy1sa87c0dh49gvj6ls8rvnyna5xc0whf0g89xlk") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.14 (c (n "semver") (v "1.0.14") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1i193dd6xkhh2fi1x7rws9pvv2ff3jfl9qjvvd9y6y6pcg2glpg2") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.15 (c (n "semver") (v "1.0.15") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1n67h12ddvvda4qgdiq8wzn1icah3b695qxy1a640c37jdpj9yiv") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.16 (c (n "semver") (v "1.0.16") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0amsbj3572r1kx5wdcpcgfhfwbmcc17axp9adc6nkiwg6xkrbg2q") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.17 (c (n "semver") (v "1.0.17") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1vf03d99sikkradjj33q1l9h4fqgd1h7darjypic6pnh4qrkdgdy") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.18 (c (n "semver") (v "1.0.18") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0659sqgnaqx42nj7n5kh3z35g3jvczsw572jhir4ibys555knadh") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.19 (c (n "semver") (v "1.0.19") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1c2gg8k8sij0vbbd2dv30mx228xxqiw37apjiqdf0v8w419715xd") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.20 (c (n "semver") (v "1.0.20") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "140hmbfa743hbmah1zjf07s8apavhvn04204qjigjiz5w6iscvw3") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.21 (c (n "semver") (v "1.0.21") (d (list (d (n "serde") (r "^1.0.194") (o #t) (k 0)))) (h "1c49snqlfcx93xym1cgwx8zcspmyyxm37xa2fyfgjx1vhalxfzmr") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.22 (c (n "semver") (v "1.0.22") (d (list (d (n "serde") (r "^1.0.194") (o #t) (k 0)))) (h "1jir6q2ps4s5v52bqxpvwj35p0m0ahl5pf62ppwksbv5kvk3zm4j") (f (quote (("std") ("default" "std")))) (r "1.31")))

(define-public crate-semver-1.0.23 (c (n "semver") (v "1.0.23") (d (list (d (n "serde") (r "^1.0.194") (o #t) (k 0)))) (h "12wqpxfflclbq4dv8sa6gchdh92ahhwn4ci1ls22wlby3h57wsb1") (f (quote (("std") ("default" "std")))) (r "1.31")))

