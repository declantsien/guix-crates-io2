(define-module (crates-io se mv semver-bump-trait) #:use-module (crates-io))

(define-public crate-semver-bump-trait-0.1.0 (c (n "semver-bump-trait") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0yggc7sq9sbcwkpaq59jjkkmiq6b1gfigb7kxpj72qiml6mqkv6k")))

