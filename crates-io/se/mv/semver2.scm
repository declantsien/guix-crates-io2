(define-module (crates-io se mv semver2) #:use-module (crates-io))

(define-public crate-semver2-0.1.0 (c (n "semver2") (v "0.1.0") (d (list (d (n "omnom") (r "^2.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "00r7qg9j7j51a0hwcp0zsmpnaqq2bphzhqz6x84l29g75ypk8wl1")))

