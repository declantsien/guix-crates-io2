(define-module (crates-io se mv semver_rs) #:use-module (crates-io))

(define-public crate-semver_rs-0.1.0 (c (n "semver_rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "0njnnyxm8dk1frz6lyydqlcnfyp973ipq7prahf0hqx5v8nyv7zc")))

(define-public crate-semver_rs-0.1.1 (c (n "semver_rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "1yhyx2qywqpcd5c6zm6yyh5wx0yfwd3j8f9jgs0amyl56insrcax")))

(define-public crate-semver_rs-0.1.2 (c (n "semver_rs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "1fs2x07fll710z35bnzhmxw7hlhynwlzjzqrvgjkriq0x86akgb4")))

(define-public crate-semver_rs-0.1.3 (c (n "semver_rs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "0xzj9hcvni7l9xxmvrhvrh0hkfgmxmz4wxfgmp07abkns4mx8y8k")))

(define-public crate-semver_rs-0.2.0 (c (n "semver_rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "11i58yfy3909ksp6njv42bfa0yqwynz581rkpgnbbrbjp6mf354m") (f (quote (("default"))))))

