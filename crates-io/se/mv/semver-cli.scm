(define-module (crates-io se mv semver-cli) #:use-module (crates-io))

(define-public crate-semver-cli-0.1.0 (c (n "semver-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0ii58zyqdqrnc74r83sycdnlbk1cc1qhavxzhhxx5y81qwrapcxp")))

(define-public crate-semver-cli-0.1.1 (c (n "semver-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "00nwfd6xygdsf08n67n1my1xs3hbmkn4f24ja6ijpl3malycllmg")))

(define-public crate-semver-cli-0.1.2 (c (n "semver-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0xhy02asygl0h1wab7531dhx2p6j4m75d2r5p9ilgahbcvzbz8ww")))

(define-public crate-semver-cli-0.1.3 (c (n "semver-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0i0bjgvs03aw37m5py073yys9kl0j2rz969ly4p905fkprmnmr8c")))

