(define-module (crates-io se mv semver-explain) #:use-module (crates-io))

(define-public crate-semver-explain-0.1.0 (c (n "semver-explain") (v "0.1.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0y8zf88xf1bp38ivbwqspkzlfa6g58l00dliq15add2gddqm77pi") (r "1.56")))

(define-public crate-semver-explain-0.2.0 (c (n "semver-explain") (v "0.2.0") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "04sn68i6jq3vmnzcgjwqw1z08qm06nl3zggx02hviw7hq4kmmpax") (r "1.56")))

(define-public crate-semver-explain-0.2.1 (c (n "semver-explain") (v "0.2.1") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0liis7w7l48qfgpk4mk7fj05wsgxdhjqzqpf04dwn18aikxd1isr") (r "1.56")))

(define-public crate-semver-explain-0.2.2 (c (n "semver-explain") (v "0.2.2") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zrwv0cqgcw7gdf6wg77f3dwgw7cnzff53s7796m2sb1vay1282n") (r "1.56")))

(define-public crate-semver-explain-0.2.3 (c (n "semver-explain") (v "0.2.3") (d (list (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rjsx693fzwa9irwfhgi906k7z9kbkwgzl52m7fcffmfgi3wvwsj") (r "1.53")))

