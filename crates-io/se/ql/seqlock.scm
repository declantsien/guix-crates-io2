(define-module (crates-io se ql seqlock) #:use-module (crates-io))

(define-public crate-seqlock-0.1.0 (c (n "seqlock") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1342s40r7q3mg0mhwl0r05c3arl9qrbc24x48rzxiziy43lln6br") (f (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.1.1 (c (n "seqlock") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1ghzy27fnv7s2nm9ppq1hk9191w91nabiyphx50nmhc31ml1hf3i") (f (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.1.2 (c (n "seqlock") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "18mgi2sk7ql7ycrhwkn7jmp92ml48iic6nw7vfj9gfmdxidr6kk6") (f (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.2.0 (c (n "seqlock") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "040r6wl53vqax3d3r2jsaiw2cdcja1mxfqzscrnbiigc2ipppimm")))

