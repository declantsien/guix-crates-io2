(define-module (crates-io se dr sedregex) #:use-module (crates-io))

(define-public crate-sedregex-0.1.0 (c (n "sedregex") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jjz9bxgrrkq703z2yalxaaf2z3dnarl98s4x5wh5cagzwf8p8im")))

(define-public crate-sedregex-0.1.1 (c (n "sedregex") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mrpr9iy74a6yqgxy3fm0jj93m2k9pfvdpm20nd9q305wf6ijqzi")))

(define-public crate-sedregex-0.2.0 (c (n "sedregex") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1aww6fggzshlixv09zhv29q9bin32c647nd5yf1753n0cdw1svm8")))

(define-public crate-sedregex-0.2.1 (c (n "sedregex") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11p1drm8xrkdfibmrn97mrpg2w61svx95x7m8073nmwxw99gvlzx")))

(define-public crate-sedregex-0.2.2 (c (n "sedregex") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1igz6m2n2saqpx19rs17k5zb7ww3w0mkhd884lsnvkab1vd3856s")))

(define-public crate-sedregex-0.2.3 (c (n "sedregex") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02lrgzb9h6mds4vm2z0hqx8xyymfgbxbjmz3668mkj2gi0n9akrm")))

(define-public crate-sedregex-0.2.4 (c (n "sedregex") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bwp2qbqgr93k4ggn9wcgb5jhinwiisnp686fsmz39ji6saahf79")))

(define-public crate-sedregex-0.2.5 (c (n "sedregex") (v "0.2.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gpy5sqck7rb7v8ryyy7zr5vnab37dh4bp0iplxz14v0b4iiwh8r")))

