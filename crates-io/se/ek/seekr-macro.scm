(define-module (crates-io se ek seekr-macro) #:use-module (crates-io))

(define-public crate-seekr-macro-0.1.0 (c (n "seekr-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seekr") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrzqir4m1pjrimnbhnslj0h1ph20m5srk4p48x4c8p5vqff6yal") (s 2) (e (quote (("dsl" "dep:seekr"))))))

(define-public crate-seekr-macro-0.1.1 (c (n "seekr-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seekr") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11cmjq4paqg2spp34vjpmf0q9qszv7ydvz7y9wpq3rm3asqvdjh0") (s 2) (e (quote (("dsl" "dep:seekr"))))))

(define-public crate-seekr-macro-0.1.2 (c (n "seekr-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "seekr") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qlwkag1a8r8y96gvd5sa4l7ch5kwxhpilin10g1gdqw3q0g99ak") (s 2) (e (quote (("dsl" "dep:seekr"))))))

