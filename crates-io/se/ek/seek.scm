(define-module (crates-io se ek seek) #:use-module (crates-io))

(define-public crate-seek-0.1.0 (c (n "seek") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "04shvllqnq541rdbb4p7zsb0f3qya48p8i56c5naavwzc1xclpwa")))

