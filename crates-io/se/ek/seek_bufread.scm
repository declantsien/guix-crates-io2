(define-module (crates-io se ek seek_bufread) #:use-module (crates-io))

(define-public crate-seek_bufread-1.0.1 (c (n "seek_bufread") (v "1.0.1") (h "00wnqsmjyj8pydij3i3ydbdr394z91ix1z2rr2b6447r4ax0s6x7")))

(define-public crate-seek_bufread-1.0.2 (c (n "seek_bufread") (v "1.0.2") (h "197p4bwljah88fvgvm308vy36kdzzd5j4ff1bjai4lwqs3larp28")))

(define-public crate-seek_bufread-1.1.1 (c (n "seek_bufread") (v "1.1.1") (h "08y8y4pnzflr0awf8fn5mjica7ws3z131nk8gxxqxfg0fpj02ar2")))

(define-public crate-seek_bufread-1.2.0 (c (n "seek_bufread") (v "1.2.0") (h "19a2nh9gxizhwyy741f7dk1wp7yv3s8w68j20mscx0zr1s0cqhw8")))

(define-public crate-seek_bufread-1.2.2 (c (n "seek_bufread") (v "1.2.2") (h "13fmvlcidy345w5sljzw829gs2vwdg9152jyqwi7hja4l6y4i8nq")))

