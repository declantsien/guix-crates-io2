(define-module (crates-io se ek seekable_reader) #:use-module (crates-io))

(define-public crate-seekable_reader-0.1.0 (c (n "seekable_reader") (v "0.1.0") (h "0jn8nk4yxl8hy681kvaasa8ghp4sdpw8v82rc3fj1raprbxyksqm")))

(define-public crate-seekable_reader-0.1.1 (c (n "seekable_reader") (v "0.1.1") (h "073x5qg10vj2imh7gf1q1b8dgpf86ci3xq5jvkhbh4ifkz4j320y")))

(define-public crate-seekable_reader-0.1.2 (c (n "seekable_reader") (v "0.1.2") (h "1l0qllh9p8dly2lc5rnv8890fx1xncc96hhc114fg345i8nvldz5")))

