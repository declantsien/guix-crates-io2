(define-module (crates-io se ek seeker) #:use-module (crates-io))

(define-public crate-seeker-0.1.0 (c (n "seeker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "memory-stats") (r "^1.1.0") (f (quote ("always_use_statm"))) (d #t) (k 0)) (d (n "pddllib") (r "^0.1.12") (d #t) (k 0)) (d (n "pddlp") (r "^0.1.4") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mpsl0i3mnhi2grrw6c5mrgl3lirfkp12fs8wvii05wh2ng4h58j")))

