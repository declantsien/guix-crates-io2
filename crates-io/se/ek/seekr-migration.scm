(define-module (crates-io se ek seekr-migration) #:use-module (crates-io))

(define-public crate-seekr-migration-0.1.0 (c (n "seekr-migration") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.0") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "19rqai0fii83n0c50rh77aw87ky696ildxcjabp0x8ivry11xi96")))

(define-public crate-seekr-migration-0.1.1 (c (n "seekr-migration") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.0") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1w372nx6y1v4j2d685730ynyihjj479ilk9rj3f11gja9igk2ay9")))

(define-public crate-seekr-migration-0.1.2 (c (n "seekr-migration") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.12.0") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "0f1wcby9q57d1p9fdwgg8b562h7lfamsdmrvgvl7kr8nkfj20rbs")))

