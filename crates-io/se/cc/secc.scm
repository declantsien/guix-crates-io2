(define-module (crates-io se cc secc) #:use-module (crates-io))

(define-public crate-secc-0.0.5 (c (n "secc") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iyxdxzm7x6wv1c15mhwcgb3mjgznn108mm0p42g2n4m5dln2x3d")))

(define-public crate-secc-0.0.6 (c (n "secc") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10jn07s4g44f4mncglysw9b5gdlksh27wz29xyhwpkz28s47l8zl")))

(define-public crate-secc-0.0.7 (c (n "secc") (v "0.0.7") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jrz0zbgyq4j5y9icd2mq7wf4kpbv6mx4c7xnv08l6qq09sgj1yn")))

(define-public crate-secc-0.0.8 (c (n "secc") (v "0.0.8") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0isr99hg0k3q0ds4g39677ji7m4zpxaax8bkqjxxbxpij151ykl3")))

(define-public crate-secc-0.0.9 (c (n "secc") (v "0.0.9") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1j542m2xvxskbdi2zs621q66i5434q6bdg3971yvcli5p0iyhyal")))

(define-public crate-secc-0.0.10 (c (n "secc") (v "0.0.10") (h "0p5r5g5p9hp3lbnqxfnsr3viqq8n0wn7a1pqq637jgjmq03q9krz")))

