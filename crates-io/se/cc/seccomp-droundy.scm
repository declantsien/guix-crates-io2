(define-module (crates-io se cc seccomp-droundy) #:use-module (crates-io))

(define-public crate-seccomp-droundy-0.1.0 (c (n "seccomp-droundy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-droundy-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1y75z8hni0mjfz6h6bjnlvr8ic3k1dhfh88b7sd1mgy6f6mn5dyl")))

