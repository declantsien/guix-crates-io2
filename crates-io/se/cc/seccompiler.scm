(define-module (crates-io se cc seccompiler) #:use-module (crates-io))

(define-public crate-seccompiler-0.1.0 (c (n "seccompiler") (v "0.1.0") (h "1dziqw3axiiqgskw5x0wksnlbjjqw1vjdsjq62gf7rjyhiqxyk93")))

(define-public crate-seccompiler-0.2.0 (c (n "seccompiler") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "0s0fahfw7p38jb9rh6pcvrfqn8zi0v8hpws9xb6246qkl69147g0") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-seccompiler-0.3.0 (c (n "seccompiler") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "0zh1i5m8r31p7l95xnl3ywlprbfym2v5af75ycpgx85kqbiparbg") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-seccompiler-0.4.0 (c (n "seccompiler") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (o #t) (d #t) (k 0)))) (h "1rg2nr757r4p6g0zd1k0y5dqy2kc7jw9filx11wa88gpvm6kwnil") (f (quote (("json" "serde" "serde_json"))))))

