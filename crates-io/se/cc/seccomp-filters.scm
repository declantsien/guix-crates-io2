(define-module (crates-io se cc seccomp-filters) #:use-module (crates-io))

(define-public crate-seccomp-filters-0.1.0 (c (n "seccomp-filters") (v "0.1.0") (h "19k5kgx39q9kyicfs24p67xb35ywi4r7qhcl5jj78wnjzpv9jx6m")))

(define-public crate-seccomp-filters-0.1.1 (c (n "seccomp-filters") (v "0.1.1") (h "1x0cd0b1hp021jg5cbdgb5ijmh6zm4l6x89gg8slr6fgakww3ldg")))

