(define-module (crates-io se cc seccomp-droundy-sys) #:use-module (crates-io))

(define-public crate-seccomp-droundy-sys-0.1.2 (c (n "seccomp-droundy-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "121l6xy5vh24dv8nvj1p6sm156m8dbz82dn2qswxhb7j9z67c290")))

