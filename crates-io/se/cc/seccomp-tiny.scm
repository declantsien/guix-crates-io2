(define-module (crates-io se cc seccomp-tiny) #:use-module (crates-io))

(define-public crate-seccomp-tiny-0.1.0 (c (n "seccomp-tiny") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1.36") (f (quote ("compiler-builtins" "mem"))) (d #t) (k 2)) (d (n "memoffset") (r "^0.5.6") (d #t) (k 2)) (d (n "sc") (r "^0.2.3") (d #t) (k 0)))) (h "02rkn70z88qghhn59fzi1g228zidgk138zi2mkyyxzjbgk2lj5s7")))

(define-public crate-seccomp-tiny-0.1.1 (c (n "seccomp-tiny") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1.36") (f (quote ("compiler-builtins" "mem"))) (d #t) (k 2)) (d (n "memoffset") (r "^0.5.6") (d #t) (k 2)) (d (n "sc") (r "^0.2.3") (d #t) (k 0)))) (h "02kyllzi98sinss8vd47sd9hxw2bxq1lnqrc2xi2vm0nrc4macw7")))

