(define-module (crates-io se cc seccomp) #:use-module (crates-io))

(define-public crate-seccomp-0.1.0 (c (n "seccomp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0q66rb410ff6iwd91pa3i16k6n0b05820biy1875pnmld244zgvh")))

(define-public crate-seccomp-0.1.1 (c (n "seccomp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "173bw9zzalxvqwf0bd8xbqly01250m2rl739nzx71a1z6zxqjpq7")))

(define-public crate-seccomp-0.1.2 (c (n "seccomp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0nrixpxgz0b5nji924q5w0dcsmccd3x1nh9wxbd6ypnifi2dv8vc")))

