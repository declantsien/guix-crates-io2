(define-module (crates-io se cc seccomp-sys) #:use-module (crates-io))

(define-public crate-seccomp-sys-0.1.0 (c (n "seccomp-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0byadsriksy25znbgsgnldhps6b56n8kmpximhm73q95a2b0a2lw")))

(define-public crate-seccomp-sys-0.1.1 (c (n "seccomp-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kbp3nkypgkm7938vdhrpw9g91li6b8dg7gh5r3w0m272y17iw7a")))

(define-public crate-seccomp-sys-0.1.2 (c (n "seccomp-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ylxk4ihmhm4n7gc5iiasygr4p56w5bdqy5zvn0z4p6j22qq4h0d")))

(define-public crate-seccomp-sys-0.1.3 (c (n "seccomp-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rkbiq1vd5d3cc4w5ql05pj1vbjqfi7zir02szwfm2khl15zgg75")))

