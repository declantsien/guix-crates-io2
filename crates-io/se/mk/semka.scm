(define-module (crates-io se mk semka) #:use-module (crates-io))

(define-public crate-semka-1.0.0 (c (n "semka") (v "1.0.0") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "0xvbbz1bm12dlzpwj6h91r2ynv0k2j6nk6qqc345khr8s7qhv01f") (y #t)))

(define-public crate-semka-1.0.1 (c (n "semka") (v "1.0.1") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "09p2zj08lshpx8i9184jaw9y8897ibv9bn9z28l2smlpq84y87k9") (y #t)))

(define-public crate-semka-2.0.0 (c (n "semka") (v "2.0.0") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1p2z4lzw3d7vbhg6v0gajpv12ihwlrcwykzfx2h3083n6zxq473a") (y #t)))

(define-public crate-semka-2.0.1 (c (n "semka") (v "2.0.1") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "068dd2s1l9gp22pxvq77zb6ii793pg76bamjc51i9v71hr4srl4a") (y #t)))

(define-public crate-semka-2.0.2 (c (n "semka") (v "2.0.2") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "0p5s2yxqdmmbrssay54m3lqpxq49c6mzkcf0cvs0ax9h8vir1qj3") (y #t)))

(define-public crate-semka-2.0.3 (c (n "semka") (v "2.0.3") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "0l9rj3v6gljg2l341qnxg1v13qcrqp8ixnc715bbbp3bjmyck4kx")))

(define-public crate-semka-2.0.4 (c (n "semka") (v "2.0.4") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1fa6b7cxjz252plbi55vfqhbc6c7r366gg12afl32xp1f5vp04gh")))

(define-public crate-semka-2.0.5 (c (n "semka") (v "2.0.5") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1wssymy3ma0rv548qcxa4b7d2vbwa5xihs338xby0mj1dpzg1v1s") (y #t)))

(define-public crate-semka-2.0.6 (c (n "semka") (v "2.0.6") (d (list (d (n "error-code") (r "^2") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1bcav3rz65clzd07vqmdyydiy315yzispn8n6278s0apy9jpdpcb")))

(define-public crate-semka-2.0.7 (c (n "semka") (v "2.0.7") (d (list (d (n "error-code") (r "^3") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "1swkgy7a7jaz83gk11qqaxy44bvlyny4v63ym23q89dikajknpn3")))

