(define-module (crates-io se r_ ser_raw_derive_serializer) #:use-module (crates-io))

(define-public crate-ser_raw_derive_serializer-0.0.1 (c (n "ser_raw_derive_serializer") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x3br3c0fbv8pk5fbivaa5mlwnhqpx14k2ha5kifqwwncl1n1fg4")))

(define-public crate-ser_raw_derive_serializer-0.1.0 (c (n "ser_raw_derive_serializer") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1abz5pbp2b9vd5z3l436xlsm5v6bpib3scqwajk33cjd2vanznip")))

