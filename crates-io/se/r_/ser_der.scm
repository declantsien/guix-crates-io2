(define-module (crates-io se r_ ser_der) #:use-module (crates-io))

(define-public crate-ser_der-0.0.0 (c (n "ser_der") (v "0.0.0") (d (list (d (n "der") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05f920lycppkhqppgfwqa3x1klsz9q6mgbh6ff1l8z8d8wf8crba") (f (quote (("std" "alloc") ("real" "der/real") ("pem" "der/pem") ("oid" "der/oid") ("default") ("bytes" "der/bytes") ("arbitrary" "der/arbitrary") ("alloc" "der/alloc"))))))

(define-public crate-ser_der-0.1.0-alpha.1 (c (n "ser_der") (v "0.1.0-alpha.1") (d (list (d (n "der") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1ayarkziizvqkjzazfi2ga0jaih963rybdkm0cxwa372ds8na2w2") (f (quote (("std" "alloc") ("real" "der/real") ("pem" "der/pem") ("oid" "der/oid") ("default") ("bytes" "der/bytes") ("arbitrary" "der/arbitrary") ("alloc" "der/alloc"))))))

