(define-module (crates-io se r_ ser_raw) #:use-module (crates-io))

(define-public crate-ser_raw-0.0.1 (c (n "ser_raw") (v "0.0.1") (d (list (d (n "ser_raw_derive") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "ser_raw_derive_serializer") (r "^0.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0gf1hfsyd4acmrx7g36kiw449kldldz3fxd6b9ffydd63qw48ihi") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ser_raw_derive"))))))

(define-public crate-ser_raw-0.1.0 (c (n "ser_raw") (v "0.1.0") (d (list (d (n "ser_raw_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ser_raw_derive_serializer") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "09d5wdkzzaglx97h1m2ngakcfifm2a2ql3wxnah52pim12bxai11") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ser_raw_derive"))))))

