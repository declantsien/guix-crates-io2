(define-module (crates-io se ca secapi) #:use-module (crates-io))

(define-public crate-secapi-0.1.0 (c (n "secapi") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "secapi-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "1i7sln79d03c5dzfa1gab4rj4zr3wz63sqwja0bvcmf5k33yg6m7") (f (quote (("system-sa-client" "secapi-sys/system-sa-client"))))))

