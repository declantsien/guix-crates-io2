(define-module (crates-io se ca secapi-sys) #:use-module (crates-io))

(define-public crate-secapi-sys-0.1.0 (c (n "secapi-sys") (v "0.1.0") (d (list (d (n "copy_dir") (r "^0.1.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "102zk03g61hk4jsw0gl70hrm276r72zg7jxsll48grch1801vyg2") (f (quote (("system-sa-client") ("default"))))))

