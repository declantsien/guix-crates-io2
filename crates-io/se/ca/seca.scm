(define-module (crates-io se ca seca) #:use-module (crates-io))

(define-public crate-seca-0.1.0 (c (n "seca") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1vzbifn9nplqn68zl3rrpyz6mn2fxq9i0cssc3xmlqq7nrjd3fv9") (y #t)))

(define-public crate-seca-1.0.0 (c (n "seca") (v "1.0.0") (h "049v02h9am118dnwjfb1yv07jwsfksclsk0ddvcf28cdqx6sd4n9") (y #t)))

(define-public crate-seca-1.1.0 (c (n "seca") (v "1.1.0") (h "0ws1w0gx2nmrlg6s7g7j5fi6nrj5dvbini068fzl09xzwmvrz2wn") (y #t)))

