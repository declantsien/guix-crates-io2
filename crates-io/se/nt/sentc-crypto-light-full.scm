(define-module (crates-io se nt sentc-crypto-light-full) #:use-module (crates-io))

(define-public crate-sentc-crypto-light-full-0.10.0 (c (n "sentc-crypto-light-full") (v "0.10.0") (d (list (d (n "sentc-crypto-common") (r "^0.10.0") (d #t) (k 0)) (d (n "sentc-crypto-light") (r "^0.10.0") (k 0)) (d (n "sentc-crypto-utils") (r "^0.10.0") (f (quote ("crypto_full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0w9ba2hs4djakcgpxjb77a1hn7r82vkjvwndjk2cgfwhblkfai3k") (f (quote (("wasm" "sentc-crypto-utils/wasm") ("rustls" "sentc-crypto-utils/rustls") ("rust" "sentc-crypto-light/rust") ("default" "sentc-crypto-light/default" "rustls"))))))

