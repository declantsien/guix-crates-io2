(define-module (crates-io se nt sentry-contrib-breakpad) #:use-module (crates-io))

(define-public crate-sentry-contrib-breakpad-0.1.0 (c (n "sentry-contrib-breakpad") (v "0.1.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "sentry-core") (r "^0.23") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d6zk4zc8bk69h16bs0519p4kvmcmb252wgh6sv7sdkz36lzvgbz") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.2.0 (c (n "sentry-contrib-breakpad") (v "0.2.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bx55mvs24i03ymcx0ikr17yv49l1djg1501q8qq7xbj2izrq7mj") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.3.0 (c (n "sentry-contrib-breakpad") (v "0.3.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.25") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vfrk4f1kf0kknz29kid7z0lmxflxvgshr9paq8c82l0xb8p3vnc") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.3.1 (c (n "sentry-contrib-breakpad") (v "0.3.1") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jj23b6r90pm4c86dm0mv4icdjgilnzz19ipck6mbzqwgfc834rr") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.4.0 (c (n "sentry-contrib-breakpad") (v "0.4.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a6y58ckcpyc19w4n1gmiji7n3d4flhig9r65ca0qj1gzqm37dc9") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.5.0 (c (n "sentry-contrib-breakpad") (v "0.5.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.27") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mdgd5cz92916idv4x2ksb5bkn23ryfk1p89fmix5ahsxfflaxfw") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.6.0 (c (n "sentry-contrib-breakpad") (v "0.6.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.28") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03lj3rwhgyb1qs4ybf2lvqh47ca78x8343jywf4dn3qjkh75p4b7") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.7.0 (c (n "sentry-contrib-breakpad") (v "0.7.0") (d (list (d (n "breakpad-handler") (r "^0.1.0") (d #t) (k 0)) (d (n "sentry-core") (r ">=0.29") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04cihl3rbllicmbhmfx1rh71nggyvk9fr2sxcmzms70sg3gq1y77") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.8.0 (c (n "sentry-contrib-breakpad") (v "0.8.0") (d (list (d (n "breakpad-handler") (r "^0.2.0") (d #t) (k 0)) (d (n "sentry-core") (r ">=0.29") (f (quote ("client"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a86qzpz5hjf749nzpx7y8dwnr984zp3m1l2bdgfd4zxgp3jkj69") (f (quote (("default") ("debug-logs")))) (y #t)))

(define-public crate-sentry-contrib-breakpad-0.8.1 (c (n "sentry-contrib-breakpad") (v "0.8.1") (d (list (d (n "breakpad-handler") (r "^0.2.0") (d #t) (k 0)) (d (n "sentry-core") (r ">=0.29, <=0.31.6") (f (quote ("client"))) (d #t) (k 0)) (d (n "sentry-types") (r ">=0.29, <=0.31.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17cafll0r3qc89f7xp18fymq4zzl7y17dg8yqpwab3lj9pi55ylh") (f (quote (("default") ("debug-logs")))) (y #t)))

(define-public crate-sentry-contrib-breakpad-0.8.2 (c (n "sentry-contrib-breakpad") (v "0.8.2") (d (list (d (n "breakpad-handler") (r "^0.2.0") (d #t) (k 0)) (d (n "sentry-core") (r ">=0.29, <=0.31.6") (f (quote ("client"))) (d #t) (k 0)) (d (n "sentry-types") (r "=0.31.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "049y0wan44p5zcfkwz91852jd36j9qg2jfzgdd03a71dhqf3lxxf") (f (quote (("default") ("debug-logs"))))))

(define-public crate-sentry-contrib-breakpad-0.9.0 (c (n "sentry-contrib-breakpad") (v "0.9.0") (d (list (d (n "breakpad-handler") (r "^0.2.0") (d #t) (k 0)) (d (n "sentry-core") (r ">=0.31.7") (f (quote ("client"))) (d #t) (k 0)) (d (n "sentry-types") (r ">=0.31.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11aljb5c2wf7q4rc2r3z0l51im5f6lqzh496b3831mnicnpa91vk") (f (quote (("default") ("debug-logs"))))))

