(define-module (crates-io se nt sentry-log4rs) #:use-module (crates-io))

(define-public crate-sentry-log4rs-0.1.0 (c (n "sentry-log4rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "sentry") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "1hyc09y1f6naz02kd1xaygdq5k2csjzi83a10bx02kis8wklmqvb")))

(define-public crate-sentry-log4rs-0.1.1 (c (n "sentry-log4rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "sentry") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "0dqk5l9aanlkcc5gs2gc0m9mjbl6m5048agcvzn1zx6fla4hdmw0")))

(define-public crate-sentry-log4rs-0.1.2 (c (n "sentry-log4rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.0.0") (d #t) (k 0)) (d (n "sentry") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "1fjs5ig2mxmv4k7w3pjnfc78jzhsns8pk42ghzyhvih43y21q1i9")))

(define-public crate-sentry-log4rs-0.1.3 (c (n "sentry-log4rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "sentry") (r "^0.29.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "0g5lidax6hn7rjl9cyqv30jv6wy0vnl1w0y87m4k63qg92fsjhaf")))

