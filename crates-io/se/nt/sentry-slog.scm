(define-module (crates-io se nt sentry-slog) #:use-module (crates-io))

(define-public crate-sentry-slog-0.19.0 (c (n "sentry-slog") (v "0.19.0") (d (list (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)))) (h "0m3xm6bavagab494i0wizja6r9ndzsvmnncxfsvhk8d8bhnfmpd4")))

(define-public crate-sentry-slog-0.19.1 (c (n "sentry-slog") (v "0.19.1") (d (list (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)))) (h "0lv2wldkqz51kplyk10jn2jpc6ksinmk2imixpiyr1ynhqbwhc2n")))

(define-public crate-sentry-slog-0.20.0 (c (n "sentry-slog") (v "0.20.0") (d (list (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)))) (h "0ndy4zd0shibdkydjjrs3qzi7hqg2xlhnr9aav2fyq07zc8va36l")))

(define-public crate-sentry-slog-0.20.1 (c (n "sentry-slog") (v "0.20.1") (d (list (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)))) (h "1f465qkdjhk57zwdxm7nk87y73fg0c703rz4cbdz2d6lr1cf59lq")))

(define-public crate-sentry-slog-0.21.0 (c (n "sentry-slog") (v "0.21.0") (d (list (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)))) (h "1byc2i1hnj0pw04rdqkjfr1jrhhp5081z35cjgizsa35xs76x33c")))

(define-public crate-sentry-slog-0.22.0 (c (n "sentry-slog") (v "0.22.0") (d (list (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0vp9ygbljsgndpqwkisrx38a2i6a6d5xgcnlxx579nq0cj5ih6da")))

(define-public crate-sentry-slog-0.23.0 (c (n "sentry-slog") (v "0.23.0") (d (list (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "02lbv6g4zj4mfzvri4n96nap5z8sm4ak107179yzbdfys9v68qrk")))

(define-public crate-sentry-slog-0.24.0 (c (n "sentry-slog") (v "0.24.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1c2gxflgw429l0rp24pbb9a8q8zv047vrcgsm56fh43par6lykqs")))

(define-public crate-sentry-slog-0.24.1 (c (n "sentry-slog") (v "0.24.1") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1j7c9mr8p2pn1d82bb0spmm1q3x8w1l0lhzj0ja98mdzy0n2zq8r")))

(define-public crate-sentry-slog-0.24.2 (c (n "sentry-slog") (v "0.24.2") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0zz2sm8wd0h07xcqdlbrnjfi9gqxg87aylnc4cddl1yrfvp4f781")))

(define-public crate-sentry-slog-0.24.3 (c (n "sentry-slog") (v "0.24.3") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1crd149jghlabbqk3msfadgjjdvmprm0zh08294fchlw44qj88aa")))

(define-public crate-sentry-slog-0.25.0 (c (n "sentry-slog") (v "0.25.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1c5b2zzash505bxh625z51ra5822m7d60x6p8whdy17mh8smsm8i")))

(define-public crate-sentry-slog-0.26.0 (c (n "sentry-slog") (v "0.26.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0395akldzqi0mjqhcp0wnn7n1fz2qsz9g6cmjy6kiiml66ppr2pm")))

(define-public crate-sentry-slog-0.27.0 (c (n "sentry-slog") (v "0.27.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "07m3hnc2y450s9ql1y5d5sc46cfkai1jnlqc7fkjdnq8bxn48mgq") (r "1.57")))

(define-public crate-sentry-slog-0.28.0 (c (n "sentry-slog") (v "0.28.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1yl7jmnr9nb82iiwza3x96n7zgaifc65dpy5pjyi9qanx5nvvwwg") (r "1.60")))

(define-public crate-sentry-slog-0.29.0 (c (n "sentry-slog") (v "0.29.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0i3mch6615xqfb2wvgcd3n5i4jbg18gwlacn8zhvrm2gbb2z2cn6") (r "1.60")))

(define-public crate-sentry-slog-0.29.1 (c (n "sentry-slog") (v "0.29.1") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "099yzz0ixkg1j3z5v7nbpjnfi8yv017pr9lwa8ilgvilbqciz4f5") (r "1.60")))

(define-public crate-sentry-slog-0.29.2 (c (n "sentry-slog") (v "0.29.2") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1dd1mwgbwxpd662wds9yikh45ascrj1532ggmvfqrhkdny487nw5") (r "1.60")))

(define-public crate-sentry-slog-0.29.3 (c (n "sentry-slog") (v "0.29.3") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1490k9b66c23p6znb28dnmrr09vmg2zfrib6z4lqfw83h334xr6i") (r "1.60")))

(define-public crate-sentry-slog-0.30.0 (c (n "sentry-slog") (v "0.30.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "130xy1cvizx2g950ncas2x52s49fswn86397jh8i6zbyy09f8705") (r "1.66")))

(define-public crate-sentry-slog-0.31.0 (c (n "sentry-slog") (v "0.31.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1mmd2ll3zdv126av03b1gld90x56byclarvg4zdzhajjwvw23za0") (r "1.66")))

(define-public crate-sentry-slog-0.31.1 (c (n "sentry-slog") (v "0.31.1") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "04wljg0ksqyldzws5nvp1bqwl96zcy1qib3rajlp5qpjwqhnsdlx") (r "1.66")))

(define-public crate-sentry-slog-0.31.2 (c (n "sentry-slog") (v "0.31.2") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0cp6rbs8126gamfgpwwb59jspwkhpp1yg4xizlgigsws64srpij5") (r "1.66")))

(define-public crate-sentry-slog-0.31.3 (c (n "sentry-slog") (v "0.31.3") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0j9c1lrchwhww1lz7bbbdz2mwkyj5i559y70gfsbwwwcy4x9mgry") (r "1.66")))

(define-public crate-sentry-slog-0.31.4 (c (n "sentry-slog") (v "0.31.4") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1q0zr767v6afjml7rpg35fccrisxkq309xyh23qaws8cjiz1wlx5") (r "1.66")))

(define-public crate-sentry-slog-0.31.5 (c (n "sentry-slog") (v "0.31.5") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "05pm9hq7n53874rq576qjpw0q2q8nlaq1kg5rjmjz8jvhisr9hax") (r "1.66")))

(define-public crate-sentry-slog-0.31.6 (c (n "sentry-slog") (v "0.31.6") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "12kb8vyh21cd19gmbpkk5386w2l8dhx98jbqzjhgj0hcfnd5kc4j") (r "1.67")))

(define-public crate-sentry-slog-0.31.7 (c (n "sentry-slog") (v "0.31.7") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1hj484hm6kd34wxmy0vaj165ahy82yangylqq9ixnx3zk10bllx6") (r "1.68")))

(define-public crate-sentry-slog-0.31.8 (c (n "sentry-slog") (v "0.31.8") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "12zy6s7m1sgbi5xhvcfpmznylvyrxh20ig07mglrbkzqa3h8iyli") (r "1.68")))

(define-public crate-sentry-slog-0.32.0 (c (n "sentry-slog") (v "0.32.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1bhsvbwvrn74jsj87ic2nz5qy45cz65684ssj4kii3il6hf8aqs7") (r "1.68")))

(define-public crate-sentry-slog-0.32.1 (c (n "sentry-slog") (v "0.32.1") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0apihvgbhgwdr09azv69cgca2pi9vag1a7j85gcliqn1dszxnsmi") (r "1.68")))

(define-public crate-sentry-slog-0.32.2 (c (n "sentry-slog") (v "0.32.2") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "1asj03mfr3n4cycvws6hfgrnpxwlqq4iysz33cyycdk7qbh94dl2") (r "1.68")))

(define-public crate-sentry-slog-0.32.3 (c (n "sentry-slog") (v "0.32.3") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "0h4zpsd2q53jxqlfl358bvqp87i6xbaw9x2644svnzfz7h00gx9y") (r "1.73")))

(define-public crate-sentry-slog-0.33.0 (c (n "sentry-slog") (v "0.33.0") (d (list (d (n "erased-serde") (r "^0.3.12") (d #t) (k 2)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("nested-values"))) (d #t) (k 0)))) (h "19pziakbpanj2vyi867b5lf953v17ql2a07yp6492hj2h8w3nmwk") (r "1.73")))

