(define-module (crates-io se nt sentence2vec) #:use-module (crates-io))

(define-public crate-sentence2vec-0.1.0 (c (n "sentence2vec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "19bgdrqlgka9ykvxvb7i0pgvdqw3zgkyz4lswg386z3ahgm7kb8f") (f (quote (("partition" "log" "env_logger" "loading") ("loading" "bincode" "serde_bytes" "serde"))))))

