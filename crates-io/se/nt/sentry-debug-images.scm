(define-module (crates-io se nt sentry-debug-images) #:use-module (crates-io))

(define-public crate-sentry-debug-images-0.19.0 (c (n "sentry-debug-images") (v "0.19.0") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "06vhg7r0m61a6hp0hn42g25kbj0irb330xch04v26spssp3xsnmm")))

(define-public crate-sentry-debug-images-0.19.1 (c (n "sentry-debug-images") (v "0.19.1") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "026x8fv1dgxf5ffl65npfrfydr8mjp2ap25snia7z4s202xc3sj9")))

(define-public crate-sentry-debug-images-0.20.0 (c (n "sentry-debug-images") (v "0.20.0") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "1bys752xpifn72y28mkkmvar9yp261wrj8bp9jvgg409844a62cn")))

(define-public crate-sentry-debug-images-0.20.1 (c (n "sentry-debug-images") (v "0.20.1") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "0spchx4qsxnip11766pvk0qh5arb5sllkk0ifx5fb9i2g0mmyg9z")))

(define-public crate-sentry-debug-images-0.21.0 (c (n "sentry-debug-images") (v "0.21.0") (d (list (d (n "findshlibs") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "08igiqgdh0by8jpbl4if9azz7rd045xgbk144k92nwgrkyqlfbk3")))

(define-public crate-sentry-debug-images-0.22.0 (c (n "sentry-debug-images") (v "0.22.0") (d (list (d (n "findshlibs") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)))) (h "0migld3my0frxz3pr3ij5vd2z4h6a9qakpfnq11f18li0p55x79j")))

(define-public crate-sentry-debug-images-0.23.0 (c (n "sentry-debug-images") (v "0.23.0") (d (list (d (n "findshlibs") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)))) (h "0hhc8g8gfri56hjril0mjcbj64g0svhl930bbiqgnflamaq54i87")))

(define-public crate-sentry-debug-images-0.24.0 (c (n "sentry-debug-images") (v "0.24.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.0") (d #t) (k 0)))) (h "1bzffykwdmf481f4ws93snhqnj6lhvy2l51qz11lr8m367011qmf")))

(define-public crate-sentry-debug-images-0.24.1 (c (n "sentry-debug-images") (v "0.24.1") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)))) (h "18b3qkzn2hm8vh0jndhzfcca3b4v9y7igajm0ip53i2302rnzzm7")))

(define-public crate-sentry-debug-images-0.24.2 (c (n "sentry-debug-images") (v "0.24.2") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)))) (h "181v0adsr42vwli532c2ihhnz9v882xamvg4g37z5k54bycxnny8")))

(define-public crate-sentry-debug-images-0.24.3 (c (n "sentry-debug-images") (v "0.24.3") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)))) (h "1b360hz0s08k66vc1m5bc9zvknd5k049qwkqlq60hk0kj2jwfdpa")))

(define-public crate-sentry-debug-images-0.25.0 (c (n "sentry-debug-images") (v "0.25.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)))) (h "0zgbbijxcm8s5lvaagapvwn3jza0z8rzsd76pmkm2w0qg7wh1ixl")))

(define-public crate-sentry-debug-images-0.26.0 (c (n "sentry-debug-images") (v "0.26.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)))) (h "1ly0lyajifqd9fj28ldlmhvw3smxcc4biw56j89whll56w4qi8mh")))

(define-public crate-sentry-debug-images-0.27.0 (c (n "sentry-debug-images") (v "0.27.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)))) (h "1n0nwfs9rbndq8fmkyafj4gnq95yjqh9aic4449w5mrqzrzbpz06") (r "1.57")))

(define-public crate-sentry-debug-images-0.28.0 (c (n "sentry-debug-images") (v "0.28.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)))) (h "0s9h0lnf75pif7gdmldv448wxzqgzbcssrh5qjv305rak764y4an") (r "1.60")))

(define-public crate-sentry-debug-images-0.29.0 (c (n "sentry-debug-images") (v "0.29.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)))) (h "1drgf1nphbd1bzg8ln84xlml6glaqrz1glvqwkwmshvdqqgfaqrz") (r "1.60")))

(define-public crate-sentry-debug-images-0.29.1 (c (n "sentry-debug-images") (v "0.29.1") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)))) (h "1cs9jxwc3kbs65dxi1a1nqxmaxv6scmz8ix2snvl852p6n0j159r") (r "1.60")))

(define-public crate-sentry-debug-images-0.29.2 (c (n "sentry-debug-images") (v "0.29.2") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)))) (h "14lqa8rq3p88wbymnar6ssdkrhg711i001lxvj77y9k4yyfhjvpp") (r "1.60")))

(define-public crate-sentry-debug-images-0.29.3 (c (n "sentry-debug-images") (v "0.29.3") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)))) (h "0z7hhy33712c4z78hz5zv9dvjvpcl4gi1735wcfkcr1sip3mhlvl") (r "1.60")))

(define-public crate-sentry-debug-images-0.30.0 (c (n "sentry-debug-images") (v "0.30.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)))) (h "0g9aj4ykffa6i19clwp6786vf515an3pxz8acyvv2a999ba6948a") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.0 (c (n "sentry-debug-images") (v "0.31.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)))) (h "133ac6gizap33bw1r1i4fi3xip0j9aixz0594isdjb7f645h6n77") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.1 (c (n "sentry-debug-images") (v "0.31.1") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)))) (h "0y6605y6zbdjhkh08w0d7fpl6d6bjyp06ifpcdakygd2w2dyk1j8") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.2 (c (n "sentry-debug-images") (v "0.31.2") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)))) (h "06nydcacqwc76d6lbpq1izc8ns1dxwc941wjnx0in99xsjvbkpd8") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.3 (c (n "sentry-debug-images") (v "0.31.3") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)))) (h "1m1av1qs69pf01df3yl6dskrsdldlha2w78ibzsnypi53f52ii2k") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.4 (c (n "sentry-debug-images") (v "0.31.4") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)))) (h "0mz190wldz7f3272k19hgx2nxnxq58xzy44mhfgpk7s0m76n155y") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.5 (c (n "sentry-debug-images") (v "0.31.5") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "0phgqyhjrq6fr4vhfa1y11na1kz8hfqq0h4rfqmkpah8zzbh6l3a") (r "1.66")))

(define-public crate-sentry-debug-images-0.31.6 (c (n "sentry-debug-images") (v "0.31.6") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)))) (h "1j4w68gxx6mynzgq2w5j5np64r102gsn0blal6bn9c09p39ff48w") (r "1.67")))

(define-public crate-sentry-debug-images-0.31.7 (c (n "sentry-debug-images") (v "0.31.7") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)))) (h "1p81wklzlm1ig8mnmbby6p3r3rjxzd7hiw698awhpm34ll7iirig") (r "1.68")))

(define-public crate-sentry-debug-images-0.31.8 (c (n "sentry-debug-images") (v "0.31.8") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)))) (h "0a6377x4g866wfdwlfjmylpv4qbmbd2ys8k0dya3jbyjfcz2dnxg") (r "1.68")))

(define-public crate-sentry-debug-images-0.32.0 (c (n "sentry-debug-images") (v "0.32.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)))) (h "0hfl9wll3pqbcifmylqljgrmin0hq9lcqwjkq8z2m2cp8chh5k8b") (r "1.68")))

(define-public crate-sentry-debug-images-0.32.1 (c (n "sentry-debug-images") (v "0.32.1") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)))) (h "0rnaqrl428w0gifg0bwg9s09lzlazbs6p054aj77m7jxswha9g6q") (r "1.68")))

(define-public crate-sentry-debug-images-0.32.2 (c (n "sentry-debug-images") (v "0.32.2") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)))) (h "19qi85a0liih23m8dd7n9k0h8kh9k88hqqihj1lsbv2ybrhhlkbv") (r "1.68")))

(define-public crate-sentry-debug-images-0.32.3 (c (n "sentry-debug-images") (v "0.32.3") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)))) (h "1s4d3k3pv3yq0z5gzi63cdl65sd51mhxlj35nr542nb56f3a9hky") (r "1.73")))

(define-public crate-sentry-debug-images-0.33.0 (c (n "sentry-debug-images") (v "0.33.0") (d (list (d (n "findshlibs") (r "=0.10.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)))) (h "1xybz6y8zzva6dkxh3jrg5cq2cx8q2nwdq0dg5w8bfjxls9ydqrb") (r "1.73")))

