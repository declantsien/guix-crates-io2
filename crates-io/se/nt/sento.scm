(define-module (crates-io se nt sento) #:use-module (crates-io))

(define-public crate-sento-0.1.0 (c (n "sento") (v "0.1.0") (d (list (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1ixsnwcyc7yxxv29sn7g8170b2919kab0mnkbacjllqd9c6gkrsx") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-sento-0.1.1 (c (n "sento") (v "0.1.1") (d (list (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1lp1vaq81aibp15qxbds3lnjxrc1sa99s7nhhnfkkinbn4hdkgm6") (f (quote (("std") ("default" "std"))))))

