(define-module (crates-io se nt sentential) #:use-module (crates-io))

(define-public crate-sentential-0.1.0 (c (n "sentential") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06whqj8x547q3j6iznppzvkcfz0p72i97kpz6j72nbq7jv51p3cc") (y #t)))

(define-public crate-sentential-0.2.0 (c (n "sentential") (v "0.2.0") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w44zzr2zibwjkinfiwby95h2naah2hbnr73z74fw0p63qyriv5a")))

(define-public crate-sentential-0.2.3 (c (n "sentential") (v "0.2.3") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0asza2sbr4fmw7qmh8dx0cvq1p41dn7zsyaw41gxlyqn413ajl7a")))

(define-public crate-sentential-0.2.4 (c (n "sentential") (v "0.2.4") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14rchm82xy62fxh269z09p9lwi31i6641269yw5vrw62kybfxdwc")))

(define-public crate-sentential-0.2.5 (c (n "sentential") (v "0.2.5") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d5bqkanl68v1c8wz6fqv32kp3m94ff6yy28qgzpglad2dz9ajdp")))

(define-public crate-sentential-0.2.6 (c (n "sentential") (v "0.2.6") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hd8a02mshxmwqjnly7qsprf3hhik2la2qik4wx3g83g8amxbzgc")))

(define-public crate-sentential-0.3.0 (c (n "sentential") (v "0.3.0") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "008y4r1h8jl8dsqk4ais93ldz81dahzbmaii5pfqxc79063qs4np")))

(define-public crate-sentential-0.4.0 (c (n "sentential") (v "0.4.0") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aia63qlfj0842vd0xrpadzplrpzj97y0ip0nvi887q7vi6bdk3c")))

(define-public crate-sentential-0.4.1 (c (n "sentential") (v "0.4.1") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0szmj8br0gqz3axviwba2kd38z5f7bnzadnijjr3y1kgc05c0kp7")))

(define-public crate-sentential-0.4.2 (c (n "sentential") (v "0.4.2") (d (list (d (n "aws-config") (r "^0.51.0") (d #t) (k 0)) (d (n "aws-sdk-ssm") (r "^0.21.0") (d #t) (k 0)) (d (n "aws-smithy-http") (r "^0.51.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wfa5ygn0srwgd3afmjj7pr6xqjm2bdkj37a5bd4y1x9q8wi3zgp")))

