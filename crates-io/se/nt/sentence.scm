(define-module (crates-io se nt sentence) #:use-module (crates-io))

(define-public crate-sentence-0.0.1 (c (n "sentence") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "1608ds9a5k53760n978x6p7r2zkpaxy7cl68qhz8gp073plfr00n")))

(define-public crate-sentence-0.0.2 (c (n "sentence") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "0xmwwr01icx3mxyr0ir9jnl577vyp2id5kkga2ffdmngv0bk31d5")))

