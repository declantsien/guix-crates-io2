(define-module (crates-io se nt sentry-eyre) #:use-module (crates-io))

(define-public crate-sentry-eyre-0.1.0 (c (n "sentry-eyre") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.5") (o #t) (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "02vh9nzzq9m5hb21f29axhx02s9h8ndwmvckmiibxnyyzdni3xqv") (f (quote (("default" "backtrace")))) (s 2) (e (quote (("backtrace" "dep:sentry-backtrace"))))))

