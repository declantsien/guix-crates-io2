(define-module (crates-io se nt sentinel-actix) #:use-module (crates-io))

(define-public crate-sentinel-actix-0.1.0 (c (n "sentinel-actix") (v "0.1.0") (d (list (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "sentinel-core") (r "^0.1") (f (quote ("async"))) (d #t) (k 0)))) (h "0a8ghxlxw370hrfljsgb88l9gic8m2mk72hdkggn4jxyc190qm5g")))

