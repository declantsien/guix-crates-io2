(define-module (crates-io se nt sentry-anyhow) #:use-module (crates-io))

(define-public crate-sentry-anyhow-0.19.0 (c (n "sentry-anyhow") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "1429iz5qc8i0x3hk20p5a9fbnqlcj2sg76v24r773jy58rh184dv")))

(define-public crate-sentry-anyhow-0.19.1 (c (n "sentry-anyhow") (v "0.19.1") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "11x3wyd4cxknidhkb4h5halxlkb2zs0xm1mfijqpf08yka7aw99k")))

(define-public crate-sentry-anyhow-0.20.0 (c (n "sentry-anyhow") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "08yf559fvs58a8rab5q0axakiq7drkxdicncxnj4368psi7w3dln")))

(define-public crate-sentry-anyhow-0.20.1 (c (n "sentry-anyhow") (v "0.20.1") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "1rkk5h9cwk45j9a2lhdncj2nfrvk8zf8fkn3sffd06b26r79hsiy")))

(define-public crate-sentry-anyhow-0.21.0 (c (n "sentry-anyhow") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "19ylph2fk2x4bvs56d5j39py4q702xbkxknmp0afzz6nk84whxk1")))

(define-public crate-sentry-anyhow-0.22.0 (c (n "sentry-anyhow") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)))) (h "1pk1ccfmnwjrsb9pz39f5941l34kg05fiv3cdzkbjp29clyj28vj")))

(define-public crate-sentry-anyhow-0.23.0 (c (n "sentry-anyhow") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.23.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)))) (h "0dhccs3khdr76s3swhm436i1l11nr8yqbcxs1q9v2byafd7z13ik") (f (quote (("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.24.1 (c (n "sentry-anyhow") (v "0.24.1") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.24.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)))) (h "0gb5drcrqj0cma530dcj27843ffikw6snacrcbb26pdl5n08s5vd") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.24.2 (c (n "sentry-anyhow") (v "0.24.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.24.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)))) (h "1wnc8b6gnhzq8cwwg1z516klf3i59ksxv464nzpihqn021chwkj6") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.24.3 (c (n "sentry-anyhow") (v "0.24.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.24.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)))) (h "1dpkix6cld1n3xb00f9dbw8fk0nri1q6nvlnldd40qfqmb8jr2g2") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.25.0 (c (n "sentry-anyhow") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.25.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)))) (h "017xmssa82n0grh49nzr17akj2bcn19nfzhs0x3p4ggbcw4wz26y") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.26.0 (c (n "sentry-anyhow") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.26.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)))) (h "0migjffqkqlk3gsmhgyv51x527ncmx2y2vh90l44i26wpvv19v20") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace"))))))

(define-public crate-sentry-anyhow-0.27.0 (c (n "sentry-anyhow") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.27.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)))) (h "1a7yrlhvr1g9x0193hzkbwi7lhi3sm21jsilzm6nnfd7b28lglq8") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.57")))

(define-public crate-sentry-anyhow-0.28.0 (c (n "sentry-anyhow") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.28.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)))) (h "0dx60fvfqf54lxykna8j31sb706rdc0g745bdln9czscg2jqad0a") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.60")))

(define-public crate-sentry-anyhow-0.29.0 (c (n "sentry-anyhow") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.29.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)))) (h "09520izv2zblp57xpixar3hg0d3736zqmpmj0747wdyzzmh5194z") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.60")))

(define-public crate-sentry-anyhow-0.29.1 (c (n "sentry-anyhow") (v "0.29.1") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.29.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)))) (h "1q93wbnw7j93pxq2zx15bkqp5hy02cbcbhw2bprrl66lmb7dqnpm") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.60")))

(define-public crate-sentry-anyhow-0.29.2 (c (n "sentry-anyhow") (v "0.29.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.29.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)))) (h "1dmsdxcw2drjfxsyig8zh7g8pf01va0nba4sw9zi1xd1ks82v9a5") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.60")))

(define-public crate-sentry-anyhow-0.29.3 (c (n "sentry-anyhow") (v "0.29.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.29.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)))) (h "0nr18i55w63rxjnd35w3hpjs0dc4qdfsj8fmxvi1swbb7rk101d8") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.60")))

(define-public crate-sentry-anyhow-0.30.0 (c (n "sentry-anyhow") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.30.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)))) (h "0kc9v6a607y0nypj8apwvnc61d5wr3wcwbpb2zc14jczdl9dfa9d") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.0 (c (n "sentry-anyhow") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)))) (h "0dljahlwaqssjkvbsn48m2ah2dxsic7g4xj97dans551axyg9xwy") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.1 (c (n "sentry-anyhow") (v "0.31.1") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)))) (h "002b2pkrij3nr366awgnmm3jic9p6dnjrhq7q5vqq5zz5w1xghyr") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.2 (c (n "sentry-anyhow") (v "0.31.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)))) (h "0hjsb4kysik4x1z682f2z2sjqb4kxan2qhmcnh391ck39jz41sa7") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.3 (c (n "sentry-anyhow") (v "0.31.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)))) (h "117vln2zmzcjamhzfcb9dqd14x0c3ifl64dxsgwim0m7b0gm1wad") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.4 (c (n "sentry-anyhow") (v "0.31.4") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)))) (h "12h1yzrjcx8l35k7chklmijg08s6259p72x3z4yfidyqw5cd188c") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.5 (c (n "sentry-anyhow") (v "0.31.5") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "0i3c9bg8ikzpv6zd46s3i9b5fj7y6x49r0mg8ps2m67r08gmfflg") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.66")))

(define-public crate-sentry-anyhow-0.31.6 (c (n "sentry-anyhow") (v "0.31.6") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.6") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)))) (h "06kwlni1516v0mi64vl73nan5gilwy82h40xll834vh3xjrhzhr7") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.67")))

(define-public crate-sentry-anyhow-0.31.7 (c (n "sentry-anyhow") (v "0.31.7") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.7") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)))) (h "1icm74r0ak8iljswbfq3n4h9j16v98hslcbak4l64rqlbk6pdzf4") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.68")))

(define-public crate-sentry-anyhow-0.31.8 (c (n "sentry-anyhow") (v "0.31.8") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.31.8") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)))) (h "12xc3x1mxd1pzsmf9db2ksvfpzhs0yvg9q5pjjrq0yiza5pcls48") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.68")))

(define-public crate-sentry-anyhow-0.32.0 (c (n "sentry-anyhow") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.32.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)))) (h "1mcxbkrrlw8lm6hwk5f32aj43djvg3yhc0mfx9ickb2zbnd6p58d") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.68")))

(define-public crate-sentry-anyhow-0.32.1 (c (n "sentry-anyhow") (v "0.32.1") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.32.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)))) (h "0x80n4ix1h2v27r6p9045x6gflr0yx2axx8bpwkgmq6waa7dhhc0") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.68")))

(define-public crate-sentry-anyhow-0.32.2 (c (n "sentry-anyhow") (v "0.32.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.32.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)))) (h "1swyjp9xp8dr48lw1dyflia62bbn1jwh5zx7ikb8i7y9cxb0392d") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.68")))

(define-public crate-sentry-anyhow-0.32.3 (c (n "sentry-anyhow") (v "0.32.3") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.32.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)))) (h "0xk02by9ix19rih7sv3bjjszlqf51bv9clf5cwgdkhl5cxpcxjyx") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.73")))

(define-public crate-sentry-anyhow-0.33.0 (c (n "sentry-anyhow") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.33.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)))) (h "03q6mpj8vg9bq6j05yg1wpbmk97s71nzrdxh4h1fyimnkra2ay1w") (f (quote (("default" "backtrace") ("backtrace" "anyhow/backtrace")))) (r "1.73")))

