(define-module (crates-io se nt sentryshot_scale) #:use-module (crates-io))

(define-public crate-sentryshot_scale-0.0.1 (c (n "sentryshot_scale") (v "0.0.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0zr4nrgjgfwrz875w5vykmb47ha75lp7qvyxa1gzsq9z6a2nxzaf") (r "1.65")))

(define-public crate-sentryshot_scale-0.1.0 (c (n "sentryshot_scale") (v "0.1.0") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jhvib4g05i8l4jhr6m1mc3hk1wxgza2nnr6l87zlppm0cm7v081") (r "1.65")))

(define-public crate-sentryshot_scale-0.1.1 (c (n "sentryshot_scale") (v "0.1.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.1.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "15x75wswmpc6bznmws82m2ln35v15djjz6fbgssva64fvyb48pr2") (r "1.65")))

(define-public crate-sentryshot_scale-0.1.2 (c (n "sentryshot_scale") (v "0.1.2") (d (list (d (n "pretty-hex") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0k7g0xfr8j7ngkn57c05v44rrfz58ci2h3xcyssh9rl4mxizq3gh") (r "1.75")))

