(define-module (crates-io se nt sent_parse) #:use-module (crates-io))

(define-public crate-sent_parse-0.1.0 (c (n "sent_parse") (v "0.1.0") (h "1wgyilkcl8ckiri7xzaf3nbx62gqbyc2gwrf3zvxx8zri5a5n72k")))

(define-public crate-sent_parse-0.2.0 (c (n "sent_parse") (v "0.2.0") (h "07hvvzmys17fr3yarahjdg95yd91ajakqp5fvl70qnc3cflj0pap")))

(define-public crate-sent_parse-0.2.1 (c (n "sent_parse") (v "0.2.1") (h "1j7kf2n1m7hw6fcnpjfyvvm4msaikqj6n04ank8hsw757wiyhif2")))

