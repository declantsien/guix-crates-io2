(define-module (crates-io se nt sentinel) #:use-module (crates-io))

(define-public crate-sentinel-0.0.1 (c (n "sentinel") (v "0.0.1") (d (list (d (n "accumulator") (r "^0.0.1") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.4") (d #t) (k 0)))) (h "18f90bispbk92djkkkn73m81xcp03f4nm4kwy68pcnl58ckwk6y3")))

(define-public crate-sentinel-0.0.2 (c (n "sentinel") (v "0.0.2") (d (list (d (n "accumulator") (r "^0.0.2") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.5") (d #t) (k 0)))) (h "1vwskaj1f4wfyvgzhp07n2idfd6li9la1r89d1d0zxdd7cwa9mi3")))

(define-public crate-sentinel-0.1.0 (c (n "sentinel") (v "0.1.0") (d (list (d (n "accumulator") (r "^0.0.2") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.5") (d #t) (k 0)))) (h "08rdk8pkjyxa7rvs6my2rnl7sqa81xmrb7qqf6fy8wd5jnz91rvl")))

(define-public crate-sentinel-0.1.1 (c (n "sentinel") (v "0.1.1") (d (list (d (n "accumulator") (r "0.0.*") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "1rbfs83v91d8gpvwdr1wcwi47xjs51z7p7g69qz46mys5bhnz6vn")))

(define-public crate-sentinel-0.1.2 (c (n "sentinel") (v "0.1.2") (d (list (d (n "accumulator") (r "0.0.*") (d #t) (k 0)) (d (n "cbor") (r "*") (d #t) (k 0)) (d (n "lru_time_cache") (r "0.1.*") (d #t) (k 0)) (d (n "maidsafe_sodiumoxide") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "08ys3ha91dxldqlb5gxs3iapq53mq5fgxna2myc61cvkkqpqzzcw")))

(define-public crate-sentinel-0.2.0 (c (n "sentinel") (v "0.2.0") (d (list (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)))) (h "1hgcjyv119mqi4ggw9brw4lb7mdlnkjd91yr3y3f7baibdg8djaj") (f (quote (("null" "memchr") ("nightly") ("default" "alloc" "null") ("alloc")))) (y #t)))

(define-public crate-sentinel-0.2.1 (c (n "sentinel") (v "0.2.1") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "04v8i9ilv4v0vk6x4zxxks12kc8a449d8nclljrgk23k7vrviq5b") (f (quote (("null" "memchr") ("nightly") ("default" "alloc" "null") ("alloc"))))))

(define-public crate-sentinel-0.2.2 (c (n "sentinel") (v "0.2.2") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1cwcmd07c0zfm1ppspv1nnaxb2r3k2096gswd37cjw1r69nqiqlx") (f (quote (("null" "memchr") ("nightly") ("default" "alloc" "null") ("alloc"))))))

(define-public crate-sentinel-0.2.3 (c (n "sentinel") (v "0.2.3") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "02yq1fk1kvldjzzrwa6k6xkdslc1vcazzyn57286v7ndgdgb0m1f") (f (quote (("null" "memchr") ("nightly") ("default" "alloc" "null") ("alloc"))))))

(define-public crate-sentinel-0.3.0 (c (n "sentinel") (v "0.3.0") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0ann5zabxix8cy72gz5wgwiiw76wilzp0mp4dqqv0dj0gaf2m1jd") (f (quote (("nightly") ("default" "alloc" "cstr") ("cstr" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.3.1 (c (n "sentinel") (v "0.3.1") (d (list (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0pljr5vwr7jhqc4r1v3cfymilnlsryhz2sgyhbfha15dgmm8hlm0") (f (quote (("nightly") ("default" "alloc" "cstr") ("cstr" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.3.2 (c (n "sentinel") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1qidgpg5q1djhkwgggjpjrfanmnd62cpxh4cipfvag7zngwc0gbx") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.3.3 (c (n "sentinel") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "00vj7b4jvf37iry7nqj933immm59pp51jp2fjbf48ph7hg77swxw") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.3.4 (c (n "sentinel") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1jvld0n9jffh7ci56p6xin33i5hybhnimvdvra6111fvzcm892lr") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.3.5 (c (n "sentinel") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0is3rj6k65q1939g0jzfrc6v4r2z5q8im1i27v0yzvc059mlvwv2") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.4.0 (c (n "sentinel") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1plfjc3g7p354ryf49x7zxca1km38bjbliqxrhvflhncz1f4rhgp") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.5.0 (c (n "sentinel") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1r1zrj21g3m3rvwi245ww0j88rmx6wh2z27rqp5zxp1fzgi714h4") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.5.1 (c (n "sentinel") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1cdw76hwyplv5d88l5r0m6090wdppzwh2pa8zin08vg40v4z7yrr") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.5.2 (c (n "sentinel") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "1yncsixg5d597y7z3qfpsqg1nn9pzff6llyn7s0jp8hs6s3zx52d") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.5.3 (c (n "sentinel") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0azgbbphnqxmjdyrrw4fi4fkvz26kd8jzi7ab5ybid6vhg1bc3ck") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

(define-public crate-sentinel-0.5.4 (c (n "sentinel") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (k 0)))) (h "0b0hqxp2522imswj8krc93g9qcil87hx1xvp2fyq1k95nnf1g6hr") (f (quote (("nightly") ("default" "alloc" "memchr") ("alloc"))))))

