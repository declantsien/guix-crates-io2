(define-module (crates-io se nt sentinel-tower) #:use-module (crates-io))

(define-public crate-sentinel-tower-0.1.0 (c (n "sentinel-tower") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "sentinel-core") (r "^0.1") (f (quote ("async"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0k38f2r5d31ixs3xanrhzlx11g3fmdxdbrk6wppa6ar2w70zghnk") (f (quote (("default")))) (s 2) (e (quote (("http" "dep:http"))))))

