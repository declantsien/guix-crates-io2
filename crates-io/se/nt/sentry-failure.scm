(define-module (crates-io se nt sentry-failure) #:use-module (crates-io))

(define-public crate-sentry-failure-0.19.0 (c (n "sentry-failure") (v "0.19.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "12lg7crpa34j6f9900pcdicy6ayj12rzvs57gxizz3ymxf8xbj2z")))

(define-public crate-sentry-failure-0.19.1 (c (n "sentry-failure") (v "0.19.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "1hzf87yp2i3ggggg3kq4npw3vxbahwfkl4rscl3f2a830isx76c5")))

(define-public crate-sentry-failure-0.20.0 (c (n "sentry-failure") (v "0.20.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "0shrdd3gcrsk4ypgfkwda2adpwvnjywc7dwjsa0z264n8zxxjd95")))

(define-public crate-sentry-failure-0.20.1 (c (n "sentry-failure") (v "0.20.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "1dx8vqndp2177qf8qcbzz00924r7hk1g7cs50gnpjypp35q5xlza")))

(define-public crate-sentry-failure-0.21.0 (c (n "sentry-failure") (v "0.21.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "089h266ggg2j41bn84cnhq1d95a2lg1lvncpdlznynv4v9bhi6ah")))

