(define-module (crates-io se nt sentry-tauri) #:use-module (crates-io))

(define-public crate-sentry-tauri-0.1.0 (c (n "sentry-tauri") (v "0.1.0") (d (list (d (n "sentry") (r "^0.27") (d #t) (k 0)) (d (n "sentry-rust-minidump") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yghpj2m49wqlwyq6a06q6a0ppvlxqd1q02270glfgzkq4r22fm1")))

(define-public crate-sentry-tauri-0.2.0 (c (n "sentry-tauri") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "sentry") (r "^0.31") (d #t) (k 0)) (d (n "sentry-rust-minidump") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r3dgzvz99s5cxi0c6izj9ihv9adkkyq9idkndnj42d89gkgwsqk")))

(define-public crate-sentry-tauri-0.3.0 (c (n "sentry-tauri") (v "0.3.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "sentry") (r "^0.32") (d #t) (k 0)) (d (n "sentry-rust-minidump") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xb2phvdbpxa2mx71vqj9jdjb37rs3dyxphj1q7qk2i7fas4rrnm")))

