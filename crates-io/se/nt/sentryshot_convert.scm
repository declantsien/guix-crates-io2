(define-module (crates-io se nt sentryshot_convert) #:use-module (crates-io))

(define-public crate-sentryshot_convert-0.0.1 (c (n "sentryshot_convert") (v "0.0.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1qnwzdpdki4m478gf28bpzr84xs2nxyx7fjqrhp3bm777h6y461v") (r "1.65")))

(define-public crate-sentryshot_convert-0.1.0 (c (n "sentryshot_convert") (v "0.1.0") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19r7qbjfibdm68s1l6grkb2wkcdvjh5714bydv6jpjy8xzs7abhz") (r "1.65")))

(define-public crate-sentryshot_convert-0.1.1 (c (n "sentryshot_convert") (v "0.1.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_util") (r "^0.1.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qqpf995z5jyd0819x6dykakkrh3pwxlpzqzk08mz1gw9276xxv0") (r "1.65")))

(define-public crate-sentryshot_convert-0.1.2 (c (n "sentryshot_convert") (v "0.1.2") (d (list (d (n "pretty-hex") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_util") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0q6pqsymzi1fjzmd6r03jm3x3wn1l2887qcw456vqm7vwwn338sn") (r "1.75")))

