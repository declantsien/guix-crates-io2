(define-module (crates-io se nt sentinel-macros) #:use-module (crates-io))

(define-public crate-sentinel-macros-0.1.0 (c (n "sentinel-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "15l12qgmss0fjvzw5nk7fp20xkyf6kpbc8gh6jiymspdpil9ark7")))

