(define-module (crates-io se nt sent-driver) #:use-module (crates-io))

(define-public crate-sent-driver-0.1.0 (c (n "sent-driver") (v "0.1.0") (d (list (d (n "asm-delay") (r "^0.9.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-rtt-target") (r "^0.1.0") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 2)) (d (n "stm32f1") (r "^0.13.0") (f (quote ("stm32f103" "rt"))) (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.6.1") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 0)))) (h "1bj91q93249qr3vna8l4n9gk00bgdbjv8yf60n3jp9ky43f50xf7")))

