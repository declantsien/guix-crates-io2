(define-module (crates-io se nt sentry-log) #:use-module (crates-io))

(define-public crate-sentry-log-0.19.0 (c (n "sentry-log") (v "0.19.0") (d (list (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "1yyw54ccbg4gmv76w81xnia6kraj99fk72jgfh2qs2f19d8262c9")))

(define-public crate-sentry-log-0.19.1 (c (n "sentry-log") (v "0.19.1") (d (list (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "0cl8gypcwablxf7wfc07zjlyp4hmfd0nlrklwsv3327rqfxg4ylf")))

(define-public crate-sentry-log-0.20.0 (c (n "sentry-log") (v "0.20.0") (d (list (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "0y1yizp7844l8bsnq7b99qadnh07jq3rdsn508gv711zw2ag4wic")))

(define-public crate-sentry-log-0.20.1 (c (n "sentry-log") (v "0.20.1") (d (list (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "1sf0lapayzl1zdb02wmxdwxz22q44m587b14h41c20h7ynpwpjkf")))

(define-public crate-sentry-log-0.21.0 (c (n "sentry-log") (v "0.21.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "0hgjpw0hd79b45gzjnli9r9g4g9pw6q95blw22ykxiacygabk4dg")))

(define-public crate-sentry-log-0.22.0 (c (n "sentry-log") (v "0.22.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)))) (h "0q79q8qgrzah2kcmds67bbzrlwiriyghzm5c8brfa2j1fbv46wb4")))

(define-public crate-sentry-log-0.23.0 (c (n "sentry-log") (v "0.23.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)))) (h "0l6csgf17fw2zxzzd3qqkrrjjd7ajl3kp1i0sjq2yjkhn1cmgnk6")))

(define-public crate-sentry-log-0.24.0 (c (n "sentry-log") (v "0.24.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.0") (d #t) (k 0)))) (h "0zqlcvf10rgs0p5cr2s81ll53za99yn9adm2nj0xyhbx63hnav2b")))

(define-public crate-sentry-log-0.24.1 (c (n "sentry-log") (v "0.24.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)))) (h "12pjy76rvpvayj3ls2l0ahicgfdic7z5fh0pqi8ncv95a5wz6v29")))

(define-public crate-sentry-log-0.24.2 (c (n "sentry-log") (v "0.24.2") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)))) (h "1k961zlk74w46zwx0zas1gaivxzmlvimg4w7jwfk8pqji5c1h2s5")))

(define-public crate-sentry-log-0.24.3 (c (n "sentry-log") (v "0.24.3") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)))) (h "1bvaxvb0x0712p5fqlhqspswpvy36i50n5f53plg7db91qgsi8p3")))

(define-public crate-sentry-log-0.25.0 (c (n "sentry-log") (v "0.25.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)))) (h "1zlynwrqwy0zaq7k0bhh65qzpwyimlr1lj53sj5kb5ajg8l6rdci")))

(define-public crate-sentry-log-0.26.0 (c (n "sentry-log") (v "0.26.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)))) (h "0lx4qszbkr2p47x6xvhbbvhm39gsmxkq213h910gasjz0dbh7q7l")))

(define-public crate-sentry-log-0.27.0 (c (n "sentry-log") (v "0.27.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)))) (h "0pzwsyncbvk1sk58mi4lw21qal6m18417yl9lsqfkg8yhr0np9sq") (r "1.57")))

(define-public crate-sentry-log-0.28.0 (c (n "sentry-log") (v "0.28.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)))) (h "1pij4hfz1bjn4na9dlsdk6bscm5gp0r22x52v0xpsw09bk8n38wd") (r "1.60")))

(define-public crate-sentry-log-0.29.0 (c (n "sentry-log") (v "0.29.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)))) (h "1hx3303f9yrwbjyvdwi7s0031zmvvfzh5pvl6plcg62l3rb7wa6x") (r "1.60")))

(define-public crate-sentry-log-0.29.1 (c (n "sentry-log") (v "0.29.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)))) (h "0pi0p6akd01z5f5lmsxggg3gcn1jv9iasydbpg8pah7s65mhj63m") (r "1.60")))

(define-public crate-sentry-log-0.29.2 (c (n "sentry-log") (v "0.29.2") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)))) (h "1d1rm9j3h4b375ij9m9cq4i4wbs3vn98kg6ydaayrg2h8zhyz2jr") (r "1.60")))

(define-public crate-sentry-log-0.29.3 (c (n "sentry-log") (v "0.29.3") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)))) (h "0hc23yasjnhp02wisz7cp2pibiz44mcab223q8s171hl80wj5fd4") (r "1.60")))

(define-public crate-sentry-log-0.30.0 (c (n "sentry-log") (v "0.30.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)))) (h "04v02asns54gcyw2ws2cqwszm7bjapix117b4qf58xvy8zsa78xz") (r "1.66")))

(define-public crate-sentry-log-0.31.0 (c (n "sentry-log") (v "0.31.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)))) (h "0mnzrdn8flyif7bxr394b8nyi3x1dssml0yw32bch5fbshl8m5zf") (r "1.66")))

(define-public crate-sentry-log-0.31.1 (c (n "sentry-log") (v "0.31.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)))) (h "099cm97wsjllx0hwih2gl44y2gmyzjy6r76b1myjr3lwivj38fd4") (r "1.66")))

(define-public crate-sentry-log-0.31.2 (c (n "sentry-log") (v "0.31.2") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)))) (h "0h3pn88rs7k5bbxxs0l6nb3m145yfxqg0ipsl1zwyjl5hnf573wm") (r "1.66")))

(define-public crate-sentry-log-0.31.3 (c (n "sentry-log") (v "0.31.3") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)))) (h "118nmdfr8gd56kvc23r5hql0qy21qaqncg1bp8d0rm76vlckjvb1") (r "1.66")))

(define-public crate-sentry-log-0.31.4 (c (n "sentry-log") (v "0.31.4") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)))) (h "0jvxvdvd782x49acs81ba47ycy4kcwaq250ppm0kjs924d249r68") (r "1.66")))

(define-public crate-sentry-log-0.31.5 (c (n "sentry-log") (v "0.31.5") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "0gpk3nn19abxav0q09bks2ncs63bxn1cwi8vf5in0vijhm5gqn15") (r "1.66")))

(define-public crate-sentry-log-0.31.6 (c (n "sentry-log") (v "0.31.6") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)))) (h "1795lldvq6xgjz67vlh2m2psg2n2ib1gpzlq2b588sx6b5chlarg") (r "1.67")))

(define-public crate-sentry-log-0.31.7 (c (n "sentry-log") (v "0.31.7") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)))) (h "0fv084smx53f4gdd0p45k384ngy5z4w7im6q3p0ya53jc8nf6vk0") (r "1.68")))

(define-public crate-sentry-log-0.31.8 (c (n "sentry-log") (v "0.31.8") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)))) (h "1z08xbqqz85p72zvdkfn35fxs8slfniy9mivrfqibsd6ki9dp0rn") (r "1.68")))

(define-public crate-sentry-log-0.32.0 (c (n "sentry-log") (v "0.32.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)))) (h "154pb6p0jiqp1d3vnxwwbnscsnrbz4nqw2aszn40w6cknazhambw") (r "1.68")))

(define-public crate-sentry-log-0.32.1 (c (n "sentry-log") (v "0.32.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)))) (h "1xicp4pqxc5fhw47f5dqbzh4b1xdb5v04zggl9if0bv65x178xbz") (r "1.68")))

(define-public crate-sentry-log-0.32.2 (c (n "sentry-log") (v "0.32.2") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)))) (h "0jx5jlshbpic81mkm1b7i27yl91831ngiav3659il6mkwxccvmyj") (r "1.68")))

(define-public crate-sentry-log-0.32.3 (c (n "sentry-log") (v "0.32.3") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)))) (h "117pnjjzdbjk1n4ay881hx343f5ndzly3wz89267mwaz4ihp4jz7") (r "1.73")))

(define-public crate-sentry-log-0.33.0 (c (n "sentry-log") (v "0.33.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)))) (h "01rg9g1glhxykzp6xq5vk8a3g1qs95hcidhsffh3aykjxv91ajqq") (r "1.73")))

