(define-module (crates-io se nt sentry-backtrace) #:use-module (crates-io))

(define-public crate-sentry-backtrace-0.19.0 (c (n "sentry-backtrace") (v "0.19.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "1s8rqbpy8mqi7fwwxk0iqz5hghg11bmspdkbd49fl91sqarnf06q")))

(define-public crate-sentry-backtrace-0.19.1 (c (n "sentry-backtrace") (v "0.19.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "1j9j88hgxzgnzmkinngx8xsrvsqp6kfgibj1jib08nj3j8ml51a5")))

(define-public crate-sentry-backtrace-0.20.0 (c (n "sentry-backtrace") (v "0.20.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "1a5kg86kigag4jypdsw8l71znw6gg9sbzkkkpjgp3fbq9h9xmmz1")))

(define-public crate-sentry-backtrace-0.20.1 (c (n "sentry-backtrace") (v "0.20.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "1s5jzibc71vnpwdmf34wr71aqgqr981y47r6dpxm5wc20j4vvnlj")))

(define-public crate-sentry-backtrace-0.21.0 (c (n "sentry-backtrace") (v "0.21.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "1izpxw0bxxyw2y4fvjnqqc95rwq50x1fn2y9svy57bvv8pxjir9q")))

(define-public crate-sentry-backtrace-0.22.0 (c (n "sentry-backtrace") (v "0.22.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)))) (h "0cx7bhmnb0sm1s0423409y8biikzbn0ykwx5mar2a3hapvcvk9c0")))

(define-public crate-sentry-backtrace-0.23.0 (c (n "sentry-backtrace") (v "0.23.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)))) (h "1fa86r3ipp4423270n0d32pxp0nypnmyg9pp123wjspkmyicpl4w")))

(define-public crate-sentry-backtrace-0.24.0 (c (n "sentry-backtrace") (v "0.24.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.0") (d #t) (k 0)))) (h "0hyf8xy3c6sig19d3if32jixfd7i747bzh2cgqfg7nk4dhgqxigk")))

(define-public crate-sentry-backtrace-0.24.1 (c (n "sentry-backtrace") (v "0.24.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)))) (h "1190nvvc985f2f3m3mrad73cpzn3d4k38zw3x2acsjlmvx89x7w8")))

(define-public crate-sentry-backtrace-0.24.2 (c (n "sentry-backtrace") (v "0.24.2") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)))) (h "16sfbnvfkshza4pdanv5xcrj1z1lh90slk5fwz64drb6fl12vzjg")))

(define-public crate-sentry-backtrace-0.24.3 (c (n "sentry-backtrace") (v "0.24.3") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)))) (h "1n8sd6widyb86725r5n1r7hhkrqdmrvmrl611710m7072khijb86")))

(define-public crate-sentry-backtrace-0.25.0 (c (n "sentry-backtrace") (v "0.25.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)))) (h "00ncxgmg3q5j6hf9mj211ima37cjx0p1jxagdbgsq84l893a8n41")))

(define-public crate-sentry-backtrace-0.26.0 (c (n "sentry-backtrace") (v "0.26.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)))) (h "1nrg3gs5h1pnqr7zi0gd691v3awrli86rr6x9gx7k63m3ffihw8n")))

(define-public crate-sentry-backtrace-0.27.0 (c (n "sentry-backtrace") (v "0.27.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)))) (h "0d2iqyxr3ndbbh83q6gk2ja9xs5ria61xjfsqw2bqvgwxrazmfj9") (r "1.57")))

(define-public crate-sentry-backtrace-0.28.0 (c (n "sentry-backtrace") (v "0.28.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)))) (h "1vr2d2636y77vhx1hbf8c68y5zcdh1104kxdb9504nz2mbwnzi9a") (r "1.60")))

(define-public crate-sentry-backtrace-0.29.0 (c (n "sentry-backtrace") (v "0.29.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)))) (h "14m59maddkwjm0vz90qjqrg2f3zz7jgz7yl15rhgw82w9qcrrmq4") (r "1.60")))

(define-public crate-sentry-backtrace-0.29.1 (c (n "sentry-backtrace") (v "0.29.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)))) (h "0gdxdiik7lczkssjmpm3xj224hix7dfisxi984qsnajm0q481r5g") (r "1.60")))

(define-public crate-sentry-backtrace-0.29.2 (c (n "sentry-backtrace") (v "0.29.2") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)))) (h "0shas6bcriw67s5fssls2zy4y3sh7cpi9mkjz3j4llwispjd34lx") (r "1.60")))

(define-public crate-sentry-backtrace-0.29.3 (c (n "sentry-backtrace") (v "0.29.3") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)))) (h "0c10kby38x7jbw204j294rryavj83gaa16k002c33kjc9ljw1miy") (r "1.60")))

(define-public crate-sentry-backtrace-0.30.0 (c (n "sentry-backtrace") (v "0.30.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)))) (h "06lv76cli9m5vsx5b7m13kjwnag109p922cki8cdxy6ish4f8zqf") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.0 (c (n "sentry-backtrace") (v "0.31.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)))) (h "1h3ls45wddfg433za3q33yajhr6b9v5s690gdwag2lfapppwvdq3") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.1 (c (n "sentry-backtrace") (v "0.31.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)))) (h "0qh96dzl8h23hyx6krlq45alrxif9smhnq1cnnr5rmyd2y1zwaf0") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.2 (c (n "sentry-backtrace") (v "0.31.2") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)))) (h "1qv8shscnsfs0smw62y2w2majwndgdclp4q8mkaqsc06vr9np6yq") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.3 (c (n "sentry-backtrace") (v "0.31.3") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)))) (h "0i3nh930s66m8wzvjrabziphw3m5w3mcrnwrnj0svld3gp93lki6") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.4 (c (n "sentry-backtrace") (v "0.31.4") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)))) (h "037df49a7kpfvg1dmrybmm5r4z3978vgn43z9c4axjdnpkwl7w4w") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.5 (c (n "sentry-backtrace") (v "0.31.5") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "1garziijl2vm2hm2wh12cxprm6vm99skap4vqlmg5dbv2j7yxwhi") (r "1.66")))

(define-public crate-sentry-backtrace-0.31.6 (c (n "sentry-backtrace") (v "0.31.6") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)))) (h "1abi17zgvkljakl0ad62qpw9gr1asd0iak89pg2c9i0hyg3bmhka") (r "1.67")))

(define-public crate-sentry-backtrace-0.31.7 (c (n "sentry-backtrace") (v "0.31.7") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)))) (h "0w4nic8v0bgqmqlwlzbvfm3r05v1jwx8v2nk92ik0s6xl47vi9qq") (r "1.68")))

(define-public crate-sentry-backtrace-0.31.8 (c (n "sentry-backtrace") (v "0.31.8") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)))) (h "03m0vxv8q2yaf6r0ykcy7rfz40vdcr1kjw6w33vyhgd70i78vk2q") (r "1.68")))

(define-public crate-sentry-backtrace-0.32.0 (c (n "sentry-backtrace") (v "0.32.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)))) (h "11cy3115q19v8rs1c4kkmncfr0zwmny6c1rils4qja1c2sbhllg6") (r "1.68")))

(define-public crate-sentry-backtrace-0.32.1 (c (n "sentry-backtrace") (v "0.32.1") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)))) (h "1rfpdlskjq1v5bpyp7mw37i5m9bhn9hfxgwwb8b26nyfspvqy0fg") (r "1.68")))

(define-public crate-sentry-backtrace-0.32.2 (c (n "sentry-backtrace") (v "0.32.2") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)))) (h "0icb8zpi4cn2iknh4y1fy87inyal64q3jc6k3hg10y1wifniqw1j") (r "1.68")))

(define-public crate-sentry-backtrace-0.32.3 (c (n "sentry-backtrace") (v "0.32.3") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)))) (h "07ljhsqsrxaa4vr9vgc9m1ispimv51cnx29kvpjwpc1l9w3r94d7") (r "1.73")))

(define-public crate-sentry-backtrace-0.33.0 (c (n "sentry-backtrace") (v "0.33.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)))) (h "1jq28caqrwwipxffkn4w6ldyflj8p5i5g1lz70hjhcz06ck2mrcc") (r "1.73")))

