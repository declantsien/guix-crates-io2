(define-module (crates-io se nt sentinel-rocket) #:use-module (crates-io))

(define-public crate-sentinel-rocket-0.1.0 (c (n "sentinel-rocket") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "sentinel-core") (r "^0.1") (f (quote ("async"))) (d #t) (k 0)))) (h "0bd8pwiss5a44adq6piqbpbywk9i30j13b7y0nfb1gjd7p7vfv42")))

