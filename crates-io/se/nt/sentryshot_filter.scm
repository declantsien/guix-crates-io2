(define-module (crates-io se nt sentryshot_filter) #:use-module (crates-io))

(define-public crate-sentryshot_filter-0.0.1 (c (n "sentryshot_filter") (v "0.0.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kcjrx0mvz48c78cry7myj6i5zc3r8ycrmsll7pjjfl8i6y2fimf") (r "1.65")))

(define-public crate-sentryshot_filter-0.1.0 (c (n "sentryshot_filter") (v "0.1.0") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "00wrsh2ariiv0hd7dp5igij8k9izaljcw5my2llniswgh6yyk51m") (r "1.65")))

(define-public crate-sentryshot_filter-0.1.1 (c (n "sentryshot_filter") (v "0.1.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.1.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08r88kyg28hpzckmdvyw5y09fs10nlnvrj0y4xm0qypqb83nkz46") (r "1.65")))

(define-public crate-sentryshot_filter-0.1.2 (c (n "sentryshot_filter") (v "0.1.2") (d (list (d (n "pretty-hex") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "sentryshot_util") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1348wrki5578ckgglfziclbfiv8cf5ywq6z3c9pb2kf8849sxkfv") (r "1.75")))

