(define-module (crates-io se nt sentry-panic) #:use-module (crates-io))

(define-public crate-sentry-panic-0.19.0 (c (n "sentry-panic") (v "0.19.0") (d (list (d (n "sentry-backtrace") (r "^0.19.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "06sdf0kvardi2xp33idxxhk3gkh8a48mc225paayqx5rp8f9vvpq")))

(define-public crate-sentry-panic-0.19.1 (c (n "sentry-panic") (v "0.19.1") (d (list (d (n "sentry-backtrace") (r "^0.19.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "0vkfd8gbazdm5kjvaqmqs3jff9ss4fr5nqlb2m295wzzqwzkr573")))

(define-public crate-sentry-panic-0.20.0 (c (n "sentry-panic") (v "0.20.0") (d (list (d (n "sentry-backtrace") (r "^0.20.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "07j4cy1g30c7nva82fdy4vylhcvw54jd4km1rp69z3fykx69xxbq")))

(define-public crate-sentry-panic-0.20.1 (c (n "sentry-panic") (v "0.20.1") (d (list (d (n "sentry-backtrace") (r "^0.20.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "0is1alic50yhb358706d352b1r2wcw19spqmsvppd6iki74km2mb")))

(define-public crate-sentry-panic-0.21.0 (c (n "sentry-panic") (v "0.21.0") (d (list (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "1vaii20rdvnfg6z9cc4x1f7arpy07gswja990gxxrz4jha6k7vh4")))

(define-public crate-sentry-panic-0.22.0 (c (n "sentry-panic") (v "0.22.0") (d (list (d (n "sentry-backtrace") (r "^0.22.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.22.0") (d #t) (k 0)))) (h "145xm6x9ivq151q97wb6lkhbhqywfi48pb5rws80p984zxf1kkw9")))

(define-public crate-sentry-panic-0.23.0 (c (n "sentry-panic") (v "0.23.0") (d (list (d (n "sentry-backtrace") (r "^0.23.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.23.0") (d #t) (k 0)))) (h "15ax4li331ncdp39i5hqdpsp2drch9i8xxfp6dg057y9y24zjav9")))

(define-public crate-sentry-panic-0.24.1 (c (n "sentry-panic") (v "0.24.1") (d (list (d (n "sentry-backtrace") (r "^0.24.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.1") (d #t) (k 0)))) (h "1h74hl8b267izkm5prxz5lz67fcss2qphv7bak52fkq6xw8ylzva")))

(define-public crate-sentry-panic-0.24.2 (c (n "sentry-panic") (v "0.24.2") (d (list (d (n "sentry-backtrace") (r "^0.24.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.2") (d #t) (k 0)))) (h "1zylb40sa95m7fi8q3vaxn49yr0qy6np62wxdgycq6il2xfkmxyh")))

(define-public crate-sentry-panic-0.24.3 (c (n "sentry-panic") (v "0.24.3") (d (list (d (n "sentry-backtrace") (r "^0.24.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.24.3") (d #t) (k 0)))) (h "0hzbyqhw60m9lhqwbfg0fc7w275b54fyijm4mm2yn5kj88nifzpk")))

(define-public crate-sentry-panic-0.25.0 (c (n "sentry-panic") (v "0.25.0") (d (list (d (n "sentry-backtrace") (r "^0.25.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.25.0") (d #t) (k 0)))) (h "05kl3kgi4swkjzkvlglpjg9rijfyybhnz01718ics2j5bjbb2mxr")))

(define-public crate-sentry-panic-0.26.0 (c (n "sentry-panic") (v "0.26.0") (d (list (d (n "sentry-backtrace") (r "^0.26.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.26.0") (d #t) (k 0)))) (h "0lxsqwvmpqks79hvkh70963w7hv3z3jbzxwk8gv1ahf3kiv45d7q")))

(define-public crate-sentry-panic-0.27.0 (c (n "sentry-panic") (v "0.27.0") (d (list (d (n "sentry-backtrace") (r "^0.27.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.27.0") (d #t) (k 0)))) (h "1gw0njjpsriclm97yfwqwyds8kdhi4wzzl1i99dhsnidi32p8v39") (r "1.57")))

(define-public crate-sentry-panic-0.28.0 (c (n "sentry-panic") (v "0.28.0") (d (list (d (n "sentry-backtrace") (r "^0.28.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.28.0") (d #t) (k 0)))) (h "05dqlgipzdr2f4sklzrxwwb4vlp96g5y4d2wfqr724awkdfh0ia1") (r "1.60")))

(define-public crate-sentry-panic-0.29.0 (c (n "sentry-panic") (v "0.29.0") (d (list (d (n "sentry-backtrace") (r "^0.29.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.0") (d #t) (k 0)))) (h "0xlijmy41hsbzl3a1hz3z0ngda7g50f77njlh490sgqf571ighjf") (r "1.60")))

(define-public crate-sentry-panic-0.29.1 (c (n "sentry-panic") (v "0.29.1") (d (list (d (n "sentry-backtrace") (r "^0.29.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.1") (d #t) (k 0)))) (h "1d4csh932rdln9j4f4rzrrj7v4zp5ws0vsynxc8yawzj022ppwqa") (r "1.60")))

(define-public crate-sentry-panic-0.29.2 (c (n "sentry-panic") (v "0.29.2") (d (list (d (n "sentry-backtrace") (r "^0.29.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.2") (d #t) (k 0)))) (h "0rnzxn699lfcwv5v1c2c2vgrrqydflpfwjsjyi2krrp326mlzayc") (r "1.60")))

(define-public crate-sentry-panic-0.29.3 (c (n "sentry-panic") (v "0.29.3") (d (list (d (n "sentry-backtrace") (r "^0.29.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.29.3") (d #t) (k 0)))) (h "05dxyzj8qg8r388hsp2r0a4w9rpgll4d5alws464gankvfpcgsxy") (r "1.60")))

(define-public crate-sentry-panic-0.30.0 (c (n "sentry-panic") (v "0.30.0") (d (list (d (n "sentry-backtrace") (r "^0.30.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.30.0") (d #t) (k 0)))) (h "009s8y4cprdix2kn7hrzdb95x7gn8vch5i7fb2fqj54cg8mfsk0z") (r "1.66")))

(define-public crate-sentry-panic-0.31.0 (c (n "sentry-panic") (v "0.31.0") (d (list (d (n "sentry-backtrace") (r "^0.31.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.0") (d #t) (k 0)))) (h "1l16dn79pbxfy0hrbddgfvgqy61xk5hxy5k9ms29w3crh5wqf2qf") (r "1.66")))

(define-public crate-sentry-panic-0.31.1 (c (n "sentry-panic") (v "0.31.1") (d (list (d (n "sentry-backtrace") (r "^0.31.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.1") (d #t) (k 0)))) (h "0iphdc2705g4ymqcrg73ikf6r08q0l0k45x0vdp6vh1z245csiaf") (r "1.66")))

(define-public crate-sentry-panic-0.31.2 (c (n "sentry-panic") (v "0.31.2") (d (list (d (n "sentry-backtrace") (r "^0.31.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.2") (d #t) (k 0)))) (h "1hjq8pks2y7k0m51wqiim4kf8w5bx529zhf7109xaainjjs2mp4l") (r "1.66")))

(define-public crate-sentry-panic-0.31.3 (c (n "sentry-panic") (v "0.31.3") (d (list (d (n "sentry-backtrace") (r "^0.31.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.3") (d #t) (k 0)))) (h "06i18rx48dqmfxgibc0vjxhjmizc1ikkk2jdk5d0mbsbl8l1150a") (r "1.66")))

(define-public crate-sentry-panic-0.31.4 (c (n "sentry-panic") (v "0.31.4") (d (list (d (n "sentry-backtrace") (r "^0.31.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.4") (d #t) (k 0)))) (h "13qqjqlnka2pad478cz241wilglscq3s0cxiz0sy8mqiy5qc4fh6") (r "1.66")))

(define-public crate-sentry-panic-0.31.5 (c (n "sentry-panic") (v "0.31.5") (d (list (d (n "sentry-backtrace") (r "^0.31.5") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.5") (d #t) (k 0)))) (h "1kdwcn98k2x4b1akprjh5bzidxyfakxgxxk4idm14blv3hvyipy4") (r "1.66")))

(define-public crate-sentry-panic-0.31.6 (c (n "sentry-panic") (v "0.31.6") (d (list (d (n "sentry-backtrace") (r "^0.31.6") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.6") (d #t) (k 0)))) (h "1dv5srmhcprj2xnqk4h1k799fhlkz02zpsi981kbsxfs0vsnjnw7") (r "1.67")))

(define-public crate-sentry-panic-0.31.7 (c (n "sentry-panic") (v "0.31.7") (d (list (d (n "sentry-backtrace") (r "^0.31.7") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.7") (d #t) (k 0)))) (h "1yp0sv9hddp95rvnhi0g8703bbqny4m1ayri6ybrym5s7chn0c9j") (r "1.68")))

(define-public crate-sentry-panic-0.31.8 (c (n "sentry-panic") (v "0.31.8") (d (list (d (n "sentry-backtrace") (r "^0.31.8") (d #t) (k 0)) (d (n "sentry-core") (r "^0.31.8") (d #t) (k 0)))) (h "16ga3r45xbapkb5n1sj7icp6az52mxrb5q8jb78akdpqcg0z3yvl") (r "1.68")))

(define-public crate-sentry-panic-0.32.0 (c (n "sentry-panic") (v "0.32.0") (d (list (d (n "sentry-backtrace") (r "^0.32.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.0") (d #t) (k 0)))) (h "1jdv1dwra84h8zzbadr6z1av5fysz4xkgwl5wkh3gzhmfsxf4rh8") (r "1.68")))

(define-public crate-sentry-panic-0.32.1 (c (n "sentry-panic") (v "0.32.1") (d (list (d (n "sentry-backtrace") (r "^0.32.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.1") (d #t) (k 0)))) (h "1n34m54q5bmbpnpihmnmzrry1mnn8f1cnjl0jqns7n1birx4w8p0") (r "1.68")))

(define-public crate-sentry-panic-0.32.2 (c (n "sentry-panic") (v "0.32.2") (d (list (d (n "sentry-backtrace") (r "^0.32.2") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.2") (d #t) (k 0)))) (h "1jmm4779wishg45q9ddb1bq7cp3j4c8c7x7ja14ml6dsbw5qvhfi") (r "1.68")))

(define-public crate-sentry-panic-0.32.3 (c (n "sentry-panic") (v "0.32.3") (d (list (d (n "sentry-backtrace") (r "^0.32.3") (d #t) (k 0)) (d (n "sentry-core") (r "^0.32.3") (d #t) (k 0)))) (h "0386jmywyzy79m5iivcp52srb2srlb7kfipxviw0qxf8lg7kxaif") (r "1.73")))

(define-public crate-sentry-panic-0.33.0 (c (n "sentry-panic") (v "0.33.0") (d (list (d (n "sentry-backtrace") (r "^0.33.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.33.0") (d #t) (k 0)))) (h "1rsh5hvgg9jm092cgl28fi8xdh1z3srvnn87qq7vjw2nmp040h3g") (r "1.73")))

