(define-module (crates-io se nt sentry-error-chain) #:use-module (crates-io))

(define-public crate-sentry-error-chain-0.19.0 (c (n "sentry-error-chain") (v "0.19.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.0") (d #t) (k 0)))) (h "14kn2c157fjgzsjx54krrifjdgvbra2a770lly4ny34f7c3fq083")))

(define-public crate-sentry-error-chain-0.19.1 (c (n "sentry-error-chain") (v "0.19.1") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.19.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.19.1") (d #t) (k 0)))) (h "132xc9y6iwmkf6fqwh3rmmsgv7ibw1mkn3jn2j5byfmmp9g203bj")))

(define-public crate-sentry-error-chain-0.20.0 (c (n "sentry-error-chain") (v "0.20.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.0") (d #t) (k 0)))) (h "0z20zmwpjc0mynpwbc9svgkdyxpb9k222xflj1i9a5ypby5pmwia")))

(define-public crate-sentry-error-chain-0.20.1 (c (n "sentry-error-chain") (v "0.20.1") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.20.1") (d #t) (k 0)) (d (n "sentry-core") (r "^0.20.1") (d #t) (k 0)))) (h "17i6qz5hzw3v4d7ij6l39v5my51fdnrydd38mgqmdpqw9d6i3via")))

(define-public crate-sentry-error-chain-0.21.0 (c (n "sentry-error-chain") (v "0.21.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0)))) (h "12izhg927mf5606srz6yawdan2ys8xy8n5kj4yw3qqrh96kw6jhx")))

