(define-module (crates-io se nt sentry_rocket) #:use-module (crates-io))

(define-public crate-sentry_rocket-0.1.0 (c (n "sentry_rocket") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "sentry") (r "^0.12.0") (d #t) (k 0)))) (h "0qcv0260swammrhaysjwcpj5y6sw30qkhvvimy8ns887z4ad9d9q")))

(define-public crate-sentry_rocket-0.1.1 (c (n "sentry_rocket") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "sentry") (r "^0.13.0") (d #t) (k 0)))) (h "0w9z7if7vgg1c0m294fpi6yksr80288bs0lh2vzgpjmhj1z6cim4")))

