(define-module (crates-io se nt sentaku) #:use-module (crates-io))

(define-public crate-sentaku-0.1.0 (c (n "sentaku") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "02qkin70y11av8f4945hcv6gc3zqk72r5jl8hppyjicwvzc0976n")))

(define-public crate-sentaku-0.1.1 (c (n "sentaku") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1rqw21gkrd6jqmazhy4af2mhzfmhl1k6v0296xvnw51sdcd5j4zl")))

(define-public crate-sentaku-0.1.2 (c (n "sentaku") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1nwjg3pgclw8pgrfivf55p8bif6z2yhbs713yf7sz0817g3r1q7c")))

(define-public crate-sentaku-0.2.0 (c (n "sentaku") (v "0.2.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 2)))) (h "17zrw5cnjxxcdizywl3byf7xq1f2z4109r28l5m5pg5qvplhfgad")))

(define-public crate-sentaku-0.3.0 (c (n "sentaku") (v "0.3.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 2)))) (h "0ms41b5d1ghh8469v6ws0nfrh7xy7q5pqqd90y7js79fqhj60z9g")))

(define-public crate-sentaku-0.3.1 (c (n "sentaku") (v "0.3.1") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 2)))) (h "0mid92h8n6z9aw37qj03720ayhx8l7gxb4vz6ypyj41gzhk6z9mz")))

(define-public crate-sentaku-0.4.0 (c (n "sentaku") (v "0.4.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.2") (d #t) (k 2)))) (h "0k4lbza13nsx8lbk0mli65f05npi4av0k3nsinvrr84f29dc9gpb")))

(define-public crate-sentaku-0.4.1 (c (n "sentaku") (v "0.4.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 2)))) (h "0rgb2yr80rcpsmla5wpjximmiljdv5jbyjwg8vg4c03ksp6fjz0h")))

