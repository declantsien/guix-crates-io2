(define-module (crates-io se nt sentencepiece-sys) #:use-module (crates-io))

(define-public crate-sentencepiece-sys-0.1.0 (c (n "sentencepiece-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19rzpf2bs67xz8sfdqa4si9j92y2d9l80wrbfd1x5sbgfbvz9dxs")))

(define-public crate-sentencepiece-sys-0.1.1 (c (n "sentencepiece-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yrysq7k34ni9528gkz9anvjh2r5jwy9n7h987g2qlpzb8svcxzl")))

(define-public crate-sentencepiece-sys-0.1.2 (c (n "sentencepiece-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07jyifjgshhzvckni0pmppacvhiw3ph9a1hg3lb9g6mdak7n69x2")))

(define-public crate-sentencepiece-sys-0.1.3 (c (n "sentencepiece-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "190hbwcypa8ji09jkm1wigwwm1z7nsjbqnmyiqm0kcy6zbvzzmmy")))

(define-public crate-sentencepiece-sys-0.2.0 (c (n "sentencepiece-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07izfl1jzwcx16c5szhshlmqbn1vbyafhlm6dmx0i55z6j53fkwr")))

(define-public crate-sentencepiece-sys-0.1.4 (c (n "sentencepiece-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14w7jz1rvka6yj0him62s16hcgf9r18j30rknvcd1pgrrk54wv69") (y #t)))

(define-public crate-sentencepiece-sys-0.2.1 (c (n "sentencepiece-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06hh77isqiam4jyd4ifj1k3a1w6m34wlb3ynm0z51vq5a2sw8373")))

(define-public crate-sentencepiece-sys-0.3.0 (c (n "sentencepiece-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c0jr1r6abi3bgic69d0bvr1x98prb796mlbji9h2pyr2d6xaipr")))

(define-public crate-sentencepiece-sys-0.3.2 (c (n "sentencepiece-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08svllzw1f0ygvzr2kr94i4ifgvm8gjaii747siw31gpx5jq7nqr")))

(define-public crate-sentencepiece-sys-0.4.0 (c (n "sentencepiece-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a8g95mjp6f5s8lss7b52kf35nx187whp1g1lnks3bjabhjhcvhs") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.5.0 (c (n "sentencepiece-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0amskay44nwvjf6iqv3cq2gmsgr857fby59mkd957jhwva2dd8x4") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.6.0 (c (n "sentencepiece-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yclhxqs1p75isxa1i6vjiddrnkamrgrpgc2if2jc3ayhkq3b5f6") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.7.0 (c (n "sentencepiece-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "013yg04rrps3ygrqj0rrhw1bl2scrys7w4w9x7c7vpgldmhh9cfs") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.7.1 (c (n "sentencepiece-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m7aksn2h30rm0f91wnv7pg3gc4ghwyr5h75fdzsv6hvvqcad0bn") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.8.3 (c (n "sentencepiece-sys") (v "0.8.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "143257g6k04kj2lybhhaf6p3nzj7f8w9cnbs97z66fy3j677mk65") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.9.0 (c (n "sentencepiece-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fvx87s1nfhs6rnhy3z7z7hcgsvan1qgk97c5wi27cg37g6y8vbs") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.10.0 (c (n "sentencepiece-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lm86ffv95gnrcsrmkjhp9axc5b2lsr1mz3ihp3hnhwawiizy66q") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.11.0 (c (n "sentencepiece-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05b1rqaawfm525pw67q2ipqfd5zy1i06iklf62ijkcm25laf38n6") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.11.1 (c (n "sentencepiece-sys") (v "0.11.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p3rwsnrki815r9x583hhx8gbaf32zvx7mjcr3j2h6c93j6f7zj8") (f (quote (("system") ("static"))))))

(define-public crate-sentencepiece-sys-0.11.2 (c (n "sentencepiece-sys") (v "0.11.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07xps136f9zn6cziysfx2dg72d0l40hiqv8iirwnarilbqqnc77j") (f (quote (("system") ("static"))))))

