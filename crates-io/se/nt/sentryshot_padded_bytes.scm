(define-module (crates-io se nt sentryshot_padded_bytes) #:use-module (crates-io))

(define-public crate-sentryshot_padded_bytes-0.0.1 (c (n "sentryshot_padded_bytes") (v "0.0.1") (h "0bfv4vp1a3yhzrirkd7qxswad0fw560gl1n4d1m4fvk1samw41bv") (r "1.65")))

(define-public crate-sentryshot_padded_bytes-0.1.0 (c (n "sentryshot_padded_bytes") (v "0.1.0") (h "1svmlbdcsx83dvqlnw3l4xfz7k1c7bxijvviffj5l0brqi11scz0") (r "1.65")))

