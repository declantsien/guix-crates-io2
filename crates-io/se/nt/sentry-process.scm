(define-module (crates-io se nt sentry-process) #:use-module (crates-io))

(define-public crate-sentry-process-1.0.0 (c (n "sentry-process") (v "1.0.0") (d (list (d (n "sentry") (r "0.13.*") (f (quote ("with_client_implementation" "with_device_info" "with_rust_info"))) (k 0)))) (h "0wizmq6ys0v2xyg11y925cmy1x3xbgnrzkzscn30vwny3c0xhcxz")))

(define-public crate-sentry-process-1.0.1 (c (n "sentry-process") (v "1.0.1") (d (list (d (n "sentry") (r "0.15.*") (f (quote ("with_client_implementation" "with_default_transport" "with_device_info" "with_rust_info"))) (k 0)))) (h "08m68cff76512007awvc20ps8a5knrqfrb4d3jzryxjj735x0hyc")))

(define-public crate-sentry-process-1.0.2 (c (n "sentry-process") (v "1.0.2") (d (list (d (n "sentry") (r "0.18.*") (f (quote ("with_client_implementation" "with_default_transport" "with_device_info" "with_rust_info"))) (k 0)))) (h "01vznqfhfsnl38n7l6zfz1qy3i5g4ycv7spixznz4xd1aximxx04")))

(define-public crate-sentry-process-2.0.0 (c (n "sentry-process") (v "2.0.0") (d (list (d (n "rbl_circular_buffer") (r "^0.1.2") (d #t) (k 0)) (d (n "sentry") (r "^0.20.1") (f (quote ("contexts" "panic" "reqwest" "rustls"))) (k 0)))) (h "0ipag0nk91sk3ihaxyi6gp50yi7fp0m23l05ng3wnbajijb04zzz")))

(define-public crate-sentry-process-2.1.0 (c (n "sentry-process") (v "2.1.0") (d (list (d (n "rbl_circular_buffer") (r "^0.1.2") (d #t) (k 0)) (d (n "sentry") (r "^0.26.0") (f (quote ("contexts" "panic" "reqwest" "rustls"))) (k 0)))) (h "0ab6v5763xm3wkppj6adwfs3j5c4gk5pgi80n1q44m7n52jcbw5p")))

