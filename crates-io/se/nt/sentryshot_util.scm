(define-module (crates-io se nt sentryshot_util) #:use-module (crates-io))

(define-public crate-sentryshot_util-0.0.1 (c (n "sentryshot_util") (v "0.0.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1743wxynzwmifklppwsavvzga68g9dw972cphsmiygkgrjayh2g3") (r "1.65")))

(define-public crate-sentryshot_util-0.1.0 (c (n "sentryshot_util") (v "0.1.0") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.0.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "022qdqqxf5ras9jphbgqnrv5aygx2nz61p555b0lxpwpvajka4s6") (r "1.65")))

(define-public crate-sentryshot_util-0.1.1 (c (n "sentryshot_util") (v "0.1.1") (d (list (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1v5l1wrb44rnk1b2n17z0v4sm6l4pwnly1xj44sff3qxxgk35gvx") (r "1.65")))

(define-public crate-sentryshot_util-0.1.2 (c (n "sentryshot_util") (v "0.1.2") (d (list (d (n "pretty-hex") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sentryshot_padded_bytes") (r "^0.1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "02kg5gbfv2cxybayk8wwwigi644l3bhrxivqfm0c1w9gm5y8xwjy") (r "1.75")))

