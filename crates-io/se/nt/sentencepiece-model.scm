(define-module (crates-io se nt sentencepiece-model) #:use-module (crates-io))

(define-public crate-sentencepiece-model-0.1.0 (c (n "sentencepiece-model") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.2") (f (quote ("prost-derive"))) (k 0)) (d (n "prost-build") (r "^0.12.2") (d #t) (k 1)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "which") (r "^5.0.0") (d #t) (k 1)))) (h "1dsm1i01w8n8fq3w8qgc2z601h97q02g54zclibdgg6y00rkfajk") (f (quote (("std" "prost/std") ("default" "std"))))))

