(define-module (crates-io se nt sentc-crypto-common) #:use-module (crates-io))

(define-public crate-sentc-crypto-common-0.1.0 (c (n "sentc-crypto-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0xksb0411q0b2wil6xbb5a4gshwymm4km1b4dy9xwyc70a053pp8")))

(define-public crate-sentc-crypto-common-0.1.1 (c (n "sentc-crypto-common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "1wzn6247w7n18bc7y3wj0b67kn3mqbrx852fn2hfkqnpqp39lk60")))

(define-public crate-sentc-crypto-common-0.1.2 (c (n "sentc-crypto-common") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "02cvxwxypzxlh036cfbasvs98fb06p0y7hbwmddgnk0dz6ls36py")))

(define-public crate-sentc-crypto-common-0.1.3 (c (n "sentc-crypto-common") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "1zcwv8lx21p9hs85m2gal2hh3gq89x2v8c8vhpbga6n4cmz71gp1")))

(define-public crate-sentc-crypto-common-0.8.0 (c (n "sentc-crypto-common") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0js31h60x099mpvfbxm04s9fx4hw79x8wrk8pamyw7dm67h4x4ck")))

(define-public crate-sentc-crypto-common-0.9.0 (c (n "sentc-crypto-common") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "10kmqniwjd0rik2xc80gxmjkf13iinyq55bk1a8hss8i4dpm2jvm")))

(define-public crate-sentc-crypto-common-0.9.1 (c (n "sentc-crypto-common") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0hkmd61cvsn0kdplgq2sdqgpa59hcvppd6nkxdv0yj6v66wz2b4x")))

(define-public crate-sentc-crypto-common-0.9.2 (c (n "sentc-crypto-common") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "08kzva8x18dbfqjpn2ak77fq5a3c6wdigm5cjjz1qkmjj6bwjnxg")))

(define-public crate-sentc-crypto-common-0.9.3 (c (n "sentc-crypto-common") (v "0.9.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0jy9fipjp5qbws2zi6wbpxywhbmfim8d3k6ph0gv2wg4g4n2sd74")))

(define-public crate-sentc-crypto-common-0.10.0 (c (n "sentc-crypto-common") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("alloc"))) (k 0)))) (h "0cwn44m15lxfsjvc947c1blj7dlk7p1g513p4mgvvwwghxkinndp")))

