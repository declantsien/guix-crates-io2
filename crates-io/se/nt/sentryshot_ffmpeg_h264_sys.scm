(define-module (crates-io se nt sentryshot_ffmpeg_h264_sys) #:use-module (crates-io))

(define-public crate-sentryshot_ffmpeg_h264_sys-0.0.1 (c (n "sentryshot_ffmpeg_h264_sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0lav7sfhp9fpb8gg22dpkag80n99x3n8fjklr3cnk0553vvgq0ha") (r "1.65")))

(define-public crate-sentryshot_ffmpeg_h264_sys-0.1.0 (c (n "sentryshot_ffmpeg_h264_sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0dshz6z2vpnai67ipbw4bimwbhk3i6h97i614jf0bg0czlad1kg7") (r "1.65")))

(define-public crate-sentryshot_ffmpeg_h264_sys-0.1.1 (c (n "sentryshot_ffmpeg_h264_sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1gqb41zy4ykhl3mn1dr916ab68s3mjm7vhy0cibx56v8gma4v5ay") (r "1.75")))

(define-public crate-sentryshot_ffmpeg_h264_sys-0.1.2 (c (n "sentryshot_ffmpeg_h264_sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0pq46l176ydn1f6fximgrkh1iq7ajaj7y9za4a16rs0gj53dagij") (r "1.75")))

