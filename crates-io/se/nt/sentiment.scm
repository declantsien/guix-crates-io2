(define-module (crates-io se nt sentiment) #:use-module (crates-io))

(define-public crate-sentiment-0.1.0 (c (n "sentiment") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1hg5s7pzs9zzn47gld1mn6k5jaqz2cky6lgzp75ym79vw0nxih13") (y #t)))

(define-public crate-sentiment-0.1.1 (c (n "sentiment") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0lxafv320f3qwvhi7v136a3jv6ca7mrqgdnj2zflzncq6n8qx5k3")))

