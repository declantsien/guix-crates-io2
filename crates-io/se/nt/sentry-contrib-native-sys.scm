(define-module (crates-io se nt sentry-contrib-native-sys) #:use-module (crates-io))

(define-public crate-sentry-contrib-native-sys-0.0.1 (c (n "sentry-contrib-native-sys") (v "0.0.1") (h "0q3y2gxmf4sn21ar0qaz1snfvznr6l2db0a3zzfvk72g5a91zm6j")))

(define-public crate-sentry-contrib-native-sys-0.1.0-alpha (c (n "sentry-contrib-native-sys") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0liphl9bvnkbvnr469xjan13h3vqdsd17z2r2b0b918fxzipm6wd") (f (quote (("nightly") ("default-transport") ("default" "default-transport")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.1.0-alpha-2 (c (n "sentry-contrib-native-sys") (v "0.1.0-alpha-2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0y97h31f0qmqqcaxyj595r01c62qzj6b96fsyxkpgs92kjahh51z") (f (quote (("nightly") ("default-transport") ("default" "default-transport")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.1.0-rc (c (n "sentry-contrib-native-sys") (v "0.1.0-rc") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1csvgyahzkp4fc1554czhy50qf3rvar3kdisyvgai03mcyq3nppj") (f (quote (("nightly") ("default-transport") ("default" "default-transport")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.1.0 (c (n "sentry-contrib-native-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08zxr0p30fgjx3qq00rzxd8v3qxa57qq03p7qlzqn77nq0w7awhx") (f (quote (("transport-default") ("nightly") ("default" "backend-default" "transport-default") ("backend-inproc") ("backend-default") ("backend-crashpad") ("backend-breakpad")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.2.0 (c (n "sentry-contrib-native-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0djf9c01gn3dd0d0ngbk850dnnyx1arhb0jl41ggjz8dp1xb3zhz") (f (quote (("transport-default") ("nightly") ("default" "transport-default") ("backend-inproc") ("backend-crashpad") ("backend-breakpad")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.2.1 (c (n "sentry-contrib-native-sys") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "191zpc33gr5hrvvg3hb2cabs86xah3ycw6sddj600xksmk6rvs9g") (f (quote (("transport-default") ("nightly") ("default" "transport-default") ("backend-inproc") ("backend-crashpad") ("backend-breakpad")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.3.0 (c (n "sentry-contrib-native-sys") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1b4j86ajw39cv8z95kfgyq28knxm9gkpb5g3b00wybp3a7z0mr3j") (f (quote (("transport-default") ("nightly") ("default" "transport-default") ("backend-inproc") ("backend-crashpad") ("backend-breakpad")))) (l "sentry-native")))

(define-public crate-sentry-contrib-native-sys-0.3.1 (c (n "sentry-contrib-native-sys") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0w3i1f5inv7bj4dif0c67jrzc34c0dh3rzrmhyfbal1zs0qfgzgj") (f (quote (("transport-default") ("nightly") ("default" "transport-default") ("backend-inproc") ("backend-crashpad") ("backend-breakpad")))) (l "sentry-native")))

