(define-module (crates-io se ax seax_svm) #:use-module (crates-io))

(define-public crate-seax_svm-0.1.2 (c (n "seax_svm") (v "0.1.2") (h "1krzkj9bixg7hdr3cwpbqv2lfj2dwjb00051g170zci777bxfcjh")))

(define-public crate-seax_svm-0.1.3 (c (n "seax_svm") (v "0.1.3") (h "004j15lv2ghhzf129n9v07nswz8s92bwrcg0jvg2wbfzw6rm95w8")))

(define-public crate-seax_svm-0.2.0-rc.2 (c (n "seax_svm") (v "0.2.0-rc.2") (h "07hd628sy270hc4c1kh5fkkb6bv20bj33kd7lg8adsp7yilija6y")))

(define-public crate-seax_svm-0.2.0-rc.3 (c (n "seax_svm") (v "0.2.0-rc.3") (h "1q4pafc1w20mwv1s7p07k9irymmrq6axqh1fjbkn1k8akr3f414v")))

(define-public crate-seax_svm-0.2.3 (c (n "seax_svm") (v "0.2.3") (h "1vnv9hjnxz0cy5p86ymjhd4vpw24bzw9s8z1rqc6dwmg2i54a9x0")))

(define-public crate-seax_svm-0.2.5 (c (n "seax_svm") (v "0.2.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "034m6vi8dr66szsaxphkn3axcfihvz0vp5jw6icg8yn21xc2fmpa")))

(define-public crate-seax_svm-0.2.6 (c (n "seax_svm") (v "0.2.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1vbdc2bhy5cvhgzlaka3s4g923zb6n98c5mg52qslcl9kasmnli6")))

(define-public crate-seax_svm-0.2.7 (c (n "seax_svm") (v "0.2.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1693vj2mhy0f9fradgbppyapp489mndbq6n7q1k4a7hi1vrd4iwz")))

(define-public crate-seax_svm-0.2.8 (c (n "seax_svm") (v "0.2.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0dyd3dkzg2r9pgbaakmg6w47i4l3pqj2vkqf8flpql3r9n9d6ppw") (f (quote (("nightly"))))))

