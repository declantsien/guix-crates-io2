(define-module (crates-io se ax seax_util) #:use-module (crates-io))

(define-public crate-seax_util-0.1.0 (c (n "seax_util") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1xlmsrnbl3z7fn534kcf05zfm38f3wkvkr47i2ngj1dmxz8nk1ds") (f (quote (("unstable"))))))

(define-public crate-seax_util-0.1.1 (c (n "seax_util") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0c0az45yv25wa9gxigp1xmrqvnk0ykrb6883chp86lyxqj5s3jg3") (f (quote (("unstable"))))))

(define-public crate-seax_util-0.1.2 (c (n "seax_util") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0bvy8xymscfbcfs2633mx4nd2pfnr7hzcrx87vspb6icjf5nl9gn") (f (quote (("unstable"))))))

