(define-module (crates-io se ax seax_scheme) #:use-module (crates-io))

(define-public crate-seax_scheme-0.0.2 (c (n "seax_scheme") (v "0.0.2") (d (list (d (n "parser-combinators") (r "~0.2.2") (d #t) (k 0)) (d (n "seax_svm") (r "^0.1.3") (d #t) (k 0)))) (h "0ssj5rdkj1y5jn1kc0vjmfigypqh5klysxs2gsy6cs60sqrq3klz")))

(define-public crate-seax_scheme-0.0.3 (c (n "seax_scheme") (v "0.0.3") (d (list (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "~0.2.0") (d #t) (k 0)))) (h "1fsp33ckff7r98i0100f83n65l7ngfrbzgh1p9ffapcl1r1czmjj")))

(define-public crate-seax_scheme-0.0.6 (c (n "seax_scheme") (v "0.0.6") (d (list (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "^0.2.2") (d #t) (k 0)))) (h "1vq0lz1j91xjhjil3xjdh8ilzjxzhd973vp9y81xyn10s0yywlrn")))

(define-public crate-seax_scheme-0.1.0 (c (n "seax_scheme") (v "0.1.0") (d (list (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "^0.2.2") (d #t) (k 0)))) (h "19mnv9rlick2darl3ca1cd0jvmpw0xvvk5smmwq1vfj4id661vb2")))

(define-public crate-seax_scheme-0.1.1 (c (n "seax_scheme") (v "0.1.1") (d (list (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "^0.2.2") (d #t) (k 0)))) (h "1qkxy199wc3jq0h6iw3xkhpr9n31avb11cbl44flqhhsjas7c61r")))

(define-public crate-seax_scheme-0.2.1 (c (n "seax_scheme") (v "0.2.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "^0.2.2") (d #t) (k 0)))) (h "01bga7z1pw732lfqrm5xpbva55g518npjw63na79xlaw9al5nkm6")))

(define-public crate-seax_scheme-0.3.0 (c (n "seax_scheme") (v "0.3.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "parser-combinators") (r "~0.2.6") (d #t) (k 0)) (d (n "seax_svm") (r "^0.2.6") (d #t) (k 0)))) (h "1iq88fzl7xhikfqlpfk3x7056r53ss5d4psnrda64940hj0f23js")))

