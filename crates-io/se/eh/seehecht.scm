(define-module (crates-io se eh seehecht) #:use-module (crates-io))

(define-public crate-seehecht-3.0.0 (c (n "seehecht") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "10pdjf458rwk8v67p0n81i1m800d702npk1if0n559za60vkniqw")))

(define-public crate-seehecht-3.0.2 (c (n "seehecht") (v "3.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "14swjncii7vfajpb78xgcy304a1x3bz4ghh2mpx3fw4y6xxcpfdv")))

