(define-module (crates-io se le selectoor) #:use-module (crates-io))

(define-public crate-selectoor-0.1.0 (c (n "selectoor") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "ethers") (r "^1.0.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1qqdbdaxc6k5xx2hfl7phwh9xw05jxdg0yzgydkbvd001r4wx4s3")))

