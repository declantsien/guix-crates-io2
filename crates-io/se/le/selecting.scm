(define-module (crates-io se le selecting) #:use-module (crates-io))

(define-public crate-selecting-1.0.0 (c (n "selecting") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0akah7hdz2kmdfgcginn2s6zamcvx9qfrvq2vvlkzi2wbkfxqlzc")))

(define-public crate-selecting-1.1.0 (c (n "selecting") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dgnjhd8lp4xlbwa83ridnf2v5qqixrwxy8gf8ywv91ylrkqf62v")))

(define-public crate-selecting-1.2.0 (c (n "selecting") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (t "cfg(unix)") (k 0)))) (h "1r18is23wfq22h5wppgwyxbrjnk5py5wa17g7yy8hzk5yr0wg58c")))

