(define-module (crates-io se le select-rustc) #:use-module (crates-io))

(define-public crate-select-rustc-0.1.0 (c (n "select-rustc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0pxss9lym73wvc12ghrf7x4cw6igz1j1a0kxz50mkiqndgrs22pg") (y #t)))

(define-public crate-select-rustc-0.1.1 (c (n "select-rustc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "08bvpkgi1shxld2hl8y1nbv2wq2g9x5blh59bjbzpjshc19di882") (y #t)))

(define-public crate-select-rustc-0.1.2 (c (n "select-rustc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0daqd56smi93g59nz43n4mh3d8whr6j5pa8dmwlf8bd76mdy3cpx") (y #t)))

