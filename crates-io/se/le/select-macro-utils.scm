(define-module (crates-io se le select-macro-utils) #:use-module (crates-io))

(define-public crate-select-macro-utils-0.1.0 (c (n "select-macro-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "123l7clzicml3lj78f81wa016ax6mkdb49zbm2bjwm0d472lrcxr")))

(define-public crate-select-macro-utils-0.1.1 (c (n "select-macro-utils") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0sszh1qzjm1xk56nk2blj933lil3qygyz6ayyphpvbkh5dj0zzhp")))

(define-public crate-select-macro-utils-0.2.0 (c (n "select-macro-utils") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0n5pm4y0i04sasldabvlw0iqn6fjfb1hf981904nmvkb6xynsf9f")))

