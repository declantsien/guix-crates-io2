(define-module (crates-io se le selecta_score) #:use-module (crates-io))

(define-public crate-selecta_score-0.0.1 (c (n "selecta_score") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0z053kv3mgi9mf0gk32gcxb201i830r1zfclcvsln2iay8ccilxm")))

(define-public crate-selecta_score-0.0.2 (c (n "selecta_score") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0pjhsila36grfrijjb49pkdapwz6wda763486nsprrhpq5jxs51c")))

(define-public crate-selecta_score-0.0.3 (c (n "selecta_score") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "14cvh915l1vzn9brnsrwa49jyy5x0l0pjf0qx4jvhii0lcycxwhl")))

