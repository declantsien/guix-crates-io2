(define-module (crates-io se le selectme-macros) #:use-module (crates-io))

(define-public crate-selectme-macros-0.1.0 (c (n "selectme-macros") (v "0.1.0") (h "0hx0195lp5ndxl4frm9mayhdigmli1qsgiz1z3pgk5pbp1d2kb3z")))

(define-public crate-selectme-macros-0.2.0 (c (n "selectme-macros") (v "0.2.0") (h "0h3lj50nqr1vcrw9sh3ihdwl1h6miix9dg14nldjy1yy276mgkhr") (y #t)))

(define-public crate-selectme-macros-0.2.1 (c (n "selectme-macros") (v "0.2.1") (h "0d267qzky6wpxnz3vm8rm579lmbiwcijv76jfq64hy1j968pld1q")))

(define-public crate-selectme-macros-0.2.2 (c (n "selectme-macros") (v "0.2.2") (h "022k6ln2833qpi7dwm3sh6j9bsfkzwm2p9cn7p4x2k4h0kxhjgw4")))

(define-public crate-selectme-macros-0.2.3 (c (n "selectme-macros") (v "0.2.3") (h "02n2d8vq57q8a3l14hyb7r5fq4qggwvgncw4l9mylqgmibwfx94v")))

(define-public crate-selectme-macros-0.2.4 (c (n "selectme-macros") (v "0.2.4") (h "003vb3qzr3w7xq861wjxjci926jlfmj8wk8x9x8r7b45p00hl7v4")))

(define-public crate-selectme-macros-0.2.5 (c (n "selectme-macros") (v "0.2.5") (h "01cqr0p7jmiy7w24xrn10ylbhplinlcab92b3p67rqx37cbrw1bm") (f (quote (("tokio-entry") ("default" "tokio-entry"))))))

(define-public crate-selectme-macros-0.3.0 (c (n "selectme-macros") (v "0.3.0") (h "176jg2vid2j5q1rsa3z03fhyhzl3p7ik2x34smyhqgbrfkyrfh1y") (f (quote (("tokio-entry") ("default"))))))

(define-public crate-selectme-macros-0.4.0 (c (n "selectme-macros") (v "0.4.0") (h "1yjmd0sjafc3biq2g5zf6lz3sb19zn8h6l8bcbwhaqlh57r4kkhq") (f (quote (("tokio-entry") ("default"))))))

(define-public crate-selectme-macros-0.4.1 (c (n "selectme-macros") (v "0.4.1") (h "11lgv708s0jpr3w0xq8jrvrbbmh6j88wizv753wrblqjj2k7ahgf") (f (quote (("tokio-entry") ("default"))))))

(define-public crate-selectme-macros-0.4.2 (c (n "selectme-macros") (v "0.4.2") (h "1ffqjwk847v7cih3vhr1yl56f6y525kqppkslp74hf7a4yngaqws") (f (quote (("tokio-entry") ("default"))))))

(define-public crate-selectme-macros-0.4.3 (c (n "selectme-macros") (v "0.4.3") (h "0s8p5pl09i8ygn9qfq3fwgnf2m43179fmlcclq2qr6m2dizybydb") (f (quote (("tokio-entry") ("tokio-diagnostics") ("default"))))))

(define-public crate-selectme-macros-0.4.4 (c (n "selectme-macros") (v "0.4.4") (h "0aq3l4zcr3vrmhk4rlkhp7b61kyfavpg51jm3ykjyh0k28g0d53q") (f (quote (("tokio-entry") ("tokio-diagnostics") ("default"))))))

(define-public crate-selectme-macros-0.5.0 (c (n "selectme-macros") (v "0.5.0") (h "0pbm4jbnhg4sscwg5ayaz1r4446i1q21irksxsl0pgmirc7gg70d") (f (quote (("tokio-entry") ("tokio-diagnostics") ("default"))))))

(define-public crate-selectme-macros-0.6.0 (c (n "selectme-macros") (v "0.6.0") (h "01dyvv3w1d2mrf6pmr6yszgz7m120sip07s5w5i4dmqm7qck118w") (f (quote (("tokio-entry") ("tokio-diagnostics") ("default"))))))

(define-public crate-selectme-macros-0.7.0 (c (n "selectme-macros") (v "0.7.0") (h "0p1fcqhc1yy37k7vqrhmdlmp8i1yzak7l1i605q0gi31ywcrj9hc") (f (quote (("tokio-entry"))))))

(define-public crate-selectme-macros-0.7.1 (c (n "selectme-macros") (v "0.7.1") (h "00h3f8kydpbk1ibhxa8x7brss3d62dcm1skrlj93qdp6x9qyj056") (f (quote (("tokio-entry"))))))

(define-public crate-selectme-macros-0.7.2 (c (n "selectme-macros") (v "0.7.2") (d (list (d (n "selectme") (r "^0.7.2") (d #t) (k 2)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time" "rt" "rt-multi-thread" "test-util"))) (d #t) (k 2)))) (h "0zj0hhlmvv0dj4d7qr5qv5rp6axydfb6vqfb5hb1jazc1sn146x0") (f (quote (("tokio-entry")))) (r "1.56")))

