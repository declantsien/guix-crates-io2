(define-module (crates-io se le select) #:use-module (crates-io))

(define-public crate-select-0.1.0 (c (n "select") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever_dom_sink") (r "^0.2.0") (d #t) (k 0)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "022ivsp9hqb1llbxs5afajxyzf5jafiak7dcljcp29ra473fiqm6")))

(define-public crate-select-0.1.1 (c (n "select") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.5") (d #t) (k 0)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "1s0qpkpbr0zl1lrabvqw71yikirawis737zv37ax2kgcy0akkzr8")))

(define-public crate-select-0.1.2 (c (n "select") (v "0.1.2") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.6") (d #t) (k 0)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "05va28cvj1a4j6fvas873h0wqq8cpwp66p0hhjk1fgay4yli5lsr")))

(define-public crate-select-0.2.0 (c (n "select") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.6") (d #t) (k 0)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)) (d (n "string_cache") (r "^0.1.13") (d #t) (k 0)) (d (n "tendril") (r "^0.1.6") (d #t) (k 0)))) (h "124a8nl6wakg22fkf5h8s7lxgkq8kib2n1cdlr6dh7f53pxh2kvb")))

(define-public crate-select-0.2.1 (c (n "select") (v "0.2.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.6") (d #t) (k 0)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)) (d (n "string_cache") (r "^0.2.0") (d #t) (k 0)) (d (n "tendril") (r "^0.1.6") (d #t) (k 0)))) (h "0wyvbgy6a3vsf6xddcmclq8rpaarj5snsl2czsz3gfzb98w4dryw")))

(define-public crate-select-0.2.2 (c (n "select") (v "0.2.2") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.2.6") (d #t) (k 0)) (d (n "speculate") (r "^0.0.18") (d #t) (k 2)) (d (n "string_cache") (r "^0.2.0") (d #t) (k 0)) (d (n "tendril") (r "^0.1.6") (d #t) (k 0)))) (h "0w6md09v6dnk02185fpx7vj6iy4yy65sqqyk8gqkl39j3cvfsx3d")))

(define-public crate-select-0.3.0 (c (n "select") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.5.3") (d #t) (k 0)) (d (n "speculate") (r "^0.0.19") (d #t) (k 2)) (d (n "string_cache") (r "^0.2.11") (d #t) (k 0)) (d (n "tendril") (r "^0.2.2") (d #t) (k 0)))) (h "1k8zwav1y0p8fgw3d2q8z2y5vfki477p3dqbfgk5z25qs5ypd43w")))

(define-public crate-select-0.4.0 (c (n "select") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "html5ever") (r "^0.14") (d #t) (k 0)) (d (n "html5ever-atoms") (r "^0.2") (d #t) (k 0)) (d (n "speculate") (r "^0.0.25") (d #t) (k 2)) (d (n "string_cache") (r "^0.4") (d #t) (k 0)) (d (n "tendril") (r "^0.2") (d #t) (k 0)))) (h "06d69r59jjas9zfhyhglca3m03q6k2vr3vjnihn0arn1hyl3ksp6")))

(define-public crate-select-0.4.1 (c (n "select") (v "0.4.1") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "html5ever") (r "^0.18") (d #t) (k 0)) (d (n "speculate") (r "^0.0.25") (d #t) (k 2)))) (h "0kskmwjwv6j4g20fm1dwnhdvdbpgmfb79vz2xc5yjgp7v2w4r5js")))

(define-public crate-select-0.4.2 (c (n "select") (v "0.4.2") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "html5ever") (r "^0.18") (d #t) (k 0)) (d (n "speculate") (r "^0.0.25") (d #t) (k 2)))) (h "0in9q7k069cml7m78pg22xmcjxx5ccdsx8xxkbi3186hhwl2j13h")))

(define-public crate-select-0.4.3 (c (n "select") (v "0.4.3") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.23") (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "0sz7bf6jlyl6flh4rvcmr67q773rq96lxlzqj0gx2211qrc5jr5c")))

(define-public crate-select-0.5.0 (c (n "select") (v "0.5.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "1dvnb12fqczq0mqgyh7pafkhngv8478x0y3sxy5ngj7w1bwn3q4f")))

(define-public crate-select-0.6.0-alpha.1 (c (n "select") (v "0.6.0-alpha.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "155qwdgdsdx0avdpbrd924c0gx6qz06qpzmc414xclmgl8d9al9b")))

(define-public crate-select-0.6.0 (c (n "select") (v "0.6.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "1kadp6rrgp7gks4jji2dhwbc7kpz5ixgzzybfiixppzlqffs17bg")))

