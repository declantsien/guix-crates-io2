(define-module (crates-io se le selenium-rs) #:use-module (crates-io))

(define-public crate-selenium-rs-0.1.0 (c (n "selenium-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "0r8bcjd9qbk1zairgl6hyicm9177ck66pa27wi6pg4l19pdscimp")))

(define-public crate-selenium-rs-0.1.1 (c (n "selenium-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1gz8izvvk56k1slrqgvcrlhc3fc9vp5cxi6i8lym4kixx42kfszd")))

(define-public crate-selenium-rs-0.1.2 (c (n "selenium-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "020i6177xpxphzwmv5fkxg63j6faa3cn1rm56jjsykz1fk93l5q1")))

