(define-module (crates-io se le selection_sort) #:use-module (crates-io))

(define-public crate-selection_sort-0.1.0 (c (n "selection_sort") (v "0.1.0") (h "1q6bh0z33syy75fvvxf6w3d6z7nldh7rbrdw8gqi2c3nqv394dq5")))

(define-public crate-selection_sort-1.0.0 (c (n "selection_sort") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 0)))) (h "0wzafzidsjf3z0p1h96rqjpxpvkmsgkhbc9ajvnq0lldrmn0gbzh")))

(define-public crate-selection_sort-1.1.0 (c (n "selection_sort") (v "1.1.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 0)))) (h "1jh6qng6hamsixsdzn068vd1gl93amrp52api8dkmpamq923qqc3")))

