(define-module (crates-io se le select-random-fastx) #:use-module (crates-io))

(define-public crate-select-random-fastx-0.1.0 (c (n "select-random-fastx") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1akn67fc1y5dwhkv8sjb69bii787fla24f8cj1p7843cqcw69f9j") (r "1.60.0")))

(define-public crate-select-random-fastx-0.1.1 (c (n "select-random-fastx") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1l4g3np51dzlmv2aj7rcf5fmg03n8ijwcbh32lbff2x29yi5qxd6") (r "1.60.0")))

