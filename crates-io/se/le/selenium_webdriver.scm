(define-module (crates-io se le selenium_webdriver) #:use-module (crates-io))

(define-public crate-selenium_webdriver-0.1.0 (c (n "selenium_webdriver") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mpb779c9ckgxb6g8sqw0xhz1jp9bdlx5fy6l2kmalanrhd7zqrq")))

(define-public crate-selenium_webdriver-0.1.1 (c (n "selenium_webdriver") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19v3d3aaj5ya15f2wpx5mqz55010ys6wmc3xai6kfahv4dwx6n1d")))

(define-public crate-selenium_webdriver-0.1.2 (c (n "selenium_webdriver") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "077d2458hv1pvdvl9c4i09h2k3znai2fldblmyxlbaw2vv6fz8ga")))

(define-public crate-selenium_webdriver-0.1.3 (c (n "selenium_webdriver") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iqwvvf741basl4g2lan3gk6db2zsl2sbqc5s6ip2zzl149ih5f1")))

(define-public crate-selenium_webdriver-0.1.4 (c (n "selenium_webdriver") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13hhprx4ji8jmidbwiw1m6q0kvjij5w91xicbkw22r949kqpi9r8")))

(define-public crate-selenium_webdriver-0.1.5 (c (n "selenium_webdriver") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jqgciz36zrr2c89d9rk683abmvdgiar9gylj3rfdvy17xf1vh9b")))

(define-public crate-selenium_webdriver-0.1.6 (c (n "selenium_webdriver") (v "0.1.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n9x6a7pvd5014fa1dvfm3v7hj30jlg488193r317bwjplip7zmg")))

(define-public crate-selenium_webdriver-0.1.7 (c (n "selenium_webdriver") (v "0.1.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ns232sx33ps5g791chk1l4bvxyniciq5j7hm4blacdcrkh4akhv")))

