(define-module (crates-io se le selector) #:use-module (crates-io))

(define-public crate-selector-1.0.0 (c (n "selector") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)))) (h "148irrm1rf4n91xr7apgfba5i9wvan5rhi8cgrs01rmb7pv1aihy")))

(define-public crate-selector-1.0.1 (c (n "selector") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)))) (h "0g1rq4qx8akcj4rvi0csfjm3bqvvwyb2phc1hnjw70pbpv8cvz5m")))

(define-public crate-selector-1.1.0 (c (n "selector") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)))) (h "0wrjhjc0x865hjxbnic5m529dc60fhsamyzs56smklvnj9p9x0r9")))

(define-public crate-selector-1.2.0 (c (n "selector") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)))) (h "05g4dqvirm6h0hrglpjmqspiahd5rprsjqa2m4cqyw3zcks3xfx2")))

