(define-module (crates-io se le select_color) #:use-module (crates-io))

(define-public crate-select_color-0.0.0 (c (n "select_color") (v "0.0.0") (d (list (d (n "image") (r "~0.2.0") (d #t) (k 0)))) (h "1fyrzkplyayi8qfawbn4jq0r7j9jzfgsl5lk22g5y2rlrczaq2lz")))

(define-public crate-select_color-0.1.0 (c (n "select_color") (v "0.1.0") (d (list (d (n "image") (r "^0.3.11") (d #t) (k 0)))) (h "10g1y4agl4n9rcfgrr1q5053akcpkbi53nqd1haqc9kspv0dm2iq")))

(define-public crate-select_color-0.2.0 (c (n "select_color") (v "0.2.0") (d (list (d (n "image") (r "^0.4.0") (d #t) (k 0)))) (h "015zzvk6d7wgil42xhwg3r0pp0cavi1i80c2n2zkp5bsmn7dfic8")))

(define-public crate-select_color-0.3.0 (c (n "select_color") (v "0.3.0") (d (list (d (n "image") (r "^0.5.0") (d #t) (k 0)))) (h "1b1m6kd9i38ps0fcapk1sdjd1ks9h7c4197q5yhn6fl5m01im1b8")))

(define-public crate-select_color-0.4.0 (c (n "select_color") (v "0.4.0") (d (list (d (n "image") (r "^0.6.0") (d #t) (k 0)))) (h "0w6g5dmlh0kibc7l02jdi73gmqrmjz3ddcl4pawx675w7gr7zxyw")))

(define-public crate-select_color-0.5.0 (c (n "select_color") (v "0.5.0") (d (list (d (n "image") (r "^0.7.0") (d #t) (k 0)))) (h "15bw02rs8zrjs4s51hjpq1gf320kq527yi6c2hmczn5aqdd7xzc6")))

(define-public crate-select_color-0.6.0 (c (n "select_color") (v "0.6.0") (d (list (d (n "image") (r "^0.8.0") (d #t) (k 0)))) (h "0czsdvhx5x633b4p6jk8y8f614q3nvv4h2i217bfgf5ha4azdqcv")))

(define-public crate-select_color-0.7.0 (c (n "select_color") (v "0.7.0") (d (list (d (n "image") (r "^0.9.0") (d #t) (k 0)))) (h "18x2mg09xshfjl4yv8cdg2ynwbckjrc49qaisp361yfwmdzh0b1q")))

(define-public crate-select_color-0.8.0 (c (n "select_color") (v "0.8.0") (d (list (d (n "image") (r "^0.10.0") (d #t) (k 0)))) (h "1jziipc3riw2va7n0v69qxn5cpqip3gbq117z3pwh0h8cw6h8g54")))

(define-public crate-select_color-0.9.0 (c (n "select_color") (v "0.9.0") (d (list (d (n "image") (r "^0.12.0") (d #t) (k 0)))) (h "13ks54fnslijhm88wa9zghf6iiffcqb2lnq2ymy2bffvlipwyil1")))

(define-public crate-select_color-0.10.0 (c (n "select_color") (v "0.10.0") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)))) (h "14n667abswvp1lr7miqbh4anx3faaw7w9pm9xihlhx1ww56g27cl")))

(define-public crate-select_color-0.11.0 (c (n "select_color") (v "0.11.0") (d (list (d (n "image") (r "^0.14.0") (d #t) (k 0)))) (h "08gpqvizg5y8xl5cr9x77aq0dpm6z0kcjdqlsv6fx8545shw2y9m")))

(define-public crate-select_color-0.12.0 (c (n "select_color") (v "0.12.0") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 0)))) (h "0v9hp7scqf1rxhz0mmba3d3h4946cgsqk3w2p93mnzgaj7b1fvdh")))

(define-public crate-select_color-0.13.0 (c (n "select_color") (v "0.13.0") (d (list (d (n "image") (r "^0.16.0") (d #t) (k 0)))) (h "190zqmfli1nfl6pdg9rvfifswz4jwfazgr7q05fvril52gg9ag58")))

(define-public crate-select_color-0.14.0 (c (n "select_color") (v "0.14.0") (d (list (d (n "image") (r "^0.17.0") (d #t) (k 0)))) (h "0avh9b1p9b7jkp0h2lqr32i6g84ynp55lyljcnqsxlxrmrxhgb0l")))

(define-public crate-select_color-0.15.0 (c (n "select_color") (v "0.15.0") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "0hl4vd0j7dk8kp5iv9mdxkczigcj7z5s93ws1n2b6n185nddq0wd")))

(define-public crate-select_color-0.16.0 (c (n "select_color") (v "0.16.0") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0blkyvcarnc0ggcck9jh7mi5s4ja4ppwccn2qxg8bl5aihi143n8")))

(define-public crate-select_color-0.17.0 (c (n "select_color") (v "0.17.0") (d (list (d (n "image") (r "^0.20.0") (d #t) (k 0)))) (h "1dsn0vqvz299ma0nffgsfnb8xplbq1v3izjy41hwnm0p30mgr59h")))

(define-public crate-select_color-0.18.0 (c (n "select_color") (v "0.18.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "0wsbzzvw5669sjf4fxkaw7pv7f28arvpnmg2rzjhq5fzl2ls2141")))

(define-public crate-select_color-0.19.0 (c (n "select_color") (v "0.19.0") (d (list (d (n "image") (r "^0.22.0") (d #t) (k 0)))) (h "046sxn6wn8cl9q09irfb02bsdq70p789052g0az28l8mj0vfcp4r")))

(define-public crate-select_color-0.20.0 (c (n "select_color") (v "0.20.0") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "14zhx3lsa5bbjsqazjsmw9bd1s4fyxw9ixj2ppshhica5nlyh2pd")))

(define-public crate-select_color-0.22.0 (c (n "select_color") (v "0.22.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "18z92bx124wl5ajkpddd604nv27x6x2w6fgfcqlya2din4lz3n8v")))

