(define-module (crates-io se le select-macro) #:use-module (crates-io))

(define-public crate-select-macro-0.1.0 (c (n "select-macro") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "select-macro-utils") (r "^0.1.0") (d #t) (k 0)))) (h "0bc98f53k8lxb50yk1zndgv8jcprwb2pjyi12mhksnd7z91bhfbk")))

(define-public crate-select-macro-0.1.1 (c (n "select-macro") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "select-macro-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "18n65a814f7007wpiqnxs2i4dds3cr95q3ry20vpzmbx9g4iiqhf")))

(define-public crate-select-macro-0.1.2 (c (n "select-macro") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "select-macro-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10vkyl9ia267mc4s1p6zlnhsbwbsvw0f9m1d2ls92rhci50j75xc")))

(define-public crate-select-macro-0.2.0 (c (n "select-macro") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "select-macro-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1lc8r5ivks6zdmjnmgix7dbp5k7kk6rhbj07994zfaz60azc0ps0")))

