(define-module (crates-io se le select-html) #:use-module (crates-io))

(define-public crate-select-html-0.1.0 (c (n "select-html") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0r04fr40xw5w2d1nwx6i2a6qby4qjc361m363pmrzi9i9mv7d8sw")))

(define-public crate-select-html-0.1.1 (c (n "select-html") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1330q9cnmxlvk3pr4fh743sl9spf0f8ahzdpnakcib5fclkjwq4c")))

(define-public crate-select-html-0.1.2 (c (n "select-html") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "14zdzpx6v9fa0bx99nzh5cpfdkl8gxhfjjqv26mc76gaasibr97h")))

