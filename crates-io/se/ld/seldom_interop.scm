(define-module (crates-io se ld seldom_interop) #:use-module (crates-io))

(define-public crate-seldom_interop-0.0.1 (c (n "seldom_interop") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0qil56if4j24qnl66qpb644h433yh6a80v7cm6n3l1rzplcvy635")))

(define-public crate-seldom_interop-0.1.0 (c (n "seldom_interop") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)))) (h "0nlqg249jh15bmyq02jnqbv74mqz0c8zpgmr07np81lf8nd0nzcb")))

(define-public crate-seldom_interop-0.2.0 (c (n "seldom_interop") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "1s7551w4pvjlpd64m3gydngi5zhg0pjxm7f0575gyz66lg1nx08w")))

(define-public crate-seldom_interop-0.3.0 (c (n "seldom_interop") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "0mb1dyh7r15yq61ymyxmgxjrd4ax258x3kk59dpqr4ihn5xz65wx")))

(define-public crate-seldom_interop-0.4.0 (c (n "seldom_interop") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)))) (h "1faw7awxdf5k9088zmhlr3gznnxxkjhqas559cv3nxiy8rz0kbay")))

(define-public crate-seldom_interop-0.5.0 (c (n "seldom_interop") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)))) (h "149b5cnl8h9vk6rzq8y8qdv9qiz59ca90xp5pz7sh5ky6wsqvk8j")))

