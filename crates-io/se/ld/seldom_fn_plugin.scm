(define-module (crates-io se ld seldom_fn_plugin) #:use-module (crates-io))

(define-public crate-seldom_fn_plugin-0.1.0 (c (n "seldom_fn_plugin") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)))) (h "0w3vfx4k2x11391aqb5bpzqfgdhizvmf14gzz657z310hv43q7xx")))

(define-public crate-seldom_fn_plugin-0.2.0 (c (n "seldom_fn_plugin") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9") (k 0)))) (h "04b66hnhpk8fv8pvdcgc667kdrpr2rxf8q39n4634r3alnxjzc5c")))

(define-public crate-seldom_fn_plugin-0.3.0 (c (n "seldom_fn_plugin") (v "0.3.0") (d (list (d (n "bevy") (r "^0.10") (k 0)))) (h "10h1bbm7bivw9f6x70djlhv8b1kk8kxg16g421agbi2l66f9rqwz")))

(define-public crate-seldom_fn_plugin-0.4.0 (c (n "seldom_fn_plugin") (v "0.4.0") (d (list (d (n "bevy") (r "^0.11") (k 0)))) (h "0indw2bld3rf65847aydxlimzf7l0c8lsib4n0yizkhq8lnqzdp8")))

(define-public crate-seldom_fn_plugin-0.5.0 (c (n "seldom_fn_plugin") (v "0.5.0") (d (list (d (n "bevy") (r "^0.12") (k 0)))) (h "13h4039zk2vhwlv1h1bcx21ay2jnkaiawc9cmm7y55kgj2aka969")))

(define-public crate-seldom_fn_plugin-0.6.0 (c (n "seldom_fn_plugin") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)))) (h "0fdljli2s5ib97ql7sjdqndqjkqn7lr0mn8ms942355fnx08gz80")))

