(define-module (crates-io se ld seldom_state) #:use-module (crates-io))

(define-public crate-seldom_state-0.0.1 (c (n "seldom_state") (v "0.0.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "1fzjyv3n7j36v8wfvn910lh9bhdx16sqa4y20jng6blyza579azw")))

(define-public crate-seldom_state-0.1.0 (c (n "seldom_state") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0fk8n41fwwym9gzamidxmfwnb4qwn7gijkhqpza6j2sz6s75a5i8")))

(define-public crate-seldom_state-0.2.0 (c (n "seldom_state") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0j47sli8nvdhdzy4fgvik5zq3n7xqlpwfv4ncg5cgvhgpzfkvqx7")))

(define-public crate-seldom_state-0.2.1 (c (n "seldom_state") (v "0.2.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "1d0vglvzzs1d5nsn6nxks53kr2gk7sxaspxln0jsk7z74hqp8vji")))

(define-public crate-seldom_state-0.2.2 (c (n "seldom_state") (v "0.2.2") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.1") (d #t) (k 0)))) (h "0y9m659dpz22zknl2bz0gqp0pbxshls55pkyghxlpp4yky29qm23")))

(define-public crate-seldom_state-0.3.0 (c (n "seldom_state") (v "0.3.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.2") (d #t) (k 0)))) (h "1yl0s51j1kzdyvsjwf36xgwf2w0avc4xrnrcdais17lw4hlg6p51")))

(define-public crate-seldom_state-0.4.0 (c (n "seldom_state") (v "0.4.0") (d (list (d (n "as-dyn-trait") (r "^0.2") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "leafwing-input-manager") (r "^0.8.0") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.8.0") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.2") (d #t) (k 0)))) (h "00nyaxvdxga3z3syqsk846y188my3r6sgsw6dlq61f2w601wkd4y") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.5.0 (c (n "seldom_state") (v "0.5.0") (d (list (d (n "as-dyn-trait") (r "^0.2") (d #t) (k 0)) (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "leafwing-input-manager") (r "^0.9") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.3") (d #t) (k 0)))) (h "07fs92lzrz0rpb267yrimjcac5x822fficlascw6s7gb5as9xz8q") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.6.0 (c (n "seldom_state") (v "0.6.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.3") (d #t) (k 0)))) (h "0ggw6sq0409bhj38zgn190z10cq2124d4vvdiparv6nklfcgv7bz") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.6.1 (c (n "seldom_state") (v "0.6.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.3") (d #t) (k 0)))) (h "1fs6xk3b5i3627ayhxkxap2wkpz38vx1sgl2ff6l9xap2kbx6l0g") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.6.2 (c (n "seldom_state") (v "0.6.2") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.3") (d #t) (k 0)))) (h "1lx50bh6ir1mjfi2yypj8bqf1hfqvzyi60r3543s84k53y2vy7qw") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.6.3 (c (n "seldom_state") (v "0.6.3") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.9") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.3") (d #t) (k 0)))) (h "0aib08gdklq9b2gfdq57ibv4qb8kpqbj97ik6q3z8q4pp9vgxp9g") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.7.0 (c (n "seldom_state") (v "0.7.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.10") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.10") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.4") (d #t) (k 0)))) (h "1c62b027z2pjdydzj2ybb6z9s6rvly9y9l6zib2f7njah85jrmv5") (f (quote (("default" "leafwing_input")))) (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.8.0 (c (n "seldom_state") (v "0.8.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.1") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.1") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.5.0") (d #t) (k 0)))) (h "19czlzgqwh7hbqkkhfw5skyg8z9wzijr4gf0144pngcwrir7r56y") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.9.0 (c (n "seldom_state") (v "0.9.0") (d (list (d (n "bevy") (r "^0.12.0") (k 0)) (d (n "bevy") (r "^0.12.0") (d #t) (k 2)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.1") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.11.1") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.5.0") (d #t) (k 0)))) (h "0n4lscp5nv0g7qvng43svgfhhv6fka8iklsgrqh52ybxwhi1k10w") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

(define-public crate-seldom_state-0.10.0 (c (n "seldom_state") (v "0.10.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (d #t) (k 2)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13.0") (o #t) (k 0)) (d (n "leafwing-input-manager") (r "^0.13.0") (d #t) (k 2)) (d (n "seldom_fn_plugin") (r "^0.6.0") (d #t) (k 0)))) (h "18fwbbxdnxrbc7aaqk8rkmgygfjh15gf44abyl5672sqkbphwzap") (s 2) (e (quote (("leafwing_input" "dep:leafwing-input-manager"))))))

