(define-module (crates-io se c1 sec1_decode) #:use-module (crates-io))

(define-public crate-sec1_decode-0.1.0 (c (n "sec1_decode") (v "0.1.0") (d (list (d (n "der") (r "^0.4.1") (f (quote ("derive" "oid"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "08xgi3hvk4264wip7z9dk6cg3jnwj84c7ch0j9rs0y33jpf6scmn")))

