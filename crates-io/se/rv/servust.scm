(define-module (crates-io se rv servust) #:use-module (crates-io))

(define-public crate-servust-0.1.0 (c (n "servust") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mnzfgibjw3scqgs7ln61i9vi8gq6ix178rnd9p1rygb92390wyl")))

(define-public crate-servust-0.1.1 (c (n "servust") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0480k89kggrbpvx127972iwljdf5a20cghmdplgfcwjsjapmmn2f")))

(define-public crate-servust-0.1.2 (c (n "servust") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bz2462jxq0bma5xlkcy0xmbqlhppw2fhffa8hwmh98qqyzihg1s")))

(define-public crate-servust-0.1.4 (c (n "servust") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0spm2r172kw12h0lanypibhpym8afzab1nmjwv3k8c1w29sm6zql")))

(define-public crate-servust-0.1.5 (c (n "servust") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a6fqg0j50s5plsbgppnadag42d3gwyh5i4pyi4q0jkjx5jcsfqp")))

(define-public crate-servust-0.2.0 (c (n "servust") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hw39c5zd5qcjljww8xpc5hn45r9n5wzpb41afpbhwyhk88y2apx")))

(define-public crate-servust-0.2.1 (c (n "servust") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2fh9yr3ahn9c3cl1fv4gl0lpbyf73n23111v12qzxw32xmfzm5")))

(define-public crate-servust-0.2.2 (c (n "servust") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1f53w6mid7skii8wg4ndq2439rqmfn7g25sky3skfnys2vw506jf")))

(define-public crate-servust-0.2.3 (c (n "servust") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ys30j8m3x0rav8q912h7s09sdkvhsmcnqvp0r0s1bjsmp20f7sy")))

(define-public crate-servust-0.2.5 (c (n "servust") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1widh2v6pxk2vj5nyb6ah0f889zarb8wvf34g1nh23lky26r2ghh")))

(define-public crate-servust-0.2.6 (c (n "servust") (v "0.2.6") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r4yhdjdcgmdy99x8lwifjld0l1kmpkfvvidlpgf3dg01xh4r69f")))

