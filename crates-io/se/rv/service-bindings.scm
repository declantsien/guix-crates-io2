(define-module (crates-io se rv service-bindings) #:use-module (crates-io))

(define-public crate-service-bindings-1.0.0 (c (n "service-bindings") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "102irnsc609pxch2l3gvfmw084l11pgbdz38wxfg4iqpwwclwj1r")))

