(define-module (crates-io se rv servo-freetype-sys) #:use-module (crates-io))

(define-public crate-servo-freetype-sys-2.4.11 (c (n "servo-freetype-sys") (v "2.4.11") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13xjn3biv1f7mmjqqwia0whjnrjbxc7iafzg9pf13nbsjxfpsa69")))

(define-public crate-servo-freetype-sys-4.0.0 (c (n "servo-freetype-sys") (v "4.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19fx4rf4xrvw2csqv0azbn9f28nafwi1hffiahs0q348rn1bzcml")))

(define-public crate-servo-freetype-sys-4.0.1 (c (n "servo-freetype-sys") (v "4.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fmflh471gkhlzz3b4c4psvcwn14r2nkr5m7p5z9hkbhfqnsdkv5")))

(define-public crate-servo-freetype-sys-4.0.2 (c (n "servo-freetype-sys") (v "4.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0y6yvnx5k4vxlrwzzzn668l50dhbxai5b8y57zwygrcgbaf79bsx")))

(define-public crate-servo-freetype-sys-4.0.3 (c (n "servo-freetype-sys") (v "4.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06qd0qh26nwhs6js06s9apk1c8xira22qmn6h818q4c55qn06clj")))

(define-public crate-servo-freetype-sys-4.0.4 (c (n "servo-freetype-sys") (v "4.0.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zy4r7gk3sq42j3slqq4asn0zqnffrh1pjbjiv2jxkb63xwsn02x") (l "freetype")))

(define-public crate-servo-freetype-sys-4.0.5 (c (n "servo-freetype-sys") (v "4.0.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1z0dvnakans4vn4vlpx4nxg984427lh8dskxxz9pglij1mnwnk1c") (l "freetype")))

