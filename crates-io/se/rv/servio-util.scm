(define-module (crates-io se rv servio-util) #:use-module (crates-io))

(define-public crate-servio-util-0.1.0 (c (n "servio-util") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "servio-http") (r "^0.1") (d #t) (k 0)) (d (n "servio-service") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1h5yr404m9w61rxdashm9pgyzrnpzvh4gwg91s04j6n54gvi1w4q") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json")))) (r "1.65")))

