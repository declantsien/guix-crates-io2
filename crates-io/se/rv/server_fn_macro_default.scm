(define-module (crates-io se rv server_fn_macro_default) #:use-module (crates-io))

(define-public crate-server_fn_macro_default-0.2.0 (c (n "server_fn_macro_default") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.2.0") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nywz6h0axi5dx803q82bgbixbjjlxclgh6z04skrz79l14gr569")))

(define-public crate-server_fn_macro_default-0.2.3 (c (n "server_fn_macro_default") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.2.3") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vqb7x3rgcb03b5292jdzrjf8z58c9shp5plgzlnvlv276idwhdx") (f (quote (("stable"))))))

(define-public crate-server_fn_macro_default-0.2.4 (c (n "server_fn_macro_default") (v "0.2.4") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.2.4") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3pimj5s6jrhqq13358hc0v5lkn808ky9s23kfw6qq3wrp0zbam") (f (quote (("stable"))))))

(define-public crate-server_fn_macro_default-0.2.5 (c (n "server_fn_macro_default") (v "0.2.5") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.2.5") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pb5gkkwn52vdm5rh344w653zl6ppl7qdw48dsiqwxg7f5b0vcxa") (f (quote (("stable"))))))

(define-public crate-server_fn_macro_default-0.3.0-alpha (c (n "server_fn_macro_default") (v "0.3.0-alpha") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.3.0-alpha") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hhraap251fzjpnh42nxx84fk221ga8ihl2nji01sy9apvy5gj1z") (f (quote (("stable" "server_fn_macro/stable"))))))

(define-public crate-server_fn_macro_default-0.3.0-alpha2 (c (n "server_fn_macro_default") (v "0.3.0-alpha2") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.3.0-alpha2") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nm39qghidkglzv6iy3ys59id9gj28mgy33kd6lmy80cvcvnbk0h") (f (quote (("stable" "server_fn_macro/stable"))))))

(define-public crate-server_fn_macro_default-0.3.0 (c (n "server_fn_macro_default") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.3.0") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wqw0lhhqj30yymdanazk2px68dwa4ih80r3ny2hknv3979gmcqs") (f (quote (("stable" "server_fn_macro/stable"))))))

(define-public crate-server_fn_macro_default-0.3.1 (c (n "server_fn_macro_default") (v "0.3.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.3.1") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9xzwr4vvzj5qwqk5an95j97l7hq25vq4wxlya48idbaa2sfylr") (f (quote (("stable" "server_fn_macro/stable"))))))

(define-public crate-server_fn_macro_default-0.4.0 (c (n "server_fn_macro_default") (v "0.4.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0nl3d2rvgn2qwh9731w291wrfa1q3pzps19ispgxq8w10b6nmgj8") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.1 (c (n "server_fn_macro_default") (v "0.4.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wvxpjz1m3n0fsfxj3qg358a9p5pwfdd3mi5cfw7pb6mvkhrcl2c") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.2 (c (n "server_fn_macro_default") (v "0.4.2") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1amp4j7adj57zp6hyq47xapclqnl42ymbrmq4qjc1jk1rivnvp95") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.3 (c (n "server_fn_macro_default") (v "0.4.3") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04fh8vwrcy2jmnb7azfaxqdkrxlqnklfms5iqnc1n9zim4s0pykn") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.4 (c (n "server_fn_macro_default") (v "0.4.4") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yglwwn9ybvik7wg4b17izq2rma934kmjp4rnxz2c3cjvx82mrgk") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.5 (c (n "server_fn_macro_default") (v "0.4.5") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.5") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0q3rylrav6z4x1i7l7zj6ggsk9g93apbva61hfjy29xfqxvbp4j8") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.6 (c (n "server_fn_macro_default") (v "0.4.6") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.6") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xzzkfmmr0ba8wc35sgzmyqnzlkpxwqhabqfv8s8j5mbk4cw78k5") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-alpha (c (n "server_fn_macro_default") (v "0.5.0-alpha") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03rkwjk02xs0dbdrrwykp0jy2a3bzavkxg10brjyxix926jhlv6n") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-alpha2 (c (n "server_fn_macro_default") (v "0.5.0-alpha2") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.5.0-alpha2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6r1w83piclsfpi9xhv3fppl8ndm2nnsgfl4jzpjbfq0jqk8iww") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.7 (c (n "server_fn_macro_default") (v "0.4.7") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.7") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0aizxw3s1h93xy19xcaclrmc6ix0b35b4fjg4ahkcsqs0bfqmn4w") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.8 (c (n "server_fn_macro_default") (v "0.4.8") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h89n4rj0cmf5zs964g9bj12a2al9picx5jhhnqk0ykfqqc7pvim") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-beta (c (n "server_fn_macro_default") (v "0.5.0-beta") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.5.0-beta") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mncknzbc7i2kfw7jhwljb559qw7ls6g9lnj7xcxq58mp0hrjhcd") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.9 (c (n "server_fn_macro_default") (v "0.4.9") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mf1rmdrnz5y7s8ss347ph98aq02fky06qav55jq45265zilgd95") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.4.10 (c (n "server_fn_macro_default") (v "0.4.10") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.4.10") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9gg85nlsp571pi58vcly2dgnga4x27c3qkjcqyqfb5vscnrfld") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-beta2 (c (n "server_fn_macro_default") (v "0.5.0-beta2") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "server_fn") (r "^0.2") (d #t) (k 2)) (d (n "server_fn_macro") (r "^0.5.0-beta2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kbzvribfhh3zngw1bykc8yzkqmcp4z3m9bwcdsrncl3x66dbjar") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-rc1 (c (n "server_fn_macro_default") (v "0.5.0-rc1") (d (list (d (n "server_fn_macro") (r "^0.5.0-rc1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "08s9izblmpdmj8mkazapbr770nx35w1a6881kl5z4f4ah1cc973h") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-rc2 (c (n "server_fn_macro_default") (v "0.5.0-rc2") (d (list (d (n "server_fn_macro") (r "^0.5.0-rc2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03j736bmqy3fpqzvb5r1i9k11sz79nlhd3pilzng3ggyflarplas") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0-rc3 (c (n "server_fn_macro_default") (v "0.5.0-rc3") (d (list (d (n "server_fn_macro") (r "^0.5.0-rc3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "030a52rslfa86fayv29xap6njn0j94fa7w9qx46dyc4chv43qk1d") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.0 (c (n "server_fn_macro_default") (v "0.5.0") (d (list (d (n "server_fn_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dr4b6652621lf4gdfzpyws18ggsb4jprhm03ciash6nazrp9s7f") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.1 (c (n "server_fn_macro_default") (v "0.5.1") (d (list (d (n "server_fn_macro") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mmjihywzfbap0280pqi3pjf1akf7b3kcvn233l2nyb3260rpljm") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.2 (c (n "server_fn_macro_default") (v "0.5.2") (d (list (d (n "server_fn_macro") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qpnb7jwk8x2snnl1bmlg595vxjj90m76j3myg9q5pjx3z7zianq") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.3 (c (n "server_fn_macro_default") (v "0.5.3") (d (list (d (n "server_fn_macro") (r "^0.5.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11qfzi7q2rhmp8gaklhmp5qyzncs7p43gj877h1qcaldvwkspvw7") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.4 (c (n "server_fn_macro_default") (v "0.5.4") (d (list (d (n "server_fn_macro") (r "^0.5.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "02zw20c1dzjgy94dri1nvyb4p8y2gdp3f65limcj1cmdvxhvlmkj") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.5.5 (c (n "server_fn_macro_default") (v "0.5.5") (d (list (d (n "server_fn_macro") (r "^0.5.5") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fn1yk59whxby2n9a5pqdfky9wjw8pbsw8laag3x75q2hz98pdpw") (f (quote (("nightly" "server_fn_macro/nightly")))) (y #t)))

(define-public crate-server_fn_macro_default-0.5.6 (c (n "server_fn_macro_default") (v "0.5.6") (d (list (d (n "server_fn_macro") (r "^0.5.6") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "08r5g7idiknhw9xfvp2zkjlkyqg6m1ydg6nqy2rw0kzd9jxr4v4n") (f (quote (("nightly" "server_fn_macro/nightly")))) (y #t)))

(define-public crate-server_fn_macro_default-0.6.0 (c (n "server_fn_macro_default") (v "0.6.0") (d (list (d (n "server_fn_macro") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qw10l0msa4wbaw6wnrrpgkfrvgnkphhr654603xpwnwlfsjczp9") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix")))) (y #t)))

(define-public crate-server_fn_macro_default-0.5.7 (c (n "server_fn_macro_default") (v "0.5.7") (d (list (d (n "server_fn_macro") (r "^0.5.7") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jmrgahlwww4hf912hqxy7gig7bx64vsmznlfh84bk1b5vr56cwa") (f (quote (("nightly" "server_fn_macro/nightly"))))))

(define-public crate-server_fn_macro_default-0.6.0-rc1 (c (n "server_fn_macro_default") (v "0.6.0-rc1") (d (list (d (n "server_fn_macro") (r "^0.6.0-rc1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1064rzh7s62arf5kzkyi1swmaq2z1zs728rscsd93zl9krgsvfwx") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.1 (c (n "server_fn_macro_default") (v "0.6.1") (d (list (d (n "server_fn_macro") (r "^0.6.1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0f5drq12zpawrl65si7x9p7rxbd4b1irlcplj0n3haglcrr2693a") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.2 (c (n "server_fn_macro_default") (v "0.6.2") (d (list (d (n "server_fn_macro") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1cw6i16rnwh9a13h556msxrz3v6xqgc8h0ax6610vcb446hqvhbf") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.3 (c (n "server_fn_macro_default") (v "0.6.3") (d (list (d (n "server_fn_macro") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k7bcm8jwbp7fr1iqmlscp0yp6k80n6869nbc59y5i97gbs9gdmk") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.4 (c (n "server_fn_macro_default") (v "0.6.4") (d (list (d (n "server_fn_macro") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1jzmglq0anzm2q18nyjs1s7jcn8mp62fjfly4nizh7svril3ml99") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.5 (c (n "server_fn_macro_default") (v "0.6.5") (d (list (d (n "server_fn_macro") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1nl1shd65nx2iv240mx776qcm02n92v5mkr2p5ad2cliqj7dfbkm") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.6 (c (n "server_fn_macro_default") (v "0.6.6") (d (list (d (n "server_fn_macro") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1v8c6f3cj9nqw3g0xa6szy5d8jch8qprsspz7nig00sq16zjqww9") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.7 (c (n "server_fn_macro_default") (v "0.6.7") (d (list (d (n "server_fn_macro") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ydis4jky4rpi7wq1ghd4gva3z4fhf3vikngb1h79n6zd7z4h65r") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.8 (c (n "server_fn_macro_default") (v "0.6.8") (d (list (d (n "server_fn_macro") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "04fdw7zdb0rzya0fkifgr6ni0axch9i5f5lhrsgx7ijwxhhla2kc") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.9 (c (n "server_fn_macro_default") (v "0.6.9") (d (list (d (n "server_fn_macro") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "07ipkhnzyawv4pr2gs5r71d0blv534zcb161ziwsflr3yzgg22h6") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.10 (c (n "server_fn_macro_default") (v "0.6.10") (d (list (d (n "server_fn_macro") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bx3s9cicalhdm7zf7mhsnpfxqlxdpxxxp4s989p73wr660h46dg") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.6.11 (c (n "server_fn_macro_default") (v "0.6.11") (d (list (d (n "server_fn_macro") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "17vangf6z5bp3d1fa1ja7i1fw0bkr6w6x4836wqvvkdw1iq13bgl") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.7.0-preview (c (n "server_fn_macro_default") (v "0.7.0-preview") (d (list (d (n "server_fn_macro") (r "^0.7.0-preview") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0961j0sjm2sg0s4vx20zvsdc7gny1r6gwn5j41iz1zyrfpliphf2") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

(define-public crate-server_fn_macro_default-0.7.0-preview2 (c (n "server_fn_macro_default") (v "0.7.0-preview2") (d (list (d (n "server_fn_macro") (r "^0.7.0-preview2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0faki7f5dbsj5r2dsfxpp3hql8dv8m1y498gsiigi2miyh0iqhv7") (f (quote (("ssr" "server_fn_macro/ssr") ("nightly" "server_fn_macro/nightly") ("axum" "server_fn_macro/axum") ("actix" "server_fn_macro/actix"))))))

