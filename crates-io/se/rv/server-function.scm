(define-module (crates-io se rv server-function) #:use-module (crates-io))

(define-public crate-server-function-0.1.0 (c (n "server-function") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xrl58ybz5d3l3jg9ssmzhlmyglaf502kyrxirqhnlz10ajzik0k")))

(define-public crate-server-function-0.1.1 (c (n "server-function") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)))) (h "0baf3g4qqwh11fc7a4z2kffpcql0qlihhg7icng1j28jn4x5dr0q")))

(define-public crate-server-function-0.1.2 (c (n "server-function") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zxl2s6fdy5qv0pkz7ml0aflbp57gcsrvpbq7nkp3h2vazfmxcxc")))

(define-public crate-server-function-0.1.3 (c (n "server-function") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jhzxj7dsvvx62zjzs8v44giqq224skmiddd82dl6h6fggdn9z3k") (s 2) (e (quote (("messagepack" "dep:rmp-serde"))))))

