(define-module (crates-io se rv serveft) #:use-module (crates-io))

(define-public crate-serveft-0.1.0 (c (n "serveft") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "astra") (r "^0.3.0") (d #t) (k 0)) (d (n "libft") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "13xvkc63ll3rvvr7k5402f1p71bn0h6r7q55irlm210apzv65s39")))

