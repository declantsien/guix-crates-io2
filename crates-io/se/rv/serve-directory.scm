(define-module (crates-io se rv serve-directory) #:use-module (crates-io))

(define-public crate-serve-directory-0.1.0 (c (n "serve-directory") (v "0.1.0") (d (list (d (n "build_html") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "local_ipaddress") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)))) (h "097w232ld9f337p9dj8si5kkj9crj3l0smc8c7fkgc9m04mzj35d")))

