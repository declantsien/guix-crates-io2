(define-module (crates-io se rv servant-codec) #:use-module (crates-io))

(define-public crate-servant-codec-0.1.0 (c (n "servant-codec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures_codec") (r "^0.3.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0-alpha-1") (d #t) (k 2)))) (h "0kr9lk1i0hc3jhpxs31s8iqcx06dkh433k46iz9mhib7bmlmd7h0")))

