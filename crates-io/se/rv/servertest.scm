(define-module (crates-io se rv servertest) #:use-module (crates-io))

(define-public crate-servertest-0.1.0 (c (n "servertest") (v "0.1.0") (h "18pgz6vsdf38mxvhkl2bv3zq0dzndqs6lgbr7xsk2g2d6k922mwi")))

(define-public crate-servertest-0.1.1 (c (n "servertest") (v "0.1.1") (h "1wbs987krgs21p6xxqj2zly11lw6pvam7g18ffznrk3cz55vdlwm")))

