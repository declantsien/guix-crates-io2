(define-module (crates-io se rv servant-macro) #:use-module (crates-io))

(define-public crate-servant-macro-0.1.0 (c (n "servant-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("default"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0-alpha-1") (d #t) (k 2)))) (h "032bgid5d5mcdfbhap5qfjq7rwd0lybn3j8545cg8kvp3d7k88fx") (f (quote (("terminal") ("report") ("query") ("notify") ("invoke") ("adapter"))))))

