(define-module (crates-io se rv service_book) #:use-module (crates-io))

(define-public crate-service_book-0.1.0 (c (n "service_book") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pnil2rz4cs6srl92bbw49g4qd7k9fsz6bb047lbffbxqrwwzmvz") (f (quote (("discover" "reqwest"))))))

(define-public crate-service_book-0.1.1 (c (n "service_book") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0d1gnh5ynss2cbi3rg420fijjn4qws0an2p2iq4lnij6mnsb4czi") (f (quote (("discover" "reqwest"))))))

(define-public crate-service_book-0.1.2 (c (n "service_book") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1d45hdlc5ss94pq72gj8kh2w52qhhk1h2c63amwgd0wng81rk8xc") (f (quote (("discover" "reqwest"))))))

