(define-module (crates-io se rv serviceless) #:use-module (crates-io))

(define-public crate-serviceless-0.1.0 (c (n "serviceless") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "sync" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ysxwc3qz2wkvshz2pnsy8mgg8hzsrbz8fr4nv77qx6wm20p7351")))

(define-public crate-serviceless-0.1.1 (c (n "serviceless") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "sync" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12lhzkmrxlgvpk4yqckxbl0qmh65fpy8im89js5vgiyaks3lr8p4")))

(define-public crate-serviceless-0.1.2 (c (n "serviceless") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "sync" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "067fgjx1xdvpv52siqpr50qsfvsrgkz9ddjb2lgz9glsv438spqf")))

