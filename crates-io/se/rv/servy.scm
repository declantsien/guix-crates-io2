(define-module (crates-io se rv servy) #:use-module (crates-io))

(define-public crate-servy-0.2.0 (c (n "servy") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "hyper") (r "^0.11.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1yfilln521brb28l5dgpdyi31ndxf7f59vcfs4dsy8gyjk2bgpqj")))

(define-public crate-servy-0.3.0 (c (n "servy") (v "0.3.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "hyper") (r "^0.11.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1wdjyj6k9x8zvpc8529avhdd04c9h32k30dgridw7xfqc5pivl4p")))

(define-public crate-servy-1.0.0 (c (n "servy") (v "1.0.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "hyper") (r "^0.11.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "10cn8amv1941is1qy0411y9zp5cn5ls6lwylxg4sgjbsjsndr6xd")))

(define-public crate-servy-1.0.1 (c (n "servy") (v "1.0.1") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "hyper") (r "^0.11.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0zdn8328wk9f56shs3ig49jsp28h39zf4gb9brvp8gvgmqc5jai3")))

