(define-module (crates-io se rv service_bus) #:use-module (crates-io))

(define-public crate-service_bus-0.0.1 (c (n "service_bus") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "seda_bus") (r "^0.0.1") (d #t) (k 0)))) (h "1dnl0b5nlw7wkwb4rf4m8yv0i17frbjmizrbx03j74dqg18fznzf")))

(define-public crate-service_bus-0.0.2 (c (n "service_bus") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)) (d (n "seda_bus") (r "^0.0.1") (d #t) (k 0)))) (h "1xz4ri89x42q3405xinkmmfgs3yqcm3fx7f4jdya49p5ikc08n2x")))

(define-public crate-service_bus-0.1.0 (c (n "service_bus") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.0") (d #t) (k 0)) (d (n "seda_bus") (r "^0.1.7") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1k99qiz1hp9a9sd8j8vgbnmsdm12nvrc9y9hgir6zf26za4ww8x9")))

(define-public crate-service_bus-0.1.1 (c (n "service_bus") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.1") (d #t) (k 0)) (d (n "seda_bus") (r "^0.1.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0zvgw4q82p9309ll4mmg0gi44w1jm6gabqb5s5s5h9ijpmaxj05j")))

(define-public crate-service_bus-0.1.2 (c (n "service_bus") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.1") (d #t) (k 0)) (d (n "seda_bus") (r "^0.1.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0x10l2smzzjdi5z6zk924j6kdigmkdbijfryai0rfjfa7sraxzxn")))

(define-public crate-service_bus-0.1.3 (c (n "service_bus") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.1.3") (d #t) (k 0)) (d (n "seda_bus") (r "^0.1.10") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1aws5ff2x9rgk0jdwj7yf8vj10wqyb717pznl1nrxsvqgrjpvqdv")))

