(define-module (crates-io se rv servos) #:use-module (crates-io))

(define-public crate-servos-0.0.1 (c (n "servos") (v "0.0.1") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1kln6r7cjq6xy77rfwaf5cw2pdypmf11v5fd89zfkhykzk4ankkx")))

(define-public crate-servos-0.0.2 (c (n "servos") (v "0.0.2") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "18fqp1jsljmi6kg5vhxrhdlk2h2wbki57wyga5yn2y4b9wqaknd1")))

(define-public crate-servos-0.0.3 (c (n "servos") (v "0.0.3") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "03v1qqwgzvw77bw3vva4mblpni68my8f01d8vmv3hcrg9ff1v2wm")))

(define-public crate-servos-0.0.4 (c (n "servos") (v "0.0.4") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1q4ad4zizc3vf7ir9q7szi76cdxs96xp2bm7fshrf84hgvgypjdi")))

(define-public crate-servos-0.0.5 (c (n "servos") (v "0.0.5") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1kj2l5i0nly0x05clm9ycbyg7z81wkpmdsilg20ip8nhmbk145ld")))

