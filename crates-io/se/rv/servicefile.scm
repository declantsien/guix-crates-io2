(define-module (crates-io se rv servicefile) #:use-module (crates-io))

(define-public crate-servicefile-0.1.0 (c (n "servicefile") (v "0.1.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "1ssnr9s68zp99zay09ndzwidvkxlx2vrlklw3fgvginn63pdyzpz")))

(define-public crate-servicefile-0.2.0 (c (n "servicefile") (v "0.2.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "1xjldfa1frlighfqpzi77amnam4rwwy9myq8a09vfpl0abb71rl0")))

(define-public crate-servicefile-0.3.0 (c (n "servicefile") (v "0.3.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "1nskxqs2snqshn6065lvc59lv99y5pczli4gkpr3y34hkygfi67c")))

