(define-module (crates-io se rv servo-egl) #:use-module (crates-io))

(define-public crate-servo-egl-0.1.0 (c (n "servo-egl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04lb85a0ngsk31g0mk3zshk3zkpjfx76a5j59zrvdmy197yaini8")))

(define-public crate-servo-egl-0.2.0 (c (n "servo-egl") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p43rb5glsclc1x1cnjg99aka1g46p6k3z5mvlvb4159cgq6rjqn")))

(define-public crate-servo-egl-0.2.1 (c (n "servo-egl") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05qj0nz8wbb76cfz3fkzhpnc927d94w1yplpjvjnxzik9j49l1i1")))

