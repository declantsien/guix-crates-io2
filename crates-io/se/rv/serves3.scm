(define-module (crates-io se rv serves3) #:use-module (crates-io))

(define-public crate-serves3-1.0.0 (c (n "serves3") (v "1.0.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "human-size") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.3") (f (quote ("tera"))) (d #t) (k 0)) (d (n "rust-s3") (r "^0.33") (f (quote ("tokio-native-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 0)))) (h "0jdiykmwc5mlbjyf2c4k3hhqb0n93233jj4jpmn32f3agqywm30s")))

