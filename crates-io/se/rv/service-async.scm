(define-module (crates-io se rv service-async) #:use-module (crates-io))

(define-public crate-service-async-0.1.0 (c (n "service-async") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kksm8xrgvb16115rs36yr63dikgpy4ngcjzsds0f7i27829lbbx")))

(define-public crate-service-async-0.1.1 (c (n "service-async") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sg5ws1ws8nfs3wyl10fgwkzjjjhddif7rbpz16waa2mi08m4sy4")))

(define-public crate-service-async-0.1.2 (c (n "service-async") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0254shpr2kgay17sji4k5wc21ds67z1nyqnz088d4ci8z26pijf8")))

(define-public crate-service-async-0.1.3 (c (n "service-async") (v "0.1.3") (d (list (d (n "monoio") (r "^0.1.4") (d #t) (t "cfg(unix)") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0z72dlh4xw8r70x2j82da9s7k0ar996w9jcvch50z5zy85zc9lfz")))

(define-public crate-service-async-0.1.4 (c (n "service-async") (v "0.1.4") (d (list (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "018xyw9h7czj99079xabkjc70pi40zq98flxxd6k71zf79gq23rg")))

(define-public crate-service-async-0.1.5 (c (n "service-async") (v "0.1.5") (d (list (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "1285qqp1n0q98yrnkdl5vsric8y364i019vkxvmcf5jdw1c16vkc")))

(define-public crate-service-async-0.1.6 (c (n "service-async") (v "0.1.6") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "02kbxc1v4hy8lzg9rsfxns0yg8l223dyscyzvhkin49wvn135n7j")))

(define-public crate-service-async-0.1.7 (c (n "service-async") (v "0.1.7") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "1iwadi35kvyx2bxfrd85narci64gwnq5v02rr67v6208q3j0m4r6")))

(define-public crate-service-async-0.1.8 (c (n "service-async") (v "0.1.8") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "06mjq116kx9npcd72whhd03amfwd9x3mzrwxsdrgbr1h1r01fv05")))

(define-public crate-service-async-0.1.9 (c (n "service-async") (v "0.1.9") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0cg6yyxxmwslaqshbnvajmw03j69zpv9l0xpn0wrj7ljjgs7ka1v")))

(define-public crate-service-async-0.1.10 (c (n "service-async") (v "0.1.10") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0dgnydscv4gvbbbp9zwqj2jf8mqd0b07334pfzv1z5kgy19vrbxj")))

(define-public crate-service-async-0.1.11 (c (n "service-async") (v "0.1.11") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "14ba055mppnffxqg5ab4rhnda7lq4vn5xi5hnm73y5ifkspps4mr")))

(define-public crate-service-async-0.1.12 (c (n "service-async") (v "0.1.12") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0bb3fhrpfvw43374f4f4dvs5qvahis99q6xx9g9ywpys8m0jwlwk")))

(define-public crate-service-async-0.1.13 (c (n "service-async") (v "0.1.13") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "monoio") (r "^0.1.5") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0yfsg3yc2fdbdwbpc6gwg0vr5yy19pvqnlysx19wls59dmp7zvzv")))

(define-public crate-service-async-0.2.0 (c (n "service-async") (v "0.2.0") (d (list (d (n "monoio") (r "^0.2.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0iz3x28fjjvnidb8k3smybc1w6fnxwfrjk6904bj8j2dlgvf48w2")))

(define-public crate-service-async-0.2.1 (c (n "service-async") (v "0.2.1") (d (list (d (n "monoio") (r "^0.2.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "19k4v457nwz3smfizqy895yzdghr8i37j5k2l2yx3v5n0rlgv1y4")))

(define-public crate-service-async-0.2.2 (c (n "service-async") (v "0.2.2") (d (list (d (n "monoio") (r "^0.2.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "1sf9yf3kiadc8zvls15gfp1m430s19pz1x4fizgj4yl28328zh74")))

(define-public crate-service-async-0.2.3 (c (n "service-async") (v "0.2.3") (d (list (d (n "monoio") (r "^0.2.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "param") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (t "cfg(not(unix))") (k 2)))) (h "0g46lyywcbmhdpicdirhgxqsl8d848q98mf8734p1hgdwxvj8jhf")))

