(define-module (crates-io se rv server-starter-listener) #:use-module (crates-io))

(define-public crate-server-starter-listener-0.1.0 (c (n "server-starter-listener") (v "0.1.0") (d (list (d (n "actix-web") (r "^1.0.8") (f (quote ("uds"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0kwa83jcah6fi8n1y3brfcpikmiksx770wn5j21wsliyyf8par3n")))

(define-public crate-server-starter-listener-0.1.1 (c (n "server-starter-listener") (v "0.1.1") (d (list (d (n "actix-web") (r "^1.0.8") (f (quote ("uds"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0q0f9zs2li0k5h74miblsl141bx9v4gjwzhxzimazwja31y7kh42")))

(define-public crate-server-starter-listener-0.1.2-alpha.0 (c (n "server-starter-listener") (v "0.1.2-alpha.0") (d (list (d (n "actix-web") (r "^1.0.8") (f (quote ("uds"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0yyaqh6lqwkx9r0053a7kymhmr94d7knxl0d55m8mz3wf1l63z8v")))

(define-public crate-server-starter-listener-0.1.2 (c (n "server-starter-listener") (v "0.1.2") (d (list (d (n "actix-web") (r "^1.0.8") (f (quote ("uds"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "10038w4asr39bvy7mpbda8yszpk8pcd1kdbqhwifrjqs2dfc7akx")))

(define-public crate-server-starter-listener-0.2.0 (c (n "server-starter-listener") (v "0.2.0") (d (list (d (n "actix-web") (r "^1.0.8") (f (quote ("uds"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0n1gi5ssvc4dlk6jhrp5vimpsnr8mgalx49i8nqnq17fh2722p1b")))

