(define-module (crates-io se rv servar) #:use-module (crates-io))

(define-public crate-servar-0.1.0 (c (n "servar") (v "0.1.0") (d (list (d (n "build_html") (r "^2.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "11qdgr5r6babhwm7gcgf2hninl6m61kn1qqwwdldf9mmq2yg9i4q")))

