(define-module (crates-io se rv server-security) #:use-module (crates-io))

(define-public crate-server-security-0.1.0 (c (n "server-security") (v "0.1.0") (h "1yp8kr7i8i3463cdaxa7lpbrr22wa416zsajr27zn4r3k8iz2crm")))

(define-public crate-server-security-0.1.1 (c (n "server-security") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shadow-rs") (r "^0.5") (d #t) (k 0)) (d (n "simple-log") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wsab99khzdi00j5s5in035pbij3a3rmvvmvz7l554dxkkvsmwbv")))

