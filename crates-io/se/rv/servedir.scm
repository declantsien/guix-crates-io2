(define-module (crates-io se rv servedir) #:use-module (crates-io))

(define-public crate-servedir-0.1.0 (c (n "servedir") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "http") (r "^0.1.15") (d #t) (k 0)) (d (n "hyper") (r "^0.12.24") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)) (d (n "nestxml") (r "^0.2.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2.8") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-fs") (r "^0.1.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "01z5rlhwq97040g2rcjnr52cr0x7my3h2jdqyx3jz4c9k8ib1v31")))

