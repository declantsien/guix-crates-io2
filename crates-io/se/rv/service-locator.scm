(define-module (crates-io se rv service-locator) #:use-module (crates-io))

(define-public crate-service-locator-0.1.0 (c (n "service-locator") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ydf9w652sx3pgr7gc1npmzbagdiird68im46j488x5avzhhjv17") (f (quote (("default"))))))

(define-public crate-service-locator-0.2.0 (c (n "service-locator") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "14dzqmr73zbar99ryiw0b11glbhsil89z39vix356yqjhhh79faj") (f (quote (("default" "log"))))))

