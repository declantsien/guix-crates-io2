(define-module (crates-io se rv servo-fontconfig) #:use-module (crates-io))

(define-public crate-servo-fontconfig-0.1.1 (c (n "servo-fontconfig") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^2.11.1") (d #t) (k 0)))) (h "0v7l83i1shdzjs5l43ll2gaxf4cqbnqq3ngmg5qf9isq98d4wgrw")))

(define-public crate-servo-fontconfig-0.2.0 (c (n "servo-fontconfig") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^2.11.1") (d #t) (k 0)))) (h "03sj71v8q4lhj3x3dadpgw90vg8v5wn5v5bcbzrb5f9kkfi2gd0a")))

(define-public crate-servo-fontconfig-0.2.1 (c (n "servo-fontconfig") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0qi2i46hgc9bkcs81rv5wz3h81r46p50x4cq4cvbz8ml96v9kxwk")))

(define-public crate-servo-fontconfig-0.4.0 (c (n "servo-fontconfig") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^4.0.0") (d #t) (k 0)))) (h "1nach6s4hdf86jz5hlm4p5r7vin91cs7gg89mr533id5fpbzi250")))

(define-public crate-servo-fontconfig-0.5.0 (c (n "servo-fontconfig") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^5.0.0") (d #t) (k 0)))) (h "0w1c7vhg77yr5b2jzs54v9aviw4m0p34jsbmij1mbysjkkvgwiqb")))

(define-public crate-servo-fontconfig-0.5.1 (c (n "servo-fontconfig") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-fontconfig-sys") (r "^5.1.0") (d #t) (k 0)))) (h "0z11bjndkp87dnqqmqww6raswgpy7sfh9ahdpx7d0wzxwlpy5qy7") (f (quote (("force_system_lib" "servo-fontconfig-sys/force_system_lib"))))))

