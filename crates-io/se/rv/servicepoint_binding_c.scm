(define-module (crates-io se rv servicepoint_binding_c) #:use-module (crates-io))

(define-public crate-servicepoint_binding_c-0.5.1 (c (n "servicepoint_binding_c") (v "0.5.1") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "servicepoint") (r "^0.5.1") (f (quote ("all_compressions"))) (d #t) (k 0)))) (h "13ry0mj2f4f06rr1wc09wc5mfb1nglqvizmxidc60n2m1ibxv1rg") (l "servicepoint")))

