(define-module (crates-io se rv servex) #:use-module (crates-io))

(define-public crate-servex-0.1.0 (c (n "servex") (v "0.1.0") (d (list (d (n "corex") (r "^0.1.0") (d #t) (k 0)) (d (n "modelx") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0fv2agbll1dia87q5vw6ncy170ij2cvlqgcplc1x0y24fjv0j3h3")))

