(define-module (crates-io se rv servito) #:use-module (crates-io))

(define-public crate-servito-0.1.0 (c (n "servito") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "onnxruntime") (r "^0.0.10") (d #t) (k 0)) (d (n "onnxruntime-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "tract") (r "^0.11.2") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.11.2") (d #t) (k 0)))) (h "08gv7d72q9fcjm7wz1f1q4iygyr6d7q1ydgs5lnyrx6nz2bm90xp")))

