(define-module (crates-io se rv servo-display-link) #:use-module (crates-io))

(define-public crate-servo-display-link-0.2.0 (c (n "servo-display-link") (v "0.2.0") (d (list (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "time-point") (r "^0.1.1") (d #t) (k 0)))) (h "1x55qadqjk7xjbixnbjz383z3pmq360yjnn35rqmxhxvl3i0n1ni")))

