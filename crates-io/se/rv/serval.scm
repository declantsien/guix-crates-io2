(define-module (crates-io se rv serval) #:use-module (crates-io))

(define-public crate-serval-0.1.0 (c (n "serval") (v "0.1.0") (d (list (d (n "combine") (r "^3.6.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "logger") (r "^0.4.0") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "082y8nn2g1a0dnxsqax0izd3vmljnjcnaffn4rwqp57n5hq03fhw")))

