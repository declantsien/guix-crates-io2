(define-module (crates-io se rv servt) #:use-module (crates-io))

(define-public crate-servt-0.1.0 (c (n "servt") (v "0.1.0") (d (list (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "12nxqzm7x7xksm9wdp27y9rycjbbwcxb6vjb3wg1jlgzbmd38q1x") (f (quote (("default" "async")))) (s 2) (e (quote (("async" "dep:smol"))))))

(define-public crate-servt-0.2.0 (c (n "servt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "now"))) (o #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1j4vd6qpwmyr7yfrw9rycahw47cvrflxv74knbvliwaw4a79ylw8") (f (quote (("default" "async" "time")))) (s 2) (e (quote (("time" "dep:chrono") ("async" "dep:smol"))))))

