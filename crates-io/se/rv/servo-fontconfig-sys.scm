(define-module (crates-io se rv servo-fontconfig-sys) #:use-module (crates-io))

(define-public crate-servo-fontconfig-sys-2.11.1 (c (n "servo-fontconfig-sys") (v "2.11.1") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^2.4.11") (d #t) (k 0)))) (h "1kzpyk1y8bqzw7a8bcdph5q04g5gndqpnswh4cr5154ma7skcakr")))

(define-public crate-servo-fontconfig-sys-2.11.2-really.1 (c (n "servo-fontconfig-sys") (v "2.11.2-really.1") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^2.4.11") (d #t) (k 0)))) (h "1m8ij5iqv38ncpbmqbcsj4pjd8mq4l4qdsdf7dhiriw7cvkf63h5")))

(define-public crate-servo-fontconfig-sys-2.11.3 (c (n "servo-fontconfig-sys") (v "2.11.3") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^2.4.11") (d #t) (k 0)))) (h "0f3qlm9yvwh0bxxs2g4vkjccajq2fdp734cfyg5050qybayblg4a")))

(define-public crate-servo-fontconfig-sys-4.0.0 (c (n "servo-fontconfig-sys") (v "4.0.0") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0pcf97jvmg2g2ys2hnwdgpqw8j2hmzipivsbw8lvczqbbqb97lvv")))

(define-public crate-servo-fontconfig-sys-4.0.1 (c (n "servo-fontconfig-sys") (v "4.0.1") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "05p4757pggl203wsds3cjadyvpzzs9i00ziinpy8620y5vx7wncf")))

(define-public crate-servo-fontconfig-sys-4.0.2 (c (n "servo-fontconfig-sys") (v "4.0.2") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0972z950zh79v0844mzmddm03k0mz0h58p3f90hpjij6fx6lmbx0")))

(define-public crate-servo-fontconfig-sys-4.0.3 (c (n "servo-fontconfig-sys") (v "4.0.3") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "13impvvkvbfn0vnlsyn5ari37znxkphpck3pz2xwppkfxrvhgs3b")))

(define-public crate-servo-fontconfig-sys-4.0.4 (c (n "servo-fontconfig-sys") (v "4.0.4") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0fih9ywppqdhi7nc4iwpsvyclif1sxx3hz8f9f8q3vh963q99d1q")))

(define-public crate-servo-fontconfig-sys-4.0.5 (c (n "servo-fontconfig-sys") (v "4.0.5") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "1b8qj4skmp5qpnf5kndjbpzdci7lk786qf1229pvvsm2xgnln3dd")))

(define-public crate-servo-fontconfig-sys-4.0.6 (c (n "servo-fontconfig-sys") (v "4.0.6") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "050s9vj7j4a6gnvsslk50nk4hd60x3502vzkma4g2mfvaq40iamh")))

(define-public crate-servo-fontconfig-sys-4.0.7 (c (n "servo-fontconfig-sys") (v "4.0.7") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "13bv3b9x1mfk3dhiw3qb9s1461y4k0fjilcag70jbgfgvld20vdl") (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-4.0.8 (c (n "servo-fontconfig-sys") (v "4.0.8") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0krya3n2bh7q1sqi60nph1m4j4wwjdjhjgx76c70h6jybjf7lqh2") (y #t) (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-4.0.9 (c (n "servo-fontconfig-sys") (v "4.0.9") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "servo-freetype-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0v0mbicy74wd6cjd5jyqnm4nvrrr5lmg053cn16kylhg8mkf3cv2") (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-5.0.0 (c (n "servo-fontconfig-sys") (v "5.0.0") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r9658krn94jkvzajd7g6cc854wxy5f2gd2dbs6vj8lm6np3zixb") (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-5.0.1 (c (n "servo-fontconfig-sys") (v "5.0.1") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pynwm13q8ckajm1kyzz94znn8nhgqkn3awh7m1nyzwmq918sphw") (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-5.0.2 (c (n "servo-fontconfig-sys") (v "5.0.2") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1iwjbppyn69lz87wkc65mcxqbbr3x327vpfzpp1kbj81m3327jzs") (l "fontconfig")))

(define-public crate-servo-fontconfig-sys-5.1.0 (c (n "servo-fontconfig-sys") (v "5.1.0") (d (list (d (n "expat-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.13.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "125k4hydb4w08568rgazh95n6haxhf5c78axz50glbc9p6fqfsz3") (f (quote (("force_system_lib") ("default")))) (l "fontconfig")))

