(define-module (crates-io se rv servo-dwrote) #:use-module (crates-io))

(define-public crate-servo-dwrote-0.2.0 (c (n "servo-dwrote") (v "0.2.0") (d (list (d (n "gdi32-sys") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.9") (o #t) (k 1)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "129785rjijw5r3yxi7ninvw10bm65k6n9c1l6mjskcizkjkks0cz") (f (quote (("nightly" "serde/unstable") ("default" "codegen") ("codegen" "serde_codegen" "serde_codegen/with-syntex"))))))

