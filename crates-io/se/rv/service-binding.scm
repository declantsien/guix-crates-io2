(define-module (crates-io se rv service-binding) #:use-module (crates-io))

(define-public crate-service-binding-0.1.0 (c (n "service-binding") (v "0.1.0") (h "1kwssj4vn5jl2cw3kkxb239963rrkqsw4qk46cs7r2zaigqslrvk")))

(define-public crate-service-binding-0.1.1 (c (n "service-binding") (v "0.1.1") (h "1n6i4i4sly4ypw7469lwm6kcvr9jnsinpb72saa50xcxlwcjj4a2")))

(define-public crate-service-binding-0.2.0 (c (n "service-binding") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "05crhv7xjaqdb3bfmcc7k2c95anx0g6wmg5lrf0sr7wp58ir8hsm")))

(define-public crate-service-binding-0.3.0 (c (n "service-binding") (v "0.3.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 2)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "0smylk8xzkmnaivv56af00sarh9m6vr89k4anhav4fi4b8p79f9f")))

(define-public crate-service-binding-0.3.1 (c (n "service-binding") (v "0.3.1") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)))) (h "0pfqgq35rfj1lr2i0pzvsx534afjrkdf8zmc6m3z77mmdrhx208q")))

(define-public crate-service-binding-1.0.0 (c (n "service-binding") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "0jvs9c6fbnskhfl8scj6f8n8z4cjk0jflm8sk65w1nh51yrapmqr") (y #t)))

(define-public crate-service-binding-1.0.1 (c (n "service-binding") (v "1.0.1") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "02yab1cd9nmyb4v0l7cmwj098kj183jak4j342124ax4hv5iyb6a")))

(define-public crate-service-binding-1.1.0 (c (n "service-binding") (v "1.1.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "06r29i3g31a02if6ba83b6zqicd0zvj3xv9qppraljh91ak1i14z")))

(define-public crate-service-binding-2.0.0 (c (n "service-binding") (v "2.0.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 2)))) (h "0a14whmi9pk383w6f11bw8swggpjjgpws4nqqfly2kq3yz337xrf")))

(define-public crate-service-binding-2.1.0 (c (n "service-binding") (v "2.1.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "raunch") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0b204ckn2ix7dyvl5ymfkp2sq6gj7yqkw0j1z5i0h58pg4bhwn75")))

(define-public crate-service-binding-3.0.0 (c (n "service-binding") (v "3.0.0") (d (list (d (n "actix-web") (r "^4") (f (quote ("macros"))) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 2)) (d (n "raunch") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "testresult") (r "^0.4.0") (d #t) (k 2)))) (h "1s6n10jcv9prdx33bajv7vdpli7507yv7vc367w3w6c3iffknri5")))

