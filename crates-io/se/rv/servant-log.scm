(define-module (crates-io se rv servant-log) #:use-module (crates-io))

(define-public crate-servant-log-0.1.0 (c (n "servant-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ring-channel") (r "^0.7") (d #t) (k 0)))) (h "08gaydmxg967v4y186x1b49rkk2w5ypdfs6xrzrb3nfjcvdnql7d")))

