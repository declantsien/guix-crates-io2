(define-module (crates-io se rv server_lib) #:use-module (crates-io))

(define-public crate-server_lib-0.1.0 (c (n "server_lib") (v "0.1.0") (d (list (d (n "mongodb") (r "^2.0.0-beta.2") (f (quote ("sync"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1zjsypwlf64w2ffsfa3504mjpz51f47isr2vgnz6v8snh44mkmsh")))

