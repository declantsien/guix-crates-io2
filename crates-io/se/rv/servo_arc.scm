(define-module (crates-io se rv servo_arc) #:use-module (crates-io))

(define-public crate-servo_arc-0.1.0 (c (n "servo_arc") (v "0.1.0") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "0ckxhnkx223822kdj42rc7in2rb59lyc9xl9dbn9yd03j5xq0lxl") (f (quote (("servo" "serde"))))))

(define-public crate-servo_arc-0.1.1 (c (n "servo_arc") (v "0.1.1") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "0cjljr9znwahry6p95xvd3p4pmy24wlf6gbfidnmglg002w3i0nr") (f (quote (("servo" "serde"))))))

(define-public crate-servo_arc-0.2.0 (c (n "servo_arc") (v "0.2.0") (d (list (d (n "nodrop") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "0hgp453zvrj7ry501jhxlcph0hc42gc26zyfwn8ys3yziwps8anm") (f (quote (("servo" "serde") ("gecko_refcount_logging"))))))

(define-public crate-servo_arc-0.3.0 (c (n "servo_arc") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "0i0s9786np106yl6w29bfzwnj29rqak912skcdxcf04yjlddfdnh") (f (quote (("servo" "serde") ("gecko_refcount_logging"))))))

