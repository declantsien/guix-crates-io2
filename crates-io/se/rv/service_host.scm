(define-module (crates-io se rv service_host) #:use-module (crates-io))

(define-public crate-service_host-0.1.0 (c (n "service_host") (v "0.1.0") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1zplqgrij971k5qag555b79jh4i84qpnrxpga3v4inr92d76ng93")))

(define-public crate-service_host-0.2.0 (c (n "service_host") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("macros" "sync" "rt-multi-thread"))) (d #t) (k 0)))) (h "0r4ynp627wp077k564im1iwgwqm5pli9ywhjiwiwsc4b3v56gfvl")))

