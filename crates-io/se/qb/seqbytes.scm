(define-module (crates-io se qb seqbytes) #:use-module (crates-io))

(define-public crate-seqbytes-0.1.0 (c (n "seqbytes") (v "0.1.0") (h "17756vixpf1xm5rvk9mlhrdwszpzmwpa8dyczqy0m6c3rbg3p5p5")))

(define-public crate-seqbytes-0.1.1 (c (n "seqbytes") (v "0.1.1") (h "0pz80k0qyacl63myffrjzydpy7wdx9iz3mv156lj7qmd3jynjjyi")))

