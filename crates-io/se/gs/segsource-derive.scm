(define-module (crates-io se gs segsource-derive) #:use-module (crates-io))

(define-public crate-segsource-derive-0.1.0 (c (n "segsource-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yv0pakjphh99dnw6vgaln98fmvbgcwjvbsylisxcgaihc0pdq96")))

(define-public crate-segsource-derive-0.2.0 (c (n "segsource-derive") (v "0.2.0") (d (list (d (n "here") (r "^1") (d #t) (k 0)) (d (n "pmhelp") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1r6fx88qxar5mc12xrgs9bxqin0gb4vxcxy1d626yslk2m0wirkw") (f (quote (("syn-full" "syn/full"))))))

