(define-module (crates-io se cf secfmt) #:use-module (crates-io))

(define-public crate-secfmt-0.1.0 (c (n "secfmt") (v "0.1.0") (h "1al90r6m2zr77mjlfb8nzbsj8wgv3g4qrvlrnn483vva82pyflqj")))

(define-public crate-secfmt-0.1.1 (c (n "secfmt") (v "0.1.1") (h "0710fwskvbgz5kzsa3c29j47bs5lbhkn27qabmy9j48phqr39iy8")))

