(define-module (crates-io se cd secded) #:use-module (crates-io))

(define-public crate-secded-0.1.0 (c (n "secded") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (f (quote ("i128"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0ib6k8q6sn7dih04lx8c41s46jrmzw2gvk87ls7918qnn3w88gnf") (f (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1.0.0 (c (n "secded") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (f (quote ("i128"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "15lhr691q9hv9i60apb5fkkn91bdgjf973nh48b4ch91ggbfx8a0") (f (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1.0.1 (c (n "secded") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (f (quote ("i128"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0293ylskkxri11mxwq9bds1axj7nh79ppk16gkckfbh3dzmxnyhw") (f (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1.1.0 (c (n "secded") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (f (quote ("i128"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "097d97jkpfkmshf7xnlyf27zwys2rag3balp998331ckzgxknxjh") (f (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

