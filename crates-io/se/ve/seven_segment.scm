(define-module (crates-io se ve seven_segment) #:use-module (crates-io))

(define-public crate-seven_segment-0.1.0 (c (n "seven_segment") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "0478fvd0p4djimh99n3mh1f2g6qv7lp01qj99hvkwxwlf8njb7k4")))

(define-public crate-seven_segment-0.2.0 (c (n "seven_segment") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "1v3adggink3riv8lz4sl1gsdbmx9l5dl64vxy8hqyh0nq333q2fr") (y #t)))

(define-public crate-seven_segment-0.2.1 (c (n "seven_segment") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1w9m3mcbm78w0sa1aw5bsbqkj7ipf2lf6kd9j7v6l3yzspn71ml0")))

(define-public crate-seven_segment-0.1.1 (c (n "seven_segment") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "v_0_2") (r "^0.2.1") (d #t) (k 0) (p "seven_segment")))) (h "1jfm1lyvps26n5306kn30pzr4sjnxci8ji52nbjs4n2l7py2n1gm")))

