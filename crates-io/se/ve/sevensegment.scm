(define-module (crates-io se ve sevensegment) #:use-module (crates-io))

(define-public crate-sevensegment-0.1.0 (c (n "sevensegment") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1gphi82ghvm76xmz8541lr3w003ybywyg6xvg9sw8jfj8idaj0vw")))

(define-public crate-sevensegment-0.2.0 (c (n "sevensegment") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1p2zxaxp2q8qjl1sf7xxx8mrlcj3vcmmj4xdn8c60vnacva3ya78")))

