(define-module (crates-io se ve seven-client) #:use-module (crates-io))

(define-public crate-seven-client-0.3.0 (c (n "seven-client") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0aiwaq8aa5h714v7zld9ijp8b2avlnxxnrqzb6qjxi1pf958piq5")))

