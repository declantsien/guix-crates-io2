(define-module (crates-io se ve sevenzip-sys) #:use-module (crates-io))

(define-public crate-sevenzip-sys-0.1.0 (c (n "sevenzip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b0adbccw594azv70mbf1g5wmd7yw016siwl013dddypj7dvrmlg")))

(define-public crate-sevenzip-sys-0.1.1 (c (n "sevenzip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01vv69mllgqpzcv2d169bypjrw9jrcq6b74r2zbl22crkpj1875l")))

