(define-module (crates-io se ve severlevel) #:use-module (crates-io))

(define-public crate-severlevel-0.9.0 (c (n "severlevel") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 0)))) (h "05h4dgw92y17j9lrqdjfqny0q1jgvq4hqs6fnmffl0avk3pnl16v")))

(define-public crate-severlevel-0.9.1 (c (n "severlevel") (v "0.9.1") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 0)))) (h "0ggym5q1gd27f18cmw0qv577q2wgxdympm0s2iz8vcbl0mbnnkfk")))

