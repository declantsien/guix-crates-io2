(define-module (crates-io se ve seven_seg) #:use-module (crates-io))

(define-public crate-seven_seg-0.1.2 (c (n "seven_seg") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cattocol") (r "^0.3.1") (d #t) (k 0)) (d (n "cpu-freq") (r "^0.0.2") (d #t) (k 2)) (d (n "txtframe") (r "^0.4.0") (f (quote ("newline" "esc"))) (d #t) (k 2)))) (h "0nh07zwicgba3fakp953z7mr9haxgjajc6p8k0jr43c32lljkpzl")))

