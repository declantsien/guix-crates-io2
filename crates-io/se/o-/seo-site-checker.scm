(define-module (crates-io se o- seo-site-checker) #:use-module (crates-io))

(define-public crate-seo-site-checker-0.1.0 (c (n "seo-site-checker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "liquid") (r "^0.26") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (d #t) (k 0)))) (h "0xbk3264f3irphdf1vak81mhi10rshpcp1z2advzjsjaxynrgdjn")))

