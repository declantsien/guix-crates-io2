(define-module (crates-io se mr semrs) #:use-module (crates-io))

(define-public crate-semrs-0.5.0 (c (n "semrs") (v "0.5.0") (h "062zg37k43fmxy2bpgzcibc15x5zdab61i2z8jqmbiwz56xf6r99")))

(define-public crate-semrs-0.5.1 (c (n "semrs") (v "0.5.1") (h "17yr29r1v6gqpd5pvlwqj873dksw2kv943sm5mk9mazw59qnp453")))

(define-public crate-semrs-0.5.2 (c (n "semrs") (v "0.5.2") (h "1hb5rv7hdyg83jwhavwq0p41knji6qq0hjp2pfz62qa430q9cwx8")))

