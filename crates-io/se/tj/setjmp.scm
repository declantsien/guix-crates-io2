(define-module (crates-io se tj setjmp) #:use-module (crates-io))

(define-public crate-setjmp-0.1.0 (c (n "setjmp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wzyzpzpv1h804n7snhc2v46nq013imwg9yqmqrhippmcpaa392p")))

(define-public crate-setjmp-0.1.1 (c (n "setjmp") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v1ydjid53jw3754fqd2dvgjc4hk5d5xficzg2gin0yndhnwjd3m")))

(define-public crate-setjmp-0.1.2 (c (n "setjmp") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hx3y9bycq0wvrh4kjvz7zf0ln0af0462gky5s3pjgqvrfdlg7gi")))

(define-public crate-setjmp-0.1.3 (c (n "setjmp") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "clang-sys") (r "^0.29") (f (quote ("clang_6_0"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "1jx02ymqgfy1fik23nnm4s9vncrz0w1nvgv55lg4642m5i9s668m")))

(define-public crate-setjmp-0.1.4 (c (n "setjmp") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "clang-sys") (r "^0.29") (f (quote ("clang_6_0"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0wrw13md93mf6d3ljfslzw3n1vmpr6sswp6fyz64jhwv5q28xklv")))

