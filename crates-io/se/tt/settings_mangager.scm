(define-module (crates-io se tt settings_mangager) #:use-module (crates-io))

(define-public crate-settings_mangager-0.0.1-init (c (n "settings_mangager") (v "0.0.1-init") (d (list (d (n "cluFlock") (r "^1.2.7") (d #t) (k 0)) (d (n "config") (r "^0.12") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1dll170g11cxdjbkqczr61q52dwzk57h41n0k77xngav52lc1gmc") (y #t)))

