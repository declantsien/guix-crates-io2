(define-module (crates-io se tt settrie) #:use-module (crates-io))

(define-public crate-settrie-0.1.0 (c (n "settrie") (v "0.1.0") (h "1acgv1r90xph8zbaazf5msilqiwmcf62dfax4i4wlgscds5f766s")))

(define-public crate-settrie-0.1.1 (c (n "settrie") (v "0.1.1") (h "0yjkp0aqgr1d1sdaygc49bakfx6nk506mpwcx1nrqkgvsazdyn00")))

(define-public crate-settrie-0.1.2 (c (n "settrie") (v "0.1.2") (h "0l4r0dgj6aciwxjzmfpivgfsxl2ya3hxl474ra30a9r8l0ggkv4a")))

