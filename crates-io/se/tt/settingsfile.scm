(define-module (crates-io se tt settingsfile) #:use-module (crates-io))

(define-public crate-settingsfile-0.2.0 (c (n "settingsfile") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "0zs2yimv6yqjh04c5l0bpmh7ykj9diybcj7kda7xf8z55xql25j9")))

(define-public crate-settingsfile-0.2.1 (c (n "settingsfile") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "16gcqkklxs30rlvfq113bn6q1gpv1ir32prpx9bgvi4qhf6dxp15")))

(define-public crate-settingsfile-0.2.2 (c (n "settingsfile") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1g59in5pzci43d2bfbiicnimyywz9q05xmfa3f101zgk3wzgv42w")))

(define-public crate-settingsfile-0.2.3 (c (n "settingsfile") (v "0.2.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "16g7wd48b5v1sxy0gspvygsx7627fyhvzkn3qj1y92nwmi6kigix")))

(define-public crate-settingsfile-0.2.4 (c (n "settingsfile") (v "0.2.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 2)))) (h "1kfkvlpz8k024r3fi401aiiqp64scwyqmp738vsqcdi6a23slg51")))

(define-public crate-settingsfile-0.2.5 (c (n "settingsfile") (v "0.2.5") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0gj3drnd6kblsa7bwkxvx5lzcxprkbq9idiv51alcrifbs5h3638")))

(define-public crate-settingsfile-0.2.6 (c (n "settingsfile") (v "0.2.6") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1isvnqhdigc9k0nyffh7j2jvwg271q693kk48hb038njb5f7hrj9")))

(define-public crate-settingsfile-0.2.7 (c (n "settingsfile") (v "0.2.7") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0xgfx7dshwv76p7xgi3yyb49jyx5hrbq1ypwhq3pbrz73ppqryal")))

(define-public crate-settingsfile-0.2.8 (c (n "settingsfile") (v "0.2.8") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nvzic6gdda8wn6454jgw8zvmv4ywjz8rnv20gb5pyxr4mdgljrb")))

