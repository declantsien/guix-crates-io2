(define-module (crates-io se tt settimeout) #:use-module (crates-io))

(define-public crate-settimeout-0.1.0 (c (n "settimeout") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)))) (h "1ckrl0sxyr8sg2g7phl4g9n8h9bgqq272ycgrcav3jnfsm9hk10c")))

(define-public crate-settimeout-0.1.1 (c (n "settimeout") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)))) (h "0iwln5nwzm679c9ikpm29hbdgcig4d12155sry2gm4kx7kgg0zhs")))

(define-public crate-settimeout-0.1.2 (c (n "settimeout") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 2)))) (h "1qkdy1196qxcq3xjaqzzs4ax435zvmabr6kav4516h3285j2li07")))

