(define-module (crates-io se tt settings-schema) #:use-module (crates-io))

(define-public crate-settings-schema-0.0.1+alvr (c (n "settings-schema") (v "0.0.1+alvr") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "settings-schema-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1q37i89bklwwhlckkanwn9xirqblwzm1594wzhci25lirlca2rz2") (f (quote (("rename_snake_case") ("rename_camel_case") ("default")))) (r "1.58")))

(define-public crate-settings-schema-0.1.0 (c (n "settings-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "settings-schema-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0qw3fibz8f4vbgs5rw7581vvfw10dbvyzmb229bsc193nkbw4bjw")))

(define-public crate-settings-schema-0.2.0 (c (n "settings-schema") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "settings-schema-derive") (r "^0.2.0") (d #t) (k 0)))) (h "03mnl9szlbrx784rdi7w12rq965h9am7jg95fal5a9d4iw5l4vhh")))

