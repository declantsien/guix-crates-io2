(define-module (crates-io se tt settings-schema-derive) #:use-module (crates-io))

(define-public crate-settings-schema-derive-0.0.1+alvr (c (n "settings-schema-derive") (v "0.0.1+alvr") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z4a97v5p7l12glm6wfw22q83ghriql7yk72aw79gyzxv7rhw23c") (r "1.58")))

(define-public crate-settings-schema-derive-0.1.0 (c (n "settings-schema-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ca10fqyzs89qzyjyl35f7ym565703psjnam6v1wh0nj8ybh0gml")))

(define-public crate-settings-schema-derive-0.2.0 (c (n "settings-schema-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04358adbh0alxwl8mjxfn5sim029a1xrp1lkzrzf9nbj7l2rmg4r")))

