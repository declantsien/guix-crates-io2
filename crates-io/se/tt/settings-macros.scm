(define-module (crates-io se tt settings-macros) #:use-module (crates-io))

(define-public crate-settings-macros-0.0.1 (c (n "settings-macros") (v "0.0.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08z0blb29nri7wxg1f9wh34yyfqysk13cgpaj5wjhbv1y6cznp55")))

(define-public crate-settings-macros-0.0.2 (c (n "settings-macros") (v "0.0.2") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mj2jifyg7zgqarpgzfwfgq3c1zbwpdhn2kyn3sgpba2kbwiysz3")))

(define-public crate-settings-macros-0.0.3 (c (n "settings-macros") (v "0.0.3") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lr80nvq59ib1px7nd17ydfar3943aack8d243czz7na1pxvypr9")))

(define-public crate-settings-macros-0.0.4 (c (n "settings-macros") (v "0.0.4") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wg1rknrayk1ds8n7fva4wjy3nhqc063s84sxybj04f5d3d71a2p")))

(define-public crate-settings-macros-0.0.5 (c (n "settings-macros") (v "0.0.5") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18lm6w1qb5b3hk6w8n2znhnsz3pala7fnwjqwlmgawbgmzwlvngm")))

