(define-module (crates-io se tt setting) #:use-module (crates-io))

(define-public crate-setting-0.0.0 (c (n "setting") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "1m0bqfy6kskm8zqqrljm02xgcx3k33a403pakj7rhmlm9dg98z62") (f (quote (("default"))))))

(define-public crate-setting-0.1.0 (c (n "setting") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "1gpdp1f7kbk25nn5k9h8fz3hmbyqgg6r365g8z0p1g0yrp462fv5") (f (quote (("default"))))))

(define-public crate-setting-0.2.0 (c (n "setting") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "1w7ry9mnkry3pc6pnfyh3ymgzb2v6jayn17az7jmnzpya1bs2z2q") (f (quote (("default"))))))

(define-public crate-setting-0.2.1 (c (n "setting") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)))) (h "05jvyqmk51dyjyc7fgc3lq4qczn6dbc14hvgj7d5mrpm2aqmsaw0") (f (quote (("default"))))))

(define-public crate-setting-0.2.2 (c (n "setting") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vykbcn34996iwl0as99syaan53fmg8mw5dbi43kagdpv86jabih") (f (quote (("default"))))))

