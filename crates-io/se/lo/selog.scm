(define-module (crates-io se lo selog) #:use-module (crates-io))

(define-public crate-selog-0.1.0 (c (n "selog") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1870b0mafp1malamwppacyn2vvp79cxf5d1jb0rb9s1szzn37bin")))

(define-public crate-selog-0.1.1-beta.0 (c (n "selog") (v "0.1.1-beta.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "08s87i7fy14ic9ifv3sbk30sxf3dlk7f0m916q6iapq9vs2mgh8c") (f (quote (("opts" "clap") ("colorchoice" "termcolor")))) (y #t)))

(define-public crate-selog-0.1.1-beta.1 (c (n "selog") (v "0.1.1-beta.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0ai15sx57g1k7zlidfwhqwkskr24yc4kknwa75jm2p49d0xgikyl") (f (quote (("opts" "clap") ("colorchoice" "termcolor"))))))

