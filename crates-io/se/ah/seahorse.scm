(define-module (crates-io se ah seahorse) #:use-module (crates-io))

(define-public crate-seahorse-0.1.0 (c (n "seahorse") (v "0.1.0") (h "1p2wc153wg7mjvhr0wnzxh6alibclb1j86lac09s0k8f2frf9nph")))

(define-public crate-seahorse-0.1.1 (c (n "seahorse") (v "0.1.1") (h "0azwrbxx6diz3q9sf18l17ys4mx8apsvlrwcq1mqdmkn4xyzz7q4")))

(define-public crate-seahorse-0.2.0 (c (n "seahorse") (v "0.2.0") (h "1jfl1yv6icrp2l8lblma3mi0q72srw197lgahf3hb8i5ba229ial")))

(define-public crate-seahorse-0.2.1 (c (n "seahorse") (v "0.2.1") (h "06hbwji6km6hrn1wyj2f8dfq0g3wk3jhvbnxd9aw7vikbysa7hfa")))

(define-public crate-seahorse-0.2.2 (c (n "seahorse") (v "0.2.2") (h "1mx8ijrb7ndqnpr9p0kqjb0rsiszq31qf436ycrgrcdjahdzk5fq")))

(define-public crate-seahorse-0.2.3 (c (n "seahorse") (v "0.2.3") (h "0q3m0sdbmkmqhs01vjw88ymakir5cff2r8lkx37v4yagwr7jfz1s")))

(define-public crate-seahorse-0.2.4 (c (n "seahorse") (v "0.2.4") (h "0cfn2hg7l7s8xbpg03056pb4cvc1wlyfzla9pf6cflr13vjgsv5f")))

(define-public crate-seahorse-0.2.5 (c (n "seahorse") (v "0.2.5") (h "1p7n70xl8xkp54f2av8h31m1z8fw827jfhvyfli2wyg50gk4ky45")))

(define-public crate-seahorse-0.3.0 (c (n "seahorse") (v "0.3.0") (h "1ac5f098dcx02ka6d68gcn77j3d75igywlqpvhj8c3b8qxqi8izc")))

(define-public crate-seahorse-0.3.1 (c (n "seahorse") (v "0.3.1") (h "0vmafbgfac12cni9ygl34vsb32arxxcdiahgckq3nq1vkvac438r")))

(define-public crate-seahorse-0.4.0 (c (n "seahorse") (v "0.4.0") (h "1xb8ibc18qkzkfiakp2iz2w6smbjgihpvgnzx08bzrq0z5bbxzrm")))

(define-public crate-seahorse-0.4.1 (c (n "seahorse") (v "0.4.1") (h "1c0g1wj3mcwgxpawsrxic52msfgkgi9485y6lv7pikd61wibq8kj")))

(define-public crate-seahorse-0.4.2 (c (n "seahorse") (v "0.4.2") (h "0ymdv69k8fyk19kp3wslbiirf9a5idbllxa2kz5vy864ygm62z5g")))

(define-public crate-seahorse-0.4.3 (c (n "seahorse") (v "0.4.3") (h "16a41542j4d66y0m2kw95ycwd00nfb5c19mg6bfrcxrwribh8z0l")))

(define-public crate-seahorse-0.4.4 (c (n "seahorse") (v "0.4.4") (h "0cr87aab2988r3cibn69k7a4kqxsn9m0pgx956rk1pz1x5lj2q3j")))

(define-public crate-seahorse-0.4.5 (c (n "seahorse") (v "0.4.5") (h "0n4wa3z8wcinrd986gs2577v68piiis6923353pb1lswsnrpw4si")))

(define-public crate-seahorse-0.4.6 (c (n "seahorse") (v "0.4.6") (h "1jr67xlzs3z1vahnfkkiklz1bkm237x9nrl16yvaly5jjz62hg2v")))

(define-public crate-seahorse-0.4.7 (c (n "seahorse") (v "0.4.7") (h "0zpbak69lq4zm2606inn21ifxanf7ymkcn84yflpdkyyx2vh5fsk")))

(define-public crate-seahorse-0.5.0 (c (n "seahorse") (v "0.5.0") (h "1bk5gm049z6ihfxvx7rzw54cpwkpzkbc32nia95asin2vnfsr9qw")))

(define-public crate-seahorse-0.5.1 (c (n "seahorse") (v "0.5.1") (h "1bfns0808bhb9rxfm6kk55xndygmbnsvb9ng6id9p436v607byz2")))

(define-public crate-seahorse-0.5.2 (c (n "seahorse") (v "0.5.2") (h "00plfb0mpy9zllr1hw1y8lppiyr8f0f82b2fv1cld5na98livsrl")))

(define-public crate-seahorse-0.5.3 (c (n "seahorse") (v "0.5.3") (h "0qbjhcz402hasvfr12w3p293lwzjw8dzrviy3c62ij78fxgq2riw")))

(define-public crate-seahorse-0.6.0 (c (n "seahorse") (v "0.6.0") (h "05cg21rlpf9qhcanqv9d1vcqm5974s0z31qd3p6cb5nvkqkx8an9")))

(define-public crate-seahorse-0.6.1 (c (n "seahorse") (v "0.6.1") (h "0i9q5ryxcpyh2i02fvkx2i6irlspg2nv08gri1karh2b1j44wdr4")))

(define-public crate-seahorse-0.6.2 (c (n "seahorse") (v "0.6.2") (h "0wqfma6cxa8ip8vbkxnr72zad8claw4w5c8hsiyylpk310mc6g0h")))

(define-public crate-seahorse-0.7.0 (c (n "seahorse") (v "0.7.0") (h "17byc80ph1lq4kkxik02kmpzn195rll3gf2qqmwj0dgwx4v9757x")))

(define-public crate-seahorse-0.7.1 (c (n "seahorse") (v "0.7.1") (h "1akf2wx8bcah9pzxji9n4x7h8a9cqknqi0096mma5f0clk3l1b94")))

(define-public crate-seahorse-1.0.0 (c (n "seahorse") (v "1.0.0") (h "1iz9sfz19g438am74s1s4316gmn5qn8mdx338ianp4q98wgdrfj8")))

(define-public crate-seahorse-1.1.0 (c (n "seahorse") (v "1.1.0") (h "118agkznwz2nrlv8pbdj9lmhlrahp5qd50dhj7r1rqw2bv7bx9sn")))

(define-public crate-seahorse-1.1.1 (c (n "seahorse") (v "1.1.1") (h "0n3dd2jrpz516k7dmfij5mxd6dqvb4qcbg9knw6wwp46w9098zff")))

(define-public crate-seahorse-1.1.2 (c (n "seahorse") (v "1.1.2") (h "1garn1xr6ng0ip2kplj2pblrdjmsvhx3vjq7d8iisvvqakj1zvlb")))

(define-public crate-seahorse-1.2.0 (c (n "seahorse") (v "1.2.0") (h "1mj3mk2lwj66yr6fwa2lzf8vb6wbzkk7zq0qs1yfm5zpm0qj0814") (y #t)))

(define-public crate-seahorse-2.0.0 (c (n "seahorse") (v "2.0.0") (h "1jjdcn3ndx6wzyxphnxn1v3cbhnmk6alx0391v8wkr1wvsxhiz4d")))

(define-public crate-seahorse-2.1.0 (c (n "seahorse") (v "2.1.0") (h "174wmxvvqz06nq4x9caym6fpys7n3ch4dyqn3zsz4w9ix48p2dmk")))

(define-public crate-seahorse-2.2.0 (c (n "seahorse") (v "2.2.0") (h "0g273i7i57gxj6kzw7p51wm3458sz5vfmg8sya2p93k75yk7jmnj")))

