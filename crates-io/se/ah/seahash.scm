(define-module (crates-io se ah seahash) #:use-module (crates-io))

(define-public crate-seahash-0.1.0 (c (n "seahash") (v "0.1.0") (h "1vlk5v3g5jqry8vqr5kjqhgik2q393lc3wrf9x9gifjcj73lbq59")))

(define-public crate-seahash-0.2.0 (c (n "seahash") (v "0.2.0") (h "02jvwxa80x25n7npmhc3i85s6qj0sscf4hn24k58ar3kjipcdq5p")))

(define-public crate-seahash-1.0.0 (c (n "seahash") (v "1.0.0") (h "1s8cf27sks7pdydd0cx3b42x11i814mq3vcpc79kcrnyv25bxnlf")))

(define-public crate-seahash-2.0.0 (c (n "seahash") (v "2.0.0") (h "1k199xapv9mlf27p07yp68arqldgk4d92sc7m5p1pky042a8smxi")))

(define-public crate-seahash-2.1.0 (c (n "seahash") (v "2.1.0") (h "0ypf7wnw6kq9ykw3d0ghblymv0v7zx7jgjcrniccyg3arbxqka03")))

(define-public crate-seahash-2.1.1 (c (n "seahash") (v "2.1.1") (h "18rcj68y7zw2nhmqwhs07jyxnw85l475i6pbdlxa53y0pwky499v")))

(define-public crate-seahash-2.2.0 (c (n "seahash") (v "2.2.0") (h "1yqrasvf2vlyfwmxy7dsvwpyzh35sxhfk2y2m3iv1mcbm07m37pz")))

(define-public crate-seahash-2.3.0 (c (n "seahash") (v "2.3.0") (h "17q4rmm92v5m1ghwwh2jgkbyr6bbwv2n5w4ci5ixvj8pzal4haqf")))

(define-public crate-seahash-2.4.0 (c (n "seahash") (v "2.4.0") (h "1sp8k11z3mpqyy469y712kgpikn9n5pwccfa13bga4s4waf2hqb3")))

(define-public crate-seahash-3.0.0 (c (n "seahash") (v "3.0.0") (h "1s5kly7wlp9mqbgnqk4y230s6335vh03692bnw6pgwvwcw27vz6w")))

(define-public crate-seahash-3.0.1 (c (n "seahash") (v "3.0.1") (h "0k04v9jw7qjgj3w19k9pdkdl2rdvbghqkkwsair06qsnb9s4icd3")))

(define-public crate-seahash-3.0.2 (c (n "seahash") (v "3.0.2") (h "1dbh1d62npz6dhhb6ky4shli61ds3knznb5m2vfzn9kxd0w7gizc")))

(define-public crate-seahash-3.0.3 (c (n "seahash") (v "3.0.3") (h "0d15lyy4mvdbb89jsx95wh3134f8giz25kiz07avpcyx8jlmjcx4")))

(define-public crate-seahash-3.0.4 (c (n "seahash") (v "3.0.4") (h "02h8wy8zc0r8vfskk41756lw4i1xs0hhnkkbs8iz88whb7pj0pc7")))

(define-public crate-seahash-3.0.5 (c (n "seahash") (v "3.0.5") (h "17fhp7i4qnlicjdpajwabd5jvdwmcbnd2nkfqg6jz115xmmn6j70")))

(define-public crate-seahash-3.0.6 (c (n "seahash") (v "3.0.6") (h "1pr8ijnxnp68ki4m4740yc5mr01zijf86yx07wbsqzwiyhghdmhq")))

(define-public crate-seahash-3.0.7 (c (n "seahash") (v "3.0.7") (h "0iqg12lxkn0ivsfa1gkylcwj5wmi6zl87mbizlrkg918s6hprxaq")))

(define-public crate-seahash-4.0.0 (c (n "seahash") (v "4.0.0") (h "0ph42nn27nb491adsyj0gppsmw9za8z8l54r5zgdkvzja14fc9xl")))

(define-public crate-seahash-4.0.1 (c (n "seahash") (v "4.0.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "05n97n62msdxz3wl9i9x4r7rinh6akgs22fa514j4b97msf4bvir")))

(define-public crate-seahash-4.1.0 (c (n "seahash") (v "4.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0sxsb64np6bvnppjz5hg4rqpnkczhsl8w8kf2a5lr1c08xppn40w") (f (quote (("use_std") ("default"))))))

