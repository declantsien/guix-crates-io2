(define-module (crates-io se co secop-derive) #:use-module (crates-io))

(define-public crate-secop-derive-0.1.3 (c (n "secop-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0x5cja4rfdm3mjnxprya0m70rp8hgixzy42m585bylk9a6wc1v4w")))

