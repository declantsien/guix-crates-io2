(define-module (crates-io se co secop-modules) #:use-module (crates-io))

(define-public crate-secop-modules-0.1.3 (c (n "secop-modules") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mlzlog") (r "^0.7.0") (d #t) (k 0)) (d (n "mlzutil") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "secop-core") (r "^0.1.3") (d #t) (k 0)) (d (n "secop-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "0iiw68h2wrxkfqfjfr714i22pqnlnfm5wn9krh7q8spfdn8s6d5b")))

