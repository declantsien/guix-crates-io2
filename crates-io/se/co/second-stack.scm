(define-module (crates-io se co second-stack) #:use-module (crates-io))

(define-public crate-second-stack-0.2.0 (c (n "second-stack") (v "0.2.0") (d (list (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0d9gymrz7jb8jzwdzy9q9w3j6hl2awdgqinq98n85jav9q3gs2b6") (f (quote (("experimental"))))))

(define-public crate-second-stack-0.2.1 (c (n "second-stack") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.1") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 2)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1nxd2i7marar43sg8hg94mcvyag9zrmysny40dy0mfwfbij7036q") (f (quote (("experimental"))))))

(define-public crate-second-stack-0.3.0 (c (n "second-stack") (v "0.3.0") (h "0shqgvrk7izpfghmqahvlvpcplp3zijsi4bq8js4wwz45kxi6ghk")))

(define-public crate-second-stack-0.3.1 (c (n "second-stack") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13mif5frah0jzvam8cvab8xd8iq0vqvixa56djwahxh9d0qnlxjz")))

(define-public crate-second-stack-0.3.2 (c (n "second-stack") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1wb8n1l5iimvzxgib4qm0m37mmmdk9pnjnxd1bzish5ba6jddn1v")))

(define-public crate-second-stack-0.3.3 (c (n "second-stack") (v "0.3.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0xiz3q7rk4bhjvsrd0b8ij5c8yi3qcsv4riidnha2z0z6z311hgl")))

(define-public crate-second-stack-0.3.4 (c (n "second-stack") (v "0.3.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1caq8xj3qsnn498gg524v9a1hwqhyh5hlmwjlqlh362jrj3nd42i")))

(define-public crate-second-stack-0.3.5 (c (n "second-stack") (v "0.3.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "1fy036i108l52danyvhq0y1qwyaibbrqcnpsifqbkwaidqych129")))

