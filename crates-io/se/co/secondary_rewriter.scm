(define-module (crates-io se co secondary_rewriter) #:use-module (crates-io))

(define-public crate-secondary_rewriter-0.1.0 (c (n "secondary_rewriter") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m3d4wnpg4d2xcsbr8grxyadx9dddcgqxhj8912kjnwfxn135iz2")))

(define-public crate-secondary_rewriter-0.1.1 (c (n "secondary_rewriter") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1zgvr93yiclajm7vrrg4xsb160ya9ifk4s3qbz7vjgzxf525qdkm")))

(define-public crate-secondary_rewriter-0.1.2 (c (n "secondary_rewriter") (v "0.1.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1w8fka4i47xffaswi6hqxwi2nbylng79055hpyljhmz0787adv6r")))

(define-public crate-secondary_rewriter-0.1.3 (c (n "secondary_rewriter") (v "0.1.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1k50wgdawgh4jhdqg557papk1f9ny1hr1j537acrgg4vlkzmdv1d")))

(define-public crate-secondary_rewriter-0.1.4 (c (n "secondary_rewriter") (v "0.1.4") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "05r05liq6rqn4mllq3761dnb2vjginip4j51m74zjs79jyb2cswk")))

(define-public crate-secondary_rewriter-0.1.5 (c (n "secondary_rewriter") (v "0.1.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0zp93q74xjmmmmzdmpq87j651dm1c0a1vrybywdl26qvx29ych96")))

(define-public crate-secondary_rewriter-0.1.6 (c (n "secondary_rewriter") (v "0.1.6") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0x5s41qqw9g398x9zq5w5fpvb2xwswspqmlmsxs5aqk45hg4dd62")))

(define-public crate-secondary_rewriter-0.1.7 (c (n "secondary_rewriter") (v "0.1.7") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1k9757d3j3vjmzddmcj7lyx1mh3b01kwa3g1iyvngiy508fx9xpv")))

(define-public crate-secondary_rewriter-0.1.8 (c (n "secondary_rewriter") (v "0.1.8") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "121nbimpxsznbgva2c34m577nn9v8ix0llalc5drd4xldv2ql131")))

(define-public crate-secondary_rewriter-0.1.9 (c (n "secondary_rewriter") (v "0.1.9") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0hrg6y2d39269ryrmplc6skpfg5ssg7spifha9sk7hx8gjvkqw7k")))

(define-public crate-secondary_rewriter-0.2.0 (c (n "secondary_rewriter") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "16rzrxkq7lnyjg88ar0npb709r4p09hhvawnw1y0c29651s71zmy")))

