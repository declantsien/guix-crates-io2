(define-module (crates-io se co second_law) #:use-module (crates-io))

(define-public crate-second_law-0.1.0 (c (n "second_law") (v "0.1.0") (h "0594zi2ahhijzgid83d9hdpcgqki7l37w1lbw78dpz602kajqw6i")))

(define-public crate-second_law-0.2.1 (c (n "second_law") (v "0.2.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0y290mcfzhlnc781s5a1wq4n37qrqbinyrq8sil70pb7542pw89l")))

(define-public crate-second_law-0.2.2 (c (n "second_law") (v "0.2.2") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1hbyki0s7zx153ib69zymxbzbdpd3lf9mywx14bhgwb1dajy43yg")))

(define-public crate-second_law-0.3.1 (c (n "second_law") (v "0.3.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0kiicq9p4s6xdhqlhp17y3rxj677di443qbpilyl3d5z0av3d1n5")))

