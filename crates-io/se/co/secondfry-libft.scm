(define-module (crates-io se co secondfry-libft) #:use-module (crates-io))

(define-public crate-secondfry-libft-0.1.0 (c (n "secondfry-libft") (v "0.1.0") (h "1fh4fl94qdinvlagv885iv07fhc35b67dw6fpdfxxxss2p85aamz")))

(define-public crate-secondfry-libft-0.1.1 (c (n "secondfry-libft") (v "0.1.1") (h "13wfs9llq3p4j7jzkhx8zdn4wvr4rwfgkcw8m6k4af8aabqgy2a2")))

