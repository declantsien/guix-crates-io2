(define-module (crates-io se co secondsight) #:use-module (crates-io))

(define-public crate-secondsight-0.1.0 (c (n "secondsight") (v "0.1.0") (h "05nf0dw1whsy8yy996b8nwixcxx8wyqzvniaricwk1zhkwkzv489")))

(define-public crate-secondsight-0.1.1 (c (n "secondsight") (v "0.1.1") (d (list (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "1c0pzmckxj4pk0lngr9fxavilxcxvd4xmbv5hf0x9y74brpqm449") (y #t)))

(define-public crate-secondsight-0.1.2 (c (n "secondsight") (v "0.1.2") (h "1d4h90g87ns6l84fr7fpraz67zppxl2m10k4svlq7d3r0wf14yyk")))

