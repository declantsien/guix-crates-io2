(define-module (crates-io se nc sencha) #:use-module (crates-io))

(define-public crate-sencha-1.0.0 (c (n "sencha") (v "1.0.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "rss") (r "^1.2") (d #t) (k 0)))) (h "19q5frpnk8xw5fdxnaqxbm683zqidb6wag6l3n7xshp8d0d9zpr9") (y #t)))

