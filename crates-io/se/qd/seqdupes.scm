(define-module (crates-io se qd seqdupes) #:use-module (crates-io))

(define-public crate-seqdupes-0.1.0 (c (n "seqdupes") (v "0.1.0") (d (list (d (n "bio") (r "^0.41.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "1a9q8zqkhl2w0ba57bn1k5hkdf49ypf1lskcfq9f6ifqj00cg6mk")))

