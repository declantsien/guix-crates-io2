(define-module (crates-io se qd seqdiff) #:use-module (crates-io))

(define-public crate-seqdiff-0.1.0 (c (n "seqdiff") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "03w9pbnw7c96q8djb40miplp0izb503mcpmbw6vb9f4ixa41sg3h")))

(define-public crate-seqdiff-0.1.2 (c (n "seqdiff") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0yplvnvq3b57drziwykrfqyhyafc377818ydg8x3ryx1rnnnab56")))

(define-public crate-seqdiff-0.1.3 (c (n "seqdiff") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "0wn4s2zwix8h0ql1gjiinfshsgcsn3z9mg1g5wnqnx6dxyvqi17m")))

(define-public crate-seqdiff-0.1.4 (c (n "seqdiff") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1rcqrawisi118pcsfhl68ssc07jq7yy2x7g5bl6kfk14llshi67f")))

(define-public crate-seqdiff-0.2.1 (c (n "seqdiff") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)))) (h "0p8ssnybqqz9k88igg89bvwbxxx43jnw9j5aiwb0wihf6bmwlsc7")))

(define-public crate-seqdiff-0.2.3 (c (n "seqdiff") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)))) (h "0msa7pahzm83z3nkbkdhrlli5iz2xlc3sab4g5xvlx75s70nrmcg")))

(define-public crate-seqdiff-0.2.4 (c (n "seqdiff") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)))) (h "1bq6k7hx0msf04vdd2jag96r6sl8vpyfp521lpvcyfxzf5alr4y5")))

(define-public crate-seqdiff-0.3.0 (c (n "seqdiff") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)))) (h "1pxvvgcz1d2rv3rx9v0aqd8d61wvi6crz057bbx6dlp6mrbfsn70")))

