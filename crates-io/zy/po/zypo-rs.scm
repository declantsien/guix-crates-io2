(define-module (crates-io zy po zypo-rs) #:use-module (crates-io))

(define-public crate-zypo-rs-0.0.0 (c (n "zypo-rs") (v "0.0.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "09ykr9321amllnhk1hm088jpcdyb4c8nw27a8i9nv2dqnsw4293r")))

(define-public crate-zypo-rs-0.0.1 (c (n "zypo-rs") (v "0.0.1") (d (list (d (n "structopt") (r "^0.3.2") (k 0)) (d (n "zypo-parser") (r "^0.0.1") (d #t) (k 0)))) (h "0rz85xmb5s5ihaf32hlicf760shbm8k23xlviifch8zwcpvwf7j3")))

(define-public crate-zypo-rs-0.0.2 (c (n "zypo-rs") (v "0.0.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)))) (h "12scri2jz1ckjs3b9pkqwy284g8x11qb30q8sbf2gfs5hbhlrp22")))

(define-public crate-zypo-rs-0.0.3 (c (n "zypo-rs") (v "0.0.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (k 0)))) (h "1niyvwxksawjclm7qvpzx37caxzdw7wjyzmpbq82nlzs23i1vdbw")))

