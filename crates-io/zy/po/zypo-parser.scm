(define-module (crates-io zy po zypo-parser) #:use-module (crates-io))

(define-public crate-zypo-parser-0.0.1 (c (n "zypo-parser") (v "0.0.1") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "12bhf26pyd2vf1l734y51q2f2p60l6hgcm0gqzn0c9jchirhcjph")))

