(define-module (crates-io zy po zypo-lib) #:use-module (crates-io))

(define-public crate-zypo-lib-0.0.0 (c (n "zypo-lib") (v "0.0.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "17zbhz49ny01xfh9dr88l01h9dsic8k03vh4m6y4n5kr6n759g0l")))

(define-public crate-zypo-lib-0.0.1 (c (n "zypo-lib") (v "0.0.1") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "09qrx3v1m1275mprjpl9s26v4x6vi3h1w6qrz39pzg4vx77vyy5s")))

(define-public crate-zypo-lib-0.0.2 (c (n "zypo-lib") (v "0.0.2") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1pzigxssf7sjynj02hh6d26xhar9px6dnw2gkbrrj5ad4156qd7z")))

