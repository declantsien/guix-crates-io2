(define-module (crates-io zy di zydis-sys) #:use-module (crates-io))

(define-public crate-zydis-sys-0.1.0 (c (n "zydis-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1m3r2ix4vbm1b3pk9k386d71zqjq7rsmv8kxz6pr40za1113kysk")))

(define-public crate-zydis-sys-0.1.1 (c (n "zydis-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1ci3gmc9abslprn29zazb2m1mjysgv781xq1q6zfiny9bxv2npnv")))

(define-public crate-zydis-sys-0.1.2 (c (n "zydis-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0j8iksd80w6cy1z2a8j8qwlnwq9nzy1r7zzmvcdfn1la99nx0y7z")))

