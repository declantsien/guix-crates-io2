(define-module (crates-io zy di zydis) #:use-module (crates-io))

(define-public crate-zydis-0.0.1 (c (n "zydis") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.53") (d #t) (k 1)))) (h "0irl8m1k004c79lp94b09yx66fhs08dbn8vbg9lvflbnyljfm8z0")))

(define-public crate-zydis-0.0.2 (c (n "zydis") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.53") (d #t) (k 1)))) (h "1c6fxvr6m14np71gzsf4hzjn3rf6v9162dvcmml2a6q2bq4bj42y") (y #t)))

(define-public crate-zydis-0.0.3 (c (n "zydis") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "gcc") (r "^0.3.53") (d #t) (k 1)))) (h "0w7sbzpcvmifjj4l9ng1dzi2vblyjpgjfcsfczzh1gnyj8hbcbkm")))

(define-public crate-zydis-3.0.0 (c (n "zydis") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17vk8kmddlr2ghm02sjvlmn1gzv2yvshyxixvgnhws1f0zq5zhv8") (f (quote (("serialization" "serde" "serde_derive") ("default"))))))

(define-public crate-zydis-3.1.1 (c (n "zydis") (v "3.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18fhnk20p665p4y55bj8kfi1fdqp4hbjh43wla56xmsm6xcq5bic") (f (quote (("serialization" "serde" "serde_derive") ("minimal") ("default"))))))

(define-public crate-zydis-3.1.2 (c (n "zydis") (v "3.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09w1cy9hcgfc21is0mbg4wbvga5w1pwclc9hgw6g1ryc9lq11mg2") (f (quote (("serialization" "serde" "serde_derive") ("minimal") ("default"))))))

(define-public crate-zydis-3.1.3 (c (n "zydis") (v "3.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f5f78chd0l9bxvyq585xqfas9q3w5xk4dv1vkbf08krx2p3vhdz") (f (quote (("wasm" "nolibc") ("serialization" "serde" "serde_derive") ("nolibc") ("minimal") ("default"))))))

(define-public crate-zydis-4.0.0-beta.1 (c (n "zydis") (v "4.0.0-beta.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gk6hls0myrsy8pjdmp9nrhyazgnvzm9x31z7glrbds9is1gpi4y") (f (quote (("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "full-decoder") ("encoder" "full-decoder") ("default" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.0.0-beta.2 (c (n "zydis") (v "4.0.0-beta.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zv79vy2j61cfclqd0b2pbjvakf8fdwjfdl1pzlxflq397l0mnps") (f (quote (("std") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "std" "full-decoder") ("encoder" "full-decoder") ("default" "std" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.0.0-beta.3 (c (n "zydis") (v "4.0.0-beta.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03x54ynw25k35q0aj64a2xzcqb9srvx7wqfc290xhi094si45a7h") (f (quote (("std") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "std" "full-decoder") ("encoder" "full-decoder") ("default" "std" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.0.0-beta.4 (c (n "zydis") (v "4.0.0-beta.4") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zbblzqb270h9ja4zms7rr4cvq6hzzhzicmd3v4nxsixjhgawndk") (f (quote (("std") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "std" "full-decoder") ("encoder" "full-decoder") ("default" "std" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.1.0-beta.1 (c (n "zydis") (v "4.1.0-beta.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sgrphvih0y1fzzmjcrq26jgy6fq0h2jd9fq95qnrh843pv46hik") (f (quote (("std") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "std" "full-decoder") ("encoder" "full-decoder") ("default" "std" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.1.0 (c (n "zydis") (v "4.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gw7vmcvfr5misrw5sj5ayx0w89q5qflyp5p5sisb757jdpdg63n") (f (quote (("std") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "std" "full-decoder") ("encoder" "full-decoder") ("default" "std" "full-decoder" "formatter"))))))

(define-public crate-zydis-4.1.1 (c (n "zydis") (v "4.1.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qcsxd77wjnii6g3zkkkxp8m2jksvdv4hkb2254g2gylr6zdr583") (f (quote (("std" "alloc") ("serialization" "serde" "bitflags/serde") ("nolibc") ("full-decoder") ("formatter" "alloc" "full-decoder") ("encoder" "alloc" "full-decoder") ("default" "std" "full-decoder" "formatter") ("alloc"))))))

