(define-module (crates-io zy gi zygisk-v4) #:use-module (crates-io))

(define-public crate-zygisk-v4-0.0.0 (c (n "zygisk-v4") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "0zpk3xmg5zmwykf06anav4i1jpizcpj5yzr67jy2dpq83sazgglk")))

