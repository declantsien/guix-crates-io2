(define-module (crates-io zy x- zyx-optim) #:use-module (crates-io))

(define-public crate-zyx-optim-0.1.0 (c (n "zyx-optim") (v "0.1.0") (d (list (d (n "zyx-core") (r "^0.1.0") (k 0)))) (h "102v68v0pfyv64ipr4zm4ipsbzv15zvs41v9ns953ncxk87zj5k2") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-optim-0.1.1 (c (n "zyx-optim") (v "0.1.1") (d (list (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "1r6946ic5daqcxzc862vmca31izsg9f1s5dxp5423337whckk1hq") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

