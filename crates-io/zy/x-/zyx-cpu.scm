(define-module (crates-io zy x- zyx-cpu) #:use-module (crates-io))

(define-public crate-zyx-cpu-0.1.0 (c (n "zyx-cpu") (v "0.1.0") (d (list (d (n "rayon") (r "^1.8.1") (o #t) (k 0)) (d (n "zyx-core") (r "^0.1.0") (k 0)))) (h "04bc1yl6fqdrm94xqdf7m461mzrz86xa5xca6acb3nyv9dkn2cmd") (f (quote (("std" "zyx-core/std" "rayon") ("default" "std"))))))

(define-public crate-zyx-cpu-0.1.1 (c (n "zyx-cpu") (v "0.1.1") (d (list (d (n "rayon") (r "^1.8.1") (o #t) (k 0)) (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "1c8nlzs2f1v90kvjwbr9vjnibrnmybmgl0x22kbyykhcsa1dqgf1") (f (quote (("std" "zyx-core/std" "rayon") ("default" "std"))))))

