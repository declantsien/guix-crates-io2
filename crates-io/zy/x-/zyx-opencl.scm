(define-module (crates-io zy x- zyx-opencl) #:use-module (crates-io))

(define-public crate-zyx-opencl-0.1.0 (c (n "zyx-opencl") (v "0.1.0") (d (list (d (n "opencl-sys") (r "^0.2.8") (k 0)) (d (n "zyx-compiler") (r "^0.1.0") (k 0)) (d (n "zyx-core") (r "^0.1.0") (k 0)))) (h "1rk60pw655173j91a8qij9xdz93igxg94723x279in7r0j2j1qyv") (f (quote (("std" "zyx-core/std" "zyx-compiler/std") ("default" "std" "CL_VERSION_1_1" "CL_VERSION_1_2") ("debug1") ("CL_VERSION_2_1" "opencl-sys/CL_VERSION_2_1") ("CL_VERSION_1_2" "opencl-sys/CL_VERSION_1_2") ("CL_VERSION_1_1" "opencl-sys/CL_VERSION_1_1"))))))

(define-public crate-zyx-opencl-0.1.1 (c (n "zyx-opencl") (v "0.1.1") (d (list (d (n "opencl-sys") (r "^0.2.8") (k 0)) (d (n "zyx-compiler") (r "^0.1.1") (k 0)) (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "0wkyaqzsmazrvm3255dkwypfwk2p2yps9h6gywll4jgf3can8qpn") (f (quote (("std" "zyx-core/std" "zyx-compiler/std") ("default" "std" "CL_VERSION_1_1" "CL_VERSION_1_2") ("debug1") ("CL_VERSION_2_1" "opencl-sys/CL_VERSION_2_1") ("CL_VERSION_1_2" "opencl-sys/CL_VERSION_1_2") ("CL_VERSION_1_1" "opencl-sys/CL_VERSION_1_1"))))))

