(define-module (crates-io zy x- zyx-torch) #:use-module (crates-io))

(define-public crate-zyx-torch-0.1.0 (c (n "zyx-torch") (v "0.1.0") (d (list (d (n "tch") (r "^0.15.0") (k 0)) (d (n "zyx-core") (r "^0.1.0") (k 0)))) (h "1jpmnj63qraxdwyyym63hrwpzgsxqfv8j59v9f58d613xhlysw0a") (f (quote (("std" "zyx-core/std") ("download-libtorch" "tch/download-libtorch") ("default" "std"))))))

(define-public crate-zyx-torch-0.1.1 (c (n "zyx-torch") (v "0.1.1") (d (list (d (n "tch") (r "^0.15.0") (k 0)) (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "05z1ai55rd72qgc7mf1297bwr763md17cdgqqi1chlncgx13zljj") (f (quote (("std" "zyx-core/std") ("download-libtorch" "tch/download-libtorch") ("default" "std"))))))

