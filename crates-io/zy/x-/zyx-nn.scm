(define-module (crates-io zy x- zyx-nn) #:use-module (crates-io))

(define-public crate-zyx-nn-0.1.0 (c (n "zyx-nn") (v "0.1.0") (d (list (d (n "zyx-core") (r "^0.1.0") (d #t) (k 0)))) (h "0kchq8awvyvw824lyabmv2qc919v6gi8lxlfmahcq8ynwbaancns") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-nn-0.1.1 (c (n "zyx-nn") (v "0.1.1") (d (list (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "06c0pp0fvs1vbqviq4qyirlp02577fvv1girxg0x0q9j8m4i7i9j") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

