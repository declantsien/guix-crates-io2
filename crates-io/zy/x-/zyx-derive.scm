(define-module (crates-io zy x- zyx-derive) #:use-module (crates-io))

(define-public crate-zyx-derive-0.1.0 (c (n "zyx-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "zyx-core") (r "^0.1.0") (d #t) (k 0)))) (h "0ihd69zc3qrhldfv0iv0v64p0hbrm6iv1mz9f860fmybx19n50m7") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-derive-0.1.1 (c (n "zyx-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "1v26jgdrchfpjk09zn3rk1y57py70bxpjapwv6zgiqd3fm17rcld") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

