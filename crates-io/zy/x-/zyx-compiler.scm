(define-module (crates-io zy x- zyx-compiler) #:use-module (crates-io))

(define-public crate-zyx-compiler-0.1.0 (c (n "zyx-compiler") (v "0.1.0") (d (list (d (n "zyx-core") (r "^0.1.0") (k 0)))) (h "0by7ndzznb1mxhfjmvy9rl1sy7m633l98mqkkl9s864jh1cj8lis") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-compiler-0.1.1 (c (n "zyx-compiler") (v "0.1.1") (d (list (d (n "zyx-core") (r "^0.1.1") (k 0)))) (h "06lmw96fnbsl9j6ix8xlxg70mf1wsm34ficf6xynf8pf9s2ma6rk") (f (quote (("std" "zyx-core/std") ("default" "std"))))))

