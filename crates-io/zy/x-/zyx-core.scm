(define-module (crates-io zy x- zyx-core) #:use-module (crates-io))

(define-public crate-zyx-core-0.1.0 (c (n "zyx-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 0)))) (h "1fxcwc0vvnyd6sw80x89681ladngmyhynbvpl50yk5avvg6xqsyw") (f (quote (("std") ("default" "std"))))))

(define-public crate-zyx-core-0.1.1 (c (n "zyx-core") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 0)))) (h "04simpa76b9mlpqhq10crgq1l9fzfg78f68jc44288fcvb15xg73") (f (quote (("std") ("default" "std"))))))

