(define-module (crates-io pc l_ pcl_rs) #:use-module (crates-io))

(define-public crate-pcl_rs-0.1.0 (c (n "pcl_rs") (v "0.1.0") (d (list (d (n "kdtree") (r "^0.7.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 0)) (d (n "ros-nalgebra") (r "^0.1.0") (d #t) (k 0)) (d (n "ros_pointcloud2") (r "^0.4.0") (d #t) (k 0)))) (h "1sxxs30k7azwhkigx1vjrw6nw3dcsif71m3vr46y7nj5wbxkfgcj") (y #t)))

(define-public crate-pcl_rs-0.1.1 (c (n "pcl_rs") (v "0.1.1") (d (list (d (n "kdtree") (r "^0.7.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 0)) (d (n "ros-nalgebra") (r "^0.1.0") (d #t) (k 0)) (d (n "ros_pointcloud2") (r "^0.4.0") (d #t) (k 0)))) (h "1w70s7vsy8rnnsn946066a0l0d13rrfc38c2rj0lfdfrbfzxmnp4") (y #t)))

(define-public crate-pcl_rs-0.1.2 (c (n "pcl_rs") (v "0.1.2") (d (list (d (n "kdtree") (r "^0.7.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 0)) (d (n "ros-nalgebra") (r "^0.1.0") (d #t) (k 0)) (d (n "ros_pointcloud2") (r "^0.4.0") (d #t) (k 0)))) (h "0wq3grw9iwigka4d4vcm30ffkdjsqwwbks20fb3bdb42np9g9926") (y #t)))

(define-public crate-pcl_rs-0.1.3 (c (n "pcl_rs") (v "0.1.3") (d (list (d (n "k_nearest") (r "^0.2.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 0)) (d (n "ros-nalgebra") (r "^0.1.0") (d #t) (k 0)) (d (n "ros_pointcloud2") (r "^0.4.0") (d #t) (k 0)))) (h "1lj92wjj5ingnrify8fh479rkj6k87cdmi1rvbmiiqbbaz0q1p8z") (y #t)))

(define-public crate-pcl_rs-0.1.4 (c (n "pcl_rs") (v "0.1.4") (d (list (d (n "k_nearest") (r "^0.2.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (d #t) (k 0)) (d (n "ros-nalgebra") (r "^0.1.0") (d #t) (k 0)) (d (n "ros_pointcloud2") (r "^0.4.0") (d #t) (k 0)))) (h "05vvbnygdny348cikvq16vkbqg902aicdvf6hgbil0wxd5s8d3p3")))

