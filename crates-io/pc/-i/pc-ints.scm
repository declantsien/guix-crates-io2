(define-module (crates-io pc #{-i}# pc-ints) #:use-module (crates-io))

(define-public crate-pc-ints-0.0.1 (c (n "pc-ints") (v "0.0.1") (h "0d3ndimawawddq3dci6flhziw5bvpixzj7bysi3w408aam03whx1") (r "1.61")))

(define-public crate-pc-ints-0.0.2 (c (n "pc-ints") (v "0.0.2") (h "1xngy1xmjs4hwfj5dd2avcjw89cvxh51i6bw0v635r848k955pqn") (r "1.61")))

(define-public crate-pc-ints-0.1.0 (c (n "pc-ints") (v "0.1.0") (h "0cv0ngsxcla0n23s6rcz86287irlcm0r40b2q1n43msi4m3iqsv1")))

(define-public crate-pc-ints-0.1.1 (c (n "pc-ints") (v "0.1.1") (h "047bn6bn2jqwnscvz00fzqymf897z7wb8fk2az7ny9b8ixzirw9n") (y #t)))

(define-public crate-pc-ints-0.1.2 (c (n "pc-ints") (v "0.1.2") (h "1svph294qk9gl8asgjdqdm0ks82n55vk799ghfhw5abd0vln0xrj") (y #t)))

(define-public crate-pc-ints-0.1.3 (c (n "pc-ints") (v "0.1.3") (h "0630jl7369nhdy2mirqgnfl70ap20854c269r4wf6hcx3x1mgpms")))

(define-public crate-pc-ints-0.1.4 (c (n "pc-ints") (v "0.1.4") (h "11f0w9nssmhcwrq19pqn3xx93gc58liqr51ggncf1xk6m71hqas2")))

(define-public crate-pc-ints-0.2.0 (c (n "pc-ints") (v "0.2.0") (h "0s27gkjbgdqwf5is8zwmsfqn7z6k69qzz5lqvhny67il0wn0iqp7")))

(define-public crate-pc-ints-0.2.1 (c (n "pc-ints") (v "0.2.1") (h "1d61in12qcw1iszim49k5ckil79dpgfdw2pin2zcvgycdswdsb5z")))

(define-public crate-pc-ints-0.3.0 (c (n "pc-ints") (v "0.3.0") (d (list (d (n "memoffset") (r "^0.8.0") (f (quote ("unstable_const"))) (d #t) (k 0)))) (h "05xdcxhzd1wldgicrq8q7wyi7d79grg1lpaprrqcwm1fasc203l0")))

(define-public crate-pc-ints-0.3.1 (c (n "pc-ints") (v "0.3.1") (d (list (d (n "memoffset") (r "^0.8.0") (f (quote ("unstable_const"))) (d #t) (k 0)))) (h "1l9dk7c115x549gyc5wl0w59gxvfvgnw5w0d48rfq62fqxahffha")))

(define-public crate-pc-ints-0.3.2 (c (n "pc-ints") (v "0.3.2") (d (list (d (n "memoffset") (r "^0.8.0") (f (quote ("unstable_const"))) (d #t) (k 0)))) (h "18j4097n742k4i4ymbld5qarqla6zn6xrlis7wddbbs230lh8918")))

(define-public crate-pc-ints-0.3.3 (c (n "pc-ints") (v "0.3.3") (d (list (d (n "memoffset") (r "^0.9.0") (f (quote ("unstable_const"))) (d #t) (k 0)))) (h "08ywc6hvmx90ziml4akk3777fsya35v57id9d92nh0wchfjwg182")))

(define-public crate-pc-ints-0.3.4 (c (n "pc-ints") (v "0.3.4") (d (list (d (n "memoffset") (r "^0.9.0") (f (quote ("unstable_const"))) (d #t) (k 0)))) (h "1dzgny9jlqnjhypx0yhd6viwkaxb0lmimmfvd0cp1bzwkw4wb826")))

