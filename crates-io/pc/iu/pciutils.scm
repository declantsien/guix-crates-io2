(define-module (crates-io pc iu pciutils) #:use-module (crates-io))

(define-public crate-pciutils-0.1.0 (c (n "pciutils") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pci-ids") (r "^0.2.5") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 0)))) (h "1dvfwxq20kd2kwdr3nw3xai82c9vvfnfkrlb9xalksnkyf8j4x7k")))

(define-public crate-pciutils-0.1.1 (c (n "pciutils") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pci-ids") (r "^0.2.5") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 0)))) (h "0z30h95x64lhqansakzjvs9mzahigypdglh315ijy6w9k2rl7vd7")))

