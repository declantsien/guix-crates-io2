(define-module (crates-io pc iu pciutils-sys) #:use-module (crates-io))

(define-public crate-pciutils-sys-0.1.0 (c (n "pciutils-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1zxgwii0gl1vg483yh7a4lqmy61hvvn7a6dy2m77fx1nrsa00v2s")))

(define-public crate-pciutils-sys-0.1.1 (c (n "pciutils-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "01n1n92mh673fil4n6hdv51zxw365n5l4487pn1rckj0l3dfnn4v")))

(define-public crate-pciutils-sys-0.1.2 (c (n "pciutils-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "0igvf9pzad7w4y09drvxpsj44g9hrz75ja94mnnxf68jl3iz73c1")))

(define-public crate-pciutils-sys-0.1.3 (c (n "pciutils-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "03hdvh08pxdyf0m91lzgiificsbx6ry7nda6999gngd6i7670d1k")))

