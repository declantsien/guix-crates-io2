(define-module (crates-io pc -t pc-timer) #:use-module (crates-io))

(define-public crate-pc-timer-0.0.1 (c (n "pc-timer") (v "0.0.1") (d (list (d (n "pc-ints") (r "^0.1.3") (d #t) (k 0)))) (h "13bkmisq2qc9ki8igvsqbr9pz9lslbvnbrvgx2dicnznhr92akdc")))

(define-public crate-pc-timer-0.0.2 (c (n "pc-timer") (v "0.0.2") (d (list (d (n "pc-ints") (r "^0.1.3") (d #t) (k 0)))) (h "1xi2fsdda40qcf8w2j6cc01l7jlrdjld7f6a077jllnl6rb5gywk")))

(define-public crate-pc-timer-0.1.0 (c (n "pc-timer") (v "0.1.0") (d (list (d (n "pc-ints") (r "^0.1.3") (d #t) (k 0)))) (h "08l3wbgrk0i7y79nddmxfhl42lp420x1x6mn8ym8jrxrjq3bz30f")))

(define-public crate-pc-timer-0.1.1 (c (n "pc-timer") (v "0.1.1") (d (list (d (n "pc-ints") (r "^0.1.4") (d #t) (k 0)))) (h "0cv4hwkzi1hjkcy9aqyqs0va24c7bd2r5rjr33wwgqrkzm5acfqh")))

(define-public crate-pc-timer-0.1.2 (c (n "pc-timer") (v "0.1.2") (d (list (d (n "pc-ints") (r "^0.2.0") (d #t) (k 0)))) (h "1dwia6n1bn61r5l214h6j6lp7ja1098rq497ls9j5bx21hp6s72i")))

(define-public crate-pc-timer-0.1.3 (c (n "pc-timer") (v "0.1.3") (d (list (d (n "pc-ints") (r "^0.2.0") (d #t) (k 0)))) (h "0g8j9l7xc3a9m6v7x8dwf9nx6za7ixxvn4vb5lhsqy626b61ksyj")))

(define-public crate-pc-timer-0.1.4 (c (n "pc-timer") (v "0.1.4") (d (list (d (n "pc-ints") (r "^0.2.0") (d #t) (k 0)))) (h "0x6ghb9ilrjn761zcf74fq79ssd3d0arq7ayh68rbb8n7hpkwh89")))

(define-public crate-pc-timer-0.1.5 (c (n "pc-timer") (v "0.1.5") (d (list (d (n "pc-ints") (r "^0.3.0") (d #t) (k 0)))) (h "0n674mp3zgmfmp730mdqal54h6xq432x4mazkswf3vgqssa7hw3r")))

(define-public crate-pc-timer-0.1.6 (c (n "pc-timer") (v "0.1.6") (d (list (d (n "pc-ints") (r "^0.3.0") (d #t) (k 0)))) (h "05rdk7302ca8jf0ppspychfy2i9qhj25rdpa0dc0192wdm4xwc69")))

(define-public crate-pc-timer-0.1.7 (c (n "pc-timer") (v "0.1.7") (d (list (d (n "pc-ints") (r "^0.3.0") (d #t) (k 0)))) (h "0lgs983mk63y6gkdg5sxa8vzcp20jc0w4gkqvf8ilw83pm6nv96p")))

