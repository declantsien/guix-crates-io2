(define-module (crates-io pc re pcre2-sys) #:use-module (crates-io))

(define-public crate-pcre2-sys-0.1.0 (c (n "pcre2-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "1hlqmkvdcb8pi1si1cld11mblz1l872nqa98w7hlcz0p4x2mlxy0")))

(define-public crate-pcre2-sys-0.1.1 (c (n "pcre2-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0pv94j1il1pgjwy6pmwa901whj5ymvyalf2mjqxkvqg4fja7y0m9")))

(define-public crate-pcre2-sys-0.2.0 (c (n "pcre2-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0jd8mj06kcfc03vaizaaxf9xy5k5jppiximygp7nkig1xakr400y")))

(define-public crate-pcre2-sys-0.2.1 (c (n "pcre2-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "10980c7q1634kcprmlx30rswb8rdlnwmg57vv87yw9dzpr1rssg9")))

(define-public crate-pcre2-sys-0.2.2 (c (n "pcre2-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0nwdvc43dkb89qmm5q8gw1zyll0wsfqw7kczpn23mljra3874v47")))

(define-public crate-pcre2-sys-0.2.3 (c (n "pcre2-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "1a1mfh6961kpl5xfj78vzfnxfxgzz1y56v967irzi9nwbvrll2k2") (y #t)))

(define-public crate-pcre2-sys-0.2.4 (c (n "pcre2-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0g6cn84hwrxa5pcf9vxy44i7l64cgc9prnwx6627lichrwkk29v9")))

(define-public crate-pcre2-sys-0.2.5 (c (n "pcre2-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "08mp6yxrvadplwd0drdydzskvzapr6dri9fyy7xvhzn3krg0xhyy")))

(define-public crate-pcre2-sys-0.2.6 (c (n "pcre2-sys") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0vmhw0b2nxyznxajpjw100287jjn7c7yggi99qnxbnvh3524y8xf")))

(define-public crate-pcre2-sys-0.2.7 (c (n "pcre2-sys") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1wpdvb5kaj8ggd3z63mhviyv5nbwkb0xz7wlh6sc1wiwy9b5b3wg")))

(define-public crate-pcre2-sys-0.2.8 (c (n "pcre2-sys") (v "0.2.8") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1bl6bjivwdpc3di1klymxrbilmpcsvlff7m2ffw6ai1s4nssgf15")))

(define-public crate-pcre2-sys-0.2.9 (c (n "pcre2-sys") (v "0.2.9") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "12hxy54i8amdnmcm034hqdc7iryd20n8aqg1hw5w540vzcc5s3sm")))

