(define-module (crates-io pc re pcre2) #:use-module (crates-io))

(define-public crate-pcre2-0.1.0 (c (n "pcre2") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "0m5b7hwmrd9mq3f9i22ks1k02454v459gzx85n596zy1607fq5hc")))

(define-public crate-pcre2-0.1.1 (c (n "pcre2") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "06f3hclr81s0yx4665h5i8s6sf16x73vn42rxsh5rv0545la5q1s")))

(define-public crate-pcre2-0.2.0 (c (n "pcre2") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "0mbidwcj7wbb04m48c4r6p19bhf3qkq4py9abqdjm2hxvnaq3350")))

(define-public crate-pcre2-0.2.1 (c (n "pcre2") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "103i66a998g1fjrqf9sdyvi8qi83hwglz3pjdcq9n2r207hsagb0")))

(define-public crate-pcre2-0.2.2 (c (n "pcre2") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "0hk4q7s71w1iiijq5c69z44n8anhk0bpi14wrm7v4h7ch9rc8xq8")))

(define-public crate-pcre2-0.2.3 (c (n "pcre2") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1c8sn70h72llf26sya9v26zmaamq350q57nwv6fl6fwhd4phzcw5")))

(define-public crate-pcre2-0.2.4 (c (n "pcre2") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.6") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1xdw5c71jlkmkw9ivrjm7g55q2j5gwbn3m28kaqcmf7dfizclsj8")))

(define-public crate-pcre2-0.2.5 (c (n "pcre2") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1jarqq6ck35hwgi0d89jkwsqdaajfc48dfi844wywwx3sq11vswx")))

(define-public crate-pcre2-0.2.6 (c (n "pcre2") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.7") (d #t) (k 0)))) (h "19w80j4f87hzdxbh5iilhbmx0vv02aybxr1ximbd7hszxal577ac")))

(define-public crate-pcre2-0.2.7 (c (n "pcre2") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pcre2-sys") (r "^0.2.9") (d #t) (k 0)))) (h "0rxx1k7gnxbb9sfwdzbmvk1rx05j10dy1vrcn4x709xxxbsjzaay")))

