(define-module (crates-io pc re pcre) #:use-module (crates-io))

(define-public crate-pcre-0.1.0 (c (n "pcre") (v "0.1.0") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1k3cicwmnv90qiqq963a0f82w1ydqbsd3klfirfpq6hn0q9qywlq")))

(define-public crate-pcre-0.1.1 (c (n "pcre") (v "0.1.1") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1lxipdn3ldzqfbh2r9mqxdgrpqf6xiqqyqp5pl6qghh20vl7amsa")))

(define-public crate-pcre-0.2.0 (c (n "pcre") (v "0.2.0") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.2") (d #t) (k 0)))) (h "02qa274b55pdgln5gaa75ck03v8qn3l3p3pa9nx9j8wfkc6i7dk8")))

(define-public crate-pcre-0.2.1 (c (n "pcre") (v "0.2.1") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1pf7wbyazczsdmpzwzj9v8gwqzm93mx8qwn0dxmql1lmi276mbc8")))

(define-public crate-pcre-0.2.2 (c (n "pcre") (v "0.2.2") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1xw1ky51hs58a10vhfs0nc1ias96r27x549hpb4s64sq8z116sj7")))

(define-public crate-pcre-0.2.3 (c (n "pcre") (v "0.2.3") (d (list (d (n "enum-set") (r ">= 0.0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpcre-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0qh535yyky9x0dlmnnrfrpcvrk0bclp19gyb7k0zlm9y9nfzcb4g")))

