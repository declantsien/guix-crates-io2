(define-module (crates-io pc ic pcics) #:use-module (crates-io))

(define-public crate-pcics-0.1.0 (c (n "pcics") (v "0.1.0") (d (list (d (n "byte") (r "^0.2.6") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1r0wrl6xcha2l5s9rl0v6ch2mk0nl0cp2wg6gagp8s01fivkb25m")))

(define-public crate-pcics-0.1.1 (c (n "pcics") (v "0.1.1") (d (list (d (n "byte") (r "^0.2.6") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2.3") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1nzhs5zz85qqxrwnsspd79d4rfyn788pgl73w9xfrn70x241434a")))

(define-public crate-pcics-0.2.0 (c (n "pcics") (v "0.2.0") (d (list (d (n "heterob") (r "^0.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "0i0835r653xcrbvb6pgs05p0rf49ialhh9khshbwwqdcgl5b256x") (f (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3.0 (c (n "pcics") (v "0.3.0") (d (list (d (n "heterob") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "0isn3cgmf08s3pl6ispcm1sfc0x461vzqv7xs44095wa9b640yvq") (f (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3.1 (c (n "pcics") (v "0.3.1") (d (list (d (n "heterob") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "1746k4kjg8v38idw4ynpvlxw60p2pblagf14avddqz868258i8qr") (f (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3.2 (c (n "pcics") (v "0.3.2") (d (list (d (n "heterob") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "15nk5mxx528pprc7klj8ffjzkf939yln54fqsx49pdpyffg27zrd") (f (quote (("caps_ea_real_entry_size"))))))

