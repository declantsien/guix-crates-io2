(define-module (crates-io pc a9 pca9548a) #:use-module (crates-io))

(define-public crate-pca9548a-0.1.0 (c (n "pca9548a") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "1kliqza49q1b7j7hin9f99r26gp6zw3hvmn7dh6vfl2rnfxjys3c") (f (quote (("std"))))))

