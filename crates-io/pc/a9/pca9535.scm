(define-module (crates-io pc a9 pca9535) #:use-module (crates-io))

(define-public crate-pca9535-0.0.1 (c (n "pca9535") (v "0.0.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rppal") (r "^0.13.0") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "1k12j0qmx5wnzhxlinb7px1w7q1k1gzwsib88ps66dwzn9526w4c") (f (quote (("std"))))))

(define-public crate-pca9535-0.1.0 (c (n "pca9535") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rppal") (r "^0.13.0") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "0kqbf02wqhwzdpk2h2idiizfwq19163dw836zjh8an8bknh21zxz") (f (quote (("std"))))))

(define-public crate-pca9535-1.0.0 (c (n "pca9535") (v "1.0.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rppal") (r "^0.13.0") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)))) (h "0v4pyr3cj24sqy5shfrs06hki1v66ydbgjws7v8j8c6qpqrym8c8") (f (quote (("std"))))))

(define-public crate-pca9535-1.1.0 (c (n "pca9535") (v "1.1.0") (d (list (d (n "hal") (r "=1.0.0-alpha.5") (d #t) (k 0) (p "embedded-hal")) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rppal") (r "^0.13.0") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)))) (h "1ad068spir73nr319z6ba276y13m3iixawnvsnhhbps9q3v4q9cl") (f (quote (("std"))))))

(define-public crate-pca9535-1.2.0 (c (n "pca9535") (v "1.2.0") (d (list (d (n "hal") (r "=1.0.0-alpha.9") (d #t) (k 0) (p "embedded-hal")) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rppal") (r "^0.14.1") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.5") (f (quote ("std" "eh-alpha"))) (d #t) (k 2)))) (h "0qyfplqlsxwbj1yb47d9nwhsglk8ggpp28yamsv68g1kzv66i9mr") (f (quote (("std"))))))

(define-public crate-pca9535-2.0.0 (c (n "pca9535") (v "2.0.0") (d (list (d (n "embedded-hal-bus") (r "^0.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "hal") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "once_cell") (r "^1.19") (d #t) (k 2)) (d (n "rppal") (r "^0.17") (f (quote ("hal"))) (d #t) (k 2)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)))) (h "1ldq5zaqzabifmnk8srylqf6vrsski5dkvrq0v6ah4ymlp96936i") (f (quote (("std"))))))

