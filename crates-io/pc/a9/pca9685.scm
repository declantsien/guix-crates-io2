(define-module (crates-io pc a9 pca9685) #:use-module (crates-io))

(define-public crate-pca9685-0.1.0 (c (n "pca9685") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "0pz25z0znsxhn1jagk46d226sqakgxccwwjwcmglc2xqzz4d0vg5")))

