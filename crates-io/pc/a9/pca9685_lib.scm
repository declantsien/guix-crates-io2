(define-module (crates-io pc a9 pca9685_lib) #:use-module (crates-io))

(define-public crate-pca9685_lib-0.1.0 (c (n "pca9685_lib") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.2") (d #t) (k 0)))) (h "1sg53wdsxq81f0yxvqgjxnhhhigz63yhz9w1cd0l7c6mn5fs0n8s") (y #t)))

(define-public crate-pca9685_lib-0.1.1 (c (n "pca9685_lib") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1nicgvfy041qq9gh7db0n3ksi3wxn2fj5km2iszqj1c1kawbql67") (y #t)))

(define-public crate-pca9685_lib-0.2.0 (c (n "pca9685_lib") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "05h42zljr8rlannqm178xf86p5lxal4pzyy3y62jgs0z4wasl8nl")))

(define-public crate-pca9685_lib-0.2.1 (c (n "pca9685_lib") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0zzcb3g4xzl2kyj3nnxwmr9p2ppm3vqid3yp8fk6p7q7krbl0px0")))

(define-public crate-pca9685_lib-0.2.2 (c (n "pca9685_lib") (v "0.2.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "1s85f17ijkh529fm3k6vbp4bpyszwvdajps9zbjs62w1mgibkihk")))

