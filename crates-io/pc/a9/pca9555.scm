(define-module (crates-io pc a9 pca9555) #:use-module (crates-io))

(define-public crate-pca9555-0.1.0 (c (n "pca9555") (v "0.1.0") (d (list (d (n "embassy-sync") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "0dnwmsbc57nd4hd93zb3wrrw52q2142k2n0w2njbf2bppiy29s2d")))

(define-public crate-pca9555-0.1.1 (c (n "pca9555") (v "0.1.1") (d (list (d (n "critical-section") (r "^1.1") (f (quote ("std"))) (d #t) (k 2)) (d (n "embassy-sync") (r "^0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1yhli76s79wxnbax5g6bi9y2k506cdfrykq6973dzdw09vhrq1p0")))

