(define-module (crates-io pc g_ pcg_rand) #:use-module (crates-io))

(define-public crate-pcg_rand-0.2.0 (c (n "pcg_rand") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "0ggsk1wilh0jiwwkniq2xkkvljhx5ivvqmnvyrifbja216p1x718")))

(define-public crate-pcg_rand-0.3.0 (c (n "pcg_rand") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "1ls7hxc0iwdng5rqrqw6454qckjpcjl7w46qlik11za96181plsm")))

(define-public crate-pcg_rand-0.3.1 (c (n "pcg_rand") (v "0.3.1") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "1xxvq3pahyhpgq4lvgrdyvvz7q89r7ys448lbyb1f9vzsbf6v5ss")))

(define-public crate-pcg_rand-0.3.2 (c (n "pcg_rand") (v "0.3.2") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "1jnfwl55l3jsm7yszl20wbw7bf6h6am9sbf63c5nmwl38vrd75sc")))

(define-public crate-pcg_rand-0.4.0 (c (n "pcg_rand") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "137di0q16f0ichrl0951pv4y62gjsczd8qigjpnbq254habq301f")))

(define-public crate-pcg_rand-0.4.1 (c (n "pcg_rand") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0g5qfdgflp96yks6mknwany91w4brfgnhcr3hb8l9sywz009p44q")))

(define-public crate-pcg_rand-0.5.0 (c (n "pcg_rand") (v "0.5.0") (d (list (d (n "extprim") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "14i2iphw4zbbzmd5xzcix0s2i4ynzd1s32hrz61cgg7qrvx2nwmd")))

(define-public crate-pcg_rand-0.6.0 (c (n "pcg_rand") (v "0.6.0") (d (list (d (n "extprim") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0l78vns68qmmprqgr40kpi9xv3820apm6xnm5yyzwniyd3dyzdk1")))

(define-public crate-pcg_rand-0.7.0 (c (n "pcg_rand") (v "0.7.0") (d (list (d (n "extprim") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0ffma4lp4qjfcs78mybd6a72c4y6fx64jpbh58igyrpnaq1irh4x")))

(define-public crate-pcg_rand-0.7.1 (c (n "pcg_rand") (v "0.7.1") (d (list (d (n "extprim") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "15rch6lvn82vm7csz4msjbpdsjjar35hwl2kgsrwc6nms1dn1wb8")))

(define-public crate-pcg_rand-0.8.0 (c (n "pcg_rand") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.2.3") (f (quote ("i128"))) (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (f (quote ("i128_support"))) (d #t) (k 0)))) (h "0mccg294i60as8hbvpdx50rqya22bv3vwpvldb8k0wya39qhswgc")))

(define-public crate-pcg_rand-0.9.0 (c (n "pcg_rand") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (f (quote ("i128"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "07j79hj6l61w15svzxfkk81aw9pxghn6ny37v831liqrrfwwjngw")))

(define-public crate-pcg_rand-0.9.1 (c (n "pcg_rand") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (f (quote ("i128"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "1l5l7yg3ff9k4m0s5b48zwr6mpxbnr2kv0r1lb4kmfc8ckv54nv6")))

(define-public crate-pcg_rand-0.9.2 (c (n "pcg_rand") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (f (quote ("i128"))) (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "1pa3fb6b1dls6hc6815lnzm5hdmp38csjbm0dylyw7xf0d7sp2d3")))

(define-public crate-pcg_rand-0.9.3 (c (n "pcg_rand") (v "0.9.3") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1xfy03b1s9jh28r12ak0n196xbhknmm4yn34nm295wvrmfb99a2v") (f (quote (("u128" "rand/i128_support" "num-traits/i128") ("serde1" "serde" "serde_derive" "serde_json") ("default" "u128"))))))

(define-public crate-pcg_rand-0.10.0 (c (n "pcg_rand") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "19sa1f4p7y0r3ax9vvffpm6admdx77hyl3as6qidjv5aiv69r4ag") (f (quote (("u128" "rand/i128_support" "num-traits/i128") ("serde1" "serde" "serde_derive" "serde_json") ("default" "u128"))))))

(define-public crate-pcg_rand-0.10.1 (c (n "pcg_rand") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1hbkllvy52kij47wrr2syannq9vyls8m1pb10zirwmkfn96ma1rs") (f (quote (("u128" "rand/i128_support" "num-traits/i128") ("serde1" "serde" "serde_derive" "serde_json") ("default" "u128"))))))

(define-public crate-pcg_rand-0.11.0 (c (n "pcg_rand") (v "0.11.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0m1h6z3s0h76ihfpyvyvb4mh53cq5s6vga36rr63i972h02817rh") (f (quote (("u128" "num-traits/i128") ("serde1" "serde" "serde_derive" "serde_json") ("default" "u128"))))))

(define-public crate-pcg_rand-0.11.1 (c (n "pcg_rand") (v "0.11.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0wdx0i938wa1xmhv5bmklh16yaxbm4pj4j92g61riqswa5ismy0r") (f (quote (("u128" "num-traits/i128") ("serde1" "serde" "serde_derive" "serde_json") ("default" "u128"))))))

(define-public crate-pcg_rand-0.12.0 (c (n "pcg_rand") (v "0.12.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0w2qijxm1x3h6riqg073f0kg4k3g3p8xdzxdly31rnv54g29ng50") (f (quote (("u128" "num-traits/i128") ("serde1" "serde") ("no_deserialize_verify") ("default" "u128"))))))

(define-public crate-pcg_rand-0.13.0 (c (n "pcg_rand") (v "0.13.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1k4x71y8jbmsv1mja683zsbx05gxkfv7gcnyyx022xyx3g1qacln") (f (quote (("u128" "num-traits/i128") ("serde1" "serde") ("no_deserialize_verify") ("default" "u128"))))))

