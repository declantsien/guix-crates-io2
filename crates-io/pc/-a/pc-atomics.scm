(define-module (crates-io pc -a pc-atomics) #:use-module (crates-io))

(define-public crate-pc-atomics-0.0.1 (c (n "pc-atomics") (v "0.0.1") (h "11ysc15d7zfw0s3kfdv3nw97lggals9h92ivphcr6p13iwj75ix3") (r "1.66")))

(define-public crate-pc-atomics-0.0.2 (c (n "pc-atomics") (v "0.0.2") (h "1fxsg8g8yxrb4r807scv49ll0g3jkfnh98b0n27bnc4s47qiqaj6") (r "1.66")))

