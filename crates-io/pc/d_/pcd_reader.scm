(define-module (crates-io pc d_ pcd_reader) #:use-module (crates-io))

(define-public crate-pcd_reader-0.1.0 (c (n "pcd_reader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "lzf") (r "^0.3.1") (d #t) (k 0)))) (h "016zfdqgipm8j9pigk25chzamg10sz355hniyc81wrc066y5lqqz")))

