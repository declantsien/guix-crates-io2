(define-module (crates-io pc ii pciids) #:use-module (crates-io))

(define-public crate-pciids-0.1.0 (c (n "pciids") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "162v1gqdm66cgafb15x1972mb0qzwc7yk1zfzprsx5d1zjnd6556")))

(define-public crate-pciids-0.1.1 (c (n "pciids") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "15qp1anhl13xwrs04hhq4cgny8qjfp4r49g9d43rizjnhr3jkxp9")))

(define-public crate-pciids-0.1.2 (c (n "pciids") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "0v23j9c92r8lbs9sgykyraw7s3hxi7h5haw5yiwrb6j8w3qsh1jq")))

(define-public crate-pciids-0.1.3 (c (n "pciids") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "0nhg1qv2w9clyrpzdr50gjj26pvhmv5mh01r5fchqd8jxsk4bplh")))

(define-public crate-pciids-0.1.4 (c (n "pciids") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.1, >=2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 2)))) (h "0q0ydwfvkvy6cwcxmy6clk2i3z92a4gh3pfssxa40sz5mpwx6wbd")))

