(define-module (crates-io pc i_ pci_fetch) #:use-module (crates-io))

(define-public crate-pci_fetch-0.1.0 (c (n "pci_fetch") (v "0.1.0") (h "1ini778jgycg4m4n5hdz8w971q35whwqq06cvssc262g9jxdxd1m") (y #t)))

(define-public crate-pci_fetch-1.0.0 (c (n "pci_fetch") (v "1.0.0") (h "1a7pxnilk1vm9ia6nz2vmrgqrb40c5xq049g2wsjk61sm73kscay") (y #t)))

(define-public crate-pci_fetch-2.0.0 (c (n "pci_fetch") (v "2.0.0") (h "17757aav8fgrg9v4bsdi9qh5xhzydnmj7rklksjhqj3van9iqmd7") (y #t)))

(define-public crate-pci_fetch-2.1.0 (c (n "pci_fetch") (v "2.1.0") (h "19yj2345cz5sg8qf0c3wr2vny67s7p6sgqjr45cm758ybp56xs4w") (y #t)))

(define-public crate-pci_fetch-2.2.0 (c (n "pci_fetch") (v "2.2.0") (h "1pwr48vbsxa217djy614ldjsgbva9pwaza8g2mrymz7pnf539yss") (y #t)))

(define-public crate-pci_fetch-2.2.1 (c (n "pci_fetch") (v "2.2.1") (h "0x8hjd8c1cmz003af4zv12c52l97prdzc72cx8alsrjahh49yiqm") (y #t)))

