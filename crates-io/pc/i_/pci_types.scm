(define-module (crates-io pc i_ pci_types) #:use-module (crates-io))

(define-public crate-pci_types-0.1.0 (c (n "pci_types") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)))) (h "0j5731nw8kwx1mqhjp495hlxh40vf8rflncwgw4kyjxma0wvf877")))

(define-public crate-pci_types-0.2.0 (c (n "pci_types") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)))) (h "01zvws6jwjdlpkadp2625sdyclq2nhn0ilyr3ama9cscmmhkfm5h")))

(define-public crate-pci_types-0.3.0 (c (n "pci_types") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)))) (h "0h3q8qy2gdbpv8mcrhavxn0hlchgnmvfspnyl2r7sig3wlkrmqdj")))

(define-public crate-pci_types-0.4.0 (c (n "pci_types") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)))) (h "0x10ap93hvzyj6rdf0f8xfnkmymi515mdk27b7028r9bcd4siyxj")))

(define-public crate-pci_types-0.5.0 (c (n "pci_types") (v "0.5.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)))) (h "190gp52q3rs71khc8fgg5bc5xpb0mcr4pmfqqp6phvm2d1kp6x7c")))

(define-public crate-pci_types-0.6.0 (c (n "pci_types") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)))) (h "0qyi1qhswk0hj3lzs9di33cxs5sbkiv9ddlxk3mygzw2qys7hcpw")))

(define-public crate-pci_types-0.6.1 (c (n "pci_types") (v "0.6.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)))) (h "1ghdc8v5dspzw63p7wvi7i2lq38j90lqj2q7x314nrqhy3mmn7l3")))

(define-public crate-pci_types-0.6.2 (c (n "pci_types") (v "0.6.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)))) (h "0ycwybmg7zfwgx73gcp75r7h9cqnm4rbd10illhzg48pw4p2pb7b")))

(define-public crate-pci_types-0.7.0 (c (n "pci_types") (v "0.7.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "1c26hd0llgmmx2kihmxbxww7jdvmir52sjybl82x6100i3r76p66")))

