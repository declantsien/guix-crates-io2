(define-module (crates-io pc oa pcoa) #:use-module (crates-io))

(define-public crate-pcoa-0.1.0 (c (n "pcoa") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)))) (h "0qp57gmyn4adg6ydw00y7fg77vb76wmqb3xwx4832pzzx9p595pk")))

(define-public crate-pcoa-0.1.1 (c (n "pcoa") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)))) (h "1mynvsvyja50pyh9vg4q16m003xn0dan5il8nkr95jyaw36ilgqk")))

(define-public crate-pcoa-0.1.2 (c (n "pcoa") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)))) (h "0yx0dsg5bjxqwshxnkgwv72ij0jlw4zzr0s0gp9daf9sdd9xipyi")))

