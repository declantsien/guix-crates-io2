(define-module (crates-io pc ge pcgen) #:use-module (crates-io))

(define-public crate-pcgen-0.1.0 (c (n "pcgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w1la564iva6gwsy26xd7k3zmpn0mzj8fm86lqjf997m0h7qah39")))

(define-public crate-pcgen-0.1.1 (c (n "pcgen") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c2il669anhn6h7afx93sbx3byl733rziqhra8n75wzm1n1h2snn")))

