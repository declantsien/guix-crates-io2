(define-module (crates-io pc i- pci-ids) #:use-module (crates-io))

(define-public crate-pci-ids-0.2.1 (c (n "pci-ids") (v "0.2.1") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1q49kb7898bw7rqnwfz514j4hlj53jp84ql67b8is490a9k83d5r")))

(define-public crate-pci-ids-0.2.2 (c (n "pci-ids") (v "0.2.2") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "015g0pilgsfw1im8rlck1knb2spqk7ab537vdch37a5xsma1h835")))

(define-public crate-pci-ids-0.2.3 (c (n "pci-ids") (v "0.2.3") (d (list (d (n "nom") (r "^7.1") (k 1)) (d (n "phf") (r "^0.10") (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "059q6rfc61y8vg9rxb1z5hl7jkbijcwlx783s737d5jahd9s3q6c")))

(define-public crate-pci-ids-0.2.4 (c (n "pci-ids") (v "0.2.4") (d (list (d (n "nom") (r "^7.1") (k 1)) (d (n "phf") (r "^0.10") (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1qjv1vn9q8q9y68dimr9xnqd1pd4b50fs47wb7zbq8n02r9l3b00")))

(define-public crate-pci-ids-0.2.5 (c (n "pci-ids") (v "0.2.5") (d (list (d (n "nom") (r "^7.1") (k 1)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1bx6acc2886p0hn52f01x6iw2w8yjvawvnrdkip8apa13clf72nq")))

