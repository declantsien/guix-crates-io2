(define-module (crates-io pc i- pci-driver) #:use-module (crates-io))

(define-public crate-pci-driver-0.1.0 (c (n "pci-driver") (v "0.1.0") (d (list (d (n "byte-strings") (r "^0.2.2") (d #t) (k 2)) (d (n "nix") (r "^0.24.1") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1k6m9zy7r4bx36m6jl7sdn9vqslnlf4kfcfxspszvn0ds9wn5b8w") (f (quote (("vfio" "nix/ioctl" "nix/mman") ("default" "vfio") ("_unsafe-op-in-unsafe-fn"))))))

(define-public crate-pci-driver-0.1.1 (c (n "pci-driver") (v "0.1.1") (d (list (d (n "byte-strings") (r "^0.2.2") (d #t) (k 2)) (d (n "nix") (r "^0.24.1") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1falill35qnffwk916yp48w29ng9j9mg6rrb813an1ifr0bmp8h3") (f (quote (("vfio" "nix/ioctl" "nix/mman") ("default" "vfio") ("_unsafe-op-in-unsafe-fn"))))))

(define-public crate-pci-driver-0.1.2 (c (n "pci-driver") (v "0.1.2") (d (list (d (n "byte-strings") (r "^0.2.2") (d #t) (k 2)) (d (n "nix") (r "^0.24.1") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "17ykdmm5niw631ybfvjv4hf9rh8gfpim11cixnp9x8yqn852lc0h") (f (quote (("vfio" "nix/ioctl" "nix/mman") ("default" "vfio") ("_unsafe-op-in-unsafe-fn"))))))

(define-public crate-pci-driver-0.1.3 (c (n "pci-driver") (v "0.1.3") (d (list (d (n "byte-strings") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "18nv642r0pac2fiphcfgl88r2g6nr3h19xh8r2p8my0kjqdz2sgf") (f (quote (("vfio" "libc/std") ("default" "vfio") ("_unsafe-op-in-unsafe-fn"))))))

(define-public crate-pci-driver-0.1.4 (c (n "pci-driver") (v "0.1.4") (d (list (d (n "byte-strings") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1zch6fh30pi2l8ra1032nk31ifxxwma2zhvc3ylpka8xx64jp26s") (f (quote (("vfio" "libc/std") ("default" "vfio") ("_unsafe-op-in-unsafe-fn")))) (r "1.56")))

