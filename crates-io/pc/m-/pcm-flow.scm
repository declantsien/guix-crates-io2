(define-module (crates-io pc m- pcm-flow) #:use-module (crates-io))

(define-public crate-pcm-flow-0.1.0 (c (n "pcm-flow") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "1vb22vavkm05xcv517q0i3yfbfiqzpqi28p53n944mszsl29cr1v")))

(define-public crate-pcm-flow-0.2.0 (c (n "pcm-flow") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "04b07d1ryn8scwjk30ab806naa2d8ik10kz2sd6lw2jxmxkhiras")))

(define-public crate-pcm-flow-0.2.1 (c (n "pcm-flow") (v "0.2.1") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "0zym3sfdwjdlx4573pajyc762nm04jp4am8rrldh4gpi0r3iqnfp")))

(define-public crate-pcm-flow-0.3.0 (c (n "pcm-flow") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "0r8nawmdd2a5ddb8zpdgckx5hv4l6bhbsil7zn4g0864vyfwl8hm")))

(define-public crate-pcm-flow-0.3.1 (c (n "pcm-flow") (v "0.3.1") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "04pa4jkn1mdbidp0jzr9hhmplmbcv4lxv001j0hbyq4icil3vn4g")))

(define-public crate-pcm-flow-0.3.2 (c (n "pcm-flow") (v "0.3.2") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.8.1") (d #t) (k 0)))) (h "0wknr5jx8qj6szy7nwm3b8z4v1x3kpv4mq7blhq2xkjgd3j3vqkl")))

(define-public crate-pcm-flow-0.3.3 (c (n "pcm-flow") (v "0.3.3") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "05k9xmgr5mswl8lmjclmkphks3dljj0xq44mf231mz8n38dhbr3a")))

(define-public crate-pcm-flow-0.4.0 (c (n "pcm-flow") (v "0.4.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "1gkiwxsqw4nvj9wa034brkdl9lrbji4m0bwdr8fdyqwip8qk79rn")))

(define-public crate-pcm-flow-0.5.0 (c (n "pcm-flow") (v "0.5.0") (d (list (d (n "petgraph") (r "^0.4.10") (d #t) (k 0)) (d (n "sample") (r "^0.9.0") (d #t) (k 0)))) (h "1p8igb8y176j70w3ngb2afrv29mrz3h8knp59rc8bwiba10vbq95")))

