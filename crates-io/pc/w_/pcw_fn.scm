(define-module (crates-io pc w_ pcw_fn) #:use-module (crates-io))

(define-public crate-pcw_fn-0.1.0 (c (n "pcw_fn") (v "0.1.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "16dx37qyh4i77ffcyb07h7bkikwvrixw4i9p9p73lc1acf6x5cq8")))

(define-public crate-pcw_fn-0.1.1 (c (n "pcw_fn") (v "0.1.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1i7jq2l7146aaw3g7vjwjdfqaf3w7yidamd9nrqm40i6d4xqryyh")))

(define-public crate-pcw_fn-0.1.2 (c (n "pcw_fn") (v "0.1.2") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "0pjgqispirdr7d7jmj0709mzsqm5bgziy43k5pv46x3vapqf5l7b") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-pcw_fn-0.2.0 (c (n "pcw_fn") (v "0.2.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "0ji3v6fj3p4664pg6dy2hd9sdli64nymcagivr5hgl7cdvzpyadj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-pcw_fn-0.2.1 (c (n "pcw_fn") (v "0.2.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r5yfp1gr74xxy3n8f9hwz6ah7pknjvhzzmqsraxcfcskd82pq9d") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

