(define-module (crates-io pc -b pc-beeper) #:use-module (crates-io))

(define-public crate-pc-beeper-0.1.0 (c (n "pc-beeper") (v "0.1.0") (d (list (d (n "x86_64") (r "^0.11.0") (d #t) (k 0)))) (h "11y7w76729vjnhr2315dz7644nagyazn1dbkpyxm1s6pxfyqwmhi")))

(define-public crate-pc-beeper-0.1.1 (c (n "pc-beeper") (v "0.1.1") (d (list (d (n "x86_64") (r "^0.14.10") (d #t) (k 0)))) (h "06yd889xlp6d8whvips4lawnxi4x8gzy6byyqcv5qy9p5z4marhn")))

