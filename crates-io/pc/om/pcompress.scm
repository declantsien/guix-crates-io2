(define-module (crates-io pc om pcompress) #:use-module (crates-io))

(define-public crate-pcompress-0.1.0 (c (n "pcompress") (v "0.1.0") (d (list (d (n "mimalloc") (r "^0.1.17") (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1r8j02nha6y95viwxw70fii0b86yd2mga1i59hayz8h7zknxmzd6") (y #t)))

(define-public crate-pcompress-0.1.1 (c (n "pcompress") (v "0.1.1") (d (list (d (n "mimalloc") (r "^0.1.17") (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0bz6ih2gmcg4035534lqxqqvax573fchjd1d8d44mslzs55jhssh") (y #t)))

(define-public crate-pcompress-0.2.1 (c (n "pcompress") (v "0.2.1") (d (list (d (n "mimalloc") (r "^0.1.17") (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1skqlqynf13mq67z4717xss8l8yaghhfxc8irn4j6zm4rap9mc9i") (y #t)))

(define-public crate-pcompress-0.2.2 (c (n "pcompress") (v "0.2.2") (d (list (d (n "mimalloc") (r "^0.1.17") (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1v5qkvc97cf8yd33sj80j78yf33szxr3n16jb0rhm8jppq7zgclb") (y #t)))

(define-public crate-pcompress-1.0.0 (c (n "pcompress") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "13n53ni4slxlc0c8rzsn3x6ns3zkl9g2da2l74w64jxdl8h000hb") (y #t)))

(define-public crate-pcompress-1.0.2 (c (n "pcompress") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1g5xnhdndgxrwjd6rm4b6hz05i29yc8397hjv7rbaggdmw9y1b9b") (y #t)))

(define-public crate-pcompress-1.0.3 (c (n "pcompress") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1m1vd83ipga6dm6kx9vz0idaza5pr97az9h8pq0mywa44r4ziank") (y #t)))

(define-public crate-pcompress-1.0.4 (c (n "pcompress") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0lvdvc9rg6wybnhy2klcbywy40dikfvrxvzqzsayqajil1qi4a8h") (y #t)))

(define-public crate-pcompress-1.0.5 (c (n "pcompress") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0qlgyx9j6h61r1gj70xhg9c4fm48cxj8k9cyzrjnc842vxr83kf4") (y #t)))

(define-public crate-pcompress-1.0.6 (c (n "pcompress") (v "1.0.6") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dk30pzn33zhfg5mwpphqcg204kclzm3axd47dqzf81cfikrbv16")))

(define-public crate-pcompress-1.0.7 (c (n "pcompress") (v "1.0.7") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "019ivckbsd62j59bybqb1hwnr5dmj0c9hzizbm8n0zm3ribkl0jr")))

