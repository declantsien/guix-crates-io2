(define-module (crates-io pc om pcomb) #:use-module (crates-io))

(define-public crate-pcomb-0.1.0 (c (n "pcomb") (v "0.1.0") (h "1c76v7q3whscw7f6n2cknkkc264zf8c6ph75ii8iyg5m3y491fsh")))

(define-public crate-pcomb-0.2.0 (c (n "pcomb") (v "0.2.0") (h "04ia5pgpzb8s6y2hldkd7svpqxpm3bqzy23l61hmm21sf7gbxr31") (f (quote (("default" "builtin_parsers") ("builtin_parsers"))))))

(define-public crate-pcomb-0.3.0 (c (n "pcomb") (v "0.3.0") (h "1i06wcdx916hdwdajn6krfqkhsr150ppdwc67m6wh4ffmhsqrv11") (f (quote (("std") ("default" "builtin_parsers" "std") ("builtin_parsers"))))))

