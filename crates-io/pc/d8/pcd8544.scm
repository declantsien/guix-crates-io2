(define-module (crates-io pc d8 pcd8544) #:use-module (crates-io))

(define-public crate-pcd8544-0.1.0 (c (n "pcd8544") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "04zvr84wjqcqvqhjm3jmsg2pmizpr6y94ys7890mi82a8iiz8j65")))

(define-public crate-pcd8544-0.1.1 (c (n "pcd8544") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0i8h4qws2gs78nqrcyvfw2djy83g5fiwfdy3zcx2snnla3bbjgc3")))

(define-public crate-pcd8544-0.1.2 (c (n "pcd8544") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0iqrj7cz4xcgpq3i1fl4awkb6xq4bh9999571glcl3vz56afbvqf")))

(define-public crate-pcd8544-0.1.3 (c (n "pcd8544") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.1.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "01m1fbjailzpyswx95gla6v77pp2xpdkqqbzbk5j4rwx3q88ag5j")))

(define-public crate-pcd8544-0.1.4 (c (n "pcd8544") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "16x59m140qg4q91yhklwb2rb28sgdf58alhvrnbh2alj5sl5wcdm")))

(define-public crate-pcd8544-0.1.5 (c (n "pcd8544") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "012sfl6xd9xld0gig5yhchgfxaipn2ksn9pf2xgm82z2qhdqmqsf")))

(define-public crate-pcd8544-0.1.6 (c (n "pcd8544") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "18wx74l9q5mjy33mfxabcmi2g7lhyjl957c8ab1qsh19shskrz6q")))

(define-public crate-pcd8544-0.1.7 (c (n "pcd8544") (v "0.1.7") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0ddnbgmk29i3nkanvpfqkll0jxw48xi8z1q3y6s3lkyfrsvs2qnq")))

(define-public crate-pcd8544-0.1.8 (c (n "pcd8544") (v "0.1.8") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1sazi7c1acfrb88hv0aw2fds7mf457nm2jvxxzdpf121pj1yn0lv")))

(define-public crate-pcd8544-0.2.0 (c (n "pcd8544") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1q5y2akgj5fghpr80aphq8lv7rz9vvyim6wvh9x2qgjf87y9f52p")))

