(define-module (crates-io pc el pcell) #:use-module (crates-io))

(define-public crate-pcell-0.7.0 (c (n "pcell") (v "0.7.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "md5") (r "^0.3.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "snap") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1h4j4bm80pbjh1cxzc65gs3yba3j43y0h886vraldna8hrpa3ynb")))

(define-public crate-pcell-0.7.1 (c (n "pcell") (v "0.7.1") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "md5") (r "^0.3.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "snap") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0pa347iymk002rp0b2i2vxrmq4rx4yrhillksnca5qpg4hzyr1ca")))

