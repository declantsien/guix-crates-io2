(define-module (crates-io pc ac pcacsv) #:use-module (crates-io))

(define-public crate-pcacsv-0.1.0 (c (n "pcacsv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "number_range") (r "^0.3.2") (d #t) (k 0)) (d (n "pca") (r "^0.1.4") (d #t) (k 0)) (d (n "trimothy") (r "^0.1.8") (d #t) (k 0)))) (h "18k16awv6ynzwlgf9p734ggsfb6xrn3j36kjm95508v4gbbd00mp")))

