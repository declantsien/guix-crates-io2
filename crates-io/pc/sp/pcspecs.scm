(define-module (crates-io pc sp pcspecs) #:use-module (crates-io))

(define-public crate-pcspecs-0.1.0 (c (n "pcspecs") (v "0.1.0") (d (list (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.7") (d #t) (k 0)))) (h "1aww8insyb8cch9zvxadvbz6pclnmy1fpkmhy7g5dqign99r8jff") (y #t)))

(define-public crate-pcspecs-0.1.1 (c (n "pcspecs") (v "0.1.1") (d (list (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.7") (d #t) (k 0)))) (h "0l854g7qj7ch9hd9hqnsl4n3799rmcswf92gawrnwf0n031iwr9x") (y #t)))

(define-public crate-pcspecs-0.2.0 (c (n "pcspecs") (v "0.2.0") (d (list (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.7") (d #t) (k 0)))) (h "17dyp51cnrb9rv4z0x94kmmrn6gxjs8imhrbl8rlrkh1shfaivw1") (y #t)))

(define-public crate-pcspecs-0.2.1 (c (n "pcspecs") (v "0.2.1") (d (list (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.7") (d #t) (k 0)))) (h "0hiqyyy024m1by73wih082a85xzdprhrkspas2wn3cqaglzs530g")))

