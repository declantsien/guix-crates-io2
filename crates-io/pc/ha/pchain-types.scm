(define-module (crates-io pc ha pchain-types) #:use-module (crates-io))

(define-public crate-pchain-types-0.1.0 (c (n "pchain-types") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)))) (h "0hpsx9jhlgjq8bsi81gws4ahrzs633hf2xj78pmh308smxml32cr")))

(define-public crate-pchain-types-0.2.0 (c (n "pchain-types") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xnmpangwy7kk5d8vr2ygwd6s4q7lmwmb7wdv421f9x437zj0bvy")))

(define-public crate-pchain-types-0.2.1 (c (n "pchain-types") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09fwkkqs1dvrvlzydik13bimrk93czkkkchck0v5q507fds6qwjv")))

(define-public crate-pchain-types-0.3.0 (c (n "pchain-types") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hotstuff_rs_types") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rs_merkle") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1zibdpdmy33rrcbqb8388nnqikz6754snscy2v70fl7sp83ymif1")))

(define-public crate-pchain-types-0.4.1 (c (n "pchain-types") (v "0.4.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hotstuff_rs") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0pcfhnjsl78nl4y0m090wn5cjkp6jdibk0jzcm2794z18ms0762v") (f (quote (("default")))) (s 2) (e (quote (("keygen" "dep:rand" "dep:rand_chacha"))))))

(define-public crate-pchain-types-0.4.2 (c (n "pchain-types") (v "0.4.2") (d (list (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hotstuff_rs") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rs_merkle") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0mkilb08k0qkik0l5k79gh7nlv4v3n46cb6x04jhnwd98y25h56k")))

(define-public crate-pchain-types-0.4.3 (c (n "pchain-types") (v "0.4.3") (d (list (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hotstuff_rs") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rs_merkle") (r "^1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1avkha2w8qlgjb9f66918nxz2n0a25kpghj5a9crvj13g6si0qyk")))

