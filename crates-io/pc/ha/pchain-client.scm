(define-module (crates-io pc ha pchain-client) #:use-module (crates-io))

(define-public crate-pchain-client-0.4.2 (c (n "pchain-client") (v "0.4.2") (d (list (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "pchain-types") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0960vddnvmcrnbcfl4kmxipl9w4h984dvapiaf7dlakrdl2q6j21")))

(define-public crate-pchain-client-0.4.3 (c (n "pchain-client") (v "0.4.3") (d (list (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "pchain-types") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bvaf1dwnwd47l6gn93hg5vn4kb0g7lj6s1cb81sdlr8gcf6gndz")))

