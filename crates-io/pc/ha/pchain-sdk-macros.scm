(define-module (crates-io pc ha pchain-sdk-macros) #:use-module (crates-io))

(define-public crate-pchain-sdk-macros-0.1.0 (c (n "pchain-sdk-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "snakecase") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1a02vslzpjdzdl4d4mm47m9sqq5r0splgcgfcx7wiv5cdz44wwmh")))

(define-public crate-pchain-sdk-macros-0.2.0 (c (n "pchain-sdk-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "snakecase") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1k4zfglalz25yy62iii6vb5j947ggz134jqpdnl95mzm1lbb7ni1")))

(define-public crate-pchain-sdk-macros-0.3.0 (c (n "pchain-sdk-macros") (v "0.3.0") (d (list (d (n "pchain-types") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "snakecase") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1qzrwcx1cq9c1yfclq6kw6j1014yl1zxwknnpdkfwcqhj0cmi5nb")))

(define-public crate-pchain-sdk-macros-0.4.2 (c (n "pchain-sdk-macros") (v "0.4.2") (d (list (d (n "base64url") (r "^0.1.0") (d #t) (k 0)) (d (n "pchain-types") (r "=0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "snakecase") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "10mb1p146qm8hswqqga0v8p6dxqq4m21v4zzah7gg7ljziprwavm")))

