(define-module (crates-io pc ha pchain-sdk) #:use-module (crates-io))

(define-public crate-pchain-sdk-0.1.0 (c (n "pchain-sdk") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "protocol_types") (r "^0.1.0") (d #t) (k 0) (p "pchain-types")))) (h "07y67r6dhfh9a2xxf7cbj08ppk7lj3s29f11p8dj51hxqrf70xzx")))

(define-public crate-pchain-sdk-0.2.0 (c (n "pchain-sdk") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.2") (d #t) (k 0)) (d (n "protocol_types") (r "^0.2") (d #t) (k 0) (p "pchain-types")))) (h "072iiv5sn3yx000vrdzlskpsxjimpi9cl0m5hfqfmqlhad5bxpam")))

(define-public crate-pchain-sdk-0.2.1 (c (n "pchain-sdk") (v "0.2.1") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.2") (d #t) (k 0)) (d (n "protocol_types") (r "^0.2") (d #t) (k 0) (p "pchain-types")))) (h "13v96nmr67kxf4qhq0fg4maw05g7wxmi974pwgw5x2gpn9ybkmal")))

(define-public crate-pchain-sdk-0.3.0 (c (n "pchain-sdk") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "pchain-types") (r "^0.3") (d #t) (k 0)))) (h "0cy9z4cr6ydm77k8gra8zxllh1asay4nxldwi1l8rqjh52vwssrk")))

(define-public crate-pchain-sdk-0.3.1 (c (n "pchain-sdk") (v "0.3.1") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "pchain-types") (r "^0.3") (d #t) (k 0)))) (h "1kwb27cid5vany6kwj7ivw6rb7np668gab8kr16vh91dgcsw651w")))

(define-public crate-pchain-sdk-0.3.2 (c (n "pchain-sdk") (v "0.3.2") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "^0.3") (d #t) (k 0)) (d (n "pchain-types") (r "^0.3") (d #t) (k 0)))) (h "1yn25gdsn8qqyq7frnxv9nmv9f47h268wjsqjf4psj0ndl6hvgm1")))

(define-public crate-pchain-sdk-0.4.2 (c (n "pchain-sdk") (v "0.4.2") (d (list (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "pchain-sdk-macros") (r "=0.4.2") (d #t) (k 0)) (d (n "pchain-types") (r "=0.4.3") (d #t) (k 0)))) (h "059pdgmd99906s77xv314kbb5mk149a8agfsz7xrwkjni26c6cac")))

