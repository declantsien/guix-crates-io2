(define-module (crates-io pc ha pchain-world-state) #:use-module (crates-io))

(define-public crate-pchain-world-state-0.4.2 (c (n "pchain-world-state") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64url") (r "^0.1.0") (d #t) (k 0)) (d (n "borsh") (r "=0.10.2") (d #t) (k 0)) (d (n "hash-db") (r "^0.15.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.15.3") (d #t) (k 0)) (d (n "pchain-types") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reference-trie") (r "^0.25.0") (d #t) (k 0)) (d (n "trie-db") (r "^0.23.1") (d #t) (k 0)))) (h "1f6ycfx2kx662ni6n52zi4b7a2fm0vlacl7vzgphkfn34c1fz5ds")))

