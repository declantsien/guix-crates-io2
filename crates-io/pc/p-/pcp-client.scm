(define-module (crates-io pc p- pcp-client) #:use-module (crates-io))

(define-public crate-pcp-client-0.1.0 (c (n "pcp-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0m6byklrlq2ydyxnvs4krh4nlgpwzk1bb9ydpk7xx71w41093v9j")))

