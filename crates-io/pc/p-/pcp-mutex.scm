(define-module (crates-io pc p- pcp-mutex) #:use-module (crates-io))

(define-public crate-pcp-mutex-0.1.0 (c (n "pcp-mutex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "16wyrz6y2p5lmprxxyyj0if3xfcfwrxbpkx42x2bmqsxy8n7kjgp")))

(define-public crate-pcp-mutex-0.1.1 (c (n "pcp-mutex") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "1naiadh2kc8a9d8bmpq60jczq32rmxg7bdsvcgzj60947rqm3dd7")))

(define-public crate-pcp-mutex-0.1.2 (c (n "pcp-mutex") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "1km4yc4w7ry9qck74h4q5jjfwz6m8r0761sqsw7dpz2gixzim4yj")))

(define-public crate-pcp-mutex-0.2.0 (c (n "pcp-mutex") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linux-futex") (r "^0.1") (d #t) (k 0)))) (h "1jrkwa4fxz8j3i5nm7yg80rc1dqd8ri8hc3kqzcmmywwgbw184z5")))

