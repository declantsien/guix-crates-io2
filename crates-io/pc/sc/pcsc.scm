(define-module (crates-io pc sc pcsc) #:use-module (crates-io))

(define-public crate-pcsc-0.1.0-alpha1 (c (n "pcsc") (v "0.1.0-alpha1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "10z7hhdbsnj4a2zs1c41cg52b3cxg1ikm9q4xyp80m9l48hhf05c")))

(define-public crate-pcsc-0.1.0-alpha2 (c (n "pcsc") (v "0.1.0-alpha2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "1fw2l8mwb8ap8j2cyivmljckr9zm4fm8fgc9fa13kqchsjkgfn1a")))

(define-public crate-pcsc-0.1.0-alpha3 (c (n "pcsc") (v "0.1.0-alpha3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "0y9qan99yjvm62kj00b8hj96mnkmr4nmw7vwc4hwldanlrd3v6q8")))

(define-public crate-pcsc-0.1.0-alpha4 (c (n "pcsc") (v "0.1.0-alpha4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1.0-alpha4") (d #t) (k 0)))) (h "14m1v0gxb0g5ip3n1bv2gvjgh1h4l1iw6gacdwladmrrlzks12k9")))

(define-public crate-pcsc-0.1.0 (c (n "pcsc") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1a6a15kq156n5x582jlrkhzyifvc34sg5p6a0ik7ik7mm8v569pa")))

(define-public crate-pcsc-0.1.1 (c (n "pcsc") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1.1") (d #t) (k 0)))) (h "13nkfz1bydmwws2vx2j0i8rmnxvc080gipykjxj927rs11bpd7hi")))

(define-public crate-pcsc-0.1.2 (c (n "pcsc") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "pcsc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1m0m303ffhxxlfsk32gq76lcfhky6999gly3h5gr4761zpq340wh")))

(define-public crate-pcsc-1.0.0 (c (n "pcsc") (v "1.0.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1lbdgbbal0lghxdxgzqv692haakls3sjap0yla9mm9sb5ymx47gr")))

(define-public crate-pcsc-1.0.1 (c (n "pcsc") (v "1.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.0.1") (d #t) (k 0)))) (h "0mw6lv5sbd8cf387wc5c1y96p3h8azin0q8n9ylnsrqps0p67prl")))

(define-public crate-pcsc-2.0.0 (c (n "pcsc") (v "2.0.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.0.1") (d #t) (k 0)))) (h "03bbr6c4vs0phyk89904537221111frdrlvvqy6jnsqx9yi7z9j3")))

(define-public crate-pcsc-2.1.0 (c (n "pcsc") (v "2.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.0.1") (d #t) (k 0)))) (h "193h8dr4jwx26wvccz55rdim8zsf2rk814bvd7sh30rz0j6fcx07")))

(define-public crate-pcsc-2.1.1 (c (n "pcsc") (v "2.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.0.1") (d #t) (k 0)))) (h "10xbag0gvgx1lcd13yzm2zi6acraddg302aw6ahxbfmhkix44753")))

(define-public crate-pcsc-2.2.0 (c (n "pcsc") (v "2.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.1.0") (d #t) (k 0)))) (h "0ach9ih4j09alip859332wassxdr8ks0923r3ip1ragafnls06ai")))

(define-public crate-pcsc-2.3.0 (c (n "pcsc") (v "2.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "1nfanhkaj832x3l0jj3dmq6alfpk19jni94caamnb5g33rabaln9")))

(define-public crate-pcsc-2.3.1 (c (n "pcsc") (v "2.3.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "0k42j09kij0fbqhvmrzmba4m14md4lb9zcsa0k6fdbq674inw4w7")))

(define-public crate-pcsc-2.4.0 (c (n "pcsc") (v "2.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "10vcnmxrpscg0fcrzz0r1klgiqzgh2axvyg1zyqwk8h5hy6rmq48")))

(define-public crate-pcsc-2.5.0 (c (n "pcsc") (v "2.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "066v2234m24z6smk3a4vv034n6sc23mh3bahm154fw14x2y2vj6l")))

(define-public crate-pcsc-2.6.0 (c (n "pcsc") (v "2.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "0nznls3mn5dkw6xark63rl5g82l7ccjk75sc0884b45r15i3ayqg")))

(define-public crate-pcsc-2.7.0 (c (n "pcsc") (v "2.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "1i7xqknwfwi990nw452sr0fy3zgrl1avvdbgs3nawcx4g3gf8aby")))

(define-public crate-pcsc-2.8.0 (c (n "pcsc") (v "2.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "1d5smcr9irdppl20r7wg2s5xqwfdxx5sb7q5v2l0is04knzb1jip") (r "1.38")))

(define-public crate-pcsc-2.8.1 (c (n "pcsc") (v "2.8.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "0nnsljri06w5c9lj99h08rprfkn0dxb4f900wx39zcri4gsyw4xv") (r "1.38")))

(define-public crate-pcsc-2.8.2 (c (n "pcsc") (v "2.8.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pcsc-sys") (r "^1.2.0") (d #t) (k 0)))) (h "027a2s8lp6w025aa758s84qszcwkyg92s1mhvplrqzbbh5zrvva5") (r "1.38")))

