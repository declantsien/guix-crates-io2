(define-module (crates-io pc sc pcsc-sys) #:use-module (crates-io))

(define-public crate-pcsc-sys-0.1.0-alpha4 (c (n "pcsc-sys") (v "0.1.0-alpha4") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "01lwgvnl6xiqpwrr163g03p0q3bpfvfcxwmm7qlq1ail8d43y90j")))

(define-public crate-pcsc-sys-0.1.0 (c (n "pcsc-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0r4phf46fzrlx4293c5qcwwh1rpayngjmj9lcv2rznm6zixmb2ji")))

(define-public crate-pcsc-sys-0.1.1 (c (n "pcsc-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "17wkvfi5jfa1hai2hnsgam46i4gl5qhaxskv4m9g31lmrjnigz88")))

(define-public crate-pcsc-sys-0.1.2 (c (n "pcsc-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1ldr7dnrrx1hdkrml5asi71ljkapfjy3g3c7zpgvmnm27hbnw7fv")))

(define-public crate-pcsc-sys-1.0.0 (c (n "pcsc-sys") (v "1.0.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0xcbnlm5fccpzx8smfqrwjka5fc1mlmgmpzhqw2dqyv2pmr39fhr")))

(define-public crate-pcsc-sys-1.0.1 (c (n "pcsc-sys") (v "1.0.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0l0z9iyxq2a9j1bpfaaq0pacz4305snyga5kc69xcrcfrpari8dy")))

(define-public crate-pcsc-sys-1.1.0 (c (n "pcsc-sys") (v "1.1.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1k7vwi0h2nmsgkl04p5mjjxgv0mh4vz8xsx0v490mzxw413gqy19") (l "pcsc")))

(define-public crate-pcsc-sys-1.2.0 (c (n "pcsc-sys") (v "1.2.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1si37v9n07r3csqcnnqn4i82j75b6dssyz0fzdg1n3rcpbnbzdz1") (l "pcsc")))

(define-public crate-pcsc-sys-1.2.1 (c (n "pcsc-sys") (v "1.2.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "00vlrfv3kcr49ajbzzr1b4ls7g28f97mj9vdjdzick9c1yl9p7mh") (l "pcsc") (r "1.38")))

