(define-module (crates-io pc sc pcsclite-sys) #:use-module (crates-io))

(define-public crate-pcsclite-sys-0.1.0 (c (n "pcsclite-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06xk9brqnaxh4nsvqcyp1gf92ws6skwz14hvwqf7pppyscy1hmvg")))

(define-public crate-pcsclite-sys-0.1.1 (c (n "pcsclite-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "143zkzg27a48f8z32cma8kw6cfzm5w4p6h5r725y9i5y20l2qa9h")))

(define-public crate-pcsclite-sys-0.1.2 (c (n "pcsclite-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17i4k5rgz3qq29a1f052kckwc1cdq5ir5irjhdvrx5a56z2l6527")))

