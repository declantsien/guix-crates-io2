(define-module (crates-io pc g- pcg-mwc) #:use-module (crates-io))

(define-public crate-pcg-mwc-0.1.0 (c (n "pcg-mwc") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)))) (h "0gczv74qpxx2ivlsbxvyvqm17ppch17m0sc5ynqc1gnk8bd3dsic")))

(define-public crate-pcg-mwc-0.2.0 (c (n "pcg-mwc") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jd08wb9zxqyvn6br2vm22rd0gxsn0anha09m408jlcxk66pfbcz") (f (quote (("serde1" "serde"))))))

(define-public crate-pcg-mwc-0.2.1 (c (n "pcg-mwc") (v "0.2.1") (d (list (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z6cpz8x387iqcx8kjnqfihgggi0yngqx73zwjz132y56f38a5i2") (f (quote (("serde1" "serde"))))))

