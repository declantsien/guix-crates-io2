(define-module (crates-io pc bc pcbc) #:use-module (crates-io))

(define-public crate-pcbc-0.0.0 (c (n "pcbc") (v "0.0.0") (h "1vs0ijdqv12aihsxp4cd8bfnjawdaaw0kwyv7kykq0xk8m6yfm58")))

(define-public crate-pcbc-0.1.0 (c (n "pcbc") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "1a8l3nv38wnyf4n3pimkmnb5j1iqcicmmr1mzj5fng70jlgms1vq") (f (quote (("zeroize" "cipher/zeroize") ("default" "block-padding") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-pcbc-0.1.1 (c (n "pcbc") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "1hwza44k6i55sxqjrklv964g53wj3n0014i1a0a65x52hw04xg54") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

(define-public crate-pcbc-0.1.2 (c (n "pcbc") (v "0.1.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "03rwsi7z340c12ll1s74pc8508gj2l2rb4izvqxw7gdr1sbyc6df") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

