(define-module (crates-io pc hm pchmod) #:use-module (crates-io))

(define-public crate-pchmod-0.1.0 (c (n "pchmod") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0wqw3gnvv3d7nk3091x46nbljcxrbln78g261d1xh6jpchhnxhpc")))

