(define-module (crates-io pc at pcat) #:use-module (crates-io))

(define-public crate-pcat-0.1.0 (c (n "pcat") (v "0.1.0") (h "1jp2l0j9i8gd1r20hbw2jrb4x5571j2k4fb5ajnr4y2i5fp2ly9s")))

(define-public crate-pcat-0.1.1 (c (n "pcat") (v "0.1.1") (h "1ghcqyzbqz60ii9nyxz2k6fn0mh5g9sd6bnx7r3ll7s040w6szmc")))

(define-public crate-pcat-0.1.2 (c (n "pcat") (v "0.1.2") (h "0jq6bl2jkhnasbsf5lqq1iqjr7i0lmj1np783v64bbymggdjb71g")))

