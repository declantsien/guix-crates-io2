(define-module (crates-io pc b- pcb-rs) #:use-module (crates-io))

(define-public crate-pcb-rs-0.1.0 (c (n "pcb-rs") (v "0.1.0") (d (list (d (n "pcb-rs-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "pcb-rs-traits") (r "^0.1.0") (d #t) (k 0)))) (h "12vpvnzv124qh4iwhrq3ldc8zigp41s9bsj4ffhyh158ayrqm5wc")))

