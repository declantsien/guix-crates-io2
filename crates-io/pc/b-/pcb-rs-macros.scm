(define-module (crates-io pc b- pcb-rs-macros) #:use-module (crates-io))

(define-public crate-pcb-rs-macros-0.1.0 (c (n "pcb-rs-macros") (v "0.1.0") (d (list (d (n "pcb-rs-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ca6np5nx6gi281hgz0ybix03birxcqmaa5x620zq51asf3sv80d")))

