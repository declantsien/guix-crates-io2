(define-module (crates-io pc b- pcb-c) #:use-module (crates-io))

(define-public crate-pcb-c-0.1.3 (c (n "pcb-c") (v "0.1.3") (d (list (d (n "pcb") (r "^0.1.2") (d #t) (k 0)))) (h "09y3ly4fh5pc8mkyc72vq51dj0l2xqsks6nm1wspqg8zwi98zxrj")))

(define-public crate-pcb-c-0.2.0 (c (n "pcb-c") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pcb") (r "^0.2.0") (d #t) (k 0)) (d (n "pcb-llvm") (r "^0.2.0") (d #t) (k 0)))) (h "0dvjway1aspwkqjsny8h50xkw94s8wyv1wadzbqjfd1mg3kjx75w")))

