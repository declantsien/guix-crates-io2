(define-module (crates-io pc b- pcb-core) #:use-module (crates-io))

(define-public crate-pcb-core-0.1.2 (c (n "pcb-core") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.1.0") (d #t) (k 0)))) (h "1wbsw8iikkxxpq9p5ghlkm4w7iz09k7qpykdqi9589fz6knkksz0")))

(define-public crate-pcb-core-0.1.3 (c (n "pcb-core") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.1.0") (d #t) (k 0)))) (h "0ysyp0l85w9dn7b7gif121i5jy8q8vb0pbfz54abwagc7l90nixm")))

(define-public crate-pcb-core-0.2.0 (c (n "pcb-core") (v "0.2.0") (d (list (d (n "typed-arena") (r "^1.1.0") (d #t) (k 0)))) (h "02k0gls1aw315wnyhpkhsg7kfzv9kbspziq952nx21dxb4r15d15")))

