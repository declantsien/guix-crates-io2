(define-module (crates-io pc d- pcd-rs-derive) #:use-module (crates-io))

(define-public crate-pcd-rs-derive-0.1.0 (c (n "pcd-rs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "112q3qdilvi73pf8s9267rcbrkv6rdy2a6lkpp9khc6h5gj08v0k")))

(define-public crate-pcd-rs-derive-0.1.1 (c (n "pcd-rs-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06ww7zy8764ll2h3lp0h1dawiqwm3yr12j943inlq4zhxs8x3pk0")))

(define-public crate-pcd-rs-derive-0.2.0 (c (n "pcd-rs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nxarvjhi3bw52d9k33a7c4w7z0fzp1h3m7qjcl2smzn28r1r0j1")))

(define-public crate-pcd-rs-derive-0.3.0 (c (n "pcd-rs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13h360nq6wbxhrajkl8cwc5rrk7mgrnhl88lhsjl5q5hkra85scx")))

(define-public crate-pcd-rs-derive-0.4.0 (c (n "pcd-rs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mawkr927qfwq6dy6q13ch5zl94b7qxrnnmsp1ccydxi0rlplhwb")))

(define-public crate-pcd-rs-derive-0.5.0 (c (n "pcd-rs-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1sxmhdy92jsb3w7ql1sslmcpq2vcl86knpqffr09s6kkifhw5h8k") (y #t)))

(define-public crate-pcd-rs-derive-0.5.1 (c (n "pcd-rs-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m7h3asq744ysgg5akpdp9bq33y1bm5h2m7977bqx8nkbl3jfl8z")))

(define-public crate-pcd-rs-derive-0.6.0 (c (n "pcd-rs-derive") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "07249vzzkf5vixp49dmjwczslvw80hrka7m9v443lm71b0261qj8") (y #t)))

(define-public crate-pcd-rs-derive-0.6.1 (c (n "pcd-rs-derive") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1s1ffkrihl7p6la00fqf3m4rlw3bmgg7nyii2kxj3psl65sa872c")))

(define-public crate-pcd-rs-derive-0.11.0 (c (n "pcd-rs-derive") (v "0.11.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0iw5b32kphsf9cwivdmk5d5z7qcb8lv19rwzdv9iz53a6ps1iv9v")))

