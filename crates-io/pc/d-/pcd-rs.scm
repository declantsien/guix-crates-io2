(define-module (crates-io pc d- pcd-rs) #:use-module (crates-io))

(define-public crate-pcd-rs-0.1.0 (c (n "pcd-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zd7jgk8w0sz08zkxff1x8b2kp65dwn2k1j2z2yfwz47i3jm2i53")))

(define-public crate-pcd-rs-0.1.1 (c (n "pcd-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pgqagq5xbjq2a2hyk34988z4c7lbw003xpvzq475dis509xqxch")))

(define-public crate-pcd-rs-0.1.2 (c (n "pcd-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bq1r59h1p3k8faa0nnxdm19yp822x6dxiw27s06y5dy643wzw57")))

(define-public crate-pcd-rs-0.2.0 (c (n "pcd-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)))) (h "026jjm9cwjcy4xms0bds2d26kn0lhr6spws34b29zhzpf8m2c1bn")))

(define-public crate-pcd-rs-0.3.0 (c (n "pcd-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "~0") (d #t) (k 0)))) (h "06j2zvjyr5pb5py1z3yyr2fy232hsfr9zvdr17gfr14ya6lnl4jw")))

(define-public crate-pcd-rs-0.4.0 (c (n "pcd-rs") (v "0.4.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "~0") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)))) (h "1ag0mrhhcc655fqcwp48xadqcbqvvkvwir96s7zm3zz0vgqv613w")))

(define-public crate-pcd-rs-0.4.1 (c (n "pcd-rs") (v "0.4.1") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "~0") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)))) (h "0xmb4ih5z1sq7j2nfrii871xzcc1smrgvm7z0lb4d9b0hfsjb18d")))

(define-public crate-pcd-rs-0.5.0 (c (n "pcd-rs") (v "0.5.0") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "~0") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)))) (h "1k3shaqh67gxgzpnqm29a34gblcxanfhc4vvzlz309r5fhvc080q")))

(define-public crate-pcd-rs-0.5.1 (c (n "pcd-rs") (v "0.5.1") (d (list (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "failure") (r "~0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)))) (h "12miqkjm8pdr0l8lw9zxprsb2zic87ljm5ah8zj73a560kl5r09m")))

(define-public crate-pcd-rs-0.5.2 (c (n "pcd-rs") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0l9mvfvggb2kl7xj45zsg987rj4b6skl8bdi6zmr43xr7qy4g4r7")))

(define-public crate-pcd-rs-0.6.0 (c (n "pcd-rs") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)))) (h "1vpyxsglwhwd0wrkywvbsx9h3ijx873n7zsviakskiq7q0dgb06i")))

(define-public crate-pcd-rs-0.6.1 (c (n "pcd-rs") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)))) (h "0kp6g91jgw5wznczcgwh1nmhzbp17xjmvw3slr4p68gm1vvyq7as")))

(define-public crate-pcd-rs-0.6.2 (c (n "pcd-rs") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.0") (d #t) (k 0)))) (h "19gd4i7g8vi0zdhfngg1cbkxc9yz864w5x3b2di8l2d5cxcjwl41")))

(define-public crate-pcd-rs-0.7.0 (c (n "pcd-rs") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v4xvw2raq1nqy7b944pgxhvk0nvck78ybv3askzabrj1403qlm0") (f (quote (("derive" "pcd-rs-derive"))))))

(define-public crate-pcd-rs-0.8.0 (c (n "pcd-rs") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0gk5avrp3kc5cfn1633w28sy58fibzsapj878fkrz39grqyz8ffl")))

(define-public crate-pcd-rs-0.9.0 (c (n "pcd-rs") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17gjydlgqynqh3naipzzkkyl38rllixarb0gks6rma42nyv3zsnj") (f (quote (("derive" "pcd-rs-derive"))))))

(define-public crate-pcd-rs-0.9.1 (c (n "pcd-rs") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05zqyb6nxvzz04i410lic7b931jx1c6nhsmaxhgf3zrwf44jmv83") (f (quote (("derive" "pcd-rs-derive"))))))

(define-public crate-pcd-rs-0.10.0 (c (n "pcd-rs") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qdili2s81byjb4llxvm3w55dsil16vkydfi2gzl5z2fnw5a0afr") (f (quote (("derive" "pcd-rs-derive"))))))

(define-public crate-pcd-rs-0.11.0 (c (n "pcd-rs") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pcd-rs-derive") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0xi61xjpda10mzl43i996cdlxd3jrggvpa8yi4hi38j2qn1v783v") (f (quote (("derive" "pcd-rs-derive"))))))

