(define-module (crates-io pc t- pct-str) #:use-module (crates-io))

(define-public crate-pct-str-1.0.0 (c (n "pct-str") (v "1.0.0") (h "12x296xynb5xw638qlvm9al3c4xqf39kacs7v1mf80l8nga0jxih")))

(define-public crate-pct-str-1.0.1 (c (n "pct-str") (v "1.0.1") (h "17rsvv0vj6xagmc32vp8yd18xly1jdrg0k4cvg4qg0d4n8n4g8g7")))

(define-public crate-pct-str-1.0.2 (c (n "pct-str") (v "1.0.2") (h "1scy2kapkw6cc7sni1ba43fa5dwy2iihdl0vri39q6mqi0lfb31d")))

(define-public crate-pct-str-1.1.0 (c (n "pct-str") (v "1.1.0") (h "1sgj3xvxlsdqjf722glpw4m0rcykclsrwgpwl05ndrfxpzky6h0j")))

(define-public crate-pct-str-1.2.0 (c (n "pct-str") (v "1.2.0") (d (list (d (n "utf8-decode") (r "^1") (d #t) (k 0)))) (h "00v07yid95fdgj118glvx0cxyw3pa2wvja7h8ppjyb0qipn0glkp")))

(define-public crate-pct-str-2.0.0 (c (n "pct-str") (v "2.0.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "utf8-decode") (r "^1.0.1") (d #t) (k 0)))) (h "1zyn3yhs40av8dmpsg67c0gsy2xm03x0v1k0xl5sb1f2jb2dq6xz")))

