(define-module (crates-io pc ap pcap-sys) #:use-module (crates-io))

(define-public crate-pcap-sys-0.1.0 (c (n "pcap-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1692dhfa595i847kwj6scsj98mf0jjkxh3yyzasdlx0k01jhn59q")))

(define-public crate-pcap-sys-0.1.1 (c (n "pcap-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03j7wafwb0garbsq626z3iifacwgmysbhkg0bigfps2f14347i5l")))

(define-public crate-pcap-sys-0.1.2 (c (n "pcap-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b1qjf9939m26g54b7lkvk0vmjrb0889idvf35jpxcvvzcg47axp")))

(define-public crate-pcap-sys-0.1.3 (c (n "pcap-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18w9c8i5zjnazay86219w4789482ya617z35cw433h12wbgjaiqi")))

