(define-module (crates-io pc ap pcap-rs) #:use-module (crates-io))

(define-public crate-pcap-rs-1.0.1 (c (n "pcap-rs") (v "1.0.1") (d (list (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "1lwgalni8y9nmxh92h9i2aw19h486y67jwgq3hyy32975ic252h4")))

(define-public crate-pcap-rs-1.0.2 (c (n "pcap-rs") (v "1.0.2") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1j7lzd4hf6x21sa425mjs6p84hnx0l90hy77lrqn1hlcqfz80j2z")))

(define-public crate-pcap-rs-1.0.3 (c (n "pcap-rs") (v "1.0.3") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1xad4lgi79rhl16icsxrpv2jc5bpq3d0kqfh1xgnh2q9s44cn4md")))

(define-public crate-pcap-rs-1.0.4 (c (n "pcap-rs") (v "1.0.4") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1yz7vk4ll7qnm4n2ivf4k4kczs252mk3xnc17i97clj426qq5y1k")))

