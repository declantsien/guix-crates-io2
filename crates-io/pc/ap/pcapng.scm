(define-module (crates-io pc ap pcapng) #:use-module (crates-io))

(define-public crate-pcapng-0.0.0 (c (n "pcapng") (v "0.0.0") (d (list (d (n "nom") (r "*") (d #t) (k 0)))) (h "1fpsslfrxjz37pcvw2d31rknmri8hr6r5hqxcagijb882d4ajr7i")))

(define-public crate-pcapng-0.0.1 (c (n "pcapng") (v "0.0.1") (d (list (d (n "nom") (r "*") (d #t) (k 0)))) (h "0m3xxxf8nh1kisri7fgvd37dlgbppbnyahj4fwqriy4xyir0idbf")))

(define-public crate-pcapng-0.0.2 (c (n "pcapng") (v "0.0.2") (d (list (d (n "nom") (r "~0.3.6") (d #t) (k 0)))) (h "1hczyf8g4h7bif836a9d202v1jq4w35drpr58s7shp0568qz69cc")))

(define-public crate-pcapng-0.0.3 (c (n "pcapng") (v "0.0.3") (d (list (d (n "nom") (r "~1.0.0") (d #t) (k 0)))) (h "0w99ql6326s8c74nb3nrlgkrf19z4a3xaj772qc5c1b2c80ggvfc")))

(define-public crate-pcapng-0.0.4 (c (n "pcapng") (v "0.0.4") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "1hzn88fznz63psj8i7fvlk01m25qv5asvzxvffdxnci9cy2ldcq8")))

(define-public crate-pcapng-1.0.0 (c (n "pcapng") (v "1.0.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1ivawawr2134lvbh1x0kxvlk4zzz2d88p2fyj80qgfj2vb4nwdaa")))

