(define-module (crates-io pc ap pcap-config) #:use-module (crates-io))

(define-public crate-pcap-config-0.1.0 (c (n "pcap-config") (v "0.1.0") (h "1vka5m7r1cnn1gy62v3pbbssd0j7b71qhn54lgsm73lbp7vjn7m9")))

(define-public crate-pcap-config-0.1.1 (c (n "pcap-config") (v "0.1.1") (h "1d297xmaah0hrghi237il27j13m480zdrbh1lg7p0m22qwbmsjyv")))

