(define-module (crates-io pc ap pcapplusplus-sys) #:use-module (crates-io))

(define-public crate-pcapplusplus-sys-0.1.0 (c (n "pcapplusplus-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0dsn5ygng2pzf3wms0z5q9jxj9390cbqfhdwccz6j4vwfczr47pv") (l "PcapPlusPlus")))

