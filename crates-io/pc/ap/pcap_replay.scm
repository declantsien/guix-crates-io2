(define-module (crates-io pc ap pcap_replay) #:use-module (crates-io))

(define-public crate-pcap_replay-0.1.0 (c (n "pcap_replay") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pcap-parser") (r "^0.13.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "iphlpapi" "iptypes" "ntdef" "ws2def"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winsockraw-sys") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "070664pd73lwfn8672pq7fkpffhnlgxq09k62pb17yn0xyrcyi34")))

