(define-module (crates-io pc ap pcap-file) #:use-module (crates-io))

(define-public crate-pcap-file-0.5.0 (c (n "pcap-file") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "06b1b7y2yxl1pf7x5pgs7hcf72620v1db8cz6844y6xr1cdzqs7q")))

(define-public crate-pcap-file-0.5.1 (c (n "pcap-file") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "161pvvdvjlq26d55hhkfvwx87izm3a0v97g54159np7khg0cb4dr")))

(define-public crate-pcap-file-0.6.0 (c (n "pcap-file") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1xzqiqfyws0dwlrs7i1cdzwd5wv17v9zjdki9j1n3bfj6jdnz402")))

(define-public crate-pcap-file-0.6.1 (c (n "pcap-file") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "04wk4ckngyf3z2y3ajc8sqsjhkd3m62172m9sxmk2v0dsxj35rrs")))

(define-public crate-pcap-file-0.7.0 (c (n "pcap-file") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "19k0m439s0iixl7a0k9zk3dqq30n53jj3qnq4l4acs6545crqrp3")))

(define-public crate-pcap-file-0.8.0 (c (n "pcap-file") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1v5sjnplznbps3djlq6b5gkx0568jjz3mm013znfl8lk3bnqhn39")))

(define-public crate-pcap-file-0.9.0 (c (n "pcap-file") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "16pla2gh4sbfd1ihx8k0v6zyhaxvg9kfnjnfhvhvf88s1i2xasy7")))

(define-public crate-pcap-file-0.10.0 (c (n "pcap-file") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "0izxf25f3i8xx93n0fdi2nl4af4sh9hd6fyyr4dbmqzsk6c74nxd")))

(define-public crate-pcap-file-1.0.0 (c (n "pcap-file") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qqsvpg3782y4cywxklxcy6laqyj8kls7d6p1q2djy23pk19n3a4") (y #t)))

(define-public crate-pcap-file-1.0.1 (c (n "pcap-file") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fh4zic9cdnhpbkj7ccyjqhiksgz25d3vs7i1m73nbxqfixdj1ax") (y #t)))

(define-public crate-pcap-file-1.1.0 (c (n "pcap-file") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "derive-into-owned") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16n8hswmbwd7raind1a9n9w8x114bkv58zc53cc3a19c78bh0c36")))

(define-public crate-pcap-file-1.1.1 (c (n "pcap-file") (v "1.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "derive-into-owned") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hd382xfr41prh86plbx2lypa7gjfi82c6x8mrch24l33bnkzlba")))

(define-public crate-pcap-file-2.0.0-rc1 (c (n "pcap-file") (v "2.0.0-rc1") (d (list (d (n "byteorder_slice") (r "^3.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derive-into-owned") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "080li0wb4jbab7cfv4m0rdnr5h64x28kwxhycfng6cvx20ar7rq3")))

(define-public crate-pcap-file-2.0.0 (c (n "pcap-file") (v "2.0.0") (d (list (d (n "byteorder_slice") (r "^3.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derive-into-owned") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1cni4pljw1hbk160sk5al2d2rlcr2x84hv5p6ygqy1bvflwz3h8z")))

(define-public crate-pcap-file-3.0.0-rc1 (c (n "pcap-file") (v "3.0.0-rc1") (d (list (d (n "byteorder_slice") (r "^3.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derive-into-owned") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0i1g163vj23rjwb2216afcc06fyv3fp867fqfsld8z8my17y8i6w")))

