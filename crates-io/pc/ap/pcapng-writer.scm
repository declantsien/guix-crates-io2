(define-module (crates-io pc ap pcapng-writer) #:use-module (crates-io))

(define-public crate-pcapng-writer-0.1.0 (c (n "pcapng-writer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 2)) (d (n "pcapng") (r "^1.0.0") (d #t) (k 2)))) (h "0c3j6z03qzdzyd19rbvglwpj16nr12mlmyvwayj6wpz5qagjcj7r")))

