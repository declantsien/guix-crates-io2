(define-module (crates-io pc ap pcap-file-ra) #:use-module (crates-io))

(define-public crate-pcap-file-ra-0.1.1 (c (n "pcap-file-ra") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "pcap-file") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)))) (h "15l2pnc0527csjj1i5k9b4ii8gkn3fanmnqmbjfp974mc6q263w0")))

