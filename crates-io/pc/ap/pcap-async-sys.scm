(define-module (crates-io pc ap pcap-async-sys) #:use-module (crates-io))

(define-public crate-pcap-async-sys-0.1.0 (c (n "pcap-async-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 1)) (d (n "locate-header") (r "^0.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 1)))) (h "1fcad1dyyadgr6kkcl5rnzlndlmnv8ic6jqwjnhdmjdk8gzxwax9")))

