(define-module (crates-io pc g3 pcg32) #:use-module (crates-io))

(define-public crate-pcg32-0.1.0 (c (n "pcg32") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6.4") (k 2)) (d (n "rand_pcg") (r "^0.3.1") (k 2)))) (h "007bkshr6v28hjf5cbsq0jgs3plcxi2pbxffgq3l0flmx9bgai1v")))

