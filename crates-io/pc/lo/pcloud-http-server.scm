(define-module (crates-io pc lo pcloud-http-server) #:use-module (crates-io))

(define-public crate-pcloud-http-server-0.2.2 (c (n "pcloud-http-server") (v "0.2.2") (d (list (d (n "actix-web") (r "^4.1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3") (f (quote ("bibytes"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pcloud") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "stream"))) (k 0)))) (h "1y3bp3lpp6v69mglda7mx26cdld1b2bawjmyy3y9syk2mkh3gp17")))

