(define-module (crates-io pc f8 pcf8563) #:use-module (crates-io))

(define-public crate-pcf8563-0.1.0 (c (n "pcf8563") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)))) (h "1an7k5pr83sy28bxhrp12gcsk774mhncx7w6m3jjnf2pr6l0v5vf")))

(define-public crate-pcf8563-0.1.1 (c (n "pcf8563") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)))) (h "04m7zmbqbpm4wihpfpyzapqf8fp34v07lj3v2mwf4ydr03la2mb1") (y #t)))

(define-public crate-pcf8563-0.1.2 (c (n "pcf8563") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)))) (h "1xip0krcq35nlz4pfrz71van4n6knkgn9palvapsp2mqlwhvlkj1")))

