(define-module (crates-io pc f8 pcf8591) #:use-module (crates-io))

(define-public crate-pcf8591-0.1.0 (c (n "pcf8591") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "1b12n978h3y3pvv057c6wpp7pvmq63npvd95qd938ypzsf0g23l3")))

(define-public crate-pcf8591-0.1.1 (c (n "pcf8591") (v "0.1.1") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "0910a3hhas642n4njn8f3375cyqzgxndm0hs3rg1yv9cqw5k3d98")))

