(define-module (crates-io pc f8 pcf85063a) #:use-module (crates-io))

(define-public crate-pcf85063a-0.1.0 (c (n "pcf85063a") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (k 0)))) (h "189jkk5929h1hipf4zk8w76sj32q7nbm04x5wh9zf4pvxwwxdxi2") (s 2) (e (quote (("defmt" "dep:defmt"))))))

