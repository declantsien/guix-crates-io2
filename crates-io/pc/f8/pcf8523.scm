(define-module (crates-io pc f8 pcf8523) #:use-module (crates-io))

(define-public crate-pcf8523-0.1.0 (c (n "pcf8523") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.2") (d #t) (k 0)))) (h "0smb5xj0n8zyajq0akiysgy1rxrmfyq9acbaxf2al3wcivg4i2js")))

