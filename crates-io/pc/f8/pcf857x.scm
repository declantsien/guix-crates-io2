(define-module (crates-io pc f8 pcf857x) #:use-module (crates-io))

(define-public crate-pcf857x-0.1.0 (c (n "pcf857x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0y6037a4algp8lpyx52qb6ivr5wajdwm9c5h5xwf265ryqww24fj") (f (quote (("default"))))))

(define-public crate-pcf857x-0.2.0 (c (n "pcf857x") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1jfl6r2vc2q5f014kzsa49my3z9fdmjh3i0jm5fqnapzfdb4lp9w") (f (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "unproven"))))))

(define-public crate-pcf857x-0.3.0 (c (n "pcf857x") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1wd0m69hwwy4cf2na7l9j9ds11wq9bx6gkkkjgvdwl69b0qklc7y") (f (quote (("unproven" "embedded-hal/unproven") ("default" "unproven"))))))

(define-public crate-pcf857x-0.4.0 (c (n "pcf857x") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0hi02w463378j3viqaylkch9698fd2qafk4j8pkl9s2ma4lnd57b") (f (quote (("unproven" "embedded-hal/unproven") ("default" "unproven"))))))

