(define-module (crates-io pc f8 pcf8591-hal) #:use-module (crates-io))

(define-public crate-pcf8591-hal-0.1.0 (c (n "pcf8591-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "19l6hk06xws5v7j6c5qn4rsidwyxxs5pb0awa0xaw284s0fcfkf0")))

