(define-module (crates-io pc rc pcrc) #:use-module (crates-io))

(define-public crate-pcrc-0.1.0 (c (n "pcrc") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvvpx4zkzlqkyjawj6d6ca0pk9ll861wjv0c81g7w6mjp9b5jpi")))

