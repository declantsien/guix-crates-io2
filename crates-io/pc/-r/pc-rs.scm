(define-module (crates-io pc -r pc-rs) #:use-module (crates-io))

(define-public crate-pc-rs-0.1.0 (c (n "pc-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "13rk0zfc8nqdwblmws6qvr4lxqy1nl6arhsfcixfccjfa6rxirk3")))

(define-public crate-pc-rs-0.2.0 (c (n "pc-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1mhvr6z19108zvllh0hjg2j3h3yml9sq7zz3a5h438rqiwwvjm63")))

(define-public crate-pc-rs-0.2.1 (c (n "pc-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0rl2jzqccz8dyyf2c4ggcs6ign8bgi0fgz0nf4875xsfl0x03kdw")))

(define-public crate-pc-rs-0.2.2 (c (n "pc-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "07yv0kqnsi5hisc6ygdi7a4k1dv91brzl5spabmg84i5pqgfsx16")))

