(define-module (crates-io pc k- pck-tweaker) #:use-module (crates-io))

(define-public crate-pck-tweaker-0.1.0 (c (n "pck-tweaker") (v "0.1.0") (d (list (d (n "binrw") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jfdp2ylrpvgh3l8qgsdmya1809qp308r03m3r2755jzkpv8apdn")))

