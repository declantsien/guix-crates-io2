(define-module (crates-io sz l- szl-simple-xml) #:use-module (crates-io))

(define-public crate-szl-simple-xml-0.1.0 (c (n "szl-simple-xml") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "15xsz1v8mk282qychzv3w2lqvbljg11lhcpir8yhfrwk4rk0j4am") (y #t)))

(define-public crate-szl-simple-xml-0.1.1 (c (n "szl-simple-xml") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0zp907jk4x4n3lyx1mcnnxzdvcvmwll6ip85lg3sq0qgfzwipkh5") (y #t)))

(define-public crate-szl-simple-xml-0.1.2 (c (n "szl-simple-xml") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0lclw26hhiwb8flcqm1znggaflgjb36fc388mn8s7gg4s34jz1gb") (y #t)))

(define-public crate-szl-simple-xml-0.1.3 (c (n "szl-simple-xml") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0i9jjc91nszqhwwg9nalinxyyf4zh3zz158hbkr5pi3qzlbnbx8l")))

