(define-module (crates-io sz of szof) #:use-module (crates-io))

(define-public crate-szof-0.1.0 (c (n "szof") (v "0.1.0") (h "087ahygcnqbf4h8ygavpswn0dws46wh4zs3zzg019ydbrg7zsmi8")))

(define-public crate-szof-0.2.0 (c (n "szof") (v "0.2.0") (h "0m35hgl5mpq8sahb19hikhqpjxll1v894f1gyfcxlasnb0m3srj8")))

