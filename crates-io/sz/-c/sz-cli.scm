(define-module (crates-io sz -c sz-cli) #:use-module (crates-io))

(define-public crate-sz-cli-0.1.0 (c (n "sz-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "tabled") (r "^0.12.0") (d #t) (k 0)))) (h "1dw5jbvhb96f2rl2a6lndlwrnwhg8nvdf67s91p2m02qix91wsv2")))

