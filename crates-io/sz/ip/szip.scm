(define-module (crates-io sz ip szip) #:use-module (crates-io))

(define-public crate-szip-0.1.0 (c (n "szip") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "snap") (r "^0.1") (d #t) (k 0)))) (h "0fr67mfhsjbpa3sjb1sg49ymaj180gab1xv9hrv1daamanvi8jwh")))

(define-public crate-szip-0.1.1 (c (n "szip") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "snap") (r "^0.2") (d #t) (k 0)))) (h "100wsfv7qngfvabzpiy3xrrz8xh95prl14wzn74g6lmjdygygv0i")))

(define-public crate-szip-1.0.0 (c (n "szip") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (k 0)) (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "snap") (r "^1.0.0") (d #t) (k 0)))) (h "02wsf3v4zmqwl63nvprx0w8y40i42iryl64ax81ahvqgkiviry8b")))

