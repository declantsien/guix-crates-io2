(define-module (crates-io sz #{3-}# sz3-sys) #:use-module (crates-io))

(define-public crate-sz3-sys-0.1.0+SZ3-3.1.8 (c (n "sz3-sys") (v "0.1.0+SZ3-3.1.8") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.3") (o #t) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.10") (k 0)))) (h "0a1gg6k2m39h71572xrahpykgai7gakbajjf635pj5mx17v8c9z6") (s 2) (e (quote (("openmp" "dep:openmp-sys"))))))

(define-public crate-sz3-sys-0.1.1+SZ3-3.1.8.1 (c (n "sz3-sys") (v "0.1.1+SZ3-3.1.8.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.3") (o #t) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.10") (k 0)))) (h "0r50bzys51rmq6s7phdmh641pnl1kj799fr7hza792z72lz6riyk") (s 2) (e (quote (("openmp" "dep:openmp-sys"))))))

