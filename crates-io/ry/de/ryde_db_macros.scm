(define-module (crates-io ry de ryde_db_macros) #:use-module (crates-io))

(define-public crate-ryde_db_macros-0.1.0 (c (n "ryde_db_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05y6whxzyxjyqnbpxj6va4bw39dxr0qkm02sax19ld42r1lr4f1l")))

(define-public crate-ryde_db_macros-0.1.1 (c (n "ryde_db_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xn73czvsq5frxyjfs70yfyvwyz5vkc2rrw73y9wa67r0i5l1kmy")))

(define-public crate-ryde_db_macros-0.1.2 (c (n "ryde_db_macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j7l9xr35ppmzjdns88s7hkn0d3160x0dhcp7jyl5c1sss1367sx")))

(define-public crate-ryde_db_macros-0.1.3 (c (n "ryde_db_macros") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hf9dcszcmbvsfb75hmqcsnb9zzdsmb4vc3w7n26gs8m3n9h19ig")))

