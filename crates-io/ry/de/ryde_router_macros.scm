(define-module (crates-io ry de ryde_router_macros) #:use-module (crates-io))

(define-public crate-ryde_router_macros-0.1.0 (c (n "ryde_router_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0085cxnpyb2ls40zwbdz6r95s0ydl0fjywp2d809f28brjlwbiim")))

(define-public crate-ryde_router_macros-0.1.1 (c (n "ryde_router_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04rh9n4d0x29hg7bcblwncf5mnc7v0mq3in2qsphhsk4gqzl6b6s")))

(define-public crate-ryde_router_macros-0.1.2 (c (n "ryde_router_macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0x9ix68k0rwcj157ymzs2gnxr7iw0s9jrwy01wp2h2a063ad81fy")))

