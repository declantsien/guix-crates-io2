(define-module (crates-io ry de ryde_css_macros) #:use-module (crates-io))

(define-public crate-ryde_css_macros-0.1.0 (c (n "ryde_css_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r3yyas4cv7waaifvpadha70zh8xvpg8vvw7cm571hj8zlgz6bfv")))

(define-public crate-ryde_css_macros-0.1.1 (c (n "ryde_css_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0id4dfjh3dh0db6hb8hxbnpy7h38x506wlk0fbz4lwxl74k9cw9m")))

