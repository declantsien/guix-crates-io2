(define-module (crates-io ry de ryde_macros) #:use-module (crates-io))

(define-public crate-ryde_macros-0.1.0 (c (n "ryde_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rstml") (r "^0.11") (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0qjknmvw9vbqi1v7nv2pf43h10klrd1nc2j5ygkvspsc94mis7fc")))

