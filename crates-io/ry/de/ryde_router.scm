(define-module (crates-io ry de ryde_router) #:use-module (crates-io))

(define-public crate-ryde_router-0.1.0 (c (n "ryde_router") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "ryde_html") (r "^0.2.0") (d #t) (k 2)) (d (n "ryde_router_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive" "derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "1jfs05vd2yigx7l7fjcivsy9z8k61vzadcj2yc7n2ddzdp4i6v9l")))

(define-public crate-ryde_router-0.1.1 (c (n "ryde_router") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "ryde_html") (r "^0.2.0") (d #t) (k 2)) (d (n "ryde_router_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive" "derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 2)))) (h "0nag639gq8lmn8bi6l4xf4m7d2bd3dwaw3x6iq4vlz198hbsdqgd")))

