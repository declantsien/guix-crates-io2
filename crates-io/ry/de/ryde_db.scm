(define-module (crates-io ry de ryde_db) #:use-module (crates-io))

(define-public crate-ryde_db-0.1.0 (c (n "ryde_db") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "ryde_db_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-rusqlite") (r "^0.5.1") (d #t) (k 0)))) (h "0q4wflah0brkjap7ahilhbkc6pyd38rikb97jazl51x8iadf0lyn")))

(define-public crate-ryde_db-0.1.1 (c (n "ryde_db") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.31.0") (d #t) (k 0)) (d (n "ryde_db_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-rusqlite") (r "^0.5.1") (d #t) (k 0)))) (h "0waq3qwkqhkinkma966c4y4c97bdvakx5ga1jgaisw1g52iywcds")))

