(define-module (crates-io ry de ryde_html) #:use-module (crates-io))

(define-public crate-ryde_html-0.2.0 (c (n "ryde_html") (v "0.2.0") (d (list (d (n "axum-core") (r "^0.4.3") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1iqhx3qpd0bmd4szhrgz7ky6g3s6s6h7m175xfzsx8byv0j4rbrf")))

(define-public crate-ryde_html-0.2.1 (c (n "ryde_html") (v "0.2.1") (d (list (d (n "axum-core") (r "^0.4.3") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "0scmlbplz4wsh34dkb0vfgpgcf9r5majfn4s0v78fzpkh5s3cphf")))

