(define-module (crates-io ry pp rypper) #:use-module (crates-io))

(define-public crate-rypper-0.1.0 (c (n "rypper") (v "0.1.0") (h "1pm1wjbrgd6naji2zvz2qc9xld0df5pans21n5ivnq7kmi27r1kx") (y #t)))

(define-public crate-rypper-0.1.0-alpha.0 (c (n "rypper") (v "0.1.0-alpha.0") (h "1w2df9f3a499iiqp50b7n36xi4lbywfdhvxf2p4qqmbal841kpq7") (y #t)))

(define-public crate-rypper-0.1.0-alpha3 (c (n "rypper") (v "0.1.0-alpha3") (h "0bmg52zgdnsb50m2ly8y9wni4zbvgqmzl6rvrbmygz8yxlsahsci") (y #t)))

(define-public crate-rypper-0.1.0-alpha.4 (c (n "rypper") (v "0.1.0-alpha.4") (h "1bgimvzp7ff88yyrnn1cmscp4jr1mfxgnp3fzjml8gxl9r8wjrhj") (y #t)))

