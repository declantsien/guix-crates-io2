(define-module (crates-io ry e- rye-grain) #:use-module (crates-io))

(define-public crate-rye-grain-0.0.1 (c (n "rye-grain") (v "0.0.1") (d (list (d (n "cargo-make") (r "^0.36.3") (d #t) (k 2)) (d (n "grcov") (r "^0.8.13") (d #t) (k 2)) (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1wvprjs2mfjfvrjbglardb1ln1xd42m26vr2zadkwcq3zq1qj065")))

