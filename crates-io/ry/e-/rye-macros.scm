(define-module (crates-io ry e- rye-macros) #:use-module (crates-io))

(define-public crate-rye-macros-0.0.1 (c (n "rye-macros") (v "0.0.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gc30z606mypp2n2hrr47rv7n9qp0wzxkzay8bicpx1lb4bb8b32")))

