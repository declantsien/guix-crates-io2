(define-module (crates-io ry g- ryg-rans-sys) #:use-module (crates-io))

(define-public crate-ryg-rans-sys-1.0.0 (c (n "ryg-rans-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1x5d6w345kghim3nfnx808nhim2aa1kfnyhbzm0lvhvfkbfbcb0f") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (y #t) (l "ryg_rans")))

(define-public crate-ryg-rans-sys-1.0.1 (c (n "ryg-rans-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1gij4ilw310dfjyc37jazbnqj131ix5gwl42pzj8cncbxd9fkr2y") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans")))

(define-public crate-ryg-rans-sys-1.0.2 (c (n "ryg-rans-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1grwklx0qpphmwis43gxprdgccdn0wkfxsl6iwppbqxjahid84s1") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans")))

(define-public crate-ryg-rans-sys-1.0.3 (c (n "ryg-rans-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1hi2xbr949jb2kqpms69crad0gllzikxcdscs3bvz2yrg1wysql1") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans")))

(define-public crate-ryg-rans-sys-1.0.4 (c (n "ryg-rans-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "14m115v6wxp11fqdcjajmacfhjcazlbwqcqc5bfyvl9qrm24vw14") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans")))

(define-public crate-ryg-rans-sys-1.0.5 (c (n "ryg-rans-sys") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0w2pwwcyhf4090b5v3y4yrvf8jnl3aii5hdwsdxqfny6x0x8hpg8") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans") (r "1.56.1")))

(define-public crate-ryg-rans-sys-1.0.6 (c (n "ryg-rans-sys") (v "1.0.6") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0cq86biczc92crmv1342pwryr3c67qz9lixz1plfl1xiywmqaphp") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans") (r "1.56.1")))

(define-public crate-ryg-rans-sys-1.0.7 (c (n "ryg-rans-sys") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1ygq9bgj3ik735g19a3sryh59cnihmmczxglw1scngp39rr32l9a") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans") (r "1.56.1")))

(define-public crate-ryg-rans-sys-1.1.0 (c (n "ryg-rans-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1dgd2x6rbq8h0hz24n5fhrrbkqpx6vp8plakqs1fpqzqcrnb7lv2") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans") (r "1.60.0")))

(define-public crate-ryg-rans-sys-1.2.0 (c (n "ryg-rans-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1xhsprrg43vcnylv4jjnz2nlz1fqnm56rv6ay93yf4hfkpgfvz81") (f (quote (("word_sse41") ("default" "byte") ("byte") ("64")))) (l "ryg_rans") (r "1.71.0")))

