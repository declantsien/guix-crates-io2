(define-module (crates-io ry nc ryncatsay) #:use-module (crates-io))

(define-public crate-ryncatsay-0.1.0 (c (n "ryncatsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^2.0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "06h1ss76j05wci08lb9x00za2jymv2dii3v1prdxs0crpd6xa2jq")))

(define-public crate-ryncatsay-1.0.0 (c (n "ryncatsay") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^2.0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1z3xm9gxj1f4qrdngnbg1b6f18d0xj9h76n6ppv0krxrddcqbyym")))

