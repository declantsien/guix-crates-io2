(define-module (crates-io ry so ryson) #:use-module (crates-io))

(define-public crate-ryson-0.1.0 (c (n "ryson") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "str-macro") (r "^1.0.0") (d #t) (k 0)))) (h "010xaxbzcq7igmk7g677rbdxz1zmybzw0xmvf67bydn1h40synff")))

