(define-module (crates-io ry u- ryu-js) #:use-module (crates-io))

(define-public crate-ryu-js-0.1.0 (c (n "ryu-js") (v "0.1.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0hjcchigmp4nkg678c0j2vc0l242vs4liq4fz41x8pf3kifcclra") (f (quote (("small"))))))

(define-public crate-ryu-js-0.2.0 (c (n "ryu-js") (v "0.2.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1sa9h443bnqvlnxzpjzggddg38kqbvx947v31l9nnmb83yfbn40h") (f (quote (("small"))))))

(define-public crate-ryu-js-0.2.1 (c (n "ryu-js") (v "0.2.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0hkgv4l2apjdyzvddzgl08dbx39w393mcn2ga9293cl12qc68s72") (f (quote (("small"))))))

(define-public crate-ryu-js-0.2.2 (c (n "ryu-js") (v "0.2.2") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0zqf0ypy6dl26bz4aigqvrvn6wfqx4iy9mi2i9km6kgdphkgq635") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-js-1.0.0 (c (n "ryu-js") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0axi48ycvk791qksfcnyaiwly35xj53prja4451zh594qmdxhl29") (f (quote (("small")))) (r "1.64")))

(define-public crate-ryu-js-1.0.1 (c (n "ryu-js") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1ma4kshqc5353hk23609bc1sc6hd6349slc9xiyf59b02p7d95xd") (f (quote (("small")))) (r "1.64")))

