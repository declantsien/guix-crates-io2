(define-module (crates-io ry u- ryu-ecmascript) #:use-module (crates-io))

(define-public crate-ryu-ecmascript-0.1.0 (c (n "ryu-ecmascript") (v "0.1.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "19xz7kr5mywd0qxf0pahsiljffw54ba1ncpm6rs8ahzid93is6qm") (f (quote (("small"))))))

(define-public crate-ryu-ecmascript-0.1.1 (c (n "ryu-ecmascript") (v "0.1.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0p2py0m62djz2yzm089ypvwwqa8m3fl5rni2jigmfqlsxbw9xwbr") (f (quote (("small"))))))

