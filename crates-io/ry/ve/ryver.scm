(define-module (crates-io ry ve ryver) #:use-module (crates-io))

(define-public crate-ryver-0.1.0 (c (n "ryver") (v "0.1.0") (d (list (d (n "calamine") (r "^0.24.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0iyrwx33sgcxbh8n05ksp82ls8p6zh387gd2djy4kp53cmsqgp1q")))

(define-public crate-ryver-0.1.1 (c (n "ryver") (v "0.1.1") (d (list (d (n "calamine") (r "^0.24.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0mkxssga7c61gvgy5y2m9bvskfdxlmzpk243hch1ch5y9dhh154p")))

(define-public crate-ryver-0.1.2 (c (n "ryver") (v "0.1.2") (d (list (d (n "calamine") (r "^0.24.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "wildmatch") (r "^2.3.3") (d #t) (k 0)))) (h "09djymj24avzpx13aqm5gxfa7a5jkknma9dkks2v6bzig7hsnhvf")))

