(define-module (crates-io ry tm rytm-rs-macro) #:use-module (crates-io))

(define-public crate-rytm-rs-macro-0.1.0 (c (n "rytm-rs-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "01klyxqqv6kd4qg7294sjdx5d2sz598iwk0idgpxvkb6zybbcrqj")))

(define-public crate-rytm-rs-macro-0.1.1 (c (n "rytm-rs-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "07l94yibk1xkm502n7g7mn3ynrzvhv63zwa0v4v6k2s3hm36ds2f")))

(define-public crate-rytm-rs-macro-0.1.2 (c (n "rytm-rs-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "1na1qzy78kaznv17zami6jhi71dkf8jldw36cdn9h8bqndiwakpr")))

