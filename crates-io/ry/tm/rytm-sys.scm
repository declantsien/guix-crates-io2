(define-module (crates-io ry tm rytm-sys) #:use-module (crates-io))

(define-public crate-rytm-sys-0.1.0 (c (n "rytm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1ymx6g7giwcjcf86881nvnap7kl7p1gyxc6c361b1fh947hgqdqc") (y #t)))

(define-public crate-rytm-sys-0.1.1 (c (n "rytm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0msn2xs9a8dfgmjmgv07mg08skx3y0m1ksy7l0kxhfjnh9rn3m59") (y #t)))

(define-public crate-rytm-sys-0.1.2 (c (n "rytm-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "01y9zl3vd05g0cipa3a0z3g47rfn7fphlspasvni48r7rzjqxasa") (y #t)))

(define-public crate-rytm-sys-0.1.3 (c (n "rytm-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1s4ibmi9z7wzkfmmb6szr5z1f367646a2x0y58k4av2b75pg85dc") (y #t)))

(define-public crate-rytm-sys-0.1.4 (c (n "rytm-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1rl7zs00aq0jhwlkw07z1rgi6lblg2ynwck2ac9dzs57jddlm6a0")))

(define-public crate-rytm-sys-0.1.5 (c (n "rytm-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1136cqljzi6jn8v7rxis7i8l2zq0q49fvfyfg8f8i5q7g6pipljb")))

