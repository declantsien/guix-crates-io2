(define-module (crates-io ry ot ryot_derive) #:use-module (crates-io))

(define-public crate-ryot_derive-0.2.2 (c (n "ryot_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22") (f (quote ("parse"))) (d #t) (k 0)))) (h "13h24jiggfnd22kx4bw8qx79bm2kzjnlv7ln8la7rb7gfiby9a0w")))

