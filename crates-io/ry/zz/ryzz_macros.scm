(define-module (crates-io ry zz ryzz_macros) #:use-module (crates-io))

(define-public crate-ryzz_macros-0.1.0 (c (n "ryzz_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11xqjgnqdhjhngar0gzzgmhlj0shzr7nvffqpnr9ixnyhrdl4ics")))

(define-public crate-ryzz_macros-0.2.0 (c (n "ryzz_macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1si3g8zzppl8vfywgyj61gkkv7rb7lgnz4silqnlyshbq459i70p")))

