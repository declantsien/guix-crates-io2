(define-module (crates-io ry ml ryml) #:use-module (crates-io))

(define-public crate-ryml-0.1.0 (c (n "ryml") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0n3ybxy1hl7mkypkyyg6qmd4vxmayjf9lh3iakxm7nmygs7vkyiv")))

(define-public crate-ryml-0.1.1 (c (n "ryml") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qafdq2k78nwczdgkhf7n79h4c8yghqyki40z233lxdrhv5nb6j8")))

(define-public crate-ryml-0.1.2 (c (n "ryml") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xsba4pj9k2grkyp39f2qir3irv4nc7f59g0bvkbjpvb7dwlxwrv")))

(define-public crate-ryml-0.1.3 (c (n "ryml") (v "0.1.3") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v3q4vi3fkm6a5iihqv3p55gmncaypsgkaq5wzfa5gj0dibvbvnh")))

(define-public crate-ryml-0.1.4 (c (n "ryml") (v "0.1.4") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00v96sfrraglkb8rlv1rp609r1nwkvg79i1dl8js37cpfd5izkp4")))

(define-public crate-ryml-0.1.5 (c (n "ryml") (v "0.1.5") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l2g5lbxza0va4ycplzg73dzsj8sqgbvxch3i4cxfa714zad5hbp")))

(define-public crate-ryml-0.1.6 (c (n "ryml") (v "0.1.6") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hnwhxswcl5y7wwgxzvkhp1j99xm5j2n142jrjnkrlbqls8yjpwh")))

(define-public crate-ryml-0.2.0 (c (n "ryml") (v "0.2.0") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1h6gmisp9jaa97rlfci9hnx5si1f3fpfffnyqdck0ha3i06zj8yn")))

(define-public crate-ryml-0.3.0 (c (n "ryml") (v "0.3.0") (d (list (d (n "acid_io") (r "^0.1.0") (d #t) (k 0)) (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1ins52x0jbcwq7k7yaig14la7qmmwmhr4p7hb3bdffb1cvmavjl1") (f (quote (("std"))))))

(define-public crate-ryml-0.3.1 (c (n "ryml") (v "0.3.1") (d (list (d (n "acid_io") (r "^0.1.0") (d #t) (k 0)) (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0jy3szsg96797pvd4d6lyl55iyaa3g8001nsfyxpw6f96zd7qwhg") (f (quote (("std"))))))

(define-public crate-ryml-0.3.2 (c (n "ryml") (v "0.3.2") (d (list (d (n "acid_io") (r "^0.1.0") (d #t) (k 0)) (d (n "auto-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1aw9qrcns982nyi9y8yia35xmhsb52ghq3shh44rvg74q16n87m3") (f (quote (("std" "thiserror-no-std/std" "acid_io/std"))))))

