(define-module (crates-io ry sk rysk-core) #:use-module (crates-io))

(define-public crate-rysk-core-0.0.1 (c (n "rysk-core") (v "0.0.1") (h "1h3mg95zb1c2nxnisrh7chwmlz0n5z718jqr8ghakljv5li5s9yp") (f (quote (("rv32e") ("ext-q") ("ext-m") ("ext-f") ("ext-d") ("ext-c") ("ext-a") ("default"))))))

(define-public crate-rysk-core-0.0.2 (c (n "rysk-core") (v "0.0.2") (h "0qbnlnswdq2xvmpi05hafnapkmdh4x1wg2sry56y77dgyqigjaz9") (f (quote (("rv32e") ("ext-m") ("ext-csr") ("default" "ext-csr"))))))

(define-public crate-rysk-core-0.0.3 (c (n "rysk-core") (v "0.0.3") (h "1a4h6m78941vh186i6px217lms8k77dy619zlr6ns3jrcngz6jd7") (f (quote (("ext-m") ("ext-csr") ("default" "ext-csr" "ext-m"))))))

