(define-module (crates-io ry #{-i}# ry-interner) #:use-module (crates-io))

(define-public crate-ry-interner-0.1.0 (c (n "ry-interner") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "04pch1h7fyrgxx884imx60w1fl400p3f0wic79ccw4skn986kwhl") (y #t)))

(define-public crate-ry-interner-0.1.1 (c (n "ry-interner") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "0lkx9sxdsn79ivi0mbfhl2vipw0ws377h1rxzshn9i5m2d6z01h1") (y #t)))

