(define-module (crates-io ry an ryan) #:use-module (crates-io))

(define-public crate-ryan-0.1.0 (c (n "ryan") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1djdvby1wy1c4yjrz057psn6lglv7nqi0qyrpia9vkcyjkgklwdi")))

(define-public crate-ryan-0.1.1 (c (n "ryan") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dri4p3ydm6i0jmn2hk6x0qhr7ka0ig2r1hlai6gi4p3p564fh7h")))

(define-public crate-ryan-0.2.0 (c (n "ryan") (v "0.2.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i683gxqawh9kgmq4p9wpqvm4v30sgjs5dp79nl86hdhqs1d48fy")))

(define-public crate-ryan-0.2.1 (c (n "ryan") (v "0.2.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13bxnf6nwnzrc404lx3lj4mh1c99zw0jm0hdi78csdqcsw1k8ky2")))

(define-public crate-ryan-0.2.2 (c (n "ryan") (v "0.2.2") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s28y4k0dwfgfaaqgl0whv2356g06abkf80bw30gdrad71nqa8hj")))

(define-public crate-ryan-0.2.3 (c (n "ryan") (v "0.2.3") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qh8b4253ax0qisrh6kj8cff2dkcq04mbn218mzi47n5dbrlbqwk")))

