(define-module (crates-io ry ze ryzen-reader) #:use-module (crates-io))

(define-public crate-ryzen-reader-0.1.0 (c (n "ryzen-reader") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0prr6mnzf9fdigr6c77gvw8ds52nxiykbr6nf2iz9fnigcgwy1dm")))

(define-public crate-ryzen-reader-0.1.1 (c (n "ryzen-reader") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jyd2q5f6snrp5vcla6pdk6pqvypkg955sf9ybbi9yhcy4x1siw0")))

