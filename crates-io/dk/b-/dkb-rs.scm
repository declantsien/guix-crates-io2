(define-module (crates-io dk b- dkb-rs) #:use-module (crates-io))

(define-public crate-dkb-rs-0.0.1-alpha (c (n "dkb-rs") (v "0.0.1-alpha") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)))) (h "1yirsn5gg073wi2hqj589p24ankj4277sck2m44660p14ivysc3b")))

(define-public crate-dkb-rs-0.0.2 (c (n "dkb-rs") (v "0.0.2") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)))) (h "0x0660c1avsqnk77skw9qvzkr6siw4b3145abm7nc3dfsck1n7hz") (y #t)))

(define-public crate-dkb-rs-0.0.1-alpha.2 (c (n "dkb-rs") (v "0.0.1-alpha.2") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)))) (h "08xb4cjyj06i9nqbawrda59h7nvjn0s208b0lqz8v386nvyf3cb7")))

