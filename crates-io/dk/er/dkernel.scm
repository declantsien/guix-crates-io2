(define-module (crates-io dk er dkernel) #:use-module (crates-io))

(define-public crate-dkernel-0.0.0 (c (n "dkernel") (v "0.0.0") (d (list (d (n "deskc-ast") (r "^0.0.0") (d #t) (k 0) (p "deskc-ast")) (d (n "deskc-hir") (r "^0.0.0") (d #t) (k 0) (p "deskc-hir")) (d (n "deskc-ids") (r "^0.0.0") (d #t) (k 0) (p "deskc-ids")) (d (n "deskc-types") (r "^0.0.0") (d #t) (k 2) (p "deskc-types")) (d (n "dkernel-card") (r "^0.0.0") (d #t) (k 0) (p "dkernel-card")) (d (n "mry") (r "^0.1.8") (d #t) (k 2)) (d (n "salsa") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 2)))) (h "0z70mhl5rvcmb2vx83jnn9yj0qcyii6pxxhlv7lv9yfrdg11v401")))

