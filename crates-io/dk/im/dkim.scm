(define-module (crates-io dk im dkim) #:use-module (crates-io))

(define-public crate-dkim-0.0.0 (c (n "dkim") (v "0.0.0") (h "0qlkbhpsqginnvqp6qh7k5h4yzy91gq0h39ivvqv5mp4gxzwcjqr")))

(define-public crate-dkim-0.1.0 (c (n "dkim") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "email-parser") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "prettydiff") (r "^0.3") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4") (d #t) (k 0)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "string-tools") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1by0v458xpgm6rzmhkzhzgz5iahszkm7lpd876gdxqikqqdwnp1m")))

(define-public crate-dkim-0.1.1 (c (n "dkim") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "email-parser") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "prettydiff") (r "^0.3") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4") (d #t) (k 0)) (d (n "rsa") (r "^0.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "string-tools") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1qycvlp7hmpm35v60xzg5d5g2r05msqkzazgvzmip0j2dhyyqsvr")))

