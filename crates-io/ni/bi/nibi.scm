(define-module (crates-io ni bi nibi) #:use-module (crates-io))

(define-public crate-nibi-0.0.1 (c (n "nibi") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "0ivfrqjsi6p4iawlww5m6lqdpwsngh5gs1br8d67v3inj5q8vd3g")))

(define-public crate-nibi-0.0.2 (c (n "nibi") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)))) (h "1svmddpqg1dr320hfvkm8nwbh26gy02q4nys5r42xf3zhcldrpkk")))

(define-public crate-nibi-0.0.3 (c (n "nibi") (v "0.0.3") (d (list (d (n "combu") (r "^1.1.22") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "0gsx89f7pn36ksa7bdk42ramc38a0k1k5vl97dndk4sj5lycjkgy")))

(define-public crate-nibi-0.0.4 (c (n "nibi") (v "0.0.4") (d (list (d (n "combu") (r "^1.1.23") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "0sz897jkfcylgh69654gmfinvkvqyhhkx2q3sp0a5p7brlfqf3qf")))

(define-public crate-nibi-0.0.5 (c (n "nibi") (v "0.0.5") (d (list (d (n "combu") (r "^1.1.23") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)))) (h "17arwcilnigfr8fipydykg9m0m8bwjxspby8i9agy4765rrmqnk5")))

