(define-module (crates-io ni bi nibiru-std) #:use-module (crates-io))

(define-public crate-nibiru-std-0.0.1 (c (n "nibiru-std") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)))) (h "0ajryylf043aqsczbcvy1v0v8fzqgcqqzhgkbhsq5sn53hd6rdvc") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-nibiru-std-0.0.3 (c (n "nibiru-std") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.1") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1in5vnc68pxb56fvrab8xyhrj0jq1krvc30ayyqlgd0v2304hl5f") (f (quote (("default") ("backtraces" "cosmwasm-std/backtraces"))))))

