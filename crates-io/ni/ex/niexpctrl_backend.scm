(define-module (crates-io ni ex niexpctrl_backend) #:use-module (crates-io))

(define-public crate-niexpctrl_backend-0.1.0 (c (n "niexpctrl_backend") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "nicompiler_backend") (r ">0.0.1") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("multiple-pymethods"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0kmk9d4vj1mhrdngml0iv9id0vpzrvz095xginkzsd9yf5klfcj7")))

