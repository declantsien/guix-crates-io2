(define-module (crates-io ni np ninput) #:use-module (crates-io))

(define-public crate-ninput-0.1.0 (c (n "ninput") (v "0.1.0") (h "1i4qnamfc2ghaxm1ch5r6dxg58kx9nfasfcabbmak04g0p0ydf2f")))

(define-public crate-ninput-0.1.1 (c (n "ninput") (v "0.1.1") (h "0id1a4c5q3y370gkphrix6ikz8ra9zb5748zqahqa2qavcbwdxx8")))

(define-public crate-ninput-0.1.2 (c (n "ninput") (v "0.1.2") (h "0f87v32g0xf2nq95clf9jl36gnsr804bj9bcinyirklnqlhd55ld")))

(define-public crate-ninput-0.1.3 (c (n "ninput") (v "0.1.3") (h "1y27lm6rh5ak7627cf6ys3czsh9bbx7dlglz9k4d21jcj0vbqmfl")))

(define-public crate-ninput-0.2.0 (c (n "ninput") (v "0.2.0") (h "1hyq4qq5nn7dvv2nvrilr3ylazpg876d8hcksw2hpwfxm9xasn94")))

