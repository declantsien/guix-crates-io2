(define-module (crates-io ni fp nifpga-dll-type-macro) #:use-module (crates-io))

(define-public crate-nifpga-dll-type-macro-0.1.6 (c (n "nifpga-dll-type-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqn8qsa7yn7lbi136w89rlzwz6r98sfqdia1yym7yk3xb0dwd7c") (y #t)))

(define-public crate-nifpga-dll-type-macro-0.1.7 (c (n "nifpga-dll-type-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0gm5l6xif2n8xz3h3xq9iffb1ra5bcci6i47yhkncwca68l1as28") (y #t)))

(define-public crate-nifpga-dll-type-macro-0.2.0 (c (n "nifpga-dll-type-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1ds19v1bm9rxjg0vgi93r80n1x1vl8brl7dmwwmbq5d5yl22gkk0") (y #t)))

(define-public crate-nifpga-dll-type-macro-0.3.0 (c (n "nifpga-dll-type-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "10w0m5mm6qh6r36gqry2nvs28alv52svs3n2lmba0ic7l51ky1xh")))

