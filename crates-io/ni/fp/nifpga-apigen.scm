(define-module (crates-io ni fp nifpga-apigen) #:use-module (crates-io))

(define-public crate-nifpga-apigen-0.1.0 (c (n "nifpga-apigen") (v "0.1.0") (h "1qd06q08r9s892z3k9cci8806alrkdhw7g3z15k75v9vs0c004g8")))

(define-public crate-nifpga-apigen-0.1.1 (c (n "nifpga-apigen") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1bxjaq9rsbjw9qfizbjpyabm03aprmpmg2h8ql181c000bj20f2n")))

(define-public crate-nifpga-apigen-0.1.2 (c (n "nifpga-apigen") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "13xw9bqcfxfcj8a8z7vglvyi3hrp5mc9gbxz8jxqznzqhfa4d699")))

(define-public crate-nifpga-apigen-0.1.3 (c (n "nifpga-apigen") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "03ihhw7hqyby82lrrc5ps6ifqb9476fhciyssw1c6khs2vbf27a4")))

(define-public crate-nifpga-apigen-0.1.4 (c (n "nifpga-apigen") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0l6gj8kfkmhs17r6wv65nxrz0si8mml7wj17xzilbj58i738vgml")))

(define-public crate-nifpga-apigen-0.1.5 (c (n "nifpga-apigen") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "01zqhkqjsvd317nx5zr1h0wmyignb9icfcndb4v43ihzni3x42fc")))

