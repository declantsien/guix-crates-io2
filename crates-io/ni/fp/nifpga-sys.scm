(define-module (crates-io ni fp nifpga-sys) #:use-module (crates-io))

(define-public crate-nifpga-sys-0.1.0 (c (n "nifpga-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0vj1y2mgzydvb3vz1glx9sh5iivn0rqmcmans91d721frk7qw4w9")))

(define-public crate-nifpga-sys-0.1.1 (c (n "nifpga-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0rj0ql30jr1n61m05c5qpdprxzhld7j76a8q8g7hakdn9i54qkla") (f (quote (("static") ("default"))))))

