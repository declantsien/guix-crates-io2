(define-module (crates-io ni fp nifpga-dll-sys) #:use-module (crates-io))

(define-public crate-nifpga-dll-sys-0.1.6 (c (n "nifpga-dll-sys") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0dwvk797wzg5m4xcfz40p0ja2i2ygf54c06l2ksgfvv3bdc63af5") (y #t) (l "NiFpga")))

(define-public crate-nifpga-dll-sys-0.1.7 (c (n "nifpga-dll-sys") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "04scwz673rd0pdf3ry55wyyrml3j4lssc5nkarmhzg3zzhz24086") (y #t) (l "NiFpga")))

(define-public crate-nifpga-dll-sys-0.2.0 (c (n "nifpga-dll-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1z6ncdsxrfvyf1xzjl7abknfaiis494mvrv0zza0q0dacrv3azc8") (y #t) (l "NiFpga")))

(define-public crate-nifpga-dll-sys-0.3.0 (c (n "nifpga-dll-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1njcy3w33622rjfklxyh4aypdqgk76jfbh8gk1lhvczpappigkyj") (l "NiFpga")))

