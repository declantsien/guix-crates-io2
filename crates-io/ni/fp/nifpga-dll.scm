(define-module (crates-io ni fp nifpga-dll) #:use-module (crates-io))

(define-public crate-nifpga-dll-0.1.6 (c (n "nifpga-dll") (v "0.1.6") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nifpga-dll-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "nifpga-dll-type-macro") (r "^0.1.6") (d #t) (k 0)))) (h "19siln0k2r91d2gr2pby9wq7k0qrqn54j1ijf59kwzsjabwi044g") (y #t)))

(define-public crate-nifpga-dll-0.1.7 (c (n "nifpga-dll") (v "0.1.7") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nifpga-dll-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "nifpga-dll-type-macro") (r "^0.1.7") (d #t) (k 0)))) (h "1r0hl8swnm1b90iv6ibs09jrk74hcbyw86lkrpvgivjn7mjagq4n") (y #t)))

(define-public crate-nifpga-dll-0.2.0 (c (n "nifpga-dll") (v "0.2.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nifpga-dll-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "nifpga-dll-type-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1rz994857wsyld347s6xc6nglni8iyymhgrlqjf23fmpcwpikqfn") (y #t)))

(define-public crate-nifpga-dll-0.3.0 (c (n "nifpga-dll") (v "0.3.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nifpga-dll-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "nifpga-dll-type-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1nfm6cgydg0fskfw6k99kis69s256myvsj6x1q74pshikbkn5xkb")))

