(define-module (crates-io ni fp nifpga-type-macro) #:use-module (crates-io))

(define-public crate-nifpga-type-macro-0.1.0 (c (n "nifpga-type-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1783vzc2vd9gv9qfafjgg77nx1phr4x0hcb2fnflxwd2pi71hi")))

(define-public crate-nifpga-type-macro-0.1.1 (c (n "nifpga-type-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1nzsxyzvcrnn0pp451zz5p43f354j1camgw4qi36rw3ycp7k3n")))

