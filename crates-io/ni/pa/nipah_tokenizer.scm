(define-module (crates-io ni pa nipah_tokenizer) #:use-module (crates-io))

(define-public crate-nipah_tokenizer-0.1.0 (c (n "nipah_tokenizer") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x8l2mcs96sfcxc25hmaiagwv84m7iapsy7imm9hhm1yjgxq6zhx")))

