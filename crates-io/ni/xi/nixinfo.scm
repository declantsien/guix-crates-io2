(define-module (crates-io ni xi nixinfo) #:use-module (crates-io))

(define-public crate-nixinfo-0.1.0 (c (n "nixinfo") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1ya978qznzaxcd8lz5pj92wf5icf1ixzmr80h4i6g9c8zl75ancd") (f (quote (("nomusic") ("music" "mpd"))))))

(define-public crate-nixinfo-0.1.1 (c (n "nixinfo") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "19wa2w7hdbggr9kwbi7nv09ndbsbx02g9yvhdlw1n9paml91jx68") (f (quote (("nomusic") ("music" "mpd"))))))

(define-public crate-nixinfo-0.1.2 (c (n "nixinfo") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1agxkfd8ya3rq3slci87pwz18wfwj57a1bgidqav6hamvk1wb71l") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.3 (c (n "nixinfo") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0mg6dg0im9dxbj4sf0byg2s1v0f5axawkv0l20ycg769wmppz9nw") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.4 (c (n "nixinfo") (v "0.1.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1hm3k2vvpl8qs2g3zvpivqk4k7c6vp67y82swrkcwfp1ly0qcka0") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.5 (c (n "nixinfo") (v "0.1.5") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0z89rhik0642ilp3bqy8snilf6brmnxvpg9x61arx26wnvy5f2g0") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.6 (c (n "nixinfo") (v "0.1.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "15nffsr0x0k1djs09idcwvpqmcq3yj9y4izkqsdp8a45g4piall9") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.7 (c (n "nixinfo") (v "0.1.7") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "13ngxa58sqmjgvma7nz79biy7ha7jy9885yh9nkjizs8x26yzss0") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.8 (c (n "nixinfo") (v "0.1.8") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1dy520dsndp5xv6i0fggzfj5qxij4larav7dyqvayrkbi05732xy") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1.9 (c (n "nixinfo") (v "0.1.9") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1m9fzix7z1yw09h3wn51l85cgr9m5r3xcn8q050j0ma0laf3a2v4") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.0 (c (n "nixinfo") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "127ykpvdknx1lkszi1ivpc24hh01mj7fgwzyjakx69qdjaz9x3dp") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.1 (c (n "nixinfo") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "068y41d0rv0mq0gdns63xikzr6zgfdd43qfyg0xfh5qdxlca270d") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.2 (c (n "nixinfo") (v "0.2.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0r6yigzqmn198grhrkpp6qpfrbsi4invj0p3bnpry71q9d49cvqx") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.3 (c (n "nixinfo") (v "0.2.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "040cr9g8hdiklnpqad53dg3vzx1bc0fyq7w11nja73is5a6szdra") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.4 (c (n "nixinfo") (v "0.2.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "07z2br7vjayvwphli3qc6y49s80apc2px3il6f9gxn5864b6gkpv") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.5 (c (n "nixinfo") (v "0.2.5") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "04r7r28dgxc1wzdnsi8ka68i5r8ijfbw3csb4pvgdpqlw75nns5b") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.6 (c (n "nixinfo") (v "0.2.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1k8a6isicmr9yjmzlp8z8dyd717v9lp606rvp7372578fvbz46y3") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.7 (c (n "nixinfo") (v "0.2.7") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0vcpwan6psrg1bmndp5c02wx8pwfdkfscrqp9k0s0rwln7i0fwpp") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.8 (c (n "nixinfo") (v "0.2.8") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0zxmr2y43ic9w5rn6aa3kkhbq5ax5c3aszczw6194gnbn4igbp9f") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2.9 (c (n "nixinfo") (v "0.2.9") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0jzx703wrqbvrm59djw0iyzflpnyx58c2ly1dpp2ls50cxp14c0r") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3.0 (c (n "nixinfo") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "0ng4yz8bs2jxbzzknvac4ir8l4wsgkznq4mnq6sqjar3gf0hpc3m") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3.1 (c (n "nixinfo") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "1i7kf381r29qj5xh6a98p6krrh529bv7jgq6vqrk18s5grcdyrjf") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3.2 (c (n "nixinfo") (v "0.3.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "007xmp1wzb43yv93q7lvxc2v99qpw1jghvm2xh08xyzs0nqgyr2w") (f (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3.3 (c (n "nixinfo") (v "0.3.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (o #t) (d #t) (k 0)))) (h "11vrmk9ld0vg6famkzlr443z6y7z5lxxbyc68kwm8gjzxnjj1z3a") (f (quote (("music" "mpd"))))))

