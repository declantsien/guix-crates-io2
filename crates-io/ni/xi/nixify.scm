(define-module (crates-io ni xi nixify) #:use-module (crates-io))

(define-public crate-nixify-0.1.0 (c (n "nixify") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "nixpkgs-fmt") (r "^1.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.8.0") (d #t) (k 0)))) (h "1gz9sqys3fggk10pqg78pdwcjy48q9bn3g61rfr01zbvkhw6f0ml")))

(define-public crate-nixify-0.1.1 (c (n "nixify") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "nixpkgs-fmt") (r "^1.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.8.0") (d #t) (k 0)))) (h "0v5h3gsgz0442786qawwb5qlp79bhw4zs15b1wqyhvw3shd0s18p")))

