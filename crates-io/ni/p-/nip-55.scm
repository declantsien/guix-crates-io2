(define-module (crates-io ni p- nip-55) #:use-module (crates-io))

(define-public crate-nip-55-0.1.0 (c (n "nip-55") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1x12kssxgl134f1551kv0hjj26v2k4glhb14rpp2z54pa179flvp")))

(define-public crate-nip-55-0.1.1 (c (n "nip-55") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0n3ywng8ijs5j1n8r6kbv6m6s2hk6xmbxbx7fyg5n85vv2cpnaaz")))

(define-public crate-nip-55-0.2.0 (c (n "nip-55") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0dak7af83vfpgwlnd7dxlk3vrw45lj0bjy3d56b22ijs430zcdwy")))

(define-public crate-nip-55-0.3.0 (c (n "nip-55") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0ky3p0ga4lpkgznh4sklkrh2k0y31y4q8rdk9b0zv3hm4falpg8m")))

(define-public crate-nip-55-0.3.1 (c (n "nip-55") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "09m2lx5bx9ddqn1hn1w83jxflzkv0vy6j6qr8y8r991m5d5rv029")))

(define-public crate-nip-55-0.3.2 (c (n "nip-55") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0q6n5cn1pclg8dh7884q93n16r2pj0497x9271a150jmp2c8f7fk")))

(define-public crate-nip-55-0.4.0 (c (n "nip-55") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1q2m20ybjillvmnl9p4hz20z4465bb76d5rdgqzxwic7d4snwibf")))

