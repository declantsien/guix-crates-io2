(define-module (crates-io ni co nico_nico_ni) #:use-module (crates-io))

(define-public crate-nico_nico_ni-0.1.1 (c (n "nico_nico_ni") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "ears") (r "^0.5.1") (d #t) (k 0)))) (h "1kwqzxcw4aiab4n50xffqm69887kczs72rx6ii53d3qpgyayzfdn")))

(define-public crate-nico_nico_ni-0.1.3 (c (n "nico_nico_ni") (v "0.1.3") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "ears") (r "^0.5.1") (d #t) (k 0)))) (h "0jmxk91ljmwxgqbvq277yryrvwm3axz5591xwyx597djy9rlz78z")))

(define-public crate-nico_nico_ni-0.1.4 (c (n "nico_nico_ni") (v "0.1.4") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "ears") (r "^0.5.1") (d #t) (k 0)))) (h "0rxqnxkn5rvmpa2rn853856hqsmynkw35gr1gm7z90g1jgnp6rlj")))

