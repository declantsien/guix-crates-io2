(define-module (crates-io ni co nicolas) #:use-module (crates-io))

(define-public crate-nicolas-0.1.0 (c (n "nicolas") (v "0.1.0") (d (list (d (n "nicolas_macros") (r "^0.1") (d #t) (k 0)))) (h "0r92232vpnr8zplbyqzxidv2k7hzlf92qj7628qjymfavx9xrl7i")))

(define-public crate-nicolas-0.1.1 (c (n "nicolas") (v "0.1.1") (d (list (d (n "nicolas_macros") (r "^0.1") (d #t) (k 0)) (d (n "sffs") (r "^0.1") (d #t) (k 0)))) (h "1mj8pm77xg48xjqf5yd9v0x81m01ldb9msdg2iv4d6bih56c3bmc")))

