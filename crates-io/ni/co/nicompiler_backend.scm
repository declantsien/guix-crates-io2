(define-module (crates-io ni co nicompiler_backend) #:use-module (crates-io))

(define-public crate-nicompiler_backend-0.1.0 (c (n "nicompiler_backend") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("multiple-pymethods"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "182ly1i0wva9nb03x9sgq29wwkflfj9snxcsm1qg2gsjvxrcrcm0")))

(define-public crate-nicompiler_backend-0.2.0 (c (n "nicompiler_backend") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("multiple-pymethods"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "01zvj9w0z7pvnfwqam9by802sm0a9miqv5s47a3wb4702fs3mbda")))

(define-public crate-nicompiler_backend-0.3.0 (c (n "nicompiler_backend") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("multiple-pymethods"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "18dd0jkh44ynapdl0zd52lqi726y36ljzwygqk8p82j4fl8rbckm")))

