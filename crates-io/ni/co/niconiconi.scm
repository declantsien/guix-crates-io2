(define-module (crates-io ni co niconiconi) #:use-module (crates-io))

(define-public crate-NicoNicoNi-0.1.0 (c (n "NicoNicoNi") (v "0.1.0") (d (list (d (n "ears") (r "^0.5.1") (d #t) (k 0)))) (h "1cnih54k83cyqa32ij4dpp5w3dyl2p7l7lnfb8xnkk26n750lq2m")))

(define-public crate-NicoNicoNi-0.1.5 (c (n "NicoNicoNi") (v "0.1.5") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "ears") (r "^0.5.1") (d #t) (k 0)))) (h "0b130lzwqgcjljghp21lfvnmydm329lpp0fq4pfkbkajjgrsj8j4")))

