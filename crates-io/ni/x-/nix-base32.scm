(define-module (crates-io ni x- nix-base32) #:use-module (crates-io))

(define-public crate-nix-base32-0.1.0 (c (n "nix-base32") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "063qpvycwl48vr8r4l1h65fg4rw95i3jlarh3ngdk13lpjjl2g4l")))

(define-public crate-nix-base32-0.1.1 (c (n "nix-base32") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "04jnq6arig0amz0scadavbzn9bg9k4zphmrm1562n6ygfj1dnj45")))

(define-public crate-nix-base32-0.1.2-alpha.0 (c (n "nix-base32") (v "0.1.2-alpha.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1x9bld19as8g1qfcwbpkz1ipa5l2cxcir93r3wr7hgwx3g800s61")))

