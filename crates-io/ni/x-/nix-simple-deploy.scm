(define-module (crates-io ni x- nix-simple-deploy) #:use-module (crates-io))

(define-public crate-nix-simple-deploy-0.1.0 (c (n "nix-simple-deploy") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmd_lib") (r "^0.7.8") (d #t) (k 0)))) (h "0hw4qdl72kacs1pfxv7kzrz2a70xp4lqsnfxgxxhhszvvdpy32jl") (y #t)))

(define-public crate-nix-simple-deploy-0.1.1 (c (n "nix-simple-deploy") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmd_lib") (r "^0.7.8") (d #t) (k 0)))) (h "1j009vdl5jcdx1vc4apygr72ljlhkvlyfhrdz5mrrb0bjian7klr") (y #t)))

