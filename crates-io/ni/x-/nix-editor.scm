(define-module (crates-io ni x- nix-editor) #:use-module (crates-io))

(define-public crate-nix-editor-0.2.3 (c (n "nix-editor") (v "0.2.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1bd0qscajnmwzdz55pwpjm3r1g5yyzjwhd744794z75p8irqkm6c")))

(define-public crate-nix-editor-0.2.4 (c (n "nix-editor") (v "0.2.4") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1kx2dgj1gzyxcjvxp08wcshy78fk3nbqmv20lp5gf47y7ma22y8z")))

(define-public crate-nix-editor-0.2.5 (c (n "nix-editor") (v "0.2.5") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1w2mrl9hsi9dq34j27lrkmaq6l1js0j9ax18zy8kbxqjgxc4ifx3")))

(define-public crate-nix-editor-0.2.6 (c (n "nix-editor") (v "0.2.6") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.3.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "097947mjimk4b9qg82l8082p23gj7vvi9i4c285xncnx6fhg1dp1")))

(define-public crate-nix-editor-0.2.7 (c (n "nix-editor") (v "0.2.7") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.3.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "10lph1mq0kw0ggvw3yjrrx4r3bih70pa566k3f1c7kqdry6sa765")))

(define-public crate-nix-editor-0.2.8 (c (n "nix-editor") (v "0.2.8") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.3.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)))) (h "1s2ngyxh370p3765y16w2zyq1hw2ib05rarwkw4jpac305y82nzi")))

(define-public crate-nix-editor-0.2.9 (c (n "nix-editor") (v "0.2.9") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)))) (h "1ni0j1478qrdafnb8vrflbm3m2ipw03fsnmbls54k4bxd1q320ij")))

(define-public crate-nix-editor-0.2.10 (c (n "nix-editor") (v "0.2.10") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)))) (h "066ss0i5j3dvrak96vbl33icg5kbfzhr7hvq88bly49w1cmqgwa0")))

(define-public crate-nix-editor-0.2.11 (c (n "nix-editor") (v "0.2.11") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)))) (h "1iy6lkg5vi3d74837fz0cpk63znf4lynwd2kjzd8bldpm3hhhqgw")))

(define-public crate-nix-editor-0.2.12 (c (n "nix-editor") (v "0.2.12") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4") (d #t) (k 0)) (d (n "rnix") (r "^0.10") (d #t) (k 0)))) (h "0cphiibsspxv3h4yv5p8ar19mqjmjyc1p9b7rw2x7010l314iwiw")))

(define-public crate-nix-editor-0.3.0-beta.1 (c (n "nix-editor") (v "0.3.0-beta.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nixpkgs-fmt") (r "^1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "rnix") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0npnanx03zmzwbdfkhdh3wvnllqakpgzp3xgz8kznzmkvs9ivk2l")))

(define-public crate-nix-editor-0.3.0 (c (n "nix-editor") (v "0.3.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nixpkgs-fmt") (r "^1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "rnix") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bhnyj36gbybk39y6d797y580iypqiyrax0abhn4v33qyf6vvxm3")))

