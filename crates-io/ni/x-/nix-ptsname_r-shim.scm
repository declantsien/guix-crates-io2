(define-module (crates-io ni x- nix-ptsname_r-shim) #:use-module (crates-io))

(define-public crate-nix-ptsname_r-shim-0.1.1 (c (n "nix-ptsname_r-shim") (v "0.1.1") (d (list (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "15d0s6p6slmpcxsmc7zvf9ys0jzqq0cprphpq5dw22j9mwn5f6d2")))

(define-public crate-nix-ptsname_r-shim-0.1.2 (c (n "nix-ptsname_r-shim") (v "0.1.2") (d (list (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "003jzxl4qczfyqw2kgx9lb06l1hl8g1cv0jlf5nway9my1ha27kk")))

(define-public crate-nix-ptsname_r-shim-0.1.3 (c (n "nix-ptsname_r-shim") (v "0.1.3") (d (list (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "1l0k3624h4xd1q0isza88ipjyrvwif3drxafnh988blamxzw4vhr")))

(define-public crate-nix-ptsname_r-shim-0.2.0 (c (n "nix-ptsname_r-shim") (v "0.2.0") (d (list (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1ia2dkspr5ga87129i15l7qwvrq707iyyg45q72wsw60yzf10zvh")))

