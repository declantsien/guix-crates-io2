(define-module (crates-io ni x- nix-query) #:use-module (crates-io))

(define-public crate-nix-query-1.0.2 (c (n "nix-query") (v "1.0.2") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skim") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1zsyhd05v3wxqfp4a5a6nwmpv6a777pplnms974babc1g5rk9h5q")))

