(define-module (crates-io ni x- nix-debug-adapter) #:use-module (crates-io))

(define-public crate-nix-debug-adapter-0.1.0 (c (n "nix-debug-adapter") (v "0.1.0") (d (list (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1xnym6divc89r3f8ldg623qxabdlm3k38gfl0aym2lyda3gcpiqq") (r "1.67.1")))

