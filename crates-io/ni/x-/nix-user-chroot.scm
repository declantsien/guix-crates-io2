(define-module (crates-io ni x- nix-user-chroot) #:use-module (crates-io))

(define-public crate-nix-user-chroot-1.1.0 (c (n "nix-user-chroot") (v "1.1.0") (d (list (d (n "nix") (r "0.19.*") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0dsgxna9cxw4achbpgdja1znzircfgjw1ap94ww47i7zvmzm0nsm")))

(define-public crate-nix-user-chroot-1.1.1 (c (n "nix-user-chroot") (v "1.1.1") (d (list (d (n "nix") (r "0.19.*") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0pknxcp4knpcc4p7jb4mv2nd5vchsx77w5aapi46dz3sqkq6p32s")))

(define-public crate-nix-user-chroot-1.2.1 (c (n "nix-user-chroot") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.88") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "14f244k3lwgyrilh44lqh77i3cwrh5j0z2xqc24d5vsdn4486zvx")))

(define-public crate-nix-user-chroot-1.2.2 (c (n "nix-user-chroot") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "02m9v6l14vwm69dvslws7css4fyqmj4dpiqcxfnqik60aspxzm0m")))

