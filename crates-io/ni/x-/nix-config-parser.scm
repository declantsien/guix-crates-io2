(define-module (crates-io ni x- nix-config-parser) #:use-module (crates-io))

(define-public crate-nix-config-parser-0.1.0 (c (n "nix-config-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0rp6s24y92bvnrdhz3nqrz5pasvw82mq79q2jb89wnxb5wn9bccr") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nix-config-parser-0.1.1 (c (n "nix-config-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "17swl1y7mjmc6im5w5ba6bp429xdby8yqfi7x8kczz4af9xczw4m") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nix-config-parser-0.1.2 (c (n "nix-config-parser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1h8z940xghzqcibvsbg9wqwm13bjvpksvrav8c7rc7szkhxagl6g") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nix-config-parser-0.1.3 (c (n "nix-config-parser") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0b4h82vrsb17mvsbisnkm6yrq1xl6rhlnym80ap14548dxypd52s") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-nix-config-parser-0.2.0 (c (n "nix-config-parser") (v "0.2.0") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0rsx8x9miws2nrj86z4v23dn4pa66hs788x5ww3cfky4yb39cg9q") (s 2) (e (quote (("serde" "dep:serde" "indexmap/serde"))))))

