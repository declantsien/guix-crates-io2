(define-module (crates-io ni x- nix-nar-cli) #:use-module (crates-io))

(define-public crate-nix-nar-cli-0.1.0 (c (n "nix-nar-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^0.14") (d #t) (k 2)) (d (n "nix-nar") (r "^0.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rml16mi8xp40j8lxj228yld2wdnl9a86xx0asif6vz67cd6vb16")))

(define-public crate-nix-nar-cli-0.2.0 (c (n "nix-nar-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "insta") (r "^0.14") (d #t) (k 2)) (d (n "nix-nar") (r "^0.2") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nvnsjznwxfip1cpkb7vv3rfjbww3yfs5y7825ycp7glnzggqbrm")))

(define-public crate-nix-nar-cli-0.2.1 (c (n "nix-nar-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nix-nar") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08cglsmbyk150kdqpsy4iipnqfc58l11d8xjjxk2qvqv62csp0wa")))

(define-public crate-nix-nar-cli-0.3.0 (c (n "nix-nar-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "insta") (r "^1.26") (d #t) (k 2)) (d (n "nix-nar") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)))) (h "00dabscy5yjq710x9jmzh0006s78g14z16l3cc3d8lf5kbywrcp2")))

