(define-module (crates-io ni x- nix-netconfig) #:use-module (crates-io))

(define-public crate-nix-netconfig-0.1.0 (c (n "nix-netconfig") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.46") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.2") (d #t) (k 0)))) (h "1czirlfsgr71prsw6wrdrqw5886qlqspn6dn5v25z1i4d4b1x939") (y #t)))

(define-public crate-nix-netconfig-0.1.1 (c (n "nix-netconfig") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.46") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.2") (d #t) (k 0)))) (h "0yrvygnpasll7kl69jgj9czmda3vwsyfi0h3zvhhlq2fbgs2kfld") (y #t)))

(define-public crate-nix-netconfig-0.1.2 (c (n "nix-netconfig") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.46") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.2") (d #t) (k 0)))) (h "1qp170y3h0v8drccgzs9a36rrlrywv0c2cj8krmczl5bic5kvky4") (y #t)))

