(define-module (crates-io ni xr nixrs-sys) #:use-module (crates-io))

(define-public crate-nixrs-sys-0.1.0 (c (n "nixrs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1gzy8mlw3jndwvvg56m3r78006dy136phh6pk7bmnjj02l8fva1r")))

