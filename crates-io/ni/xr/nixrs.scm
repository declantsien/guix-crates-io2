(define-module (crates-io ni xr nixrs) #:use-module (crates-io))

(define-public crate-nixrs-0.1.0 (c (n "nixrs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "nixrs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0y3algwg3m6iajh8mkq7y2cr5gr7cb1dl1snq82rg6gah7ib6naj")))

