(define-module (crates-io ni ft niftygate-bindgen) #:use-module (crates-io))

(define-public crate-niftygate-bindgen-0.6.2 (c (n "niftygate-bindgen") (v "0.6.2") (d (list (d (n "ethcontract") (r "^0.12.2") (f (quote ("derive"))) (k 0)) (d (n "ethcontract-generate") (r "^0.12.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "01d0jijk4bvl0y3r6h9krmjbyvd0bmfwzwgd4l825iy2fbxq6njj")))

(define-public crate-niftygate-bindgen-0.6.3 (c (n "niftygate-bindgen") (v "0.6.3") (d (list (d (n "ethcontract") (r "^0.15.3") (f (quote ("derive"))) (k 0)) (d (n "ethcontract-generate") (r "^0.15.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "07745nid95dp6lgrijl9jkdgjyi0qbvs9j4gd0frn70yqgs1g1db")))

(define-public crate-niftygate-bindgen-0.7.0 (c (n "niftygate-bindgen") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "ethcontract") (r "^0.15.3") (f (quote ("derive"))) (k 0)) (d (n "ethcontract-generate") (r "^0.15.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)))) (h "068x8bwqvn1265xpxf6rd8h1m95zmhf9sx5m23wv98m6ibdwavdm")))

(define-public crate-niftygate-bindgen-0.8.0 (c (n "niftygate-bindgen") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "ethcontract") (r "^0.17.0") (f (quote ("derive"))) (k 0)) (d (n "ethcontract-generate") (r "^0.17.0") (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "101gf89q9ggncdh526p5r6np2b5djsmxmfgj5hzkmz1piik6ydnq")))

