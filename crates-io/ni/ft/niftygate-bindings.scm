(define-module (crates-io ni ft niftygate-bindings) #:use-module (crates-io))

(define-public crate-niftygate-bindings-0.6.2 (c (n "niftygate-bindings") (v "0.6.2") (d (list (d (n "ethcontract") (r "^0.12.2") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "07r6m6lznzw42k2p7mqmfhywjcl4dxsn9nxazjkg60r3cc4mhqfj")))

(define-public crate-niftygate-bindings-0.6.3 (c (n "niftygate-bindings") (v "0.6.3") (d (list (d (n "ethcontract") (r "^0.15.3") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "00hvk40ia95ws1w4581lrzqnlzpqwaamwhx8fcang48c2k1gn5br")))

(define-public crate-niftygate-bindings-0.7.0 (c (n "niftygate-bindings") (v "0.7.0") (d (list (d (n "ethcontract") (r "^0.15.3") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "08j1rrm43dvdwj068nqy567490xa2szcwx8svrh51vnsm6fgdwv3")))

(define-public crate-niftygate-bindings-0.8.0 (c (n "niftygate-bindings") (v "0.8.0") (d (list (d (n "ethcontract") (r "^0.17.0") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "00gl9n5h49a4zjs3vnzkqx6dq86ywac3pcwlf6xjf59i62q8a7sy")))

