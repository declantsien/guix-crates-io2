(define-module (crates-io ni ft nifty-asset-types) #:use-module (crates-io))

(define-public crate-nifty-asset-types-0.0.0 (c (n "nifty-asset-types") (v "0.0.0") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "~1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ycj6kwv6gwc6w7mp5yarvm7j0lcx624f791n8iz3swv6f5l70r7") (y #t)))

(define-public crate-nifty-asset-types-0.0.1-alpha.1 (c (n "nifty-asset-types") (v "0.0.1-alpha.1") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0194fsvvmd5fhyhdzzdayp19nwyhbbqgk1ld3iwf35yyyvxji2xb")))

(define-public crate-nifty-asset-types-0.0.1-alpha.2 (c (n "nifty-asset-types") (v "0.0.1-alpha.2") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0638bvpvm327n9iq841x05qqhgar7gpmxadk3iy02wlajf8lgfg0")))

(define-public crate-nifty-asset-types-0.0.1-alpha.3 (c (n "nifty-asset-types") (v "0.0.1-alpha.3") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mjwp0qj7gxkn6frajzmmw4cd230pnqhz64gccg72pl3nk2q0ycb")))

(define-public crate-nifty-asset-types-0.1.0 (c (n "nifty-asset-types") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m65rl94b35cr5wifhxvjykkw2yp6bb0bnp7v1pkv6hp9vlb0svj")))

(define-public crate-nifty-asset-types-0.2.0 (c (n "nifty-asset-types") (v "0.2.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mbch7p0ini7fbv2skyxc708d953zxvpnz5x08ani5dhxfzaq1k1")))

(define-public crate-nifty-asset-types-0.2.1 (c (n "nifty-asset-types") (v "0.2.1") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02la85kiyd4nr0m8zq2q7mjy00h69a3hd18axp6yp3nsn2w3jcwd")))

(define-public crate-nifty-asset-types-0.3.0 (c (n "nifty-asset-types") (v "0.3.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "175fpacmzm613lp0j3sxs8kypynhqmnjp2xn5nzdx8f0dd18ffa6")))

(define-public crate-nifty-asset-types-0.4.0 (c (n "nifty-asset-types") (v "0.4.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07js51601s3a2zxnwqssl78d73whbsqh2bccargcsh1h2nw0y58a")))

(define-public crate-nifty-asset-types-0.5.0 (c (n "nifty-asset-types") (v "0.5.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cyb7b67hq66qy0rcn5qyq7wyv2d5qnabjh9pgxc8315sm3vvk52")))

(define-public crate-nifty-asset-types-0.5.1 (c (n "nifty-asset-types") (v "0.5.1") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04skml0kx7m6508jgwyp8hhs6640d6lqfk7288rnwn2g7lzr54p9")))

(define-public crate-nifty-asset-types-0.6.0 (c (n "nifty-asset-types") (v "0.6.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "podded") (r "^0.5.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1329l7y40s8qbn7bxvm23qk6x556vcvczxga50aafc8vb3dc5w8v")))

