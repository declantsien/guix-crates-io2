(define-module (crates-io ni ft niftygate-guide) #:use-module (crates-io))

(define-public crate-niftygate-guide-0.8.0 (c (n "niftygate-guide") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0yfxiphgf8l7fcr6mfzsz6pngclaz51pp4v9yg0ffj80mkhas0gy")))

