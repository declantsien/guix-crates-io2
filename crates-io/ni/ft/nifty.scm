(define-module (crates-io ni ft nifty) #:use-module (crates-io))

(define-public crate-nifty-0.1.0 (c (n "nifty") (v "0.1.0") (h "1dpj0vsn0vn546zacsp7c7d99akgidfwmyi3jjh9kw37nbhpap6s")))

(define-public crate-nifty-0.1.1 (c (n "nifty") (v "0.1.1") (h "09rx6qyyn3vzzj3d79z9h6s7bfjgnwrkslqr4f90f9dc48vgn53b")))

(define-public crate-nifty-0.1.2 (c (n "nifty") (v "0.1.2") (h "0hm2ljmfdbs2k77y3w3pbj92ycs6wb77q2nr6wzafasb7ml3fnh2")))

(define-public crate-nifty-0.1.3 (c (n "nifty") (v "0.1.3") (h "0hignabwjdkdn37zv4d3dw88qsfivwvwf1s7cjr85akxk7h9gl4p")))

(define-public crate-nifty-0.1.4 (c (n "nifty") (v "0.1.4") (h "18pn69m63j0g30k7h4kdjpyn6x3rxp5a9fb41ayhjxhxyvzffbwg")))

(define-public crate-nifty-0.1.5 (c (n "nifty") (v "0.1.5") (h "1fgyj4jcc1924hfrprghw1nyf02svv50m7mhf953hqy2ii15lrb8")))

(define-public crate-nifty-0.1.6 (c (n "nifty") (v "0.1.6") (h "1rgqzl15yqx1f8qdni0hzjjkys7zj19f75w3zdv4ywn2agdgw9l4")))

(define-public crate-nifty-0.1.7 (c (n "nifty") (v "0.1.7") (h "0bk1k907lgriqarl8g6j3g7fcqrw43pmz84686k98s3dghmi9jva")))

(define-public crate-nifty-0.1.8 (c (n "nifty") (v "0.1.8") (h "0i7l7yd18j86dlrvzqfd1sgzlsapqn11inib10441gyd15950g3x")))

(define-public crate-nifty-0.1.9 (c (n "nifty") (v "0.1.9") (h "170f5gxxnls8rsf5brx1f53fanzbbqgwwkhxfyp9qnfyckfm4hqq")))

(define-public crate-nifty-0.2.0 (c (n "nifty") (v "0.2.0") (h "0463kw81q3vb28ir6y9gzpczn177zi87bqldwzrlrhwrdz5d54m6")))

(define-public crate-nifty-0.2.1 (c (n "nifty") (v "0.2.1") (h "0sla4cnab40gilhmfwbz69mdm53rb4sailrnmzb46z490wxxb043")))

(define-public crate-nifty-0.2.2 (c (n "nifty") (v "0.2.2") (h "16y9d6h2vdy7zwz1z5vj5a6q6vc82mn73l310sg6mj9si3008zsa")))

(define-public crate-nifty-0.2.3 (c (n "nifty") (v "0.2.3") (h "162srgs8cdbavrhgisw709mgqnr3kn82rsncwb4hac6dl273lilj")))

(define-public crate-nifty-0.2.4 (c (n "nifty") (v "0.2.4") (h "1lig233cicnhz72fr3wbglshbflsngb43vy5wdk6djq214qxwq3p")))

(define-public crate-nifty-0.2.5 (c (n "nifty") (v "0.2.5") (h "0g1mb8vqcd4sv6qkgclqb4vxylv495n4qz5mz5ydnp0c24qs6v4i")))

(define-public crate-nifty-0.2.6 (c (n "nifty") (v "0.2.6") (h "00n9lhg5g3jf1zy524a50h81scj7i68fnrcfvijs0l84g914fsv2")))

(define-public crate-nifty-0.2.7 (c (n "nifty") (v "0.2.7") (h "15ja2aiik90rl9asiggilnyql7l4v4x34wj8kk5l7c002agxmfhs")))

(define-public crate-nifty-0.2.8 (c (n "nifty") (v "0.2.8") (h "0xli766yypg0sqq68p9anqvxsg31qj2v04lrjd6d22f71x3qigdh")))

(define-public crate-nifty-0.2.9 (c (n "nifty") (v "0.2.9") (h "0kxgfjh1gp7mc4yv7dmnpmrl50cy0l727njcjicrn5lnx56ghi45")))

(define-public crate-nifty-0.3.0 (c (n "nifty") (v "0.3.0") (h "0jjgxggrfinpvl08yjns2rrsyn8z328r5fiz1vxxlnf1fw9x4hhk")))

(define-public crate-nifty-0.3.1 (c (n "nifty") (v "0.3.1") (h "050aq7zlkdd10q4sk018wymfpa172gv50n24q2d0h5vdxr6yka1x")))

(define-public crate-nifty-0.3.2 (c (n "nifty") (v "0.3.2") (h "0kriypw2bwxj9mb9x2yczkfnac08cxh8b2xxc6802jmhkjv0zryd")))

(define-public crate-nifty-0.3.3 (c (n "nifty") (v "0.3.3") (h "05cms4qlwd03qhh0bjrf0rh95vn63alqjvv1ny62wicxkz0d3rsv")))

