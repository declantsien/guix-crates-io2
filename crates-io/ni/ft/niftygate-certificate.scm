(define-module (crates-io ni ft niftygate-certificate) #:use-module (crates-io))

(define-public crate-niftygate-certificate-0.7.0 (c (n "niftygate-certificate") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.13") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "x509-parser") (r "^0.11.0") (d #t) (k 0)))) (h "0j16gfylc6bnnl4in060f37pprv56s3hwqb145kknahkzi9nyi6k")))

(define-public crate-niftygate-certificate-0.8.0 (c (n "niftygate-certificate") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "pem") (r "^1.0.2") (d #t) (k 0)) (d (n "rcgen") (r "^0.9.2") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("serde-well-known"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.13.2") (d #t) (k 0)))) (h "0fivrsk3ridgzxnxdb35dmbj3z8pnvh6shjghfrc5q18h7hr8r9h")))

