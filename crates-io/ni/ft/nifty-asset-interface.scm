(define-module (crates-io ni ft nifty-asset-interface) #:use-module (crates-io))

(define-public crate-nifty-asset-interface-0.1.0 (c (n "nifty-asset-interface") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.1.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "04nvwmzs4syf8ijirj8a5m6xs9b7n044cffw5ihgigwxawn1lnzh")))

(define-public crate-nifty-asset-interface-0.2.0 (c (n "nifty-asset-interface") (v "0.2.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.2.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "13kch4hvwndp82c8l341iczir5pp42dw5g2wcl2cf8qiqw7yfzh3")))

(define-public crate-nifty-asset-interface-0.2.1 (c (n "nifty-asset-interface") (v "0.2.1") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.2.1") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "03lh7pv9kgx424kca6qjwjgz0c1mdav4yfpmjngx73gslkcmv8yl")))

(define-public crate-nifty-asset-interface-0.3.0 (c (n "nifty-asset-interface") (v "0.3.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.3.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "1igsbmqfi6nn6dwlyb3i606c2pa7sh7cljrm220iglg4ii310kmi")))

(define-public crate-nifty-asset-interface-0.4.0 (c (n "nifty-asset-interface") (v "0.4.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.4.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "17ll2rw5x4kx5a47dqsxbwxww0cdp18w5vb34gm4ayky74wvxwjn")))

(define-public crate-nifty-asset-interface-0.5.0 (c (n "nifty-asset-interface") (v "0.5.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.5.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "04fc8smnww95dm0f2kbz7irlqxi90rdr8s84ww5s5x5s800rzp61")))

(define-public crate-nifty-asset-interface-0.5.1 (c (n "nifty-asset-interface") (v "0.5.1") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.5.1") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "0pijvnx1zpnhqrsr5kjgmwg587xy91krs76ynzhkhb6qabiv92ah")))

(define-public crate-nifty-asset-interface-0.6.0 (c (n "nifty-asset-interface") (v "0.6.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "nifty-asset-types") (r "^0.6.0") (d #t) (k 0)) (d (n "shank") (r "^0.3.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "1cq14l3w5ckys1dygdvsxa1j76iw58dl1wsn2qpw3pddg98yadyd")))

