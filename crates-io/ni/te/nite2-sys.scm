(define-module (crates-io ni te nite2-sys) #:use-module (crates-io))

(define-public crate-nite2-sys-0.0.1 (c (n "nite2-sys") (v "0.0.1") (d (list (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "09yns0pkdwanwgwblr9iwffn60g9mwb8n1gv496sqwm1x4gl3v4j") (l "nite2")))

(define-public crate-nite2-sys-0.1.0 (c (n "nite2-sys") (v "0.1.0") (d (list (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "0ycwq7sx834mrlam6miah98f34xcsfvq42iq9ipmn97iwzsl7myb") (l "nite2")))

(define-public crate-nite2-sys-0.1.1 (c (n "nite2-sys") (v "0.1.1") (d (list (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "0ngsilyjwzpl9lc9w36gbvzlaxdplza2450c1x5h9v8m9cmy557k") (l "nite2")))

(define-public crate-nite2-sys-0.2.0 (c (n "nite2-sys") (v "0.2.0") (d (list (d (n "openni2-sys") (r "^1.0") (d #t) (k 0)))) (h "1lcfdb2b6zf93g5lakbf94g9faajz7i70wl99sivrzirgdm2w5h1") (l "nite2")))

