(define-module (crates-io ni ml nimlib) #:use-module (crates-io))

(define-public crate-nimlib-0.0.0 (c (n "nimlib") (v "0.0.0") (h "1nj5wda2k8j4yzwj2ds7b78vfxvglpi8aga7cwg6d0zd3v89x010")))

(define-public crate-nimlib-0.0.1 (c (n "nimlib") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("std" "serde_derive"))) (d #t) (k 0)))) (h "07vaqv23v5h1m7f14hbvp86fphqy6n7gd53m7rhl6hq52yrqb1ki")))

(define-public crate-nimlib-0.1.0 (c (n "nimlib") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("std" "serde_derive"))) (d #t) (k 0)))) (h "0qd8j9s63rgcmbx8lvs3f29lghbf7fagqz32fq7iiwh47lpzanj5")))

(define-public crate-nimlib-0.1.1 (c (n "nimlib") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("std" "serde_derive"))) (d #t) (k 0)))) (h "0lb06r0s34k01q24spzbf2n9afqq78lm4sna0izsi59xgagz6s6r")))

