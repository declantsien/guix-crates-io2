(define-module (crates-io ni xe nixel) #:use-module (crates-io))

(define-public crate-nixel-1.0.0 (c (n "nixel") (v "1.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0) (p "clap")) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0) (p "santiago")))) (h "0y4dzpvqdvkpzj880fy6hshya93x3la1caifvrxldz6dc45wcyx6") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-1.0.1 (c (n "nixel") (v "1.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0) (p "clap")) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0) (p "santiago")))) (h "0ckv26lb5x169cwksy08yrx093bsc77qrvsxfasii03bggcjz4nf") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-1.0.2 (c (n "nixel") (v "1.0.2") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0) (p "clap")) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0) (p "santiago")))) (h "0yspl29n9w11rzb7vf7mw8c8wzplalnzn573pf5lw7rydma2v84b") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-1.1.0 (c (n "nixel") (v "1.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "14y3ccrx8qy770hmhiq35kb26bvdhnn2xfj3wdv9kbn3jry5l2bl") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-1.2.0 (c (n "nixel") (v "1.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0qj8qzqkarn5fj0djs2yg1m2i6bnyvrj4mg63pf0ly6n67hihdjj") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-2.0.0 (c (n "nixel") (v "2.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "07y52v8k14wr62y0smijzsgs2ykmn4chwkwvac1v42r85866mz4x") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-2.1.0 (c (n "nixel") (v "2.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0wfy9a66mgilfkiig4z7q208bj3ai0b56485hm232vm37hf1r7ls") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-3.0.0 (c (n "nixel") (v "3.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0q5c3dsp8381l3hnamv0v0sindfhj8z32j8w4x586v2w26k8f4b5") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-4.0.0 (c (n "nixel") (v "4.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0kkk4zf8dwa8zyfy4hxcsnmv15yp435g6rd7rhylhlky2vw59af4") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-4.1.0 (c (n "nixel") (v "4.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("std"))) (o #t) (k 0)) (d (n "santiago") (r "^1") (f (quote ("crate_regex"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1m8aglwb8dk65ysl6i0v2bwbi5g5qdrag03clljgnca5xi2dg0n6") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-nixel-5.0.0 (c (n "nixel") (v "5.0.0") (d (list (d (n "bindgen") (r "^0") (k 1)) (d (n "cc") (r "^1") (k 1)) (d (n "clap") (r "^4") (f (quote ("std" "help" "usage"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "walkdir") (r "^2") (k 2)))) (h "052xmp0y4x0l6cvwlr3qmdkfg1hmrgvnrc556a5nfjiwdkmxj0qz") (f (quote (("default" "all") ("all" "serde" "serde_json"))))))

(define-public crate-nixel-5.1.0 (c (n "nixel") (v "5.1.0") (d (list (d (n "bindgen") (r "^0") (k 1)) (d (n "cc") (r "^1") (k 1)) (d (n "clap") (r "^4") (f (quote ("std" "help" "usage"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "walkdir") (r "^2") (k 2)))) (h "1fjyd76jjcd41d26b1l39zbv8bacz8rfc4gd6ahmp4svmq100yyl") (f (quote (("default" "all") ("all" "serde" "serde_json"))))))

(define-public crate-nixel-5.1.1 (c (n "nixel") (v "5.1.1") (d (list (d (n "bindgen") (r "^0") (k 1)) (d (n "cc") (r "^1") (k 1)) (d (n "clap") (r "^4") (f (quote ("std" "help" "usage"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "walkdir") (r "^2") (k 2)))) (h "0zh3969ska03qgzgj5zkw5wygk2704padhrmljzn0kzkzdff2pyg") (f (quote (("default" "all") ("all" "serde" "serde_json"))))))

(define-public crate-nixel-5.2.0 (c (n "nixel") (v "5.2.0") (d (list (d (n "bindgen") (r "^0") (k 1)) (d (n "cc") (r "^1") (k 1)) (d (n "clap") (r "^4") (f (quote ("std" "help" "usage"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "walkdir") (r "^2") (k 2)))) (h "0m2svgi2h4rvllqr1rspmj7awcvl7zxj9cshs3bsilv360hd6xcx") (f (quote (("default" "all") ("all" "serde" "serde_json"))))))

