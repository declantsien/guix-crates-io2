(define-module (crates-io ni nt nintendo64-pac) #:use-module (crates-io))

(define-public crate-nintendo64-pac-0.0.0 (c (n "nintendo64-pac") (v "0.0.0") (h "1j9h9fp72ca64j4v1xjxcgz4f9ddk9hz9xi51xxi9d4amx4i7h9x")))

(define-public crate-nintendo64-pac-0.1.0 (c (n "nintendo64-pac") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "0sc15rgsnl2mm9zjzx8z6459lipppgqqwb75jlymnkmn4nmpljx0")))

(define-public crate-nintendo64-pac-0.2.0 (c (n "nintendo64-pac") (v "0.2.0") (d (list (d (n "proc-bitfield") (r "^0.3.0") (d #t) (k 0)) (d (n "ux") (r "^0.1.5") (d #t) (k 0)))) (h "1fq4dn2pz3nd7jv0pnmqn474b4q8v0q0cpqrilv47s4f7qhzgvqw")))

