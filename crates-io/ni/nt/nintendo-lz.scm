(define-module (crates-io ni nt nintendo-lz) #:use-module (crates-io))

(define-public crate-nintendo-lz-0.1.0 (c (n "nintendo-lz") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.1") (d #t) (k 0)))) (h "1lg7azjnhw56sqk4l7yam09sfbn8cq2wl84j10lmzz5n42mj2q4j")))

(define-public crate-nintendo-lz-0.1.1 (c (n "nintendo-lz") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.1") (d #t) (k 0)))) (h "19jq1in5c9h9l20ilb90rizryykhp1bnrlv38dr3dwch10z2sjxh")))

(define-public crate-nintendo-lz-0.1.2 (c (n "nintendo-lz") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "needle") (r "^0.1.1") (d #t) (k 0)))) (h "0ncsgwh5abbh1i9fpzk9gcwvq2b8ddnm2bzk0kacy5sy1k7cn1pi")))

(define-public crate-nintendo-lz-0.1.3 (c (n "nintendo-lz") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0ip6b863bjsvmf3jkmkchxm9j80b12fsd43ydhnvia51cp6rb2vn")))

