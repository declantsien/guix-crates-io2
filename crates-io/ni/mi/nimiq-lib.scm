(define-module (crates-io ni mi nimiq-lib) #:use-module (crates-io))

(define-public crate-nimiq-lib-0.0.0 (c (n "nimiq-lib") (v "0.0.0") (h "01k18c6n3dkv951z315fn0rc3xwc9jyx79nafmd0nrlf314j5v7n")))

(define-public crate-nimiq-lib-0.1.0 (c (n "nimiq-lib") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "nimiq-consensus") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-database") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-mempool") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-network") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-network-primitives") (r "^0.1") (f (quote ("all"))) (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.1") (f (quote ("networks"))) (d #t) (k 0)))) (h "1y1fdyb679rwy9hjrc6h6194xbg989f2x2z7p3whc97w1wc1cgn2")))

(define-public crate-nimiq-lib-0.2.0 (c (n "nimiq-lib") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "nimiq-consensus") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-database") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-mempool") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-network") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-network-primitives") (r "^0.2") (f (quote ("all"))) (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.2") (f (quote ("networks"))) (d #t) (k 0)))) (h "1f4gy2xdyf7sjddgff5h13wqlyvk5af4y61mfy7h4qpbk46k2c20")))

