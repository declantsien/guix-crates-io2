(define-module (crates-io ni mi nimiq-tree-primitives) #:use-module (crates-io))

(define-public crate-nimiq-tree-primitives-0.0.0 (c (n "nimiq-tree-primitives") (v "0.0.0") (h "1d2h1i4fdyxij5dy2ikczdsy5niv9q2h6pkzs1akdlw9vasyf9hk")))

(define-public crate-nimiq-tree-primitives-0.1.0 (c (n "nimiq-tree-primitives") (v "0.1.0") (d (list (d (n "beserial") (r "^0.1") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "nimiq-account") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.1") (f (quote ("coin"))) (d #t) (k 2)))) (h "15az9qqw03sv0iqc64srbxbqd1gnqgx6fp4v25x5raqvhrzji6a9")))

(define-public crate-nimiq-tree-primitives-0.2.0 (c (n "nimiq-tree-primitives") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "nimiq-account") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.2") (f (quote ("coin"))) (d #t) (k 2)))) (h "0wzgpxgv6ck1b8i508h8jl7kxis5nhk6m3m6p8gfp2nnlvm094j3")))

