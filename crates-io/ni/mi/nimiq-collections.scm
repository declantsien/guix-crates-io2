(define-module (crates-io ni mi nimiq-collections) #:use-module (crates-io))

(define-public crate-nimiq-collections-0.0.0 (c (n "nimiq-collections") (v "0.0.0") (h "1spwk88j91yhm2ri9zlliij8ginrdncncxn7yfabd9b6vc5vmz7i")))

(define-public crate-nimiq-collections-0.1.0 (c (n "nimiq-collections") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0qzw761d3sw3hldwp39h3azqnprvj9gshhrwa3pgh6b76bm4fp8p")))

(define-public crate-nimiq-collections-0.2.0 (c (n "nimiq-collections") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "171dqy1y8bqcfgp2h1h6gnl3k3mvb1f9gvj04cjipby9b6qlf7hl")))

