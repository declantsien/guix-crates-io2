(define-module (crates-io ni mi nimiq-macros) #:use-module (crates-io))

(define-public crate-nimiq-macros-0.0.0 (c (n "nimiq-macros") (v "0.0.0") (h "1cls1fqb7izsws43s5jhm80fkjrzciwn6hpl79r8910nkzj57sx3")))

(define-public crate-nimiq-macros-0.1.0 (c (n "nimiq-macros") (v "0.1.0") (h "1armpzlijr7np5w44y4vkaff7lbs0j2f28258jbgfd5zip0jmkdr")))

(define-public crate-nimiq-macros-0.1.1 (c (n "nimiq-macros") (v "0.1.1") (h "0x6k99l0fw1sr17iiixlip9ygr2hw921wvx0y4lkglf6iyrivdjr")))

(define-public crate-nimiq-macros-0.2.0 (c (n "nimiq-macros") (v "0.2.0") (h "19pard8zwcdk0k8q12jyngs711mjd44lxfdj05dspjarl6w38k86")))

