(define-module (crates-io ni mi nimiq-transaction) #:use-module (crates-io))

(define-public crate-nimiq-transaction-0.0.0 (c (n "nimiq-transaction") (v "0.0.0") (h "03dz3fxcsb6bj958s6rrldvxnj99qlylwrfva93v9wvi23lbnav0")))

(define-public crate-nimiq-transaction-0.1.0 (c (n "nimiq-transaction") (v "0.1.0") (d (list (d (n "beserial") (r "^0.1") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.1") (f (quote ("policy" "networks" "account" "coin"))) (d #t) (k 0)) (d (n "nimiq-utils") (r "^0.1") (f (quote ("merkle"))) (d #t) (k 0)))) (h "0vi0s04fn6ypsdxsh0rnv4i8zwaz6hyvp3czp5q8w1w9qiy471f3")))

(define-public crate-nimiq-transaction-0.2.0 (c (n "nimiq-transaction") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.2") (f (quote ("policy" "networks" "account" "coin"))) (d #t) (k 0)) (d (n "nimiq-utils") (r "^0.2") (f (quote ("merkle"))) (d #t) (k 0)))) (h "1s711yj1f705m9yx0immwz3w34ld4lzvjq0r2lnrrcj918fix200")))

