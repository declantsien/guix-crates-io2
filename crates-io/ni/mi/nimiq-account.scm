(define-module (crates-io ni mi nimiq-account) #:use-module (crates-io))

(define-public crate-nimiq-account-0.0.0 (c (n "nimiq-account") (v "0.0.0") (h "0hdsxcg49a8fni3w08dqx4id5srpzb0i1h8rcc14s29v7pgv6vzv")))

(define-public crate-nimiq-account-0.1.0 (c (n "nimiq-account") (v "0.1.0") (d (list (d (n "beserial") (r "^0.1") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.1") (f (quote ("coin"))) (d #t) (k 0)) (d (n "nimiq-transaction") (r "^0.1") (d #t) (k 0)))) (h "1p7diyhm0bivn9z64k36jmmwafa7q34ycidyak9d03zb3p8giyzl")))

(define-public crate-nimiq-account-0.2.0 (c (n "nimiq-account") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-primitives") (r "^0.2") (f (quote ("coin"))) (d #t) (k 0)) (d (n "nimiq-transaction") (r "^0.2") (d #t) (k 0)))) (h "18vm8m8zn4fh12z99q05hs9074knfl24hi8h6rqc6q0c5jj2ihkh")))

