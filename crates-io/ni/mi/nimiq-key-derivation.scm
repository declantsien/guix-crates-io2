(define-module (crates-io ni mi nimiq-key-derivation) #:use-module (crates-io))

(define-public crate-nimiq-key-derivation-0.0.0 (c (n "nimiq-key-derivation") (v "0.0.0") (h "15cjxhgkwn9fpcsjhsv3ybkdngzxl8rqfvarqxnkwxg2d31m886j")))

(define-public crate-nimiq-key-derivation-0.2.0 (c (n "nimiq-key-derivation") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-keys") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17agmpan0vc1ic8rqqgl5da1qnsc0nbiz91xs4932zgm0wzzr63v")))

