(define-module (crates-io ni mi nimiq-mnemonic) #:use-module (crates-io))

(define-public crate-nimiq-mnemonic-0.0.0 (c (n "nimiq-mnemonic") (v "0.0.0") (h "02vq4p9b50agajslrg9n5lgvmn3mscd6p0cfadqpjb4ck7zzyax4")))

(define-public crate-nimiq-mnemonic-0.2.0 (c (n "nimiq-mnemonic") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-key-derivation") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nimiq-macros") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-utils") (r "^0.2") (f (quote ("bit-vec" "crc"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1fas67qrihb5nivp7wmqwfmyc9djfw2rksscin2wi6zmhhn2s8zs") (f (quote (("key-derivation" "nimiq-key-derivation") ("default" "key-derivation"))))))

