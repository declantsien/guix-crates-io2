(define-module (crates-io ni mi nimiq-hash) #:use-module (crates-io))

(define-public crate-nimiq-hash-0.0.0 (c (n "nimiq-hash") (v "0.0.0") (h "0inirmlcgssfmm048clw5pcgdg4zj7i436i1vk4dww6mfx417jm2")))

(define-public crate-nimiq-hash-0.1.0 (c (n "nimiq-hash") (v "0.1.0") (d (list (d (n "beserial") (r "^0.1") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libargon2-sys") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-macros") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0zhr5f45p4pp5yl4y056vxyx862dpn39pz0k4pswadvak30yhasd")))

(define-public crate-nimiq-hash-0.2.0 (c (n "nimiq-hash") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libargon2-sys") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-macros") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1jlclmw2bp1w4vbz7k1kbq2618njia0wiqyxkhvaw2ci1ny30sx6")))

