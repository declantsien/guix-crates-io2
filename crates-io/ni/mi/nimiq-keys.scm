(define-module (crates-io ni mi nimiq-keys) #:use-module (crates-io))

(define-public crate-nimiq-keys-0.0.0 (c (n "nimiq-keys") (v "0.0.0") (h "1pk6cwnbryl442mrjygknvpxh4ghqyjpj7w1q89pj7nm62i479pc")))

(define-public crate-nimiq-keys-0.1.0 (c (n "nimiq-keys") (v "0.1.0") (d (list (d (n "beserial") (r "^0.1") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1.0.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.1") (d #t) (k 0)) (d (n "nimiq-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "15hxq72n0zharv1shcaq7vh4iapkxdni64045p0mq9ks5r07piiy")))

(define-public crate-nimiq-keys-0.2.0 (c (n "nimiq-keys") (v "0.2.0") (d (list (d (n "beserial") (r "^0.2") (d #t) (k 0)) (d (n "beserial_derive") (r "^0.2") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1.0.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "nimiq-hash") (r "^0.2") (d #t) (k 0)) (d (n "nimiq-macros") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "13cymlbp8iv2cx5nlr4rfqmsdmjkdqpv2k4qkpirk7lrhcx232pw")))

