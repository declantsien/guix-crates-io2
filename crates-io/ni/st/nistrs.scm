(define-module (crates-io ni st nistrs) #:use-module (crates-io))

(define-public crate-nistrs-0.1.0 (c (n "nistrs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "0w14l6h7x5ra4zp7kybw4v4qm58m1c2ymrr3v16hrpihwmn8mbf6")))

(define-public crate-nistrs-0.1.1 (c (n "nistrs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "14b8adr2y7qdx3iphgknqfv5xqiiifmpkjg382zwica6rdrld35l")))

(define-public crate-nistrs-0.1.2 (c (n "nistrs") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "04viyihv6gr7hl19kndjbhjw4ifay71c2gp039r4iphb6vbllafh")))

