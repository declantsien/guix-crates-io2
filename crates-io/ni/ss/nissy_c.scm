(define-module (crates-io ni ss nissy_c) #:use-module (crates-io))

(define-public crate-nissy_c-0.0.1 (c (n "nissy_c") (v "0.0.1") (d (list (d (n "nissy_a") (r "^0.0.1") (d #t) (k 0)) (d (n "nissy_b") (r "^0.0.1") (d #t) (k 0)))) (h "1365izqwv1bqm2kihqzx7cgikfzlc3dfsj8sihj34b9isyjqi44g") (y #t)))

(define-public crate-nissy_c-0.0.2 (c (n "nissy_c") (v "0.0.2") (d (list (d (n "nissy_a") (r "^0.0.2") (d #t) (k 0)) (d (n "nissy_b") (r "^0.0.2") (d #t) (k 0)))) (h "07rcggf8bli9vp08jgi4ydplhg957d4mwnh3yxy4312w76i9n46m") (y #t)))

(define-public crate-nissy_c-0.0.3 (c (n "nissy_c") (v "0.0.3") (d (list (d (n "nissy_a") (r "^0.0.3") (d #t) (k 0)) (d (n "nissy_b") (r "^0.0.3") (d #t) (k 0)))) (h "03inim6h614plfx32dfjv3a5fmdlz81ydlrmkbgknvdq25kyix73") (y #t)))

