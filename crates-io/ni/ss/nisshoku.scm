(define-module (crates-io ni ss nisshoku) #:use-module (crates-io))

(define-public crate-nisshoku-0.1.0 (c (n "nisshoku") (v "0.1.0") (d (list (d (n "mlua") (r "^0.9.1") (f (quote ("luau-jit"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)) (d (n "raylib-ffi") (r "^4.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rouille") (r "^3.6.2") (d #t) (k 0)) (d (n "three-d") (r "^0.16.0") (d #t) (k 0)))) (h "0jvafxi1j5han6xggw9vghxl1yrxl34jwpb5vmi1xi8q7yql8dfr")))

