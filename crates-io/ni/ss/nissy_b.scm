(define-module (crates-io ni ss nissy_b) #:use-module (crates-io))

(define-public crate-nissy_b-0.0.1 (c (n "nissy_b") (v "0.0.1") (d (list (d (n "nissy_a") (r "^0.0.1") (d #t) (k 0)))) (h "1g5g2p421r23m4pjr2mn4f06h5yyj6p89sl7rdry27finf4d508c") (y #t)))

(define-public crate-nissy_b-0.0.2 (c (n "nissy_b") (v "0.0.2") (d (list (d (n "nissy_a") (r "^0.0.2") (d #t) (k 0)))) (h "110cn18isyx0w39184y6p0gcnpfk8a9w5f6g7c7nscksgwvfdp7g") (y #t)))

(define-public crate-nissy_b-0.0.3 (c (n "nissy_b") (v "0.0.3") (d (list (d (n "nissy_a") (r "^0.0.3") (d #t) (k 0)))) (h "1db6lp7jgndn788ic72xillfhlqrfzy2i0v0s3zcd69ax821s9qk") (y #t)))

