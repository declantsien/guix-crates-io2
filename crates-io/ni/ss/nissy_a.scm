(define-module (crates-io ni ss nissy_a) #:use-module (crates-io))

(define-public crate-nissy_a-0.0.1 (c (n "nissy_a") (v "0.0.1") (h "1d2djripn7fvvhzwjasajwrf9gkam63n7zfdx440fvwgid7i74yf") (y #t)))

(define-public crate-nissy_a-0.0.2 (c (n "nissy_a") (v "0.0.2") (h "0lmcfdhm788bkhmmka9x41n5li4z8k8dddljlmpgqx8jx28dlc6l") (y #t)))

(define-public crate-nissy_a-0.0.3 (c (n "nissy_a") (v "0.0.3") (h "1bfqh2x1zxhnpwpdyjm07jihqz5f1w5sd6x7hhl1lfag25jlqkb0") (y #t)))

