(define-module (crates-io ni ps nips) #:use-module (crates-io))

(define-public crate-nips-0.0.35 (c (n "nips") (v "0.0.35") (h "080l8y5rqdrmzxzx8fvy4wvcc39c0h2rg9grf9k156fy2kzsrjbr") (r "1.73")))

(define-public crate-nips-0.0.36 (c (n "nips") (v "0.0.36") (h "0id7j88mdjh3jsi21b0fqjk09rg1ff4aj5pyvc7g8v8smjjfl32l") (r "1.73")))

