(define-module (crates-io ni mb nimble-derive) #:use-module (crates-io))

(define-public crate-nimble-derive-0.1.0 (c (n "nimble-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "064a01c0h7bm2qh9y7dkkmxca4k4y8h8bfv3lw0mcw00i1mr42mf")))

(define-public crate-nimble-derive-0.2.0 (c (n "nimble-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12fq5w0la6gjjfrf93y4ni6pmi8ggh68b3p0jd434cygkj2g7ry8")))

