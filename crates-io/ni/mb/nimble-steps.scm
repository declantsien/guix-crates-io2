(define-module (crates-io ni mb nimble-steps) #:use-module (crates-io))

(define-public crate-nimble-steps-0.0.1 (c (n "nimble-steps") (v "0.0.1") (d (list (d (n "discoid") (r "^0.0.1") (d #t) (k 0)) (d (n "tick-id") (r "^0.0.6") (d #t) (k 0)))) (h "106fggb2l96zcqrsxkgg1spg5wdsrni48jkyd57f49v27kddjlj2")))

(define-public crate-nimble-steps-0.0.2 (c (n "nimble-steps") (v "0.0.2") (d (list (d (n "discoid") (r "^0.0.1") (d #t) (k 0)) (d (n "tick-id") (r "^0.0.6") (d #t) (k 0)))) (h "0p5r3g6cbaf7ddq96z4mmvnrb4l3czc62qi8i0vhps85azhailjd")))

(define-public crate-nimble-steps-0.0.3 (c (n "nimble-steps") (v "0.0.3") (d (list (d (n "discoid") (r "^0.0.1") (d #t) (k 0)) (d (n "tick-id") (r "^0.0.6") (d #t) (k 0)))) (h "0ycdywk12vzr7mx59i18k21rv3p3j4w5p8gk886vv2n86xpkn8rd")))

(define-public crate-nimble-steps-0.0.4 (c (n "nimble-steps") (v "0.0.4") (d (list (d (n "discoid") (r "^0.0.1") (d #t) (k 0)) (d (n "tick-id") (r "^0.0.6") (d #t) (k 0)))) (h "0ljx3c00zyfva9bgdr2zbf114rgh2chlm9rzvzzj1hf6prv2k8x9")))

(define-public crate-nimble-steps-0.0.5 (c (n "nimble-steps") (v "0.0.5") (d (list (d (n "discoid") (r "^0.0.3") (d #t) (k 0)) (d (n "tick-id") (r "^0.0.7") (d #t) (k 0)))) (h "1jzcxf4f59a5j026kla8ipasz7vx08hqr41xg5864bsy86qldzbv")))

