(define-module (crates-io ni mb nimble-assent) #:use-module (crates-io))

(define-public crate-nimble-assent-0.0.1 (c (n "nimble-assent") (v "0.0.1") (h "0hf8hs3gg7q1p85jzahg6brrqkkz3hic1ajmyacr65ji1f0jca1p")))

(define-public crate-nimble-assent-0.0.2 (c (n "nimble-assent") (v "0.0.2") (d (list (d (n "nimble-steps") (r "^0.0.3") (d #t) (k 0)))) (h "0hnrab34y0zg4v970xj0b2lxhxkmg17zbbkl3lbgq91hcv1fxa14")))

(define-public crate-nimble-assent-0.0.3 (c (n "nimble-assent") (v "0.0.3") (d (list (d (n "nimble-steps") (r "^0.0.4") (d #t) (k 0)))) (h "0m8z72yjy2xna1r2dqy7dwh9apkgnczhz56x0sh8y10krv9zjwbm")))

