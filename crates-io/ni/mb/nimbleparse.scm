(define-module (crates-io ni mb nimbleparse) #:use-module (crates-io))

(define-public crate-nimbleparse-0.8.1 (c (n "nimbleparse") (v "0.8.1") (d (list (d (n "cfgrammar") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.8") (d #t) (k 0)) (d (n "lrpar") (r "^0.8") (d #t) (k 0)) (d (n "lrtable") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l7micd43dw5l6prvv0b37qnqbpqp2mnj9h3mgg7kl4fx7kkbi8n")))

(define-public crate-nimbleparse-0.9.0 (c (n "nimbleparse") (v "0.9.0") (d (list (d (n "cfgrammar") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.9") (d #t) (k 0)) (d (n "lrpar") (r "^0.9") (d #t) (k 0)) (d (n "lrtable") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "085rwfr1cs242hgj80qqbqhdcnayxv6a29h0imi5ml0j5gginikv")))

(define-public crate-nimbleparse-0.9.1 (c (n "nimbleparse") (v "0.9.1") (d (list (d (n "cfgrammar") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.9") (d #t) (k 0)) (d (n "lrpar") (r "^0.9") (d #t) (k 0)) (d (n "lrtable") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "128nc89miwpgn3z1nh77x112a6l4n0lr4r4hmq78rg433fwz2rp0")))

(define-public crate-nimbleparse-0.9.2 (c (n "nimbleparse") (v "0.9.2") (d (list (d (n "cfgrammar") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.9") (d #t) (k 0)) (d (n "lrpar") (r "^0.9") (d #t) (k 0)) (d (n "lrtable") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07sfbkzsx3qjs2dspdcxvkszjsbpyfmp98xkbjh0sixd160pzg6p")))

(define-public crate-nimbleparse-0.9.3 (c (n "nimbleparse") (v "0.9.3") (d (list (d (n "cfgrammar") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.9") (d #t) (k 0)) (d (n "lrpar") (r "^0.9") (d #t) (k 0)) (d (n "lrtable") (r "^0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pd1qhx1bi8p5pv17143p47sjhakj0ddq4dcr4gdk5rikiyldw56")))

(define-public crate-nimbleparse-0.10.0 (c (n "nimbleparse") (v "0.10.0") (d (list (d (n "cfgrammar") (r "^0.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.10") (d #t) (k 0)) (d (n "lrpar") (r "^0.10") (d #t) (k 0)) (d (n "lrtable") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16zj0vfbn7hrlv7w85qrwxnbjcz6b2wn2076lz3vw2913c8s76qx")))

(define-public crate-nimbleparse-0.10.1 (c (n "nimbleparse") (v "0.10.1") (d (list (d (n "cfgrammar") (r "^0.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.10") (d #t) (k 0)) (d (n "lrpar") (r "^0.10") (d #t) (k 0)) (d (n "lrtable") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "17nljm66lvrzj0v23bwpn3k651sr16zjd5s4vp1bvp90vw3nwz24")))

(define-public crate-nimbleparse-0.10.2 (c (n "nimbleparse") (v "0.10.2") (d (list (d (n "cfgrammar") (r "^0.10") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.10") (d #t) (k 0)) (d (n "lrpar") (r "^0.10") (d #t) (k 0)) (d (n "lrtable") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wfva7ihcrv6qznrl4p67ixjishh585s67px8a6yd17dn3sncvq9")))

(define-public crate-nimbleparse-0.11.0 (c (n "nimbleparse") (v "0.11.0") (d (list (d (n "cfgrammar") (r "^0.11") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.11") (d #t) (k 0)) (d (n "lrpar") (r "^0.11") (d #t) (k 0)) (d (n "lrtable") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fqplmh682afprc3d12n44chsq4k7hm4swv343lsmrw11dh12382")))

(define-public crate-nimbleparse-0.11.1 (c (n "nimbleparse") (v "0.11.1") (d (list (d (n "cfgrammar") (r "^0.11") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.11") (d #t) (k 0)) (d (n "lrpar") (r "^0.11") (d #t) (k 0)) (d (n "lrtable") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14dv78y4gbdicqsdka7finy3lpnh9bz1hgv1jq0kq9hr8791arnf")))

(define-public crate-nimbleparse-0.12.0 (c (n "nimbleparse") (v "0.12.0") (d (list (d (n "cfgrammar") (r "^0.12") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.12") (d #t) (k 0)) (d (n "lrpar") (r "^0.12") (d #t) (k 0)) (d (n "lrtable") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19hqjblwr480g8vy6bzwm8773p6wn24lysh509ppjllhybp05bl8")))

(define-public crate-nimbleparse-0.13.0 (c (n "nimbleparse") (v "0.13.0") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wm65rv1hyvasnsywr8cf6bp98nryqikl884zhmah6ik3dnda5dl")))

(define-public crate-nimbleparse-0.13.1 (c (n "nimbleparse") (v "0.13.1") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0fai0mrn8nwcvp49b0la73wyxjdn93c0lrwacnzpapr0rrcch75x")))

(define-public crate-nimbleparse-0.13.2 (c (n "nimbleparse") (v "0.13.2") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0w69rfy1n5h3mrcfly38d8za5yij800fjisbyc5jgdk5w6swc1q8")))

(define-public crate-nimbleparse-0.13.3 (c (n "nimbleparse") (v "0.13.3") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wxcr743pvffl83lxma464jsb0gk36hp99x544ww03rii9a5c6ka")))

(define-public crate-nimbleparse-0.13.4 (c (n "nimbleparse") (v "0.13.4") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hjmli3fxjz332rcij978l5pw1l3das0s27d79jldqsbwf5739cc")))

(define-public crate-nimbleparse-0.13.5 (c (n "nimbleparse") (v "0.13.5") (d (list (d (n "cfgrammar") (r "^0.13") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lrlex") (r "^0.13") (d #t) (k 0)) (d (n "lrpar") (r "^0.13") (d #t) (k 0)) (d (n "lrtable") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0g1hgcymvg2glsc4ly0idfmfynawazp9c8zmmkbalx9yq9nmd60c")))

