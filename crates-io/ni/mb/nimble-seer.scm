(define-module (crates-io ni mb nimble-seer) #:use-module (crates-io))

(define-public crate-nimble-seer-0.0.1 (c (n "nimble-seer") (v "0.0.1") (d (list (d (n "nimble-steps") (r "^0.0.2") (d #t) (k 0)))) (h "1wa8yzg845lj3wsai1742r1q8qmwkj4jrnncxggjxa46a6cp3n3r")))

(define-public crate-nimble-seer-0.0.2 (c (n "nimble-seer") (v "0.0.2") (d (list (d (n "nimble-steps") (r "^0.0.3") (d #t) (k 0)))) (h "13p1hb3lcldlahcfx02x9gfmjqywfwc3bz20bw202rq4cxw3zpym")))

(define-public crate-nimble-seer-0.0.3 (c (n "nimble-seer") (v "0.0.3") (d (list (d (n "nimble-steps") (r "^0.0.4") (d #t) (k 0)))) (h "0g9g161mkkicxpi5f3y34blzwc870v2vrn2qdxz0iji12chwrd7y")))

