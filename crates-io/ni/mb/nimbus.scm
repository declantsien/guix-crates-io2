(define-module (crates-io ni mb nimbus) #:use-module (crates-io))

(define-public crate-nimbus-0.1.0 (c (n "nimbus") (v "0.1.0") (h "1gxvngvh2ycij7aqsb9wbml5w81a8xhgrx0pg9wf4pdfkaw219bk")))

(define-public crate-nimbus-0.1.1 (c (n "nimbus") (v "0.1.1") (h "04cbc88xcqnkm2l9bz8pjm90fbrk2cmasahbw6bdxyrg84wki0c1")))

