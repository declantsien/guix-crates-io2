(define-module (crates-io ni sp nispor-clib) #:use-module (crates-io))

(define-public crate-nispor-clib-1.2.2 (c (n "nispor-clib") (v "1.2.2") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p7sqx7r5hrlvfjazligg9hqwn3dc6ky86cfjzmb21drwcz442fd")))

(define-public crate-nispor-clib-1.2.4 (c (n "nispor-clib") (v "1.2.4") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l4jrkfzz0qc8gqq42vmy5ii1r1lpvmcrnb393ydm8f3dhfpmz2a")))

(define-public crate-nispor-clib-1.2.5 (c (n "nispor-clib") (v "1.2.5") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01m4cbz5mnfbqvb404758nr1zbvxswwk7r6qldqk9h1id5n1rsa6")))

(define-public crate-nispor-clib-1.2.6 (c (n "nispor-clib") (v "1.2.6") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "042sl0iws3jqfcp494m2rg0s2h2n039zks9g4s28r1f8plbfijy7")))

(define-public crate-nispor-clib-1.2.9 (c (n "nispor-clib") (v "1.2.9") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0shygxl0l3pk40clhpa9k53qhqfimvad8v8kwd6gkawms24q7mby")))

(define-public crate-nispor-clib-1.2.19 (c (n "nispor-clib") (v "1.2.19") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "nispor") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01x53k4arblcfrigj1kglvjz7p45dz3y0zi1km4fb3yhi7smllfi")))

