(define-module (crates-io ni sl nislib) #:use-module (crates-io))

(define-public crate-nislib-0.1.0 (c (n "nislib") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "palserializer") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (d #t) (k 0)) (d (n "spki") (r "^0.7.2") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "10nizzzxjxpv4c2arw89yprffs797wgf1k7js104shp2m1lhiyy5")))

