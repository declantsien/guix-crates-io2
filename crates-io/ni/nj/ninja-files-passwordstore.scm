(define-module (crates-io ni nj ninja-files-passwordstore) #:use-module (crates-io))

(define-public crate-ninja-files-passwordstore-0.1.0 (c (n "ninja-files-passwordstore") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.4") (d #t) (k 0)) (d (n "ninja-files") (r "^0.1.0") (d #t) (k 0)))) (h "05ifyn8vgqr2z926vsgsi0asns2yk3h4z3mchjyn58mbzlkchi1y")))

(define-public crate-ninja-files-passwordstore-0.2.0 (c (n "ninja-files-passwordstore") (v "0.2.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "0wnpm8clsl1rxzbc75l9c38p5faajc0mg9gq5aglkbykkwcc5jq8")))

