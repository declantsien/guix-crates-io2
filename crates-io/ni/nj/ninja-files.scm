(define-module (crates-io ni nj ninja-files) #:use-module (crates-io))

(define-public crate-ninja-files-0.1.0 (c (n "ninja-files") (v "0.1.0") (d (list (d (n "ninja-files-cookie") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "19harv0xr4mbfkc7srwj6fy6ynkzz2q06bhnf7bwyp4s8vxx4mmc") (f (quote (("default" "format")))) (s 2) (e (quote (("format" "dep:ninja-files-cookie"))))))

(define-public crate-ninja-files-0.2.0 (c (n "ninja-files") (v "0.2.0") (d (list (d (n "ninja-files-cookie") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "1bn9xbd47bkpjhdlmvd36n9lfwqq5sjqr53y50a0cwqqiv9sf12l") (f (quote (("default" "format")))) (s 2) (e (quote (("format" "dep:ninja-files-cookie"))))))

