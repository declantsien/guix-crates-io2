(define-module (crates-io ni nj ninjen) #:use-module (crates-io))

(define-public crate-ninjen-0.0.0 (c (n "ninjen") (v "0.0.0") (h "17324j1vmxrh56bg1l2izgmb9lascq6yhn3609ccvkq1d2zvr6j9")))

(define-public crate-ninjen-0.1.0 (c (n "ninjen") (v "0.1.0") (d (list (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "literally") (r "^0.1.3") (d #t) (k 2)))) (h "0blg1kkdx4l0dizxpc88x57r2xfs7zxr76bmns3327grbpkzr3qp")))

(define-public crate-ninjen-0.1.1 (c (n "ninjen") (v "0.1.1") (d (list (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "literally") (r "^0.1.3") (d #t) (k 2)))) (h "1k1sf493f9145jaqlm5q2kds8nlsriy3v78d4zb75c6hp6r16pvy")))

