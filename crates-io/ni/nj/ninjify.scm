(define-module (crates-io ni nj ninjify) #:use-module (crates-io))

(define-public crate-ninjify-0.1.4 (c (n "ninjify") (v "0.1.4") (d (list (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_any") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0lfzml04s8cjas7gwkn32mjr689jhky6cjklqrbxpk6ihikk6rz7")))

(define-public crate-ninjify-0.1.5 (c (n "ninjify") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_any") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0llcx5fgxf1c8ghp25h571qp3hzvpfqzhp0d6b38lra3kiwzkbzy")))

(define-public crate-ninjify-0.1.6 (c (n "ninjify") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_any") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0xakc6b8b1vrmc4wb2j72x6n7rc6r2wgn64pw2xq1554mnlbhm0h")))

(define-public crate-ninjify-0.1.7 (c (n "ninjify") (v "0.1.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_any") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "06ph6hckqag451dg47i4mw9mv2ixypchyrkrp2zp4fsv2gzjsx6f")))

