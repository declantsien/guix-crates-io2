(define-module (crates-io ni nj ninja_syntax) #:use-module (crates-io))

(define-public crate-ninja_syntax-0.1.0 (c (n "ninja_syntax") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "099yjp6s6rssik8h5hgh956fkl2xshaf3r4294ac8pz7pzw47nf5")))

(define-public crate-ninja_syntax-0.1.1 (c (n "ninja_syntax") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "139nmv38w5wwx85cxfgib2vb1sxkvk4bpkzvxl94d8026blnjrw6")))

