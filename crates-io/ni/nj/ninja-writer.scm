(define-module (crates-io ni nj ninja-writer) #:use-module (crates-io))

(define-public crate-ninja-writer-0.1.0 (c (n "ninja-writer") (v "0.1.0") (h "0xm5xhfa6cd3s40pl9lvf5snvl609nls1jx8zpxc1h947ffcjp39") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ninja-writer-0.1.1 (c (n "ninja-writer") (v "0.1.1") (h "1ry8f5l33jkk63l5h31lkysbfjrpq4985ir1p3nz4v74maa2fk53") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ninja-writer-0.1.2 (c (n "ninja-writer") (v "0.1.2") (h "14chwnfx4176klcv7jbj4igygjgswjaia8qf3vfss4injvv376zs") (f (quote (("std") ("default" "std"))))))

(define-public crate-ninja-writer-0.2.0 (c (n "ninja-writer") (v "0.2.0") (h "0r0slr6434wahdq8zaldpv0ylwpc4vd24qz0id9ykjhxr75pjiwf") (f (quote (("thread-safe" "std") ("std") ("default" "std"))))))

