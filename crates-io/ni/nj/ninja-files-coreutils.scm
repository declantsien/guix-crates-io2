(define-module (crates-io ni nj ninja-files-coreutils) #:use-module (crates-io))

(define-public crate-ninja-files-coreutils-0.1.0 (c (n "ninja-files-coreutils") (v "0.1.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "0344ivzn2bfqfs29j5nnzndziv1a6zv1bmfb8rrywnfjq1lgl0f3")))

(define-public crate-ninja-files-coreutils-0.2.0 (c (n "ninja-files-coreutils") (v "0.2.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "1ja1ccy0j4lrzl73m33qpwfbyb9hfs9vjv5xbxkqv31jx7spk3aq")))

