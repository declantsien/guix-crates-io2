(define-module (crates-io ni nj ninja-files-cookie) #:use-module (crates-io))

(define-public crate-ninja-files-cookie-0.1.0 (c (n "ninja-files-cookie") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3.2") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "15cv6d0zgccg681pvbgr7d47r9h8jf4hy29kjdhrhg7ari165325")))

