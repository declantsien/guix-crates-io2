(define-module (crates-io ni nj ninja-files-talosctl) #:use-module (crates-io))

(define-public crate-ninja-files-talosctl-0.1.0 (c (n "ninja-files-talosctl") (v "0.1.0") (d (list (d (n "ninja-files") (r "^0.1.0") (d #t) (k 0)))) (h "1lckzdz673cppx7ps2q6gs4xhh5w3mhnr645qzgmmk57r0b90vh5")))

(define-public crate-ninja-files-talosctl-0.2.0 (c (n "ninja-files-talosctl") (v "0.2.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "0giq3rj3h31wg4mgqwvpzlfc5y9iv9l894wcfkz80a8vg4aqyfgs")))

