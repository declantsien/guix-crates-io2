(define-module (crates-io ni nj ninjars) #:use-module (crates-io))

(define-public crate-ninjars-0.1.0 (c (n "ninjars") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ninja-builder") (r "^0.1") (d #t) (k 0)) (d (n "ninja-metrics") (r "^0.2") (d #t) (k 0)) (d (n "ninja-parse") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w8vr2145zpq72rkk7big6k81hnhlp2z4j3zb7x2hfwarmf203h0")))

