(define-module (crates-io ni nj ninja-builder) #:use-module (crates-io))

(define-public crate-ninja-builder-0.1.0 (c (n "ninja-builder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.11") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "ninja-metrics") (r "^0.2") (d #t) (k 0)) (d (n "ninja-parse") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "rt-core" "process" "rt-util"))) (k 0)))) (h "10xrmp6g2bxnc54x9jnancbqhfbds7l0zppygaxiimjbg5cvyf9y")))

