(define-module (crates-io ni nj ninja-files-kubectl) #:use-module (crates-io))

(define-public crate-ninja-files-kubectl-0.1.0 (c (n "ninja-files-kubectl") (v "0.1.0") (d (list (d (n "ninja-files") (r "^0.1.0") (d #t) (k 0)))) (h "14zyhqbcdjkcyvy2x5gvdpnzq65pwxxziln4paw51y630lpk039x")))

(define-public crate-ninja-files-kubectl-0.2.0 (c (n "ninja-files-kubectl") (v "0.2.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "05vvhlzv18g2cvi9mc90j0vdvcd94als3wgr5g2138bb7skfhbbw")))

