(define-module (crates-io ni nj ninja-parse) #:use-module (crates-io))

(define-public crate-ninja-parse-0.1.0 (c (n "ninja-parse") (v "0.1.0") (d (list (d (n "globwalk") (r "^0.8") (d #t) (k 2)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "ninja-metrics") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0spbm5dbkgayhrzyx9mr7lkn8sa9m9xz4wai1vvv5d0x2xx0d0cm")))

