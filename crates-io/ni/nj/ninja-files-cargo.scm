(define-module (crates-io ni nj ninja-files-cargo) #:use-module (crates-io))

(define-public crate-ninja-files-cargo-0.1.0 (c (n "ninja-files-cargo") (v "0.1.0") (d (list (d (n "ninja-files") (r "^0.1.0") (d #t) (k 0)))) (h "1qmrwbpzvdry3wi5x291dzs4kv8ifmag1dh1v0pjijkxkh9a6jxs")))

(define-public crate-ninja-files-cargo-0.2.0 (c (n "ninja-files-cargo") (v "0.2.0") (d (list (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0)))) (h "1jvysy64dfm9hmdjxgsh69nhxigln7ryr63vx70ds9qjk2pdq8qa")))

