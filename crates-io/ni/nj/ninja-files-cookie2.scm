(define-module (crates-io ni nj ninja-files-cookie2) #:use-module (crates-io))

(define-public crate-ninja-files-cookie2-0.1.0 (c (n "ninja-files-cookie2") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3.2") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0) (p "ninja-files-data2")) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "07qcin7j1zrb64vc6hpwqcvk142h44irf0yznsphixn2wafl6pzf")))

