(define-module (crates-io ni nj ninja-build-syntax) #:use-module (crates-io))

(define-public crate-ninja-build-syntax-0.0.1 (c (n "ninja-build-syntax") (v "0.0.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "xz2") (r "^0.1") (d #t) (k 2)))) (h "0ll3lip7r227bb1mg6x9mj5rx9izl7z9ralrls7lqxzi1g738adp")))

(define-public crate-ninja-build-syntax-0.0.2 (c (n "ninja-build-syntax") (v "0.0.2") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "xz2") (r "^0.1") (d #t) (k 2)))) (h "1068igw9wm1q1vsvn02klif7vfkyfgv39p8mh0c2lv3isnqcirp5")))

