(define-module (crates-io ni nj ninja-metrics) #:use-module (crates-io))

(define-public crate-ninja-metrics-0.1.0 (c (n "ninja-metrics") (v "0.1.0") (h "1hp7prr3mv7ii2n3aaw5iwkxqhp7nc5i31i7mbbrzqdx9903my3n")))

(define-public crate-ninja-metrics-0.2.0 (c (n "ninja-metrics") (v "0.2.0") (h "0f32l2k6jn3gks4bffhf3wsfy6ayr9b8d7b6n90sw949q17hwngp")))

