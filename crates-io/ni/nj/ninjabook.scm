(define-module (crates-io ni nj ninjabook) #:use-module (crates-io))

(define-public crate-ninjabook-0.1.0 (c (n "ninjabook") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0n1sr5a15kkq4gxkbh2xgjggfv7nnd8gy6iga67d2h2afh65fpsc")))

(define-public crate-ninjabook-0.1.1 (c (n "ninjabook") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0mgbwn8647gwnvqf2cprnbr2392k2yw8mnvspsq7r3rn2izd45y3")))

(define-public crate-ninjabook-0.1.2 (c (n "ninjabook") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0iivycbqgmr26sm7c5x45fsda8d9zxzmljicrpcd40jx2py5zr84")))

(define-public crate-ninjabook-0.1.3 (c (n "ninjabook") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1mzs3xsimy3rds9byh32bfdz781bdimw6vc2gzy44hlc9l0bidz5")))

(define-public crate-ninjabook-0.1.5 (c (n "ninjabook") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1qib0m9l108k4ar55dzrcvglkzldaxsy5hd32hpmss79vsig3pbz")))

(define-public crate-ninjabook-0.1.6 (c (n "ninjabook") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "01zkyf3pag99zkg3hicga4zlf7bsnk6c27qrnpiafpzxrw752n6j")))

