(define-module (crates-io ni nj ninja-files2) #:use-module (crates-io))

(define-public crate-ninja-files2-0.2.0 (c (n "ninja-files2") (v "0.2.0") (d (list (d (n "ninja-files-cookie") (r "^0.1.0") (o #t) (d #t) (k 0) (p "ninja-files-cookie2")) (d (n "ninja-files-data") (r "^0.1.0") (d #t) (k 0) (p "ninja-files-data2")))) (h "1nppnsl1hwf2cv7zyb5vwbxaw72pn55s4nsqd9ra0j567mpyqmxz") (f (quote (("default" "format")))) (s 2) (e (quote (("format" "dep:ninja-files-cookie"))))))

