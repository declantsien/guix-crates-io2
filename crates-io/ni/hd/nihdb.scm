(define-module (crates-io ni hd nihdb) #:use-module (crates-io))

(define-public crate-nihdb-0.1.0 (c (n "nihdb") (v "0.1.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1n1fi3pgq9mhxjhbj6j4nqwajb72km5si4m2qrv9k0xrzbpq7dm7")))

(define-public crate-nihdb-0.2.0 (c (n "nihdb") (v "0.2.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0h4wnnspwdrrgqn8ppl8f4cs69sz59a5hx04jg3r4598grd8krgz")))

(define-public crate-nihdb-0.3.0 (c (n "nihdb") (v "0.3.0") (d (list (d (n "crc") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0pv6sc5mady24hy580y4vh8qps2l7l3ip45mlg0f9ycild309dnr")))

