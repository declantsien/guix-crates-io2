(define-module (crates-io ni nr ninres-cli) #:use-module (crates-io))

(define-public crate-ninres-cli-0.0.1 (c (n "ninres-cli") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "ninres") (r "^0.0") (f (quote ("bfres" "sarc" "tar_ninres" "zstd"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a6z0l7qi0fggdns2cya1r1jf4miw5r1nxxwhgvbrijnn015gnfz")))

