(define-module (crates-io ni nr ninres) #:use-module (crates-io))

(define-public crate-ninres-0.0.1 (c (n "ninres") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "ruzstd") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bfr27hn51w7vhm0mv758hz8ww6wmpnc23gfzh0lnff12z8nczsm") (f (quote (("zstd" "ruzstd") ("tar_ninres" "tar") ("sarc") ("default") ("bfres"))))))

(define-public crate-ninres-0.0.2 (c (n "ninres") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "ruzstd") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1809kjmzqgdm793k9xm9fbr0bx6g8drb2j91swv98hyxl0dzhgy2") (f (quote (("zstd" "ruzstd") ("tar_ninres" "tar") ("sarc") ("default") ("bfres"))))))

