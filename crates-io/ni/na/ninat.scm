(define-module (crates-io ni na ninat) #:use-module (crates-io))

(define-public crate-ninat-0.1.0 (c (n "ninat") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.3") (d #t) (k 0)) (d (n "socks") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1cr2m0xqnd3p6n1i7nf2aph9fd0vmij1hy11w9621h0rr9ch1a5b")))

