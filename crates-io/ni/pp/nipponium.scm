(define-module (crates-io ni pp nipponium) #:use-module (crates-io))

(define-public crate-nipponium-0.1.0 (c (n "nipponium") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1f4xmca1r4r02dkyc7s0yhcvqnvh8jd764mr9xdvq2i7v3bz3rgc")))

(define-public crate-nipponium-0.1.1 (c (n "nipponium") (v "0.1.1") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0z22ycx9gzvj0y520ap2w5wc7w4jwsdy9s6krilryg08snz8m8sl")))

