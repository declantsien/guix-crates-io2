(define-module (crates-io ni pp nippy) #:use-module (crates-io))

(define-public crate-nippy-1.0.0 (c (n "nippy") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sf11v0w85axjnvy0pp8d13wkdqi6qhcryw08wdlqkqfp50v63p1") (y #t)))

(define-public crate-nippy-1.0.1 (c (n "nippy") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wjynn07c34b31fnfcbiyh3a4z4r3sqd64wvnvxrh3parxhzl0rx") (y #t)))

(define-public crate-nippy-2.0.0 (c (n "nippy") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "136imjs3b6ihnksb90m5h26cll9q3kq2ixwl0zharsvkmvz7hq4n") (f (quote (("tokio-runtime" "tokio") ("default" "async-std") ("async-std-runtime" "async-std"))))))

