(define-module (crates-io ni pp nipper-trunk) #:use-module (crates-io))

(define-public crate-nipper-trunk-0.1.9 (c (n "nipper-trunk") (v "0.1.9") (d (list (d (n "cssparser") (r "^0.27.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "readability") (r "^0.1.5") (d #t) (k 2)) (d (n "regex") (r "^1.3.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "selectors") (r "^0.22.0") (d #t) (k 0)) (d (n "tendril") (r "^0.4.2") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 2)))) (h "0268br4pm9r88fz5dmm3sg1pmz1p3y4d4647v5zsw4j4983d6k2a")))

