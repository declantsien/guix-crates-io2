(define-module (crates-io ni xc nixcfg) #:use-module (crates-io))

(define-public crate-nixcfg-0.2.0 (c (n "nixcfg") (v "0.2.0") (d (list (d (n "rnix") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0k9yf0vhv7zqjlhhlc09lj4qn69djqahi08iin4qf0c6ggfiyn79")))

