(define-module (crates-io ni b- nib-server) #:use-module (crates-io))

(define-public crate-nib-server-0.0.1 (c (n "nib-server") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "0.13.*") (d #t) (k 0)) (d (n "tokio") (r "0.2.*") (f (quote ("fs" "macros" "stream"))) (k 0)) (d (n "tokio-util") (r "^0.3.1") (f (quote ("codec"))) (k 0)))) (h "0zg22a8sl17mg3jrxr3kahaz16icvw4r7cqxypw0wyz66p43bjvm")))

(define-public crate-nib-server-0.0.2 (c (n "nib-server") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "0.14.*") (f (quote ("http1" "runtime" "server" "stream"))) (k 0)) (d (n "tokio") (r "1.7.*") (f (quote ("fs" "macros" "rt" "rt-multi-thread"))) (k 0)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (k 0)))) (h "03s408d6nqr6qaqn1w4kkjjg8wpfy2h590hsvwgsnq9rinn10xh8")))

