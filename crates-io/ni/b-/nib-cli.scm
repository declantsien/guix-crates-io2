(define-module (crates-io ni b- nib-cli) #:use-module (crates-io))

(define-public crate-nib-cli-0.0.1 (c (n "nib-cli") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "nib") (r "^0.0.6") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gnxlm5mr7ib6lxq2giw3s13sag0mgmzlmp8x9jhlqgg18y7dy0q")))

(define-public crate-nib-cli-0.0.2 (c (n "nib-cli") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "nib") (r "^0.0.7") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1dpcjyjm19yjhn1nrzcpfvgqmfbbks1hw0fyc8h4yhsdnjgqqnq1")))

(define-public crate-nib-cli-0.0.3 (c (n "nib-cli") (v "0.0.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "nib") (r "^0.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0nwkp37wlkjcn2nvb86kw050rw3fm8zxfzrlj8xxalln0l509bds")))

