(define-module (crates-io ni wl niwl) #:use-module (crates-io))

(define-public crate-niwl-0.1.0 (c (n "niwl") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fuzzytags") (r "^0.4.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "secretbox") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0glxq784xr0zgbf7h9csly1kmjg25jqhlcdg188kmqn68mxx051b")))

