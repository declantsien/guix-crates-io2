(define-module (crates-io ni tp nitpickers) #:use-module (crates-io))

(define-public crate-nitpickers-0.1.0 (c (n "nitpickers") (v "0.1.0") (h "1yjnf6vir1v6vpkxnz9v0xxic05qk7wm6kv3jvpar65yll9x8ziw") (y #t)))

(define-public crate-nitpickers-0.1.1 (c (n "nitpickers") (v "0.1.1") (h "1f434nzw20jlsq60j5i9r4rbj9s5pzcc46s22f3l2bl54ldvs863")))

