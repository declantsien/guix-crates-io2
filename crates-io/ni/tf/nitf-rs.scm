(define-module (crates-io ni tf nitf-rs) #:use-module (crates-io))

(define-public crate-nitf-rs-0.1.0 (c (n "nitf-rs") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0siz7bc505rvwg78b2sjrxa33bcns4z2qrsxkyp56y852flwca3d")))

(define-public crate-nitf-rs-0.1.1 (c (n "nitf-rs") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "1vdl9rvmqxgzwf0a9kfsd72rj851rda1p2rb7nbqvb13waxg58fm")))

(define-public crate-nitf-rs-0.1.2 (c (n "nitf-rs") (v "0.1.2") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "02qq75syxvm4q2gkbvkdc3mxb5kvh6v670b3m5cwxark7ffmsjp9")))

(define-public crate-nitf-rs-0.1.3 (c (n "nitf-rs") (v "0.1.3") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0w0a02pb5777vwpsgznxjmsyvx47f210rdgk4a8gis61y0li4s60") (y #t)))

(define-public crate-nitf-rs-0.1.4 (c (n "nitf-rs") (v "0.1.4") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0hg7mpb4c73v46pnj3gk0ryffnhg6s7x00f8njgi3fxg1wcsqmpf")))

(define-public crate-nitf-rs-0.1.5 (c (n "nitf-rs") (v "0.1.5") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "1qd2x22f3kw2v4gy7jayjln3jb16pylc20yiaf1kgy493k3q2jfp")))

(define-public crate-nitf-rs-0.1.6 (c (n "nitf-rs") (v "0.1.6") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0753jvix22n6rw27wc4ar53fls5958hpj8dwb8icywind7kir55q")))

(define-public crate-nitf-rs-0.1.7 (c (n "nitf-rs") (v "0.1.7") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.2") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "01bgqsm2ix4j4h5iiq6qk076vpnbp34i06z4a74ji3mf44lp99f4")))

(define-public crate-nitf-rs-0.1.8 (c (n "nitf-rs") (v "0.1.8") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d48433l88za28b9hbyx820wm3i6b9gq6g48x9cr8qxl6sds258h")))

(define-public crate-nitf-rs-0.2.0 (c (n "nitf-rs") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hzv52592pb6n4xxlnrg9iab02a2c17fkmfsr53bz5r091jlrbmk")))

(define-public crate-nitf-rs-0.2.1 (c (n "nitf-rs") (v "0.2.1") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "memmapix") (r "^0.7.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cyrbb7p700fgnl0d1d2lx2f20c95svlj21winsn9avfwiv6gj7c") (y #t)))

(define-public crate-nitf-rs-0.2.2 (c (n "nitf-rs") (v "0.2.2") (d (list (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11px6m2vim7v89fx8zbxvb1s2lls1vmc6k7x1ayxmps08z126j7f")))

(define-public crate-nitf-rs-0.2.3 (c (n "nitf-rs") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "171cly9fim5w2qn5k1a8dfz5wync00l42c6bz4cii3r6gsk3hgj2")))

(define-public crate-nitf-rs-0.2.4 (c (n "nitf-rs") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02lc5x2karbm0lgr3y8lqjvhz2k9mxz2ndxkn1p4kx5xn1fv18wi") (y #t)))

(define-public crate-nitf-rs-0.3.0 (c (n "nitf-rs") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06ynm4sf8sljffb7a8ykb2440z0iczd54c6dv7bsdmkbxbavik31")))

(define-public crate-nitf-rs-0.3.1 (c (n "nitf-rs") (v "0.3.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y6z7m5410gjwh6lf9ykr301isw4hrnf3afq02vysr60zghhkw20")))

