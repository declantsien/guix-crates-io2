(define-module (crates-io ni _d ni_display) #:use-module (crates-io))

(define-public crate-ni_display-0.1.0 (c (n "ni_display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "1yi8y9h2rxs6m3l799pn2yfah42vaic1ajjrh1qx9igrl67521ij")))

