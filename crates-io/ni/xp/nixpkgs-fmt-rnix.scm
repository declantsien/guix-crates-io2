(define-module (crates-io ni xp nixpkgs-fmt-rnix) #:use-module (crates-io))

(define-public crate-nixpkgs-fmt-rnix-1.2.0 (c (n "nixpkgs-fmt-rnix") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.10") (d #t) (k 0)) (d (n "rnix") (r "^0.10.1") (d #t) (k 0)) (d (n "rowan") (r "^0.12.5") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smol_str") (r "^0.1.17") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 2)))) (h "0q5shbm0ahyghmc4m7y44nq725k34wddgpgnalcmfqc3r6hjik9q")))

