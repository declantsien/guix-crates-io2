(define-module (crates-io ni -s ni-sync-rs) #:use-module (crates-io))

(define-public crate-ni-sync-rs-23.8.0 (c (n "ni-sync-rs") (v "23.8.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "15vh232q9fq4mld7pxj27pb9mgcvdzmcckihas957h945zyp65bg") (l "ni-sync")))

