(define-module (crates-io ni c- nic-port-info) #:use-module (crates-io))

(define-public crate-nic-port-info-0.0.1 (c (n "nic-port-info") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "15f6dmywwsjq1qqcd2w56pp8il3yzphydpzzdxhjykb3jxxkwmjs")))

(define-public crate-nic-port-info-0.0.2-alphav1 (c (n "nic-port-info") (v "0.0.2-alphav1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "17fy2yfb20222sp25lpm6nb9x6iixia2b8vigw91pscsaglciqq2")))

