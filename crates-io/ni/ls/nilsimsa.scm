(define-module (crates-io ni ls nilsimsa) #:use-module (crates-io))

(define-public crate-nilsimsa-0.1.0 (c (n "nilsimsa") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "137vzxmbrzlrppg7q92j556ylickjs30781rj1fh9bbc63amsdm9")))

(define-public crate-nilsimsa-0.1.1 (c (n "nilsimsa") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1v6bgg7j8r8inxalxg3fnkzlgvmfpjhc0zcz3piwg6ac2ihv97xk")))

(define-public crate-nilsimsa-0.2.0 (c (n "nilsimsa") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1ps0a7kwsdyg73ls2d5h6c9wwcmgcb9kl22wnla4al13p7x50i6v")))

