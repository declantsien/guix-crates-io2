(define-module (crates-io ni dr nidrs-diesel) #:use-module (crates-io))

(define-public crate-nidrs-diesel-0.0.1 (c (n "nidrs-diesel") (v "0.0.1") (d (list (d (n "diesel") (r "^2.1.6") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "nidrs") (r "^0.0.8") (d #t) (k 0)) (d (n "nidrs-extern") (r "^0.0.8") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.8") (d #t) (k 0)))) (h "1cq4q9bj52np4l3mymjrcaqqgqn3ds255797xyhfmw0h98mcnd7i") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql"))))))

