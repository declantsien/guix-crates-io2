(define-module (crates-io ni dr nidrs) #:use-module (crates-io))

(define-public crate-nidrs-0.0.1 (c (n "nidrs") (v "0.0.1") (d (list (d (n "axum") (r "^0.7.4") (f (quote ("ws"))) (d #t) (k 0)) (d (n "axum-extra") (r "^0.9.2") (f (quote ("typed-header" "typed-routing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "026cbxi767b8zsiv2hgiggfbwnxrzxhh74i84kf8iwpqgm4gnsm5")))

(define-public crate-nidrs-0.0.2 (c (n "nidrs") (v "0.0.2") (d (list (d (n "axum") (r "^0.7.4") (f (quote ("ws"))) (d #t) (k 0)) (d (n "axum-extra") (r "^0.9.2") (f (quote ("typed-header" "typed-routing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0mp98iicxz240vrsh08p4rbp207zkjcyqmyvzni6xxsls5ryaaxg")))

(define-public crate-nidrs-0.0.3 (c (n "nidrs") (v "0.0.3") (d (list (d (n "nidrs-extern") (r "^0.0.3") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1v17jjhi548xy0y2y3y6habggs0nv4a6r0kw6sssxdrgi4p981l7")))

(define-public crate-nidrs-0.0.4 (c (n "nidrs") (v "0.0.4") (d (list (d (n "nidrs-extern") (r "^0.0.4") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0sbnadni9grnxlx473vyq3f302bhydsgafskn7f1i4jhbrqij8dn")))

(define-public crate-nidrs-0.0.5 (c (n "nidrs") (v "0.0.5") (d (list (d (n "nidrs-extern") (r "^0.0.5") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0vsap8jdrvjizf580vyynm57567c5dbh545zy9yj53jph025vnkk")))

(define-public crate-nidrs-0.0.7 (c (n "nidrs") (v "0.0.7") (d (list (d (n "nidrs-extern") (r "^0.0.7") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0i5a9343nyncd5jr2606zki5x8q0gi8ja9bl3q0rh3fl4c9s2hz6")))

(define-public crate-nidrs-0.0.8 (c (n "nidrs") (v "0.0.8") (d (list (d (n "nidrs-extern") (r "^0.0.8") (d #t) (k 0)) (d (n "nidrs-macro") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1wamgi4ns4ah462qbyww7rdb4wllh6z3lkb40x2y63ykfgda1f6x")))

