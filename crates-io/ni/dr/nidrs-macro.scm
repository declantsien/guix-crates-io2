(define-module (crates-io ni dr nidrs-macro) #:use-module (crates-io))

(define-public crate-nidrs-macro-0.0.1 (c (n "nidrs-macro") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "08m3pqnlr4yv5052n6accrkgsvrsfkb5bajwrx2c7h7g6n002spv")))

(define-public crate-nidrs-macro-0.0.2 (c (n "nidrs-macro") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9r3pqhr1w21842rchpdl6wv33f1xlrp0g8vfvq7vwccrp9cmd2")))

(define-public crate-nidrs-macro-0.0.3 (c (n "nidrs-macro") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqabw0kr816lp4k11j3sw56zsdhyy67z5irnjm8bn3rvwpi91rh")))

(define-public crate-nidrs-macro-0.0.4 (c (n "nidrs-macro") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "14v77kjplj4cl3h3l0rs199rmn3bkphs452crcm3ql633mywdr6f")))

(define-public crate-nidrs-macro-0.0.5 (c (n "nidrs-macro") (v "0.0.5") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1da32ng2w936gf389hy9igzsx8p004j6khhw8pbab8vwfhhf7b7y")))

(define-public crate-nidrs-macro-0.0.6 (c (n "nidrs-macro") (v "0.0.6") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.3") (f (quote ("json"))) (d #t) (k 0)))) (h "1cp24hinzr3y7ypky2xlpv1n7zb9s9cck50fc2mn3hkbfnda4g55")))

(define-public crate-nidrs-macro-0.0.7 (c (n "nidrs-macro") (v "0.0.7") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.3") (f (quote ("json"))) (d #t) (k 0)))) (h "0lfq353wy71wliczjrv2svlhcb310a2kmsp8mya4pkcxvn1rkc2f")))

(define-public crate-nidrs-macro-0.0.8 (c (n "nidrs-macro") (v "0.0.8") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.3") (f (quote ("json"))) (d #t) (k 0)))) (h "0nrshwg0mlrv2kr9k3y3z6gml877mv70g0nxlcz7c5lxsp47s1rs")))

