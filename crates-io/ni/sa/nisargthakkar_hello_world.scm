(define-module (crates-io ni sa nisargthakkar_hello_world) #:use-module (crates-io))

(define-public crate-nisargthakkar_hello_world-0.1.0 (c (n "nisargthakkar_hello_world") (v "0.1.0") (h "1163r765rbnv0zyy35075mm6prc04sakwy9aa5ycjmmf2f90kdnk")))

(define-public crate-nisargthakkar_hello_world-0.1.1 (c (n "nisargthakkar_hello_world") (v "0.1.1") (h "0x0xz46mg5gpgddbcw5yc674qzp2yyqf46cg99mgjsf0ynnv5q4l")))

(define-public crate-nisargthakkar_hello_world-0.1.2 (c (n "nisargthakkar_hello_world") (v "0.1.2") (h "081zg3x6pj1sk4wklfxxmh23rgm4fx46991f2sn9shz7blkwc735")))

