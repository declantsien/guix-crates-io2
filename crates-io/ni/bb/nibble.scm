(define-module (crates-io ni bb nibble) #:use-module (crates-io))

(define-public crate-nibble-0.1.0 (c (n "nibble") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.2") (k 0)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "14ypdq06g8r7jb3h90qq7nlck134j26jvjh4bbdp44gqrzvzq2y1") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

