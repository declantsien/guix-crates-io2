(define-module (crates-io ni bb nibble_vec) #:use-module (crates-io))

(define-public crate-nibble_vec-0.0.1 (c (n "nibble_vec") (v "0.0.1") (h "0q826anwds6s5fpr8k0r50532wxkf97yrk51x984pr9i159lib90")))

(define-public crate-nibble_vec-0.0.2 (c (n "nibble_vec") (v "0.0.2") (h "1dbk1b1fkpap248dyllz76grcdl4vfvj785pb1xn1vrmvb5w17f4")))

(define-public crate-nibble_vec-0.0.3 (c (n "nibble_vec") (v "0.0.3") (h "17px618yya2fknjyh1hz880giyqd11yzvkhpp7rcaw2cg8ipirk2")))

(define-public crate-nibble_vec-0.0.4 (c (n "nibble_vec") (v "0.0.4") (h "1ykgfnksyvw811fz51jfnh13s77gn9wq1c2ds3s37q5wnhypzmy8")))

(define-public crate-nibble_vec-0.0.5 (c (n "nibble_vec") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "0k6y8a4i4ny5bwpgqymjhvnfw3fnxn5apd95zw5y6khqzw2i0m94") (y #t)))

(define-public crate-nibble_vec-0.1.0 (c (n "nibble_vec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)))) (h "0hsdp3s724s30hkqz74ky6sqnadhp2xwcj1n1hzy4vzkz4yxi9bp")))

