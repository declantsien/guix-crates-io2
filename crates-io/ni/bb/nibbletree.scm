(define-module (crates-io ni bb nibbletree) #:use-module (crates-io))

(define-public crate-nibbletree-0.1.0 (c (n "nibbletree") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "ip_network") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.7.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (k 2)) (d (n "rstest_reuse") (r "^0.5.0") (k 2)) (d (n "thin-vec") (r "^0.2.10") (d #t) (k 0)))) (h "0s564q4cinhxd46fw1np1z5rv1sq40wzw7yd8w4sv0v1f4g6ind2")))

(define-public crate-nibbletree-0.2.0 (c (n "nibbletree") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "ip_network") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 2)) (d (n "rstest") (r "^0.18") (k 2)) (d (n "rstest_reuse") (r "^0.6") (k 2)) (d (n "thin-vec") (r "^0.2") (d #t) (k 0)))) (h "0xy0zxhkszpml3djc8ls2v9kcfl2513jw74s4rrmdcf3vny9z04y")))

