(define-module (crates-io ni bb nibbler) #:use-module (crates-io))

(define-public crate-nibbler-0.1.1 (c (n "nibbler") (v "0.1.1") (h "16s2iipvs23nr4hqpy19p3q3645c8vvsv009rpfw7fpxky2nhd4q") (y #t)))

(define-public crate-nibbler-0.1.2 (c (n "nibbler") (v "0.1.2") (h "11pl4b8p6gd2f6z0pqh0k7ipqs4k18j14j53s7wp5bb76g40cbsx")))

(define-public crate-nibbler-0.1.3 (c (n "nibbler") (v "0.1.3") (h "1vvhdlk5hq28qvgvwsyr8gmds06c5aqw01n2mdqakm9wzhkl1q4d")))

(define-public crate-nibbler-0.1.8 (c (n "nibbler") (v "0.1.8") (h "1ghhlk1i3fwa1wgp1gz001nwp8x1wqx4z2dwfsfxn93dcijcakkg")))

(define-public crate-nibbler-0.2.1 (c (n "nibbler") (v "0.2.1") (h "1688vi8rid6350487b5h1icq3xwj084m0jy2gnm7y7plzkwvd053")))

(define-public crate-nibbler-0.2.3 (c (n "nibbler") (v "0.2.3") (h "1hcvclq2a6fpwx2ya367wrvmfag099q49zpac0p9xzsj5g6m6jrs")))

