(define-module (crates-io ni bb nibble8) #:use-module (crates-io))

(define-public crate-nibble8-0.1.0 (c (n "nibble8") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "0sc5brf4kfcxyc21w9gyqng1gvc4byxpchh5wc5af9f4947g1arh") (y #t)))

(define-public crate-nibble8-0.1.1 (c (n "nibble8") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "0wwwb0d7fpym4zhjci36s31bfznp4cwra4mxjs5i9p8sfib0hsqk") (y #t)))

(define-public crate-nibble8-0.1.2 (c (n "nibble8") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "1zjr6i6pkm4rnbdhjmmpkm9dckyciwizkpji3gfmg92gglqj68c8")))

(define-public crate-nibble8-0.1.3 (c (n "nibble8") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "11mw6zcp5rl06rhj8k3ga29xf59zs57a8m00b9mjwslykivx3ir0")))

(define-public crate-nibble8-0.1.4 (c (n "nibble8") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "0rlmx4h1q7bbxlhqqjhxmfzc35y27aq3lbdfxn07710kk7vbcvrq")))

(define-public crate-nibble8-0.2.0 (c (n "nibble8") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (d #t) (k 0)))) (h "10bw9r4xypkkbbvgq8va1w11105h3304bzw1npd4fb1svlfflvxi")))

