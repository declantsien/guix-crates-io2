(define-module (crates-io ni -d ni-daqmx-sys) #:use-module (crates-io))

(define-public crate-ni-daqmx-sys-20.7.0 (c (n "ni-daqmx-sys") (v "20.7.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "1sc5bn3x2gxv850xkdcp7xif953gc44dnppx67snf234grgw2sdy") (l "nidaqmx")))

(define-public crate-ni-daqmx-sys-20.7.1 (c (n "ni-daqmx-sys") (v "20.7.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "1y7pkdvvj2r1kk3xg5kci69zd7znz1xr6vgpal1diaigbsx4px3g") (l "nidaqmx")))

