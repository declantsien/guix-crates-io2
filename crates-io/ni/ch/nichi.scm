(define-module (crates-io ni ch nichi) #:use-module (crates-io))

(define-public crate-nichi-0.11.0 (c (n "nichi") (v "0.11.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bgfxza6hrllp2xhfk0g0vjsdbk9kzd0f68apdcxw2fafvwgxipb") (f (quote (("default" "serde" "bincode")))) (y #t)))

(define-public crate-nichi-0.1.0 (c (n "nichi") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nsjmj0qpvqnkdkl1f7z008sh27pag77h7by58jgx0470m526q10") (f (quote (("default" "serde" "bincode"))))))

(define-public crate-nichi-0.2.0 (c (n "nichi") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v9qyknxxskkcq1dg89ncd63dvqc8byig25s55y62j40syglkfhr") (f (quote (("default" "serde" "bincode"))))))

(define-public crate-nichi-0.3.0 (c (n "nichi") (v "0.3.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02d7b5bd2i50841l3ii8fix8gk3m9jm71x5xwhms10ddy4l1aijg") (f (quote (("default" "serde" "bincode"))))))

(define-public crate-nichi-0.4.0 (c (n "nichi") (v "0.4.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00969qhwwysxp3gfp5d23bddzbr4gi997lavy767zkdldf0ch0g5") (f (quote (("default" "serde" "bincode"))))))

(define-public crate-nichi-0.5.0 (c (n "nichi") (v "0.5.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1872kqf31s18m23pk0398gpyzx3wv12g0dnxwbhvci2c9ms19i5a") (f (quote (("default" "serde" "bincode"))))))

(define-public crate-nichi-0.5.1 (c (n "nichi") (v "0.5.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1y2xb1vnbd30nfhqv7j0d9n41r2zyi134hib33r257dn177hvf9f") (f (quote (("default" "serde" "bincode"))))))

