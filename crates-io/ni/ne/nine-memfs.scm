(define-module (crates-io ni ne nine-memfs) #:use-module (crates-io))

(define-public crate-nine-memfs-0.2.0 (c (n "nine-memfs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nine") (r "0.4.*") (d #t) (k 0)))) (h "0rx38mq03qvhzsazpsh2n91f0prvzn5fc9nkxa3cp996wnr64ph2")))

