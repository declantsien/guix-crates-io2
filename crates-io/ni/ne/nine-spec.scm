(define-module (crates-io ni ne nine-spec) #:use-module (crates-io))

(define-public crate-nine-spec-0.0.0 (c (n "nine-spec") (v "0.0.0") (d (list (d (n "brev") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "1h0hj3a08ms0h8fzhp8cgbxy092k12csl4363xrci5kq0ymqwbf5")))

