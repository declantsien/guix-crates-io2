(define-module (crates-io ni ne nine) #:use-module (crates-io))

(define-public crate-nine-0.1.0 (c (n "nine") (v "0.1.0") (h "1mhjl75xadm91kl3j5mb3wgxpvnsbd03bi99w5f12dj64pwid19r")))

(define-public crate-nine-0.1.1 (c (n "nine") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "brev") (r "^0.1.4") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "handlebars") (r "^0.18.2") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 1)) (d (n "nom") (r "^1.2.3") (d #t) (k 1)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 1)))) (h "06184c3sr2ib2rk2gl5xidzd03cq23gz7bsmvp4bhffp4mjqnavq")))

(define-public crate-nine-0.1.2 (c (n "nine") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "brev") (r "^0.1.4") (d #t) (k 0)) (d (n "brev") (r "^0.1.4") (d #t) (k 1)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "handlebars") (r "^0.18.2") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 1)) (d (n "nine") (r "^0.1.1") (d #t) (k 1)) (d (n "nom") (r "^1.2.3") (d #t) (k 1)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 1)))) (h "01kjmv6zbdx3v1bbh65ss29hz68wr220zp9wy4lq0cryr4ghkvfk")))

(define-public crate-nine-0.2.0 (c (n "nine") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1wyaljz67xd9ybgn30mgdw347bmihkp1vcj2z30dv745qz4g5c9f")))

(define-public crate-nine-0.2.1 (c (n "nine") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jfzxg3rdrrwvy87v4n14xffzc17j29w18g57q9kjk8bhl6clzhb")))

(define-public crate-nine-0.3.0 (c (n "nine") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0b2ly01zsavx165js11mpf79c8cs5w152aj5ms3vijfr8q0f7mcn")))

(define-public crate-nine-0.4.0 (c (n "nine") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19hmkgkzyf5fsk8phskxxkmhcgqc3jgps4gnwzg5610xv2vr9a9d")))

(define-public crate-nine-0.5.0 (c (n "nine") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1mxzj0crfb3imwryq0mic9bnybcjhkgyy3q6pj0k7xy0idxi6xs2")))

