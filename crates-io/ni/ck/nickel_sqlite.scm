(define-module (crates-io ni ck nickel_sqlite) #:use-module (crates-io))

(define-public crate-nickel_sqlite-0.0.1 (c (n "nickel_sqlite") (v "0.0.1") (d (list (d (n "nickel") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "092qv0f3ld07av4pbgn2zx0cwby11mq9bgddj6aaqn86ikrwq0gi")))

(define-public crate-nickel_sqlite-0.0.2 (c (n "nickel_sqlite") (v "0.0.2") (d (list (d (n "nickel") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "1xx7rx670yj6nxczxzzxij0zq1xl0ivc5raz0a5sfa2ry1wb6i7v")))

(define-public crate-nickel_sqlite-0.0.3 (c (n "nickel_sqlite") (v "0.0.3") (d (list (d (n "nickel") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "r2d2") (r "*") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "0x57z2cgqwk10yqkdbzy0317i3yhlcmkmmzxd8djc8bjj7hsrs03")))

(define-public crate-nickel_sqlite-0.0.4 (c (n "nickel_sqlite") (v "0.0.4") (d (list (d (n "nickel") (r "^0.7") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "~0.0.4") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "019zgz89z80447mjhgdd1kx6qk1qaym51h1xi5n8an7hfyyd3fca")))

(define-public crate-nickel_sqlite-0.2.0 (c (n "nickel_sqlite") (v "0.2.0") (d (list (d (n "nickel") (r "^0.8") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.7") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.0.6") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1dq19q2ys9x9qn6mqy4mj766lvmfn787nfvlw5i1l79j6hflixk5")))

(define-public crate-nickel_sqlite-0.3.0 (c (n "nickel_sqlite") (v "0.3.0") (d (list (d (n "nickel") (r "^0.9") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.1") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.1.0") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "0gyznncnxkp9jsvmkam1h1ay91yaxiwv76zlrxi08d4xn2vgvl85")))

(define-public crate-nickel_sqlite-0.4.0 (c (n "nickel_sqlite") (v "0.4.0") (d (list (d (n "nickel") (r "^0.10") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.5") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1kclj4hm4x52myvsaxmi3x2vm4ps89alra8pv9kyyvnjfg8dmd60")))

