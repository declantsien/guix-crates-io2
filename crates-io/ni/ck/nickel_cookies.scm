(define-module (crates-io ni ck nickel_cookies) #:use-module (crates-io))

(define-public crate-nickel_cookies-0.1.0 (c (n "nickel_cookies") (v "0.1.0") (d (list (d (n "cookie") (r "^0.2") (k 0)) (d (n "hyper") (r "^0.8") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nickel") (r "^0.8") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "06a60sgfqr3p5avfskdc4gfk4r7hz65g64r767z5cmc723zh870w") (f (quote (("secure" "cookie/secure") ("default" "secure"))))))

(define-public crate-nickel_cookies-0.2.0 (c (n "nickel_cookies") (v "0.2.0") (d (list (d (n "cookie") (r "^0.2") (k 0)) (d (n "hyper") (r "^0.9") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nickel") (r "^0.9") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1byfbpv3685hmm0nfq7xm72mr1yhg48iclqlv304zb8095gzby04") (f (quote (("secure" "cookie/secure") ("default" "secure"))))))

