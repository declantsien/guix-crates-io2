(define-module (crates-io ni ck nickel_cors) #:use-module (crates-io))

(define-public crate-nickel_cors-0.1.0 (c (n "nickel_cors") (v "0.1.0") (d (list (d (n "nickel") (r "^0.10") (d #t) (k 0)))) (h "0dspkp0drsddzcsyqayvqr91mqjil995c28w7vap508i25a4i0pk")))

(define-public crate-nickel_cors-0.2.0 (c (n "nickel_cors") (v "0.2.0") (d (list (d (n "nickel") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 2)))) (h "1f87zg3qdwd6d56iqbmkrfa6sf68n58fqkrjk16yv1i6vmxj50yr")))

(define-public crate-nickel_cors-0.2.1 (c (n "nickel_cors") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "nickel") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 2)))) (h "11w3h6wm6ygmybj4y3c3gcilxq5667ldrz3swsgxncsb14ydzark")))

(define-public crate-nickel_cors-0.2.2 (c (n "nickel_cors") (v "0.2.2") (d (list (d (n "nickel") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 2)))) (h "1kywdj92v4lanwz19chzlqppipd3qc0yjs3mxf59q0nlyrncmy39")))

(define-public crate-nickel_cors-0.2.3 (c (n "nickel_cors") (v "0.2.3") (d (list (d (n "nickel") (r "^0.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 2)))) (h "0nyacpjwizmaqrijj71d0fxzr17vr4hrnrlznwvjjlxk4nbcll2i")))

(define-public crate-nickel_cors-0.3.0 (c (n "nickel_cors") (v "0.3.0") (d (list (d (n "nickel") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 2)))) (h "0mkznzqwp8r5lpbziva02n6yqmvc2yaxgmcm22b1iz8qn4gwcbgq")))

(define-public crate-nickel_cors-0.3.1 (c (n "nickel_cors") (v "0.3.1") (d (list (d (n "nickel") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)))) (h "1bkra4sbkyf2p8wlbb045drmsqw948xgh8l7p4lwrxqj87jhkh9r")))

(define-public crate-nickel_cors-0.3.2 (c (n "nickel_cors") (v "0.3.2") (d (list (d (n "nickel") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 2)))) (h "1rjbkka9nnkf9mnwlvbbnk1ncwf6wrzr55amja39xzzsv50hblc2")))

(define-public crate-nickel_cors-0.3.3 (c (n "nickel_cors") (v "0.3.3") (d (list (d (n "nickel") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.15") (d #t) (k 2)))) (h "18ya8g9zwyl1z1yjp05sh8zsa3fbv2wqajrhw0h3i7w87v1ax5dn")))

