(define-module (crates-io ni ck nickel_view) #:use-module (crates-io))

(define-public crate-nickel_view-0.1.0 (c (n "nickel_view") (v "0.1.0") (d (list (d (n "mustache") (r "^0.6") (d #t) (k 0)) (d (n "nickel") (r "^0.9.0") (d #t) (k 0)))) (h "032bjsmc1i7fpgrkazq3abvhmck5abx6jlznibbndf18y65z4jp4")))

