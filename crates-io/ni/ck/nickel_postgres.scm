(define-module (crates-io ni ck nickel_postgres) #:use-module (crates-io))

(define-public crate-nickel_postgres-0.1.0 (c (n "nickel_postgres") (v "0.1.0") (d (list (d (n "nickel") (r "^0.7") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "r2d2") (r "^0.6") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "0kk1byp6fc0z8rnwsd670xl96ci6fgww3pcziny3gi8g1p8hfv75")))

(define-public crate-nickel_postgres-0.2.0 (c (n "nickel_postgres") (v "0.2.0") (d (list (d (n "nickel") (r "^0.8.1") (d #t) (k 0)) (d (n "plugin") (r "^0.2.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)) (d (n "r2d2_postgres") (r "^0.10.1") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "02a4mv3rig3b32vz9drfjzij3czm85q2fzrb2sp0vls65jpqhkl0")))

