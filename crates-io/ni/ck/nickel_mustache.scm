(define-module (crates-io ni ck nickel_mustache) #:use-module (crates-io))

(define-public crate-nickel_mustache-0.1.0 (c (n "nickel_mustache") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 2)) (d (n "mustache") (r "^0.6") (d #t) (k 0)) (d (n "nickel") (r "^0.7.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "156cip7jqh3ic8ksksw1a9ddaan9qlwi0pzh67917sqapbz2bp90")))

