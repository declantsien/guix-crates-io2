(define-module (crates-io ni ck nickelodeon) #:use-module (crates-io))

(define-public crate-nickelodeon-0.0.2 (c (n "nickelodeon") (v "0.0.2") (d (list (d (n "config-finder") (r "^0.1.2") (d #t) (k 0)) (d (n "nickel-lang") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "0488mnk4qfqxgbrd4ayyrlx4lg2d6dhpw62w4vkcbxj10r0821n8")))

(define-public crate-nickelodeon-0.0.3 (c (n "nickelodeon") (v "0.0.3") (d (list (d (n "config-finder") (r "^0.1.2") (d #t) (k 0)) (d (n "nickel-lang") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1sykcbyddvsr1jdkgb2fw5appadpdnn0p89hbhdqzg6ndm82z6zi")))

(define-public crate-nickelodeon-0.0.4 (c (n "nickelodeon") (v "0.0.4") (d (list (d (n "config-finder") (r "^0.1.2") (d #t) (k 0)) (d (n "nickel-lang-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)))) (h "1fjk6z6qp78nrilaiynqbd0man0x1425kdxca0v1rzxpfsgplbv4")))

