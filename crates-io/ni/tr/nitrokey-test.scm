(define-module (crates-io ni tr nitrokey-test) #:use-module (crates-io))

(define-public crate-nitrokey-test-0.1.0 (c (n "nitrokey-test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "00d0f36rqm9qm69j664z5ig9njmkzsskmwi7jy1x66bzmih3c7qz")))

(define-public crate-nitrokey-test-0.1.1 (c (n "nitrokey-test") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1sihng589vhkx87rd9rx0pkp0smzw6q8wa6r2h9h7fqv3niyl9k2")))

(define-public crate-nitrokey-test-0.2.0 (c (n "nitrokey-test") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0pxsz832a3lj75wiqdkf946vii3lz2zd850n8c2zkavv0c29x6al")))

(define-public crate-nitrokey-test-0.2.1 (c (n "nitrokey-test") (v "0.2.1") (d (list (d (n "nitrokey") (r ">= 0.4.0-alpha.0") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0ygkkp09jc1m3gh1cfg3l0w7406iwxsb0sa1sa9xlf2f24jmn6gj")))

(define-public crate-nitrokey-test-0.3.0 (c (n "nitrokey-test") (v "0.3.0") (d (list (d (n "nitrokey") (r ">= 0.4.0-alpha.2") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1dpfnijy1h4qf8pd96d1ax68jplz9dwba40azrd8q1408fwzydja")))

(define-public crate-nitrokey-test-0.3.1 (c (n "nitrokey-test") (v "0.3.1") (d (list (d (n "nitrokey") (r ">= 0.4.0-alpha.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0n82qnqa6h5rzcrazk10rplpxp7jnkdkr9l8kqh6j5xmbnsq2zky")))

(define-public crate-nitrokey-test-0.3.2 (c (n "nitrokey-test") (v "0.3.2") (d (list (d (n "nitrokey") (r ">= 0.4.0-alpha.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzy0lfz2zwn7f1d126avig6risj78a4bvf3zdwjyldaxln0rnpk")))

(define-public crate-nitrokey-test-0.4.0 (c (n "nitrokey-test") (v "0.4.0") (d (list (d (n "nitrokey") (r "^0.7") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpizbg81cllnv1six4nf546qs0frmylqq837w71qjmwy59zha1c")))

(define-public crate-nitrokey-test-0.5.0 (c (n "nitrokey-test") (v "0.5.0") (d (list (d (n "nitrokey") (r "^0.8") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "197j9r2s4ydzbqfydza6v31mgcsgd29jpidz4psqawjdm49f8lg6")))

