(define-module (crates-io ni tr nitrate-macro) #:use-module (crates-io))

(define-public crate-nitrate-macro-0.1.0 (c (n "nitrate-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yl910k4lw3bym70cnciis326ash7jccg5qglawfikgvfv3hfmwc")))

