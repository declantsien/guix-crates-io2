(define-module (crates-io ni tr nitric-lock-internals) #:use-module (crates-io))

(define-public crate-nitric-lock-internals-0.0.0 (c (n "nitric-lock-internals") (v "0.0.0") (d (list (d (n "lock_api") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.0") (d #t) (k 0)))) (h "0cxz08a8acg7il7602hfzvx1aldyj5yg102zicz9b7rfzcziwr38")))

(define-public crate-nitric-lock-internals-0.0.1 (c (n "nitric-lock-internals") (v "0.0.1") (d (list (d (n "lock_api") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.0") (d #t) (k 0)))) (h "0pj12bqnzry6v52vaircc86lf6s6hdqki4vl86yr6w4fn3jkllxl")))

