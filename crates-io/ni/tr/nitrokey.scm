(define-module (crates-io ni tr nitrokey) #:use-module (crates-io))

(define-public crate-nitrokey-0.1.0 (c (n "nitrokey") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0imvsx1b4wlcgjxwcnik7fpc67235rv4sn7fyfzd5q1nkhbkjw40") (f (quote (("test-pro") ("test-no-device") ("default" "test-no-device"))))))

(define-public crate-nitrokey-0.1.1 (c (n "nitrokey") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1jipwzgmqyppm021d5yf79ypwqiljyd6fzlihz788laxvn30jzif") (f (quote (("test-pro") ("test-no-device") ("default" "test-no-device"))))))

(define-public crate-nitrokey-0.2.0 (c (n "nitrokey") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0gamx7ps3nn9jvkxjd9imd6b6iv6r4nw4wdx3xzkp7nngnizbyw4") (f (quote (("test-storage") ("test-pro") ("test-no-device"))))))

(define-public crate-nitrokey-0.2.1 (c (n "nitrokey") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "11yq9hm917f53zc6y9b5g1vdcp60zkdw5xyy7k98l4kfvblj86np") (f (quote (("test-storage") ("test-pro") ("test-no-device"))))))

(define-public crate-nitrokey-0.2.2 (c (n "nitrokey") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1cv05i0qs2i8radgm9mvws0r3aicjfxgm8bqcswf3g78572vr9p8") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.2.3 (c (n "nitrokey") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1wshdy4brax33rlxzzmh81ns2papb6l06lmc9jx3dhl7cbjwdvlz") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.3.0 (c (n "nitrokey") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "13p62q7cf9lcarryix4nqpdljfd8smibpzmlacam4n7vq4di6g5p") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.3.1 (c (n "nitrokey") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0n31s4bjbq74mhyc4dbq5lnilw3ny0nqs23ic268vn9r1yqv0jb4") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.3.2 (c (n "nitrokey") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1m39a0dr26dqhyydj5f04wfhlg84vbf0dk5h37vizb158ci51xw2") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.3.3 (c (n "nitrokey") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "18bv3w4wljyvrdjmcd5hx0l8045v7ly47gl8d1g08nfk6snhbllb") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.3.4 (c (n "nitrokey") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "1j3yapwcnmmk497n4bi9a4ac3d9zlsqn6iqckr6nvpwrm47v5zc1") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.4.0-alpha.0 (c (n "nitrokey") (v "0.4.0-alpha.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "1a83gb210p0r6iq9c9jdw3rqri0g3ryx6i87nqzv78sjdabfwc0c")))

(define-public crate-nitrokey-0.4.0-alpha.1 (c (n "nitrokey") (v "0.4.0-alpha.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "0aqj4wld10a75nba0r583f72zidlzgmzkkgm6hnq96kfzx3i7by7")))

(define-public crate-nitrokey-0.4.0-alpha.2 (c (n "nitrokey") (v "0.4.0-alpha.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "~3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "= 0.2.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "1361jzly1xbr5inq7ny38yv5ji70vi8bbwcm9wlckr3n428rff5k")))

(define-public crate-nitrokey-0.4.0-alpha.3 (c (n "nitrokey") (v "0.4.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "0zz5bfka4p4z4fg7fida1bdqpqrx20wgfjizmh5g5iyi09fh1cl9")))

(define-public crate-nitrokey-0.3.5 (c (n "nitrokey") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "~3.4") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)))) (h "15zpmf8q7hgjw5cd77vjvp0icf41dydxwjwv6vwsfgimwy0v26kq") (f (quote (("test-storage") ("test-pro"))))))

(define-public crate-nitrokey-0.4.0 (c (n "nitrokey") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1ghavbff4gpxzccqwc7b2rwqkbz0bz4x32p9f6qm022kw4b9rwd5")))

(define-public crate-nitrokey-0.5.0 (c (n "nitrokey") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0pv3n9ki29rm4qb274mbn8zdjq9pjvb3dgisds87jvvynccxadgj")))

(define-public crate-nitrokey-0.5.1 (c (n "nitrokey") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1jrrxaxi7n6lwlbcy7gz82jx5jzhx6h43gs1ska6y8llcfcqmv6w")))

(define-public crate-nitrokey-0.5.2 (c (n "nitrokey") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0gnwc2cnhps0zglq278578ivqd8yvms651gbric5a6f4zr1mfjw0")))

(define-public crate-nitrokey-0.6.0 (c (n "nitrokey") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1h3sr30bidsh2zg6nirrmpjs48ws6sd1q4dr9zvmhnck8h8wd1qm")))

(define-public crate-nitrokey-0.7.0 (c (n "nitrokey") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0il7f3ddk64sb9h00spb62bvnn101nwg6rm69avygiq2kcpvrkhg")))

(define-public crate-nitrokey-0.7.1 (c (n "nitrokey") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.5") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0jmwnrln47s2b4kk1c71dd2bdldx0iinzv016mm54ym9g3cm3bb2")))

(define-public crate-nitrokey-0.8.0 (c (n "nitrokey") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.6") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1cdpzgyz47mwqbxc1f6wm7d1jr67ws9kiqr828vzshwi8qgry1a8")))

(define-public crate-nitrokey-0.9.0 (c (n "nitrokey") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nitrokey-sys") (r "^3.6") (d #t) (k 0)) (d (n "nitrokey-test") (r "^0.3") (d #t) (k 2)) (d (n "nitrokey-test-state") (r "^0.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "0c8mj52ckvy1311vwxs6jpw16b2bihp5cc811isb96j9slcjvsyx")))

