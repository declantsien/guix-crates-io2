(define-module (crates-io ni tr nitrogen-lib) #:use-module (crates-io))

(define-public crate-nitrogen-lib-0.2.1 (c (n "nitrogen-lib") (v "0.2.1") (d (list (d (n "aws-config") (r "^0.49.0") (d #t) (k 0)) (d (n "aws-sdk-cloudformation") (r "^0.19.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1wlpssq3rdnbraxrzmvsdgkbdd3xvw23r2brp1nas72zfhywsfb7")))

