(define-module (crates-io ni tr nitrate-program) #:use-module (crates-io))

(define-public crate-nitrate-program-0.1.0 (c (n "nitrate-program") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16") (d #t) (k 0)))) (h "1hqykmi475n8zb8cbk03lv961pp2zrfy6asnx2mdk546nvpfbrkx") (f (quote (("logging"))))))

