(define-module (crates-io ni tr nitroglycerin_derive) #:use-module (crates-io))

(define-public crate-nitroglycerin_derive-0.1.0 (c (n "nitroglycerin_derive") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jhg37gwrs5k5b8ahc98wj49w8knh8hjg8y905hz2hf9whbj5yb1")))

(define-public crate-nitroglycerin_derive-0.2.0 (c (n "nitroglycerin_derive") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04sldk7gqsk4z7rdpfwakjplq408sdhnc4wl95k4vvflbvdgwslz")))

(define-public crate-nitroglycerin_derive-0.2.1 (c (n "nitroglycerin_derive") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0av4v66kk1kan6ykp5qlnsy4qihd7qxyjab1fiv2bbg9cxsbg3jb")))

(define-public crate-nitroglycerin_derive-0.2.2 (c (n "nitroglycerin_derive") (v "0.2.2") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xm0r7w3nwyawi4rky77q3l217k4zlz7bi38bcw176mknw1ixj94")))

(define-public crate-nitroglycerin_derive-0.3.0 (c (n "nitroglycerin_derive") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k15qib9f06sd25g458h6bny71p5hzdgxy7kfivqb99ncddbwn1q")))

(define-public crate-nitroglycerin_derive-0.4.0 (c (n "nitroglycerin_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m3wkm0lfgkwfa0winb3j9nm4l3y66nlih0ind6084h08p1lk0wz")))

(define-public crate-nitroglycerin_derive-0.4.1 (c (n "nitroglycerin_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19cpxp5im28cslcjscfmnxadpgw605qgs6hf8ig5wlbd7j7rm60g")))

(define-public crate-nitroglycerin_derive-0.4.2 (c (n "nitroglycerin_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13n5rh7dm1k4d3xyfhvf1n70gbpi9kh9c2psa4gqqw1ascbsnz2m")))

