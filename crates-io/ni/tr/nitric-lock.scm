(define-module (crates-io ni tr nitric-lock) #:use-module (crates-io))

(define-public crate-nitric-lock-0.0.0 (c (n "nitric-lock") (v "0.0.0") (d (list (d (n "lock_api") (r "^0.1.5") (d #t) (k 0)) (d (n "nitric-lock-internals") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.0") (d #t) (k 0)))) (h "1fjw43q32r0wqlsfpy0kmgn8cxsipj4yqn82nl2z1x2645gks9f8")))

(define-public crate-nitric-lock-0.0.1 (c (n "nitric-lock") (v "0.0.1") (d (list (d (n "lock_api") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.0") (d #t) (k 0)))) (h "0h6dls2qzinp5kxqs7ygvwy7svw9q0jk95kyl5kblxy438cy8ri1")))

