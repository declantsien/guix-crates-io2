(define-module (crates-io ni tr nitride) #:use-module (crates-io))

(define-public crate-nitride-0.0.1 (c (n "nitride") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "1c3isgcy42rn2clh1b8hmff3h34q6f9hrk1g98m0g8svmi5cir2h") (r "1.61.0")))

