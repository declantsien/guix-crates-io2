(define-module (crates-io ni tr nitrous) #:use-module (crates-io))

(define-public crate-nitrous-0.1.1 (c (n "nitrous") (v "0.1.1") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "fst") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "135m73x7ngvj0nipz88didy4jzphydjyjgdz6m29bci11j7mpmji")))

(define-public crate-nitrous-0.1.2 (c (n "nitrous") (v "0.1.2") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "fst") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qp502zc6gsvi40r5dlvwrrry650rv4vxzw3v61d2abfkm546wjy")))

(define-public crate-nitrous-0.1.3 (c (n "nitrous") (v "0.1.3") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1bbll4pfzhm0vn807wqkppmn7z7wwf115rgl3pm0cz93ajll25sq")))

(define-public crate-nitrous-0.1.5 (c (n "nitrous") (v "0.1.5") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jdqvrcp4mpb6431hrmg7wicnc9as7hhid42fhhqzdwim8fx69yn")))

(define-public crate-nitrous-0.1.6 (c (n "nitrous") (v "0.1.6") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0829v5aglday59i9rp1kcapixwz9cwfrhnf53d88n5ydqys6l996")))

(define-public crate-nitrous-0.1.7 (c (n "nitrous") (v "0.1.7") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fp0p7k8rjyni1z49fi0rsyjdjv3gwqxyf1xhb607qbcviqbxcqv")))

(define-public crate-nitrous-0.1.9 (c (n "nitrous") (v "0.1.9") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14r9hnlmblf4y2mwdwkdd1zlrvm6gzy1sv4sy6nx4f91y6vsdlmy")))

(define-public crate-nitrous-0.1.10 (c (n "nitrous") (v "0.1.10") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xvrzmzhknkf36c06x00zw3ssvj5wmhlli0s9h9pqz0pqa8r07m3")))

(define-public crate-nitrous-0.1.11 (c (n "nitrous") (v "0.1.11") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1da31qisjb8rlbmbnmzafrnm6bs7qfz5rp767xbzdzd6rdjx5b7l")))

(define-public crate-nitrous-0.1.13 (c (n "nitrous") (v "0.1.13") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0c69y8cq3fk7b5q9nss3jyzvjrjrvid5wqcsjkv262fapv6v8wk4")))

(define-public crate-nitrous-0.1.14 (c (n "nitrous") (v "0.1.14") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "119a7xd1vkib2yrhifqg22m225h4nsqydxxfnclcghqg400yawnx")))

(define-public crate-nitrous-0.1.15 (c (n "nitrous") (v "0.1.15") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.26") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dzmyw1clbiwijajb3m8yb03yb7aj2ldb3myg6q1m1av4nzjh46n")))

