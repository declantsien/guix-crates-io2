(define-module (crates-io ni tr nitric) #:use-module (crates-io))

(define-public crate-nitric-0.0.0 (c (n "nitric") (v "0.0.0") (h "0xq52vf99cgra51rjizc49ggjim04raw3g7mp85x1s9ra417ka9y")))

(define-public crate-nitric-0.0.1 (c (n "nitric") (v "0.0.1") (d (list (d (n "nitric-lock") (r "^0.0.0") (o #t) (d #t) (k 0)))) (h "13nyzvqa9ziwqpk7r3bmisf3cv3n8zd9c1gal9ffxnngjjbvnv1k") (f (quote (("lock" "nitric-lock"))))))

