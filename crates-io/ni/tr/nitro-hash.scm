(define-module (crates-io ni tr nitro-hash) #:use-module (crates-io))

(define-public crate-nitro-hash-1.0.0 (c (n "nitro-hash") (v "1.0.0") (h "00qd9y6bhfsvqhf86kbr4sx458w9i77y8ff738fwrnp585w3jjyp") (y #t)))

(define-public crate-nitro-hash-1.1.0 (c (n "nitro-hash") (v "1.1.0") (h "1fqq5bdn34jx3qydp0hw4243bxb7cxpsciiqdhfwhf1zx2jkclpa") (y #t)))

(define-public crate-nitro-hash-1.2.0 (c (n "nitro-hash") (v "1.2.0") (h "0biajr6n063qcrkjxb996f3nxrip792xc6s4gfb0i6m05rc73pka")))

(define-public crate-nitro-hash-1.3.0 (c (n "nitro-hash") (v "1.3.0") (h "1a5kw8hgzg02pjr2sgc4s1jikj4gaybcz06k7kl37jhmvmxcc7ca")))

(define-public crate-nitro-hash-1.4.0 (c (n "nitro-hash") (v "1.4.0") (h "16jjkdh6lnjkr5x2w6ir6kkz548yjlnmn6qmw080977f56zi89ks")))

