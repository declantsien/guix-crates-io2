(define-module (crates-io ni tr nitrokey-sys) #:use-module (crates-io))

(define-public crate-nitrokey-sys-0.1.0 (c (n "nitrokey-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l98ac1ky80qm4hq3dcnzhxfz9hn88pyvfsywsa59rc2iac4jc07")))

(define-public crate-nitrokey-sys-3.3.0 (c (n "nitrokey-sys") (v "3.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ifhg717jnzbp9ddz6pkiwsxdm8wzsidflf3qrbbc4pwc3p4drf5")))

(define-public crate-nitrokey-sys-3.4.0 (c (n "nitrokey-sys") (v "3.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0347f72sg6djlx78ys6q6ppg2g8z3bcwabmiiihfwyry4av3dlpr")))

(define-public crate-nitrokey-sys-3.4.1 (c (n "nitrokey-sys") (v "3.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x44hiwz8a7zzrj21i4xj3k1c77f70dq46z3y2ir78201milsy9l")))

(define-public crate-nitrokey-sys-3.4.2 (c (n "nitrokey-sys") (v "3.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kb93p8mzkx144z2lkahvpz1yk8kk2m07r30akyv3c62mdiap4yz") (l "nitrokey")))

(define-public crate-nitrokey-sys-3.4.3 (c (n "nitrokey-sys") (v "3.4.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wlrp6pbp621jqh71v278dkj0z3zx5z7mb3sgqmbd04wikp55bc6") (l "nitrokey")))

(define-public crate-nitrokey-sys-3.5.0 (c (n "nitrokey-sys") (v "3.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jac2aj8pryqfcjnyr8mb9nbjwgj04lfr3sws8br6c1pxcm0biki") (l "nitrokey")))

(define-public crate-nitrokey-sys-3.6.0 (c (n "nitrokey-sys") (v "3.6.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08c3lnb8iqx000jd5rzvrdvv4ihjyw3x3s8q11dw56is0nmjyvws") (f (quote (("default")))) (l "nitrokey")))

(define-public crate-nitrokey-sys-3.7.0 (c (n "nitrokey-sys") (v "3.7.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hcns32sclzf8xbdjg5iqndbn1b66l3j61zbgbl8ds8n6nind16q") (f (quote (("default")))) (l "nitrokey")))

