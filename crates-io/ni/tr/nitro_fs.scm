(define-module (crates-io ni tr nitro_fs) #:use-module (crates-io))

(define-public crate-nitro_fs-0.1.0 (c (n "nitro_fs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1x785p10rhy1ljr72gnds1hcginn0x02j4aw2a0566fdjrs1spzm")))

(define-public crate-nitro_fs-0.1.1 (c (n "nitro_fs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "146xh6pydx57sc6dxfvfwx7n0q06300lr5q2p615bw6p9dsg4wdw")))

(define-public crate-nitro_fs-0.2.0 (c (n "nitro_fs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14xi73jqq74iiygdhksasl8yg7xa7kwz30kvhaaxiai9ca24qbik")))

