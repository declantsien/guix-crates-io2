(define-module (crates-io ni tr nitric-component) #:use-module (crates-io))

(define-public crate-nitric-component-0.1.0 (c (n "nitric-component") (v "0.1.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 0)))) (h "1a5mlbwj22mabsgbnsw8srlkq8n75yx2h3pq55bvi8d9jvqpjhf7")))

