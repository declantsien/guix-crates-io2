(define-module (crates-io ni tr nitrokey-dmenu) #:use-module (crates-io))

(define-public crate-nitrokey-dmenu-0.1.0 (c (n "nitrokey-dmenu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8") (d #t) (k 0)) (d (n "nitrokey") (r "^0.9") (d #t) (k 0)) (d (n "pinentry") (r "^0.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "15c9ym5l25is0gifzxwlm271rqffaziypfhr7vnhai0j1n8h5z6n")))

(define-public crate-nitrokey-dmenu-0.2.0 (c (n "nitrokey-dmenu") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8") (d #t) (k 0)) (d (n "nitrokey") (r "^0.9") (d #t) (k 0)) (d (n "pinentry") (r "^0.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "1dl8s473f08zqqrq5qayqbx16s5n7kxx7jih02nqqizy3vmracdd")))

(define-public crate-nitrokey-dmenu-0.2.1 (c (n "nitrokey-dmenu") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8") (d #t) (k 0)) (d (n "nitrokey") (r "^0.9") (d #t) (k 0)) (d (n "pinentry") (r "^0.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "1nf4jzwa4rbibkif7cjzsig9cri5lly2brp7sc665dr2842al6iq")))

(define-public crate-nitrokey-dmenu-0.2.2 (c (n "nitrokey-dmenu") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "copypasta") (r "^0.10") (d #t) (k 0)) (d (n "nitrokey") (r "^0.9") (d #t) (k 0)) (d (n "pinentry") (r "^0.5") (d #t) (k 0)) (d (n "secrecy") (r "^0.8") (d #t) (k 0)))) (h "177dgphmff2mkn2w5v4z1q5y7wwn71giiqb5pn6g61ppm1zqxc7b")))

