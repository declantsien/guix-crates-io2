(define-module (crates-io ni tr nitrate) #:use-module (crates-io))

(define-public crate-nitrate-0.0.0 (c (n "nitrate") (v "0.0.0") (h "0rrw01d4hffy7adab5rvcih8rm4l1pr332axmn0s2hks19qazijp")))

(define-public crate-nitrate-0.1.0 (c (n "nitrate") (v "0.1.0") (d (list (d (n "nitrate-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "nitrate-program") (r "^0.1.0") (d #t) (k 0)))) (h "018j0055kl08iif730m6wnwvnvmazjfl1xqxpnv6kr3gmll19nfd") (f (quote (("logging" "nitrate-program/logging"))))))

