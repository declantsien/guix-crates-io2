(define-module (crates-io ni gh night-buddy-allocator) #:use-module (crates-io))

(define-public crate-night-buddy-allocator-0.0.1-alpha (c (n "night-buddy-allocator") (v "0.0.1-alpha") (h "14dqfrnhqmd0sc4zb8jh9y42g2dhp1mdwmzvq27l4yg3nfvrpxgl") (f (quote (("no-std") ("no-generic-std-mutex-impl")))) (y #t)))

(define-public crate-night-buddy-allocator-0.0.2-alpha (c (n "night-buddy-allocator") (v "0.0.2-alpha") (h "0ns7766fjp1q04hfdfxa7pj6hndsys1pfs4n9qyc8dr8mm0xkgl3") (f (quote (("no-std") ("no-generic-std-mutex-impl")))) (y #t)))

(define-public crate-night-buddy-allocator-0.0.3-alpha (c (n "night-buddy-allocator") (v "0.0.3-alpha") (h "1wjfkr3i290xh6xdvn10h76s55rf89qxa7kqna9fjwwg6k0qkyyc") (f (quote (("no-std") ("no-generic-std-mutex-impl")))) (y #t)))

