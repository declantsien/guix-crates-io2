(define-module (crates-io ni gh nightly-quirks) #:use-module (crates-io))

(define-public crate-nightly-quirks-0.1.0 (c (n "nightly-quirks") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0j3dxi41ff60axnh35qyq40crygwaq6v7cqrj4v1dmxjichbj53v") (f (quote (("nightly"))))))

(define-public crate-nightly-quirks-0.1.1 (c (n "nightly-quirks") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1qz1xywv67zk6xiwz8v3lm8sqd3lf3f0b96wk5gyk8jvc9ncvaby") (f (quote (("nightly"))))))

(define-public crate-nightly-quirks-0.1.2 (c (n "nightly-quirks") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0995lv7w0akdpsmg8s1b85jm8f1hfn24gppw6zjr4h7i0kwyf8x4") (f (quote (("nightly"))))))

(define-public crate-nightly-quirks-0.1.4 (c (n "nightly-quirks") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "133c0lf608hf5wj7c23iphnc7ghk28377wnh1n26xiy9g294x17s") (f (quote (("nightly"))))))

