(define-module (crates-io ni gh night) #:use-module (crates-io))

(define-public crate-night-0.0.0 (c (n "night") (v "0.0.0") (h "1bg9jxa8qlgx7l1gg7z7i4zf36g3ckai3i61lacszcskzp75p10h") (y #t)))

(define-public crate-night-0.0.1 (c (n "night") (v "0.0.1") (h "03prqbjgfiz42myzmsawm36f5slw2050964ww790vlvhjzsp3mc3")))

(define-public crate-night-0.0.2 (c (n "night") (v "0.0.2") (h "1b975zmykdbqi20ahg3386fpi130k61a1dc9f3lvysxrlpd5371y")))

