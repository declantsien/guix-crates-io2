(define-module (crates-io ni gh nightlight) #:use-module (crates-io))

(define-public crate-nightlight-0.0.5 (c (n "nightlight") (v "0.0.5") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "13pbxkb3b8lnin3zq1b0a8k2bzlpddk81mcgkq6jaxdmn4vcm0mg")))

(define-public crate-nightlight-0.0.6 (c (n "nightlight") (v "0.0.6") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "13c84agdvlyivnb3x1v03qbyh3i4vpfpj1lv7806p2zvb7gq2d14")))

(define-public crate-nightlight-0.0.7 (c (n "nightlight") (v "0.0.7") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "1nw2hcw0x9d27kn3advhgsc5gxjyic8v3lpyg9s9djyfd8nbd2xy")))

(define-public crate-nightlight-0.1.0 (c (n "nightlight") (v "0.1.0") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "11b1m078nlkrn7lsh4dr52g8qk4pzzw925r1i4slv5hbshlx19dp")))

(define-public crate-nightlight-0.1.1 (c (n "nightlight") (v "0.1.1") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "0g35syh3qz5r2pv5fhwb1myi4726d43isjaydj9qnqn0hqn53wpy")))

(define-public crate-nightlight-0.2.0 (c (n "nightlight") (v "0.2.0") (d (list (d (n "gio") (r "^0.8.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "1b4bx5l7j9a2r5ava6g1jj0k8wlljn2wdyk052dbcz9hg4aqi9s6")))

