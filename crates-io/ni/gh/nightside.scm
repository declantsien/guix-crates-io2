(define-module (crates-io ni gh nightside) #:use-module (crates-io))

(define-public crate-nightside-0.0.0 (c (n "nightside") (v "0.0.0") (h "1rs6dv7q8gdksl57xmsnhw3cd7zlk93sy44k7sn59aws8l85ff3y")))

(define-public crate-nightside-0.0.1 (c (n "nightside") (v "0.0.1") (h "1d6i00fnwlcha85pm48cvh8j4isissi4d9ladgnng7ra34i6v3k9")))

