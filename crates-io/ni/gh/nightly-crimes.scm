(define-module (crates-io ni gh nightly-crimes) #:use-module (crates-io))

(define-public crate-nightly-crimes-1.0.0 (c (n "nightly-crimes") (v "1.0.0") (d (list (d (n "yolo-rustc-bootstrap") (r "^1.0.0") (d #t) (k 0)))) (h "1l3s1bsgc4agwwpskq0056bfdwcxdrjr6x6ffdnzs6j6j3sls062")))

(define-public crate-nightly-crimes-1.0.1 (c (n "nightly-crimes") (v "1.0.1") (d (list (d (n "yolo-rustc-bootstrap") (r "^1.0.0") (d #t) (k 0)))) (h "1rvxvax9bxzm4b0vw2im1cm5x15wmrb6hsxvjc4k6ngaxi8519k4")))

(define-public crate-nightly-crimes-1.0.2 (c (n "nightly-crimes") (v "1.0.2") (d (list (d (n "yolo-rustc-bootstrap") (r "^1.0.0") (d #t) (k 0)))) (h "1qyjwjyifm697vc1v1qx4qnm206pvimq4zzmavavaxllrybbpdq5")))

