(define-module (crates-io ni gh nightshift) #:use-module (crates-io))

(define-public crate-nightshift-0.0.1 (c (n "nightshift") (v "0.0.1") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "1ringr266l71fb54dsfd5a6gjy1fp46m1ffab37k7hs06mdbjfd2") (y #t)))

(define-public crate-nightshift-0.0.2 (c (n "nightshift") (v "0.0.2") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "0df5gg61kr9l6rkgzy90s19iharzz4rfyxv6d0v88v4hcnfjqhd0") (y #t)))

(define-public crate-nightshift-0.0.3 (c (n "nightshift") (v "0.0.3") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "0czgx6av6r0jhjszw8qvfq8bd15r580fqbgm96vcdkha9rqikb2p") (y #t)))

(define-public crate-nightshift-0.0.4 (c (n "nightshift") (v "0.0.4") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.2.10") (d #t) (k 0)))) (h "1fy1lzggr2dav975h4br1af75cwjng7qd52fipi4cyfivdmbs4ka") (y #t)))

(define-public crate-nightshift-0.0.5-pre (c (n "nightshift") (v "0.0.5-pre") (d (list (d (n "nightlight") (r "^0.0.5") (d #t) (k 0)))) (h "0isv4wvrqkdliaxni8rak619pdcqfq7gxb8cjfjg5wzvlrmy9h2i")))

