(define-module (crates-io ni -f ni-fpga-macros) #:use-module (crates-io))

(define-public crate-ni-fpga-macros-0.1.0 (c (n "ni-fpga-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1gcarjl46i043d9vf27v1f56s5qb957q22v6b2kzg7djdr9jm6cf")))

(define-public crate-ni-fpga-macros-1.0.0 (c (n "ni-fpga-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1k9fwrdybmv16xkrz64qcdf4x2lsbcwfcy9342lnny8rii3lwzlc")))

(define-public crate-ni-fpga-macros-1.0.1 (c (n "ni-fpga-macros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1divm8gpj7m56jgg2jg0vsz77n58qs98m4bw55pkb1bwmdkbagdn")))

