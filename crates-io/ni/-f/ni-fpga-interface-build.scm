(define-module (crates-io ni -f ni-fpga-interface-build) #:use-module (crates-io))

(define-public crate-ni-fpga-interface-build-0.1.0 (c (n "ni-fpga-interface-build") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lang-c") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit" "parsing"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v0gg2qjszz6fnbhg6nyl7yqn9bmkindfj9kc1f8dkfb9c706awd")))

