(define-module (crates-io ni -f ni-fpga-sys) #:use-module (crates-io))

(define-public crate-ni-fpga-sys-0.1.0 (c (n "ni-fpga-sys") (v "0.1.0") (h "1c2dv2z81s7d751xi3b1diqk5m8xdwxcxwlz0h3njhjgdiy93796") (l "NiFpga")))

(define-public crate-ni-fpga-sys-1.0.0 (c (n "ni-fpga-sys") (v "1.0.0") (h "1vzkzq9j00hpkyf01y0sv5ibl27a6x5pjdh57hyim2waxfs60z13") (l "NiFpga")))

(define-public crate-ni-fpga-sys-1.0.1 (c (n "ni-fpga-sys") (v "1.0.1") (h "0l80gzdrm9jwqx6gcgpd4wai3fgfqi8c3m5v5kmpaijq3f3fq8py") (l "NiFpga")))

