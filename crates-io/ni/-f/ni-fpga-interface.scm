(define-module (crates-io ni -f ni-fpga-interface) #:use-module (crates-io))

(define-public crate-ni-fpga-interface-0.1.0 (c (n "ni-fpga-interface") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0mxmh472a2nhywhkcdiyxkyjqw08plzrca1y7xsk9l9bjw4yjgb4")))

