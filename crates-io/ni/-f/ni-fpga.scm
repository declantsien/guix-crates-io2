(define-module (crates-io ni -f ni-fpga) #:use-module (crates-io))

(define-public crate-ni-fpga-0.1.0 (c (n "ni-fpga") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "17r2vffxhlgwqwdzybs35zbybdqfpa6w25iyhmb4w85rljwapgm9")))

(define-public crate-ni-fpga-1.0.0 (c (n "ni-fpga") (v "1.0.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1r7ygdzxl7w0m5s0459c0hla8hwzr2gmirpgl7yhxpsxgi0csyky")))

(define-public crate-ni-fpga-1.1.0 (c (n "ni-fpga") (v "1.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "13qxzaw0vgcq5y0mq4v4g722papkg84fbbxkxndplizxa5n912if")))

(define-public crate-ni-fpga-1.2.0 (c (n "ni-fpga") (v "1.2.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1xwwda4mgl1v8qcafpd77i64ccxn9xvc97dpm9c40h4n7hvg9zss")))

(define-public crate-ni-fpga-1.3.0 (c (n "ni-fpga") (v "1.3.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "01dm8ac46nw1kkhkgkdwb9gc3zaww9r5ajdm11rkw2z7f3jk2qip")))

(define-public crate-ni-fpga-1.4.0 (c (n "ni-fpga") (v "1.4.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1y1rizwvbsmxs23nrl92ksnhjnqgjp5i2l25q4658fi51xa7hpfy")))

(define-public crate-ni-fpga-1.4.1 (c (n "ni-fpga") (v "1.4.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "ni-fpga-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "08bwry8g6mlfnwm5nyn01x8zz2pkx4mcwgyafgpggag0jj06pczb")))

