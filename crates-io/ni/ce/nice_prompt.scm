(define-module (crates-io ni ce nice_prompt) #:use-module (crates-io))

(define-public crate-nice_prompt-0.1.0 (c (n "nice_prompt") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)))) (h "01rrs35mshsmkkwnh6j035zrj3f057rihsgnjlcwjvkzlvlvw6zh")))

(define-public crate-nice_prompt-0.1.1 (c (n "nice_prompt") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)))) (h "1dv5ik7nn56d226a50nsgj20cym75arhxb4vv2zgfbkfjwp4gsdy")))

