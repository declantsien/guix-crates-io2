(define-module (crates-io ni ce nicelocal-ext-php-rs-derive) #:use-module (crates-io))

(define-public crate-nicelocal-ext-php-rs-derive-0.10.1 (c (n "nicelocal-ext-php-rs-derive") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ident_case") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "19x9jfbd2ych2gf6yi9x0w97clbm8hsnjvlfn285mixl94266q54") (y #t)))

