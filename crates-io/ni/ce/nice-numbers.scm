(define-module (crates-io ni ce nice-numbers) #:use-module (crates-io))

(define-public crate-nice-numbers-1.0.0 (c (n "nice-numbers") (v "1.0.0") (h "1m10rsbjhv9z5hz1r16nmkaah8zlqa4jzrw2gzaiglsmx36dm9vs")))

(define-public crate-nice-numbers-1.0.1 (c (n "nice-numbers") (v "1.0.1") (h "0xs41l63g3lx8fhh13kzix2yah072lcg356a1zxz49wqrr7fxsr6")))

(define-public crate-nice-numbers-1.0.2 (c (n "nice-numbers") (v "1.0.2") (h "0xv7q725vx4fglz0pf6f50q9ahd9zcxr4y73vmywcrcj1lb5r1sm")))

(define-public crate-nice-numbers-1.0.3 (c (n "nice-numbers") (v "1.0.3") (h "1hrvn2pkinliwajdbdb36jq06nxpym8xm4i5af55fkvqzky9l8qk")))

