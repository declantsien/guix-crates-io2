(define-module (crates-io ni ce niced) #:use-module (crates-io))

(define-public crate-niced-0.2.0 (c (n "niced") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "procfs") (r "^0.8") (d #t) (k 0)))) (h "1skp4rvn8j7d9q7237iqvfd54s0k4v5nh8mzw4ncbcb6197h7lkm")))

(define-public crate-niced-0.2.1 (c (n "niced") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "procfs") (r "^0.8") (d #t) (k 0)))) (h "0c6j9ih0nsjm898iy1hgs8zr33320jkzr5sm620rfnpfmiv6iydk")))

