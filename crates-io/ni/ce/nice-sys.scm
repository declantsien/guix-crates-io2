(define-module (crates-io ni ce nice-sys) #:use-module (crates-io))

(define-public crate-nice-sys-0.1.0 (c (n "nice-sys") (v "0.1.0") (d (list (d (n "gio-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "glib-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "gobject-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3") (d #t) (k 1)))) (h "0fj963kk4dlqcc8fw8792xk5wlk5lrbpvd5n36kjh54zlwcwq87d")))

