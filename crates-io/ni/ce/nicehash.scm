(define-module (crates-io ni ce nicehash) #:use-module (crates-io))

(define-public crate-nicehash-0.1.0 (c (n "nicehash") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1qpml2grz3lsv5djzzkvk6llj7s9m84nmynvaajczgc79v1qqh33")))

(define-public crate-nicehash-0.1.1 (c (n "nicehash") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0yfz9pcxmrm4c578y53qx5837qv5m2dw0rzys8657qljw602l97g")))

(define-public crate-nicehash-0.1.2 (c (n "nicehash") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "01la9kfil25w7apxiqyg2gzi5r1jpdgrqc0g17ppy5cjdhhbkwfi")))

(define-public crate-nicehash-0.1.4 (c (n "nicehash") (v "0.1.4") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "semver") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0wkjgl4v0dj1c4i3sssqj5bz41fl4njb5njjdg46r2fkagn326s7")))

(define-public crate-nicehash-0.2.0 (c (n "nicehash") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "semver") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0czxggg7ffj38nr4kp0108xpa75k9jl7kwbpkwcgb1605z950825")))

