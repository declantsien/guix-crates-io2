(define-module (crates-io ni ce niceware) #:use-module (crates-io))

(define-public crate-niceware-0.4.0 (c (n "niceware") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mpm97j63pnvmzmk165asijny26k4wi6rckh8s1fsr33zr1v1xcd")))

(define-public crate-niceware-0.5.0 (c (n "niceware") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1za2dmqn0dikkg6k3fz1kcwrf19mxlfz9kwmwn09739j0ahv55ar")))

(define-public crate-niceware-0.6.0 (c (n "niceware") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ym1b6bq4mpnbypi84fpifqnp4dp4l6rdh385yfvzc9r0lpyzif6")))

(define-public crate-niceware-1.0.0 (c (n "niceware") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ggw2xcss4mkk37wazyvhww7zavvgwf4s7pm22zn13s38byqwm5z")))

