(define-module (crates-io ni as nias) #:use-module (crates-io))

(define-public crate-nias-0.1.0 (c (n "nias") (v "0.1.0") (h "10r0nskd0i949sx53iycry07iqxqs0qdy4hc6zyvn1m57s69ia70")))

(define-public crate-nias-0.3.0 (c (n "nias") (v "0.3.0") (d (list (d (n "rusty-hook") (r "^0.9.1") (d #t) (k 2)))) (h "1jr1iy52vg05ysih6xga8a64ks7xps3ndjp9cazh9d92zmfr3gpm")))

(define-public crate-nias-0.3.1 (c (n "nias") (v "0.3.1") (d (list (d (n "rusty-hook") (r "^0.9.1") (d #t) (k 2)))) (h "14r4mxhcmn4686npnhian3qh7hrqzxrwjdgn8fk0ghg7dcl8jnnn")))

(define-public crate-nias-0.4.0 (c (n "nias") (v "0.4.0") (d (list (d (n "rusty-hook") (r "^0.9.1") (d #t) (k 2)))) (h "0xk30isvyh6p3n0mim92pc8ypypb2c97dpb19l78ss9jq3sh2vzp")))

(define-public crate-nias-0.5.0 (c (n "nias") (v "0.5.0") (d (list (d (n "rusty-hook") (r "^0.10.0") (d #t) (k 2)))) (h "1w0jrshjqr1dxd01kg12f1ic067hvwwqc9jxbf0m063gr11089db")))

(define-public crate-nias-0.7.0 (c (n "nias") (v "0.7.0") (d (list (d (n "rusty-hook") (r "^0.11.0") (d #t) (k 2)))) (h "08milmdr9z3jxzccgjfmx7ldjf8vplb8d8qspp47lm35c8wzsd81")))

