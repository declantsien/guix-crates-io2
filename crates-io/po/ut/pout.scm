(define-module (crates-io po ut pout) #:use-module (crates-io))

(define-public crate-pout-0.0.1 (c (n "pout") (v "0.0.1") (d (list (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1pakgjmhbyp9lv73s7g91wigxpbqlk0imqf1732qhdqjjis3grq7") (y #t)))

(define-public crate-pout-0.0.2 (c (n "pout") (v "0.0.2") (d (list (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (f (quote ("paw"))) (d #t) (k 0)))) (h "0n39mrhccp91m884bxldh8ix5adykzjcbfj56ms81w7fhyhdbs3n") (y #t)))

