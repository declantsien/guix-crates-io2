(define-module (crates-io po em poem-openapi-response) #:use-module (crates-io))

(define-public crate-poem-openapi-response-0.2.0 (c (n "poem-openapi-response") (v "0.2.0") (d (list (d (n "poem") (r "^1.3.30") (k 0)) (d (n "poem-openapi") (r "^2.0.0") (k 0)))) (h "13zz90n9n6az8ana1xnhxx4rqq4pm1sj595s51b4a0h7cfam02ih") (f (quote (("default" "400" "401" "404" "405" "415") ("416") ("415") ("413") ("412") ("405") ("404") ("403") ("401") ("400"))))))

(define-public crate-poem-openapi-response-0.3.0 (c (n "poem-openapi-response") (v "0.3.0") (d (list (d (n "poem") (r "^1.3.31") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.1") (d #t) (k 0)))) (h "1pn6la8xc9zhk5nl33vp7hhvq7rx2d209vnggn1lwzb181sw0ysm")))

(define-public crate-poem-openapi-response-0.4.0 (c (n "poem-openapi-response") (v "0.4.0") (d (list (d (n "poem") (r "^1.3.32") (o #t) (k 0)) (d (n "poem-openapi") (r "^2.0.2") (o #t) (k 0)) (d (n "poem-openapi-macro") (r "^0.2.0") (f (quote ("response" "one_response" "uni_response"))) (o #t) (k 0)))) (h "164wvgvlbn45ar7s9da8p0vag9wa9w1hp6a5qd2r6x3xyp3wdg9c") (s 2) (e (quote (("default" "dep:poem" "dep:poem-openapi" "dep:poem-openapi-macro"))))))

(define-public crate-poem-openapi-response-0.5.0 (c (n "poem-openapi-response") (v "0.5.0") (d (list (d (n "poem") (r "^1.3.35") (o #t) (k 0)) (d (n "poem-openapi") (r "^2.0.5") (o #t) (k 0)) (d (n "poem-openapi-macro") (r "^0.2.0") (f (quote ("response" "one_response" "uni_response"))) (o #t) (k 0)))) (h "04c03c2p2vn00i901mz1dk1r9prg2s289d9h4zdbc051p25kc98h") (s 2) (e (quote (("default" "dep:poem" "dep:poem-openapi" "dep:poem-openapi-macro"))))))

