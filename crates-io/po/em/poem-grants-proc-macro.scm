(define-module (crates-io po em poem-grants-proc-macro) #:use-module (crates-io))

(define-public crate-poem-grants-proc-macro-1.0.0-beta.1 (c (n "poem-grants-proc-macro") (v "1.0.0-beta.1") (d (list (d (n "poem") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0w4whb2dhic3gi3d5z67aghahwd1sc1dq0i55sqc62xz27sk2cxf")))

(define-public crate-poem-grants-proc-macro-1.0.0-beta.2 (c (n "poem-grants-proc-macro") (v "1.0.0-beta.2") (d (list (d (n "poem") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "172hcd1agil0v1a7qy32awscymc91ss6s06pyxxivgf05ppm5gl7")))

(define-public crate-poem-grants-proc-macro-1.0.0-beta.3 (c (n "poem-grants-proc-macro") (v "1.0.0-beta.3") (d (list (d (n "poem") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0cyy6y9hfgh6j3i0f2s4j5kci554f5qbykz50d40h94vs1d0zib6")))

