(define-module (crates-io po em poem-extensions) #:use-module (crates-io))

(define-public crate-poem-extensions-0.1.0 (c (n "poem-extensions") (v "0.1.0") (d (list (d (n "poem") (r "^1.3.35") (k 0)) (d (n "poem-extensions-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.5") (k 0)))) (h "1d5kj3bzihr2bghq24dk576zqksr6qj90f9c791ndkl5gc6m8xk6")))

(define-public crate-poem-extensions-0.2.0 (c (n "poem-extensions") (v "0.2.0") (d (list (d (n "poem") (r "^1.3.37") (k 0)) (d (n "poem-extensions-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.7") (k 0)))) (h "1fw1ygw2bj8l0isclbcaknzkf8vs7vm545k09q1mm2fj9a097bad")))

(define-public crate-poem-extensions-0.3.0 (c (n "poem-extensions") (v "0.3.0") (d (list (d (n "poem") (r "^1.3.45") (k 0)) (d (n "poem-extensions-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.16") (k 0)))) (h "06x3sbci8mc3h6qcjsybc7kv2ymv2bd089z8s6fwa2f5cfqyknv9")))

(define-public crate-poem-extensions-0.4.0 (c (n "poem-extensions") (v "0.4.0") (d (list (d (n "poem") (r "^1.3.49") (k 0)) (d (n "poem-extensions-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.20") (k 0)))) (h "12pm1cx6rl9higcbfjp8ws6dch5py5j7abgkh50a5mpzhjrdrm43")))

(define-public crate-poem-extensions-0.5.0 (c (n "poem-extensions") (v "0.5.0") (d (list (d (n "poem") (r "^1.3.51") (k 0)) (d (n "poem") (r "^1.3.51") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.22") (k 0)) (d (n "serde_json") (r "^1.0.91") (k 2)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0aj98flvqnw0qh0vc1asapl9yi7mh0yvaif4n6yw193lm2ak04ws")))

(define-public crate-poem-extensions-0.6.0 (c (n "poem-extensions") (v "0.6.0") (d (list (d (n "poem") (r "^1.3.54") (k 0)) (d (n "poem") (r "^1.3.54") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2.0.25") (k 0)) (d (n "serde_json") (r "^1.0.91") (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0bl4svn4d2cbahj5dnwbxlvfa8fq6vyi45mc70f1jk8g4038xsfs")))

(define-public crate-poem-extensions-0.7.0 (c (n "poem-extensions") (v "0.7.0") (d (list (d (n "poem") (r "^1") (k 0)) (d (n "poem") (r "^1") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "poem-openapi") (r "^2") (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1hizxxsm3nzv3pkz5f80sxi2jhgybr5s5fsbmha25afgr960vj8i")))

(define-public crate-poem-extensions-0.7.1 (c (n "poem-extensions") (v "0.7.1") (d (list (d (n "poem") (r "^1") (k 0)) (d (n "poem") (r "^1") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "poem-openapi") (r "^2") (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0xv3b7n15ksa0wkmj7wsk88r61067gramimc5kgzaa3h7jhf093w")))

(define-public crate-poem-extensions-0.7.2 (c (n "poem-extensions") (v "0.7.2") (d (list (d (n "poem") (r "^1") (k 0)) (d (n "poem") (r "^1") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.7.2") (k 0)) (d (n "poem-openapi") (r "^3") (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0pn3zscwicb32ww1qpvwq4im96k2dpyxc3x24k3qgcy1fav4kflp")))

(define-public crate-poem-extensions-0.8.0 (c (n "poem-extensions") (v "0.8.0") (d (list (d (n "poem") (r "^2") (k 0)) (d (n "poem") (r "^2") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.8.0") (k 0)) (d (n "poem-openapi") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "1jrgsfkjssfxd801b0911r51gca8i6bf1hgrwml1rqck9pcj7dqw")))

(define-public crate-poem-extensions-0.9.0 (c (n "poem-extensions") (v "0.9.0") (d (list (d (n "poem") (r "^3") (k 0)) (d (n "poem") (r "^3") (f (quote ("test"))) (k 2)) (d (n "poem-extensions-macro") (r "^0.9.0") (k 0)) (d (n "poem-openapi") (r "^5") (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "194j6cfnzcw9hv9baijqa32z1cl3nhqprrblqq7za4bacnc3slqr")))

