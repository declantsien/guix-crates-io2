(define-module (crates-io po em poem-reverse-proxy) #:use-module (crates-io))

(define-public crate-poem-reverse-proxy-0.0.0 (c (n "poem-reverse-proxy") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "poem") (r "^3") (f (quote ("websocket"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "1j7qi36s90n5f8h4m6jcizbd366m1rd0jyss4ignxyck7rlcsjph") (r "1.65.0")))

