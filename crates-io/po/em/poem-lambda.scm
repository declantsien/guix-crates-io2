(define-module (crates-io po em poem-lambda) #:use-module (crates-io))

(define-public crate-poem-lambda-0.7.2 (c (n "poem-lambda") (v "0.7.2") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1fv1idkvn9h6m9i44ffa2af4xpca1ma9p5xl5f5s5ahxn8sjh35p")))

(define-public crate-poem-lambda-0.7.3 (c (n "poem-lambda") (v "0.7.3") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "01lkck1s9m3qh7n5ml6wjv2k9dzazbqfxv4z8cargddvw9q036yw")))

(define-public crate-poem-lambda-0.7.4 (c (n "poem-lambda") (v "0.7.4") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0b6p7dwmmsb06vql7x65fx8pygrjjp0iyy81kxww0jwzlbi6bayw")))

(define-public crate-poem-lambda-0.7.6 (c (n "poem-lambda") (v "0.7.6") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1v609lniav1r2cbcci12ds04n1a7zcjb967z9vwlgyjxih1zc8g6")))

(define-public crate-poem-lambda-0.7.7 (c (n "poem-lambda") (v "0.7.7") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "=0.7.7") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "159p2z99ll4g4wrm8vw857aw953wfar1g82hiby7z501j8mwj3lv")))

(define-public crate-poem-lambda-0.7.8 (c (n "poem-lambda") (v "0.7.8") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "=0.7.8") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0l75ahr64v5s9piimqmskk4dszr2za8gv88j89lckdxf6i8kawrf")))

(define-public crate-poem-lambda-0.8.0 (c (n "poem-lambda") (v "0.8.0") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "12jxxs0xbhf3b0369sa3zklyf84wq9dl97sn84yfrw6k7m6hjzwn")))

(define-public crate-poem-lambda-0.8.1 (c (n "poem-lambda") (v "0.8.1") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1wil52y93p7lhh94kwdqrm2367v8np9z9z1rda9pijh4nnx5imsi")))

(define-public crate-poem-lambda-0.8.2 (c (n "poem-lambda") (v "0.8.2") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0li7ifj3pz074izqcbby8k4id02caqv2aqg42y8d9kq0sh4fjbgp")))

(define-public crate-poem-lambda-0.9.0 (c (n "poem-lambda") (v "0.9.0") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0v906py8ilkfv1527lracw3bz6fmp8j41g2n21iqvsphbx06b7dl")))

(define-public crate-poem-lambda-1.0.0 (c (n "poem-lambda") (v "1.0.0") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0y24hvqmga2di8y04bn8lyqgn1w7mifr5s5rpgkfa2yv3dm8s2w0")))

(define-public crate-poem-lambda-1.0.1 (c (n "poem-lambda") (v "1.0.1") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "19gd26db4631bligrgfsy2i8g27jgjvk82ahlpmfnbs4byd00wl4")))

(define-public crate-poem-lambda-1.0.3 (c (n "poem-lambda") (v "1.0.3") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vcvymzmnqh2azi48xsc1s4xnw9615fg25cm5mq78h32hyx0fh5k")))

(define-public crate-poem-lambda-1.0.12 (c (n "poem-lambda") (v "1.0.12") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "05w2kq7m2fj3jk0bsr40083ax8jmfxia7vf1iqb093y0mminxn9v")))

(define-public crate-poem-lambda-1.0.13 (c (n "poem-lambda") (v "1.0.13") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "=1.0.13") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "153brd2smd275a282l0dxlvs1c5aw8qwksdgikapqw0xmxj73bln")))

(define-public crate-poem-lambda-1.0.14 (c (n "poem-lambda") (v "1.0.14") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0wg8i9izd6fykdgadwady8dpppv6xdm4hz9wq8xdpn6kzi4qykvg")))

(define-public crate-poem-lambda-1.0.18 (c (n "poem-lambda") (v "1.0.18") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0wx9sqc86p535879y90qmj5ip1wpqlf5245vv24k4mczmxjjdvkm")))

(define-public crate-poem-lambda-1.0.19 (c (n "poem-lambda") (v "1.0.19") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "03miildad4ph34hzshzdryxd9wi5a5acpzygyyzk8gh83k6lffjb")))

(define-public crate-poem-lambda-1.0.20 (c (n "poem-lambda") (v "1.0.20") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "05y7b5dyxmns7alva21lr8r9zr9s6qghi85shswvh8gxbqkbpfrc")))

(define-public crate-poem-lambda-1.1.0 (c (n "poem-lambda") (v "1.1.0") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0js9fr9xf7qf3sk3raq974f42y1l62zzac13x16pb35ycszl8l0l")))

(define-public crate-poem-lambda-1.1.1 (c (n "poem-lambda") (v "1.1.1") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0kl86wd8fc1pzcqxjix9rjp6zlk547f7yg6rrlj89adfk7i7lkda")))

(define-public crate-poem-lambda-1.2.1 (c (n "poem-lambda") (v "1.2.1") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0kz2qmig66p9a5b7awb5vxcp79x3a8r66lwz5rarnpmxb1av9zqj")))

(define-public crate-poem-lambda-1.2.2 (c (n "poem-lambda") (v "1.2.2") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1mqrggr3i9y90bmhpxgjq4s2wjy2vhr4xbs2p5acgagfc7i19f14")))

(define-public crate-poem-lambda-1.2.3 (c (n "poem-lambda") (v "1.2.3") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vq3ypbkzvny1bikl1xqpnxipp3n43a4x9h6l74szxxw5ll1vd2z")))

(define-public crate-poem-lambda-1.2.4 (c (n "poem-lambda") (v "1.2.4") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "17q9lkby5hyxlhg9lq1cvc5y6fyssxb8l28m0cc2011fd9pacj31")))

(define-public crate-poem-lambda-1.2.5 (c (n "poem-lambda") (v "1.2.5") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0m6rs4sdx02fqymi57zq4y40vrraqxjb9mm8df8jrgp3z7qc1nh5")))

(define-public crate-poem-lambda-1.2.6 (c (n "poem-lambda") (v "1.2.6") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1xfkaxj0bsqnxqw4d03fasm9196ima9x1ycxf1znnipc27grfqc0")))

(define-public crate-poem-lambda-1.2.7 (c (n "poem-lambda") (v "1.2.7") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1w3da0vjhc8ba5yh9r7kwiv63bh9rp9w4p1bzp1lq907cb0khy0c")))

(define-public crate-poem-lambda-1.2.8 (c (n "poem-lambda") (v "1.2.8") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.8") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0hlzzix5ag0d92ip2fh8lff53yfr16ja9mgmk1gb3d5fi6njpggq")))

(define-public crate-poem-lambda-1.2.9 (c (n "poem-lambda") (v "1.2.9") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1b77x81si92i1s4mk6gmdbpjgm86dwmm1w4flmjn5bw6xni7bscd")))

(define-public crate-poem-lambda-1.2.10 (c (n "poem-lambda") (v "1.2.10") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.10") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0f8hjkr5nbj50slxf3xw28nvjikwhznyy821fz4k3sn1wirqgwmx")))

(define-public crate-poem-lambda-1.2.11 (c (n "poem-lambda") (v "1.2.11") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.11") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0rfk4h9hibhdjfka2q3y9fmpb99zx4qhdff40db69304r8fndph0")))

(define-public crate-poem-lambda-1.2.12 (c (n "poem-lambda") (v "1.2.12") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.12") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1w1bf75m2ssnncqymmgbqdy53zyldkd5sy5zfp1m87wyr7yzy59h")))

(define-public crate-poem-lambda-1.2.13 (c (n "poem-lambda") (v "1.2.13") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "10y4kljkdfhw6l46c6prkamx7hzxjd7hmrb3zfw89igsywxlirfj")))

(define-public crate-poem-lambda-1.2.14 (c (n "poem-lambda") (v "1.2.14") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "14qd6gl602xq6rqw0jkbg3q0rc03i1dn4iqr2m9kzbi5rf85c70l")))

(define-public crate-poem-lambda-1.2.15 (c (n "poem-lambda") (v "1.2.15") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.15") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0hpd08bbhc4ksgnnzr8ldyif6jb3dgb98j8pm21q0lqi5525jd7q")))

(define-public crate-poem-lambda-1.2.16 (c (n "poem-lambda") (v "1.2.16") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.16") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0vw0v42yk1jf9gpd45r3xccdd0r26cbq37az5jmsm3bawwwgb3xh")))

(define-public crate-poem-lambda-1.2.17 (c (n "poem-lambda") (v "1.2.17") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.17") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0vi2n8bg23g4xp1d237jam8a5crgicknd9w7pa4fkr1g7gw8cgk4")))

(define-public crate-poem-lambda-1.2.18 (c (n "poem-lambda") (v "1.2.18") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.18") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "105zv7h9m3j1bp4b0qd1difd155nlh9k4nfqj2v39hrp4rqbz6i7")))

(define-public crate-poem-lambda-1.2.19 (c (n "poem-lambda") (v "1.2.19") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.19") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1rp8ihapymmbww8f2297cwm6czrxn8dq9kgxxy984gmca4g5pvbq")))

(define-public crate-poem-lambda-1.2.20 (c (n "poem-lambda") (v "1.2.20") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.20") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0210q0mmy244flwmdg86gbqj6g5kkpsq3vmcp4cw1wnbshp99psd")))

(define-public crate-poem-lambda-1.2.21 (c (n "poem-lambda") (v "1.2.21") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.21") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0492wmdy0mlb8brj0aaa7za9mjiqkjvvjv7nfdq1dq2s7cy3jbi9")))

(define-public crate-poem-lambda-1.2.22 (c (n "poem-lambda") (v "1.2.22") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.22") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0a8f787ij5vvi3im40svka95qrn736zjbk9vr5sipdlbjwah6k41")))

(define-public crate-poem-lambda-1.2.23 (c (n "poem-lambda") (v "1.2.23") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.23") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qqhqp9pbfa7v1s20sx78dbjzr5sidxc7l9d52hngi9wvxxswl3y")))

(define-public crate-poem-lambda-1.2.24 (c (n "poem-lambda") (v "1.2.24") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.24") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vrx87h47pw5mkvgphfpyc3nnp28z3dhl8j2aqnhkxbharpvlizj")))

(define-public crate-poem-lambda-1.2.25 (c (n "poem-lambda") (v "1.2.25") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.25") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0mvca0nxl898nd5psin90x8247vqa01rdnpnmswmhfvlv9qimniq")))

(define-public crate-poem-lambda-1.2.26 (c (n "poem-lambda") (v "1.2.26") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.26") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "016aigknqj46n93f6ma489813qql9fwlwnx6asi3cqs52nxjg3wg")))

(define-public crate-poem-lambda-1.2.27 (c (n "poem-lambda") (v "1.2.27") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.27") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "09xrxfz79ch1knmhdig0fpmgc31ynq6ys14a0cqsab1xinn5x6p7")))

(define-public crate-poem-lambda-1.2.28 (c (n "poem-lambda") (v "1.2.28") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.28") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1zvxzrb48a9z4rbb3i13ws4137kpywgpb1s8g86sidgibsjsipd8")))

(define-public crate-poem-lambda-1.2.29 (c (n "poem-lambda") (v "1.2.29") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.29") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00h31bc6f2ywhk3w52ci5rzj3kbdbwzpbirgbhkwc17cxjg9chn4")))

(define-public crate-poem-lambda-1.2.30 (c (n "poem-lambda") (v "1.2.30") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qjz5h7302f5njysqfhdllfgqarv1nway66367y6fp9fkvzkr5g4")))

(define-public crate-poem-lambda-1.2.31 (c (n "poem-lambda") (v "1.2.31") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.31") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "10jljdqzcdsrpzgrawc6ddy91bkyi1drf7wmn2m2mnb6zklsv5lh")))

(define-public crate-poem-lambda-1.2.32 (c (n "poem-lambda") (v "1.2.32") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.32") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qfx7kmkp6v8rqi9hry6iszgnflj55zppfhfzqw9zjabcvaqkxh3")))

(define-public crate-poem-lambda-1.2.33 (c (n "poem-lambda") (v "1.2.33") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.33") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1crgkfk61s8mkvffarabicw1q33b9583l739a3q7vm6ndlhfwfci")))

(define-public crate-poem-lambda-1.2.34 (c (n "poem-lambda") (v "1.2.34") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.34") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "02wlykx25c8pfqjzmvbjnxfvjfqqsypqgry8gxbq9sk4wkqss51h")))

(define-public crate-poem-lambda-1.2.35 (c (n "poem-lambda") (v "1.2.35") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.35") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "13nlh4bvm1vdv3kvym2nyvi08j90lb9syggvhmfq3hjq6yssrlv1")))

(define-public crate-poem-lambda-1.2.36 (c (n "poem-lambda") (v "1.2.36") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.36") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0dcm5wb0v88qib5w1jnfyi4ikzkjg2sxfa0asfqynf5vd0djg6lm")))

(define-public crate-poem-lambda-1.2.37 (c (n "poem-lambda") (v "1.2.37") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.36") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0pp3vna7s9d7v2fnmhh4z9f430zgs3jx9lqvm6jv7mag2r478s70")))

(define-public crate-poem-lambda-1.2.38 (c (n "poem-lambda") (v "1.2.38") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.38") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0gk8m3nakv7hv7z20z87n7azkqyvqpkwxxy3i2c65427rxgmcfby")))

(define-public crate-poem-lambda-1.2.39 (c (n "poem-lambda") (v "1.2.39") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.39") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0kgvgdwc0r5md226p99alhs908p43i9qsjxqcqdwxvg72xvif4cz")))

(define-public crate-poem-lambda-1.2.40 (c (n "poem-lambda") (v "1.2.40") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.40") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0acx2wm02yh233b2s4nlblxlqich2vkvv8ppfcnvlg7myjxnghsr")))

(define-public crate-poem-lambda-1.2.41 (c (n "poem-lambda") (v "1.2.41") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.41") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0acii6sd53qa7z9rp7fj0r5a5rw2h23qv4flnqfm04i6blrf855j")))

(define-public crate-poem-lambda-1.2.42 (c (n "poem-lambda") (v "1.2.42") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.42") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0bjvax0cypr4fshdnbskvbncijfyzb9n86kcfkr4v4dmaw7kcfkn")))

(define-public crate-poem-lambda-1.2.43 (c (n "poem-lambda") (v "1.2.43") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.43") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "15ac2wm9v470lavkk2h0cfw21a0jkm4ankq50g8ihis2v5qiv5cx")))

(define-public crate-poem-lambda-1.2.44 (c (n "poem-lambda") (v "1.2.44") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.44") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lfdz40i5c2ka8iwfdngyw2rlc6463ji3zwrh2rwwzk2l6rpxl3y")))

(define-public crate-poem-lambda-1.2.45 (c (n "poem-lambda") (v "1.2.45") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.45") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0mr5s15ya77xcb81jlv0i96g14br6azwxaxb1fyhza1rzijvalmr")))

(define-public crate-poem-lambda-1.2.46 (c (n "poem-lambda") (v "1.2.46") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.46") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lwp7m08hf42brqa8ljwnazhmkxj4gas2lqn7sd6qblmfnxw76vx")))

(define-public crate-poem-lambda-1.2.47 (c (n "poem-lambda") (v "1.2.47") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.47") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1b383b20h1hi7j12fv22qz8hsjm5ngxmxqyl7nxcg5yjc22z0wmg")))

(define-public crate-poem-lambda-1.2.48 (c (n "poem-lambda") (v "1.2.48") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.48") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lbvp3kyj1fmhpk1b31ax0i1dlcza2d87dsv2273h2z2vyrs5n4k")))

(define-public crate-poem-lambda-1.2.49 (c (n "poem-lambda") (v "1.2.49") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.49") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1j4i8sivfqpcp9m1mq18n4bvdbmgnnrbj6397qzzyaw83nllycq0")))

(define-public crate-poem-lambda-1.2.50 (c (n "poem-lambda") (v "1.2.50") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.50") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vg7whgjfachyndlig7nac6qj3js77yknb8sqcwi7dcx3c7jp0j5")))

(define-public crate-poem-lambda-1.2.51 (c (n "poem-lambda") (v "1.2.51") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.51") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0bbh29isfi465099r48rvv2mzlm0m7ci84vcixh99pdv31k444qp")))

(define-public crate-poem-lambda-1.2.52 (c (n "poem-lambda") (v "1.2.52") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.52") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1324bccd59rzmqrb57b71phay0jq6mbyvlxz2s4j5krq0xrr6h31")))

(define-public crate-poem-lambda-1.2.53 (c (n "poem-lambda") (v "1.2.53") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.53") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "105njn7v3nf3appln1fiq8ivy7bz5h1kr846y4q68l25f6hlphlr")))

(define-public crate-poem-lambda-1.2.54 (c (n "poem-lambda") (v "1.2.54") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.54") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0qb2419livi4kza8vagdd4ynm3cyin4smwb3hc1r91z9k7z0nivs")))

(define-public crate-poem-lambda-1.2.55 (c (n "poem-lambda") (v "1.2.55") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.55") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1iyjshr6jawi157801i21g8r8dqxybr5k6lnkiasp3l3kidm03vx")))

(define-public crate-poem-lambda-1.2.56 (c (n "poem-lambda") (v "1.2.56") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.56") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1dgc90qvxf337q8cs42g8mr5fdsrnis4i4i4lwm9cbqwmw2zr1nz")))

(define-public crate-poem-lambda-1.2.58 (c (n "poem-lambda") (v "1.2.58") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.58") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1pz27qfrlvpx425j6lgdqdkr6djfcczn9khn4lh70x38yh5b04vj")))

(define-public crate-poem-lambda-1.2.59 (c (n "poem-lambda") (v "1.2.59") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.2.59") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "04s1439ags2bd6jx540mpm6abg9p5bfbrh75ng2cxvf6kc80qdq6")))

(define-public crate-poem-lambda-1.3.0 (c (n "poem-lambda") (v "1.3.0") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lngb09kfhh550d1p11l32kmn05cmd3assidz0fklq10qp4sz396")))

(define-public crate-poem-lambda-1.3.1 (c (n "poem-lambda") (v "1.3.1") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1kbfgcyma2wyaggpm0cvwsliqya71dmbysps0vw28zvqyrg93q80")))

(define-public crate-poem-lambda-1.3.2 (c (n "poem-lambda") (v "1.3.2") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1z8l1a1p1nswwwg44l4z6b00zsmfm1y20m4m4csr5mmnhmnrvdpg")))

(define-public crate-poem-lambda-1.3.3 (c (n "poem-lambda") (v "1.3.3") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0dld2czjflx6v4m9bb4n8vba4grqnxs71cyjrv26ak28kblszf70")))

(define-public crate-poem-lambda-1.3.4 (c (n "poem-lambda") (v "1.3.4") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0p4wbvdzr6r661wfmhvscckygwbxs9jgc91an22b38mn0amhajkn")))

(define-public crate-poem-lambda-1.3.5 (c (n "poem-lambda") (v "1.3.5") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0bw1m1wc6333jp6l54ls9yix280igdmhcz6kpx1ibmqn9vqvnv7v")))

(define-public crate-poem-lambda-1.3.6 (c (n "poem-lambda") (v "1.3.6") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.6") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1a2d8shkq60kc4g7bzk6rdsnskbzkdhzjd7bnhrx7asjd1qnlf3y")))

(define-public crate-poem-lambda-1.3.7 (c (n "poem-lambda") (v "1.3.7") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.7") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qcgw39lzx8nidmqmkncgwskj1vr00gr0pxzfg5bsjzdv8zk04iz")))

(define-public crate-poem-lambda-1.3.8 (c (n "poem-lambda") (v "1.3.8") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.8") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0d2n75h6p7d6cjk48i02vrblzwir52fn7ljlbbx5p24y1yyd8bgd")))

(define-public crate-poem-lambda-1.3.9 (c (n "poem-lambda") (v "1.3.9") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.9") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "150xh57xp93jkspnp1q00vz17z54gysm1pmsh4wbix8a667cqr76")))

(define-public crate-poem-lambda-1.3.10 (c (n "poem-lambda") (v "1.3.10") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.10") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "182cyalasfjqqh9n5z1kxwgp85mbszb4xlgp30igsj5crkz2ahgi")))

(define-public crate-poem-lambda-1.3.11 (c (n "poem-lambda") (v "1.3.11") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.11") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qi2rx2y5jiwdqk7fhxxbjbzfc2a3lb7jixqrayd9kh1j0lis7pi")))

(define-public crate-poem-lambda-1.3.12 (c (n "poem-lambda") (v "1.3.12") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.12") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1j20dl6p9hq1xhns5b4an87h4m9ykcyjig2rndlgzdbia6nc2kjk")))

(define-public crate-poem-lambda-1.3.13 (c (n "poem-lambda") (v "1.3.13") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.13") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0vgy0wvirfnjbmyncncmjl353rm41z4zsll0hmna2gpl8waa4xdg")))

(define-public crate-poem-lambda-1.3.14 (c (n "poem-lambda") (v "1.3.14") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.13") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "04nfbszawjha8ff669ysdv0rj8mz7dbzixz68hdfyb8xylcfj1pl")))

(define-public crate-poem-lambda-1.3.15 (c (n "poem-lambda") (v "1.3.15") (d (list (d (n "lambda_http") (r "^0.4.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "16j4cawrvy7l1jh5nm5rx1jahhpmdk7lsb3x6xl05b8jafh1mpmr")))

(define-public crate-poem-lambda-1.3.16 (c (n "poem-lambda") (v "1.3.16") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "18hzvbwax0nslvn2kx60k34xxf31dkqxxjgmdzrgr78vfq3dyq4b")))

(define-public crate-poem-lambda-1.3.17 (c (n "poem-lambda") (v "1.3.17") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.17") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1x6063fl8gzzzk9p7y5h5092ng4p2wx6qsyj1dczhsazvpvysy9d")))

(define-public crate-poem-lambda-1.3.18 (c (n "poem-lambda") (v "1.3.18") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.18") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "02igbz43rffjawhza7h0njqbvh4fmhj2bwm67fh8a2w491wk2zd7")))

(define-public crate-poem-lambda-1.3.19 (c (n "poem-lambda") (v "1.3.19") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.19") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "075gprj0gspg3fwaj21x2sgw5an89m8xrh7r1gf1z28fiyphkc07")))

(define-public crate-poem-lambda-1.3.20 (c (n "poem-lambda") (v "1.3.20") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.20") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1dilqrzfq90rdv59h8r6dkggr2frghqr42451ac2d5kicpp2kfaz")))

(define-public crate-poem-lambda-1.3.21 (c (n "poem-lambda") (v "1.3.21") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.21") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0xb9vxyh4416wf4kaqj1hgv11jzn7bpa1ivc1348x6zq5hc9pssy")))

(define-public crate-poem-lambda-1.3.22 (c (n "poem-lambda") (v "1.3.22") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.22") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1r7nsan79k4knxgm5pcm2vhghwa4875q53ri2cbg2091lgvq81db")))

(define-public crate-poem-lambda-1.3.23 (c (n "poem-lambda") (v "1.3.23") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.23") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1cjq0sxsh6k8nl0sgcidwixy0msib2aqylx7wzqpl0lrs4jnm5sf")))

(define-public crate-poem-lambda-1.3.24 (c (n "poem-lambda") (v "1.3.24") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.24") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0vpgwvm2fwdjdmhivxma8k9bpif1fkd7askxxmfs3h3ig1lnnn19")))

(define-public crate-poem-lambda-1.3.25 (c (n "poem-lambda") (v "1.3.25") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.25") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1xmp3j1pw4fzx1f910l3ljyrw5i6waf126vvrwznziijw1hnfmba")))

(define-public crate-poem-lambda-1.3.26 (c (n "poem-lambda") (v "1.3.26") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.26") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0825cxfkxqaf2iry5jhc4ag8hv61g51vh9yaxv145vakjv7012ld")))

(define-public crate-poem-lambda-1.3.27 (c (n "poem-lambda") (v "1.3.27") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.27") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gkyp24wpsbs67wqlp3d1bnmql1k0kq18dgas1mllx6ldcdd9x9p")))

(define-public crate-poem-lambda-1.3.28 (c (n "poem-lambda") (v "1.3.28") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.28") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "15n5d0djxwi7pnl7fjzkbnfhdh7aq5iss33ydi00nz4am62iwrvb")))

(define-public crate-poem-lambda-1.3.29 (c (n "poem-lambda") (v "1.3.29") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.29") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1p6dlhns1jzb29ab2hhp07kf5r3fbjjwn96q161pm0jhkjlsycpp")))

(define-public crate-poem-lambda-1.3.30 (c (n "poem-lambda") (v "1.3.30") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.30") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1kmgy6np3cc314nm5a70imalblfxhlpmh6mfwjr2i41yjdp8vvqf")))

(define-public crate-poem-lambda-1.3.31 (c (n "poem-lambda") (v "1.3.31") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.30") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0fr5jnrnkxlbm83l5m2gmppni1a8ggpm8sd4zas7yvj8kvqpi0h1")))

(define-public crate-poem-lambda-1.3.32 (c (n "poem-lambda") (v "1.3.32") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.32") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0ib246jpahymyf01q9sqr4pm3gxf6sfswc0y7fnrqw5jpsil2njf")))

(define-public crate-poem-lambda-1.3.33 (c (n "poem-lambda") (v "1.3.33") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.33") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0ms12nh7irjd58ph5122na758jvq3mqlpyqs8wrn17mabalnhkdn")))

(define-public crate-poem-lambda-1.3.34 (c (n "poem-lambda") (v "1.3.34") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.34") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1icqhfnzw9zzw1i7p5yq7g883vh57fspgwkn45q3255a8yrwq8pw")))

(define-public crate-poem-lambda-1.3.35 (c (n "poem-lambda") (v "1.3.35") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.35") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1c2xamvvdc51hi6pbnlqlzk3bvh6cia9ckjs2w17x305xyblzymg")))

(define-public crate-poem-lambda-1.3.36 (c (n "poem-lambda") (v "1.3.36") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.36") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "068ylpxlnn9c8pn5f8kp50k3xbbdnxh5gwgnl35ww5kvfb983d0z")))

(define-public crate-poem-lambda-1.3.37 (c (n "poem-lambda") (v "1.3.37") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.37") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "06jy479mv4xjafimbdjvc3rvqiqvq5c58zx78gvillyhpviimdza")))

(define-public crate-poem-lambda-1.3.38 (c (n "poem-lambda") (v "1.3.38") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.38") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "16rdkim56yk43yl6jrbc2r3pfp4kr6xaqkvzrlnqzsk9s1zjznvf")))

(define-public crate-poem-lambda-1.3.39 (c (n "poem-lambda") (v "1.3.39") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.39") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "14xg1r1y6335apjy6wi4g8av8w5vhg3pz63fgn38m4qzx94pkkb3")))

(define-public crate-poem-lambda-1.3.40 (c (n "poem-lambda") (v "1.3.40") (d (list (d (n "lambda_http") (r "^0.5.1") (d #t) (k 0)) (d (n "poem") (r "^1.3.40") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0jslpviwj2qlzsc68nzr4wpq1xxlg5dfp8xp3jmpzdfj2ssyiqy9")))

(define-public crate-poem-lambda-1.3.41 (c (n "poem-lambda") (v "1.3.41") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.41") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "077vx3s0g361s3xhqslx9nji0sva7hiiwvqsvcra5ky3v2b2sbc6")))

(define-public crate-poem-lambda-1.3.42 (c (n "poem-lambda") (v "1.3.42") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.42") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1kfw7gkb60fds62g9hvgx0439gnl1c3ipnqzk0f7hy1jdh5psbc9")))

(define-public crate-poem-lambda-1.3.43 (c (n "poem-lambda") (v "1.3.43") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.43") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0sb1ig8zficg3ch3i2lm9hcwz5y9xgzi5bd050p4j3l78yiqgm9k")))

(define-public crate-poem-lambda-1.3.44 (c (n "poem-lambda") (v "1.3.44") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.44") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1cyzl1gv117b6gsrkzx8qrxqhq4rc4kxkxg3b34bmdz1x6l3r7fh")))

(define-public crate-poem-lambda-1.3.45 (c (n "poem-lambda") (v "1.3.45") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.45") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "16d8kswk4dh6as51x1jg9ig9z6h5iiy721a53nv5b0saga4nhqj7")))

(define-public crate-poem-lambda-1.3.46 (c (n "poem-lambda") (v "1.3.46") (d (list (d (n "lambda_http") (r "^0.6.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.46") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "15w50jr769pwq96p59w3dmw4bv643sffwbf98in1cv804cfpcbjj")))

(define-public crate-poem-lambda-1.3.47 (c (n "poem-lambda") (v "1.3.47") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.47") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0rasn4yzialxf7jpnbcsxjp69fvg5gvm7px9k7cj2ckn647pdksp")))

(define-public crate-poem-lambda-1.3.48 (c (n "poem-lambda") (v "1.3.48") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.48") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1my2m7dh6h7w3iyikh838r2kim48mm6k9y54s0mmqacsy0hiccs3")))

(define-public crate-poem-lambda-1.3.49 (c (n "poem-lambda") (v "1.3.49") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.49") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0pwgd68s9w1wlm1wag0ih5361d24drdyma100xry3wq286xlzdbd")))

(define-public crate-poem-lambda-1.3.50 (c (n "poem-lambda") (v "1.3.50") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.50") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1rynkq2wwpirzy3klf221k2rwk830syg2788hzifxvdmh1p3myjh")))

(define-public crate-poem-lambda-1.3.51 (c (n "poem-lambda") (v "1.3.51") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.51") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "04ahjicqjvl8ns21lhajp87vknj00lvg1yr67j6liza6k3niibr3")))

(define-public crate-poem-lambda-1.3.52 (c (n "poem-lambda") (v "1.3.52") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.52") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vbps3fv5m5h39xnx2j8ih0m7qhr6yxpb00c84jkwi9n3wyfd5y9")))

(define-public crate-poem-lambda-1.3.53 (c (n "poem-lambda") (v "1.3.53") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.53") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1fbay1mp78xwjxjvvfxbl15r22zlk86l5fc5hhk8x8syy5cdz664") (r "1.64")))

(define-public crate-poem-lambda-1.3.54 (c (n "poem-lambda") (v "1.3.54") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.54") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0qw1pny7czxfzax8mg2wql912hmhz6bag3nk0mxpzyiwnm687hif") (r "1.64")))

(define-public crate-poem-lambda-1.3.55 (c (n "poem-lambda") (v "1.3.55") (d (list (d (n "lambda_http") (r "^0.7.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.55") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0894cj7bqjb1p1c9i9l690gybszcidpd0ajax6mn5l24ms5kbfyw") (r "1.64")))

(define-public crate-poem-lambda-1.3.56 (c (n "poem-lambda") (v "1.3.56") (d (list (d (n "lambda_http") (r "^0.8.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.56") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0khmcnpljjjkcm17cv6ldm3ah499ll2krckpnnygc1bdji2cgv27") (r "1.64")))

(define-public crate-poem-lambda-1.3.57 (c (n "poem-lambda") (v "1.3.57") (d (list (d (n "lambda_http") (r "^0.8.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.57") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1z0671rx3pb5spnv0k0cpk743sc1zdy2xnj5zykfbq05xaja3dd6") (r "1.64")))

(define-public crate-poem-lambda-1.3.58 (c (n "poem-lambda") (v "1.3.58") (d (list (d (n "lambda_http") (r "^0.8.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.58") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1cji9iii97pmrjlxc6nyhim69zps3f8dikv8l4mphnfjlg48b0yk") (r "1.64")))

(define-public crate-poem-lambda-1.3.59 (c (n "poem-lambda") (v "1.3.59") (d (list (d (n "lambda_http") (r "^0.8.0") (d #t) (k 0)) (d (n "poem") (r "^1.3.59") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1by0abysabbl8648s1hphzxdxvv75p74nqyqzxl315rjz6glfia4") (r "1.64")))

(define-public crate-poem-lambda-4.0.0 (c (n "poem-lambda") (v "4.0.0") (d (list (d (n "lambda_http") (r "^0.9.0") (d #t) (k 0)) (d (n "poem") (r "^2.0.0") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0m5zd3l47sni8wjy3gzbbmfc1yfy95nmm000mzxzr33g134gxr0y") (r "1.64")))

(define-public crate-poem-lambda-5.0.0 (c (n "poem-lambda") (v "5.0.0") (d (list (d (n "lambda_http") (r "^0.11.0") (d #t) (k 0)) (d (n "poem") (r "^3.0.0") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1ij2bv37nmpycji92alzgy2f0nqqksj0lymq1zck52r1hjislxr4") (r "1.75")))

