(define-module (crates-io po em poem-derive) #:use-module (crates-io))

(define-public crate-poem-derive-0.1.1 (c (n "poem-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0y1rv27nnm4dmj814inkcswiq0cbknj6x9vgfalpb30n1zpghccw") (y #t)))

(define-public crate-poem-derive-0.1.2 (c (n "poem-derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0i3kwqfgggkfvqpmdpjxg5wwgm7ahw9j9pk2il8xlm3g987y25ni") (y #t)))

(define-public crate-poem-derive-0.1.3 (c (n "poem-derive") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "19ssy49sz0lw74j5hbrf7bhjzfsn5bbd4fvkzxx7vwbhydzmvmhp") (y #t)))

(define-public crate-poem-derive-0.1.4 (c (n "poem-derive") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1disaawhf0rhbpk0mspm7h9hwlcjvzw3mppmb2n6l2ifgwxi9ncw")))

(define-public crate-poem-derive-0.1.5 (c (n "poem-derive") (v "0.1.5") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "12mym3z62l5c6dd609i6080qrvlcw842f8yh1md722v8rmwn6940")))

(define-public crate-poem-derive-0.2.0 (c (n "poem-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4y6y69nkpzvd9i6nvl934c4pznqvxfx8gd28w2ivsv6v2ja3vd")))

(define-public crate-poem-derive-0.3.0 (c (n "poem-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1vib4kj4zmz64pg7i6icpakzh9mp529lzls4dn6n45hd8srgbi0i")))

(define-public crate-poem-derive-0.3.1 (c (n "poem-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "04l7f20y0y3z9vfb5gn5z4k0fl2a78ql0bw897x18c5ly0an5mza")))

(define-public crate-poem-derive-0.3.2 (c (n "poem-derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "068z35wab9mqd8bn9n3hl3rz73hj6mx5ipias98l3avr50xwbgiq")))

(define-public crate-poem-derive-0.3.3 (c (n "poem-derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4jph177ihgkvh4amyc0pn9ddzx186fxzf5hgq6b6kpdqk7gc1p")))

(define-public crate-poem-derive-0.3.4 (c (n "poem-derive") (v "0.3.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1y3r9sbxms7mg345vc3jzpdkqyrxyclzjank6ac89xwd4a6lww0c")))

(define-public crate-poem-derive-0.3.5 (c (n "poem-derive") (v "0.3.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "14j29ss3z98wali4l5gfmkb0gc8p0dxw0gk5jphbsza0a9cl01lf")))

(define-public crate-poem-derive-0.3.6 (c (n "poem-derive") (v "0.3.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "05w5546a162h2sbb8nx75869la5hkr8ylba7xrn7allpd5qxqchm")))

(define-public crate-poem-derive-0.3.7 (c (n "poem-derive") (v "0.3.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1ppz6ph0v06ksn6hjsxcjbkqgj70vfp2p9qwc14jy45482pflvqg")))

(define-public crate-poem-derive-0.3.8 (c (n "poem-derive") (v "0.3.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "19nmxj91phsfwnr3blgxi5m4263jdlvdn1cdxzp762xyviv9ym69")))

(define-public crate-poem-derive-0.3.9 (c (n "poem-derive") (v "0.3.9") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzcqswz3l6spwaxqq89h8jnkwxfwh9y0navzxiikkmsd7sighny")))

(define-public crate-poem-derive-0.3.10 (c (n "poem-derive") (v "0.3.10") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1cg7h249zaki8sbkx3n18gs28qi0nkj8g7mni6gcpkwij762dvjl")))

(define-public crate-poem-derive-0.4.0 (c (n "poem-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "03kkxnsk4w558spb6pcffig9ls0a6hpp676wlbzm5cg96fh1apq5")))

(define-public crate-poem-derive-0.4.1 (c (n "poem-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgp69cbysrclby4n6cb2cx6vndn276mn65vrndsbj4lik4679kv")))

(define-public crate-poem-derive-0.4.2 (c (n "poem-derive") (v "0.4.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1ijhllh2p3vz3bhwymzpnacxskrnk0pr8ynhw5kjfs6cbf4l5bai")))

(define-public crate-poem-derive-0.4.3 (c (n "poem-derive") (v "0.4.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0g637xlhb5kmall49af5k178da4xv8ffhh5ilqkpvjvqcwxxp2g8")))

(define-public crate-poem-derive-0.4.4 (c (n "poem-derive") (v "0.4.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "007hvm9acrx193j3x9va4p556dfpkzyrbz6pv384j1z624gqyq3j")))

(define-public crate-poem-derive-0.4.5 (c (n "poem-derive") (v "0.4.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0n67bw8r2ndz9xs5b97s8shzskxj0cfxldw6waa1mh711x7iy39x")))

(define-public crate-poem-derive-0.4.6 (c (n "poem-derive") (v "0.4.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1c0608c93rs4i6c7wkczs0klj4qwgvm429s7zxgy4h8xic5x0l1d")))

(define-public crate-poem-derive-0.4.7 (c (n "poem-derive") (v "0.4.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "147ly7x7vhg27ww80wxswjmpm581i3ic64g8gw1wcb54bfhddlbp")))

(define-public crate-poem-derive-0.4.8 (c (n "poem-derive") (v "0.4.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1glvk449ya82js9a4j027s4c34xj7rfkvwirnf1c48db3zw9ba4j")))

(define-public crate-poem-derive-0.4.9 (c (n "poem-derive") (v "0.4.9") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "04yiw7hcpbzzhcqq9a9lj3jpsqd3x93rfr5rw03l8bidqw3w119v")))

(define-public crate-poem-derive-0.4.11 (c (n "poem-derive") (v "0.4.11") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "17l93ms8cmdfx5dzk7gf6gk29rcq5s3gavk43fn9bxjyi4bmybp8")))

(define-public crate-poem-derive-0.4.12 (c (n "poem-derive") (v "0.4.12") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1384rkn48mzpn8vr6ycdm6qiwdflb0pn8c4krqq00pqj431vxaqa")))

(define-public crate-poem-derive-0.5.0 (c (n "poem-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1kxfgd3cfkahlhp3iy4q8mf56bbsy9an46xg8ma9caimjn6azm43")))

(define-public crate-poem-derive-0.5.1 (c (n "poem-derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "154avlxazgzpnaq0y5p5lfl6bfxp63c77ydm7343sfk7w5lyr9qv")))

(define-public crate-poem-derive-0.5.2 (c (n "poem-derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1qrf3z7vih2lhn2kwzy0pr2ycpcy9462vsygd5fpch8jza16k14r")))

(define-public crate-poem-derive-0.5.3 (c (n "poem-derive") (v "0.5.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1v40q1ccnxgj9aq21g9gljfaw890zgrg45w90pvph432nfia504v")))

(define-public crate-poem-derive-0.5.4 (c (n "poem-derive") (v "0.5.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0vw26f80j7ywdf6jxc3w0zhs6q6x9hczhf1a9pdnhm7wm7adi4il")))

(define-public crate-poem-derive-0.5.5 (c (n "poem-derive") (v "0.5.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "02p2d9njisa51jz80niyr3ln44b1803n6kvi8sgj9cng17wdmy8y")))

(define-public crate-poem-derive-0.5.7 (c (n "poem-derive") (v "0.5.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "19kgivzvjnq3rr6wr6lj6lvd991a2fjm3ma2dcppjrq2rwz1a3ih")))

(define-public crate-poem-derive-0.6.0 (c (n "poem-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1azkqdihj449mw1dipq7rq89lwqi296va2gvx51jkis34kgqfn2s")))

(define-public crate-poem-derive-0.6.1 (c (n "poem-derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7gf448f3niw9g6wiarm64cskkx8cpf6wg7a7rm8ahb0xr4fp9c")))

(define-public crate-poem-derive-0.6.2 (c (n "poem-derive") (v "0.6.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "015bzr5az1p4bscznby5a4n6vwmsfr17d3vm2cq9h7j346al916n")))

(define-public crate-poem-derive-0.6.4 (c (n "poem-derive") (v "0.6.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgb67q60f1xp24wvr805rlvng0y435caqszxiicw61hkmdys4zf")))

(define-public crate-poem-derive-0.6.5 (c (n "poem-derive") (v "0.6.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0d9ijp57y0xdb3idj4iffh2bkazmi5mxj6f00igxnsjlk4y2vyyz")))

(define-public crate-poem-derive-0.6.6 (c (n "poem-derive") (v "0.6.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzjkxj83h044sq060642z2f57j4x22zw1wzckxipcr80ywff90z")))

(define-public crate-poem-derive-0.6.7 (c (n "poem-derive") (v "0.6.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "17likj8sg3kz9c6iz59dw8y20wznn35y9p7k61zcrhi3jp6qmbbl")))

(define-public crate-poem-derive-0.6.8 (c (n "poem-derive") (v "0.6.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1b1n43dfyx90xlsn1f2pvcd2s604pdxwdb8g5l82fnw9ih351pr7")))

(define-public crate-poem-derive-0.6.9 (c (n "poem-derive") (v "0.6.9") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "00bbwl0chr6l8z9hqq4dklskwslb2w411wnlhcqmjpc7mz1074kl")))

(define-public crate-poem-derive-0.6.10 (c (n "poem-derive") (v "0.6.10") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "10p0z6ls4xpqyn6pr77z1ym08cywjkrgaabwan53gkrbw73hf3s7")))

(define-public crate-poem-derive-0.7.0 (c (n "poem-derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0hqw56w5grx1ldlb0f8aw3vvmadzmyh2xwxbc8lmg41yb47nmh6f")))

(define-public crate-poem-derive-0.7.1 (c (n "poem-derive") (v "0.7.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3gj7qnsr03w099vbfj0j35m9q3cmwfvzrvp8gfm91swr8l5r1v")))

(define-public crate-poem-derive-0.7.2 (c (n "poem-derive") (v "0.7.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0hxz320jg73qnving56vpy6166jd4pk8bqvfykddr6003lzawlwy")))

(define-public crate-poem-derive-0.7.3 (c (n "poem-derive") (v "0.7.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1b0n4lfkp8a3k9wprij5qx79iyhnw3i5gwzqsc9p8hr3bbgf9ssp")))

(define-public crate-poem-derive-0.7.4 (c (n "poem-derive") (v "0.7.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0z2hw0d0klzgv6wj0rfmb75is7jchzw0dyniijjqjsddzsqsnx8g")))

(define-public crate-poem-derive-0.7.6 (c (n "poem-derive") (v "0.7.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1y1kfsd8aapldb1d1qxlga391w1szy2pfiqcn25n603x08w5z829")))

(define-public crate-poem-derive-0.7.7 (c (n "poem-derive") (v "0.7.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0m9g9xf33b14j854j8gf64382bw9z406dnfvmfp6d5mnrymkniaq")))

(define-public crate-poem-derive-0.7.8 (c (n "poem-derive") (v "0.7.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "12xhgkhdbr3g0bz6ja99lpdgx9l4nypfyc1qnbr73zpx8lx15lkf")))

(define-public crate-poem-derive-0.8.0 (c (n "poem-derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjmvyzmgiph2lf6n2m6k5zj45gwhjqfa86lm9s14krxsipxm244")))

(define-public crate-poem-derive-0.8.1 (c (n "poem-derive") (v "0.8.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0bfc5dv1iplhcbsc39062fkgdvdcw2f7q3w9vaa4dwz8sp8gs2hg")))

(define-public crate-poem-derive-0.8.2 (c (n "poem-derive") (v "0.8.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywsnm258d60yr98cd4nzy2l8ib34l9qnfq96d6azk7wdpvqgacs")))

(define-public crate-poem-derive-0.9.0 (c (n "poem-derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ijb8bxciqibp076hacqv88my8adygcfmnmi6nfggjadkm5fwqzl")))

(define-public crate-poem-derive-1.0.0 (c (n "poem-derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0whqiwh6hy97vb1yp23qc3xff7vqj3s7629ls58ndipflnh2smmn")))

(define-public crate-poem-derive-1.0.1 (c (n "poem-derive") (v "1.0.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1a455h9lkxfzvlbxi9gj3zc8plm14fa8ffamq3h18j3c33zjvafr")))

(define-public crate-poem-derive-1.0.3 (c (n "poem-derive") (v "1.0.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1d08pfdbijyl6nqd43nfk0682xfwq35az7r6m4gw27s71q350dic")))

(define-public crate-poem-derive-1.0.19 (c (n "poem-derive") (v "1.0.19") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1al65qhf68llmhwrzc3myh6y0m7xwiikqni2fjd16xx2ih24jl2j")))

(define-public crate-poem-derive-1.0.20 (c (n "poem-derive") (v "1.0.20") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0xcvkmypddj84fi4w7if1wfdiqmcfwdk9z5268wwlqqnzif9g59p")))

(define-public crate-poem-derive-1.1.0 (c (n "poem-derive") (v "1.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0xqsxry8qc4ag9j4kbmh0mwxfab58pg5m7d1mv5mxvml3ra8a807")))

(define-public crate-poem-derive-1.1.1 (c (n "poem-derive") (v "1.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "17p94g8sgn0n9s3qld07jbfqa5yg9l4xvb8ahgvxm3jrrrp7rf1x")))

(define-public crate-poem-derive-1.2.0 (c (n "poem-derive") (v "1.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "14gh2v3wlrj2ixz8xa2hfs85y9zrmpkvl2kga2y8hgwnf46qaf1k")))

(define-public crate-poem-derive-1.2.1 (c (n "poem-derive") (v "1.2.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1svxwc3ws3mk855pqjk06xn6bzpssns091whz6w8npp674b4pg32")))

(define-public crate-poem-derive-1.2.2 (c (n "poem-derive") (v "1.2.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "166xwg7wjwf02p5vllxbkiqg1r84kfac49jz9l1ww71w2phhi422")))

(define-public crate-poem-derive-1.2.3 (c (n "poem-derive") (v "1.2.3") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0nv5s295bikg1iy9cv1qni1akq4b5lakygwv1ahg9qi4zsg0an1l")))

(define-public crate-poem-derive-1.2.4 (c (n "poem-derive") (v "1.2.4") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "13rvn29zcp2fw02r3z5an041lkrpyvzc71pqxiygbb22fpx7zri4")))

(define-public crate-poem-derive-1.2.5 (c (n "poem-derive") (v "1.2.5") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7svh8wpj4vh7w5n53xx5pd2n7qb3jdb1fyzqwh2wda4xjh88g8")))

(define-public crate-poem-derive-1.2.6 (c (n "poem-derive") (v "1.2.6") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "02xgkr5q75jjkngv6y57xcl97bk2iwl3bgdyw5f9vcbfw6afwii9")))

(define-public crate-poem-derive-1.2.7 (c (n "poem-derive") (v "1.2.7") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgpmhixx84bdpkmqm1n0ydmk26prv4d5sr7aqzsdhbf1zd61pn7")))

(define-public crate-poem-derive-1.2.8 (c (n "poem-derive") (v "1.2.8") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "183q0vr4cxpsiwqjbdryp7s5nn9j2dq92dh9nr2kpbsx9lkpn8h6")))

(define-public crate-poem-derive-1.2.9 (c (n "poem-derive") (v "1.2.9") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0whh4nqin1rvsx6cfk0g0l76ajs60dls1zv880agrlnncalfbk5b")))

(define-public crate-poem-derive-1.2.10 (c (n "poem-derive") (v "1.2.10") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgnrbvbbp429nqv6aa0vrkix8imyxlglpbhh8qcinmsp62q89n3")))

(define-public crate-poem-derive-1.2.11 (c (n "poem-derive") (v "1.2.11") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbbgb1qasf7hzqnkic5h01yk40c9kgb27jcki6ln2xh4rqgz96y")))

(define-public crate-poem-derive-1.2.12 (c (n "poem-derive") (v "1.2.12") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0w5gndxfsnzy7v4ic7v1g50dp9yv4zbnndf2m15kj6bn6jk6ln48")))

(define-public crate-poem-derive-1.2.13 (c (n "poem-derive") (v "1.2.13") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0bjgq0z284xp50q8hlhhjb8lqx8r753ddvkl7n5pxbv7dg3ly0ay")))

(define-public crate-poem-derive-1.2.14 (c (n "poem-derive") (v "1.2.14") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0gl1vj6mk5hggggczwq1i31n9s5brhvlm0kyz439w95f9g8z3gjw")))

(define-public crate-poem-derive-1.2.15 (c (n "poem-derive") (v "1.2.15") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1i3l7dlp4a8klb1n0wk1by2w67in24vk6nic00pgm5yhjn8c0gy1")))

(define-public crate-poem-derive-1.2.16 (c (n "poem-derive") (v "1.2.16") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1ksb0ilqyks1wk0gwmwdgydx8scvfyy5mpsjmw5lla1bma24d7zn")))

(define-public crate-poem-derive-1.2.17 (c (n "poem-derive") (v "1.2.17") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1yryb4aqwfg1n9vmyw0aaarwirzipall8sgd2j3q1y6a8hyfc40r")))

(define-public crate-poem-derive-1.2.18 (c (n "poem-derive") (v "1.2.18") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0wqwsv8823vk4711dpmv3fcxnsxibjidrdg185sn014bzn36jh5z")))

(define-public crate-poem-derive-1.2.19 (c (n "poem-derive") (v "1.2.19") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0gih506qdk12a27bx5s8jmif1awxgix1jmh62kvzkic1yd1hk2m2")))

(define-public crate-poem-derive-1.2.20 (c (n "poem-derive") (v "1.2.20") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1n92zj8i6p3k8cbxz1n876f3g4k99x9ghvciq9g6p6lcd13b0s1q")))

(define-public crate-poem-derive-1.2.21 (c (n "poem-derive") (v "1.2.21") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvk3m8av3bp50pmkq5pqav8qn2jhdjwzy28l85hc89g9jcm96n8")))

(define-public crate-poem-derive-1.2.22 (c (n "poem-derive") (v "1.2.22") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1iwbdym4mps9z6ibymzcnvqhxd6fzmd2154m2585mvzhsbi4hilc")))

(define-public crate-poem-derive-1.2.23 (c (n "poem-derive") (v "1.2.23") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ac9vwmngvzx0gb7gvjqkgfc880hwm7dml6h0fmxrlx939ir0km6")))

(define-public crate-poem-derive-1.2.24 (c (n "poem-derive") (v "1.2.24") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "00hxm0ridy9r5f98nsaz1gcqylxpw1i0n5g32lahkzq7h00hv0d7")))

(define-public crate-poem-derive-1.2.25 (c (n "poem-derive") (v "1.2.25") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0gwgrxrvyppjaiw8bbjs9zyv88jg7plmxcccsaq0f0i8cj758hmv")))

(define-public crate-poem-derive-1.2.26 (c (n "poem-derive") (v "1.2.26") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1wj4wrwmswvmm88cn487qzpf2vj4dg33x1vsqgdns2iq1bfd7ps4")))

(define-public crate-poem-derive-1.2.27 (c (n "poem-derive") (v "1.2.27") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0vjvn8bvh6d3hrlmg1b0qp8x782fckj9jdq1f6dq04pw10rr47g5")))

(define-public crate-poem-derive-1.2.28 (c (n "poem-derive") (v "1.2.28") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0kh89bin7zzvfqxq78fanz1xbzhbd9cfvrinmxbivkkwwjbz5ql3")))

(define-public crate-poem-derive-1.2.29 (c (n "poem-derive") (v "1.2.29") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4v7anll91297x8ksk3pvf32h8kqfg997qa7sc0ln3imd5i837j")))

(define-public crate-poem-derive-1.2.30 (c (n "poem-derive") (v "1.2.30") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsrv3xynqfcblx0idil39gsc4klw6glb26v9dfrshmxfxiaclv6")))

(define-public crate-poem-derive-1.2.31 (c (n "poem-derive") (v "1.2.31") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1p8g7y287wn6y8h904ywx64dfkfqii3zxpw3qf30w1xjw4d8hi9n")))

(define-public crate-poem-derive-1.2.32 (c (n "poem-derive") (v "1.2.32") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1yj58ivp600c7rf2s76vxdx42qsb7d89bb4ivc1r4gn1haibv0a5")))

(define-public crate-poem-derive-1.2.33 (c (n "poem-derive") (v "1.2.33") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "02a07igf795zp4db5v9agq8z7h3wk002v1xavlfgql2a4kck7xmb")))

(define-public crate-poem-derive-1.2.34 (c (n "poem-derive") (v "1.2.34") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0j02dmxcvybqixz9dflhg4z5yjr2iqsw1c3aw1mxipyxv848l9im")))

(define-public crate-poem-derive-1.2.35 (c (n "poem-derive") (v "1.2.35") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0660sqq9nj61vyyarrwckkgjjalk2p60vmilsnkbvhgq3wpbf3i9")))

(define-public crate-poem-derive-1.2.36 (c (n "poem-derive") (v "1.2.36") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1p53ky43z0kimzzh4wrs1pyikh6j60npppig1b9sd7733p8b9wmz")))

(define-public crate-poem-derive-1.2.37 (c (n "poem-derive") (v "1.2.37") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "08ws79zla26mw2r8r3k26dz2j5c0ps3wcz1lygab1nd9482h3g7f")))

(define-public crate-poem-derive-1.2.38 (c (n "poem-derive") (v "1.2.38") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "19szi8v0knrj533v3azl5f96x4mrnbd59ghp25ync7gxa4g3cvwn")))

(define-public crate-poem-derive-1.2.39 (c (n "poem-derive") (v "1.2.39") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "16rdpik6k7ipwfar8c3xrb0vb190lwcjvj38zhnwdgzvldqiijr7")))

(define-public crate-poem-derive-1.2.40 (c (n "poem-derive") (v "1.2.40") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqx4yq14sjy1ssqqpigr368m113nifapqg4v1m77320j86iw0x2")))

(define-public crate-poem-derive-1.2.41 (c (n "poem-derive") (v "1.2.41") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5bnyizdwd6q0c77cjrbgd4i8xrjscm4pkxkicgxvwrni77r4sg")))

(define-public crate-poem-derive-1.2.42 (c (n "poem-derive") (v "1.2.42") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1d85a8cfama45yvgn9vjfdgjnd2a45h9nwk49avf9vqdr5v99kgf")))

(define-public crate-poem-derive-1.2.43 (c (n "poem-derive") (v "1.2.43") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0g446bp3858bzvgnb247lgdlsk7mxyvlz4lbpk3z91g3h4sqskkr")))

(define-public crate-poem-derive-1.2.44 (c (n "poem-derive") (v "1.2.44") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0hhyz858z9a051iz9qb9sh58gp6zcfwk6k6bskdbi3h9w98ysq11")))

(define-public crate-poem-derive-1.2.45 (c (n "poem-derive") (v "1.2.45") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0j4shhrylmnglzcmg75lwyshgbh6q5pncxggq43lbbl3ayqp0w5c")))

(define-public crate-poem-derive-1.2.46 (c (n "poem-derive") (v "1.2.46") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0f65np7m5adfknlsw6rwpf4zky5glyxvyp8w35nz72146q2hhwig")))

(define-public crate-poem-derive-1.2.47 (c (n "poem-derive") (v "1.2.47") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0cnknbmnml8v61syx8nrkd03namsqdca9xw7ixawrh62ffy9iggp")))

(define-public crate-poem-derive-1.2.48 (c (n "poem-derive") (v "1.2.48") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1h74bpygwcqdl3yk64h7krad50m3n2j57zcsh55jq264i36gm3sb")))

(define-public crate-poem-derive-1.2.49 (c (n "poem-derive") (v "1.2.49") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "02c52isy3p3lb7jvb1ldkp0mqi2c0kk83fjs66b18139zk0y7nd1")))

(define-public crate-poem-derive-1.2.50 (c (n "poem-derive") (v "1.2.50") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1as3n7z5j83jz7ijb7h9ahwjpxwky3s3q6312szi7xds7qh4py7h")))

(define-public crate-poem-derive-1.2.51 (c (n "poem-derive") (v "1.2.51") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1l6vinxcsmgy8hpkrcm2nhynrnyqa5w71jdac51kr84ynq16llk3")))

(define-public crate-poem-derive-1.2.52 (c (n "poem-derive") (v "1.2.52") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3kkgxcxf7rkr2jqklhiw7jx3vlplqawy508jfzr6pvyd2k1qvg")))

(define-public crate-poem-derive-1.2.53 (c (n "poem-derive") (v "1.2.53") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0qny1wvg22c0hgbmbfr14mzkmsm7kcp4lrnlcwh85lwfinwwa2y9")))

(define-public crate-poem-derive-1.2.54 (c (n "poem-derive") (v "1.2.54") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1x8vd86alcg3pzna2p377dijsppmbykgxd0xrbpjcy3scf6bswa0")))

(define-public crate-poem-derive-1.2.55 (c (n "poem-derive") (v "1.2.55") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1abz4vkxkrf74lnvcj52s6y7hm88fdcd2n34fawddj1dbwziy6lw")))

(define-public crate-poem-derive-1.2.56 (c (n "poem-derive") (v "1.2.56") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjnp789xnpxgbwvis9vd2dsb27dw4h25f8q9bh6wy2l1w1ilvd4")))

(define-public crate-poem-derive-1.2.58 (c (n "poem-derive") (v "1.2.58") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0jjsjvw30iak0a1spdcf1k7jvlqkwyny7iy6vr3mdhfnzmbmagpl")))

(define-public crate-poem-derive-1.2.59 (c (n "poem-derive") (v "1.2.59") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1ddhvahxvjab8a2dws823c1v5c8zz5zynd8v3wxr3pl9ap672fv8")))

(define-public crate-poem-derive-1.3.0 (c (n "poem-derive") (v "1.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "03sahgmh7dbmcww3n7rmxmk2mj9hsdy614v2xlkbv0z1a8gdmrs9")))

(define-public crate-poem-derive-1.3.1 (c (n "poem-derive") (v "1.3.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "09qa4x94plj4vhmgdn3p1ss0cpr3641v4zb4iw96p7dpa5rdwf5v")))

(define-public crate-poem-derive-1.3.2 (c (n "poem-derive") (v "1.3.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "04f2jn6iis8wh68rz8ys6gyl6168xsgq5q8adp148bxydj9ig4ap")))

(define-public crate-poem-derive-1.3.3 (c (n "poem-derive") (v "1.3.3") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0n20ciih4gdbpgmnma9vdl6ln96sb7n22dgv8v80j17n4wqab3xi")))

(define-public crate-poem-derive-1.3.4 (c (n "poem-derive") (v "1.3.4") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "14hpw9cbwi9lv5xn06rfvpdhn8vn9y4hk6bk7x8in3qmpasfwsyw")))

(define-public crate-poem-derive-1.3.5 (c (n "poem-derive") (v "1.3.5") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0sird60vrzamyzlvhr6wm437vbkqdik33shizkv3yy7fgfsibrhh")))

(define-public crate-poem-derive-1.3.6 (c (n "poem-derive") (v "1.3.6") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "19mab2779il31j3ax5q3n0r6mgczxw9gsk46ac0z6g0mlcd2pzcb")))

(define-public crate-poem-derive-1.3.7 (c (n "poem-derive") (v "1.3.7") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4yxs39a9df59fw982ppgzsn59lddnha7iqp06xhm340c8vcxs0")))

(define-public crate-poem-derive-1.3.8 (c (n "poem-derive") (v "1.3.8") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1ars46gpg4wk2kyb0n2mcjar8jn64wms3cw287dz01045683arhb")))

(define-public crate-poem-derive-1.3.9 (c (n "poem-derive") (v "1.3.9") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "02pbmnhafgb9a5ng9wdknaihwsq565aq9iqsc4fxkky32hpsfxm3")))

(define-public crate-poem-derive-1.3.10 (c (n "poem-derive") (v "1.3.10") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0as8hnz47fip2m4zfdd86qx6c206707j5qvz0bip2s9gnwdv51s2")))

(define-public crate-poem-derive-1.3.11 (c (n "poem-derive") (v "1.3.11") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "17g6av6376ns6s68p55i55s02gbz2bgnml4zx5c18gd936xqy97n")))

(define-public crate-poem-derive-1.3.12 (c (n "poem-derive") (v "1.3.12") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "025r65h87d436pd2yrdmyz836pk3pfmc93yf40ip87fi42v8s2ad")))

(define-public crate-poem-derive-1.3.13 (c (n "poem-derive") (v "1.3.13") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1iqigzycw3csdmg5qh2akz724q865dx1zl6v94688jn4c2icc85s")))

(define-public crate-poem-derive-1.3.14 (c (n "poem-derive") (v "1.3.14") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1dmcl33dl7bjrclglj8hcnavy2al47jznwxih1zs7cdimx94qw4k")))

(define-public crate-poem-derive-1.3.15 (c (n "poem-derive") (v "1.3.15") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1vf4f9b3adk1jv0lcndxm21vwl5ryfkzwdgbgzy4d3rhq4dlcj61")))

(define-public crate-poem-derive-1.3.16 (c (n "poem-derive") (v "1.3.16") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1aarka6w2an6h411cb4q7gr1am1ynbci8mikjwadfqx23di6pr2b")))

(define-public crate-poem-derive-1.3.17 (c (n "poem-derive") (v "1.3.17") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1gnn0hj5mvhbbbql1qnplb45dg515nva9qzc6zxzgl9cyspzfv1c")))

(define-public crate-poem-derive-1.3.18 (c (n "poem-derive") (v "1.3.18") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0byh13qjf9ll5h9pxa6wnj7adzqy6nymxsv3ih4qjg4s98kmy4hd")))

(define-public crate-poem-derive-1.3.19 (c (n "poem-derive") (v "1.3.19") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "01v5pr22bbhk6b1fmylvmwpa4k5k4r60gs15xkjx2sjdhlb0pf04")))

(define-public crate-poem-derive-1.3.20 (c (n "poem-derive") (v "1.3.20") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdsnqbfjrpfb6z39mk8jffr0imfjlf0h9c160s1n0qr9xcwjnxi")))

(define-public crate-poem-derive-1.3.21 (c (n "poem-derive") (v "1.3.21") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0iy645fmnn9gibwbq9h9ja9v2afq3zhz5vx1wwjj7iibk4sxqw1f")))

(define-public crate-poem-derive-1.3.22 (c (n "poem-derive") (v "1.3.22") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "12js9pdws2fnm6bgkcpqzmaxmpfnkqaprshf4986507l0birgppw")))

(define-public crate-poem-derive-1.3.23 (c (n "poem-derive") (v "1.3.23") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0w70z0nqs80sb7l7j6hizv00709rcfdxjdjfzzfad720199p93pw")))

(define-public crate-poem-derive-1.3.24 (c (n "poem-derive") (v "1.3.24") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0chd1gl2wdixdh5rmacd3fmyki6fpr8rpwblm7czvyw8ysar1ylp")))

(define-public crate-poem-derive-1.3.25 (c (n "poem-derive") (v "1.3.25") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ag83mplh8mkamkhjpv54bjh9gz4iva3lk21ckihqz2344bh2iga")))

(define-public crate-poem-derive-1.3.26 (c (n "poem-derive") (v "1.3.26") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "096gamh00yxrh1c7asmkahgyxh4gy7ipmn6v5xirg2007zy2g2ym")))

(define-public crate-poem-derive-1.3.27 (c (n "poem-derive") (v "1.3.27") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1cqqvi6kvymqls9apm2j4bszr8yj3d56z9nkpw2wlyd99khpv903")))

(define-public crate-poem-derive-1.3.28 (c (n "poem-derive") (v "1.3.28") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1qfg987ak621m9f474xm4zn4skmnmni7bp0hhmp72i0hmfa3rky8")))

(define-public crate-poem-derive-1.3.29 (c (n "poem-derive") (v "1.3.29") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1z18kvbiyk46m2hmn3xagv5md0mrgfrkap5hbm16rvqvlqcj1l99")))

(define-public crate-poem-derive-1.3.30 (c (n "poem-derive") (v "1.3.30") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0dpvlb0sgix1h8nj49icj1cyzgj8mrk3qm6wmlv0zp29ywg4xmzy")))

(define-public crate-poem-derive-1.3.31 (c (n "poem-derive") (v "1.3.31") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0bfgynqz4fk2bfra5ay0czh507kylxmdrrz23r6s05drm4cfw8yb")))

(define-public crate-poem-derive-1.3.32 (c (n "poem-derive") (v "1.3.32") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1ha8yzqnsi4p4dwq6lxcbmpkgiflcrz47p9z36pw6jdbsy66yhww")))

(define-public crate-poem-derive-1.3.33 (c (n "poem-derive") (v "1.3.33") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0awv89lzb1b13r0ihnkqppjx5xvi6ycd75vqf13092p83gb8mvim")))

(define-public crate-poem-derive-1.3.34 (c (n "poem-derive") (v "1.3.34") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc2mihylfh5w4j01v4h6k9l64rcf91qj8g26bfm7w15hfwxjkjv")))

(define-public crate-poem-derive-1.3.35 (c (n "poem-derive") (v "1.3.35") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0cz01vdw57cwh3w6fwgl08dkq31zhb5ly8b1ca7fp6ajbg04libd")))

(define-public crate-poem-derive-1.3.36 (c (n "poem-derive") (v "1.3.36") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0yss0prjsqxcl4516zxlsp512ks5dy919sn4562ipdcm21zcv85v")))

(define-public crate-poem-derive-1.3.37 (c (n "poem-derive") (v "1.3.37") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1alymjwwbpigzlrj5a7gbpvad3x7dsaag810qcxd9zl78sv55rc9")))

(define-public crate-poem-derive-1.3.38 (c (n "poem-derive") (v "1.3.38") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0cycjlq0ym68v60b5i9qspxpvxfnpwgyqqw4gkck8d5a6lv504ky")))

(define-public crate-poem-derive-1.3.39 (c (n "poem-derive") (v "1.3.39") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "14nzf8a0rnirs1xwj9g24fxg31185124ldmw6yflclp01np31363")))

(define-public crate-poem-derive-1.3.40 (c (n "poem-derive") (v "1.3.40") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1rjpd3v7v8lags1r31iid9v087ych9bgis11phn8cwy5qysj0zpf")))

(define-public crate-poem-derive-1.3.41 (c (n "poem-derive") (v "1.3.41") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2327aab3m06adcxxs4nkdmih2ljps9hkmrv1kx00i2mn260h57")))

(define-public crate-poem-derive-1.3.42 (c (n "poem-derive") (v "1.3.42") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0vkbjjmanqgl2c26fgvbjnl0ib1ldbmip2hnpk5l04slzzbrx7cb")))

(define-public crate-poem-derive-1.3.43 (c (n "poem-derive") (v "1.3.43") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "11kny3mdkqahcbjjnnhsy4njzxhzvra3b46pq4x9agk6s0fwd453")))

(define-public crate-poem-derive-1.3.44 (c (n "poem-derive") (v "1.3.44") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "10rhx2511pwakja4y0g82qhjfdxygkdh2mnjasfdbhak278xkwkd")))

(define-public crate-poem-derive-1.3.45 (c (n "poem-derive") (v "1.3.45") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0dlgzpnsj3w3l48axfc9v9acd85xx6x9i3x4ijwi09m2651mslwz")))

(define-public crate-poem-derive-1.3.46 (c (n "poem-derive") (v "1.3.46") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "11k86cbi28v2hdmcy2kfjh29dpw61rvbdqacrw1zrm45ihkjbhda")))

(define-public crate-poem-derive-1.3.47 (c (n "poem-derive") (v "1.3.47") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1hmswc11kh16f21v1f38ixpaycyxjfsxyc9v5mr0yddxn2r5js6r")))

(define-public crate-poem-derive-1.3.48 (c (n "poem-derive") (v "1.3.48") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "04qli77h5a1dfx1bs809dnvgnh8w09dz2jzha4jirv4yr25anwnk")))

(define-public crate-poem-derive-1.3.49 (c (n "poem-derive") (v "1.3.49") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqv5973bjvcd2ics3q0siy42mmg1rhxrny45lm2q5pbygfv7gyr")))

(define-public crate-poem-derive-1.3.50 (c (n "poem-derive") (v "1.3.50") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzl4jhvhym6sp3p8rsaw4fkk7gi421clq8hbdx4aq0z35yavqwx")))

(define-public crate-poem-derive-1.3.51 (c (n "poem-derive") (v "1.3.51") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1clqgb3pb199axrqczmmz6im76hivzjisdn7z22bkwilkvwn3llp")))

(define-public crate-poem-derive-1.3.52 (c (n "poem-derive") (v "1.3.52") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "05hwpz4k4nir28ii7c0m0b8ldml9dm0vz5z8cdsy5wj8qj1r4h7v")))

(define-public crate-poem-derive-1.3.53 (c (n "poem-derive") (v "1.3.53") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "171w6vqa6hvk4s8r0crdg5f8hd3d8m44y0cc6lgx5d8gln572m4x") (r "1.64")))

(define-public crate-poem-derive-1.3.54 (c (n "poem-derive") (v "1.3.54") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ymihwxff3nyk8fncz5pqjsj4nvy3ly513gw9s3q14kalqc79x1p") (r "1.64")))

(define-public crate-poem-derive-1.3.55 (c (n "poem-derive") (v "1.3.55") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1ki8n3zr30kl3qxx6ywcq62ja4g89bs6lh03fk7abshynrp0rv") (r "1.64")))

(define-public crate-poem-derive-1.3.56 (c (n "poem-derive") (v "1.3.56") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikq3c53qf49dc17nk7kahsviz8fxa33c19cyhys0l6nl9vzj08p") (r "1.64")))

(define-public crate-poem-derive-1.3.57 (c (n "poem-derive") (v "1.3.57") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f53m2hx147aczy8yycxiqcfzjb21h4w71036waj4n0zda25ipgm") (r "1.64")))

(define-public crate-poem-derive-1.3.58 (c (n "poem-derive") (v "1.3.58") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19l0m7xdjvl2qqy2y13hd0dh7rv9d1dcqg7gjj42ffr7wyya0l15") (r "1.64")))

(define-public crate-poem-derive-1.3.59 (c (n "poem-derive") (v "1.3.59") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cdvid2ryn4h9wj7087shf20ijvahh1n44bmwghngn6qh13czpa2") (r "1.64")))

(define-public crate-poem-derive-2.0.0 (c (n "poem-derive") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13sz33smy9cld8ri9awblbm0lm0pk8dvn1hf5p5hx84r4kwd6avy") (r "1.64")))

(define-public crate-poem-derive-2.0.1 (c (n "poem-derive") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3zb1pcc74b734jqg1ayckdfhwawfjkixz99g9fby30cwxxc9j8") (r "1.74")))

(define-public crate-poem-derive-3.0.0 (c (n "poem-derive") (v "3.0.0") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ksdjqr8jqzib2qh5ybp6nhgv4lj3lw98qr34063hlvciban3ff2") (r "1.75")))

