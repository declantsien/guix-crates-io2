(define-module (crates-io po em poem_openapi_validator_derive) #:use-module (crates-io))

(define-public crate-poem_openapi_validator_derive-0.1.0 (c (n "poem_openapi_validator_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "1q7zcq4rqn6yn3n1pdda4k9ha7zhl1mfdsk3s03f38wbn9cb2673")))

(define-public crate-poem_openapi_validator_derive-0.1.1 (c (n "poem_openapi_validator_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "validator") (r "^0.14") (d #t) (k 0)))) (h "0aqbrx4pp5kjb8kiqwfx3143k3625l1r1a9199lg80svlg1ipyrf")))

