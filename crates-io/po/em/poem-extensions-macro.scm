(define-module (crates-io po em poem-extensions-macro) #:use-module (crates-io))

(define-public crate-poem-extensions-macro-0.1.0 (c (n "poem-extensions-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (k 0)) (d (n "proc-macro2") (r "^1.0.40") (k 0)) (d (n "quote") (r "^1.0.20") (k 0)) (d (n "syn") (r "^1.0.98") (k 0)) (d (n "thiserror") (r "^1.0.31") (k 0)))) (h "0wswd9hffjpi4bgrcgqpldcrqps86hwkdd9xkwfd9ssw4qaa5ky2")))

(define-public crate-poem-extensions-macro-0.2.0 (c (n "poem-extensions-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (k 0)) (d (n "quote") (r "^1.0.21") (k 0)) (d (n "syn") (r "^1.0.99") (k 0)) (d (n "thiserror") (r "^1.0.32") (k 0)))) (h "172vazh7vfpn1xjz40bc642fbp8pc2p5s5ajrsd5l4f3ybgvxdfh")))

(define-public crate-poem-extensions-macro-0.3.0 (c (n "poem-extensions-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (k 0)) (d (n "proc-macro2") (r "^1.0.46") (k 0)) (d (n "quote") (r "^1.0.21") (k 0)) (d (n "syn") (r "^1.0.102") (k 0)) (d (n "thiserror") (r "^1.0.37") (k 0)))) (h "0mgp8x1jw3ffpqv7s207p1j3c74nmw6xq4l07qg0d995cbnkmq4a")))

(define-public crate-poem-extensions-macro-0.4.0 (c (n "poem-extensions-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.2") (k 0)) (d (n "proc-macro2") (r "^1.0.47") (k 0)) (d (n "quote") (r "^1.0.21") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)) (d (n "thiserror") (r "^1.0.37") (k 0)))) (h "0pq34d0fy5hsl2849wgmsj2r42bx7r0sg6z67w53m6xbfb8x4dha")))

(define-public crate-poem-extensions-macro-0.5.0 (c (n "poem-extensions-macro") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.2") (k 0)) (d (n "proc-macro2") (r "^1.0.49") (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.107") (k 0)) (d (n "thiserror") (r "^1.0.38") (k 0)))) (h "1lds29g4pmmkdxg68c8ifmaf3z1x9wk635mqqjq2i20l92921yzl")))

(define-public crate-poem-extensions-macro-0.6.0 (c (n "poem-extensions-macro") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.2") (k 0)) (d (n "proc-macro2") (r "^1.0.50") (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.107") (k 0)) (d (n "thiserror") (r "^1.0.38") (k 0)))) (h "1p5xnjl8wavp6mg9v4fa298wi97pklbrc1327jxlfsl2figh993v")))

(define-public crate-poem-extensions-macro-0.7.0 (c (n "poem-extensions-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "01nrqf0dxag9jqbmimgw8hrm8s9d474jrpal9xf61ra3g1h0n6ak")))

(define-public crate-poem-extensions-macro-0.7.1 (c (n "poem-extensions-macro") (v "0.7.1") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "0jagmgmjnffmdl45s8h3qg9ags2616i17q4hl7kbrrqhb9w78h29")))

(define-public crate-poem-extensions-macro-0.7.2 (c (n "poem-extensions-macro") (v "0.7.2") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "1rz81ii6iwrvpamyszmh3417z1cbcvqjn3wrdxi90fisxkyssvph")))

(define-public crate-poem-extensions-macro-0.8.0 (c (n "poem-extensions-macro") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "0bg6srfnsmv8v0j8iimh02q6jj91gaa0vhmdxvsrcflvfjadh83z")))

(define-public crate-poem-extensions-macro-0.9.0 (c (n "poem-extensions-macro") (v "0.9.0") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "059cikg5cf0dlaza8xgchi31bz1vlckv9c58jjw3i7hq0drvhy4k")))

