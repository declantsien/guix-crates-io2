(define-module (crates-io po em poem-openapi-api-derive) #:use-module (crates-io))

(define-public crate-poem-openapi-api-derive-0.1.0 (c (n "poem-openapi-api-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1aq25qv8ilmyscfp1bp7niv4im777rxl47sxdv50l7qa0igm3p4b")))

(define-public crate-poem-openapi-api-derive-0.2.0 (c (n "poem-openapi-api-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0f72wghg0x9gv72vd2ss2j7d6fykdqpsn0chdm0ms3qcxnim7fw6")))

(define-public crate-poem-openapi-api-derive-0.3.0 (c (n "poem-openapi-api-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1ng4wv6f8azvy9b60gzr3bd6sx5zyx112m4l9aws4171lgiqqfra")))

