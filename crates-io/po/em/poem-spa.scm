(define-module (crates-io po em poem-spa) #:use-module (crates-io))

(define-public crate-poem-spa-0.9.0 (c (n "poem-spa") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("static-files"))) (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("static-files" "test"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (d #t) (k 2)))) (h "1x2wq9i4wifi3r61im083brp4nrg7zmjslciq9m44d7dpncr983a") (y #t)))

(define-public crate-poem-spa-0.9.1 (c (n "poem-spa") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("static-files"))) (d #t) (k 0)) (d (n "poem") (r "^1.3") (f (quote ("static-files" "test"))) (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (d #t) (k 2)))) (h "1r71l1xlx7h8423h9lmc8gic108wrdffhqrca2gr1jn0ylq6qz5y")))

