(define-module (crates-io po as poasync) #:use-module (crates-io))

(define-public crate-poasync-0.1.0 (c (n "poasync") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "gzip" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (f (quote ("env_logger"))) (d #t) (k 0)))) (h "01s6ys9rslsrr344h4aazqw9gijybpdn74d0pp3mwvh1ksbqp6cn")))

(define-public crate-poasync-0.1.1 (c (n "poasync") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (f (quote ("env_logger"))) (d #t) (k 0)))) (h "0gbzl1sgksfac02w3bxqdc5j6n1ha57amids9n6qs7cjnnk8nhkj")))

(define-public crate-poasync-0.2.0 (c (n "poasync") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (f (quote ("env_logger"))) (d #t) (k 0)))) (h "1iahzmysiyy3b3qkmf6kqjrlxwia0v2jasg0yb1w3zpcc55nd8nk")))

