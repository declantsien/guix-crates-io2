(define-module (crates-io po md pomd) #:use-module (crates-io))

(define-public crate-pomd-1.4.0 (c (n "pomd") (v "1.4.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "pausable_clock") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "1pjx7hqpmndp0rjy8viv56a7vli499q4lbyhdqq6ynv084phmcmf")))

