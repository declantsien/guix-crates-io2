(define-module (crates-io po ki poki-launcher-notifier) #:use-module (crates-io))

(define-public crate-poki-launcher-notifier-0.1.0 (c (n "poki-launcher-notifier") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "1v5zd16mf4l8v10795jsvalzz5ii6cxrfnq6hvz6b46bsk1mr8jr")))

