(define-module (crates-io po ly polyeval) #:use-module (crates-io))

(define-public crate-polyeval-0.1.0 (c (n "polyeval") (v "0.1.0") (h "0gy624pa535pbl2vsksyg0vlibgsysgpyllcw6d1ybv62yq9qjd6")))

(define-public crate-polyeval-0.1.1 (c (n "polyeval") (v "0.1.1") (h "15lc17n6x25zd2kzq14ibb2dks0k21f3yzvqancbxwnfcfqzsi9x")))

(define-public crate-polyeval-0.1.2 (c (n "polyeval") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "18j3i3yakky044xwg11s2ig9952fl90dm8cyyz19pmramkmw7s15")))

(define-public crate-polyeval-0.2.0 (c (n "polyeval") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1hpdx02mdczpn2n1xf1n27a8rgpn2n6pm6rgvcrwb23vihz14zs4")))

(define-public crate-polyeval-0.3.0 (c (n "polyeval") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "06g3hbcwszf2nw78wmv1kg7pnvkb0cn3cpyaxdcnyqrfcvmqi89b")))

(define-public crate-polyeval-0.3.1 (c (n "polyeval") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "052kbjwnbdixa9v254hv0rw8n660il5smxmk5hmdidkka95lg6vn")))

(define-public crate-polyeval-0.4.0 (c (n "polyeval") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)))) (h "0nyfgfiymw5j6iz90ffg81hkl1zwwci60dpl37m2w26sw3ynhrw3") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-polyeval-0.4.1 (c (n "polyeval") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)))) (h "0pp7640081x16cf0qv2d86hip3m82irf9hswg0v31scj777ndzaq") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-polyeval-0.4.2 (c (n "polyeval") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)))) (h "1lp9lwj98ayvf1fa1cxddda767220qghg37ayircypvpkbncfgnx") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-polyeval-0.4.3 (c (n "polyeval") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2.17") (k 0)))) (h "1zrpd4573w40ly35qqvp7v113j19mbvwzkrhss37b2mc0z7lfmaq") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

