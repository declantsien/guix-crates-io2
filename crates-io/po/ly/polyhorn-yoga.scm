(define-module (crates-io po ly polyhorn-yoga) #:use-module (crates-io))

(define-public crate-polyhorn-yoga-0.3.2 (c (n "polyhorn-yoga") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "1klk5shhx4xbihhvv05siq98f8dy9q672pyi844x0py4xakmnprx") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

(define-public crate-polyhorn-yoga-0.3.3 (c (n "polyhorn-yoga") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "1h7lxqf753pgbc0h8j7i0li0ymm84vwvvd63b85chi7r1j9g6m66") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

(define-public crate-polyhorn-yoga-0.3.4 (c (n "polyhorn-yoga") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "1v7sprahn5rcib6mwsmzmabmj42nxv6vqqa5n84hv191v9dwwqww") (f (quote (("serde_support" "serde" "serde_derive" "ordered-float/serde") ("default"))))))

