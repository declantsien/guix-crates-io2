(define-module (crates-io po ly polyglot_interpreter) #:use-module (crates-io))

(define-public crate-polyglot_interpreter-0.1.0 (c (n "polyglot_interpreter") (v "0.1.0") (h "1c6yjjb39d4lf6q29b8k8l5n8gf2paf3p3nlym3s35wfvn8ib90b")))

(define-public crate-polyglot_interpreter-1.0.0 (c (n "polyglot_interpreter") (v "1.0.0") (h "01prjj1q8apxackc4sh5crr22lqjk0wzj7cd9nqq6p4i3ybd8g75")))

(define-public crate-polyglot_interpreter-1.0.1 (c (n "polyglot_interpreter") (v "1.0.1") (h "15fm01h8r3znzw9zcph26l5rhgk16d0l0g76gaj21l59i2s6lbgd")))

