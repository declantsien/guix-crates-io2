(define-module (crates-io po ly polynom) #:use-module (crates-io))

(define-public crate-polynom-0.1.0 (c (n "polynom") (v "0.1.0") (h "0zll1z19vc7kgx52j3cyhqs0ck1zigd773jnc3qlnqzxpjqaypyy")))

(define-public crate-polynom-0.1.1 (c (n "polynom") (v "0.1.1") (h "13iab39q9xw9wqndxh0yfg1hwjac49prcrx9bvhv69cshsx6aii0")))

