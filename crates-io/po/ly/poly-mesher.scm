(define-module (crates-io po ly poly-mesher) #:use-module (crates-io))

(define-public crate-poly-mesher-0.1.0 (c (n "poly-mesher") (v "0.1.0") (d (list (d (n "plotpy") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "voronator") (r "^0.2.1") (d #t) (k 0)))) (h "1kcfd09nw91bm5yfan4s254jlf7y5f47r2nngrpi9hl6as48ffp9")))

