(define-module (crates-io po ly polyzero) #:use-module (crates-io))

(define-public crate-polyzero-0.0.1 (c (n "polyzero") (v "0.0.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1i0gdmnall0y9g32vklps7yypaci64qxdqvrssxd1ah54kgwigik")))

