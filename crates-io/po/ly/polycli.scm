(define-module (crates-io po ly polycli) #:use-module (crates-io))

(define-public crate-polycli-0.0.0 (c (n "polycli") (v "0.0.0") (h "1hqysjfdzykn0kqgh00lp3ngamswfsgv61n1ckhk9rzcj6sb7p6b")))

(define-public crate-polycli-0.1.0 (c (n "polycli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "num-decimal") (r "^0.2") (k 0)) (d (n "polyio") (r "^0.5") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "time-util") (r "^0.2") (f (quote ("chrono"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.1.6") (f (quote ("ansi" "chrono" "env-filter" "fmt"))) (k 0)))) (h "0w4n0k3si1yvijbq3znqv8iwbkgzls8cyn0yy7c7ihybdnwl6hva")))

(define-public crate-polycli-0.1.1 (c (n "polycli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "num-decimal") (r "^0.2") (k 0)) (d (n "polyio") (r "^0.9") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "time-util") (r "^0.3") (f (quote ("chrono"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("ansi" "chrono" "env-filter" "fmt"))) (k 0)))) (h "0cxsvrv2bp6i067qm260yv83llf1kwyjz6x9sapvfz4q4wxdppil")))

