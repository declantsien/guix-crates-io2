(define-module (crates-io po ly polywrap_plugin_creator) #:use-module (crates-io))

(define-public crate-polywrap_plugin_creator-0.1.2 (c (n "polywrap_plugin_creator") (v "0.1.2") (d (list (d (n "polywrap_core") (r "^0.1.2") (d #t) (k 0)) (d (n "polywrap_msgpack") (r "^0.1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "wrap_manifest_schemas") (r "^0.1.2") (d #t) (k 2)))) (h "0l9cx3pj9876c9rlgxpnq540722kxi2mic2sjrac8469d7m2qsm8") (r "1.67")))

