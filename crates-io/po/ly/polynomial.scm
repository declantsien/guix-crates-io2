(define-module (crates-io po ly polynomial) #:use-module (crates-io))

(define-public crate-polynomial-0.0.1 (c (n "polynomial") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0na2rinwmsmwlzrsqwmaxys95h9jc4k5kg58xzxgbf1iamfim6jp")))

(define-public crate-polynomial-0.0.2 (c (n "polynomial") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0frn872n00y6fqbwxnbp2qk8mpy812zkazmjfna7l6x11spbn9zg")))

(define-public crate-polynomial-0.0.3 (c (n "polynomial") (v "0.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0qyz1pxixd028ika09js9jg1z6mkg6izkn5wvhj37j0kqnamliym")))

(define-public crate-polynomial-0.0.4 (c (n "polynomial") (v "0.0.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1bfj7x16i0w6pp2zrc7pkgmyjg6r50nw72j9swnrksf0r01p9z6v")))

(define-public crate-polynomial-0.0.5 (c (n "polynomial") (v "0.0.5") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0vz09cc3n7gn2hyb4ascffv36bgjvky1vhqkid7qsrwqb80hx1kb")))

(define-public crate-polynomial-0.0.6 (c (n "polynomial") (v "0.0.6") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "07fvd5l0lpiv4hgsd3zppya4l40jq1280i7mczmjs8wi126krhx5")))

(define-public crate-polynomial-0.1.0 (c (n "polynomial") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nqdvivjlf5i02ms8fhylrm0j9hnmvdmrwk5ak9ljl90cv3cgss6")))

(define-public crate-polynomial-0.1.1 (c (n "polynomial") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hby0cdcjjyp9f8nayq8qrvjzhm2k1rwxyf498p0jnkidgcdsfid")))

(define-public crate-polynomial-0.1.2 (c (n "polynomial") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1saab2xz0hrmdjyvklr1hfsvwwmjg40j3z1ba8z70wxnh82xf98b")))

(define-public crate-polynomial-0.2.0 (c (n "polynomial") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g08zn8l92bvdlb8b7i417y66q0658aqykq03x97527w6jzwa3gv") (r "1.56.1")))

(define-public crate-polynomial-0.2.1 (c (n "polynomial") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "190zaf31pwhqkf1xcwpw8br8b14fs3wr94wsxj0imknchcng0k9n") (r "1.56.1")))

(define-public crate-polynomial-0.2.2 (c (n "polynomial") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1al9gb4i542p7g3c4vnl6c2gr88rllk13a8q21z4y9p8wsqllwna") (r "1.56.1")))

(define-public crate-polynomial-0.2.3 (c (n "polynomial") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wcldn7lz7rqfwahnisddkdwk27rl1xr0gg2kjffzmw2xvdrkbfk") (r "1.56.1")))

(define-public crate-polynomial-0.2.4 (c (n "polynomial") (v "0.2.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0brfsfww5v40maf3nsrhs6ss098lk8kvzk5ggmr869pf3x5xd8z4") (r "1.56.1")))

(define-public crate-polynomial-0.2.5 (c (n "polynomial") (v "0.2.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0db6ss2i2n72xpw0dr356yaykbz95drmfwnw3glfzyn9hdagfqd4") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56.1")))

(define-public crate-polynomial-0.2.6 (c (n "polynomial") (v "0.2.6") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17byhzs83y3bgfcdnwdshylxxmkxv3qxfl1bm5jwrkldcgjbdar7") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.70.0")))

