(define-module (crates-io po ly polypaging) #:use-module (crates-io))

(define-public crate-polypaging-0.1.0 (c (n "polypaging") (v "0.1.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fvl5pc95wd4wy9fdgyf970cbbbvw5gr2qdy9myq2h2w33vl6s0k")))

(define-public crate-polypaging-0.1.1 (c (n "polypaging") (v "0.1.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02bywfgnzrm7i8ydbxmywviypv5wa2q0wvpk9ws2ks98y63bqqbf")))

(define-public crate-polypaging-0.1.2 (c (n "polypaging") (v "0.1.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wd3bwaa1nwr52nqsrp1fjhzmibnblbdfgh4g2l3s08bqgnfsj57")))

