(define-module (crates-io po ly polys) #:use-module (crates-io))

(define-public crate-polys-0.1.0 (c (n "polys") (v "0.1.0") (h "0hdvcxp308313w9m11x59cx7x9v0fik81s0rf65r8b5yfi8s9kr4")))

(define-public crate-polys-0.1.1 (c (n "polys") (v "0.1.1") (h "1rf24abc91kv6pbayfak28yhvzwqr00icri911a778j0rnbl2ksg")))

(define-public crate-polys-0.1.2 (c (n "polys") (v "0.1.2") (h "0lbiwr01y5pcq7gdh8v0i86k73qqknh1pqdc9fffsqav2n69bpfs")))

(define-public crate-polys-0.1.3 (c (n "polys") (v "0.1.3") (h "0pvjr4v5qc147lm1ywzqhvphzxd4jphma6vb45ivv6z3v48mnlma")))

(define-public crate-polys-0.1.4 (c (n "polys") (v "0.1.4") (h "1v1alizy9x5k73yi3brng0fbnwpmzj5mx0c4mph8damb5w5x5pk9")))

(define-public crate-polys-0.1.5 (c (n "polys") (v "0.1.5") (h "0aqchk8gclwd3dghw8wrgvh4w7vsndkj3icm9pkw13ckbl39rd1l")))

(define-public crate-polys-0.2.0 (c (n "polys") (v "0.2.0") (h "0afs5mb75fkwz32yynjpsch77m0572j57jsd3pyp0aw7gffm0pyd")))

(define-public crate-polys-0.3.0 (c (n "polys") (v "0.3.0") (h "13iap0a1cbfhrm6qpzk437zifl52kamz82xbh2vafng7dyp2nqv8")))

(define-public crate-polys-0.3.1 (c (n "polys") (v "0.3.1") (h "036p7i1wmgwig43dg3ff4wy15q0jqyjy3w6ry48m4izszwzzhfvz")))

(define-public crate-polys-0.4.0 (c (n "polys") (v "0.4.0") (h "1xafw3hsz4hrlfp9zx1l3bwkal856c6xk19f701lxvil8kny78il")))

(define-public crate-polys-0.4.1 (c (n "polys") (v "0.4.1") (h "0xnws2jw8dvc4y7kv96sbwsdysddfcfhv4dw12k0qgkms5zvs9br")))

(define-public crate-polys-0.4.2 (c (n "polys") (v "0.4.2") (h "00lzz2sqc273lnhhbs3wdqqhmzmh8bhgvhy3j5hqiw0f9sc7va74")))

