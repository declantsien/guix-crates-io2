(define-module (crates-io po ly polyfit-rs) #:use-module (crates-io))

(define-public crate-polyfit-rs-0.1.0 (c (n "polyfit-rs") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "1saqbmhjfzcnwsfnlikj6rdi5f9w50lkb9p8z7imj01wlzagmnx9")))

(define-public crate-polyfit-rs-0.1.1 (c (n "polyfit-rs") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "0kk1bjfh3z2fxc4yksmhbbm247sl4dffqa384jpcm2zcw5xn7ssp")))

(define-public crate-polyfit-rs-0.2.0 (c (n "polyfit-rs") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13h86cvrz4qwcrjarfdi6m3jnjwsmrw8b4mlsqlm5mrbhrhx7yhb")))

(define-public crate-polyfit-rs-0.2.1 (c (n "polyfit-rs") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "15xnsa86ghjkm9zp728q99k1sl5ggr20lyd6f9sf603k9iqdk65b")))

