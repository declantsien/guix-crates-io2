(define-module (crates-io po ly polyline) #:use-module (crates-io))

(define-public crate-polyline-0.1.0 (c (n "polyline") (v "0.1.0") (h "0vkrbbpi6vv4iw9rygq0y59nhwfnhapj05z4x2jdnqisa2pc0qq7")))

(define-public crate-polyline-0.2.0 (c (n "polyline") (v "0.2.0") (h "08ann9sfig3hwc0bgqyhgp39ibwqzpqlf0rcg0af89wa49ap4snc")))

(define-public crate-polyline-0.3.0 (c (n "polyline") (v "0.3.0") (h "0sm5rwhwlwgmkckyl7244acwg8xr8jjdjy8zrnf9lmh6pagwcs59")))

(define-public crate-polyline-0.4.0 (c (n "polyline") (v "0.4.0") (h "051rrsg3zr5vpckc28dwjmhx7w18yn28fi787wa7b4h0f7calqmk")))

(define-public crate-polyline-0.5.0 (c (n "polyline") (v "0.5.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0wa159f0spsxa8x863n9pm0yq1map5aiif9p3d97cl1wzcrxawxw")))

(define-public crate-polyline-0.6.0 (c (n "polyline") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "1h3xhmvga6mw7wa4r8f84cq0sibla8sdplap6ii1jj05qdrzbzqn")))

(define-public crate-polyline-0.7.0 (c (n "polyline") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "1z2wkivcc7qs4379h64gbfhwvpfqxcz8xypyrhj2yaqf0mjqxpix")))

(define-public crate-polyline-0.7.1 (c (n "polyline") (v "0.7.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "12x8qgclpys3szihrcm3k0vwjkva70amvxrpapsi7rzhlymwgs43")))

(define-public crate-polyline-0.7.2 (c (n "polyline") (v "0.7.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "15l4y2q26s6svyn239rvbcwkpyxjx7yswawccqnm3m4l769mpx4m")))

(define-public crate-polyline-0.8.0 (c (n "polyline") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "191nn3vm636syqf1mvn28kkd3alpm3w318ivy7vz3sd6d8b3qfjm")))

(define-public crate-polyline-0.9.0 (c (n "polyline") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r ">= 0.6, < 0.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "0ybpyirx5vv2m5dhyq8p3394vy4lyrn099pam10i100v6wf2gyxl")))

(define-public crate-polyline-0.10.0 (c (n "polyline") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "geo-types") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "069fr1b97c111jkcx70hchwkzwdfgpk7fi8nnl5037fh4iaz35al")))

(define-public crate-polyline-0.10.1 (c (n "polyline") (v "0.10.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "06b742239gagqs9i7f9nqbscsmxjqwmfyvsl7y1jdvsba8rxp3b5")))

(define-public crate-polyline-0.10.2 (c (n "polyline") (v "0.10.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0irddc5k8fh25f95w0qk9afrcqpcls87ps4n0c2hq5iyfy9sp7kc")))

(define-public crate-polyline-0.11.0 (c (n "polyline") (v "0.11.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flexpolyline") (r "^0.1.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0rsghxydb4hpabdxvvwyj4c2sss4dcj7pjn6sj352npyk91bz77i")))

