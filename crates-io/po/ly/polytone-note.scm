(define-module (crates-io po ly polytone-note) #:use-module (crates-io))

(define-public crate-polytone-note-1.0.0 (c (n "polytone-note") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.4") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.2") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "polytone") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1jg3knzny893l9bfyp39bxxpwarff2aa9ipvnlvkhnifyyj5vj6v") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces")))) (r "1.67")))

