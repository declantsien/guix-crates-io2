(define-module (crates-io po ly polymath-rs) #:use-module (crates-io))

(define-public crate-polymath-rs-0.1.1-alpha.0 (c (n "polymath-rs") (v "0.1.1-alpha.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1v6dz3302v8x9njn7wwg7dnsl09ak7n6hf2asdd32rp980hm8qln")))

(define-public crate-polymath-rs-0.1.1-alpha.2 (c (n "polymath-rs") (v "0.1.1-alpha.2") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1fdmzc7akiw9lqd1apjyjz9smhnhw2sdazjhxpl2j2shplz0qljl")))

(define-public crate-polymath-rs-0.1.1-alpha.4 (c (n "polymath-rs") (v "0.1.1-alpha.4") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wjb08zqvinf4si43l36f3rbmx4m0afkmdy85h54fhfymmpi4nh1")))

(define-public crate-polymath-rs-0.1.1-alpha.5 (c (n "polymath-rs") (v "0.1.1-alpha.5") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04x8krhdr2j8ss3wkr894gz0qi4md6lrl8mki6lq2rcwls271x9z")))

(define-public crate-polymath-rs-0.1.1-alpha.6 (c (n "polymath-rs") (v "0.1.1-alpha.6") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mcnxfs1dqbj5b02n9wa65w1wk3qw65s5kpj4ay6q5yjnwjx6jaz")))

(define-public crate-polymath-rs-0.1.1 (c (n "polymath-rs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j8vdp14q362fj5s4as6kajsffrwbpf6wwixdcni2x9ai0r09aar")))

(define-public crate-polymath-rs-0.1.2 (c (n "polymath-rs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rywgj7kbyb0hg1wkxfypwykf14b6ia46s9xf9v7z7m3p6cc9xhf")))

