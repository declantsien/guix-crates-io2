(define-module (crates-io po ly polyverse-file-rotate) #:use-module (crates-io))

(define-public crate-polyverse-file-rotate-0.3.0 (c (n "polyverse-file-rotate") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "06rf742gfz5av35cqfr8zy9rhwnspgg4s2sxy6f9sq124g94g5d6") (f (quote (("async" "futures" "futures-util" "tokio"))))))

