(define-module (crates-io po ly polynomen) #:use-module (crates-io))

(define-public crate-polynomen-0.1.0 (c (n "polynomen") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "complex-division") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "zip-fill") (r "^1.0") (d #t) (k 0)))) (h "0vw425rh2w8chz14ca3zh0p822k3xwdy3kadbfaj524vpdnaccbm")))

(define-public crate-polynomen-1.0.0 (c (n "polynomen") (v "1.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "complex-division") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "zip-fill") (r "^1.0") (d #t) (k 0)))) (h "1ra634z8danjfy2r2hlnf2rhh5aryhw3jq5dk3g6cnyw8zwlm4vv")))

(define-public crate-polynomen-1.1.0 (c (n "polynomen") (v "1.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "complex-division") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "zip-fill") (r "^1.0") (d #t) (k 0)))) (h "0hh3b8g8pav8span268jyl06lnk6dkc3pxy37wp83m6j5r30vbxn") (r "1.56")))

