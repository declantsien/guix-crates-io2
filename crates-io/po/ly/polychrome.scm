(define-module (crates-io po ly polychrome) #:use-module (crates-io))

(define-public crate-polychrome-0.1.0 (c (n "polychrome") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "005iv6c4akpz5gn671ckq37if4fhv4bb5lkv2x3m05z81dl60s4i")))

(define-public crate-polychrome-0.1.2 (c (n "polychrome") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lzrzgkq26dl3qb3c0s9znr2pw7igparckzymnqx7292bhhg15np")))

