(define-module (crates-io po ly polyrhythmix) #:use-module (crates-io))

(define-public crate-polyrhythmix-0.1.0 (c (n "polyrhythmix") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 0)) (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1qadhmvdjjzw2k7xp0rlwilzg59wwj6g0z3qcjls14zcypsp60zq")))

