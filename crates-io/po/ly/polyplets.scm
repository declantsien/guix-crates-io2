(define-module (crates-io po ly polyplets) #:use-module (crates-io))

(define-public crate-polyplets-0.1.0 (c (n "polyplets") (v "0.1.0") (d (list (d (n "fluence") (r "^0.6.5") (f (quote ("logger"))) (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1kzxax917fkq2r6w3bm7b1ilmj75y3bwvvf244fd96jvlb6y7x17")))

(define-public crate-polyplets-0.1.1 (c (n "polyplets") (v "0.1.1") (d (list (d (n "fluence") (r "^0.6.8") (f (quote ("logger"))) (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1y5n29r74x1jh22scbmvr821ggrlcm5n3qhf47va4mc58ih4rf6f")))

(define-public crate-polyplets-0.1.2 (c (n "polyplets") (v "0.1.2") (d (list (d (n "marine-rs-sdk") (r "^0.6.11") (f (quote ("logger"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0va7hd7kyhqmqjx78nv4mas5bg7hjxk757g2jqhw1g8kbxc3xn68")))

(define-public crate-polyplets-0.2.0 (c (n "polyplets") (v "0.2.0") (d (list (d (n "marine-macro") (r "^0.6.14") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.6.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "13ldrzwlkh7njc5iq4bfnagv7kz12mra2xbnq81sj3w60dw6xicc")))

(define-public crate-polyplets-0.3.0 (c (n "polyplets") (v "0.3.0") (d (list (d (n "marine-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0ms2is55pfsq6g4yxv80qdimpfz6saxdzbv0c04b43rykz812y20")))

(define-public crate-polyplets-0.3.1 (c (n "polyplets") (v "0.3.1") (d (list (d (n "marine-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0ki7s3xawkzjrlh1qz9x2fzpcjgsj7ghjp5r832q6fc9bk5l1r8q")))

(define-public crate-polyplets-0.3.2 (c (n "polyplets") (v "0.3.2") (d (list (d (n "marine-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "07s1vmwa66a1pa68xbn73pp27mcrl7nxrdh6mspr3qv408i1y1bc")))

(define-public crate-polyplets-0.3.3 (c (n "polyplets") (v "0.3.3") (d (list (d (n "marine-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1lxkv3c19d8vmmzcvm0ndmrs0ghi1yv7zd8vhqngjrhdpkzs3lns")))

(define-public crate-polyplets-0.4.0 (c (n "polyplets") (v "0.4.0") (d (list (d (n "marine-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "marine-rs-sdk-main") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "12qasdy2wdc1w74pa5x9j7qgv0l7dp3n3b1f42xhm98w8sdafydm")))

(define-public crate-polyplets-0.5.0 (c (n "polyplets") (v "0.5.0") (d (list (d (n "marine-rs-sdk") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0va0r4d73mxqhk0pbnwq1x38m9r2djf6w5smr97x5g7kkbydh561")))

(define-public crate-polyplets-0.5.1 (c (n "polyplets") (v "0.5.1") (d (list (d (n "marine-call-parameters") (r "^0.10.0") (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0k2x6fpvl3z80x23fd9mjqh1gs6az8hfzzf3gykrlp7k3cn414k6")))

(define-public crate-polyplets-0.5.2 (c (n "polyplets") (v "0.5.2") (d (list (d (n "marine-call-parameters") (r "^0.10.3") (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1g79hbzfhgl1al29n1zwbh604bdzg5x7s5xdif7g7a0zrg292854")))

(define-public crate-polyplets-0.6.0 (c (n "polyplets") (v "0.6.0") (d (list (d (n "marine-call-parameters") (r "^0.10.3") (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "1njx1qqfn7az7zxp94z6svza753z8yyc2rc4i2ni0fysf74nd10z") (f (quote (("rkyv" "marine-call-parameters/rkyv"))))))

(define-public crate-polyplets-0.7.0 (c (n "polyplets") (v "0.7.0") (d (list (d (n "marine-call-parameters") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("rc" "derive"))) (d #t) (k 0)))) (h "0ii4j9f9rmdk5jma45il30wc6icgj42595xw68g80afhz3vm4x57") (f (quote (("rkyv" "marine-call-parameters/rkyv"))))))

