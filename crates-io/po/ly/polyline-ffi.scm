(define-module (crates-io po ly polyline-ffi) #:use-module (crates-io))

(define-public crate-polyline-ffi-0.1.1 (c (n "polyline-ffi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "polyline") (r "^0.4") (d #t) (k 0)))) (h "1w0r6dnkygj3awaq27kpl4a061ddi62pgmgqp4w2dg77kxpsxlsd")))

(define-public crate-polyline-ffi-0.1.6 (c (n "polyline-ffi") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "polyline") (r "^0.4") (d #t) (k 0)))) (h "0qxjnx6ks1dfl968jspdaidyi1raqz4434c80s0mcxqz0b9n9s7r")))

(define-public crate-polyline-ffi-0.1.7 (c (n "polyline-ffi") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "polyline") (r "^0.4") (d #t) (k 0)))) (h "1vgjv24sfdxvn7h2v3zgh6k9nwzqx73bvx07fpk8141qmifhd048")))

(define-public crate-polyline-ffi-0.1.8 (c (n "polyline-ffi") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "polyline") (r "^0.4") (d #t) (k 0)))) (h "1515ymffh2i9wb6kss8fj73a62f77q4zibirjjrwn27r6h6b3ha5")))

(define-public crate-polyline-ffi-0.1.9 (c (n "polyline-ffi") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "polyline") (r "^0.4") (d #t) (k 0)))) (h "1wdi6chdszh06rrviwy631icw7s6rqik7wq2k99f1jb4cqr5piqw")))

(define-public crate-polyline-ffi-0.3.1 (c (n "polyline-ffi") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.14.1") (d #t) (k 1)) (d (n "geo-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "polyline") (r "^0.7.2") (d #t) (k 0)))) (h "05x4rf0k5g4qxm78nxl8yp1saqsn0fj9bnc7kadg5gb6rwgb0l4n")))

(define-public crate-polyline-ffi-0.4.4 (c (n "polyline-ffi") (v "0.4.4") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "polyline") (r "^0.9.0") (d #t) (k 0)))) (h "1d3fa22iy7mfh4yyp7j8a5kww37g3z65rbdq1z0p3rv4smz1nddw")))

(define-public crate-polyline-ffi-0.4.5 (c (n "polyline-ffi") (v "0.4.5") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "polyline") (r "^0.9.0") (d #t) (k 0)))) (h "0rvg40xvnz077nrzlk8wa0wwl48mmibz50g5n9vwx141p08npv8p") (f (quote (("headers"))))))

(define-public crate-polyline-ffi-0.5.0 (c (n "polyline-ffi") (v "0.5.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "polyline") (r "^0.9.0") (d #t) (k 0)))) (h "1qpchqy8jvn1r1rj8vfd891ib0cxk0z2ac285hwzj87ja7dhfjyb") (f (quote (("headers"))))))

(define-public crate-polyline-ffi-0.6.1 (c (n "polyline-ffi") (v "0.6.1") (d (list (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "polyline") (r "^0.10.0") (d #t) (k 0)))) (h "1qk1ib9086h121jljfpz3as33ab8yjpapvzvycrf4xl6156gdj57") (f (quote (("headers"))))))

(define-public crate-polyline-ffi-0.6.5 (c (n "polyline-ffi") (v "0.6.5") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "polyline") (r "^0.10.2") (d #t) (k 0)))) (h "0m1kgk98lw8ypwm401rq6hhyvahr18qkdnkybiinwsqchiq03dqg") (f (quote (("headers"))))))

