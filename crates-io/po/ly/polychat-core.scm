(define-module (crates-io po ly polychat-core) #:use-module (crates-io))

(define-public crate-polychat-core-0.1.0 (c (n "polychat-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 2)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.1.1") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.1.0") (d #t) (k 2)))) (h "1ll4vlmqsrb1fpcyyipglzcx56cxpmmkdlcw25m60m9ssjiz5v1g")))

(define-public crate-polychat-core-0.1.1 (c (n "polychat-core") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.1.2") (d #t) (k 0)))) (h "12clg04y4icajp9g74jzi377mvwnrssgvav9vmmknnga3yfqyr1i")))

(define-public crate-polychat-core-0.1.2 (c (n "polychat-core") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.2.0") (d #t) (k 0)))) (h "0k4gn7yld0awrqiwy53wg1fq7p4sjgdj0b6pybfp7rgdfkd17jky")))

(define-public crate-polychat-core-0.1.3 (c (n "polychat-core") (v "0.1.3") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.2.0") (d #t) (k 0)))) (h "13dfsad0h2n9xgxj2isd71kx9cr71yqvrjvlsqap0cg242spvsvy")))

(define-public crate-polychat-core-0.1.4 (c (n "polychat-core") (v "0.1.4") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.3.0") (d #t) (k 0)))) (h "048myhvyxbh63nw0m4d68z98hqxdz97dzc7ambyj4kg7y1y4jhah")))

(define-public crate-polychat-core-0.1.5 (c (n "polychat-core") (v "0.1.5") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "polychat-plugin") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "14mayh2c55mal7vizz1s0nrfnlrrxxvmvsg0f8cwx7vfrm3wrxqc")))

