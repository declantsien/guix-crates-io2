(define-module (crates-io po ly polyerror) #:use-module (crates-io))

(define-public crate-polyerror-0.0.1 (c (n "polyerror") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "0gqcliw7ia5nhgx7jbbaykfrrrd8b2xr09bsf9sbvywjsmdr1mvf")))

(define-public crate-polyerror-0.0.2 (c (n "polyerror") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "1s9s5sqd52r1g7wzflk3bqhls3qy9bbb9md9icqnn8fyf5ki0klz")))

