(define-module (crates-io po ly polymorphic_enum) #:use-module (crates-io))

(define-public crate-polymorphic_enum-0.1.0 (c (n "polymorphic_enum") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0690h0nxvvr45rd4c1yyds2lzm1p3p6b2m3a6jgllbxdljcqna8x")))

(define-public crate-polymorphic_enum-0.1.1 (c (n "polymorphic_enum") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yrvflflv5fm7cdghi255nyw27cw2r3j693qmwjazvlfx1md35gs")))

(define-public crate-polymorphic_enum-0.1.2 (c (n "polymorphic_enum") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cq12minc3vf5qkflhhzfpyvn7r1agqmzwi301fi8lnfl7rcy7fy")))

(define-public crate-polymorphic_enum-0.1.4 (c (n "polymorphic_enum") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a0zdf6m276wxav7f7haarpjfwhhn7cbgb6rv2x2hgd7gqcnk05p")))

(define-public crate-polymorphic_enum-0.1.5 (c (n "polymorphic_enum") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lmrc4awkyvdx6dv9ljwf40zxpwwgcm05h29is3dxdam2kcdqnvf")))

