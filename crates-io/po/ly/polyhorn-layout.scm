(define-module (crates-io po ly polyhorn-layout) #:use-module (crates-io))

(define-public crate-polyhorn-layout-0.1.0 (c (n "polyhorn-layout") (v "0.1.0") (d (list (d (n "polyhorn-style") (r "^0.1.0") (d #t) (k 0)) (d (n "stretch") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "yoga") (r "^0.3.2") (o #t) (d #t) (k 0) (p "polyhorn-yoga")))) (h "073jg1sfcvc99zasczhk38s6mp4h9gyn4ap407xaa1ysy9xvsg9g") (f (quote (("impl-yoga" "yoga") ("impl-stretch" "stretch") ("default" "impl-yoga"))))))

(define-public crate-polyhorn-layout-0.2.0 (c (n "polyhorn-layout") (v "0.2.0") (d (list (d (n "polyhorn-ui") (r "^0.2.0") (d #t) (k 0)) (d (n "stretch") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "yoga") (r "^0.3.2") (o #t) (d #t) (k 0) (p "polyhorn-yoga")))) (h "19amwqsiw84jpllfqba433rzj0iky44qn9slllqmi0rxc6znlrg5") (f (quote (("impl-yoga" "yoga") ("impl-stretch" "stretch") ("default" "impl-yoga"))))))

