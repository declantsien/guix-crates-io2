(define-module (crates-io po ly polychat-plugin) #:use-module (crates-io))

(define-public crate-polychat-plugin-0.1.0 (c (n "polychat-plugin") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "0i6pyi0829c2jk7ckszwixhg1vpdva896jr4lvnr62744ixpgyfb")))

(define-public crate-polychat-plugin-0.1.1 (c (n "polychat-plugin") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "0jyrb5kr7hm2ddya1q145j7radwnkrmzcal8zg8q3q35fsy3196p")))

(define-public crate-polychat-plugin-0.1.2 (c (n "polychat-plugin") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "0a87jx431qx98h3bzrzgwn14lbw3srmbl87k7d16as2qi7bl7c2y")))

(define-public crate-polychat-plugin-0.2.0 (c (n "polychat-plugin") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "17pbpmqqdcflchysrri8f6kghk42z6ijc15agqvv20fsrdp409wy")))

(define-public crate-polychat-plugin-0.3.0 (c (n "polychat-plugin") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "1himhajzppmx10g3hy21y2lf0rqj234mibgsd9j55rqf16z2rc9d")))

(define-public crate-polychat-plugin-0.3.1 (c (n "polychat-plugin") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "0yjbg6bd70q65zkl2p5dzd1caiqkqnfb9zczaj8b65fx6vc9nwvp")))

