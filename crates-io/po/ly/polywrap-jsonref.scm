(define-module (crates-io po ly polywrap-jsonref) #:use-module (crates-io))

(define-public crate-polywrap-jsonref-0.1.0 (c (n "polywrap-jsonref") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "charset"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gn3p5fqs5l02fa85ix4pg20mnrxd9k8ajxbvfc15gslkqh8fjrb")))

