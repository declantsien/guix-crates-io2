(define-module (crates-io po ly polynomical) #:use-module (crates-io))

(define-public crate-polynomical-1.0.0 (c (n "polynomical") (v "1.0.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0dz47w6nxz6z974md61i2rjhavp8a9dvsxdggj55ycnh9mis1ikb") (f (quote (("std" "num/std") ("serde" "num/serde") ("libm" "num/libm") ("default" "std") ("bigint" "num/num-bigint") ("alloc" "num/alloc"))))))

