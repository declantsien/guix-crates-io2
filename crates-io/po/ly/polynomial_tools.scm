(define-module (crates-io po ly polynomial_tools) #:use-module (crates-io))

(define-public crate-polynomial_tools-0.1.0 (c (n "polynomial_tools") (v "0.1.0") (h "1hi53jcz6zdc668fkfyb8b77b5br67bkj0g457265ilcjh32bg21")))

(define-public crate-polynomial_tools-0.1.1 (c (n "polynomial_tools") (v "0.1.1") (h "03w3yygb2clnwns921lprpp56qns39fwn04rxlph4avwd6av92wn")))

(define-public crate-polynomial_tools-0.2.0 (c (n "polynomial_tools") (v "0.2.0") (h "1a8ngbgj1hinkv06asxxpjjm7b9ddmn4byg9b68lmxqidnfi6sfv")))

(define-public crate-polynomial_tools-0.2.1 (c (n "polynomial_tools") (v "0.2.1") (h "02cc3q846rr8gsc7043r9b0qksrk8hdbj9iq3j3ympmyvp4f3c07")))

(define-public crate-polynomial_tools-0.2.2 (c (n "polynomial_tools") (v "0.2.2") (h "1llsmha089p04h0lmrwfs48ygm6c1zrzjpx07dbapnhbhw4nnwm5")))

