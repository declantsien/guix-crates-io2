(define-module (crates-io po ly polystem) #:use-module (crates-io))

(define-public crate-polystem-0.1.0 (c (n "polystem") (v "0.1.0") (h "18p5r500nm1yi89gy6kcq63n625nj68ir45asscvpq64124k70nb")))

(define-public crate-polystem-0.2.0 (c (n "polystem") (v "0.2.0") (h "0q1qs61i9dr02ffdsdrlp3zlc49k3dlqic6kpc7nydlq1z7rp0j6")))

(define-public crate-polystem-0.3.0 (c (n "polystem") (v "0.3.0") (h "1g9kwaqxq3shmq008bma4wryn7asrb34572pq6rcyra6bq2mkir4")))

