(define-module (crates-io po ly polywrap_schemafy_lib) #:use-module (crates-io))

(define-public crate-polywrap_schemafy_lib-0.1.0 (c (n "polywrap_schemafy_lib") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemafy_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uriparse") (r "^0.6") (d #t) (k 0)))) (h "172jkpplx7dspfvnz6aw1jsjf9q4fbrgiva76zz3b842ck0185xg")))

