(define-module (crates-io po ly polynomials) #:use-module (crates-io))

(define-public crate-polynomials-0.1.0 (c (n "polynomials") (v "0.1.0") (h "0ki4jqf6fhjyvrz49lq3hnzwvr76y1zbr4d49p8b12426h3i03rf")))

(define-public crate-polynomials-0.1.1 (c (n "polynomials") (v "0.1.1") (h "1xbpi85n6444mvwxdlmchl5sqkszwv2zx2x1fh3b99nsl3mz5vrp")))

(define-public crate-polynomials-0.2.0 (c (n "polynomials") (v "0.2.0") (h "1jfxdwjdvbzpwnz19b22a133a5rc21rhnlnk4mip99p9fmrc4crx")))

(define-public crate-polynomials-0.2.1 (c (n "polynomials") (v "0.2.1") (h "0yqvfxsbpfrq0fz7vs33021s8j9qf840ip0siycyg8k501l4ahdv")))

(define-public crate-polynomials-0.2.2 (c (n "polynomials") (v "0.2.2") (h "1rl0c79d1wvj5lz7j2xysg38dq08w76prfsz3m973m559wpdp065")))

(define-public crate-polynomials-0.2.3 (c (n "polynomials") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j7k6vpdvzq1bkwjh6d4sr7mw9b6r8cp1h1hb3nsjk3hr8hmcmrx")))

(define-public crate-polynomials-0.2.4 (c (n "polynomials") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kg0y8yq36hzn4nvk83nnaccb3h8lzz1ihh57y17j874y3i56wr0")))

