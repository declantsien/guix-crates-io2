(define-module (crates-io po ly polytone-proxy) #:use-module (crates-io))

(define-public crate-polytone-proxy-1.0.0 (c (n "polytone-proxy") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.4") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.2") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "polytone") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d5ifdzb0606v0j35kankaai90npxvxwq6y18vvc0i93rms3cdqv") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces")))) (r "1.67")))

