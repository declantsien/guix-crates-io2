(define-module (crates-io po ly polygondb) #:use-module (crates-io))

(define-public crate-polygondb-0.1.16 (c (n "polygondb") (v "0.1.16") (d (list (d (n "json_value_remove") (r "^1.0.2") (d #t) (k 0)) (d (n "jsonptr") (r "^0.4.4") (d #t) (k 0)) (d (n "mimalloc-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-websockets") (r "^0.1.6") (d #t) (k 0)))) (h "14ga6sm7vkhk1p89a891pycm6qigsmhrqi4jkbm5j25f5asp1c1k") (y #t)))

(define-public crate-polygondb-1.16.0 (c (n "polygondb") (v "1.16.0") (d (list (d (n "json_value_remove") (r "^1.0.2") (d #t) (k 0)) (d (n "jsonptr") (r "^0.4.4") (d #t) (k 0)) (d (n "mimalloc-rust") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-websockets") (r "^0.1.6") (d #t) (k 0)))) (h "1if99ac2n72g8qsh24gkpr891paaf2flwmja1faj3lmkmmi5lk9a") (y #t)))

