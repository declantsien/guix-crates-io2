(define-module (crates-io po ly polygon2) #:use-module (crates-io))

(define-public crate-polygon2-0.1.0 (c (n "polygon2") (v "0.1.0") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)))) (h "0hgq00l2y62sfa73lrl5wqqpsdvpsp8dh479blzd6a9d06ryg6pm")))

(define-public crate-polygon2-0.1.1 (c (n "polygon2") (v "0.1.1") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "1qgp2wab0yvfbbyp5kxi8sfzvphjhlkb449avpg7n10psacl7gbq")))

(define-public crate-polygon2-0.1.2 (c (n "polygon2") (v "0.1.2") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "0q76vw4b4gb4xyhgs1cy84pl769ldg3qhjgci8q25lizpv73hq2r")))

(define-public crate-polygon2-0.2.0 (c (n "polygon2") (v "0.2.0") (d (list (d (n "number_traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.1") (d #t) (k 0)))) (h "00qy1k4a5nmi9fxvcdrqsrywnwblk430847ky117qskk4c145a6a")))

(define-public crate-polygon2-0.3.0 (c (n "polygon2") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vec2") (r "^0.2") (d #t) (k 0)))) (h "0fk66yvnkxxkqh6v43zb51n688qdj6gid404pbw2i9bagsvf5n63")))

