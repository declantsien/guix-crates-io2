(define-module (crates-io po ly polyp) #:use-module (crates-io))

(define-public crate-polyp-0.1.0 (c (n "polyp") (v "0.1.0") (h "097dg7vzp17674gk6dip9wllzmvgvfp2zl6j4a7xw2rpppmza1xc")))

(define-public crate-polyp-0.2.0 (c (n "polyp") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jifw7wsy19g7ddwahayxhdbwfp4slnvg6lfw46fw409q0pkqzad")))

(define-public crate-polyp-0.2.1 (c (n "polyp") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vyj9rrzcj32zj4g2kkkimz1g28b1izrr7s352p26r2j5jfw6w2l")))

(define-public crate-polyp-0.3.0 (c (n "polyp") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zc66h69j20ljhagkhqrpwbgf4pildzny6rwrb9kd48gvx713m2f")))

(define-public crate-polyp-0.4.0 (c (n "polyp") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzgc8jjlywfmhwx9kqkqkcg524rk1pl0xhq6kb2mvsqccspbvvl")))

(define-public crate-polyp-0.5.0 (c (n "polyp") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hb7a6rzlaabrbfdgz4as8gnx3x1d6vsic5p6vz242pfa6324vr0")))

(define-public crate-polyp-0.5.1 (c (n "polyp") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xfwm5g4379msz2l8qn4av3x8c5rqy5b78izan48aca16ll7rfak")))

(define-public crate-polyp-0.6.0 (c (n "polyp") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17zwym0w3v7vfrqnxgnl7b5d51rdhfdd066pikqcd1njwhny593r")))

(define-public crate-polyp-0.6.1 (c (n "polyp") (v "0.6.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rmxzhvyh016xw4m0b05wbry77ac7xb1fwkbimaq453l0g5cz71s")))

(define-public crate-polyp-0.7.0 (c (n "polyp") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cmjgxc0ka9nbjrhpm6mpkb2z31k82pfw3c909iqz11lgk3ls04l")))

