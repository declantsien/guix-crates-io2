(define-module (crates-io po ly polygon_clipping) #:use-module (crates-io))

(define-public crate-polygon_clipping-0.1.0 (c (n "polygon_clipping") (v "0.1.0") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1frf713xn3r2ah05jv0m9zn3y4qf7wlwzfmpf7hpqj5xp9b1zyyy")))

