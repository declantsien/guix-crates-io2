(define-module (crates-io po ly polyglot_rs) #:use-module (crates-io))

(define-public crate-polyglot_rs-0.1.0 (c (n "polyglot_rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1b290fdacjkcmqcy7y8ibsaxrw09wsk685byms2iwf30b3yzgiks")))

(define-public crate-polyglot_rs-0.6.0 (c (n "polyglot_rs") (v "0.6.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "0r91m3dx0lg3s4sbq1vwifg4fk1dmk1app10kdfxzdbmflp3ad3n")))

(define-public crate-polyglot_rs-0.6.1 (c (n "polyglot_rs") (v "0.6.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1179ivyj64nrc26hi0786fnb428v3yrz88ficai6zs1afj0pqr0y")))

(define-public crate-polyglot_rs-0.7.0 (c (n "polyglot_rs") (v "0.7.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1ywmjclw357i6r1f33ilzidr3s8dl51djlxfk1w1p761ibdjf6nj")))

(define-public crate-polyglot_rs-1.1.0 (c (n "polyglot_rs") (v "1.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "1gayrzhn5a45nhm19j342z20i434c2fkhw9by89sy7h7x45gv559")))

(define-public crate-polyglot_rs-1.1.1 (c (n "polyglot_rs") (v "1.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1g63m6sywnm7mx1zgmch2jxqv0hlrryvhx8k5ywr610ibj2gz5pr")))

(define-public crate-polyglot_rs-1.1.2 (c (n "polyglot_rs") (v "1.1.2") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "0inh7fwrx8cd8y883ralgcg1bnfy3h7m68c0zkw8kpmi4wyszf4p")))

(define-public crate-polyglot_rs-1.1.3 (c (n "polyglot_rs") (v "1.1.3") (d (list (d (n "base64") (r "^0.21.3") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "1fnwrgxzf9v64l8lhnm408mpfrylmc8gbi7i46gfvhfa79inpc1a")))

(define-public crate-polyglot_rs-1.1.4 (c (n "polyglot_rs") (v "1.1.4") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0bgx2b9i6j8zw5mdsgzmdr536vqbz5q86zsi6ql5l6wy4dwmd54h")))

(define-public crate-polyglot_rs-1.2.0 (c (n "polyglot_rs") (v "1.2.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0zlqqlznngskn5qz5csx4v1x3mmicvvbch222pnxqvvikliaa8p0")))

(define-public crate-polyglot_rs-1.2.1 (c (n "polyglot_rs") (v "1.2.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "1an9mzabs0fi0m2537w5575wdlcqqi4g78jn70kvprz8sbvrya5g")))

(define-public crate-polyglot_rs-1.2.2 (c (n "polyglot_rs") (v "1.2.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "1lxf3f8vm5gqicivc80040bn9i85k14c4w2sxa6gic35qbq9a0j0")))

(define-public crate-polyglot_rs-1.3.0 (c (n "polyglot_rs") (v "1.3.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0sifv8lf72i770fiwmc7sc3m2zbsv8jlv7l6jq9d7ljqg01ch6bp")))

