(define-module (crates-io po ly polymers) #:use-module (crates-io))

(define-public crate-polymers-0.1.0 (c (n "polymers") (v "0.1.0") (h "1v5mglmj3lg25fk1vz0h3xnyj40l619n537k451vxrn3nzpc0jgg") (y #t)))

(define-public crate-polymers-0.0.0 (c (n "polymers") (v "0.0.0") (d (list (d (n "blas-src") (r "^0.8") (f (quote ("openblas"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("blas"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14.1") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.10") (f (quote ("cblas" "static"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hmbnx4rsv1wrlny1h5qrn83svbdbypdsdi5wj6a6nadcgawgbnv")))

(define-public crate-polymers-0.0.1 (c (n "polymers") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "155bg7nl7bn52zbjlv6l1gbvxfpw3ndqlmv4yvvcl2sgx9hy1817")))

(define-public crate-polymers-0.0.2 (c (n "polymers") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xbxcsr28i3cah4610l2sxhbn7ws316kzsm8agn94insr49pqwa3")))

(define-public crate-polymers-0.0.3 (c (n "polymers") (v "0.0.3") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0naby3da151xs7i5wyk95smk9b030qfbnwp9j9v506pk3a12d8d6") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.4 (c (n "polymers") (v "0.0.4") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xpk52vl80ynrfn3rvnyl79p02ymnls5g8bg8lacjvwbxm6gxcvc") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.5 (c (n "polymers") (v "0.0.5") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15rf6bxzkqikl3vkgzhh3ckrk7fl5gx4alh6w02dszrqlhm8rdw6") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.6 (c (n "polymers") (v "0.0.6") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12hqf3famk6n8bwdgmq31q8xi02q3qkvf9b6qj0am7smrmpp86kg") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.7 (c (n "polymers") (v "0.0.7") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nal374l094z1ny2qf93g5vabfchjh44s45s1vb50h4q82w7d6rm") (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.8 (c (n "polymers") (v "0.0.8") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1pin6hwrhc919n8bp0w10m7331hgwxvd8w153hifvmkpq885mqgz") (f (quote (("julia")))) (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.9 (c (n "polymers") (v "0.0.9") (d (list (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1194fqj1jwickydnpsh7bdwphgi2vzvsvmcz07ca6npr2xgyfccd") (f (quote (("julia")))) (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-polymers-0.0.10 (c (n "polymers") (v "0.0.10") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1l64alinrh58yks64jy2nrpma2wby33n5mrr6jnh6wd1cj5v8ipk") (f (quote (("julia")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.0.11 (c (n "polymers") (v "0.0.11") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07rqk534sdl2v251g84945sy9lh4z9cyb83yj3741zbm3vv9ddd7") (f (quote (("julia")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.0.12 (c (n "polymers") (v "0.0.12") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1s9ygwnljfhasanrjrqzs9wksvqb0a8avax1pbyi7mpgbdi32i5b") (f (quote (("julia")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.0.13 (c (n "polymers") (v "0.0.13") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wjvk8kdrjj63wxzlmx7cla1fn7k3r577rfyywb20qjaghhxp4hw") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.1.1 (c (n "polymers") (v "0.1.1") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0sp9qxf36i8zc875d81c1pb84xwmz7vm67lhx1pj94rprbj6h4k8") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.1.2 (c (n "polymers") (v "0.1.2") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17r40pk2kkj8c1r9j76aswc702qplvdf4sgqmk5g1jmlybg66jgx") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.1.3 (c (n "polymers") (v "0.1.3") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hs14zb22qrqsg70pcx6jwcnnrdd3zlf53gf4zbxv0mzcm217y5i") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.1.4 (c (n "polymers") (v "0.1.4") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0y8x6bvzrvp4rz1k6wydknmvafh112y5z5brxclhbjlzfvpvq7cz") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.2.0 (c (n "polymers") (v "0.2.0") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02546zr0lb6gvl34x8m99bg436qimxvbr0i6807bwvz5yavxymrd") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.2.1 (c (n "polymers") (v "0.2.1") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0cfnfsb0dpl18w7qzz6v13gzg61vzb138idl9xwhzyr41jw398vr") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.0 (c (n "polymers") (v "0.3.0") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bg7p9z55waqg9pq81lxhmiaapr76a7i77yjhbhzppcznr64dcch") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.1 (c (n "polymers") (v "0.3.1") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ba900iw780vldfnq11ygjz3ygnp7wkjjiyarfandxmriglh6hk7") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.2 (c (n "polymers") (v "0.3.2") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "148k77fnzgd6sh9n5g879bx0bw04xnbd3a352fh596fpgv7bdlf8") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.3 (c (n "polymers") (v "0.3.3") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xmzmw8bw3p02s9jk3f6aqnzvyh6jdp8mxyjaypdkb9dln84p52n") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.4 (c (n "polymers") (v "0.3.4") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lm4nb1bf2ivyn7dbwp5cp3wpi145rb3brl224rfbgc2ccdhq0yq") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.5 (c (n "polymers") (v "0.3.5") (d (list (d (n "numpy") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0di2717lxlmsp7cwm901myinmmgh1fkmx41zbg2cih2x0sx71hlq") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.6 (c (n "polymers") (v "0.3.6") (d (list (d (n "numpy") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vv3cxlpyrmy5c67r7wqfqv9pb4dqk9yrc2n17xrfrk72rg35g7f") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

(define-public crate-polymers-0.3.7 (c (n "polymers") (v "0.3.7") (d (list (d (n "numpy") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "027frkz0kh31c3v6mkrfr03fghzwc9a8mxijmwxhgag2zc395hqi") (f (quote (("extern")))) (s 2) (e (quote (("python" "dep:numpy" "dep:pyo3"))))))

