(define-module (crates-io po ly polyhorn-ui-macros) #:use-module (crates-io))

(define-public crate-polyhorn-ui-macros-0.2.0 (c (n "polyhorn-ui-macros") (v "0.2.0") (d (list (d (n "casco") (r "^0.2.0") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "polyhorn-ui") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "11r5nv7v1c968zvy03s4an6i8ri0wi25wmlglgl0abfsrjjfifnc")))

(define-public crate-polyhorn-ui-macros-0.3.0 (c (n "polyhorn-ui-macros") (v "0.3.0") (d (list (d (n "casco") (r "^0.3.0") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "polyhorn-ui") (r "^0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "041xi2m90z3s0gm6bcjaas8i7idclpq3pn5qxrxifccb364bngd1")))

(define-public crate-polyhorn-ui-macros-0.3.1 (c (n "polyhorn-ui-macros") (v "0.3.1") (d (list (d (n "casco") (r "^0.3.0") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "polyhorn-ui") (r "^0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "06h746jba50p2hwb2j336zvjfasg9mdw1a6ym7dwrdb3awhwd3b8")))

(define-public crate-polyhorn-ui-macros-0.3.2 (c (n "polyhorn-ui-macros") (v "0.3.2") (d (list (d (n "casco") (r "^0.3.2") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "polyhorn-ui") (r "^0.3.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0vpmrv53w10phw4b81bl8v17x0rs0gg2s811pdpid6jv6vziv2c5")))

(define-public crate-polyhorn-ui-macros-0.4.0 (c (n "polyhorn-ui-macros") (v "0.4.0") (d (list (d (n "polyhorn-ui") (r "^0.4.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)))) (h "0gbxnpfcaq2abzcdzs1qa4gdq24qb3kv066xn4avh0pgl6017pa2")))

