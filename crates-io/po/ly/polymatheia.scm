(define-module (crates-io po ly polymatheia) #:use-module (crates-io))

(define-public crate-polymatheia-0.0.0 (c (n "polymatheia") (v "0.0.0") (h "0ch7jybjhk7kiyyjhsa6sqfqqypgpfhb4v3svww95sagz9hgarpg")))

(define-public crate-polymatheia-0.1.0 (c (n "polymatheia") (v "0.1.0") (h "05k5drcz705gng68hiyzmxf2gxcb1gmji5bzivgdj0vbv7vmkwgy")))

(define-public crate-polymatheia-0.2.0 (c (n "polymatheia") (v "0.2.0") (h "1g1q07vj86mxg0c4jqzm7fwcy0idqvwwh1xwwjanmq0pd37k7myv")))

(define-public crate-polymatheia-0.3.0 (c (n "polymatheia") (v "0.3.0") (h "033bbq61h6x51cvkg2zw5217rbd604innhzcby2ghvi3z5m3qnal")))

(define-public crate-polymatheia-0.4.0 (c (n "polymatheia") (v "0.4.0") (h "0k63lnvzlicf5p58h0q8j619waj64nnpgbhi5y00xz7xh1wvd75y")))

(define-public crate-polymatheia-0.4.1 (c (n "polymatheia") (v "0.4.1") (h "1wcfyw8mihsgxln0kc95s7497xhjh1xidcnzqmhp9jy0ligvclak")))

(define-public crate-polymatheia-0.4.2 (c (n "polymatheia") (v "0.4.2") (h "0b16dx4qkz7wralxnfaprdz3gq05q48nw297a37368xlcrvi2hpa")))

(define-public crate-polymatheia-0.4.3 (c (n "polymatheia") (v "0.4.3") (h "019v48q8liymv8xqml1k3dl181q9nizzx735sadd681w2w3n0rfz")))

