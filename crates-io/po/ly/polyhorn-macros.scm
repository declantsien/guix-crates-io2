(define-module (crates-io po ly polyhorn-macros) #:use-module (crates-io))

(define-public crate-polyhorn-macros-0.1.0 (c (n "polyhorn-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "0ymcp81bn391xicim3zy0jyb3hi7gbmp3jyhk7n0m6jz5ry5v6cd")))

(define-public crate-polyhorn-macros-0.2.0 (c (n "polyhorn-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "0xhkn2qk5zk3vprw9vck5d9masg684m65000nfxb8d3l5rkq2b2a")))

(define-public crate-polyhorn-macros-0.3.0 (c (n "polyhorn-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "1zh84wbrxvj95wdmvz9r9ymyr13xi86yz2is62c7hgyc4i609dv8")))

(define-public crate-polyhorn-macros-0.3.1 (c (n "polyhorn-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "152f0dd9m2i2gf3s3xmmmm4vbbi15kn4s2vm15344m21989vnk6p")))

(define-public crate-polyhorn-macros-0.3.2 (c (n "polyhorn-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "1yi1q2qs82jph0lx3g1krqqfsncxd5hv1rm1y3dknn6djs6501wv")))

(define-public crate-polyhorn-macros-0.4.0 (c (n "polyhorn-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)))) (h "0axfhidvg9x27ypfvy03b63mcvjmbfn8lbq7rc5p8d2m30lw24ix")))

