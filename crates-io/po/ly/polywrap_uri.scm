(define-module (crates-io po ly polywrap_uri) #:use-module (crates-io))

(define-public crate-polywrap_uri-0.1.6-beta.8 (c (n "polywrap_uri") (v "0.1.6-beta.8") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "02xb7gk9n3h3c94sjpqar5q7j59yrlf87xjjca7dd95q6mvyyygl") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.9 (c (n "polywrap_uri") (v "0.1.6-beta.9") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1cjv4dab97wvhcygnfkpspfizlr55ym0lys8rz4w5p1jmpad5f") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.10 (c (n "polywrap_uri") (v "0.1.6-beta.10") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vik64g7awh5d0s0rc0hqvsa9ibkyp6l0rrqs11c78bwh5gc4dvy") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.11 (c (n "polywrap_uri") (v "0.1.6-beta.11") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "01f6laq4v46zdn2rqqlph3gaynpbn1j4g3mpijaq20zy4hxi3n9h") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.12 (c (n "polywrap_uri") (v "0.1.6-beta.12") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gi9qs6dshbh046k4rmwr66zhw257ywcdkbf5fnkzlp3dinj7yvs") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.13 (c (n "polywrap_uri") (v "0.1.6-beta.13") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "02g8859mkfq7qs6k0x64zkck6pvghnyv04l49yvzajf20yz9rghl") (r "1.69")))

(define-public crate-polywrap_uri-0.1.6-beta.14 (c (n "polywrap_uri") (v "0.1.6-beta.14") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iybjx3rgb2c0l084pj8kvxz20p698sp6ryllvrbizf20xy22gzb") (r "1.69")))

(define-public crate-polywrap_uri-0.1.7 (c (n "polywrap_uri") (v "0.1.7") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ficmxcys00f47y4py61jw9ryywim0wka1kn9k0h5c47s787wdb") (r "1.69")))

(define-public crate-polywrap_uri-0.1.8-beta.2 (c (n "polywrap_uri") (v "0.1.8-beta.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m3vmx0d6jm4cmf55yfrcy8zclkkf2s6rvgdqmadqwkw083gd02c") (r "1.69")))

(define-public crate-polywrap_uri-0.1.8 (c (n "polywrap_uri") (v "0.1.8") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "03fcv8pa6ncj3g2d0r8x262zj2dr6k1inpcylaqpn33b4k9wlrl3") (r "1.70")))

(define-public crate-polywrap_uri-0.1.9-beta.1 (c (n "polywrap_uri") (v "0.1.9-beta.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ljsqs47s2id98bnq28vvgxk2h6bjcgc0nyrxwgjxwjj5mv9hhjh") (r "1.70")))

(define-public crate-polywrap_uri-0.1.9-beta.2 (c (n "polywrap_uri") (v "0.1.9-beta.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "12sg276zkcxly2rhhw44s3dwqghcqvw1xgdjvmaw0jrak3mlbilk") (r "1.70")))

(define-public crate-polywrap_uri-0.1.9 (c (n "polywrap_uri") (v "0.1.9") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ib7yaip4i3vn7hk6wsk7s252drjy2q82zzavc05srlh1v7z4wl") (r "1.70")))

(define-public crate-polywrap_uri-0.1.10 (c (n "polywrap_uri") (v "0.1.10") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "08gifrblnxdlpl8i58pqi2a99n634zajx3jdccpa1n9ngf6cbj2b") (r "1.70")))

(define-public crate-polywrap_uri-0.1.11 (c (n "polywrap_uri") (v "0.1.11") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hykkzmx9ndv32svq8049mpn86jp2x5hvcaq1bjsznpmpwa9hs7s") (r "1.70")))

