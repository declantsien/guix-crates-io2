(define-module (crates-io po ly polykill) #:use-module (crates-io))

(define-public crate-polykill-0.1.0 (c (n "polykill") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "0g05s7a5gy4b0hi13cf34f583680v42qx21vihr5qaxkgrm21ng1")))

(define-public crate-polykill-0.2.0 (c (n "polykill") (v "0.2.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "1dj69a25n2h94a5cwkib5yvb629mgv9hrnwhw2m82lzzxj2pmqvz")))

(define-public crate-polykill-0.3.0 (c (n "polykill") (v "0.3.0") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "1z1qhx5n1r56bpl6jf41r7p85xsx4fg6dc2s2di2pkxaym8fs9i2")))

(define-public crate-polykill-0.3.1 (c (n "polykill") (v "0.3.1") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "0madllp50qy5vr9l3jwpiixxc002kwpa1f06nxbjkwhnb6rh553q")))

(define-public crate-polykill-0.3.2 (c (n "polykill") (v "0.3.2") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "0dacmdpi70cn71d45vzj4lb00459n374mxprb8jv4g26b616mdsc")))

(define-public crate-polykill-0.3.3 (c (n "polykill") (v "0.3.3") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "108dd2jgsifz9lkgs6nrzgjqgasq99jg792wpw1p396x1dixn8nz")))

(define-public crate-polykill-0.4.0 (c (n "polykill") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "18rgsr1nlyhjc3ibmzhbppz17g0f2afbhzc1p26nd61jzjjjj52r")))

(define-public crate-polykill-0.4.1 (c (n "polykill") (v "0.4.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "1m0dgdb69bs5wv1jk85xicaxzzjlb3lgzjvqa63y6fbwbqq5f2y9")))

(define-public crate-polykill-0.5.0 (c (n "polykill") (v "0.5.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "0dg100sw89gdhf7l0qkc4xkkm9inqqjgsa15ms9k2gqfjlzfgr71")))

(define-public crate-polykill-0.5.1 (c (n "polykill") (v "0.5.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "1py9j2sck41ipc0wd5saisbhyznb4yd392zwsa152l0cznkc3lzj")))

(define-public crate-polykill-0.5.2 (c (n "polykill") (v "0.5.2") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "16wkb64k8kmqfgv54bgc8aigrq5ya8f8p8lwzl7j2ig162z8r4qp")))

(define-public crate-polykill-0.5.3 (c (n "polykill") (v "0.5.3") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "1n7ix9sp0i52fzw67bz5g36vac0795ma5smrjnhgi7wf19hz9j20")))

(define-public crate-polykill-1.0.0 (c (n "polykill") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1980n0qmdb1jl8sx1iqhjkmiqv7bs89l38vbj1af0n49yf4v1yyq")))

(define-public crate-polykill-1.1.0 (c (n "polykill") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1m9whiz0p7xpk6isv6jgh6mzj73987h6dngbklrgzxxblsxrd0gj")))

(define-public crate-polykill-1.1.1 (c (n "polykill") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0vzsfpgd1s7z1gwy57721cdq9bszqzd07jvc2ydical6spfx1yg3")))

(define-public crate-polykill-1.1.2 (c (n "polykill") (v "1.1.2") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0vbwj7h8yyhnp3fwd56mh9aw2h1wihr587nhjqcihk1ymw15q7gc")))

(define-public crate-polykill-1.2.0 (c (n "polykill") (v "1.2.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "169i16lpg8xqmwpcwblszqmjv084plr4r1smpz6z67gcs6g2dby4")))

(define-public crate-polykill-1.2.1 (c (n "polykill") (v "1.2.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1h4cjvv061bbiln3qznmi8s2qg5zzhm7p0cw2knn55jf4lwi86ng")))

(define-public crate-polykill-1.2.2 (c (n "polykill") (v "1.2.2") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1589yr7m8psjmf2svzf9593q50i22m5ibfax7238xys6xzbmgajn")))

(define-public crate-polykill-1.2.3 (c (n "polykill") (v "1.2.3") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1yig08a0hgjy1rphi8klw0hbm33gmhmrgxnccgnipfxyc8vdd89n")))

(define-public crate-polykill-1.2.4 (c (n "polykill") (v "1.2.4") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "036irwzhlpijkc7g6qy1fagz2gkbiagv4qa9511q0az2fgpwmj4j")))

(define-public crate-polykill-1.2.5 (c (n "polykill") (v "1.2.5") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "072rlp0gi891jkjfha871ixwgn0f202fhrg019kmh9z9cd6vx327")))

(define-public crate-polykill-1.2.6 (c (n "polykill") (v "1.2.6") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1aihbg3f7chd1j9ajpmmhsqp8yi1m1xn4haksp001qk2vqn25w1l")))

(define-public crate-polykill-1.3.0 (c (n "polykill") (v "1.3.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1isah8i4m2d617ahmipf0nhvrqq26avgq9rgsfppvwj8h5dmsph7")))

(define-public crate-polykill-1.3.1 (c (n "polykill") (v "1.3.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "~4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1zw1nc0k0204kis2rhn64mgaa4pgqa37mfbvp7bak56rk58dcmyp")))

