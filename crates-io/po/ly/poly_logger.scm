(define-module (crates-io po ly poly_logger) #:use-module (crates-io))

(define-public crate-poly_logger-0.1.0 (c (n "poly_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0mwdynq1zggs2v0jz29dpdcrjqqjrl0sapdhdla71a67baq96idw")))

(define-public crate-poly_logger-0.1.1 (c (n "poly_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1mq1bvf6jj7112dlvzyshb2jgsdnf3svkpv1rs9fxmwg0kk0l8dm")))

