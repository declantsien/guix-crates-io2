(define-module (crates-io po ly polyglot_tokenizer) #:use-module (crates-io))

(define-public crate-polyglot_tokenizer-0.1.0 (c (n "polyglot_tokenizer") (v "0.1.0") (d (list (d (n "logos") (r "^0.11.0") (d #t) (k 0)))) (h "0xpi828cmxm6npivgxfqxhzal9ibxqrjjl0j0z60w4djz80wpvb2")))

(define-public crate-polyglot_tokenizer-0.1.1 (c (n "polyglot_tokenizer") (v "0.1.1") (d (list (d (n "logos") (r "^0.11.0") (d #t) (k 0)))) (h "13nv3y1bxpl2i38n7mazjbhxgs88bzjkzx9msfdd19ilwmijw32y")))

(define-public crate-polyglot_tokenizer-0.1.2 (c (n "polyglot_tokenizer") (v "0.1.2") (d (list (d (n "logos") (r "^0.11.0") (d #t) (k 0)))) (h "083j2j4hcvwyp5b4vjpmasah2fs7hgk5bms8f8hrk87kxcs39a6w")))

(define-public crate-polyglot_tokenizer-0.1.3 (c (n "polyglot_tokenizer") (v "0.1.3") (d (list (d (n "logos") (r "^0.11.0") (d #t) (k 0)))) (h "1iq8x6x2zrkhzza6kgi5wnz330szylhaqhjqbbdjvf9wqidkklm5")))

(define-public crate-polyglot_tokenizer-0.2.0 (c (n "polyglot_tokenizer") (v "0.2.0") (h "0ql92nl6lck5wwjcyad43qp5pb5w800vx7vvax3129vb7fs3ybvx")))

(define-public crate-polyglot_tokenizer-0.2.1 (c (n "polyglot_tokenizer") (v "0.2.1") (h "17chxxw5wqhhpqnvj9wsh9dn0da0l7bsfv17ajqkk0n5sf31a2fn")))

