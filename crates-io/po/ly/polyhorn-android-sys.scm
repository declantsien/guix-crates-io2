(define-module (crates-io po ly polyhorn-android-sys) #:use-module (crates-io))

(define-public crate-polyhorn-android-sys-0.3.0 (c (n "polyhorn-android-sys") (v "0.3.0") (d (list (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.0") (d #t) (k 1)))) (h "009y6sf78m97ajzss7hvik5f1gjmmqgwqy9gx84caa2llfz18p2q")))

(define-public crate-polyhorn-android-sys-0.3.1 (c (n "polyhorn-android-sys") (v "0.3.1") (d (list (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.0") (d #t) (k 1)))) (h "1cmnrk2ani0mnf2wdhrga9zyqvgjkbwbraiw9cm1zwsgwmvhsing")))

(define-public crate-polyhorn-android-sys-0.3.2 (c (n "polyhorn-android-sys") (v "0.3.2") (d (list (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.2") (d #t) (k 1)))) (h "0l4yl0cdi1w50zn6affb4kp4jaxxdxqgkg1sdwfcd6jxd1qf8lpp")))

(define-public crate-polyhorn-android-sys-0.4.0 (c (n "polyhorn-android-sys") (v "0.4.0") (d (list (d (n "jni") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.4.0") (d #t) (k 1)))) (h "1fpq9f4420zx6kr395x9gras8ya56sncp4cqp6wpqsyqm8nc5xr8")))

