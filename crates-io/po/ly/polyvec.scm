(define-module (crates-io po ly polyvec) #:use-module (crates-io))

(define-public crate-polyvec-0.1.0 (c (n "polyvec") (v "0.1.0") (d (list (d (n "raw-parts") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11qg6mpc0g9jspvaxcg7vaasxk4nx1gp3nl0l0l3ca4f6shmbp7p")))

(define-public crate-polyvec-0.1.1 (c (n "polyvec") (v "0.1.1") (d (list (d (n "raw-parts") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p7nwss3scq8i72djs0c6s4qk1d1l7yjykd9318bxa73xi0cgyfr")))

