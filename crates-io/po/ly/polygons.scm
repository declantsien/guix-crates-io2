(define-module (crates-io po ly polygons) #:use-module (crates-io))

(define-public crate-polygons-0.1.0 (c (n "polygons") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0j133xxrn3i0l8jn7gfxbq38nbgvfipr4dnxdpl31fj8vj7y76l6")))

(define-public crate-polygons-0.1.1 (c (n "polygons") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "07m4bzyxgr73nqjrxj4jq7sbqwrif26zbdg9xkgv1ssdmmvl1j1j")))

(define-public crate-polygons-0.1.2 (c (n "polygons") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0hbid52l4p2316pljjs31fhm3f514rfawn714qy3pb3qqgpy21hg")))

(define-public crate-polygons-0.1.3 (c (n "polygons") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "16z8xc68jh27c2m2h91f8qw26qizsnvlirbn4lpkrzaj3l6fh2af")))

(define-public crate-polygons-0.1.4 (c (n "polygons") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1w642nc1956wwymc1sii745yjsc0hkqf3cpz7sqyy9nqlkfbr1gb")))

(define-public crate-polygons-0.2.0 (c (n "polygons") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.13") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "110bwfbk2n492x1xxysy7vm9nmq4znjnxjkvc8rzgyp67cbv4ww6")))

(define-public crate-polygons-0.3.0 (c (n "polygons") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.13") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0if5xkqk75bkcv7lh9bwmp4mmsdvc4nh3s3s61priajpai36yagq")))

(define-public crate-polygons-0.3.1 (c (n "polygons") (v "0.3.1") (d (list (d (n "pyo3") (r "^0.13") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "0zxj1177mg5ifmngihm1xml6jafsp19s3rmxcg53bmrk1rlw9a03") (f (quote (("default" "rayon" "pyo3"))))))

(define-public crate-polygons-0.3.2 (c (n "polygons") (v "0.3.2") (d (list (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "048mh6nxri4vlld9byp7w11bhmg1md14mxwfc9svdapxywxddric") (f (quote (("default" "rayon" "pyo3"))))))

(define-public crate-polygons-0.3.3 (c (n "polygons") (v "0.3.3") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "0650j851a8yhh630qdmixh27hn3gd6rqjalc2qg7bh0f0qk8v24s") (f (quote (("default" "rayon" "pyo3"))))))

