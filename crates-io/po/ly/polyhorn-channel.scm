(define-module (crates-io po ly polyhorn-channel) #:use-module (crates-io))

(define-public crate-polyhorn-channel-0.1.0 (c (n "polyhorn-channel") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhorn-core") (r "^0.1.0") (d #t) (k 0)))) (h "153iw5l4shgqf6smjrpnvdhzlhip376mln8jx6apiwj6gm8scxhx")))

(define-public crate-polyhorn-channel-0.2.0 (c (n "polyhorn-channel") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhorn-core") (r "^0.2.0") (d #t) (k 0)))) (h "0877fjav2pc3g8ib7ni487knmdwfkpl43i4njjjvpm4wsm7882my")))

(define-public crate-polyhorn-channel-0.3.0 (c (n "polyhorn-channel") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhorn-core") (r "^0.3.0") (d #t) (k 0)))) (h "1v8vr8s7s59wbvwz9dc52mvj2q7sv0si449qxf1bmwchn8y7kgki")))

(define-public crate-polyhorn-channel-0.3.1 (c (n "polyhorn-channel") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhorn-core") (r "^0.3.0") (d #t) (k 0)))) (h "0jy1zphp4c27zcng6088xc583n7l55nmxvsl6fgixd8qiqd5p405")))

(define-public crate-polyhorn-channel-0.3.2 (c (n "polyhorn-channel") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhorn-core") (r "^0.3.2") (d #t) (k 0)))) (h "1ppjry5sldkyk88drs1gb3jz7kvmrc4qsabnaksfri8akgllrcv9")))

