(define-module (crates-io po ly polyester) #:use-module (crates-io))

(define-public crate-polyester-0.1.0 (c (n "polyester") (v "0.1.0") (d (list (d (n "coco") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "synchronoise") (r "^0.4.0") (d #t) (k 0)))) (h "1q8znhf81sizb96s6bslrn8fbwgg37khaggjzhxdq18w6jlhn3dj")))

