(define-module (crates-io po ly polygraph-macro) #:use-module (crates-io))

(define-public crate-polygraph-macro-0.1.0 (c (n "polygraph-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tinyset") (r "^0.4.2") (d #t) (k 0)))) (h "0k29r7xkmqfky7741ic3jw29dcg6882kszvbnxhrqdll3ssiz3m8")))

(define-public crate-polygraph-macro-0.1.2 (c (n "polygraph-macro") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tinyset") (r "^0.4.2") (d #t) (k 0)))) (h "00lmzz8xwb47ldirga2hdhmjarkwlh0fklrww7b7078c0330cmpn")))

