(define-module (crates-io po ly poly2tri-rs) #:use-module (crates-io))

(define-public crate-poly2tri-rs-0.1.0 (c (n "poly2tri-rs") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "askama") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "0fhfxpx0y1d4lcrm3n2sfd0yzw6vzn5p6d3ns7298xqdb3ik28dv")))

(define-public crate-poly2tri-rs-0.1.1 (c (n "poly2tri-rs") (v "0.1.1") (d (list (d (n "robust") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "askama") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "13x33fbhjvppk73qw2iw68a29wxgi2i677b14vd5imkznr4l70ly")))

(define-public crate-poly2tri-rs-0.1.2 (c (n "poly2tri-rs") (v "0.1.2") (d (list (d (n "robust") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "askama") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "170363n3yiyyjly8ch8fqdir156kijavw4jn4wx3b5ly2qcy21b7")))

