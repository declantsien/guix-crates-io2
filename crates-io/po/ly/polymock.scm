(define-module (crates-io po ly polymock) #:use-module (crates-io))

(define-public crate-polymock-0.1.0 (c (n "polymock") (v "0.1.0") (h "07j8k0j32l69vsikmd2wdcd647cgrs5xk0mjp54ql181j0sggjs8")))

(define-public crate-polymock-0.2.0 (c (n "polymock") (v "0.2.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)))) (h "0yb5h4zb0l1mqqg85x5mv50rrz1gcmnpg7jpxaf9kd3j5dlv2568") (f (quote (("std") ("default" "std"))))))

(define-public crate-polymock-0.2.1 (c (n "polymock") (v "0.2.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)))) (h "026n8qnlkr7qi5n25bf9wylfank7ignil7vvb95gsfz3ch69n57c") (f (quote (("std") ("default" "std"))))))

(define-public crate-polymock-0.2.2 (c (n "polymock") (v "0.2.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)))) (h "1n7jf9m7j1iihwfg37pnnm6i2yys4bxxm0pi3z7zs7q2k09czcmp") (f (quote (("std") ("default" "std"))))))

