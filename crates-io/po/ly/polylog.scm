(define-module (crates-io po ly polylog) #:use-module (crates-io))

(define-public crate-polylog-0.1.0 (c (n "polylog") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0g10ygrcqcc7k55w0nldqpgjnqq5w4d5v15gylhqydi43y6zzh7z")))

(define-public crate-polylog-0.2.0 (c (n "polylog") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "11k5c0n3vbc20iwjy1wwl4qam6d7220642p1f528xixkjyzcc5q4")))

(define-public crate-polylog-0.2.1 (c (n "polylog") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "03valgnzm7x16hk11yrxc1fccny2ygl0x8icdhsg35whp3rxybq0")))

(define-public crate-polylog-0.2.2 (c (n "polylog") (v "0.2.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "01ffsx5chsvk6ipmy9aw9hvgx4a2f07lqm72dkd7gg0lhx1a69rx")))

(define-public crate-polylog-0.2.3 (c (n "polylog") (v "0.2.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "13qnifghqfhd66jp18gpmgryqbllb6yp263m6260w78q0pskb875")))

(define-public crate-polylog-0.2.4 (c (n "polylog") (v "0.2.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0k38hr6ccp2q32sbh6i98d89ihp1q1q748y73wyxq5gfa98ay79l")))

(define-public crate-polylog-0.3.0 (c (n "polylog") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0430mx2j9aih7rrv159ywjc7v4apwfv52n4d0c0pd518sf27wqw6")))

(define-public crate-polylog-0.3.1 (c (n "polylog") (v "0.3.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "044hhdwlnl63jk5sc5gz47qspgl47jcsklj1gj77c96vygjb0zig")))

(define-public crate-polylog-0.3.2 (c (n "polylog") (v "0.3.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1ci79h61zdm5zp10rjv53ljh3kp45a03nmkbzzf3wm72wjlldg22")))

(define-public crate-polylog-1.0.0 (c (n "polylog") (v "1.0.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "071bxj41bvm883magkm3d5zicvhbxla7x3khshfbak1w0kx0k8s7")))

(define-public crate-polylog-1.1.0 (c (n "polylog") (v "1.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1blcfvyg98q41bk77i8dfnypf6rdbhcm61zh8ffh4hzy8vnrpj40")))

(define-public crate-polylog-1.1.1 (c (n "polylog") (v "1.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0gj72p1fqw4qjsm28b7s9nhl7gcn3hr5a7ap0rlh9qpq0ls4hqfh")))

(define-public crate-polylog-1.1.2 (c (n "polylog") (v "1.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1yv2x5ssi54c60zmn1x86w365i2z8c5w331fbcwxai6rx0cbw2pj")))

(define-public crate-polylog-1.1.3 (c (n "polylog") (v "1.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1y2p80m9wqb0rc39cfiwjs4mxd8x68w7gpdx7d43lsdsinzw5i1a")))

(define-public crate-polylog-1.2.0 (c (n "polylog") (v "1.2.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gcrzyvgzagnmln075an12s5prlss9kjnlks4l28ay1f2ylvw6hd")))

(define-public crate-polylog-1.2.1 (c (n "polylog") (v "1.2.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1ap40figys6minswq952ykhchi0qrvp9fpl89yy8wq1l4wh9niia")))

(define-public crate-polylog-1.3.0 (c (n "polylog") (v "1.3.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ljbqrzicg3sx9b4y2gij2smbzycn2zxy0mss5fa54binzpyk1y9")))

(define-public crate-polylog-1.4.0 (c (n "polylog") (v "1.4.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xanqvyqqr1ljhlr2fvhg89vad858xzfpwf1k7z71shnq8zjlsxr")))

(define-public crate-polylog-1.5.0 (c (n "polylog") (v "1.5.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0anynw53mdmdbhsgfsd16577jrglicipxjzz0zfdxqgdi0acfzyw")))

(define-public crate-polylog-1.6.0 (c (n "polylog") (v "1.6.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1kdllsrh4cif8qbsbshcl7kp1cjwm57hfwfg1k7jcwvh61ind79y")))

(define-public crate-polylog-1.6.1 (c (n "polylog") (v "1.6.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "03h5m9p9wgm22r42wnrk8bw1nmqi785l49zwci7wkwskm5kk3b6x")))

(define-public crate-polylog-1.7.0 (c (n "polylog") (v "1.7.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1qh04xlrq94jjawvmiqx9f1lfznx9bfz3wm4h7sgayczf3fr6qi2")))

(define-public crate-polylog-1.8.0 (c (n "polylog") (v "1.8.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ld8maypmms5bams0rq0sjqchh6k0y0v7jfv379i3xlh172d5yf7")))

(define-public crate-polylog-2.0.0 (c (n "polylog") (v "2.0.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1cg86s2yrvmfy05aldpblggiz59yrx973szhag9i6ni8vdxxx9fx")))

(define-public crate-polylog-2.1.0 (c (n "polylog") (v "2.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1jcvfljgmhn6r1dlbwqrfa01sdnrbv9jbk9y5zsl2v47nh9valfv")))

(define-public crate-polylog-2.2.0 (c (n "polylog") (v "2.2.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ly90i9v0azpbjwl4zfbvk8jawyqm2j5f5qh3qc7yriyp85kqbbw")))

(define-public crate-polylog-2.3.0 (c (n "polylog") (v "2.3.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05hiv7gi49l1gmigbalf9i0h8vd1rxla5h074aa9av1n5hdbqnn7")))

(define-public crate-polylog-2.4.0 (c (n "polylog") (v "2.4.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08pm8rwpilm0npv1g3afhd1z0h7nzsgx03qzihpqp4k52m3rdshr")))

(define-public crate-polylog-2.5.0 (c (n "polylog") (v "2.5.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09jx214vi3shcnqjj5560y553f7nxcggkilb26rmcbv870bqhzh3")))

(define-public crate-polylog-2.5.1 (c (n "polylog") (v "2.5.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1g7vgac33z8gj01q5k95h8z64apmbli8lzcbxppwp86h8j9w2jh7")))

(define-public crate-polylog-2.5.2 (c (n "polylog") (v "2.5.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06vh0sgabsmbd41q7bzcq12s9nm7ls52wim2l19mmxdarql5gqjb")))

(define-public crate-polylog-2.5.3 (c (n "polylog") (v "2.5.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "114mfbrg28qsshcx7gld5lmw59fpwjlxhfqmnw2aia5p4d4vyqnl")))

(define-public crate-polylog-2.6.0 (c (n "polylog") (v "2.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "10gpgjqhin3svrcmzm97xqjf4w5zyws9s831k9sc8cvg1wgc0s48")))

