(define-module (crates-io po ly polymath) #:use-module (crates-io))

(define-public crate-polymath-0.0.0 (c (n "polymath") (v "0.0.0") (h "07dv9i8x8kryrlnkwvbfjr0kicbcn4ir043lbx0964rgmnbf3m03") (y #t)))

(define-public crate-polymath-0.1.0 (c (n "polymath") (v "0.1.0") (h "0d8k5v59q20knlhivh5hsgpj0fm2m8270h3hwxri69caifiacz5a") (y #t)))

(define-public crate-polymath-0.1.1 (c (n "polymath") (v "0.1.1") (h "1n25770zrmzf3zc1crx4w7bpn0kvi9gyjkjiyabp70rhb47a9qk0") (y #t)))

(define-public crate-polymath-0.2.0 (c (n "polymath") (v "0.2.0") (h "0irdbxv3yhhv5mg40qn88vwfj58fa1m5wvvkj50zgrif7f9d50y9") (y #t)))

(define-public crate-polymath-0.2.1 (c (n "polymath") (v "0.2.1") (h "0zwv603jxp0lqkrnd9mphf97f83fw85ifj199sg0xm43qdq6cxx7") (y #t)))

(define-public crate-polymath-0.3.0 (c (n "polymath") (v "0.3.0") (h "1sbbk7kywq5vx01aiw09n08xsi2mdbsbvdr2pbdqq8p7qwajm8iy") (y #t)))

(define-public crate-polymath-0.3.1 (c (n "polymath") (v "0.3.1") (h "1vm555lnz13sf7sdm9vi7kx6phs4h1mdlly1knk3nw2n1k01skhs")))

