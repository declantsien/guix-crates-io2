(define-module (crates-io po ly poly2geojson) #:use-module (crates-io))

(define-public crate-poly2geojson-0.1.0 (c (n "poly2geojson") (v "0.1.0") (d (list (d (n "geojson") (r "^0.21.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "048y0qm6l5q3lcdar7jw13945qf0biff3xpah5jrngn2hbq1s5ra")))

(define-public crate-poly2geojson-0.1.1 (c (n "poly2geojson") (v "0.1.1") (d (list (d (n "geojson") (r "^0.21.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "133syihqr2mpdzc89jvvdd05mvp1wf24m4101id05ldbdb701qrf")))

(define-public crate-poly2geojson-0.1.2 (c (n "poly2geojson") (v "0.1.2") (d (list (d (n "geojson") (r "^0.21.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "15fkximz81jhx3q8ncskcsfmjbg85195ijdkndjlcxg53z2ffqzc")))

