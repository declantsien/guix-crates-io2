(define-module (crates-io po ly polyhorn-ios-sys) #:use-module (crates-io))

(define-public crate-polyhorn-ios-sys-0.1.0 (c (n "polyhorn-ios-sys") (v "0.1.0") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.1.0") (d #t) (k 1)))) (h "1hjll8l22gy4456dzl9sncrjm1zj7phl0i833r7zcllc0mwrsz6z")))

(define-public crate-polyhorn-ios-sys-0.2.0 (c (n "polyhorn-ios-sys") (v "0.2.0") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.2.0") (d #t) (k 1)))) (h "0d5ddz2x8l5ywcp6nk0r5bqxpki7i6h7awi1fk9fds0aan5na5a3")))

(define-public crate-polyhorn-ios-sys-0.3.0 (c (n "polyhorn-ios-sys") (v "0.3.0") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.0") (d #t) (k 1)))) (h "0yqlb102nw3clgdns3s0rjss9824sznj6ya6wchz72fd6a5i1d71")))

(define-public crate-polyhorn-ios-sys-0.3.1 (c (n "polyhorn-ios-sys") (v "0.3.1") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.0") (d #t) (k 1)))) (h "0g9jy06m7q8rnz8f8yx53fc69w190a0565dhxi0py804nj5lfmfr")))

(define-public crate-polyhorn-ios-sys-0.3.2 (c (n "polyhorn-ios-sys") (v "0.3.2") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.3.2") (d #t) (k 1)))) (h "141zcjrjc875hnrgij7968vmqy1m6z7gl6j73bn5qf1446mjaj1n")))

(define-public crate-polyhorn-ios-sys-0.4.0 (c (n "polyhorn-ios-sys") (v "0.4.0") (d (list (d (n "fsize") (r "^0.1.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "polyhorn-build") (r "^0.4.0") (d #t) (k 1)))) (h "0b1wqal631sj93pjhx5hl1ybjgn0zxild0m9j12800x8nxigikbr")))

