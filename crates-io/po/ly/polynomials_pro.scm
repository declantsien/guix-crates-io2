(define-module (crates-io po ly polynomials_pro) #:use-module (crates-io))

(define-public crate-polynomials_pro-0.1.0 (c (n "polynomials_pro") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0j4p7zqzm3g114a8iilrmr9674n00flm8ccaa7a2485n8lfxxwwx")))

(define-public crate-polynomials_pro-0.2.0 (c (n "polynomials_pro") (v "0.2.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0vch251j7hwi3r5z6jivwl4diingfkw7ic054d3k8waraf1pasvd")))

(define-public crate-polynomials_pro-0.3.0 (c (n "polynomials_pro") (v "0.3.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "19y3p1s6wcxv9myg6vicpd0x3pl9pz59f5f3rq12k12ans2r3lvy")))

(define-public crate-polynomials_pro-0.4.0 (c (n "polynomials_pro") (v "0.4.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0zyk9jnyap41kjwamh7vg527gg46zc1w7q5dglqzdysgp385zymp")))

(define-public crate-polynomials_pro-0.5.0 (c (n "polynomials_pro") (v "0.5.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1hc9cbkrwbaiwfwk0p37fwk3kn5gxlwg1f6rcir0vjqglb9rgcg2")))

