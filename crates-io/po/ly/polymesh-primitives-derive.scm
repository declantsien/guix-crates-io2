(define-module (crates-io po ly polymesh-primitives-derive) #:use-module (crates-io))

(define-public crate-polymesh-primitives-derive-0.1.0 (c (n "polymesh-primitives-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "07lml91wam0pa4q9l6k451xkrvzjrgw6cks73bfdk8hs679vsjm5") (f (quote (("std") ("no_std") ("default" "std"))))))

