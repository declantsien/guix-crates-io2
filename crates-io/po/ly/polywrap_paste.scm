(define-module (crates-io po ly polywrap_paste) #:use-module (crates-io))

(define-public crate-polywrap_paste-1.0.0 (c (n "polywrap_paste") (v "1.0.0") (d (list (d (n "paste-test-suite") (r "^0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0w7k7ajxg30yxgk6mdbrnp229fgqwmr37h9s7flclm5km4wxrbfj") (r "1.31")))

