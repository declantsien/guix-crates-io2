(define-module (crates-io po ly polylabel) #:use-module (crates-io))

(define-public crate-polylabel-0.1.0 (c (n "polylabel") (v "0.1.0") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1r1q5qj7pcmzmqb9hyfsz59w91264yfli2gx1wzg5vhrwfyhx6dp")))

(define-public crate-polylabel-0.1.1 (c (n "polylabel") (v "0.1.1") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1mx145frmi9484yspvnqzwmb0c2hvcz521qmkdybkzl9c8qwv8zn")))

(define-public crate-polylabel-0.1.2 (c (n "polylabel") (v "0.1.2") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1sgs2fmvznq01q9skv4h66b0csm7p388rcgljqwiyvili5a002gb")))

(define-public crate-polylabel-0.1.4 (c (n "polylabel") (v "0.1.4") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0hlz1kmf869rddwdvrpiwkmw7p45bbhxgnmw0rs8qbkxiwc783fw")))

(define-public crate-polylabel-0.1.5 (c (n "polylabel") (v "0.1.5") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1hawl61x3sk2hjbmwgldlxhh4jz74kaaddjdif1rl747pnbflak0")))

(define-public crate-polylabel-0.1.6 (c (n "polylabel") (v "0.1.6") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0xdd37izfcf9f7nzz5196kd40cyi9jcv9xyadgml83rw6x4dasj7")))

(define-public crate-polylabel-0.1.7 (c (n "polylabel") (v "0.1.7") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1yaqvq0bpni8b8d628fp43ydx0y5fvmfdxc0x2rnd71qfdq6mkgr")))

(define-public crate-polylabel-0.1.9 (c (n "polylabel") (v "0.1.9") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1nwplgwk102rlj27kp5p78g3qg2y6w7gnkw6zk12qw6czl7dj6b2")))

(define-public crate-polylabel-0.1.10 (c (n "polylabel") (v "0.1.10") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1434bqw9cmq41yrx6kylcr9y2hzkyinaikhxkycx7anffc6fvqdg")))

(define-public crate-polylabel-0.1.11 (c (n "polylabel") (v "0.1.11") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0rbzwkxms9wms3ww76dpd1dd2gm2qbhb8yil88ag41vsckki58s3")))

(define-public crate-polylabel-1.0.0 (c (n "polylabel") (v "1.0.0") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1lwhi0gxa4pmcsk3zr9cscaklz8hy8zwpzmjwfdbk2ysfgksdf58")))

(define-public crate-polylabel-1.0.2 (c (n "polylabel") (v "1.0.2") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0crmbmqrd09jngi0i19nvhq9xhwvzi3v99y1x8z6xgbdvzj5lq4n")))

(define-public crate-polylabel-1.0.3 (c (n "polylabel") (v "1.0.3") (d (list (d (n "geo") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1k5zhwjh06b35j3l5a4wf71w2z3p52kyg1dykl9psrk3ra5pqdmn")))

(define-public crate-polylabel-1.0.34 (c (n "polylabel") (v "1.0.34") (d (list (d (n "geo") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1ga11gw6ss53ah832nkra00nyrg3kxxx8vqy8aaxbcny64932f9m")))

(define-public crate-polylabel-1.0.40 (c (n "polylabel") (v "1.0.40") (d (list (d (n "geo") (r "^0.4.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1pvxfdrzhvwpjz61qp33wxf8xrnr5yp84m9skjdccw9jb4k12cxd")))

(define-public crate-polylabel-1.0.41 (c (n "polylabel") (v "1.0.41") (d (list (d (n "geo") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "06kap0bsnqgy10fcmkpp1d9q88c95mh97afzx81dcz96n8s0mb5d")))

(define-public crate-polylabel-1.0.42 (c (n "polylabel") (v "1.0.42") (d (list (d (n "geo") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "15wmphnjkh06acvhiw0q7zw9rlwlxbjyjk73v79fhrdm9xya32mb")))

(define-public crate-polylabel-1.0.43 (c (n "polylabel") (v "1.0.43") (d (list (d (n "geo") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0zhqv13nvg95farh0hc5yr2496p59iczjq52y5xpcqff585if0fc")))

(define-public crate-polylabel-1.0.44 (c (n "polylabel") (v "1.0.44") (d (list (d (n "geo") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0h363909qz2jh6jiwih0y36v1hdazv3if7h09qzsb583bd4ny3qn")))

(define-public crate-polylabel-1.1.0 (c (n "polylabel") (v "1.1.0") (d (list (d (n "geo") (r "^0.8.3") (d #t) (k 0)) (d (n "geo-types") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1zzz538iasjvx0vj6ikd31afvybaz8l7sdlr8rg189aky5j561hk")))

(define-public crate-polylabel-1.2.0 (c (n "polylabel") (v "1.2.0") (d (list (d (n "geo") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0jkfccwxsrlj9i0yz5z0j567ni9y6ykrih0mdxfdhv4gr96pkvyv")))

(define-public crate-polylabel-1.2.2 (c (n "polylabel") (v "1.2.2") (d (list (d (n "cbindgen") (r "^0.6.8") (d #t) (k 1)) (d (n "geo") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0ql6dsfwsf9mh7gn1jkn0sgdiaz4amjyxwq5rvhfw86pvf2hwc1m")))

(define-public crate-polylabel-1.3.0 (c (n "polylabel") (v "1.3.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0a3l2z6bdwhry8m3ggihj9kfjv98szbx71zh9k59j2j3s71y373z")))

(define-public crate-polylabel-1.3.1 (c (n "polylabel") (v "1.3.1") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1zmfmjv86w6c24s3plci54fs2dv0ywz5r2lkhxp5h9kc22l1fsj7")))

(define-public crate-polylabel-1.3.2 (c (n "polylabel") (v "1.3.2") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0fii9dszxix31ddz9jrnd318wlrny9n4qm8gh4ymd29za9gyk6z5")))

(define-public crate-polylabel-2.0.0 (c (n "polylabel") (v "2.0.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "0ph0087mpap863m26yjkc5yswc8ka7g8zqw5klsym264mvny1hch")))

(define-public crate-polylabel-2.1.0 (c (n "polylabel") (v "2.1.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.13.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1mjdha2jgym00l0mv7lgx92wm7bp8bgdm4yw84hibl6m6h5a0926")))

(define-public crate-polylabel-2.2.0 (c (n "polylabel") (v "2.2.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.13.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "0nq4xh1bf0s8h732hc4lcf0jqjmav85fmxpkikacwwmg8x1dhdzn")))

(define-public crate-polylabel-2.3.0 (c (n "polylabel") (v "2.3.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1rh119ald7vyrwihbqr9f4w329845ahb1pcvvrddhdg4j5156qxq")))

(define-public crate-polylabel-2.3.1 (c (n "polylabel") (v "2.3.1") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1ds30in07ysn5dln82gwhj611mrqd0w9x3y31ha2sj3zpa8bfbvd")))

(define-public crate-polylabel-2.4.0 (c (n "polylabel") (v "2.4.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1ywd1jz7s3d86r33ffylmci13pgrzabc5qgfk18rs80lcyc3fjvw")))

(define-public crate-polylabel-2.4.1 (c (n "polylabel") (v "2.4.1") (d (list (d (n "cbindgen") (r "^0.22.0") (o #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.20.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "124g82ziz6na55iicjnyk2y6xzz98ajjxfank9dmnrwmhk8v3b4y") (f (quote (("headers" "cbindgen"))))))

(define-public crate-polylabel-2.4.2 (c (n "polylabel") (v "2.4.2") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "geo") (r "^0.23.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1izxnb2sxjy129wahhhbdkamxraj5v35rgincm54a8zbkqhbs307") (f (quote (("headers" "cbindgen"))))))

(define-public crate-polylabel-2.4.3 (c (n "polylabel") (v "2.4.3") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo") (r "^0.24.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1jnxyly0f6jwfx55bs9g4mc4y8g03b9bc1lasbgm7mqipklrc2z1") (f (quote (("headers" "cbindgen"))))))

(define-public crate-polylabel-2.4.4 (c (n "polylabel") (v "2.4.4") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo") (r "^0.25.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1z1fm3w1bydmvlk0fl9d33vkzv1pxn62kwr4f8mi74hm88jm1sni") (f (quote (("headers" "cbindgen"))))))

(define-public crate-polylabel-2.5.0 (c (n "polylabel") (v "2.5.0") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1jxzhxrqr4n04k8qs002ls63qykyivjmdzbpm50lynlwfr3jh4v1") (f (quote (("headers" "cbindgen"))))))

(define-public crate-polylabel-3.0.0 (c (n "polylabel") (v "3.0.0") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "080nw7g1i4a35pica946knskk5g0dc0kxhk27767z9gipnnh18c4") (f (quote (("headers" "cbindgen") ("ffi" "libc"))))))

(define-public crate-polylabel-3.1.0 (c (n "polylabel") (v "3.1.0") (d (list (d (n "cbindgen") (r "^0.24.2") (o #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo") (r "^0.27.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1aqgcsdrgb3gqc377kfvwi7d6r0rzal0a013bgcvnq0scwjhrcck") (f (quote (("headers" "cbindgen") ("ffi" "libc"))))))

(define-public crate-polylabel-3.2.0 (c (n "polylabel") (v "3.2.0") (d (list (d (n "cbindgen") (r "^0.26.0") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "geo") (r "^0.28.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1ch4sqh9np4b7dg1mrs6z5dvxfljfq6l2in5qandqslwxpd85di0") (f (quote (("headers" "cbindgen") ("ffi" "libc")))) (r "1.70")))

