(define-module (crates-io po ly polynomint) #:use-module (crates-io))

(define-public crate-polynomint-0.1.1 (c (n "polynomint") (v "0.1.1") (h "15l0vyjq9knyrji52pv886g4i8zykgc9giv1icif73whp1z27flb")))

(define-public crate-polynomint-0.1.2 (c (n "polynomint") (v "0.1.2") (h "0r8yn3kl4pypds8x10j6xm5ivhc3zprxd88nc9xlbvap722m358l")))

(define-public crate-polynomint-0.1.3 (c (n "polynomint") (v "0.1.3") (h "05iz450z9kphvlfdbzsf365sr6p0ca7snibnb1dfmi5iv09n7ck9")))

(define-public crate-polynomint-0.2.0 (c (n "polynomint") (v "0.2.0") (h "14rpsmxcfbsjl96pyssrqhrkri64b1blk67s7kx5pnaf0z5d5z0d")))

(define-public crate-polynomint-0.3.0 (c (n "polynomint") (v "0.3.0") (h "0275g4mf6p9pzvdrjvsn8xzhvnsz5ajxmnjzc8lhpnzqapbc8a06")))

(define-public crate-polynomint-0.4.0 (c (n "polynomint") (v "0.4.0") (h "1v738ylgnnkknlvg5n4akg66lbj7rhk2y9zyqjsps7wvgas6l69j")))

(define-public crate-polynomint-0.5.0 (c (n "polynomint") (v "0.5.0") (h "1miz06xfjsp6rcgp0harr1nx6kbzm6fvpd49fwv1qldw356np1lg")))

(define-public crate-polynomint-0.5.1 (c (n "polynomint") (v "0.5.1") (h "1dfp72ak48b5ixbrqc145kx4iwrza9nqswlbsd5s9zfn19nvwazg")))

(define-public crate-polynomint-0.5.2 (c (n "polynomint") (v "0.5.2") (h "02yb6v5l72wivphfp6ph36fl0zjja66ppr30v9fqg532i1n2rd02")))

(define-public crate-polynomint-0.5.3 (c (n "polynomint") (v "0.5.3") (h "1fyrapcbhsks8qwmh4xqd1h0jzv0znxjn5a58acw8vrvi3md7dyf")))

