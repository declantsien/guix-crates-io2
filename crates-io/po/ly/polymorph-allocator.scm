(define-module (crates-io po ly polymorph-allocator) #:use-module (crates-io))

(define-public crate-polymorph-allocator-0.1.0-beta.1 (c (n "polymorph-allocator") (v "0.1.0-beta.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0jyhvz88m2905avfa78czkqvrsrldnxigpb7nf4awq10zqyb1q3i")))

(define-public crate-polymorph-allocator-0.1.0-beta.2 (c (n "polymorph-allocator") (v "0.1.0-beta.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1f6n506nl7rr8igdvwxa1d2767f5yf7sbfx2cy6fghd60rgs6y2i")))

(define-public crate-polymorph-allocator-0.1.0 (c (n "polymorph-allocator") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0lldgp2anypjzw6vhrdcdf8gvlrcy4mgfs7gaz6ypi8bxddixfz3")))

(define-public crate-polymorph-allocator-1.0.0 (c (n "polymorph-allocator") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1biwghzpxlj8nh2h72c7ppygbbcblpdg347dhcaawlbswmgny40l")))

(define-public crate-polymorph-allocator-1.0.1 (c (n "polymorph-allocator") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "026wzaprg915vnzc2vx7gsp3za121fxxxnba3s0yb88fksbbl7xy")))

(define-public crate-polymorph-allocator-1.1.0 (c (n "polymorph-allocator") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1fgf1z98acgnha5zrn73f0irajmizsgrw7gf5gck2dj789ypd6d6")))

(define-public crate-polymorph-allocator-1.2.0 (c (n "polymorph-allocator") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0cfnlcwwqar52kskyxg4rwfsvxqj4457m7lk9604g0s8w0rq7zz5")))

