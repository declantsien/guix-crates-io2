(define-module (crates-io po ly poly1305) #:use-module (crates-io))

(define-public crate-poly1305-0.0.0 (c (n "poly1305") (v "0.0.0") (h "12dcya2r5qi1axh54yqa2r7iv2irlwiq0k3mk6abf4h0skxllgsk")))

(define-public crate-poly1305-0.1.0 (c (n "poly1305") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (k 0)))) (h "08bl196gv5c5hgmljha5vqsd3qj5k2b6393chhvd73cdb0pj3g5n")))

(define-public crate-poly1305-0.2.0 (c (n "poly1305") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (k 0)))) (h "0w2znqmzdlrrw49jkvz2vf28sni3c96dz3xz75z6fjfxv33dyngv")))

(define-public crate-poly1305-0.3.0 (c (n "poly1305") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)) (d (n "zeroize") (r "^0.9") (o #t) (k 0)))) (h "1wykada7agfy1ywp4h5v4ibdxz7nndylaf1yb4h3bkfvskzl1wdz")))

(define-public crate-poly1305-0.4.0 (c (n "poly1305") (v "0.4.0") (d (list (d (n "universal-hash") (r "^0.2") (k 0)) (d (n "zeroize") (r "^0.10") (o #t) (k 0)))) (h "1cc24p76zwxzsnrysajphdjbrlsf5s6yrhn8cjjxq2k76z772dc3") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.4.1 (c (n "poly1305") (v "0.4.1") (d (list (d (n "universal-hash") (r "^0.2") (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "1qb6s7aadj3sddwsprgly0mz9116k9ssx2d7cdg1vl9gdcpshhnz") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.5.0 (c (n "poly1305") (v "0.5.0") (d (list (d (n "universal-hash") (r "^0.3") (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "1ddr7h5n3vv4ymlg7dwhzdd1jlfidcxpxp5r3fb1vwpg5vcaspjl") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.5.1 (c (n "poly1305") (v "0.5.1") (d (list (d (n "universal-hash") (r "^0.3") (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "0bjb7d115yqzkq0nvmai21dcq1bqdqbxzhfchwr9ib7kfs3yk6b8") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.5.2 (c (n "poly1305") (v "0.5.2") (d (list (d (n "universal-hash") (r "^0.3") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1f40n01zcva186rcqsi8z0qfxjnh55q0khzpydwxp7cfyi89z0mm") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.6.0 (c (n "poly1305") (v "0.6.0") (d (list (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1ccvpwpvbzbwap6y465jachrlfkkqvwsg27qkdhxfghlmf923d6r") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.6.1 (c (n "poly1305") (v "0.6.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1079zi6y6jqblhi6dds8m7xi532xqzxgwayhli643vjkivg4dki2") (f (quote (("std" "universal-hash/std"))))))

(define-public crate-poly1305-0.6.2 (c (n "poly1305") (v "0.6.2") (d (list (d (n "cpuid-bool") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1a7w61pzwiylxc1ic34rpw8sz3f4596bw5m0nf1czm6j3ay5cx2b") (f (quote (("std" "universal-hash/std") ("force-soft"))))))

(define-public crate-poly1305-0.7.0 (c (n "poly1305") (v "0.7.0") (d (list (d (n "cpufeatures") (r "^0.1") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1ghlyj9vqcvg3a0yf65j57ff5cncdy15cfyj7h48bni5adlh1s2g") (f (quote (("std" "universal-hash/std") ("force-soft"))))))

(define-public crate-poly1305-0.7.1 (c (n "poly1305") (v "0.7.1") (d (list (d (n "cpufeatures") (r "^0.1") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r "=1.3") (o #t) (k 0)))) (h "0jg3wkjj8avkjnksrvbiybnpw0r4pkz1z35njd5xzfwfyyqzmkwz") (f (quote (("std" "universal-hash/std") ("force-soft"))))))

(define-public crate-poly1305-0.7.2 (c (n "poly1305") (v "0.7.2") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "^0.4") (k 0)) (d (n "zeroize") (r ">=1, <1.4") (o #t) (k 0)))) (h "1pkf4jlriskq9rvz8y5fjj9dw42q6yg5djijlin4n6p1dd3yp2h4") (f (quote (("std" "universal-hash/std") ("force-soft"))))))

(define-public crate-poly1305-0.8.0-pre (c (n "poly1305") (v "0.8.0-pre") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "=0.5.0-pre") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0y2ffhmx0ncrirww5f3qnlwyx0c21k5y5xz6q1p9ascbjjr5nc1v") (f (quote (("std" "universal-hash/std") ("force-soft")))) (r "1.56")))

(define-public crate-poly1305-0.8.0-pre.1 (c (n "poly1305") (v "0.8.0-pre.1") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "=0.5.0-pre.1") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0xddc4yfskkvmq49hh2s2xnc72sgbdkb00bhjnsndvkjg4kjhx9q") (f (quote (("std" "universal-hash/std") ("force-soft")))) (r "1.56")))

(define-public crate-poly1305-0.8.0-pre.2 (c (n "poly1305") (v "0.8.0-pre.2") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "=0.5.0-pre.2") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1yf3dacmc1zdqbc8snzdk84vx2bjcpn0cjjh33q1a04wjysdcd7g") (f (quote (("std" "universal-hash/std")))) (r "1.56")))

(define-public crate-poly1305-0.8.0 (c (n "poly1305") (v "0.8.0") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "^0.5") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1grs77skh7d8vi61ji44i8gpzs3r9x7vay50i6cg8baxfa8bsnc1") (f (quote (("std" "universal-hash/std")))) (r "1.56")))

(define-public crate-poly1305-0.9.0-pre (c (n "poly1305") (v "0.9.0-pre") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "universal-hash") (r "=0.6.0-pre.0") (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "048h6vqlvp77gfhir60s9yka37lgkxz4fzld97jf9b3jc1w8xh66") (f (quote (("std" "universal-hash/std")))) (r "1.65")))

