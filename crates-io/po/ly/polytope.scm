(define-module (crates-io po ly polytope) #:use-module (crates-io))

(define-public crate-polytope-0.1.0 (c (n "polytope") (v "0.1.0") (h "02kvjxd64sqcp76q045an6ijkrnjbss4b079f9fs208wd370a3ds")))

(define-public crate-polytope-0.1.1 (c (n "polytope") (v "0.1.1") (h "13mzpx839wv9xrii1p2fjcs4871ln145p4rpn4yy334mq15c3f1r")))

(define-public crate-polytope-0.1.2 (c (n "polytope") (v "0.1.2") (h "1kd39z6lqk1ccaqixidjpbqzj6yj5k7dhcf4kzfqw515rzj8gj8j")))

