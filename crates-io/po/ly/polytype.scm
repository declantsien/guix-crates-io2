(define-module (crates-io po ly polytype) #:use-module (crates-io))

(define-public crate-polytype-1.0.0 (c (n "polytype") (v "1.0.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "1jn2g3k6rz0pxn98b6wz35qsrlacxc2vn8fjqfaqr64zb13pkpzs")))

(define-public crate-polytype-1.0.1 (c (n "polytype") (v "1.0.1") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "0zj4rqjslqwakxm07lnj3np07xlwc6sklhr8qjyj15dfcrm23d5i")))

(define-public crate-polytype-1.0.2 (c (n "polytype") (v "1.0.2") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "1rvh7zn845qhha5qaxbs97f747rcxblhvkbc2amklg5hfcanzpgx")))

(define-public crate-polytype-1.0.3 (c (n "polytype") (v "1.0.3") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "1pqgg4m56bhvfmwb2mig3bshr3hv1a765hdhw3cr2h6saxh4lwkn")))

(define-public crate-polytype-1.0.4 (c (n "polytype") (v "1.0.4") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "0ak9m960fnklp88czn89py1kxiclzgdadwwvzq019yxvgwhqicr1")))

(define-public crate-polytype-1.1.0 (c (n "polytype") (v "1.1.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "09nq7d2vhg8nbhrfg1kzl1ixybky961ylh6v22p94nr6xrzjqz65")))

(define-public crate-polytype-1.2.0 (c (n "polytype") (v "1.2.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "14c4fhzvallq3c68vr8ibfp76ddf9qw4fsxacfifw48drda2xk22")))

(define-public crate-polytype-1.2.1 (c (n "polytype") (v "1.2.1") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "0w256d3i97qd82k2m5b6qjwvm846mv9ppsgb1sskv2lmly93la9n")))

(define-public crate-polytype-1.2.2 (c (n "polytype") (v "1.2.2") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)))) (h "1wpxkadnwklgk55v1sw1cx9d3wv50lpkjly9dsrxqbh236c7mss7")))

(define-public crate-polytype-1.2.3 (c (n "polytype") (v "1.2.3") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "04cwiacdijnbj4v2ykidvn6m3xfncgmmlfilg68pzf9dyxa8pkk5")))

(define-public crate-polytype-1.2.4 (c (n "polytype") (v "1.2.4") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "10n992wpslfxsx9qln568dpvdnryn8ccpnr0kgdfs6a18z264kbx")))

(define-public crate-polytype-1.2.5 (c (n "polytype") (v "1.2.5") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "19bhxba7ixcrlpvsilrml8whw6hprdky2fvji7nfci0i8r2vxdzd")))

(define-public crate-polytype-1.2.6 (c (n "polytype") (v "1.2.6") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1x5wscpm1nk8m59nizmpi68pzmx2h3z0a66m16brdvfr27n1n9q9")))

(define-public crate-polytype-2.0.0 (c (n "polytype") (v "2.0.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1ldpbm2plnss4k1i21rbg441vy9zwkx8rbj9k6iinnswgs114zyd")))

(define-public crate-polytype-2.1.0 (c (n "polytype") (v "2.1.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1i9mphn5kvhcna6akidw6x59x4651s9llbxpd6vpn59yasnsj9xw")))

(define-public crate-polytype-2.1.1 (c (n "polytype") (v "2.1.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "16mcxfcryd3fmgj6ph0mqrs8pa59di7fvjns95glpfv9h9kv6b99")))

(define-public crate-polytype-3.0.0 (c (n "polytype") (v "3.0.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "0aa76jp5gqk942mgpl0qm6iagfgwj9qblfppjldgd2g359k6ic37")))

(define-public crate-polytype-4.0.0 (c (n "polytype") (v "4.0.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1lpr2lk0nbanp52pisi0byfx95nral7qi4rq1lh1k1wx5jdqxmhp")))

(define-public crate-polytype-4.0.1 (c (n "polytype") (v "4.0.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "00xb4j6h7nfyb2cjyq7nd29fywwylnkqa0ilywihhynzpg832hr3")))

(define-public crate-polytype-4.0.2 (c (n "polytype") (v "4.0.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "0lqqr5wfr2xbrq0havqfaizsg2z13r9m000zx4wz3g48da97sj74")))

(define-public crate-polytype-4.1.0 (c (n "polytype") (v "4.1.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1b5zqldc2h8gpvhfg1ayx2y4krifnqisvsg3gkygn85s45byra4z")))

(define-public crate-polytype-4.1.1 (c (n "polytype") (v "4.1.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1ld5cjcll7sw9kxsqjaad86qwkfn4498a72rigcg9x7llwvl98g9")))

(define-public crate-polytype-4.2.0 (c (n "polytype") (v "4.2.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1zs8jj3m78alpcbqc797qgg7wy4wm9bab27fgz716piahha7pclj")))

(define-public crate-polytype-4.2.1 (c (n "polytype") (v "4.2.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)))) (h "00qi80ak628xa2c7vc3ymmp3nyvl6jwd1hpgfh1hwa8h2g99jqgb")))

(define-public crate-polytype-5.0.0 (c (n "polytype") (v "5.0.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "1234rlxqm50w2fdsh3m7yl5hf3k5ycpsbvjkfglb3102c9zmi9xs")))

(define-public crate-polytype-5.0.1 (c (n "polytype") (v "5.0.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "06h0bhamgfn7gj0i32kig6fvl1fc1aa3rmghy8b6q1ag18xk5hqp")))

(define-public crate-polytype-5.0.2 (c (n "polytype") (v "5.0.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "1c5mqpiqpkgzi2cmgfp0k812rxlkfjs8mvzijplkfvh5va35fjmn")))

(define-public crate-polytype-6.0.0 (c (n "polytype") (v "6.0.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "00l00g0hvchvz4iqd4h8j8dvl69jagw4vi2pjxmc29frfnyrv1i8")))

(define-public crate-polytype-6.1.0 (c (n "polytype") (v "6.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1qh03k1ymixnqkc3f54ykf76vy4zrrg2kvmlqscvvcrzw6z15aaj")))

(define-public crate-polytype-6.1.1 (c (n "polytype") (v "6.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "0y3iqnnxrzh0y4f2rjksf5bxz9v7mr4w4yhcnrhzy6ngwi0dz3li")))

(define-public crate-polytype-6.2.0 (c (n "polytype") (v "6.2.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "=4.1.1") (o #t) (d #t) (k 0)))) (h "0x7jm3v7dgwsi0dyhihwasnq7g1q214dkwkkfpzyygph3fy2asy7") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-polytype-6.2.1 (c (n "polytype") (v "6.2.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "=4.1.1") (o #t) (d #t) (k 0)))) (h "1icj0vjys2p103nk2wik0wylaslllnx5bqn5k32bgd7zvxaqpmq1") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-polytype-7.0.0 (c (n "polytype") (v "7.0.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "winnow") (r "^0.5.28") (d #t) (k 0)))) (h "1gad15w7qmalam76k77mi1k153xdw6crjd9xz2v2qdzcl1bh45g2") (f (quote (("parser") ("default" "parser"))))))

(define-public crate-polytype-7.0.1 (c (n "polytype") (v "7.0.1") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "winnow") (r "^0.5.28") (d #t) (k 0)))) (h "162izx30bnv93j4ma2x87jxq9zdnvmyg7mjz2vn2y92949gr9m00") (f (quote (("parser") ("default" "parser"))))))

