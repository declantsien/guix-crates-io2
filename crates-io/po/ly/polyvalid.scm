(define-module (crates-io po ly polyvalid) #:use-module (crates-io))

(define-public crate-polyvalid-0.1.0 (c (n "polyvalid") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "0mj9qps450wzfa2a428mqk29gxl7dd66qp6jrz650hin140nb9cn")))

(define-public crate-polyvalid-0.1.1 (c (n "polyvalid") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.3") (d #t) (k 0)))) (h "16brhqj2rmyadanv4bhp6h7h79nvigaf2a4bsmi430myz75kppgw")))

(define-public crate-polyvalid-0.1.5 (c (n "polyvalid") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "insta") (r "^1.18.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "wasmer-pack-testing") (r "^0.6.0") (d #t) (k 2)))) (h "1m67pq6azdy26gs6gj0cvv8ygs4q3lbnm9dafjfal9id69hmqf1g")))

(define-public crate-polyvalid-0.1.6 (c (n "polyvalid") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "insta") (r "^1.18.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "wasmer-pack-testing") (r "^0.6.0") (d #t) (k 2)))) (h "1g1jhvbgj2216dk754s76gx069p0b6g39fb41bgq12ics6bw1lv2") (y #t)))

(define-public crate-polyvalid-0.1.7 (c (n "polyvalid") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "insta") (r "^1.18.2") (f (quote ("filters"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "wasmer-pack-testing") (r "^0.6.0") (d #t) (k 2)))) (h "1b02pw42p2wnhg9lxchlxf653b3a4fx37s3r436zr4k1ar3nhm38")))

