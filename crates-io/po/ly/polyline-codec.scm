(define-module (crates-io po ly polyline-codec) #:use-module (crates-io))

(define-public crate-polyline-codec-0.1.0 (c (n "polyline-codec") (v "0.1.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "18mabx3xavk1ha07lm1j56fcbzihglk7xhv1nv5g3qxb0ak87mh5") (y #t)))

(define-public crate-polyline-codec-0.1.1 (c (n "polyline-codec") (v "0.1.1") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "05rslb8raiailnhkxl1ghj4d1d53v841krbm6l7llpnfzbnlkyh8")))

(define-public crate-polyline-codec-0.1.2 (c (n "polyline-codec") (v "0.1.2") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0prx72wm2sbnjnaz8z5y15pr6kz83q3kw4s54dnq3nmavwhb3lzg")))

(define-public crate-polyline-codec-0.1.3 (c (n "polyline-codec") (v "0.1.3") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0s4znkafnyji3l70ip4lqczrghv238lh6f4cfwlj8mh8bql29n8k")))

