(define-module (crates-io po ly polyhedron-faces) #:use-module (crates-io))

(define-public crate-polyhedron-faces-0.0.1 (c (n "polyhedron-faces") (v "0.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1pqd07maxbwb2alqk81xs1cp1aybk3d3iyv3r6vqaargcywikx9c") (y #t)))

(define-public crate-polyhedron-faces-0.1.1 (c (n "polyhedron-faces") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1qw867mxr5f1ccjlsk35gsp9lb079h9blnkqyb4z3c0shdxiwl3r") (y #t)))

(define-public crate-polyhedron-faces-0.1.2 (c (n "polyhedron-faces") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1zjz4ad1qxnkcqi7rnm5iqah22mpy7m39bn9jhpffcvmypj1f8sx") (y #t)))

(define-public crate-polyhedron-faces-0.2.1 (c (n "polyhedron-faces") (v "0.2.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1dx8y8qpgxnws3djhqpcvlhsyhinsm955m8r1nlfljcbf6f3yn2j") (y #t)))

(define-public crate-polyhedron-faces-0.3.0 (c (n "polyhedron-faces") (v "0.3.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "0npcv8bkrzls47k9827sspnbc5w3vzdkr24a1jm1l3bdc4ixsj63") (y #t)))

(define-public crate-polyhedron-faces-0.3.1 (c (n "polyhedron-faces") (v "0.3.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "0fysq0z4xnbjv1szcwiwnhxa7q19df6wmf0cyy737vqlp84k4g9i") (y #t)))

(define-public crate-polyhedron-faces-0.3.2 (c (n "polyhedron-faces") (v "0.3.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "035v27ci2kk1b65if823kb2davwyvkax5mcziq6rlh95iskwsc3i") (y #t)))

(define-public crate-polyhedron-faces-0.3.3 (c (n "polyhedron-faces") (v "0.3.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "01rhfdwp5fmpzwfsabxv5696p8v533ki0glygifjc6g3y5g9xy4y") (y #t)))

(define-public crate-polyhedron-faces-0.3.4 (c (n "polyhedron-faces") (v "0.3.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "173fr4264vja67wyjpkah8fm147mcwssg8ibzx2vljzjk6l88hf7") (y #t)))

(define-public crate-polyhedron-faces-0.3.5 (c (n "polyhedron-faces") (v "0.3.5") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "1346szm88ica1hfm3l2qrs19dykf7v8dmabkg4xcrj6y0mskg5fi") (y #t)))

(define-public crate-polyhedron-faces-0.4.1 (c (n "polyhedron-faces") (v "0.4.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "126l7n9hdcnqp04hrnwxs9mrnd7mfbxsacb5qahnsfc0liqj4k65") (y #t)))

(define-public crate-polyhedron-faces-0.4.2 (c (n "polyhedron-faces") (v "0.4.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "0xzw89l1mdv37zyzniyc1ly2jk596jf145g1qqv6d111nd1kc5rh") (y #t)))

(define-public crate-polyhedron-faces-0.4.3 (c (n "polyhedron-faces") (v "0.4.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "quaternion-matrix") (r "^0.1") (d #t) (k 0)))) (h "0r5hqi0s676dgvm4n0layn0rgam1l1lq9f9r56n2dr3ggg28y335")))

