(define-module (crates-io po ly poly) #:use-module (crates-io))

(define-public crate-poly-0.1.0 (c (n "poly") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0ky523xbdnh7b67bij2sk0jaaw5sgfp4lkqrxjdyblv1rzx5gjra")))

