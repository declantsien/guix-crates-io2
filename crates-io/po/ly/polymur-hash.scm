(define-module (crates-io po ly polymur-hash) #:use-module (crates-io))

(define-public crate-polymur-hash-0.1.0 (c (n "polymur-hash") (v "0.1.0") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)))) (h "1zfgkr6if6289wwap58y8lhsvx00nh1ylb7scfj11i0spsqnfmfz")))

(define-public crate-polymur-hash-0.2.0 (c (n "polymur-hash") (v "0.2.0") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.8") (f (quote ("xxh3"))) (d #t) (k 2)))) (h "05h0w4mddpnacv0hamx9wfcaca71la206hmi350n9zwn5nm6s6ji")))

(define-public crate-polymur-hash-0.2.1 (c (n "polymur-hash") (v "0.2.1") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.8") (f (quote ("xxh3"))) (d #t) (k 2)))) (h "15d6n99rg21nkxp98j38d2klx7506jgap6sqizxrcrzyw4zn16dd")))

