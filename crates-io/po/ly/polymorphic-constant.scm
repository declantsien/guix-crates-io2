(define-module (crates-io po ly polymorphic-constant) #:use-module (crates-io))

(define-public crate-polymorphic-constant-0.1.0 (c (n "polymorphic-constant") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0ykfgivpsx1gazw00n2xiph5ahd6wpq3v9hgrig8mrdw3gj2g938")))

(define-public crate-polymorphic-constant-0.1.1 (c (n "polymorphic-constant") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1scf32qni8bkf6frfqzpbwfmg9qs8m73kbhj2hy74j4kjsn0vcd7")))

(define-public crate-polymorphic-constant-0.1.2 (c (n "polymorphic-constant") (v "0.1.2") (h "06b0wjm056ifmaiqliwhlsgihk6312idgbyr31saixbhi1qjqf7d")))

(define-public crate-polymorphic-constant-0.1.3 (c (n "polymorphic-constant") (v "0.1.3") (h "17pjrkh62kfx1d2d1qwcpwng3z9a57vzv60gliaaxg68niz71sn1")))

(define-public crate-polymorphic-constant-0.1.4 (c (n "polymorphic-constant") (v "0.1.4") (h "1l4s0n42d3ffyqga5c7v19aaavyiph8hqld5q0ymxn2z87jggw11")))

(define-public crate-polymorphic-constant-0.1.5 (c (n "polymorphic-constant") (v "0.1.5") (h "0bm7705blwsb5yi0l6khq24i50v1gi69k9j7lagv2z60ljm2zk4b") (y #t)))

(define-public crate-polymorphic-constant-0.1.6 (c (n "polymorphic-constant") (v "0.1.6") (h "1hxbrf9mzlyhlm272gjb0n6x9d24lfanz1k32bc26q1zj7w2b58f")))

(define-public crate-polymorphic-constant-0.2.0 (c (n "polymorphic-constant") (v "0.2.0") (h "1z8ssdhk18sk0njfqx8m85i94a68r7inqzbxgn4f6z509kvpxwdw")))

