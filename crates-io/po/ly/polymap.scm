(define-module (crates-io po ly polymap) #:use-module (crates-io))

(define-public crate-polymap-0.0.1 (c (n "polymap") (v "0.0.1") (h "0ckf52vg5hqsldxan5kd0n5w4vfs6hzdypkjk2a2mpkknwm6wf65")))

(define-public crate-polymap-0.0.2 (c (n "polymap") (v "0.0.2") (h "0ksz7xg05akbss1afgzvglfw4p27ygc8qsiw3l8ijy6wvngjlyk4")))

(define-public crate-polymap-0.0.3 (c (n "polymap") (v "0.0.3") (h "1yzkszjyhbdv0yazzckwlx07zfsvx7h5r2pm76aksk3dbv95561p")))

(define-public crate-polymap-0.0.4 (c (n "polymap") (v "0.0.4") (h "1chabldnkkr5sgq0wnnj7kfjxn4cvd4bhswvsqwaff356b9id7zw")))

(define-public crate-polymap-0.0.5 (c (n "polymap") (v "0.0.5") (h "1vvkm6r92xprqjn0y2lq5hgjb810cxp3010z70fh9wn6aan1p1sf")))

(define-public crate-polymap-0.0.6 (c (n "polymap") (v "0.0.6") (h "08nbzrla375lp605y3c6vxzikmz3f49iw7zmazh3v8appixm702v")))

(define-public crate-polymap-0.1.0 (c (n "polymap") (v "0.1.0") (h "0b1xqaa7crfmax9s4r399a33yvga82x3xrdmprwmm62dpnlw1sf5")))

(define-public crate-polymap-0.1.1 (c (n "polymap") (v "0.1.1") (h "0ymvmccmyflm73n8r98wda06d684wmq5awqzqz5kpmmfqmh5j90f")))

(define-public crate-polymap-0.2.0 (c (n "polymap") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0vkdljny39ig8jgdhk33sh4y1dn5r11ljzah1kw7886i5wgxfrk6")))

(define-public crate-polymap-0.3.0 (c (n "polymap") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0prxg4zav2s0cfypzv02c9f9i9mqaxa0r8pfyk3glkl2zppk0nai")))

