(define-module (crates-io po ly polynomial-roots) #:use-module (crates-io))

(define-public crate-polynomial-roots-0.1.0 (c (n "polynomial-roots") (v "0.1.0") (h "12kzaiq6afa7r17w3p3jvq2ml6y4n98q3y58lzwsk4y3ar77bk8m")))

(define-public crate-polynomial-roots-1.0.0 (c (n "polynomial-roots") (v "1.0.0") (h "0d61lsgky6ygyprg9flji5yyavibz3wzyfs0fmbl1b6yddlzj87j")))

