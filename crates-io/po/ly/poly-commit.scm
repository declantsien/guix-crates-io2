(define-module (crates-io po ly poly-commit) #:use-module (crates-io))

(define-public crate-poly-commit-0.0.10 (c (n "poly-commit") (v "0.0.10") (d (list (d (n "bls-12-381") (r "^0.0.10") (k 0)) (d (n "ec-pairing") (r "^0.0.10") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zkstd") (r "^0.0.10") (k 0)))) (h "16qa9ba4dnznw5sirbpn9pqadbd0q1xhwq9aa3n7wrq3s3104m78") (f (quote (("std") ("default" "std"))))))

(define-public crate-poly-commit-0.0.11 (c (n "poly-commit") (v "0.0.11") (d (list (d (n "bls-12-381") (r "^0.0.12") (k 0)) (d (n "ec-pairing") (r "^0.0.11") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zkstd") (r "^0.0.12") (k 0)))) (h "1a4llh55hrjb21l200scn77qx549cfypc58b7knnmk0kz571jlgb") (f (quote (("std" "rand_core/getrandom") ("default" "std"))))))

(define-public crate-poly-commit-0.0.12 (c (n "poly-commit") (v "0.0.12") (d (list (d (n "bls-12-381") (r "^0.0.13") (k 0)) (d (n "ec-pairing") (r "^0.0.12") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "zkstd") (r "^0.0.13") (k 0)))) (h "01ydzvzxwfh09a5kdsv56nawz0jipsgb3s0psvv0gfqrr9zqm30f") (f (quote (("std" "rand_core/getrandom") ("default" "std"))))))

(define-public crate-poly-commit-0.0.13 (c (n "poly-commit") (v "0.0.13") (d (list (d (n "bls-12-381") (r "^0.0.23") (k 0)) (d (n "ec-pairing") (r "^0.0.14") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "zkstd") (r "^0.0.22") (k 0)))) (h "0ifpwn5szfmnd4vnmz1jll853rlnr1xhjv2xgg3fgp76khvq8rws") (f (quote (("std" "rand_core/getrandom" "rayon") ("default" "std"))))))

