(define-module (crates-io po ly polyrhythm) #:use-module (crates-io))

(define-public crate-polyrhythm-0.1.0 (c (n "polyrhythm") (v "0.1.0") (h "0qr76l6wlv3l07hwdmcxfvdawz08yd62w9msiaxa6v8l7fkjfxx1")))

(define-public crate-polyrhythm-0.1.1 (c (n "polyrhythm") (v "0.1.1") (h "0yxfmsik7mhnd46xyzbs0rrkgx24ifgsgqcmzbb1wkkbhya6wvch")))

