(define-module (crates-io po ly polyhedra) #:use-module (crates-io))

(define-public crate-polyhedra-0.1.0 (c (n "polyhedra") (v "0.1.0") (d (list (d (n "kiss3d") (r "^0.23.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0z5lvjdrx6pa4dqw7q0qfgyacg7zwyy7kf3nz583msw6zphflvn8")))

