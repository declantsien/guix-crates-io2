(define-module (crates-io po ly polyhorn-build) #:use-module (crates-io))

(define-public crate-polyhorn-build-0.1.0 (c (n "polyhorn-build") (v "0.1.0") (d (list (d (n "polyhorn-build-ios") (r "^0.1.0") (d #t) (k 0)))) (h "1blkc214vfhp0fikakmp2s6z1xnk31d7niazgcrmlmyq25chpc85")))

(define-public crate-polyhorn-build-0.2.0 (c (n "polyhorn-build") (v "0.2.0") (d (list (d (n "polyhorn-build-ios") (r "^0.2.0") (d #t) (k 0)))) (h "1nicw0l4mkii37cspcy6d1az5bb4qacfgx1sv74s6qf3jimjzhg5")))

(define-public crate-polyhorn-build-0.3.0 (c (n "polyhorn-build") (v "0.3.0") (d (list (d (n "polyhorn-build-android") (r "^0.3.0") (d #t) (k 0)) (d (n "polyhorn-build-ios") (r "^0.3.0") (d #t) (k 0)))) (h "0l6bfy037ijyzc8wy1l49irkdgwiajd5q72fc5f09fdfa5kbsdhz")))

(define-public crate-polyhorn-build-0.3.1 (c (n "polyhorn-build") (v "0.3.1") (d (list (d (n "polyhorn-build-android") (r "^0.3.0") (d #t) (k 0)) (d (n "polyhorn-build-ios") (r "^0.3.0") (d #t) (k 0)))) (h "069y6bqb5anvaqjbpvcv2pnz4ym796hphdyn409b337l40mhqf2f")))

(define-public crate-polyhorn-build-0.3.2 (c (n "polyhorn-build") (v "0.3.2") (d (list (d (n "polyhorn-build-android") (r "^0.3.2") (d #t) (k 0)) (d (n "polyhorn-build-ios") (r "^0.3.2") (d #t) (k 0)))) (h "0spc9ni5w3xpcy45jfiwqrl6hdw0b4z74aa030n4rfd5028g9x4j")))

(define-public crate-polyhorn-build-0.4.0 (c (n "polyhorn-build") (v "0.4.0") (d (list (d (n "polyhorn-build-android") (r "^0.4.0") (d #t) (k 0)) (d (n "polyhorn-build-ios") (r "^0.4.0") (d #t) (k 0)))) (h "1d42kyqj22dqw5n3d8cbqlijnpfhh3k9h4csq0h8isg15lchxips")))

(define-public crate-polyhorn-build-0.4.1 (c (n "polyhorn-build") (v "0.4.1") (d (list (d (n "polyhorn-build-android") (r "^0.4.0") (d #t) (k 0)) (d (n "polyhorn-build-ios") (r "^0.4.0") (d #t) (k 0)))) (h "1ki43f1wvcnppwrqc4xlrzinnmz63m56hwj190qylvjqyi9yh43s")))

