(define-module (crates-io po ly polysite) #:use-module (crates-io))

(define-public crate-polysite-0.0.1 (c (n "polysite") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fronma") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18wpcn2nayjik7cb0v6mdqdlpmbwh6k2js7sb1fhijqlwgdapz5c")))

