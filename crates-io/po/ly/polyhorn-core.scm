(define-module (crates-io po ly polyhorn-core) #:use-module (crates-io))

(define-public crate-polyhorn-core-0.1.0 (c (n "polyhorn-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "0k1r6sm1xq7kgyiz3fcrb82c3ak1k7y7hz6f2p1bs3lqlkbpjr52")))

(define-public crate-polyhorn-core-0.2.0 (c (n "polyhorn-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "19zhzkzlyw158q9zxq6ar24686q6jijq95g8a564aak8j5nq6mr4")))

(define-public crate-polyhorn-core-0.3.0 (c (n "polyhorn-core") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "1z6phwrgkccmzwn79csrvs96dy9zd36dmhyciq0c9c4ybzxcjmwn")))

(define-public crate-polyhorn-core-0.3.1 (c (n "polyhorn-core") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "13kmarg9j9pa9i9qbrsb7d27dbgnqnfxsd1cjckf27vwfnxry7sk")))

(define-public crate-polyhorn-core-0.3.2 (c (n "polyhorn-core") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "1bqssg3cp073wj8yn041kvp0061cly1j26d9y0csfjgq8zzmnxb7")))

(define-public crate-polyhorn-core-0.4.0 (c (n "polyhorn-core") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)))) (h "1jp0zrig6l33wm66v9bqwm025c7sq3yav97zyjq17s6062bql9lw")))

