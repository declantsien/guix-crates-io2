(define-module (crates-io po ly polyhaven) #:use-module (crates-io))

(define-public crate-polyhaven-0.1.0 (c (n "polyhaven") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11g1hwgmldklvfmi62ihpl5xilw3jwbd37rg67gymm81518f9x0x")))

(define-public crate-polyhaven-0.1.1 (c (n "polyhaven") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03mjwglqjw3xfjlzd3dnhaii3gmcv5869lmk7hnk1v7vmwxip2zw")))

(define-public crate-polyhaven-0.2.0 (c (n "polyhaven") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nsyq388xg58l85zcjg075jia3vj03m52r87nxmbxrk74pq0zaxq")))

(define-public crate-polyhaven-0.2.1 (c (n "polyhaven") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wydl6njwhfaaam57z0qf4qmmqcdd6541zjgwhw8x3nibzwkg2i8")))

(define-public crate-polyhaven-0.2.2 (c (n "polyhaven") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hd4rn9imnf0si8svbjllcixcvlg87g96nbj4h75gwdr6jj6fbk7")))

(define-public crate-polyhaven-0.2.3 (c (n "polyhaven") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13wsxqp4c5k8q623a8730gsvcm0jpnaalv0cb42vqv63y8raijhf")))

(define-public crate-polyhaven-0.2.4 (c (n "polyhaven") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gi64rw51aldd3raz7v91kk0i6byh2sr563r94ychgm1i171plp0")))

(define-public crate-polyhaven-0.3.0 (c (n "polyhaven") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qfb09jpn436m27v7h5r91p8nlvxm0gfqcx99isn29z7dkw8769q")))

