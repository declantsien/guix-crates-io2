(define-module (crates-io po ly polywrap_core_macros) #:use-module (crates-io))

(define-public crate-polywrap_core_macros-0.1.6-beta.8 (c (n "polywrap_core_macros") (v "0.1.6-beta.8") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "184pyi7bpjvk1qvk94acn6libjyxjq4qg4swv29k33klpb1a58k1") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.9 (c (n "polywrap_core_macros") (v "0.1.6-beta.9") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "16zzb86x4qfzibw7gjwyh0vfmm9c6yr670sx4x11a84d3lh89xac") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.10 (c (n "polywrap_core_macros") (v "0.1.6-beta.10") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "19c6ixk211gm7p2ig2a3ng2673xmrams4f3b6hmpvc33l331fx0r") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.11 (c (n "polywrap_core_macros") (v "0.1.6-beta.11") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1gjszgz585554ikjqjpivjpsj066axd3815v39ann9zslic3wriz") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.12 (c (n "polywrap_core_macros") (v "0.1.6-beta.12") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0imrw3jgylacwz7kdv30v890hk9qlgqnx9syxmmg3gfx48fw6b4l") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.13 (c (n "polywrap_core_macros") (v "0.1.6-beta.13") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.13") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0zgyk8qf0nfr5f6zvnd2rpgf276h2vl98p9bdilnnyplapr6wsnw") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.6-beta.14 (c (n "polywrap_core_macros") (v "0.1.6-beta.14") (d (list (d (n "polywrap_uri") (r "^0.1.6-beta.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1np9dgq1ln1pw71phzb6p7h6r09bbz8q4ahnvr3pg0v8f05ib1dv") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.7 (c (n "polywrap_core_macros") (v "0.1.7") (d (list (d (n "polywrap_uri") (r "^0.1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1hl6cz6v1ack3l95z0f452wibkyb5cfxahc1fpgzp0viwykhl8fx") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.8-beta.2 (c (n "polywrap_core_macros") (v "0.1.8-beta.2") (d (list (d (n "polywrap_uri") (r "^0.1.8-beta.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "03acx9rs562ik4pgrahjrpjr9xawxk4q8yw52d8j15zgyzr7fjx4") (r "1.69")))

(define-public crate-polywrap_core_macros-0.1.8 (c (n "polywrap_core_macros") (v "0.1.8") (d (list (d (n "polywrap_uri") (r "^0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "16q9cxc67k5814drdg2vsaxppkbjgq2rns9r099hi09llzsgqlfn") (r "1.70")))

(define-public crate-polywrap_core_macros-0.1.9-beta.1 (c (n "polywrap_core_macros") (v "0.1.9-beta.1") (d (list (d (n "polywrap_uri") (r "^0.1.9-beta.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1423bawnhv8fymp1fja5j1f04xmvsqykjlnxfq4p08p7sm8b1ixk") (r "1.70")))

(define-public crate-polywrap_core_macros-0.1.9-beta.2 (c (n "polywrap_core_macros") (v "0.1.9-beta.2") (d (list (d (n "polywrap_uri") (r "^0.1.9-beta.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1b9z7bnv1msn5jxnjn7i9cghm9crglvb8vzgz2pm3s1rwdk8cij1") (r "1.70")))

(define-public crate-polywrap_core_macros-0.1.9 (c (n "polywrap_core_macros") (v "0.1.9") (d (list (d (n "polywrap_uri") (r "^0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0ihnmxmgs5k8qvkgmhc5c5i8flxdvp2xz028n26v50alz6y5rq14") (r "1.70")))

(define-public crate-polywrap_core_macros-0.1.10 (c (n "polywrap_core_macros") (v "0.1.10") (d (list (d (n "polywrap_uri") (r "^0.1.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0pg8a54xpg1p0pw8q70619mhjf805pznq0g31z12jmfk27yyakqz") (r "1.70")))

(define-public crate-polywrap_core_macros-0.1.11 (c (n "polywrap_core_macros") (v "0.1.11") (d (list (d (n "polywrap_uri") (r "^0.1.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0h0wp4yy1imclks54myv8yndwqr8ya1ks9kwlhm25mqhqkxcalzn") (r "1.70")))

