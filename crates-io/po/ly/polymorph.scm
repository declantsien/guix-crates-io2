(define-module (crates-io po ly polymorph) #:use-module (crates-io))

(define-public crate-polymorph-0.1.0 (c (n "polymorph") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 2)) (d (n "dyn-clone") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^1.5.0") (d #t) (k 2)))) (h "182nasy1mgckscpnij9pjcvlpwaf9phpa6c4xh98kndiaq8425zj") (f (quote (("trait-clone" "dyn-clone")))) (r "1.56")))

