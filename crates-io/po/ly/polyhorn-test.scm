(define-module (crates-io po ly polyhorn-test) #:use-module (crates-io))

(define-public crate-polyhorn-test-0.4.0 (c (n "polyhorn-test") (v "0.4.0") (d (list (d (n "ctor") (r "^0.1.16") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "polyhorn") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json" "native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (f (quote ("rt-core" "stream" "sync" "time"))) (d #t) (k 0)))) (h "1c45bz54xg5nbil5f9516phx11y950vdvnwalnmf2wrv4sv8b1i9")))

(define-public crate-polyhorn-test-0.4.1 (c (n "polyhorn-test") (v "0.4.1") (d (list (d (n "ctor") (r "^0.1.16") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "polyhorn") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json" "native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (f (quote ("rt-core" "stream" "sync" "time"))) (d #t) (k 0)))) (h "0pv4i2gmyz4gnq6h9iblv2m9nfmb340x230lfd4zbsa5vfrhbcwa")))

