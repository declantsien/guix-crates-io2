(define-module (crates-io po ly polyhorn-build-ios) #:use-module (crates-io))

(define-public crate-polyhorn-build-ios-0.1.0 (c (n "polyhorn-build-ios") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "0rs0xliz2bxk1m5ka2nwci44bkvy6161sy34fzxv06jjd6x7gvz3")))

(define-public crate-polyhorn-build-ios-0.2.0 (c (n "polyhorn-build-ios") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "0nrps5ifsqsmz7c3i1wy16wxgz4ii4znbj75c5mnkq6bx3w3wdq1")))

(define-public crate-polyhorn-build-ios-0.3.0 (c (n "polyhorn-build-ios") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "1a8yvp5pyskr1xkjy7g1mf67q1lrpwjx467zcwzjbanx4zzg7iqj")))

(define-public crate-polyhorn-build-ios-0.3.1 (c (n "polyhorn-build-ios") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "19m97g1wq4w4y0ji0h78bx6hzvvbgx36c85v87wgx1irgpmvd6zh")))

(define-public crate-polyhorn-build-ios-0.3.2 (c (n "polyhorn-build-ios") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "15rv6p5a2mr9hwzbaay3cg58nsz48f9br4115z148qx9dx5789b1")))

(define-public crate-polyhorn-build-ios-0.4.0 (c (n "polyhorn-build-ios") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 0)))) (h "0zw1ij5r7abxy40wkvj1ib2j7rxmx6rq62x1m3cphbi7drwijl8z")))

