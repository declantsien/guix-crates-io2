(define-module (crates-io po ly polygonical) #:use-module (crates-io))

(define-public crate-polygonical-0.1.0 (c (n "polygonical") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "18ma7cs9ydvzcw11zqpzwhxbfhrb8j3x5al81arcmf0ymcjfr20s")))

(define-public crate-polygonical-0.2.0 (c (n "polygonical") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0b19a5s40x5sch2bc6ckpk3la9vkbwamczvw1xrwjkivvmn487py")))

(define-public crate-polygonical-0.3.0 (c (n "polygonical") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "0z012h3hz1lgqqw6zw75fjk6kv2mc4052zlvysb2iii2mxhzvd1j")))

(define-public crate-polygonical-0.4.0 (c (n "polygonical") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "1x0zla2xmghqlj640bn1p36jqncz3928dgxf7s6rwf9acqgfm4fw")))

(define-public crate-polygonical-0.5.0 (c (n "polygonical") (v "0.5.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)))) (h "11mcn8i2xq1j7crf3bmwwvp6jm8jczbmf7lh71alb6f3majm9sm7")))

