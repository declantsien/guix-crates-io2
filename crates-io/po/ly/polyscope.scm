(define-module (crates-io po ly polyscope) #:use-module (crates-io))

(define-public crate-polyscope-0.1.0 (c (n "polyscope") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)))) (h "0c7y0r9wca6f6w6648qv850czy3fl60dr0vz7hhyryb76x5m0hkx")))

(define-public crate-polyscope-0.2.0 (c (n "polyscope") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)))) (h "0lbaxnz0kp0n7wwsyprx449qwrfrlmxzjz3h8s974bx0s6fdqfny")))

(define-public crate-polyscope-0.2.1 (c (n "polyscope") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.4") (d #t) (k 0)))) (h "0vy30i6lf2awd14h3pmfcfc1gg3s37r8fmkx6mrwyz2vxjibx68s")))

