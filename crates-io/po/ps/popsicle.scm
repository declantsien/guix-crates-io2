(define-module (crates-io po ps popsicle) #:use-module (crates-io))

(define-public crate-popsicle-0.1.5 (c (n "popsicle") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b68awyclxibc3h7l3m8rkdzv3yvzhmrzjizw3b54gr6zcy1542i")))

