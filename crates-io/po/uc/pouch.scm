(define-module (crates-io po uc pouch) #:use-module (crates-io))

(define-public crate-pouch-0.1.0 (c (n "pouch") (v "0.1.0") (h "082ii6jyqj076vycwihi7i61a901piq9gzlspln6rssqp96yp4gy") (y #t)))

(define-public crate-pouch-0.0.2 (c (n "pouch") (v "0.0.2") (h "074n09na0wljrfaqbasr9p88maln0g9avsaz7yawjzhrrdqbwl9g") (y #t)))

(define-public crate-pouch-0.0.3 (c (n "pouch") (v "0.0.3") (h "07675l2mjz0dyp7d1hk5bc206jsi0am41aknsn7aq5klbynszsjx") (y #t)))

(define-public crate-pouch-0.0.3-alpha (c (n "pouch") (v "0.0.3-alpha") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "034606wcjhw4gx7rgbs51lh9r1yf92y9jb9imkzimssa2wgblmm7") (y #t)))

(define-public crate-pouch-0.0.4-alpha (c (n "pouch") (v "0.0.4-alpha") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0m0i8f7m7hr5rk33aj9dfr0mlq255c73vi9c9b7xxkb0h898f1w3") (y #t)))

(define-public crate-pouch-0.0.5-alpha (c (n "pouch") (v "0.0.5-alpha") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.21") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "02zckp6hxa39pp0d1rr08ry73zr94q9fsri4nqvmnnjld9c53zzy")))

(define-public crate-pouch-0.0.6-alpha (c (n "pouch") (v "0.0.6-alpha") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.21") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "08wr5pxsmvwykxjnghfffpjsazw94d0jp4lkjrv4h9ph6p5kc32z")))

(define-public crate-pouch-0.0.7-alpha (c (n "pouch") (v "0.0.7-alpha") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.21") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1r22naiv6zn4g01g7wlj4v32dhsmgvxri9z7bc7nnnk1rhld1xyg")))

