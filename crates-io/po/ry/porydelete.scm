(define-module (crates-io po ry porydelete) #:use-module (crates-io))

(define-public crate-porydelete-2.0.1 (c (n "porydelete") (v "2.0.1") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q40nkjiyk65zy2mkxajlngb5f9v1472np49m9sk9r4g3kpzpaka") (y #t)))

(define-public crate-porydelete-0.2.1 (c (n "porydelete") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wlwf3864l7gk0fz1mrgdi5p989f9gf5kgsbz5r3gwz0y33qdqdz")))

