(define-module (crates-io po rc porco) #:use-module (crates-io))

(define-public crate-porco-0.1.0 (c (n "porco") (v "0.1.0") (h "0h5gk3jih97bg05wwzarqmzzynb59p4fagrw6gh3ix3ipxibd87m")))

(define-public crate-porco-0.1.1 (c (n "porco") (v "0.1.1") (h "04y4f9wf6sa0gxhi07c27yq6y8irvddx9r1jh60s4vc8ca5h3150")))

(define-public crate-porco-0.1.2 (c (n "porco") (v "0.1.2") (d (list (d (n "assoc") (r "^0.1.0") (d #t) (k 0)))) (h "1dr605jkzyf4zyzbcrpdjgs5dspvm54z70iwwp9fkn3d8ffzar1j")))

(define-public crate-porco-0.1.3 (c (n "porco") (v "0.1.3") (d (list (d (n "assoc") (r "^0.1.2") (d #t) (k 0)))) (h "1yx5w3qn0v60k232x8hh2zf58v154jkhd6bcvsgpr3ds9s4hm4gn")))

(define-public crate-porco-0.1.4 (c (n "porco") (v "0.1.4") (d (list (d (n "assoc") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "0wdmlhkq0hknqwjrwwz8bbxnv95xl6sfyqwrav00v6hfq6q607mw")))

