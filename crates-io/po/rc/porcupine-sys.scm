(define-module (crates-io po rc porcupine-sys) #:use-module (crates-io))

(define-public crate-porcupine-sys-0.0.1 (c (n "porcupine-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00v5xwlc2w5szc5yarg755gkyp5yrfaxd51yvf4ba1dq0ksn2fkp")))

(define-public crate-porcupine-sys-0.0.2 (c (n "porcupine-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fqqi9csx21xgs88w0b63abnjk8p9090wqzsxd6ply29246xkk97")))

(define-public crate-porcupine-sys-0.0.3 (c (n "porcupine-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1skh1i4cfjl9yy8zwsxvzx282aln73cr4j814j8f0g65wxchphmn")))

(define-public crate-porcupine-sys-0.0.4 (c (n "porcupine-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "081lgni34r67h17iic1nvlnji02wq2qiyh030s5d4x2hfsnh6s26")))

