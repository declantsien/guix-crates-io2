(define-module (crates-io po ll poll_token_derive) #:use-module (crates-io))

(define-public crate-poll_token_derive-0.1.0 (c (n "poll_token_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10s67by4vpnszspngx2b97dhpghpzx3lfnk5b8v42h6iynlikd6d")))

