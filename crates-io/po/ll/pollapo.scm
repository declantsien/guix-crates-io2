(define-module (crates-io po ll pollapo) #:use-module (crates-io))

(define-public crate-pollapo-0.0.1 (c (n "pollapo") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "github-rs") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "open") (r "^2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "spinners") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ag4r9lfg1i880k3gvw8nbbg0vdqbd9h0jfmq40gbxnyz2j4zjbk")))

(define-public crate-pollapo-0.0.2 (c (n "pollapo") (v "0.0.2") (h "1gmbq90igw9h5jhh3m6j7cjslmzpz4w4mn0gc7fb3q992xlr3qwd")))

