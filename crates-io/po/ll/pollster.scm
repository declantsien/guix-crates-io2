(define-module (crates-io po ll pollster) #:use-module (crates-io))

(define-public crate-pollster-0.1.0 (c (n "pollster") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)))) (h "1vy80wlalib608ryiiwx8ssm988n2gs9qqq6ivsx9xp818mz0ixn")))

(define-public crate-pollster-0.2.0 (c (n "pollster") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "0n5bl7hdn14pdymjhbvw8k4smhp8ifp346gs71d0ngq0hn7f294q")))

(define-public crate-pollster-0.2.1 (c (n "pollster") (v "0.2.1") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "0yx0s915whsklyk6l0livn7ckfcjcws3nav47f9abb2f8ry4430s")))

(define-public crate-pollster-0.2.2 (c (n "pollster") (v "0.2.2") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "0qz66pkcnhr9ygllbalf7lxasxfs9gjf2gqxdw01xmkdfm61nwkm")))

(define-public crate-pollster-0.2.3 (c (n "pollster") (v "0.2.3") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "0i6rqv7yvscnl6nm7wa4db13bmbrazvhch0f3biwnsk4s9pi1kkc")))

(define-public crate-pollster-0.2.4 (c (n "pollster") (v "0.2.4") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "03p3jbnz2avz2alwsdfg9lsyfbxvk7ix0zflfn75189n0p1xq85v")))

(define-public crate-pollster-0.2.5 (c (n "pollster") (v "0.2.5") (d (list (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 2)))) (h "1xzji8cq4crr795c7liy3ksd798sb45pjphbm8h5gvnp7whb18sx")))

(define-public crate-pollster-0.3.0 (c (n "pollster") (v "0.3.0") (d (list (d (n "pollster-macro") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-timer") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 2)))) (h "1wn73ljx1pcb4p69jyiz206idj7nkfqknfvdhp64yaphhm3nys12") (f (quote (("macro" "pollster-macro"))))))

