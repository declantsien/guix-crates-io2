(define-module (crates-io po ll polling-async-trait) #:use-module (crates-io))

(define-public crate-polling-async-trait-0.1.0 (c (n "polling-async-trait") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0zkby3s9y0v8w85slrz35flrjf6nc58sa337hii6wa4nw93r40x8")))

(define-public crate-polling-async-trait-0.1.1 (c (n "polling-async-trait") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0p4h47p1x0wrf7w2rp3wp7vk84vbwj5v02ry2wb4y1ri6gzmrs8f")))

