(define-module (crates-io po ll poll-channel) #:use-module (crates-io))

(define-public crate-poll-channel-0.1.0 (c (n "poll-channel") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "03l6r212ans53g9bd9mcv4fsdlsk1bnp57yapnvv7n80yi3wr80k")))

(define-public crate-poll-channel-0.1.1 (c (n "poll-channel") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "04c6x0misv1kkggqr5l11j7y997nhxkgbslca1x4ghpwihanjikn")))

(define-public crate-poll-channel-0.1.2 (c (n "poll-channel") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "1jb3sg78gjs35cm9w2rdwha7s9g2n69l2ak70vfzx75p00a797a8")))

(define-public crate-poll-channel-0.1.3 (c (n "poll-channel") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "0469w2k3kl8i9svswqghmaanv6vq17ym1z9k9pbckz0qzn7x344q")))

(define-public crate-poll-channel-0.1.4 (c (n "poll-channel") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "1wljfrnb1pyhcl74y8ymabw99ashjwgjkcb3ww277fkivgk8yy17")))

(define-public crate-poll-channel-0.1.5 (c (n "poll-channel") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "1w47gbj7b83fxrvvn72ci3khai6m04fvv9nvy963nqp7r0pjyvp0")))

(define-public crate-poll-channel-0.1.6 (c (n "poll-channel") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "033lb5janlz1qfcag8d0waky5wc92zmnp6chwmyn9d87q87jsfd9")))

(define-public crate-poll-channel-0.1.7 (c (n "poll-channel") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "1d925qn1g38mh443hc6g4hb9iycvzq91cldmjssy9x011imkv3z5")))

(define-public crate-poll-channel-0.1.8 (c (n "poll-channel") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "134rpvw2mjwinzns800xybvzvpgblc9j9ys0kimxc5awvy8iblb3")))

(define-public crate-poll-channel-0.1.9 (c (n "poll-channel") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.8") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "1w83hf82a1phrj6672aar9bdh2c6lf0lgkni4k22y5x1cksvgrm5")))

