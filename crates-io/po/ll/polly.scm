(define-module (crates-io po ll polly) #:use-module (crates-io))

(define-public crate-polly-0.1.0 (c (n "polly") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "1b34yz9y97c5xlr82rkpdlxxw9wbyi8rj3mv9zlxkpxmswfr3g3y")))

(define-public crate-polly-0.1.1 (c (n "polly") (v "0.1.1") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "0v24qx1zkkv4dri47ia6ripk0fs59nlz259lmcsrx6rqhyijm6yv")))

(define-public crate-polly-0.1.2 (c (n "polly") (v "0.1.2") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "03cc8iji9kdbj5gjr2ilhw151y6xb34vx778kfvwmpb1q3g410c0")))

