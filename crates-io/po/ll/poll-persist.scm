(define-module (crates-io po ll poll-persist) #:use-module (crates-io))

(define-public crate-poll-persist-0.0.1 (c (n "poll-persist") (v "0.0.1") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("test-util" "macros" "time"))) (d #t) (k 2)))) (h "0qbj8sgz4i7mbf3l4bkpr5s5rgby2acfmxi671i2maknh5bfwsqr")))

