(define-module (crates-io po ll pollster-macro) #:use-module (crates-io))

(define-public crate-pollster-macro-0.1.0 (c (n "pollster-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "00fk326pj6pam402ygh2srs13bbjnnyfck41155ml1ck87pz0y7a")))

