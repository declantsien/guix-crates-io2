(define-module (crates-io po ll poller) #:use-module (crates-io))

(define-public crate-poller-0.1.0 (c (n "poller") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "1shk1mhv6vyxpsrlvpw6v3ky6l8qmxxwjd56vv4p51bvdn059zy6")))

(define-public crate-poller-0.1.1 (c (n "poller") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "1qqzk5samychak0ih0y724vv6qr12wyz6yil57zpsndy9kzw5y8h")))

(define-public crate-poller-0.1.2 (c (n "poller") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "0zgx42j5v06dfzn15n3baa3ww4hnwb4ii1ckkdlgm6hh8ww0g0di")))

(define-public crate-poller-0.2.0 (c (n "poller") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1kh0dgm7a4ng66pyknb07qykid04c63i17i6b875i7jf843fpw4z")))

