(define-module (crates-io po ll poll-buf-utils) #:use-module (crates-io))

(define-public crate-poll-buf-utils-0.0.0 (c (n "poll-buf-utils") (v "0.0.0") (d (list (d (n "bytes") (r ">=0.0.1, <0.6") (d #t) (k 0)) (d (n "futures-io") (r ">=0.2, <0.4") (d #t) (k 0)))) (h "1d01hrwr32bs4gi040gs95h6wv622rgindk124cphq6m11gvs0c2")))

(define-public crate-poll-buf-utils-0.1.0 (c (n "poll-buf-utils") (v "0.1.0") (d (list (d (n "bytes") (r ">=0.0.1, <0.6") (d #t) (k 0)) (d (n "futures-io") (r ">=0.2, <0.4") (d #t) (k 0)))) (h "0j8jikall2lzylf8zalf8hc84xx2794giizhm7gsnyxapx5amm6z")))

(define-public crate-poll-buf-utils-0.2.0 (c (n "poll-buf-utils") (v "0.2.0") (d (list (d (n "bytes") (r ">=0.0.1, <0.6") (d #t) (k 0)) (d (n "futures-io") (r ">=0.2, <0.4") (d #t) (k 0)))) (h "015k4a42csdphdwkdagjbav9a1iz4yzg36ra0l6xx16wx5vp418j")))

