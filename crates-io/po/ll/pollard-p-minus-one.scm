(define-module (crates-io po ll pollard-p-minus-one) #:use-module (crates-io))

(define-public crate-pollard-p-minus-one-0.1.0 (c (n "pollard-p-minus-one") (v "0.1.0") (d (list (d (n "primal") (r "^0.3.0") (d #t) (k 0)) (d (n "ramp") (r "^0.5.9") (d #t) (k 0)))) (h "0lpl9s28kc46zqc7y2jz54cis4lrpc1andc0vqvz3jym7vrcg3hr")))

