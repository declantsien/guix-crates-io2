(define-module (crates-io po dc podclaw) #:use-module (crates-io))

(define-public crate-podclaw-1.0.1 (c (n "podclaw") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rss") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)))) (h "19241i51apapdb3zkiijlbal8ldbgll2xkdy9jib30yj27rwp5dr")))

