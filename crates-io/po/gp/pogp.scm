(define-module (crates-io po gp pogp) #:use-module (crates-io))

(define-public crate-pogp-0.1.0 (c (n "pogp") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "16az98zr51mda6wvpj59w79psz7pdgvbnsr3ji0lzjapywxk87pi") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

(define-public crate-pogp-0.0.5 (c (n "pogp") (v "0.0.5") (h "1kmv8868mp5prxl9rr0qip7mkzi8wjnm0q7swr4229yyl2xsmz5w")))

(define-public crate-pogp-0.0.6 (c (n "pogp") (v "0.0.6") (h "0lahq6sdf2wg7zyz4fyc1171b0r8vr5ziswncbw3fzwzvay5lmvh")))

(define-public crate-pogp-0.0.7 (c (n "pogp") (v "0.0.7") (h "0npsn6wm028n9k4zfampfcsn1kx79v99qh201fv8vinzkavp5hgf")))

(define-public crate-pogp-0.0.8 (c (n "pogp") (v "0.0.8") (h "03yn8qg0rp9j0b9xmhypw50xkv5f7lr170gg913n1rhsqk40crl9")))

(define-public crate-pogp-0.0.9 (c (n "pogp") (v "0.0.9") (h "119z2h75f6ky7d5p8h5hxjqpw7g8y93hyjk5b3902silyk1pnnhx")))

(define-public crate-pogp-0.0.10 (c (n "pogp") (v "0.0.10") (h "0bsljhhk4qkpzxvan2177g2rzxhfhdpw2vq11wbl884sqyqnjlr8")))

(define-public crate-pogp-0.0.11 (c (n "pogp") (v "0.0.11") (h "02mr3ypwdb7v86n1mgdnp7f0kd8n6g1g7vzn2gwpn1pkncicyw1q")))

(define-public crate-pogp-0.0.14 (c (n "pogp") (v "0.0.14") (h "0i30nvxk75n1l9qmzwbfq7m66nq0ca8lbd9wssnqc2669yr7i1vh")))

(define-public crate-pogp-0.0.16 (c (n "pogp") (v "0.0.16") (h "1q7yh4q7svmg85yb6viradgcwn636dbx73zffws7khdij1np7p3s")))

(define-public crate-pogp-0.0.17 (c (n "pogp") (v "0.0.17") (h "12scp2p5qnclpp51hp40y6pn9zy6dnqd6zgrd4ypdn1153pdm6vz")))

(define-public crate-pogp-0.0.18 (c (n "pogp") (v "0.0.18") (h "1q7hhbw2v6a7jd2mmzsv485xrq2h48hj88vzi7h42lcnqc9sw53z")))

(define-public crate-pogp-0.0.19 (c (n "pogp") (v "0.0.19") (h "1vvj5cx7damsgn862rh46d3hldggbdjhbjpwn78c0cf2yf4vm7kl")))

