(define-module (crates-io po gp pogpen) #:use-module (crates-io))

(define-public crate-pogpen-0.2.0 (c (n "pogpen") (v "0.2.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "handlebars") (r "^0.26.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1mq7sj33d76b5qkc7cw9srn28vwflgpzafpizfg74ja0n6yl952x")))

