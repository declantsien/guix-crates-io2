(define-module (crates-io po pu populated) #:use-module (crates-io))

(define-public crate-populated-0.0.1 (c (n "populated") (v "0.0.1") (h "1pnh8gi93x5krib6x7yh4vak4k61irglcmp9v0p98a0q7a9x5ajh")))

(define-public crate-populated-0.0.2 (c (n "populated") (v "0.0.2") (h "01grx99s6jljljhk6iwwms1cllzx47d6id28p25vznvwcfxh5xa2")))

(define-public crate-populated-0.0.3 (c (n "populated") (v "0.0.3") (h "1ilwfb8swamglvqfsl0xdyvnjb5dwinr7f7n60i0argfsp9lfsm1")))

(define-public crate-populated-0.0.4 (c (n "populated") (v "0.0.4") (h "1axvvpq4pahk3iwpzwr2y5fjp5mbd8xx9wb4lwwjk6gh0ysh0hsj")))

(define-public crate-populated-0.0.5 (c (n "populated") (v "0.0.5") (h "00d0za5vhhq9skhy41h96r4m2b4z0ifsggyhj1rz634h7qbdkijs")))

