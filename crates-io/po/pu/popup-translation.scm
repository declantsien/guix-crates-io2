(define-module (crates-io po pu popup-translation) #:use-module (crates-io))

(define-public crate-popup-translation-0.2.2 (c (n "popup-translation") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wry") (r "^0.26.0") (d #t) (k 0)) (d (n "x11-clipboard") (r "^0.7.0") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (t "cfg(windows)") (k 0)))) (h "1bp1my4d8hx6mm28pbix9d9yh2crwkkp9hxliqzj1ygqd81jh8qc")))

