(define-module (crates-io po e_ poe_bundle) #:use-module (crates-io))

(define-public crate-poe_bundle-0.1.0 (c (n "poe_bundle") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0d0qbgx6ly04ipqymh3vmg8d86g41x9yhrcj6qfi006hp8h59qk8") (l "libooz")))

(define-public crate-poe_bundle-0.1.1 (c (n "poe_bundle") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1a078m3s8kaf4p9xg8y73qsajbk6pr2s7p1ivb0ky8bxnhrrv8js") (l "libooz")))

(define-public crate-poe_bundle-0.1.2 (c (n "poe_bundle") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0qkbhdcy8kjqim6idcqvshp1sdl6ngjs9kiaz6h0a7zg0badbl32") (l "libooz")))

(define-public crate-poe_bundle-0.1.3 (c (n "poe_bundle") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0acb86y27y55d8ac208imfxkxi1p0wngchs8ps220xvhqazdcanz") (l "libooz")))

(define-public crate-poe_bundle-0.1.4 (c (n "poe_bundle") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "14fv9j9qhrgyb12llcqbgq2v5cxgs2wykc8f9dlyp88k5a6b5cw5") (l "libooz")))

(define-public crate-poe_bundle-0.1.5 (c (n "poe_bundle") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "ggpk") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1fp8kfgc4w6m0v91fpdkazvxaj14lm30sl999csnv6cmk73a8xy7") (l "libooz")))

