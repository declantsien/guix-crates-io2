(define-module (crates-io po is poise-i18n) #:use-module (crates-io))

(define-public crate-poise-i18n-0.1.0 (c (n "poise-i18n") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "poise") (r "^0.5.7") (d #t) (k 0)) (d (n "rusty18n") (r "^0.1.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qj0bzbv90zvba5q0dk9sxqjhy0kb4vikhlgi8mxb8r2q9r65hkg")))

(define-public crate-poise-i18n-0.1.1 (c (n "poise-i18n") (v "0.1.1") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "poise") (r "^0.5.7") (d #t) (k 0)) (d (n "rusty18n") (r "^0.1.1") (f (quote ("bevy_reflect"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13g54cxpwkyfl7jww982cf8gskhw16y2yxqcfp1kp5lrzpidjyqm")))

