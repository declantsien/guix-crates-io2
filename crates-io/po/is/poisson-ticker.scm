(define-module (crates-io po is poisson-ticker) #:use-module (crates-io))

(define-public crate-poisson-ticker-0.1.0 (c (n "poisson-ticker") (v "0.1.0") (d (list (d (n "async-timer") (r "^0.7") (f (quote ("stream" "tokio_on"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1yw3b07xbadl3gyajsxacdx6saz82k09kdsh3mbv6vip3sm8l0x3")))

