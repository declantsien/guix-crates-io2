(define-module (crates-io po is poisson) #:use-module (crates-io))

(define-public crate-poisson-0.1.0 (c (n "poisson") (v "0.1.0") (d (list (d (n "image") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1zgxa2naqkwmwf0pxrbz65p4cpqy8dxsl1sr75c9pwc7lnadmk3c") (f (quote (("visualise" "image"))))))

(define-public crate-poisson-0.2.0 (c (n "poisson") (v "0.2.0") (d (list (d (n "image") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "0lcd6lp6abxyfa03pmzm081imc0703mc9h2a473xnpgvxkkih29p") (f (quote (("visualise" "image"))))))

(define-public crate-poisson-0.3.0 (c (n "poisson") (v "0.3.0") (d (list (d (n "image") (r "^0.3.10") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1dx9xrn2dgkpl6gvcc9bnsqalcqb8fnqr024a3yxxl115j59j1pn") (f (quote (("visualise" "image"))))))

(define-public crate-poisson-0.4.0 (c (n "poisson") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.17") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1kfyn3zwabczb25gbzw88hkia8bkx9dj3h395hpk5da8vyjanw7m")))

(define-public crate-poisson-0.5.0 (c (n "poisson") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.18") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1jzgm2k5dlamzgjy1xzkw0566bp5brd1cpzbi6l1452xvlgii19c")))

(define-public crate-poisson-0.6.0 (c (n "poisson") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.18") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1v22cxffqn5xs7hnjg7fmydvxn9dhbl81dbb1j8rq1nfas2dpa28")))

(define-public crate-poisson-0.7.0 (c (n "poisson") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "modulo") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.18") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1cchy1sazfd591wr946m7x5bsvpsnbvbldknv0xi1dwayhx4fps0")))

(define-public crate-poisson-0.7.1 (c (n "poisson") (v "0.7.1") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "modulo") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.18") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1qsjgzzrz2gp25z1bcwfgkv9wh9ly9rqhcp7shyvzyv83pwwapiz")))

(define-public crate-poisson-0.8.0 (c (n "poisson") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.1.11") (d #t) (k 0)) (d (n "modulo") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.18") (d #t) (k 0)) (d (n "num") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "sphere") (r "^0.2.0") (d #t) (k 0)))) (h "1a5hpf0rdkacdih94j4yssmavm9cpnnqp58739mcnz3z98khvaql")))

(define-public crate-poisson-0.9.0 (c (n "poisson") (v "0.9.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "modulo") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sphere") (r "^0.2") (d #t) (k 0)))) (h "0ag8sl7d7z10lhzm2m48m7cinr61662yaq2d036cihmg4dhhhy6l")))

(define-public crate-poisson-0.10.0 (c (n "poisson") (v "0.10.0") (d (list (d (n "alga") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "modulo") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sphere") (r "^0.3") (d #t) (k 0)))) (h "162j1fbnqwzq87z5l1xc1fbyr29q1l5968c7n8zasy09q2903nlx")))

(define-public crate-poisson-0.10.1 (c (n "poisson") (v "0.10.1") (d (list (d (n "alga") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "modulo") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sphere") (r "^0.3") (d #t) (k 0)))) (h "0p2vsyn0lvyi8kqpwx8jjz5k9kf0gc3jkazkhgyn2c3zfd80wkp2")))

