(define-module (crates-io po is poise_macros) #:use-module (crates-io))

(define-public crate-poise_macros-0.1.0 (c (n "poise_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11psajfp79smdyp5hwbpabq5i104srwqpvfy13a41mq3dx41cpfs")))

(define-public crate-poise_macros-0.2.0 (c (n "poise_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1kwna2jciybvwbbwl7jbbd38wqwcdlq0qhk98hh4b0y5pns8ddqm")))

(define-public crate-poise_macros-0.2.1 (c (n "poise_macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1bx1j60h4x9jp9gc65kd14qr74r9qz2zbk24ydkgdkp3qw99gncl")))

(define-public crate-poise_macros-0.3.0 (c (n "poise_macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "034ab82dlpf2fri06di8x10b1vjz096445zbajy4gv2jd8dqdzsj")))

(define-public crate-poise_macros-0.4.0 (c (n "poise_macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hj66za40903f3sgkb9xpylq86imzgfmr6kjd6mzbvkszw9i5ljk")))

(define-public crate-poise_macros-0.5.0 (c (n "poise_macros") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1y6lhrk377smd9y8s0vr75k99rgqmwpqpvr7fmb25maf9vklg4mb")))

(define-public crate-poise_macros-0.5.1 (c (n "poise_macros") (v "0.5.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1baafavw2f6vcxdd1r7170a4ls59nb6f0nskwa4y9n1d5v0h3k7f")))

(define-public crate-poise_macros-0.5.2 (c (n "poise_macros") (v "0.5.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1i37bar65w0irbrsxc0vymv542bsf0qvpmhyskwjfi8i0i71y35q")))

(define-public crate-poise_macros-0.5.3 (c (n "poise_macros") (v "0.5.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "100inbbhphgym0az2lkamxksz6h7yjpkvs93fgf2l03fxjyks4yn")))

(define-public crate-poise_macros-0.5.5 (c (n "poise_macros") (v "0.5.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ym07j082kdiib9hm66l56si0qz7jz9hj1nwkzlzlzjjw6ch09s0")))

(define-public crate-poise_macros-0.5.6 (c (n "poise_macros") (v "0.5.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "184400axpa7a5rhfbgj0g3mzaa52kpcfmy0k7m9z1ki8fzl0z5h5")))

(define-public crate-poise_macros-0.5.7 (c (n "poise_macros") (v "0.5.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1hbhq2517igfwczfm6qs8rv8jh6aqq49v0gmgsyy8jjfryl1ddag")))

(define-public crate-poise_macros-0.6.0 (c (n "poise_macros") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1xbfb0nqzqbg3bf903rlxwmd54nzr4l65m5gad200n11zrsbfwvz")))

(define-public crate-poise_macros-0.6.0-rc1 (c (n "poise_macros") (v "0.6.0-rc1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0g9gdgrq0lm9376s9k3yvckjqgabr2f81cjall4w23vbgnnvl0k2")))

(define-public crate-poise_macros-0.6.1-rc1 (c (n "poise_macros") (v "0.6.1-rc1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "0bcj28657phkys7zzpxs7xh37nprz5gqp0s7n6smlbn8h1rd43qk")))

(define-public crate-poise_macros-0.6.1 (c (n "poise_macros") (v "0.6.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("fold"))) (d #t) (k 0)))) (h "1spz3rxx6ziayvdl0m0g8vj2pwbp65kcgsixrlaq7rv1r4iw38lg")))

