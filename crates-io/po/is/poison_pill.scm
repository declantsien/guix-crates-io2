(define-module (crates-io po is poison_pill) #:use-module (crates-io))

(define-public crate-poison_pill-0.1.0 (c (n "poison_pill") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 0)))) (h "0vdjnksw0zbiwichd4xgmkzxc49nsgkqcsgspxkr39bp7y4a5gqr")))

(define-public crate-poison_pill-0.1.1 (c (n "poison_pill") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 0)))) (h "10jm1lh4mq4xbz16nvajk33r1fmfzb8vgyyzfmfmrz1nk9b9q27h")))

(define-public crate-poison_pill-0.1.2 (c (n "poison_pill") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("io"))) (d #t) (k 0)))) (h "1cxg9ndsw389lqwglalkbhvvvwbcgw475p7hl0m6v24rxxz60466")))

