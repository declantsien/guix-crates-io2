(define-module (crates-io po is poison-guard) #:use-module (crates-io))

(define-public crate-poison-guard-0.1.0 (c (n "poison-guard") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0zcpw9n4f7ycpmj6b73gdd8r5jzgm0vvb8373bhl6rs7qpwhq3ls")))

