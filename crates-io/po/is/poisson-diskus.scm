(define-module (crates-io po is poisson-diskus) #:use-module (crates-io))

(define-public crate-poisson-diskus-1.0.0-pre.1 (c (n "poisson-diskus") (v "1.0.0-pre.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "18x5nxm76vjwd5p2v7k2gnyazn9jcgszbj151a7iyh9g1mvs7bi7")))

(define-public crate-poisson-diskus-1.0.0-pre.2 (c (n "poisson-diskus") (v "1.0.0-pre.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "0g5216ahgh0aka3iyicjzk9mvb195gv6b23ihzsds4j64vjxkrzz")))

(define-public crate-poisson-diskus-1.0.0-pre.3 (c (n "poisson-diskus") (v "1.0.0-pre.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "106sfak9cqgcpkpm41x5dqii43z060fvi4xi3jxvnjwy03c4a5a5")))

(define-public crate-poisson-diskus-1.0.0-pre.4 (c (n "poisson-diskus") (v "1.0.0-pre.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "0bydy7pj9qqvw3ly175bndczxn69y357s8pac08aqdv7ghsph8wn")))

(define-public crate-poisson-diskus-1.0.0 (c (n "poisson-diskus") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "02356z8wrssldbyy644d70kmsradwsnh2lmnlxkzwkmw7h164bbs")))

