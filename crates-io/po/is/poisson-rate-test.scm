(define-module (crates-io po is poisson-rate-test) #:use-module (crates-io))

(define-public crate-poisson-rate-test-0.1.0 (c (n "poisson-rate-test") (v "0.1.0") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "02aarg661n3hlkkkrcnmw5pm4kpn3x744nfl6lym5xn6gi67z6rp")))

(define-public crate-poisson-rate-test-1.0.0 (c (n "poisson-rate-test") (v "1.0.0") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "19kyp8w9syq7h1vw02f7p54l3aiagmz7ydsn37k9vqkglxqaxk70")))

(define-public crate-poisson-rate-test-1.0.1 (c (n "poisson-rate-test") (v "1.0.1") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "01xkw7g4snvs8smjw0lwifn16n1ix39csnah5v7k0m3nh6dj6bq3")))

(define-public crate-poisson-rate-test-1.0.2 (c (n "poisson-rate-test") (v "1.0.2") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "13dq0p4cfyxslharqh7c8lli00c1n795p33bvw3ssw48plbasxgb")))

(define-public crate-poisson-rate-test-1.0.3 (c (n "poisson-rate-test") (v "1.0.3") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0km42yxsqcvjfsvpm7kadf6rhv3apsh1vf78zh61hwrv2zqks7zn")))

(define-public crate-poisson-rate-test-1.0.4 (c (n "poisson-rate-test") (v "1.0.4") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0xwhdd9msw98xn7p7ys8sjhi4fm5sgpxzbc36vvzfvwmclw02all")))

(define-public crate-poisson-rate-test-1.0.5 (c (n "poisson-rate-test") (v "1.0.5") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0wsr80c8fsf3r3mnhciznli2kphiwc8l0q08apshljn44bvjap62")))

(define-public crate-poisson-rate-test-1.1.0 (c (n "poisson-rate-test") (v "1.1.0") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "1zhkblpqyzv3ylgnkw85k0cnq4f2m68l0pzgdd2k1yl0n3f4klm2")))

(define-public crate-poisson-rate-test-1.2.0 (c (n "poisson-rate-test") (v "1.2.0") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "01gb5d0gk3bsyvqcmljkqic09njmn92551bl03fr7w6jncr7dakh")))

(define-public crate-poisson-rate-test-1.2.2 (c (n "poisson-rate-test") (v "1.2.2") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "08n9fwb11qwwmiqw50597nrgzc85500gasc75yvfdrnbvccgp6cm")))

