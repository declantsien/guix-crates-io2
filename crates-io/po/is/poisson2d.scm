(define-module (crates-io po is poisson2d) #:use-module (crates-io))

(define-public crate-poisson2d-0.1.0 (c (n "poisson2d") (v "0.1.0") (d (list (d (n "glam") (r "^0.9") (f (quote ("mint" "rand"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)) (d (n "modulo") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "sphere") (r "^0.3") (d #t) (k 0)))) (h "1aj3ncj46wv3ws0k0rdqk2pwdp29vs0aj0zb8p4awhlcd6jwni2w")))

