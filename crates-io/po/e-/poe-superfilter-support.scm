(define-module (crates-io po e- poe-superfilter-support) #:use-module (crates-io))

(define-public crate-poe-superfilter-support-0.1.0 (c (n "poe-superfilter-support") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1x2jgw0sgx7aggshd6f10ypcpb91148qiic5dwa19dx8jda8q7kh")))

(define-public crate-poe-superfilter-support-0.1.1 (c (n "poe-superfilter-support") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "16d4iqq3c30ykb1hdbprwjlpj9f7ilh1qcjxwzhd5mg672sbgddi")))

(define-public crate-poe-superfilter-support-0.2.0 (c (n "poe-superfilter-support") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "04lgd8qwbm27lr5s9c9w2pgqivq3qnyrhd6gyjpgshnq2wb86wva")))

