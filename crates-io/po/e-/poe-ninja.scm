(define-module (crates-io po e- poe-ninja) #:use-module (crates-io))

(define-public crate-poe-ninja-0.1.0 (c (n "poe-ninja") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lviqn7yqxdjfdjssp5pdg6sh6sv7f5i3vmaymili5ysw5izjyca")))

(define-public crate-poe-ninja-0.1.1 (c (n "poe-ninja") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1jlhcz7409b7y1axlibj2ggjkc7knhs4hfd1k89419nl30imdy8l")))

