(define-module (crates-io po e- poe-types) #:use-module (crates-io))

(define-public crate-poe-types-0.1.0 (c (n "poe-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "18dahb01czifp0zird0d6fjwlfvqxbxajgw79db18ny2ad66zlhs")))

(define-public crate-poe-types-0.1.1 (c (n "poe-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "06mbhwl3ch0iwhq1w5qwfyhg568vr74cg4vnpj2aa8dsyp11lr6x")))

(define-public crate-poe-types-0.1.2 (c (n "poe-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1pcyqasps9vdrycbkl1zszi05s2qqg10xvjl5xmpv7844gwk9an8")))

