(define-module (crates-io po p- pop-trait) #:use-module (crates-io))

(define-public crate-pop-trait-0.2.0 (c (n "pop-trait") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "len-trait") (r "^0.2") (d #t) (k 0)))) (h "1z0y0lkrikpqqpyfsm5landa1nm6adw0148lf9xq1yljf0gfxs76")))

(define-public crate-pop-trait-0.2.1 (c (n "pop-trait") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0") (o #t) (d #t) (k 0)) (d (n "blist") (r "^0") (o #t) (d #t) (k 0)) (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "interval-heap") (r "^0") (o #t) (d #t) (k 0)) (d (n "len-trait") (r "^0.2") (k 0)) (d (n "linked-hash-map") (r "^0") (o #t) (d #t) (k 0)))) (h "0p4ba8ccbn0yf9z9s016z6r9ys5m1fi84wmbc2jv09y57wvspjwm") (f (quote (("no-std" "len-trait/no-std") ("default" "collections" "contain-rs") ("contain-rs" "bit-vec" "blist" "interval-heap" "linked-hash-map" "len-trait/contain-rs") ("collections" "len-trait/collections"))))))

