(define-module (crates-io po p- pop-launcher) #:use-module (crates-io))

(define-public crate-pop-launcher-1.0.0 (c (n "pop-launcher") (v "1.0.0") (d (list (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "0ysrww5l7ag7bpw1sl2ygw5rgh55fwagk4ly553k5m7kb5jsjl9n")))

(define-public crate-pop-launcher-1.0.1 (c (n "pop-launcher") (v "1.0.1") (d (list (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "0gkn2gpnsfwkpjh332rqhp1q3c517k19dpkmg05zy1yxg5kr4704")))

(define-public crate-pop-launcher-1.0.3 (c (n "pop-launcher") (v "1.0.3") (d (list (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "0krgpf3l4z9hvhgnl6mj8zjl42fcnz4yj96zfxva8kn62xb0r52a")))

(define-public crate-pop-launcher-1.1.0 (c (n "pop-launcher") (v "1.1.0") (d (list (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "1rmmdl52qxzs49nfy8s81xa1jb4sl2lydhn45cg9v1zi90n2vy8s")))

