(define-module (crates-io po p- pop-prob) #:use-module (crates-io))

(define-public crate-pop-prob-0.1.0 (c (n "pop-prob") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kbqyj9f0fviqjx8ajq25zwb00ygsixxsdpacbpc3838951aaqb0")))

(define-public crate-pop-prob-0.1.1 (c (n "pop-prob") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14iagp4ldgh3mi2yn44mi4k64rwpsk55kqvg4dik40v0g1b47sbz")))

