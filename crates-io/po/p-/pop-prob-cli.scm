(define-module (crates-io po p- pop-prob-cli) #:use-module (crates-io))

(define-public crate-pop-prob-cli-0.1.0 (c (n "pop-prob-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pop-prob") (r "^0.1") (d #t) (k 0)))) (h "1y287fvh9cfdbhfqpswchqkg70ngpz59g8s1f0ihgwdxp5mdakrx")))

(define-public crate-pop-prob-cli-0.1.1 (c (n "pop-prob-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pop-prob") (r "^0.1") (d #t) (k 0)))) (h "0isr1422fw74zcxq1fbx76n50ilqp1j0drpwdnm6806kjxz4gmr6")))

