(define-module (crates-io po gg poggers-derive) #:use-module (crates-io))

(define-public crate-poggers-derive-0.1.1-dev (c (n "poggers-derive") (v "0.1.1-dev") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "07wb60bixyq5fbcsq825z49hjl7nvyjczkp56zxik8gh3cz8yi5y") (y #t)))

(define-public crate-poggers-derive-0.1.1-dev1 (c (n "poggers-derive") (v "0.1.1-dev1") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2p1k7gsx6bm9x8fng9qhdj81r2lz8aah0w48yabin8mfbl86kp")))

(define-public crate-poggers-derive-0.1.1-dev2 (c (n "poggers-derive") (v "0.1.1-dev2") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvh33hb8710iw3bzyspxq45q4rzqs5yhf4ia4r7i512pyhbbvgh")))

(define-public crate-poggers-derive-0.1.1-dev3 (c (n "poggers-derive") (v "0.1.1-dev3") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "02ylafs1clqvkyvffjl9abcbbrzsvav8dllnh50iwcrh6h2dq1cj")))

(define-public crate-poggers-derive-0.1.1-dev4 (c (n "poggers-derive") (v "0.1.1-dev4") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1xms62jir1ngzfph0rx6axf968kgc7j87nidvknaamdg6gh6vqxb")))

(define-public crate-poggers-derive-0.1.1-dev5 (c (n "poggers-derive") (v "0.1.1-dev5") (d (list (d (n "ctor") (r "^0.1.23") (o #t) (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0x11949zanrxgnzivchvj3gzrqxwvyfvraw24w132iawz160pqa6") (f (quote (("win") ("macos" "ctor") ("linux" "ctor") ("default" "win"))))))

(define-public crate-poggers-derive-0.1.2 (c (n "poggers-derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0x48hg02cp625l4gavv65jjhfca63i6cg156qx5qh9zmcaxx988l")))

(define-public crate-poggers-derive-0.1.3 (c (n "poggers-derive") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "08w3wz9lnmfsq285im9gvds3ccpxblz53p24n0knbs9d1qh31kx5")))

(define-public crate-poggers-derive-0.1.4 (c (n "poggers-derive") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "13r8cc5vpw672j2p5b3dr2qmn1byp9jd1vpszs9j19al27z68d2p")))

(define-public crate-poggers-derive-0.1.5 (c (n "poggers-derive") (v "0.1.5") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "18qn072mpqn97wzbdxi20q8nvskg1b59d17hk0c3v43233awfhrb")))

