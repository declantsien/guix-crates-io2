(define-module (crates-io po d- pod-enum-macros) #:use-module (crates-io))

(define-public crate-pod-enum-macros-0.1.0 (c (n "pod-enum-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wlmd8yrs6054aihil6k2dyvi5f50272mayafa59jjr9k8rap9x2")))

