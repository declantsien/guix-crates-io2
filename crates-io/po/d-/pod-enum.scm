(define-module (crates-io po d- pod-enum) #:use-module (crates-io))

(define-public crate-pod-enum-0.1.0 (c (n "pod-enum") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (k 0)) (d (n "pod-enum-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ldiknz6jff1ykk7cmk1fk2cih3cckk07215z8ywv1plz13032f9")))

