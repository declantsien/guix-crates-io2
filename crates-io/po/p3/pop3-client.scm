(define-module (crates-io po p3 pop3-client) #:use-module (crates-io))

(define-public crate-pop3-client-0.0.1 (c (n "pop3-client") (v "0.0.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rustls") (r "^0.15.2") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1lz7q0gwcgwcqxwnrpqwgw5a1jilg37ffnvyn9f2gbpaxqnjxk09") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("default"))))))

(define-public crate-pop3-client-0.0.2 (c (n "pop3-client") (v "0.0.2") (d (list (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "06x8g4m7s1rihqxcr71nlghvz5jhywm85bmsrq4npk5hfzsyjxqr") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("default"))))))

(define-public crate-pop3-client-0.0.3 (c (n "pop3-client") (v "0.0.3") (d (list (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1glibyypqxq014c8ihvmsf9l9x83z07d3l6x8919rdgyd7grj7na") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("default"))))))

(define-public crate-pop3-client-0.1.0 (c (n "pop3-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (o #t) (d #t) (k 0)))) (h "01hpqs9y518r7rpy02zrr53nf4cqfbidf2cz4vm8ki0q04r8g773") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("with-encoding" "encoding_rs") ("default"))))))

(define-public crate-pop3-client-0.1.1 (c (n "pop3-client") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (o #t) (d #t) (k 0)))) (h "07vxmn2y7kbxhsr20mm4jravamjicpc1z8qqwksp5s9144wbvd6x") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("with-encoding" "encoding_rs") ("default"))))))

(define-public crate-pop3-client-0.2.0 (c (n "pop3-client") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (o #t) (d #t) (k 0)))) (h "0pyyy4nq2hcw7p0z7b1y8pn4qh6s7w83yir09gvqmdazif0jxldv") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("default"))))))

(define-public crate-pop3-client-0.2.1 (c (n "pop3-client") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (o #t) (d #t) (k 0)))) (h "0kbs31b4h35n848rxv5jr9z734sxxgb2gm8qg2pz5sc29vwbnli2") (f (quote (("with-rustls" "rustls" "webpki" "webpki-roots") ("default"))))))

