(define-module (crates-io po p3 pop3-codec) #:use-module (crates-io))

(define-public crate-pop3-codec-0.2.0 (c (n "pop3-codec") (v "0.2.0") (d (list (d (n "abnf-core") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rhyd2lpyfc10npqrpjcfarifys7g694j1jbfv09sph1k2vc9dg9") (f (quote (("utils" "md5") ("serdex" "serde") ("default"))))))

(define-public crate-pop3-codec-0.3.0 (c (n "pop3-codec") (v "0.3.0") (d (list (d (n "abnf-core") (r "^0.5") (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01x8d5rf731bppbnimmkp8lxababfd1ai3amadzcjkjxm1096049") (f (quote (("utils" "md5") ("serdex" "serde") ("default"))))))

