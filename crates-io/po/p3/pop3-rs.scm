(define-module (crates-io po p3 pop3-rs) #:use-module (crates-io))

(define-public crate-pop3-rs-0.1.0 (c (n "pop3-rs") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0ivvg2rnll4rn0hlsbkbyss7cz5zl649a0ggmzw2jjqj8y5id7r2") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

