(define-module (crates-io po p3 pop3) #:use-module (crates-io))

(define-public crate-pop3-0.0.1 (c (n "pop3") (v "0.0.1") (d (list (d (n "openssl") (r "~1.0.0") (d #t) (k 0)))) (h "1z9wfmiq3kphbr9mk9va8iqnmlkpq70n9pz13w001ga9bdnzf1bs")))

(define-public crate-pop3-0.0.2 (c (n "pop3") (v "0.0.2") (d (list (d (n "openssl") (r "^0.2.15") (d #t) (k 0)))) (h "0kps6kj13rwxvblc59q40lvmxdflj48wfzl9rs67l0aa9bhv67ww")))

(define-public crate-pop3-0.0.3 (c (n "pop3") (v "0.0.3") (d (list (d (n "openssl") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.20") (d #t) (k 0)))) (h "1kkzwms4ya23vl5yz6sapfdwjc75cfanabkl4whjw3wl16qhbxcs")))

(define-public crate-pop3-0.0.4 (c (n "pop3") (v "0.0.4") (d (list (d (n "openssl") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.28") (d #t) (k 0)))) (h "1873fxw6a53wr19awl0hf7cqiclw778pia4g1fz5z5jkb4g8318g")))

(define-public crate-pop3-0.0.5 (c (n "pop3") (v "0.0.5") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "0bq2y46fwmlsiglsyljrm9c88cz5lsd46k3fjk1imbwkhslkwg14")))

(define-public crate-pop3-0.0.6 (c (n "pop3") (v "0.0.6") (d (list (d (n "openssl") (r "^0.7.13") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0x0jfxl0x8yybgazykzz2pqzfnd12axy2nlr88cvwm5kn6aw6n5b")))

(define-public crate-pop3-1.0.6 (c (n "pop3") (v "1.0.6") (d (list (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1mpmkzkxzg9fr2ahs7i6g16b4mq08r9m7rgsgw81nhr4hx1n2yil")))

