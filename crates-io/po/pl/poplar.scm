(define-module (crates-io po pl poplar) #:use-module (crates-io))

(define-public crate-poplar-0.1.0 (c (n "poplar") (v "0.1.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "compiler_builtins") (r "^0.1.35") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pci_types") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ptah") (r "^0.2.0") (o #t) (k 0)))) (h "1yyg4vsidbibndvarxxaq4k4xqi4slmml350k8sfdp54nj2h5940") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins" "alloc") ("pci" "pci_types") ("default" "can_alloc" "pci") ("can_alloc" "log" "ptah"))))))

