(define-module (crates-io po ss possiblyrandom) #:use-module (crates-io))

(define-public crate-possiblyrandom-0.1.0 (c (n "possiblyrandom") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (t "cfg(not(any(target_os = \"unknown\", target_os = \"none\")))") (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (k 0)))) (h "0zvzhfh6mmyf6dvp5f32v55k4sx41cwqhxj6a7zc7w3sl26xh582")))

(define-public crate-possiblyrandom-0.1.1 (c (n "possiblyrandom") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2") (t "cfg(not(any(target_os = \"unknown\", target_os = \"none\")))") (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (k 0)))) (h "1h4cp13s003b29h4y2z12rj812p34nqfrj6ypp5qvhk59ar0yjsj")))

(define-public crate-possiblyrandom-0.2.0 (c (n "possiblyrandom") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (t "cfg(not(any(target_os = \"unknown\", target_os = \"none\")))") (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (k 0)))) (h "1zfz2m6rdzz39izb77wl0vpdhb13z7yj6rdjv2rly43jbmhjl4hv")))

