(define-module (crates-io po ss possum) #:use-module (crates-io))

(define-public crate-possum-0.1.0 (c (n "possum") (v "0.1.0") (d (list (d (n "possum-core") (r "^0.1.0") (d #t) (k 0)) (d (n "possum-script") (r "^0.1.0") (d #t) (k 0)))) (h "07w5hg53jsfkcj746blgljbplbpz73y4miij74cc7bqpkjf6d87q")))

