(define-module (crates-io po ss possible) #:use-module (crates-io))

(define-public crate-possible-0.1.0 (c (n "possible") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "ron") (r "^0.6.4") (d #t) (k 2)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "serde_qs") (r "^0.8.4") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 2)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "01cfmxxcv2xdnfifz0q7sa0b3f0bf2n3y4whjg2nr51x0mjp1hgh")))

