(define-module (crates-io po ss possibly_uninit) #:use-module (crates-io))

(define-public crate-possibly_uninit-0.1.0-preview (c (n "possibly_uninit") (v "0.1.0-preview") (h "1wsb755xi1jpkbfykp04syl4qfhrzg0qyc35cdvr0dmnjv1y0y33") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-possibly_uninit-0.1.0 (c (n "possibly_uninit") (v "0.1.0") (h "14jhah4hiqd1in2ch0xg6xvw6r7sb5151fnkjjfmphmg0a6cvdqv") (f (quote (("std" "alloc") ("default") ("alloc"))))))

