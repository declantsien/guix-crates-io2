(define-module (crates-io po ss possum-script) #:use-module (crates-io))

(define-public crate-possum-script-0.1.0 (c (n "possum-script") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hvx20rkzvdjsyqba32a2i6aywqm5kpmnpr724ffgxf5418fj542")))

