(define-module (crates-io po t- pot-conditioner) #:use-module (crates-io))

(define-public crate-pot-conditioner-0.1.0 (c (n "pot-conditioner") (v "0.1.0") (d (list (d (n "backlash") (r "^0.1") (d #t) (k 0)) (d (n "dyn-smooth") (r "^0.2") (d #t) (k 0)))) (h "1jqzj6q2vnj94ikgf2fsa1pfd7jdl0szwlkk2dzh5ywdisxjcp62") (r "1.56")))

