(define-module (crates-io po sh posh-transient) #:use-module (crates-io))

(define-public crate-posh-transient-0.1.0 (c (n "posh-transient") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1hg48achg0lk0vnfxjpm4360gsdw2lzcryvqk4srjcg38qy7hy96") (y #t)))

(define-public crate-posh-transient-0.2.0 (c (n "posh-transient") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "15b03yq135sv5028abbznhpijsfhz6qi88804f96ziz1r3xfqsgm") (y #t)))

(define-public crate-posh-transient-0.2.1 (c (n "posh-transient") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "024l9y5gmlqvsjiqi71gjrjlb53sfx0686l8sv3dq9wn3abvv5hs")))

