(define-module (crates-io po sh posh-fzf) #:use-module (crates-io))

(define-public crate-posh-fzf-0.1.2 (c (n "posh-fzf") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)))) (h "0wjm3m96cbw9h4y24bsq1zkcj5p7lsn7h0d9f6lp44fli8kr04hy")))

(define-public crate-posh-fzf-0.1.3 (c (n "posh-fzf") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)))) (h "0yqkcxqjidg7z4bmk537dqjhvc0m5i438z3p7hxw7mnll4153vjy")))

(define-public crate-posh-fzf-0.1.4 (c (n "posh-fzf") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)))) (h "17kfad5ahql2n33iika30cw2z7rwp68asqf0xg4dwv8i5r36mkpr")))

(define-public crate-posh-fzf-0.1.5 (c (n "posh-fzf") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0k51zla7d0z2jqifvvs8760pr3ddkwywgxbbn4zlfciz2qvz4yzq")))

(define-public crate-posh-fzf-0.2.0 (c (n "posh-fzf") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0byly8h7pfprad1x68xdz62j1q8q5bsjq752sbhrg7v6f47svhdr")))

(define-public crate-posh-fzf-0.2.5 (c (n "posh-fzf") (v "0.2.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1xbc02m2a6yizviqkd0z6n3gan1bndmv9hsndx0hxhx25ydam10f")))

(define-public crate-posh-fzf-0.2.6 (c (n "posh-fzf") (v "0.2.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0pbvwm6gwbpv1ihxcvymbb7zlp14brzi6d6j6bjh8q0bzqz0xxng")))

(define-public crate-posh-fzf-0.2.7 (c (n "posh-fzf") (v "0.2.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0jc3sm0xaan6rkh9zwl710p6cc0sj020s997i82qg3hjdyqyb29y")))

(define-public crate-posh-fzf-0.2.8 (c (n "posh-fzf") (v "0.2.8") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1niym1sq032nwpha32h1w06laknnibf2wv5ixc9cx7ismd5nnh85")))

