(define-module (crates-io po dp podping-api) #:use-module (crates-io))

(define-public crate-podping-api-0.1.0 (c (n "podping-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "0vv6ssx4k2adn0xzkgwih4gc8ki3qpa5kxwjh2sxp2rvnycrbnxj")))

