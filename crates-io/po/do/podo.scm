(define-module (crates-io po do podo) #:use-module (crates-io))

(define-public crate-podo-0.1.0 (c (n "podo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "01cni7k7rf85awidasiybx918kk5zc705d9yl9r0v1f4qabymz9b")))

