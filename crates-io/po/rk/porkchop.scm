(define-module (crates-io po rk porkchop) #:use-module (crates-io))

(define-public crate-porkchop-0.1.0 (c (n "porkchop") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pelite") (r "^0.9") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w25f7h598fq16agc4cxn48zssxgc503z40m5dq1vliwp1pb13gb")))

