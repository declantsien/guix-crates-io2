(define-module (crates-io po rk porky) #:use-module (crates-io))

(define-public crate-porky-0.1.0 (c (n "porky") (v "0.1.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)))) (h "1rx387m2vj5jr142pj2wpnqhh7rmdg1pb79wdwl86k1py5ianih7")))

(define-public crate-porky-0.1.1 (c (n "porky") (v "0.1.1") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)))) (h "0jjax5ixlc6pc8yrm093j3zchfchl7q8jd3rfi8f4dv65dr540jb")))

(define-public crate-porky-0.1.2 (c (n "porky") (v "0.1.2") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)))) (h "0zpwaxvy0g16z8h8k8pvbgijgaszjrp5dml7imhkjdgzk39qxizq")))

