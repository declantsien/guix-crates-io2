(define-module (crates-io po tt potterscript-parser) #:use-module (crates-io))

(define-public crate-potterscript-parser-0.1.0 (c (n "potterscript-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vlfzzdqqvjzx5lhzssvihv59whbvaq1dyy4vbykmnjbcla5jv1x")))

(define-public crate-potterscript-parser-0.2.0 (c (n "potterscript-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "16ggbqqhrbpbv3aiy38pxlm16qc8y6cmw3fvrnkzy36z6npjwywz")))

