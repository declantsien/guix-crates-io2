(define-module (crates-io po tt potting-helper-database) #:use-module (crates-io))

(define-public crate-potting-helper-database-0.1.0 (c (n "potting-helper-database") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)))) (h "0s8qm7fn5hjc6dklk6843vrakyzcby1jbhvj59nc0llz0vyi8k5p") (y #t)))

(define-public crate-potting-helper-database-0.1.1 (c (n "potting-helper-database") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)))) (h "1h5kdfj12ddwc05di84bz5623kx081vanky3fyfdiirjwvcjv32h") (y #t)))

