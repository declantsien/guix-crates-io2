(define-module (crates-io po tt potterscript-runtime) #:use-module (crates-io))

(define-public crate-potterscript-runtime-0.1.0 (c (n "potterscript-runtime") (v "0.1.0") (d (list (d (n "colored") (r "^1.1.0") (d #t) (k 0)) (d (n "potterscript-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0yv44m0l11qihx0kdws16p3hc2pjffc1j9rpcv8370c4dn21fr3z")))

(define-public crate-potterscript-runtime-0.2.0 (c (n "potterscript-runtime") (v "0.2.0") (d (list (d (n "colored") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (o #t) (d #t) (k 0)) (d (n "potterscript-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (o #t) (d #t) (k 0)))) (h "14pfh954cp139qjkv2mvmsmiw7m4kp7yl00vllv8zliz547dbv5c") (f (quote (("std" "colored" "rand") ("js" "js-sys" "wasm-bindgen" "web-sys") ("default" "std"))))))

