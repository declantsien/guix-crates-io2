(define-module (crates-io po pt poptea) #:use-module (crates-io))

(define-public crate-poptea-0.1.0 (c (n "poptea") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "rustls") (r "^0.20.1") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "x509-parser") (r "^0.12.0") (d #t) (k 0)))) (h "02lmp4k0qlis6yx6mcilpc77k80agdiq367m2y0wspzhd28wy58m")))

