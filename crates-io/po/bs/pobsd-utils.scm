(define-module (crates-io po bs pobsd-utils) #:use-module (crates-io))

(define-public crate-pobsd-utils-0.1.0 (c (n "pobsd-utils") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "pledge") (r "^0.4") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "unveil") (r "^0.3") (d #t) (k 0)))) (h "0ri4al9s0qf47az0msbsm3zlaks67rkja3i91i4zx1s6i81b8m5m")))

