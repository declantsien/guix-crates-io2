(define-module (crates-io po bs pobsd-parser) #:use-module (crates-io))

(define-public crate-pobsd-parser-0.1.0 (c (n "pobsd-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1j6rirzkx9lr1zyx0vkp3447q0hwjb5h7yf4y231hgaqz457bbx7")))

(define-public crate-pobsd-parser-0.1.1 (c (n "pobsd-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15s2aacvrdzg6nsz5xn9v5752a8i31fhb3mpgyq9fbvs082zs5xx")))

(define-public crate-pobsd-parser-0.2.0 (c (n "pobsd-parser") (v "0.2.0") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kpcchqzdvj10gns64x5dgj865xsan1dc9x07ady5ffzdybqsqqd")))

(define-public crate-pobsd-parser-0.3.0 (c (n "pobsd-parser") (v "0.3.0") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bhns1kslal27sldp87bqnhf098xccv02y8ywv5lys4vpz2i6zhw")))

(define-public crate-pobsd-parser-0.3.1 (c (n "pobsd-parser") (v "0.3.1") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nvp7y816wxhiiryml6jab3bvgvp1gysdfjxs259m6la81adzs82")))

(define-public crate-pobsd-parser-0.4.0 (c (n "pobsd-parser") (v "0.4.0") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17g3ax6ycsf824iwglf1hdrkg53476g1qdb1f7qngy7fhmjl7pr9")))

(define-public crate-pobsd-parser-0.4.1 (c (n "pobsd-parser") (v "0.4.1") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jx49dw6iq7fh6jnf59iiqbh6k76l0n5sdnvvbh4vcawfy0pdsap")))

