(define-module (crates-io po bs pobsd-db) #:use-module (crates-io))

(define-public crate-pobsd-db-0.1.0 (c (n "pobsd-db") (v "0.1.0") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.3.0") (d #t) (k 0)))) (h "0chyns7f1qk21l9m5mqggrpzg0wngcgciihz5nkngfnm8082pf69")))

(define-public crate-pobsd-db-0.1.1 (c (n "pobsd-db") (v "0.1.1") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.3.0") (d #t) (k 0)))) (h "153pj45k4y3lif7075vv0pg48mkwgp7cxd20f6kkgnnka7ynl6fn")))

(define-public crate-pobsd-db-0.1.2 (c (n "pobsd-db") (v "0.1.2") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.3.0") (d #t) (k 0)))) (h "014ip5flkx01xymc43y01ikyaqsamlzdamr9zmgy4cvw4d3xdrvc")))

(define-public crate-pobsd-db-0.1.3 (c (n "pobsd-db") (v "0.1.3") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.3.0") (d #t) (k 0)))) (h "0fba6w5r25l9shy9ah4lrvnjwzw4b4qbcg4nk3bb9bh0r48qahvg")))

(define-public crate-pobsd-db-0.1.4 (c (n "pobsd-db") (v "0.1.4") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "pobsd-parser") (r "^0.4.1") (d #t) (k 0)))) (h "13yr3arj5vgq9w3a3b7r07j9bkpji5wlc8zkv0q5b0afp6mm3srs")))

