(define-module (crates-io po li polish) #:use-module (crates-io))

(define-public crate-polish-0.1.0 (c (n "polish") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "01y54gv4zwpfrzsh0famvx6i06lmy6m8469lhnk1hxnc3salwwbp")))

(define-public crate-polish-0.1.1 (c (n "polish") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "06xksd4i3mgzxx375p2qsi0g3lrasxfcf237avc0jl7d4pdqq71p")))

(define-public crate-polish-0.1.2 (c (n "polish") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1csnxpcyk6zsrp6768rdlzzn27dx3i1gs1jb20pck8dpayk77y0s")))

(define-public crate-polish-0.1.3 (c (n "polish") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "04kxz2q7hyxzx9afrlnawkzi50pbjcjjwa97ddgz1f55ar3vssnh")))

(define-public crate-polish-0.2.0 (c (n "polish") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0in604bmafdrsqfsp2ykwsm6ahv5xy1lrwgqjwxpxb3r4lvmjpiy")))

(define-public crate-polish-0.3.1 (c (n "polish") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0x4gbksbajcwhz0b7xmjqsrnji5jvkfiskibsqkg4qszi26nyrww")))

(define-public crate-polish-0.3.2 (c (n "polish") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1dn36708q21g49kfn26rb6v63vbzj9c9a351l7g5rmvhlhi9ybd8")))

(define-public crate-polish-0.3.3 (c (n "polish") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "124c3nlvc4qyil9n6l9g09qxw6ajhvaxikkpqqap255ry7hqnx79")))

(define-public crate-polish-0.3.4 (c (n "polish") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "00d7nvixp4mfwdid9yv1i4fqm33a37si5wbrpk3nr2i0l7g14mfj")))

(define-public crate-polish-0.4.0 (c (n "polish") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0a7xn5pyx33qpndqwc7f527bzfi2jj3fky9az6i7igfp6jxp9w47")))

(define-public crate-polish-0.5.0 (c (n "polish") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1m3h0blgf17gxpd3lx8caddjzd7vjx4gxhz38sd1ccf31qg6qhlm")))

(define-public crate-polish-0.5.1 (c (n "polish") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0flqg9smghk02v0g9fdgrvz44d93m7c11ysrng97rmzgqfmjj9qk")))

(define-public crate-polish-0.6.1 (c (n "polish") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "03yhpc5qvgc117679s74fnaj3w6217klxmg7mdlzfb1s7109x2v1")))

(define-public crate-polish-0.6.2 (c (n "polish") (v "0.6.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0iql0v0vnz28j8gi5sq8lxkjw8pjlfvcv6j4kjdbsb5fr5dkaf1y")))

(define-public crate-polish-0.6.3 (c (n "polish") (v "0.6.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1lxbfwwm47za1q3awf402v6pb73xjx7mj5abdxr7vz4b4s4vyl2z")))

(define-public crate-polish-0.6.4 (c (n "polish") (v "0.6.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0i2ns4d9irw4wiy95zzdln1y56h3gap2bafa1lkj9xarqkcjkmxc")))

(define-public crate-polish-0.6.5 (c (n "polish") (v "0.6.5") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "06pkjzxvq1r2lkzf4wrj00qbfg6g8s2kxm01a37q4j61g55q1n2k")))

(define-public crate-polish-0.6.6 (c (n "polish") (v "0.6.6") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1f7snrhi4416lwqmdkyy6n99dnaxvd3zq3wl8g4ldyc96bpb6x9h")))

(define-public crate-polish-0.6.7 (c (n "polish") (v "0.6.7") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0khb471yz1qmka86w8c9xhyazpz1awlb6glxsdbslwz0xxyqk0zv")))

(define-public crate-polish-0.6.8 (c (n "polish") (v "0.6.8") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1iz1byvkbhvx7kzvi3cy7643837ki9wpajw1pvlymjal5lfv7npw")))

(define-public crate-polish-0.6.9 (c (n "polish") (v "0.6.9") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0lwbfrlrizf0qk7y47g4iwbqgalq78hb83m6nbndjdwxbk7h1iqy")))

(define-public crate-polish-0.7.0 (c (n "polish") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "04xrcc0xdn1xahbaiwggqvq013bnqgbg67y9h3fy992qnplwc4mx")))

(define-public crate-polish-0.7.1 (c (n "polish") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0f1nyg9kinf96819f4h085m5cvq4c6jz7sc6kdxsa1ilq336r3y0")))

(define-public crate-polish-0.7.2 (c (n "polish") (v "0.7.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1k67kr2h511s4g6jzcwhlskm9d9xr0ycpgdfgbzw7qy74f89bv5y")))

(define-public crate-polish-0.7.3 (c (n "polish") (v "0.7.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0g8ygmam07iyqanjc8qhiyk7yrxyr7gp6q2lkpzpsq23qq8r81rg")))

(define-public crate-polish-0.8.0 (c (n "polish") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "19dah3cvxab0k4y4a8ifc8lx0rbhrh89hx9hl5a7jwb6fg56109j")))

(define-public crate-polish-0.9.3 (c (n "polish") (v "0.9.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "133151b80kpfk5bg188syamw3pcvz8zigh8sds1fn8llimdbkga7")))

(define-public crate-polish-0.9.4 (c (n "polish") (v "0.9.4") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1650qk83mvmp0rl9di35jqgl4814fcz56z8qxs4584gmcwi4xwyv")))

(define-public crate-polish-0.9.5 (c (n "polish") (v "0.9.5") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0g2ii2225qzyw0mfw7yqawh62j5730y0jj8x2xqasq91hzcb7rv7")))

(define-public crate-polish-0.9.6 (c (n "polish") (v "0.9.6") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1xjcm8vx8g5sh302088hcmvx8rvadxqzcwsww0dx5yi8qjn76sgf")))

(define-public crate-polish-0.9.8 (c (n "polish") (v "0.9.8") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "12xg3cd43f37ikf96k4nbnn7im4p04qgfxvg8nn3wkl4lm6dk5is")))

(define-public crate-polish-0.9.9 (c (n "polish") (v "0.9.9") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0s9n01k8kq59dbx871mnlmi9fnlhi79bxaich3qfyl239gkzal8c")))

