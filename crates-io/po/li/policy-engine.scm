(define-module (crates-io po li policy-engine) #:use-module (crates-io))

(define-public crate-policy-engine-0.1.0 (c (n "policy-engine") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.24") (d #t) (k 0)))) (h "16zh2gxdnlcxcbjw8lvw7dz0jqvnf38j7bkxff4vfsffiw9hlcyf") (y #t)))

