(define-module (crates-io po li polished-css-macros) #:use-module (crates-io))

(define-public crate-polished-css-macros-0.1.0 (c (n "polished-css-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "18zl8ks6f9836mnzq7brh2a4rgv1w6qfwv139q5jg019y3k1pg5d")))

