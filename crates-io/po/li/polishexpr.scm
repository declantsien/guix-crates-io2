(define-module (crates-io po li polishexpr) #:use-module (crates-io))

(define-public crate-polishexpr-0.1.0 (c (n "polishexpr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)))) (h "04qn6025cvd5v1s5qi7acprbiy3f1lsb4bgdqb661jahdxvqd3ky") (y #t)))

(define-public crate-polishexpr-0.1.1 (c (n "polishexpr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)))) (h "0k3gsq6yi08vz5q5pjyc09kfw670c64f7q5m714gn3w8fj513qqb") (y #t)))

(define-public crate-polishexpr-0.1.2 (c (n "polishexpr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)))) (h "145fb3rbnyncx9ci6r55nzcidzhpcf1c81zds5v07kfwzld3kaiw") (y #t)))

(define-public crate-polishexpr-0.1.4 (c (n "polishexpr") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)))) (h "14mj85ljnmc5rci0q0j8bn7m8psq2xj4mdic087rav9pdf8fnsxk") (y #t)))

