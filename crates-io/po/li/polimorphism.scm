(define-module (crates-io po li polimorphism) #:use-module (crates-io))

(define-public crate-polimorphism-0.7.0 (c (n "polimorphism") (v "0.7.0") (h "1ljavwbj4jjqbv8sapgnf5n2rr5h0ixxk2nnps9g8sn7ziq170ig") (y #t)))

(define-public crate-polimorphism-0.7.1 (c (n "polimorphism") (v "0.7.1") (h "1kcmm8bcg4li92425q05gwjrcgz8ks2jimpxbkwq0gjp31lh78dx")))

(define-public crate-polimorphism-0.7.2 (c (n "polimorphism") (v "0.7.2") (h "0lz4p325znf264wmnb5fz7x3svf9dp4mvx8l5wh9s2mm2jncs6r4")))

(define-public crate-polimorphism-0.7.3 (c (n "polimorphism") (v "0.7.3") (h "0g1a3l4z3jq0qidp35fa9cwshvsvlmgajciy494my3ma4xkpfsx7")))

