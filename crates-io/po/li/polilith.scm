(define-module (crates-io po li polilith) #:use-module (crates-io))

(define-public crate-polilith-0.1.0 (c (n "polilith") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1vgayvvs32yhjg6x6nvry8wijakwjfv7bhpmwz0xgxb184817jhf")))

(define-public crate-polilith-0.1.2 (c (n "polilith") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "17aykils6cb6z3zd8j0iljks1ax6ay4sacwqhhfssn1pb7ny0jq6")))

(define-public crate-polilith-0.1.3 (c (n "polilith") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1jyz3k48fx8rqc5lp3flc6zm6jcyjx228v994ba8nv2wpc66p49d")))

