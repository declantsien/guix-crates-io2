(define-module (crates-io po li policyd-rate-limit) #:use-module (crates-io))

(define-public crate-policyd-rate-limit-0.0.1 (c (n "policyd-rate-limit") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0fmp3ap0yx0jviaq3g6sfrv72wk0ysia48jj4773djkxcdxwk9mp")))

(define-public crate-policyd-rate-limit-0.1.0 (c (n "policyd-rate-limit") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dsn") (r "^1") (d #t) (k 0)) (d (n "mysql") (r "^17.0.0") (d #t) (k 0)))) (h "1sm1x6q3lyis2kjnawry1zcvnww99zv7dz79vj0lcjp5ifdzhrgj")))

