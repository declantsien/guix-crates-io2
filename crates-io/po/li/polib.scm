(define-module (crates-io po li polib) #:use-module (crates-io))

(define-public crate-polib-0.0.1 (c (n "polib") (v "0.0.1") (h "1yh4yxgnw4hmyhf255vw15q41smxv5idv9r1qz92v6w18p0ryzvs") (y #t)))

(define-public crate-polib-0.1.0 (c (n "polib") (v "0.1.0") (h "0i2n9b3hs4zmgj7mvjq4y197xwz8zfa3ax3s94vl3vsfc3qrl00p")))

(define-public crate-polib-0.2.0 (c (n "polib") (v "0.2.0") (d (list (d (n "linereader") (r "^0.4.0") (d #t) (k 0)))) (h "06xvydhifynkfq6sr53dqq9643myh765c6xskhj8dgprbhaknfbb")))

