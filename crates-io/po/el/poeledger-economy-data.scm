(define-module (crates-io po el poeledger-economy-data) #:use-module (crates-io))

(define-public crate-poeledger-economy-data-0.1.0 (c (n "poeledger-economy-data") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0nz0y88ql95zypnyw8cxbhy29q6wvny0hrijq6j21b7l3p2m7sfd")))

(define-public crate-poeledger-economy-data-0.1.1 (c (n "poeledger-economy-data") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "09dy0n0i96dqd91xrfan30b2qmqyiddhqxkgwpgkzcjbbvmq93pa")))

(define-public crate-poeledger-economy-data-0.2.0 (c (n "poeledger-economy-data") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "03kr5l6v9pg8zpgmih7kr7hi253nz84qfp9z95wxnxyvbhd3f47c")))

(define-public crate-poeledger-economy-data-0.3.0 (c (n "poeledger-economy-data") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typeshare") (r "^1.0.1") (d #t) (k 0)))) (h "0zpa6rscg4xx886jbalscmclpvg7s7nllxgzh7z3adfsfxczi6m4")))

