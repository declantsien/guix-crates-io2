(define-module (crates-io po l_ pol_export) #:use-module (crates-io))

(define-public crate-pol_export-0.1.0 (c (n "pol_export") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w3sf3ry6dyszimr334z4dzbyb43d5i6dkmz6jchg3g2lcd0m57d")))

(define-public crate-pol_export-0.1.1 (c (n "pol_export") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lqkcxqp67qf4jk0lninpjjpfmln4x8l0rhwb3vf2b90ccdbmnwm")))

(define-public crate-pol_export-0.2.0 (c (n "pol_export") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "windows_types_registry") (r "^0.1") (d #t) (k 0)))) (h "1gjvbz5x426cr8jgvd4vsff306571290jk3k9bp1kjiq3yl5znrd")))

(define-public crate-pol_export-0.2.1 (c (n "pol_export") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "windows_types_registry") (r "^0.1") (d #t) (k 0)))) (h "0j94mmm84516krqyary293xhvmvfqfw83dc0299a4bnlsw5s94v3")))

