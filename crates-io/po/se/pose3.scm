(define-module (crates-io po se pose3) #:use-module (crates-io))

(define-public crate-pose3-0.1.0 (c (n "pose3") (v "0.1.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jhwbdn5pc03gjx4i2d5zxhq4drwji0cp4d1xb3rd7r9s4ywx4kw")))

(define-public crate-pose3-0.2.0 (c (n "pose3") (v "0.2.0") (d (list (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0namfhgbb4gbdxi27j2hihmnbrg2rihacqp3521mis5bk08vzhq8")))

