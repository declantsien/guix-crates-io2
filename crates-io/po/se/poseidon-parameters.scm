(define-module (crates-io po se poseidon-parameters) #:use-module (crates-io))

(define-public crate-poseidon-parameters-0.2.0 (c (n "poseidon-parameters") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "ark-ed-on-bls12-377") (r "^0.3") (d #t) (k 2)) (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0xsbqinl223cjvlil79i7hznks7wr34nqqx9y3i3r01pb8rwn0r2") (f (quote (("std" "anyhow/std" "ark-ff/std" "num-integer/std"))))))

(define-public crate-poseidon-parameters-0.3.0 (c (n "poseidon-parameters") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "ark-ed-on-bls12-377") (r "^0.4") (d #t) (k 2)) (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0d47rqnmjhv7whw0mxqsi4nrk74h530z2hjiknw6fcik51rm38vh") (f (quote (("std" "anyhow/std" "ark-ff/std" "num-integer/std"))))))

(define-public crate-poseidon-parameters-0.4.0 (c (n "poseidon-parameters") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "ark-ed-on-bls12-377") (r "^0.4") (d #t) (k 2)) (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "048pnjz0xiynfw8si1wkh5j0xzf4hzl43w4jdh216p25pzw6y8sq") (f (quote (("std" "anyhow/std" "ark-ff/std" "num-integer/std"))))))

(define-public crate-poseidon-parameters-1.0.0 (c (n "poseidon-parameters") (v "1.0.0") (d (list (d (n "decaf377") (r "^0.9") (k 0)))) (h "1pmm9i3n8s31pj36cfg8jk9kas2p4gd9bq8b8ik6vsf7w6bmhcj5") (f (quote (("std" "decaf377/std") ("default" "std") ("alloc" "decaf377/alloc"))))))

