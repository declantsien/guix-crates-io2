(define-module (crates-io po ri poriborton) #:use-module (crates-io))

(define-public crate-poriborton-0.1.0 (c (n "poriborton") (v "0.1.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1lnwsd1z3ix4fm0lm1zflkb0fr2jy7z5xzmgz17s8flc96r7n8xj")))

(define-public crate-poriborton-0.2.0 (c (n "poriborton") (v "0.2.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "0ibxp9gibcsf43lai03k3jrngaa2hb1riv9skjxvzghrjf341jr5")))

(define-public crate-poriborton-0.2.1 (c (n "poriborton") (v "0.2.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1q80rbk4fj622v1r432p4skl9vphps4a11ziyjgn9limwz00h6g6")))

(define-public crate-poriborton-0.2.2 (c (n "poriborton") (v "0.2.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)))) (h "1pdhanm4v31q3nydxc01xcjp5r5ih8sr9r2rrnfg6mp897pwk0f0")))

(define-public crate-poriborton-0.2.3 (c (n "poriborton") (v "0.2.3") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)))) (h "053g6y81mqnn4af9gzxp3w2mwmi75amj3cf01q023wrjmdxafjln") (r "1.63.0")))

