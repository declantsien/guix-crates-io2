(define-module (crates-io po ri porigon) #:use-module (crates-io))

(define-public crate-porigon-0.1.0 (c (n "porigon") (v "0.1.0") (d (list (d (n "fst") (r "^0.4.0") (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "levenshtein_automata") (r "^0.1.1") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06q40fnnsc6lxw24184p1d9sjpw9mi7q4vqfkriqicdyi7jgw2n6")))

(define-public crate-porigon-0.2.0 (c (n "porigon") (v "0.2.0") (d (list (d (n "fst") (r "^0.4.5") (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "levenshtein_automata") (r "^0.2.0") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.2") (d #t) (k 0)) (d (n "rkyv") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v6ks86y776ciqjycp8flsb8bppq1vxkc79a035550njhhaq3wjy") (f (quote (("serde_support" "serde") ("rkyv_support" "rkyv"))))))

(define-public crate-porigon-0.3.0 (c (n "porigon") (v "0.3.0") (d (list (d (n "fst") (r "^0.4.7") (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "levenshtein_automata") (r "^0.2.1") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.29") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "015lgf6sxjglcx7bgn37df7i8d7x8rbkmzbykidd8a4g57hkzxqp") (f (quote (("serde_support" "serde") ("rkyv_support" "rkyv"))))))

(define-public crate-porigon-0.4.0 (c (n "porigon") (v "0.4.0") (d (list (d (n "fst") (r "^0.4") (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "levenshtein_automata") (r "^0.2") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j784hj2hkxfqlci0z64s9c5yfns865cv8hcx05c005nc95kjky2") (f (quote (("serde_support" "serde") ("rkyv_support" "rkyv"))))))

