(define-module (crates-io po ru porus) #:use-module (crates-io))

(define-public crate-porus-0.1.0 (c (n "porus") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mml8pvgjy89zfwszg4wylw0pry9rd9wgi841scgwhns4d33a2z0")))

(define-public crate-porus-0.1.1 (c (n "porus") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05ww61l6rclazz44lbs5gn52vgmxf20dfgs3ilsi3w8sf21jpax4")))

(define-public crate-porus-0.1.2 (c (n "porus") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "008zj9qcmmc2lcxswpjkxxrhvjlliwdrq266hca1jafpzi9v1bhq")))

(define-public crate-porus-0.1.3 (c (n "porus") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cnix3pp9dcidxab7ymbcp1d4j0x5x8sl8w5abhikd2src5f9frg")))

(define-public crate-porus-0.1.4 (c (n "porus") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "018pa61mxi37pbz6kj1p1fi34kfwsx91rc0inpq0sd0pngcp7jz0")))

(define-public crate-porus-0.1.5 (c (n "porus") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12hrgcq0axs7v714ja6b28zs01l2d1wzgxwwa71vvd4hsxvvn89y")))

(define-public crate-porus-0.1.6 (c (n "porus") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vsdzp704rqrcb4ffv0mvj1pb2qjm91yrn1c06dihs4yanmxf4ff")))

(define-public crate-porus-0.1.7 (c (n "porus") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qrcmh5ghypq3yw66m3gq4df32r0ypx2flc54irdvrzvbqyff849")))

(define-public crate-porus-0.1.8 (c (n "porus") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08q6r60spnvkabym83q8i2wixj9k9zl0i21mf4xaq73l4f0bg46j")))

(define-public crate-porus-0.1.9 (c (n "porus") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qn7m3fsi8pd0ffi641ymyjh9kpf0f78d12282fgi58bp2007rp3")))

(define-public crate-porus-0.1.11 (c (n "porus") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkzb6df4zjqav6956gyn5f6db7x2svncqxkbrsm30gmg2l0nv7b")))

(define-public crate-porus-0.1.12 (c (n "porus") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fpsh4cqgvh4v6qph09wgab5ld4sakq5p0xs6i8hyxp0hp7pm8m9")))

(define-public crate-porus-0.1.13 (c (n "porus") (v "0.1.13") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hywryp4hf17k3a0hjqgllxqrf40686qrcf46h74pyli19169vkk")))

(define-public crate-porus-0.1.14 (c (n "porus") (v "0.1.14") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzkldpaalkgb2mqpm7i2r9ig62506xv96my6ihpfzx28nbs6g06")))

(define-public crate-porus-0.1.15 (c (n "porus") (v "0.1.15") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1wqc0qa1nk9zidpx9dzdm7gky0v1f9krv2jz2wgna4spy94jswip")))

(define-public crate-porus-0.1.16 (c (n "porus") (v "0.1.16") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "162mhjsk5q8l71khanvh0ysfiwi9qajh01cajgx7kgdjnw4pbp7d")))

(define-public crate-porus-0.1.17 (c (n "porus") (v "0.1.17") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0qfv28iw4w4nrbkv88d5ji2cm7pylwpj7hbngdk0jpwywixrxi4y")))

(define-public crate-porus-0.1.18 (c (n "porus") (v "0.1.18") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "09gfmdqlhd943iicw0j8y1bhzsxb5c8glzfjhh54bbj0mg9lngds")))

(define-public crate-porus-0.1.19 (c (n "porus") (v "0.1.19") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1pifdjx0ypyxbjwfc1sxapqb5ag6mc3a729628f8gf50hm8v3svb")))

(define-public crate-porus-0.1.20 (c (n "porus") (v "0.1.20") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9m3badlpzb9fdzp2xjdpxak0652pm4zgvppwvr5gfqmxa023vp")))

(define-public crate-porus-0.1.21 (c (n "porus") (v "0.1.21") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)))) (h "17qlcz5qxakgi3226n6d9vbxjj6djfz76s8ibc6xn6iak9n2grjw")))

