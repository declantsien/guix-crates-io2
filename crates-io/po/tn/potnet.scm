(define-module (crates-io po tn potnet) #:use-module (crates-io))

(define-public crate-potnet-0.4.4 (c (n "potnet") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pot-rs") (r "=0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "structopt-flags") (r "^0.3") (f (quote ("simplelog"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "145k5zyscl6dbphc8c7sjkdb49cp4ar472chikf4nsqbgr9v1mzk")))

