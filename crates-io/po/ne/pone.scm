(define-module (crates-io po ne pone) #:use-module (crates-io))

(define-public crate-pone-0.1.0 (c (n "pone") (v "0.1.0") (h "0qkizx9vjilladjp33sfklgy15zc0f0dxsgj2n3cahxxcfbv7g2v")))

(define-public crate-pone-0.1.1 (c (n "pone") (v "0.1.1") (d (list (d (n "pone") (r "^0.1.0") (d #t) (k 0)))) (h "0qc6nsf78ff10xcxlc1h02g204cmazb0mi06scalwdi72xywc3xn")))

(define-public crate-pone-0.1.2 (c (n "pone") (v "0.1.2") (h "0l9sm90dlh730rbbrvms3drnbay5xn35wn329b4gqgbnnr7f9kqb")))

(define-public crate-pone-0.1.3 (c (n "pone") (v "0.1.3") (h "1rpbyk4m6y9v0s7n8l0gsp5l5ic7x1k7bq4xqwzjamigqh3bnla8")))

(define-public crate-pone-0.1.4 (c (n "pone") (v "0.1.4") (h "1gd2gvazncqg8zd74r2yp0494fyc4avnbznbpkvnhjq3zqs4l46k")))

(define-public crate-pone-0.1.5 (c (n "pone") (v "0.1.5") (h "19qnn43njg8yq5knpkcyhalx1sxdnp4xd0gzqgk8dimvshkdbb01")))

