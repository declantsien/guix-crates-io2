(define-module (crates-io po rt port-alloc) #:use-module (crates-io))

(define-public crate-port-alloc-0.1.0 (c (n "port-alloc") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.2.1") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.1.0") (f (quote ("support-v0_3"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("rt"))) (d #t) (k 0)))) (h "1bpqjri3v689ql140cs1wza4w00f2yfznpyd4w532ds3fj310jij")))

