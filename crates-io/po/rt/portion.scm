(define-module (crates-io po rt portion) #:use-module (crates-io))

(define-public crate-portion-0.1.0 (c (n "portion") (v "0.1.0") (h "0qydq9b5j7p7ggn551czd8if197pdbp6ydd9sr9jh50gy8p47hr8") (y #t)))

(define-public crate-portion-0.2.0 (c (n "portion") (v "0.2.0") (h "1r3jfjs9s9fi4wplb57k10qg38w0sa0fbcfihqdlqlm48smm036p") (y #t)))

(define-public crate-portion-0.2.1 (c (n "portion") (v "0.2.1") (h "1nf0d4glqiydxhdnammfigrql0hkqk2iyjjx7aqg7c2h0lv4q8kg") (y #t)))

(define-public crate-portion-0.3.0 (c (n "portion") (v "0.3.0") (h "0xkizqrnmhafc2x3sjyv98hi7fxngmd9ya04q94rnczxh8i58wx3") (y #t)))

