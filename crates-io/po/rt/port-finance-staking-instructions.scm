(define-module (crates-io po rt port-finance-staking-instructions) #:use-module (crates-io))

(define-public crate-port-finance-staking-instructions-0.1.0 (c (n "port-finance-staking-instructions") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "0h5cdm53sn0y8vzdzxb8y6kxznfr76x3w5z2gsf339c1ilhynybg")))

