(define-module (crates-io po rt portal-screencast) #:use-module (crates-io))

(define-public crate-portal-screencast-0.1.0 (c (n "portal-screencast") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11il2i4ylvj4cbf5csmfm1l974vhn8qay7wl36xs7iik2363fcmm")))

