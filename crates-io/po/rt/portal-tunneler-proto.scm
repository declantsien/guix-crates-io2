(define-module (crates-io po rt portal-tunneler-proto) #:use-module (crates-io))

(define-public crate-portal-tunneler-proto-0.1.0-beta.0 (c (n "portal-tunneler-proto") (v "0.1.0-beta.0") (d (list (d (n "quinn") (r "^0.10.2") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gyis7fwfrhc59p1skxc9k04llpm1s9gzmca20zy4wv64ls1ns9w") (r "1.77.1")))

