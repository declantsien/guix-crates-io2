(define-module (crates-io po rt portman_client) #:use-module (crates-io))

(define-public crate-portman_client-0.1.0 (c (n "portman_client") (v "0.1.0") (d (list (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0vhbykn0rwwg84c4m7yf8mfpdq10jbqh2wh1j9hcmjfzgls145z5")))

(define-public crate-portman_client-0.2.0 (c (n "portman_client") (v "0.2.0") (d (list (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0y5blx5jrz2kimagmz8149pmig72illkid40f42m707p8aay1yfa") (f (quote (("test_with_portman"))))))

