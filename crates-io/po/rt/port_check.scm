(define-module (crates-io po rt port_check) #:use-module (crates-io))

(define-public crate-port_check-0.1.0 (c (n "port_check") (v "0.1.0") (h "0ylphgbb9c8gln475b55462vwv1mcgxd17n620ym9apqf8g5rcq7")))

(define-public crate-port_check-0.1.1 (c (n "port_check") (v "0.1.1") (h "1slrwry8h7bhbafv3afmyj0imvv16kzylby2mnhsyvwlx0b6kk2c")))

(define-public crate-port_check-0.1.2 (c (n "port_check") (v "0.1.2") (h "0q4r7ayc9v4482b56pzx171ap3kqdkbj3fwn8a69apry68qnisjf")))

(define-public crate-port_check-0.1.3 (c (n "port_check") (v "0.1.3") (h "1nv3085ybdmxf4qdf6z47m89cjkvpg948m8jbs6hxf28kbdcvcm7")))

(define-public crate-port_check-0.1.4 (c (n "port_check") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dh9n5pkc7cx6kn9w9gjhkjzbyw93gmsf6c5xzdvyaaphppkm9s4")))

(define-public crate-port_check-0.1.5 (c (n "port_check") (v "0.1.5") (h "1v85f4n6h7kvnaqxvijg695l7jqr9lv1h1lzkdbvxm70r4998lgn")))

(define-public crate-port_check-0.2.0 (c (n "port_check") (v "0.2.0") (d (list (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0glln10i4ac1fflnn59v1hp4aghiwzfwk2s9rlx141q1gapv6mp9")))

(define-public crate-port_check-0.2.1 (c (n "port_check") (v "0.2.1") (d (list (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "1vgy7ar1dvzkx4phi04fs73si9hvr11nr7g6sikv7kb3p2gn0411")))

