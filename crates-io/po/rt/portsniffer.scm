(define-module (crates-io po rt portsniffer) #:use-module (crates-io))

(define-public crate-portsniffer-1.0.1 (c (n "portsniffer") (v "1.0.1") (h "08l4fwb8a1byjn73bvgc7jdqdk6g4z4kiqsnkb8adcvpq1ldsqq9")))

(define-public crate-portsniffer-1.0.2 (c (n "portsniffer") (v "1.0.2") (h "0j5px8cvj62mkkj233i9br9jvmcivwxw789yqbx3dxwkkp47dpa0")))

