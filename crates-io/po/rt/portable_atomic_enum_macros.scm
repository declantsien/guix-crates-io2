(define-module (crates-io po rt portable_atomic_enum_macros) #:use-module (crates-io))

(define-public crate-portable_atomic_enum_macros-0.1.0 (c (n "portable_atomic_enum_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0326vdsl813xmc2icfhwby7gr8ghvr6vgx1zn5r83416p48vrbig")))

(define-public crate-portable_atomic_enum_macros-0.1.1 (c (n "portable_atomic_enum_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "096a5hglxwylq832h0hkfa0z5dv81ak1akzr417vjavsdgspphhn")))

(define-public crate-portable_atomic_enum_macros-0.1.2 (c (n "portable_atomic_enum_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwndhm51177q9l7fxzm52f89awbkisbh92q55cjri8g8mh7w6hw")))

(define-public crate-portable_atomic_enum_macros-0.2.0 (c (n "portable_atomic_enum_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "04rhw9zfr28y63d3f9l9ziqjjbplgyfhpz15lbs09ij26cgnvxdr")))

(define-public crate-portable_atomic_enum_macros-0.2.1 (c (n "portable_atomic_enum_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxh0bz1qw6sjbj6wnvfkklms6c7kjhwq5wksirgair0gznacgx3")))

