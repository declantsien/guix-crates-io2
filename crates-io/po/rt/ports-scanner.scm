(define-module (crates-io po rt ports-scanner) #:use-module (crates-io))

(define-public crate-ports-scanner-0.0.1 (c (n "ports-scanner") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive" "help" "std" "usage"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (k 0)))) (h "04m7w7yac7pmd6nhdw5q2in6wrvbhf161r995g1nbnraq98ld078")))

(define-public crate-ports-scanner-0.0.2 (c (n "ports-scanner") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive" "help" "std" "usage"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (k 0)))) (h "044b1agywhxaia1chgwnpnl3x6zi7rf24lsvyh9607l4lsbsd7yq")))

