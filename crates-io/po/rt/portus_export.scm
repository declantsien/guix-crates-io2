(define-module (crates-io po rt portus_export) #:use-module (crates-io))

(define-public crate-portus_export-0.1.0 (c (n "portus_export") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1p0d061g7878vm4zvx472lqyi7pr5sm1vgl8pvyjr5dx2gw6m5qx")))

(define-public crate-portus_export-0.2.0 (c (n "portus_export") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10xzm3dd4chfv35jf27ra3gjf6jbgd0m1g8c9xhg7y4bqq2c8zii")))

