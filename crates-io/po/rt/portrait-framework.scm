(define-module (crates-io po rt portrait-framework) #:use-module (crates-io))

(define-public crate-portrait-framework-0.1.0 (c (n "portrait-framework") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1c89vnq759i3xdlyp3zlryvk1qrrwf1wgk4jz73f6zlp23jswsbq")))

(define-public crate-portrait-framework-0.2.0 (c (n "portrait-framework") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "02mqw6psvkvxx14sy5hgd3h8lfq1h7v229kvidfqyi2hdjp8sp0g")))

(define-public crate-portrait-framework-0.2.1 (c (n "portrait-framework") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0k7nm77rfav20wzqyfb0c35mcyzzkqq9k2qh8ks88xdk78j1fq8d")))

(define-public crate-portrait-framework-0.3.0-alpha.1 (c (n "portrait-framework") (v "0.3.0-alpha.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "096140m324cfyyar9jirjmxlbxgq4xv9zch75qnwhzpkyhqlrgcq")))

(define-public crate-portrait-framework-0.3.0 (c (n "portrait-framework") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1vv6z69vp9f8f6pbfwy0jw1w9q1n4vgzasbpv3f9f8zq6g7d1izp")))

