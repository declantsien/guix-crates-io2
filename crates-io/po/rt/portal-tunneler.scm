(define-module (crates-io po rt portal-tunneler) #:use-module (crates-io))

(define-public crate-portal-tunneler-0.1.0-beta.0 (c (n "portal-tunneler") (v "0.1.0-beta.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "inlined") (r "^0.1.0") (d #t) (k 0)) (d (n "portal-puncher-sm") (r "^0.1.0-beta.0") (d #t) (k 0)) (d (n "portal-tunneler-proto") (r "^0.1.0-beta.0") (d #t) (k 0)) (d (n "quinn") (r "^0.10.2") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "quinn-udp") (r "^0.4.1") (d #t) (k 0)) (d (n "rcgen") (r "^0.12.1") (d #t) (k 0)) (d (n "rustls") (r "^0.21.10") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09zqzr2b8w8ldh3g5mzrp2x3zqxvg4jvps8cf4gwhjp0frnia19v") (r "1.77.1")))

