(define-module (crates-io po rt ports-sniffer) #:use-module (crates-io))

(define-public crate-ports-sniffer-0.0.1 (c (n "ports-sniffer") (v "0.0.1") (h "0bprpn1fhjyx6fh0lmzg8m92zj03hxlym0hv518lqy4li8klrkgh")))

(define-public crate-ports-sniffer-0.0.2 (c (n "ports-sniffer") (v "0.0.2") (h "1cjsj15x8araqfw64s8vn8xp9sqjjgw7g3wxa9krhmppgi1lwx3g")))

(define-public crate-ports-sniffer-0.0.3 (c (n "ports-sniffer") (v "0.0.3") (h "0kzccspzjdrb4645lbs7k61n0f1vcrrc5i8bblgbyvhmphv9i56i")))

(define-public crate-ports-sniffer-0.0.4 (c (n "ports-sniffer") (v "0.0.4") (h "17d065ml73sgic3hk9krs3k2s0260gj4mxqk7n3japbmnfinfqmm")))

(define-public crate-ports-sniffer-0.1.0 (c (n "ports-sniffer") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 0)))) (h "04i4z2s1a0cvrfjfiirl9avi7n0rkv5h1ld0ynwxnyfvv9484jyj")))

