(define-module (crates-io po rt portaudio) #:use-module (crates-io))

(define-public crate-portaudio-0.3.0 (c (n "portaudio") (v "0.3.0") (h "0w0rap7d9677adwlyh3c3m91ms3jf2n63h0j9p46bmkp5is6ywpm")))

(define-public crate-portaudio-0.3.1 (c (n "portaudio") (v "0.3.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0wgzrlf4gw233cfq0vdcs1pkxg0xhddpfs0mjdzg3vm5pcjbpz2y")))

(define-public crate-portaudio-0.3.2 (c (n "portaudio") (v "0.3.2") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1jvdnsjkasqlfv1znpclmfsdwg05xck9b19g14vz288d3dhm8r2z")))

(define-public crate-portaudio-0.3.3 (c (n "portaudio") (v "0.3.3") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "14wf4vg97pqapijz4rsaqq9w2k553ycnh97cpncvfw1hww1kwraz")))

(define-public crate-portaudio-0.3.4 (c (n "portaudio") (v "0.3.4") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1pv5ffqpfgvh8ylaiw8n2jm22ij0zmrrnmzymcgk3gnhsjqk82kv")))

(define-public crate-portaudio-0.3.5 (c (n "portaudio") (v "0.3.5") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0ac9yc12l6p338y2x6j9kj0mbzb6ng9b1ih8a1w0ib3xfg9wsw4s")))

(define-public crate-portaudio-0.3.6 (c (n "portaudio") (v "0.3.6") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0kwwn4rrhxjb89g3yjwd1067mgj4dr2i8gg0835956s3dfj89v7g")))

(define-public crate-portaudio-0.4.0 (c (n "portaudio") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1idx3p8fhfbdj20h230z38pw70z16amx9ff6svs6hh9z01wqfgg6")))

(define-public crate-portaudio-0.4.1 (c (n "portaudio") (v "0.4.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "084nb6bqvj3iax6nkmfm0i7k2pp9h4cj3k04xfw9jzngsf0hma0s")))

(define-public crate-portaudio-0.4.2 (c (n "portaudio") (v "0.4.2") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0zgi8j5qhxcdjfrv5jimgiq5iwr7lijw5qsj2c2adhlfw4f3mh21")))

(define-public crate-portaudio-0.4.3 (c (n "portaudio") (v "0.4.3") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1c9icba7js5ikp78gq1p130syd9c4iwzzh1686kc3jchh2ks41ci")))

(define-public crate-portaudio-0.4.4 (c (n "portaudio") (v "0.4.4") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.22") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0m7qbcijb0x9rhn6r7c7gnh381l4rj8ikhxcvhd6b2avhm6zp4m2")))

(define-public crate-portaudio-0.4.5 (c (n "portaudio") (v "0.4.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0d8nbpnx2rx4bpaxc0207zz104vprh8ypp05d6www24yhvqzj06m")))

(define-public crate-portaudio-0.4.6 (c (n "portaudio") (v "0.4.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15g7b8b1j9bkbwcq8bqkllcihcks0382rcnyxzvz64bx1x3q0hii")))

(define-public crate-portaudio-0.4.7 (c (n "portaudio") (v "0.4.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0kbl7iixnx6yxw8bvx8d1z9mz8vwxz69ikxfnqkfxzg54ngyblb9")))

(define-public crate-portaudio-0.4.8 (c (n "portaudio") (v "0.4.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "102qw220231xjq7mn4420nyiqdgpdx214pwc17d8fhdfikckgx7f")))

(define-public crate-portaudio-0.4.9 (c (n "portaudio") (v "0.4.9") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "10zksfkq9b07sl3hq10bg52xc1phgwd89q84yfrxsylc40d32g9h")))

(define-public crate-portaudio-0.4.10 (c (n "portaudio") (v "0.4.10") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1xbvnpph29shizsv45rdlhbsf4wjhz48jvcv6298c28395d9344j")))

(define-public crate-portaudio-0.4.11 (c (n "portaudio") (v "0.4.11") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1wm92qyv7ha7rya2zr5dzpvbjmsyqazf8w1jpjx80gbbmlfdnac3")))

(define-public crate-portaudio-0.4.12 (c (n "portaudio") (v "0.4.12") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "12f15p5b9761f01ljfl0pr93yp5fxj2lhmdmlanb2a4d9ndw8d2m")))

(define-public crate-portaudio-0.4.13 (c (n "portaudio") (v "0.4.13") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0286jf9byvifxkbyswhmgx6jzn32wfh3p916bqvmqp6yw1b00rxa")))

(define-public crate-portaudio-0.4.14 (c (n "portaudio") (v "0.4.14") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1wprrib62a1chyma3pvixsx30vvsczz9c8kd2nvwimkw14hb6sb5")))

(define-public crate-portaudio-0.4.15 (c (n "portaudio") (v "0.4.15") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "03ldhsx5ss13fa7cw9jplv8k3ychrakpp9wn2l0cbgrvamk9pg74")))

(define-public crate-portaudio-0.4.16 (c (n "portaudio") (v "0.4.16") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0d0bwx6aw3g7mijq0g47kk6b4pbxx95pm59z18c5hzqlvmarggzq")))

(define-public crate-portaudio-0.4.17 (c (n "portaudio") (v "0.4.17") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "176ks4552kvr4lqmy5p15dglj7q3jzi2s5jiij0knfpm8720xia3")))

(define-public crate-portaudio-0.4.18 (c (n "portaudio") (v "0.4.18") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1jdv97skqifxgy9pfyq3ycjsmixvby9xlhdjv0c99m3wcsv3851x")))

(define-public crate-portaudio-0.4.19 (c (n "portaudio") (v "0.4.19") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "15ws7vfp0ddq07gz430b4wyvqscmd024lbn3ap0fbm3370k23xkk")))

(define-public crate-portaudio-0.4.20 (c (n "portaudio") (v "0.4.20") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0vcbd559jz0hhjns7davg0rlpb905wqmxhiz311skyijab2r38qr") (y #t)))

(define-public crate-portaudio-0.5.0 (c (n "portaudio") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1bajbfib9pk2dsgi6050hmm26gvhshnfxdhm46bxk1sdkadg0shp")))

(define-public crate-portaudio-0.5.1 (c (n "portaudio") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0rv0gaf0lhl68yzwjq24rxnb9054n50x9syn55rlh50n6xm36wms")))

(define-public crate-portaudio-0.6.0 (c (n "portaudio") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "01i5x622rx42fd4px9i7dxxqv7awqvizkn1zk65ngfbx06xf9pyq") (y #t)))

(define-public crate-portaudio-0.6.1 (c (n "portaudio") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1sp747jy73awd9vgmhn9lacdcjlr7jdsm8ypj5rd26hadbwh3rgh")))

(define-public crate-portaudio-0.6.2 (c (n "portaudio") (v "0.6.2") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "02hc3g86dl6zfv97dmxkp06sgd2xhpavxxv72vvbchmm9565chz3")))

(define-public crate-portaudio-0.6.3 (c (n "portaudio") (v "0.6.3") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1xwyrbia9vd5x24qc5ks5nwwnzzpzgnz8rrsqnh9yhqyx56l2ifk")))

(define-public crate-portaudio-0.6.4 (c (n "portaudio") (v "0.6.4") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0aixdpn8c9676xd04wwhh4vj8sdm6h6fh387fxxx70r01lyhdl3z")))

(define-public crate-portaudio-0.7.0 (c (n "portaudio") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1hy5znizvvw9i7d5x2mbzqx29llyqy37fxfqnq47cnhmyqar6a0d")))

