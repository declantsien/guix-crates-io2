(define-module (crates-io po rt port-rs) #:use-module (crates-io))

(define-public crate-port-rs-0.1.0 (c (n "port-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qkxqjwjca30kkcjmv8slh059i5shmryxz3wqf4g9l6i5zbyls8h")))

(define-public crate-port-rs-0.1.1 (c (n "port-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pr2l0j2hbrl6aj9nav8qc93yv28z7mgx8rzw99rymj3z3q12glh")))

