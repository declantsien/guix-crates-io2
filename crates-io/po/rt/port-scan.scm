(define-module (crates-io po rt port-scan) #:use-module (crates-io))

(define-public crate-port-scan-0.1.0 (c (n "port-scan") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1sm0b0ajm1lbc01mmqnp3q2shpz0jprrx0nf5bpk5p8b0nxl9pl4")))

(define-public crate-port-scan-0.1.1 (c (n "port-scan") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "096x46xwhs1r3pqd0n90q09f41kwcqmvs72dndqvlik2g0fi7rf7")))

