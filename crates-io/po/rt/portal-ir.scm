(define-module (crates-io po rt portal-ir) #:use-module (crates-io))

(define-public crate-portal-ir-0.1.0 (c (n "portal-ir") (v "0.1.0") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "14l21hgmza5hfikmcalhmyadck7739bkf8vyy1845fwp87mgs1la")))

(define-public crate-portal-ir-0.2.0 (c (n "portal-ir") (v "0.2.0") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "1arg6ygh1c6cwxwigm9nypmzqhbkslv6k28ljvxk4mlpqcq4avqr")))

