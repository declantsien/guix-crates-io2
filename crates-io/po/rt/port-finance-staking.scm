(define-module (crates-io po rt port-finance-staking) #:use-module (crates-io))

(define-public crate-port-finance-staking-0.1.0 (c (n "port-finance-staking") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "00dpq7s5ad7g9v2jx9lmbw24bpb09ja4ffp360klb3khfix8ycl5")))

(define-public crate-port-finance-staking-0.2.0 (c (n "port-finance-staking") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "01jy084wa2cmp36vy47q9d2vc8n3g64xz9h5sw4wllarpi9m78gc")))

(define-public crate-port-finance-staking-0.2.1 (c (n "port-finance-staking") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.7") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "0715qr9zl47cdj967fxpldj57kdy3a74kxjwz4143x3gzvm1kc1k")))

