(define-module (crates-io po rt portable_atomic_enum) #:use-module (crates-io))

(define-public crate-portable_atomic_enum-0.1.0 (c (n "portable_atomic_enum") (v "0.1.0") (d (list (d (n "portable-atomic") (r "^1.4") (d #t) (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1vkmx5xm483zzkwdgbfarjr7k6gxg0pkhxi2ayiyb1sh1fa2407m")))

(define-public crate-portable_atomic_enum-0.1.1 (c (n "portable_atomic_enum") (v "0.1.1") (d (list (d (n "portable-atomic") (r "^1.4") (d #t) (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.1.0") (d #t) (k 0)))) (h "14wh3w052yqmqqmd8lg4c3j7dbgpkqv9xqjhvqgy40ix6c9w4jbq")))

(define-public crate-portable_atomic_enum-0.1.2 (c (n "portable_atomic_enum") (v "0.1.2") (d (list (d (n "portable-atomic") (r "^1.4") (d #t) (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1w2pkdvzdlprxp3vf6kjjb0myx16m3rzban4mq37alaha8f2qaxh")))

(define-public crate-portable_atomic_enum-0.2.0 (c (n "portable_atomic_enum") (v "0.2.0") (d (list (d (n "portable-atomic") (r "^1.5") (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0hv6iky7ar1d5bsybbi12p3lyy613fvlll4zbsy1jxss4bd2jymi")))

(define-public crate-portable_atomic_enum-0.3.0 (c (n "portable_atomic_enum") (v "0.3.0") (d (list (d (n "portable-atomic") (r "^1.5") (o #t) (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0v8q2drjw9kmdcvw5189wgrkdqbrkpcf67ndbf8k9vlc6d1kw980") (s 2) (e (quote (("portable-atomic" "dep:portable-atomic"))))))

(define-public crate-portable_atomic_enum-0.3.1 (c (n "portable_atomic_enum") (v "0.3.1") (d (list (d (n "portable-atomic") (r "^1.5") (o #t) (k 0)) (d (n "portable_atomic_enum_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1188wbhc03rdkwlixr0vj55jzvabdnhqk1dl5fxj009yqih8zm1h") (s 2) (e (quote (("portable-atomic" "dep:portable-atomic"))))))

