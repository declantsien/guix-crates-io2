(define-module (crates-io po rt porter-stemmer) #:use-module (crates-io))

(define-public crate-porter-stemmer-0.1.0 (c (n "porter-stemmer") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^0.1.2") (d #t) (k 0)))) (h "04nr68kid19l0w7is5kgyg1jhqk94kv75wl7ng0h1va72b2jgqpd")))

(define-public crate-porter-stemmer-0.1.1 (c (n "porter-stemmer") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^0.1.2") (d #t) (k 0)))) (h "1f1w38xvi418amx2xs3vwkb861q3d2n2dc3sxq7v9335vhy8a5cw")))

(define-public crate-porter-stemmer-0.1.2 (c (n "porter-stemmer") (v "0.1.2") (d (list (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "0f531zz8bbfx57bwgv5wqp086cxmznabcgzg068qcm7a994k16dp")))

