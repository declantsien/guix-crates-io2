(define-module (crates-io po rt ports-cli) #:use-module (crates-io))

(define-public crate-ports-cli-0.1.0 (c (n "ports-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1y5iab18a1dspdagq02277clgv1ndjks1vyfv2c5hc4z9j6wl3pv")))

(define-public crate-ports-cli-0.1.1 (c (n "ports-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0ff5zwn8yblq0psn008cy6kcsr3s4772xkch3mqgz8625hwx4wvg")))

(define-public crate-ports-cli-0.1.2 (c (n "ports-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "00f0vngivfz26m55p03gk54kzcbriaradjhbl8zc9nhl0zbcyyk4")))

(define-public crate-ports-cli-0.2.0 (c (n "ports-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0b01hzfh8mqy795nwzsqhq7fph8ic9blpfkm8l0hfzwpfxf73nl1")))

(define-public crate-ports-cli-0.2.1 (c (n "ports-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0ighlhazwl7r8d858d66hq9sg3x6lg5s7wffj8bmdi4jrgx0k372")))

(define-public crate-ports-cli-0.2.2 (c (n "ports-cli") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "netstat2") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.22.4") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0xh4g0sv525liy2wy2y74qyafyms9ha87ncbdnikymbsgdp43p7k")))

