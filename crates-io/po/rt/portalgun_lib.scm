(define-module (crates-io po rt portalgun_lib) #:use-module (crates-io))

(define-public crate-portalgun_lib-0.2.0 (c (n "portalgun_lib") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "12v9f0zk7rd800ywlh2why6scsv98y47mgyh2gcwiglfnqc7xnj3")))

(define-public crate-portalgun_lib-0.2.1 (c (n "portalgun_lib") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1qs678k9yjlzg07flil06wdxrpa575sjpb84j3ilscd7ymzvl9hf")))

(define-public crate-portalgun_lib-0.2.2 (c (n "portalgun_lib") (v "0.2.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0zikw0dav5l2a9cf5vq1rd697dyxn0jjbpp1j085c0xrxnvlwmcz")))

