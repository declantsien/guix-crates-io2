(define-module (crates-io po rt portogrrs) #:use-module (crates-io))

(define-public crate-portogrrs-0.1.0 (c (n "portogrrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "0w6zj3llb1dqblapggjl33plzzbhd79yvwbh4jfzrnk1yy3xhz8n")))

