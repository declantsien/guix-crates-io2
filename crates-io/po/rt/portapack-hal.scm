(define-module (crates-io po rt portapack-hal) #:use-module (crates-io))

(define-public crate-portapack-hal-0.1.0 (c (n "portapack-hal") (v "0.1.0") (d (list (d (n "hackrf-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "lpc43xx") (r "^0.0.0") (d #t) (k 0)))) (h "15dcshjhcz5yirvncamlrvpkbm3c7bl5kzsibwd7p9ya6bpy5hr8")))

(define-public crate-portapack-hal-0.1.1 (c (n "portapack-hal") (v "0.1.1") (d (list (d (n "hackrf-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "lcd-ili9341") (r "^0.1.0") (d #t) (k 0)) (d (n "lpc43xx") (r "^0.0.0") (d #t) (k 0)))) (h "1bmzaxzd67x4g64x4hzabclpx04sw0v5a4bisk3xcxri64gpyynf")))

