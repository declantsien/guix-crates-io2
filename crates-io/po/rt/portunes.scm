(define-module (crates-io po rt portunes) #:use-module (crates-io))

(define-public crate-portunes-0.0.0 (c (n "portunes") (v "0.0.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "19ir5pl4dwml6kl35kkmml0s7jzsx8w0yma0g4qvwnvqhcwvhl2y")))

(define-public crate-portunes-0.0.1 (c (n "portunes") (v "0.0.1") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0y7m9qlxmw8syq86k5kic8r9ry7bymmvf3gz05d1ilm5jdpbmhrl")))

(define-public crate-portunes-0.0.2 (c (n "portunes") (v "0.0.2") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "078r4hjskd2ah5yd7x53pbiqbk6iqmqrlsx078ynr96z79x0siwb")))

