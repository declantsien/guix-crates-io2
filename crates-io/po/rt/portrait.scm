(define-module (crates-io po rt portrait) #:use-module (crates-io))

(define-public crate-portrait-0.1.0 (c (n "portrait") (v "0.1.0") (d (list (d (n "portrait-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "0wg9r6vrw47g9bcbf9z5wj53q07i5dpj151af0sxa9gafzs70i20")))

(define-public crate-portrait-0.2.0 (c (n "portrait") (v "0.2.0") (d (list (d (n "portrait-codegen") (r "^0.2.0") (d #t) (k 0)))) (h "077zszan6fmz1nigbpy4x0jkn9xrbd9lhpcc8c1cbdak3fzf9xjp") (f (quote (("delegate-filler" "portrait-codegen/delegate-filler") ("default-filler" "portrait-codegen/default-filler") ("default" "default-filler" "delegate-filler"))))))

(define-public crate-portrait-0.2.1 (c (n "portrait") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "portrait-codegen") (r "^0.2.1") (d #t) (k 0)))) (h "17kd7yxpns4kxc9d2wwk47bhlidkaqb4fghzffhj3ryfns55k3nk") (f (quote (("log-filler" "portrait-codegen/log-filler") ("delegate-filler" "portrait-codegen/delegate-filler") ("default-filler" "portrait-codegen/default-filler") ("default" "default-filler" "delegate-filler" "log-filler"))))))

(define-public crate-portrait-0.2.3 (c (n "portrait") (v "0.2.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "portrait-codegen") (r "^0.2.2") (d #t) (k 0)))) (h "0qbc35x5ixhcgxh2kqx404adg6b9f2xnkbqi3ykwlm7h613s8b5z") (f (quote (("log-filler" "portrait-codegen/log-filler") ("delegate-filler" "portrait-codegen/delegate-filler") ("default-filler" "portrait-codegen/default-filler") ("default" "default-filler" "delegate-filler" "log-filler"))))))

(define-public crate-portrait-0.3.0-alpha.1 (c (n "portrait") (v "0.3.0-alpha.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "portrait-codegen") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "0mw4s425x9pizcvs4ldiscjbcvdgj21jp6xmbqhcx4rkpjy42szw") (f (quote (("log-filler" "portrait-codegen/log-filler") ("derive-delegate-filler" "portrait-codegen/derive-delegate-filler") ("delegate-filler" "portrait-codegen/delegate-filler") ("default-filler" "portrait-codegen/default-filler") ("default" "default-filler" "delegate-filler" "log-filler" "derive-delegate-filler"))))))

(define-public crate-portrait-0.3.0 (c (n "portrait") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "portrait-codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0hdqliy2sbr8pgqkh85j9d4000db8466hlpvcjzlpjp22dn9figg") (f (quote (("log-filler" "portrait-codegen/log-filler") ("derive-delegate-filler" "portrait-codegen/derive-delegate-filler") ("delegate-filler" "portrait-codegen/delegate-filler") ("default-filler" "portrait-codegen/default-filler") ("default" "default-filler" "delegate-filler" "log-filler" "derive-delegate-filler"))))))

