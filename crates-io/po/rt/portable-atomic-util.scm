(define-module (crates-io po rt portable-atomic-util) #:use-module (crates-io))

(define-public crate-portable-atomic-util-0.1.0 (c (n "portable-atomic-util") (v "0.1.0") (d (list (d (n "portable-atomic") (r "^1.0.0") (k 0)))) (h "1g6xq3avx28wfmpx3rn3p56g60my39z1c3ixxdk48dvkqgm07fy5") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t) (r "1.34")))

(define-public crate-portable-atomic-util-0.1.1 (c (n "portable-atomic-util") (v "0.1.1") (d (list (d (n "portable-atomic") (r "^1") (k 0)))) (h "13sa9ax1p0bpxbs97ihkdzgv1nw7wwd9hmp88mhh8m5n6j5s92ya") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t) (r "1.34")))

(define-public crate-portable-atomic-util-0.1.2 (c (n "portable-atomic-util") (v "0.1.2") (d (list (d (n "portable-atomic") (r "^1") (k 0)))) (h "0qd9a0p8bziz74lhaa5v8csd9r4lkfdlad1n6yjffaq2w72bygjc") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t) (r "1.34")))

(define-public crate-portable-atomic-util-0.1.3 (c (n "portable-atomic-util") (v "0.1.3") (d (list (d (n "portable-atomic") (r "^1.3") (f (quote ("require-cas"))) (k 0)))) (h "1k64a1yq96f8xdvgqls58qf630gpjhjjfmv2cfbx0r3jvgbzf12v") (f (quote (("std" "alloc") ("default") ("alloc")))) (y #t) (r "1.34")))

(define-public crate-portable-atomic-util-0.1.4 (c (n "portable-atomic-util") (v "0.1.4") (d (list (d (n "portable-atomic") (r "^1.5.1") (f (quote ("require-cas"))) (k 0)))) (h "1zm60i599qrh30vl3dwwk6vmk2czgf2q06jdrl62avx1mazb6cfj") (f (quote (("std" "alloc") ("default") ("alloc")))) (r "1.34")))

(define-public crate-portable-atomic-util-0.1.5 (c (n "portable-atomic-util") (v "0.1.5") (d (list (d (n "portable-atomic") (r "^1.5.1") (f (quote ("require-cas"))) (k 0)))) (h "16s4ag42sjkkas49fnlj99kx9zxdfqwk7ii12js533dk4lb439x1") (f (quote (("std" "alloc") ("default") ("alloc")))) (r "1.34")))

(define-public crate-portable-atomic-util-0.2.0 (c (n "portable-atomic-util") (v "0.2.0") (d (list (d (n "portable-atomic") (r "^1.5.1") (f (quote ("require-cas"))) (k 0)))) (h "1d9w8nvb2ak54wjql70sl9rivma9jh5sg1i86xliffgbc5w2k35i") (f (quote (("std" "alloc") ("default") ("alloc")))) (r "1.34")))

