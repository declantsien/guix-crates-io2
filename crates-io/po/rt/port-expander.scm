(define-module (crates-io po rt port-expander) #:use-module (crates-io))

(define-public crate-port-expander-0.1.0 (c (n "port-expander") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "01c0qxf17rd2zfy9riyq7kpybnqbhw4qpv31llcfib428ym8932q")))

(define-public crate-port-expander-0.2.0 (c (n "port-expander") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "0f8xccs2hqmayzl5v5q1cb3d5cslr4ldc3ixvnaqdl65ddgdyz7r")))

(define-public crate-port-expander-0.2.1 (c (n "port-expander") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "1dqnrnd04lifafwl9mcc2j5g8faz1b4wil8wwpm685li65p3203m")))

(define-public crate-port-expander-0.3.0 (c (n "port-expander") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "1ij3mz98li7pik9144z79r26ka6vcxx49lsb5p3wr20pmg3hzdcm")))

(define-public crate-port-expander-0.3.1 (c (n "port-expander") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "shared-bus") (r "^0.2.2") (d #t) (k 0)))) (h "1m64s5j129ckk5hri08bja2yhgmdfvyqp1kij09n1cq1wbbhsqqz")))

(define-public crate-port-expander-0.4.0 (c (n "port-expander") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "shared-bus") (r "^0.3.0") (d #t) (k 0)))) (h "0bi841x6ndi6wj4x1s0nhx18d6a4bszf1mc3gmni3a04kkbsxb1z")))

(define-public crate-port-expander-0.4.1 (c (n "port-expander") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "shared-bus") (r "^0.3.0") (d #t) (k 0)))) (h "1vprla97v6dnis5y1hygfd1x9ga1kim64apd5247sdn0bzw8dxlw")))

(define-public crate-port-expander-0.5.0 (c (n "port-expander") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)))) (h "07q9skgiwlgyccg2qc20sk5i84znv4swsjwbdixp494vrsd3x2sr") (f (quote (("std"))))))

(define-public crate-port-expander-0.6.0 (c (n "port-expander") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)))) (h "07scxk8x0k9djcdp1jynd703yfmnypmm27x87qfmlxk9vjmgfmdy") (f (quote (("std"))))))

(define-public crate-port-expander-0.6.1 (c (n "port-expander") (v "0.6.1") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)))) (h "0gqlqy5r59xzzky82yg3gdih21nlm7c3vd42zqpp50516ciss02r") (f (quote (("std"))))))

