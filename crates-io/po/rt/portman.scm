(define-module (crates-io po rt portman) #:use-module (crates-io))

(define-public crate-portman-0.1.0 (c (n "portman") (v "0.1.0") (d (list (d (n "clap") (r "= 2.27.1") (d #t) (k 0)))) (h "1lnz24gxc17a9whwskm86cmg9mc9fhav0rq48a89xld2wllrgqas")))

(define-public crate-portman-0.1.1 (c (n "portman") (v "0.1.1") (d (list (d (n "clap") (r "= 2.27.1") (d #t) (k 0)))) (h "06qgmyz7n9an0zs3kd3gyqdz96q4v7c2vpi8ax7931pfaw4pj27r")))

(define-public crate-portman-0.2.0 (c (n "portman") (v "0.2.0") (d (list (d (n "clap") (r "= 2.27.1") (d #t) (k 0)))) (h "0i2hwh8ls99bm47dka460rr92rgnzcsrjidxxdjjkm1c7dgyqyg7")))

(define-public crate-portman-0.2.1 (c (n "portman") (v "0.2.1") (d (list (d (n "clap") (r "=2.27.1") (d #t) (k 0)))) (h "0dcc25l9zal3mxsan88n8rqs1yis0g7bmgx4mz7gr8mi0n2cgl5v")))

