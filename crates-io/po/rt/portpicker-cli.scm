(define-module (crates-io po rt portpicker-cli) #:use-module (crates-io))

(define-public crate-portpicker-cli-0.1.0 (c (n "portpicker-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "portpicker") (r "^0.1.1") (d #t) (k 0)))) (h "03kdqw04vac5y9y72cwkndiyc96w6y268ahp93gdk185ps5nii6p")))

