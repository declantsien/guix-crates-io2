(define-module (crates-io po rt portpulse) #:use-module (crates-io))

(define-public crate-PortPulse-0.1.0 (c (n "PortPulse") (v "0.1.0") (h "1nrgwikb2csib2nprv4bw07rvsma7lf1mifvvzkn4bdsm7wxp298") (y #t)))

(define-public crate-PortPulse-0.2.0 (c (n "PortPulse") (v "0.2.0") (h "0cr5z4qnnncavqzld1gnj2pf0albninmz5x937hrvdw06l3dww8a") (y #t)))

