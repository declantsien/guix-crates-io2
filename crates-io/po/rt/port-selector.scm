(define-module (crates-io po rt port-selector) #:use-module (crates-io))

(define-public crate-port-selector-0.1.1 (c (n "port-selector") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qphrs5pakin8ydivm1fds7mgb36jzhkfca77fiijvadf3byxgsp")))

(define-public crate-port-selector-0.1.2 (c (n "port-selector") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b6zag17ndyjh72fw2xxlljk660d8lh86ypy2s104zm845csfaw3")))

(define-public crate-port-selector-0.1.3 (c (n "port-selector") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ws219jnvfg8725ikd4whrzc1f0257k69y4kgvrv5b99rsmpmjm8")))

(define-public crate-port-selector-0.1.4 (c (n "port-selector") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kymrng2372kvj51il31bzys11pi6rx1rxizyslpjkb33v4dnxk6")))

(define-public crate-port-selector-0.1.5 (c (n "port-selector") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ynsam1s3wm2jzfl2xk83l3n07z6yvydckkwszld723lkmnyjhsk")))

(define-public crate-port-selector-0.1.6 (c (n "port-selector") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s7zpap1774avw2ll6vppc6pn7w9cb83py8gky9xh355a7srw4dx")))

