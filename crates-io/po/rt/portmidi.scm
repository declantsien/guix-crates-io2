(define-module (crates-io po rt portmidi) #:use-module (crates-io))

(define-public crate-portmidi-0.0.1 (c (n "portmidi") (v "0.0.1") (h "1k0j31bfgg16r48fbmx1336kjyqc2irc67plvksiabiag2k1hr8n")))

(define-public crate-portmidi-0.0.2 (c (n "portmidi") (v "0.0.2") (h "19lwig95lfv40wfv517dlnbk214is5py6f6f9c63z395kkaykx52")))

(define-public crate-portmidi-0.0.3 (c (n "portmidi") (v "0.0.3") (h "0v995dwh88crha9dn7vvq8y68km7dhqqj89gm279n8lprfwqw3kj")))

(define-public crate-portmidi-0.0.4 (c (n "portmidi") (v "0.0.4") (h "188la6625xm2b5ynkw4264rsvzhwvfy16s7dlqlzaig69b7cfvys")))

(define-public crate-portmidi-0.1.0 (c (n "portmidi") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "06mg8v64p4kkgllabhfvimnxy8p702sc1rcvdyq9h5nvk0daf5iw")))

(define-public crate-portmidi-0.1.1 (c (n "portmidi") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "15l3g6p70yvgcd16pbxsakxivlnr1higxl9k5zi959h0g1gr9kjq")))

(define-public crate-portmidi-0.1.2 (c (n "portmidi") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0l9sjhwpc5m449jzdgfx8ifdwhvhbbndwx4sq622h1z1lvp3pi1l")))

(define-public crate-portmidi-0.1.3 (c (n "portmidi") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0f5i1c5libfi6lmx43n7lnjlwm9ff600lzmkr9bma2xx3j7mzq19")))

(define-public crate-portmidi-0.1.4 (c (n "portmidi") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fc0389nyrv4583asvkn07ngb01zjbxabjd6i370jmx5jqpn1qrb")))

(define-public crate-portmidi-0.1.5 (c (n "portmidi") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "1wgqkiw8ds0h9hn92mfpn7l6ymvvhjgyklpvfs87wgkzd3lq4dkv")))

(define-public crate-portmidi-0.1.6 (c (n "portmidi") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "0imqb0p5b8ssblyanwfvnlhhmw1w3qxws5sindpz3mzi8zi0b7cs")))

(define-public crate-portmidi-0.2.0 (c (n "portmidi") (v "0.2.0") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 2)))) (h "0hdbcx042sgkyym91lmh69cn3va128612kxbw7kyjwvb9h8pszh8")))

(define-public crate-portmidi-0.2.1 (c (n "portmidi") (v "0.2.1") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 2)))) (h "0n5bcv55spacqn2mpzahghilycbnh3wx6lq650iyavck6yhf468p")))

(define-public crate-portmidi-0.2.2 (c (n "portmidi") (v "0.2.2") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 2)))) (h "195z1l8788dwl8hsq06j4s3jb70782s6y3j9mp09p6cfgaw8lxnn")))

(define-public crate-portmidi-0.2.3 (c (n "portmidi") (v "0.2.3") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 2)))) (h "0ida869fgamd332zcga081j90lsavnl7xb1p8m4vfd4pq9906jwh")))

(define-public crate-portmidi-0.2.4 (c (n "portmidi") (v "0.2.4") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 2)))) (h "0iw7i0zwznribaawzk8yrf2abfacmcj15abfmwlc2zqnr1nnawlf")))

(define-public crate-portmidi-0.2.5 (c (n "portmidi") (v "0.2.5") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)))) (h "0mr5vivn3yzs8a2zhbmvwngnf9zrk95rmnf172vljw86xia5isqp")))

(define-public crate-portmidi-0.2.6 (c (n "portmidi") (v "0.2.6") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)))) (h "1ds9dpgky3znw8g3w93fqjvn1imj6hx4l40pd2n06kwmcvqmlhy0")))

(define-public crate-portmidi-0.3.0 (c (n "portmidi") (v "0.3.0") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 2)) (d (n "rci") (r "~0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)))) (h "1jqd11apb5a7vg9y1528d7mg23fhcw624xlj2qqs1rzp9kwj40ap")))

