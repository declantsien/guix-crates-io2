(define-module (crates-io po rt portunusd) #:use-module (crates-io))

(define-public crate-portunusd-0.1.0 (c (n "portunusd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "04rcyvwxmgl0ndap3wfv0lllcphi0wcv9kal6ky7rvba8msvx7n3")))

(define-public crate-portunusd-0.2.0 (c (n "portunusd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.96") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0krcnsh6qg0g2b6w1qrdpqkmdx7fd7cp0zx4c6bx1sq5c3jcq17a")))

(define-public crate-portunusd-0.3.0 (c (n "portunusd") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.96") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1lgjkib2hm8vavcnylacpchi55psb65m92zpbhxcxbi04pak37iy")))

