(define-module (crates-io po rt portaudio-sys) #:use-module (crates-io))

(define-public crate-portaudio-sys-0.1.1 (c (n "portaudio-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1xdpywirpr1kqkbak7hnny62gmsc93qgc3ij3j2zskrvjpxa952i")))

