(define-module (crates-io po rt portpicker) #:use-module (crates-io))

(define-public crate-portpicker-0.1.0 (c (n "portpicker") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1fgb5pdayxy5nlx1lh60r7gx7wx45zza802w8hwhkq3gq42psjav")))

(define-public crate-portpicker-0.1.1 (c (n "portpicker") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1acvi1m6g7d3j8xvdsbn0b7yqyfy7yr7fm1pw5kbdyhvmxpxg5xy")))

