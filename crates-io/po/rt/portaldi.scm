(define-module (crates-io po rt portaldi) #:use-module (crates-io))

(define-public crate-portaldi-0.1.0 (c (n "portaldi") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1xhiqaqpdmjj6vc2d38b4805rpi5mx80g96ibmxx674prxcz3pi4")))

(define-public crate-portaldi-0.1.1 (c (n "portaldi") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.1.1") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0j1ilm2kmjqmzwvw4r2zvjxgfxya9dnpww3xy4svf60as30j0amr")))

(define-public crate-portaldi-0.1.2 (c (n "portaldi") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.1.2") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10kn7l92ana7mzqnf6qbxhbp7h01f84n6s6d59ch6sklzaj5c4da")))

(define-public crate-portaldi-0.1.3 (c (n "portaldi") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.1.3") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yf1ngdkifmg3d6k50klzagwhni8rb08bj39jspjw4wf3nig65lm")))

(define-public crate-portaldi-0.1.4 (c (n "portaldi") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.1.4") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1mdqjp353s4gxi428gcxwwgz6107aibinlbwqdv4bvzl7gk474bf")))

(define-public crate-portaldi-0.2.1 (c (n "portaldi") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.0") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08yw9r630y9n8c4l8f946cbnfjid183wbp7mn20axbf0n4z74cdr")))

(define-public crate-portaldi-0.2.2 (c (n "portaldi") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.0") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06wxb367drcyihdh4iry0jcw9a8d0gbrq7dc7gjdnw5xjdr6pxbk")))

(define-public crate-portaldi-0.2.3 (c (n "portaldi") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.3") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1yijs7bgak1h0rbrlfl52ijky1amxj7mclf2lw8dirlvr9l57692")))

(define-public crate-portaldi-0.2.4 (c (n "portaldi") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.4") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "19xcjgls5nn1pa5lr6a5yas867hmdfhxkmwbd50d3i8vz6m5jd6y") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.5 (c (n "portaldi") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.5") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0w3r5m8wqrfdxv8p4plrf0k01g3yqi081swi001yynplbdncy9ni") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.6 (c (n "portaldi") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.6") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wgzrczma63fr84fdixrjps4a4606phwbhy01j1s8sw9nfixivr5") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.7 (c (n "portaldi") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.7") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wspx36bcqcjgaza4agsbxjp2j6cnaq6k3k1g3y7yl9iy81xrr1y") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.8 (c (n "portaldi") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.8") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1kgv33rdr7j5kipi50pgvknj4wa1p9nhsc9fhyjxb0nznlngimpl") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.9 (c (n "portaldi") (v "0.2.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.9") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0pfyps40dji0y4hdppxpwv1ywbs6r84bfxcmkwpbhmk2rgd6jl2r") (f (quote (("wasm" "portaldi-core/wasm" "portaldi-macros/wasm") ("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.10 (c (n "portaldi") (v "0.2.10") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.10") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "120am9d3x7qgjvlzbsq4kb2w5sfsspbxr9hcwlqryqzyy4i357jk") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.11 (c (n "portaldi") (v "0.2.11") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.11") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "03hphw65d2imasfwj3zyfmjin9vjcqm2h2ngiqra0l7rpcg0zbqg") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.12 (c (n "portaldi") (v "0.2.12") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.12") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "142g05fcb89g8fiq1rqzybnncqy936nsd1mq66abdzykcmwkab5j") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.13 (c (n "portaldi") (v "0.2.13") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.13") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "02a6y5prgwa4w43j84zizypniw727drmvga5biz63ki788vakmwb") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.14 (c (n "portaldi") (v "0.2.14") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.14") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1b342zsxnscdici7c78w1srir3q77cr25i6vz4w7vxz7k8z521pd") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

(define-public crate-portaldi-0.2.15 (c (n "portaldi") (v "0.2.15") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "portaldi-core") (r "^0.2.15") (d #t) (k 0)) (d (n "portaldi-macros") (r "^0.2.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11i8xlq0fmkgcb46hkcylb11s1v19rs58c09hp8bdwhk1z3dkdxy") (f (quote (("futures-join" "portaldi-macros/futures-join") ("default"))))))

