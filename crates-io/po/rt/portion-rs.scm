(define-module (crates-io po rt portion-rs) #:use-module (crates-io))

(define-public crate-portion-rs-0.0.1 (c (n "portion-rs") (v "0.0.1") (h "196034df8lwarxlc727c0q43lcx5fpwx7f10s3znr3c6779apz6g")))

(define-public crate-portion-rs-0.1.0 (c (n "portion-rs") (v "0.1.0") (h "1bdl6ypnsqg3qwsjzigsl3dnnhg3a3b0q3czdhkb9z25vi64zxf5")))

(define-public crate-portion-rs-0.2.0 (c (n "portion-rs") (v "0.2.0") (h "1p589mhq5y9bjars9yhjnyckla43xhjlparzvk8jdk2zxpxqx6j3")))

(define-public crate-portion-rs-0.3.0 (c (n "portion-rs") (v "0.3.0") (h "162cjxxlqp584n08pckvfdzrz006lr2i7jj47gdbrw5yy26fcw94")))

(define-public crate-portion-rs-0.3.1 (c (n "portion-rs") (v "0.3.1") (h "0p0x01d6gd4xb1r1qkjlinbck62crlxkyq2xd8qbaivgi75gp5n8")))

