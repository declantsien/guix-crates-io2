(define-module (crates-io po rt port_scanner) #:use-module (crates-io))

(define-public crate-port_scanner-0.1.0 (c (n "port_scanner") (v "0.1.0") (h "1khj15mnf31d32klcqnbf6gvnzzfswx6fbliy85vykbim93ygk6g") (y #t)))

(define-public crate-port_scanner-0.1.1 (c (n "port_scanner") (v "0.1.1") (h "087ihvqkw0gw42xjmin3x8kzxbxikjm3nzfhggb586wyhrdm9ilz") (y #t)))

(define-public crate-port_scanner-0.1.2 (c (n "port_scanner") (v "0.1.2") (h "1ph6fdzqzflq4kh9qv9gp87wxskbxw015xbqasjc0b694fqpr9w4") (y #t)))

(define-public crate-port_scanner-0.1.3 (c (n "port_scanner") (v "0.1.3") (h "0ff4a7pyppykkvwihf4aw4s5za8jc6idc2crscqziysxq68f87kp")))

(define-public crate-port_scanner-0.1.4 (c (n "port_scanner") (v "0.1.4") (h "0iapwi3sql41aan3f5mik4xrk33bzvgv2zfrdqacbq8hp9vca6w4")))

(define-public crate-port_scanner-0.1.5 (c (n "port_scanner") (v "0.1.5") (h "1nh599w9azgdf2p2s2kb17zivv4ap69ljbb1nb1r7qnyqlm6snij")))

