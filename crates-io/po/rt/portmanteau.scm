(define-module (crates-io po rt portmanteau) #:use-module (crates-io))

(define-public crate-portmanteau-0.1.0 (c (n "portmanteau") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1zw6ihzdpqja3qf6vyvpmsql2xd9qcsxmh9piv79fai5wslnybl1")))

(define-public crate-portmanteau-0.2.0 (c (n "portmanteau") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1r9ri04y3awv90yx142467zzsjzs30pxk3p1z3xzq1s6514dbm1c")))

(define-public crate-portmanteau-0.2.1 (c (n "portmanteau") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "068bdc8nf74h9sxz9ymzwixnd862rzisnnmhxkapllwjqkgm0ca3")))

