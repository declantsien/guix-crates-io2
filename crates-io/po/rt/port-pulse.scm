(define-module (crates-io po rt port-pulse) #:use-module (crates-io))

(define-public crate-port-pulse-0.2.0 (c (n "port-pulse") (v "0.2.0") (h "02hr57vblmrxil8q5izjx34v03qjx90p6gcs14f5li189z4m6c3p") (y #t)))

(define-public crate-port-pulse-0.3.0 (c (n "port-pulse") (v "0.3.0") (h "0im96g0xbbfkf4jyq8n0bm3xqgdqwa3cpd0q4y8kra8xfbwar88y") (y #t)))

(define-public crate-port-pulse-0.4.0 (c (n "port-pulse") (v "0.4.0") (h "040blwimr48wls49c8fykwfhfndcc5rcim4j3kwp6ff7kzqcahg2")))

