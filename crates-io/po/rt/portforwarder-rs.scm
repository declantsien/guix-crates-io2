(define-module (crates-io po rt portforwarder-rs) #:use-module (crates-io))

(define-public crate-portforwarder-rs-0.1.0 (c (n "portforwarder-rs") (v "0.1.0") (d (list (d (n "chan") (r "^0.1.21") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3.1") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.4.0") (d #t) (k 0)) (d (n "igd") (r "^0.6.0") (d #t) (k 0)))) (h "0wnsmg5b5q3mjy63vfxdxksplmrqr9a76dri4k9rc5fc3glhiwdb")))

