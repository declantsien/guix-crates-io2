(define-module (crates-io po rt portaldi-core) #:use-module (crates-io))

(define-public crate-portaldi-core-0.1.0 (c (n "portaldi-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qryiq0sggcyv2nvqiyql5k196j9czhc9j9jl3wzqbwli6r4h9s3")))

(define-public crate-portaldi-core-0.1.1 (c (n "portaldi-core") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "19djprybyn846fcxa5aqf662hk5lqrnxagn5s8i1ygvrlnvwqrr3")))

(define-public crate-portaldi-core-0.1.2 (c (n "portaldi-core") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1iv4q2hcrkv3dzbizbv7zckp42w3w467qndl1gbvm7kfnvf7c7jn")))

(define-public crate-portaldi-core-0.1.3 (c (n "portaldi-core") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0d3zh0a4195sk75hp91w445k1i2k85pzz9pqhp116m2hgm5ci3k3")))

(define-public crate-portaldi-core-0.1.4 (c (n "portaldi-core") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13flaah2mrhv4bl6cnpv1l446psnzw2rv0f0mgf8yvq6wvnfarar")))

(define-public crate-portaldi-core-0.2.1 (c (n "portaldi-core") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1scwk52rld2pa4b0m4mljyz73kzxc3s3pg1x7b959dka6x3hvnlx")))

(define-public crate-portaldi-core-0.2.2 (c (n "portaldi-core") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17xaw058ll9yqqfabkdf8pr4iw4nzdpk6way8h6dyxppc5a55s90")))

(define-public crate-portaldi-core-0.2.3 (c (n "portaldi-core") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "173ldh6d674ifvgcqbzlbm1vrj7cgdcf7i98jpamv94mps4jlx3m")))

(define-public crate-portaldi-core-0.2.4 (c (n "portaldi-core") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ncail2xskfxy0hbgdvfl9sf5hzryc5lfpz26052wjhi9h8aisz4")))

(define-public crate-portaldi-core-0.2.5 (c (n "portaldi-core") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0379gmmkqadr4laa870xnqz3xz15mzlcf2zd1n0g8iprcjxaw4k2")))

(define-public crate-portaldi-core-0.2.6 (c (n "portaldi-core") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "04bjxsvb2j1j7kwain5wj9pzc2j3hsjm8svxcrhgcks8gs30mdzi")))

(define-public crate-portaldi-core-0.2.7 (c (n "portaldi-core") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1iyii2zax9520kr9izxmv6virxbwshda5qm61s2r25lpl51fnirf")))

(define-public crate-portaldi-core-0.2.8 (c (n "portaldi-core") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0122b4ys6m0fjav7xk54094sp8zpx664zwy0k1q6s0swi12jglwn")))

(define-public crate-portaldi-core-0.2.9 (c (n "portaldi-core") (v "0.2.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12mkm9a4mcx3r3z13pian6243dfgqm0nnczhd0jhrrm2a2vg0g5n") (f (quote (("wasm") ("default"))))))

(define-public crate-portaldi-core-0.2.10 (c (n "portaldi-core") (v "0.2.10") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1vca37c9iw9ir2vbyrg9475kh7mcbgdi17vpml31v73dg2n41lad") (f (quote (("default"))))))

(define-public crate-portaldi-core-0.2.11 (c (n "portaldi-core") (v "0.2.11") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1pdc9hwyfxzqn9zk6byh0vkq6anz1bra5whcavvn9c06s902wcir") (f (quote (("default"))))))

(define-public crate-portaldi-core-0.2.12 (c (n "portaldi-core") (v "0.2.12") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05lk2pm4lbqbsr7kx26h8h3vb36r556y4alc96223mx0z1pw1yix") (f (quote (("default"))))))

(define-public crate-portaldi-core-0.2.13 (c (n "portaldi-core") (v "0.2.13") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0vla9a59bz7j1wdnwvq70jhsx0w98kiyzck6fpzfnzw8n38w04p0") (f (quote (("default"))))))

(define-public crate-portaldi-core-0.2.14 (c (n "portaldi-core") (v "0.2.14") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1d4l1m37i2jnx88j0sypp5ajymgib3cja5yyyj9slf9ykmmf3zcr") (f (quote (("default"))))))

(define-public crate-portaldi-core-0.2.15 (c (n "portaldi-core") (v "0.2.15") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0nb76hq99pzwfhhpcy1gn9s2ncgwgl9ld2ifjqqv0b6hl0kd7qza") (f (quote (("default"))))))

