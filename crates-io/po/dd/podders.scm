(define-module (crates-io po dd podders) #:use-module (crates-io))

(define-public crate-podders-0.1.0 (c (n "podders") (v "0.1.0") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0za8zh118sf64y9bp8s93h6vq3mxnr1xq03p7rgz07nsk01f6k6q") (y #t)))

(define-public crate-podders-0.1.1 (c (n "podders") (v "0.1.1") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1d03qn42imp0857il2ac91w9gcxh6yki8ds5imip6b1maccxc154") (y #t)))

(define-public crate-podders-0.1.2 (c (n "podders") (v "0.1.2") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "12if4q3xksrcyy4j0sghxcq44ihly9ksaxy8gbjd5k0gn7xxzklv")))

(define-public crate-podders-0.1.3 (c (n "podders") (v "0.1.3") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "flatbuffers") (r "^23.5.26") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yc2clmw7cbxi97igap55sjv3pzlnv3zfjfm8k8wm1blxp7kldvb")))

