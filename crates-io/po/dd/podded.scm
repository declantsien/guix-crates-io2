(define-module (crates-io po dd podded) #:use-module (crates-io))

(define-public crate-podded-0.1.0 (c (n "podded") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "11ghfz85zm6j9qlrrgdnhyxc3pcv0fx4gxiw0a0zmqzfw8dvrhx1")))

(define-public crate-podded-0.2.0 (c (n "podded") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h0z6zabqy7vzq6122zqr3gsvgscyjk6gkny0j22m704kgalxpcv")))

(define-public crate-podded-0.3.0 (c (n "podded") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wwm2bczk8kva4a5ps6zsvdcj1ggd2swlpn92b03vg3ji2iipz7r")))

(define-public crate-podded-0.3.1 (c (n "podded") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pjhgzfl297w3c1w9acdv4nfgjavr99gj7i2shslj4aj1hx3ndkg")))

(define-public crate-podded-0.4.0 (c (n "podded") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p82wj63ffp4h9cjv5jimw5n6v0jln95s7b7mk1qz551qdq0g6i3")))

(define-public crate-podded-0.5.0 (c (n "podded") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fniaf6l2vz1zs6jnyp31rpj8rbj2l0jwkrhgyi9n5s48v82a0w8")))

(define-public crate-podded-0.5.1 (c (n "podded") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kpv5jrya7ngvy13q32ncpx1km2xpvr0qh9qw0w7islmaf2h7vz5")))

