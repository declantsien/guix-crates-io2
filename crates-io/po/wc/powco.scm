(define-module (crates-io po wc powco) #:use-module (crates-io))

(define-public crate-powco-0.1.0 (c (n "powco") (v "0.1.0") (h "0algmsx9gpd9qkmidgsvr54gsdvajwv3kw7i780bqq2m29s9bx0q")))

(define-public crate-powco-0.1.1 (c (n "powco") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.21.2") (f (quote ("rand" "bitcoin_hashes"))) (d #t) (k 0)) (d (n "sv") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1amaakq5zc5mwl7jny9rzz069f68mfz0jpwj4pba803lx7sb6nb9")))

(define-public crate-powco-0.1.2 (c (n "powco") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.21.2") (f (quote ("rand" "bitcoin_hashes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sv") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03qmjzcgmji9yialzb1h270w2x9ka4l05xlpjcwdw1p40gwwb75n")))

(define-public crate-powco-0.1.3 (c (n "powco") (v "0.1.3") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.21.2") (f (quote ("rand" "bitcoin_hashes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sv") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0na6cwk8d4h63qzc0y9zcvwyyrjdj3n7iklak92311bf05sl4hdz")))

