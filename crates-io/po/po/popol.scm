(define-module (crates-io po po popol) #:use-module (crates-io))

(define-public crate-popol-0.1.0 (c (n "popol") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "012innlnwxg1kihy08x1k8hsjgvdcnm1148an4hmm7dqsdviz40n")))

(define-public crate-popol-0.2.0 (c (n "popol") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "066pw6g6mqwzps5w12ih12hz7i6dkh63ab6jn9im21ljvln0djqz")))

(define-public crate-popol-0.3.0 (c (n "popol") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1gzsqhsibp384p6mxic8ysabwg2r7bx3dk228jdb6va61r4nkfsk")))

(define-public crate-popol-0.4.0 (c (n "popol") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0bas5bn53brsd7srhmm6rh76g36k63pcvx7k3z46a8r9pwr29k6r")))

(define-public crate-popol-0.5.0 (c (n "popol") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "158l44krvmldvsqvcl91m3qzfa2c44rj8diwpwdrjbgwwq891b1d")))

(define-public crate-popol-1.0.0 (c (n "popol") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "07m4v5pvzydl1ykp0yg58rbi2ycxyfdidq51q73k5m3fwnlc4nkw")))

(define-public crate-popol-2.0.0 (c (n "popol") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "1klsnwfp1gwyfrvrkvw2ngw8apfivnq691vqri57dhzzn0376my6")))

(define-public crate-popol-2.1.0 (c (n "popol") (v "2.1.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "15zk4395n7hg4p212spk1wzpngjpbid3c8hsw1cliiyh32mz96w3")))

(define-public crate-popol-2.2.0 (c (n "popol") (v "2.2.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "0cf5yg57cmwp4bkww4pf5yd0a04pba1fciqd6h89sfxybh8xslx9")))

(define-public crate-popol-3.0.0 (c (n "popol") (v "3.0.0") (d (list (d (n "libc") (r "^0.2.134") (d #t) (k 0)))) (h "1qskbisrdkpzyyn45dr55avgwl98wvarbks114jlci1fa0rnjh4k")))

