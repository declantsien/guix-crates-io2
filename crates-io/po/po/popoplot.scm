(define-module (crates-io po po popoplot) #:use-module (crates-io))

(define-public crate-popoplot-0.2.0 (c (n "popoplot") (v "0.2.0") (d (list (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0d1nvkgprbblh8g79qx4hyl0z0iy6qx784aq13lsfbysr2cfqg17")))

(define-public crate-popoplot-0.3.0 (c (n "popoplot") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1fgx71l7ybcb0yr4rjhjwgyxp8v6gpxc8xkcnrwqbya3m30fk2p2")))

(define-public crate-popoplot-0.4.0 (c (n "popoplot") (v "0.4.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0aarka01h90kgrya6bpih10igmk4zpflspakmi4223jk9r9npf5i")))

(define-public crate-popoplot-0.5.0 (c (n "popoplot") (v "0.5.0") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1lc3gvyx4hbi527x9v8xr24fbq13fdw9n8jw8bx0slvcw0vii8vi")))

(define-public crate-popoplot-0.6.0 (c (n "popoplot") (v "0.6.0") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0v0vfiskzx7xggxpi2101fhv41dmb1f5gkdcj83gxj97l7i4vx9j")))

(define-public crate-popoplot-0.7.0 (c (n "popoplot") (v "0.7.0") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1447iw5xjhsihil81vzadqvm70m9fvckxv08irkgh73dggk07r9l")))

