(define-module (crates-io po nd pond) #:use-module (crates-io))

(define-public crate-pond-0.2.2 (c (n "pond") (v "0.2.2") (h "0j561y7dzk2lq1c17fhk170sx4avg84acyjhxil1lh2rjdxvrj8n")))

(define-public crate-pond-0.2.3 (c (n "pond") (v "0.2.3") (h "10i8pm5b85lwdga5ncg6f1si4k0nyvic1rrary74xnk5r0lxlv4g")))

(define-public crate-pond-0.3.0 (c (n "pond") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "0bja2jind3y3j4lbb5a2g4ccz38wsgzjnmsw7p54sansz005kdwc")))

(define-public crate-pond-0.3.1 (c (n "pond") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "0blqmylx1nx7r58gk551z0j54cvhrkan2m1xvgrbyrnsljb7hsfx")))

