(define-module (crates-io po k9 pok9) #:use-module (crates-io))

(define-public crate-pok9-0.1.0 (c (n "pok9") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01nxhrzpx8s6s5b22lalw5d6fsa19d7nhhsh2xzyjm275myqh40l")))

(define-public crate-pok9-0.1.1 (c (n "pok9") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "132afkp9khkzb8jksapdm4glq73wgnxvj1ixmdlr8ag56cx3k9g8")))

