(define-module (crates-io po ds podstakannik) #:use-module (crates-io))

(define-public crate-podstakannik-0.0.1 (c (n "podstakannik") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "043iskkhfb7h7n8py2bik72j9q1m7v1kbj9nfapg08xjvkgxpc0b")))

(define-public crate-podstakannik-0.0.2 (c (n "podstakannik") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "11936ma9mkrhywl5n1s02czzjyx4bd8z8y1f8jv7ic552wy5makw")))

