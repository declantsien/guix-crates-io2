(define-module (crates-io po ds podserve) #:use-module (crates-io))

(define-public crate-podserve-0.2.0 (c (n "podserve") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "id3") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.1") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.1") (d #t) (k 0)) (d (n "rss") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "structopt") (r "^0.2.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "00zq111dq47yipw2icz1zr9cjm1pmpb5b182q5xh4j8j8i2n618b")))

