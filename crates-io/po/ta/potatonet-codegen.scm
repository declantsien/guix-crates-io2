(define-module (crates-io po ta potatonet-codegen) #:use-module (crates-io))

(define-public crate-potatonet-codegen-0.2.0 (c (n "potatonet-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0zl4pmh4jzivrp5dwyw4kqf0x5krxpnhgvirwmy652r2gs7v0l0n")))

(define-public crate-potatonet-codegen-0.3.0 (c (n "potatonet-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1c60vmk7mqndi5lpaqdbdcg44is47nvyc6yi566zw7f0ws84bw5s")))

(define-public crate-potatonet-codegen-0.3.1 (c (n "potatonet-codegen") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "10bzbmg68i79lq0fb0ba2y4n6cdhdzp1m9s4z9i5dhwncg8pq5w0")))

(define-public crate-potatonet-codegen-0.3.2 (c (n "potatonet-codegen") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1qpiv1qmkaqwsn015g6wcdxsz67df1ggqzs51p7q4wlp81cck56w")))

(define-public crate-potatonet-codegen-0.3.3 (c (n "potatonet-codegen") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1x2k8fjnqhqx8vs3cgx3hwpgvig4ihz0nxl8gi7kj4qmw7n4c6vv")))

(define-public crate-potatonet-codegen-0.3.4 (c (n "potatonet-codegen") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5gm6cdwbr0sfvywngwh6n9jfpa4waja3xbw4jhnmk8qkacfvr7")))

(define-public crate-potatonet-codegen-0.4.0 (c (n "potatonet-codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1svw8dw6w0bhmx5kx1pbq05gifa9p31czfvsyvrwma4nfarviadb")))

(define-public crate-potatonet-codegen-0.4.1 (c (n "potatonet-codegen") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0rzd9gn61blc7g5lkq3ay47m29sk611lp3mi9yschcczric8xfsb")))

(define-public crate-potatonet-codegen-0.4.3 (c (n "potatonet-codegen") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "11zi2nds23mw8wv6fz7286q6dmhavi7vvq8anv4yh6ap3218x1kk")))

