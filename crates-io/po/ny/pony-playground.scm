(define-module (crates-io po ny pony-playground) #:use-module (crates-io))

(define-public crate-pony-playground-0.1.0 (c (n "pony-playground") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "hubcaps") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "1lkninw8dni2jbypb0zbpbzbbvnljga2jysgzqxjcykszk6n8l2j")))

