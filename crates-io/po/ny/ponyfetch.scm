(define-module (crates-io po ny ponyfetch) #:use-module (crates-io))

(define-public crate-ponyfetch-0.1.0 (c (n "ponyfetch") (v "0.1.0") (h "0vwhfbxj5s64by4j1mr8nkgi8m1simg30v0fbhph5cnp7ab5yyyh")))

(define-public crate-ponyfetch-0.2.0 (c (n "ponyfetch") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0ma13q6na0f5k4wmd7rzml1pw83yis9im51vrzza1mg1lvbzxhl2")))

(define-public crate-ponyfetch-0.2.1 (c (n "ponyfetch") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0h26wz2119cs1h54r6hp423ssazb8h205i98v94ik88rxrnnwmmh")))

