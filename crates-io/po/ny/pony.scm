(define-module (crates-io po ny pony) #:use-module (crates-io))

(define-public crate-pony-0.1.0 (c (n "pony") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "01wlp93f3yw551xjcwz5nhrvn8avny1h4wif27c0zz22lzczdilk") (y #t)))

(define-public crate-pony-0.1.1 (c (n "pony") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "0w6s8ylam438ari1cpg3q94spfz19znlfnpv2yinva5r7xkcghvb") (y #t)))

(define-public crate-pony-0.1.2 (c (n "pony") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "0dkm6fipsyda78dpbvm4a4zv0hyxr53zzg1sbqmwcpfwqhi7ykf5") (y #t)))

(define-public crate-pony-0.1.3 (c (n "pony") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "1p55dx2n1q741hppnf1mcjfip1mwg2zapq2skljm52c6k00aa136") (y #t)))

(define-public crate-pony-0.1.4 (c (n "pony") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "0kxf0z6qx275mhjyddmadinp2287xn7mb06s4f71hwyns93qffa8") (y #t)))

(define-public crate-pony-0.1.5 (c (n "pony") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "18njm2z3ni58pvvvlzlzpnx0fvxnbniqmm7rylxf93na52c8k7x5") (y #t)))

(define-public crate-pony-0.1.6 (c (n "pony") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)))) (h "19f4bwmxax8r2ngqrb0yjdg73x555fyjwr8zadigh2jx2nz582j3") (y #t)))

(define-public crate-pony-0.1.7 (c (n "pony") (v "0.1.7") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "0bgyrvzb8b8drz9chpr44ivncadmglyy64wjlykkdqw495jf8r9g") (y #t)))

(define-public crate-pony-0.1.8 (c (n "pony") (v "0.1.8") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "13sb3cw87a8r2rq4ddmj8470d4hw6zm7sz1w6gq9x60gfa68yg2z") (y #t)))

(define-public crate-pony-0.1.9 (c (n "pony") (v "0.1.9") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 0)))) (h "1d3wn83i7hn1xcfvvy4mi9r7pd754wlg0nhidbwrzdx657mv0j90") (y #t)))

(define-public crate-pony-0.1.10 (c (n "pony") (v "0.1.10") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)) (d (n "sha-1") (r "^0.7.0") (d #t) (k 0)))) (h "1gsghikhp46s1wv8h7f7zjir0clnsb67fmqh5lr1x2kjw8y8159w") (y #t)))

