(define-module (crates-io po mp pomprt) #:use-module (crates-io))

(define-public crate-pomprt-0.2.0 (c (n "pomprt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fp4zcp8p0aix79g63i75qbirb4mqa5sw9kwnb50n83awy58rpna")))

(define-public crate-pomprt-0.3.0 (c (n "pomprt") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pbbzbhi24j9np3da2mhkj9as1c6y4nwzl8cy74xhp3ypavii2qa")))

(define-public crate-pomprt-0.4.0 (c (n "pomprt") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g4cyinjhd05bn9pnrbvmlk7rdw2wpb7sipkgc6xb3scpxdgnbrk")))

(define-public crate-pomprt-0.5.0 (c (n "pomprt") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rawrrr") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "051bmqic1d909ygfsfwvcqcyiglmvyyqcm8by491vbzrz2qlfsl9")))

(define-public crate-pomprt-0.5.1 (c (n "pomprt") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rawrrr") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y0zwfphd4ilmirpdgrgbn9wrma4q7bwmhd19r0pbd8clgg151rj")))

(define-public crate-pomprt-0.5.2 (c (n "pomprt") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rawrrr") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0s9v84vlsjx1v0d2wbbf95mph2a775sq1k2jww3bw07mpz2p7rgd")))

(define-public crate-pomprt-0.5.3 (c (n "pomprt") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rawrrr") (r "^0.2.1") (d #t) (k 0)))) (h "14472f6bkwb9vr22mkkkpfcfsadixwysa7469ibqy86z29g8krch")))

