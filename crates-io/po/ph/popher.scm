(define-module (crates-io po ph popher) #:use-module (crates-io))

(define-public crate-popher-0.1.0 (c (n "popher") (v "0.1.0") (h "06xrkfa3dayb0cf8jfsgzwhid321p3blxfbqxq5rkxg6ydrzjzjs")))

(define-public crate-popher-0.1.1 (c (n "popher") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f6my9nkymzy9wsfrvmdxwj02nzqjg8s76ywd0r4kfawj7kkywqg")))

