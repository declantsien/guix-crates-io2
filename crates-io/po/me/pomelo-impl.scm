(define-module (crates-io po me pomelo-impl) #:use-module (crates-io))

(define-public crate-pomelo-impl-0.0.1 (c (n "pomelo-impl") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1703n4wvbskdwbkx9s45gp8z1znl0xyc644bkj03vf31g1clw6pv")))

(define-public crate-pomelo-impl-0.1.0 (c (n "pomelo-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z3hjpaz66mmhs87da7gax3xyp29srgdq95dmspkp5bsqnz4lgd7")))

(define-public crate-pomelo-impl-0.1.1 (c (n "pomelo-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yh9fa9gfn6wirr64dn2b351wim9im76r3dnacxyv9jjd0v5dqmw")))

(define-public crate-pomelo-impl-0.1.2 (c (n "pomelo-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xlxdidpqwqi9k7g3xrimq690ds27xgrbnkpb2yf2mp9353ja9kh")))

(define-public crate-pomelo-impl-0.1.3 (c (n "pomelo-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15imsa67f3ngnlz3g39bk7fm1ysxakqqpl3rqpwlg12mlgnp56sx")))

(define-public crate-pomelo-impl-0.1.4 (c (n "pomelo-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r4w9jwvlqxrwkmspxnqhxnnl67jbz9ibqd7vdr6x3b207ijvlxb")))

(define-public crate-pomelo-impl-0.1.5 (c (n "pomelo-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1caj1qziq3lf9q8r6l5x4xsrq26yxjajgnkar8d1bnj9ln3k4z2r")))

