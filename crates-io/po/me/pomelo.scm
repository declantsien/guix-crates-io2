(define-module (crates-io po me pomelo) #:use-module (crates-io))

(define-public crate-pomelo-0.0.1 (c (n "pomelo") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.0.1") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "1gyxamjfgk5hq8dln3dy1pcqgx53hpmbbv8zqr4wd0g3ll6h89p5")))

(define-public crate-pomelo-0.1.0 (c (n "pomelo") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.1.0") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "1b3xidq9bzq841135g4r8i50hfp0h5q3avrb8mmrdn7nywmq0iqb")))

(define-public crate-pomelo-0.1.1 (c (n "pomelo") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.1.1") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "1wvi05zyn2n6nbvz6xa3ip5d5bqsiyvwzhgnkhfmvvx522c1ngpy")))

(define-public crate-pomelo-0.1.2 (c (n "pomelo") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.1.2") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "09jbic23sx7ggbjkkyd5x9iv0wwch4zavflv4qfg16ii3744x1vy") (f (quote (("doc_generated"))))))

(define-public crate-pomelo-0.1.3 (c (n "pomelo") (v "0.1.3") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.1.3") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 2)))) (h "0gb17jk795z202zrc616k4g4c06s4ch288j3xjcm3877q97yzn3k") (f (quote (("doc_generated"))))))

(define-public crate-pomelo-0.1.4 (c (n "pomelo") (v "0.1.4") (d (list (d (n "compiletest_rs") (r "^0.4") (f (quote ("stable"))) (d #t) (k 2)) (d (n "pm-lexer") (r "= 0.1.4") (d #t) (k 2)) (d (n "pomelo-impl") (r "= 0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1020ba49k140x4dkbrldx7z4261v2qx9q54k2w0i2c7ihjfw7lr3") (f (quote (("doc_generated"))))))

(define-public crate-pomelo-0.1.5 (c (n "pomelo") (v "0.1.5") (d (list (d (n "compiletest_rs") (r "^0.4") (f (quote ("stable"))) (d #t) (k 2)) (d (n "logos") (r "^0.11.4") (d #t) (k 2)) (d (n "pomelo-impl") (r "=0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1kd1sazixjajjl5d94yzavdaayrcncbgvdd9rb46ig6vcfgj9pbn") (f (quote (("doc_generated"))))))

