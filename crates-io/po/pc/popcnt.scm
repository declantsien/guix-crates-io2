(define-module (crates-io po pc popcnt) #:use-module (crates-io))

(define-public crate-popcnt-0.1.0 (c (n "popcnt") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "pulp") (r "^0.18.9") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mqmg6hzcyh9f5lfbn5x0xi7yyf7k94ap2xrmwnwgdix1y675x21") (f (quote (("std" "pulp/std") ("nightly" "pulp/nightly") ("default" "std"))))))

