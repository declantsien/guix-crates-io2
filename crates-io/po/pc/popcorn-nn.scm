(define-module (crates-io po pc popcorn-nn) #:use-module (crates-io))

(define-public crate-popcorn-nn-0.1.0 (c (n "popcorn-nn") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "popcorn") (r "^0.1") (d #t) (k 0)))) (h "1ivx7qi957nfadxyymmvvyfascvj9czh7l3bnapsg1gn1xa1jrln")))

