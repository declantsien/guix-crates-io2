(define-module (crates-io po pc popcorn_cli) #:use-module (crates-io))

(define-public crate-popcorn_cli-0.1.1 (c (n "popcorn_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0j0lnbks4nc1n07qkb80hv514ky48b6gx0p8156cd1nib635rzk6")))

(define-public crate-popcorn_cli-0.1.2 (c (n "popcorn_cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nipper") (r "^0.1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0shrhap7rggnxrllzbn19sgw035i0fq03xb7arwlrxpvmgbzj34g")))

