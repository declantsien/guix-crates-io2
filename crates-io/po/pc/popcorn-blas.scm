(define-module (crates-io po pc popcorn-blas) #:use-module (crates-io))

(define-public crate-popcorn-blas-0.1.0 (c (n "popcorn-blas") (v "0.1.0") (d (list (d (n "blas-sys") (r "^0.6.6") (k 0)) (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "popcorn") (r "^0.1") (d #t) (k 0)))) (h "1hyy9n0gcpxbhxvyhf7d37glc0drvxpsckaxhir992y6kah1ka0c")))

