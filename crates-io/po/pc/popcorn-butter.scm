(define-module (crates-io po pc popcorn-butter) #:use-module (crates-io))

(define-public crate-popcorn-butter-0.1.0 (c (n "popcorn-butter") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "popcorn") (r "^0.1") (d #t) (k 0)) (d (n "popcorn-blas") (r "^0.1") (d #t) (k 0)))) (h "08rw7bl9ik9pnisq6h77hhaiys7ggplcszpm5idm89v7wh768l4c")))

