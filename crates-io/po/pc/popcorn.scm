(define-module (crates-io po pc popcorn) #:use-module (crates-io))

(define-public crate-popcorn-0.1.0 (c (n "popcorn") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)))) (h "0p6w3lmv7qyh0dvjb5a5i7i3bhhpxsjbfcx16i4frsj33s6mvgra") (f (quote (("opencl") ("native") ("default" "native" "cuda" "opencl") ("cuda"))))))

