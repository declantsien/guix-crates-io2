(define-module (crates-io po rs porsmo) #:use-module (crates-io))

(define-public crate-porsmo-0.1.0 (c (n "porsmo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1vn6s1pcf3pab6m2l0aarsvsrbsv057czhmvbgzznyj7mawgm9qa")))

(define-public crate-porsmo-0.1.1 (c (n "porsmo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1nkff4b21nfn8iihv35qr86xs2p53hpwd6a24iwbbzyipci42m5p")))

(define-public crate-porsmo-0.1.2 (c (n "porsmo") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "061gi0a52yr9f7fysbrwkq6w3h8zjqvjvv9wi4b0zbafrk5v2gw7")))

(define-public crate-porsmo-0.1.3 (c (n "porsmo") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0h1dwl37mkk1qvrki371sl5s17dwfr86jpacyzi1rnq20pn5h4bb")))

(define-public crate-porsmo-0.2.0 (c (n "porsmo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)))) (h "0raq2c9cfcfa4js856sslzqd5knamd8q7xbmn13gww9q49in8yi4")))

(define-public crate-porsmo-0.2.2 (c (n "porsmo") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)))) (h "04rvf3k2c54zyaaqd85cx0w5rwjak205dx2ax867kc6jd6k6hqg5")))

(define-public crate-porsmo-0.2.3 (c (n "porsmo") (v "0.2.3") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0787bxxzg2hramf1kfhs2j89jagpjxs4j5d35ccc7kki5jrgymxq")))

(define-public crate-porsmo-0.2.4 (c (n "porsmo") (v "0.2.4") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "15ihswk1rs6mi1aa0dhm708nz7v0xayxbh0i3b3z78c92rps4fnf")))

(define-public crate-porsmo-0.2.5 (c (n "porsmo") (v "0.2.5") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0sm3kyl34bvdnjv257agffcvxw64si4ch3i987xn8vb8xz1g3gb3")))

(define-public crate-porsmo-0.3.0 (c (n "porsmo") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0aagyri66r18dhxdn8cbhnzrk43qdaw0ysxyxn58vrqrndbh53vb")))

(define-public crate-porsmo-0.3.1 (c (n "porsmo") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "08338aasqfqcvg3vj5dv9v3qh7ggc0d3452n2kdlaqi8h77n56jf")))

(define-public crate-porsmo-0.3.2 (c (n "porsmo") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1yap6q74dj2pcmg12myc06fnl2x29kjsma2f92dzz97n2al6x5m3")))

