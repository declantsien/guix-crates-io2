(define-module (crates-io po w2 pow2game) #:use-module (crates-io))

(define-public crate-pow2game-1.0.0 (c (n "pow2game") (v "1.0.0") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1yqcyv38w3rlbjx74mi5bn06msjxllxzl3zfhyrpsxy4dybr66ri")))

(define-public crate-pow2game-1.0.1 (c (n "pow2game") (v "1.0.1") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1jvy5d6djp8v4l1sihfk5v580rbanwp1xviw0g2i2x811jm8yh80")))

(define-public crate-pow2game-1.0.2 (c (n "pow2game") (v "1.0.2") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1xmh6isk0zhkq4q31h51ndpz1jmawrgdmp5pk57hjflw5rgzd90s")))

(define-public crate-pow2game-1.1.0 (c (n "pow2game") (v "1.1.0") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "006lp11qg3133gp1va9q00sc3d1zmzcyhkif6c4c4pd0av6ij2yj")))

(define-public crate-pow2game-1.1.1 (c (n "pow2game") (v "1.1.1") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0n6j1drjrpnjhk64p56s5rhh4n6rjrs73g5x86qi6v7aywzv8xb7")))

(define-public crate-pow2game-1.1.2 (c (n "pow2game") (v "1.1.2") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0y5mcx8d8rdm059cx94zfjy2xid1i3kxc3jjhb3sf445r84197dc")))

(define-public crate-pow2game-1.1.3 (c (n "pow2game") (v "1.1.3") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0066kp67cg8gz6q60jiz4l22dbjl26d9zqfh3nr57p9rv65ym8qa")))

(define-public crate-pow2game-1.1.4 (c (n "pow2game") (v "1.1.4") (d (list (d (n "noiserand") (r "^1.0.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0pljzdblyqsp5215ik949py1dgsdpi8rn64d8pgcraxwp1jdm5kx")))

