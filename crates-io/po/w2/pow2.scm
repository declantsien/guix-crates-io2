(define-module (crates-io po w2 pow2) #:use-module (crates-io))

(define-public crate-pow2-0.1.0 (c (n "pow2") (v "0.1.0") (d (list (d (n "zerocopy") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0gn74sqpzavdpc4gayp1aqkg82yjxb7fpvpvhl9g9ndpvk6wxjfm") (f (quote (("default"))))))

(define-public crate-pow2-0.1.1 (c (n "pow2") (v "0.1.1") (d (list (d (n "zerocopy") (r "^0.7.3") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.3") (d #t) (k 0)))) (h "124dax16yrl2qvxd3fhqqdkbxqfnhpcsvfaa4kpccflv7kyyzqls")))

