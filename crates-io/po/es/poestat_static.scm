(define-module (crates-io po es poestat_static) #:use-module (crates-io))

(define-public crate-poestat_static-0.1.0 (c (n "poestat_static") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 1)) (d (n "uneval_static") (r "^0.1.2") (d #t) (k 1)))) (h "03ic10psrv4w9z78g69q5h61wvpvrzprm6fzzgk10sn3h8h1c546")))

