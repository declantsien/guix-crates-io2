(define-module (crates-io po tp potpourri) #:use-module (crates-io))

(define-public crate-potpourri-0.0.1 (c (n "potpourri") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.15") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ractor") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1dy6ydpbvyv3wbcj3cjjsagnaig88dksx0rrvxwzdvca19i01yha") (s 2) (e (quote (("ractor" "dep:ractor") ("ndarray" "dep:ndarray" "dep:ndarray-rand")))) (r "1.65")))

