(define-module (crates-io po tp potpack) #:use-module (crates-io))

(define-public crate-potpack-0.1.0 (c (n "potpack") (v "0.1.0") (h "0cpfn5hpszfgmpvlq5hy5ackw1zcinakywl38g3665dxhmzc1nbw")))

(define-public crate-potpack-0.1.1 (c (n "potpack") (v "0.1.1") (h "0xji7if2i2wzwj403j3h8fd5i0adcany1ylxlsgl8x70176cliiy")))

(define-public crate-potpack-0.1.2 (c (n "potpack") (v "0.1.2") (h "0blwga0zwmb100l3r6s2bv3i6fjs92xyqi2qrc862ca7j73ydhc8")))

