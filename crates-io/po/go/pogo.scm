(define-module (crates-io po go pogo) #:use-module (crates-io))

(define-public crate-pogo-0.0.1 (c (n "pogo") (v "0.0.1") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "pogo_attr") (r "^0.0.1") (d #t) (k 0)))) (h "05c65k159523g2ldckrcv9h3xv36cs1n0cv2kz9f7ydk4ps9c6g0")))

