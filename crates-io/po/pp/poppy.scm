(define-module (crates-io po pp poppy) #:use-module (crates-io))

(define-public crate-poppy-0.0.1 (c (n "poppy") (v "0.0.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "opus-sys") (r "^0.1.0") (d #t) (k 2)))) (h "06qa3n0qglfn2m557a8wwa1684k8r46b0fws46h5i3hl188ibyig")))

