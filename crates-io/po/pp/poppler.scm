(define-module (crates-io po pp poppler) #:use-module (crates-io))

(define-public crate-poppler-0.2.0 (c (n "poppler") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1135vhp246dgjdfi3crjw70zdifss3nnrz84169vznhsyf4nwric")))

(define-public crate-poppler-0.2.1 (c (n "poppler") (v "0.2.1") (d (list (d (n "cairo-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1a7sv61w71wh183vlr84fa2ci90x6wb3y772insafy1vfb69c1vk")))

(define-public crate-poppler-0.2.3 (c (n "poppler") (v "0.2.3") (d (list (d (n "cairo-rs") (r "^0.3.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0yqcbvj5h6ywg4ibk1mz95zcwhjcdwsx38plkn5smsc3ags8dfqq")))

(define-public crate-poppler-0.2.4 (c (n "poppler") (v "0.2.4") (d (list (d (n "cairo-rs") (r "^0.3.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0yi1v98lmknyn2p9c6bnqngfyf1zlrkf2xwsr06vmllkwqs07vpi")))

(define-public crate-poppler-0.2.5 (c (n "poppler") (v "0.2.5") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("png" "pdf"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1209wh4mgs69a4ap3vzdngqy6crg77cx4kq91hgqsxsvg3gm1m5v")))

(define-public crate-poppler-0.3.0 (c (n "poppler") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.6.0") (f (quote ("png" "pdf"))) (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1rbhdxjqcfv5jhizp5b1mcfv9nfi96kd5hsxji93z4cmjz0s24c2")))

(define-public crate-poppler-0.3.1 (c (n "poppler") (v "0.3.1") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf"))) (d #t) (k 0)) (d (n "glib") (r "^0.14") (d #t) (k 0)))) (h "1bzdyvrvz93557i3qlil1g88b4ppjlq65sal0j6jvqzgfbfaz8fp")))

(define-public crate-poppler-0.3.2 (c (n "poppler") (v "0.3.2") (d (list (d (n "cairo-rs") (r "^0.15") (f (quote ("png" "pdf"))) (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)))) (h "03n2wy44mry3l612jngmaglllv0p21gdx90wh4vf9gwg2jsmykcf")))

(define-public crate-poppler-0.4.0 (c (n "poppler") (v "0.4.0") (d (list (d (n "cairo-rs") (r "^0.18.5") (f (quote ("png" "pdf"))) (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.18.5") (d #t) (k 0)))) (h "13fs66c8fwr2qz85j0jiqyzxfd6mhj912rwm56av4yd6mccbg035") (s 2) (e (quote (("render" "dep:cairo-rs"))))))

(define-public crate-poppler-0.5.0 (c (n "poppler") (v "0.5.0") (d (list (d (n "cairo-rs") (r "^0.18.5") (f (quote ("png" "pdf"))) (o #t) (d #t) (k 0)) (d (n "glib") (r "^0.18.5") (d #t) (k 0)))) (h "0pa8xs8clqafg8nyd4bf17f5rv3k7jwj2rj0n53hvyp22rl4ry5n") (s 2) (e (quote (("render" "dep:cairo-rs"))))))

