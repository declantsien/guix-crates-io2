(define-module (crates-io po lo polonius-parser) #:use-module (crates-io))

(define-public crate-polonius-parser-0.1.0 (c (n "polonius-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.15.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0my8i85nrxbc84306r2r9xnrm1js382z08mjida3yy8b3480r509")))

(define-public crate-polonius-parser-0.2.0 (c (n "polonius-parser") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.15.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1zlhnxhd8jk9mlrjhwm3m7k4p0jfra21ll2aw4w2lyy3z982724p")))

