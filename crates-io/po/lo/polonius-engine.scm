(define-module (crates-io po lo polonius-engine) #:use-module (crates-io))

(define-public crate-polonius-engine-0.1.0 (c (n "polonius-engine") (v "0.1.0") (h "1q4v1xhg65zrgg73a6p09dcyjscaxgxlazksyldyxylk9h201vx4")))

(define-public crate-polonius-engine-0.1.1 (c (n "polonius-engine") (v "0.1.1") (h "1vmpirjxqbb102rrpcrq2310snhy76djmv7vcp83p99xkvkzy0b2")))

(define-public crate-polonius-engine-0.2.0 (c (n "polonius-engine") (v "0.2.0") (d (list (d (n "datafrog") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1pfg0dacpzn4sw2bdsmnh35cyqyzlfhv346kfifp2qqgxp08dgl9")))

(define-public crate-polonius-engine-0.3.0 (c (n "polonius-engine") (v "0.3.0") (d (list (d (n "datafrog") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0ypnmwl20r7h4z8sax0qkfxyyd8jh5kyv71fgr8whczkz35mfypa")))

(define-public crate-polonius-engine-0.4.0 (c (n "polonius-engine") (v "0.4.0") (d (list (d (n "datafrog") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1r5rz27vqjs9fbwnpbz2qyfr5k49zdvnlg40ibjdfvzzz3cad1ck")))

(define-public crate-polonius-engine-0.5.0 (c (n "polonius-engine") (v "0.5.0") (d (list (d (n "datafrog") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "009jsh42y2pg6w14d5yj38917c013p7an57x3ycqn9zlynkv1dm5")))

(define-public crate-polonius-engine-0.6.0 (c (n "polonius-engine") (v "0.6.0") (d (list (d (n "datafrog") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1l1d19125p0ql0wvaavd1y53229hwz0mq7630hkcxs7s0a0c4k29")))

(define-public crate-polonius-engine-0.6.1 (c (n "polonius-engine") (v "0.6.1") (d (list (d (n "datafrog") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1zbh955iz21fcl6ghf1q387yszmnfl54ivqhx3vqgyxznwfll9yr")))

(define-public crate-polonius-engine-0.6.2 (c (n "polonius-engine") (v "0.6.2") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0ic1k9bz1v329ri5v1zhah1qkc06v2x5h5zripwan0aq12bc7414")))

(define-public crate-polonius-engine-0.7.0 (c (n "polonius-engine") (v "0.7.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1vdfn5bvzdvnxgxhbv63k5852zmvc8vlajj851ba87hlxqpr894b")))

(define-public crate-polonius-engine-0.8.0 (c (n "polonius-engine") (v "0.8.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1shkrfb8xc06yj8hby14vl54kqgmm3xv7kgr7cqimjibhgkk4ff2")))

(define-public crate-polonius-engine-0.9.0 (c (n "polonius-engine") (v "0.9.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0nrv9yz48gnwd8q4m5imxbn25ih1pigp9alsk6j9pvrazbgabf7n")))

(define-public crate-polonius-engine-0.10.0 (c (n "polonius-engine") (v "0.10.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1zmv4y97b6gxhdz1pcg4rbp37102jipzx31kvaa0bmnks2zrvyjh")))

(define-public crate-polonius-engine-0.11.0 (c (n "polonius-engine") (v "0.11.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "1v1w88zx0bjb0hz6v6vmlgpplpd55bqqvrfb2rj5qy7b71y8siqy")))

(define-public crate-polonius-engine-0.12.0 (c (n "polonius-engine") (v "0.12.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0k02y81jz69lz6y1hh29si976qdl244hxjx7kkncm7pqwdjyzn04")))

(define-public crate-polonius-engine-0.12.1 (c (n "polonius-engine") (v "0.12.1") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0bdminrprjb52kxnr19q9i3aabmnxcvjk2p0dsrqbqb4njj5h9gg")))

(define-public crate-polonius-engine-0.13.0 (c (n "polonius-engine") (v "0.13.0") (d (list (d (n "datafrog") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.0") (d #t) (k 0)))) (h "0gwh9vqjxj4gp3p30ka8837zbynnhb5lsrxns2bx6i906h2ybs64")))

