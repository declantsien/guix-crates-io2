(define-module (crates-io po lo polonium) #:use-module (crates-io))

(define-public crate-polonium-0.1.0 (c (n "polonium") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (k 0)))) (h "1dqmz2y1an5dv13f8h2jmb8yg0i0wzq0z01gw5iigik3h5las3x5")))

(define-public crate-polonium-0.2.0 (c (n "polonium") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (k 0)))) (h "177vp4qyjzd3b6jrrzf6h8m0sdkkrzfbgxk73sxa63xivka13n8b")))

