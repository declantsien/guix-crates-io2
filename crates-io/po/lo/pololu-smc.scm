(define-module (crates-io po lo pololu-smc) #:use-module (crates-io))

(define-public crate-pololu-smc-0.1.0 (c (n "pololu-smc") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0hh3dmngl1c3gjcqq1j191583g351pf35lhpkpn9kdq9f1p0d61x")))

(define-public crate-pololu-smc-0.2.0 (c (n "pololu-smc") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1bmxqf98z5ahqz9a6hscb470xkb875f4bkw77dpbkh53ncawqcc5")))

(define-public crate-pololu-smc-0.2.1 (c (n "pololu-smc") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "1n0lmhcm6d72av1wasm6vj5z72dw4knzy9ixwrnynivrxzfr71pj")))

