(define-module (crates-io po lo poloto-evcxr) #:use-module (crates-io))

(define-public crate-poloto-evcxr-0.1.0 (c (n "poloto-evcxr") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1km4h4ly9ny0mnd757mqc4hwmzxg6xwj5hxzn387mrd8aj4gb8v5") (y #t)))

(define-public crate-poloto-evcxr-0.1.1 (c (n "poloto-evcxr") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1l3594wvnyjbxbcm67kamgqav9dxsgwm688qg292gwxavfbl4rsz") (y #t)))

(define-public crate-poloto-evcxr-0.1.2 (c (n "poloto-evcxr") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0sya65zs44nssvba4ssx8m7a0g9psi3c6mi5aw1kznrfh9gixpnb") (y #t)))

(define-public crate-poloto-evcxr-0.1.3 (c (n "poloto-evcxr") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0zlr5rixc9ab9zxcgkz1cbhadnkc9wl1lhwrfrnc3pac1j84z8nw") (y #t)))

(define-public crate-poloto-evcxr-0.1.4 (c (n "poloto-evcxr") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0mrrlxbnxfsk4rrn2ny87a7mxpkvz8j6c4l5nn8prhbjwxmvdbhw") (y #t)))

(define-public crate-poloto-evcxr-0.1.5 (c (n "poloto-evcxr") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1wgpafrs2w2l6877b5fgx7jn0hk9sryzl6jc3a0kj6kp0dbqwgdm") (y #t)))

(define-public crate-poloto-evcxr-0.1.6 (c (n "poloto-evcxr") (v "0.1.6") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "08wvnmqm47a7jymc1wv4h7j6nxva30c2vzcfrr7fzz6yn3npzbzf") (y #t)))

(define-public crate-poloto-evcxr-0.1.7 (c (n "poloto-evcxr") (v "0.1.7") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "css-inline") (r "^0.8.5") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0v7kv7699yil81yp2628agjbw462df3xwf60fi666fw13w76li9s") (y #t)))

(define-public crate-poloto-evcxr-0.1.8 (c (n "poloto-evcxr") (v "0.1.8") (d (list (d (n "css-inline") (r "^0.8.5") (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1g2j8pwlnaxl7aaim5z39gpnd3rgg1yq2kkxjalmnvd8lb1dzv3i") (y #t)))

(define-public crate-poloto-evcxr-0.1.9 (c (n "poloto-evcxr") (v "0.1.9") (d (list (d (n "css-inline") (r "^0.8.5") (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0i19k5v2n969bv2bj0rpzhlpm0ig0ba3w2vj7cmvhd6lfcca07c3")))

(define-public crate-poloto-evcxr-0.1.10 (c (n "poloto-evcxr") (v "0.1.10") (d (list (d (n "css-inline") (r "^0.8.5") (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1yzyyzigjy9066a0vylh0skmlzydv1hk4h2iqdmxnhafpnk4rh0y")))

(define-public crate-poloto-evcxr-0.1.11 (c (n "poloto-evcxr") (v "0.1.11") (d (list (d (n "css-inline") (r "^0.8.5") (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1dvncyk00f9waysrxwvwwvvd83n3fliba51abhjilsscndiq5729") (y #t)))

(define-public crate-poloto-evcxr-0.1.12 (c (n "poloto-evcxr") (v "0.1.12") (d (list (d (n "css-inline") (r "^0.8.5") (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0nsmphx9pycd31nz6x9yl5b79bdy22ml4jqr0d43h8hbskgn0v9f")))

(define-public crate-poloto-evcxr-0.1.13 (c (n "poloto-evcxr") (v "0.1.13") (d (list (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1f95yrwvs20hf4ld7h8ck9y34inb3aqadjrfm5gpkzfi6mrj4x2c")))

(define-public crate-poloto-evcxr-0.1.14 (c (n "poloto-evcxr") (v "0.1.14") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "1fd36l2mnl34s35jp7nsvfs1xfm0zbj7z9p30gv8briqw0p6s4xj")))

(define-public crate-poloto-evcxr-0.1.15 (c (n "poloto-evcxr") (v "0.1.15") (d (list (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "19m8164zf542ln1cigf88dby3bg621ch61nsj3hbzv6mf9ksprf8")))

(define-public crate-poloto-evcxr-0.1.16 (c (n "poloto-evcxr") (v "0.1.16") (d (list (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0h2xmm1b9a4as9290y7db3pzwa3m5x8aaqxlffn6ishwsq3q64hl")))

(define-public crate-poloto-evcxr-0.1.17 (c (n "poloto-evcxr") (v "0.1.17") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0in8kvz91wfyzc7akabac77f4pgddc320wy20x9qr2pwgw1gz0im")))

(define-public crate-poloto-evcxr-0.1.18 (c (n "poloto-evcxr") (v "0.1.18") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0nmmfgcmwjdibgphf4rn3affl7z3d3hkw63fhx0m8mkpa5ndq3sl")))

(define-public crate-poloto-evcxr-0.1.19 (c (n "poloto-evcxr") (v "0.1.19") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0v91563jihdlrcw3faplw2pk6548j48qhl5bx1brz7w3lh7f8ndp")))

(define-public crate-poloto-evcxr-0.1.20 (c (n "poloto-evcxr") (v "0.1.20") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "0jrhxwp4d4q1cmp93iyyrvzhmp1b9dvjdz7sn0lyk7irab4mhbl2")))

(define-public crate-poloto-evcxr-0.1.21 (c (n "poloto-evcxr") (v "0.1.21") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0.6") (d #t) (k 0)))) (h "02hvq1vv6j8zhhn5a1xqjq2nprq4b5im0snlvfw4xam1gq306yvb")))

(define-public crate-poloto-evcxr-0.2.0 (c (n "poloto-evcxr") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "poloto") (r "^19.1") (d #t) (k 0)) (d (n "tagu") (r "^0.1") (d #t) (k 0)))) (h "023k1d0y5jk83hyjghldd5m7nhdq5qd7wr4ngxq4gf7glal8pmpr")))

