(define-module (crates-io po lo poloto) #:use-module (crates-io))

(define-public crate-poloto-0.1.0 (c (n "poloto") (v "0.1.0") (d (list (d (n "svg") (r "^0.8.2") (d #t) (k 0)))) (h "145r8qd8zcmwkw32428qj688ap4jjxm56grygc53g552blcj6rch")))

(define-public crate-poloto-0.2.0 (c (n "poloto") (v "0.2.0") (d (list (d (n "svg") (r "^0.8.2") (d #t) (k 0)))) (h "06apppva8rzjvp1prknx46xzyrarrsrna0czhy41ixn3ckmhryd8")))

(define-public crate-poloto-0.2.1 (c (n "poloto") (v "0.2.1") (d (list (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "0nf2f7q66xa2yipmmbwnhpmw8xdwgakxk1d1wbxkchd9xdvx7jfh")))

(define-public crate-poloto-0.2.2 (c (n "poloto") (v "0.2.2") (d (list (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "1hfajzriigkh62vndxd14f6bf6rksxxgwk9rcfab6ywia0nj3gbl")))

(define-public crate-poloto-0.2.3 (c (n "poloto") (v "0.2.3") (d (list (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "15wn59gpxgrc2pk4a4v7f1f4zlcmr9qjh3zbb6f5hfxvwlqnlhs3")))

(define-public crate-poloto-0.2.4 (c (n "poloto") (v "0.2.4") (d (list (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "0s2ggn9md08i9bkd034f79b61av4yghh2glg9syrm55k2kdqr87x")))

(define-public crate-poloto-0.3.0 (c (n "poloto") (v "0.3.0") (d (list (d (n "tagger") (r "^0.1") (d #t) (k 0)))) (h "15g4cggssab22ccv94ifzyx21m1yrkd2v5skriy7nd3xhw46qd79")))

(define-public crate-poloto-0.4.0 (c (n "poloto") (v "0.4.0") (d (list (d (n "tagger") (r "^0.2") (d #t) (k 0)))) (h "0k7ggm7c0ks5ibz2l1l076wm6gl6n908xyqxfyd4073z9bkm8hsp")))

(define-public crate-poloto-0.5.0 (c (n "poloto") (v "0.5.0") (d (list (d (n "tagger") (r "^0.3") (d #t) (k 0)))) (h "17b3s65nhdl9ghqrlf569bk7zaqmnwa1064w5hss5xpinvvp8r7m")))

(define-public crate-poloto-0.5.1 (c (n "poloto") (v "0.5.1") (d (list (d (n "tagger") (r "^0.3") (d #t) (k 0)))) (h "0fam04id0gg8zl1qbik53ad25w8g2118xsv7j9p2zhl3vdjs2fl9")))

(define-public crate-poloto-0.5.2 (c (n "poloto") (v "0.5.2") (d (list (d (n "tagger") (r "^0.3") (d #t) (k 0)))) (h "14csngpgbd74dgv5ydlwzxz1g45yahafxfn9k681iisarz6vj9dv")))

(define-public crate-poloto-0.6.0 (c (n "poloto") (v "0.6.0") (d (list (d (n "tagger") (r "^0.4") (d #t) (k 0)))) (h "1yzywprfr0j7l9va7lcz24sl6wqc2dk0wzqph887gisj257a61yc")))

(define-public crate-poloto-0.6.1 (c (n "poloto") (v "0.6.1") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "0g93a52h8n90vgwc965lij9q35l27jr6rai6vfbwsmj8xswwpvh1")))

(define-public crate-poloto-0.6.2 (c (n "poloto") (v "0.6.2") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "1k4a4srfx9p56g5r6bsmc89nj7413wwzsm0vils343l54m3bw3jv")))

(define-public crate-poloto-0.7.0 (c (n "poloto") (v "0.7.0") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "1fn90xrwkcddckqbgib28zd0j5sbx2501dv18cdqxqb74aaasn2i")))

(define-public crate-poloto-0.8.0 (c (n "poloto") (v "0.8.0") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "18s9y6zxzan66s67nfca698blhcfjyxk29hry7zjpf3sca4kwjkq")))

(define-public crate-poloto-0.9.0 (c (n "poloto") (v "0.9.0") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "0fd00n22236mk2ndm8396bw55jz7lqpswa995ys8qgvvdgycjdgm")))

(define-public crate-poloto-0.9.1 (c (n "poloto") (v "0.9.1") (d (list (d (n "tagger") (r "^0.4.2") (d #t) (k 0)))) (h "13lf2hg5xycr2jjdh3qd0wb00wzhfa5524sjiar4baqf7xxwa6kp")))

(define-public crate-poloto-0.10.0 (c (n "poloto") (v "0.10.0") (d (list (d (n "tagger") (r "^0.6") (d #t) (k 0)))) (h "0mk1yk8prrlhg1il98vcivgxaky1s1k2y815bk31y52mrglxwmsw")))

(define-public crate-poloto-0.11.0 (c (n "poloto") (v "0.11.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1d6hwch45vibsncimc3zqyl7dj6dd0cmzd1v3rfc6snf0rlyacfx")))

(define-public crate-poloto-0.12.0 (c (n "poloto") (v "0.12.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0pwdk4d16f987z1x8k92w30sw85lmya40g45j998xjkwf14grds8")))

(define-public crate-poloto-0.12.1 (c (n "poloto") (v "0.12.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1a6jgggaxcxjdwh2sgkr92yv71496x5r7nk221gjs4dvf71x4rsl")))

(define-public crate-poloto-1.0.0 (c (n "poloto") (v "1.0.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0wxnah8jilx0ck3nwkn6h0hgxp9wf6ppznp3si1jqk34bvhywj0l")))

(define-public crate-poloto-1.0.1 (c (n "poloto") (v "1.0.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "02d665l2mjvjvm8kvv0wv9z81bai2xbb0z8yq3557zbxg69ycnv7")))

(define-public crate-poloto-1.0.2 (c (n "poloto") (v "1.0.2") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0h6lhwbhkm35r5pmvkdnz50kbgi5fkv641lvpyvhfc4lbkwd34gh")))

(define-public crate-poloto-1.1.0 (c (n "poloto") (v "1.1.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1dxhm016rbn5jyizp9g377hhgnj0fkzy0r6ca23sm5r9d2z56wi6") (y #t)))

(define-public crate-poloto-1.1.1 (c (n "poloto") (v "1.1.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1pqzrw0darxms140sszixglvh76h0qb8l1inch6d2wwsbnbhh69c") (y #t)))

(define-public crate-poloto-1.2.0 (c (n "poloto") (v "1.2.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0q0i3z9s79vcz6yh9kgw8vssadm3dgdbgmlakgvhcax7n49p76ps")))

(define-public crate-poloto-1.2.1 (c (n "poloto") (v "1.2.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0jazgxachwdri2q33l0vx16fxabpdn56j4c365qldj271zdplxks")))

(define-public crate-poloto-1.3.0 (c (n "poloto") (v "1.3.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0rpyvcy481s31lmhjxb0qz6kkxy2p9ac5dc3kxx0v48d0d2y42zb")))

(define-public crate-poloto-1.4.0 (c (n "poloto") (v "1.4.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1b9i25q3lah0g3qgdagwchp17lwny6spjf6a7b06jqir7nqm8fs8")))

(define-public crate-poloto-1.5.1 (c (n "poloto") (v "1.5.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0b1rdkmp2rmyq6xvgdwvii1pkx3m062rqw183xnkcjk5aqscb8f8")))

(define-public crate-poloto-1.5.2 (c (n "poloto") (v "1.5.2") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1lwpklf3nrz3ix67jvxl6qb4kysyzr0aypa8qyq5xfn64p49bkpn")))

(define-public crate-poloto-1.5.3 (c (n "poloto") (v "1.5.3") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1rifgqf9gdwrjdfzxn6y1hb1aaqyc8dm5mkhcf6nzrqb2zsazzpv")))

(define-public crate-poloto-1.5.4 (c (n "poloto") (v "1.5.4") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "1rzfly4pqrkcf9rijg12727a53m24p3bxikzvlwlrvqzdajpsip2")))

(define-public crate-poloto-1.5.5 (c (n "poloto") (v "1.5.5") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "03xk3s76d2444x0jcv8iv7b6mglz19r3zjwl1wc89g0v7az5l7fa")))

(define-public crate-poloto-1.5.6 (c (n "poloto") (v "1.5.6") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "01blh1h50d1mhzp852dy23h05m6vdh0p56qxsqj6vnljl0ax5xxa")))

(define-public crate-poloto-1.6.0 (c (n "poloto") (v "1.6.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0s0zpnyg40hwm8k7sbyknndgak6nr9lvxig36a5qhkr454yzwmr8")))

(define-public crate-poloto-1.6.1 (c (n "poloto") (v "1.6.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "011dd6wrv4kya9nfdbqam530rk0c1j9azal7shzawqjaw3y0csx8")))

(define-public crate-poloto-1.7.0 (c (n "poloto") (v "1.7.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "021c67d3p1cnigdz16d7ciznqdvkm7b2w20ll3y5hbsl6ij3hfha")))

(define-public crate-poloto-1.8.0 (c (n "poloto") (v "1.8.0") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "086ilbknfjskf1y3y9vq7yvmlava7mchndq3z86b5appf9vf2rzx")))

(define-public crate-poloto-1.8.1 (c (n "poloto") (v "1.8.1") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "0h97ldvxvax8h5ksnpzhlv7p7c4hql63868767imy3l1ck77dpgk")))

(define-public crate-poloto-1.8.2 (c (n "poloto") (v "1.8.2") (d (list (d (n "tagger") (r "^0.7") (d #t) (k 0)))) (h "180hgp3lvd2h0nx4jkh1qms2fw6vp8n1pf9p9rjpkqk3mg3xhf4k")))

(define-public crate-poloto-1.8.3 (c (n "poloto") (v "1.8.3") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1rma1hyzighkqmqwqsy20y5cb695q0akqa9c38jxrbkwsa24ngah")))

(define-public crate-poloto-2.0.0 (c (n "poloto") (v "2.0.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0i0l1489j4w36830acla7b9gyrhwmavk0ry4zszhysg1crcrb1za") (y #t)))

(define-public crate-poloto-2.0.1 (c (n "poloto") (v "2.0.1") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1vcjfafbsbjabpqgd5rlqskcfgkkcxc54c2zhzfj7mdx7nr97rn5") (y #t)))

(define-public crate-poloto-2.1.0 (c (n "poloto") (v "2.1.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1vxjmwaijgl5m2yifz2fc593qnrshdk4hscvjmgrsk2nlybarbfg")))

(define-public crate-poloto-2.2.0 (c (n "poloto") (v "2.2.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1b6q1y4yz7j50k6dmkx5hhi1ys0pzbpwck16xd4c9ji5ik3qf51b")))

(define-public crate-poloto-2.2.1 (c (n "poloto") (v "2.2.1") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "128bpqabr0r77gdi7dd1dn5v6ks0ngbg2gyifry1j7dp4x67l80d")))

(define-public crate-poloto-2.2.2 (c (n "poloto") (v "2.2.2") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0hwz3zmyqasmgz1rzipidwlyd1d3jxximgpjc8gf15n2vpq33lg3")))

(define-public crate-poloto-2.2.3 (c (n "poloto") (v "2.2.3") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0ca5xr69yjxq99hflp4dwqnxy6qbdr4hd38jm4q5xnli3l58ri9i")))

(define-public crate-poloto-2.2.4 (c (n "poloto") (v "2.2.4") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1skrai2bhqi7xgkajm6jnmj9ync1dp2bi5rxs7g8ajiziyqmk5sz")))

(define-public crate-poloto-2.3.0 (c (n "poloto") (v "2.3.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "097wal3jc9b2zj4pw64bj36n7p2nphkh37ddiilh93nd7hin6ad1")))

(define-public crate-poloto-2.3.1 (c (n "poloto") (v "2.3.1") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1h9xy2120cafb6na6aq2d8dgdj8pgn2hg0y0mlcpqiibibpvw3la")))

(define-public crate-poloto-2.4.0 (c (n "poloto") (v "2.4.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1y3qf3ml9ixm9219bvylfns5apz71nki03wczrirmq86x2lvm9db")))

(define-public crate-poloto-2.5.0 (c (n "poloto") (v "2.5.0") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1ag87j6wr4hqn8cjc9w01qn7g6w78jhizbczk72fq9m89hm9is1c") (y #t)))

(define-public crate-poloto-2.5.1 (c (n "poloto") (v "2.5.1") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1v4rwgyg3qwn8d5nr3s4z8m6ywqj3rcrdq1n5ycyvm499pqkrdn6") (y #t)))

(define-public crate-poloto-2.5.2 (c (n "poloto") (v "2.5.2") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1d4wwpai9s093vhvccn1c5ba7nlm1hhiynk6z1nnxv46cwgivf16")))

(define-public crate-poloto-2.5.3 (c (n "poloto") (v "2.5.3") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0pwf7n7bsi4ipqmfsxag80sr6cjk37bnnw7n3lix3ia160shin5z")))

(define-public crate-poloto-2.5.4 (c (n "poloto") (v "2.5.4") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "12rqpki7c3r6r7vzfmdwndy6359d50mjxjw4ncpwa6vq0p65a6d2")))

(define-public crate-poloto-2.5.5 (c (n "poloto") (v "2.5.5") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0qmaj7d2b387c0pxssd46zsm6dnbxxcl98x6qpqyz03j24a7zw4p")))

(define-public crate-poloto-2.5.7 (c (n "poloto") (v "2.5.7") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1ki9379nnk22g25iwfh62pvn2s2s3xpk6y7ch6vaj8wphnq34lqr")))

(define-public crate-poloto-2.5.8 (c (n "poloto") (v "2.5.8") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0jjxikaw6xvszbfp1v1dawbr3h1l1dqnv25vmjil08dbyrknx8x9")))

(define-public crate-poloto-2.6.0 (c (n "poloto") (v "2.6.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1hib595cq47p8dyzyij1zc536amycdbsqj8inrpiw0qf3ggp8w7f")))

(define-public crate-poloto-2.6.1 (c (n "poloto") (v "2.6.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "0kxc9kpfqxyx0ryzc5c1zrln6cgc0fay9rlkx6frqwqjymyccyga")))

(define-public crate-poloto-2.6.2 (c (n "poloto") (v "2.6.2") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "16aw7w00cf0w4r2bhcbgxsw0zjvpynjd2iwjifqcknj76y95ajm9")))

(define-public crate-poloto-2.6.3 (c (n "poloto") (v "2.6.3") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1632mvb9icrnlj55l8kwigpqh99z0vax0fpn35xnw3s7jmv0fhrr")))

(define-public crate-poloto-2.6.4 (c (n "poloto") (v "2.6.4") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "1m8jaf3l6pi3wcy9qmyqwzsrm1jfpg2cay1gs0hjdcxlm4pqxhnr")))

(define-public crate-poloto-2.6.5 (c (n "poloto") (v "2.6.5") (d (list (d (n "tagger") (r "^1.0") (d #t) (k 0)))) (h "00y40admc90f985b4r2y8f9bbfrc4afm2r0075fkijz0px9s4l73")))

(define-public crate-poloto-2.7.0 (c (n "poloto") (v "2.7.0") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "194q9ncz7i98fj4cpr5b17nfz2fdmd524bn2gj4ns871h3rydbl2") (y #t)))

(define-public crate-poloto-2.8.0 (c (n "poloto") (v "2.8.0") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0634xyi07mdxliycdjk2kan8822jrdfif832v6ci2lgalcxydmkq") (y #t)))

(define-public crate-poloto-2.8.1 (c (n "poloto") (v "2.8.1") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "11cqifrdv94qp8hbqw4cjx7m6dxvly4q8p26bd57nsyfb0qcl1x1") (y #t)))

(define-public crate-poloto-2.8.2 (c (n "poloto") (v "2.8.2") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0385b7q0h5rjk3a75rgf0nn9vdrrkazgnwrfrcq25wrz0js38j46")))

(define-public crate-poloto-2.8.3 (c (n "poloto") (v "2.8.3") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0yh1gq8k9hajrbrb2h5hybxaw8kr5mhff5ynp6yzxyp912dl9jfd")))

(define-public crate-poloto-2.8.4 (c (n "poloto") (v "2.8.4") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "14y11rz3pzjjh221hm0bq2l82hdi58gpavahymcpmwbk7la7arsj")))

(define-public crate-poloto-2.8.5 (c (n "poloto") (v "2.8.5") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "1lljpxnccsnpfdjj4x8hhpp2fca58cqympz3jfc1gjklk0x2gv40")))

(define-public crate-poloto-2.9.0 (c (n "poloto") (v "2.9.0") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0f95j5a0rsf4bw2m1va3ryawfp7iwsjnnndym23rg5x6hfq79qp7") (y #t)))

(define-public crate-poloto-2.9.1 (c (n "poloto") (v "2.9.1") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0501p2giiib5pkqlgbd5sw6gbdmkphppi43ff93f5xhn1v88izm6")))

(define-public crate-poloto-2.9.2 (c (n "poloto") (v "2.9.2") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "1f0s7phy90szmqzh64nmm8h9vsz1bi4k08zvy76ql8w43fjffyxx")))

(define-public crate-poloto-2.9.3 (c (n "poloto") (v "2.9.3") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0agfjgr5cj2r5v4i85vry1cmzrdd364d5mrqf6rd6kh2qj857xld")))

(define-public crate-poloto-2.9.4 (c (n "poloto") (v "2.9.4") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "03qiw75bk6b043dfgg64nzchr5amk5lxpj3wlnwj6qbis1nlwznw")))

(define-public crate-poloto-2.9.5 (c (n "poloto") (v "2.9.5") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "070zhxdnzcx21nnsvlncslmm757jjri89afsawfi8s8v1pbjqdvv")))

(define-public crate-poloto-2.9.6 (c (n "poloto") (v "2.9.6") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "115h7hf28hwcl9ib9m4assi52a5k9wynhji4cdmzafcqrmc5dcp1")))

(define-public crate-poloto-2.9.7 (c (n "poloto") (v "2.9.7") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0kawq3m1b3svpbil84l32qr8cv5y17b3lgrp5m79wpm2p22r11ya")))

(define-public crate-poloto-2.9.8 (c (n "poloto") (v "2.9.8") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0dracy6x4n34qdsw0akzd7x1avmijyddzxd8bc6imppjm6lv8dr6")))

(define-public crate-poloto-2.10.0 (c (n "poloto") (v "2.10.0") (d (list (d (n "tagger") (r "^2.1") (d #t) (k 0)))) (h "0w3c4bfpwcmkvyaymz5vj2y5jryrbxbzw1girgkxl0mb5s3mp8z9")))

(define-public crate-poloto-2.11.0 (c (n "poloto") (v "2.11.0") (d (list (d (n "tagger") (r "^2.2") (d #t) (k 0)))) (h "04hyx2qzvdk54xkq6mnbf36whf7j5jr6qbixck2ld1wdvn2sq53g")))

(define-public crate-poloto-2.12.0 (c (n "poloto") (v "2.12.0") (d (list (d (n "tagger") (r "^3.1") (d #t) (k 0)))) (h "15nlgqpxzkli9wmddhdv9w9qlbc1bixr7xm8rbsflb3p7yzn2923")))

(define-public crate-poloto-2.13.0 (c (n "poloto") (v "2.13.0") (d (list (d (n "tagger") (r "^3.1") (d #t) (k 0)))) (h "1h2sn2472l3sx9f5w96xffvfjvrppiivh6brxpr85di3nmi3ig7j")))

(define-public crate-poloto-2.14.0 (c (n "poloto") (v "2.14.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1b3pf0pwgac1fhlz2017p2zlkj5nhpd50dcg94aas52yfxsxp7ng")))

(define-public crate-poloto-2.15.0 (c (n "poloto") (v "2.15.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1w4jssh4y8rylrvsvs5nsmsv4x3b4n84i2w41hiib2f4lal8rczx")))

(define-public crate-poloto-2.16.0 (c (n "poloto") (v "2.16.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1900l2ziqr3cjkl4r9nvnr4gby86bcsssfsx1d542srp1bmg3g7s")))

(define-public crate-poloto-2.16.1 (c (n "poloto") (v "2.16.1") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0gcidcvfrpyknanhvmw2ck0snx2dc9m6rdx2sqwcjw2cja93xphw")))

(define-public crate-poloto-2.17.0 (c (n "poloto") (v "2.17.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1s96jxcvk1c7h1d06a4y9iisciyzrr5iazbgsdbb01vr8w8kbakc")))

(define-public crate-poloto-2.18.0 (c (n "poloto") (v "2.18.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0cmsyjyjmjy4ql17pgngkngrvfxgxxl6dhzjbxdkx9fbaazkpf5i") (y #t)))

(define-public crate-poloto-2.19.0 (c (n "poloto") (v "2.19.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0pnn0hj4d13n21wdc3wkzamblrcvn5y2880y0h8dnjf0cqggjg64")))

(define-public crate-poloto-2.20.0 (c (n "poloto") (v "2.20.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1prx1dnbjyh66gan9haqgrhkd6y1pzlc2wp34zlp33b0y0y74z4w")))

(define-public crate-poloto-2.20.1 (c (n "poloto") (v "2.20.1") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0drk2bnm0in09gdqjjq8nvcz610w6vyc0jbzmgav9l0s3m0gw2dh")))

(define-public crate-poloto-2.20.2 (c (n "poloto") (v "2.20.2") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1x9cx0xa9d721qk7xjq8w98wkh2jh93kqklhz54l0igafbk8sdry")))

(define-public crate-poloto-2.20.3 (c (n "poloto") (v "2.20.3") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1cf0mnabzlsjzvj4laa78v4nbi4rccrcwq18l9ikqx3rl8rgkgnm")))

(define-public crate-poloto-2.20.4 (c (n "poloto") (v "2.20.4") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0jicmk1cc20a1a0im6q2a9mr2kvrgwxdj9c7ja0h2axnn980vnih")))

(define-public crate-poloto-3.0.0 (c (n "poloto") (v "3.0.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1mxa56bw79nnihmv23z96y3gk0mz96p4pmc4rxfk4201m7rilmv2") (y #t)))

(define-public crate-poloto-3.0.1 (c (n "poloto") (v "3.0.1") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0h7yg33zlfnj13pa5q185izdqvz4hvp9bcjzz01jg18ij4irbass") (y #t)))

(define-public crate-poloto-3.0.2 (c (n "poloto") (v "3.0.2") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0dcis55rbwnc4lf6z5426k62q86hvcykxm8dnff98wnjldmhg58a") (y #t)))

(define-public crate-poloto-3.1.0 (c (n "poloto") (v "3.1.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1jscmx2qvx2pa9fhlhswqssv18nhlrkli21ik1b0adgiyhdgn82f") (y #t)))

(define-public crate-poloto-3.1.1 (c (n "poloto") (v "3.1.1") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "1vvp332i6m0g252gkxs29r7fsma7zlgkb5pshyrwj8x4d5s46v3b") (y #t)))

(define-public crate-poloto-3.2.0 (c (n "poloto") (v "3.2.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "0fjzfh2p87316c0rh5k8snvpmzb428a2ds53zkgkcpc5zni4p1c7") (y #t)))

(define-public crate-poloto-3.3.0 (c (n "poloto") (v "3.3.0") (d (list (d (n "tagger") (r "^3.2") (d #t) (k 0)))) (h "06a14flwia6ip0chxc0asqjhakc94rpxmmmapa2pkg9ikdbnj05a") (y #t)))

(define-public crate-poloto-3.4.0 (c (n "poloto") (v "3.4.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "07g4nnk2xrbr2pcx2hydsnz7j3f309rpb71qaxh8c28x7qn5ylps") (y #t)))

(define-public crate-poloto-3.5.0 (c (n "poloto") (v "3.5.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0n3p086b2zxsz15wzv04iyd052s5cvqgj664c6381z27v799k9z7") (y #t)))

(define-public crate-poloto-3.5.1 (c (n "poloto") (v "3.5.1") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "1lzxps3jb3szwgbw3l1675rirz2v4p6xcvqpnmzcyw04y2n659yj") (y #t)))

(define-public crate-poloto-3.6.0 (c (n "poloto") (v "3.6.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0cfnx9bxrraqf2iphyfc8hwvjzmpld053php46f50x7xs7xj7675")))

(define-public crate-poloto-3.6.1 (c (n "poloto") (v "3.6.1") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0fi8v84s5yv2v87xiwm388f8b11bg182ajwcwgqcsdvlsapb25n5")))

(define-public crate-poloto-3.6.2 (c (n "poloto") (v "3.6.2") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "1s4mycpvg1pq55xd8fq0gz0p23df7yv2v1ca1gvr2wg69q0kz5ap")))

(define-public crate-poloto-3.6.3 (c (n "poloto") (v "3.6.3") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0w8mqhi7x4jsi9plsxpmgxdg91yjfpzn952f5x3mpq7a2a5qg2qj")))

(define-public crate-poloto-3.6.4 (c (n "poloto") (v "3.6.4") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "01yr0iandcasy8a09h3icgzh6cjv50x3qh53pqpl9fp0qiiy9pky")))

(define-public crate-poloto-3.6.5 (c (n "poloto") (v "3.6.5") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0c60c913cc08c9yszlai1zis5mgjxdwklkanmskymplbpj31apjb")))

(define-public crate-poloto-3.7.0 (c (n "poloto") (v "3.7.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "04padbk3hqcir0bi126m1qyp1k3m472kh1pr40rh4lxyy7wj5xd9")))

(define-public crate-poloto-3.8.0 (c (n "poloto") (v "3.8.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "101n9c8pwic216p9sddffvzr64ra24jdgz9hymgaz6vfbzb3n6wf")))

(define-public crate-poloto-3.9.0 (c (n "poloto") (v "3.9.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "0vc9905wi0c2ak02435dvvvz19jbjma7jzkacwsfza9pas7wrliq")))

(define-public crate-poloto-3.10.0 (c (n "poloto") (v "3.10.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "05jqw2y0xinr66mc1y1b5cn041dvnmbnmijz9a9784dw0rwfx248")))

(define-public crate-poloto-3.11.0 (c (n "poloto") (v "3.11.0") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "02axk6ji0x4hz8xxk8n5byhm3k6yli0dcnhb5dlcgim5vkq8m3f1")))

(define-public crate-poloto-3.11.1 (c (n "poloto") (v "3.11.1") (d (list (d (n "tagger") (r "^3.3") (d #t) (k 0)))) (h "1xb5pph4i7j21i0q4mghr3606glb9l81inbgi6psm7w29rvamq84")))

(define-public crate-poloto-3.12.0 (c (n "poloto") (v "3.12.0") (d (list (d (n "tagger") (r "^4.0") (d #t) (k 0)))) (h "0ryi53bj7c4rcnymgvdrqd6spfgcl61vg2s47dk6hs4wv2r2kr94") (y #t)))

(define-public crate-poloto-3.13.0 (c (n "poloto") (v "3.13.0") (d (list (d (n "tagger") (r "^4.1") (d #t) (k 0)))) (h "1pj4rp1swc5rxyv97wrv5wj8alm5b4pqhd1yhxg37zvhc9nx3fbp") (y #t)))

(define-public crate-poloto-3.13.1 (c (n "poloto") (v "3.13.1") (d (list (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "10ijs727vr7fqgnd0gnxg222rc473n5vsi146iz2jav2q0l1qm52")))

(define-public crate-poloto-4.0.0 (c (n "poloto") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1xrbc4d9lkbgfzvpd4f3b0ahsc1byhs7kf51wn4jaicwr9k107x1") (y #t)))

(define-public crate-poloto-4.0.1 (c (n "poloto") (v "4.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1dm6dr34aldlqms2p8kc8sh9m3lrhigkmvz62xxjkswy3bg4710l") (y #t)))

(define-public crate-poloto-4.0.2 (c (n "poloto") (v "4.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1b6nyjrjh0sq9rsdj7l2q7h1mhgfqx8y1m4v2kkbigi6xyw3j453") (y #t)))

(define-public crate-poloto-4.0.3 (c (n "poloto") (v "4.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "05n6xwfv2j0hkb3xc7yp9z63mawn7qm5115jqrmizh3p0h8922d1") (y #t)))

(define-public crate-poloto-4.0.4 (c (n "poloto") (v "4.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0hkiiyknqbnmbqdlzhl9kacm33nisin197irh6m2rcnxfkf754fv") (y #t)))

(define-public crate-poloto-4.0.5 (c (n "poloto") (v "4.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "10ygs5hrs4javzd2igpr08v6ajgfzv856590ff1vdp89wf27ns9s") (y #t)))

(define-public crate-poloto-4.0.6 (c (n "poloto") (v "4.0.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "15zg0x6dlwjnfkp43d3khy7w30ckn21lc3c4wzd6pj6gbrhpds8w") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.0 (c (n "poloto") (v "4.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1kr72dhsqbdy4iqn2y7s0zmaq7c3s9d6cczn4ikx6xqf6yys7iv8") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.1 (c (n "poloto") (v "4.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "05x2y947han00d1khl41rv6ainkpchpsbjj294cq7ycwnxvfa1v0") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.2 (c (n "poloto") (v "4.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "09p2qnppvji8yissfch6y3vva0wwx1k7sm36278yw06bw4drkj1n") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.3 (c (n "poloto") (v "4.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1ra9rhx8f4mz7radm4vvx716ghwx6cym50c415nfcllknlw1jnvp") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.4 (c (n "poloto") (v "4.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0758lhas6hdh601w63k8aqfpl67a3gxfsk0dr9k9aj2clap3rvhq") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.5 (c (n "poloto") (v "4.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (o #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1bzk28m0jc1hdqw77ax73rd10316yi9a5pzrrl5qam066jjgi7gy") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.6 (c (n "poloto") (v "4.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (o #t) (k 0)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "125dqhyya6jj6a84jvmkyvdkabflm7nqd4087kq251wplvbbbhdd") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-4.1.7 (c (n "poloto") (v "4.1.7") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0jmfn3yrv8am528zsirzr8bf1gnkwagr8pmkfy363flh23wwf40v") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.2.0 (c (n "poloto") (v "4.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1c98wmvc5qqwmyak5liz3p25hgmsnlmd0ygcnrqc0c0qn00hj0qa") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.3.0 (c (n "poloto") (v "4.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "18iv2h91f6x25f2sggbh48rhnpaj93g6mkb4brwxn9nc2v1cylyc") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.3.1 (c (n "poloto") (v "4.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0d71ybn0iy7hx0x8vjj1wjd0f39gq0f3zsvqfc07190p93vl562y") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.4.0 (c (n "poloto") (v "4.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0s6j1p7f9fvr5z34qjsvd5x4ky3kydp6c3v3ygm92v8fdvnmvxz4") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.4.1 (c (n "poloto") (v "4.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "17zmjy67j1p6pc94spq9g284hjr6i0ifb8x68g3df612kd2sjacq") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.4.2 (c (n "poloto") (v "4.4.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1v6p94x3zkzwqpq4bd71lipcg9892kvhb2bxc9lv3g2n59xkcprc") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.4.3 (c (n "poloto") (v "4.4.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "12y1511k36f97rz8n911knl7qbkq05q9yanb2160c83d6gxrn45s") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-4.4.4 (c (n "poloto") (v "4.4.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "12i70p4yiahgbdym12l21x5mqhi9is0z3bdl71kjr2cymxzy9xww") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.0.0 (c (n "poloto") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0rkqm78vjdl2vvs11b69xhkjx4ka8w88abskmbngy3kv4plwpqwd") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.1.0 (c (n "poloto") (v "5.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1dklcmq412zgp3ywzv0rcxi50ajk99gqd6gvigd65ckygymrr0ic") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.2.0 (c (n "poloto") (v "5.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "13hpqxym0pggwgham7p5a5257b346rzs2465ird4z8fvjqylyl2f") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.3.0 (c (n "poloto") (v "5.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0xys4y1zpr1fksf95y5d5x7ng4bk5ib3yas7hkiv1w0sni6nlp11") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.4.0 (c (n "poloto") (v "5.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1qsg5zixwyma47pqii2cqfjl283l5ws7fylrvy3c545dfj7b9rkm") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.5.0 (c (n "poloto") (v "5.5.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0yamsm8gd2z0awwk4i2a5gf6cj2bm755hpjh56dhdrqj5d2p915r") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.6.1 (c (n "poloto") (v "5.6.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0fxn0wdl8jwizvxzrjbckvz3ncngn3l90axabvlhk6iryjjln364") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.6.2 (c (n "poloto") (v "5.6.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1570q9qd0kdh61107jr5aymqmh4rjv1fxwrqpdrvlfj7jz2k518w") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.7.0 (c (n "poloto") (v "5.7.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1q1nhpvd8z8l389w00vvl6yy3dsz36bh9bsglj3hc94ik9p077w2") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.8.0 (c (n "poloto") (v "5.8.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0wjqfhmi0b72yd3113r5zg6nsahmls7xlna731xag0v2x7f2zg61") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.9.0 (c (n "poloto") (v "5.9.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "174bvk00jia8arp7n8zmj99cn7p3sbpng400xjzcammr6hq0xnvn") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.11.0 (c (n "poloto") (v "5.11.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1iyvlmyd8dx9qkzx6ki88sfcgl6m940ijz4nf67svff5l4aj1xaa") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.12.0 (c (n "poloto") (v "5.12.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1cr4ak1mylz5fz3bcl62pfdwdpiwa48v121r7knh3wziwz63djrz") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-5.13.0 (c (n "poloto") (v "5.13.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0hl0smfnd1fdznpi8yizglilnrkhs2bd1yzadrb0zd8ms8if9dzd") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.13.1 (c (n "poloto") (v "5.13.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0hzn3jk1qkaidhmgz94pjzqa5962j3ga7qq85r15jc8ws4vm1wq2") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.14.1 (c (n "poloto") (v "5.14.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1knnihzk0542pg4bybjd15ngh9zkd1nqnxwa8db6my4s5ipsfnwq") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.15.1 (c (n "poloto") (v "5.15.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "034brf55wp02fwa03lfp5rhydjyhyll1nar2vkzj0qvi1ycia446") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.16.1 (c (n "poloto") (v "5.16.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "011xws7p1fv8mv8a78xxifc2c0w2liyykr1qvk12pfzazfvj6pgx") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.16.2 (c (n "poloto") (v "5.16.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1fvycsljf5x2vphj6d89y86bwm2s0w25yd029z86sjq28nf15z2g") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.17.0 (c (n "poloto") (v "5.17.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0hvw41lhwif6p1fqyr16hsqisjg6b0skmmqdv9v9ljnw3xa97dxx") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.18.0 (c (n "poloto") (v "5.18.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "14h5b6g7grvb6l641zn4253ckqqj4vm6p8ck0cqwd13bdcd96af6") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.18.1 (c (n "poloto") (v "5.18.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "02bnyzfwpi87cp59bp3mfsc8bbjcnbg5v21bdlx9ywpgbz1m4dc9") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.19.1 (c (n "poloto") (v "5.19.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0h6dfm0x5hldhjgswhric9in7ridsi7m253xxlwq1wy0v9almwi7") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.20.1 (c (n "poloto") (v "5.20.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1pyrfbsj46cmwa3yhypnmqki321wnzry347bk515cvrb01g57jxk") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.21.1 (c (n "poloto") (v "5.21.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0zqb2qzxg133b7mpdwcvyqpblbl29f1pbybxhpflb98d25kl8szv") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.21.2 (c (n "poloto") (v "5.21.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "02hwcvqm30rbgxahnnc31wqkfssmkic2sn8ljnq07a28y55hr9r6") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.21.3 (c (n "poloto") (v "5.21.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1lx1c0zxyk93kpcapsgpabr516jsndr3yr9050sm5mh8ycl7iqa2") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.22.0 (c (n "poloto") (v "5.22.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0sxrv1k64kqbyfgg1k0hzwfp6sq1sgd3i0wbmyhxz1sa4sz8pm7i") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.22.1 (c (n "poloto") (v "5.22.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "13p2w44nqialxmdgj5rzs89jqrdwn4977iz3bfds6mxd36m955b6") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.23.0 (c (n "poloto") (v "5.23.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1p5qsbcki1mrcfq5fyhrxrf8wd3mjc0ha0s11jahhlx90b31d8v1") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-5.23.1 (c (n "poloto") (v "5.23.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "066dznjkzaqr7jj408lw5ik1xgdfy6rk7fvix9dqhvffsr5v73s7") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-6.0.0 (c (n "poloto") (v "6.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1n34d12yazywmh7c9iq483j2h8f36hlp01jzf2vnz8h2fyhj8v0g") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-6.0.1 (c (n "poloto") (v "6.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "03yps36z3sjs6whfgpc9j9f34apm1ziqmz3b92180ldlfnyci9fs") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-6.0.2 (c (n "poloto") (v "6.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0qazip7szxij4ig26ld02xy8wsb6k44yzv0f14whabh2mqisj41s") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-6.0.3 (c (n "poloto") (v "6.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1lni2lk0zvq468lvn7qpazvq7pb92s3gi9ddnhszra4gv64giky7") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-6.0.4 (c (n "poloto") (v "6.0.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "13ypl23xycgk87h5bvzv6yfxffd67krp8ddpcylg6kamyyl5dfh7") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-6.1.0 (c (n "poloto") (v "6.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "00r7f50i5ysc4h78daw0jpm7h53x0vzr5ipk8g7ng0rl0dg0296d") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-7.0.0 (c (n "poloto") (v "7.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0n9zwigg2kslyffbxxvznk28qzx3c10zqkdfy30idc260dn72nm6") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-7.0.1 (c (n "poloto") (v "7.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "1isqhnkqcbn3yls9ng6iy7wb792nf0sacxrbcgs5xlkd56k09lr9") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-7.1.0 (c (n "poloto") (v "7.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.2") (d #t) (k 0)))) (h "0fwxr6q0qczgqni633lxblmy509vhx37m1ymgkgl93jq7ai2br5w") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-7.2.0 (c (n "poloto") (v "7.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0bjmh226gh6ankp47g7426aq4s7y3ymihwpd1lis8x2sb13xs59n") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-8.0.0 (c (n "poloto") (v "8.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1d6kwn5wvr7lci3v2vmbmycpsbqyd7kj0rji2kmxyjgcvxlrvkvp") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-8.0.1 (c (n "poloto") (v "8.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1w8kg9ihsfygr3hydbgh4d3mx0jim5972cyy09i4w2cr4pffn71n") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-8.0.2 (c (n "poloto") (v "8.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0w24ly39yg1lm0ap1p5kfdra4963f7fsbb3hd5arqkx5ffvbqlhx") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.0.0 (c (n "poloto") (v "9.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1l4wgds1l6jc42adsibbz41cp3zy0z0y4k56y647jk6ybjcma2jj") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.0.1 (c (n "poloto") (v "9.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "08cbma5b9sq3lkgmb22xgwcba0jpj1fx13kfv6yl4n7jk9zf1azj") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.0.2 (c (n "poloto") (v "9.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1p6ds1lqi5faniyrvybnn237bp2il68jxr6llc69y75g9rzsmrmq") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.0.3 (c (n "poloto") (v "9.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0mp6aj9v38fy2xjhr4cji9cmhf2wxx5s8867cv0gfrfl2vykcnyq") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.0.4 (c (n "poloto") (v "9.0.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0l7ip3ggzvbz124g4rrhddv16y3if1cpw3nwnxa170hvvj7fxvd6") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.0 (c (n "poloto") (v "9.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1x5pwn72max83jwbjh024447hwacfvmkbknv7b22r13wibvs4g4j") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.1 (c (n "poloto") (v "9.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0qq764kljfz4bz8m7a6h129zyl0719wa02rl6581bkkczx4p6472") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.2 (c (n "poloto") (v "9.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0478gpa11pvj6gkqw7i7qvmq0liyz5l8k4gbyvl2474ylgk9p7b5") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.3 (c (n "poloto") (v "9.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0cgvp5d08rpz1sgyzs50wgr3wbq6j08y5jxvc08y0z67xzwvlvrq") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.4 (c (n "poloto") (v "9.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "101ada0mq04vl81pzglxdrf5wqxkl2wjbp6wh1v49545knp82z79") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.1.5 (c (n "poloto") (v "9.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1j06cdjnz1jg0gaajps8qjj9h0smkjz7kgg1gzx8k1mm3544d6ax") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.1.6 (c (n "poloto") (v "9.1.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1gqj67533il18g9k6frafj1skvpd5iw96scvbkcan3vi371k4jlc") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.2.0 (c (n "poloto") (v "9.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1f8baidhrar8msd1lw7k91qlhp2j11dfz90zfiq9mh34w6v14bhx") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.2.1 (c (n "poloto") (v "9.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0qwkcyq06whc55n8icz29mrgqk6aynwl86979vk4j5kiy89snf7r") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.3.1 (c (n "poloto") (v "9.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0vrdvl34vvpwhjrcx3yvylk8417yjgy5wd3hxz01785snxfrq3sv") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.3.2 (c (n "poloto") (v "9.3.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1492gv7pm2ksp2h4mwq7b45p7bxhj0ibd8p169glyqm6q9mzj5wc") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.4.0 (c (n "poloto") (v "9.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0y1npqg3l4j894rh67jjj97313dpziacmffv8gxmf5fm9mz576mv") (f (quote (("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-9.4.1 (c (n "poloto") (v "9.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0zzccisgw5wxf21vgk6f3q86j36yfa5flsh7laj1y288xhw7mamb") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.4.2 (c (n "poloto") (v "9.4.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "088v4gwld0lzlm6d1c7djp68d6mm6mzb337pm3nb974qi44fq5cw") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-9.4.3 (c (n "poloto") (v "9.4.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "12rjsdpzzdn944276ki5svwxry4j4s2rggsny0j2csv67f8kvrhg") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.0 (c (n "poloto") (v "10.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "10hh5maxhw9a4vp6j5kvgi5sq6xkhlhhsmc2213brb9rrcr3brm7") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.1 (c (n "poloto") (v "10.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "19jjha6mg1q7l94gkfmqahd1xagjx65zl1b4k78fxbbx6i22imhs") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.2 (c (n "poloto") (v "10.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1q2qqkrs1c2i0nsyapkhgm0kbvmd13bylbyl3fzdfpv1fgqhgzxk") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.3 (c (n "poloto") (v "10.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1fh01s9spc76i59n20a1ji5vqk9vgrph63fahh2w93diims2yij4") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.4 (c (n "poloto") (v "10.0.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0r4a1dpq6kdlrkqy1q1yijd4sh3cv1095wa47zxc5j0lzp0dvp39") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.5 (c (n "poloto") (v "10.0.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1n05j4a0na5akwsac0wnlhry0dc626xylx06l5s22iil90sjw1p2") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.0.6 (c (n "poloto") (v "10.0.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0f9scmg642qzb2yrg856k2w83sn9xac0gvxgih8ny9f8jxli0kr4") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.1.1 (c (n "poloto") (v "10.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1lz0q6gipnyhk7yg5y3pskhcrs9rjd98gd3kqph11z5d4z7sl9ky") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.2.0 (c (n "poloto") (v "10.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "096xpha0k1ayx6i2vg4yd4sjiv0jzyg5h86nfkvnd4cbzcvwa0jy") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.2.1 (c (n "poloto") (v "10.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "163f0a43vcr8yz7z87zynnc4x47rwm7avg9ssdf5bl0kmpdman6x") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.2.2 (c (n "poloto") (v "10.2.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0cfdvz4rg7awyy8aq4k9rc2r6qhi0irz25p77zg3ckbcidgj5f8p") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.3.0 (c (n "poloto") (v "10.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1y60d2618qavb3f2gsiq69a566kfz7wykliq982bq48dkxplaswk") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.4.0 (c (n "poloto") (v "10.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0ql2abj23w1cymvz787v56gqjxvnrx0g0vz7l6y16swg1cpwxrp8") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.4.1 (c (n "poloto") (v "10.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1p0z9dwby804lrfgynhf9vz0f0xg3r3sjhj0ra0nr75hlcs3b85i") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.5.0 (c (n "poloto") (v "10.5.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1g37ap07v7iwj8r1wf3pk3680v9ng0bvwyqln95i68c6npq334vr") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.5.1 (c (n "poloto") (v "10.5.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0x0f73a94n0a5v416b3i7sl6ih084wyvnbqhlfpdr15nz27492zc") (f (quote (("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.5.2 (c (n "poloto") (v "10.5.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0lbjhfaj6fl08lgx18c3rxzkd1394yrwlgwkw5xz1l5pxvm0njad") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-10.6.0 (c (n "poloto") (v "10.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1zwa84pm9qys4gkny50hn3chp6b3p0l4ckhax6p470kpqyjiwfvz") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.0.0 (c (n "poloto") (v "11.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0qvj0n2wz27qbi2cc879dvqni9ipz3zhm6fkl53a3dbpcws0qbax") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.0.1 (c (n "poloto") (v "11.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "01621g6qjaw0akl38fx9px7vmgjkfjnrcihaqls069xx7ibfcm5q") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.0.2 (c (n "poloto") (v "11.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0pj09aj40lypjx2n759v65gqymr85k9f52zk00ksll0glxiakx05") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.1.0 (c (n "poloto") (v "11.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0prx9c3xj3b6y4s1la237w10h8c3rrfzw0hikmp61cx45fwwkfra") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.1 (c (n "poloto") (v "11.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1d7fmhq2jgy97bnz9535si3pblkq23ri9x41ii0q0hh8fvh7qbsz") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.2 (c (n "poloto") (v "11.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0z08fx15sfnd6zklvd3q1z3swgc1n6lyr061sgx1chgxadyk3d4w") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.3 (c (n "poloto") (v "11.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1823fzydqgaqp7flgjjhzg0l0lydxh33gfs9aj264l5ssd2j4ip5") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.4 (c (n "poloto") (v "11.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "183j3zl9ws1vd4f0a1zv35124qh3hxfw5zdj2jzbm57avnv725rb") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.5 (c (n "poloto") (v "11.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0jq4fzk0lyv47m67w9x1g1s460z10lm1b7fz20vkr80vrg10xg9p") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-11.1.6 (c (n "poloto") (v "11.1.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1mhwb4xdp17yykig2sym6nhr08xbargjx7n5kznwr4f6v9v3f67i") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.2.0 (c (n "poloto") (v "11.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0rzadcsxny3m0gbpvk6zix4m3y170ld2ivby3qiqhf4sfj9774xd") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.3.0 (c (n "poloto") (v "11.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1fv7nrhnqyb0xvkr8knkk69ivkkcnz5nbrbxhw2kx00hfjclppfs") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.3.1 (c (n "poloto") (v "11.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0f2q8inkl8pkq5szqj7gypnxjmdxiplbvr0x46ifkhbxgpskg2vz") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.3.2 (c (n "poloto") (v "11.3.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0k40wvsh2sgagy2jpa9zl27lnznnffafrri4ldrisn7g1l3g8arp") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-11.4.0 (c (n "poloto") (v "11.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "113c141q06kcc97zpzi5a35wk4l5phyh1rff7l8n00ffnhsgvvzl") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-12.0.0 (c (n "poloto") (v "12.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "078lx84lp2xzlbrwwcph3s9g67r4j94r98s8cw1508p3vr52ilrk") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-12.0.1 (c (n "poloto") (v "12.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0vak9f79jb3jkai8jwlbj36665cxpw0qhjp3b7bjnpxzy7kg7qwr") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-12.0.2 (c (n "poloto") (v "12.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "11qasv5g6ab8zcp2kgv22xw1c4nih46880qcp1ka1bhxp81zw0p7") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-13.0.0 (c (n "poloto") (v "13.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "15284x3vwjnk85v2cyq1k5qlkhq1vskbd8wvh90c2ip4c7sswd7k") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-13.0.1 (c (n "poloto") (v "13.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "01fcfa2wm0f3w6b7m15hna0bc980ad8faqip8yd6rabwz03w6v0r") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-14.0.0 (c (n "poloto") (v "14.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0wiln6wcca7a4gwsq6hacga9iny3xzf5c8x144654qg459p4xa7r") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-15.0.0 (c (n "poloto") (v "15.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0krkklkccc6d0ip2qsj9xpydl5lxw0nxf7ap0gvaifv1ygfndf4w") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-15.1.0 (c (n "poloto") (v "15.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "0k5vlp181ig23fp9y4jriv2wx9scmiwifjnlr2sa9m7lmiajnzy3") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-15.2.0 (c (n "poloto") (v "15.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "tagger") (r "^4.3") (d #t) (k 0)))) (h "1khk41ydqy58mqwz3anxnhfqi26jw6wb1mjnmqm06fwf2gsr4l8q") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-16.0.0 (c (n "poloto") (v "16.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.2") (d #t) (k 0)))) (h "1gr2mrq1r3siz5ddb71p3fawp2mdad4idmahrk6zy9b8nibs77bi") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.0 (c (n "poloto") (v "16.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "0j1ra8n7yl4pfpd1dc5mvkx2xx9v768wfn7kl7mkr34n4w6rljdg") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.1 (c (n "poloto") (v "16.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "0dq066bfv4k83a4m5nqn3c6jd6ikpv64hl2zbw75541y2pfliadj") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.2 (c (n "poloto") (v "16.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "038l6gw1lfx3z6yfv4l00smyswqvbnxm62xsixvbd1wrfl3gp5yx") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.3 (c (n "poloto") (v "16.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "06p1m3fsifzihjn693scr8zqyavly4csnq8v1wzpxxybxx71a7az") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.4 (c (n "poloto") (v "16.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "0azmix187xh3sa8xq3w2h00spbzbbqbp37lz722zd4zn3yk12jgy") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.5 (c (n "poloto") (v "16.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "1i4ky0dgx750pmifl164f3rh9mmbsbgs17px5pvbvp07rxkhcq89") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.1.6 (c (n "poloto") (v "16.1.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.3") (d #t) (k 0)))) (h "0w9wqhi45kn9wh5wfgpcivb16fs0l3w3c4z7bmqpi9q3fi4dcsj3") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-16.2.0 (c (n "poloto") (v "16.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "07mmsz31hdlzk5pmdapsbvvhyy1ngpapnrr44vr14yvj5s627dhs") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.1 (c (n "poloto") (v "16.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "15q062y3x946bfj403axm1pn73bx4d3kr1y7y6kd327fymlxbpy6") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.2 (c (n "poloto") (v "16.2.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "01gdmkdmdmk03frzpagn79lvdzfccyic02j6ax27csnlskdhs1ak") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.3 (c (n "poloto") (v "16.2.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0ai4w6zn3ijk2w69y3n59qcahqn8sf9hi3fxl9nv2jy7z9srxvh6") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.4 (c (n "poloto") (v "16.2.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "1g76nd2b2ll7c7cg71y0rxm0s6sml8q5vvk7s340g5f0mgjrkz2y") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.5 (c (n "poloto") (v "16.2.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "19qr51qkhgmf0h646kw2x8i1cx6djbgfpmjx4p2smraasgryylws") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-16.2.6 (c (n "poloto") (v "16.2.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "15q7klnbvn9jh2vfrb12cmwscch85dhvxpqw2c7lkxpam2day77z") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-17.0.0 (c (n "poloto") (v "17.0.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "1lzhp4bigngih50m94i3qnwrf4nz28lwcq4jjwxsz5cqqdgb3jk3") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.1 (c (n "poloto") (v "17.0.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0azrvz2z313fsb32295ablnlv0jjbxfdaqdhw8nr85spqyzk6srd") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.2 (c (n "poloto") (v "17.0.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "1525rk8qlz7fabw25lhsawcpgyzkc64i29l1ic09pcw8a4bfgiww") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.3 (c (n "poloto") (v "17.0.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0llhync2xnd4qmdxpjkp332fbrrfk1fgf0spwldz5giihmahxml0") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.4 (c (n "poloto") (v "17.0.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0dpqddmlbmj8jlcvhyy79kk4ld1qf4zbqrxjfbvh71xqbvyn86bv") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.5 (c (n "poloto") (v "17.0.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "163m8m1k1lk8fq71dq0hzh3gp2ry1bj3zwi512gw9cjlf9zjbmzg") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.6 (c (n "poloto") (v "17.0.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0nvgxawid8vcc0adawmz1rnrwdk7jg2r1k5iz1vlwpkf7axyr4j8") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.8 (c (n "poloto") (v "17.0.8") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0nfhylmw4bq0hdlk67m1lbcacsw2ch09zps26kdw16gwv06lddmi") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.9 (c (n "poloto") (v "17.0.9") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "04l9r86f0rabhds9zqzl2pcx148nrpic9mvwaf92dkgibcs1pzpi") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.10 (c (n "poloto") (v "17.0.10") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0kf3h5kffhwlwyksfx2yi0iabpl0vwv47v0ripm1gjjp2fh7r2s1") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.11 (c (n "poloto") (v "17.0.11") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "1h16gs6jfn504jv80xfhlrlsr229v6hh4jnglbgq2snrn80z8n3h") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.12 (c (n "poloto") (v "17.0.12") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "05dprfijch7zdra84vph0dq4pr0vs3h7lva59gq8ixx9iv74dqvr") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.13 (c (n "poloto") (v "17.0.13") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "1ylvy0i487vb4861k812s171ghvazdhfnbg97zlik209wc4rvac9") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.0.14 (c (n "poloto") (v "17.0.14") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0xaaj4g97r087i69v7i07n9v1ah6v88yfqyd1y52q31b66z4k9vw") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp")))) (y #t)))

(define-public crate-poloto-17.1.0 (c (n "poloto") (v "17.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.4") (d #t) (k 0)))) (h "0lxxmp9kfn3d8rm9j5dh72qqgg437d84v5hl3v5pxfvbl82z8kik") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-17.2.0 (c (n "poloto") (v "17.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "14lyqzmx4074nym8g65aw628iciwzsy2mg9lbw0lcm4bzmx0y0g4") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-17.2.1 (c (n "poloto") (v "17.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "0havpp9914qf687xvbfw37jcg19p59q3yfr664rsjvhxjrvwfzid") (f (quote (("timestamp_full" "chrono" "chrono/std") ("timestamp" "chrono") ("default" "timestamp"))))))

(define-public crate-poloto-18.0.0 (c (n "poloto") (v "18.0.0") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "0jqxjpyqvlsdg4f7j84hnr2divqwg022x64kp3wvg6w2irlhg8sa") (y #t)))

(define-public crate-poloto-18.0.1 (c (n "poloto") (v "18.0.1") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "1271400c0xbzpk6pdpampaql4dm01ira048lk0k9419w9hlqlyph") (y #t)))

(define-public crate-poloto-18.0.2 (c (n "poloto") (v "18.0.2") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "11dxzv71n126fqk4igmry6asv96sj0h10bri7m054hfj71mav2j4") (y #t)))

(define-public crate-poloto-18.0.3 (c (n "poloto") (v "18.0.3") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "0g8r9r2sd2n6h0ji3c82v7wdr30fb918syg3d10rg7irn2i048ml") (y #t)))

(define-public crate-poloto-18.0.4 (c (n "poloto") (v "18.0.4") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "0mvz0n4qzjbxjv2k3clrb56lia2d5wfpks7qn9frbh5kd4cqlxb7") (y #t)))

(define-public crate-poloto-18.0.5 (c (n "poloto") (v "18.0.5") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "1qjxghxpr7g5w208a3qmn0vkyi33p9hi1d1k1q8g55g31y6lim68")))

(define-public crate-poloto-18.0.6 (c (n "poloto") (v "18.0.6") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "13ncp45bjr91wlc7gsy6l974sdymqr29087i2gdyn0hkhlkdhxl3")))

(define-public crate-poloto-18.1.0 (c (n "poloto") (v "18.1.0") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "194271h3d5i6cj9zs159y8vy1z626yvq4dgva8j2hssyl89vypyj")))

(define-public crate-poloto-18.1.1 (c (n "poloto") (v "18.1.1") (d (list (d (n "hypermelon") (r "^0.5") (d #t) (k 0)))) (h "005v61zaqlx0gas8zpxzwxb1a4vscgxhyl608gg8ak2lv9x1w3la")))

(define-public crate-poloto-19.0.0 (c (n "poloto") (v "19.0.0") (d (list (d (n "hypermelon") (r "^0.6") (d #t) (k 0)))) (h "11pp9yiqalzq6c0kyh7m5x5wr0ppjbafjhavcng6krnlq6rca2ss") (y #t)))

(define-public crate-poloto-19.1.0 (c (n "poloto") (v "19.1.0") (d (list (d (n "tagu") (r "^0.1") (d #t) (k 0)))) (h "1bdcvl3xrygys1inmzhq8d8x7hvvzk2vaxbpw28rk56qwfivfr1k") (y #t)))

(define-public crate-poloto-19.1.1 (c (n "poloto") (v "19.1.1") (d (list (d (n "tagu") (r "^0.1") (d #t) (k 0)))) (h "049zz8aszjzpziif3pr6761f5bpdmg4xxmf89vygblbg44b6lwq1") (y #t)))

(define-public crate-poloto-19.1.2 (c (n "poloto") (v "19.1.2") (d (list (d (n "tagu") (r "^0.1") (d #t) (k 0)))) (h "15kbwkairzh24kxlk19zi9a5llcbx71fjlj4lcpyjclq3iabsk8n")))

