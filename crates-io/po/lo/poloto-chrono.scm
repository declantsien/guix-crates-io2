(define-module (crates-io po lo poloto-chrono) #:use-module (crates-io))

(define-public crate-poloto-chrono-0.1.0 (c (n "poloto-chrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "0k2bx5124zjcgafwmrvjbhzvgm9p80k3zv2nzabx7d7ik07s6f40") (y #t)))

(define-public crate-poloto-chrono-0.1.1 (c (n "poloto-chrono") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "1jjs2wmwxr4wkcdp5dms3vfwn7rmhlphnan4ja5rn4k0p66q0rav") (y #t)))

(define-public crate-poloto-chrono-0.2.0 (c (n "poloto-chrono") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "1hcvblkzs1gi61qv16aivrcr8wxa0y4si3n2zaq6ib6j6v6n5kzw") (y #t)))

(define-public crate-poloto-chrono-0.2.1 (c (n "poloto-chrono") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "12shd2kz2czayqmfiy7vrqp4vg4mxzk1qsx0d8dzmryhhlirds91") (y #t)))

(define-public crate-poloto-chrono-0.2.2 (c (n "poloto-chrono") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "06iy3zjvad01zn4344y7k5fm2px4pyr7ygn9kqrr48jwa87ivl1a") (y #t)))

(define-public crate-poloto-chrono-0.2.3 (c (n "poloto-chrono") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.5") (d #t) (k 0)) (d (n "poloto") (r "^18.0") (d #t) (k 0)))) (h "0127d84wfz56gsri8i17amiyh7mzpxxbf2x74x31d9qk2mrdqgpf")))

(define-public crate-poloto-chrono-0.3.0 (c (n "poloto-chrono") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hypermelon") (r "^0.6") (d #t) (k 0)) (d (n "poloto") (r "^19.0") (d #t) (k 0)))) (h "1c3kiriy7ghhjj0zda8gncbb26xvp84ysyzz8dwcxyqc9bj5d2ry")))

(define-public crate-poloto-chrono-0.4.0 (c (n "poloto-chrono") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "poloto") (r "^19.1") (d #t) (k 0)) (d (n "tagu") (r "^0.1") (d #t) (k 0)))) (h "0z13gf5qvc6ialkm4h1w4mnk1pyhyrdsj2c8lsl5xzgsnkdjdf3d")))

