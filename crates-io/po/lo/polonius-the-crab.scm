(define-module (crates-io po lo polonius-the-crab) #:use-module (crates-io))

(define-public crate-polonius-the-crab-0.1.0-rc1 (c (n "polonius-the-crab") (v "0.1.0-rc1") (h "00flp59j4xdcyf4n6hykcllyhdgzq54711bbg5yg3adisy46m8v0") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.0-rc2 (c (n "polonius-the-crab") (v "0.1.0-rc2") (h "16ixd6k1lnyc04qyv49i55aa02jiiydddfgpyyc8gwg8vha9cwsz") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.0-rc3 (c (n "polonius-the-crab") (v "0.1.0-rc3") (h "1mjwwq9rgzx175hmgmydca4bqpjsd7f4ln2hd81nvs575mfzmfha") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.0 (c (n "polonius-the-crab") (v "0.1.0") (h "0iz646s4axb3sbmhv2dia2ib1rpb68ba7qww3wx263gz2vsikfz3") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.1-rc1 (c (n "polonius-the-crab") (v "0.1.1-rc1") (h "17dlr5rgf54ngrmrvadp6pjv88qa9xy3afgla6md09z285iy8nnq") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.1 (c (n "polonius-the-crab") (v "0.1.1") (h "1yd0dh9qlssv3mhyxdibik20dxx6ykimrbazxjx6kklwy4zr5z58") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.2-rc1 (c (n "polonius-the-crab") (v "0.1.2-rc1") (h "1lyymbv538c5j1dnb66qmfrhicnjcnh2ljj85vhw3wsgsyvskhqh") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.2 (c (n "polonius-the-crab") (v "0.1.2") (h "0sl3laf303rind4kmis2dh7ilh2lamkgnz7g1s6jiwjxxsw1zgdw") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.1.3-rc1 (c (n "polonius-the-crab") (v "0.1.3-rc1") (h "025fbvsx1w2w6qcf1mk635vhi4vhg0qinb8z2a308a005f5lag4r") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.2.0 (c (n "polonius-the-crab") (v "0.2.0") (h "02rb79frxxhz03j575mbpam9w1kcs7w3l4phgkv3qcsjybsp0wmh") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.2.1 (c (n "polonius-the-crab") (v "0.2.1") (h "17rgijifijfqpsri5scrhyb5x5lcqgcf1wdbca22ya56jzlrx9n2") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.2.2-rc1 (c (n "polonius-the-crab") (v "0.2.2-rc1") (h "1cfm1a99mmvbza4sdkbzhmd984j9s5g6wzgxdhakiy7h4405xxc1") (f (quote (("ui-tests" "docs-rs") ("polonius") ("docs-rs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.3.0-rc1 (c (n "polonius-the-crab") (v "0.3.0-rc1") (h "1ympmrxm4xzzhw6wirzr9plq5na3knhiai2inpylv8ww3rg61sf5") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.3.0-rc2 (c (n "polonius-the-crab") (v "0.3.0-rc2") (h "1jf4xz30mi0652mgv5a4a6pihxirskadx6zy0a146ap2xvi9sj4c") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.3.0 (c (n "polonius-the-crab") (v "0.3.0") (h "108d7w7j4rhxjla4z96zzlczmzf5y2nally526cg0msa4nzy6kj3") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.3.1 (c (n "polonius-the-crab") (v "0.3.1") (h "09jl0z4v7pi0rfl6cfxz703qghdr18l9cqvr2wh3pjb0a3brl4dh") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.56.0")))

(define-public crate-polonius-the-crab-0.4.0-rc1 (c (n "polonius-the-crab") (v "0.4.0-rc1") (d (list (d (n "higher-kinded-types") (r "^0.1.1") (d #t) (k 0)))) (h "0rb3xn5h7b1glg7v2cnk0n39db5br7sbc1hrn8fxzbai2v0pm2bd") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.67.0")))

(define-public crate-polonius-the-crab-0.4.0 (c (n "polonius-the-crab") (v "0.4.0") (d (list (d (n "higher-kinded-types") (r "^0.1.1") (d #t) (k 0)))) (h "04xbrj0c0rmady3nac6v2xyb4sj9j3sibcmlss6424bihblv88h9") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.67.0")))

(define-public crate-polonius-the-crab-0.4.1 (c (n "polonius-the-crab") (v "0.4.1") (d (list (d (n "_never-say-never") (r "^6.6.666") (d #t) (k 0) (p "never-say-never")) (d (n "higher-kinded-types") (r "^0.1.1") (d #t) (k 0)))) (h "149ykbn06xprj08vnm4rirslc9y18lgcanvrs3y99hp28nxb1ncn") (f (quote (("ui-tests" "better-docs") ("polonius") ("better-docs")))) (r "1.67.0")))

