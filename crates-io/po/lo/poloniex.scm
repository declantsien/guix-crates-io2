(define-module (crates-io po lo poloniex) #:use-module (crates-io))

(define-public crate-poloniex-0.0.1-alpha (c (n "poloniex") (v "0.0.1-alpha") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1xb96a411gg8hzkd8lgyjgd3lzw559x86sz0zyzv90yhbbk0aq9r")))

