(define-module (crates-io po si posixmq) #:use-module (crates-io))

(define-public crate-posixmq-0.1.0 (c (n "posixmq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.35") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (o #t) (d #t) (k 0)))) (h "11xy4j2z0mm2vagfhgpg0mn8mi8sjw3787bpzrpz1fnsmzzhy8xa")))

(define-public crate-posixmq-0.1.1 (c (n "posixmq") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (o #t) (d #t) (k 0)))) (h "1lgchlv705icyirp14niqrzm99imv3i50gw5x606wd4gk5qij306")))

(define-public crate-posixmq-0.2.0 (c (n "posixmq") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.59") (d #t) (k 0)) (d (n "mio") (r "^0.6.14") (o #t) (d #t) (k 0)))) (h "1dy84hcdkyaakvrxffgj38c8b163brp3rgc2b71234l0zjy27gid")))

(define-public crate-posixmq-1.0.0 (c (n "posixmq") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.59") (d #t) (k 0)) (d (n "mio_06") (r "^0.6.14") (o #t) (d #t) (k 0) (p "mio")) (d (n "mio_07") (r "^0.7.0") (f (quote ("os-util"))) (o #t) (d #t) (k 0) (p "mio")))) (h "0yk91r8zw8s8fyg93f0w5haar6q1a1wzns320qiwic50qmh1iq75")))

