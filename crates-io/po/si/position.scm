(define-module (crates-io po si position) #:use-module (crates-io))

(define-public crate-position-0.0.0 (c (n "position") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "oi") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0aj79c4a9yzhr5azs1pcpikx9k97b1a7n65m9cb5mz4x94717mc5") (f (quote (("location" "oi" "failure") ("default"))))))

(define-public crate-position-0.0.1 (c (n "position") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "oi") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0p3ah92m0rq8k8wrdqkd3266njbn8acdr5dngqvhp9f6f1camfam") (f (quote (("location" "oi" "failure") ("default"))))))

(define-public crate-position-0.0.2 (c (n "position") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "oi") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0px24p4wbhqispgl8sqg589zqg16yn40c3jdm97wm59xjw1lq5cl") (f (quote (("location" "oi" "failure") ("default"))))))

(define-public crate-position-0.0.3 (c (n "position") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "oi") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "16mrkvp9g7pfakmbgafxq24scsbyqcgvvjfk1nn9cds4w3pzkvj6") (f (quote (("location" "oi" "failure") ("default" "location"))))))

