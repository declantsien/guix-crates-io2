(define-module (crates-io po si positron) #:use-module (crates-io))

(define-public crate-positron-0.1.0 (c (n "positron") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0sdxisi97b1vksilxp80mxs1kg2dnzrxhdyfp18sqjl95xfa69x1")))

(define-public crate-positron-0.2.0 (c (n "positron") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1kjzrq9p2f0ai6g0zna9bwskqqhlyc5bwyflxv8fs54madz8ycll")))

(define-public crate-positron-0.2.1 (c (n "positron") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1md95qh4xdiypk0x283k9rfnl1m6zhl8czc4za3f8vjfzw8m3k01") (f (quote (("json" "serde"))))))

(define-public crate-positron-0.2.2 (c (n "positron") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g0arv59bdqayh0rb2w2q456gvhzd4mp4aaphpvz3m2mic8wsk0f") (f (quote (("json" "serde"))))))

(define-public crate-positron-0.2.3 (c (n "positron") (v "0.2.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rk13ywn0h4nfplp1v1ywiqf79xs7aimvk97xp6b640ag114krwv") (f (quote (("json" "serde"))))))

(define-public crate-positron-0.2.4 (c (n "positron") (v "0.2.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xbgr0x5l284cqdc9mb1zr0bmgx7qgc1iyqmm8b5dr3ll8ki3djr") (f (quote (("json" "serde"))))))

