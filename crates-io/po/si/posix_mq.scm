(define-module (crates-io po si posix_mq) #:use-module (crates-io))

(define-public crate-posix_mq-0.1.0 (c (n "posix_mq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "12bvkyx03vjn2a10i27jz2n3gd1dbf1zpaydl5j63w84gamrza5p")))

(define-public crate-posix_mq-0.1.1 (c (n "posix_mq") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1xa9fmfy16k1z16hlgqiar0wqirpj7mqjvr570363am5rnx1vd9a")))

(define-public crate-posix_mq-0.1.2 (c (n "posix_mq") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "0gzha49kaa6vzm7p6w0lr52csmlxkvzlgpybvqjf5azgls2lx6if")))

(define-public crate-posix_mq-0.1.3 (c (n "posix_mq") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9") (d #t) (k 0)))) (h "1wfpz16fcii0bsj61wz47bp41hjc01kj4rclkzzwxcd45r8yvgc2")))

(define-public crate-posix_mq-0.9.0 (c (n "posix_mq") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)))) (h "0gspd0rbz9zn6fx3cpf5d4whjwv4nyvam9cp8lm915nc2fg37bhk")))

(define-public crate-posix_mq-3771.0.0 (c (n "posix_mq") (v "3771.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1a4x0zfnnawckijahrzbb8zih979aqcjg73dywz3z8cym5wssqpl")))

