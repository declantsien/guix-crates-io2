(define-module (crates-io po si posix-ipc) #:use-module (crates-io))

(define-public crate-posix-ipc-0.0.1 (c (n "posix-ipc") (v "0.0.1") (h "1in2j7g4rjmkf8k4ma4yjkkn36g6895a1948y7ysxacnyp1kxy05")))

(define-public crate-posix-ipc-0.0.2 (c (n "posix-ipc") (v "0.0.2") (h "01v8bgmlz4fqrxvsp1lsg65gl8pnja478iscy1mz4cqd3wz662bb")))

(define-public crate-posix-ipc-0.0.3 (c (n "posix-ipc") (v "0.0.3") (h "195h32p88w8rp84qf5iamsi0n6cmda8s9l9h7x0wl7cxnlzxyli7")))

