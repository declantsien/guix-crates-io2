(define-module (crates-io po si posix-regex) #:use-module (crates-io))

(define-public crate-posix-regex-0.1.0 (c (n "posix-regex") (v "0.1.0") (h "1cqdmk303n9bn8yp2nnvan1l2bxyfz0n6v904856qb02ynj1rcsq") (f (quote (("no_std") ("bench"))))))

(define-public crate-posix-regex-0.1.1 (c (n "posix-regex") (v "0.1.1") (h "0hfkrlrfj10i7a54p3fg4c8ww01dy0v7l3lvmwc9pfliprf6dy0d") (f (quote (("no_std") ("bench"))))))

