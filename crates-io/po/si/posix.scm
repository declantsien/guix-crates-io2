(define-module (crates-io po si posix) #:use-module (crates-io))

(define-public crate-posix-0.0.1 (c (n "posix") (v "0.0.1") (h "0y9wk6mad2pibc4w72m0irpcyzknmmy9j06bs2iqlqnsi9inv5qh")))

(define-public crate-posix-0.0.2 (c (n "posix") (v "0.0.2") (h "0f7xzr8vw0lpwh69bvv8rpf9qkm1lg3g59aa0avycai1l9y58zsp")))

