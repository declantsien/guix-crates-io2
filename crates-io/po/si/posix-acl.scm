(define-module (crates-io po si posix-acl) #:use-module (crates-io))

(define-public crate-posix-acl-0.1.0 (c (n "posix-acl") (v "0.1.0") (d (list (d (n "acl-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.12") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)))) (h "07py15n4wzbhkyc36pi48x5j8nybgi9xcwjfj1c9ssv63cc3vy3r")))

(define-public crate-posix-acl-0.2.0 (c (n "posix-acl") (v "0.2.0") (d (list (d (n "acl-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.12") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)))) (h "0bc812k5hwz9mrz9k8v5cjl2k3l0xm230w3wip91mqx0bqip3ady")))

(define-public crate-posix-acl-0.3.0 (c (n "posix-acl") (v "0.3.0") (d (list (d (n "acl-sys") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)))) (h "1pyj6w80z5bid0rj912vjhdjy5x1v3v3jmclgcz2k09vizr4rqjh")))

(define-public crate-posix-acl-0.4.0 (c (n "posix-acl") (v "0.4.0") (d (list (d (n "acl-sys") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "06slp690d1jdj0khij6sshw4bzn4k6p7bmm7sm7n215b6fj0vygx")))

(define-public crate-posix-acl-0.5.0 (c (n "posix-acl") (v "0.5.0") (d (list (d (n "acl-sys") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "139fib9gw876jz4k1dfkk18vyamzpxamxav3rvjyf7vi0xigc5i2")))

(define-public crate-posix-acl-1.0.0 (c (n "posix-acl") (v "1.0.0") (d (list (d (n "acl-sys") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "04hqx4na99pvlj2ak0dvgyx5014wci63zd1khmrzlra3kvlxm99f")))

(define-public crate-posix-acl-1.1.0 (c (n "posix-acl") (v "1.1.0") (d (list (d (n "acl-sys") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0lcgb4bdqa2dhnnpva498p902ys5sbw0785zfvpphvagjz269yh0")))

(define-public crate-posix-acl-1.2.0 (c (n "posix-acl") (v "1.2.0") (d (list (d (n "acl-sys") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "0hblp0anala69pdihzhhciqkybhl54qb03nryaj4qjly61hvfa4r") (r "1.60.0")))

