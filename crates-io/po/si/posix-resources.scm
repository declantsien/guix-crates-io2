(define-module (crates-io po si posix-resources) #:use-module (crates-io))

(define-public crate-posix-resources-0.1.0 (c (n "posix-resources") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xnaf7fjli26j63l1py5wd7rssh4jj52ra50pidsylvxbllwrsqq")))

(define-public crate-posix-resources-0.2.0 (c (n "posix-resources") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zmjlhzdak84rr3q83fbprb6zpr68zfjq3gydb6m3d2bn5ay5yz4")))

(define-public crate-posix-resources-0.3.0 (c (n "posix-resources") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13bz86zhy3yx9gqwk623k1cdw257gl20m278fgvmcxs4n02b6pwr")))

