(define-module (crates-io po si posix-socket) #:use-module (crates-io))

(define-public crate-posix-socket-0.1.0 (c (n "posix-socket") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "filedesc") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1nnql3i3hd1kxi7r8mhxb7inzanznknjk2j9mffhjb3vbxzk0sbi")))

(define-public crate-posix-socket-0.2.0 (c (n "posix-socket") (v "0.2.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "filedesc") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0wsjr6fcxwpjdlqbh0f75pxbzw9vyayznn6cz2xg994b2xhpyhap")))

