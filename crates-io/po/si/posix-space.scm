(define-module (crates-io po si posix-space) #:use-module (crates-io))

(define-public crate-posix-space-1.0.0 (c (n "posix-space") (v "1.0.0") (h "0rrwi407j2b85ahv42ggy62xw44h2k7zj8f8pb15vwlz0phhhr5h")))

(define-public crate-posix-space-1.0.1 (c (n "posix-space") (v "1.0.1") (h "0ls487ildgjh0ly79pql3bn8x7q6n75130x4dldyh1gxn8b3w1bv")))

(define-public crate-posix-space-1.0.2 (c (n "posix-space") (v "1.0.2") (h "1zrvn3vqkx1skzv3f1n206c9xj16bmdkz0x6x02x0jh15vpx4kbp")))

(define-public crate-posix-space-1.0.3 (c (n "posix-space") (v "1.0.3") (h "174kpm78vd431disnj2c8ks1jdnqyv6xywbcsnnmsbv6rbldc3mx")))

(define-public crate-posix-space-1.0.4 (c (n "posix-space") (v "1.0.4") (h "0lpk25zm8vyqm89khm326lzq1gm965r8436i37hffsiiz9r2a5zv") (r "1.31.0")))

