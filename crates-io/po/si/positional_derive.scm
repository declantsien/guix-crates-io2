(define-module (crates-io po si positional_derive) #:use-module (crates-io))

(define-public crate-positional_derive-0.1.0 (c (n "positional_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1ik0r94l4fgbakp2m23iscj3156igcvl1c1f9az0h1gwjndnspd1")))

(define-public crate-positional_derive-0.1.1 (c (n "positional_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "0jwa7g0kp7yj2y0fframnwdsxy3as4p4c9645hwvq9n248snsd7j")))

(define-public crate-positional_derive-0.1.2 (c (n "positional_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "0g4gixi61gflhigkrgi5mlvrqi9na442zaz9bbjsqnnj7yhcrz57")))

(define-public crate-positional_derive-0.1.3 (c (n "positional_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "1pvl87hjzxhdd3d1risgkzjw87ilvy6h5qkpfffz29z4x4jlap6i")))

(define-public crate-positional_derive-0.2.0 (c (n "positional_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "04m916791ck3rmpr8n2x4w61ksdca3rl0y0kjw419hi02yla6lkr") (y #t)))

(define-public crate-positional_derive-0.2.1 (c (n "positional_derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "1ls9ai5lxbmkphdks8gillzv6ypkzs2xgy1jw81vfkci9p8mw3fz") (y #t)))

(define-public crate-positional_derive-0.2.3 (c (n "positional_derive") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "1mp1n719pkcfjg311nh2zj9452b69i6p9qjc2b09mi0kfww3rmz4")))

(define-public crate-positional_derive-0.3.0 (c (n "positional_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "1cav22a3bnp9lvm7csmj6h68gqmc4fqlaway9g9ml92v68j9dmc9")))

(define-public crate-positional_derive-0.4.0 (c (n "positional_derive") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "0vksi4lb5c3hly14d7nwhjfr7lg6hl2s1vygh9yr72dkc7a3y1wa")))

(define-public crate-positional_derive-0.4.1 (c (n "positional_derive") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "1xrsls8gn3wghqwkyb87skn8na4inq9sy46hzp7k19w30pa1nmi5")))

(define-public crate-positional_derive-0.4.1-rc.0 (c (n "positional_derive") (v "0.4.1-rc.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 0)))) (h "0bdx4q6v1mpp8zb3z7m9wgl1ksg3gb390c5psnpvwc2hjqhic245")))

