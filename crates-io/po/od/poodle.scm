(define-module (crates-io po od poodle) #:use-module (crates-io))

(define-public crate-poodle-0.6.0 (c (n "poodle") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a3knhgz9589mnsqpi4lzxzvcw0jsnpv0pzgr2yphy5adp0h7wac")))

(define-public crate-poodle-0.6.1 (c (n "poodle") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i2313yqhahwfb5zqjpwvmynwhw007mrmwkjpg5p8pw5bhsny28f")))

(define-public crate-poodle-0.6.2 (c (n "poodle") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vn8ksm7imykb5wwclw1cc9abad1x9kxkikb7ybqz6qlniai80b5")))

(define-public crate-poodle-0.6.3 (c (n "poodle") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vb9xdr6r94myrphqf838izag41rzsng430ji62irra8xaa97dbn")))

