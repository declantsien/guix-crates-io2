(define-module (crates-io po wi powierza-coefficient) #:use-module (crates-io))

(define-public crate-powierza-coefficient-1.0.0 (c (n "powierza-coefficient") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "strsim") (r "^0.10") (d #t) (k 2)))) (h "0hjq0vi9gnww3yypmnihcmgwk74jln0w676xcr68rksj69w47alw") (y #t)))

(define-public crate-powierza-coefficient-1.0.1 (c (n "powierza-coefficient") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "strsim") (r "^0.10") (d #t) (k 2)))) (h "0zxslbnwka95wkiq82if1dv8nrci94xnn6h25ix2mbvkr8xw1qk4") (y #t)))

(define-public crate-powierza-coefficient-1.0.2 (c (n "powierza-coefficient") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "strsim") (r "^0.10") (d #t) (k 2)))) (h "0nrpq1hvdnl7ddlgffrz0a36cvv7rbhyys0fzy6mc9h0fmwk04h4")))

