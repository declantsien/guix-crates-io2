(define-module (crates-io po ch pochita) #:use-module (crates-io))

(define-public crate-pochita-0.1.0 (c (n "pochita") (v "0.1.0") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "07wdv6ilvcqpx2ymmfaz03cb46b7xkkvnlhqa8fm3xhjrnn069yx") (y #t)))

(define-public crate-pochita-0.1.1 (c (n "pochita") (v "0.1.1") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0v6yn4601x52h3dkjz2mcwdvd8295y1s798hxl1bqzl6knik4pd4") (y #t)))

(define-public crate-pochita-0.1.2 (c (n "pochita") (v "0.1.2") (d (list (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1q9c4mlsls73n087rqpf3a3d0zq3cb6ssz407k5pwxgfpj5y743j") (y #t)))

(define-public crate-pochita-0.1.3 (c (n "pochita") (v "0.1.3") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1cksdv79rq2bgrvc7aqxbzs921hpvnh16gw8aqwibsbqahciaj73") (y #t) (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-pochita-0.1.4 (c (n "pochita") (v "0.1.4") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "0ssw1zmbczlbacz47zi9sp4qpzh2bbyq111kfzk6fvjl3ps1gp5i") (y #t) (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-pochita-0.1.5 (c (n "pochita") (v "0.1.5") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (o #t) (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1ix31nnijgxj3yaxaza51if2z381amdq22q8xqcj24nswdz364jn") (y #t) (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

