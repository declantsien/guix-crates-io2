(define-module (crates-io po m- pom-rs) #:use-module (crates-io))

(define-public crate-pom-rs-0.1.0 (c (n "pom-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0l8vv97jx7px62wnd086qmk4wkds311n8vf4qjz28a7bl37pkhy2")))

(define-public crate-pom-rs-0.1.1 (c (n "pom-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "1a72im6j2fjh21jxznyfqxy90hngp0rnwr92ih73rir00ysdz5z2")))

(define-public crate-pom-rs-0.1.2 (c (n "pom-rs") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "02d1mmvkg0pmlcyvlw6k1fm54hgrk0glw37cvvr0i817pw6sg9dh")))

