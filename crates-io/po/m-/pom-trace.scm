(define-module (crates-io po m- pom-trace) #:use-module (crates-io))

(define-public crate-pom-trace-4.0.0 (c (n "pom-trace") (v "4.0.0") (h "1n1fng9w321lly7hdxdwbffidw2r5k5lngdaamd9g99x9b8cv6hx")))

(define-public crate-pom-trace-4.0.1 (c (n "pom-trace") (v "4.0.1") (h "1amf4wr0ljkhdlfgr4i0xx60rg8hpbzs9y11mipjpigzhjqjys7v")))

(define-public crate-pom-trace-4.0.2 (c (n "pom-trace") (v "4.0.2") (h "1qa25dgw42jsfzhhz020dn9ldnc8rg59mkdql7ji52hglrizwg9f")))

(define-public crate-pom-trace-4.0.3 (c (n "pom-trace") (v "4.0.3") (h "13pkm0525nfl4bnc86ylx8k48ign60dgw3slhcm0am77jl63raj9")))

