(define-module (crates-io po in pointillism) #:use-module (crates-io))

(define-public crate-pointillism-0.1.0 (c (n "pointillism") (v "0.1.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bb4dkrb36w32n17isq20pjxg31hpa34b88w1s8aisaps29hcp42") (y #t)))

(define-public crate-pointillism-0.1.1 (c (n "pointillism") (v "0.1.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l7c84wfxwr3wndd3agc2fxiyrrrw0w6j41cq8qmfxba7dchfff8") (y #t)))

(define-public crate-pointillism-0.1.2 (c (n "pointillism") (v "0.1.2") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vzig3xf0yd4lgbgi9dc6wby4inzkp7frq1ldmhn8p219yg0n4cv") (y #t)))

(define-public crate-pointillism-0.1.3 (c (n "pointillism") (v "0.1.3") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dx95fgs62swzwd1gk8j35mglw172ai593249a3swyjikrwl6mgh") (y #t)))

(define-public crate-pointillism-0.1.4 (c (n "pointillism") (v "0.1.4") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c2fnyfkcqhwlg6vjqgbh9zkx66qvnb5724ih8xqd4cf5q6asa1b") (y #t)))

(define-public crate-pointillism-0.1.5 (c (n "pointillism") (v "0.1.5") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10zvck1qrrr72bpkjjnlh92bcfrayxmkvwpl7f8gydlg7jcm0hd6") (y #t)))

(define-public crate-pointillism-0.1.6 (c (n "pointillism") (v "0.1.6") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09wklygi9npp15phj9qgqw9mwh5hxczjszm3915y2khsljqysqmf") (y #t)))

(define-public crate-pointillism-0.1.7 (c (n "pointillism") (v "0.1.7") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01pwjh03w2pl3hf290yxgzx6zxqnjizxldxnbcr71r2yvpz9li0b") (y #t)))

(define-public crate-pointillism-0.2.0 (c (n "pointillism") (v "0.2.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qk05sn5l5dzkapp33jizfyvnanpvnjnn3bfkw3azl64wvfsjsdf")))

(define-public crate-pointillism-0.2.1 (c (n "pointillism") (v "0.2.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07dms047fypwacnlcbkwnhscgdj9vndmpd75ljavpp92wibabzgr")))

(define-public crate-pointillism-0.2.2 (c (n "pointillism") (v "0.2.2") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "195fzkmyz0rcis18jnxdnxkjcnn2amxkbrj60yy3hi6k969fjnfv")))

(define-public crate-pointillism-0.2.3 (c (n "pointillism") (v "0.2.3") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05sym0f8l76phs6gglwz30ha1rcl0sacj1bbq6vvxn5r6qhrbhk0")))

(define-public crate-pointillism-0.2.4 (c (n "pointillism") (v "0.2.4") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03g5l0d5jg95a0kkgyn1x87lg9laqqz1ccq4aj2ghgq00xqbr4hi")))

(define-public crate-pointillism-0.2.5 (c (n "pointillism") (v "0.2.5") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0aad3c9rijnvr9p1vmw8xja9bw17vr25xdkrs8zc7lrf3zx1fapn")))

(define-public crate-pointillism-0.2.6 (c (n "pointillism") (v "0.2.6") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k2iqhrc775sp2gb55sfyc1h1z23d6s1qrd7qq01ypqbvd360snk")))

(define-public crate-pointillism-0.2.7 (c (n "pointillism") (v "0.2.7") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fi5sbi4cygbllmgi463njmvxap4w09ww4x8z7bp5vnszrhqa28p")))

(define-public crate-pointillism-0.2.8 (c (n "pointillism") (v "0.2.8") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08zyxqg0z08qy5rq9rapqzlppmymprwaybv01ccwhr3ljdw4f1pk")))

(define-public crate-pointillism-0.2.9 (c (n "pointillism") (v "0.2.9") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yi97hdxx6zh5n4wr6d144zrwdppvpwswh5kl7qwizrarypbqyd8")))

(define-public crate-pointillism-0.2.10 (c (n "pointillism") (v "0.2.10") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rswbrxkanb9rrbzh22q3hiy5dqxb2xl8slz0xv1j6w7nz2hlfvk")))

(define-public crate-pointillism-0.3.0 (c (n "pointillism") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06zil9wp6bcypgfl6hfikp57myk0jb6f11c7h60wssmnik8inwv3") (f (quote (("default" "assert_approx_eq" "human-duration"))))))

(define-public crate-pointillism-0.3.1 (c (n "pointillism") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qgdqqxz2sqizhksw51g3sxv54zjpxmm6kljm7gh07a5yrvijchb") (f (quote (("default" "assert_approx_eq" "human-duration"))))))

(define-public crate-pointillism-0.3.3 (c (n "pointillism") (v "0.3.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00n0rbp4cl8qcbss5pj9wbziv8mk0h1pvfaqs81kcblrqmf3fq8x") (f (quote (("default" "assert_approx_eq" "human-duration"))))))

(define-public crate-pointillism-0.3.4 (c (n "pointillism") (v "0.3.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpal") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06bhp3284a6nq8hfgg8rh057frfjxzhzp3hribfnvm5pck60a9lq") (f (quote (("github-actions-hack") ("default" "human-duration" "hound")))) (s 2) (e (quote (("cpal" "dep:cpal"))))))

(define-public crate-pointillism-0.3.5 (c (n "pointillism") (v "0.3.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpal") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0sdms63mlzsiz9a50r658h1r7sk34ynmxz0a46i0ibd1324p2f5d") (f (quote (("github-actions-hack") ("default" "human-duration" "hound")))) (s 2) (e (quote (("cpal" "dep:cpal"))))))

(define-public crate-pointillism-0.3.6 (c (n "pointillism") (v "0.3.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cpal") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "human-duration") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jms670h369vsgxvx2lhffxlbkaj2m636871padxrdh2nw4w9jlh") (f (quote (("github-actions-hack") ("default" "human-duration" "hound")))) (s 2) (e (quote (("midly" "dep:midly") ("cpal" "dep:cpal"))))))

(define-public crate-pointillism-0.4.2 (c (n "pointillism") (v "0.4.2") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "cpal") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "human-duration") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "pointillism-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1z4npi7j6bc1qhb55ng0lfxq64jabx4v7qw9nx3lps6ms2yjsxww") (f (quote (("github-actions-hack") ("github-actions-all-features" "all-features" "github-actions-hack") ("default" "human-duration" "hound") ("all-features" "cpal" "midly")))) (s 2) (e (quote (("midly" "dep:midly") ("cpal" "dep:cpal"))))))

(define-public crate-pointillism-0.4.3 (c (n "pointillism") (v "0.4.3") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "cpal") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "human-duration") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "pointillism-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19r4a6wgpk4pn0yyv5fh4k3zlb0vai8fvghbayifq77shs96sjp1") (f (quote (("github-actions-hack") ("github-actions-all-features" "all-features" "github-actions-hack") ("default" "human-duration" "hound") ("all-features" "cpal" "midly")))) (s 2) (e (quote (("midly" "dep:midly") ("cpal" "dep:cpal"))))))

