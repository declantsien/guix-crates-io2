(define-module (crates-io po in point-and-click) #:use-module (crates-io))

(define-public crate-point-and-click-0.0.0 (c (n "point-and-click") (v "0.0.0") (d (list (d (n "pixels") (r "^0.7") (d #t) (k 0)) (d (n "web-audio-api") (r "^0") (d #t) (k 0)) (d (n "web-local-storage-api") (r "^0") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10") (d #t) (k 0)))) (h "0y1b9rzh4frk6iq1x3mc8zkdwi889laspgbfjqj9x3gd07lffmsz")))

