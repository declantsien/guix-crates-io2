(define-module (crates-io po in points_on_curve) #:use-module (crates-io))

(define-public crate-points_on_curve-0.1.0 (c (n "points_on_curve") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "piet") (r "^0.5") (d #t) (k 2)) (d (n "piet-common") (r "^0.5") (f (quote ("png"))) (d #t) (k 2)))) (h "1z97dwkgwya4ggdkbqa1i6jlqbijri47czgqk9iav7070j74k5qn")))

(define-public crate-points_on_curve-0.2.0 (c (n "points_on_curve") (v "0.2.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "piet") (r "^0.5") (d #t) (k 2)) (d (n "piet-common") (r "^0.5") (f (quote ("png"))) (d #t) (k 2)))) (h "01n69lv7afm9cnlwn4jj89879qgjfnlnfd9hrmnc58pn2czijrx6")))

(define-public crate-points_on_curve-0.4.0 (c (n "points_on_curve") (v "0.4.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "piet") (r "^0.5") (d #t) (k 2)) (d (n "piet-common") (r "^0.5") (f (quote ("png"))) (d #t) (k 2)))) (h "19yz4kd6l5nazqfjl1p93jnnbc1gjr71z5yki9vgmc5w2135s59s")))

(define-public crate-points_on_curve-0.5.0 (c (n "points_on_curve") (v "0.5.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "piet") (r "^0.5") (d #t) (k 2)) (d (n "piet-common") (r "^0.5") (f (quote ("png"))) (d #t) (k 2)))) (h "15b2fdqklbv2d78b7ch99fmm9pxgp4y6c5b5p88fgwwyc6m5mmfv")))

