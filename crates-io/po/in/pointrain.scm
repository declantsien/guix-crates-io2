(define-module (crates-io po in pointrain) #:use-module (crates-io))

(define-public crate-pointrain-0.1.0 (c (n "pointrain") (v "0.1.0") (d (list (d (n "pointrain-core") (r "=0.1.0") (d #t) (k 0)) (d (n "pointrain-io") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rerun") (r "^0.8") (f (quote ("native_viewer"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1cchblwy3rbzwmc6p1fzmb2m3g6gwabgf6ipxc04gmm7v3fjmc51") (f (quote (("rerun" "pointrain-core/rerun") ("io" "pointrain-io")))) (r "1.69.0")))

