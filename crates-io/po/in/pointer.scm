(define-module (crates-io po in pointer) #:use-module (crates-io))

(define-public crate-pointer-0.0.1 (c (n "pointer") (v "0.0.1") (d (list (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)))) (h "048vcpfv7aw2svv9dc510gnrbc1kvym96lijpaclwycf0sjlmfs8")))

(define-public crate-pointer-0.0.2 (c (n "pointer") (v "0.0.2") (d (list (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)))) (h "0f1cf0hmmqshp2m5lf9zbags05bzz4llbw6jrv7dgagwznjikyha")))

