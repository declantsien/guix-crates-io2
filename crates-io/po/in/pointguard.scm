(define-module (crates-io po in pointguard) #:use-module (crates-io))

(define-public crate-pointguard-0.1.0 (c (n "pointguard") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.2.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1rbhyphgrn4b1j1bk073b956ljy4v9r5z4prscl5cv4rsw3d0ihg")))

(define-public crate-pointguard-0.1.1 (c (n "pointguard") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.2.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "023fkqirrz2bjpbvx15xglhqgbj8bmpv0p8kdi1bfcfvqk6igcj2")))

