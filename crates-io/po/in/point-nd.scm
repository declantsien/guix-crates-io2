(define-module (crates-io po in point-nd) #:use-module (crates-io))

(define-public crate-point-nd-0.1.0 (c (n "point-nd") (v "0.1.0") (h "0qmk5iby7mvggsz43asxxvrb7wjz4rwypb89h5d7p0hl57nhlll1")))

(define-public crate-point-nd-0.1.1 (c (n "point-nd") (v "0.1.1") (h "1kkrbw05qxk87nxm3sk180kilf2w7cdck84vinhdwdsc815g254r")))

(define-public crate-point-nd-0.2.0 (c (n "point-nd") (v "0.2.0") (h "16588sz0239dpiibxls0j8pp2dpjliv0rxa1f3zwxn3g422gf6ac")))

(define-public crate-point-nd-0.2.1 (c (n "point-nd") (v "0.2.1") (h "08ld1pyw9fyqi5r4gn0s5a9ak4dimadvzyxbfj7lczi8r370wnml")))

(define-public crate-point-nd-0.3.0 (c (n "point-nd") (v "0.3.0") (h "106lkyzxg9k7v7jvqd1ylwgmb07mjlzgp9mr8r604ivxszra8d5l")))

(define-public crate-point-nd-0.4.0 (c (n "point-nd") (v "0.4.0") (h "0srpak7fn0sxbmgsmfh2x3xmw5vhmgrr1hhzfranw6lyyxg3g8zh")))

(define-public crate-point-nd-0.4.1 (c (n "point-nd") (v "0.4.1") (h "0c5zbzama0s825rsiggbjg7bavz80vdd3jjfpliry1s4nbyy4q1j")))

(define-public crate-point-nd-0.5.0 (c (n "point-nd") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)))) (h "1nk1gbgf1y4np8gz6w9lbw925n7r49s1vny4by1609kgxnvilpkl") (f (quote (("z") ("y") ("x") ("w") ("var-dims" "arrayvec") ("full" "default" "var-dims") ("default" "conv_methods" "appliers") ("conv_methods" "x" "y" "z" "w") ("appliers" "arrayvec"))))))

