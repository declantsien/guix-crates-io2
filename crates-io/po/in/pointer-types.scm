(define-module (crates-io po in pointer-types) #:use-module (crates-io))

(define-public crate-pointer-types-0.1.0 (c (n "pointer-types") (v "0.1.0") (h "06m6cavvhdywrqdagyx0kcf2d1hrgz40fb5hw3w6bh3snc2iy782") (y #t)))

(define-public crate-pointer-types-0.2.0 (c (n "pointer-types") (v "0.2.0") (h "0vcydniahg2y1xfn9b95x6nfcdsnsgi27vxs2yj9b7kz7lpybgp2") (y #t)))

(define-public crate-pointer-types-0.2.1 (c (n "pointer-types") (v "0.2.1") (h "13p8ss6nnysv6n7ilrr2m78k84rn0qp97g7zxv6pfly1sk60hda0") (y #t)))

(define-public crate-pointer-types-0.3.0 (c (n "pointer-types") (v "0.3.0") (d (list (d (n "keyboard-types") (r "^0.6") (d #t) (k 0)))) (h "0z4j5l7hs8rllkkhrc4s8d93gklxzp2v45chkjdamvygz6h7hpky")))

