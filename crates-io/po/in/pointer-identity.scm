(define-module (crates-io po in pointer-identity) #:use-module (crates-io))

(define-public crate-pointer-identity-0.1.0 (c (n "pointer-identity") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-strategy") (r "^0.3.1") (d #t) (k 2)))) (h "0qlkpfgix6k770j23jbbsyq8xzzn8lrs209vhzclxv2qx9yy2mqn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("bytes" "dep:bytes"))))))

(define-public crate-pointer-identity-0.1.1 (c (n "pointer-identity") (v "0.1.1") (d (list (d (n "bytes") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-strategy") (r "^0.3.1") (d #t) (k 2)))) (h "0ipg1gl229z65g3mx3nc5i46z6f7k2mz4275yjsp7aivbgd459j2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("bytes" "dep:bytes"))))))

