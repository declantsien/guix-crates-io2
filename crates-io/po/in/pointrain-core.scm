(define-module (crates-io po in pointrain-core) #:use-module (crates-io))

(define-public crate-pointrain-core-0.1.0 (c (n "pointrain-core") (v "0.1.0") (d (list (d (n "colorgrad") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rerun") (r "^0.8") (o #t) (d #t) (k 0)))) (h "143f0r9p4xhfaj9rak2lyhnw6gw2mrr1fsaanqncm0v70mncdy9i") (s 2) (e (quote (("rerun" "dep:rerun" "colorgrad")))) (r "1.69.0")))

