(define-module (crates-io po in pointer_vec) #:use-module (crates-io))

(define-public crate-pointer_vec-0.1.0 (c (n "pointer_vec") (v "0.1.0") (h "16dwjv1y7blph9260r350fv2v6vxvp1agnylgbiarg7iy4i0rgc0")))

(define-public crate-pointer_vec-0.1.1 (c (n "pointer_vec") (v "0.1.1") (h "1yjyyfr68927mhd5r5z15fbmlsyh2vjqlsz89gm1br4k8kp5ix6s")))

