(define-module (crates-io po in pointer-width) #:use-module (crates-io))

(define-public crate-pointer-width-0.1.0 (c (n "pointer-width") (v "0.1.0") (h "1y4rwv4lslyw9l925azy5gbw7px2aqrm8fdcazf8wirwrxw4gi48")))

(define-public crate-pointer-width-0.1.1 (c (n "pointer-width") (v "0.1.1") (h "1vdz2axv4ylfbkmvrr9x7bpfg2fvqcnsaj8swf84wyjmbn0pr4ac")))

