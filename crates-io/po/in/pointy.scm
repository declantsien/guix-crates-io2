(define-module (crates-io po in pointy) #:use-module (crates-io))

(define-public crate-pointy-0.1.0 (c (n "pointy") (v "0.1.0") (h "049rk25ccd1pl0vy4giinvkc12jg9k7hd29b5ljs3ja2zvhpcsqs")))

(define-public crate-pointy-0.2.0 (c (n "pointy") (v "0.2.0") (h "1g7pgvnn108gybz3da114zqajcdr7fvxkmxvvwn09h0jj1sfnr7q")))

(define-public crate-pointy-0.2.1 (c (n "pointy") (v "0.2.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)))) (h "05i6q93z3akd5s2w7kfzfgxkqhc1skd2zml7x0h0gj9m8jh8ml54")))

(define-public crate-pointy-0.3.0 (c (n "pointy") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00a9k73fjf0xj3l42x1psyyrx2ab5dymsyr52cr78yinjbjz16qk") (f (quote (("default"))))))

(define-public crate-pointy-0.4.0 (c (n "pointy") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17a0md01jdydaag986jd7fg7rv3414m5ygis4gvziizp8sdci3gh") (f (quote (("default"))))))

(define-public crate-pointy-0.5.0 (c (n "pointy") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s0jzvk31gd8dpjp8x745r953vj2958ysfr6sfglgl96kdqmbgqs") (f (quote (("default"))))))

(define-public crate-pointy-0.6.0 (c (n "pointy") (v "0.6.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w7z83ry6scl9cipphd8x5jf6zpf5n3fv42vaj4clspcyb0239hd") (f (quote (("default"))))))

