(define-module (crates-io po in point-derive) #:use-module (crates-io))

(define-public crate-point-derive-0.1.0 (c (n "point-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0pw2z6r6ljmkw6x0i54xjxnqkr3ry98qhf6w79dh8vfwdk2af91j")))

(define-public crate-point-derive-0.1.1 (c (n "point-derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "~1.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0r2w82cacky4ay26k5q2wnxhqb9gym92zly8sdrb0glm2i4zwbq4")))

(define-public crate-point-derive-0.1.2 (c (n "point-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "1j7h5q8mw1mx876ccpwg5hg7v1924sd92jdzz3q67c0isl20sq8d")))

(define-public crate-point-derive-0.1.3 (c (n "point-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0z58p39amx56m25mx703l3426pg11rn3kx109zf0ns4xmsg121q0")))

(define-public crate-point-derive-0.1.4 (c (n "point-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0dpx6bg86jpkir89vcd3pmig03h9hcv3gb56ps9l3r9y2spnk2x7")))

