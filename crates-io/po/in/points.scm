(define-module (crates-io po in points) #:use-module (crates-io))

(define-public crate-points-0.1.1 (c (n "points") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0aw5nsjr0ffwlgzg3zjbc10dqxhjd847q3f3djxfd6myw5ji0pg0") (y #t)))

(define-public crate-points-0.1.2 (c (n "points") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1r196i709fi2k36mmyw4zyl5y96z15y9qi041hrwd0kil32jng51") (y #t)))

(define-public crate-points-0.1.3 (c (n "points") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0v2mdiqgqgcmn60f79d4qm3ag4gap8a9ghjvwg2dk8rjh1s89hib") (y #t)))

(define-public crate-points-0.1.4 (c (n "points") (v "0.1.4") (h "1k2i2bn5a11hdy7rdryzj21cvvif0x73i9mar2yn05flblfpiiaa") (y #t)))

