(define-module (crates-io po in point_like) #:use-module (crates-io))

(define-public crate-point_like-0.1.0 (c (n "point_like") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1q7jwkxab774yb9v9achmbxaibwx09s7dkw1ch8zwbnjrk85vfl1")))

(define-public crate-point_like-0.1.1 (c (n "point_like") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "05rfgxhgmkm1gq58cragir6d034f5zb99vqniyr83f14hn61v2wz")))

(define-public crate-point_like-0.1.2 (c (n "point_like") (v "0.1.2") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1kyap2p7568jj9r68f2l9wn3cr7840sfa3jr7dp11v5xlwix6mqa")))

