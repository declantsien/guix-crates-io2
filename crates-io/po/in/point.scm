(define-module (crates-io po in point) #:use-module (crates-io))

(define-public crate-point-0.1.0 (c (n "point") (v "0.1.0") (h "0v5xyy46idks61mr0aapl6bw7y48kqxkjfsxki9jw7m5c7a8py9k")))

(define-public crate-point-0.2.0 (c (n "point") (v "0.2.0") (h "19a47lnh2pxd1sblbxfvnpfy2w3wj2q5fmawr2rcbr43k6cnkb1w")))

(define-public crate-point-0.3.0 (c (n "point") (v "0.3.0") (h "00l530hf0b6015kg8f1m686hzsxdcysy416pzc1s7wv40drd958k")))

(define-public crate-point-0.3.1 (c (n "point") (v "0.3.1") (h "04g68xp34k4rk4h82mccx3zm5r8na32vz3n2qh0gsa9d9w29c3rc")))

