(define-module (crates-io po in point-index) #:use-module (crates-io))

(define-public crate-point-index-0.1.0 (c (n "point-index") (v "0.1.0") (h "026dpgcfcydbim7bzr05mn907f2mp0qq2kb1dzp6x1w37kq8dg7a")))

(define-public crate-point-index-0.1.1 (c (n "point-index") (v "0.1.1") (h "0y6zbvaix59rr5l54s9fvmp9q3kagyf6cjyx71f5mvfsn7f1xhym")))

(define-public crate-point-index-0.1.2 (c (n "point-index") (v "0.1.2") (h "0wlv245qj34iylac7y6q3s8wd6ldllrxg8rzh6rjn1flichddkc6")))

