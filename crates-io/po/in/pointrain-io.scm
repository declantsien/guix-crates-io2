(define-module (crates-io po in pointrain-io) #:use-module (crates-io))

(define-public crate-pointrain-io-0.1.0 (c (n "pointrain-io") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "pointrain-core") (r "=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jwrqp3v29rvqjf6mggpwsd3prw99w7imjrcxlybzr550rdgx8ag") (r "1.69.0")))

