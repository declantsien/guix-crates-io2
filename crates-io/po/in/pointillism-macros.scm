(define-module (crates-io po in pointillism-macros) #:use-module (crates-io))

(define-public crate-pointillism-macros-0.1.0 (c (n "pointillism-macros") (v "0.1.0") (h "03n1hci8vzh2qaz60g32kkir7xq1gxyiiavn3bfp4fycx6m7piha")))

(define-public crate-pointillism-macros-0.1.1 (c (n "pointillism-macros") (v "0.1.1") (h "042ziid2x6s1754dmmrsmxv0mv5x7f33vgb7s7cqid7p5v6g3b1l")))

