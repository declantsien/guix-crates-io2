(define-module (crates-io po dm podman-api-stubs) #:use-module (crates-io))

(define-public crate-podman-api-stubs-0.0.1 (c (n "podman-api-stubs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14id3f30nln2dw1bwv53s9lim0drnylc9j14dfypvrv6arwcjfvn") (y #t)))

(define-public crate-podman-api-stubs-0.1.0 (c (n "podman-api-stubs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fgfibp1ng2g5dg7m07gscgdx117v9cldcfdd17ygnpkhplxhp8y")))

(define-public crate-podman-api-stubs-0.2.0 (c (n "podman-api-stubs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nb9rhjdgss2lr1d3lwzw6n3bs6r3iba86vb88zxw948y3xvqikc")))

(define-public crate-podman-api-stubs-0.2.1 (c (n "podman-api-stubs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0sgpf8s0hymrj2w9ashbmaj5dknqm0hvrj71kk0qlkhvcwvq803s")))

(define-public crate-podman-api-stubs-0.3.0 (c (n "podman-api-stubs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kyibsw4rsls8vaqi3p64b6d24v4pppm87wvzn4y8l06fjyfbfk1")))

(define-public crate-podman-api-stubs-0.4.0 (c (n "podman-api-stubs") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vly1q8ixwm63ld2w2n9lcavr6xq6gdd4yczjkwhp0v45vr0j8dp") (y #t)))

(define-public crate-podman-api-stubs-0.5.0 (c (n "podman-api-stubs") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04qln50bvb0pdspfgl4rg23rz2kqckv7x6bhhndffcsp4gfmyb7x")))

(define-public crate-podman-api-stubs-0.6.0 (c (n "podman-api-stubs") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mxv9sc1fxcpdjg45l76is9pzw1dvqyv6hq8qzi1y60i3g3fvdnl")))

(define-public crate-podman-api-stubs-0.6.1 (c (n "podman-api-stubs") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1clsrnghnd77i9298sa6xf67p2cfhc72p90jf7vynxnx62l53pm8")))

(define-public crate-podman-api-stubs-0.7.0 (c (n "podman-api-stubs") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1k6mh5kqn8wrk96ga1rl4q441c2z1rji4rjgmzxggzpignkx5g85")))

(define-public crate-podman-api-stubs-0.8.0 (c (n "podman-api-stubs") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1afnvk42znzz0im0w5zc7xz0n9ql94gxcai5rppnjvhc7ja676nd")))

(define-public crate-podman-api-stubs-0.9.0 (c (n "podman-api-stubs") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vnw0lrn9345i8qbgm9ici85317rhzivkslgv3g0sfk37xi0qa1d")))

(define-public crate-podman-api-stubs-0.10.0 (c (n "podman-api-stubs") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15vjbad0dmii5h1862p307z33rm46h8gl2p1hjn3kbwlqb7kgdj8")))

(define-public crate-podman-api-stubs-0.11.0 (c (n "podman-api-stubs") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1d86hcwwxp9l851ahm36m277vzl0qy3cfki03fvpmy3qyhpd4jr8")))

