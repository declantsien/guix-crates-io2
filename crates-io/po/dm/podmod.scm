(define-module (crates-io po dm podmod) #:use-module (crates-io))

(define-public crate-podmod-0.3.0 (c (n "podmod") (v "0.3.0") (h "14lwfwf8j64i3ci0zw4i991vfygsgsy5xv9lszql0bgi9kjz7inn")))

(define-public crate-podmod-0.3.2 (c (n "podmod") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "1ywd2d5crrrb1yim7n0zjiyldjxh4y82nsjl4xlb53642zfmv9am")))

(define-public crate-podmod-0.3.3 (c (n "podmod") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "1rqdxcwhhlr9gmm32ldqzb6sj5mn277l2qg0jzsqrkj73iwlzzs6")))

(define-public crate-podmod-0.3.4 (c (n "podmod") (v "0.3.4") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "01ycmchqnip4v8pjh214h1sm8y7v122mnf3cpdiba6jflzfaswsh")))

(define-public crate-podmod-0.3.5 (c (n "podmod") (v "0.3.5") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)))) (h "0hbsbb9kvihyv6wxzf7hmqrkz70hw7wrg1gpb1ykyaw03n4n1sia")))

(define-public crate-podmod-0.3.6 (c (n "podmod") (v "0.3.6") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "16s3yzxkj1703v1jdab924434s6vbnkjbz7b7z0j0k6cl82mrvhg")))

(define-public crate-podmod-0.4.0 (c (n "podmod") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1y0kwa4vcfmdvqgx8mc7y0x831y1hz2fdp9fjfz6x1p6dh7kz129")))

(define-public crate-podmod-0.4.1 (c (n "podmod") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 1)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "048wf138qxcj25mji840iknp6pd4l5243mfrdw9qw1hfh4pv4db4")))

(define-public crate-podmod-0.4.2 (c (n "podmod") (v "0.4.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 1)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0aciyk6dgp87gb6kzb6bb13hn9yyy49y5sxvhxqa9jbbnqqjs3x1")))

(define-public crate-podmod-0.4.3 (c (n "podmod") (v "0.4.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 1)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0n05i0lp5ks09l5p3cx70s8cw6rrl7ghc33mamnz2g5nrmcqzy5q")))

