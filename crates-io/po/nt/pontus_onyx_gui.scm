(define-module (crates-io po nt pontus_onyx_gui) #:use-module (crates-io))

(define-public crate-pontus_onyx_gui-0.1.0 (c (n "pontus_onyx_gui") (v "0.1.0") (d (list (d (n "pontus_onyx") (r "^0.16.1") (f (quote ("server" "server_file_storage" "actix_server"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tauri") (r "^1.0.5") (f (quote ("api-all"))) (d #t) (k 0)) (d (n "tauri-build") (r "^1.0.4") (d #t) (k 1)))) (h "0g1wqcsllg0r9zhxdi7qgmild2jn9lll9yblpcsrbjwlg0c4p3rd") (f (quote (("default" "custom-protocol") ("custom-protocol" "tauri/custom-protocol")))) (r "1.57")))

