(define-module (crates-io po mi pomidorka) #:use-module (crates-io))

(define-public crate-pomidorka-0.1.1 (c (n "pomidorka") (v "0.1.1") (d (list (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "1k3yylgs2l93rlmdlf3kbqx8hfzhil8srlh50m82dddr54idphdr")))

(define-public crate-pomidorka-0.1.2 (c (n "pomidorka") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "1xv4p6bg8dzw8f9ipap6sp8q4xqz4jxvda38gqs923yz7rhrdyiy")))

