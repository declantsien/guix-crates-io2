(define-module (crates-io po my pomy) #:use-module (crates-io))

(define-public crate-pomy-0.1.1 (c (n "pomy") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.10.0") (f (quote ("vorbis"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "00n78ag8pf97c0zl48f584sr2lwkqcb2gqnqhx9h4hjydbwr7wjq")))

