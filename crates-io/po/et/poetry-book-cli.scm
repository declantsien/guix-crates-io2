(define-module (crates-io po et poetry-book-cli) #:use-module (crates-io))

(define-public crate-poetry-book-cli-0.1.0 (c (n "poetry-book-cli") (v "0.1.0") (d (list (d (n "poetry-book") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1vzv9ly3l19y733s8rzfv0x2i2wm55zpgqx45y01pi95hc0j5npd")))

(define-public crate-poetry-book-cli-0.1.1 (c (n "poetry-book-cli") (v "0.1.1") (d (list (d (n "poetry-book") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1q46rhp9llnw6d4x9gv6h3sx50gcfs2g5xy8aqblvcwjim9jbv4v")))

(define-public crate-poetry-book-cli-0.1.2 (c (n "poetry-book-cli") (v "0.1.2") (d (list (d (n "poetry-book") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0w9ss3md4qpjrlvyi0s072rnx62pnfxwb7j4xhw9v2jii1b6arcb")))

(define-public crate-poetry-book-cli-0.1.3 (c (n "poetry-book-cli") (v "0.1.3") (d (list (d (n "poetry-book") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1vpx18baq5f1jwb2hk1cxk6zg7n84xgq6gx1zky7i0hfx522p1xd")))

(define-public crate-poetry-book-cli-0.1.4 (c (n "poetry-book-cli") (v "0.1.4") (d (list (d (n "poetry-book") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0chzmglrab8bs4zxvs6cflgja4wf66mslcabxslz9fdhdwybidq2")))

