(define-module (crates-io po et poetry-book) #:use-module (crates-io))

(define-public crate-poetry-book-0.1.0 (c (n "poetry-book") (v "0.1.0") (h "03d0rnpalvhyycssqxxyxa45nwyfv681ws4kbadlj3g2a4ncl5hp")))

(define-public crate-poetry-book-0.1.1 (c (n "poetry-book") (v "0.1.1") (h "1ffk10irmzp542ymx115xllkm927z5fc2bbr0bx7jlsybcphsg3b")))

(define-public crate-poetry-book-0.1.2 (c (n "poetry-book") (v "0.1.2") (h "1wb82sgi4wdfdg4kr7in90l268ndygdpi8mlrw0qh9xrdqb3vgrd")))

(define-public crate-poetry-book-0.1.3 (c (n "poetry-book") (v "0.1.3") (h "0bj8yjb5kfsmxy7k2gr7xn5kp7zv0b1k95sj5w7crawwm5vzlkai")))

