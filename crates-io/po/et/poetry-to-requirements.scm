(define-module (crates-io po et poetry-to-requirements) #:use-module (crates-io))

(define-public crate-poetry-to-requirements-0.1.0 (c (n "poetry-to-requirements") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "19kb6w0w0zrf4dfd6ja2b6igd03s50a4is3b31hfl3wi7gsm3x8y")))

(define-public crate-poetry-to-requirements-0.2.0 (c (n "poetry-to-requirements") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "17if4nz6sfkd9l2mapzdmyhxx9csjy45hkfjdv0c73r4wwss773l")))

