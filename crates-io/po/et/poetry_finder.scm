(define-module (crates-io po et poetry_finder) #:use-module (crates-io))

(define-public crate-poetry_finder-0.1.0 (c (n "poetry_finder") (v "0.1.0") (h "0f2mh4l7lkk5k9ing2hiqjkxbdwgll42kma003lwhxkc5vly22wq") (y #t)))

(define-public crate-poetry_finder-0.2.0 (c (n "poetry_finder") (v "0.2.0") (h "17cimkmyr56izyv1vhwm9hjgkn4fh5gjyjxadvhyz4r9kpdmric7") (y #t)))

(define-public crate-poetry_finder-0.3.0 (c (n "poetry_finder") (v "0.3.0") (h "1hzfzs188gzr0nsw8i5ng9pdk438ypx5bsmd58mgkhqg5jglxq0l") (y #t)))

(define-public crate-poetry_finder-0.4.0 (c (n "poetry_finder") (v "0.4.0") (h "0m6wb2k6ii7h5m83w4ldxqslhwvg7l1xhxcw8bhxk31pvk2n3zch") (y #t)))

(define-public crate-poetry_finder-0.5.0 (c (n "poetry_finder") (v "0.5.0") (h "17wz8rbs6a4dp4hvba79ml56yzlnvsvg5p9q9g99x82s7r9bxvcx") (y #t)))

(define-public crate-poetry_finder-0.6.0 (c (n "poetry_finder") (v "0.6.0") (h "1ijjj6l2ja1x7akqldclas724jl4p9gzyj0zphzmskj75i3kxfkk") (y #t)))

(define-public crate-poetry_finder-0.7.0 (c (n "poetry_finder") (v "0.7.0") (h "0l1gf7y2vn7ss53nzm2ymhhs4i6xhsvmzaf6ldnpp9znxsvbxi7n")))

