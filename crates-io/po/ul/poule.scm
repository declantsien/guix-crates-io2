(define-module (crates-io po ul poule) #:use-module (crates-io))

(define-public crate-poule-0.2.0 (c (n "poule") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fdv4myqw0kj6628d6hd52lbqyr73aymi33ni3hbqhxv27s9a669")))

(define-public crate-poule-0.3.0 (c (n "poule") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hcml98afjl4f95jiwnfjmmcf2xlrcqpa2c9q13g9lv8648l6dcb")))

(define-public crate-poule-0.3.1 (c (n "poule") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mk4xnf92ml3rmqxfzdrylqyilrrg6clfkhkwhclabkkgx12psyz")))

(define-public crate-poule-0.3.2 (c (n "poule") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wwjf9fnnvgzgg1xwzrmjzxkcbm07az7s3n0rgz1fb011lyzjc2q")))

