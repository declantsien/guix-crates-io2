(define-module (crates-io po re poreader) #:use-module (crates-io))

(define-public crate-poreader-1.0.2 (c (n "poreader") (v "1.0.2") (d (list (d (n "lalrpop") (r "~0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "~0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1w9s6vjv57gli68yqmga83an73qhc1wizfdd14v1i1ba5bsxs04p") (y #t)))

(define-public crate-poreader-1.0.5 (c (n "poreader") (v "1.0.5") (d (list (d (n "lalrpop") (r "~0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "~0.19.6") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10l83wzi222pj8wwh8k4rzw73nvcb6smr9d1hx10agqymf4y84i5")))

(define-public crate-poreader-1.0.6 (c (n "poreader") (v "1.0.6") (d (list (d (n "lalrpop") (r "~0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "~0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0l4vfvp9ngbml18lx6vip0gq0za01qc7dmzahlwxl1mzaf71xdll")))

(define-public crate-poreader-1.1.0 (c (n "poreader") (v "1.1.0") (d (list (d (n "lalrpop") (r "~0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "~0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1zvyh5zcgn14bdxpgrxaxy2290ryhkk2lz7il1vx1qw0970v3bx0") (y #t)))

(define-public crate-poreader-1.1.1 (c (n "poreader") (v "1.1.1") (d (list (d (n "lalrpop") (r "~0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "~0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1zqf0qzd5kj2hh7h5xqdbyim09v26x82d60lib9w2jz0hf0i24z7")))

(define-public crate-poreader-1.1.2 (c (n "poreader") (v "1.1.2") (d (list (d (n "lalrpop") (r "^0.19.12") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.2") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1629z9f18fra73w5zd6b4gcbyl4ajdw7wgmd9pfdyabhvqhc81mh")))

