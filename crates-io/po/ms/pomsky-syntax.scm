(define-module (crates-io po ms pomsky-syntax) #:use-module (crates-io))

(define-public crate-pomsky-syntax-0.7.0 (c (n "pomsky-syntax") (v "0.7.0") (d (list (d (n "arbitrary") (r "^1.1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0n71rsqxn49d00wnsig47mnp02nw2p4l3yby8fnv42mpc91vwwwh") (f (quote (("suggestions" "strsim") ("default") ("dbg"))))))

(define-public crate-pomsky-syntax-0.8.0 (c (n "pomsky-syntax") (v "0.8.0") (d (list (d (n "arbitrary") (r "^1.1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1b20jvf969zqla50c1f43ifyhwxn6928kwjf8mnxqxfq2dz00ik6") (f (quote (("suggestions" "strsim") ("default") ("dbg"))))))

(define-public crate-pomsky-syntax-0.9.0 (c (n "pomsky-syntax") (v "0.9.0") (d (list (d (n "arbitrary") (r "^1.1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "03jz7b2yj1gp1k8w0abfgj773yw7msj57xi1j6js9p3bvf1ny44a") (f (quote (("suggestions" "strsim") ("default") ("dbg"))))))

(define-public crate-pomsky-syntax-0.10.0 (c (n "pomsky-syntax") (v "0.10.0") (d (list (d (n "arbitrary") (r "^1.2.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "17c8yw8wj0fm7k9g85zj2i61mghb13c3nxdawddc57a1309a4jh3") (f (quote (("suggestions" "strsim") ("default") ("dbg"))))))

(define-public crate-pomsky-syntax-0.11.0 (c (n "pomsky-syntax") (v "0.11.0") (d (list (d (n "arbitrary") (r "^1.3.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1ywwjydcw0iynkjny65k58h5b3bmqyckh5r9nch8vxrhpqpfvhq1") (f (quote (("default") ("dbg")))) (s 2) (e (quote (("suggestions" "dep:strsim"))))))

