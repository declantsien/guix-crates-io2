(define-module (crates-io po ms pomsky-macro) #:use-module (crates-io))

(define-public crate-pomsky-macro-0.5.0 (c (n "pomsky-macro") (v "0.5.0") (d (list (d (n "pomsky") (r "^0.5.0") (d #t) (k 0)))) (h "1ryj9j6zirakg748kw1qz052b553k2ivrfc4c99dnryghs85wh85") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.6.0 (c (n "pomsky-macro") (v "0.6.0") (d (list (d (n "pomsky") (r "^0.6.0") (d #t) (k 0)))) (h "0rl13ngdw7i1595wc4d7kziqqrzsi1din6b30hd1alipnb6a35sr") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.7.0 (c (n "pomsky-macro") (v "0.7.0") (d (list (d (n "pomsky") (r "^0.7.0") (d #t) (k 0)))) (h "058j8q1jihdvrq5d2zqivc526hnlra9xpyfvz06cw6wsg2lq4bh0") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.8.0 (c (n "pomsky-macro") (v "0.8.0") (d (list (d (n "pomsky") (r "^0.8.1") (d #t) (k 0)))) (h "1qgb65jsp05afwd2hzl1xc810ybpys6afai1d6faj4w3q7jz9dsh") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.9.0 (c (n "pomsky-macro") (v "0.9.0") (d (list (d (n "pomsky") (r "^0.9.0") (d #t) (k 0)))) (h "02way1n4zns3mn6i68sq3lznbx7n6rvsfhvdcyqvh32jlyi1hm9a") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.10.0 (c (n "pomsky-macro") (v "0.10.0") (d (list (d (n "pomsky") (r "^0.10.0") (d #t) (k 0)))) (h "06w7wf22knx7qlpcjall9zf5hri1zp6d4xfaf76hfmmppc813ijf") (f (quote (("diagnostics") ("default"))))))

(define-public crate-pomsky-macro-0.11.0 (c (n "pomsky-macro") (v "0.11.0") (d (list (d (n "pomsky") (r "^0.11.0") (d #t) (k 0)))) (h "1kr6yi2y1imnfbhrwvcb4f588kv88dlhfm16qfjv7d3y07wis16m") (f (quote (("diagnostics") ("default"))))))

