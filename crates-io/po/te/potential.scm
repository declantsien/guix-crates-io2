(define-module (crates-io po te potential) #:use-module (crates-io))

(define-public crate-potential-1.0.0 (c (n "potential") (v "1.0.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bvplv3fcsrsviisprk32c85s059issvn1p5b9f8p7x35ahw4d7q")))

(define-public crate-potential-2.0.0 (c (n "potential") (v "2.0.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c9sgdzpzbxazhizjwj8m9ralf7qqxfc1xn5l6siqqzgzwhmc0hs")))

(define-public crate-potential-2.1.0 (c (n "potential") (v "2.1.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f8rj5p49pnwhdc3jjqrlql8hyd8blndrycj0qhcb3hk7kwp5hr2")))

(define-public crate-potential-2.2.0 (c (n "potential") (v "2.2.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fr6zrzyjrqgn9yn4a5dsg25sskymc1gcbymbs2j7rcjifqf0s58")))

(define-public crate-potential-2.2.1 (c (n "potential") (v "2.2.1") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1p124b61jbajsqgrd36dzxfxbhwjj2wjwsc31xv4amj62r5mhajh")))

