(define-module (crates-io po te potenv) #:use-module (crates-io))

(define-public crate-potenv-0.1.0 (c (n "potenv") (v "0.1.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "18h8qhzxyia6nmy10np9v2kp1sddl1a5kyyhn53mxq4lg1nhkzss")))

(define-public crate-potenv-0.2.0 (c (n "potenv") (v "0.2.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0al28hrzdv9fhdjn92nrnywk1jsszaj8hny5rbh4a5rplqk0lvf8")))

