(define-module (crates-io po c_ poc_library) #:use-module (crates-io))

(define-public crate-poc_library-0.0.1 (c (n "poc_library") (v "0.0.1") (h "08c2r2dhwlnsxd2c3vqxr9j2w0bfssxiyf43blc26cqyysxc1drd")))

(define-public crate-poc_library-0.0.4 (c (n "poc_library") (v "0.0.4") (h "0pdbp7qqrkz3vk9la1kgk0f42j7x8dsjxhnlrpmsir7w4dfghk0v")))

