(define-module (crates-io po c_ poc_lib) #:use-module (crates-io))

(define-public crate-poc_lib-0.1.0 (c (n "poc_lib") (v "0.1.0") (h "0h6k98hxamd7xgszn0j8lgyd0fjzz44q507qanicrgp49klx5wrs")))

(define-public crate-poc_lib-0.1.1 (c (n "poc_lib") (v "0.1.1") (h "0gfilak4a6znfnm0iii4chxxh6kjfjw498q4300g9v4xqvq78s98")))

(define-public crate-poc_lib-3.4.5 (c (n "poc_lib") (v "3.4.5") (h "1269xlc42hhp8k9vkasjgz757snpawdy5s6sqx83a8x1y12hvsv9") (y #t)))

(define-public crate-poc_lib-0.0.2 (c (n "poc_lib") (v "0.0.2") (h "0sspx9blfqgi45c1yi2m4wf164v6lsr131jfv2picw6s2a6a1hl2")))

