(define-module (crates-io po di podio) #:use-module (crates-io))

(define-public crate-podio-0.0.1 (c (n "podio") (v "0.0.1") (h "0wbrs0zx4mja297g1gqxsa7lknkvjin5hdh6gacwv2sx8kwz2vib")))

(define-public crate-podio-0.0.2 (c (n "podio") (v "0.0.2") (h "1xfvb500bcgavvbxy3x3f30h0jl1m9a2hcwr9x7g9qr96q70sm0k")))

(define-public crate-podio-0.0.3 (c (n "podio") (v "0.0.3") (h "1v5hfwdzy5dc3h758r3c43ssr0644mj87ibg6s2srrniykzyrfv9")))

(define-public crate-podio-0.0.4 (c (n "podio") (v "0.0.4") (h "1hrn1cslpsyqzvvjvbgqffhn2h7h6qdzxh23xpw1wqr60m35m6w2")))

(define-public crate-podio-0.0.5 (c (n "podio") (v "0.0.5") (h "012h8r7nsalgbpyv872zm42877mk4lz8cz41ddiq51rhx6azcl1s")))

(define-public crate-podio-0.0.6 (c (n "podio") (v "0.0.6") (h "0zd23246v9yhsbw8bvyirlldv6m8wp18b8rdchgxaznkxb7dcy3y")))

(define-public crate-podio-0.1.0 (c (n "podio") (v "0.1.0") (h "04cg11wmh4r7a3qimsac9r7m6vxqwx8fh4pryw49iqjdr0rj56nd")))

(define-public crate-podio-0.1.1 (c (n "podio") (v "0.1.1") (h "07vszgxk23ibl3v97ghck0k33byhyhnksasdrj2gd33r9myx0n05")))

(define-public crate-podio-0.1.2 (c (n "podio") (v "0.1.2") (h "1addi15p79m12dfp6b80swwv5lgl6qvbq1x71n86dchq6w1a66zf")))

(define-public crate-podio-0.1.3 (c (n "podio") (v "0.1.3") (h "0vb2l2i953amn2ra3ri0v6p17nhiqcs7vcjchaglw1qc9rd1hxci")))

(define-public crate-podio-0.1.4 (c (n "podio") (v "0.1.4") (h "01zqhx1hhsh9n6y77vfajpfiys0hkfbf0rxa9my0a5ws3xnrgixm")))

(define-public crate-podio-0.1.5 (c (n "podio") (v "0.1.5") (h "1843adrajhrz13m3qpzx727i6n2264vh2yvimr3wqmxww4g2lhp5")))

(define-public crate-podio-0.1.6 (c (n "podio") (v "0.1.6") (h "1ga5arhwakj5rwrqzf9410zrbwnf24jd59af8kr9rgwbd6vb83vq")))

(define-public crate-podio-0.1.7 (c (n "podio") (v "0.1.7") (h "06bzjxrl0h8rp5860n51dlr1g143grg2jmx4g6y1mdn2ignyz2xi")))

(define-public crate-podio-0.2.0 (c (n "podio") (v "0.2.0") (h "1hla8fjfa8aj1w2lawjs5wz1xr2608jd70w3rmdfjg05dknpqfg8")))

