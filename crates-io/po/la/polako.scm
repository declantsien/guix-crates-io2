(define-module (crates-io po la polako) #:use-module (crates-io))

(define-public crate-polako-0.1.0 (c (n "polako") (v "0.1.0") (h "1qbjdgplbprdy49y7jcay2a1acxs84ykwg3k8ach1dv435p3qp99")))

(define-public crate-polako-0.1.1 (c (n "polako") (v "0.1.1") (h "1a095wnig2zhz01pp58lb5bhx0m4bz99lgrg03in9ri4zknwfd14")))

(define-public crate-polako-0.1.2 (c (n "polako") (v "0.1.2") (h "1r4jfsqgs0mjnzylh68ph0zxfbi7f8a0kjd3nk9biarafpd3xlb1")))

