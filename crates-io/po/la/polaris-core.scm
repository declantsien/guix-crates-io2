(define-module (crates-io po la polaris-core) #:use-module (crates-io))

(define-public crate-polaris-core-0.1.2 (c (n "polaris-core") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1qp9018wh7p1bb6mxw99jhzd3xfgdf7yqab0jb51ncc0ln5nkx65")))

(define-public crate-polaris-core-0.1.3 (c (n "polaris-core") (v "0.1.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07qv11982wdhym9s1w4sqj71yw5bc7jvfqll7pz4746p5qvk7lzg")))

