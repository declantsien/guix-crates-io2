(define-module (crates-io po la polar-prost) #:use-module (crates-io))

(define-public crate-polar-prost-0.1.0 (c (n "polar-prost") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (o #t) (d #t) (k 1)))) (h "07qpdvfzlpzwi0987izs1n5ibxm6y5xmsfw7xzf42nqnqchcqyba") (f (quote (("rebuild-proto" "prost-build") ("default"))))))

