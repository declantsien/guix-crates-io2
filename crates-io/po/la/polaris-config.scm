(define-module (crates-io po la polaris-config) #:use-module (crates-io))

(define-public crate-polaris-config-0.1.2 (c (n "polaris-config") (v "0.1.2") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "0y2xb2jh21fmqxh3vlkbfl4imk04pfqqkv58brxpl5rpqa96g4yw")))

(define-public crate-polaris-config-0.1.3 (c (n "polaris-config") (v "0.1.3") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "0cqssqf4ql94y43avxqivzaf8xwv3g3ds5qznw2b63jmiakjrv9i")))

