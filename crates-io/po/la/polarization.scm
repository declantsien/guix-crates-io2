(define-module (crates-io po la polarization) #:use-module (crates-io))

(define-public crate-polarization-0.1.0 (c (n "polarization") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 0)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 0)))) (h "1wh9r6kck6fl5yj1dj4xqdf5ri8pndix7hxxqmkc9bwqpghnss8j")))

(define-public crate-polarization-0.1.1 (c (n "polarization") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 0)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 0)))) (h "09rk32nnxpdxpp136jad0cm924j8miw81yxinca8ynhwxx17frgg")))

(define-public crate-polarization-0.1.2 (c (n "polarization") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 2)))) (h "0fmljc77bkgak56mpizv3v331sbb6awsrcwsc0j5ridjabvpzhkz")))

(define-public crate-polarization-0.1.3 (c (n "polarization") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 2)))) (h "1aksr47k36nwkw4dkjl3b38i4fxqj8iaddhdf4w67sp6mwndizh0")))

(define-public crate-polarization-0.1.4 (c (n "polarization") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 2)))) (h "19f5gcg01bwjcmd1q95mqn2a9l4h2gpc5h0x5qzh5dk94936hqcl")))

(define-public crate-polarization-0.1.5 (c (n "polarization") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 2)))) (h "1iwnvamqdv0ab0b6gkdih4c21bjg2kaa7b2ays18czi8al94nmgx") (y #t)))

(define-public crate-polarization-0.2.0 (c (n "polarization") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "~1.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "~0.16.0") (d #t) (k 0)) (d (n "num") (r "~0.2.0") (d #t) (k 0)) (d (n "proptest") (r "~0.8.5") (d #t) (k 2)))) (h "1qbgmyrzgh94h0rij014jsrays95744b5h5lxrg8r2v5k5kr6mzm")))

