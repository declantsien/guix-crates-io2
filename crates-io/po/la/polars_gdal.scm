(define-module (crates-io po la polars_gdal) #:use-module (crates-io))

(define-public crate-polars_gdal-0.1.0 (c (n "polars_gdal") (v "0.1.0") (d (list (d (n "gdal") (r "^0.14") (d #t) (k 0)) (d (n "gdal-sys") (r "^0.8") (d #t) (k 0)) (d (n "polars") (r "^0.26") (f (quote ("dtype-binary" "dtype-date" "dtype-datetime" "rows"))) (d #t) (k 0)) (d (n "polars") (r "^0.26") (f (quote ("ipc"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gn93cp686ll7fgm48h1ngvrmgf03krqy90czc88anl13w1silz1")))

