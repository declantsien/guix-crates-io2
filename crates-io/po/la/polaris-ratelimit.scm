(define-module (crates-io po la polaris-ratelimit) #:use-module (crates-io))

(define-public crate-polaris-ratelimit-0.1.2 (c (n "polaris-ratelimit") (v "0.1.2") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "1vkyix6x3h68d053l4np1nlpjddy44sz2asb4kg2y2i50zfag2f1")))

(define-public crate-polaris-ratelimit-0.1.3 (c (n "polaris-ratelimit") (v "0.1.3") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "1wh0x5i5dxxjrf9m2blci9gsablrca7mp8ffm3acczq2xy0g4p07")))

