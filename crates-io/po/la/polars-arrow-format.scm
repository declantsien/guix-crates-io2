(define-module (crates-io po la polars-arrow-format) #:use-module (crates-io))

(define-public crate-polars-arrow-format-0.1.0 (c (n "polars-arrow-format") (v "0.1.0") (d (list (d (n "planus") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "prost-derive") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "tonic") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0k5qci66rcwqv4ycb43zlkk128wrdvcqkc95j2qrd4xgfhjfzc0r") (f (quote (("ipc" "planus" "serde") ("full" "ipc" "flight-data" "flight-service") ("flight-service" "flight-data" "tonic") ("flight-data" "prost" "prost-derive") ("default"))))))

