(define-module (crates-io po la polars-mongo) #:use-module (crates-io))

(define-public crate-polars-mongo-0.1.0 (c (n "polars-mongo") (v "0.1.0") (d (list (d (n "mongodb") (r "^2.2.1") (f (quote ("sync"))) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.23.1") (f (quote ("lazy" "dtype-full"))) (d #t) (k 0)) (d (n "polars-core") (r "^0.23.1") (d #t) (k 0)) (d (n "polars-time") (r "^0.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0r7ld4m3b69b3iii35n5grb2fjvcl8j33nb06jn715drnq961ivb")))

(define-public crate-polars-mongo-0.2.0 (c (n "polars-mongo") (v "0.2.0") (d (list (d (n "mongodb") (r "^2.2.1") (f (quote ("sync"))) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polars") (r "^0.24.0") (f (quote ("lazy" "dtype-full"))) (d #t) (k 0)) (d (n "polars-core") (r "^0.24.0") (d #t) (k 0)) (d (n "polars-time") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1p549y7xy10irpw3mprgp3f0axbfrh2djnp61bv8hf7bmragniq2")))

