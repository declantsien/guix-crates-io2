(define-module (crates-io po la polaris-rust) #:use-module (crates-io))

(define-public crate-polaris-rust-0.1.0 (c (n "polaris-rust") (v "0.1.0") (h "0hp2491vbaarql7kx5z572g49hzaa0w6mcqfa6bl9md74sxk8s20")))

(define-public crate-polaris-rust-0.1.1 (c (n "polaris-rust") (v "0.1.1") (h "0v94wv3hvk4gy764wnzwiky70zzhdmyvrp19yyfkn8l8v7n0c8mg")))

(define-public crate-polaris-rust-0.1.3 (c (n "polaris-rust") (v "0.1.3") (d (list (d (n "polaris-config") (r "^0.1.3") (d #t) (k 0)) (d (n "polaris-core") (r "^0.1.3") (d #t) (k 0)) (d (n "polaris-ratelimit") (r "^0.1.3") (d #t) (k 0)) (d (n "polaris-registry") (r "^0.1.3") (d #t) (k 0)) (d (n "polaris-router") (r "^0.1.3") (d #t) (k 0)))) (h "1vph5zyjfq97pc8h3ika73qbv0czr6d7ljixskh2ahflqhrybwgq")))

