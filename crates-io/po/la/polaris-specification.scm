(define-module (crates-io po la polaris-specification) #:use-module (crates-io))

(define-public crate-polaris-specification-0.1.0 (c (n "polaris-specification") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "01d8mv3zc9fkx3fp52xzcf4in89ygsjp1hy96cp3n0n1gwapg7gj")))

(define-public crate-polaris-specification-0.1.1 (c (n "polaris-specification") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1nfzyivih0hpga64z7bwrjawij3yq7qc0cljf5akc21ig72sxcnr")))

(define-public crate-polaris-specification-0.1.3 (c (n "polaris-specification") (v "0.1.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1gqbncc7cqpjn0hzn7w235avjxgivydfqdsl9s78nny921v50xc5")))

(define-public crate-polaris-specification-1.2.3-alpha.1 (c (n "polaris-specification") (v "1.2.3-alpha.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0im6y27dl3jac4hw76g6sgwlk6mw1vdryxk23wwf6b6f2dls4h9a")))

(define-public crate-polaris-specification-1.3.0 (c (n "polaris-specification") (v "1.3.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1idf7npwqdi6fa2rzb2imzh7y6yl27jbhyprdk9njcjdcw3n3v5y")))

(define-public crate-polaris-specification-1.3.1 (c (n "polaris-specification") (v "1.3.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1qsw9xxfhy2j58dgyfi6445a0i2cjkcfbkfll1w5h2yc9bw4riz7")))

(define-public crate-polaris-specification-1.2.4 (c (n "polaris-specification") (v "1.2.4") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0x76w5rblj9na4jqvl46m0k0k2g85qfq9zzq1lv3c21al2ipcsp6") (y #t)))

(define-public crate-polaris-specification-1.3.2 (c (n "polaris-specification") (v "1.3.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "13wxl6py3nafq1wipk1rdp9vnw1af2v04i36bg13s2kxswyfd732")))

(define-public crate-polaris-specification-1.4.0-alpha.0 (c (n "polaris-specification") (v "1.4.0-alpha.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "06vq55szkq0pzf03xzarkhlaz6rcfb1b26y38rv3g5xbc95mmwi1")))

(define-public crate-polaris-specification-1.4.0-alpha.1 (c (n "polaris-specification") (v "1.4.0-alpha.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0ljfx76ii4a0i3dbiifx62dmx2cjwfb2gzw4n6vhi1cgvx6m39ni")))

(define-public crate-polaris-specification-1.4.0-alpha.2 (c (n "polaris-specification") (v "1.4.0-alpha.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1ldyhgpfsscpkmjkddsj2r53ff1rkqk0hd3wrccmbfwja507iz4w")))

(define-public crate-polaris-specification-1.4.0-alpha.3 (c (n "polaris-specification") (v "1.4.0-alpha.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "02mz46awki12v8zqrh77f2r20pq9y1cgq8vnw5x3yq35546wyn86")))

(define-public crate-polaris-specification-1.4.0-alpha.4 (c (n "polaris-specification") (v "1.4.0-alpha.4") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1ixj6569i50w7l7falrs4gj6xjzknzvirsszd656jsh23vz7prcs")))

(define-public crate-polaris-specification-1.4.0-alpha.5 (c (n "polaris-specification") (v "1.4.0-alpha.5") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1xkwsiv9lxgq4hfmvch0wb82ilc8viz1jbpxavg6rl04dg11b7nb")))

(define-public crate-polaris-specification-1.4.0-alpha.6 (c (n "polaris-specification") (v "1.4.0-alpha.6") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0q4f29863ng2j7fvxiqagapi1br21v2mg8w167yx40mn0sg4w5yp")))

(define-public crate-polaris-specification-1.4.0-alpha.7 (c (n "polaris-specification") (v "1.4.0-alpha.7") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1h848q618dc20svsz0kaw26p9l2x0922ww2759mxmhx03grsibxz")))

(define-public crate-polaris-specification-1.4.0 (c (n "polaris-specification") (v "1.4.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0jl57kz8skw2mmj12pnw5s8gbkdf5l6andcdwr2v8y1wx4dadch7")))

(define-public crate-polaris-specification-1.4.1-alpha (c (n "polaris-specification") (v "1.4.1-alpha") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1q5rls1z21w5fq72pnm2pipcg5jhvb7dlj4nbbi638ahkpp4fbrd")))

(define-public crate-polaris-specification-1.4.1 (c (n "polaris-specification") (v "1.4.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "01j6g7shwbl63kpnmwdaa21az99cmiffiicg998xsh006yyhv5vd")))

(define-public crate-polaris-specification-1.4.2-alpha (c (n "polaris-specification") (v "1.4.2-alpha") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0ira0jqnn9cak7r7kfbccf7df9ghyn59j7rq0w3iyqdhrdyqi11m")))

(define-public crate-polaris-specification-1.4.2-alpha.1 (c (n "polaris-specification") (v "1.4.2-alpha.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0cni31x33x14i58q78ich6ms77976snmbism9pc9jzapydnyzrpf")))

(define-public crate-polaris-specification-1.4.2-alpha.3 (c (n "polaris-specification") (v "1.4.2-alpha.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0k8n97ibbblxi5qiriwrzwd9jbz48xgqsrr9falzacx9dsb3cwmr")))

(define-public crate-polaris-specification-1.4.2-alpha.4 (c (n "polaris-specification") (v "1.4.2-alpha.4") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1g774nzi67q02m2k2xb80648788x42pm66zqphyw3krr0907icrc")))

(define-public crate-polaris-specification-1.4.2-alpha.5 (c (n "polaris-specification") (v "1.4.2-alpha.5") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "1qjq2gqd9q4drs1vi39cdgkhgm9fpydqwpbcx0212kkcl3k0w9g3")))

(define-public crate-polaris-specification-1.4.2-alpha.6 (c (n "polaris-specification") (v "1.4.2-alpha.6") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0yg8jcw17n8bchcs77fdjlahzml6g74l8b1aa27s2wchakg70bii")))

(define-public crate-polaris-specification-1.4.2-alpha.7 (c (n "polaris-specification") (v "1.4.2-alpha.7") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0ljrnsavmk9rj2d5m808g7bgw3305lw59xp783ar33c1i53fl32f")))

(define-public crate-polaris-specification-1.4.2-alpha.8 (c (n "polaris-specification") (v "1.4.2-alpha.8") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "13nmcanvjr5i1mjb2vi0fqckpqk8fcll98hzpnn15n00r5d6xayc")))

(define-public crate-polaris-specification-1.4.2-alpha.9 (c (n "polaris-specification") (v "1.4.2-alpha.9") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "13r04hlg8qx33c2a186arza617jn80nvg2ll6qipz5xhzv7wxrbl")))

(define-public crate-polaris-specification-1.4.2 (c (n "polaris-specification") (v "1.4.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0g2r3n3gaai4dyls94h46bk2nv77psfc7mwvx8pjf79rjdbq2b66")))

(define-public crate-polaris-specification-1.5.0 (c (n "polaris-specification") (v "1.5.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "13b4lini8r1vjld7lxd8a0q0dl98y33kcfy3mv6cj3q5x5zvv4l4")))

(define-public crate-polaris-specification-1.5.1 (c (n "polaris-specification") (v "1.5.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)))) (h "0xgsavz3w6ri56slzncm6g52sl7pgh3wgnnq1i4g5amay20x2cyj")))

