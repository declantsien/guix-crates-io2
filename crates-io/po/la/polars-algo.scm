(define-module (crates-io po la polars-algo) #:use-module (crates-io))

(define-public crate-polars-algo-0.24.0 (c (n "polars-algo") (v "0.24.0") (d (list (d (n "polars-core") (r "^0.24.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.24.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "0mrak7l6rb921isc2m5ybgk803ylsycxrwqd40gi4iyx92d1pnkz")))

(define-public crate-polars-algo-0.24.1 (c (n "polars-algo") (v "0.24.1") (d (list (d (n "polars-core") (r "^0.24.1") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.24.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "0cw3vvn8vf6yx2zd6p4spjbp3065xcpvq6f6dwx5zpmff2m6lslk")))

(define-public crate-polars-algo-0.24.2 (c (n "polars-algo") (v "0.24.2") (d (list (d (n "polars-core") (r "^0.24.2") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.24.2") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "1rwfay1m779pxd3l3vbjap73blzakm8zggfcgmy5qdky1nfk54q5")))

(define-public crate-polars-algo-0.24.3 (c (n "polars-algo") (v "0.24.3") (d (list (d (n "polars-core") (r "^0.24.3") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.24.3") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "036qg2c74g7zbgqm6xr3bgvavvkpvszg6nvki2a4bzapxs3sw76i")))

(define-public crate-polars-algo-0.24.4 (c (n "polars-algo") (v "0.24.4") (d (list (d (n "polars-core") (r "^0.24.4") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.24.4") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "18yry53ww5yxsi3hd36pif1ny9j1xzisczh57i56v1xm6bgmwsj8")))

(define-public crate-polars-algo-0.25.0 (c (n "polars-algo") (v "0.25.0") (d (list (d (n "polars-core") (r "^0.25.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.25.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "1524abrdyi67ihfjs6nb871jdihnhi9dl7zld418q9vbrg6lg92c")))

(define-public crate-polars-algo-0.25.1 (c (n "polars-algo") (v "0.25.1") (d (list (d (n "polars-core") (r "^0.25.1") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.25.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)))) (h "13qq5i4j0v400h3qq8w9y9jmc4hd4mlhxy1rq2bmgjpxilgcf8jm")))

(define-public crate-polars-algo-0.26.0 (c (n "polars-algo") (v "0.26.0") (d (list (d (n "polars-core") (r "^0.26.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.26.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.26.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0m52v01r7psypyfgy81csjkd8z7hiqvyckddynjfx4izczvq5acc")))

(define-public crate-polars-algo-0.26.1 (c (n "polars-algo") (v "0.26.1") (d (list (d (n "polars-core") (r "^0.26.1") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.26.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.26.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "185agpv07grcd9p5kfv6dfkz2wah7rk7vymh7prczhjvghmza3lg")))

(define-public crate-polars-algo-0.27.1 (c (n "polars-algo") (v "0.27.1") (d (list (d (n "polars-core") (r "^0.27.1") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.27.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.27.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "1177271nn715p4yr5v9ynd5ygv3csv56c8yklxjs3wwbhyi0f5q2")))

(define-public crate-polars-algo-0.27.2 (c (n "polars-algo") (v "0.27.2") (d (list (d (n "polars-core") (r "^0.27.2") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.27.2") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.27.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "107hkljmmh7ni9sqalqx1xpz8c7sbmda7ic39qwz09p8b2xikq1w")))

(define-public crate-polars-algo-0.28.0 (c (n "polars-algo") (v "0.28.0") (d (list (d (n "polars-core") (r "^0.28.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.28.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.28.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "1h6yicjw25h2rzgvmrlmrqh5fbb37nkk8ai6x6kpv8nq30yg2ci5")))

(define-public crate-polars-algo-0.29.0 (c (n "polars-algo") (v "0.29.0") (d (list (d (n "polars-core") (r "^0.29.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.29.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.29.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0h6qqznv3138s0n39h9c5mn876m0ayisq9620sm65briq92pc6ai")))

(define-public crate-polars-algo-0.30.0 (c (n "polars-algo") (v "0.30.0") (d (list (d (n "polars-core") (r "^0.30.0") (f (quote ("private" "dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.30.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.30.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0db5gssj15g62mjflji1rrhkcjk37vc6nh144r0fiivbmqg7qnlq")))

(define-public crate-polars-algo-0.31.0 (c (n "polars-algo") (v "0.31.0") (d (list (d (n "polars-core") (r "^0.31.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.31.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.31.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "1v4cfncbgzbagihljjsnmj05vavblnfpjgmlfs0sxmncl61yrn1j")))

(define-public crate-polars-algo-0.31.1 (c (n "polars-algo") (v "0.31.1") (d (list (d (n "polars-core") (r "^0.31.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.31.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.31.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0yzzrvllxsd44argc3p0k23xhd3qqffiz048zdqmm2xk44a4yn09")))

(define-public crate-polars-algo-0.32.0 (c (n "polars-algo") (v "0.32.0") (d (list (d (n "polars-core") (r "^0.32.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.32.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.32.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0y5l02sgkv1jkw4g1wvwr3kikcy4clri64adrrbk9mnr5si6fpcq")))

(define-public crate-polars-algo-0.32.1 (c (n "polars-algo") (v "0.32.1") (d (list (d (n "polars-core") (r "^0.32.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.32.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.32.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0y7pyg7piywl0399j0nvwg0qa2cb521kwn7qp43365mmr3pi5j1g")))

(define-public crate-polars-algo-0.33.0 (c (n "polars-algo") (v "0.33.0") (d (list (d (n "polars-core") (r "^0.33.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.33.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.33.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0r1hcj8x3m98mzxc4m971z06ra79ks715j1v25w6b7fxxzn00y9j")))

(define-public crate-polars-algo-0.33.1 (c (n "polars-algo") (v "0.33.1") (d (list (d (n "polars-core") (r "^0.33.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.33.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.33.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0g5q5b7zsx0pl3hmxrcf9ca1rs0xmsil1kwcvamfxadlpd2pqigv")))

(define-public crate-polars-algo-0.33.2 (c (n "polars-algo") (v "0.33.2") (d (list (d (n "polars-core") (r "^0.33.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.33.2") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.33.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "03lr7yhj9w4v42a8mha4gc33baf2fl78hs03a78lvfvyfrr6a4xd")))

(define-public crate-polars-algo-0.34.0 (c (n "polars-algo") (v "0.34.0") (d (list (d (n "polars-core") (r "^0.34.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.34.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.34.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "1k64fb597dgb6hbvl1kkd0pwy035na91zy6znjpbbjy4akxpjca5")))

(define-public crate-polars-algo-0.34.1 (c (n "polars-algo") (v "0.34.1") (d (list (d (n "polars-core") (r "^0.34.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.34.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.34.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0hxbammlgzbww0xbzz17zqrg8p9296xgg2n9vxdwrx3rmrv8l0pg")))

(define-public crate-polars-algo-0.34.2 (c (n "polars-algo") (v "0.34.2") (d (list (d (n "polars-core") (r "^0.34.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.34.2") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.34.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "18sfc1r9dnc3a37ap1h3d8j3i2mqkcmrm8sy48la1dkh5hpchf3y")))

(define-public crate-polars-algo-0.35.0 (c (n "polars-algo") (v "0.35.0") (d (list (d (n "polars-core") (r "^0.35.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.35.0") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.35.0") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0q3b0qskbg5lf6gfplk7yda03rkzbhbyyyrijl5pxr78hlda4bqj")))

(define-public crate-polars-algo-0.35.1 (c (n "polars-algo") (v "0.35.1") (d (list (d (n "polars-core") (r "^0.35.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.35.1") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.35.1") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0drkwanbcyhpcsxkw6ysip7sg6b32rlrmvnwdlqvhv1l02gr1ig4")))

(define-public crate-polars-algo-0.35.2 (c (n "polars-algo") (v "0.35.2") (d (list (d (n "polars-core") (r "^0.35.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.35.2") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.35.2") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "1fprx45yv4xpqg5a9r4l2x4jyyxck6cqcaf7lvzpnv8qw9b5xhz2")))

(define-public crate-polars-algo-0.35.3 (c (n "polars-algo") (v "0.35.3") (d (list (d (n "polars-core") (r "^0.35.3") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.35.3") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.35.3") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0bhz016fmgcj312wqmgxfjnd5c4xbrj0f3f9s38kx5mdgjy7zsqj")))

(define-public crate-polars-algo-0.35.4 (c (n "polars-algo") (v "0.35.4") (d (list (d (n "polars-core") (r "^0.35.4") (f (quote ("dtype-categorical" "asof_join"))) (k 0)) (d (n "polars-lazy") (r "^0.35.4") (f (quote ("asof_join" "concat_str" "strings"))) (d #t) (k 0)) (d (n "polars-ops") (r "^0.35.4") (f (quote ("dtype-categorical" "asof_join"))) (k 0)))) (h "0iyqz84gv8b56w1aljybwllibs9qkq89qn3v7fqzl4iyzy4p117n")))

