(define-module (crates-io po la polars-row-derive) #:use-module (crates-io))

(define-public crate-polars-row-derive-0.1.0 (c (n "polars-row-derive") (v "0.1.0") (d (list (d (n "polars") (r "^0.39.1") (f (quote ("lazy"))) (k 2)) (d (n "proc-macro2") (r "^1.0.74") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1rs83jn4fjbz6ddmvhb00m25msbk020bdmm6qpiffa2z50ibj9cq")))

