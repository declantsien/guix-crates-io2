(define-module (crates-io po la polaris-rs) #:use-module (crates-io))

(define-public crate-polaris-rs-0.1.0 (c (n "polaris-rs") (v "0.1.0") (h "04c389syrl5aj634pakbmjk2s1abs5159lg5qqbpvmpin393sb6d")))

(define-public crate-polaris-rs-0.1.1 (c (n "polaris-rs") (v "0.1.1") (h "18rka1qqv1325gq50fsx2g0hk18j4bj07y3f11zwm6h0wk1pm3gn")))

