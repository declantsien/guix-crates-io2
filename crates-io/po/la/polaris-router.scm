(define-module (crates-io po la polaris-router) #:use-module (crates-io))

(define-public crate-polaris-router-0.1.2 (c (n "polaris-router") (v "0.1.2") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1makqhmfwiqd2z44w4n7hviarggj4gkkmsy2a3iaywf7mfvqbmfm")))

(define-public crate-polaris-router-0.1.3 (c (n "polaris-router") (v "0.1.3") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0frh20i28zmrh617jrjcjfsnf3mxk0s3dy5ih7lvq69pikrxg5l9")))

