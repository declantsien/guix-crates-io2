(define-module (crates-io po la polars-ai) #:use-module (crates-io))

(define-public crate-polars-ai-0.0.0 (c (n "polars-ai") (v "0.0.0") (h "0cswmcddnkwad5kp7sydslsmkn9crngsvrplyq0dppyhqf13vfyw")))

(define-public crate-polars-ai-0.0.1 (c (n "polars-ai") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eval") (r "^0.4.3") (d #t) (k 0)) (d (n "evalexpr") (r "^11.1.0") (d #t) (k 0)) (d (n "polars") (r "^0.34.2") (f (quote ("csv"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkj3mx66y2lgpm3b7fzvr9h9qgnifvp6dvybrfz8353ll9k9k6p")))

(define-public crate-polars-ai-0.0.2 (c (n "polars-ai") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eval") (r "^0.4.3") (d #t) (k 0)) (d (n "evalexpr") (r "^11.1.0") (d #t) (k 0)) (d (n "polars") (r "^0.34.2") (f (quote ("csv"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d33nrpa9lfbja602wx2qdfh4iww33b503sbmjfksydnkywmayl1")))

