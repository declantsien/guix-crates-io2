(define-module (crates-io po la polaris-registry) #:use-module (crates-io))

(define-public crate-polaris-registry-0.1.2 (c (n "polaris-registry") (v "0.1.2") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "1vxk3d25cw87930p8xvb2ldnmscxm2p29jjnic12lk28xqnm9j32")))

(define-public crate-polaris-registry-0.1.3 (c (n "polaris-registry") (v "0.1.3") (d (list (d (n "polaris-specification") (r "^0.1.1") (d #t) (k 0)))) (h "0hq9cd86j3pryn62nqswikg0qkxbjvba13adarjxhchhwyblh8z8")))

