(define-module (crates-io po ck pocket-rtc) #:use-module (crates-io))

(define-public crate-pocket-rtc-0.1.0 (c (n "pocket-rtc") (v "0.1.0") (h "138hd4r4sbr2iba646nrp9svggzm3l36g4kpz9mjfzsv67mzqcsk")))

(define-public crate-pocket-rtc-0.1.1 (c (n "pocket-rtc") (v "0.1.1") (h "0q2wgwqvg577x80468fvsf3azcsml5sbi1mcgpaylwd9dk3dmmq5")))

