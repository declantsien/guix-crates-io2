(define-module (crates-io po ck pocket-relay-mitm) #:use-module (crates-io))

(define-public crate-pocket-relay-mitm-0.1.0 (c (n "pocket-relay-mitm") (v "0.1.0") (d (list (d (n "blaze-pk") (r "^0.8") (d #t) (k 0)) (d (n "blaze-ssl-async") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (f (quote ("gzip" "console_appender" "file_appender"))) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "parking_lot" "rt-multi-thread" "signal" "sync"))) (d #t) (k 0)))) (h "16r4zxb9b9nm6qpl437d3zyx3366py09gazlmiv8pm3h13gp2psf")))

