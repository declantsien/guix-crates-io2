(define-module (crates-io po ck pocketsphinx) #:use-module (crates-io))

(define-public crate-pocketsphinx-0.1.0 (c (n "pocketsphinx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1jqc54qykj5v0d8glny1361lmrgi8smjwr7i4nici5y6m0dn0djd")))

(define-public crate-pocketsphinx-0.2.0 (c (n "pocketsphinx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.2.*") (d #t) (k 0)))) (h "0dz6q5z3yn3nkz9cxs7js5fn067j0ykxdz16mp66mjy5d558yk82")))

(define-public crate-pocketsphinx-0.3.0 (c (n "pocketsphinx") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.3.*") (d #t) (k 0)))) (h "03b8whq4mqf3ibklbqxafx7xvk4fq3742mcrnrllmhgm30s95rx6")))

(define-public crate-pocketsphinx-0.3.1 (c (n "pocketsphinx") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.3.*") (d #t) (k 0)))) (h "1c5a0nv9pbkn4vsjrw2ndpj7kkipw35krvdj0jy0yfzbxix304y2")))

(define-public crate-pocketsphinx-0.4.0 (c (n "pocketsphinx") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.4.*") (d #t) (k 0)))) (h "1wavr1ypb1zc0bsq8sjgqc2phjfi1rv5j1ram5r8ydgr13zwca4g")))

(define-public crate-pocketsphinx-0.5.0 (c (n "pocketsphinx") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.4.*") (d #t) (k 0)))) (h "1h7xhzfp9skjrg26dbr3z7a5694vvg2sppv0ds67n96prkdy9rgc")))

(define-public crate-pocketsphinx-0.6.0 (c (n "pocketsphinx") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pocketsphinx-sys") (r "0.5.*") (d #t) (k 0)))) (h "1lkbrvc5jp0b8zsnsr2mbayh4gr6f9gy7c4pbs09fcgziy7fvgvz")))

