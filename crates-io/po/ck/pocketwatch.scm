(define-module (crates-io po ck pocketwatch) #:use-module (crates-io))

(define-public crate-pocketwatch-0.1.0 (c (n "pocketwatch") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0hhx6a4bg7s84w3h382kws2y9ryx6h9xqx4savnbrjk10yn99nfi")))

(define-public crate-pocketwatch-0.1.1 (c (n "pocketwatch") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "web-audio-api") (r "^0.43.0") (d #t) (k 0)))) (h "1pfbbalhq3cbfq96za30a6h5d583g7igb15jndhyd22403y8n2lx")))

