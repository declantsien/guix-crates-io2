(define-module (crates-io po ck pocket_prover-set) #:use-module (crates-io))

(define-public crate-pocket_prover-set-0.1.0 (c (n "pocket_prover-set") (v "0.1.0") (d (list (d (n "pocket_prover") (r "^0.4.0") (d #t) (k 0)))) (h "1ci8bswvcwx2kzzfv5ij4gf0hiq9dwyacj56fh4hcql30f9j5bw2")))

(define-public crate-pocket_prover-set-0.2.0 (c (n "pocket_prover-set") (v "0.2.0") (d (list (d (n "pocket_prover") (r "^0.5.0") (d #t) (k 0)))) (h "0scpiv1brri623jrdxj2zxmkn071kd8j0q31zn768q52fz1c2nfp")))

(define-public crate-pocket_prover-set-0.2.1 (c (n "pocket_prover-set") (v "0.2.1") (d (list (d (n "pocket_prover") (r "^0.5.0") (d #t) (k 0)) (d (n "pocket_prover-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1wbi1qcj9chb6jdlks0gbrmh7kxvm1hrxgvkzp0fc7m7kn82ckig")))

(define-public crate-pocket_prover-set-0.3.0 (c (n "pocket_prover-set") (v "0.3.0") (d (list (d (n "pocket_prover") (r "^0.6.0") (d #t) (k 0)) (d (n "pocket_prover-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0yr0mvbccas4xwliirn5xjl4qz9das2gfyag1v8x4gdnwfn616br")))

(define-public crate-pocket_prover-set-0.4.0 (c (n "pocket_prover-set") (v "0.4.0") (d (list (d (n "pocket_prover") (r "^0.8.0") (d #t) (k 0)) (d (n "pocket_prover-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0afxjmcizbmv2jh9iwq4yajlw7bly35mbvnv0ybbw2x3lb3hdf8i")))

(define-public crate-pocket_prover-set-0.5.0 (c (n "pocket_prover-set") (v "0.5.0") (d (list (d (n "pocket_prover") (r "^0.17.0") (d #t) (k 0)) (d (n "pocket_prover-derive") (r "^0.8.0") (d #t) (k 0)))) (h "0kmy8bqhkjl4iykgpr1ii5sf0l4jhw37d9w9293nk11aqv6j0zqn")))

