(define-module (crates-io po ck pocket-resources) #:use-module (crates-io))

(define-public crate-pocket-resources-0.1.0 (c (n "pocket-resources") (v "0.1.0") (h "11574ww0qsv8bzi5xr8gpr4lllhb835n4avchm0bqw774rpbwrlb")))

(define-public crate-pocket-resources-0.1.1 (c (n "pocket-resources") (v "0.1.1") (h "1fkjjisnqwiwjk7k6qwwpkkcs9m2kg3322rlm8qqc3xby7r3j3k3")))

(define-public crate-pocket-resources-0.1.2 (c (n "pocket-resources") (v "0.1.2") (h "1p993anncgqq2i772w1j0rdw8pfkydx3w8fy6mcn4vc5s077gqzr")))

(define-public crate-pocket-resources-0.2.0 (c (n "pocket-resources") (v "0.2.0") (h "1y86wb3vpn8dq0qc9j2kj7zvbck11ks9vnd7l1d12r61916b62vs")))

(define-public crate-pocket-resources-0.2.1 (c (n "pocket-resources") (v "0.2.1") (h "14p81bw36zy3zp24il4lrjqr8yjpbngkl6ghv63b0wk7bn30vd1p")))

(define-public crate-pocket-resources-0.2.2 (c (n "pocket-resources") (v "0.2.2") (h "1rz875dydmzwm52l4hkixab21hsmjk30k4br3yg8l19n8b6ry4sk")))

(define-public crate-pocket-resources-0.3.0 (c (n "pocket-resources") (v "0.3.0") (h "1996z7xidxz4hi25pzn1riwmv7mc8xiq15j8683l16cw3mnf6dc0")))

(define-public crate-pocket-resources-0.3.1 (c (n "pocket-resources") (v "0.3.1") (h "13vv8jvc5wmmfmwgm5fggcfd1nb62nhnfvlvchj5kh7hj78lpzzy")))

(define-public crate-pocket-resources-0.3.2 (c (n "pocket-resources") (v "0.3.2") (h "1n2i5vmi8fdbw89wm5nz1ws1z9f1qax911p6ksg4scmdg23z6df1")))

