(define-module (crates-io po ck pocket-relay-client) #:use-module (crates-io))

(define-public crate-pocket-relay-client-0.1.1 (c (n "pocket-relay-client") (v "0.1.1") (d (list (d (n "blaze-pk") (r "^0.2.5") (d #t) (k 0)) (d (n "blaze-ssl") (r "^0.1.2") (d #t) (k 0)) (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.19.0") (f (quote ("image"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1585fln1rd90s1lj8fp3qqj2wdggynhpr7nfgwr2nalp67x5c31l")))

