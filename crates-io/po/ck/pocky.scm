(define-module (crates-io po ck pocky) #:use-module (crates-io))

(define-public crate-pocky-0.1.0 (c (n "pocky") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)))) (h "0ikhq6lgr8wl2j5m9mibq135gxc4imq0n7xshk9c23ib2jbyhyvd")))

(define-public crate-pocky-0.1.1 (c (n "pocky") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)))) (h "1hab2krcdgmkl4csnygahig4jqg8gagx4igjmrkrxjl5wqq8lmcv")))

(define-public crate-pocky-0.2.0 (c (n "pocky") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "=1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)))) (h "13xxb967krk364ly4hd0n4p7645ibpdqkhnz29scxmj7jc5r9bd9")))

(define-public crate-pocky-0.3.0 (c (n "pocky") (v "0.3.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "=1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)))) (h "03x4mnf17a98pd2h45mgi42j7c158s053ybi1xflb5ja2957pjks")))

(define-public crate-pocky-0.4.0 (c (n "pocky") (v "0.4.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "=1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)))) (h "1rqw5l7slzx9bky8aslirfgh3nbxighmpmb0mi2ffal6gcrrxqr4")))

(define-public crate-pocky-0.4.1 (c (n "pocky") (v "0.4.1") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "=1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)))) (h "1jcafp4lf6v44561c0v3qn9ci1jwlp7h4sk5dy7d2d7v1xqms6i9")))

(define-public crate-pocky-0.5.0 (c (n "pocky") (v "0.5.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "03p5zgjqw85qvkgcqs00nacwy63kf1g61nrm3k86irr5z8m8crvn")))

(define-public crate-pocky-0.5.1 (c (n "pocky") (v "0.5.1") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "0hpi684cy3lnpqzl1raj9y1lxxgqqa38nsf9mqwxqg2w5k6wi2cz")))

(define-public crate-pocky-0.5.2 (c (n "pocky") (v "0.5.2") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "0ij0rg8mh677qlv31kphsclplxxbnj1gp012f91a4kw3gyz1xb0z")))

