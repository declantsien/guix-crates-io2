(define-module (crates-io po ck pocketsphinx-sys) #:use-module (crates-io))

(define-public crate-pocketsphinx-sys-0.1.0 (c (n "pocketsphinx-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "019v9ki8kcff9d3pszrv26pvf41wa34p4kvr4020yj4kp7hxnxza")))

(define-public crate-pocketsphinx-sys-0.2.0 (c (n "pocketsphinx-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1n4vcycr51h9ddn8rdkw82k21wbpyl3njwiqn0xd47w7wi1r546m")))

(define-public crate-pocketsphinx-sys-0.3.0 (c (n "pocketsphinx-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1lw2cqwca0wwphj1vy4aw9dmq98h8h2w947akpl3axwyzid4vs6q")))

(define-public crate-pocketsphinx-sys-0.3.1 (c (n "pocketsphinx-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "14caq1qpidm0434aqahb6fa1fw72akhvv5pjmqrs5mbr6l3gipq4")))

(define-public crate-pocketsphinx-sys-0.4.0 (c (n "pocketsphinx-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0jm0ypgkjdimcbmr0gnyiy8snk05p9p331a8vxl5pfpfnqi2pja5")))

(define-public crate-pocketsphinx-sys-0.5.0 (c (n "pocketsphinx-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0chic1pqgbn1r2p4wx1zncbhqmdhd1l9d3brkrzyrji13hg7qdil")))

