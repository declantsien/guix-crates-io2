(define-module (crates-io po ck pocketcraft_engine_api) #:use-module (crates-io))

(define-public crate-pocketcraft_engine_api-0.1.1 (c (n "pocketcraft_engine_api") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "1ivf50c16kmhqym2j6vdf92i74azq3fqsym5g0bg84arrjbz1haw")))

(define-public crate-pocketcraft_engine_api-0.1.1-alpha (c (n "pocketcraft_engine_api") (v "0.1.1-alpha") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "10fp1wqjhs324cbmk6d1qq60qdj0nj17ydia1v29jpmyhmxh18r0")))

(define-public crate-pocketcraft_engine_api-0.1.2 (c (n "pocketcraft_engine_api") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "1g8izw2y1jkfaxyfvwpl4d8givblbys3z66n2i120y578rh5s671")))

(define-public crate-pocketcraft_engine_api-0.1.3 (c (n "pocketcraft_engine_api") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "139jss69laskvh771v19kmgmcxqb6am50n0qp1j6528qvj2livkj")))

(define-public crate-pocketcraft_engine_api-0.1.4 (c (n "pocketcraft_engine_api") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "0qhgp4d5h712dvq884223w8g5cbrcdk86mlybbv4ni0n9gpiq1h0")))

(define-public crate-pocketcraft_engine_api-0.1.5 (c (n "pocketcraft_engine_api") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "11y5cnlbz6n7in9l4mw5bdjyx390fb7qyd1pgwmkfbf84fsnfiag")))

(define-public crate-pocketcraft_engine_api-0.1.6 (c (n "pocketcraft_engine_api") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "1r3933llb73p9ng16nn35p97nr3xh0fi86748salcgfv3vaxp56f")))

(define-public crate-pocketcraft_engine_api-0.1.7 (c (n "pocketcraft_engine_api") (v "0.1.7") (d (list (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "1454ryvi3d8kj17f0m0lbviilyqsw1kkxpy91w835ms0nkl8ah71")))

(define-public crate-pocketcraft_engine_api-0.1.8 (c (n "pocketcraft_engine_api") (v "0.1.8") (d (list (d (n "uuid") (r "^1.8.0") (f (quote ("v3"))) (d #t) (k 0)))) (h "0jgzscnhz8670c4a53xp3da8zpk6ln18z872s2jfyby3y33yyapb")))

