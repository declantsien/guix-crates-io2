(define-module (crates-io po ck pocket_prover) #:use-module (crates-io))

(define-public crate-pocket_prover-0.1.0 (c (n "pocket_prover") (v "0.1.0") (h "0s7nmf6rnh6gm8pkrn3nrxn20x7frgykc2lbxfxdzbhhn30cw6hi")))

(define-public crate-pocket_prover-0.2.0 (c (n "pocket_prover") (v "0.2.0") (h "1hk3limk4ny9r91bql1h1mjkfblvwc30h0waa276nniqkffpap33")))

(define-public crate-pocket_prover-0.3.0 (c (n "pocket_prover") (v "0.3.0") (h "083bjfn2kjy5mhxvmfkig1xjv3f5fxc8m78m0n0sqifi9rw3qxil")))

(define-public crate-pocket_prover-0.3.1 (c (n "pocket_prover") (v "0.3.1") (h "1sr8hrb7dcwl0nfhw59gflyw9svp6ip33hl5r2amp0zj9pblmzq5")))

(define-public crate-pocket_prover-0.3.2 (c (n "pocket_prover") (v "0.3.2") (h "0x6sczl7aar7b4r9hkrqrmqwhqkbdrxafbapf9405xskba4s0r2m")))

(define-public crate-pocket_prover-0.3.3 (c (n "pocket_prover") (v "0.3.3") (h "1g0a1qwk2iv66aa10k52djka7kic2j2w4f0f77jck8p1g4qz8qap")))

(define-public crate-pocket_prover-0.4.0 (c (n "pocket_prover") (v "0.4.0") (h "04ypacgqay97wavmh8nadfqmylaj6v7p4xawqh2bznsykqw2fs4y")))

(define-public crate-pocket_prover-0.5.0 (c (n "pocket_prover") (v "0.5.0") (h "1ksr9vkjad8y4snb2jr8hhdpg1qmi1y6m2khiwnhvnkdi9d402lk")))

(define-public crate-pocket_prover-0.5.1 (c (n "pocket_prover") (v "0.5.1") (h "19y80ighmb7ajr49g644m28kc1bziaq6rdgf0psl6p3g40xczdl4")))

(define-public crate-pocket_prover-0.6.0 (c (n "pocket_prover") (v "0.6.0") (h "01w78jnln86c7jafsqsw4bji9qxh4rznwkj4fswzjysqw6zi5n85")))

(define-public crate-pocket_prover-0.7.0 (c (n "pocket_prover") (v "0.7.0") (h "0qsqdld4yazzdr2hrdlz6r0qx3a32xriv2gd5xsnqiwjvichjqpc")))

(define-public crate-pocket_prover-0.7.1 (c (n "pocket_prover") (v "0.7.1") (h "1aljz4788phi5lkqrl1adlp64cd3ls2hakajbw738h5hjj3dp302")))

(define-public crate-pocket_prover-0.8.0 (c (n "pocket_prover") (v "0.8.0") (h "0jbxzdzrvqaxxi4srcg9v6n1qm85796dysnriwcsvcy45i0b0s75")))

(define-public crate-pocket_prover-0.9.0 (c (n "pocket_prover") (v "0.9.0") (h "1wskbd7a6kr2sdwjcfq2gfvgv4vz3rvvm3l8lkycnavgwvv3cd45")))

(define-public crate-pocket_prover-0.9.1 (c (n "pocket_prover") (v "0.9.1") (h "0gnrn7gmiqg6vwvyi3g9r2vifd8scn7znd55jx9ygl0wk9dw9wbm")))

(define-public crate-pocket_prover-0.10.0 (c (n "pocket_prover") (v "0.10.0") (h "17n528kkn9yshg2rv1jng3f592skqb8905xjwab362195c3ijapm")))

(define-public crate-pocket_prover-0.11.0 (c (n "pocket_prover") (v "0.11.0") (h "0kgfsw3sx6dh433yf5pyq0l7r31wcc0nf76x9pkbk7vcksh29iyj")))

(define-public crate-pocket_prover-0.11.1 (c (n "pocket_prover") (v "0.11.1") (h "0nibrnf8673gql0jklcm97k42fjlpv96l7d5cyv6qqm4b9vfyv4r")))

(define-public crate-pocket_prover-0.12.0 (c (n "pocket_prover") (v "0.12.0") (h "1zw1fp50c0pl5agm1k52001l70qkskdnwgrnfkblfh4l63rqdsjs") (y #t)))

(define-public crate-pocket_prover-0.13.0 (c (n "pocket_prover") (v "0.13.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "1c0l41ihvg9sp0bnmyxfdd3h4j1cqdqbwagc1y8gfzyba1d70cq7")))

(define-public crate-pocket_prover-0.13.1 (c (n "pocket_prover") (v "0.13.1") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0gyn41qpx2r1d3w7wp1hpac7cx2n78xyazbbyl9kl0344jjxm225")))

(define-public crate-pocket_prover-0.13.2 (c (n "pocket_prover") (v "0.13.2") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0h6wrvzr553pjwc125sfxaykjqqisihm86yji3xi3pdqi3gs908n")))

(define-public crate-pocket_prover-0.14.0 (c (n "pocket_prover") (v "0.14.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0skvdiy2gfifkni8l4msi8yq67a1282knnssz5rc2nnn7pz1x5is")))

(define-public crate-pocket_prover-0.14.1 (c (n "pocket_prover") (v "0.14.1") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0553v1x30y36g95bwhd2q5xzdiihdfl33xrsapwi17x6fq46xh9h")))

(define-public crate-pocket_prover-0.15.0 (c (n "pocket_prover") (v "0.15.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "07y5dyqxmp8vhw3y4p5gzdwvnv1wzq1r6ay11idjpk3h1ql2ql3w")))

(define-public crate-pocket_prover-0.16.0 (c (n "pocket_prover") (v "0.16.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0b6drgzmhmyqn1dhmx8kj6l06qljl059rbzn5mpndnzbs3lxypd9")))

(define-public crate-pocket_prover-0.17.0 (c (n "pocket_prover") (v "0.17.0") (d (list (d (n "current") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)))) (h "0winz38xympmjzhg0zklhvimq2827z5n4l226fdi472b84d7m1m4")))

