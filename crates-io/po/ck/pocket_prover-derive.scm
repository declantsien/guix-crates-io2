(define-module (crates-io po ck pocket_prover-derive) #:use-module (crates-io))

(define-public crate-pocket_prover-derive-0.1.0 (c (n "pocket_prover-derive") (v "0.1.0") (d (list (d (n "pocket_prover") (r "^0.5.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "00c83lydl5lyhwis867fkadk1vpw7qdh09kyah3iwmszh3s01jii")))

(define-public crate-pocket_prover-derive-0.2.0 (c (n "pocket_prover-derive") (v "0.2.0") (d (list (d (n "pocket_prover") (r "^0.6.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1c03ab7mpv3z08vwblg4jay3fqbnsvikjr24hfnl6xb92m3zqdql")))

(define-public crate-pocket_prover-derive-0.3.0 (c (n "pocket_prover-derive") (v "0.3.0") (d (list (d (n "pocket_prover") (r "^0.7.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1x0hwrc6h9dz141gqv9b0rqwx89pb8pxn1sgsmc12am15pcx04zp")))

(define-public crate-pocket_prover-derive-0.4.0 (c (n "pocket_prover-derive") (v "0.4.0") (d (list (d (n "pocket_prover") (r "^0.8.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "08v62mp92kl8nxch9q6xdnpgpl5ar08bhdvjkws7ic9d0mrs4qkw")))

(define-public crate-pocket_prover-derive-0.5.0 (c (n "pocket_prover-derive") (v "0.5.0") (d (list (d (n "pocket_prover") (r "^0.9.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0cykv12zkk2pr5kwx750r801l6zijkvkl26vf25a752hgj2vvp79")))

(define-public crate-pocket_prover-derive-0.6.0 (c (n "pocket_prover-derive") (v "0.6.0") (d (list (d (n "pocket_prover") (r "^0.12.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0c6xlmp215hydc8lkl6ah5hj51yry9jip11gm7kpkl71jg0qm4si") (y #t)))

(define-public crate-pocket_prover-derive-0.7.0 (c (n "pocket_prover-derive") (v "0.7.0") (d (list (d (n "pocket_prover") (r "^0.13.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0x5i5my74h333qpy6zgmsfapl3xsz43x3q9nng625qjr8p05vq2z")))

(define-public crate-pocket_prover-derive-0.8.0 (c (n "pocket_prover-derive") (v "0.8.0") (d (list (d (n "pocket_prover") (r "^0.17.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0ll4clwjiq020xsn24ca74khkj1i10kc57qmqqh68hqgdk32al2f")))

