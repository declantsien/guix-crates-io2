(define-module (crates-io po ck pocket-relay-redirector-server) #:use-module (crates-io))

(define-public crate-pocket-relay-redirector-server-1.0.0 (c (n "pocket-relay-redirector-server") (v "1.0.0") (d (list (d (n "blaze-pk") (r "^0.6.0-alpha-6") (f (quote ("async" "blaze-ssl"))) (k 0)) (d (n "blaze-ssl-async") (r "^0.1.1") (d #t) (k 0)) (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "pocket-relay-core")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "utils") (r "^0.1.0") (d #t) (k 0) (p "pocket-relay-utils")))) (h "1fssymwj64k6adnlvh20hdwnwl14p8xbw2kqah96cb63cqnv1gl1")))

