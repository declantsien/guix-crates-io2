(define-module (crates-io po ck pocket) #:use-module (crates-io))

(define-public crate-pocket-0.0.1 (c (n "pocket") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "12acfz2qwv40d6hzk8g15iall5sijq1nmsyqpcqmav4n4240n7cz")))

(define-public crate-pocket-0.0.2 (c (n "pocket") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0xban8x8z8sw3y7qg18c4l8fcibcilllnifsgw30w1nxywpxhqvf")))

(define-public crate-pocket-0.0.3 (c (n "pocket") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0isqp4yk2w46s5lf1g7b8r5gga7gwmx2mrgzgjskgmlid4rd7jxv")))

(define-public crate-pocket-0.0.4 (c (n "pocket") (v "0.0.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0b3d6hz1gxiiq26ndilfh00ywdvcxxp752713rifka9zn71x7b4l")))

(define-public crate-pocket-0.0.5 (c (n "pocket") (v "0.0.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01qkc0fxjanvmaij7vaicacwcb8fimc5hcf46nf3468afzj5xk43")))

(define-public crate-pocket-0.0.6 (c (n "pocket") (v "0.0.6") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0yz90fz6zlm8alrcqxqkihc0d8f5n57jqm7yj8cskcd27z5l35cc")))

(define-public crate-pocket-0.1.0 (c (n "pocket") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0vaniannna3is22ff4lb9mnrs1q6934afzihs2lf9rfxbapczl08")))

(define-public crate-pocket-0.1.1 (c (n "pocket") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0fpq1g7k5igcky8m7y4vj873kfn796vd29wc114x11n5h0i74nyb")))

(define-public crate-pocket-0.1.2 (c (n "pocket") (v "0.1.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 2)) (d (n "mime") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01j0m2lqxy8vf0gbpxraa51whnhisndqxfvscpsw7vfwwsmpj514")))

(define-public crate-pocket-0.1.3 (c (n "pocket") (v "0.1.3") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 2)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0ksrk2zmvw20wzh6vz6qw25d2j491q8sz5vl49wl0qcb78fnid0i")))

