(define-module (crates-io po ke pokemon-go-protobuf) #:use-module (crates-io))

(define-public crate-pokemon-go-protobuf-0.1.0 (c (n "pokemon-go-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.0.23") (d #t) (k 0)))) (h "1km6m9rgmavrh4l5bdz33ja29kgfmh4d0mhsz7ddl8rh9bbzgp89")))

(define-public crate-pokemon-go-protobuf-0.1.1 (c (n "pokemon-go-protobuf") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.0.23") (d #t) (k 0)))) (h "0xwhlda0qfqwg0v82gz14mza07csmaqxdlarpr0iq5vg3j5lx1kj")))

(define-public crate-pokemon-go-protobuf-0.1.2 (c (n "pokemon-go-protobuf") (v "0.1.2") (d (list (d (n "protobuf") (r "^1.0.23") (d #t) (k 0)))) (h "1l5hbf2s2zjbz3wnxvx01w47ndhlzqzaqankh4mwb50xrqlhr0z8")))

(define-public crate-pokemon-go-protobuf-0.1.4 (c (n "pokemon-go-protobuf") (v "0.1.4") (d (list (d (n "protobuf") (r "^1.0.23") (d #t) (k 0)))) (h "1abckvpwps6ybwalyg9bq8wgar9gp9abc4w8rfbhn77sq9q7sk8d")))

