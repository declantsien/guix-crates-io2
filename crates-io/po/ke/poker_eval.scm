(define-module (crates-io po ke poker_eval) #:use-module (crates-io))

(define-public crate-poker_eval-0.1.0 (c (n "poker_eval") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_big_array") (r "^0.5.1") (o #t) (d #t) (k 0) (p "serde-big-array")) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "03rylw2dfx3g1gkppfndvxyp4qaddn89rl4n6rwd6alh1ig5mlxf") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_big_array"))))))

