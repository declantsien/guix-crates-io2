(define-module (crates-io po ke pokemon_go_data) #:use-module (crates-io))

(define-public crate-pokemon_go_data-0.1.0 (c (n "pokemon_go_data") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 1)))) (h "12vq6mvw12dia3kfic31qvw1iq50k396z7vz1s66mq2f5hsqcqgp")))

(define-public crate-pokemon_go_data-0.1.1 (c (n "pokemon_go_data") (v "0.1.1") (d (list (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 1)))) (h "0dlfi9j5w2zy6nfxyaxpb6ma4m689hsczp4ha1lc2w7a2jqp6zr9")))

(define-public crate-pokemon_go_data-0.1.2 (c (n "pokemon_go_data") (v "0.1.2") (h "0w7a8bfk5ajmyxldl7as6xxdq3zz2hwpd88183ka4c8fz9saa3g2")))

(define-public crate-pokemon_go_data-0.2.0 (c (n "pokemon_go_data") (v "0.2.0") (h "1jlbmbi228ilni6r8vidp5nq2rvw2zlqk4ff0fk6vhmqpgp7v805")))

(define-public crate-pokemon_go_data-0.4.0 (c (n "pokemon_go_data") (v "0.4.0") (h "06z7kmlc6a3wz9mbzbhc338rkn03cnjp0jryga837c8rbkzalxvl")))

(define-public crate-pokemon_go_data-0.5.0 (c (n "pokemon_go_data") (v "0.5.0") (h "1fzlp62nqy4nrgscbppy1660wa5ppylzx0nm0q1iish6jjkfspa2")))

(define-public crate-pokemon_go_data-0.6.0 (c (n "pokemon_go_data") (v "0.6.0") (h "1dv91qjvkgfdi72mx3id3gg15l8021ga5kkn17ls1w36qk7avcvf")))

