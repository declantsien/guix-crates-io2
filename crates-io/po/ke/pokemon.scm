(define-module (crates-io po ke pokemon) #:use-module (crates-io))

(define-public crate-pokemon-0.1.0 (c (n "pokemon") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0v9figxhdabxpg3q2pxyjwgg1ms4f2czbi0amymfrnslc2cq1ln4")))

(define-public crate-pokemon-1.0.0 (c (n "pokemon") (v "1.0.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0z34liccqkc45dm5dmv3igwf3hyylksp7wrsq144r2h6jadx687h")))

