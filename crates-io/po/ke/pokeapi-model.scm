(define-module (crates-io po ke pokeapi-model) #:use-module (crates-io))

(define-public crate-pokeapi-model-0.1.0 (c (n "pokeapi-model") (v "0.1.0") (d (list (d (n "pokeapi-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yzywdcnsfi39vzr2lzp2ivblfbkpbdynfcji2rrgihicljsliy6")))

(define-public crate-pokeapi-model-0.1.1 (c (n "pokeapi-model") (v "0.1.1") (d (list (d (n "pokeapi-macro") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "14x2b173m4mnpi257q86szdf121bjx0c9gfxp4m79za4h0yyp1jl")))

(define-public crate-pokeapi-model-0.1.3 (c (n "pokeapi-model") (v "0.1.3") (d (list (d (n "pokeapi-macro") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yd9ljpa6vda91ayymzqzq0djc7i29gn9iv7r8zgvcp0r1nd5lpw")))

(define-public crate-pokeapi-model-0.1.4 (c (n "pokeapi-model") (v "0.1.4") (d (list (d (n "pokeapi-macro") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "03cj0qbv02km25vrg1v9d2jkfpzin832jm9lrxzbhj21ziwa0rl9")))

