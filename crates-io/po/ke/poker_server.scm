(define-module (crates-io po ke poker_server) #:use-module (crates-io))

(define-public crate-poker_server-0.1.0 (c (n "poker_server") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (f (quote ("macros"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "poker_eval") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1slkfz3qwcgnra3c86m6gcvha9nr7byb3slwr0a7l3a77ffd7g64")))

