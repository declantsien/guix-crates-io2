(define-module (crates-io po ke poker_rs) #:use-module (crates-io))

(define-public crate-poker_rs-0.1.0 (c (n "poker_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f59hzgz9bvyf0gh15lvc8p3vjrs47ki34viq4qicp7wi01ra114") (y #t)))

(define-public crate-poker_rs-0.1.1 (c (n "poker_rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g86qwna7ibn6a4jz08pj0nrwgaayh5lz1slclsb7pk1qn2n7dgl") (y #t)))

