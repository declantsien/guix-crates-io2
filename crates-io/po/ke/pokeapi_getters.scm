(define-module (crates-io po ke pokeapi_getters) #:use-module (crates-io))

(define-public crate-pokeapi_getters-0.1.0 (c (n "pokeapi_getters") (v "0.1.0") (d (list (d (n "pokeapi_types") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07sqvzy7whbc6mphp5ql8dpslp4zsg06ikk7b4138k1n72q4g1i5")))

(define-public crate-pokeapi_getters-0.2.0 (c (n "pokeapi_getters") (v "0.2.0") (d (list (d (n "pokeapi_types") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nbalb640b46dkcz4hbhh7x2dnpqcdsgvfl3paxqv76xxyf4bn23")))

(define-public crate-pokeapi_getters-0.2.1 (c (n "pokeapi_getters") (v "0.2.1") (d (list (d (n "pokeapi_types") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18z7xbfp973xls4dnzhvhddwzashsr3hx41sjszhaicyidca4k7z")))

