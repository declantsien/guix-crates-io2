(define-module (crates-io po ke pokereval_cactus) #:use-module (crates-io))

(define-public crate-pokereval_cactus-0.1.0 (c (n "pokereval_cactus") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1jcc7y0j7hr5dp1pc4493km1krakn9zlwbkv9l5dq6ih6q326z00")))

(define-public crate-pokereval_cactus-0.1.1 (c (n "pokereval_cactus") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0m2y3gbzkv78lv293iwzn7yya1594jsr2qas3jhh91fd61nza4p3")))

(define-public crate-pokereval_cactus-0.1.2 (c (n "pokereval_cactus") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0vj2pn0qaj89slx91819bhj1n2vf9m7rr2imgzl6i8rqgf67q9m9")))

