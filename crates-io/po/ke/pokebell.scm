(define-module (crates-io po ke pokebell) #:use-module (crates-io))

(define-public crate-pokebell-0.1.0 (c (n "pokebell") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s1wdryy8wlgn666x20njflnfk9ngr9iivnzwbqpmyh9f2kpiq6a")))

