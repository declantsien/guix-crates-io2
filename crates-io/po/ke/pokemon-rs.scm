(define-module (crates-io po ke pokemon-rs) #:use-module (crates-io))

(define-public crate-pokemon-rs-1.0.0 (c (n "pokemon-rs") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0xylxbbxqj52cy92n7qpgsam7q2fgf4bgw3g2vy0mri730aprb0n")))

(define-public crate-pokemon-rs-1.0.1 (c (n "pokemon-rs") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0aj06d3l2j8frgydp6wz522wj6hd53m3yzk8ynpkazyighaf7b53")))

(define-public crate-pokemon-rs-1.0.2 (c (n "pokemon-rs") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1wrpwmp6sfmlwvglpcbnvjki53fmm3caiaddii3x5fsq32xra98h")))

(define-public crate-pokemon-rs-1.0.3 (c (n "pokemon-rs") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d8p5m6mvlfs4zvlqs6yg8r2c4nfybf909jnfkm9j642g1hflqza")))

(define-public crate-pokemon-rs-1.0.4 (c (n "pokemon-rs") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "066bc3ikzn8jgwqrj66dzpqhc4l92nzp039yapf4nb7sxkr4v4m2")))

(define-public crate-pokemon-rs-1.0.5 (c (n "pokemon-rs") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sxqzcy1fcl21kxpdmdwqhq5lxh4i2aji0cl93c45bwqm3cwhcsa")))

(define-public crate-pokemon-rs-1.1.0 (c (n "pokemon-rs") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gbq0bs9dgw2m0giy54sdb1y0ndadq1jwarq7zi3dyhgw6c21v79")))

(define-public crate-pokemon-rs-1.1.1 (c (n "pokemon-rs") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qx19pic2s8aax986p9qw8gwpqxqrjk13988v3m2c5ri53860f12")))

(define-public crate-pokemon-rs-1.1.2 (c (n "pokemon-rs") (v "1.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01rwv537wzfhmq89alij2fr52zyhba9nj8ydbnzngv45lq1p19zs")))

(define-public crate-pokemon-rs-1.1.3 (c (n "pokemon-rs") (v "1.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sawybvgq9m9mhbza0zwp92pxakmmyq7q17ll0h7sl01n4wnpynf")))

(define-public crate-pokemon-rs-1.1.4 (c (n "pokemon-rs") (v "1.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "073146dxvjxvhk40cpvfz8cizcc8awaczpn3ki8aff645l1gg4w9")))

(define-public crate-pokemon-rs-1.1.5 (c (n "pokemon-rs") (v "1.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15924h1bwwn052788lj2d14gr6nhhqxy8l2cg6jfpcifhnn0rai9")))

(define-public crate-pokemon-rs-1.2.0 (c (n "pokemon-rs") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ygd9lv5ip1hsxgcl7pi5xfgn4l3km729ykiz60gxc89c9bpd0ki")))

(define-public crate-pokemon-rs-1.2.1 (c (n "pokemon-rs") (v "1.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s609k9fhl6yna5s8amnjn941q8np9ng5f722w9yjs8plq6gihhw")))

(define-public crate-pokemon-rs-1.2.2 (c (n "pokemon-rs") (v "1.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rggzj42zzggw5yhvrkv6wzq6gxh9gnn7ni8b9ngaxjpqrmc9l39")))

(define-public crate-pokemon-rs-1.2.3 (c (n "pokemon-rs") (v "1.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12x79p2z66gjwn1gqhkwlsa4077q9q0a6nz1brcqas11cga2d0r7")))

(define-public crate-pokemon-rs-1.3.0 (c (n "pokemon-rs") (v "1.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z3wwac2wzc54kzzvdipjpi5glcx2l3h2dj7vkg1hj88yf060gph")))

(define-public crate-pokemon-rs-1.3.1 (c (n "pokemon-rs") (v "1.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qc0ikbi9gw61cdgsz2931aachv38ascpils17c8k23yxzm77c77")))

(define-public crate-pokemon-rs-1.3.2 (c (n "pokemon-rs") (v "1.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ayzddgzz9c5cpdw8w74zw16kxlngg179h3mg0l277gj8rrafyjb")))

(define-public crate-pokemon-rs-1.3.3 (c (n "pokemon-rs") (v "1.3.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bb3vsllpy1rlf2bc3vmiza6lcmzvnh8yhpvrkidwirjzwm1qnsr")))

(define-public crate-pokemon-rs-1.4.0 (c (n "pokemon-rs") (v "1.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02s835b1hdhkchpbr79y5grw55fdvm9y04wys6jd5ffa0xxnhmi6")))

(define-public crate-pokemon-rs-1.5.0 (c (n "pokemon-rs") (v "1.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0828vqhfp2fz9kigh5bbk559hrgdfvc3mlgn5rdqpq5fj5xvwvfa")))

(define-public crate-pokemon-rs-1.5.1 (c (n "pokemon-rs") (v "1.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0j0kwl18mddcbba9pxvksclzr40c9cxhb4r67yswy23qacwpp2g4")))

(define-public crate-pokemon-rs-1.5.2 (c (n "pokemon-rs") (v "1.5.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14zvg2rdm270lqf81c57faxmvy2jc025vsqgf98sxr9j1ag58xyc")))

(define-public crate-pokemon-rs-1.6.0 (c (n "pokemon-rs") (v "1.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gfcvcq3kp7qxz24w0rmvqgr5hdnl5x1l66x0s0yacxgqms3381z")))

(define-public crate-pokemon-rs-1.6.1 (c (n "pokemon-rs") (v "1.6.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zr55sgklzaff519xi2hc3l1s75kdpi3ngabgs1gyv56ir2714g4")))

(define-public crate-pokemon-rs-1.6.2 (c (n "pokemon-rs") (v "1.6.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lklkamm638ck5vvawlxmkiwmsk7cl6sh1xv6z2xwksr6s8d9p66")))

(define-public crate-pokemon-rs-1.6.3 (c (n "pokemon-rs") (v "1.6.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0552yyndfhl79zgrrszk0b365w1mqgvgslkjngbmr46dnzky089p")))

