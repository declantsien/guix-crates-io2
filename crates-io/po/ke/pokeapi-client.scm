(define-module (crates-io po ke pokeapi-client) #:use-module (crates-io))

(define-public crate-pokeapi-client-0.1.0 (c (n "pokeapi-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("parking_lot" "macros"))) (d #t) (k 0)))) (h "05wagabxskpxpzc5vnjp0hq71xqlla7c4lm4s1wgh56z453a422b")))

(define-public crate-pokeapi-client-0.1.1 (c (n "pokeapi-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("parking_lot" "macros"))) (d #t) (k 0)))) (h "102smkflcfbx9871ifl2ccyn8l05jw7sy1d4ivvkiaqig0s6nflv")))

