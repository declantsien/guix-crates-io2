(define-module (crates-io po ke pokemon-utils) #:use-module (crates-io))

(define-public crate-pokemon-utils-0.1.0 (c (n "pokemon-utils") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16hvngsyyrxd0lwrkicbwbh2nr3k47j5qfqccdbgxh9xjskisk0c")))

(define-public crate-pokemon-utils-0.1.1 (c (n "pokemon-utils") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rv2qwza3g9xzs8rmdnavr74n134krh8r1djc2swbgwi20pmbv6h")))

(define-public crate-pokemon-utils-0.1.2 (c (n "pokemon-utils") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jbxmvpflz08nvc0pwxx05720lg1ah7h84nq4v92hh3wdwpli2n9")))

(define-public crate-pokemon-utils-0.1.3 (c (n "pokemon-utils") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "03948s47cmmrhpi9qy042jz1jd2wqxa8vlwhy88zd5acc4dsydwx")))

