(define-module (crates-io po ke poke-type) #:use-module (crates-io))

(define-public crate-poke-type-0.1.0 (c (n "poke-type") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vzjw9hj7hkf4kqjdjs71qfnw0cx9knqp9dlj9yvg2hwllldsqab") (y #t)))

(define-public crate-poke-type-0.1.2 (c (n "poke-type") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q5315mnj0q96vnrkvqynrhhndijhcpwlydn09b0z1l7r1ij4kkf")))

