(define-module (crates-io po ke pokedex) #:use-module (crates-io))

(define-public crate-pokedex-0.1.0 (c (n "pokedex") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15c3akv21hdnxzjfyh4bmc47mrb8kqyjb8j49frh85z14kmzi11m")))

(define-public crate-pokedex-0.1.1 (c (n "pokedex") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ifz93bcapqjbsfy76gbf2p09gjcy3l7kw50snql7b23j9bpq0s2")))

(define-public crate-pokedex-0.1.2 (c (n "pokedex") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ja7jnwj32563jbhcxz3b4k7kjjc7m1109c42sy9mc9fqwbzz3n5")))

(define-public crate-pokedex-0.1.3 (c (n "pokedex") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xhzl39361v6izlqd4r1cmk128ka9jp8d2dkyxqkvv6h1p16kfxq")))

(define-public crate-pokedex-0.1.4 (c (n "pokedex") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1px25h05x5cxs6lnhpldg6wh57lj4m0cs71ffg6j7j4rh2ivrzk9")))

(define-public crate-pokedex-0.1.5 (c (n "pokedex") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jbqk2b6haz1bnljgarl9w56b78xdbrnj7gyzm8ds7wq8ha1i22w")))

(define-public crate-pokedex-0.1.6 (c (n "pokedex") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10mrxq2jfxarq3vmpa7ks9rjzcxfi8f07y5pzq1w7x719g5mvazm")))

(define-public crate-pokedex-0.1.7 (c (n "pokedex") (v "0.1.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzr7iq8cns7c0r8wm4s9wylkacxn4gjj2j239670yzljkfs07nw")))

(define-public crate-pokedex-0.1.8 (c (n "pokedex") (v "0.1.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qsc1c2rvfafrnc4n20qcmw5hhpc0krdxpfsn8669fmglynbyzbv")))

