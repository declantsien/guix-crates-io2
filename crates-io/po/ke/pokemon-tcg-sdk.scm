(define-module (crates-io po ke pokemon-tcg-sdk) #:use-module (crates-io))

(define-public crate-pokemon-tcg-sdk-0.1.0 (c (n "pokemon-tcg-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5.2") (d #t) (k 2)))) (h "06rx41d20b2l6vz866waj3m6ywic6w7frch82rlqjc4s5b03vvgr")))

(define-public crate-pokemon-tcg-sdk-0.1.1 (c (n "pokemon-tcg-sdk") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5.2") (d #t) (k 2)))) (h "18fsgk7y41pfvqlc18h3nair61267fysil1ixii6j8c3y4kgzp40")))

(define-public crate-pokemon-tcg-sdk-0.2.0 (c (n "pokemon-tcg-sdk") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5.2") (d #t) (k 2)))) (h "1zbyjiz9kl2zqk58l05hxaxwpbmsrzn4n11bj14anj2vb6ba60f0")))

(define-public crate-pokemon-tcg-sdk-0.3.0 (c (n "pokemon-tcg-sdk") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiremock") (r "^0.5.2") (d #t) (k 2)))) (h "0ll0fxzxjz4yqfzazg1x36mjbll609bdariknvgi2m29dbsn8bzw")))

