(define-module (crates-io po ke pokers) #:use-module (crates-io))

(define-public crate-pokers-0.1.0 (c (n "pokers") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "03ask3yrd9spyzipswi5cnhjzax1vz9k5j9cmqkm0zhkqpqq0xaw")))

(define-public crate-pokers-0.2.0 (c (n "pokers") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0ggxxzv8y34iwyghdzaqbmz8ig3qldalk09dlfmd928lpvn5p3w2")))

(define-public crate-pokers-0.3.0 (c (n "pokers") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1qxmvzj0pvn20ngihr5mxir61qbm6nvyrrr223y6jizw8f5fv93y")))

(define-public crate-pokers-0.3.1 (c (n "pokers") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0lglfz2vhh0ac8dg4hac91k4v9bn5gkhx1ln57wjc6g5dgwlwzz0")))

(define-public crate-pokers-0.3.2 (c (n "pokers") (v "0.3.2") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0l88mzr2cvwxxv8krhbi4xl7kybqvqg0w63sjs8q6pn5y1nixlcq")))

(define-public crate-pokers-0.3.3 (c (n "pokers") (v "0.3.3") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0n8jg171vdb79awh90ama2yfd9djiy1vw64788ph1zk77k07v2d5")))

(define-public crate-pokers-0.3.4 (c (n "pokers") (v "0.3.4") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1vi8y792fhr4s28lq9p6ypqswhc31xyx4lbkxs1376fyifcy437x")))

