(define-module (crates-io po ke pokestat) #:use-module (crates-io))

(define-public crate-pokestat-0.1.0 (c (n "pokestat") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "18q2n3yb3d4aqk1f16d2qb51vjbvzx7msch0w58qnriiiab9sy5w") (s 2) (e (quote (("genpokedb" "dep:serde_json"))))))

