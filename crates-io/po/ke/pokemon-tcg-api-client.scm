(define-module (crates-io po ke pokemon-tcg-api-client) #:use-module (crates-io))

(define-public crate-pokemon-tcg-api-client-0.2.1 (c (n "pokemon-tcg-api-client") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0j0h76xhxw424j4y6l77b98bfvs63lyh878j12hffk9a1s119qlq")))

(define-public crate-pokemon-tcg-api-client-0.2.2 (c (n "pokemon-tcg-api-client") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0nj906dikm1fkmi22gzgqzjy3sccdzs3lbjp7n2cvmp793s935ip")))

