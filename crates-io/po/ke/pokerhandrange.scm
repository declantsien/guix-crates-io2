(define-module (crates-io po ke pokerhandrange) #:use-module (crates-io))

(define-public crate-pokerhandrange-0.1.0 (c (n "pokerhandrange") (v "0.1.0") (d (list (d (n "cards") (r "^1.1.1") (d #t) (k 0)) (d (n "holdem") (r "^0.1.1") (d #t) (k 0)) (d (n "pokereval") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "13kg6dhjhx2v7rk1hj9c8r47b6ivhjj87sryiyacqrihn4c5f4p3")))

(define-public crate-pokerhandrange-0.1.1 (c (n "pokerhandrange") (v "0.1.1") (d (list (d (n "cards") (r "^1.1.2") (d #t) (k 0)) (d (n "holdem") (r "^0.1.2") (d #t) (k 0)) (d (n "pokereval") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "0hslfdiaimlp6a8rz1z8m25fb9snp2bnmd6fw1qwiqcac3gdgr93")))

