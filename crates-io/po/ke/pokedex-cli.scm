(define-module (crates-io po ke pokedex-cli) #:use-module (crates-io))

(define-public crate-pokedex-cli-0.1.0 (c (n "pokedex-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "150w3jgbz6wbz974yyk2n1aj41r58sc2pd5hj8ldx63w9dqjlxgk")))

