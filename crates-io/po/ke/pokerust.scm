(define-module (crates-io po ke pokerust) #:use-module (crates-io))

(define-public crate-pokerust-0.1.0 (c (n "pokerust") (v "0.1.0") (d (list (d (n "cached") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https" "json-using-serde" "punycode"))) (d #t) (k 0)) (d (n "mockito") (r "^0.23.0") (d #t) (k 2)) (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pvmcailrfyky4yghgzq2n0czladxf3cgxrd3x17rva1z0596sz6")))

(define-public crate-pokerust-0.2.0 (c (n "pokerust") (v "0.2.0") (d (list (d (n "cached") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https" "json-using-serde" "punycode"))) (d #t) (k 0)) (d (n "mockito") (r "^0.23.0") (d #t) (k 2)) (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sjv2i1r41xwgs39b8il1gkvy6a59v6p0ma7g6vbw8hi0x7grgw2")))

