(define-module (crates-io po ke pokemon-synthesizer) #:use-module (crates-io))

(define-public crate-pokemon-synthesizer-0.1.0 (c (n "pokemon-synthesizer") (v "0.1.0") (d (list (d (n "rodio") (r "^0.17.1") (k 2)))) (h "0b1paj2v4zzhlkyjm39jw50dszvb9l5ch8nw5frkhjsdywbp33p9") (r "1.66")))

(define-public crate-pokemon-synthesizer-0.2.0 (c (n "pokemon-synthesizer") (v "0.2.0") (d (list (d (n "rodio") (r "^0.17.1") (k 2)))) (h "1ppyy9fh7vs1iv4yz04hms8hzjl4khbc37d7w0s9sqqjf4p64aj3") (r "1.66")))

