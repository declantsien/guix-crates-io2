(define-module (crates-io po ke pokemon-sprite-compression) #:use-module (crates-io))

(define-public crate-pokemon-sprite-compression-0.1.0 (c (n "pokemon-sprite-compression") (v "0.1.0") (h "0sw7w5v24wxxw930vlp80rh4q1mk0apvcwbnz1pkjm80y9gpxg6r") (r "1.66")))

(define-public crate-pokemon-sprite-compression-0.1.1 (c (n "pokemon-sprite-compression") (v "0.1.1") (h "1fqnmfl1788lyvvlysx60vyl3l2mxqdv2j3y3adi80xrqmmng007") (r "1.66")))

(define-public crate-pokemon-sprite-compression-0.1.2 (c (n "pokemon-sprite-compression") (v "0.1.2") (h "0lx6qh3skzq19cr6pxfy6sq25pdbsz8v7g2fcq4qqhvrc6mrsbda") (r "1.66")))

