(define-module (crates-io po ke poke) #:use-module (crates-io))

(define-public crate-poke-0.1.0 (c (n "poke") (v "0.1.0") (h "12r7wj3xf6v4xwdgli8lpq0rrryhyvvd7dj8r394p16kv096yhqr") (y #t)))

(define-public crate-poke-0.1.1 (c (n "poke") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06306217xcsamdznyy45p4xjhcn1zgsbf0syba5qb8kmb2ppyrq1")))

(define-public crate-poke-0.1.2 (c (n "poke") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11s0q3gapj5m5yy0955fvlvlg2wlv25n8vqwmmj11kv9l1d0h61f")))

(define-public crate-poke-0.1.3 (c (n "poke") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.7") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fcppiblh95bghgrk2707bqmllhyhgkigpppw9ccy6mim8xpgcrm")))

