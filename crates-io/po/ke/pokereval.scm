(define-module (crates-io po ke pokereval) #:use-module (crates-io))

(define-public crate-pokereval-0.1.0 (c (n "pokereval") (v "0.1.0") (d (list (d (n "cards") (r "^1.1.0") (d #t) (k 0)) (d (n "holdem") (r "^0.1.0") (d #t) (k 0)))) (h "0c1vb29hpfawjpj7sqbyrc6isjjivhsxj3vd5ri6aq8357lcf1rl")))

(define-public crate-pokereval-0.1.1 (c (n "pokereval") (v "0.1.1") (d (list (d (n "cards") (r "^1.1.1") (d #t) (k 0)) (d (n "holdem") (r "^0.1.1") (d #t) (k 0)))) (h "1spwljzxhgdshm7r9691ig9wbl47c5laf9x0hqhzkmdmhh1wnvrc")))

(define-public crate-pokereval-0.1.2 (c (n "pokereval") (v "0.1.2") (d (list (d (n "cards") (r "^1.1.2") (d #t) (k 0)) (d (n "holdem") (r "^0.1.2") (d #t) (k 0)))) (h "0cmpkqcj99fxqjmrb0490xgl0v1s7qd9y181fcr801pg49rjp9gr")))

