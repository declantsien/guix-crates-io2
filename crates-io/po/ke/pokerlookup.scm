(define-module (crates-io po ke pokerlookup) #:use-module (crates-io))

(define-public crate-pokerlookup-0.1.0 (c (n "pokerlookup") (v "0.1.0") (d (list (d (n "cards") (r "^1.1.0") (d #t) (k 0)) (d (n "holdem") (r "^0.1.0") (d #t) (k 0)) (d (n "pokereval") (r "^0.1.0") (d #t) (k 0)))) (h "05v49nb8irpqd5z602s3bmqsdh46vjysrm3iqmgivqs0fm88g8hg")))

(define-public crate-pokerlookup-0.1.1 (c (n "pokerlookup") (v "0.1.1") (d (list (d (n "cards") (r "^1.1.1") (d #t) (k 0)) (d (n "holdem") (r "^0.1.1") (d #t) (k 0)) (d (n "pokereval") (r "^0.1.1") (d #t) (k 0)))) (h "10gwc5p76ba561gdjwcmzrmmg515r5p4b1b4hv6sx7006jhhbl7b")))

(define-public crate-pokerlookup-0.1.2 (c (n "pokerlookup") (v "0.1.2") (d (list (d (n "cards") (r "^1.1.2") (d #t) (k 0)) (d (n "holdem") (r "^0.1.2") (d #t) (k 0)) (d (n "pokereval") (r "^0.1.2") (d #t) (k 0)))) (h "1wfczhz869bf027g3njcrzi2vrjd8g3xqdj15rfsn4c3ahv4jqym")))

