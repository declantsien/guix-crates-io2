(define-module (crates-io po mf pomf) #:use-module (crates-io))

(define-public crate-pomf-0.1.0 (c (n "pomf") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.3.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "multipart") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "08c0nax8rypam2cxz0dxmvjc3swcdd2rq5si6amgqb4zwkn9ipx1")))

