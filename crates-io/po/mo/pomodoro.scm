(define-module (crates-io po mo pomodoro) #:use-module (crates-io))

(define-public crate-pomodoro-0.1.0 (c (n "pomodoro") (v "0.1.0") (d (list (d (n "notify-rust") (r "^3.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.14") (d #t) (k 0)))) (h "1zs3nsrxx3mjaiqkj3nqnzqb6h8s2x0fbrd3qzi75cjhw8dwhm3v")))

(define-public crate-pomodoro-0.1.1 (c (n "pomodoro") (v "0.1.1") (d (list (d (n "notify-rust") (r "^3.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.14") (d #t) (k 0)))) (h "18lkjqvf33xfkh6yhrjnpjkqqq5vcffg0lvp5mlcywn0n4rgy851")))

