(define-module (crates-io po mo pomors) #:use-module (crates-io))

(define-public crate-pomors-0.1.0 (c (n "pomors") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1qxavb63wcxm5cm22jncnz0ni8gr1pvvmw8553ncqgvcddxs0329")))

(define-public crate-pomors-0.1.1 (c (n "pomors") (v "0.1.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "03f1j1q6yvz2gb2vz9fd2m12qlnsl0b9bmwr59phlagpvijs41wq")))

(define-public crate-pomors-0.1.2 (c (n "pomors") (v "0.1.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1rqp48xjq6vfq3g32vsa99xbv7sp1a0wd4f5qs6k56iqlbwxvzc1")))

(define-public crate-pomors-0.2.0 (c (n "pomors") (v "0.2.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "rodio") (r "^0.10.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "0mxrslmzaxdm4yd101x4wxzciy2fvyybvxygkdjdqsz0i4ih3a6z") (y #t)))

(define-public crate-pomors-0.2.1 (c (n "pomors") (v "0.2.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "rodio") (r "^0.10.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "00mmmpwhp2l6yswzmdhrwmzlbrqsdjasnlxhhhiwh1x8f0sqb2vm")))

(define-public crate-pomors-0.2.2 (c (n "pomors") (v "0.2.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify-rust") (r "^3.6.3") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1m3kpsnzxcnlg3yvzkcqvs4hhpwnqy0xmfysznbbs3qbpsjs4pc5")))

(define-public crate-pomors-0.2.3 (c (n "pomors") (v "0.2.3") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "notify-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.12.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "14hn9km0pra6lfkbb9k68gz16g1cmazwjx66xq495svvc8978srr")))

