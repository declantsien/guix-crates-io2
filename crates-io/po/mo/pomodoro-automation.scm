(define-module (crates-io po mo pomodoro-automation) #:use-module (crates-io))

(define-public crate-pomodoro-automation-0.1.0 (c (n "pomodoro-automation") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "07qbkli0ff27bnx48nbxhwk7s886whcwki3x5p37ljbj00p1q4iw")))

