(define-module (crates-io po mo pomo) #:use-module (crates-io))

(define-public crate-pomo-0.1.0 (c (n "pomo") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0iwq63rxz51xld9rwqxh21yshx49fiv9bfng83d8fr3ihbjjzygd")))

(define-public crate-pomo-1.0.0 (c (n "pomo") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0xpazd2q603ppzqgw0105v2kqp582kg2v3s3862r81s1s1b3qxpw")))

