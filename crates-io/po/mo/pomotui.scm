(define-module (crates-io po mo pomotui) #:use-module (crates-io))

(define-public crate-pomotui-0.1.0 (c (n "pomotui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "18ds1g9qk0dv23hld53srwxsn05dr0zqixbcw8xh8riim56wffjb")))

