(define-module (crates-io po ol poolcache) #:use-module (crates-io))

(define-public crate-poolcache-0.1.0 (c (n "poolcache") (v "0.1.0") (h "1jmf6gvnps02zxkva682ja2kq3sbyhq8ffwapn1xbzizsvfm9sfk")))

(define-public crate-poolcache-0.1.1 (c (n "poolcache") (v "0.1.1") (h "0zh20k03c3h73nkh8ccvzim7wfhrb6agmllkvx1v5135naci25gf")))

