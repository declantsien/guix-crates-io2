(define-module (crates-io po ol poolparty) #:use-module (crates-io))

(define-public crate-poolparty-0.1.0 (c (n "poolparty") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "0w668r8ix1412qkgmxg87iqwqknliwbb1rl39d53jiljh5xfcd25")))

(define-public crate-poolparty-0.1.1 (c (n "poolparty") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "1rmj80rh3byncamq6qf249s2hvyv4sxrjfbyrlysxmmg2lj59avg")))

(define-public crate-poolparty-0.1.2 (c (n "poolparty") (v "0.1.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "05syf6nwdycjgz1zf26a7s9xa2hp0zkjgpjmnh56wl0d2brpd38h")))

(define-public crate-poolparty-0.2.0 (c (n "poolparty") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "0x0i8mmx5ay2mpl2s3abc4g31ldlh7v4ab8s8mksnq7icc996856")))

(define-public crate-poolparty-0.2.1 (c (n "poolparty") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "13vcfg1h2hx8vrby15ai7vyfpqm9pb7bci7651px78riawh0hkxm")))

(define-public crate-poolparty-1.0.0 (c (n "poolparty") (v "1.0.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "17cgaqpviccb62p2y8arrnmhymx48blf20rgp3hibf13vsq04yz1")))

(define-public crate-poolparty-1.0.1 (c (n "poolparty") (v "1.0.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "0h7i8wmy76xkb1fmmivyapy4wqvy7nc7xk005w44498lcb9s33g1")))

(define-public crate-poolparty-1.0.2 (c (n "poolparty") (v "1.0.2") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "0xa47j6h1a156clnpy4q9q24hnm15kz72qv1cfy5k79x67p3fz7m")))

(define-public crate-poolparty-2.0.0 (c (n "poolparty") (v "2.0.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "07f8y5s8s6xcr8y27ljy7xicha0lxh8763f70ay053g2qg1jd7mw")))

(define-public crate-poolparty-2.0.1 (c (n "poolparty") (v "2.0.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0) (p "futures")))) (h "0rb41af3i2633y4x5pv6xqps17gn1xsm8a378r67zjh5inx743gf")))

