(define-module (crates-io po ol poolx-redis) #:use-module (crates-io))

(define-public crate-poolx-redis-0.1.2 (c (n "poolx-redis") (v "0.1.2") (d (list (d (n "poolx") (r "^0.1.2") (d #t) (k 0)) (d (n "redis") (r "^0.24.0") (f (quote ("tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0arp7rhisp21pkqh2fyr6pc7k7hpgi0h0hpnpymkjgfl5ssphwdn")))

