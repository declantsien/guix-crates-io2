(define-module (crates-io po ol pool) #:use-module (crates-io))

(define-public crate-pool-0.1.0 (c (n "pool") (v "0.1.0") (h "0dzc19a0zabq443avhnfv63hxgkfdglrffjcxkh9gsd1ilmbv8rg")))

(define-public crate-pool-0.1.1 (c (n "pool") (v "0.1.1") (h "122ni3w6byyhnqdrvlxl5v2mihsbagjf7lxsfg3xspd2x5ljvr04")))

(define-public crate-pool-0.1.2 (c (n "pool") (v "0.1.2") (h "159bqsi6z2znlvl60jj813ibrmjbbc9mqv7brz6m432grp1nnh6n")))

(define-public crate-pool-0.1.3 (c (n "pool") (v "0.1.3") (h "01zdyy3xmdg2q4sa1zrh6bk8glipajzvcgshr4p5gd7g76h6pz95")))

(define-public crate-pool-0.1.4 (c (n "pool") (v "0.1.4") (h "0vaky4wihwbxf35hna0dy2wx1yldaf0nx0af5fclas81l0qibb67")))

