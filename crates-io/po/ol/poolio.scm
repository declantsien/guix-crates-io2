(define-module (crates-io po ol poolio) #:use-module (crates-io))

(define-public crate-poolio-0.1.0 (c (n "poolio") (v "0.1.0") (h "0dkvlqyvgwqb3fd4r7w1lrvdii5fffqddvaklilyl4mslx97mqmn")))

(define-public crate-poolio-0.1.1 (c (n "poolio") (v "0.1.1") (h "0kc6ky3zsf6f6ga6sp9y9ibhp23vn6n3gnifiz8k5swp33ic9b4q")))

(define-public crate-poolio-0.2.0 (c (n "poolio") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "13zja1ms43a622ngzy34far9wm4f4pbh8xzb6an9bmn1kzrmclrf")))

(define-public crate-poolio-0.2.1 (c (n "poolio") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "0ilb91f1ymmf9bb2l698rcb6clfmnhn8g3ba1zvfh2ircnfca594")))

(define-public crate-poolio-0.2.2 (c (n "poolio") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "threadpool") (r "^1.8") (d #t) (k 2)))) (h "089vdps6ix4ijhmpv0488y0dvhh3qdj6akclyf88clz7yix92dps")))

