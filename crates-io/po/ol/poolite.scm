(define-module (crates-io po ol poolite) #:use-module (crates-io))

(define-public crate-poolite-0.2.0 (c (n "poolite") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "stderr") (r "^0.2.1") (d #t) (k 0)))) (h "0yjjqmbmbcddd1idzw4qa91xqkvidwi190cxs4i18adi14f7z1ih")))

(define-public crate-poolite-0.2.1 (c (n "poolite") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.2.1") (d #t) (k 0)))) (h "0lmfmlm2pw5py7qir3rybw433bx9wfv9zm09k3yajy8igb10jk5c")))

(define-public crate-poolite-0.3.0 (c (n "poolite") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.3.0") (d #t) (k 0)))) (h "1v4c5rxqdplrv31sh4b3f71zl4ff2br29jjxx5kfxkhmpvarcvj8")))

(define-public crate-poolite-0.4.0 (c (n "poolite") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.3.0") (d #t) (k 0)))) (h "0nxayjnxb7yxn4yj40018m1hh64mgk6cpzbg3ngsjr34qrxnq528")))

(define-public crate-poolite-0.4.1 (c (n "poolite") (v "0.4.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.3.0") (d #t) (k 0)))) (h "1ynzw931dzwddwvx8047ji51yaqws75958ambmv26542wvbw8ymc")))

(define-public crate-poolite-0.4.2 (c (n "poolite") (v "0.4.2") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.3.0") (d #t) (k 0)))) (h "1790mdwc91jxpnrpkdq30ncpj4fvssnckrp5id6rvwhxmicjfin9")))

(define-public crate-poolite-0.4.3 (c (n "poolite") (v "0.4.3") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.3.0") (d #t) (k 0)))) (h "0afhvfj440zyzwy3wzc4nvjfmfcp2g4bqw904a8ha0iq6xv3fsdj")))

(define-public crate-poolite-0.4.4 (c (n "poolite") (v "0.4.4") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.5.0") (d #t) (k 0)))) (h "1ijqy57npkv5xzsb8d7bq24acc45h3q76ia1b8f9q4w2hvfdwj8h")))

(define-public crate-poolite-0.5.0 (c (n "poolite") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.6.1") (d #t) (k 0)))) (h "069bpm9n9xpaz020ms1pa13q7f2zgn483s0jx2g0b42fxkqfv3py")))

(define-public crate-poolite-0.5.1 (c (n "poolite") (v "0.5.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.6.1") (d #t) (k 0)))) (h "18yf04q9abknqv68i9q49g3dcfnjhxv5rnpmyahwgjx3c29glblm")))

(define-public crate-poolite-0.5.2 (c (n "poolite") (v "0.5.2") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.6.1") (d #t) (k 0)))) (h "0bq16i0mzfczfj17l2l0hra34kb345rlplc897jaxsl9pwyaiq2j")))

(define-public crate-poolite-0.5.3 (c (n "poolite") (v "0.5.3") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0fvyfyc520s95r1vfgl9lhik7h303bk20x44z2fgjw0n9gfhaghw")))

(define-public crate-poolite-0.5.4 (c (n "poolite") (v "0.5.4") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1x627k051anln6l4jkmgxrb3ninhj33w44sz1rpcb3w7miak492k")))

(define-public crate-poolite-0.5.5 (c (n "poolite") (v "0.5.5") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1zp5i60j60haxmwmrsp04jy25ml508nr4kizrp9m9wz1710sfsa3")))

(define-public crate-poolite-0.6.0 (c (n "poolite") (v "0.6.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1j4hdlrdhsjdmsdq3rpq7dr3ydmcdy2alil3vv2q3akqwcww8riw")))

(define-public crate-poolite-0.6.1 (c (n "poolite") (v "0.6.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "11x0icp1c122x69zhxqpi83vgg9k7ddk6vp7n4gd784xija54vgm")))

(define-public crate-poolite-0.6.2 (c (n "poolite") (v "0.6.2") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.7.1") (d #t) (k 0)))) (h "047dkkh33vhwc9899z2qgrylcs9ywxzf9grlpriw1r58pgjz735f")))

(define-public crate-poolite-0.6.3 (c (n "poolite") (v "0.6.3") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)))) (h "01piph1rafdwkgr9ppk6clqq7bl4x1y29qnn20d3sl6zc61rr21c")))

(define-public crate-poolite-0.6.4 (c (n "poolite") (v "0.6.4") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)))) (h "09kld1sk0c6yyaqi8wg5h144hnjv6mbjkkp2k0mqqgpc80na45qb")))

(define-public crate-poolite-0.7.0 (c (n "poolite") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mxo_env_logger") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)))) (h "1s1m00aaxdsbzdl7fqqxa25073ryhrsxlys1c323kz4ywymykq6n") (y #t)))

(define-public crate-poolite-0.7.1 (c (n "poolite") (v "0.7.1") (d (list (d (n "crossbeam-channel") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "mxo_env_logger") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)))) (h "0iqcpk9hjxfb6l95ccjfwpgb8kis65c62p5h0gryp11vx6zr02w8")))

