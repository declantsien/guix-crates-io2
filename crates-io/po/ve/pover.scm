(define-module (crates-io po ve pover) #:use-module (crates-io))

(define-public crate-pover-0.1.0 (c (n "pover") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v7m48087mafdq0fa0b5k2hcqyzjjlq4d38k2swdh36y5fydna9x")))

(define-public crate-pover-0.2.0 (c (n "pover") (v "0.2.0") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0b2vmllhvqbz4xscca66b6z495mlcp5ygp16sfsw5vv1z6w06k8x")))

(define-public crate-pover-0.2.1 (c (n "pover") (v "0.2.1") (d (list (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "12say4w7mgkhnqjvqg27vr20cpfmigsqv2ifwzgfgagf7plqr0il")))

(define-public crate-pover-0.2.2 (c (n "pover") (v "0.2.2") (d (list (d (n "open") (r "^1.7.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0h7r121hpq712mwf7m1a23mxldqhx8j04axmypq9xxq3iy4s3x59")))

