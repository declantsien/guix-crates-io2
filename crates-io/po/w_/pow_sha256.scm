(define-module (crates-io po w_ pow_sha256) #:use-module (crates-io))

(define-public crate-pow_sha256-0.1.0 (c (n "pow_sha256") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1zzil3906fd6xhrl5xgb10x4x95li46xn7fhqxv9mkkzsa4qnmhm")))

(define-public crate-pow_sha256-0.2.0 (c (n "pow_sha256") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1fvh8nqggljh27p4bq5512xnl880a0miq01m5d4dq7f9i3npbxja")))

(define-public crate-pow_sha256-0.2.1 (c (n "pow_sha256") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0bqqb60acqk4ddabpxra7kfivk6vrqsq07q1ib07zd8hisn67a2r")))

