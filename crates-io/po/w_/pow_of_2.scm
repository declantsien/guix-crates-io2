(define-module (crates-io po w_ pow_of_2) #:use-module (crates-io))

(define-public crate-pow_of_2-0.1.0 (c (n "pow_of_2") (v "0.1.0") (h "1i77lbb475ycfs7sb8zw0awi3szaaiqfgmizqhvi8l5f1h5ma7pn")))

(define-public crate-pow_of_2-0.1.1 (c (n "pow_of_2") (v "0.1.1") (h "0hdcl6wbzpr03iw0grlminmyqp5mihp078np2r9fnan8q4asfkb9")))

(define-public crate-pow_of_2-0.1.2 (c (n "pow_of_2") (v "0.1.2") (h "10yk0i16r3qz5g7sx94vlpmsalwgfw25s1v6p8yb6p7k8qgfqh22")))

