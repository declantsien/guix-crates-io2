(define-module (crates-io po lk polkit) #:use-module (crates-io))

(define-public crate-polkit-0.1.0 (c (n "polkit") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gio-sys") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0") (d #t) (k 0)) (d (n "polkit-sys") (r "^0") (d #t) (k 0)))) (h "14a1raw9rjl8l0055wpxfrlqs7pcy4p013fb66a35nq3vlph482w")))

(define-public crate-polkit-0.15.0 (c (n "polkit") (v "0.15.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.15") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)))) (h "1s27lly11hg1i3w1a043y3gbppk0pij5ib1302121n4wq35ywmp9")))

(define-public crate-polkit-0.15.1 (c (n "polkit") (v "0.15.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.15") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)))) (h "1wdwzja9v2baxq22wr6v532j2bqaz7yhb04q1pwbm2gg0r8bq2ys")))

(define-public crate-polkit-0.15.2 (c (n "polkit") (v "0.15.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.15") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)))) (h "0wk3faz6virj9kz906w6nmr4xpvwcdz00xbpzwczzwbsmc8ydxf7")))

(define-public crate-polkit-0.15.3 (c (n "polkit") (v "0.15.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.15") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)))) (h "158v2hrhj4yjrm4f634j5xhh514g6wmyc9vzg7kkw282b62wyjb8")))

(define-public crate-polkit-0.15.4 (c (n "polkit") (v "0.15.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.15") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "gio-sys") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)))) (h "1mvqnpxq9jksqxqr0sgzrn069xsjmjvbmh944xs8d7k7arps4hws") (f (quote (("dox" "ffi/dox"))))))

(define-public crate-polkit-0.17.0 (c (n "polkit") (v "0.17.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.17") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)))) (h "172mj73ldaa9z2mppmd3l7nnbbq39vwaiv74sqpj25g1q4hn31pp") (f (quote (("dox" "ffi/dox"))))))

(define-public crate-polkit-0.17.1 (c (n "polkit") (v "0.17.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.17") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)))) (h "1b79zcdq2hzh73h1pyicqw03jmc601qcdvgw08sfjfy02jhmsg61") (f (quote (("dox" "ffi/dox"))))))

(define-public crate-polkit-0.19.0 (c (n "polkit") (v "0.19.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.19") (d #t) (k 0) (p "polkit-sys")) (d (n "gio") (r "^0.19") (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)))) (h "0gxmg1kmw9ck6qc0kpnaqyybhfk0jbbz7n8id74wvx3fjlhlv51s")))

