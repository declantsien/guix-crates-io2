(define-module (crates-io po lk polkavm-assembler) #:use-module (crates-io))

(define-public crate-polkavm-assembler-0.1.1 (c (n "polkavm-assembler") (v "0.1.1") (d (list (d (n "iced-x86") (r "^1.19.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "090sva2jk0hy1nx5gbl8s491l14v24vgd2fb6nn9lq81jm6v1sqi") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.2.0 (c (n "polkavm-assembler") (v "0.2.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0hc46s7cn3szmbshxgnlrkrvfx85bjwl0fv75anqswiba7vq2fa8") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.3.0 (c (n "polkavm-assembler") (v "0.3.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0fscnkzvn7swpaqpjm7lrmpsw68si7g48x7d954qiyjbxrwzkijm") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.4.0 (c (n "polkavm-assembler") (v "0.4.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0pr6vbs4d471b9rlky9v4wqdgri4fz1h6rxvk3vmdnq2yb5nx9z0") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.5.0 (c (n "polkavm-assembler") (v "0.5.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "10mwzpvmzg76d6pg32dmamm74j1d3mlmj2q57a1j2xs29c0jrny6") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.6.0 (c (n "polkavm-assembler") (v "0.6.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1pldj0kqgsh5136343z0q90gqzq24avj3sfaa5diwnadh6gkz3d4") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.7.0 (c (n "polkavm-assembler") (v "0.7.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0s1yn55gfd5nq91vsbvndzkl8lq5q6nwxwyh1bx7nn5dhgfqpsd4") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.8.0 (c (n "polkavm-assembler") (v "0.8.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0rpc6n8chak28yfq1l04g3gqxzz64lsqd270w3ynm4cbszj27m1v") (r "1.70.0")))

(define-public crate-polkavm-assembler-0.9.0 (c (n "polkavm-assembler") (v "0.9.0") (d (list (d (n "iced-x86") (r "^1.20.0") (f (quote ("code_asm"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1hddvrk34xbjs9q4r5hkiv6avjsnwx4ds4z85p0sqhw2hrnnva8z") (r "1.70.0")))

