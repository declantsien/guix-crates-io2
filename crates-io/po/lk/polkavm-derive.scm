(define-module (crates-io po lk polkavm-derive) #:use-module (crates-io))

(define-public crate-polkavm-derive-0.1.1 (c (n "polkavm-derive") (v "0.1.1") (d (list (d (n "polkavm-common") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0c68vzfs50wxsj49zaxziiygmh235h1mbll5jdbr0mr4w40mxh5d") (r "1.70.0")))

(define-public crate-polkavm-derive-0.2.0 (c (n "polkavm-derive") (v "0.2.0") (d (list (d (n "polkavm-derive-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0slbcmsxfcvk7m2lac27qaxg23s8qh7lyc63maz0r66bna914l16") (r "1.70.0")))

(define-public crate-polkavm-derive-0.3.0 (c (n "polkavm-derive") (v "0.3.0") (d (list (d (n "polkavm-derive-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxa2a335s525w1ay09sirca19zjgirdvzaqjvc2yyadlk6lhkdy") (r "1.70.0")))

(define-public crate-polkavm-derive-0.4.0 (c (n "polkavm-derive") (v "0.4.0") (d (list (d (n "polkavm-derive-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1p3a8mbm8rhvm9p3yh5x5izvnkvywdjy6skj7j4p9xddsh0aarfv") (r "1.70.0")))

(define-public crate-polkavm-derive-0.5.0 (c (n "polkavm-derive") (v "0.5.0") (d (list (d (n "polkavm-derive-impl") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5xvb5ncy4p035xssdsag8j40j8fpy1r12xsm5cgv03zghxp033") (r "1.70.0")))

(define-public crate-polkavm-derive-0.6.0 (c (n "polkavm-derive") (v "0.6.0") (d (list (d (n "polkavm-derive-impl") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "16nb8jnv9dr36y15lzwyd54rixg79q44qmivk9gj3fl21nxaafmw") (r "1.70.0")))

(define-public crate-polkavm-derive-0.7.0 (c (n "polkavm-derive") (v "0.7.0") (d (list (d (n "polkavm-derive-impl") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0fm5c9izw56g2gx3v5yr4ggq1rin88535lfd8lj5v189mgx3jnwx") (r "1.70.0")))

(define-public crate-polkavm-derive-0.8.0 (c (n "polkavm-derive") (v "0.8.0") (d (list (d (n "polkavm-derive-impl-macro") (r "^0.8.0") (d #t) (k 0)))) (h "09b1ivjhhc9az6ygv82idk46ypk704sahr8spg8qnd32g5pr3ykr") (r "1.70.0")))

(define-public crate-polkavm-derive-0.9.0 (c (n "polkavm-derive") (v "0.9.0") (d (list (d (n "polkavm-derive-impl-macro") (r "^0.9.0") (d #t) (k 0)))) (h "1hv8zkx1pafrjgk2m50yik0j090klxypp7nzchl84g8w7pq6hbn9") (y #t) (r "1.70.0")))

(define-public crate-polkavm-derive-0.9.1 (c (n "polkavm-derive") (v "0.9.1") (d (list (d (n "polkavm-derive-impl-macro") (r "^0.9.0") (d #t) (k 0)))) (h "01lnl62sjs5dhp2d36rr2m0a5n8bq7ddvg0qpf4ws49ydzm4p35f") (r "1.70.0")))

