(define-module (crates-io po lk polkadot-voter-bags) #:use-module (crates-io))

(define-public crate-polkadot-voter-bags-0.0.0 (c (n "polkadot-voter-bags") (v "0.0.0") (h "1pv6d3bczgli7g1xa05dy4am9syp9waak8wdfq9d98pnb5djy9jd") (y #t)))

(define-public crate-polkadot-voter-bags-0.1.0-dev.6 (c (n "polkadot-voter-bags") (v "0.1.0-dev.6") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "=21.1.0-dev.6") (d #t) (k 0)) (d (n "kusama-runtime") (r "=0.1.0-dev.6") (d #t) (k 0) (p "staging-kusama-runtime")) (d (n "polkadot-runtime") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "sp-io") (r "=23.1.0-dev.6") (d #t) (k 0)) (d (n "westend-runtime") (r "=0.1.0-dev.6") (d #t) (k 0)))) (h "16qjhkx8np1pygjrxlihxq89fsf1hsv16g39ipnh6rba6wrp25d4")))

(define-public crate-polkadot-voter-bags-1.0.0 (c (n "polkadot-voter-bags") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^22.0.0") (d #t) (k 0)) (d (n "kusama-runtime") (r "^1.0.0") (d #t) (k 0) (p "staging-kusama-runtime")) (d (n "polkadot-runtime") (r "^1.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^24.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^1.0.0") (d #t) (k 0)))) (h "0yp78q6pab93cg30f51qfq14f06aaiwzw6110wvhpdl9wfzjfk64")))

(define-public crate-polkadot-voter-bags-2.0.0 (c (n "polkadot-voter-bags") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^23.0.0") (d #t) (k 0)) (d (n "kusama-runtime") (r "^2.0.0") (d #t) (k 0) (p "staging-kusama-runtime")) (d (n "polkadot-runtime") (r "^2.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^25.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^2.0.0") (d #t) (k 0)))) (h "0c8v8rxbfjy91wvbh1rl2nmk6pwzkbvdxx0zjlywjyvp71bzz82x")))

(define-public crate-polkadot-voter-bags-3.0.0-dev.1 (c (n "polkadot-voter-bags") (v "3.0.0-dev.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "=24.0.0-dev.1") (d #t) (k 0)) (d (n "kusama-runtime") (r "=3.0.0-dev.1") (d #t) (k 0) (p "staging-kusama-runtime")) (d (n "polkadot-runtime") (r "=3.0.0-dev.1") (d #t) (k 0)) (d (n "sp-io") (r "=26.0.0-dev.1") (d #t) (k 0)) (d (n "westend-runtime") (r "=3.0.0-dev.1") (d #t) (k 0)))) (h "1yv68akjz72xbp30ks8l1xkymf1vdpbwg8glq809pk57gk8gz937")))

(define-public crate-polkadot-voter-bags-3.0.0 (c (n "polkadot-voter-bags") (v "3.0.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^24.0.0") (d #t) (k 0)) (d (n "kusama-runtime") (r "^3.0.0") (d #t) (k 0) (p "staging-kusama-runtime")) (d (n "polkadot-runtime") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^26.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^3.0.0") (d #t) (k 0)))) (h "1q8qmdmhbgi6lqgnk7am3rk6b8yv4n8vbwa0g6dxsrrgnq91hwg1")))

(define-public crate-polkadot-voter-bags-4.0.0 (c (n "polkadot-voter-bags") (v "4.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^25.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^27.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^4.0.0") (d #t) (k 0)))) (h "15vg1izm1a7gxp7c3biaa71vbygpfywslgvqcbfkr6nhlg5yj36r")))

(define-public crate-polkadot-voter-bags-5.0.0 (c (n "polkadot-voter-bags") (v "5.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^26.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^28.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^5.0.0") (d #t) (k 0)))) (h "1ywhkw6hmwybx9rdqj269dlfxl7yaam8izpn8k55qhkadkcaiqw0")))

(define-public crate-polkadot-voter-bags-6.0.0 (c (n "polkadot-voter-bags") (v "6.0.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^27.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^29.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^6.0.0") (d #t) (k 0)))) (h "1ik28q41fgcpf7196yfkj7zmhxsyrlqll81hrshfnkjgais3pqam")))

(define-public crate-polkadot-voter-bags-7.0.0 (c (n "polkadot-voter-bags") (v "7.0.0") (d (list (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^28.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^30.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^7.0.0") (d #t) (k 0)))) (h "1w14rvdx4w4n9g620kkmgh9mf9k76mksgfjy75mxmj8ilhfa1a5g")))

(define-public crate-polkadot-voter-bags-8.0.0 (c (n "polkadot-voter-bags") (v "8.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^29.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^31.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^8.0.0") (d #t) (k 0)))) (h "18ivdj12ibwibd7xdyysh6r82qsqplmky00fgcrzkl5lwmcwckn8")))

(define-public crate-polkadot-voter-bags-9.0.0 (c (n "polkadot-voter-bags") (v "9.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^30.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^32.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^9.0.0") (d #t) (k 0)))) (h "1jx0d0fk5i4qi1lpcrzpx7wjngdrjpfx92zv3b65iwqyq7ylf3j6")))

(define-public crate-polkadot-voter-bags-10.0.0 (c (n "polkadot-voter-bags") (v "10.0.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^31.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^33.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^10.0.0") (d #t) (k 0)))) (h "0h26625jzz2c11yxg7mv7can94yc44c35q5fraysr8sx84936pz8")))

(define-public crate-polkadot-voter-bags-11.0.0 (c (n "polkadot-voter-bags") (v "11.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^32.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^34.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^11.0.0") (d #t) (k 0)))) (h "1mlg08y5j8wpl4vsykzmhc9bwkgjqz2l04iaz3jaf1f8skhf6y0k")))

(define-public crate-polkadot-voter-bags-12.0.0 (c (n "polkadot-voter-bags") (v "12.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^33.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^35.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^12.0.0") (d #t) (k 0)))) (h "10lzgb7bik8fghvjsrmgkiskm3ijd4bddi9pnwzmwpcjwbd1366d")))

(define-public crate-polkadot-voter-bags-13.0.0 (c (n "polkadot-voter-bags") (v "13.0.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "generate-bags") (r "^34.0.0") (d #t) (k 0)) (d (n "sp-io") (r "^36.0.0") (d #t) (k 0)) (d (n "westend-runtime") (r "^13.0.0") (d #t) (k 0)))) (h "022llgnqdirs4b7a99qng90w8fk3c9k6kz5gmia0blavhfgqxr1a")))

