(define-module (crates-io po lk polkavm-linux-raw) #:use-module (crates-io))

(define-public crate-polkavm-linux-raw-0.1.1 (c (n "polkavm-linux-raw") (v "0.1.1") (h "0mk3153zxylk2q32izrh06apfyswx5j4w98qyc5hdxg9k5a4ix93") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.2.0 (c (n "polkavm-linux-raw") (v "0.2.0") (h "1bcm37vl5ym75bghj67l6rqdrss2ddz8sp0zvmbn34way7gbmigs") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.3.0 (c (n "polkavm-linux-raw") (v "0.3.0") (h "1lx0ji6793056p4b60v34smcds2irrisbp53ws1vmvckcbg862jv") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.4.0 (c (n "polkavm-linux-raw") (v "0.4.0") (h "120q8kdqlw0h4y8m6ksf8d271839gh7fis6zsc2imnqsn0bixyvq") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.5.0 (c (n "polkavm-linux-raw") (v "0.5.0") (h "1baxhic69vvwx225pij2l0vwckcch3l7kn1jm3f30j8c9rb4kzm6") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.6.0 (c (n "polkavm-linux-raw") (v "0.6.0") (h "0x2jr7ijnz69iam5vspl77nd717yxc4bm7yqbwnm1if6l9gz3sdw") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.7.0 (c (n "polkavm-linux-raw") (v "0.7.0") (h "035d9hbxh8rc3pb23419f47gk04zyiy1mhc33rzg0yqpsdxbm22c") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.8.0 (c (n "polkavm-linux-raw") (v "0.8.0") (h "050qk3lqg692h1qcw5a7ls6j16a0z2b8jr4hizwvp8595zpm5zps") (f (quote (("std") ("default")))) (r "1.70.0")))

(define-public crate-polkavm-linux-raw-0.9.0 (c (n "polkavm-linux-raw") (v "0.9.0") (h "083iaps2xf0b09mydl9yi6ppz10m75h8bz0czw6nb3llaqs5vs16") (f (quote (("std") ("default")))) (r "1.70.0")))

