(define-module (crates-io po lk polkadot-node-subsystem) #:use-module (crates-io))

(define-public crate-polkadot-node-subsystem-0.0.0 (c (n "polkadot-node-subsystem") (v "0.0.0") (h "17skvg7r3phj6f34qjlj7826irf8akrfp8b1zb6wr1yasyxvncfw") (y #t)))

(define-public crate-polkadot-node-subsystem-0.1.0-dev.6 (c (n "polkadot-node-subsystem") (v "0.1.0-dev.6") (d (list (d (n "polkadot-node-jaeger") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "=0.1.0-dev.6") (d #t) (k 0)) (d (n "polkadot-overseer") (r "=0.1.0-dev.6") (d #t) (k 0)))) (h "1jblh4ahz3a8r9y4iky68hrg2zbxy189nchp9pjc0vf3mgipxb1m")))

(define-public crate-polkadot-node-subsystem-1.0.0 (c (n "polkadot-node-subsystem") (v "1.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^1.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^1.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^1.0.0") (d #t) (k 0)))) (h "03sgvpa7qg1ah3c1953sx1sb5ddyf3hy1ln0q3l7xpb9anifs4wz")))

(define-public crate-polkadot-node-subsystem-2.0.0 (c (n "polkadot-node-subsystem") (v "2.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^2.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^2.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^2.0.0") (d #t) (k 0)))) (h "11wddagvys3z8glpzm29jf9iaz2w4x09jqhz3jqbqmqwpf48cswh")))

(define-public crate-polkadot-node-subsystem-3.0.0-dev.1 (c (n "polkadot-node-subsystem") (v "3.0.0-dev.1") (d (list (d (n "polkadot-node-jaeger") (r "=3.0.0-dev.1") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "=3.0.0-dev.1") (d #t) (k 0)) (d (n "polkadot-overseer") (r "=3.0.0-dev.1") (d #t) (k 0)))) (h "1i1rvj880zg50l9b1vkx446c2wr4129qinkj6nzfhdmlamz0igm4")))

(define-public crate-polkadot-node-subsystem-3.0.0 (c (n "polkadot-node-subsystem") (v "3.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^3.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^3.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^3.0.0") (d #t) (k 0)))) (h "0y6y8ij8l0sknm7i6fsh8i8dgrnzkdvqdxbmc10j4hspl9csmc6w")))

(define-public crate-polkadot-node-subsystem-4.0.0 (c (n "polkadot-node-subsystem") (v "4.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^4.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^4.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^4.0.0") (d #t) (k 0)))) (h "0vf96hav63lc4c51mb6yzsc0c8axfp6d7x7zq9sp5mgbckwx728f")))

(define-public crate-polkadot-node-subsystem-5.0.0 (c (n "polkadot-node-subsystem") (v "5.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^5.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^5.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^5.0.0") (d #t) (k 0)))) (h "0kxwz5cwq8f6dd5w8l5hs37wjpw3ja7sajcsrr41xqm6czq4z3z1")))

(define-public crate-polkadot-node-subsystem-6.0.0 (c (n "polkadot-node-subsystem") (v "6.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^6.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^6.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^6.0.0") (d #t) (k 0)))) (h "04vkflhf7md6c2jzvpf8p73jcbxc7p6whlb7mbnwjnylgdx4zvrr")))

(define-public crate-polkadot-node-subsystem-7.0.0 (c (n "polkadot-node-subsystem") (v "7.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^7.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^7.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^7.0.0") (d #t) (k 0)))) (h "0rn40czim8gq6j2h5y7s8d12qjxjnhag5y3jp257v3aqyrxz0743")))

(define-public crate-polkadot-node-subsystem-8.0.0 (c (n "polkadot-node-subsystem") (v "8.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^8.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^8.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^8.0.0") (d #t) (k 0)))) (h "0w916p9zcwbprz02zv1flzwkj65h1wpm3n06wnqqmm5wrlkc5mi4")))

(define-public crate-polkadot-node-subsystem-9.0.0 (c (n "polkadot-node-subsystem") (v "9.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^9.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^9.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^9.0.0") (d #t) (k 0)))) (h "0rs7v4zfi0c0qi332k8qr5fcfp4477ylwl75v6wmqb5gs7fh4ads")))

(define-public crate-polkadot-node-subsystem-10.0.0 (c (n "polkadot-node-subsystem") (v "10.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^10.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^10.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^10.0.0") (d #t) (k 0)))) (h "1xp6a07nnbcl4fpw1j7gp5yzgs5lmkid9ca0dj0rvaqfgx85r1n7")))

(define-public crate-polkadot-node-subsystem-11.0.0 (c (n "polkadot-node-subsystem") (v "11.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^11.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^11.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^11.0.0") (d #t) (k 0)))) (h "1scdqnbzm3i6a39j4mcmk3m4qv7287v0pag1f76ffk01nf3bsnvd")))

(define-public crate-polkadot-node-subsystem-12.0.0 (c (n "polkadot-node-subsystem") (v "12.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^12.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^12.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^12.0.0") (d #t) (k 0)))) (h "0rkx8a85psn9s8cbg7rm4psh21d4r15myy0pyvpv4370n770mfds")))

(define-public crate-polkadot-node-subsystem-13.0.0 (c (n "polkadot-node-subsystem") (v "13.0.0") (d (list (d (n "polkadot-node-jaeger") (r "^13.0.0") (d #t) (k 0)) (d (n "polkadot-node-subsystem-types") (r "^13.0.0") (d #t) (k 0)) (d (n "polkadot-overseer") (r "^13.0.0") (d #t) (k 0)))) (h "0yw7aq4hfzfm7rjaqdbhaxr2k39habcnwvl9jqg326wc944hqmla")))

