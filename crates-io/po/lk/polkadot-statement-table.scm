(define-module (crates-io po lk polkadot-statement-table) #:use-module (crates-io))

(define-public crate-polkadot-statement-table-0.0.0 (c (n "polkadot-statement-table") (v "0.0.0") (h "0b3a1vq4g18zm76740dssqxmvv85ny8m47i3q0s9rhq81z5zkl5y") (y #t)))

(define-public crate-polkadot-statement-table-0.1.0-dev.1 (c (n "polkadot-statement-table") (v "0.1.0-dev.1") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^0.1.0-dev.1") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^21.1.0-dev.2") (d #t) (k 0)))) (h "1ib96iqj2pb8h8zsidqkyf1l9li0n77nps6c18idczv2fxx8xcd5")))

(define-public crate-polkadot-statement-table-0.1.0-dev.3 (c (n "polkadot-statement-table") (v "0.1.0-dev.3") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^0.1.0-dev.3") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^21.1.0-dev.3") (d #t) (k 0)))) (h "1khj6ng4pc1mprdx47hplrcpha5wv8bpqrx5v8a0lbkdgnhwclka")))

(define-public crate-polkadot-statement-table-0.1.0-dev.6 (c (n "polkadot-statement-table") (v "0.1.0-dev.6") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "=0.1.0-dev.6") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "=21.1.0-dev.6") (d #t) (k 0)))) (h "0b184zfblaqwqhg7mrrsj0829x655h0smn8dxjy26ym3h9q9kfcf")))

(define-public crate-polkadot-statement-table-1.0.0 (c (n "polkadot-statement-table") (v "1.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^1.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^22.0.0") (d #t) (k 0)))) (h "08wc75n63y7pxjvdzzpkp8snvny3c9b0xsapv8bh02b5blzpv4gp")))

(define-public crate-polkadot-statement-table-2.0.0 (c (n "polkadot-statement-table") (v "2.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^2.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^23.0.0") (d #t) (k 0)))) (h "1nkywz17ch2fgw09laqalv5jl9zn3il49hr6v39xmb9kh60bym9b")))

(define-public crate-polkadot-statement-table-3.0.0-dev.1 (c (n "polkadot-statement-table") (v "3.0.0-dev.1") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "=3.0.0-dev.1") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "=24.0.0-dev.1") (d #t) (k 0)))) (h "00a70hxhwhnmx8kvdwly88xgdqwjryzw6n8zr33ib5spdvcdqamv")))

(define-public crate-polkadot-statement-table-3.0.0 (c (n "polkadot-statement-table") (v "3.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^3.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^24.0.0") (d #t) (k 0)))) (h "0las5g3mkma47i9qw1c1irvlvnp589av2kcfyd43qd9f0a1lqyw5")))

(define-public crate-polkadot-statement-table-4.0.0 (c (n "polkadot-statement-table") (v "4.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^4.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^25.0.0") (d #t) (k 0)))) (h "1kmckpw0ymfsmkwh1vj4aq38d88r5p4mbr7mm0q7w7w7p0fa3ci2")))

(define-public crate-polkadot-statement-table-5.0.0 (c (n "polkadot-statement-table") (v "5.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^5.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^26.0.0") (d #t) (k 0)))) (h "07600k3h5s99n0v7hr7rrprx245rqfbkr91f3sryvm430cqahdcz")))

(define-public crate-polkadot-statement-table-6.0.0 (c (n "polkadot-statement-table") (v "6.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^6.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)))) (h "1bsgs777gsk42q8dzm4d0nfbjhx1v9b63lligax3k9z7srfi144f")))

(define-public crate-polkadot-statement-table-7.0.0 (c (n "polkadot-statement-table") (v "7.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^7.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^28.0.0") (d #t) (k 0)))) (h "13wc72g9xhbdm6qp2dhhis2zwa2gdrhlkqfs2184dcjy4q1ym3a7")))

(define-public crate-polkadot-statement-table-8.0.0 (c (n "polkadot-statement-table") (v "8.0.0") (d (list (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^8.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^29.0.0") (d #t) (k 0)))) (h "1dkwfishf7byr0x155ri1jx302gq58z2dm77j14y2w67vg6digx2")))

(define-public crate-polkadot-statement-table-9.0.0 (c (n "polkadot-statement-table") (v "9.0.0") (d (list (d (n "gum") (r "^9.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^9.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^30.0.0") (d #t) (k 0)))) (h "0nfpym09vib0qw57h6kfhxlacfl345bvdc995msqscdi4508kyxj")))

(define-public crate-polkadot-statement-table-8.0.1 (c (n "polkadot-statement-table") (v "8.0.1") (d (list (d (n "gum") (r "^8.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^8.0.1") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^29.0.0") (d #t) (k 0)))) (h "00cd2nzp7g6b9xknz23zlg4gz5fr0id843rx4s7mv9n6lc6h2pny")))

(define-public crate-polkadot-statement-table-10.0.0 (c (n "polkadot-statement-table") (v "10.0.0") (d (list (d (n "gum") (r "^10.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^10.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^31.0.0") (d #t) (k 0)))) (h "0fw0pcg1x5li161g46znp9yccggp0gxhphk07cjf2iyd19r490d1")))

(define-public crate-polkadot-statement-table-11.0.0 (c (n "polkadot-statement-table") (v "11.0.0") (d (list (d (n "gum") (r "^11.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^11.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)))) (h "19yw06zhqrgh0nzw2fhbfy5h6912vdclik142yxb8gp6d968kjgg")))

(define-public crate-polkadot-statement-table-12.0.0 (c (n "polkadot-statement-table") (v "12.0.0") (d (list (d (n "gum") (r "^12.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^12.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)))) (h "1dnyz2aiqnq5s5ca1i1gywgf1hc5fxnq5m0bnw2jmpkvnb19x1sb")))

(define-public crate-polkadot-statement-table-13.0.0 (c (n "polkadot-statement-table") (v "13.0.0") (d (list (d (n "gum") (r "^13.0.0") (d #t) (k 0) (p "tracing-gum")) (d (n "parity-scale-codec") (r "^3.6.1") (f (quote ("derive"))) (k 0)) (d (n "primitives") (r "^13.0.0") (d #t) (k 0) (p "polkadot-primitives")) (d (n "sp-core") (r "^33.0.0") (d #t) (k 0)))) (h "0bkwbdh37yvqwlf2skhiwrj5rzlji1qw5q55hpscshyhvg4m96wv")))

