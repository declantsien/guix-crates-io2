(define-module (crates-io po lk polkavm-derive-impl-macro) #:use-module (crates-io))

(define-public crate-polkavm-derive-impl-macro-0.8.0 (c (n "polkavm-derive-impl-macro") (v "0.8.0") (d (list (d (n "polkavm-derive-impl") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0rpg5mrw02km9njdsp50fyvmykw0w03jdii1y34rs4nml0cm7s0m") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-macro-0.9.0 (c (n "polkavm-derive-impl-macro") (v "0.9.0") (d (list (d (n "polkavm-derive-impl") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0adlxhjskpn87djikyhgiq0b22wy7ipql5dnir91xj5abxxiza4b") (r "1.70.0")))

