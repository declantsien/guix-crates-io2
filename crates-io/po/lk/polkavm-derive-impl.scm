(define-module (crates-io po lk polkavm-derive-impl) #:use-module (crates-io))

(define-public crate-polkavm-derive-impl-0.2.0 (c (n "polkavm-derive-impl") (v "0.2.0") (d (list (d (n "polkavm-common") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0bfv57pcngfl5ncaic4952gznsmgi8pgzb20dxgg6s6p7snicglh") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.3.0 (c (n "polkavm-derive-impl") (v "0.3.0") (d (list (d (n "polkavm-common") (r "^0.3.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "08rip91938j957pwxvjxmvqrgmdh89lbk1q86iq143i7jcgqfad9") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.4.0 (c (n "polkavm-derive-impl") (v "0.4.0") (d (list (d (n "polkavm-common") (r "^0.4.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0zqviid29lmikdbfiqzm7dlsbhr4v2clr1svi2ffyd7lkxx4x7y9") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.5.0 (c (n "polkavm-derive-impl") (v "0.5.0") (d (list (d (n "polkavm-common") (r "^0.5.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0zcqnfa9n1wqkgdgkxjwkw3mv98w1s5xcmr0ncpzpfjv6sri30nw") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.6.0 (c (n "polkavm-derive-impl") (v "0.6.0") (d (list (d (n "polkavm-common") (r "^0.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9jk4pmi1gv01c4b21zlyx5nl7bk9k0hqjm92vv69jh23f7bapj") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.7.0 (c (n "polkavm-derive-impl") (v "0.7.0") (d (list (d (n "polkavm-common") (r "^0.7.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0psndxszy79p9qd9pmh650scpl8q9l37zhg7y881qb3nycag27il") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.8.0 (c (n "polkavm-derive-impl") (v "0.8.0") (d (list (d (n "polkavm-common") (r "^0.8.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "173cvskfvq49sdjlmfaa94bh1kv2nabkxfdzc31862m1m1a2c2y1") (r "1.70.0")))

(define-public crate-polkavm-derive-impl-0.9.0 (c (n "polkavm-derive-impl") (v "0.9.0") (d (list (d (n "polkavm-common") (r "^0.9.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0366jv4im8rk26qkx01c98wynr671f7d59bldqcrmyqpjz2dyksw") (r "1.70.0")))

