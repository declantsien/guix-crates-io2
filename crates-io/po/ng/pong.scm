(define-module (crates-io po ng pong) #:use-module (crates-io))

(define-public crate-pong-0.1.0 (c (n "pong") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z9w2rjwpdr0cav5gn5zb6rqmqakhzpnnx6dnc13zmc3gx8if26j")))

(define-public crate-pong-0.1.1 (c (n "pong") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jxrjk6fsd5d4q5ad8kd3svslva1kkmadnxzzzn4669afsagl3cb")))

