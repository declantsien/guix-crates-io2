(define-module (crates-io po we powershell_script) #:use-module (crates-io))

(define-public crate-powershell_script-0.1.0 (c (n "powershell_script") (v "0.1.0") (h "0mhpk7p544zf6634icvfkhyrmjbyzs03hdxhs9gjvdhmkm8a5nzz")))

(define-public crate-powershell_script-0.1.1 (c (n "powershell_script") (v "0.1.1") (h "0vyjzcz4pj43ax3ipbfxqakk9lmpj86kz9lm1mkl573hyz8dp9a1")))

(define-public crate-powershell_script-0.1.2 (c (n "powershell_script") (v "0.1.2") (h "11pciyk3dzw6r5ycxsczhb584s9j7624nzil2558lxjqya4iz9g3")))

(define-public crate-powershell_script-0.1.3 (c (n "powershell_script") (v "0.1.3") (h "18djiqw9pwb8msb43ysp6dw81xgxyh9jgjpn2ybn357m12py6wqj")))

(define-public crate-powershell_script-0.1.4 (c (n "powershell_script") (v "0.1.4") (h "19h78gm8k53rc3qpp65gsywdll9xy8ndn110a7v5z206dfwn7907")))

(define-public crate-powershell_script-0.1.5 (c (n "powershell_script") (v "0.1.5") (h "0gmhm1f0dx2yfw55pdym4hk6ja620nnnf0fgpn6qiv1a9b546b13")))

(define-public crate-powershell_script-0.2.0 (c (n "powershell_script") (v "0.2.0") (h "0qx2mkw7k3sw9lh2j41y70zfv0girb408a43sfr5b7jlqlfz56rc") (f (quote (("core"))))))

(define-public crate-powershell_script-0.2.1 (c (n "powershell_script") (v "0.2.1") (h "1cmd6pv6askbj3a1bbw537zmp3ms8a3r30hdkpcqh3jryna2imin") (f (quote (("core"))))))

(define-public crate-powershell_script-0.3.0 (c (n "powershell_script") (v "0.3.0") (h "0gcvby41nxmfxmg1i39ray1q58g51ajpy39wr3np8wnjzbz6r9vh") (f (quote (("core"))))))

(define-public crate-powershell_script-0.3.1 (c (n "powershell_script") (v "0.3.1") (h "0aqmpwzqaa5scqbjjahc4nz7lzair6msl6p5ad12lkivlinavkc2") (f (quote (("core"))))))

(define-public crate-powershell_script-0.3.2 (c (n "powershell_script") (v "0.3.2") (h "0rgqvwy0h7znapxyq316jljjp46jljjvr3ry31hyj70rhf3k6nha") (f (quote (("core"))))))

(define-public crate-powershell_script-1.0.0 (c (n "powershell_script") (v "1.0.0") (h "06vg8z12bn0zkrzsa43i34xqaqhi7xli6zr05ynry6zgdzaix36v") (f (quote (("core"))))))

(define-public crate-powershell_script-1.0.1 (c (n "powershell_script") (v "1.0.1") (h "0vf2v6hrw447z27lm7pf077nibzjdndj1pdpiw52hbccylvpg85g") (f (quote (("core"))))))

(define-public crate-powershell_script-1.0.2 (c (n "powershell_script") (v "1.0.2") (h "00vcmgf48rxjkxsqnrbv9prnzxy2rb5pnhq4ih6bhhmlrjcqap4x") (f (quote (("core"))))))

(define-public crate-powershell_script-1.0.3 (c (n "powershell_script") (v "1.0.3") (h "0sn52v5q86chd9vpn1z0xp7903sqzhwsc5i3qg6gklrpj7s3g1w1") (f (quote (("core"))))))

(define-public crate-powershell_script-1.0.4 (c (n "powershell_script") (v "1.0.4") (h "0m6dy823vy4rvba1gpqf29xpp8qdc60xfwih8blsxh4mlghy5gal") (f (quote (("core"))))))

(define-public crate-powershell_script-1.1.0 (c (n "powershell_script") (v "1.1.0") (h "0m546593q23893gs3pq9bl745maibh7bqmj20hx3szwij1h37y5y") (f (quote (("core"))))))

