(define-module (crates-io po we powercap-sys) #:use-module (crates-io))

(define-public crate-powercap-sys-0.1.0 (c (n "powercap-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1siyqibiv3s4nvax2rynkd5alphzscndbxfn1gn622ihz6n8xyyc")))

(define-public crate-powercap-sys-0.2.0 (c (n "powercap-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1f30n1l91qwd5xj0zd46mr2yii4rdggg32vyb4gxyrw0nlxw7qwk")))

