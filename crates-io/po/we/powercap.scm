(define-module (crates-io po we powercap) #:use-module (crates-io))

(define-public crate-powercap-0.1.0 (c (n "powercap") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0nkx0z302pfnlgk2dx3gdn3rpl3sbh7nq76l0qbbxk1qhp8mv7rk")))

(define-public crate-powercap-0.2.0 (c (n "powercap") (v "0.2.0") (d (list (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "0m385z4g52qsa5zscw4970px07p863w1b877w88a867954k67cml") (f (quote (("modules" "procfs") ("default" "modules"))))))

(define-public crate-powercap-0.3.0 (c (n "powercap") (v "0.3.0") (d (list (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "1bikxfnfm32g5fnbb2xyrhzkml64y98jxi9c8rhdrvg5ffapz7k4") (f (quote (("modules" "procfs") ("default" "modules"))))))

(define-public crate-powercap-0.3.1 (c (n "powercap") (v "0.3.1") (d (list (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "04c1ggfpdy91n24iaa0ikzlc9xd7bmpgkswk190sgsfg7i04lqyr") (f (quote (("modules" "procfs") ("default" "modules"))))))

(define-public crate-powercap-0.3.2 (c (n "powercap") (v "0.3.2") (d (list (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19rm1jxgc79ybzv0vy1bd7a2y7q03dfrk9yvmx970v71r74f6466") (f (quote (("with-serde" "serde") ("modules" "procfs") ("default" "modules"))))))

(define-public crate-powercap-0.3.3 (c (n "powercap") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "1yh8a49jxfw9ms499mcmbcv0602dqbm179h6c3dziyfh0fi9iavx") (f (quote (("modules" "procfs") ("mock") ("default" "modules"))))))

(define-public crate-powercap-0.3.4 (c (n "powercap") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "1dvznw0aqy4260kg9sx152p800w1zg4y1mhhgyiqyivl74msknvk") (f (quote (("modules" "procfs") ("mock") ("default" "modules"))))))

(define-public crate-powercap-0.3.5 (c (n "powercap") (v "0.3.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "procfs") (r "^0.11") (o #t) (k 0)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "0zhkk5x1z7q679drvf0dl9nlxgnvd66c42d203zahcx95xcyg5wc") (f (quote (("with-serde" "serde") ("modules" "procfs") ("mock") ("default" "modules"))))))

