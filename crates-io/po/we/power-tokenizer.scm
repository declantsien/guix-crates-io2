(define-module (crates-io po we power-tokenizer) #:use-module (crates-io))

(define-public crate-power-tokenizer-0.1.0 (c (n "power-tokenizer") (v "0.1.0") (h "1mwlvmmvqa24pl200jcaaxbyglllpmg47pa1aqhg4pn20515nkvr") (y #t)))

(define-public crate-power-tokenizer-0.1.1 (c (n "power-tokenizer") (v "0.1.1") (h "1al4ngh0j6cp0pq87h7vdwr8cc51nbhp7i798c7hw0h79m1qvzzz") (y #t)))

(define-public crate-power-tokenizer-0.2.1 (c (n "power-tokenizer") (v "0.2.1") (h "060g3d8jx8bx6dwc5k0j78qqiii4km7rgcqzgh4rbgj96ixn5s22") (y #t)))

(define-public crate-power-tokenizer-0.3.0 (c (n "power-tokenizer") (v "0.3.0") (h "1d1pyjm90m3sc9jx7093arkixymzsmcij0686jhqdgpwxbv8s6cd") (y #t)))

