(define-module (crates-io po we power-consistent-hash) #:use-module (crates-io))

(define-public crate-power-consistent-hash-0.1.0 (c (n "power-consistent-hash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "seahash") (r "^4.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "02634v720igc6pafkaichjgxlwmnig0w3f4nvhxxp31nw0psnm8v") (s 2) (e (quote (("seahash-bench" "seahash" "dep:uuid") ("seahash" "dep:seahash"))))))

