(define-module (crates-io po we power-assert) #:use-module (crates-io))

(define-public crate-power-assert-0.1.0 (c (n "power-assert") (v "0.1.0") (h "1fin6qvgasfsc65nx049brpvbs59ph785xxf3v9mxmdp8z43zi3h") (f (quote (("replace-builtin") ("default"))))))

(define-public crate-power-assert-0.2.0 (c (n "power-assert") (v "0.2.0") (h "1j1sg65rx3vgnlmg01rbzngias546mzivkriayl48pwmcc0n4xk7")))

(define-public crate-power-assert-0.2.1 (c (n "power-assert") (v "0.2.1") (h "0ggd43mjjpmlhkx6dvc1f6iqnfqwzl6m0rid4dxik31x8jffzwhi")))

(define-public crate-power-assert-0.3.0 (c (n "power-assert") (v "0.3.0") (h "09jfwhsmpxbq5bkvkqghrg01230pn3p9amg3aw6c8xxr3ak05n6v")))

(define-public crate-power-assert-0.3.1 (c (n "power-assert") (v "0.3.1") (h "143cfiy78rh0zi2zgflskq2qy3k9pk4rj5kah5lafjrpbn10njz4")))

(define-public crate-power-assert-0.3.2 (c (n "power-assert") (v "0.3.2") (h "18320djmkv4cl1bs26ygb5x1f09cjg6c11baxnlc147g65gvs4zm")))

(define-public crate-power-assert-0.3.3 (c (n "power-assert") (v "0.3.3") (h "1sylkrdvgahca8i97ph3pkcsbn8hyrs92v8q3l4jcqb3j51qc0z0")))

(define-public crate-power-assert-0.3.4 (c (n "power-assert") (v "0.3.4") (h "121bczc2r8d6iqdw8vqzzqhj0r6xbwg7r4mwf1ddqfwpal95a99f")))

(define-public crate-power-assert-0.3.5 (c (n "power-assert") (v "0.3.5") (h "054flk1zfq2s1lvvimgvsgav6pqijag7w30ipb0a6n8cm37p13ks")))

(define-public crate-power-assert-0.3.6 (c (n "power-assert") (v "0.3.6") (h "1xcrm04isq6pfh63brw7qmcpq382q3ayxznqpcda8zl1i3zjfv9f")))

(define-public crate-power-assert-0.3.7 (c (n "power-assert") (v "0.3.7") (h "18xxjkpp340w0qwdhzix74rzjrl3lh0j71r4bhqrx9dyn2zpzxnr")))

(define-public crate-power-assert-0.3.8 (c (n "power-assert") (v "0.3.8") (h "0yig09kll5j8mr8fsvlk73v8cdgwh1s11gmb3zcr546cr2vwpx8s")))

(define-public crate-power-assert-0.3.9 (c (n "power-assert") (v "0.3.9") (h "1f5dhyrjzgd5l01nkcr9zmyg45k51vwqfr1x69fsg7s8b84bvr4s")))

