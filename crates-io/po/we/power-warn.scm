(define-module (crates-io po we power-warn) #:use-module (crates-io))

(define-public crate-power-warn-1.0.0 (c (n "power-warn") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "09xilgyw6f0p0mydc7ck1n4hwlgr8y8b0f5s4qsls4yjicppxmja")))

(define-public crate-power-warn-1.1.0 (c (n "power-warn") (v "1.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "19pplfj51fwjigpwfw4kray0w9mq5567pammhmrb7ha6xw7m1zb5")))

(define-public crate-power-warn-1.2.0 (c (n "power-warn") (v "1.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1ddhgqjzaws80x12ajzlwi64w9wzv50qw8cwvlmdkfliahsl5grg")))

