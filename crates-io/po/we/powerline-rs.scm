(define-module (crates-io po we powerline-rs) #:use-module (crates-io))

(define-public crate-powerline-rs-0.1.0 (c (n "powerline-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0gax7m62wlik3mgk94p4cgn04nhkszzx57killnbdj4bj14769ls") (f (quote (("default" "git2"))))))

(define-public crate-powerline-rs-0.1.1 (c (n "powerline-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 1)) (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0spw2r9md5yb8h5z4lfnjy1ryskkqnibg2s3nz3gpazrv0nvh6ls") (f (quote (("default" "git2"))))))

(define-public crate-powerline-rs-0.1.2 (c (n "powerline-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 1)) (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0wm8m6ahx38dlczg4f2qv3835fhp6zbiy35q12da6rs653850q1b") (f (quote (("default" "git2"))))))

(define-public crate-powerline-rs-0.1.4 (c (n "powerline-rs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 1)) (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0sldpqjdvn60q3m78yvsn05wcz7k2g1qi07msc4m3mvgn5n3zvrg") (f (quote (("default" "chrono" "git2"))))))

(define-public crate-powerline-rs-0.1.5 (c (n "powerline-rs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 1)) (d (n "flame") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1330kcfwckl5l9dxg1bksiy4k4lgvpwjg0b8qipjih096dr0782k") (f (quote (("default" "chrono" "git2"))))))

(define-public crate-powerline-rs-0.1.6 (c (n "powerline-rs") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 1)) (d (n "flame") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (o #t) (d #t) (k 0)))) (h "04g40dgaxzrxy04979w1w94xh7i06xiyhflg4cq3k6izyy8pxssj") (f (quote (("default" "chrono" "git2"))))))

(define-public crate-powerline-rs-0.1.7 (c (n "powerline-rs") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 1)) (d (n "flame") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (o #t) (d #t) (k 0)))) (h "1clf9vrhxawkb1r49rn99812170hhvi3m0wnf3m939zf6852xxzw") (f (quote (("default" "chrono" "git2"))))))

(define-public crate-powerline-rs-0.1.8 (c (n "powerline-rs") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "clap") (r "^2.29.2") (d #t) (k 1)) (d (n "flame") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.6.11") (o #t) (d #t) (k 0)))) (h "1sd251scnk4z938ammcl6j29sbmvjcxmrq78xvcng124j8bx8sap") (f (quote (("default" "chrono" "git2"))))))

(define-public crate-powerline-rs-0.1.9 (c (n "powerline-rs") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 1)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "flame") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (o #t) (d #t) (k 0)) (d (n "users") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0f319lp1j79cgm7pxy3kgfd29zcr68yax9la0jrmbay0gj0ajkbh") (f (quote (("default" "chrono" "git2" "users"))))))

(define-public crate-powerline-rs-0.2.0 (c (n "powerline-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 1)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "flame") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.10.1") (o #t) (k 0)) (d (n "users") (r "^0.9.1") (o #t) (d #t) (k 0)))) (h "07w54g08zix0p4f4d0rlq7cn3190rfbvpry566wyismjidxxsfsh") (f (quote (("default" "chrono" "git2" "users"))))))

