(define-module (crates-io po we powerbool) #:use-module (crates-io))

(define-public crate-powerbool-0.1.1 (c (n "powerbool") (v "0.1.1") (h "1zn0crysx453x1rghiyfzwkxzc0mrz0znpxl9ki53sxpk9szg0la")))

(define-public crate-powerbool-0.1.2 (c (n "powerbool") (v "0.1.2") (h "0kx8bwy43lc737s1vn99hmmr8k1rz788s2lk7p6qw5npxrjy5j6a")))

