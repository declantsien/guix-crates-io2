(define-module (crates-io po we powerpack) #:use-module (crates-io))

(define-public crate-powerpack-0.1.0 (c (n "powerpack") (v "0.1.0") (d (list (d (n "beef") (r "^0.5") (f (quote ("impl_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ji4vfahv945p7j5nlr94m6sg79zki0d3nwnvrb13ni8mbvj3bkg")))

(define-public crate-powerpack-0.1.1 (c (n "powerpack") (v "0.1.1") (d (list (d (n "beef") (r "^0.5") (f (quote ("impl_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x6mj8afiszl7a6i9gy69cba74ncv95f6pvxm88acgkyc9firjhz")))

(define-public crate-powerpack-0.1.2 (c (n "powerpack") (v "0.1.2") (d (list (d (n "beef") (r "^0.5") (f (quote ("impl_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s80f2zi124a1c15vif2ljy27jmnxw31kbxkzhia8r66pyn8xp27")))

(define-public crate-powerpack-0.2.0 (c (n "powerpack") (v "0.2.0") (d (list (d (n "dairy") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lrspvja4q61n7ivi28fhimaj6hfm8x9i6j7b265r41gl8qaz8zg")))

(define-public crate-powerpack-0.2.1 (c (n "powerpack") (v "0.2.1") (d (list (d (n "dairy") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qkasxlg5kbx8fcisszvzr9gk5j8ifiilycvk231kdg5h1f3cg2s")))

(define-public crate-powerpack-0.2.2 (c (n "powerpack") (v "0.2.2") (d (list (d (n "dairy") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j97x0pp6cs3bhrwv9xflzm93ik9lgyhh9gjffrw6ik4cwy6fspv")))

(define-public crate-powerpack-0.3.0 (c (n "powerpack") (v "0.3.0") (d (list (d (n "dairy") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0y66bfbsvmjzzcaqqmab0dabsfn997xijvmnalhcpgzfiyvxnbjx")))

(define-public crate-powerpack-0.3.1 (c (n "powerpack") (v "0.3.1") (d (list (d (n "dairy") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "powerpack-detach") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "145ay0axjfhhs4xb7was5y17p405wa7bdknkwdbr06i5xlx9nacy") (f (quote (("detach" "powerpack-detach") ("default"))))))

(define-public crate-powerpack-0.4.0 (c (n "powerpack") (v "0.4.0") (d (list (d (n "goldie") (r "^0.2.0") (d #t) (k 2)) (d (n "powerpack-detach") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "11qhsglnal04989g39dva6cq5jr6hvf0ly59q4qdwk4r5jmlcfv1") (f (quote (("detach" "powerpack-detach") ("default"))))))

(define-public crate-powerpack-0.4.1 (c (n "powerpack") (v "0.4.1") (d (list (d (n "goldie") (r "^0.2.0") (d #t) (k 2)) (d (n "powerpack-detach") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "11lqpwyi0jr8rjlrnfr58b9acxy3f2imvaf4xn8p9km196lp9a6d") (f (quote (("detach" "powerpack-detach") ("default"))))))

(define-public crate-powerpack-0.4.2 (c (n "powerpack") (v "0.4.2") (d (list (d (n "goldie") (r "^0.2.0") (d #t) (k 2)) (d (n "powerpack-detach") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "powerpack-env") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1ij7z56jpbrr5zd94rxb22sy133x7lm1ajcq5xazw4hln8dznlhg") (f (quote (("env" "powerpack-env") ("detach" "powerpack-detach") ("default" "env"))))))

(define-public crate-powerpack-0.5.0 (c (n "powerpack") (v "0.5.0") (d (list (d (n "goldie") (r "^0.4.3") (d #t) (k 2)) (d (n "powerpack-detach") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "powerpack-env") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0237xbj1zmcvlyzxl7d81qrya9m63gn71wr8lj3y2bn9in2f2sad") (f (quote (("default" "env")))) (s 2) (e (quote (("env" "dep:powerpack-env") ("detach" "dep:powerpack-detach"))))))

