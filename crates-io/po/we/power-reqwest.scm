(define-module (crates-io po we power-reqwest) #:use-module (crates-io))

(define-public crate-power-reqwest-0.1.0 (c (n "power-reqwest") (v "0.1.0") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 2)) (d (n "power-reqwest-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dbs2bydrxpq3srfydn6qj943n1gdbbp3h6i2cnvd7px1zfzv2ds")))

