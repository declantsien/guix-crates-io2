(define-module (crates-io po we power-instruction-analyzer) #:use-module (crates-io))

(define-public crate-power-instruction-analyzer-0.2.0 (c (n "power-instruction-analyzer") (v "0.2.0") (d (list (d (n "power-instruction-analyzer-proc-macro") (r "=0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3") (d #t) (k 0)))) (h "02jdg40m9adbxg35fzhrxr84k06x8l6zad7fp92pzxrfavbj6i3f") (f (quote (("python-extension" "python" "pyo3/extension-module") ("python" "pyo3") ("native_instrs"))))))

