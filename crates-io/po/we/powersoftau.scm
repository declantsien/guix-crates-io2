(define-module (crates-io po we powersoftau) #:use-module (crates-io))

(define-public crate-powersoftau-0.1.0 (c (n "powersoftau") (v "0.1.0") (d (list (d (n "blake2") (r "^0.6.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.8.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "pairing") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1aw7lbid8sgzppziq3mbijc0q2vn3fkgavkym8a2rklm0f2jalm2") (f (quote (("u128-support" "pairing/u128-support"))))))

