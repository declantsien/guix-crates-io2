(define-module (crates-io po we power_systems) #:use-module (crates-io))

(define-public crate-power_systems-0.1.0 (c (n "power_systems") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 1)) (d (n "gtmpl") (r "^0.7") (d #t) (k 1)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 1)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 1)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0yjr7qc1fxp6l7rcwm9mr1pla7cchk36lyhdj25rza30w9bsriib")))

