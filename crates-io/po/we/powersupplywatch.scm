(define-module (crates-io po we powersupplywatch) #:use-module (crates-io))

(define-public crate-powersupplywatch-1.0.0 (c (n "powersupplywatch") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03l7nii3rfqzp6mz2b01aixjsg78pvjmbgml8vw36pvf2q6m6k3d") (y #t)))

(define-public crate-powersupplywatch-1.0.1 (c (n "powersupplywatch") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q5q98j7dpvby7ak8ff393gx524ddc8msqdy5il3ydxkwbiw6lg9") (y #t)))

(define-public crate-powersupplywatch-1.0.2 (c (n "powersupplywatch") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19msa5388pzass4rnci9ghy20cafqj7aapd2jsp6f3ykx5w81gmv") (y #t)))

(define-public crate-powersupplywatch-1.0.3 (c (n "powersupplywatch") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wzbd0mdn4xv67diaji70sdgy5h8ip6xm44kg9gw8l4cy8crmmbx")))

(define-public crate-powersupplywatch-1.0.4 (c (n "powersupplywatch") (v "1.0.4") (d (list (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bc44y76pgf0v4was7sqz679ippcj720443pdw8kf5kdcz0f19x7")))

