(define-module (crates-io po we power-reqwest-lib) #:use-module (crates-io))

(define-public crate-power-reqwest-lib-0.1.0 (c (n "power-reqwest-lib") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-prelude") (r "^0.1.6") (d #t) (k 0)))) (h "0k62bj8nh7ylzml9pwkzqjjyinddldci0ri86nw6735md5xx0gyy")))

