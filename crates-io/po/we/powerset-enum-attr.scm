(define-module (crates-io po we powerset-enum-attr) #:use-module (crates-io))

(define-public crate-powerset-enum-attr-0.1.0 (c (n "powerset-enum-attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lbv837jf94fk1j6qjy2l07q21l3zb66fdklhrpc9iqdgvgnpgb5")))

