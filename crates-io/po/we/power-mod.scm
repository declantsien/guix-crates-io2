(define-module (crates-io po we power-mod) #:use-module (crates-io))

(define-public crate-power-mod-0.0.0 (c (n "power-mod") (v "0.0.0") (h "0fnbk923li3w4n7k34ri7wfp67aqnsbgvk6ngnwdpr7d19xj6g6f") (f (quote (("default"))))))

(define-public crate-power-mod-0.1.0 (c (n "power-mod") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0gcx38gaz4d7mxn4rls6gdzz1s5zjy0kv9s66bg83vwn0n2jw0ki") (f (quote (("default"))))))

