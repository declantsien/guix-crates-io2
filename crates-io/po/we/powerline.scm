(define-module (crates-io po we powerline) #:use-module (crates-io))

(define-public crate-powerline-0.1.0 (c (n "powerline") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b5m9gw9lhvmv64wyv9p1as6ncp2p3p4cqb9vh79ijk47b15h5kl")))

