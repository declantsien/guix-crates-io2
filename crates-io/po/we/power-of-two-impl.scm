(define-module (crates-io po we power-of-two-impl) #:use-module (crates-io))

(define-public crate-power-of-two-impl-0.1.0 (c (n "power-of-two-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15fv13fbci8ddc3qzi8wz992g6wxj62f88936d8zdypmbh7yncdm")))

