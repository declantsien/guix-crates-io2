(define-module (crates-io po we power-instruction-analyzer-proc-macro) #:use-module (crates-io))

(define-public crate-power-instruction-analyzer-proc-macro-0.2.0 (c (n "power-instruction-analyzer-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0zazyn2cgshvzs6jpi68lwbfbcg791391xczfp2xpn1wby2qhmvf")))

