(define-module (crates-io po we power-of-two) #:use-module (crates-io))

(define-public crate-power-of-two-0.1.0 (c (n "power-of-two") (v "0.1.0") (d (list (d (n "power-of-two-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "183sj7mhh7ab131sxda7jd2fyx7iqyasg7j2wc4wib8czpcrfzrl")))

