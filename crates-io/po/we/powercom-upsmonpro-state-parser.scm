(define-module (crates-io po we powercom-upsmonpro-state-parser) #:use-module (crates-io))

(define-public crate-powercom-upsmonpro-state-parser-1.0.0 (c (n "powercom-upsmonpro-state-parser") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1cgs6vlsaihwrbwpka03g88d130s6n6xnd81ps193pi0f45hir3m")))

