(define-module (crates-io po we powerfmt) #:use-module (crates-io))

(define-public crate-powerfmt-0.1.0 (c (n "powerfmt") (v "0.1.0") (d (list (d (n "powerfmt-macros") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "0mr2nbk6p23ly2n5kgd1ai2iq8zca34z234pv78hqjv7nxasbb7a") (f (quote (("std" "alloc") ("default" "std" "macros") ("alloc")))) (s 2) (e (quote (("macros" "dep:powerfmt-macros")))) (r "1.65.0")))

(define-public crate-powerfmt-0.1.1 (c (n "powerfmt") (v "0.1.1") (d (list (d (n "powerfmt-macros") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "021c0q3158gcnandz5mq1l87851dnwnsaha58pkmx71lv4g6gwp4") (f (quote (("std" "alloc") ("default" "std" "macros") ("alloc")))) (s 2) (e (quote (("macros" "dep:powerfmt-macros")))) (r "1.65.0")))

(define-public crate-powerfmt-0.1.2 (c (n "powerfmt") (v "0.1.2") (d (list (d (n "powerfmt-macros") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "1bkgzpw63z6sfdqm13igl7gcj8mi0bmqrpis05rd9hj78cd483xv") (f (quote (("std" "alloc") ("default" "std" "macros") ("alloc")))) (s 2) (e (quote (("macros" "dep:powerfmt-macros")))) (r "1.67.0")))

(define-public crate-powerfmt-0.2.0 (c (n "powerfmt") (v "0.2.0") (d (list (d (n "powerfmt-macros") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "14ckj2xdpkhv3h6l5sdmb9f1d57z8hbfpdldjc2vl5givq2y77j3") (f (quote (("std" "alloc") ("default" "std" "macros") ("alloc")))) (s 2) (e (quote (("macros" "dep:powerfmt-macros")))) (r "1.67.0")))

