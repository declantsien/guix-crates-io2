(define-module (crates-io po st postal-rs) #:use-module (crates-io))

(define-public crate-postal-rs-0.1.0 (c (n "postal-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "131vnwxkf6gwygcmy3h30i7zdhjhjfsxmvdmcz8qg8shnq8qwk8l")))

