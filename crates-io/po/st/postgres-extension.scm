(define-module (crates-io po st postgres-extension) #:use-module (crates-io))

(define-public crate-postgres-extension-0.1.0 (c (n "postgres-extension") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.47") (d #t) (k 0)))) (h "0rn8bl79841mz7sp3g0z46hinyhc392133c5jjxyjdwrlnf0nz8c")))

(define-public crate-postgres-extension-0.1.1 (c (n "postgres-extension") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.47") (d #t) (k 0)))) (h "1j96gvmhrqwsk2kbhkqsqmwkvhnyjna6ky0c8rq41gjgmhs2nwxq")))

