(define-module (crates-io po st postgres_query_derive) #:use-module (crates-io))

(define-public crate-postgres_query_derive-0.1.0 (c (n "postgres_query_derive") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "12cgc3rc9wfypbqkcyyhn4lsb38agwmlxjhmckhv95cxvw9l5w9g")))

(define-public crate-postgres_query_derive-0.1.1 (c (n "postgres_query_derive") (v "0.1.1") (d (list (d (n "postgres") (r "^0.15.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "19cid2j00chly1wgw0qz4a84gxy4vm8s5v4xd6y8wv81dqx400mk")))

