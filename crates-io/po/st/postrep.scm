(define-module (crates-io po st postrep) #:use-module (crates-io))

(define-public crate-postrep-0.1.0 (c (n "postrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "etcd-client") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("net" "time" "rt-multi-thread" "macros" "io-util" "fs" "sync" "signal"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0kv67fl9n7jrij4hw6n2ylpjw8asn8vg26ganskkrx0f8ygp8wd6")))

