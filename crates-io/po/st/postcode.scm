(define-module (crates-io po st postcode) #:use-module (crates-io))

(define-public crate-postcode-0.1.0 (c (n "postcode") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)))) (h "06dnm63wc6r3404siga2fk6pjblmyb9l6bhz596z7pskg12jf7kj")))

(define-public crate-postcode-0.1.1 (c (n "postcode") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "11g9yhr5k7wf9dwnyw8lxqvvc0z4r91x5zxmmq5k3dk6472ggg9w")))

