(define-module (crates-io po st postgres-derives) #:use-module (crates-io))

(define-public crate-postgres-derives-0.1.0 (c (n "postgres-derives") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6lxsyjkdddakkq8na8pyiqc28f6n235igy3b2x03mifvxk41ck")))

