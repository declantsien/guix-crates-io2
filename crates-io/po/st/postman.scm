(define-module (crates-io po st postman) #:use-module (crates-io))

(define-public crate-postman-0.1.0 (c (n "postman") (v "0.1.0") (h "1nrayivfv9z9af2d1vwlq8yfdvj7xd8jvh7sabjcb9pa5zil42s6")))

(define-public crate-postman-1.0.0 (c (n "postman") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "11x20l9chbkzmpmnsksfbbgrsvny6vha4y3iha71zfr3d4gc1wdw")))

(define-public crate-postman-2.0.0 (c (n "postman") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1rfvslyh5lmi0dkqhg6zl2v75y1qjyn0n7gs2krmvb8mpj3f8qlq")))

(define-public crate-postman-3.0.0 (c (n "postman") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1grxfsn3lbwcygmij8f6zqjyg08x76gs8y5bz6sgxc5d800k40g3")))

