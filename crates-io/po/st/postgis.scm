(define-module (crates-io po st postgis) #:use-module (crates-io))

(define-public crate-postgis-0.1.0 (c (n "postgis") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.8.9") (d #t) (k 0)))) (h "0b3giy5cm3vlb0082ckdazsqqpz5b6k9ig2vnab2j5wg59f6gzg0")))

(define-public crate-postgis-0.1.1 (c (n "postgis") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.8.9") (d #t) (k 0)))) (h "0vp96fwij8m1hkhx9638zk371jcw684b7bmh2xwavl62sjsh9456")))

(define-public crate-postgis-0.1.2 (c (n "postgis") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.8.9") (d #t) (k 0)))) (h "1vgxag3nvj709wyd7543lhzxrgadc8y5v3y8yjvdb0w0hmijnjdl")))

(define-public crate-postgis-0.1.3 (c (n "postgis") (v "0.1.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.8.9") (d #t) (k 0)))) (h "0w5xr30xx09i1mq9kvjcxnj048mqfcr7x5pj53gpw4d1nkzm9qyx")))

(define-public crate-postgis-0.1.4 (c (n "postgis") (v "0.1.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)))) (h "1bf6l537yzppxd0iw9qglca6g0cqw9aca5blnl1gqk9f10s9mwb9")))

(define-public crate-postgis-0.1.5 (c (n "postgis") (v "0.1.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)))) (h "0vbbqxr108fj7hqndpkvk1rmvzay8647f2ib6wkyqrjly8jqjziq")))

(define-public crate-postgis-0.1.6 (c (n "postgis") (v "0.1.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)))) (h "0l33z95i3g7yj85cfm0hdzlrlxxdgvh0v2xfgpal7alk156vzscc")))

(define-public crate-postgis-0.2.0 (c (n "postgis") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.10") (d #t) (k 0)))) (h "1612sykz8mhpx6v1qzvkgf6mabzkps78kdhfnfrf36crc303ck2v")))

(define-public crate-postgis-0.2.1 (c (n "postgis") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "0bhyi5ghj18m3xkb8sqfgpk4qcxc251s2kvkk1n36igfv7fdynf8")))

(define-public crate-postgis-0.2.2 (c (n "postgis") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "0wpgw598j2yrbw3f40lws77cdpfvhcza7wvwwp4whkv3mdxmp58k")))

(define-public crate-postgis-0.3.0 (c (n "postgis") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.13") (d #t) (k 0)))) (h "1vscqa0r1zd2rqzdyb6indz5bmk5q89fl1m38fz4anga2z8cr621")))

(define-public crate-postgis-0.4.0 (c (n "postgis") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)))) (h "1q1pqy9mwsaiznk9n3ylqjm97bcqgzw1clwq4x0625kqyk1vdffp")))

(define-public crate-postgis-0.5.0 (c (n "postgis") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0hsbazs1kr5wj9i1kfxsiri7ngw8lya8d1bdiw8grikss77qkf9g")))

(define-public crate-postgis-0.6.0 (c (n "postgis") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "1ljg1kfyw8jya6nvxyb1ir8ssyklfhzlycj4gw2mwwkhqn85igvp")))

(define-public crate-postgis-0.7.0 (c (n "postgis") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 0)))) (h "0nxsjb26szfigx60gxmavp4syw6p17c0b0jsikcc2jkd5v066i6q")))

(define-public crate-postgis-0.8.0 (c (n "postgis") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0y8az8qfqsb58ccfdmip72zs6g4v2rjjsi2hwb08aqhx3x4afjxc")))

(define-public crate-postgis-0.8.1 (c (n "postgis") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0flbpd6bay8k777y5spcd53d5iapil6spp6qwl5vd9can9a3h2ha")))

(define-public crate-postgis-0.9.0 (c (n "postgis") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "02gcw12jdyayhmzfk8l4irb27csyj11rqczhs2njqs3s1dchc95m")))

