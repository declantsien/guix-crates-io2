(define-module (crates-io po st postman-pop3) #:use-module (crates-io))

(define-public crate-postman-pop3-0.1.0 (c (n "postman-pop3") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.3.4, <0.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bdrqv69ldsz1fahqwf68362vls56wsjk02bbs6rniqlwi5ljp12")))

(define-public crate-postman-pop3-0.1.1-alpha.1 (c (n "postman-pop3") (v "0.1.1-alpha.1") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.3.4, <0.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n2ablj28xbzry8ksv3rz6dqiqia82dz4jpdkfqcq82wmwh93qyh")))

(define-public crate-postman-pop3-0.1.1-alpha.2 (c (n "postman-pop3") (v "0.1.1-alpha.2") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.3.4, <0.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gs5fddgqm0zs4vgf6m3lmb38d27nfd7827dk4r385gicvlrn9m9")))

(define-public crate-postman-pop3-0.2.0 (c (n "postman-pop3") (v "0.2.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "bincode") (r ">=1.3.1, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.2, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r ">=0.34.6, <0.35.0") (d #t) (k 0)))) (h "1rk4w5fsskw3mxx3k4jml67cnx34mppm8gb91cwns3br0xd3mnsl")))

