(define-module (crates-io po st postgres_large_object) #:use-module (crates-io))

(define-public crate-postgres_large_object-0.1.0 (c (n "postgres_large_object") (v "0.1.0") (d (list (d (n "postgres") (r "^0.6.4") (d #t) (k 0)))) (h "0lm1vsb740gd7hwl9bzgnh0y2smy14agm7i2x9xmpd12bc2j0k40")))

(define-public crate-postgres_large_object-0.1.1 (c (n "postgres_large_object") (v "0.1.1") (d (list (d (n "postgres") (r "^0.6.5") (d #t) (k 0)))) (h "01r1fzwggxk9m5gar7vh44kaykgs439msmirihx5z5avlmyqnf3b")))

(define-public crate-postgres_large_object-0.2.0 (c (n "postgres_large_object") (v "0.2.0") (d (list (d (n "postgres") (r "^0.7") (d #t) (k 0)))) (h "0w6jyr4zn0yynjflnfrs7pzxs9rrlq07n1xlcsrcvpss4fnaxxwp")))

(define-public crate-postgres_large_object-0.3.0 (c (n "postgres_large_object") (v "0.3.0") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)))) (h "1cgbi32z38wj9agmkh6ni5l5j9wd774bjf6gwsfpsf4jix1cs5v4")))

(define-public crate-postgres_large_object-0.3.1 (c (n "postgres_large_object") (v "0.3.1") (d (list (d (n "postgres") (r "^0.8") (d #t) (k 0)))) (h "0z2xjdf68vzqqhi5412fdjpj80cayvxpc0yh7g4nwcfxlb0v2nj0")))

(define-public crate-postgres_large_object-0.3.2 (c (n "postgres_large_object") (v "0.3.2") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)))) (h "1lz58h342dc080bakdpnb762bz1ppafp2558fn3cv8bw2qdnhyn3")))

(define-public crate-postgres_large_object-0.3.3 (c (n "postgres_large_object") (v "0.3.3") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)))) (h "06fxvnhlc24xhcscvxwm4236fia4rh2fx694qln7mmxcazki2z7z")))

(define-public crate-postgres_large_object-0.3.4 (c (n "postgres_large_object") (v "0.3.4") (d (list (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)))) (h "0q53zdd4cmhg6bslzz5zl0r3mwhv533hidv4dxf6f0qj1g5fjmca")))

(define-public crate-postgres_large_object-0.4.0 (c (n "postgres_large_object") (v "0.4.0") (d (list (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "0qxjk6z9fds6gn05ff67c614aqp1d2zp3dwm9icsl1kpv8vdwp2h")))

(define-public crate-postgres_large_object-0.5.0 (c (n "postgres_large_object") (v "0.5.0") (d (list (d (n "postgres") (r "^0.12") (d #t) (k 0)))) (h "1gnd42kddwgs0id1kiyrpim9qd06w0dnwzfy5frx2sf1jz3vri6q")))

(define-public crate-postgres_large_object-0.5.1 (c (n "postgres_large_object") (v "0.5.1") (d (list (d (n "postgres") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "1a53czpbny3h6dxm0xr5zplnx7h30x82wgzmhp1ih0bn4cdy6z7f")))

(define-public crate-postgres_large_object-0.6.0 (c (n "postgres_large_object") (v "0.6.0") (d (list (d (n "postgres") (r "^0.14") (d #t) (k 0)))) (h "19ak1l9d1h6kqakdnkrh6cx5nymqlcl2mg7c8pnvw3v2272x7lsz")))

(define-public crate-postgres_large_object-0.7.0 (c (n "postgres_large_object") (v "0.7.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "01ag553fnzy9wp1k8684hqchx64a2f02c8a3qcw9pyic6dx9gn21")))

(define-public crate-postgres_large_object-0.7.1 (c (n "postgres_large_object") (v "0.7.1") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "14mwamcf2hj973mc03p9dp1dpfcy588aknr5qx3s0zwfglk71458")))

