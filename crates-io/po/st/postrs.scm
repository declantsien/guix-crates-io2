(define-module (crates-io po st postrs) #:use-module (crates-io))

(define-public crate-postrs-0.1.0 (c (n "postrs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "validator") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w1l2qrx896mlnrk3w02djkwapf2xwcxs3xsswmdikcwqm5k1q5p")))

