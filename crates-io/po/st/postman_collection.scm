(define-module (crates-io po st postman_collection) #:use-module (crates-io))

(define-public crate-postman_collection-0.1.0 (c (n "postman_collection") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "000x7araqkb8pk57r22rfl86jw18zflsk8sijfw4ljfn955cgk8m")))

(define-public crate-postman_collection-0.2.0 (c (n "postman_collection") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1s16fznh2yzlwlbzfbxvww478dhwiw9bah4nyd3alwsldiysybmw")))

