(define-module (crates-io po st postquet) #:use-module (crates-io))

(define-public crate-postquet-0.1.0 (c (n "postquet") (v "0.1.0") (d (list (d (n "arrow") (r "^36.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.19") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parquet") (r "^36.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.2") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)))) (h "01knbqi7d3j0ici0y8vhr3lbby0sw7fd8k8cd4hls4ympjk5xadd")))

