(define-module (crates-io po st postgres-named-parameters-derive) #:use-module (crates-io))

(define-public crate-postgres-named-parameters-derive-0.1.0 (c (n "postgres-named-parameters-derive") (v "0.1.0") (d (list (d (n "attribute-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19glql1dyqzhgrb7fp6sqf8vxzx6p6j83zjr7n9zi7mp16sxxwlc")))

