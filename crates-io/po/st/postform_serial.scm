(define-module (crates-io po st postform_serial) #:use-module (crates-io))

(define-public crate-postform_serial-0.4.0 (c (n "postform_serial") (v "0.4.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jp9696jksra20h0mpcwnp1crnz0iv4clbix6zhsma5bah7g5pb3")))

(define-public crate-postform_serial-0.5.0 (c (n "postform_serial") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.5") (d #t) (k 0)) (d (n "serialport") (r "^4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1givpcq3pph8bjlq4gi9irm9yrv1sim8sg41rpjzzdf8zvbq6qlm")))

