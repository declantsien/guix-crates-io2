(define-module (crates-io po st posterust) #:use-module (crates-io))

(define-public crate-posterust-0.0.1 (c (n "posterust") (v "0.0.1") (d (list (d (n "image") (r "^0.23") (f (quote ("jpeg" "png"))) (k 0)) (d (n "palette") (r "^0.5") (f (quote ("std"))) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "11arxipn8b946n4hx8gqwbpycbhrv2qxksgbjc6br3jgi6mflr9a")))

