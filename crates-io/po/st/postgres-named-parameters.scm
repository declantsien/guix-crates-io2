(define-module (crates-io po st postgres-named-parameters) #:use-module (crates-io))

(define-public crate-postgres-named-parameters-0.1.0 (c (n "postgres-named-parameters") (v "0.1.0") (d (list (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "postgres-from-row") (r "^0.5.2") (d #t) (k 0)) (d (n "postgres-named-parameters-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0pgbhg8kbignrlvs6kn2x53d43514fj9sqv9rn29rzd83wxkw6hp")))

