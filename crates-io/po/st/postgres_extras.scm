(define-module (crates-io po st postgres_extras) #:use-module (crates-io))

(define-public crate-postgres_extras-0.1.0 (c (n "postgres_extras") (v "0.1.0") (d (list (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "postgres_extras_macros") (r "^0.1") (d #t) (k 0)))) (h "1f45pk7nrxpbq7cbibyjad9c30l95agnlfvrd4qr67h6wk1j502r")))

