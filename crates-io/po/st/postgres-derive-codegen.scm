(define-module (crates-io po st postgres-derive-codegen) #:use-module (crates-io))

(define-public crate-postgres-derive-codegen-0.1.0 (c (n "postgres-derive-codegen") (v "0.1.0") (d (list (d (n "syntex") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.29") (o #t) (d #t) (k 0)))) (h "0j4cw1y8bz1zqxf77iidq6c9vqcpnnpwjzv2v9l59s9kcp7anpkp") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("nightly") ("default" "with-syntex"))))))

(define-public crate-postgres-derive-codegen-0.1.1 (c (n "postgres-derive-codegen") (v "0.1.1") (d (list (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "0qnqfq1cbhha08wmrqpj0xj8lkm5kjqv5b7iy0srqfxfwhysr2dz") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("nightly") ("default" "with-syntex"))))))

(define-public crate-postgres-derive-codegen-0.1.2 (c (n "postgres-derive-codegen") (v "0.1.2") (d (list (d (n "syntex") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.31") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.31") (o #t) (d #t) (k 0)))) (h "0i6b2aagdsmqq977d0y9qa1dq3kzr35fz2f0qmf0rdpij41vhxd3") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("nightly") ("default" "with-syntex"))))))

(define-public crate-postgres-derive-codegen-0.1.3 (c (n "postgres-derive-codegen") (v "0.1.3") (d (list (d (n "syntex") (r "^0.36") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.36") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.36") (o #t) (d #t) (k 0)))) (h "0w2srlxdf27likaghcxm2bxjnk5jwggc3ykxa1gan9p4j5q47h33") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("nightly") ("default" "with-syntex"))))))

(define-public crate-postgres-derive-codegen-0.1.4 (c (n "postgres-derive-codegen") (v "0.1.4") (d (list (d (n "syntex") (r "^0.44") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.44") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "^0.44") (o #t) (d #t) (k 0)))) (h "15dg4l9047xgs68rajjgqz7x5f6gfv0d1k8r640phl2zh8gggc0k") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("nightly") ("default" "with-syntex"))))))

(define-public crate-postgres-derive-codegen-0.2.0 (c (n "postgres-derive-codegen") (v "0.2.0") (d (list (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "syntex") (r "^0.44") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44") (d #t) (k 0)))) (h "0lwy24ml33rya6x0wb02iziaxzb2framicjw63bkx222pn9640my")))

(define-public crate-postgres-derive-codegen-0.2.1 (c (n "postgres-derive-codegen") (v "0.2.1") (d (list (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "syntex") (r "^0.48") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48") (d #t) (k 0)))) (h "1qa663zqb2r908mapwdxvd1kgs0490l2bapvqmva180gp7g90kh3")))

(define-public crate-postgres-derive-codegen-0.2.2 (c (n "postgres-derive-codegen") (v "0.2.2") (d (list (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "syntex") (r "^0.50") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.50") (d #t) (k 0)))) (h "09brg4xj11ahzwd179xbsf6r2pa9dgk458x4nz2abwrnf4dvwga3")))

