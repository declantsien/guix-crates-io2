(define-module (crates-io po st postgres-derive-internals) #:use-module (crates-io))

(define-public crate-postgres-derive-internals-0.2.0 (c (n "postgres-derive-internals") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1mkjwhnrn91vr958xkdyq8212gj0d9di510fii8j3kkjxw7vqyf0")))

(define-public crate-postgres-derive-internals-0.2.1 (c (n "postgres-derive-internals") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0357l5z5kfgiqh0d45w0dr5kx2id728pzf6q6gyh826rmx89y16p")))

(define-public crate-postgres-derive-internals-0.2.2 (c (n "postgres-derive-internals") (v "0.2.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0pkabnh6a6cy518kfb8ikg3bmj148ww98zzqisbf2fhy0jn5s8fq")))

