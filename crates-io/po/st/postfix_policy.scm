(define-module (crates-io po st postfix_policy) #:use-module (crates-io))

(define-public crate-postfix_policy-0.1.0 (c (n "postfix_policy") (v "0.1.0") (h "11bdmnm0dhcq5ki6qcjdzzbrkv1mhbsi6289alkjrd1y7v1mcaw5") (y #t)))

(define-public crate-postfix_policy-0.1.1 (c (n "postfix_policy") (v "0.1.1") (h "065c3l88ys4i3055sqh6yjgkwrk3q5siszzca5mnrisayqp3n50b")))

(define-public crate-postfix_policy-0.1.2 (c (n "postfix_policy") (v "0.1.2") (h "1mq3fykzdrjziz52q6c6xq774nv5lrw4klwv8ni5p5s5qd2k46dz")))

(define-public crate-postfix_policy-0.2.0 (c (n "postfix_policy") (v "0.2.0") (h "1w0k99zklayi94mnj927zv2lixy75fxq9270pwpmww1bm0p57ccg")))

