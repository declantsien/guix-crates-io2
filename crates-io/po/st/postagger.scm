(define-module (crates-io po st postagger) #:use-module (crates-io))

(define-public crate-postagger-0.0.1 (c (n "postagger") (v "0.0.1") (d (list (d (n "jni") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0bjjagr2s7nz4vvpd8l59h7aa153zvm5kcd7fb94npwwssc3cd58") (s 2) (e (quote (("java" "dep:jni"))))))

(define-public crate-postagger-0.0.2 (c (n "postagger") (v "0.0.2") (d (list (d (n "jni") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0y12kv9p33bhjjkw0vwv50dw61018436lwhg9ypzinl3g8x1qd0x") (s 2) (e (quote (("java" "dep:jni"))))))

(define-public crate-postagger-0.0.3 (c (n "postagger") (v "0.0.3") (d (list (d (n "jni") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "034ky4kn5gj4yk6hp62kh2n6dhka91ahpspba98bhysncm7dfa0s") (s 2) (e (quote (("java" "dep:jni"))))))

