(define-module (crates-io po st postgres_queue) #:use-module (crates-io))

(define-public crate-postgres_queue-0.1.0 (c (n "postgres_queue") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (f (quote ("with-chrono-0_4" "with-serde_json-1"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "14nxq4kik8rvzf913mr72vfxn4jwm1b1s9i131w1a2wps2mj5gw5")))

