(define-module (crates-io po st postgres-derive) #:use-module (crates-io))

(define-public crate-postgres-derive-0.2.0 (c (n "postgres-derive") (v "0.2.0") (d (list (d (n "postgres") (r "^0.12") (d #t) (k 2)) (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)))) (h "1wsqz31xabfq9ajb09wi2asr4ikl5gmrmgb6b5zx7gkc16bql2ad")))

(define-public crate-postgres-derive-0.2.1 (c (n "postgres-derive") (v "0.2.1") (d (list (d (n "post-expansion") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.12") (d #t) (k 2)) (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0cilqc4iqd8y323688qrwsq3m4i82f9jaxil8qxh4r0c9dlz91fj")))

(define-public crate-postgres-derive-0.2.2 (c (n "postgres-derive") (v "0.2.2") (d (list (d (n "postgres") (r "^0.12") (d #t) (k 2)) (d (n "postgres-derive-internals") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0q1kk2jwfz73jxx8wzi5xkh067b31agxr5k26m4ybqvxashnhzj3")))

(define-public crate-postgres-derive-0.2.3 (c (n "postgres-derive") (v "0.2.3") (d (list (d (n "postgres") (r "^0.13") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0n35gyxhcz9l1s46l51ps9iamqajngbd3a90f8781zh1zsmh0syg")))

(define-public crate-postgres-derive-0.3.0 (c (n "postgres-derive") (v "0.3.0") (d (list (d (n "postgres") (r "^0.14") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18slbxk2w4iy1wlsyms087g3jls30ycpacxsh52r2m6hpzwjpjmb")))

(define-public crate-postgres-derive-0.3.1 (c (n "postgres-derive") (v "0.3.1") (d (list (d (n "postgres") (r "^0.14") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "11zqqpzbz8df0pgfgcaxvwwkh3fj0971gk8kw7s8pzd1j0gg1hq4")))

(define-public crate-postgres-derive-0.3.2 (c (n "postgres-derive") (v "0.3.2") (d (list (d (n "postgres") (r "^0.15.1") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0jjkxyaz20icpqf6gbq51km0x5yhd6f2p71adp4pdg6i9vxk3czm")))

(define-public crate-postgres-derive-0.3.3 (c (n "postgres-derive") (v "0.3.3") (d (list (d (n "postgres") (r "^0.15.1") (d #t) (k 2)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "08f2gwrni85s5gf6vkkp9qgz5jw987jday5a6vg7sm7ia2p45vs4")))

(define-public crate-postgres-derive-0.4.0-alpha.1 (c (n "postgres-derive") (v "0.4.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12m87gsm1l6p6p6ih6mwqgz9xrhkiv58n6adwv0in5z1wl9n2bx5")))

(define-public crate-postgres-derive-0.4.0 (c (n "postgres-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xqlf1gffy3q8hra3fm0vm9x8i5fkvi0qjl99d0xirxh3hidsmy8")))

(define-public crate-postgres-derive-0.4.1 (c (n "postgres-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02dl3xry9dl4fbd4prlhf0ijikbzld59skl51gfhki7j5x2d8sd2")))

(define-public crate-postgres-derive-0.4.2 (c (n "postgres-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "008jz6m6wv28jy5sq44lz8d0s7wkxv8yjfkgbsq488dr827c3hnh")))

(define-public crate-postgres-derive-0.4.3 (c (n "postgres-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1whjb7yfnbw19hw4j4djk1p0b62vg0byasbk16bgd73wx40whxly")))

(define-public crate-postgres-derive-0.4.4 (c (n "postgres-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1kqgdv5snwmyzy9qb5lyc7r9qiw0ah1wwn4kkyqpkisri2kzl3q7")))

(define-public crate-postgres-derive-0.4.5 (c (n "postgres-derive") (v "0.4.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18l065qawh5lm1d350s32mg6d9rzhj6878d9h7whw18vfjx5w543")))

