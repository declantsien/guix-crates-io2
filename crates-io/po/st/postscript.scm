(define-module (crates-io po st postscript) #:use-module (crates-io))

(define-public crate-postscript-0.0.1 (c (n "postscript") (v "0.0.1") (h "16dh7iisk1sk4nm7hjcyqx56pzqgff38avm0w0j8yi8d98qq24y7")))

(define-public crate-postscript-0.1.0 (c (n "postscript") (v "0.1.0") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "0hibzw1l62f0144bly9m2ndmnybhs7vq18x5dc0gc9sivza9sz3i")))

(define-public crate-postscript-0.1.1 (c (n "postscript") (v "0.1.1") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "0jmx9zbmwz2jw73q7d84qvrnrb4zq78aix91ar43r6kmqc17azib")))

(define-public crate-postscript-0.2.0 (c (n "postscript") (v "0.2.0") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "0a0a9h54m39syld718xasjxv70fikj7gqs9kfb5rpvfnbkg8kdxj")))

(define-public crate-postscript-0.2.1 (c (n "postscript") (v "0.2.1") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "1rz87rqydjj2d7cknjy5d17gdqx5b1q25l0harlhac7apqpslhbv")))

(define-public crate-postscript-0.2.2 (c (n "postscript") (v "0.2.2") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "0mh1b7353800bvm14knab00vcb3lp6k6s0prdcmpbj1bkfmrjhbk")))

(define-public crate-postscript-0.2.3 (c (n "postscript") (v "0.2.3") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "1zzmlk888f5lq92v6s0zkcx89c25cmm68acy7hy2b3qxb8488zmd")))

(define-public crate-postscript-0.2.4 (c (n "postscript") (v "0.2.4") (d (list (d (n "random") (r "*") (d #t) (k 2)))) (h "1x4cic178m43hxw6mbzzrq8z8d9p3qvkbwy8rp7shs3a0kpm3j8s")))

(define-public crate-postscript-0.2.5 (c (n "postscript") (v "0.2.5") (d (list (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "0csnmcdcw5wijzg4bfq32nwpaiyhdxlv0xvma9x084mpqkjq801k")))

(define-public crate-postscript-0.3.0 (c (n "postscript") (v "0.3.0") (d (list (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "19zbj1frqz8s515g4xr1ryvlb8yzrilfz140l5szy0qvacd97zkl")))

(define-public crate-postscript-0.4.0 (c (n "postscript") (v "0.4.0") (d (list (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "02hnka98m7by9givb2ijb5cw00z6315dgiw2y22z1xamff24rpwd")))

(define-public crate-postscript-0.5.0 (c (n "postscript") (v "0.5.0") (d (list (d (n "random") (r "^0.10") (d #t) (k 2)))) (h "0n1299akqg5w93aqygghrnlzaq1aww8ra0sbx28g28v7s3n6cqjq")))

(define-public crate-postscript-0.5.1 (c (n "postscript") (v "0.5.1") (d (list (d (n "random") (r "^0.11") (d #t) (k 2)))) (h "0rfpls1wsbnz6f9fiparwr3ika4qhpamick05pzargq7w9wlalli")))

(define-public crate-postscript-0.5.2 (c (n "postscript") (v "0.5.2") (d (list (d (n "random") (r "^0.11") (d #t) (k 2)))) (h "0qc5a6d86wgdipww322iyn71dbm9q60b8rx1z2cxvr9sv7lpj9hc")))

(define-public crate-postscript-0.6.0 (c (n "postscript") (v "0.6.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "03h2bwdzjfv2n8vzz63i22qs28krrsw95fmmqa3m9hgaxw5rrcab")))

(define-public crate-postscript-0.6.1 (c (n "postscript") (v "0.6.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "14szv4qsjmgw3as2l47nq4s5gf2a13m3kwllkmb49d434pl55a6p")))

(define-public crate-postscript-0.7.0 (c (n "postscript") (v "0.7.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "12269w80h9a0mc4w4z3vypnnf7zzy7wq1ah8ghil8ryhf6i3qy7l")))

(define-public crate-postscript-0.7.1 (c (n "postscript") (v "0.7.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1djsd3rmh29j8l8b5gsq0brv455b9ssv71fgsxs3r6kks90p4789")))

(define-public crate-postscript-0.7.2 (c (n "postscript") (v "0.7.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1yndjmwcvs9c9av6xa4zcvpfw2r687csldl7p351948bm164xp9l")))

(define-public crate-postscript-0.7.3 (c (n "postscript") (v "0.7.3") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1rg74225hx3spwk76l3waw3js94b45vhc1xjwcmj0mg0bmrww3ww")))

(define-public crate-postscript-0.8.0 (c (n "postscript") (v "0.8.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0xbw3y3s3l0zmjibj2w60qzph5j4p82fyya35pp0zcxwwr3wlzsl")))

(define-public crate-postscript-0.8.1 (c (n "postscript") (v "0.8.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1hc4cswjnivz2bhl9a1ihgh42isfb5av4dghygk245w9sdip5rvy")))

(define-public crate-postscript-0.8.2 (c (n "postscript") (v "0.8.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0dixakdj7zppxrd7sc20gmmv80734hqxa1nlq5s37hq5vvdz9lcr")))

(define-public crate-postscript-0.9.0 (c (n "postscript") (v "0.9.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0kcgkh90h1lvzba93vsbpihkvri7zhsacsyb3xk14njkq3q8fgzr")))

(define-public crate-postscript-0.10.0 (c (n "postscript") (v "0.10.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1wg80526c4ws751yw7f0r04qgj2xzni1i4pjfc81p7apq1xias0i")))

(define-public crate-postscript-0.10.1 (c (n "postscript") (v "0.10.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "03n74bam1c1jrp9zv07ws0ya4dakvwbfhvhsnbfc0x6an1n34haq")))

(define-public crate-postscript-0.11.0 (c (n "postscript") (v "0.11.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0dp44dqiz4mach8kswisqkyn4dqlqgyjiy7nh4iy0zijbbv5y262")))

(define-public crate-postscript-0.11.1 (c (n "postscript") (v "0.11.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0csjcbyvmfhc7k205ihs51illscz39phdgh4qq027ab5h1zvmjf3")))

(define-public crate-postscript-0.12.0 (c (n "postscript") (v "0.12.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "07jc1sbppaja9sdrclqlj1ihvjkb4n8gpm4sfq1g6wr0x7vrlgzz")))

(define-public crate-postscript-0.12.1 (c (n "postscript") (v "0.12.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1din0ddyxfrml2znpdcacijld5ki5i5r84sm4i2xw8hn1c72mp86")))

(define-public crate-postscript-0.12.2 (c (n "postscript") (v "0.12.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0b16yhci63155v4spsnccwmfafkh5bdlnf6vlsjsk1gy748k4pqi")))

(define-public crate-postscript-0.13.0 (c (n "postscript") (v "0.13.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1bbkmfczczs0aqy4qchzw3v45vr3x8026rgv93k3l3s0r6c2q503")))

(define-public crate-postscript-0.13.1 (c (n "postscript") (v "0.13.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0ghbv4ayhm7pf888jnpnkr8s5gqxw60zyl0hlnjv63zv7h35k56y")))

(define-public crate-postscript-0.13.2 (c (n "postscript") (v "0.13.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "189dn6yk1cslryysl6cr7isjlqc3bn4v7z8va4lizxpy9k32f491")))

(define-public crate-postscript-0.14.0 (c (n "postscript") (v "0.14.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0x95l8g1nkin33c459gwp8mp4psyzwps1l8229w2z7jgbk02a65c")))

(define-public crate-postscript-0.14.1 (c (n "postscript") (v "0.14.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "01pk2b3skak4g1gb9b2f20bbaczv7wmibn9z0mzz3fmfpnninibq") (f (quote (("ignore-missing-operators"))))))

(define-public crate-postscript-0.16.0 (c (n "postscript") (v "0.16.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.1") (d #t) (k 0)))) (h "1f9s1y901dymm69mnlvrxd6fw6rhy98wpdmkccd4gkr6p0wimb24")))

(define-public crate-postscript-0.16.1 (c (n "postscript") (v "0.16.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "0ihvbbqyp9pxlfbg8qpb03vh3rp1gk41banvfi4w39n2qhd99nr1")))

(define-public crate-postscript-0.16.2 (c (n "postscript") (v "0.16.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "0ai9gark24lq2qyrsjr3vxzy71zncjyjn4qbxbwl8jlijhvfw4wh")))

(define-public crate-postscript-0.16.3 (c (n "postscript") (v "0.16.3") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "0r6y196mhnqff3bai8vjsp91l32jgv6bcadhcibfmribv2qpb7f6")))

(define-public crate-postscript-0.16.4 (c (n "postscript") (v "0.16.4") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2") (d #t) (k 0)))) (h "06andn3ibi2m9rin2ngv145f4bj2rx0k9k1afbss927darfjanyy")))

(define-public crate-postscript-0.16.5 (c (n "postscript") (v "0.16.5") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "0jxf2rv5i95fjgvhhf7m9h4laznc6szqnqannz1d131przsbgbib")))

(define-public crate-postscript-0.16.6 (c (n "postscript") (v "0.16.6") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "1dii0wm3lz5psb48z6zhdxxyxr9wlglqbdfwnsrpfzzbz20gm6c7")))

(define-public crate-postscript-0.17.0 (c (n "postscript") (v "0.17.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "139f0fcmb688i068a6318hqcsigz9rz3b9bkq355c4v661h9rcas")))

(define-public crate-postscript-0.17.1 (c (n "postscript") (v "0.17.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "11nfdcs6jbhr828in69p0h3s5c6b65a4c4j7kb7iwxwgd8yidjhi")))

(define-public crate-postscript-0.17.2 (c (n "postscript") (v "0.17.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.3") (d #t) (k 0)))) (h "0djk5baywigjl927wk4slfk6zxapqgkjk15dg9izyj6rraf04fhz")))

(define-public crate-postscript-0.17.3 (c (n "postscript") (v "0.17.3") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.2.6") (d #t) (k 0)))) (h "1y6bcwzglbdzz87qjc8xl5zk5y1i61dv45yd64k32sgjhmbjxsv7")))

(define-public crate-postscript-0.17.4 (c (n "postscript") (v "0.17.4") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1f4pdp9i8gnvmf02yxg9r8rnyr8wndaa63a52qh714bxxc9idpxz")))

(define-public crate-postscript-0.17.5 (c (n "postscript") (v "0.17.5") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.3.0") (d #t) (k 0)))) (h "1wmyja3grcb35p1vrcywyvs050gv6hgclqhm8aid9sbhbapna3db")))

(define-public crate-postscript-0.18.0 (c (n "postscript") (v "0.18.0") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.4.0") (d #t) (k 0)))) (h "08gcvsnjnflkdr6cpcv2hasxkkpd7lr2la7f7k9zl9r3jplnqvxn")))

(define-public crate-postscript-0.18.1 (c (n "postscript") (v "0.18.1") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.4.0") (d #t) (k 0)))) (h "13yx52j2adm7cah15v7f6ihn3nc1vgq8vgj8ssycxznwjl7r9pbq")))

(define-public crate-postscript-0.18.2 (c (n "postscript") (v "0.18.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "typeface") (r "^0.4.2") (d #t) (k 0)))) (h "1rch22bbi6523v2ffxhdvddlbvq42h97y4fbl2lcw3hfd70qbir7")))

