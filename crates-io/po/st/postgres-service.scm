(define-module (crates-io po st postgres-service) #:use-module (crates-io))

(define-public crate-postgres-service-0.1.0 (c (n "postgres-service") (v "0.1.0") (d (list (d (n "postgres-shared") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "1h7sc55jxq0hcyydf1bkwxbqv1as9nnndyghyl9yrxggkq8882cz") (y #t)))

(define-public crate-postgres-service-0.1.1 (c (n "postgres-service") (v "0.1.1") (d (list (d (n "postgres-shared") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "0gljj2zbks1bsvmdjm89jyscwc4ypdw8mh9iba06qi5xvqwgqs38")))

(define-public crate-postgres-service-0.1.2 (c (n "postgres-service") (v "0.1.2") (d (list (d (n "postgres") (r "^0.14.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "1vbb03zka7vzrrbbbcv9hahq8zd2lc3bwxk83qg10wyn9wcwix4g")))

(define-public crate-postgres-service-0.15.0 (c (n "postgres-service") (v "0.15.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "039fpm7i8v6159a13qj55ldw3qal6dmip09nd1ynwvqv33bmg6qg")))

(define-public crate-postgres-service-0.15.1 (c (n "postgres-service") (v "0.15.1") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13") (d #t) (k 0)))) (h "1mkw0cljcd8fqwrx6nvnn1xhikpvca38la8mxh9cb0avkf28wmra")))

(define-public crate-postgres-service-0.17.0 (c (n "postgres-service") (v "0.17.0") (d (list (d (n "postgres") (r "^0.17") (d #t) (k 0)) (d (n "rust-ini") (r "^0.14") (d #t) (k 0)))) (h "0gci4s34w0v6c3ll4gw2mrd1ccxkc11abq01drahcggskg9k1dy8")))

(define-public crate-postgres-service-0.18.0 (c (n "postgres-service") (v "0.18.0") (d (list (d (n "postgres") (r "^0.18") (d #t) (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)))) (h "08xw80j49ympdfrb0ckakyrjmrl9441dqzw4pyz2rmyf2ngav41n")))

(define-public crate-postgres-service-0.19.0 (c (n "postgres-service") (v "0.19.0") (d (list (d (n "postgres") (r "^0.19") (k 0)) (d (n "rust-ini") (r "^0.16") (d #t) (k 0)))) (h "1n1jwjf8y5mzsmr2aw3a463lglpp8pz68i5ill0q71ziqk0d2nj6")))

(define-public crate-postgres-service-0.19.1 (c (n "postgres-service") (v "0.19.1") (d (list (d (n "postgres") (r "^0.19") (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)))) (h "0d7r8nc133xkxc64cl66rdgaji1w3crwrg591jmlq0ifwcjp2bbd")))

(define-public crate-postgres-service-0.19.2 (c (n "postgres-service") (v "0.19.2") (d (list (d (n "postgres") (r "^0.19") (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "1z54x0l0096q5qc64sclvfi7637gkpv6b2wspvlbzfzx3hwavq9j")))

(define-public crate-postgres-service-0.19.3 (c (n "postgres-service") (v "0.19.3") (d (list (d (n "postgres") (r "^0.19") (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "1glah5086nw2bza02vjlnvs4bvhb92nrf3j0aybwigdyvjny68bd")))

(define-public crate-postgres-service-0.19.4 (c (n "postgres-service") (v "0.19.4") (d (list (d (n "postgres") (r "^0.19") (k 0)) (d (n "rust-ini") (r "^0.20") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (k 0)))) (h "006zb3p14hpfpaicpbnxxpqf73lfsv0r44dfas8v68ajs7pbp4mr")))

