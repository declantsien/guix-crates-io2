(define-module (crates-io po st postsse) #:use-module (crates-io))

(define-public crate-postsse-0.1.0 (c (n "postsse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "http2" "server" "runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "time" "rt"))) (d #t) (k 0)))) (h "1l1ygazahmwijkxi4mnv3ys0szxh3cvhd8wp09agbq056mrgb2li")))

