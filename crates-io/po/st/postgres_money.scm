(define-module (crates-io po st postgres_money) #:use-module (crates-io))

(define-public crate-postgres_money-0.1.0 (c (n "postgres_money") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "1lsxh29idxlf3vc5pyw86dnya1nd3dg1zll9bm0kgg9c34709lb5")))

(define-public crate-postgres_money-0.1.1 (c (n "postgres_money") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "0a0kv7iqw5mgh6j8g2ih5y0mmrpz8yi6hd85ihhs58ica1a691lq")))

(define-public crate-postgres_money-0.2.0 (c (n "postgres_money") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("serde_derive"))) (o #t) (k 0)))) (h "143jj7bb4mn5g0qiwkdq74qgv77bx3qy0dpj935r1lg0ngnl7rkx")))

(define-public crate-postgres_money-0.3.0 (c (n "postgres_money") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "postgres-types") (r "^0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "15r8mjviqhnkf5080ch7715m03z96rfaw8gqxrlpfy64a9ya3xsf") (f (quote (("sql" "postgres-types" "byteorder" "bytes"))))))

