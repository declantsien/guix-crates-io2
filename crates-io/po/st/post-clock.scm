(define-module (crates-io po st post-clock) #:use-module (crates-io))

(define-public crate-post-clock-0.1.1 (c (n "post-clock") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "capctl") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "18w49fzwc7b431npn517nra82x263s63j53iq3qrlgdrwnsdfz2p")))

