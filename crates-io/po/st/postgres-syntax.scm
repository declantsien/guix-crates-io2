(define-module (crates-io po st postgres-syntax) #:use-module (crates-io))

(define-public crate-postgres-syntax-0.1.0 (c (n "postgres-syntax") (v "0.1.0") (d (list (d (n "libpg_query2-sys") (r "^0.2") (k 0)) (d (n "litrs") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)))) (h "0a35ys84gjjwk9g2skfhsdcsv868xi26qf59hqbmg53mhr8asw25")))

(define-public crate-postgres-syntax-0.1.1 (c (n "postgres-syntax") (v "0.1.1") (d (list (d (n "libpg_query2-sys") (r "^0.2") (k 0)) (d (n "litrs") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)))) (h "085ikkhil07dgrg2232sk7dd3jwx2kmv4z1xgpb4imjw03v6jaq4")))

(define-public crate-postgres-syntax-0.1.2 (c (n "postgres-syntax") (v "0.1.2") (d (list (d (n "libpg_query2-sys") (r "^0.2") (k 0)) (d (n "litrs") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)))) (h "08dsdrb948qq0s74la9p24c3zkwd3s0jjq57rb3iszslzhvqnlzy")))

(define-public crate-postgres-syntax-0.1.3 (c (n "postgres-syntax") (v "0.1.3") (d (list (d (n "libpg_query-sys") (r "^0.2") (k 0)) (d (n "litrs") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)))) (h "0bygijq277bgmy6xydcmha1qzp5ih5zry8wmq4g1q91p0dyac8kn") (y #t)))

(define-public crate-postgres-syntax-0.2.0 (c (n "postgres-syntax") (v "0.2.0") (d (list (d (n "libpg_query2-sys") (r "^0.3") (k 0)) (d (n "litrs") (r "^0.4") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)))) (h "0fwxy8f1maiv63wsa8ixjdp050075czkfflpws510i84wz4gkp6z")))

(define-public crate-postgres-syntax-0.3.0 (c (n "postgres-syntax") (v "0.3.0") (d (list (d (n "litrs") (r "^0.4") (k 0)) (d (n "pg_query") (r "^5.1") (k 0)) (d (n "quote") (r "^1") (f (quote ("proc-macro"))) (k 0)))) (h "1ynr1sl8a4mz9lz6wizdardin6mss54ci924fprx1wi11879f6sl")))

