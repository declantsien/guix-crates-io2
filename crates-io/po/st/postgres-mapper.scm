(define-module (crates-io po st postgres-mapper) #:use-module (crates-io))

(define-public crate-postgres-mapper-0.1.0 (c (n "postgres-mapper") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0zp4yqyvim6jkpb25193qbajr5262w3g2xl8sjrndnw7h0xiszdq") (f (quote (("tokio-postgres-support" "tokio-postgres") ("postgres-support" "postgres") ("default" "postgres-support"))))))

(define-public crate-postgres-mapper-0.1.1 (c (n "postgres-mapper") (v "0.1.1") (d (list (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1hb6x4wsb00pzgdj17sdlz9rx5xhi7pq49ab0icabvspdpsgpf32") (f (quote (("tokio-postgres-support" "tokio-postgres") ("postgres-support" "postgres") ("default" "postgres-support"))))))

