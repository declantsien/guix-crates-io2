(define-module (crates-io po st postgres_macros) #:use-module (crates-io))

(define-public crate-postgres_macros-0.0.1 (c (n "postgres_macros") (v "0.0.1") (h "0skm3c4fm5172xx1lg2aj8gvjwjcspalx57bjb3ga0mmg18y8c9s")))

(define-public crate-postgres_macros-0.0.2 (c (n "postgres_macros") (v "0.0.2") (h "1hrhpc5jjb95b2x2p5hjpvsdfb34k0mzqq8lnvwva632r1dqiqnw")))

(define-public crate-postgres_macros-0.0.3 (c (n "postgres_macros") (v "0.0.3") (h "1cjf5v2lxy0ccdbbajs0iv8pvs63fxs4v4ciwn11dj3kcck14ffi")))

(define-public crate-postgres_macros-0.0.4 (c (n "postgres_macros") (v "0.0.4") (h "1parji629g75asvbamr0gm0k7vgbrafjmhind7rbv7x1376zb4ad")))

(define-public crate-postgres_macros-0.0.5 (c (n "postgres_macros") (v "0.0.5") (h "11qs06k9iiz1nnbkq10m9c665i753x7hamjsvsay3s78k3wdnsrq")))

(define-public crate-postgres_macros-0.0.6 (c (n "postgres_macros") (v "0.0.6") (h "1kdv9lv6r2nayqkvwva8cr87qw9cd1p2k1nqjd99r3k5lb6xvx4w")))

(define-public crate-postgres_macros-0.0.7 (c (n "postgres_macros") (v "0.0.7") (h "1zj8sjrhrdgyfcb4mxqpdmgmhayy3kci6cqnxkj0l59y2lmxlnhw")))

(define-public crate-postgres_macros-0.1.0 (c (n "postgres_macros") (v "0.1.0") (h "1606nnllyf4xzj0skiig2gkc8v35azrszw1427gy3l5gaymvjq1v")))

(define-public crate-postgres_macros-0.1.1 (c (n "postgres_macros") (v "0.1.1") (h "1p9k36vqkhwc7jc55ccwh3kcc5vl3q1f3l4xyb3zpn5zl97p1yv1")))

(define-public crate-postgres_macros-0.1.2 (c (n "postgres_macros") (v "0.1.2") (h "0yva3sfcpyvxzf2d39pxssnrz7kcv6dkbfhyspj90vsgn64s2j1g")))

(define-public crate-postgres_macros-0.1.3 (c (n "postgres_macros") (v "0.1.3") (h "1z9v2fmcn8ihh8ads2zq5lgmk6yqr6qpprb5rhzz5hnmd4m7a9mx")))

(define-public crate-postgres_macros-0.1.4 (c (n "postgres_macros") (v "0.1.4") (h "1hhj2xsjpsz40brw3ngqfawld5y5dj6v260pp74saybp19mvvz85")))

(define-public crate-postgres_macros-0.1.5 (c (n "postgres_macros") (v "0.1.5") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "0rn7463yjkb0314ndd2pmd8fnyx9b148js82zz96jax3wdin59hs")))

(define-public crate-postgres_macros-0.1.6 (c (n "postgres_macros") (v "0.1.6") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "084fa63q490g1fg90l947crfigm65gg7xl1n17qkw892bpif2xzb")))

(define-public crate-postgres_macros-0.1.7 (c (n "postgres_macros") (v "0.1.7") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "046h6k7i84zl66h32j1clj7dnngk53zzf1x4c2d5amxfwpby6f02")))

(define-public crate-postgres_macros-0.1.8 (c (n "postgres_macros") (v "0.1.8") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "1m2zm7alzd121vq5zd9jrxsvdl64kxa5z1wymfkl22kzd9a0nfk9")))

(define-public crate-postgres_macros-0.1.9 (c (n "postgres_macros") (v "0.1.9") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "103f028la8lz91yln6gp8c6qdb8ifacrk7lkj6rrbdbmp7wymv4f")))

(define-public crate-postgres_macros-0.1.10 (c (n "postgres_macros") (v "0.1.10") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "06gyp44njrmkj816nzxg5x1nynnfmvcsyaifwzkn7hn9p4710h6q")))

(define-public crate-postgres_macros-0.1.11 (c (n "postgres_macros") (v "0.1.11") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)))) (h "02vgibv59z1zr2an2ak8z9h3aw6ivh5jlbwwcwin3mf1rny2apzh")))

(define-public crate-postgres_macros-0.1.12 (c (n "postgres_macros") (v "0.1.12") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)))) (h "0yjq4nas5i4b9hwz813a42w0lhhm7pkr38709vjan0w6kyb355l4")))

(define-public crate-postgres_macros-0.1.13 (c (n "postgres_macros") (v "0.1.13") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)))) (h "0yawfsb9f1330wb7xz0wmfzpm12v5dkqs7y1psz57lksvjbhkw68")))

