(define-module (crates-io po st postmark-client) #:use-module (crates-io))

(define-public crate-postmark-client-0.1.0 (c (n "postmark-client") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kzibbmf5pxa374p5bbr8iiajxsbbc8k3ql2mf195yqrcl7x687b")))

(define-public crate-postmark-client-0.2.0 (c (n "postmark-client") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bqbadzxhz5d22c5gns7xqrmjghv8inwglqganbx40dj9xplg4mp")))

(define-public crate-postmark-client-0.2.1 (c (n "postmark-client") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "081r0fapkxj92fcvc45gqgkmhz58hci3fa0i4arjmhhaz32hzrci")))

(define-public crate-postmark-client-0.3.0 (c (n "postmark-client") (v "0.3.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q9hqpyq3wsncxvlyg3i3bv8cljzca4slnwd9c9gn4vgi9c39dp0")))

