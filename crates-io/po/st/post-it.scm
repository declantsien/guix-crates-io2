(define-module (crates-io po st post-it) #:use-module (crates-io))

(define-public crate-post-it-1.0.0 (c (n "post-it") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "1kr9r3gx86k6yjvz4xbmwkmh8q1q56zwzd2zpn5xa7n48shngqs1")))

(define-public crate-post-it-1.1.0 (c (n "post-it") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "123wmkb3vj8n0gcnp8l90k8fgx2bm33b7ncs2826hdg2y1h20c3r")))

(define-public crate-post-it-2.0.0 (c (n "post-it") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "1vz7bhlfbix3327z5s6pdq3ncg5vlc1zp20bmfjd1l3rn6579q5c")))

(define-public crate-post-it-3.0.0 (c (n "post-it") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "1flhy212p74k8prw4m3wv6vwh5qizbwl6hd03z6xp8psxxgp6mhb")))

(define-public crate-post-it-3.1.0 (c (n "post-it") (v "3.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "config") (r "^0.11.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "1c24qqj186ba75gs26p2h6gv3mg4x1cfwvmgbwba9cg12g375gks")))

