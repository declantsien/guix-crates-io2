(define-module (crates-io po st postmill) #:use-module (crates-io))

(define-public crate-postmill-0.1.0 (c (n "postmill") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "01cyfx303avnrlg1hm5npxq5imz3dikah8gzmb54x128y1pqznav")))

(define-public crate-postmill-0.1.1 (c (n "postmill") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "07lq5q8bvfygavlwsp839j9d28lxhpnkabkfw6w13as64sjvix27")))

(define-public crate-postmill-0.1.2 (c (n "postmill") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "11n4g3q10jwvbv7zmpwms84rzymiqsmry5839n9zkar8dy7br19l")))

(define-public crate-postmill-0.1.3 (c (n "postmill") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1vnh0hx0x9qhs70lncay4pylrk0qbghslp4xlhk3k4z030bpr878")))

(define-public crate-postmill-0.1.4 (c (n "postmill") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "06m23cg9yjnhhbhpcrz47v39j742b0vgif57y7qyvajgxigb0kp2")))

(define-public crate-postmill-0.1.5 (c (n "postmill") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0di7g1m47s3pflr7zxs392jw8bwbah2ndqvqbmgm5g3zrkk5k04j")))

(define-public crate-postmill-0.1.6 (c (n "postmill") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0ppk4vsr1cxhpc95ixcryf5swik6fpw5dlx5rpa3gd1c9y39da88")))

(define-public crate-postmill-0.2.0 (c (n "postmill") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0rjb26vm8sd99y9xqjc98qij8d8bvqhksi64anirdji1m65hydjk")))

(define-public crate-postmill-0.2.1 (c (n "postmill") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "007qjcnhg69ayjk74xhw40dfhk08zhg6bl1wjmqismz04b0dn4kq")))

(define-public crate-postmill-0.2.2 (c (n "postmill") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1wyamxq4cjfgv4l86xs2bc0aqcg3q3q57axipp4c5ildjn1ikq5q")))

(define-public crate-postmill-0.2.3 (c (n "postmill") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0rwgyxy7smsgkg9xaznl6zmczc7vnag7z2v5q363mj7gbl3h75v4")))

(define-public crate-postmill-0.2.4 (c (n "postmill") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0drp6x4r24p0haxlk87rv0piwbzaw88y12q36466wy8k909vdh11")))

(define-public crate-postmill-0.2.5 (c (n "postmill") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "08w8xdgwg7dzid4jkzfsr50l43r0560xrp2a43klsghyc037m4c2")))

(define-public crate-postmill-0.2.6 (c (n "postmill") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1wssqh8ckf4xlfifyfs0gmczn1lsprj6pk1lxlv1jyln310ga2vv")))

(define-public crate-postmill-0.2.7 (c (n "postmill") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0n618fnydzlvpdcwm429h7nprih7v0znss5g2qhcl141l4qbqjp0")))

