(define-module (crates-io po st postguard) #:use-module (crates-io))

(define-public crate-postguard-0.1.0 (c (n "postguard") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libpgquery-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-test") (r "^0.1.0") (d #t) (k 2)))) (h "0p828xdxga7f4iwc0fl0ixl20lvrv4khkhfv72nidhgpiz1i27qg")))

