(define-module (crates-io po st postfix) #:use-module (crates-io))

(define-public crate-postfix-0.0.0 (c (n "postfix") (v "0.0.0") (h "170w9jiz080qwnskrwkkrp0mwdavb0m4lqsnpll2g5jnmcq1gl63")))

(define-public crate-postfix-0.1.0 (c (n "postfix") (v "0.1.0") (h "076smjdlhcdfyhsx5k54vxpi7amp3mgjapqxqznlgc1r0lagqp7k")))

(define-public crate-postfix-0.1.1 (c (n "postfix") (v "0.1.1") (h "12wfyigia8ymz9m0kqms061qjqig5yy7a6p0b4q41cz6yxcwp34c")))

(define-public crate-postfix-0.1.2 (c (n "postfix") (v "0.1.2") (h "0jk81h955wdbv6qhqlrz8rciinfxxxd3grjxh0vg4q25nd96l3la")))

