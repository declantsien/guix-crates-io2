(define-module (crates-io po st post-expansion) #:use-module (crates-io))

(define-public crate-post-expansion-0.0.1 (c (n "post-expansion") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "01hf3q1mpca7xlphwzp4kpzhgqwzihax8kr5wx8kjf3blh9y637v") (y #t)))

(define-public crate-post-expansion-0.0.2 (c (n "post-expansion") (v "0.0.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "0yx1fa0qasv0pnvpzaya7z3n9m44qkin230n5dsa0jvjv9d266vr") (y #t)))

(define-public crate-post-expansion-0.1.0 (c (n "post-expansion") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1y964pj8g9cvd1qgxfs2w3pnzf526z5gcy6qm1sfzjha0sk39a1i") (y #t)))

(define-public crate-post-expansion-0.2.0 (c (n "post-expansion") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "00rwh5ah79dbhp6rg40c0hg3w276fr8xvkb1byqknk27l1g8lspg") (y #t)))

