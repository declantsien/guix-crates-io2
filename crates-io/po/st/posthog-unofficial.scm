(define-module (crates-io po st posthog-unofficial) #:use-module (crates-io))

(define-public crate-posthog-unofficial-0.2.3 (c (n "posthog-unofficial") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "00aqp86yjwhbx953gw9k35p4pirkaqd1j2mpdrii03h2ymg32v3a")))

