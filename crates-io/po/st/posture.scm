(define-module (crates-io po st posture) #:use-module (crates-io))

(define-public crate-posture-0.1.0 (c (n "posture") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (f (quote ("serde-serialize-no-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h7va23mgbzc4ral7mfaij4yzb2jbkra1qk8w902knsn1v8dj7ry")))

