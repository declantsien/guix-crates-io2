(define-module (crates-io po st postgres-sys) #:use-module (crates-io))

(define-public crate-postgres-sys-0.1.0 (c (n "postgres-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "postgres-util") (r "^0.1") (d #t) (k 1)))) (h "0rf9rxp354f4kidgvsm2ix742lnrvy6zysg5fm8jri3z46xwjsfi")))

