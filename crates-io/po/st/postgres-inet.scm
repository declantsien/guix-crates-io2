(define-module (crates-io po st postgres-inet) #:use-module (crates-io))

(define-public crate-postgres-inet-0.1.0 (c (n "postgres-inet") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.14.0") (d #t) (k 0)))) (h "06mfxjxcc0d5n5nrfws692yqshv77zb12j66iv3yzn3jdppjhxqd")))

(define-public crate-postgres-inet-0.1.1 (c (n "postgres-inet") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.14.0") (d #t) (k 0)))) (h "1hpsir852vwfib00gzhzc43zk7s1j4wx3ibvzlwqhbk755w5yfp4")))

(define-public crate-postgres-inet-0.1.2 (c (n "postgres-inet") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)))) (h "14lh1f8y5ni80kbgf67p6lnvv5b3zlpdhg66xnsa75n15d6i24dz")))

(define-public crate-postgres-inet-0.1.3 (c (n "postgres-inet") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "07xmvp5hlxvyj1mdrw76qxi7nplq69v0g71qz5jac637yjjrhrdg")))

(define-public crate-postgres-inet-0.1.4 (c (n "postgres-inet") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "019m4f0cb21j70ln0dv84j5a6r684k35zjx01mm04fnmqyich0av")))

(define-public crate-postgres-inet-0.15.0 (c (n "postgres-inet") (v "0.15.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "ipnetwork") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0sh1n1j9p3nwp4dqh6hdpfhbb7pznvv95hyr2hrigjb1fh5ns492")))

(define-public crate-postgres-inet-0.15.1 (c (n "postgres-inet") (v "0.15.1") (d (list (d (n "ipnetwork") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0ag2gs1bmi916267qvhkkp1qqsw9v07jly6rfpzzp4x5p0ngbw1v")))

(define-public crate-postgres-inet-0.15.2 (c (n "postgres-inet") (v "0.15.2") (d (list (d (n "ipnetwork") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0xacaxyx9c5jmcw8vqv8f783mc7kppi9n4wx4zmhpf7d0j94w7rz")))

(define-public crate-postgres-inet-0.15.3 (c (n "postgres-inet") (v "0.15.3") (d (list (d (n "ipnetwork") (r ">= 0.7, < 0.15") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "1gci8ksazwf6bzpkpvjdzqnm58arb6y4a2sg36bcqr87fj24w88v")))

(define-public crate-postgres-inet-0.15.4 (c (n "postgres-inet") (v "0.15.4") (d (list (d (n "ipnetwork") (r ">= 0.7, < 0.15") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 2)) (d (n "postgres-shared") (r "^0.4") (d #t) (k 0)))) (h "1n19m0hj66v4id6ppqlyr87qmx3rk8zj2jb19dkllj9hazyw1mjk")))

(define-public crate-postgres-inet-0.17.0 (c (n "postgres-inet") (v "0.17.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ipnetwork") (r ">= 0.7, < 0.16") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "1x8m8yqdvz1z5575pp7k6wpqxbra0r3yax4411d3pxdy96ww33ns")))

(define-public crate-postgres-inet-0.19.0 (c (n "postgres-inet") (v "0.19.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "ipnetwork") (r ">=0.7, <0.19") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.19.2") (d #t) (k 2)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0sbsasjkkzyc4mbj24b1v07c7axk8x982415v0m9czd0niir3dav")))

