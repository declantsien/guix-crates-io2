(define-module (crates-io po st postgres-querybuilder) #:use-module (crates-io))

(define-public crate-postgres-querybuilder-0.1.0 (c (n "postgres-querybuilder") (v "0.1.0") (d (list (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "0zgb78pzgnlid6dvkmwdriq836p8giyaqpahy7riv78ccnj03i26")))

(define-public crate-postgres-querybuilder-0.1.1 (c (n "postgres-querybuilder") (v "0.1.1") (d (list (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "1h8wx2lin1q6i135m6q1p37pl3d5q6rs1njc2jmda0w65gqm8r3i")))

(define-public crate-postgres-querybuilder-0.1.2 (c (n "postgres-querybuilder") (v "0.1.2") (d (list (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4") (d #t) (k 2)))) (h "1v5saxqclzj0c9zvv7qm01f99p3ky33ilnvb4lgkspygrf20rh1m")))

(define-public crate-postgres-querybuilder-0.2.0 (c (n "postgres-querybuilder") (v "0.2.0") (d (list (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4") (d #t) (k 2)))) (h "178vjfdd3hk8ywzifrvwv9bn52wf5c6ic82v9wvfb8crpynn8pm6")))

(define-public crate-postgres-querybuilder-0.3.0 (c (n "postgres-querybuilder") (v "0.3.0") (d (list (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.4") (d #t) (k 2)))) (h "0jy2cgwbpmh9zv0cgv53vymqizmld1sxfkdfdbv5mj0650n1d9zl")))

