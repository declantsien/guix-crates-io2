(define-module (crates-io po st postfix_assert) #:use-module (crates-io))

(define-public crate-postfix_assert-0.1.0 (c (n "postfix_assert") (v "0.1.0") (h "1sy6niypx05p5p8xh68llhkgra7cgscr7ch8bna2cd1vpxdgm1j3") (f (quote (("debug"))))))

(define-public crate-postfix_assert-0.1.1 (c (n "postfix_assert") (v "0.1.1") (h "0m10b5hsb4njm1yi9kjh3av3s0v5akxbqgyd43qnwd0zndcz9kpn") (f (quote (("debug"))))))

