(define-module (crates-io po st postgres_range) #:use-module (crates-io))

(define-public crate-postgres_range-0.1.0 (c (n "postgres_range") (v "0.1.0") (d (list (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "151q6xwj6kv00c10l5aal6gjx7q1161cv6jm50q3hmqk3v54jh3h")))

(define-public crate-postgres_range-0.1.1 (c (n "postgres_range") (v "0.1.1") (d (list (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1nmqcjr0h3vnhd9f9qs4l3f4pjfz0492b8hsmbfbb91080na3wqm")))

(define-public crate-postgres_range-0.2.0 (c (n "postgres_range") (v "0.2.0") (d (list (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zrldjgknyjav8qbr6p5r2r03mhjqzvlnshbz70hkmjnsfjrswda")))

(define-public crate-postgres_range-0.3.0 (c (n "postgres_range") (v "0.3.0") (d (list (d (n "postgres") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ycvc18zaqihs1z0jmj03b9rc6vahv988k34mjz3yjc1x4r5xd3b")))

(define-public crate-postgres_range-0.3.1 (c (n "postgres_range") (v "0.3.1") (d (list (d (n "postgres") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "031cv61qx861n07qgs2bljnx34w7h7p07gaaq89sz722zwilwncd")))

(define-public crate-postgres_range-0.4.0 (c (n "postgres_range") (v "0.4.0") (d (list (d (n "postgres") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bklrvks7djmbqjarijgr87wbkly2r6w0vcx4nrcr2h3bsyzqwpy")))

(define-public crate-postgres_range-0.5.0 (c (n "postgres_range") (v "0.5.0") (d (list (d (n "postgres") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ahg2irmf9cm6r071l2vz9bcx2dvhws2w5k5y5sdfla3smsbpasl")))

(define-public crate-postgres_range-0.6.0 (c (n "postgres_range") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.7.1") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1kwa5a43w0nn214vidrybki4f8hwws1ma3kzkjiirhg5gl6jx2mn")))

(define-public crate-postgres_range-0.7.0 (c (n "postgres_range") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "18xjn6z4jddm2sj62sg929dvq7289cxmdix7mrs2znhj2gj20x7q")))

(define-public crate-postgres_range-0.7.1 (c (n "postgres_range") (v "0.7.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0pfa5j5dkf46ia0fwf17d0lvaq6i3l54xj4w02rlj065784llqah")))

(define-public crate-postgres_range-0.7.2 (c (n "postgres_range") (v "0.7.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17y9riir92zzmr7p211aw8i8hv31ja86zycpw8my5x82sllsa4xr")))

(define-public crate-postgres_range-0.7.3 (c (n "postgres_range") (v "0.7.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ym5vnvr7srdl54r1dg43fps2c465nv17j3r0iak4q27a55hbgwn")))

(define-public crate-postgres_range-0.7.4 (c (n "postgres_range") (v "0.7.4") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1phsn08wnpn4x5jh539jrdm8nnx0hn7dkrawkijfs4b0fw18hx12")))

(define-public crate-postgres_range-0.8.0 (c (n "postgres_range") (v "0.8.0") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "10d9zkpfmd4dxnpa1gcnb2i2rx0a5mla15nfw298dnjimd2wa33z")))

(define-public crate-postgres_range-0.8.1 (c (n "postgres_range") (v "0.8.1") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09n7pb18x94xz7dgancjk8pad3y9pfsrg2i059g6rk0ihf91kmcr")))

(define-public crate-postgres_range-0.8.2 (c (n "postgres_range") (v "0.8.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (f (quote ("time"))) (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ddsgxjyfjgpfim2g8196x88q0wqsjj6aqa3w8f3hfdggx3kd1mj")))

(define-public crate-postgres_range-0.9.0 (c (n "postgres_range") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-time"))) (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.3") (d #t) (k 0)) (d (n "postgres-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fkx5zq4yznnbh6k7llgs3c3kqr46jx8c5g73f5ldpphg4yrvbyz") (f (quote (("with-time" "time" "postgres-shared/with-time") ("with-chrono" "chrono" "postgres-shared/with-chrono"))))))

(define-public crate-postgres_range-0.9.1 (c (n "postgres_range") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-time"))) (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.3") (d #t) (k 0)) (d (n "postgres-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1gzsa80ljhpm6akxy5m02y2p9gcm4k6193qan9qd5wp77ngqibp5") (f (quote (("with-time" "time" "postgres-shared/with-time") ("with-chrono" "chrono" "postgres-shared/with-chrono"))))))

(define-public crate-postgres_range-0.10.0 (c (n "postgres_range") (v "0.10.0") (d (list (d (n "chrono-04") (r "^0.4") (o #t) (d #t) (k 0) (p "chrono")) (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.5") (d #t) (k 0)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "16hkmnw6z30d4fj3s87379z1v4fzx1vrsp1l5ffmssbg1y85bh8q") (f (quote (("with-chrono-0_4" "chrono-04" "postgres-types/with-chrono-0_4"))))))

(define-public crate-postgres_range-0.11.0 (c (n "postgres_range") (v "0.11.0") (d (list (d (n "chrono-04") (r "^0.4") (o #t) (d #t) (k 0) (p "chrono")) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.6") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0mfjkwhdan45rz7p1zardf5bxhn18swi9ybwi0dg74zvk0ficj5l") (f (quote (("with-chrono-0_4" "chrono-04" "postgres-types/with-chrono-0_4"))))))

(define-public crate-postgres_range-0.11.1 (c (n "postgres_range") (v "0.11.1") (d (list (d (n "chrono-04") (r "^0.4") (o #t) (k 0) (p "chrono")) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.6") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0091cj3nms10nxc22ya0bic1qnmf06n2mdjpn673s55sqn6y5p7n") (f (quote (("with-chrono-0_4" "chrono-04" "postgres-types/with-chrono-0_4"))))))

