(define-module (crates-io po st posthog-rs) #:use-module (crates-io))

(define-public crate-posthog-rs-0.1.0 (c (n "posthog-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "03kjivaq897335sl7w5nrhk3x3mrq2hs53phhbbivsd2sc5bc7jf")))

(define-public crate-posthog-rs-0.1.1 (c (n "posthog-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0p0w6nv3z59x631ir018adq3qczavasx4s2z5hb9jck7jv9ri4sa")))

(define-public crate-posthog-rs-0.1.2 (c (n "posthog-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0nxcy09bv9v972rji1f6d60l1irkpsdk4czix0w9yk4m9ai27q11")))

(define-public crate-posthog-rs-0.1.3 (c (n "posthog-rs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1c6qp0rnvx7qwyv2i86l6vqigg2q66f5dwcda4k219q71mrvqkjk")))

(define-public crate-posthog-rs-0.2.0 (c (n "posthog-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "08d0xmg32qbwv0wfbg8n2kfz50gizj522y1ly1jp5qaydzgc9wgs")))

(define-public crate-posthog-rs-0.2.1 (c (n "posthog-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0zqxpnhwxj1r9g6yllyrzvslwz6vvyyj651mxrhwlrwkxd7sv491")))

(define-public crate-posthog-rs-0.2.2 (c (n "posthog-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "18yz3i7acpk72ha2bbvg9kd31gqgx47c922jc699jhahzrgv7lca")))

