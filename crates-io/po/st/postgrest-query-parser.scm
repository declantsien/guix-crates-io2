(define-module (crates-io po st postgrest-query-parser) #:use-module (crates-io))

(define-public crate-postgrest-query-parser-0.1.0 (c (n "postgrest-query-parser") (v "0.1.0") (d (list (d (n "peekmore") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nnpcclc2c5jpsspb0lcl94vfxd2p1jc00navsq4bh33lywwbm5h")))

