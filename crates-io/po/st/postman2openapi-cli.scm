(define-module (crates-io po st postman2openapi-cli) #:use-module (crates-io))

(define-public crate-postman2openapi-cli-1.1.0 (c (n "postman2openapi-cli") (v "1.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postman2openapi") (r "^1.1.0") (d #t) (k 0)))) (h "112jkh6ls7iqx58h9vj8ibfpismp0n05pbqc8i28nkd0dwgkgvxy")))

(define-public crate-postman2openapi-cli-1.1.1 (c (n "postman2openapi-cli") (v "1.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postman2openapi") (r "^1.1.0") (d #t) (k 0)))) (h "0n0412brbs0v6gmicppgn25jadf8r4g9raq45yl22bzrcdg55jmf")))

(define-public crate-postman2openapi-cli-1.2.0 (c (n "postman2openapi-cli") (v "1.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postman2openapi") (r "^1.1.0") (d #t) (k 0)))) (h "199h3j807wj9knjm2q2bkkl24ba2lc72gl34zgcwz87h7dvf174l")))

(define-public crate-postman2openapi-cli-1.2.1 (c (n "postman2openapi-cli") (v "1.2.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postman2openapi") (r "^1.1.0") (d #t) (k 0)))) (h "0kzg6b2vks27wxsn4rqnfavkpn733qkf6fcrvq3jbypyy2m6z55a")))

