(define-module (crates-io po st postgres-from-row) #:use-module (crates-io))

(define-public crate-postgres-from-row-0.1.0 (c (n "postgres-from-row") (v "0.1.0") (d (list (d (n "postgres") (r "^0.19.4") (o #t) (k 0)) (d (n "postgres-from-row-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (o #t) (k 0)))) (h "1qlxlf7srjd5yqjv8zr390wbjlb0pdaw9i26rakdwfgq712sspc7") (f (quote (("default" "postgres")))) (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-postgres-from-row-0.2.0 (c (n "postgres-from-row") (v "0.2.0") (d (list (d (n "postgres") (r "^0.19.4") (o #t) (k 0)) (d (n "postgres-from-row-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (o #t) (k 0)))) (h "0kdy6l4h34jhcxn0q5hgnmq40rk2vx5zmslwkghkah4jbhp2lc0n") (f (quote (("default" "postgres")))) (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-postgres-from-row-0.3.0 (c (n "postgres-from-row") (v "0.3.0") (d (list (d (n "postgres") (r "^0.19.4") (o #t) (k 0)) (d (n "postgres-from-row-derive") (r "=0.3.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (o #t) (k 0)))) (h "044n2wilvl2xr4lr4bnp54pavpc0sbv4f6ivp4l6vm9qkbdg68jz") (f (quote (("default" "postgres")))) (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-postgres-from-row-0.4.0 (c (n "postgres-from-row") (v "0.4.0") (d (list (d (n "postgres") (r "^0.19.4") (o #t) (k 0)) (d (n "postgres-from-row-derive") (r "=0.4.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (o #t) (k 0)))) (h "043yzd2rr837isrffc0i5pbc0cjma57sjqsf8v0a0sfgid0rving") (f (quote (("default" "postgres")))) (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-postgres-from-row-0.5.0 (c (n "postgres-from-row") (v "0.5.0") (d (list (d (n "postgres-from-row-derive") (r "=0.5.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (k 0)))) (h "00sh3mdcsc71yqlg7vbrqy2s990js84l0ypgyic7d2njkvkk2jnw")))

(define-public crate-postgres-from-row-0.5.1 (c (n "postgres-from-row") (v "0.5.1") (d (list (d (n "postgres-from-row-derive") (r "=0.5.1") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.7") (k 0)))) (h "1rg1hd4m1adls8gsb6wmqycxvnqncb99qyniqylfvm8x9y45r50y")))

(define-public crate-postgres-from-row-0.5.2 (c (n "postgres-from-row") (v "0.5.2") (d (list (d (n "postgres-from-row-derive") (r "=0.5.2") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.8") (k 0)))) (h "0j6rwngpx3rfh33v4pwhav77l8j3w4qc1zmvyfab9mq07gq6r40k")))

