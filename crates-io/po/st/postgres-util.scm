(define-module (crates-io po st postgres-util) #:use-module (crates-io))

(define-public crate-postgres-util-0.1.0 (c (n "postgres-util") (v "0.1.0") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1w11rwj565n44np89n60gcx6gzfysahjnym75myqcvggnmlcszdq")))

(define-public crate-postgres-util-0.1.1 (c (n "postgres-util") (v "0.1.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0mxq8wrvndc84fac0yvp7q2r0pnzqxdp9w333wy0ks0bnrm2xpy8")))

