(define-module (crates-io po st postgres_query_macro) #:use-module (crates-io))

(define-public crate-postgres_query_macro-0.2.0 (c (n "postgres_query_macro") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "115kcfrmy5zagnmhvfbrhn331npjbfr9n7k4ggshw21z0pvc54ha")))

(define-public crate-postgres_query_macro-0.3.0 (c (n "postgres_query_macro") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0yggslqa65aqk2ysal92895sil5yrhid0wb4smc92972p65rlbhh")))

(define-public crate-postgres_query_macro-0.3.1 (c (n "postgres_query_macro") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1bavpff9v3k2r18g28h6bk3jygf0zcl5lg1clyvw50mms07dw3bg")))

