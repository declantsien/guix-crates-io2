(define-module (crates-io po st postform_decoder) #:use-module (crates-io))

(define-public crate-postform_decoder-0.1.0 (c (n "postform_decoder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "object") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hxawvi205pyqkbqkmmnrx5n0f87ypn5f7azvsmzv0s1n7f45y3p")))

(define-public crate-postform_decoder-0.2.0 (c (n "postform_decoder") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "object") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jk3vlqk2llp9vm8frppvqj2cs7pjzjh5mr4hk2gszaacvwzmh6r")))

(define-public crate-postform_decoder-0.3.0 (c (n "postform_decoder") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "object") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yvs16vycb6h5ba6ig1iidrd7kh0gxq7lw5741c5n1mya2pqp01w")))

(define-public crate-postform_decoder-0.4.0 (c (n "postform_decoder") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.22") (d #t) (k 0)) (d (n "rcobs") (r "^0.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dq6j3kwggx29zcawqiabaz89g4kiimlmmiwwfkkmgn64nb5jmy3")))

(define-public crate-postform_decoder-0.5.0 (c (n "postform_decoder") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.22") (d #t) (k 0)) (d (n "rcobs") (r "^0.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y71w41a2hp386dk1k6q6l6b2n8jzjczag4bdn5yzh9zn2lw5p9c")))

