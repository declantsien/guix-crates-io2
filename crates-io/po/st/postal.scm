(define-module (crates-io po st postal) #:use-module (crates-io))

(define-public crate-postal-0.1.0 (c (n "postal") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "1p75ibxqxgp7w56bk5xim07xkmsr4icl4sqh5xr3dzq34ayvlimz") (l "libpostal")))

(define-public crate-postal-0.2.0 (c (n "postal") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0q2x1bg4ywm441zlw9rqzvjlyai6vgzk29y1xwwkmz10n0wgmbaa") (l "libpostal")))

(define-public crate-postal-0.2.1 (c (n "postal") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0kaiwn95fbdbnaijd5xv3gx7pv5p87rpcwqv6sqi5g2kzn43xal5") (l "libpostal")))

(define-public crate-postal-0.2.2 (c (n "postal") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0vvh5zd60n2p3nrhdvk75hb3rpv7riancrixsf59i0qhi0iycgw7") (l "libpostal")))

(define-public crate-postal-0.2.3 (c (n "postal") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "10gz57aq4va5dy4vlyi42jysirpvnmpbwkxn4msdzy9xl7lm5q6b") (l "libpostal")))

(define-public crate-postal-0.2.4 (c (n "postal") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0ximkl4czadns3n6djzhvy41qcv3pylbk7ql0zshkmvc4fpv2y6s") (l "libpostal")))

(define-public crate-postal-0.2.5 (c (n "postal") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0nzq89yq5q1xkx5sz2xbnp454zxm6vj9xxjl88crc5fkng7syms5") (l "libpostal")))

(define-public crate-postal-0.2.6 (c (n "postal") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "1af3qs6lc9730kbmqpkk28g2p42ig60cx4k5y74gyakkwfyjqa9g") (l "libpostal")))

