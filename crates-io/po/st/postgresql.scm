(define-module (crates-io po st postgresql) #:use-module (crates-io))

(define-public crate-postgresql-0.1.0 (c (n "postgresql") (v "0.1.0") (h "17xnfw42ihsp25y7lx25zj842pf5h8jpdhgam249nki8xqr7admg") (y #t)))

(define-public crate-postgresql-0.1.1 (c (n "postgresql") (v "0.1.1") (h "1kszidpzxn9ign8a83qhi6vql1wkqnzj419cvzgzy06djrhxgy49")))

