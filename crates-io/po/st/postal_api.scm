(define-module (crates-io po st postal_api) #:use-module (crates-io))

(define-public crate-postal_api-0.1.0 (c (n "postal_api") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1gprhclnhqxm5zahw3v8f34b4072a0njhhln64dq6cpaagnfsm7j")))

(define-public crate-postal_api-0.2.0 (c (n "postal_api") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1fizbg1p9kn26ypjxp5l48prfpbagajqmc0a9nq4xamlzbxr57a6")))

(define-public crate-postal_api-0.2.1 (c (n "postal_api") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1pjwgc7k8dqs7n3xz4mj68r9ir24q7k32i8a9ah2q7x279acpd0v")))

