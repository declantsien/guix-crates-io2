(define-module (crates-io po st postgres-binary-copy) #:use-module (crates-io))

(define-public crate-postgres-binary-copy-0.1.0 (c (n "postgres-binary-copy") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.9.3") (d #t) (k 0)))) (h "03xcs5wlbby2q7vhrhw63yzyzwh2g7p3zqd2hn708zwcb9avqagj")))

(define-public crate-postgres-binary-copy-0.1.1 (c (n "postgres-binary-copy") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.10") (d #t) (k 0)))) (h "16hpaswb57m6s3w0dm2wqwkxh77sagzyq9f966166wz7qbnx7ba2")))

(define-public crate-postgres-binary-copy-0.1.2 (c (n "postgres-binary-copy") (v "0.1.2") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.10") (d #t) (k 0)))) (h "1v54rfp35agrfchc20a46sc7fiffs0f3ngj9jif5rc2q52nxcpm6")))

(define-public crate-postgres-binary-copy-0.2.0 (c (n "postgres-binary-copy") (v "0.2.0") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "1dck5ixj0a84aky0p07z3778wm8wlfrw0b3824fklrl2a7czwm8g")))

(define-public crate-postgres-binary-copy-0.2.1 (c (n "postgres-binary-copy") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "0g024plin6hwhdjbabk58sb235rpgh8i3d24q1h2ylhp16pspn97")))

(define-public crate-postgres-binary-copy-0.3.0 (c (n "postgres-binary-copy") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.12") (d #t) (k 0)))) (h "06qa65d7rm946cqip56j1r5l74k8rcm9r91hr38fv6sl0wfnam21")))

(define-public crate-postgres-binary-copy-0.3.1 (c (n "postgres-binary-copy") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r ">= 0.12, < 0.14") (d #t) (k 0)))) (h "10vzf23171ck1abjh2hsn6a87388yriclxzhmxiv6b7dijisxms4")))

(define-public crate-postgres-binary-copy-0.4.0 (c (n "postgres-binary-copy") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.1") (d #t) (k 0)))) (h "0jgaimhkaixnygwx8b01bkmjvp05qhd1hrd5ril63wpak0yyvxwq")))

(define-public crate-postgres-binary-copy-0.5.0 (c (n "postgres-binary-copy") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.1") (d #t) (k 0)))) (h "18iglrj2r3yp2yasi7xlzxl8cmijz9s8advag93ajm9ydndmg0p6")))

