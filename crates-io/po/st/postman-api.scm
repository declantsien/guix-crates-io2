(define-module (crates-io po st postman-api) #:use-module (crates-io))

(define-public crate-postman-api-0.1.0 (c (n "postman-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1glrmzcxf646ahagif5hgc2jzi68mf91ccb00b7jmq83zjiv1rm4")))

(define-public crate-postman-api-1.0.0 (c (n "postman-api") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0vrgkcjjsmp2pg9fjni4zh3cv04bvwd58ypi2nis4l136ghvl07m")))

(define-public crate-postman-api-2.0.0 (c (n "postman-api") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1xvzrdxp0m1p45y93f4hbnmi0ngpk2gsq855ha83jbbzwr01c0dx")))

