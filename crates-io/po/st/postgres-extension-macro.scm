(define-module (crates-io po st postgres-extension-macro) #:use-module (crates-io))

(define-public crate-postgres-extension-macro-0.1.0 (c (n "postgres-extension-macro") (v "0.1.0") (h "1vm2d08807klqc3m5p105ggk8zxpx2np30ifa5955yf1kkpnb2v6")))

(define-public crate-postgres-extension-macro-0.1.1 (c (n "postgres-extension-macro") (v "0.1.1") (h "0gkgk60942dci3gyghyh3vc02mhc9fbvh5w62d0xl7c2dqzi2n64")))

