(define-module (crates-io po st postgres_array) #:use-module (crates-io))

(define-public crate-postgres_array-0.1.0 (c (n "postgres_array") (v "0.1.0") (d (list (d (n "postgres") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qd9cdvwn69202hzfxi65wnydxz3ig9rwkrjh8hs77scb7d62ian") (f (quote (("default" "uuid"))))))

(define-public crate-postgres_array-0.1.1 (c (n "postgres_array") (v "0.1.1") (d (list (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1w7il86kh5j1g7jwacxfjr5rk8zdn135r87hw70kha4cvcig0ixc") (f (quote (("default" "uuid"))))))

(define-public crate-postgres_array-0.1.2 (c (n "postgres_array") (v "0.1.2") (d (list (d (n "postgres") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jbkvk4lk7jsp4vhmhbakkxhjz76gq8a6r86m332qigla3gwyr7w") (f (quote (("default" "uuid"))))))

(define-public crate-postgres_array-0.2.0 (c (n "postgres_array") (v "0.2.0") (d (list (d (n "postgres") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0d5hci8swazdqqf44yghmv75rffnmrj9kji9x8izk8dsbzlhcddw") (f (quote (("default" "uuid"))))))

(define-public crate-postgres_array-0.2.1 (c (n "postgres_array") (v "0.2.1") (d (list (d (n "postgres") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jm1msbsxjfppcpay2x6mni4l3vnqy92p970wlfsm2gi6mb3ins3") (f (quote (("default" "uuid"))))))

(define-public crate-postgres_array-0.3.0 (c (n "postgres_array") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.2.11") (d #t) (k 0)) (d (n "postgres") (r "^0.7.1") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "083n0h6zrp7qjw4p8rzwis0ifr1g62xmfwk2jbjr3f45i55d6jbz")))

(define-public crate-postgres_array-0.3.1 (c (n "postgres_array") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.7.1") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "195wkscx4lnkfhfwj1ij30kw96cndddpisphbqy6dz4qrc97zqa8")))

(define-public crate-postgres_array-0.4.0 (c (n "postgres_array") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "19ykxsihc21q00gapxh40x25i6rzpy9b5p0vi9v6v5zb45r706vp")))

(define-public crate-postgres_array-0.4.1 (c (n "postgres_array") (v "0.4.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "1r5dxnn4xn2676grlq71gvdfjd997czy4c4hp04hcda446zn3q1m")))

(define-public crate-postgres_array-0.4.2 (c (n "postgres_array") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.8") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "1sfgkds165ynz1qxkvv07jim485zm41w3a7isnzndi4z6pd131an")))

(define-public crate-postgres_array-0.5.0 (c (n "postgres_array") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.9") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "05wdglaqd5wcx9x07m94akjmzbsdq5yq2l5gmjc7yvq5p3pbz35i")))

(define-public crate-postgres_array-0.5.1 (c (n "postgres_array") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "0c6y0mr6pi6zqz0b5s227k14fl06ixbyr9m9wjai951mdkj261q0")))

(define-public crate-postgres_array-0.5.2 (c (n "postgres_array") (v "0.5.2") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "0hb02k7m2cvr5wcx4qw55bv73x7v3zn5d0xsxqlhcr6jfx02rw6b")))

(define-public crate-postgres_array-0.6.0 (c (n "postgres_array") (v "0.6.0") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "*") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "0dhziyinmvdq6xn5mhy99rapsnshp2g3irh6nl8haqzjp0nmcazg")))

(define-public crate-postgres_array-0.6.1 (c (n "postgres_array") (v "0.6.1") (d (list (d (n "byteorder") (r ">= 0.3, < 0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "0f3f5dmd973z8dsqx9kpv8xwxdzf1z52mh2m92wsib4j33g6yz9w")))

(define-public crate-postgres_array-0.6.2 (c (n "postgres_array") (v "0.6.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (f (quote ("rustc-serialize" "time" "uuid"))) (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.1") (d #t) (k 2)))) (h "0czgnb9r4wg975s4i6kbgjj361v1d2dx499r8k6x8vzwv2196w7h")))

(define-public crate-postgres_array-0.7.0 (c (n "postgres_array") (v "0.7.0") (d (list (d (n "fallible-iterator") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.12") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.1") (d #t) (k 0)))) (h "0hvw3n6agcqvsyi9kxlm2akfyjk0d6jq2r7rg133fn3l5yk30qdm")))

(define-public crate-postgres_array-0.7.1 (c (n "postgres_array") (v "0.7.1") (d (list (d (n "fallible-iterator") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r ">= 0.12, < 0.14") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.1") (d #t) (k 0)))) (h "16qlk7k2m59qcf08a2px9699agnkh03sdar3yvk1llilnqvawgy8")))

(define-public crate-postgres_array-0.8.0 (c (n "postgres_array") (v "0.8.0") (d (list (d (n "fallible-iterator") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.14") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.3") (d #t) (k 0)))) (h "1dwx967l7nlwy86a24zifqpy7fqawhakaf74c6hrjf5m4a58qm6r")))

(define-public crate-postgres_array-0.9.0 (c (n "postgres_array") (v "0.9.0") (d (list (d (n "fallible-iterator") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.3") (d #t) (k 0)) (d (n "postgres-shared") (r "^0.4") (d #t) (k 0)))) (h "0dva6xqss08biv463jibdnc1rh61dn25gh8zkasj9d02pa8nb9j7")))

(define-public crate-postgres_array-0.10.0 (c (n "postgres_array") (v "0.10.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.5") (d #t) (k 0)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "0ap4qzglqymzhamvdwc20dk3ih20k12hjq1a11578qdhxmnicpp4")))

(define-public crate-postgres_array-0.11.0 (c (n "postgres_array") (v "0.11.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.6") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "104sng1sz4fhm04rym3rm0x4z7nswhpy1yxfvvliyhv4lmmmmw0c")))

(define-public crate-postgres_array-0.11.1 (c (n "postgres_array") (v "0.11.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 2)) (d (n "postgres-protocol") (r "^0.6") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)))) (h "0kmzlwb1m9697cqap9b3fm52b19k5jbp4lgwhw6n4mx8qfwchrr6")))

