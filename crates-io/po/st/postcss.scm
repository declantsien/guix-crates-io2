(define-module (crates-io po st postcss) #:use-module (crates-io))

(define-public crate-postcss-0.0.1 (c (n "postcss") (v "0.0.1") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1l4197vljqmz35i8ikhm1sa4l7mifsmqc09g4ci73wwvwldfabr3")))

(define-public crate-postcss-0.0.2 (c (n "postcss") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "ropey") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0s6lnkkhfkx1mfk1xqsw2vhnhpgp1c4ny6rwih6wvdj1sjb0bkm9")))

