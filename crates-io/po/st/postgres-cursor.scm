(define-module (crates-io po st postgres-cursor) #:use-module (crates-io))

(define-public crate-postgres-cursor-0.1.0 (c (n "postgres-cursor") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0lpas6b962ssg4y09hw5hc2zd1d2ag7jmajvhbcfzb4n256rakc8")))

(define-public crate-postgres-cursor-0.2.1 (c (n "postgres-cursor") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19wnbjrnvk8f7ilmlb31rc4zxzzh47jk5f17qpa6h3k5i6h7d3d0")))

(define-public crate-postgres-cursor-0.2.2 (c (n "postgres-cursor") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ws9w60y65vhm3567mfbd2zbjdy31dbswhas9sdmcn6mwvaqms78")))

(define-public crate-postgres-cursor-0.3.0 (c (n "postgres-cursor") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.15.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06q1vy1zph1f73r7jc0r4gi9g6y3m9vj717sz4iqikbyc1b2zaka")))

(define-public crate-postgres-cursor-0.4.0 (c (n "postgres-cursor") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0bbdadjbajh5ydyms44m4w0iq2y282srfhiad0js6f0xj5ccw6ag")))

