(define-module (crates-io po st postbus) #:use-module (crates-io))

(define-public crate-postbus-0.1.0 (c (n "postbus") (v "0.1.0") (h "1vqizbv9xkd3vfyiin3snms0wa0sipyl2kvsljajaws8qvb6mvzm")))

(define-public crate-postbus-0.2.0 (c (n "postbus") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mailparse") (r "^0.13.6") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("rt" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "18klmk7i5j2bdjfds1fgfz2p8ib28wrc5lbiyazkv1ydkrv80kn3")))

