(define-module (crates-io po st postgrest-query) #:use-module (crates-io))

(define-public crate-postgrest-query-0.1.0 (c (n "postgrest-query") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0pl55365qwnljb4nl26qg87dfg5618p17w49whvsk07f1d6nrnxi")))

