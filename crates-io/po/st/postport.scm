(define-module (crates-io po st postport) #:use-module (crates-io))

(define-public crate-postport-0.1.1 (c (n "postport") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)))) (h "1jixlcx8wvvcdjgi19c491hp1g0yqm6yjicxazq7njh8ya140vzb")))

