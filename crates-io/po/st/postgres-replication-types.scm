(define-module (crates-io po st postgres-replication-types) #:use-module (crates-io))

(define-public crate-postgres-replication-types-0.1.0 (c (n "postgres-replication-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1f73wg1f9b0sadh72ckx4kd17j480kpd6wdcaiqvnrqjq6xfqgyk")))

(define-public crate-postgres-replication-types-0.1.1 (c (n "postgres-replication-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1mmn87m1q3kbxa70d5f5sp62c7j530kx9b318k80p1pk27b70n18")))

