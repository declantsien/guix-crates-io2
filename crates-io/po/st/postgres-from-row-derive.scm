(define-module (crates-io po st postgres-from-row-derive) #:use-module (crates-io))

(define-public crate-postgres-from-row-derive-0.1.0 (c (n "postgres-from-row-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cml3i5fwxq4ddwfxi7ni3p6f97w8fzq1qcq7vcjc62lcb31qjr0")))

(define-public crate-postgres-from-row-derive-0.2.0 (c (n "postgres-from-row-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09iva7z6pgc07b0z3r69sykhddlxhnf9pkpr0w7qf6fyq71k3a3p")))

(define-public crate-postgres-from-row-derive-0.3.0 (c (n "postgres-from-row-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cvp3dw0wl5y42b75q4sx9kqgj40ipla043gdbh3g33bfh581kfk")))

(define-public crate-postgres-from-row-derive-0.4.0 (c (n "postgres-from-row-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1idmiw0dncjkrdmcszmp637h7g76w0y4sapfzdff79lgx9rhkpra")))

(define-public crate-postgres-from-row-derive-0.5.0 (c (n "postgres-from-row-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wb7g0fc91s16k7ich9316y03p054vj5q8v96ifrrng74c1ka82z")))

(define-public crate-postgres-from-row-derive-0.5.1 (c (n "postgres-from-row-derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a3aw1hq6b189m6wjhhl1wrpj7d28bxbcynffjvqhjky7k8ix9pm")))

(define-public crate-postgres-from-row-derive-0.5.2 (c (n "postgres-from-row-derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (d #t) (k 0)))) (h "19mh0gzyw2wq1568w2gc0zbaxfklyfkqhfwwjv20pz6xqzn2y9hn")))

