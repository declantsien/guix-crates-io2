(define-module (crates-io po st postgres-derive-macros) #:use-module (crates-io))

(define-public crate-postgres-derive-macros-0.1.0 (c (n "postgres-derive-macros") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)) (d (n "postgres") (r "^0.11.3") (d #t) (k 2)) (d (n "postgres-derive-codegen") (r "^0.1") (f (quote ("nightly"))) (k 0)))) (h "1fby2k855p62sghxran0i91zamff39z1g54qsghdqzz97yksczxi")))

(define-public crate-postgres-derive-macros-0.1.1 (c (n "postgres-derive-macros") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)) (d (n "postgres") (r "^0.11.3") (d #t) (k 2)) (d (n "postgres-derive-codegen") (r "^0.1.1") (f (quote ("nightly"))) (k 0)))) (h "1b6pdis9nan6a1cpy86axjczrj532bh9vlaahagsmz9svchcmcgh")))

(define-public crate-postgres-derive-macros-0.1.2 (c (n "postgres-derive-macros") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "postgres") (r "^0.11.3") (d #t) (k 2)) (d (n "postgres-derive-codegen") (r "^0.1.2") (f (quote ("nightly"))) (k 0)))) (h "0k73z40fy0kxjqvkaf7pvhrngy0l2571fhs6c9byqm6jmncxxnaf")))

(define-public crate-postgres-derive-macros-0.1.3 (c (n "postgres-derive-macros") (v "0.1.3") (d (list (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "postgres") (r "^0.11.3") (d #t) (k 2)) (d (n "postgres-derive-codegen") (r "^0.1.3") (f (quote ("nightly"))) (k 0)))) (h "1j0azf22izschd307mvbga66r0gg71ahjdil8j9q5i4f27s3jx9b")))

(define-public crate-postgres-derive-macros-0.1.4 (c (n "postgres-derive-macros") (v "0.1.4") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.11.3") (d #t) (k 2)) (d (n "postgres-derive-codegen") (r "^0.1.4") (f (quote ("nightly"))) (k 0)))) (h "0vd94754y7y4h2837nb8dsv4z22xglpk9bdigq36r276cnykxywy")))

