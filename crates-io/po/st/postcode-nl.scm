(define-module (crates-io po st postcode-nl) #:use-module (crates-io))

(define-public crate-postcode-nl-0.1.0 (c (n "postcode-nl") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0fj3r13m0ddspj6mmwbllj65id3chc3s8i37w91n61kmf0qxa7br")))

(define-public crate-postcode-nl-0.1.1 (c (n "postcode-nl") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1gk0jkbr8g1yg2w8vgz8wsm0p15qij5z61flcdhas9rznkwbd8am")))

