(define-module (crates-io po st postform_persist) #:use-module (crates-io))

(define-public crate-postform_persist-0.2.0 (c (n "postform_persist") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xlmfs5nd3krkyayki88dypnhg5wp6c3v8prbzmihn8a76772zqc")))

(define-public crate-postform_persist-0.3.0 (c (n "postform_persist") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vzix46cyqylfx9wwlkkr5fi706068b990ic7szqry7y3gygy03a")))

(define-public crate-postform_persist-0.4.0 (c (n "postform_persist") (v "0.4.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "173gfgb2yrp2ryk0x1fdiznxxg5cfssc1mg51qlpwvkmq2s9748z")))

(define-public crate-postform_persist-0.5.0 (c (n "postform_persist") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postform_decoder") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "124ivdkmal7sdr864cvp763wrf7p6w7kby98f3h3ng1mz4fkpn8h")))

