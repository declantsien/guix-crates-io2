(define-module (crates-io po st postgres-mapper-derive) #:use-module (crates-io))

(define-public crate-postgres-mapper-derive-0.1.0 (c (n "postgres-mapper-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "04bl75l95mdgqf7x1005i5dzs30p55c20grd03p2fq2ic6dvcwjd") (f (quote (("tokio-postgres-support") ("postgres-support") ("postgres-mapper") ("default"))))))

(define-public crate-postgres-mapper-derive-0.1.1 (c (n "postgres-mapper-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04ay7kybdx7f33akp86scy69rilx0lshkma932p55lkgmw6w63zq") (f (quote (("tokio-postgres-support") ("postgres-support") ("postgres-mapper") ("default"))))))

