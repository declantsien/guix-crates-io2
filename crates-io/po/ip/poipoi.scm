(define-module (crates-io po ip poipoi) #:use-module (crates-io))

(define-public crate-poipoi-2.0.0 (c (n "poipoi") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "06s3nq4shzc5svi4fgjsshq0vqxs7zw3q8ddvyw4yj2ymlx3z3rx")))

(define-public crate-poipoi-2.0.1 (c (n "poipoi") (v "2.0.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "190slzvl2vj9rg3ilb4ys83c8rln3nm5vmriqw71wz8frwr47x4c")))

(define-public crate-poipoi-2.0.2 (c (n "poipoi") (v "2.0.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00c1lf8d7dggzsakgpmrwj9pgp9jhxzpzjb9vxp3xmlwpbf1p1w6")))

(define-public crate-poipoi-2.0.3 (c (n "poipoi") (v "2.0.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17qa5bh0myx39sgri996pp6958292ydsc313b03ffbrri5fwli4f")))

(define-public crate-poipoi-2.1.0 (c (n "poipoi") (v "2.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "04plflcqq3di7shz33fldp2d62c8b2pwpy42bg4dgdsbb3kfd9a5")))

(define-public crate-poipoi-2.2.0 (c (n "poipoi") (v "2.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1gfv04yh5mxjnc5cx1kaz9x6vl378029fcsc7hl1158lgqcxvn8j")))

(define-public crate-poipoi-2.3.0 (c (n "poipoi") (v "2.3.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0a1avba8r6kjn2as3322a5wwdblkvr2i20x4mhw5yg5q0yw7hqcg")))

