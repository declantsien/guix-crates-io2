(define-module (crates-io yt _t yt_tsu) #:use-module (crates-io))

(define-public crate-yt_tsu-0.1.0 (c (n "yt_tsu") (v "0.1.0") (d (list (d (n "mpeg2ts") (r "^0.2.0") (d #t) (k 0)))) (h "15b79bmcfmyj428yfacxyjcvj0fqc5vxm5b8sl44mkv7133w97nx")))

(define-public crate-yt_tsu-0.1.1 (c (n "yt_tsu") (v "0.1.1") (d (list (d (n "mpeg2ts") (r "^0.3.1") (d #t) (k 0)))) (h "0dnvz827w2vqmy8yf9vfvpp45pc45cd6zyfc5xs9p6435rn0jmn3")))

