(define-module (crates-io yt ne ytnef) #:use-module (crates-io))

(define-public crate-ytnef-0.1.2 (c (n "ytnef") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ytnef_sys") (r "^0.1") (d #t) (k 0)))) (h "007ri94sq7rg7mb3mmwp0d1zkrsdja99d319p6h2akdkx9n5mgry")))

(define-public crate-ytnef-0.2.0 (c (n "ytnef") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ytnef_sys") (r "^0.2") (d #t) (k 0)))) (h "1mix8w3bi6cgq2lvibqs81xm76bm4ijqlqdb2nbq2q8xd43liq85")))

(define-public crate-ytnef-0.3.0 (c (n "ytnef") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ytnef_sys") (r "^0.3") (d #t) (k 0)))) (h "08rd25wgcrzvcz2vpdx1m3g7x4b60bgsss0badnv0wfk6hz18a2s")))

(define-public crate-ytnef-0.4.0 (c (n "ytnef") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ytnef_sys") (r "^0.3") (d #t) (k 0)))) (h "11ip2n4j31w623i59yn7jjj68wja9a1jgxl0vwrzyzivbhwng0xv")))

