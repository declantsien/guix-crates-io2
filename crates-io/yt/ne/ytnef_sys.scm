(define-module (crates-io yt ne ytnef_sys) #:use-module (crates-io))

(define-public crate-ytnef_sys-0.1.0 (c (n "ytnef_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1c21420da14pg8kcfhpc8gx1skwiqbygffww869vnlznsjjvdhwr")))

(define-public crate-ytnef_sys-0.2.0 (c (n "ytnef_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07jadvf76km2q5h9la8gxizbjdzikmah1zpvwhzwky4qj5wyx8k8")))

(define-public crate-ytnef_sys-0.3.0 (c (n "ytnef_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1n1kasqr89rqmmlgmy27zkjl5s51ilkn1vqsfxnwknqd1nj0c2m8")))

(define-public crate-ytnef_sys-0.3.1 (c (n "ytnef_sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "091fny6bn4gm2pl27bl3s3xnjhlw6vv796qwcnmnc8fgajvdljvh")))

