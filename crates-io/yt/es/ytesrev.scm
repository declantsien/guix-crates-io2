(define-module (crates-io yt es ytesrev) #:use-module (crates-io))

(define-public crate-ytesrev-0.1.0 (c (n "ytesrev") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1nyaxpnw0s87xvi866iljb2mlfl14kjq60i84cdryjy4vhfbgj51")))

(define-public crate-ytesrev-0.1.1 (c (n "ytesrev") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "0a8xvcqw02kg0jk1xblnam3yfmghj2w9irsar2cbsrdqrhiw9rq6")))

(define-public crate-ytesrev-0.2.0 (c (n "ytesrev") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "12z9ff7ikvrl7wlj3i9c26syqdcqwgslwich2g7srj6cs33qxn2f")))

(define-public crate-ytesrev-0.2.1 (c (n "ytesrev") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1y8nrldm0c9q7pwcar0dbkl7jg8xyfz3ih8l0dyvd2h3kpa5wdfm")))

(define-public crate-ytesrev-0.2.2 (c (n "ytesrev") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1n1nb8bzffnx7qm7c9affg70zwrqdcxhb4gxccp1rb0l785xrpbc")))

(define-public crate-ytesrev-0.2.3 (c (n "ytesrev") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "14bcdac7lwsm93ih9cxcjk4x42hvdg70y04l8bq8hjrnpfx1rdh6")))

(define-public crate-ytesrev-0.2.4 (c (n "ytesrev") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "004n516186vvpvanxnd0zi4f6dfbmdssk4lrslmkhas9qg838pdv")))

(define-public crate-ytesrev-0.2.5 (c (n "ytesrev") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1vh3a8mcjcsrrbsmvy8gdgnnwbg62vc4772czip2jg7zzqngg7s1")))

(define-public crate-ytesrev-0.2.6 (c (n "ytesrev") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1rg3xn19g0xc3w78ki247ilm9zlcm278gcc4vcfwl6ckfhqw0afm")))

(define-public crate-ytesrev-0.2.7 (c (n "ytesrev") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0mg2rqxh8hnq13a0cwl409isyxhdasql1rsw6cjwfjkh3lhx65v8")))

