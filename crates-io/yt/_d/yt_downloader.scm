(define-module (crates-io yt _d yt_downloader) #:use-module (crates-io))

(define-public crate-yt_downloader-0.1.0 (c (n "yt_downloader") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "rafy") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)))) (h "06043jwn53gbvws9lr1lvi7gfg2jrswrvnbspwxqr5190bg2500p")))

(define-public crate-yt_downloader-1.0.0 (c (n "yt_downloader") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "rafy") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)))) (h "0c1vyksyfs034kh3k5hkhxzzqklf9hnnln88cr6dyjxcjsqh09ma")))

