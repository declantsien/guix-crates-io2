(define-module (crates-io yt ui ytui) #:use-module (crates-io))

(define-public crate-ytui-0.1.0 (c (n "ytui") (v "0.1.0") (h "0slcj2q5sncszyha31bpkyvz2pxyjpjxy92dcwvm0s2n5h892pni") (y #t)))

(define-public crate-ytui-0.1.1 (c (n "ytui") (v "0.1.1") (h "1y2s96kkfadiwbpz5094yaps99q228phmpmh47p26yf9njsid9sy")))

