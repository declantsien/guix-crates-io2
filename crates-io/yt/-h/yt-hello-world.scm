(define-module (crates-io yt -h yt-hello-world) #:use-module (crates-io))

(define-public crate-yt-hello-world-1.0.0 (c (n "yt-hello-world") (v "1.0.0") (h "0m62mhvs2jffv062vpjbz582w8hiy646fnayz1vixgsh6jiv9d6x") (y #t)))

(define-public crate-yt-hello-world-1.0.1 (c (n "yt-hello-world") (v "1.0.1") (h "15s81grhh4i4f2fyhbkz4aj0cqgqh494l2qhmsgnnzkpb2l2adab") (y #t)))

(define-public crate-yt-hello-world-0.9.10 (c (n "yt-hello-world") (v "0.9.10") (h "0rbgpk6m1bvcrk1qgi16g3ik5j09f9ajiw5slb884bkhq47ar6g3") (y #t)))

(define-public crate-yt-hello-world-0.9.11 (c (n "yt-hello-world") (v "0.9.11") (h "1mxb1fjyw78xwfxr09lldgqmq5zk5ma3h0b3mwz7wqiz13mmamqc") (y #t)))

(define-public crate-yt-hello-world-0.9.12 (c (n "yt-hello-world") (v "0.9.12") (h "0q2lvcymars0v444w05blk3zl6vvgg6rbywsfz5i9ng4zjas5yhi") (y #t)))

