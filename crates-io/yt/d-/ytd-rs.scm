(define-module (crates-io yt d- ytd-rs) #:use-module (crates-io))

(define-public crate-ytd-rs-0.1.0 (c (n "ytd-rs") (v "0.1.0") (h "1gnb48zf6fmmy5vr45mb9bn7ja4kmi3vcd45kbkxmchs46131cpg")))

(define-public crate-ytd-rs-0.1.1 (c (n "ytd-rs") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13iqga64z5a8yz72z05pqcagd58g5hmqnnr9y36jra32a3dgim85")))

(define-public crate-ytd-rs-0.1.2 (c (n "ytd-rs") (v "0.1.2") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1758sf86nh5qicbdvnj03kr7rf0396lmxnc2d13jp7r71q1jrmlk")))

(define-public crate-ytd-rs-0.1.3 (c (n "ytd-rs") (v "0.1.3") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "01zzr42gslr2bc4jrkvy1c2n3gdqy562qhkcah7l5z27ngiavsxh")))

(define-public crate-ytd-rs-0.1.4 (c (n "ytd-rs") (v "0.1.4") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0xjpnxqqrx261p7x0830qbmcy0kw9bhc2f1w4m5al6ig96kiqfg3")))

(define-public crate-ytd-rs-0.1.5 (c (n "ytd-rs") (v "0.1.5") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0r56c8w5zx8j0hkrzjk1v1rqxilq2i1qg7zz50wp9628qhqbvqv6") (f (quote (("yt-dlp") ("youtube-dlc"))))))

(define-public crate-ytd-rs-0.1.6 (c (n "ytd-rs") (v "0.1.6") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b2vxabjq5frha0xfc6dzvmhcfrjpn9bmb6iyqjjxwvq2gpgk631") (f (quote (("yt-dlp") ("youtube-dlc"))))))

(define-public crate-ytd-rs-0.1.7 (c (n "ytd-rs") (v "0.1.7") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1sb5278286sp3bdllsvn9rw8ycddfchyxhisngf6pk9g67km64a9") (f (quote (("yt-dlp") ("youtube-dlc"))))))

