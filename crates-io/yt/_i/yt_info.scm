(define-module (crates-io yt _i yt_info) #:use-module (crates-io))

(define-public crate-yt_info-0.3.0 (c (n "yt_info") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0hsahfnpfawbjk75wjrf4w22andiy597ayk577vrsj01lkw9f7bc")))

(define-public crate-yt_info-0.3.1 (c (n "yt_info") (v "0.3.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "14ypxa68xa233ybcwkdf1xjwps3y5j9w4b6vgrzhqvi6kcrakl3n")))

(define-public crate-yt_info-0.3.2 (c (n "yt_info") (v "0.3.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "183sg4bjklr98wwjrr261kq3xvf0l42yw6i1p5iplld2swy84c23")))

