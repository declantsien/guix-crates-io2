(define-module (crates-io yt tr yttrium_std_kick) #:use-module (crates-io))

(define-public crate-yttrium_std_kick-0.1.0 (c (n "yttrium_std_kick") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "00jc8s25di7swrclid3jwqvdfsnv95vsrlr0l3sx492w81pcsnwk") (f (quote (("loader"))))))

