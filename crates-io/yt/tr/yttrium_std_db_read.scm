(define-module (crates-io yt tr yttrium_std_db_read) #:use-module (crates-io))

(define-public crate-yttrium_std_db_read-0.1.0 (c (n "yttrium_std_db_read") (v "0.1.0") (d (list (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "10m23kqg4zmhsm5ls1r50l8qjchpil6xlmfvfix8hqhd2vlfijhg") (f (quote (("loader"))))))

