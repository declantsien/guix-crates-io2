(define-module (crates-io yt tr yttrium_std_channel) #:use-module (crates-io))

(define-public crate-yttrium_std_channel-0.1.0 (c (n "yttrium_std_channel") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1v87af3fwhfzxxjz218b8rxs1kmfr77ci638h1ssri07s6mm75kn") (f (quote (("loader"))))))

