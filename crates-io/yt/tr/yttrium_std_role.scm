(define-module (crates-io yt tr yttrium_std_role) #:use-module (crates-io))

(define-public crate-yttrium_std_role-0.1.0 (c (n "yttrium_std_role") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1cj51vk21pq1cknppv8spsdkvy8kyjbrfcxy026aiy4lajd736h2") (f (quote (("loader"))))))

