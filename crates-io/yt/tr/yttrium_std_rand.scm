(define-module (crates-io yt tr yttrium_std_rand) #:use-module (crates-io))

(define-public crate-yttrium_std_rand-0.1.0 (c (n "yttrium_std_rand") (v "0.1.0") (d (list (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1qyfgvlba2y7liy7d6jz9n9jk41jjqz6hjx54iidnhmr8l64j0vm") (f (quote (("loader"))))))

