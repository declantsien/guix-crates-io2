(define-module (crates-io yt tr yttrium_std_selfdelete) #:use-module (crates-io))

(define-public crate-yttrium_std_selfdelete-0.1.0 (c (n "yttrium_std_selfdelete") (v "0.1.0") (d (list (d (n "humantime") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0xpl1psj7b9dx57lf02g201i4yi0f1dza6r8yqng4xfp0i7a8i4j") (f (quote (("loader"))))))

