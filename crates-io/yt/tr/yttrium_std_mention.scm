(define-module (crates-io yt tr yttrium_std_mention) #:use-module (crates-io))

(define-public crate-yttrium_std_mention-0.1.0 (c (n "yttrium_std_mention") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1j9a7hzpb9v58vydjfvkc0v4laz7idbhvwgz7c3ac6sdli3rdw16") (f (quote (("loader"))))))

