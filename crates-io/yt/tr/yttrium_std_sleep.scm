(define-module (crates-io yt tr yttrium_std_sleep) #:use-module (crates-io))

(define-public crate-yttrium_std_sleep-0.1.0 (c (n "yttrium_std_sleep") (v "0.1.0") (d (list (d (n "humantime") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "09ilaazl17d4m6adks3ysiahxrfnwj14qdslz16kra4k2bibvdxr") (f (quote (("loader"))))))

