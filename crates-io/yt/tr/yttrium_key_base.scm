(define-module (crates-io yt tr yttrium_key_base) #:use-module (crates-io))

(define-public crate-yttrium_key_base-0.1.0 (c (n "yttrium_key_base") (v "0.1.0") (d (list (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)))) (h "1x933bccpxqhvqlfrq2dbnm62dc45rba2k29s1x2w7g1bypgj60q")))

(define-public crate-yttrium_key_base-1.0.0 (c (n "yttrium_key_base") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serenity") (r "^0.10") (d #t) (k 0)))) (h "1la0xr66ab4j05gyrzwkblfxmn3758cbxd9r2mbdzdrrx43ky3y3")))

