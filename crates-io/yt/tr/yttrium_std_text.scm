(define-module (crates-io yt tr yttrium_std_text) #:use-module (crates-io))

(define-public crate-yttrium_std_text-0.1.0 (c (n "yttrium_std_text") (v "0.1.0") (d (list (d (n "glob") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0djrk14r3gr9snnqgf05185vn40k07hqiil9b8lihmj7vyys165r") (f (quote (("loader"))))))

