(define-module (crates-io yt tr yttrium_std_math) #:use-module (crates-io))

(define-public crate-yttrium_std_math-0.1.0 (c (n "yttrium_std_math") (v "0.1.0") (d (list (d (n "cxx") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cxx-build") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0rmlv98xx3x0aagyz2a2qrg4rsdwnhbs43i00b086pxl9kb86pix") (f (quote (("loader"))))))

(define-public crate-yttrium_std_math-1.0.0 (c (n "yttrium_std_math") (v "1.0.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "serenity") (r "^0.10") (d #t) (k 0)) (d (n "yttrium_key_base") (r "^1.0") (d #t) (k 0)))) (h "1ajh7s59aa2dmmrnb6xvr5zmh120sv57vs1w9dvva8wq6zckq3cn")))

