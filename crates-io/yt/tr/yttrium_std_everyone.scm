(define-module (crates-io yt tr yttrium_std_everyone) #:use-module (crates-io))

(define-public crate-yttrium_std_everyone-0.1.0 (c (n "yttrium_std_everyone") (v "0.1.0") (d (list (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0f70vbw41d3pm4i2w7dpnbis3llhkwm29shpx2zlbxsyzic448zq") (f (quote (("loader"))))))

