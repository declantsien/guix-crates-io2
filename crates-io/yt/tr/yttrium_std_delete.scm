(define-module (crates-io yt tr yttrium_std_delete) #:use-module (crates-io))

(define-public crate-yttrium_std_delete-0.1.0 (c (n "yttrium_std_delete") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "humantime") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1jja4vfyzcsx18zgjz6n11l2x6gdzgbj44kjp6m4gqjbl8bwsjlz") (f (quote (("loader"))))))

