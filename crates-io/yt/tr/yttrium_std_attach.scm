(define-module (crates-io yt tr yttrium_std_attach) #:use-module (crates-io))

(define-public crate-yttrium_std_attach-0.1.0 (c (n "yttrium_std_attach") (v "0.1.0") (d (list (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0lmi8kf5znp7qwfnq58rg8by5p31vm5fb1v3v5hbap66s9mpnk8y") (f (quote (("loader"))))))

