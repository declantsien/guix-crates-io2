(define-module (crates-io yt tr yttrium_std_joined) #:use-module (crates-io))

(define-public crate-yttrium_std_joined-0.1.0 (c (n "yttrium_std_joined") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1w5yrg3wc8lwby3rsiam07ayhvnyb6xnv2l82nh6j0yja2jf8vzm") (f (quote (("loader"))))))

