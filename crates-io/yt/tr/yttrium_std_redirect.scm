(define-module (crates-io yt tr yttrium_std_redirect) #:use-module (crates-io))

(define-public crate-yttrium_std_redirect-0.1.0 (c (n "yttrium_std_redirect") (v "0.1.0") (d (list (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "16x4vggpqhbw5w5ca2wzw75y0s80mbcd5abpby51ngkvz324zbvf") (f (quote (("loader"))))))

