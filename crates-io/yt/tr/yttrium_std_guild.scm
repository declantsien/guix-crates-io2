(define-module (crates-io yt tr yttrium_std_guild) #:use-module (crates-io))

(define-public crate-yttrium_std_guild-0.1.0 (c (n "yttrium_std_guild") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0hgv1r8bn58s2v0dahzps5j6l24s6fjcb7vjhldj9hddvvnizjdc") (f (quote (("loader"))))))

