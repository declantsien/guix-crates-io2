(define-module (crates-io yt tr yttrium_std_parameter) #:use-module (crates-io))

(define-public crate-yttrium_std_parameter-0.1.0 (c (n "yttrium_std_parameter") (v "0.1.0") (d (list (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "06a3zqx115j68aqkpvprfmkqzhvz6fjaqpj02xjf4wx037m7i6if") (f (quote (("loader"))))))

