(define-module (crates-io yt tr yttrium_std_pin) #:use-module (crates-io))

(define-public crate-yttrium_std_pin-0.1.0 (c (n "yttrium_std_pin") (v "0.1.0") (d (list (d (n "futures") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1hq153ingazc8a3mw4abxzpqmvlakgli86m5rwn10pp4lq68ghms") (f (quote (("loader"))))))

