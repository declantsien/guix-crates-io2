(define-module (crates-io yt tr yttrium_std_database_exists) #:use-module (crates-io))

(define-public crate-yttrium_std_database_exists-0.1.0 (c (n "yttrium_std_database_exists") (v "0.1.0") (d (list (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1ibpqw5cqdhcymvn30cpyxvjf7irirffaspq9cqi0mj0dzpffy5s") (f (quote (("loader"))))))

