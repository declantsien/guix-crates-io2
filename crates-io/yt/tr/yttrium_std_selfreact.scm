(define-module (crates-io yt tr yttrium_std_selfreact) #:use-module (crates-io))

(define-public crate-yttrium_std_selfreact-0.1.0 (c (n "yttrium_std_selfreact") (v "0.1.0") (d (list (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "serenity") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "yttrium_key_base") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1x3x3mmw867h8sikv8wa8h17v2k8r6a50knq4lqx36qrakzzg92z") (f (quote (("loader"))))))

