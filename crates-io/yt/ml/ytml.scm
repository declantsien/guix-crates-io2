(define-module (crates-io yt ml ytml) #:use-module (crates-io))

(define-public crate-ytml-0.1.0 (c (n "ytml") (v "0.1.0") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "078bj1ahwbl74n9v22rbsgdfa03zrq581skaw32781gz5d3yvzd1") (y #t)))

(define-public crate-ytml-0.1.1 (c (n "ytml") (v "0.1.1") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "0fmfv7ciqj6m02556i54rg8b9inw8w3qdd97nhskzyxhvx16vp4y") (y #t)))

(define-public crate-ytml-0.1.2 (c (n "ytml") (v "0.1.2") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "1j9cnd3azylql5l8zz425nnm7vpsj4zwi4vz40bzjlm4dajczx1i") (y #t)))

(define-public crate-ytml-0.1.3 (c (n "ytml") (v "0.1.3") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "0s2y1ni0sbd7l6hmbvgcghiqrp30v9gv3djfr6madg3b24aziv07") (f (quote (("file-handling")))) (y #t)))

(define-public crate-ytml-0.2.0 (c (n "ytml") (v "0.2.0") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "17xzbbyvxzjcy1rgawn0mbx1lnap64s6qs9c81mlir0vkxsf2rxw") (f (quote (("file-handling")))) (y #t)))

(define-public crate-ytml-0.2.1 (c (n "ytml") (v "0.2.1") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "09wmfqfjz0m8xxywcmhc55w7x0jmh08xja2mi2f8vgxpxxbrl951") (f (quote (("fs")))) (y #t)))

(define-public crate-ytml-0.2.2 (c (n "ytml") (v "0.2.2") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "1115k0cjrvh3dnqr5kpp6xfvxmggbjrk5893c9hv1i7vs3p01x1q") (f (quote (("fs")))) (y #t)))

(define-public crate-ytml-0.2.3 (c (n "ytml") (v "0.2.3") (d (list (d (n "pest") (r "^2.5.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.5") (d #t) (k 0)))) (h "1i0jga6l5h7khlp92cr0ry433nr15w40ssx719828bdnlw60si1n") (f (quote (("fs")))) (y #t)))

