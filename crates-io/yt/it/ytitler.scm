(define-module (crates-io yt it ytitler) #:use-module (crates-io))

(define-public crate-ytitler-0.0.1 (c (n "ytitler") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "02x8php9rpa00f7lk1nn63sakf0wjj1j0a9wzdkld26kylrskgsm")))

(define-public crate-ytitler-0.0.2 (c (n "ytitler") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "015cy121mq6qq9ryibg5ny11dsapnm6014a9ywdzyc17msrdg92m")))

(define-public crate-ytitler-0.0.3 (c (n "ytitler") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "17gnk7xnv8jml2yfp2vxysacanvvrzxarvy4grbjzdry8xl45jxi")))

(define-public crate-ytitler-0.0.4-pre0 (c (n "ytitler") (v "0.0.4-pre0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "02air6snz8lh93ll881xabl9mq1qg4dpvxcb56jbb3v2zv4d6hfp")))

(define-public crate-ytitler-0.0.4 (c (n "ytitler") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0ldhqzvmbvvv6mv85yms5pr60jp1hsqc7bbhb68i0hk946ydiz1v")))

(define-public crate-ytitler-0.1.0-pre0 (c (n "ytitler") (v "0.1.0-pre0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "01f36l6piir7cy362v4gcfnq07j587mwr246shd4g0n6bg0h28p2")))

(define-public crate-ytitler-0.1.0-pre1 (c (n "ytitler") (v "0.1.0-pre1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0pa8wyhrgf0fgf9jyc2wlgyvnnpdkls5l5ysgpbbsgklfwicz9j1")))

(define-public crate-ytitler-0.1.0 (c (n "ytitler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0biv1fhmd5f4k0zjdbxqhhxgn7x9g3bwk7x42sb60616bjxqcqwf")))

(define-public crate-ytitler-0.1.1 (c (n "ytitler") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0x4z56l833nvm4pz5j7r7lmwsbpsbb2pa1mms5fw0w858srfylav")))

