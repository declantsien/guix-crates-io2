(define-module (crates-io yt ra ytranscript) #:use-module (crates-io))

(define-public crate-ytranscript-0.1.0 (c (n "ytranscript") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xm6sn0bxck9m5klpl2d964nciza0vviqwk1ppyyigs89ilq4412")))

