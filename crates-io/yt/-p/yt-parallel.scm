(define-module (crates-io yt -p yt-parallel) #:use-module (crates-io))

(define-public crate-yt-parallel-0.5.10 (c (n "yt-parallel") (v "0.5.10") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "056vcpma7vz5c1gnlwkq6l2ki16ymdz45bl5q3yjch8y3krgagha")))

(define-public crate-yt-parallel-0.5.11 (c (n "yt-parallel") (v "0.5.11") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1nnvwqdm0c1hqfpzqlvgjz57z8b273x1dcxq429bh6dngnichgfl")))

(define-public crate-yt-parallel-0.5.22 (c (n "yt-parallel") (v "0.5.22") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "06fizfsvprhwvxshgywh1nkqr8hy0ccafx2a84zabp2bc610xxw6")))

