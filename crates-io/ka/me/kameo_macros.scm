(define-module (crates-io ka me kameo_macros) #:use-module (crates-io))

(define-public crate-kameo_macros-0.3.0 (c (n "kameo_macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gxzhk2p4xklap0zvrir9z68n1nmd0ckv0h1z10b6ipszxvmrc77")))

(define-public crate-kameo_macros-0.3.1 (c (n "kameo_macros") (v "0.3.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15zcq6x6gk6djxdh0l7fnnl7jc2igd07a4ar0hrdn458qb2fqj3z")))

(define-public crate-kameo_macros-0.3.2 (c (n "kameo_macros") (v "0.3.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11zj000viv884rp1skj79a90yjdxk5zvlck0djx7mj36h3jbmhd9")))

(define-public crate-kameo_macros-0.3.4 (c (n "kameo_macros") (v "0.3.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1iy8fphr7zxrs6rzzirw0rm8b6cm36z134pvfv26grffychlbsi7")))

(define-public crate-kameo_macros-0.4.0 (c (n "kameo_macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04x03m48lzdv5ki4dfkg1bysvgbjw9w7f5m5bzgks537562i0ld6")))

(define-public crate-kameo_macros-0.5.0 (c (n "kameo_macros") (v "0.5.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1inbvl2gh3q3i4vsnfsjpd7j19m4i5wv5pkmnfivq21w6aji4n48")))

(define-public crate-kameo_macros-0.6.0 (c (n "kameo_macros") (v "0.6.0") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ajyspjhv533l3wx8cja9zl4amln847qh9zw6pza2f6qsn4lcvng")))

(define-public crate-kameo_macros-0.7.0 (c (n "kameo_macros") (v "0.7.0") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xk4wawyq864f6ga0qpx79hnh39pr84v91bc716nlr1z8cka2bzn")))

(define-public crate-kameo_macros-0.8.0 (c (n "kameo_macros") (v "0.8.0") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pyp55v0qdpd0j1d98y2bcqibxl9r334rfj4lr2cxcr2rkffyf4c")))

(define-public crate-kameo_macros-0.8.1 (c (n "kameo_macros") (v "0.8.1") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0n2gk1mad3s5fr013akgnlkiknza6zycrigj3c2p5mmi7gz3b0xs")))

