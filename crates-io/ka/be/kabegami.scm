(define-module (crates-io ka be kabegami) #:use-module (crates-io))

(define-public crate-kabegami-0.1.0 (c (n "kabegami") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "infer") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wall") (r "^0.2.3") (d #t) (k 0)))) (h "046a884qm6px4nmp00wzs6nxbm6bk39zwzfvqnlbpnyr4gkhd8vp") (y #t)))

(define-public crate-kabegami-0.1.1 (c (n "kabegami") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "infer") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "wall") (r "^0.2.3") (d #t) (k 0)))) (h "1mvmphnxggzrqmcki6wdy05pfilid28g9x1wb03zrylrg59xbdv9") (y #t)))

(define-public crate-kabegami-0.2.0 (c (n "kabegami") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "waraq") (r "^0.1.0") (d #t) (k 0)))) (h "1yk21153w4yrk3w6iwanyjk4ixf62gr9yr0rnp899if8dcqjl180")))

(define-public crate-kabegami-0.2.1 (c (n "kabegami") (v "0.2.1") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "waraq") (r "^0.1.1") (d #t) (k 0)))) (h "0iskiq9dswi1wq7245icfnw2v6xcbwkafyq5hyr1hig0x3w41acc")))

(define-public crate-kabegami-0.2.2 (c (n "kabegami") (v "0.2.2") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "waraq") (r "^0.1.1") (d #t) (k 0)))) (h "1xbj8shl2nwl5sjcrnwx3lfvggjxhskilya57rh6h81d6xihx062")))

(define-public crate-kabegami-0.3.0 (c (n "kabegami") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png"))) (k 0)) (d (n "infer") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.35") (d #t) (k 0)) (d (n "waraq") (r "^0.2.1") (d #t) (k 0)))) (h "0xwb94b49srp0mnmzxmrkg9yf9gjj1zsjl7gqw9vsmkv3z2anwi6")))

