(define-module (crates-io ka tn katniss-pb2arrow) #:use-module (crates-io))

(define-public crate-katniss-pb2arrow-0.0.1 (c (n "katniss-pb2arrow") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "arrow-array") (r "^33.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^33.0.0") (d #t) (k 0)) (d (n "prost-reflect") (r "^0.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0mlqgwa5gqwgm0bkng7dv0jfbjxibg8xd1kdd1fm31nv50y5sxxg")))

(define-public crate-katniss-pb2arrow-0.0.2 (c (n "katniss-pb2arrow") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "arrow-array") (r "^37.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^37.0.0") (d #t) (k 0)) (d (n "prost-reflect") (r "=0.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "02yqy2fhj4y7whb7jpwqpsm98bn5mxnz3vf9rfvm61jcw818hl60")))

(define-public crate-katniss-pb2arrow-0.0.3 (c (n "katniss-pb2arrow") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "arrow-array") (r "^37.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^37.0.0") (d #t) (k 0)) (d (n "prost-reflect") (r "=0.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "01dra512mld34mg3ngayn0fp4xfscm78i74c63h6wwzi4pys5d77")))

