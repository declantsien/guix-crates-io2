(define-module (crates-io ka sh kashida) #:use-module (crates-io))

(define-public crate-kashida-0.0.1 (c (n "kashida") (v "0.0.1") (d (list (d (n "constcat") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)))) (h "0xdv9yhj17h5r0v6yid9q8813cia14k09736f30bf1m55qbjxww5")))

(define-public crate-kashida-0.0.2 (c (n "kashida") (v "0.0.2") (d (list (d (n "constcat") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)))) (h "04a6swxjhnaxjdv8xa17f1psfjv0l5ibyr01damqq3ibwqcinvny")))

(define-public crate-kashida-0.0.3 (c (n "kashida") (v "0.0.3") (d (list (d (n "constcat") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)))) (h "0qzbgw8hlxmdswjrmp5rj5xi5f04h139z6qgp3499grw7nvf7dqv")))

(define-public crate-kashida-0.0.4 (c (n "kashida") (v "0.0.4") (d (list (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)) (d (n "unicode-joining-type") (r "^0.7.0") (d #t) (k 0)))) (h "1q2pllvmyyv6xwcbylzjc8y8gc2jb2zamg4z2g9a9vs8acqdvhl7")))

(define-public crate-kashida-0.0.5 (c (n "kashida") (v "0.0.5") (d (list (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)) (d (n "unicode-joining-type") (r "^0.7.0") (d #t) (k 0)))) (h "12s6q3qp3sl08p5jdm4zh7wfkmshkd9ikl00cmb966qkmb4qmgm6")))

(define-public crate-kashida-0.0.6 (c (n "kashida") (v "0.0.6") (d (list (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (k 0)) (d (n "unicode-joining-type") (r "^0.7.0") (d #t) (k 0)))) (h "1flvib77prr66rvsgyzb3mmsbc0wprhw51zyznig7q3989pwv3fz")))

(define-public crate-kashida-0.0.7 (c (n "kashida") (v "0.0.7") (d (list (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "icu_segmenter") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (k 0)) (d (n "unicode-joining-type") (r "^0.7.0") (d #t) (k 0)))) (h "11q37d4qzlgx9nbg70z0hdj44qjgsfxxdabvrrxf8w4qi98dqclm")))

