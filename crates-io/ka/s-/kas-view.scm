(define-module (crates-io ka s- kas-view) #:use-module (crates-io))

(define-public crate-kas-view-0.11.0 (c (n "kas-view") (v "0.11.0") (d (list (d (n "kas") (r "^0.11.0") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.11.0") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0riw90y0mjn9rpc2hczxb4bbj08vslmil07bydm3ly49aw4kvzl1")))

(define-public crate-kas-view-0.12.0 (c (n "kas-view") (v "0.12.0") (d (list (d (n "kas") (r "^0.12.0") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.12.0") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "041l8bvz4m3vzgy9h8372gls8mn6yxikf6017ar0ib2pr9rz0arl")))

(define-public crate-kas-view-0.13.0 (c (n "kas-view") (v "0.13.0") (d (list (d (n "kas") (r "^0.13.0") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.13.0") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maybe-owned") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02n9d1rdgjnga2402ad5yq9zm9r1whjcr3nnna7dss136wlk51nq")))

(define-public crate-kas-view-0.14.0-alpha (c (n "kas-view") (v "0.14.0-alpha") (d (list (d (n "kas") (r "^0.14.0-alpha") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.14.0-alpha") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bgfs2hfi6433zxb2bb0yivb02m0rvrzylwwqa4kxvw3vzvzx7s9")))

(define-public crate-kas-view-0.14.1 (c (n "kas-view") (v "0.14.1") (d (list (d (n "kas") (r "^0.14.1") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.14.1") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dyhk2clb7ivx21inhygsp1kra6v938qm0n8n0dkx9lgx73cwgdx")))

(define-public crate-kas-view-0.14.2 (c (n "kas-view") (v "0.14.2") (d (list (d (n "kas") (r "^0.14.1") (d #t) (k 0) (p "kas-core")) (d (n "kas-widgets") (r "^0.14.1") (d #t) (k 0)) (d (n "linear-map") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c7yj4icq79zx8kynhj1nrlvc3cvyrc7689aqj0nv8zpw4gjvszc")))

