(define-module (crates-io ka s- kas-dylib) #:use-module (crates-io))

(define-public crate-kas-dylib-0.10.0 (c (n "kas-dylib") (v "0.10.0") (d (list (d (n "kas-core") (r "^0.10.0") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "kas-theme") (r "^0.10.0") (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.10.0") (d #t) (k 0)) (d (n "kas-widgets") (r "^0.10.0") (d #t) (k 0)))) (h "0cs5ffy1jlva2rcq320rm46yak07safy4q5g0qp5x84wqc8qnl83")))

(define-public crate-kas-dylib-0.11.0 (c (n "kas-dylib") (v "0.11.0") (d (list (d (n "kas-core") (r "^0.11.0") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "kas-theme") (r "^0.11.0") (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.11.0") (d #t) (k 0)) (d (n "kas-widgets") (r "^0.11.0") (d #t) (k 0)))) (h "16yn10v6awz47pshwshph0fngnkz1n0cq14h91djzzy5jd5k7ndx") (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

(define-public crate-kas-dylib-0.12.0 (c (n "kas-dylib") (v "0.12.0") (d (list (d (n "kas-core") (r "^0.12.0") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "kas-theme") (r "^0.12.0") (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.12.0") (d #t) (k 0)) (d (n "kas-widgets") (r "^0.12.0") (d #t) (k 0)))) (h "1v7hmydv1w7yy9jhm29z922kii40d9mpnv0ps262lgv1ys971k20") (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

(define-public crate-kas-dylib-0.13.0 (c (n "kas-dylib") (v "0.13.0") (d (list (d (n "kas-core") (r "^0.13.0") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.13.0") (d #t) (k 0)) (d (n "kas-widgets") (r "^0.13.0") (d #t) (k 0)))) (h "0jfagzs8nknp4jmyi3cbhiqfy67j2m0w3yaijk8xq6ln7wk414ax") (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

(define-public crate-kas-dylib-0.14.0-alpha (c (n "kas-dylib") (v "0.14.0-alpha") (d (list (d (n "kas-core") (r "^0.14.0-alpha") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.14.0-alpha") (o #t) (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.14.0-alpha") (k 0)) (d (n "kas-widgets") (r "^0.14.0-alpha") (d #t) (k 0)))) (h "14189a05h54ah1kc5w23c53mp1issn0m4wlvy3vzvxdzqsvbj1a2") (f (quote (("raster" "kas-wgpu/raster") ("default" "raster")))) (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

(define-public crate-kas-dylib-0.14.1 (c (n "kas-dylib") (v "0.14.1") (d (list (d (n "kas-core") (r "^0.14.1") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.14.1") (k 0)) (d (n "kas-widgets") (r "^0.14.1") (d #t) (k 0)))) (h "115vj0b5s7ng359ayk78dxgaq61rqajmqvck82w6cbx7d11m5258") (f (quote (("raster" "kas-wgpu/raster") ("default" "raster")))) (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

(define-public crate-kas-dylib-0.14.2 (c (n "kas-dylib") (v "0.14.2") (d (list (d (n "kas-core") (r "^0.14.1") (d #t) (k 0)) (d (n "kas-resvg") (r "^0.14.2") (o #t) (d #t) (k 0)) (d (n "kas-wgpu") (r "^0.14.1") (k 0)) (d (n "kas-widgets") (r "^0.14.2") (d #t) (k 0)))) (h "087mg1sxm472i0sz25ws8nn3zjlw5a8pcx3xgsxy8kfrnnbp2icx") (f (quote (("raster" "kas-wgpu/raster") ("default" "raster")))) (s 2) (e (quote (("resvg" "dep:kas-resvg"))))))

