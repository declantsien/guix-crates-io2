(define-module (crates-io ka s- kas-gtk) #:use-module (crates-io))

(define-public crate-kas-gtk-0.0.1 (c (n "kas-gtk") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gdk") (r "^0.9") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "glib") (r "^0.6") (d #t) (k 0)) (d (n "gtk") (r "^0.5") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.7") (d #t) (k 0)) (d (n "kas") (r "^0.0.1") (d #t) (k 0)))) (h "1ljy333jcc5yy114klxf4jn72cmqpqlj3r0sh3z419nh7ri96ds4") (f (quote (("layout" "kas/layout"))))))

(define-public crate-kas-gtk-0.0.2 (c (n "kas-gtk") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gdk") (r "^0.9") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "glib") (r "^0.6") (d #t) (k 0)) (d (n "gtk") (r "^0.5") (f (quote ("v3_12"))) (d #t) (k 0)) (d (n "gtk-sys") (r "^0.7") (d #t) (k 0)) (d (n "kas") (r "^0.0.2") (d #t) (k 0)))) (h "1315j1g0gzcd7lk8msz42frlbxj1ksi3v3ywd18wrapvn00znglv") (f (quote (("layout" "kas/layout"))))))

