(define-module (crates-io ka s- kas-macros) #:use-module (crates-io))

(define-public crate-kas-macros-0.0.1 (c (n "kas-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0hzmgxvbzx0xh95c9k20pz01lpb59jq8k8rmmh4c6xq8dg41r71i") (f (quote (("cassowary"))))))

(define-public crate-kas-macros-0.0.2 (c (n "kas-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wrqp2f3l12rrpjdpvcvij1i27g813b30jpirxqb3a0a0xczlp2m") (f (quote (("cassowary"))))))

(define-public crate-kas-macros-0.1.0-pre.1 (c (n "kas-macros") (v "0.1.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00azhpv4rbhp1mlsylqq9b41rpvxxkqkvyy2pvr24m9q9fl0cgny")))

(define-public crate-kas-macros-0.1.0 (c (n "kas-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0viz19cxi296f7bg6d79bppiy3ghf1afdv6x77821hzazj4vwfch")))

(define-public crate-kas-macros-0.2.0 (c (n "kas-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ix6ip3vmn6wza5y2pi71q5k9p3b6nr0z5n2ifdx1z62gjh3mqd5")))

(define-public crate-kas-macros-0.3.0 (c (n "kas-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qdjqf5gdmqm1ixjf02s1wykk59mnnkn5gl6mwhw85knss1z0ylw")))

(define-public crate-kas-macros-0.4.0 (c (n "kas-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0k2ppkr85adarmh2p5a78v5f8m8g9q23gvw9ig9szy419kp2mwg7")))

(define-public crate-kas-macros-0.5.0 (c (n "kas-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0v8hhv4vpkzrfnhlkks31fa9wfskch8yl4d85jfxfg7lsk6n2h2a")))

(define-public crate-kas-macros-0.6.0 (c (n "kas-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.14, <2.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r ">=0.9.0, <0.10.0") (d #t) (k 1)))) (h "0a4a48vh7j1fnpjv9gsc2z66rjs8bkc1py9xy1xahnwrqhcp9wha")))

(define-public crate-kas-macros-0.7.0 (c (n "kas-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1ip2sxgih7748b7dylw74pvl9y8cznrbv082gmznc1qgf5kw95xw")))

(define-public crate-kas-macros-0.8.0 (c (n "kas-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1crwn5b4y8crwclkmp8j52shp40qq17ql1fi04zykzm97c9n8p9s")))

(define-public crate-kas-macros-0.9.0 (c (n "kas-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1lpaka23b8n7ah4inpnych2zhak9f30vk629m7dly511c9cgx88f")))

(define-public crate-kas-macros-0.9.1 (c (n "kas-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1pmmdd3dahj8ffw43rkyd0dj9hmvf5sm49y5yxin24ylidwzz61n") (f (quote (("log"))))))

(define-public crate-kas-macros-0.10.0 (c (n "kas-macros") (v "0.10.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "04yviihk70fqfv8ggwvkyrda15p5kiv8mc591wi1hl4zn96yqidd") (f (quote (("log"))))))

(define-public crate-kas-macros-0.10.1 (c (n "kas-macros") (v "0.10.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1r8mzw8vksqsdw2wd73g1ahzrg7js17lnkp9ar2kff4a1khbh5g4") (f (quote (("log"))))))

(define-public crate-kas-macros-0.11.0 (c (n "kas-macros") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "impl-tools-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1dsh1ykibxqa0j7x2aqvi6zjwb312ps3nrnv22gnnqryl6f5qhqq") (f (quote (("log"))))))

(define-public crate-kas-macros-0.12.0 (c (n "kas-macros") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "impl-tools-lib") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1l1f25pg3r9q9qfddjggl6b83iq6i4mdpjd4v8ngbry467immwl9") (f (quote (("log"))))))

(define-public crate-kas-macros-0.13.0 (c (n "kas-macros") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.3.1") (d #t) (k 0)) (d (n "impl-tools-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0a8dlyn4i8bwz60iafh677q4aarh9f9dk1ncj6xh15lsy5g2acay") (f (quote (("log"))))))

(define-public crate-kas-macros-0.14.0-alpha (c (n "kas-macros") (v "0.14.0-alpha") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "impl-tools-lib") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0f11x6xf8ski6r7fypghy8idgnifbh7h8gqsj7mmqcpylhapragp") (f (quote (("log"))))))

(define-public crate-kas-macros-0.14.1 (c (n "kas-macros") (v "0.14.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "impl-tools-lib") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0gk9whb2fsfkvqfgg8x8c128lg2rnzdfbybfgk7bb5c4yis25ps9") (f (quote (("log"))))))

