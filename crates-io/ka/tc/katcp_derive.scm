(define-module (crates-io ka tc katcp_derive) #:use-module (crates-io))

(define-public crate-katcp_derive-0.1.0 (c (n "katcp_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p5zyzddgmx2s3jf8ajhf2pz9xi6j4f73c7p7f6wqqd31cqidx4r")))

(define-public crate-katcp_derive-0.1.1 (c (n "katcp_derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j4v8q3lfwa1wiv3mnv6igrjxqy58p52hsprp6lagkpzk32v9vha")))

(define-public crate-katcp_derive-0.1.2 (c (n "katcp_derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "100jydf1dnwyif08nj9f27h84v5w94vqc1029n3lh3hdz1h3c6bb")))

(define-public crate-katcp_derive-0.1.3 (c (n "katcp_derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hfqa0qv3c7j4fasm1zk8xrgm5hv9wcbllq24p8639wgwq4adb61") (r "1.57.0")))

(define-public crate-katcp_derive-0.1.5 (c (n "katcp_derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0drvz9sr5jnc118igqncyb227b18gd4fwi9gqszhf3sivcr2lakb") (r "1.57.0")))

(define-public crate-katcp_derive-0.1.6 (c (n "katcp_derive") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1knj5xdn2q7ah4fa94mrqq673zbrkifx0fpk43iq3fm4kyggyz2b") (r "1.57.0")))

(define-public crate-katcp_derive-0.1.7 (c (n "katcp_derive") (v "0.1.7") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pzws9vsk0w6rnibnaiahw37c3mx6m975hs77y7m1wb9xcn4xk6b") (r "1.57.0")))

