(define-module (crates-io ka tc katcp) #:use-module (crates-io))

(define-public crate-katcp-0.1.0 (c (n "katcp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "0pnxh0j2xw5l3xv9xh9lsrmmgmvsb2sm5cchy4gsggcc5c419b6z") (r "1.59.0")))

(define-public crate-katcp-0.1.1 (c (n "katcp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1z1qbhm7lw59rsbds5qk2pq8gg7shq9l0bfp6r4i6d3ydgmqfr2s") (r "1.59.0")))

(define-public crate-katcp-0.1.2 (c (n "katcp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1ajdb0qshcc6f5sbrxamqvdsrxfqi08k4hczkzvy4f39fw8jn0g7") (r "1.59.0")))

(define-public crate-katcp-0.1.3 (c (n "katcp") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1rrqn2faqra5fhf85zvg78fzbb38knbhs8xm9g2q9civn12fgmwd") (r "1.57.0")))

(define-public crate-katcp-0.1.4 (c (n "katcp") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "043pz11hqqdj6igj7xkw1lx0zj9siwgzyadgnjhcpcw35vxx0vd1") (r "1.57.0")))

(define-public crate-katcp-0.1.5 (c (n "katcp") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1p2wd2pk88sfm8anhmdgfxnz1lsvs9vp90bjglmd8jajj9fga1pp") (r "1.57.0")))

(define-public crate-katcp-0.1.6 (c (n "katcp") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "1qix877a7wibvnm7n68ps4vgv6xbcgsklix4fhc2k4wpz6csksl2") (r "1.57.0")))

(define-public crate-katcp-0.1.7 (c (n "katcp") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "katcp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "0dii80s1zxvrxmp81zji34inczp9dhj83x4i2djvpwzdmf020vgw") (r "1.57.0")))

