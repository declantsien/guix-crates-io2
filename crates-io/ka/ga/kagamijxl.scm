(define-module (crates-io ka ga kagamijxl) #:use-module (crates-io))

(define-public crate-kagamijxl-0.1.0 (c (n "kagamijxl") (v "0.1.0") (d (list (d (n "libjxl-sys") (r "^0.2.1") (d #t) (k 0)))) (h "19ni02rnf1rq6g9prgvy016w5qn0phh3419zfcj3i0ff52yni941")))

(define-public crate-kagamijxl-0.1.1 (c (n "kagamijxl") (v "0.1.1") (d (list (d (n "libjxl-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0azb0qv30mw9jzb96vr2ws7k21sw0b23qrpmbxv2iwd72ffb4gki")))

(define-public crate-kagamijxl-0.2.0 (c (n "kagamijxl") (v "0.2.0") (d (list (d (n "libjxl-sys") (r "^0.3.0") (d #t) (k 0)))) (h "08852p1rw7im2m4zxyqyvib7zfs4dmr1595xqqigrhs5vld3p9yb")))

(define-public crate-kagamijxl-0.2.1 (c (n "kagamijxl") (v "0.2.1") (d (list (d (n "libjxl-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0wx68g9z5ybfbnfkgyjxzxh3fbv3gwfm4vrgsr96ms64gkxf2yla")))

(define-public crate-kagamijxl-0.2.2 (c (n "kagamijxl") (v "0.2.2") (d (list (d (n "libjxl-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0sspdfa6cb3rqk15rx6gjg1afk9b79mxrbirg9n9r9i2386f1yq2")))

(define-public crate-kagamijxl-0.2.3 (c (n "kagamijxl") (v "0.2.3") (d (list (d (n "libjxl-sys") (r "^0.3.3") (d #t) (k 0)))) (h "16j4y3yzmli0091xl1jm2vllrndbpxcrlq1y1ksqkx9vfl0qccyf")))

(define-public crate-kagamijxl-0.2.4 (c (n "kagamijxl") (v "0.2.4") (d (list (d (n "libjxl-sys") (r "^0.3.4") (d #t) (k 0)))) (h "19f1a0sn78r8gp88mry1pjd0r9zzza6djckw4bzysqj4mnr6jnr8")))

(define-public crate-kagamijxl-0.2.5 (c (n "kagamijxl") (v "0.2.5") (d (list (d (n "libjxl-sys") (r "^0.3.5") (d #t) (k 0)))) (h "1492n4vpxgmnkfa8qp737bds6awzbi5dcyxglbzdca2pbmg0s6f3")))

(define-public crate-kagamijxl-0.2.6 (c (n "kagamijxl") (v "0.2.6") (d (list (d (n "libjxl-sys") (r "^0.3.5") (d #t) (k 0)))) (h "0nkc5q2j1fbszlb3mxy8zd96zjgy8vqfqjs6l5xywmfksdz2vg3k")))

(define-public crate-kagamijxl-0.2.7 (c (n "kagamijxl") (v "0.2.7") (d (list (d (n "libjxl-sys") (r "^0.3.5") (d #t) (k 0)))) (h "1xif8m6c6w3z8jwy3671gq8rnv7iqaaa2239fgrba993rb19vh9x")))

(define-public crate-kagamijxl-0.2.8 (c (n "kagamijxl") (v "0.2.8") (d (list (d (n "libjxl-sys") (r "^0.3.5") (d #t) (k 0)))) (h "101hiyk3g5g2kiqr8bzsh5h8axj7fg9pxd7wwzbi7f45jv7bd3vd")))

(define-public crate-kagamijxl-0.2.9 (c (n "kagamijxl") (v "0.2.9") (d (list (d (n "libjxl-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1dpsfrqqshgp8frmhwr1xa3bhncpprs88bpm4jpmvcb18sdnd3h1")))

(define-public crate-kagamijxl-0.3.0 (c (n "kagamijxl") (v "0.3.0") (d (list (d (n "libjxl-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0wkv06nmw7zhacx4bn95z407zvxzc9h1y8p1grw60q12cq5w2psb")))

(define-public crate-kagamijxl-0.3.1 (c (n "kagamijxl") (v "0.3.1") (d (list (d (n "libjxl-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1r7vm0950f6p9a8vcdmmgjv9pv73by0khffjixs2ra8d8lb12q8b")))

(define-public crate-kagamijxl-0.3.2 (c (n "kagamijxl") (v "0.3.2") (d (list (d (n "libjxl-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1x737vrpqy167a6ziypzbhjrrc402wpmb9jx3gpizl9n19a1rz13")))

(define-public crate-kagamijxl-0.3.3 (c (n "kagamijxl") (v "0.3.3") (d (list (d (n "libjxl-sys") (r "^0.7.1") (d #t) (k 0)))) (h "1yn8y4ri5499s2zq17m20l60nyaaxys7mx4hl3m3xwcdahamymwl")))

