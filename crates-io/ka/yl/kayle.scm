(define-module (crates-io ka yl kayle) #:use-module (crates-io))

(define-public crate-kayle-0.0.1 (c (n "kayle") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1bpjp010771i0fn7lr3ni5rvxs99ly9afxqn9ib16cw9gqdyp7lx") (f (quote (("default" "console_error_panic_hook"))))))

