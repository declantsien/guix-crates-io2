(define-module (crates-io ka yl kayle_cli) #:use-module (crates-io))

(define-public crate-kayle_cli-0.0.1 (c (n "kayle_cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ily5cynqywxk5jznz2gjy66l58h7y29m47n6b5yvkfiwnrlx5kq")))

(define-public crate-kayle_cli-0.0.2 (c (n "kayle_cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qf205dyi9ni5rgn6v2zag38527asfgsp8jhfillshzk10chqvmj")))

(define-public crate-kayle_cli-0.0.3 (c (n "kayle_cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "066npxcw6q76rjsnic0r30gkzb14l45i05w2lh5mp3v6a79qb4af")))

(define-public crate-kayle_cli-0.0.4 (c (n "kayle_cli") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0fq6zll46mi6fn0ga2rxxp389pb9z8qfcbcyq3wr8yinp1zwqfpa")))

(define-public crate-kayle_cli-0.0.5 (c (n "kayle_cli") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "03vrp8y22qjq9bmjcr350564y6h8sf1p6r0jw6hf22fbp4l7rx83")))

