(define-module (crates-io ka yl kayle_innate) #:use-module (crates-io))

(define-public crate-kayle_innate-0.0.1 (c (n "kayle_innate") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "05yqzy4sfwczhmysh874qhhhwc7r7pnmq3v0x5xrhgvspmc0il3b") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-kayle_innate-0.0.5 (c (n "kayle_innate") (v "0.0.5") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "13zcvcrnspm5xyyab1jj823sz6dy922cdb18g0vzv8kir35qiyw0") (f (quote (("default" "console_error_panic_hook"))))))

