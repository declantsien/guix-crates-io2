(define-module (crates-io ka sm kasm) #:use-module (crates-io))

(define-public crate-kasm-0.1.0 (c (n "kasm") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1hd7drbhg1s2jcsvk5d7wzv5qp7dal7d3slrkv0cszdgxqwb8s97")))

(define-public crate-kasm-0.9.12 (c (n "kasm") (v "0.9.12") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^2.1.0") (d #t) (k 0)))) (h "0lqjldi7p7q05rl83i1wifa1zh3wj0ykq4i4rlxs9c8jvxcnqsqx")))

(define-public crate-kasm-0.11.0 (c (n "kasm") (v "0.11.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.12") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1002jq4hy1s5j4g2p3dplc9lb6hk633yxnbgnfbd5xgc56a7c286")))

(define-public crate-kasm-1.0.0 (c (n "kasm") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "kerbalobjects") (r "^3.1.13") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0rp32zl6wvlqlb3qgbdsvxgl7yqj3mc0cp9jz8nfiv233d4a9376")))

(define-public crate-kasm-2.0.2 (c (n "kasm") (v "2.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1qckw3s3fvzjz2hsl4nf3532im8gfbpaibk1s023gghw40hsqpym")))

(define-public crate-kasm-2.0.3 (c (n "kasm") (v "2.0.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kerbalobjects") (r "^4.0.2") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0nh8r2n7l6yj71akszzp5zafcl8ssin53lv9p7rdb9gbqw63v2z8")))

