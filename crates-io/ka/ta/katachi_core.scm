(define-module (crates-io ka ta katachi_core) #:use-module (crates-io))

(define-public crate-katachi_core-0.0.0 (c (n "katachi_core") (v "0.0.0") (d (list (d (n "easy_from") (r "^0.0.0") (d #t) (k 0)) (d (n "lasso") (r "^0.7.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1xsmlbhdvyiscwx1kmpnywd0lnahab211nzwzxmkxq5s2jck704y")))

