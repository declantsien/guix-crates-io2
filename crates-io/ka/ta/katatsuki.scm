(define-module (crates-io ka ta katatsuki) #:use-module (crates-io))

(define-public crate-katatsuki-0.1.0 (c (n "katatsuki") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)))) (h "0ppz3mh8q6nyrcdx1xhb3g48akl1h904sxs7h4r7qfhx3g61aman")))

(define-public crate-katatsuki-0.1.1 (c (n "katatsuki") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)))) (h "115p24qa8156pxxfq13qghfd5zlcfsm84vixiqlm2v1xs0iyyvzn")))

(define-public crate-katatsuki-0.1.2 (c (n "katatsuki") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)))) (h "1dfy91ws2y4igamyj6vsncr8b19f923avkd5zlyiv3milvf0xfb5")))

(define-public crate-katatsuki-0.1.3 (c (n "katatsuki") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)))) (h "0mi4qabzmqnipjnq5n2czyg7aqas99v3p8gdxlm8s2zhwjf0z0dn")))

(define-public crate-katatsuki-0.1.4 (c (n "katatsuki") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0fvj350mf8gf1n8nhs06wxxlz5hwinksyk28gks0rv31hh6zwrvb")))

(define-public crate-katatsuki-0.1.5 (c (n "katatsuki") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "14cwnfhln9fg2ljcp5hgwdgi9n3px88x24qpgxp5v0iyz3jm9v3d")))

(define-public crate-katatsuki-0.1.6 (c (n "katatsuki") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "07p1lfi4rcraql75q600191ag8j0v06mw7fafdamvzzd807qmmd2")))

(define-public crate-katatsuki-1.0.3 (c (n "katatsuki") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "13iwfnbkiv1nkpxj9mycispbcshly0hwgmnrf6g5nrqa8k3k00fl") (y #t)))

(define-public crate-katatsuki-1.0.4 (c (n "katatsuki") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "04xnv6n0psmaixqscixjqaa0kjf3h9707mq2l1s6pnnqy1k0hw6z")))

(define-public crate-katatsuki-1.0.5 (c (n "katatsuki") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1r444xjjy0pcxpxk61f35m2084k1n3mszx9z0v5vlinmahr5fwd8")))

(define-public crate-katatsuki-1.0.7 (c (n "katatsuki") (v "1.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1jp4zwymxdi56k40ldk5lr3viianwymz2db492h6606a4rinhqv8")))

(define-public crate-katatsuki-1.0.8 (c (n "katatsuki") (v "1.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "10qhy23g57cy4xlw0c6am1fkkcc6j5sryi68ysc3inh4pbs503bg")))

(define-public crate-katatsuki-1.0.9 (c (n "katatsuki") (v "1.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0l42s729nap8ca3sqrmn1mjf9cdvcjpricipgc42rr2yci16q86x")))

(define-public crate-katatsuki-1.0.10 (c (n "katatsuki") (v "1.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "imagesize") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "01byg0zvihkn57pmlh8kcvqhxg2z1wghcm6ddasd07acjam66l5h")))

(define-public crate-katatsuki-1.0.11 (c (n "katatsuki") (v "1.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "imagesize") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libkatatsuki-sys") (r "^1.0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hfjfsp0clmqhn5crqw02ws25mdp62xswijykpjw2qzhnjyv4zid")))

