(define-module (crates-io ka ta katana) #:use-module (crates-io))

(define-public crate-katana-1.0.0 (c (n "katana") (v "1.0.0") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "0a6f0hadmfqjqg5km65jpqly3w07vj4c7gbbxs44priqi31mg9g5")))

(define-public crate-katana-1.0.1 (c (n "katana") (v "1.0.1") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "00hynmrbbgrspia7kk2zsqnwkwvawq02vww22n55nib5z3k0wgkm")))

(define-public crate-katana-1.0.2 (c (n "katana") (v "1.0.2") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "1i6q68ydr5mbsldfbz5ayjhabaijhz8h195aqwkilxq0x7zzm0sk")))

