(define-module (crates-io ka ta katalyst_macros) #:use-module (crates-io))

(define-public crate-katalyst_macros-0.2.0 (c (n "katalyst_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqqzkph4188vgdb9nwcnzr9z16pxyy7ng8vfkd7iffq64grkald")))

