(define-module (crates-io ka ta kata_machine) #:use-module (crates-io))

(define-public crate-kata_machine-0.1.0 (c (n "kata_machine") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1gxpv5ps047s2bjgvkfxsiacz9szjd0fyhrk7kijj193vz7yz6j2")))

(define-public crate-kata_machine-0.1.1 (c (n "kata_machine") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1n0ibdkd0v1z18x81x892qsch1im1cl8l4hjqgicxszffrkxswvj")))

(define-public crate-kata_machine-0.1.2 (c (n "kata_machine") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "00wx7nf4ryg1b7r274qhd7zgjlbff112jgg1dvf616ligks9x4vd")))

(define-public crate-kata_machine-0.1.3 (c (n "kata_machine") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1rmdqwaj0q1i2gxp3v7p24hid1jcm19k1x27rfh2ca290hpnmq4a")))

(define-public crate-kata_machine-0.1.4 (c (n "kata_machine") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1xq773kxzglscqjs1sbmqcd6a5l4gh2an9ykh74xzv41b1z5dqi3")))

(define-public crate-kata_machine-0.1.5 (c (n "kata_machine") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1n1dmc2gyl8nah9grq3n1sxjhhfhhmf7n92f79fikhzabzj7447h")))

(define-public crate-kata_machine-0.1.6 (c (n "kata_machine") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0w7jv0d18w7zndcy9092qxmadsmcf6p5gk3jpnclqh1dphhj23vn")))

(define-public crate-kata_machine-0.1.7 (c (n "kata_machine") (v "0.1.7") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1l84xzilc5x219zhkh8a126ncki6di58zq884w0ksac9xilyym5s")))

(define-public crate-kata_machine-0.1.8 (c (n "kata_machine") (v "0.1.8") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0xpvfi64r5g6d2qzxq5x3lkp27awq992254k2524jcz73vjqw1dq")))

