(define-module (crates-io ka ta katabastird) #:use-module (crates-io))

(define-public crate-katabastird-1.6.0 (c (n "katabastird") (v "1.6.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "suggestions" "usage"))) (d #t) (k 0)) (d (n "eframe") (r "^0.19") (f (quote ("wgpu"))) (d #t) (k 0)) (d (n "egui") (r "^0.19") (f (quote ("default"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.19") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 0)))) (h "0g8vz81q21nvdqnbkvgfdy8mvxk3l3vf9yhi3c9bxi0qxg2b0xbd") (r "1.61")))

(define-public crate-katabastird-1.6.1 (c (n "katabastird") (v "1.6.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "suggestions" "usage"))) (d #t) (k 0)) (d (n "eframe") (r "^0.19") (f (quote ("wgpu"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.19") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (d #t) (k 0)))) (h "0h8vx271hbrz1i6yixr3bg0jvp9carq579praclmmnmmk2bg26kr") (r "1.61")))

