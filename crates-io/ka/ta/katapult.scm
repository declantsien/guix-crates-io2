(define-module (crates-io ka ta katapult) #:use-module (crates-io))

(define-public crate-katapult-0.0.0 (c (n "katapult") (v "0.0.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("string"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0ms9hzlz9323wka9lz6sm1svz7sw464ahybwa8jfhf89b411qwfc")))

