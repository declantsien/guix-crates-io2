(define-module (crates-io ka ld kaldera) #:use-module (crates-io))

(define-public crate-kaldera-0.1.0 (c (n "kaldera") (v "0.1.0") (d (list (d (n "gltf") (r "^0.15.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "10kigxsaiwqpnkqcvx10srdhfs5yyg1n4zs4vf0bsql9qdfkhmia") (f (quote (("with-nalgebra" "nalgebra-glm") ("with-gltf" "gltf") ("default" "with-nalgebra" "with-gltf"))))))

