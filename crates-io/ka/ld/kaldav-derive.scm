(define-module (crates-io ka ld kaldav-derive) #:use-module (crates-io))

(define-public crate-kaldav-derive-0.1.0 (c (n "kaldav-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zwhfk36k2p8qdifw8dd8zq1bq1fz2c5qwxi04ms33vpcjc338r1")))

(define-public crate-kaldav-derive-0.2.0 (c (n "kaldav-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "125a9wa9jr573pnrpysliqp1gyqhmm2ff8avvkfaaisy0s4cpyj8")))

