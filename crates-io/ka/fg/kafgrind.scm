(define-module (crates-io ka fg kafgrind) #:use-module (crates-io))

(define-public crate-kafgrind-0.1.0 (c (n "kafgrind") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.2") (d #t) (k 0)) (d (n "rdkafka") (r "^0.29.0") (f (quote ("sasl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15syzjnln1x97bydxb1zg4zpd1ryxhgddch4sf89vbqf3a9p8rrd")))

