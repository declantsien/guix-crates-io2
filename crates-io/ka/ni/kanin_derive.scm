(define-module (crates-io ka ni kanin_derive) #:use-module (crates-io))

(define-public crate-kanin_derive-0.4.0 (c (n "kanin_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lhzgrl3j7aadazj29bvdrzznq3afghxwfjsfkz5adyff2yck21p") (r "1.60")))

(define-public crate-kanin_derive-0.4.1 (c (n "kanin_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k5ir0j42y6v5dqxjn506f0lvk1dwgkkxvijvjj48fbyz1p8hpl4") (r "1.60")))

(define-public crate-kanin_derive-0.5.0 (c (n "kanin_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wavhs8zl4giqmc6ij8vv7yy49cjc1kj9pvcgqfrnrf3p6j94jkd") (r "1.60")))

(define-public crate-kanin_derive-0.5.1 (c (n "kanin_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jq8id1g9rs86p7jsb27vsarh6gkwx0lq6paj26pihwia7qrq6sh") (y #t) (r "1.60")))

(define-public crate-kanin_derive-0.5.2 (c (n "kanin_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14j9504s392jc3y5xxpjl7w5l7k37g2bqxrspgwb1g3yqqps67l4") (r "1.60")))

(define-public crate-kanin_derive-0.6.0 (c (n "kanin_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p7wa4lf9x68sh2s7d388yxpvydjygpnhhc3mbs2s7w3wisn660n") (r "1.60")))

(define-public crate-kanin_derive-0.7.0 (c (n "kanin_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17hdw61aygqrcdss0i0wwzgiav8l3s5q69y2q64jdldyj1cjviml") (r "1.60")))

