(define-module (crates-io ka ni kaniexpect) #:use-module (crates-io))

(define-public crate-kaniexpect-0.1.0 (c (n "kaniexpect") (v "0.1.0") (h "04p9w05ankax8p2g9pk2cw7vfjfggb0k0nd6jpn29bzx74mwbrzn")))

(define-public crate-kaniexpect-0.1.1 (c (n "kaniexpect") (v "0.1.1") (h "1s4xir16l11y5w1pfxljk8k603hvfpmmvng6ca028h89l98gl625")))

(define-public crate-kaniexpect-0.1.2 (c (n "kaniexpect") (v "0.1.2") (h "0wd4bz7qvmlqwvfvx42393qwygxdb77b6qyvsqbpaijy4q0h6dkz")))

(define-public crate-kaniexpect-0.1.3 (c (n "kaniexpect") (v "0.1.3") (h "1nh7j4xv7ab867a8h7y328gk2bl5s24jdkgjwi483q08l2scqrk8")))

