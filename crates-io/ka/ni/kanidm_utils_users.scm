(define-module (crates-io ka ni kanidm_utils_users) #:use-module (crates-io))

(define-public crate-kanidm_utils_users-1.1.0-rc.15-dev (c (n "kanidm_utils_users") (v "1.1.0-rc.15-dev") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "148ar5jhdr30lwav7bg26d4hdpwnmnp99arfsm7s8g7cd8rykvzh") (r "1.66")))

(define-public crate-kanidm_utils_users-1.1.0-rc.16 (c (n "kanidm_utils_users") (v "1.1.0-rc.16") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1rhxym5qlmz439rxpvb657wrsf6rlkx4cy5jdzcdhm95qk3mrhs9") (r "1.66")))

(define-public crate-kanidm_utils_users-1.2.0 (c (n "kanidm_utils_users") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1g54w6kzyhvs7a69g897jr7v3yzghxf7204qig82blfnhh6xdi52") (r "1.77")))

(define-public crate-kanidm_utils_users-1.2.1 (c (n "kanidm_utils_users") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1fpzd6rv7cmr7z1wz0g6nazwbj1gqnfzzq2psc8p1wf964sd49ya") (r "1.77")))

