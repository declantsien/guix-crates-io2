(define-module (crates-io ka ni kanidmd_lib_macros) #:use-module (crates-io))

(define-public crate-kanidmd_lib_macros-1.2.0 (c (n "kanidmd_lib_macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1l76qz3hr1v38f2dpp6xdzbhy8bmywgmngcpw48vwr34811vvnnj") (r "1.77")))

