(define-module (crates-io ka ni kanils) #:use-module (crates-io))

(define-public crate-kanils-1.0.0 (c (n "kanils") (v "1.0.0") (d (list (d (n "cannyls") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2.20") (d #t) (k 0)))) (h "1ylvl9r9w28fcihla53snfgbkqzmj4mdnpkbsag5dcd7fvz8ddaq")))

(define-public crate-kanils-1.0.1 (c (n "kanils") (v "1.0.1") (d (list (d (n "cannyls") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2.20") (d #t) (k 0)))) (h "0m99d5jpjpnp3p847jhdyz5as44cllf9hg46mxxlzl2mzbxmz4ym")))

(define-public crate-kanils-1.2.0 (c (n "kanils") (v "1.2.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "cannyls") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.11") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "trackable") (r "^0.2.20") (d #t) (k 0)))) (h "0aaql22l5qmfkd5ma98qn9wily8w0k9291liphsdr81lyl229cch")))

