(define-module (crates-io ka ni kanidm_build_profiles) #:use-module (crates-io))

(define-public crate-kanidm_build_profiles-1.1.0-beta.13 (c (n "kanidm_build_profiles") (v "1.1.0-beta.13") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 1)) (d (n "git2") (r "^0.17.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1rpv6mg1n17c0f4k06wi49gvs46qdymbvm0kvazqfxm5rw7790hk") (r "1.66")))

(define-public crate-kanidm_build_profiles-1.1.0-rc.15-dev (c (n "kanidm_build_profiles") (v "1.1.0-rc.15-dev") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 1)) (d (n "gix") (r "^0.53.1") (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0vy2zcp1fy12d8xaprd610pdpm8ix5qi2v6wqd76xddi97zvy5bf") (r "1.66")))

(define-public crate-kanidm_build_profiles-1.1.0-rc.16 (c (n "kanidm_build_profiles") (v "1.1.0-rc.16") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 1)) (d (n "gix") (r "^0.53.1") (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1dqz0ikqil09l7vl4zzzv3yy0wxc4rqjddgs61k8bhwsgpici73l") (r "1.66")))

(define-public crate-kanidm_build_profiles-1.2.0 (c (n "kanidm_build_profiles") (v "1.2.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "base64") (r "^0.21.7") (d #t) (k 1)) (d (n "gix") (r "^0.53.1") (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "1f2ky2vhz16k48n6ki7ha0cyxsw9wl9kxkd3w53wik6dqavn4zs4") (r "1.77")))

(define-public crate-kanidm_build_profiles-1.2.1 (c (n "kanidm_build_profiles") (v "1.2.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "base64") (r "^0.21.7") (d #t) (k 1)) (d (n "gix") (r "^0.53.1") (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.11") (d #t) (k 0)))) (h "0sspsvj35p3gi8y232qrkb4qc4njccm2a67wwmaig0ig91b54j62") (r "1.77")))

