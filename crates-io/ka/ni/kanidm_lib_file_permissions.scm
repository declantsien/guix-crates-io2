(define-module (crates-io ka ni kanidm_lib_file_permissions) #:use-module (crates-io))

(define-public crate-kanidm_lib_file_permissions-1.1.0-rc.15-dev (c (n "kanidm_lib_file_permissions") (v "1.1.0-rc.15-dev") (d (list (d (n "kanidm_utils_users") (r "^1.1.0-rc.15-dev") (d #t) (t "cfg(not(target_family = \"windows\"))") (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "0clssqz35ggql5cg6iiwjb3crfbbp5qajzicfz0n0h6sij8jwc1h") (r "1.66")))

(define-public crate-kanidm_lib_file_permissions-1.1.0-rc.16 (c (n "kanidm_lib_file_permissions") (v "1.1.0-rc.16") (d (list (d (n "kanidm_utils_users") (r "^1.1.0-rc.16") (d #t) (t "cfg(not(target_family = \"windows\"))") (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "04i2xphgv8sd2rzny1ggpwcbns2v7bnxkdf7pkbnan2akzgwha97") (r "1.66")))

(define-public crate-kanidm_lib_file_permissions-1.2.0 (c (n "kanidm_lib_file_permissions") (v "1.2.0") (d (list (d (n "kanidm_utils_users") (r "=1.2.0") (d #t) (t "cfg(not(target_family = \"windows\"))") (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1133xh6l0lp25pr5qn0bbhpl83vg7y530lrf8v4v41x0brk1m0vd") (r "1.77")))

(define-public crate-kanidm_lib_file_permissions-1.2.1 (c (n "kanidm_lib_file_permissions") (v "1.2.1") (d (list (d (n "kanidm_utils_users") (r "=1.2") (d #t) (t "cfg(not(target_family = \"windows\"))") (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1q9rh3bdp3bbwk4cqz8skj9g60zwx464fn7q8bpf2962vgl1c63p") (r "1.77")))

