(define-module (crates-io ka fi kafi) #:use-module (crates-io))

(define-public crate-kafi-0.1.0 (c (n "kafi") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1x2iqwxq8axk3wfhpn97p81n9dsrijz6s0mpil2h4kqgbvif42sx")))

(define-public crate-kafi-0.1.1 (c (n "kafi") (v "0.1.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0n1frp85dzw8w4q84w1y6xj6cik6mlr5jvq98j1ab3n1mc8zfxb8")))

(define-public crate-kafi-0.1.2 (c (n "kafi") (v "0.1.2") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15s4maqqgbzvxaamf1p9nd68zwvxisk52lyqnjrwhdmma303kkin")))

