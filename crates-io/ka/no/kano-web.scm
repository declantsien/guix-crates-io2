(define-module (crates-io ka no kano-web) #:use-module (crates-io))

(define-public crate-kano-web-0.0.1 (c (n "kano-web") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "gloo") (r "^0.10") (f (quote ("events"))) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "kano") (r "^0.0.1") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Node" "Text" "Document" "Element" "Event" "EventTarget" "HtmlElement" "Comment"))) (d #t) (k 0)))) (h "0xnv6h2f37in9qjxp0accxwn0lcvbky6r33qa6jrkdi5pp6wyf9x") (f (quote (("web-component"))))))

