(define-module (crates-io ka no kano-macros) #:use-module (crates-io))

(define-public crate-kano-macros-0.0.1 (c (n "kano-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17rkk7ngi2za1vfpglhp9a5dcg2by2a146jyhxl2ck3cdjgnm5ga")))

