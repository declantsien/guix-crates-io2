(define-module (crates-io ka no kano-tui) #:use-module (crates-io))

(define-public crate-kano-tui-0.0.1 (c (n "kano-tui") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "kano") (r "^0.0.1") (f (quote ("tui"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0fxszvm4255qs7nckwzkm79arjsjhlasvkc3fs17d6v8rdmmms3p") (f (quote (("web-component"))))))

