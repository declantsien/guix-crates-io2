(define-module (crates-io ka no kano) #:use-module (crates-io))

(define-public crate-kano-0.0.1 (c (n "kano") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "kano-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (k 0)))) (h "11zz0sah7shyk64m8p9vbm71cr2qp30b2f4g42avbsmkv6da6i47") (f (quote (("web" "wasm-bindgen" "wasm-bindgen-futures") ("tui"))))))

