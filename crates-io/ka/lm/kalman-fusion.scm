(define-module (crates-io ka lm kalman-fusion) #:use-module (crates-io))

(define-public crate-kalman-fusion-0.1.0 (c (n "kalman-fusion") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "08mbbav4nv50hi7fjz98rf3c7q9ahjkwidxl5n250jh8zpb00gv2")))

(define-public crate-kalman-fusion-0.1.1 (c (n "kalman-fusion") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "1vfhnhgawg3j05lxfn9w3bbgz24zh1k44rd7z1545z0s90356by5")))

