(define-module (crates-io ka lm kalman-rust) #:use-module (crates-io))

(define-public crate-kalman-rust-0.1.0 (c (n "kalman-rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0bd2r5c55q46ndfvqbrpiskd6f8nf3f24pwj3lazhyhp05ry2prp")))

(define-public crate-kalman-rust-0.2.0 (c (n "kalman-rust") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1fxi8jyfiqyzwi9q8yphzscayajszxwxhq5jlqg96ahskavz6spy")))

(define-public crate-kalman-rust-0.2.1 (c (n "kalman-rust") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1npkzmss80gpbk28vziqr7axz23cxl9vqlnjk1mc131x0gcam2lw")))

(define-public crate-kalman-rust-0.2.2 (c (n "kalman-rust") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0hgf25r71ag9lrzfay6mxdxvc686f3izxn43k2crapbvwk9m1yll")))

(define-public crate-kalman-rust-0.2.3 (c (n "kalman-rust") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1486z25136zax8am3bg93nxavrl146120mcgwx5lbdxnlp07cc2y")))

