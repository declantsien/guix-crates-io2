(define-module (crates-io ka lm kalman_filter) #:use-module (crates-io))

(define-public crate-kalman_filter-0.1.0 (c (n "kalman_filter") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.14.1") (d #t) (k 0)))) (h "1hg5j8cmnap9lny45ba1iqh6sk0dxbk88zpkz3ysmi8qx3czlwnn")))

(define-public crate-kalman_filter-0.1.1 (c (n "kalman_filter") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.14.1") (d #t) (k 0)))) (h "0ampxxy051bapby1dn05gph5847pa08spxvvsq7qfcybglfni9xx")))

(define-public crate-kalman_filter-0.1.2 (c (n "kalman_filter") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.14.1") (d #t) (k 0)))) (h "07c4yn2k28lhm8vrb4qrpr6c1qmnlimbya7ak1g607casvf7wc4l")))

