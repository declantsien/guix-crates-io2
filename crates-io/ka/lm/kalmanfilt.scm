(define-module (crates-io ka lm kalmanfilt) #:use-module (crates-io))

(define-public crate-kalmanfilt-0.2.2 (c (n "kalmanfilt") (v "0.2.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (f (quote ("libm"))) (k 0)))) (h "07chq1r3a5bf9kx8msbzrns2qpy2fqf6l2nl62zp4dspfmjnq42n") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-kalmanfilt-0.2.3 (c (n "kalmanfilt") (v "0.2.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (f (quote ("libm"))) (k 0)))) (h "1hxrdq0dfk51yndnlc7i56ik6aw9mibx4r3l90drbvivx9s87nad") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-kalmanfilt-0.2.4 (c (n "kalmanfilt") (v "0.2.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.17") (f (quote ("libm"))) (k 0)))) (h "1dzinfp8ls6s7gq383ga41afypsr2z4v37w32lg0a1wjgnlzx0av") (f (quote (("default" "alloc") ("alloc"))))))

