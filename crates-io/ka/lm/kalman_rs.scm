(define-module (crates-io ka lm kalman_rs) #:use-module (crates-io))

(define-public crate-kalman_rs-0.1.0 (c (n "kalman_rs") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.17.2") (d #t) (k 0)))) (h "0kx01vgib0ia0lccnycazklkdbq34za33aw6ym4nh5zrdmj20j81")))

(define-public crate-kalman_rs-0.1.1 (c (n "kalman_rs") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.17.2") (d #t) (k 0)))) (h "11bvk9xfznqf49iwazsd1whj7lbnfhlk38sl39p2p7chpmg6lbh1")))

(define-public crate-kalman_rs-0.1.2 (c (n "kalman_rs") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.17.2") (d #t) (k 0)))) (h "1mabi7qnilyxbav2vsgl7x173rljnn0qr0ziby0fydvc4wpmg7m7")))

(define-public crate-kalman_rs-0.1.3 (c (n "kalman_rs") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.17.2") (d #t) (k 0)))) (h "15zyslp9wpiv2fpihwsrdl4h38ba0pn0vmbjl03l6qb6pm9c053p")))

