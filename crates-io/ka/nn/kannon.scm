(define-module (crates-io ka nn kannon) #:use-module (crates-io))

(define-public crate-kannon-0.0.2 (c (n "kannon") (v "0.0.2") (d (list (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1512fbl7k2cyr1qqq7x9ylrcjwpila68qf1i1paxvqbbzcgv77ra")))

