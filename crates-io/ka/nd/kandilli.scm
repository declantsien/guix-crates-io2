(define-module (crates-io ka nd kandilli) #:use-module (crates-io))

(define-public crate-kandilli-0.1.0 (c (n "kandilli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dateparser") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1mxzyszfnfdl5sjvghql66hdl7mp0y3iw5x2gmm3xdv9hm8a6h03")))

