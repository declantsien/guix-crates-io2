(define-module (crates-io ka rs karsa) #:use-module (crates-io))

(define-public crate-karsa-0.1.0 (c (n "karsa") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "011mb7xpddg9pfnvdqxpyw8njjzr0w5b6zmba7swd3vayqkpg0j3")))

(define-public crate-karsa-0.1.1 (c (n "karsa") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0n00yfj0wxrnkjr3ax14zm2v8q241n95sjfvx9gmf2lk37ghcv1j")))

(define-public crate-karsa-0.1.2 (c (n "karsa") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)))) (h "0gn7lvpdwnimnqq38rb8pypvyclnbxng7w9sm33pfhiy663b71fl")))

