(define-module (crates-io ka l- kal-derive) #:use-module (crates-io))

(define-public crate-kal-derive-0.1.0 (c (n "kal-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kal") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0dsppf7xnm4skd41ql4j6vppa28z2hj3wpwnd2xdkza2p9ykm37a")))

(define-public crate-kal-derive-0.1.1 (c (n "kal-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1blvyaawfp5mg8xqcvgz5c2r08kvpjs1yqllwk1pzmy505sqhsv4")))

(define-public crate-kal-derive-0.2.0 (c (n "kal-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "16wd4wav4m06lzaclxkqig2zhs1jnqji4yy7gpawhg3b8938ssjm")))

(define-public crate-kal-derive-0.2.1 (c (n "kal-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "08qsyaqsrhiaynccsdw1hl5p27b4fcaczlblga6hpmpxc1is4k7n")))

(define-public crate-kal-derive-0.2.2 (c (n "kal-derive") (v "0.2.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "13bsycwcfsj6yn4grs04y7f3m4091xi7zykz7r0lii4xgkiqbdzc")))

(define-public crate-kal-derive-0.3.0 (c (n "kal-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1nqanx90jfv6rf0d087b0maj9y6ws3wkicdm9r2vwbc0dbxg75z4") (f (quote (("lex"))))))

(define-public crate-kal-derive-0.3.1 (c (n "kal-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "138c25ia5wiy0kd816hxzymgbfwfr34z8qywp08n39fnq5mqk72p") (f (quote (("lex"))))))

(define-public crate-kal-derive-0.4.0 (c (n "kal-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07wzv054x7z5q3fh4s333r7pj6llj2v0brf7c1kigvgrk7ra2jmp") (f (quote (("lex")))) (y #t)))

(define-public crate-kal-derive-0.4.1 (c (n "kal-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ybd16cv6g9m8lnj7x0absj1nzww6v45r1dv8zc16izx6a6nkd1b") (f (quote (("lex"))))))

(define-public crate-kal-derive-0.5.0 (c (n "kal-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wccrinbx749mmccpwsph187010yklgkwhz9ibn469666ls2s76n") (f (quote (("lex"))))))

