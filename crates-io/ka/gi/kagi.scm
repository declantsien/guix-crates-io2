(define-module (crates-io ka gi kagi) #:use-module (crates-io))

(define-public crate-kagi-0.1.0 (c (n "kagi") (v "0.1.0") (h "0r4icwrk0d29sp49jlc85qagdl8lsc91zsh8i31d0ylw5wfrv70j")))

(define-public crate-kagi-0.1.1 (c (n "kagi") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-beta.3") (d #t) (k 0)))) (h "0cxg1x4ffx6j06ak0xfc3pb1z4xqq6vq2qnq0wnmv4wka3r10k1l")))

