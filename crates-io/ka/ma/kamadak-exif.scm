(define-module (crates-io ka ma kamadak-exif) #:use-module (crates-io))

(define-public crate-kamadak-exif-0.1.0 (c (n "kamadak-exif") (v "0.1.0") (h "1jinfiwh816mbjajl2r4z6vqk2ki462rmli1jbbyvm9m8bdpvrzp")))

(define-public crate-kamadak-exif-0.1.1 (c (n "kamadak-exif") (v "0.1.1") (h "1qmiyzdljsnz876zn188v0wsypl1r6iasn0j3r54gq7618jfhyha")))

(define-public crate-kamadak-exif-0.1.2 (c (n "kamadak-exif") (v "0.1.2") (h "19rvnzmz0pczxjpm2c04npiiwgxmjnlg62kyym7p3x1d8ggvbgcp")))

(define-public crate-kamadak-exif-0.1.3 (c (n "kamadak-exif") (v "0.1.3") (h "0ml45kbvhb8jj5mvnb7m3j2k2k2m9ffziaz4i3wvc82l9nrddmh7")))

(define-public crate-kamadak-exif-0.2.0 (c (n "kamadak-exif") (v "0.2.0") (h "1n8s9z6fjg2h9k90bhpy1vdk1zphnkb2i09rxwrs9db8qfmixysn")))

(define-public crate-kamadak-exif-0.2.1 (c (n "kamadak-exif") (v "0.2.1") (h "0va5l66c7b7rm12sj73d8j6dz5kxiskzhnmxr9q4znshqb0j8ya2")))

(define-public crate-kamadak-exif-0.2.2 (c (n "kamadak-exif") (v "0.2.2") (h "1l9jikngpry4d91bwz5fmml95lagavmp6ws4nbwygk5mbkndgl5v")))

(define-public crate-kamadak-exif-0.2.3 (c (n "kamadak-exif") (v "0.2.3") (h "02pnly0rs7yhwp7p6lg58h71pkzvgc1lsbr5hhl0jb58nmdrykc4")))

(define-public crate-kamadak-exif-0.3.0 (c (n "kamadak-exif") (v "0.3.0") (h "1mh1inh7540a34wzjr3ic2kmragg5s44d08nmlmfdfvp5a0xyf22")))

(define-public crate-kamadak-exif-0.3.1 (c (n "kamadak-exif") (v "0.3.1") (h "1bcp3xa02y8i1srgkmi6x66n3fzj7ycabf8xdnljzbddspa6scbc")))

(define-public crate-kamadak-exif-0.4.0 (c (n "kamadak-exif") (v "0.4.0") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "0mqwm6i6b47fbliysw3k4yc6ym695j5v5k2wla5hwmv107gyicv8")))

(define-public crate-kamadak-exif-0.5.0 (c (n "kamadak-exif") (v "0.5.0") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "1fkph8dcjlbs544g452ajv1l9fzm6g0h3cp3ybj7nf7hxh9siy0c")))

(define-public crate-kamadak-exif-0.5.1 (c (n "kamadak-exif") (v "0.5.1") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "1rxcvcxaxj5lbdp7p62lzpj8k2d48plg1xqihq1j34s6npancpka")))

(define-public crate-kamadak-exif-0.5.2 (c (n "kamadak-exif") (v "0.5.2") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "1a0ac77fp117vd3qdws1kp0947mgzd7dai5agi74yz9z6yk24ksj")))

(define-public crate-kamadak-exif-0.5.3 (c (n "kamadak-exif") (v "0.5.3") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "1qsi1jv05bwi6k61p71z90d60dj0wz2h4ackg7ib6dm32s5icff2")))

(define-public crate-kamadak-exif-0.5.4 (c (n "kamadak-exif") (v "0.5.4") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "1dng0brs7fa8sxbcrrpd01qyn9wn1kbwal9rxf8y9y1b95j4jjbh")))

(define-public crate-kamadak-exif-0.5.5 (c (n "kamadak-exif") (v "0.5.5") (d (list (d (n "mutate_once") (r "^0.1.1") (d #t) (k 0)))) (h "0xw0lpmra8j1y98c0agwrmjajpkh91mnl89hzaxbdrdp186wfkzg")))

