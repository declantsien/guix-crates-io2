(define-module (crates-io ka na kana-converter) #:use-module (crates-io))

(define-public crate-kana-converter-0.1.0 (c (n "kana-converter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1g6zwzy6n66q95wq94hwq67myrcjjjj3q1bsrhqnmkwbv77rc75y")))

(define-public crate-kana-converter-0.1.1 (c (n "kana-converter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1ln5rfl2ws2nqac83wvrixd5w3a23xih1d1c4hw68a3ffmn1l6l2")))

(define-public crate-kana-converter-0.1.2 (c (n "kana-converter") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0za4pwxkbjwlhhwhzrrvx20pa30w8c0warvdaghjzrkpwl7zjw08")))

