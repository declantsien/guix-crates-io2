(define-module (crates-io ka na kanata-tcp-protocol) #:use-module (crates-io))

(define-public crate-kanata-tcp-protocol-0.160.2 (c (n "kanata-tcp-protocol") (v "0.160.2") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "0nsxsq11n2ln8nvapfx5cq4clhcdwcjrwr76zg9r2ym33a50gz1p")))

(define-public crate-kanata-tcp-protocol-0.160.3 (c (n "kanata-tcp-protocol") (v "0.160.3") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "0dsgvcznsdb990a44i380rg8fbwxj1cd7frdrncnwz7xia9xy95n")))

(define-public crate-kanata-tcp-protocol-0.160.4 (c (n "kanata-tcp-protocol") (v "0.160.4") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "1cc1znivzrn08shql350hkgvvacqmcqcbyh54vylysr56ffjyx1q")))

(define-public crate-kanata-tcp-protocol-0.160.5 (c (n "kanata-tcp-protocol") (v "0.160.5") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "160fgamzfmi2x4y3lck1pypccsamsnfxvqiyfraay6l1as0n93rl")))

(define-public crate-kanata-tcp-protocol-0.161.0 (c (n "kanata-tcp-protocol") (v "0.161.0") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "1ap8l34ni86s6lhgbmfpvd7z98a3dz74wpsrp6ash2b1kw19cq1i")))

(define-public crate-kanata-tcp-protocol-0.161.1 (c (n "kanata-tcp-protocol") (v "0.161.1") (d (list (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "0j1vlp5grzhfy6mfy8isp3sxabfrfiskwsmdv19abz9w5lz4grd9")))

