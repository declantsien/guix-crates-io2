(define-module (crates-io ka na kanata-interception) #:use-module (crates-io))

(define-public crate-kanata-interception-0.2.0 (c (n "kanata-interception") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "interception-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kpl88nnjp6v4nb9zl5s3vy9k2bjyc2x06mjf5499iqyappqygg4")))

