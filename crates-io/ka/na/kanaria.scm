(define-module (crates-io ka na kanaria) #:use-module (crates-io))

(define-public crate-kanaria-0.1.0 (c (n "kanaria") (v "0.1.0") (h "1903jklv07qwrmr3f7scy3wc7d9k1y7wy0rxxgmysh74caxb0yq2")))

(define-public crate-kanaria-0.1.1 (c (n "kanaria") (v "0.1.1") (h "0z8i8kx1pybimyqy7ma7m1wwfbc11q7naq52z16r899crzzjvdi8")))

(define-public crate-kanaria-0.2.0 (c (n "kanaria") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1zwn86j7shw74lhv2yh7k5c9czd9mirqm6gdzp25l1a04mjxkyf0")))

