(define-module (crates-io ka na kanata-keyberon-macros) #:use-module (crates-io))

(define-public crate-kanata-keyberon-macros-0.1.0 (c (n "kanata-keyberon-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1722smq92y0zr638wz8drkfb83rsnnaqpi0cvjr3znsyqcm6vdnv")))

(define-public crate-kanata-keyberon-macros-0.2.0 (c (n "kanata-keyberon-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0lj7ldiazmszh0k01h7mjzhjg59bdakvx2pnpc9mq2ir0czzixkk")))

