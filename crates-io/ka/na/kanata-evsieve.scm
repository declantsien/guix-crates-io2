(define-module (crates-io ka na kanata-evsieve) #:use-module (crates-io))

(define-public crate-kanata-evsieve-1.3.1 (c (n "kanata-evsieve") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0br1vnsw30dhaxjp5spd50j89dpl17vxf7l8wwni2cpj8kqfkl7x") (f (quote (("systemd") ("auto-scan")))) (y #t) (l "evdev")))

