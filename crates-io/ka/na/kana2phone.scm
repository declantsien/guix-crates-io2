(define-module (crates-io ka na kana2phone) #:use-module (crates-io))

(define-public crate-kana2phone-0.1.0 (c (n "kana2phone") (v "0.1.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1gb95zj4yk67c8l7js0kpcy2z6j6mpa8j17ybb8hq4xzap99vl11")))

(define-public crate-kana2phone-0.2.0 (c (n "kana2phone") (v "0.2.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0sbbbgzcbm9ik1f0mqsg4gfz4i4kalr6rwdz7whaf51c1ghn1s8a")))

(define-public crate-kana2phone-0.3.0 (c (n "kana2phone") (v "0.3.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0n0kimw9z8v3ah8yshl68cmj7hrnjbir7rm62ghqvs44m5qnwv7w")))

