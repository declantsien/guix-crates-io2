(define-module (crates-io ka na kanata-keyberon) #:use-module (crates-io))

(define-public crate-kanata-keyberon-0.2.0 (c (n "kanata-keyberon") (v "0.2.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "18a9rrm1bj9fsghydwd2qz0ph56630x9rk9wfdqm622jz7yl9f75")))

(define-public crate-kanata-keyberon-0.2.1 (c (n "kanata-keyberon") (v "0.2.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "082l86dw9fbgl81sxkhvv34sxsi4xpyab1ifrlx4bmcvx9ax757v")))

(define-public crate-kanata-keyberon-0.2.2 (c (n "kanata-keyberon") (v "0.2.2") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "02y6f48naa0slzb4mrq0v0haj17y9g9ihgywmg861igy7h7kmi47")))

(define-public crate-kanata-keyberon-0.2.3 (c (n "kanata-keyberon") (v "0.2.3") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0pjy5x1v3ssm9j65ps7b0hlipadhylaj3zkpybhfl9kcazm8jmfz")))

(define-public crate-kanata-keyberon-0.2.4 (c (n "kanata-keyberon") (v "0.2.4") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0qhn0ba4xwh3k22k2b9z10wsw6m6q9ffli7pfgyia8xnab4dv3wl")))

(define-public crate-kanata-keyberon-0.2.5 (c (n "kanata-keyberon") (v "0.2.5") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1bqpa2ym6jz6dl6dgg23mjcf9rrfps1y41087hj253iqiyarv6z5")))

(define-public crate-kanata-keyberon-0.2.6 (c (n "kanata-keyberon") (v "0.2.6") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "18r8r21m44b9sp7cv6xppgi1gqnzpbqn2a0shbqvbwfm6sx0xchk")))

(define-public crate-kanata-keyberon-0.2.7 (c (n "kanata-keyberon") (v "0.2.7") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "143r18rssw6wldvfrnvgkzz71lmlq4xvyl8yixzfijz71f4d679r")))

(define-public crate-kanata-keyberon-0.2.8 (c (n "kanata-keyberon") (v "0.2.8") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0x91r6xaw74cdsgp1sfnwipsllswdkl2n4vl41bw10d839bj4s2r")))

(define-public crate-kanata-keyberon-0.2.9 (c (n "kanata-keyberon") (v "0.2.9") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1ggk2zh67msi1f5g4cz30pw8q06hsfzd1766a8v9cw6y2cp3np5d")))

(define-public crate-kanata-keyberon-0.3.0 (c (n "kanata-keyberon") (v "0.3.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0p6iqsacb744sgipl8kmcyj6asdaipw9spmzvjr3y2zvqp87rfvq")))

(define-public crate-kanata-keyberon-0.3.1 (c (n "kanata-keyberon") (v "0.3.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "12ja0npqnxw02q5apkzilwja88pmn51id44larvkgcyfwsfsbxmd")))

(define-public crate-kanata-keyberon-0.3.2 (c (n "kanata-keyberon") (v "0.3.2") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1gxln8kc88mpykf7z5j9nlvj5q1y22irdlanhmshpk161qs9nvm5")))

(define-public crate-kanata-keyberon-0.3.3 (c (n "kanata-keyberon") (v "0.3.3") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "00j3bhpxwxmfahhka7031yzzkqm01lpavqr354ym3gr34wb5y17i")))

(define-public crate-kanata-keyberon-0.4.0 (c (n "kanata-keyberon") (v "0.4.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1jnqwh0cqfqsfzzlgb9di2iqw4c4zjaxpgznj1ir4gxyxwnxgpiv")))

(define-public crate-kanata-keyberon-0.5.0 (c (n "kanata-keyberon") (v "0.5.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0qp1qkn7mg2w668nsllf3yb4sp22x1j85pn7p9hw7y45g04i5z21")))

(define-public crate-kanata-keyberon-0.6.0 (c (n "kanata-keyberon") (v "0.6.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0mfh3w6fxcj03dn4f4wm0im9nkvvcrrkpwlx2sadz4y62s4ahjzn")))

(define-public crate-kanata-keyberon-0.6.1 (c (n "kanata-keyberon") (v "0.6.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0idmrk32kr86fg9pmxlzvz2xksxj3ml1ma5iahycmbs5226pqp4i")))

(define-public crate-kanata-keyberon-0.7.0 (c (n "kanata-keyberon") (v "0.7.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0vh5z6qqnjn9cv1imzpmm3g5x9plsla524xcxvq4r64aihvl4cm8")))

(define-public crate-kanata-keyberon-0.7.1 (c (n "kanata-keyberon") (v "0.7.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0nrhkfccl1419i5q00hpg0gxsl6jz23bhif6kfvwinnm7hamrpjw")))

(define-public crate-kanata-keyberon-0.8.0 (c (n "kanata-keyberon") (v "0.8.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "0xcwk5kfxwc9h3npi1k4rpmn6kgvfdz8yj6d67rj34bsr9nhc3cn")))

(define-public crate-kanata-keyberon-0.9.0 (c (n "kanata-keyberon") (v "0.9.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "17apr94f5b58ghbpb3wkx4nir6xms1nqq9yqchyljz46bljsnkd3")))

(define-public crate-kanata-keyberon-0.10.0 (c (n "kanata-keyberon") (v "0.10.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.6") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1qwr1dinjsmzhlklw0dy44f931b4syislknzrg7vyd1l55lvcran")))

(define-public crate-kanata-keyberon-0.11.0 (c (n "kanata-keyberon") (v "0.11.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1v7v48y7g9f78aqdpqb8xdgs4cbdlhlrja53w4nj10hhq23zmj53")))

(define-public crate-kanata-keyberon-0.12.0 (c (n "kanata-keyberon") (v "0.12.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0sdhk8bjzcayqjibq5cjgh7d4q7925jrgygngrvrhf8n059wazny")))

(define-public crate-kanata-keyberon-0.13.0 (c (n "kanata-keyberon") (v "0.13.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lfp8vxz8q4g0hp71y8yyz4s5iniir5ahbdgn36n9wravj76g6px")))

(define-public crate-kanata-keyberon-0.14.0 (c (n "kanata-keyberon") (v "0.14.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0b9av5wffs14w56wffb3vhfp0xvzm1b0x1rz7pszzycvf618mm7w")))

(define-public crate-kanata-keyberon-0.15.0 (c (n "kanata-keyberon") (v "0.15.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "13z92nr2rfcwf3vjym9rkjjf8vhvqdqcv9n663f3fn6azqa3ihwq")))

(define-public crate-kanata-keyberon-0.16.0 (c (n "kanata-keyberon") (v "0.16.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1a49ak1cj6654px0mmnpc4bs11p6yaiphk3xzcnkjskn21si05k9")))

(define-public crate-kanata-keyberon-0.17.0 (c (n "kanata-keyberon") (v "0.17.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "03a9rbvzxkcvbg0pgskj593ciw1mmnw2ms8yrksjciirvhnq3m7q")))

(define-public crate-kanata-keyberon-0.19.0 (c (n "kanata-keyberon") (v "0.19.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1g1jfvls32z8f4y7myrmp5j9b71fhrcqcz4ybyb9r8aq0amhv22g")))

(define-public crate-kanata-keyberon-0.20.0 (c (n "kanata-keyberon") (v "0.20.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0z6l18ckwfsh69flj0isr4474rw01dsp3v2vjnp1427pyxla2fyv")))

(define-public crate-kanata-keyberon-0.150.0 (c (n "kanata-keyberon") (v "0.150.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0avhyaaqzvxl6kfif7pdl1zq14s6hjamxhf180z0chnv2jq61imb")))

(define-public crate-kanata-keyberon-0.150.2 (c (n "kanata-keyberon") (v "0.150.2") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1fizqygi8w5v300yxg53syi5awmdr2kd9zllhbjpbs0z3vyzb3qs")))

(define-public crate-kanata-keyberon-0.150.3 (c (n "kanata-keyberon") (v "0.150.3") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0kp3c2d03lcrh3x8qfjh63s1p0pxc2fnkjxldccd1plp4jfq0k80")))

(define-public crate-kanata-keyberon-0.150.4 (c (n "kanata-keyberon") (v "0.150.4") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0ip7yas1qh4xz8v1hh5hknwz3rf9j8ah4djaab72ljvdll7a2hyq")))

(define-public crate-kanata-keyberon-0.160.1 (c (n "kanata-keyberon") (v "0.160.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0rd1krk7pms5m4gg99583wvv9n44hl4zcbyq98cz1b70ar6pmkii")))

(define-public crate-kanata-keyberon-0.160.2 (c (n "kanata-keyberon") (v "0.160.2") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "07ld9xfv0acrs7aa540gx50m9zx8gv7zfrqlhqk632ga70cxj9cj")))

(define-public crate-kanata-keyberon-0.160.3 (c (n "kanata-keyberon") (v "0.160.3") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1z09lg7867dddah04nvkgwsn1vlppnmz1mgjpqpda8lykwkywhib")))

(define-public crate-kanata-keyberon-0.160.4 (c (n "kanata-keyberon") (v "0.160.4") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0svp2faizn76zikan76sarkji500q8d0q4v6k8hfkxr3x3z468k1")))

(define-public crate-kanata-keyberon-0.160.5 (c (n "kanata-keyberon") (v "0.160.5") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1ajvzxq8ddc9yk1ph1wbn3sgq391wdh2rvh1884v39riphfqpzla")))

(define-public crate-kanata-keyberon-0.161.0 (c (n "kanata-keyberon") (v "0.161.0") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0jq14sxqcj7rkza5fq1rcbfjkn1s1p6yljrj53lbfdi7kx9xfdgm")))

(define-public crate-kanata-keyberon-0.161.1 (c (n "kanata-keyberon") (v "0.161.1") (d (list (d (n "arraydeque") (r "^0.5.1") (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "kanata-keyberon-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "10afqja92dyad61jfd6pn961amd2z9vii2x4dslpqbzvn1nh2w56")))

