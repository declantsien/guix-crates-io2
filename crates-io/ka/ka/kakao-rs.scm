(define-module (crates-io ka ka kakao-rs) #:use-module (crates-io))

(define-public crate-kakao-rs-0.1.0 (c (n "kakao-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b05ln038zrxdnxr7q5mm1gd4zyl5s99ya0wml2yz34mfccl3vrj")))

(define-public crate-kakao-rs-0.2.0 (c (n "kakao-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "181nlsghsn4ma0dr2ri3yqmnkbddx2ks7h6875p9kfh6v3dd4fk9")))

(define-public crate-kakao-rs-0.2.1 (c (n "kakao-rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xarjl9vph1al8jz6s699qsc8sa8jmzndh5kjngwpq9rx5m354r0")))

(define-public crate-kakao-rs-0.2.2 (c (n "kakao-rs") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g0yrjzbmq3nnzdzns1ir99ra6d4s8lg36y7haaf4ckkh1ps1yh3")))

(define-public crate-kakao-rs-0.2.3 (c (n "kakao-rs") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c60cibfqlz0yw3nrqv3fb3jshjp3jiz5b5r2r6ydpy8sxl41n7y")))

(define-public crate-kakao-rs-0.2.4 (c (n "kakao-rs") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17rffizqzlq0blv101y83fcq7234zlai3ql1v6prqa8a8a5x4wfr")))

(define-public crate-kakao-rs-0.2.5 (c (n "kakao-rs") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14bdczib7w2xw279yp1sl1l5k1qy3mh22xjj6z7rm6bb3wh5w9z5")))

(define-public crate-kakao-rs-0.2.6 (c (n "kakao-rs") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "081i31qm7gzjl70czgs9ly558kyv38d74qfrhg5k7954xydf8w1z")))

(define-public crate-kakao-rs-0.2.7 (c (n "kakao-rs") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19shz0hs4inyqbqwqjjq0897zsy48bx2gqvdh3gz4qdg5zjspq8m")))

(define-public crate-kakao-rs-0.2.8 (c (n "kakao-rs") (v "0.2.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rlgcq5ybwsq3vsjm5cagm39vq7l5m9fv1z5ajx05g4ardq7wchq")))

(define-public crate-kakao-rs-0.2.9 (c (n "kakao-rs") (v "0.2.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "007wrairsq12vfd4wq29jdibz52hc2pg87ffjc0llylxrcnm1m6a")))

(define-public crate-kakao-rs-0.3.0 (c (n "kakao-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06r0f103jxp631mm9h35dgb3mg5gi9dxj1zm3i2x86slq891qbl2")))

(define-public crate-kakao-rs-0.3.1 (c (n "kakao-rs") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06xwivwak0qga8vm53lz8aj7l14vrz5xnhhhcjcvig54giyhic1k")))

(define-public crate-kakao-rs-0.3.2 (c (n "kakao-rs") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r65r6jahx01jwy5957ib22s4x0x1hi8dhb2rinxfq8z1sw6m9ln")))

(define-public crate-kakao-rs-0.3.3 (c (n "kakao-rs") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1il61w201943qvn3smzcxpdxd93k48pqnwjn2y4snwm03xjpf71f")))

(define-public crate-kakao-rs-0.3.4 (c (n "kakao-rs") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01qc3w576simqmim9jcgwqc14c92b24arq7d9ypw28icf9lf4wjm")))

(define-public crate-kakao-rs-0.3.6 (c (n "kakao-rs") (v "0.3.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0as4zdw1wg1d1bdnr67bl6m51l9hlksl98slbbgm3nqqvh7dd7q8")))

(define-public crate-kakao-rs-0.3.7 (c (n "kakao-rs") (v "0.3.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13ii70v09cgx3d2fcqw303wxfk83mi2kl8ksp1aqwia8d9g5hz8g")))

