(define-module (crates-io ka ka kakasi) #:use-module (crates-io))

(define-public crate-kakasi-0.1.0 (c (n "kakasi") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (k 2)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "1sby6dbxyh1n1y00x88nnbbwwj5736szaj09lp7c81kacldksc7r")))

