(define-module (crates-io ka ka kakapo) #:use-module (crates-io))

(define-public crate-kakapo-0.0.1 (c (n "kakapo") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "shaderc") (r "^0.6") (d #t) (k 1)) (d (n "wgpu") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "1bqawy92lqsc81wmiv1f7j6dhjg7kvycij9x32ifckpc9lfqla8g")))

