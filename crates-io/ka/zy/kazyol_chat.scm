(define-module (crates-io ka zy kazyol_chat) #:use-module (crates-io))

(define-public crate-kazyol_chat-0.1.0 (c (n "kazyol_chat") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0pkwz17zmjdvgy1v9w0ysj391l110bmj6w7ksicn200j1i4348r0")))

(define-public crate-kazyol_chat-0.1.1 (c (n "kazyol_chat") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "12mbl1djk3067hh2jq5wqx1l78w42wz4n6lva3f8msml1872w4an")))

