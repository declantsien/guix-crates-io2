(define-module (crates-io ka tw katwalk) #:use-module (crates-io))

(define-public crate-katwalk-0.0.1 (c (n "katwalk") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1ikhg6aildrsv7gvz9v1bawgcl55m4crzvd9pgfqxsa2q5633ypm")))

(define-public crate-katwalk-0.0.2 (c (n "katwalk") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1s8g8ilbsi37w2yg5v2yrrdp60xg29x054pkbpxwi7h4dwmb2368")))

(define-public crate-katwalk-0.0.3 (c (n "katwalk") (v "0.0.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0v3z4i9v844b6539315mkpw6if5wmawfc9iqjn3ld4jl13yxfm9z")))

(define-public crate-katwalk-0.0.4 (c (n "katwalk") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1nv8xaa65qq36zjlgh9xsf1bhnaicng8cy2bki4lh6kxz9xy38zg")))

(define-public crate-katwalk-0.0.5 (c (n "katwalk") (v "0.0.5") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "007cm4hgggrdv49zjm6cwinqzzwp1a1c123yqq0690xz44q6105m")))

(define-public crate-katwalk-0.0.6 (c (n "katwalk") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0gsv2vcyjd3218vq6hlff6q528im68bv6ryy82dj9fww55fhdkxk")))

(define-public crate-katwalk-0.0.7 (c (n "katwalk") (v "0.0.7") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "15v6k7m0axz2p7pvjj3qdchfmbqc3k12fr10jnk37psk8qc1dcf6")))

(define-public crate-katwalk-0.0.8 (c (n "katwalk") (v "0.0.8") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0b9b9ywhf7g6q3pfy9sv8vk54ffk0lylx8kn2z48jvkas806qlld")))

(define-public crate-katwalk-0.0.9 (c (n "katwalk") (v "0.0.9") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1pwxp5x0nfv6b6jrm586ba32ai9nwxg0icijpxdvr2g9m3yh2k44")))

(define-public crate-katwalk-0.0.10 (c (n "katwalk") (v "0.0.10") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1r7s4h9352araw6d2173jppqfdl7mglqvqx0qlnmiqglh7gr360p")))

(define-public crate-katwalk-0.0.11 (c (n "katwalk") (v "0.0.11") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0818mqrh0n47rcfrh3pyyy1jk04iwlbc1yhhi4ycs8lhsv3ylz2m")))

(define-public crate-katwalk-0.0.12 (c (n "katwalk") (v "0.0.12") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0smjah2lhhdj1xpxaqq6maamxfn79ha387a4js542q2y8b2m1h10")))

