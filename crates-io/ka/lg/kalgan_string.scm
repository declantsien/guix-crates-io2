(define-module (crates-io ka lg kalgan_string) #:use-module (crates-io))

(define-public crate-kalgan_string-0.9.0 (c (n "kalgan_string") (v "0.9.0") (h "0g3gkmcv2a264885m9jvzr83cpa916vwykafirms0bs5168xx8bx")))

(define-public crate-kalgan_string-0.9.1 (c (n "kalgan_string") (v "0.9.1") (h "00lmblvmqg649n2krrqwbk99mpnbw5kvrznwc8ahbfn3892d29g8")))

