(define-module (crates-io ka lg kalgan_i18n) #:use-module (crates-io))

(define-public crate-kalgan_i18n-0.9.0 (c (n "kalgan_i18n") (v "0.9.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "04mlj0k0fkl0sh43zvyari0my8x91rv72kmygdvdm2hb5w4s7ar0")))

(define-public crate-kalgan_i18n-0.9.1 (c (n "kalgan_i18n") (v "0.9.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1k1p1xszsna5pm1nnr5mxl95hnpw1aw3fh7vy9dqr38z0xhn67ms")))

