(define-module (crates-io ka lg kalgan_router) #:use-module (crates-io))

(define-public crate-kalgan_router-0.9.0 (c (n "kalgan_router") (v "0.9.0") (d (list (d (n "kalgan_string") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "0gaj9fg6bbpkfssq34pxvjd6wzwai3y6n9r6pr04036rik5h27bi")))

(define-public crate-kalgan_router-0.9.1 (c (n "kalgan_router") (v "0.9.1") (d (list (d (n "kalgan_string") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1iz59my6yx2gagirbyql9g07kcnpa7p14z2ayfqy2x7vq4mjqwyz")))

