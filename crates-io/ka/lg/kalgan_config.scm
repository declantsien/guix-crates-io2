(define-module (crates-io ka lg kalgan_config) #:use-module (crates-io))

(define-public crate-kalgan_config-0.9.0 (c (n "kalgan_config") (v "0.9.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1nr8bmv5bac76ds237ky8qycaz2g7kv6ldf28jlcb298r19zwqdh")))

(define-public crate-kalgan_config-0.9.1 (c (n "kalgan_config") (v "0.9.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1dibysi43j2sa1z4d9y80b7ci402rjbzk9j1dka8r65rfbm582ii")))

