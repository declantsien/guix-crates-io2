(define-module (crates-io ka pr kaprekar) #:use-module (crates-io))

(define-public crate-kaprekar-0.1.0 (c (n "kaprekar") (v "0.1.0") (h "0wq18sbm2arpknwhkw5cicw6i9ipzfiylzqsdldma7r1fsn2fq1b")))

(define-public crate-kaprekar-0.1.1 (c (n "kaprekar") (v "0.1.1") (h "14zszqi7cya9y8ns83ryqkyfykvfki54276rnm9v9pdhn3b8nbia")))

(define-public crate-kaprekar-0.1.2 (c (n "kaprekar") (v "0.1.2") (h "01ymkwzkam3ch5f7rv5j9zn3bfaka6rligg48qdwia0qr1i9v5vv")))

(define-public crate-kaprekar-0.1.3 (c (n "kaprekar") (v "0.1.3") (h "1yc92dhmh6vq3jc02j237njvrsp00s146d9lql9wv32n2f3ahrb5")))

(define-public crate-kaprekar-0.1.4 (c (n "kaprekar") (v "0.1.4") (h "042lgmrpd6wywv04zayc856kcs7611ha1m1sc9iv28q884q91h55")))

(define-public crate-kaprekar-0.1.5 (c (n "kaprekar") (v "0.1.5") (h "1yn8bbk8cx9h8axik31hbg9h7a783c9yq9lnyq86a5rxc949672p")))

