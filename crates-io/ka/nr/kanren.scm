(define-module (crates-io ka nr kanren) #:use-module (crates-io))

(define-public crate-kanren-0.1.0 (c (n "kanren") (v "0.1.0") (h "1b24cq3fs8wmg0ln6aid2w3x4hx5242allwx0vm2nqaifljfq9q7") (y #t)))

(define-public crate-kanren-0.1.1 (c (n "kanren") (v "0.1.1") (h "0b6jl5cpcsss4r08pd7yl36r94xx5071darlfkzaqkk2z62rmyl9") (y #t)))

