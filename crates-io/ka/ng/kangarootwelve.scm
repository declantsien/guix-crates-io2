(define-module (crates-io ka ng kangarootwelve) #:use-module (crates-io))

(define-public crate-kangarootwelve-0.1.0 (c (n "kangarootwelve") (v "0.1.0") (d (list (d (n "turboshake") (r "=0.1.4") (f (quote ("dev"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1g9nk1iylhdab1r135lh62dqghyhnp63ljdrjzr798pfq2a0dgnk")))

(define-public crate-kangarootwelve-0.1.1 (c (n "kangarootwelve") (v "0.1.1") (d (list (d (n "num_cpus") (r "=1.15.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "=1.7.0") (o #t) (d #t) (k 0)) (d (n "turboshake") (r "=0.1.4") (f (quote ("dev"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "182x22xx9p56m51flw4hd5yvkg4nmcihq0ylqvmnbqkf0wlnfsj0") (s 2) (e (quote (("multi_threaded" "dep:rayon" "dep:num_cpus"))))))

(define-public crate-kangarootwelve-0.1.2 (c (n "kangarootwelve") (v "0.1.2") (d (list (d (n "num_cpus") (r "=1.15.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "=1.7.0") (o #t) (d #t) (k 0)) (d (n "turboshake") (r "=0.1.4") (f (quote ("dev"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19920qj99s9d5zsyayp0bgf1bgjfwx7gslic8dckj58k9fagrm4j") (s 2) (e (quote (("multi_threaded" "dep:rayon" "dep:num_cpus"))))))

