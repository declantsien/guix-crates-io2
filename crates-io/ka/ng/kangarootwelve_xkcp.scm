(define-module (crates-io ka ng kangarootwelve_xkcp) #:use-module (crates-io))

(define-public crate-kangarootwelve_xkcp-0.1.0 (c (n "kangarootwelve_xkcp") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "00zhg3y9g7rzbb8f3hazn0y59dsxy2fz5sinlk18bmh6yvfypn8p")))

(define-public crate-kangarootwelve_xkcp-0.1.1 (c (n "kangarootwelve_xkcp") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "096s7md1jzj8g2h084s5ib1xqa4pvcs859fvd00vm55y8vznqbiw")))

(define-public crate-kangarootwelve_xkcp-0.1.2 (c (n "kangarootwelve_xkcp") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1f27mk15qi5s6kd2gyddi63q66na45865yqqgiaapmfz7n6rjrrg")))

(define-public crate-kangarootwelve_xkcp-0.1.3 (c (n "kangarootwelve_xkcp") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1b2nf14az7rzx7ckcihavlizk17x56kwkldqnh2c8mw1nsk9jb76")))

(define-public crate-kangarootwelve_xkcp-0.1.4 (c (n "kangarootwelve_xkcp") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0y5qa0lblxybygli9qvpaagr9sj369sy10fh12rkqqz0lkyzzajg")))

(define-public crate-kangarootwelve_xkcp-0.1.5 (c (n "kangarootwelve_xkcp") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "k12") (r "^0.1.0") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "149xrvl7vbaifw5595805k0jyi7f58pp45kcbw8clgmg4gaym249")))

