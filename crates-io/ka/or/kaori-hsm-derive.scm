(define-module (crates-io ka or kaori-hsm-derive) #:use-module (crates-io))

(define-public crate-kaori-hsm-derive-0.1.0 (c (n "kaori-hsm-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gcjv3avc1c14lgib6k2avy7mkv1kbjwih0h7s2q8x0wmqzbsr5c")))

(define-public crate-kaori-hsm-derive-0.1.1 (c (n "kaori-hsm-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "073wid9mgb1d0q64qir9vqvzq6m44y9snvs12sccccg5af96i2jd") (r "1.70")))

