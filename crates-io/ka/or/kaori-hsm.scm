(define-module (crates-io ka or kaori-hsm) #:use-module (crates-io))

(define-public crate-kaori-hsm-0.1.0 (c (n "kaori-hsm") (v "0.1.0") (d (list (d (n "kaori-hsm-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wk9kzj1g7a6psvvl9ai7m913mrz9ah7d7awgma77gqv2m52z2mq")))

(define-public crate-kaori-hsm-0.1.1 (c (n "kaori-hsm") (v "0.1.1") (d (list (d (n "kaori-hsm-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dypz6fx75vkjg5gc5j9pz90qi717ss1j9cq68bzafiq0nlg2261") (r "1.70")))

