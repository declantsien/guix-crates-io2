(define-module (crates-io ka bo kaboom) #:use-module (crates-io))

(define-public crate-kaboom-1.0.0 (c (n "kaboom") (v "1.0.0") (h "03mrgip15kn0hfxlmzx64xpjw9nhfvb1dsnd1yh7l63vkx683bh1")))

(define-public crate-kaboom-1.0.1 (c (n "kaboom") (v "1.0.1") (h "0yx9fgpyimqf66i84vkcjnihcys2d3lyh8vgmamkl7wz4ivif327")))

