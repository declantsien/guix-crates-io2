(define-module (crates-io ka sp kaspa-wrpc-wasm) #:use-module (crates-io))

(define-public crate-kaspa-wrpc-wasm-0.0.0 (c (n "kaspa-wrpc-wasm") (v "0.0.0") (h "0m0903qj97yxvagvnlzl6yzzrm5wyphyl9yaf02c1cp1gchpg7v3")))

(define-public crate-kaspa-wrpc-wasm-0.0.1 (c (n "kaspa-wrpc-wasm") (v "0.0.1") (d (list (d (n "kaspa-wrpc-client") (r "^0.0.1") (d #t) (k 0)))) (h "04877m97b7avrd2fnbqbi3v4vy9vsdbp2ca63nd31gskmwjkasb1")))

(define-public crate-kaspa-wrpc-wasm-0.0.2 (c (n "kaspa-wrpc-wasm") (v "0.0.2") (d (list (d (n "kaspa-wrpc-client") (r "^0.0.2") (d #t) (k 0)))) (h "0qadrrcl7c4g8dczm0i595wk0ixacyyl60iknn61cp36s035varz")))

(define-public crate-kaspa-wrpc-wasm-0.0.3 (c (n "kaspa-wrpc-wasm") (v "0.0.3") (d (list (d (n "kaspa-wrpc-client") (r "^0.0.3") (d #t) (k 0)))) (h "087w8gqn7wwhnd9y67jlawsrw9qzdr2clyk4l8hi21vwd3wlh34m")))

(define-public crate-kaspa-wrpc-wasm-0.0.4 (c (n "kaspa-wrpc-wasm") (v "0.0.4") (d (list (d (n "kaspa-wrpc-client") (r "^0.0.4") (d #t) (k 0)))) (h "169dp7sfn4xn2h27m48d0js5h0w3707fpivr1ndgrzci642kbmxx")))

(define-public crate-kaspa-wrpc-wasm-0.13.4 (c (n "kaspa-wrpc-wasm") (v "0.13.4") (d (list (d (n "kaspa-wrpc-client") (r "^0.13.4") (d #t) (k 0)))) (h "13k0alh2v8ziwg0kyrcsd8psmaj04ia9rqhcb3jn9xcyf7drf0cy")))

