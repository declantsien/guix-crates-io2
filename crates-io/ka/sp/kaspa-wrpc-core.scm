(define-module (crates-io ka sp kaspa-wrpc-core) #:use-module (crates-io))

(define-public crate-kaspa-wrpc-core-0.0.0 (c (n "kaspa-wrpc-core") (v "0.0.0") (h "0zgpnzi8qzyrypihjb0k4m6sv4azprs0byzjhbg7k5migdir15a2")))

(define-public crate-kaspa-wrpc-core-0.0.4 (c (n "kaspa-wrpc-core") (v "0.0.4") (h "1k6bvaldcjnj4c9w9v8z1wr831bsjghx9qzg88whhqh1hfgjjkgx")))

