(define-module (crates-io ka sp kaspa-alloc) #:use-module (crates-io))

(define-public crate-kaspa-alloc-0.0.0 (c (n "kaspa-alloc") (v "0.0.0") (h "1mva38qvi6z5wmc59mvpanmic0kdwzbscm5c1ax6jah74zz8zg7l")))

(define-public crate-kaspa-alloc-0.13.4 (c (n "kaspa-alloc") (v "0.13.4") (d (list (d (n "mimalloc") (r "^0.1.39") (f (quote ("override"))) (t "cfg(not(target_os = \"macos\"))") (k 0)) (d (n "mimalloc") (r "^0.1.39") (t "cfg(target_os = \"macos\")") (k 0)))) (h "14nqhfjz8dlljy5njmwdqf2b2n7ral6z6iixhhm0db7fidb1n0yg") (f (quote (("heap"))))))

