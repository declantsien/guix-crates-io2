(define-module (crates-io ka sp kaspa-muhash) #:use-module (crates-io))

(define-public crate-kaspa-muhash-0.0.0 (c (n "kaspa-muhash") (v "0.0.0") (h "1pay4g5x9nkqdvg9gqhk5rjwx8kw18avlan73fdi64y0d2gs11cc")))

(define-public crate-kaspa-muhash-0.0.1 (c (n "kaspa-muhash") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "kaspa-hashes") (r "^0.0.1") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "01f8qg2vwczi5jwzawmbzq33r6hggq0s399bspaaa84ymj6rciyc")))

(define-public crate-kaspa-muhash-0.0.2 (c (n "kaspa-muhash") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "kaspa-hashes") (r "^0.0.2") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1lw87bbkmbbh6avxshqvs1c2w89swgmlnflzdybc6md5hm08riy6")))

(define-public crate-kaspa-muhash-0.0.3 (c (n "kaspa-muhash") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "kaspa-hashes") (r "^0.0.3") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0nhy9n4c5r5i0ss23f7r849gzmwhnhhhz4ff1g9djwp4mrnkzsvm")))

(define-public crate-kaspa-muhash-0.0.4 (c (n "kaspa-muhash") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "kaspa-hashes") (r "^0.0.4") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "05yp9ys4ifhzvrxr7z1f7p7jvm6i2p9cdfbqwryx53gg3bcl996q")))

(define-public crate-kaspa-muhash-0.13.4 (c (n "kaspa-muhash") (v "0.13.4") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "kaspa-hashes") (r "^0.13.4") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.13.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0w2l1afdb3abjw28sxd69pc5vg1y1i6lgi0l0x59kpsd71l92kdx")))

