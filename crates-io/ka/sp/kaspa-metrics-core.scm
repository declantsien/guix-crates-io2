(define-module (crates-io ka sp kaspa-metrics-core) #:use-module (crates-io))

(define-public crate-kaspa-metrics-core-0.0.0 (c (n "kaspa-metrics-core") (v "0.0.0") (h "0n568wmqwpjjsi3vd0kjh8z7fq4d0l6pdy1r2ap1a8d3sy0j4k5s")))

(define-public crate-kaspa-metrics-core-0.13.4 (c (n "kaspa-metrics-core") (v "0.13.4") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "kaspa-core") (r "^0.13.4") (d #t) (k 0)) (d (n "kaspa-rpc-core") (r "^0.13.4") (d #t) (k 0)) (d (n "separator") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "workflow-core") (r "^0.10.3") (d #t) (k 0)) (d (n "workflow-log") (r "^0.10.3") (d #t) (k 0)))) (h "1lyhs72mwg8rw8bxv472n0sgds1w8rrb8mlxk8h362hj4c0s1z7d")))

