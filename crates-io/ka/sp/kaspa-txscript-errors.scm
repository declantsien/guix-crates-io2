(define-module (crates-io ka sp kaspa-txscript-errors) #:use-module (crates-io))

(define-public crate-kaspa-txscript-errors-0.0.0 (c (n "kaspa-txscript-errors") (v "0.0.0") (h "1c4ajki85van4i3bsy85p3ha9qphi21dwf7738gzlff6jnjcgvl6")))

(define-public crate-kaspa-txscript-errors-0.0.1 (c (n "kaspa-txscript-errors") (v "0.0.1") (d (list (d (n "secp256k1") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1324r4cmmbwzwkhvjza0rhdlj2x5ygbh1y849k06bf8xv4l7sljg")))

(define-public crate-kaspa-txscript-errors-0.0.2 (c (n "kaspa-txscript-errors") (v "0.0.2") (d (list (d (n "secp256k1") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00ynp58zg6l5i6yadwlbyzjbqspjsrhydqklcsl5v5g2djw9j5ya")))

(define-public crate-kaspa-txscript-errors-0.0.3 (c (n "kaspa-txscript-errors") (v "0.0.3") (d (list (d (n "secp256k1") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zbdyg7az21psl17nbmgshy5wmc6s5km5ns1rayv0g8a5b4zr803")))

(define-public crate-kaspa-txscript-errors-0.0.4 (c (n "kaspa-txscript-errors") (v "0.0.4") (d (list (d (n "secp256k1") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "10pylvixnp4qcyzqfp16vz262kxf3xxf1i9pfc1bi5j5ibfw5g6q")))

(define-public crate-kaspa-txscript-errors-0.13.4 (c (n "kaspa-txscript-errors") (v "0.13.4") (d (list (d (n "secp256k1") (r "^0.24.3") (f (quote ("global-context" "rand-std" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0m5wd6lmapwrfzrimqwd2z6jw0kkjpr63ii4vdmi8rd56k0aflhg")))

