(define-module (crates-io ka sp kaspa-mining-errors) #:use-module (crates-io))

(define-public crate-kaspa-mining-errors-0.0.0 (c (n "kaspa-mining-errors") (v "0.0.0") (h "1485wd99syvcdh3856chvbsn4hdrvarp4lgk76rq8bfsipa16knn")))

(define-public crate-kaspa-mining-errors-0.0.4 (c (n "kaspa-mining-errors") (v "0.0.4") (d (list (d (n "kaspa-consensus-core") (r "^0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "16isyjk5bkzqc0lz7ksh4k67lrbch097wia6nhc5aiqxhw3kyb3i")))

(define-public crate-kaspa-mining-errors-0.13.4 (c (n "kaspa-mining-errors") (v "0.13.4") (d (list (d (n "kaspa-consensus-core") (r "^0.13.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1l5pm2h2pl7fgpzpsnfcs0z2cm7x3ql0dsskjm28msb158hqw82w")))

