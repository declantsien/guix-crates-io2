(define-module (crates-io ka sp kaspa-wallet-macros) #:use-module (crates-io))

(define-public crate-kaspa-wallet-macros-0.0.0 (c (n "kaspa-wallet-macros") (v "0.0.0") (h "0pc7gxpxg6q7nk8abxiqryrr2ki7d17lhyvsj9bn8hi8ppycvsnb")))

(define-public crate-kaspa-wallet-macros-0.13.4 (c (n "kaspa-wallet-macros") (v "0.13.4") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.7") (f (quote ("xxh3" "xxh32"))) (d #t) (k 0)))) (h "0i5jbn12s5cj5lppvx9acqvd0k49acgd9yw4qax0zx5hv23n8gx4")))

