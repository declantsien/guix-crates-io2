(define-module (crates-io ka sp kaspa-pow) #:use-module (crates-io))

(define-public crate-kaspa-pow-0.0.0 (c (n "kaspa-pow") (v "0.0.0") (h "0klm2hm0qrbnp1v37lryc48rxkgvj8h01yy7g54iqs97r727gsvk")))

(define-public crate-kaspa-pow-0.0.1 (c (n "kaspa-pow") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "kaspa-consensus-core") (r "^0.0.1") (d #t) (k 0)) (d (n "kaspa-hashes") (r "^0.0.1") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.1") (d #t) (k 0)))) (h "09kp8vbg4fnvjha7jlrgnv8jdd1a5wz6z3j69d0zsw10jx7vd9m6")))

(define-public crate-kaspa-pow-0.0.2 (c (n "kaspa-pow") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "kaspa-consensus-core") (r "^0.0.2") (d #t) (k 0)) (d (n "kaspa-hashes") (r "^0.0.2") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1yy6ph8gal2ij8k3jf1rn6n3ir17fwg414kzydxab3waqiqljl30")))

(define-public crate-kaspa-pow-0.0.3 (c (n "kaspa-pow") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "kaspa-consensus-core") (r "^0.0.3") (d #t) (k 0)) (d (n "kaspa-hashes") (r "^0.0.3") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1jp83lw9r4c30959j85kcdn5asd2z5ybwzviax5w6d22bmwsqifg")))

(define-public crate-kaspa-pow-0.0.4 (c (n "kaspa-pow") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "kaspa-consensus-core") (r "^0.0.4") (d #t) (k 0)) (d (n "kaspa-hashes") (r "^0.0.4") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.0.4") (d #t) (k 0)) (d (n "kaspa-utils") (r "^0.0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "workflow-wasm") (r "^0.8.0") (d #t) (k 0)))) (h "0dk39zz97ljvkxr2c4gxzp0jc4i6l7371q9qqmgvfm2pbyf21nah")))

(define-public crate-kaspa-pow-0.13.4 (c (n "kaspa-pow") (v "0.13.4") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "js-sys") (r "=0.3.64") (d #t) (k 0)) (d (n "kaspa-consensus-core") (r "^0.13.4") (d #t) (k 0)) (d (n "kaspa-hashes") (r "^0.13.4") (d #t) (k 0)) (d (n "kaspa-math") (r "^0.13.4") (d #t) (k 0)) (d (n "kaspa-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.87") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "workflow-wasm") (r "^0.10.3") (d #t) (k 0)))) (h "05lp2r4dblk3gjgqn5cc3fwika4nnz90wb4ywfa0pvrq5b4s036c")))

