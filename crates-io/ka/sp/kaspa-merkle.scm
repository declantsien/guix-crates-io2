(define-module (crates-io ka sp kaspa-merkle) #:use-module (crates-io))

(define-public crate-kaspa-merkle-0.0.0 (c (n "kaspa-merkle") (v "0.0.0") (h "0x6p0ah4ip2i2jlha618i83n2rc30lg0iamq5srq0z141zqn7ijd")))

(define-public crate-kaspa-merkle-0.0.1 (c (n "kaspa-merkle") (v "0.0.1") (d (list (d (n "kaspa-hashes") (r "^0.0.1") (d #t) (k 0)))) (h "091k5x8kyvwqqdq3g995bz82d2vmc8mgbcaxdv0qrfdssri1s9sd")))

(define-public crate-kaspa-merkle-0.0.2 (c (n "kaspa-merkle") (v "0.0.2") (d (list (d (n "kaspa-hashes") (r "^0.0.2") (d #t) (k 0)))) (h "0ablsdsvbnklmxqj2hx0324l0lkgw969y9jvb0iybxvrfh0a09k5")))

(define-public crate-kaspa-merkle-0.0.3 (c (n "kaspa-merkle") (v "0.0.3") (d (list (d (n "kaspa-hashes") (r "^0.0.3") (d #t) (k 0)))) (h "1jfh94kyvpbbc2jxfj6q6gy9hy1n3z6cfzi0zhc53bfyf0frqk1i")))

(define-public crate-kaspa-merkle-0.0.4 (c (n "kaspa-merkle") (v "0.0.4") (d (list (d (n "kaspa-hashes") (r "^0.0.4") (d #t) (k 0)))) (h "0nwxgqj359yxnv7sj6kh13pzja5hwbb8fwbc2s1jw1hrl8xkgqb8")))

(define-public crate-kaspa-merkle-0.13.4 (c (n "kaspa-merkle") (v "0.13.4") (d (list (d (n "kaspa-hashes") (r "^0.13.4") (d #t) (k 0)))) (h "0rkz5l7qma58l6qfg727l4x0wmvcqxldmk5vis2hnibvy6h4j7pn")))

