(define-module (crates-io ka sp kaspa-utils-tower) #:use-module (crates-io))

(define-public crate-kaspa-utils-tower-0.0.0 (c (n "kaspa-utils-tower") (v "0.0.0") (h "0r2kdw8g9a5zhyl9l3k90l7sfj66agcgjx6n5hp9ab1qmkqyyx8g")))

(define-public crate-kaspa-utils-tower-0.13.4 (c (n "kaspa-utils-tower") (v "0.13.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("sync" "rt-multi-thread"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tower") (r "^0.4.7") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tower-http") (r "^0.4.4") (f (quote ("map-response-body" "map-request-body"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0sxbny1wn16056qjpwvr06yl2gw96ig80p3qvff4qmcz35yizp95")))

