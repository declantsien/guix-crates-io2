(define-module (crates-io ka sp kaspa-ng-macros) #:use-module (crates-io))

(define-public crate-kaspa-ng-macros-0.0.0 (c (n "kaspa-ng-macros") (v "0.0.0") (h "0ph1dpk5ls2vqaydh4w757rlhhw36pqh8n334dq7s08wn0ajsqsp")))

(define-public crate-kaspa-ng-macros-0.2.3 (c (n "kaspa-ng-macros") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "parse-variants") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "fold" "extra-traits" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "11ahsld1qy09c6dsac2l8yx54ffi78hpwqaasc86zla7jwc041nx")))

