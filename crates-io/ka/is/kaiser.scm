(define-module (crates-io ka is kaiser) #:use-module (crates-io))

(define-public crate-kaiser-0.1.0 (c (n "kaiser") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.12") (d #t) (k 0)))) (h "1vjl7ms2pqghr0wx1b0ly6pcnkpx748751pj8k8r2qian8x86bg8")))

(define-public crate-kaiser-0.1.1 (c (n "kaiser") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.12") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)))) (h "1z2hfk1r7qrhzdsfmv4hpdc70m4263g5mhb7j7jrf47g8pbfy2bm")))

(define-public crate-kaiser-0.1.2 (c (n "kaiser") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.12") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)))) (h "0yrab0ismxyb2hbq3n2sgix9275jykbz28g5a9pxlij0lqynbd2y")))

(define-public crate-kaiser-0.1.3 (c (n "kaiser") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.1.3") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.12") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)))) (h "0xd9nn0xvq1y42ajnqibz2bw7p3cw2n8qp8fgk6r4wxxbqx5hn4m")))

