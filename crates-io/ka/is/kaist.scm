(define-module (crates-io ka is kaist) #:use-module (crates-io))

(define-public crate-kaist-0.1.0 (c (n "kaist") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0lqikil5lvq9lmcc0n6aih51hrm9lk515b614zv1n63sihnyrcgd")))

(define-public crate-kaist-0.1.1 (c (n "kaist") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0ybq7rps6xfvwj2kyvd248q9nw1hvp6bv9dff7mli7wklpqjd15a")))

