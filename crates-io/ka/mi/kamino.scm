(define-module (crates-io ka mi kamino) #:use-module (crates-io))

(define-public crate-kamino-1.0.0 (c (n "kamino") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1j0md3ilnwychckwbcx3zcwhfxzyhzfaw9l1b0bd4l8c3n41r8z2") (f (quote (("static_ssl" "openssl/vendored"))))))

(define-public crate-kamino-1.1.0 (c (n "kamino") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1xsypijnwq8fid155f9mr5gmrmdnzxyvpi5pfmk2c0aa3wb1spj5") (f (quote (("static_ssl" "openssl/vendored"))))))

(define-public crate-kamino-1.1.1 (c (n "kamino") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "openssl") (r "^0") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "15any0pfm0f5j3gnxq91lricpvj0wh08wg3dmjr3aainvydhkxp8") (f (quote (("static_ssl" "openssl/vendored"))))))

