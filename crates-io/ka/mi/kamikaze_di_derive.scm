(define-module (crates-io ka mi kamikaze_di_derive) #:use-module (crates-io))

(define-public crate-kamikaze_di_derive-0.1.0 (c (n "kamikaze_di_derive") (v "0.1.0") (d (list (d (n "kamikaze_di") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qrqw8y7jhmavf77rdryhski918597hcx7kj9lxpkh2yvabcrv7i") (f (quote (("logging") ("default"))))))

