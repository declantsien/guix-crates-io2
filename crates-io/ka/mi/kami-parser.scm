(define-module (crates-io ka mi kami-parser) #:use-module (crates-io))

(define-public crate-kami-parser-0.1.0 (c (n "kami-parser") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q63ligv0ynfwh1rw4xrwahpizvyvyhqg95i0ldd4p3xm8n1lcn5")))

(define-public crate-kami-parser-0.2.0 (c (n "kami-parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bjhfcp6cg5h3gszawzqi93bnclw93w68sy64br1lhhyzfqjwpnj")))

(define-public crate-kami-parser-0.3.0 (c (n "kami-parser") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1d4vz47b8i91jxf0kzmqhifdzxr1kl3c92km09h16jgca3ww9mcd")))

(define-public crate-kami-parser-0.3.1 (c (n "kami-parser") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1by7kk8xa2aa5xm3iq40f3c9b1kcw5cxv02209k19zz3sbsnyy6a")))

(define-public crate-kami-parser-0.3.2 (c (n "kami-parser") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pj05bvapyn5c3y1i00xdrck7p4icd7hp93xcs3xpai5kk1pzgzr")))

(define-public crate-kami-parser-0.4.0 (c (n "kami-parser") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0vfn77djcxff1x73qvyhc0m5zi841pi5n91bg0f7fk57w8v8h17v")))

(define-public crate-kami-parser-0.4.1 (c (n "kami-parser") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wwz0i15n0rb0j74ls04vsy3bc2b74phhyl781sm9k7v1syw547a")))

(define-public crate-kami-parser-0.4.2 (c (n "kami-parser") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hlpnpg1hqvahjzxrk9k2av6pccwr95kbld9fmp5l6h31rz3iv7z") (y #t)))

(define-public crate-kami-parser-0.4.3 (c (n "kami-parser") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19wfg56d78mkidyrsin5s6yj741kx4lmbgiilpbij9bx9xlwxp1z")))

(define-public crate-kami-parser-0.4.4 (c (n "kami-parser") (v "0.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fdaa5hrxqzqnh29dlaziwrkyrqldl0n7pn6q19jg99q7wjfqfha")))

(define-public crate-kami-parser-0.4.5 (c (n "kami-parser") (v "0.4.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "11jfb272vdavd1h4w7s9h3k80yrxni63x31zglqynryga7s3xdgz")))

(define-public crate-kami-parser-0.5.0 (c (n "kami-parser") (v "0.5.0") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hpd2lqr99mfwyhhyjiwkyy930g4qc1ly86hzsbwm387h23d00w6")))

(define-public crate-kami-parser-0.5.1 (c (n "kami-parser") (v "0.5.1") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0m39b270pk0qrxc7qr4krh38k0bl0gn02kp2lhxh0jak7g02cbqk")))

(define-public crate-kami-parser-0.5.2 (c (n "kami-parser") (v "0.5.2") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0kx808147fysravwj2r1vz2vi4sfp4a9zwi8626zp36cdgkfgcaa")))

(define-public crate-kami-parser-0.5.3 (c (n "kami-parser") (v "0.5.3") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19x0w9kwrph2r01k7c97b4ldalf1bv74148dpr5bpfphc68jyj1q")))

(define-public crate-kami-parser-0.6.0 (c (n "kami-parser") (v "0.6.0") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1amxr6d2xz1ln54m5fmb1h63b4m17g46p6mv7mm9jn9m29f63k3i")))

(define-public crate-kami-parser-0.6.1 (c (n "kami-parser") (v "0.6.1") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0rx2253pbk7xp3509zrhgd7nhfj96ia3x78ip1wdrm4vyyk59qnc")))

(define-public crate-kami-parser-0.6.2 (c (n "kami-parser") (v "0.6.2") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13f6ibl8yxmmpx7dvcb6j1hcm6l3wfn0hzgnsbqzkma8hm71pw52")))

(define-public crate-kami-parser-0.6.3 (c (n "kami-parser") (v "0.6.3") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ljp15icx8hd2hz95qshffwc8np3w904dwnjjkig2vrsx4476bwh")))

(define-public crate-kami-parser-0.6.4 (c (n "kami-parser") (v "0.6.4") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qpbn9ra213i3jk6f57234zpm17lwzcwzjv7d451w65cqn53mpck")))

(define-public crate-kami-parser-0.7.0 (c (n "kami-parser") (v "0.7.0") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1v6iv8y66drx2wify2va3cq77slzxkwcxnw4paqwm988isq9liav")))

(define-public crate-kami-parser-0.8.0 (c (n "kami-parser") (v "0.8.0") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1lf9lc7yqlmccshr4nx7lg8bdmsmpcg660g7gm52vz2w2vfbgaaf")))

(define-public crate-kami-parser-0.8.1 (c (n "kami-parser") (v "0.8.1") (d (list (d (n "htmlentity") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "00nvf3r97l1m3pqdgs4rb4gmb2h0n31pmfafmfljrpab3nj3lzjf")))

