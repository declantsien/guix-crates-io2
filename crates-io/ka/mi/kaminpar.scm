(define-module (crates-io ka mi kaminpar) #:use-module (crates-io))

(define-public crate-kaminpar-0.1.0 (c (n "kaminpar") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)))) (h "05dpaklby0m3dx4yl3hcnik1h2bpq93fq9afx8ryhlgb09r3bbw1") (l "kaminpar")))

(define-public crate-kaminpar-0.2.0 (c (n "kaminpar") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "039632yarsn9zlv9x0xm78rq19hf60zwh767d3gap4j1zffdj7bw") (l "kaminpar")))

