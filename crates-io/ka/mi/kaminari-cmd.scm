(define-module (crates-io ka mi kaminari-cmd) #:use-module (crates-io))

(define-public crate-kaminari-cmd-0.3.1 (c (n "kaminari-cmd") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "0qxvjrh6cy77471zzqfnfqnm4kvsa14l7z3z7hmy4ky1dbhdm4qq")))

(define-public crate-kaminari-cmd-0.3.2 (c (n "kaminari-cmd") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "0w4ylagbv2a32w27mnhbns9fnbpv5yxnlvvlazv9nncbsvnffj34")))

(define-public crate-kaminari-cmd-0.4.0 (c (n "kaminari-cmd") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "1g837j1id4cslzsmayjlfca66qx6xpnbklqv5b84klch7qcr6z8g")))

(define-public crate-kaminari-cmd-0.4.1 (c (n "kaminari-cmd") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "1bd3jhiqq5shvy4aw1lqmbjhsk4dgcvmr13wz883h02af4rv5jaf")))

(define-public crate-kaminari-cmd-0.4.2 (c (n "kaminari-cmd") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "0kjick4wnyjbwfqs24hbnfx8sirazzss65bifcn85jqvr9hi1aw3")))

(define-public crate-kaminari-cmd-0.4.3 (c (n "kaminari-cmd") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "0sxw9s8gilylwlhwk0n0hb22hqilg8jfd42cvnmrp1dqaaryfyd6")))

(define-public crate-kaminari-cmd-0.4.4 (c (n "kaminari-cmd") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "macros" "rt-multi-thread" "io-util"))) (d #t) (k 0)))) (h "0dczklvxavc5w5zvh7grjv4ra6l7cbpi5652g70d6f7yvx7q9bni")))

(define-public crate-kaminari-cmd-0.5.0 (c (n "kaminari-cmd") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.7.0") (d #t) (k 0)) (d (n "realm_io") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "1ns86h5mb450j0a7xkc913788xr8hq073zfl690ya2ny9p8pb6vv")))

(define-public crate-kaminari-cmd-0.5.1 (c (n "kaminari-cmd") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.7.1") (d #t) (k 0)) (d (n "realm_io") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "0sgf91j28x15llp9x97acvbywm4wfp9bwdi13hwm0nyi5z59ipwl")))

(define-public crate-kaminari-cmd-0.5.3 (c (n "kaminari-cmd") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.8.3") (d #t) (k 0)) (d (n "realm_io") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "1j7vgg2wddsav9lfxv4yxkrm0vyk9zxbk9yry3nwnsfiqc50y5vv")))

(define-public crate-kaminari-cmd-0.5.4 (c (n "kaminari-cmd") (v "0.5.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.9.0") (f (quote ("ws" "tls"))) (d #t) (k 0)) (d (n "realm_io") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "18zq5dlvxcmn19qxkad3k4z174rmzmr9mj0ifmlpm4zfhr22bxfv")))

(define-public crate-kaminari-cmd-0.5.5 (c (n "kaminari-cmd") (v "0.5.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.9.1") (f (quote ("ws" "tls"))) (d #t) (k 0)) (d (n "realm_io") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "1lkfnc8wgxl8ddx3v70wfjksspyfr2g88svnp42sxx98sjyn4yfz")))

(define-public crate-kaminari-cmd-0.5.6 (c (n "kaminari-cmd") (v "0.5.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.9.2") (f (quote ("ws" "tls"))) (d #t) (k 0)) (d (n "realm_io") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "1nkdrfsfm4wgv3rh271hbcsvx46v7faqnd43x5n6k88fkl5qfila")))

(define-public crate-kaminari-cmd-0.6.0 (c (n "kaminari-cmd") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "kaminari") (r "^0.12") (f (quote ("ws"))) (d #t) (k 0)) (d (n "realm_io") (r "^0.4.0") (d #t) (k 0)) (d (n "realm_syscall") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("rt" "net" "macros"))) (d #t) (k 0)))) (h "1vdvk74zg1vl9f97znbykm7mjafvp02f0cy0kp7830qrci97azf4") (f (quote (("tls-rustls" "kaminari/tls") ("tls-openssl") ("default" "tls-rustls"))))))

