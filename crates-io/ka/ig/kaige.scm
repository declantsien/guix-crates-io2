(define-module (crates-io ka ig kaige) #:use-module (crates-io))

(define-public crate-kaige-0.0.1 (c (n "kaige") (v "0.0.1") (d (list (d (n "kaige_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "kaige_renderer") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08xxbmkxia49jk3msam4djmfh8qy35qk0l2ghnhsp7mr2yik2303")))

