(define-module (crates-io ka lc kalc) #:use-module (crates-io))

(define-public crate-kalc-0.1.0 (c (n "kalc") (v "0.1.0") (h "1bxkxs1gbl3yyd9jmslb78fgijsldlc2az48hm152av0m8754j84") (y #t)))

(define-public crate-kalc-0.1.1 (c (n "kalc") (v "0.1.1") (h "0i334qa1qg88jb0k6gngak86xi8x19gsx241hbjpw52rw9qpzla9") (y #t)))

(define-public crate-kalc-0.1.2 (c (n "kalc") (v "0.1.2") (h "1y37x6jzdl3qwrs1g3n5sfxbpycpwmbvy9d0zqzvxprgs3nb98yy") (f (quote (("default") ("debug")))) (y #t)))

(define-public crate-kalc-0.1.3 (c (n "kalc") (v "0.1.3") (h "0hsx7kglj0xwi15ld0fvj50fdgqhs8ihsh8q8yc120d37w2mczn6") (f (quote (("default") ("debug")))) (y #t)))

(define-public crate-kalc-0.1.4 (c (n "kalc") (v "0.1.4") (h "1lvq0ad5fslkswvfnyf8lg3dv26nly2zjiyd0fn4yj5g4rw97gw8") (f (quote (("default") ("debug")))) (y #t)))

(define-public crate-kalc-0.7.4 (c (n "kalc") (v "0.7.4") (d (list (d (n "console") (r "^0.15.7") (t "cfg(not(unix))") (k 0)) (d (n "gnuplot") (r "^0.0.38") (k 0)) (d (n "libc") (r "^0.2.147") (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.19.2") (f (quote ("complex" "integer"))) (k 0)) (d (n "term_size") (r "^0.3.2") (t "cfg(not(unix))") (k 0)))) (h "13nzbnffv1bjxlpqx410qv8vm4bl49x9d06f7jfg0zi918zghwpm")))

(define-public crate-kalc-0.8.0 (c (n "kalc") (v "0.8.0") (d (list (d (n "console") (r "^0.15.7") (t "cfg(not(unix))") (k 0)) (d (n "gnuplot") (r "^0.0.38") (k 0)) (d (n "libc") (r "^0.2.147") (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.19.2") (f (quote ("complex" "integer"))) (k 0)) (d (n "term_size") (r "^0.3.2") (t "cfg(not(unix))") (k 0)))) (h "1ivmjlkcgi0q9ss5kx0d9kxx5rx5hi26v10mmv7i8lrnx4c26y8d")))

(define-public crate-kalc-0.8.1 (c (n "kalc") (v "0.8.1") (d (list (d (n "console") (r "^0.15.7") (t "cfg(not(unix))") (k 0)) (d (n "gnuplot") (r "^0.0.39") (k 0)) (d (n "libc") (r "^0.2.147") (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.19.2") (f (quote ("complex" "integer"))) (k 0)) (d (n "term_size") (r "^0.3.2") (t "cfg(not(unix))") (k 0)))) (h "0yxmfjwr4b1n1w42sjkfimr2xmwz54zc9006hfqfknifh73fq1jy")))

(define-public crate-kalc-0.8.2 (c (n "kalc") (v "0.8.2") (d (list (d (n "console") (r "^0.15.7") (k 0)) (d (n "gnuplot") (r "^0.0.39") (k 0)) (d (n "libc") (r "^0.2.147") (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.19.2") (f (quote ("complex" "integer"))) (k 0)) (d (n "term_size") (r "^0.3.2") (t "cfg(not(unix))") (k 0)))) (h "0y8yvghlm4m3z0a9cxrd7324mcn9qy5f0h9knk2pl8kq61mibr0y")))

(define-public crate-kalc-0.8.3 (c (n "kalc") (v "0.8.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.20.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0m537k91lpl68l9vhcvqpnx469vwrwv396mp4573sqvpdivqfr68")))

(define-public crate-kalc-0.8.4 (c (n "kalc") (v "0.8.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.20.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "11alkk7vp2rvg71z8xacsncs8iv1kilwak4154xk80aa8fyss5hw")))

(define-public crate-kalc-0.8.5 (c (n "kalc") (v "0.8.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0v4mjd0jlmfj6yv2yb7rhlxsmfmd9ali9rsv5hlh128asv01z0za")))

(define-public crate-kalc-0.9.0 (c (n "kalc") (v "0.9.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1nil3n2541k7jygb02kbwc2vpip0h96n2wsk2zp032n56r5df59r")))

(define-public crate-kalc-0.9.1 (c (n "kalc") (v "0.9.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0sgw79446nlrsjy9ffycjk0vngp3awsvrghpiwz4iffjqg1zsi1j")))

(define-public crate-kalc-0.9.2 (c (n "kalc") (v "0.9.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1bklvfh5fmavwb29wr6f9l84zfglgn01afc6vs4f44ywplr6yihv")))

(define-public crate-kalc-0.9.3 (c (n "kalc") (v "0.9.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.41") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1cs7in73ja4bd5jbaawkmk9c0wnagvrdlqk27yqb604qk4c7gld8")))

(define-public crate-kalc-1.0.0 (c (n "kalc") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1qkr2x3cjb7w7a96haiqj2k074mgdj9q5qc258wc92m13bcjxsds")))

(define-public crate-kalc-1.0.1 (c (n "kalc") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0hazsa854pcayamzr1w7czg4y05py8dkz2287p43ja4d86jd1660")))

(define-public crate-kalc-1.0.2 (c (n "kalc") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1880yr9dwlmlf63b0286zggvqb73w3m5gmmfz1jz2a0mcl6rbcpc")))

(define-public crate-kalc-1.0.3 (c (n "kalc") (v "1.0.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "14wxmax1bzl08halrfirchp0523di0dix18y2mv413sng2nmjjdd")))

(define-public crate-kalc-1.0.4 (c (n "kalc") (v "1.0.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.42") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1x8r2ifyn4qxfj5bzaqnm1cv81fkk8mn1f3sp31dn8llxf19ckjp")))

(define-public crate-kalc-1.0.5 (c (n "kalc") (v "1.0.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0n19xfb6m3lsiz489n2rmi9il24fl8rvc269mxp7dg6647nps4yg")))

(define-public crate-kalc-1.1.0 (c (n "kalc") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1bw4nq8sb68p9s4nms4kba17m7dd2xa8378fvvq23lpjpvv4zdj7")))

(define-public crate-kalc-1.1.1 (c (n "kalc") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0c46dp3drbq03q14cvlncjy0mqb8vz4z3f0ixh7mmqnk6bqsnxz0")))

(define-public crate-kalc-1.1.2 (c (n "kalc") (v "1.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1pj6qgnm6iw02m2pa68h69aq7a5r7c5bahlik8i6p4hds0jyvfmr")))

(define-public crate-kalc-1.1.3 (c (n "kalc") (v "1.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "09x8ap72bqp8spnspjir9v2bzms8m0ymm9973x1y83klvkis3lgf")))

(define-public crate-kalc-1.1.4 (c (n "kalc") (v "1.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "1il8gpbs6k0zk1dczgc5nj7a0ravhr7gjxbdi9m0ljzcir3b8bi4")))

(define-public crate-kalc-1.1.5 (c (n "kalc") (v "1.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.43") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(unix)") (k 0)) (d (n "rug") (r "^1.24.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (t "cfg(not(unix))") (k 0)))) (h "0cnpmkd7q39ilrd7m5ik4xdvd3f77zj8kp3s350knpq8hd8mgbfr")))

