(define-module (crates-io ka lo kalosm-learning-macro) #:use-module (crates-io))

(define-public crate-kalosm-learning-macro-0.1.0 (c (n "kalosm-learning-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1f2zgamhwfmgw3jm2n2jlq0b890aq17dz25gidf139825pijchi0")))

(define-public crate-kalosm-learning-macro-0.2.0 (c (n "kalosm-learning-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0r590my2mhikrc2vqqv82iq6mbzln9i5xrszz7rqkal0hl2rvx7n")))

(define-public crate-kalosm-learning-macro-0.2.1 (c (n "kalosm-learning-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vq9zagmafs79y5942m5kr8ghsr13pkv3z1fjl8lni9lv9qkqy12")))

