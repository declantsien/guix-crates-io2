(define-module (crates-io ka lo kalosm-streams) #:use-module (crates-io))

(define-public crate-kalosm-streams-0.1.0 (c (n "kalosm-streams") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pxxs576slk5ygdiq4n487cwql8faxwnbb1m2w2vqr4x9mnfggdv")))

(define-public crate-kalosm-streams-0.2.0 (c (n "kalosm-streams") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0b8gw9c81wwv7wv26s7hlwfzl7y9l7w31dcrhfyrhhsijzkjysn3")))

(define-public crate-kalosm-streams-0.2.1 (c (n "kalosm-streams") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0kdcvnlxv7avnm22gy1vh8pwdw21pr3q92wajbk5f5wmfiqk5vcs")))

