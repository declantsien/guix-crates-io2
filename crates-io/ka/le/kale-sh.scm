(define-module (crates-io ka le kale-sh) #:use-module (crates-io))

(define-public crate-kale-sh-0.0.1 (c (n "kale-sh") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)))) (h "04vzck8fd4q9lns24j70l68irxc069jcyf7byg46vgvhplrdpaiw")))

