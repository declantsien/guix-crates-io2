(define-module (crates-io ka le kaleidoscope-focus-cli) #:use-module (crates-io))

(define-public crate-kaleidoscope-focus-cli-0.1.0 (c (n "kaleidoscope-focus-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("std" "derive" "env" "help" "usage"))) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "kaleidoscope-focus") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wn0n1swm0f50nciqhd4a3ixn0apmzcqmqz0lrnkk19riframqzc") (r "1.59.0")))

