(define-module (crates-io ka ya kayak_ui_macros) #:use-module (crates-io))

(define-public crate-kayak_ui_macros-0.1.0 (c (n "kayak_ui_macros") (v "0.1.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11l6kmikmzqjrabgv8nk4csvz80mw2in0lg0vg87b3bsbnxdm42z")))

(define-public crate-kayak_ui_macros-0.2.0 (c (n "kayak_ui_macros") (v "0.2.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00cnp1gwjbng6jaacklj78r1nwjr3ii0xs1lmn1w1k1va7dspiik")))

(define-public crate-kayak_ui_macros-0.3.0 (c (n "kayak_ui_macros") (v "0.3.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ak6gfvyfbmlsvykm2v94chfq60x9g0kqs8mkf3pl2w003vaadz")))

(define-public crate-kayak_ui_macros-0.4.0 (c (n "kayak_ui_macros") (v "0.4.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13ddz5ycdl6hw57fdkjg7h413syd1ccf3j0qn6pzgfg8y7af0cib")))

(define-public crate-kayak_ui_macros-0.5.0 (c (n "kayak_ui_macros") (v "0.5.0") (d (list (d (n "find-crate") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pj7i7b1fvdllwylm3g6d17rakgjpyd2aqi78gismmgf6yc27a40")))

