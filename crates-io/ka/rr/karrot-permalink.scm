(define-module (crates-io ka rr karrot-permalink) #:use-module (crates-io))

(define-public crate-karrot-permalink-0.1.0 (c (n "karrot-permalink") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0nw7pjglm0v0wcbhbkkikc624cipyl5k145jnn844al6vc0m0m7f")))

(define-public crate-karrot-permalink-0.2.0 (c (n "karrot-permalink") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "170dqr4q8b2v7cm9px60xd5py2cc0vknpg4hdks9g2wphjvx7g4x")))

(define-public crate-karrot-permalink-0.3.0 (c (n "karrot-permalink") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0ly6invck4q9xk56si3sdd4ixs8illqk2cg00qc8k4iiqai6n666")))

