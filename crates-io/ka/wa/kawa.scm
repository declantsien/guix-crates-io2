(define-module (crates-io ka wa kawa) #:use-module (crates-io))

(define-public crate-kawa-0.6.1 (c (n "kawa") (v "0.6.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "12f6839laywbasrsam9j3640vq2idghn4w2mpfjh5qmqrs2ikgkf") (f (quote (("rc-alloc")))) (r "1.66.1")))

(define-public crate-kawa-0.6.2 (c (n "kawa") (v "0.6.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1xnarffzqn03114qk8g58ydrlqfvs0p06164bfz29pg4fcin24q7") (f (quote (("rc-alloc") ("custom-vecdeque")))) (r "1.66.1")))

(define-public crate-kawa-0.6.3 (c (n "kawa") (v "0.6.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "109xasyrga7lw2g6vrjpr1nbrmwjf3i0dkpal1z23wqaww65yprg") (f (quote (("simd") ("rc-alloc") ("default" "simd") ("custom-vecdeque")))) (r "1.66.1")))

(define-public crate-kawa-0.6.4 (c (n "kawa") (v "0.6.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0l6bnbv37yg9n5f09z2v08f2n01j167cph5xirgkyrfribyc4ji8") (f (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing") ("custom-vecdeque")))) (r "1.66.1")))

(define-public crate-kawa-0.6.5 (c (n "kawa") (v "0.6.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1q5r97wv44fps1xzxkl0r4qizrpd7ikcnyrp4vqxi1difdf6k1fv") (f (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing") ("custom-vecdeque")))) (r "1.66.1")))

(define-public crate-kawa-0.6.6 (c (n "kawa") (v "0.6.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1fgmznj08s0gwly846zwyj47gdc35d3n80qhbp186i924piq95gq") (f (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing" "rc-alloc") ("custom-vecdeque")))) (r "1.66.1")))

