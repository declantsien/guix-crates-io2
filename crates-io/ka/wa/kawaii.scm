(define-module (crates-io ka wa kawaii) #:use-module (crates-io))

(define-public crate-kawaii-0.1.0-alpha (c (n "kawaii") (v "0.1.0-alpha") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "1chs6rjp9zbxnkldrij5whasn6fmnfxhljddrji3jbx4761d0dgy") (f (quote (("unstable"))))))

