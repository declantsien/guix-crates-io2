(define-module (crates-io ka mo kamo-macros) #:use-module (crates-io))

(define-public crate-kamo-macros-0.1.0 (c (n "kamo-macros") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "195p7pcvg5swxqij0l5i7ncfasxm5x77xl51rpzlphc8ch6scmxb")))

(define-public crate-kamo-macros-0.1.1 (c (n "kamo-macros") (v "0.1.1") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1gs5w395ql2fh9n5di4y0aw80xhcalnysxv5jx5ri7vlxc1nq2hg")))

(define-public crate-kamo-macros-0.1.2 (c (n "kamo-macros") (v "0.1.2") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "00w7hgc4d2mlhj1a4dqsklsas6af5ypdv824l2z9i77h38g3kpqm")))

(define-public crate-kamo-macros-0.1.4 (c (n "kamo-macros") (v "0.1.4") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1b6fygrmw50xmvb0g3hr49s4rllj6zhw0l91f6bv54dc4hb1cpzj")))

(define-public crate-kamo-macros-0.1.5 (c (n "kamo-macros") (v "0.1.5") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0lqs9wabxm0lg4a8yhrkhq411ykmq7j647b0njkhfc1pv8kwlx25")))

(define-public crate-kamo-macros-0.1.6 (c (n "kamo-macros") (v "0.1.6") (d (list (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "12hm54kwqqglwbqhag4hirrf313k4hi17i4kg6xq89x2x89cpryg")))

