(define-module (crates-io ka tt katte-lib-rs) #:use-module (crates-io))

(define-public crate-katte-lib-rs-0.1.0 (c (n "katte-lib-rs") (v "0.1.0") (h "13mkarckwh520qdw8i1bajl6gbscb0kbbg1rda58vaf9hxxrb1r9")))

(define-public crate-katte-lib-rs-0.1.1 (c (n "katte-lib-rs") (v "0.1.1") (h "1gdanx1bdxlg0whimsf4ba3sfcxi6gd35vvxkc1c0d8zg2z42rmy") (y #t)))

