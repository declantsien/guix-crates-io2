(define-module (crates-io ka ze kaze) #:use-module (crates-io))

(define-public crate-kaze-0.1.0 (c (n "kaze") (v "0.1.0") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0cqdvgnsn2i3pbdxbcc7bhm14y8r3bcnysazn56bry1khb3gpnw9")))

(define-public crate-kaze-0.1.1 (c (n "kaze") (v "0.1.1") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "03xkrxxxpfhky397qw6m5z3jv2ys64rj0j16s0r2yzjq1hh26mn4")))

(define-public crate-kaze-0.1.2 (c (n "kaze") (v "0.1.2") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "1j3844mvs9pmsgg3nirs9c9kb8qmxg86zqhqk3sj29xgsyws13dj")))

(define-public crate-kaze-0.1.3 (c (n "kaze") (v "0.1.3") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "14j36l0rpxgfhya3lkpk3dnhs9b2cwbn13wr34a81qy1y6fhi01h")))

(define-public crate-kaze-0.1.4 (c (n "kaze") (v "0.1.4") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0dr3k08f5m4vpax30p1bdkb7lpikhh91m8p6ykj442fgg93kcpza")))

(define-public crate-kaze-0.1.5 (c (n "kaze") (v "0.1.5") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0gcl5br1z4kzi7x7p7bk7yvrgm162n075zn14s4dg8xh4b945637")))

(define-public crate-kaze-0.1.6 (c (n "kaze") (v "0.1.6") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "04k9q2s2rfim7sbiwg3jf5f3721nfdxhz85pdsxlgkcxr8gbbn8r")))

(define-public crate-kaze-0.1.7 (c (n "kaze") (v "0.1.7") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "044lqzpyf7y9nci76inpb4caysr9hcsnlz6401ba60w61vv58dmm")))

(define-public crate-kaze-0.1.8 (c (n "kaze") (v "0.1.8") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0iyn6bmlszii6pkd9zwwyavqi2qrwrh40crnmsk05lxvy95sv997")))

(define-public crate-kaze-0.1.9 (c (n "kaze") (v "0.1.9") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "1nbwfj8n6h0k8d2wwa5rqa61r6pqwj7f56v6fa0nhm3kjl30l6hk")))

(define-public crate-kaze-0.1.10 (c (n "kaze") (v "0.1.10") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "0vlkyl1751wfmniqlach8jbz5id5ijssq704libdzlbi5i13xxsb")))

(define-public crate-kaze-0.1.11 (c (n "kaze") (v "0.1.11") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "1j2asac5yqsfwabz5rgywhwciygqf0l6g8b06knxw1l72wd54vxs")))

(define-public crate-kaze-0.1.12 (c (n "kaze") (v "0.1.12") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)))) (h "182377zkrms8vzlcwjzap5qyc06fbicm5y3vlxbp7ngs4ig96zlr")))

(define-public crate-kaze-0.1.13 (c (n "kaze") (v "0.1.13") (d (list (d (n "typed-arena") (r "^2.0.0") (d #t) (k 0)) (d (n "vcd") (r "^0.6") (d #t) (k 0)))) (h "1h614s7q385yvjnflzhv04nzmb2ha7zfgvfnd873mzy94dh6p1md")))

(define-public crate-kaze-0.1.14 (c (n "kaze") (v "0.1.14") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "0020gq93ylgm1n3v7fzx3kxkbylyglf0iwsawsrlacfxbww83yg6")))

(define-public crate-kaze-0.1.15 (c (n "kaze") (v "0.1.15") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "0rmp02klbm1vpkpw7n17sp5r053rs2sly37rs1fk9hak50wsris8")))

(define-public crate-kaze-0.1.16 (c (n "kaze") (v "0.1.16") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "0jh71zc6aggvfj4xscr9gy6gcqs26yywhsgh6ww9782araffiqms")))

(define-public crate-kaze-0.1.17 (c (n "kaze") (v "0.1.17") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "1n4wf9dqc9a91w14329iglqp6mqbv149bbbh5qy205hg3yss62fb")))

(define-public crate-kaze-0.1.18 (c (n "kaze") (v "0.1.18") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "02g9jnljxjbr1ah221bs2rawkdz4piswwm64hw3mj60xn7cpx7zg")))

(define-public crate-kaze-0.1.19 (c (n "kaze") (v "0.1.19") (d (list (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 0)))) (h "0lk2bjk2zjcx118wcmg8dnhy1nl2n5244fpfbi2wmscx5nk344z6")))

