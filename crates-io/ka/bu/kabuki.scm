(define-module (crates-io ka bu kabuki) #:use-module (crates-io))

(define-public crate-kabuki-0.0.1 (c (n "kabuki") (v "0.0.1") (h "15m0h51q13y6cq13zaqm3nlhw43jzadjx00ys8l44z6m0bcx2pyl")))

(define-public crate-kabuki-0.0.2 (c (n "kabuki") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-mpsc") (r "^0.1") (d #t) (k 0)) (d (n "futures-spawn") (r "^0.1") (f (quote ("use_std" "tokio"))) (k 0)) (d (n "futures-threadpool") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0cas7d77wz9ncxx38c6q3j80mvp5mxavbry4k23qsinfndrn6kpf")))

