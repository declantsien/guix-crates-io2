(define-module (crates-io ka nj kanji) #:use-module (crates-io))

(define-public crate-kanji-1.0.0 (c (n "kanji") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1m0hn50pvmg55sbfzz15dxb609hsksgajy37vidpg88fzdfd1jys")))

(define-public crate-kanji-1.0.1 (c (n "kanji") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "13ahnf6fjai5ldz2yvzmkai0y7crxli1dfi339avzdw8181j6sgq")))

(define-public crate-kanji-1.1.0 (c (n "kanji") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10517l2l9bsxs5v82kar358h0z43dxn74d2y04zdd6hvannmij81")))

(define-public crate-kanji-2.0.0 (c (n "kanji") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1cr0z1g6w26nm0wawriqns1skkafsv7jlf8v04gksdls1ii4c92q")))

