(define-module (crates-io ka nj kanjidic_types) #:use-module (crates-io))

(define-public crate-kanjidic_types-0.1.0 (c (n "kanjidic_types") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zni4ycxkgmpzg9y8y8rjf6lr6yx2aajwnhjl3lmn5a601lmg7xq")))

(define-public crate-kanjidic_types-0.1.1 (c (n "kanjidic_types") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1byby9ywqqp8a1wipzhgimv6x073k39199kjq21ban2c2q6lj903")))

(define-public crate-kanjidic_types-0.1.2 (c (n "kanjidic_types") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0q9hyql1f0qgld83iikawrnq9mjglf2c9xry9fcxfnx33ih8xljq")))

(define-public crate-kanjidic_types-0.1.3 (c (n "kanjidic_types") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0w365jcvwbm24qrxyg9a5sxvl2a6d9nzbzxc6hxkqfgfr7gg02gj")))

(define-public crate-kanjidic_types-0.1.4 (c (n "kanjidic_types") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "006sdbgyp72jsy8h4blq57nrygbpc3b0xf4s5pzznm3dspaif5yf")))

