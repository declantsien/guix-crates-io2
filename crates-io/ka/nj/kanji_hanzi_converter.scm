(define-module (crates-io ka nj kanji_hanzi_converter) #:use-module (crates-io))

(define-public crate-kanji_hanzi_converter-0.1.0 (c (n "kanji_hanzi_converter") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1hf3lss211ldmb8dzwq82s1r0x5zj7znqd5bxb9z95pj74wcyb27")))

(define-public crate-kanji_hanzi_converter-0.2.0 (c (n "kanji_hanzi_converter") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1kqb8cjjkyqd6ws99n8ddpk5jf0ab9w1sbqahcqk3s34d57wxpam")))

