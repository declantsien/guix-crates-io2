(define-module (crates-io ka nj kanji-number-parser) #:use-module (crates-io))

(define-public crate-kanji-number-parser-0.1.0 (c (n "kanji-number-parser") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0933a6a2iqdyx6xmg47qv63w0ynsj7s3pcykyg317y6z2sayqv2m")))

