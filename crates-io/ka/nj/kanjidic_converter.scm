(define-module (crates-io ka nj kanjidic_converter) #:use-module (crates-io))

(define-public crate-kanjidic_converter-0.1.0 (c (n "kanjidic_converter") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kanjidic_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1h3k91kr2f5nnc6vgfhqpllr65c5bfgbam6idi6rfy2kxjajgdji")))

(define-public crate-kanjidic_converter-0.1.1 (c (n "kanjidic_converter") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kanjidic_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1m390q20k3m5r0ckhnhchalg63ajq437d3nqy7cckgnfk9fw4mcw")))

(define-public crate-kanjidic_converter-0.1.2 (c (n "kanjidic_converter") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kanjidic_parser") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "07fd02i0hpkmz8kzqjjc7fqvpyn42lagksafpn3rri4rfhmd5vj4")))

