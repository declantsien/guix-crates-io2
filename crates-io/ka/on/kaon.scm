(define-module (crates-io ka on kaon) #:use-module (crates-io))

(define-public crate-kaon-0.1.4 (c (n "kaon") (v "0.1.4") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.8.0") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0h3fz8gci9j3nq38ld4pddqgazlxn59d4j3s8qaxwkspz3zp4lbl")))

