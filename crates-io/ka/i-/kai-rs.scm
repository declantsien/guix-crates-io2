(define-module (crates-io ka i- kai-rs) #:use-module (crates-io))

(define-public crate-kai-rs-0.1.0 (c (n "kai-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "182kj2hd5rd70m0hsrswkhar53cqmis43yr0yk75sw1fz97ggqxm")))

