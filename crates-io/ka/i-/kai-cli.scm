(define-module (crates-io ka i- kai-cli) #:use-module (crates-io))

(define-public crate-kai-cli-0.1.0 (c (n "kai-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "045qjhqi65nz11ppcmbs8621y00nqry2939niqcwnzl48i49q325")))

