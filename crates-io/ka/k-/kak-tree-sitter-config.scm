(define-module (crates-io ka k- kak-tree-sitter-config) #:use-module (crates-io))

(define-public crate-kak-tree-sitter-config-0.1.0 (c (n "kak-tree-sitter-config") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1g2za0n1d9f5vdv6h3vrlgij98spjxng97fhwvya8hvlr3vqnm2w") (r "1.65.0")))

(define-public crate-kak-tree-sitter-config-0.2.0 (c (n "kak-tree-sitter-config") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0fxv4485dk3j8bwplz5yw8cr26x844i6p79mi4zr1vrcs8w809nb") (r "1.65.0")))

(define-public crate-kak-tree-sitter-config-0.3.0 (c (n "kak-tree-sitter-config") (v "0.3.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1nd80aj2lxdp8q3m09dgrm333i3k4jrar6kfpmwlq5vbdfdfg6wq") (r "1.65.0")))

(define-public crate-kak-tree-sitter-config-0.4.0 (c (n "kak-tree-sitter-config") (v "0.4.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "01s44l3ih6xyvsds6gxlz8k3hfh7m7fl0mrzcnhkway548bw2zgd") (r "1.65.0")))

(define-public crate-kak-tree-sitter-config-0.5.0 (c (n "kak-tree-sitter-config") (v "0.5.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1a3fzzwwhkm44yfsxikqi4yscz9vq46bhg05ph2qwk9wyyczg2fm") (r "1.65.0")))

