(define-module (crates-io ka k- kak-subvert) #:use-module (crates-io))

(define-public crate-kak-subvert-1.0.0 (c (n "kak-subvert") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)))) (h "0d2k7sb73yrxqkpn895x96q63wsffw5d2hx09vrdzy8i3siyfs70")))

(define-public crate-kak-subvert-1.0.1 (c (n "kak-subvert") (v "1.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)))) (h "0rmwby1syg458xn6sb3vxl34wyzxydwjw7wb25mj25cn2rzmiabm")))

(define-public crate-kak-subvert-1.0.2 (c (n "kak-subvert") (v "1.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)))) (h "1z3ygrm1v2c4d9kwwxfy6y6mlpr60zbr6vh00r81amc15f25yp46")))

(define-public crate-kak-subvert-1.0.3 (c (n "kak-subvert") (v "1.0.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)))) (h "00h8366lk3x52kv40b276cxb7kw667m0b6hpwkzlzg7pkhdrr2mc")))

