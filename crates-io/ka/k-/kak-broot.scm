(define-module (crates-io ka k- kak-broot) #:use-module (crates-io))

(define-public crate-kak-broot-0.1.0 (c (n "kak-broot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "kak-ui") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hrfchgl41nlh6yid44gb85dv5s4ygj7h8iwvyqqi0d6w1fiyihy")))

(define-public crate-kak-broot-1.0.0 (c (n "kak-broot") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "kak-ui") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0b32mmm7nry07kl21ynnw5icxr1vibh0p8f5v3n9q46ybs74rs6m")))

