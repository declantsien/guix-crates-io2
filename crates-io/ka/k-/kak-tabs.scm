(define-module (crates-io ka k- kak-tabs) #:use-module (crates-io))

(define-public crate-kak-tabs-0.1.0 (c (n "kak-tabs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hy1mnfwbjl3yzwdvx854wdcrfpcp5y8vnbaxv4i2pwcb6zbcixm")))

(define-public crate-kak-tabs-0.1.4 (c (n "kak-tabs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "145z90ybhqaps19jp62arh5g18nsf0i2a62q3l7pand5vw67486y")))

(define-public crate-kak-tabs-0.1.6 (c (n "kak-tabs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "095fdk7jyj57js2j6l8bhhra48f5f9v7dz2n677j5c2ard6wiwya")))

(define-public crate-kak-tabs-0.1.7 (c (n "kak-tabs") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1capq03c7qhb5yxxplprwlnsl3h7ms1jcrh09qnvdnc3s8zl0f12")))

