(define-module (crates-io ka k- kak-ui) #:use-module (crates-io))

(define-public crate-kak-ui-0.1.0 (c (n "kak-ui") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j2n8z57lyhyd6bw8w1gsp7dsy9jcxzywfc2w3xg9syl1hm87hw7")))

(define-public crate-kak-ui-0.2.0 (c (n "kak-ui") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05z5jrcgrb5k46rbqi8yhgcr1wkfl819l7blhp685wd5zfz6hcj7")))

