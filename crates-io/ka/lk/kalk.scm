(define-module (crates-io ka lk kalk) #:use-module (crates-io))

(define-public crate-kalk-0.1.0 (c (n "kalk") (v "0.1.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "04nq7fj4x8l90cjl2jvn173yb27bagkiv4hhs8zg8s6i4xzxh99i")))

(define-public crate-kalk-0.1.1 (c (n "kalk") (v "0.1.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0hh0wr033sjvjs8m3ig4lypiqf19h9xkfa2935mfn0zsb9srf8hl")))

(define-public crate-kalk-0.1.2 (c (n "kalk") (v "0.1.2") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0lqqk64msxcdy0n65njrazy86kg9frd1xjyali7mfwb2j937n4hn")))

(define-public crate-kalk-0.1.3 (c (n "kalk") (v "0.1.3") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "041sn8w348vasriag52fvhchpksyj3c9p4whbimnm07a2h5zi7fg")))

(define-public crate-kalk-0.1.4 (c (n "kalk") (v "0.1.4") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "11nhsvp3hb07qs7h7glclc682vvhn0wdqk4kaf20kmhpmr50z6nk")))

(define-public crate-kalk-0.1.5 (c (n "kalk") (v "0.1.5") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1s6pixhn87i3kasg993m1c13gywqjxb327c07kyw1l9xv0xh6nny")))

(define-public crate-kalk-0.1.6 (c (n "kalk") (v "0.1.6") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1l245f1y03zkmyzzkxi2zyrbhh6kdnqnwy04ryb2nrnv8cfkssg3")))

(define-public crate-kalk-0.1.7 (c (n "kalk") (v "0.1.7") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0d194p5rzix1g6chsgadlc0fy9n86q8qnddkc55mf0njhj75ylmq")))

(define-public crate-kalk-0.1.8 (c (n "kalk") (v "0.1.8") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "00mb83jhw830lihb27sx93x7iwj8cmqlxrl98vjxvgpr4lysjacn")))

(define-public crate-kalk-0.1.9 (c (n "kalk") (v "0.1.9") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0cqfxhc5594smq1asw9yywijwl5hz1z2k0k9ima9yq998l4hvrfy")))

(define-public crate-kalk-0.1.10 (c (n "kalk") (v "0.1.10") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "01vg7cbj4hxhv2cjpdhf760mkmsagx9w368v9mf986yannyifj1l")))

(define-public crate-kalk-0.1.11 (c (n "kalk") (v "0.1.11") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1vg7i9smayx9gb74nqxqyrmi79x97lq9alh02rci3mz4p9xi134f")))

(define-public crate-kalk-0.2.0 (c (n "kalk") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0incnpbqyvjjf9mbqky2scfvglncjvj8hiwx774i1wgz73g59afn")))

(define-public crate-kalk-0.2.1 (c (n "kalk") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "182f1n898l87d9yw4dckh2idhqcnx1743zcp42bhg1qjb1y91ckl")))

(define-public crate-kalk-0.2.2 (c (n "kalk") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "00lzsmw0vamp8pjyssfhf16gncxvjcycfksz22d4hxjv3b7a7a76")))

(define-public crate-kalk-0.2.3 (c (n "kalk") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0q4b4ydlms247n7r70p8ysh0db0a8mx0vxl823hh4napxv9xjflb")))

(define-public crate-kalk-1.2.3 (c (n "kalk") (v "1.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1aypyc8zckch446khhhbjl3k8vx01w8lvdlfbh9nm1n7xwk35vcq")))

(define-public crate-kalk-1.2.4 (c (n "kalk") (v "1.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "18apxc6b3hdx9vzwmi2vn0b91w5v3lhxfcyfi95rfqi2dj8sp176")))

(define-public crate-kalk-1.2.5 (c (n "kalk") (v "1.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0naw2vj0hsngsnr4xz87lf85fbdhrchrlkgwch08l0g0sycq1ibd")))

(define-public crate-kalk-1.2.6 (c (n "kalk") (v "1.2.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0w4d4hnmxic992iahcfwinblw51f6sg6wd1qs0gg81ap3pa8cikr")))

(define-public crate-kalk-1.3.0 (c (n "kalk") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1w9ghfscpz4fbhdqsal0s11qhrlz0iymkz2cay99jkg9mwwxp3fr")))

(define-public crate-kalk-1.4.0 (c (n "kalk") (v "1.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1gdcig23mm80wgvix6n82x4d9cn3qf5zbhdi0ky3b4820cn4yzqz") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.1 (c (n "kalk") (v "1.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0ys9v71nj0m1z2kwal2zdya2vgrjc76dxav4zhbfqsjn7bdjhp5x") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.2 (c (n "kalk") (v "1.4.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1pd2vjflkn72xckyq2ch0bbn3a9hnypg527dij9sc3jgbmi03wr0") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.3 (c (n "kalk") (v "1.4.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1lqj6alwlylz8mwl19h2bm4qmkiyl88a8a3x8pckgrsl2azwm779") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.4 (c (n "kalk") (v "1.4.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "15lhlx7ph0ly15ppgj8v623f9dpgipz4fv2c8v0ly4bbxr95c8bf") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.6 (c (n "kalk") (v "1.4.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1p768k0fb99kfjgayaza20q3irkszapkyvfpn42lgf1q2zppch1p") (f (quote (("default" "rug"))))))

(define-public crate-kalk-1.4.9 (c (n "kalk") (v "1.4.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0aaj2yhs8978zcig608zg7p08rrmz6360j8sshc71x015sfs4w50") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.0.0 (c (n "kalk") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0yvpanxcg0rcgrjchiyc0njy4rm2ic0k10jkxfxc13w62aql056y") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.0.1 (c (n "kalk") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0f2y38c7nnfnm5a30ah0q1ypqq73kyzxbms8wayhj0yw1xb4kbpg") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.0.2 (c (n "kalk") (v "2.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "03kfq2fgl0qa0vb435k2wi277n8dg1x70zybaaw6z26y4gsg8qfj") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.0.3 (c (n "kalk") (v "2.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0qkhl0rld21xx6762r08wb2bgf9gd0mm899xqh63pri6cb08m7sw") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.0.4 (c (n "kalk") (v "2.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "01i6abn70nlx1kbj5nwnrn3a7wbb09hfbiiazxrpq1xijbfa1r9y") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.1.0 (c (n "kalk") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "08kyh91r3y571mgxjs3jfsd74lqkhp9dk9rvsjk0hzdjjnfsm0j7") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.1.1 (c (n "kalk") (v "2.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "01239fp1wf1i765vkwzapq3yjhk54l682wsm7839fjhyrxm4pbb1") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.1.2 (c (n "kalk") (v "2.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "13cihcgn7k2a8makn1bl0zk93raa11im5v90c2di1625khlawwpl") (f (quote (("default" "rug"))))))

(define-public crate-kalk-2.2.0 (c (n "kalk") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0wxi3nb9zd89xlbf0wdd92b11h8wyqfiban703px4n7n652kjprg") (f (quote (("default" "rug"))))))

(define-public crate-kalk-3.0.0 (c (n "kalk") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "0j4nx0gqa914ggrxa9q6an1ll1jg5q6sdif3abxslbz1i8r0w9fx") (f (quote (("default" "rug"))))))

(define-public crate-kalk-3.0.1 (c (n "kalk") (v "3.0.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.17.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1y8anv3rnbkrhx1c0wxbzq360cm7pwx12wsabj0a1rjd1ibrhx8z") (f (quote (("default" "rug"))))))

(define-public crate-kalk-3.0.3 (c (n "kalk") (v "3.0.3") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.9") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.17.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1n3fdv2xhqc44ffyl94j82qascxmqkzv91fr6adf9arjhnp2c38j") (f (quote (("default" "rug" "gmp-mpfr-sys"))))))

(define-public crate-kalk-3.0.4 (c (n "kalk") (v "3.0.4") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.9") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.17.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "153qn2dgrlqfj0vz6m60f9sq91q5fiwm1pb3p0ksv6qrni73gg7c") (f (quote (("default" "rug" "gmp-mpfr-sys"))))))

(define-public crate-kalk-3.1.0 (c (n "kalk") (v "3.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.4.9") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.17.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "1m1f4bfazar76a3z6hzhxwp56sxdjd3p0wdmyl3qxxqhqlhrbdqw") (f (quote (("default" "rug" "gmp-mpfr-sys"))))))

(define-public crate-kalk-3.2.0 (c (n "kalk") (v "3.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "rug") (r "^1.24.0") (f (quote ("float"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (k 2)))) (h "10b6mgp7a18fd34cj3cfia72zgairnlh8gw5w24yhrwr6880q3v3") (f (quote (("default" "rug"))))))

