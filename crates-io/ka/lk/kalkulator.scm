(define-module (crates-io ka lk kalkulator) #:use-module (crates-io))

(define-public crate-kalkulator-0.1.0 (c (n "kalkulator") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "197fphlay5wlgsvvbclypaamk4qbw4g9gj1c2a6mywbszccz5fms") (y #t)))

(define-public crate-kalkulator-0.1.1 (c (n "kalkulator") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sv0vbb9rb5vn4dfzl409ckvxw1f7dzp02wjwxwka7ck0d0p19mp")))

(define-public crate-kalkulator-0.2.0 (c (n "kalkulator") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1663lmdrbyj8hcb866ky1ylqgjaxfjxbqmkiydxx1r94q7lzxykk")))

(define-public crate-kalkulator-0.2.1 (c (n "kalkulator") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sbpg0khnanqisz9k5q35xrismk1358yq8whyzj7f621777a73lg")))

(define-public crate-kalkulator-0.2.2 (c (n "kalkulator") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14afv8vdncvk5vw85f7qsm3xp4ajrjrvyzbkaa9nxpd7z9acvy82")))

