(define-module (crates-io ka za kazari) #:use-module (crates-io))

(define-public crate-kazari-0.0.1 (c (n "kazari") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-alpha.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "0fmlf0s50r01dylbj0bwyb1r8vlrs06ngr3859dz7s35rqalbyfq")))

