(define-module (crates-io ka te katex-doc) #:use-module (crates-io))

(define-public crate-katex-doc-0.0.1 (c (n "katex-doc") (v "0.0.1") (h "1c2la9vixjwli3q6f2qvnxc08ldng1qg8a33hsvx7njdwiil9px7") (y #t)))

(define-public crate-katex-doc-0.0.2 (c (n "katex-doc") (v "0.0.2") (h "14rhj1034lvfkmb4aq6iab6ys3ryr46bdalmykkx4n19zmhln9dv") (y #t)))

(define-public crate-katex-doc-0.1.0 (c (n "katex-doc") (v "0.1.0") (h "1fa0fi3bv9qdab0cb49q8971vszf0nzmvnvm70lns5xrpyyq0nqy")))

