(define-module (crates-io ka te katex-wasmbind) #:use-module (crates-io))

(define-public crate-katex-wasmbind-0.1.0 (c (n "katex-wasmbind") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "135cj7kffasmljj3ssrm5pssiivzq55rjz1z9vghask38nf521qg")))

(define-public crate-katex-wasmbind-0.2.0 (c (n "katex-wasmbind") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0wdb3zj88fgph5jhbq29jwh7kc0qbphxyfnfb1n72kj5q6vhfnir")))

(define-public crate-katex-wasmbind-0.3.0 (c (n "katex-wasmbind") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "05ydjy8f66nqx3mr7nym6wn6fq1wkfrc1h5pml7h419s22ylsraw")))

(define-public crate-katex-wasmbind-0.4.0 (c (n "katex-wasmbind") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1fljm52bc4dnkn1dih1p75bgbsp1zlh1nvl55b4d16rm3pp56r2v")))

(define-public crate-katex-wasmbind-0.5.0 (c (n "katex-wasmbind") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0wyprfkfy2c05vyvjz81bs0gm8la301zi20yv9k4726p3kmb73sy")))

(define-public crate-katex-wasmbind-0.6.0 (c (n "katex-wasmbind") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1lxc7hk8bg6sgkinp2pdg9kpmvg97l5x5ns46sq5ccr4wnd1ca3j")))

(define-public crate-katex-wasmbind-0.7.0 (c (n "katex-wasmbind") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0g0myrxd7izmrdcaqbakg2aaj9z3frmh26fj4i1pl0dw4hlk7bg4")))

(define-public crate-katex-wasmbind-0.8.0 (c (n "katex-wasmbind") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0c1dq2ayjv2jzvcqn08f214gih4aj8f0frxcsq50p6n86h3sy4sr")))

(define-public crate-katex-wasmbind-0.9.0 (c (n "katex-wasmbind") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "0blnnhwjs6ihvhi2aab2yvp924pksnmr5pmfmqwb1svmrsml7ab1")))

(define-public crate-katex-wasmbind-0.9.1 (c (n "katex-wasmbind") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "08933l4xl7djlfd4hhm5jcxna2mnsbs2rpbr23jah2k17wsxflyk")))

(define-public crate-katex-wasmbind-0.9.2 (c (n "katex-wasmbind") (v "0.9.2") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "0x6mqxzfmjgyc92sg6pf7jj8fphvinbz60vqpabc7b3ipvpzl3qd")))

(define-public crate-katex-wasmbind-0.9.3 (c (n "katex-wasmbind") (v "0.9.3") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 2)))) (h "0s4bw9pwghlv7jsiq0vma1krdwhhkayywx439fcz217l0f2xdbh9")))

(define-public crate-katex-wasmbind-0.10.0 (c (n "katex-wasmbind") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.30") (d #t) (k 2)))) (h "0k9xcxn6al8sg6sj3rfghn0161jv3vqchk9d7d9jyh0pxpssd6wz")))

