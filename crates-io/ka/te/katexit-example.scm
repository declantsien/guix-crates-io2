(define-module (crates-io ka te katexit-example) #:use-module (crates-io))

(define-public crate-katexit-example-0.1.0 (c (n "katexit-example") (v "0.1.0") (d (list (d (n "katexit") (r "^0.1.0") (d #t) (k 0)))) (h "1hjsqkkbbdfwn2zyfzghgjxhmwrj1zq4rlcyiq7k46i9zgq0zc92")))

(define-public crate-katexit-example-0.1.2 (c (n "katexit-example") (v "0.1.2") (d (list (d (n "katexit") (r "^0.1.0") (d #t) (k 0)))) (h "0xdbinn6nrkbr121j1n9agqp2gb8snxyjlgphg52cqm8np4cx6rx")))

(define-public crate-katexit-example-0.1.3 (c (n "katexit-example") (v "0.1.3") (d (list (d (n "katexit") (r "^0.1.0") (d #t) (k 0)))) (h "0q1kpx6xq3628w5k34nf9911qqa5zrg65qbbvsl5wwjsifkq6f3x")))

(define-public crate-katexit-example-0.1.4 (c (n "katexit-example") (v "0.1.4") (d (list (d (n "katexit") (r "^0.1.0") (d #t) (k 0)))) (h "0hzfjq4bamvydirjh1vfnwx9fvfgviz4rymk99iqqxp1cnpzyscs")))

