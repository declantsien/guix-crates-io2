(define-module (crates-io ka ls kalshi) #:use-module (crates-io))

(define-public crate-kalshi-0.1.0 (c (n "kalshi") (v "0.1.0") (h "0cpap799r282fg06xgbzdw1wpzm0vgkilsx9f52pdb5zq9w2kkx8")))

(define-public crate-kalshi-0.9.0 (c (n "kalshi") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0ahh8db2il2kq9hrjf8dy92f0l3xd6gxm4iqz1cv57zx8mkgmvyr") (r "1.72")))

