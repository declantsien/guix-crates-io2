(define-module (crates-io ka il kailua_test) #:use-module (crates-io))

(define-public crate-kailua_test-1.0.4 (c (n "kailua_test") (v "1.0.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "kailua_diag") (r "^1.0.4") (d #t) (k 0)) (d (n "kailua_env") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "11kfyjk8cig9k4wzlfsd8m9yycm0wxvhk1bs73gng6l53glv4gmb")))

