(define-module (crates-io ka il kailua_langsvr_protocol) #:use-module (crates-io))

(define-public crate-kailua_langsvr_protocol-1.0.4 (c (n "kailua_langsvr_protocol") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sdl61fakpb9dmfh85q0agpzl316872zf2if0q7pw8ckrk8cgk5h")))

(define-public crate-kailua_langsvr_protocol-1.0.5 (c (n "kailua_langsvr_protocol") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mgh1hmzsznghv6rxbbpdz5cc3wdlygqvnala3rqpbwqj6yq56f3")))

