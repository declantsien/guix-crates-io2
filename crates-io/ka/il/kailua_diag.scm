(define-module (crates-io ka il kailua_diag) #:use-module (crates-io))

(define-public crate-kailua_diag-1.0.4 (c (n "kailua_diag") (v "1.0.4") (d (list (d (n "kailua_env") (r "^1.0.4") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parse-generics-shim") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.7") (d #t) (k 0)))) (h "0rhr47w9hc99jgwx0x1ii6dwvali233qd4jzff0dv35c1v1c7pww")))

