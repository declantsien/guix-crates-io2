(define-module (crates-io ka se kase) #:use-module (crates-io))

(define-public crate-kase-0.1.1 (c (n "kase") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0r6bkhf159zgxg0jvppv8l0d44kr5bbk18p0ics0pwq6p6xy8afa")))

(define-public crate-kase-0.1.2 (c (n "kase") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "14g8v07d91q38w3zpbw1kh4b55x8svsb3xlqayqwggacy14hbh05")))

(define-public crate-kase-0.1.3 (c (n "kase") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0yiczp7ni2l1bd3cxl1017hagy0h80ypxk81hasj39jnf51ada22")))

