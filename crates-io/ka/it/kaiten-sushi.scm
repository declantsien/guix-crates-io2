(define-module (crates-io ka it kaiten-sushi) #:use-module (crates-io))

(define-public crate-kaiten-sushi-0.1.0 (c (n "kaiten-sushi") (v "0.1.0") (h "0xr8vwiflf55fm7lbhcagwrljw6d7wamsiqv4szgmvh669fqbq4s")))

(define-public crate-kaiten-sushi-0.2.0 (c (n "kaiten-sushi") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "061v7l6g5pmsqb8ds92m0w2raa7p571dk5gml66cfx41lv3jphz8")))

