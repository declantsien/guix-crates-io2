(define-module (crates-io ka it kaitai-macros) #:use-module (crates-io))

(define-public crate-kaitai-macros-0.1.0 (c (n "kaitai-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "023g5s4gl4h212bn3xan9l27db6a33glb7sl85jpi7kc39hbsz2a")))

(define-public crate-kaitai-macros-0.1.1 (c (n "kaitai-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1k1g18qiarp0rrqkxjyn1idckxxm0kasddk67h3c0piiqk4c63q1")))

(define-public crate-kaitai-macros-0.1.2 (c (n "kaitai-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "126za2qrld3y5ak2ldjc67q296wkcpgmg1rjfali14wym5h3b7gf")))

