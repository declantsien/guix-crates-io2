(define-module (crates-io ka ny kanye) #:use-module (crates-io))

(define-public crate-kanye-0.1.0 (c (n "kanye") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (d #t) (k 0)))) (h "141p6zhba1mrwyvk1nwq43mgiwlsphxf2hl9didyg58mabw94gb1")))

(define-public crate-kanye-0.1.1 (c (n "kanye") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "ureq") (r "^2.1.0") (d #t) (k 0)))) (h "1wa7mjgi2q2cp3gg8g9wvkycxk80zf7k8zx3s466gk6xhw5khcxf")))

