(define-module (crates-io ka ny kanyey) #:use-module (crates-io))

(define-public crate-kanyey-0.1.0 (c (n "kanyey") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "03rchwz0vbhvmadvl6qz344sk45fgxrkxipq0ga9qr5plm8nr3wp")))

(define-public crate-kanyey-0.1.1 (c (n "kanyey") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "165x8b0sv7hqq16v7jj00fp8mqp9x8a74vgp7xdfy03bzh2mcypf")))

(define-public crate-kanyey-0.1.2 (c (n "kanyey") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "158kp3d0fg3z5mb255002lx51p06197525blhnpqd6f4j3nffc4m")))

(define-public crate-kanyey-0.1.3 (c (n "kanyey") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0ghi4vq73whfsf69gqx7jxmm2i72j639hcbi5wy3d127hdhg84fa")))

