(define-module (crates-io ka th kathoey) #:use-module (crates-io))

(define-public crate-kathoey-1.0.2 (c (n "kathoey") (v "1.0.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "1j4kphvg9m6vn8shxiszy9hyc9ajhiyzvv7ay2cpjgpb01cl1y13")))

(define-public crate-kathoey-1.0.3 (c (n "kathoey") (v "1.0.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "0rl4mvnz3lvl82gh3g92lwvz9i84ni67dvc9bixdbg5bcafsrscs")))

(define-public crate-kathoey-1.0.4 (c (n "kathoey") (v "1.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "15fwk76r2rnfdyw9a0p1q79dlc7l3v7m23853kib5mnlh9k6zv52")))

(define-public crate-kathoey-1.0.5 (c (n "kathoey") (v "1.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "1hkfgbvxiiddazhixgavx9xkw4mwv6k6zimf8mx3pff0v96ws5xz")))

(define-public crate-kathoey-1.0.6 (c (n "kathoey") (v "1.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "1lf81br75ks49vqf2wcspb0hgmxms0wxjv9q6dv163237p4l3abh")))

(define-public crate-kathoey-1.0.7 (c (n "kathoey") (v "1.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "14l74vvymzvfrsbrlhb4qlcffr9frgy6bb4i7990lp7sw5s8bll7")))

(define-public crate-kathoey-1.0.9 (c (n "kathoey") (v "1.0.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "0r26gpmxq2my2yxbcb27hk2w7yrd9h2nc2nqwbl4sc6ii5cdjrr1")))

(define-public crate-kathoey-1.0.10 (c (n "kathoey") (v "1.0.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "0dr5df333qp09n2q9ixjrd4nyp9id80sjfkn5hmmak3pfnxfs0f4") (r "1.56.0")))

(define-public crate-kathoey-1.1.0 (c (n "kathoey") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13") (d #t) (k 0)))) (h "08343djbkz5cgn90vbzgmw9zhpl3lzkjwhxzs9q78fjc3qs1xafk") (r "1.58.0")))

(define-public crate-kathoey-1.1.1 (c (n "kathoey") (v "1.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "13ign78jmaxq8s9x7h9abm1rc1wjd976547hm0q7hwkb3mqn1g1d") (r "1.58.0")))

(define-public crate-kathoey-1.1.2 (c (n "kathoey") (v "1.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "serial_test") (r "^0.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.3") (d #t) (k 0)))) (h "0ibfcmz4kcvzlkd5y1547kvpiw739vv6y2zrdf9h57jwa2hz5clr") (r "1.58.0")))

(define-public crate-kathoey-1.1.4 (c (n "kathoey") (v "1.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "0k98zr1baf6pp1smw6g8lsn34sfhs1882gkblkp4z1bwz581rf00") (r "1.58.0")))

(define-public crate-kathoey-1.1.5 (c (n "kathoey") (v "1.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "serial_test") (r "^2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.5") (d #t) (k 0)))) (h "11qzpf4krlhr94z0cqhxb4wbbpwlsn1af4pzzvggnhvxh8my5a7c") (r "1.58.0")))

