(define-module (crates-io ka gu kaguya_rs) #:use-module (crates-io))

(define-public crate-kaguya_rs-0.1.0 (c (n "kaguya_rs") (v "0.1.0") (h "0jxv213n045kg8zfwcsz22gqcsqp6xymhm0mch59iz6h3lriw0ff")))

(define-public crate-kaguya_rs-0.1.1 (c (n "kaguya_rs") (v "0.1.1") (h "0px4qpdsq686drmhfmx8n9zqfw39nd9d2xwlzf10qdwsybyxb2bw")))

(define-public crate-kaguya_rs-0.1.2 (c (n "kaguya_rs") (v "0.1.2") (h "1il119kq22hxgsrljgb28cxyx63a8jxkhj3ahjad17d61nf1092r")))

(define-public crate-kaguya_rs-0.1.3 (c (n "kaguya_rs") (v "0.1.3") (h "13a19n9lkkgkxwb788fr7f9kjjr7g0srrmb44lsmfgili4hp5rvz")))

(define-public crate-kaguya_rs-0.1.4 (c (n "kaguya_rs") (v "0.1.4") (h "1wlsmkrw7kz0zvqsqkc43ngnc5k3phajhkim9mkh2y45q1gryvrr")))

(define-public crate-kaguya_rs-0.1.5 (c (n "kaguya_rs") (v "0.1.5") (h "0yhpj3g456wrkp1frfv5qk85qazyfhnhzd1sa8n9ai529pn6g1wc")))

