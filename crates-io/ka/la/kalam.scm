(define-module (crates-io ka la kalam) #:use-module (crates-io))

(define-public crate-kalam-0.1.0 (c (n "kalam") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.24") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "03mlhwycpgkb495axfpzb6cw8np61dxxgmfb89dd09riqj08dmj0")))

(define-public crate-kalam-0.1.1 (c (n "kalam") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.24") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1y0gw9r7xsk7440p9nc63hhkfbblj040n5cbx2ak91rkpq87rafd")))

