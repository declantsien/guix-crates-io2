(define-module (crates-io ka la kalavor) #:use-module (crates-io))

(define-public crate-kalavor-0.1.0 (c (n "kalavor") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1zchj08s90bnn9sr4d73vkyasb21d1adf9s9ksla7qw7im0aazqk")))

