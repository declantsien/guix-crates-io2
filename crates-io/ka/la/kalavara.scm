(define-module (crates-io ka la kalavara) #:use-module (crates-io))

(define-public crate-kalavara-0.1.0 (c (n "kalavara") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 2)) (d (n "rocksdb") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6.2") (d #t) (k 0)))) (h "1in7yyvlmnsxgb694wc9gn3xzv8wbmdfmag0a9qb6ah5w6wv0q1s")))

(define-public crate-kalavara-0.2.0 (c (n "kalavara") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "minreq") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6.2") (d #t) (k 0)))) (h "08w52npi4vja6ki2v18yslax3k26amf1n6jkwxgj4gldpa6ibv6j")))

(define-public crate-kalavara-0.3.0 (c (n "kalavara") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "minreq") (r "^1.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6.2") (d #t) (k 0)))) (h "0y9kmma7s7sn7xbhbmz5vxfmlzy86ks2sqrmn7j46wlwssd3f2jb")))

