(define-module (crates-io ka ri karima-nova-ntoken) #:use-module (crates-io))

(define-public crate-karima-nova-ntoken-0.2.0 (c (n "karima-nova-ntoken") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "karima-anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "karima-anchor-spl") (r "^0.19.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15mp0n4l96j33lmz2gqxn0bcx6qjik9487mj19sic256q4d76wwb")))

