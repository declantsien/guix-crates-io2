(define-module (crates-io ka ri kari) #:use-module (crates-io))

(define-public crate-kari-0.1.0 (c (n "kari") (v "0.1.0") (d (list (d (n "acc_reader") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "16n5467nqiig3g9is2ra46zmb6inffflaz6pxrc726v6zdag3g8r")))

