(define-module (crates-io ka ri karima-anchor-spl) #:use-module (crates-io))

(define-public crate-karima-anchor-spl-0.19.0 (c (n "karima-anchor-spl") (v "0.19.0") (d (list (d (n "karima-anchor-lang") (r "^0.19.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serum_dex") (r "^0.4.0") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1q74xz1xwvs5yja2cbv6sppd3xcp0y8rz0ai3ni38frbrxs8nr97") (f (quote (("token") ("shmem") ("mint") ("governance") ("dex" "serum_dex") ("devnet") ("default" "mint" "token" "associated_token") ("associated_token"))))))

