(define-module (crates-io ka rm karmarkar) #:use-module (crates-io))

(define-public crate-karmarkar-0.1.0 (c (n "karmarkar") (v "0.1.0") (d (list (d (n "nalgebra") (r ">=0.21.0, <0.22.0") (d #t) (k 0)))) (h "1fn3h9j0x7v88nh39q8zvzkfwbglypw4ayi61s8q0crvznwhhv75")))

(define-public crate-karmarkar-0.2.0 (c (n "karmarkar") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)))) (h "0fv6jqv3q48vszdp86i11y4y07grxhkasrykhdzjjjiyhgksp30a")))

(define-public crate-karmarkar-0.3.0 (c (n "karmarkar") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)))) (h "15rlpvaxwj7h18cn7a2d4nb45a9ryarbsqzfb7clkrlhp32xy9gn")))

(define-public crate-karmarkar-0.4.0 (c (n "karmarkar") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "00n414q9ls36r7ciixvz2a34x37kz021k6999x5kqf6bk2hrzvsc")))

(define-public crate-karmarkar-0.5.0 (c (n "karmarkar") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0admqvy87558lvjp54l1a7p3d4539lrksh75iasifi850fzpmhqa")))

