(define-module (crates-io ka rm karma-p2p) #:use-module (crates-io))

(define-public crate-karma-p2p-0.1.0 (c (n "karma-p2p") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "0q1r6j1bl7isd6l1rmq41gbzgabx0g6jvxim9r8xz907zdxfk030")))

(define-public crate-karma-p2p-0.1.1 (c (n "karma-p2p") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "0j7s3xfj3vx3vkfnlxqdh13wv31wm0jlih825k1wl97div9yf2p9")))

(define-public crate-karma-p2p-0.1.2 (c (n "karma-p2p") (v "0.1.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "03ihn1zfcbin6463qqcx8b9f5mvs3fhpc9wldacjjnlmnzpxfb9k")))

(define-public crate-karma-p2p-0.1.3 (c (n "karma-p2p") (v "0.1.3") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)))) (h "03i7n3wspibbaxiwlnhcm63sh6n53nnds0d0cwc9n4k53n0c7qkv")))

