(define-module (crates-io ka rm karmen) #:use-module (crates-io))

(define-public crate-karmen-0.1.0 (c (n "karmen") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "0c6lra9r7p4iasqzm6nq2h7amrgfv4jpkmzxch6d2fg8b8jr1dxz") (y #t)))

(define-public crate-karmen-2.0.0 (c (n "karmen") (v "2.0.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1z6lq9jz6826lbnr41vwy30a48dncjg4wh6cmr26z28vdl0c843l")))

(define-public crate-karmen-2.0.1 (c (n "karmen") (v "2.0.1") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "0d6lm4sb1sxrjmql7qny4gyk51csm2sr84rczli18rpsghni9van")))

