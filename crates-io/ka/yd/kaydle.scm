(define-module (crates-io ka yd kaydle) #:use-module (crates-io))

(define-public crate-kaydle-0.1.0 (c (n "kaydle") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "kaydle-primitives") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_format") (r "^1.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1gq3jg3p26spijm8883sxh61blf0szmwibhf27k68316x8jx1q2n") (y #t)))

(define-public crate-kaydle-0.2.0 (c (n "kaydle") (v "0.2.0") (d (list (d (n "kaydle-primitives") (r "^3.0.0") (d #t) (k 0)) (d (n "lazy_format") (r "^1.9.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-mobile") (r "^3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0lyvf98pd1f3s5a5xz12qfarx1npn0w1rvpmi0r6yzags69y2b9b")))

