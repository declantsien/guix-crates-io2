(define-module (crates-io ka yd kaydle-primitives) #:use-module (crates-io))

(define-public crate-kaydle-primitives-1.0.0 (c (n "kaydle-primitives") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "derive_destructure") (r "^1.0.0") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "030ivn7c6n166xpdkl9hvrfcxlin0ax5mx0q85s2pmrpphq7r2xf")))

(define-public crate-kaydle-primitives-2.0.0 (c (n "kaydle-primitives") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "15bcf29f58p9jf0bnak859fmgj04yfccwj95jmkqfffb8wnqri26")))

(define-public crate-kaydle-primitives-2.1.0 (c (n "kaydle-primitives") (v "2.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "0zqwa7j7xg86rs3h6b0qpr073g3l1j5j4glgrirqyhh7qc92zqlj")))

(define-public crate-kaydle-primitives-2.3.0 (c (n "kaydle-primitives") (v "2.3.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "0vq93bvzbh8nas4j4pyhcm7sl474wa0w8isk5p54b4kamgaq57zd")))

(define-public crate-kaydle-primitives-3.0.0 (c (n "kaydle-primitives") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)))) (h "0wl2v1hksw982xwn0i00sxi08vamk8y3yfhy7y2w8123ydbzcc9g")))

