(define-module (crates-io ka #{30}# ka3005p) #:use-module (crates-io))

(define-public crate-ka3005p-0.1.0 (c (n "ka3005p") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1bld21w603v3a9va9r5cgdsyzl6shnsx7pgynj7vrwfv108gnafc")))

(define-public crate-ka3005p-0.1.1 (c (n "ka3005p") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0zy3klh79wj6imzpvh5gra3swrr4vh4qhn109zyix39yw1yqd5vs")))

(define-public crate-ka3005p-0.1.2 (c (n "ka3005p") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0sbylbrnr76d093yb5dd48gc2aab6d6fmrfg4dysfm2mkk8z72ll")))

(define-public crate-ka3005p-0.2.0 (c (n "ka3005p") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0m596mgbx6ibf5107qc16phscsdrw8d1c9ks3516f8k2zdv2ycwf")))

(define-public crate-ka3005p-0.2.1 (c (n "ka3005p") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "10im86wbs86lm7nhbl755pnnhdqzshxns2hgha9sl0pzh8bqrabb")))

(define-public crate-ka3005p-0.2.3-alpha.0 (c (n "ka3005p") (v "0.2.3-alpha.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1m1k8l6iya0vz1a1rsx6gxljlpzg2d9hz4jxaa3mg1byyrrv41qk") (y #t)))

(define-public crate-ka3005p-0.2.2 (c (n "ka3005p") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1hs4y1vqfn2g6ilvafxlx518qan4v5pbqkvpfx9n68pp4nqmf8zf")))

(define-public crate-ka3005p-0.3.0 (c (n "ka3005p") (v "0.3.0") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "1.*") (d #t) (k 0)) (d (n "serialport") (r "4.*") (d #t) (k 0)))) (h "15bjmdzrmnirkx5rlarx3rqnpk8ymq00cqzws1hqlb8lwqm0g1x9")))

(define-public crate-ka3005p-0.4.0 (c (n "ka3005p") (v "0.4.0") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "1.*") (d #t) (k 0)) (d (n "serialport") (r "4.*") (d #t) (k 0)))) (h "077hz9b11kwim10qj8fg5bncmw853444qjjpnv1v1xnd985fflvi")))

(define-public crate-ka3005p-0.5.0 (c (n "ka3005p") (v "0.5.0") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "human-panic") (r "1.*") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (o #t) (d #t) (k 0)) (d (n "serialport") (r "4.*") (d #t) (k 0)))) (h "1lx0kanxrv0nn18iylxw3q03q309qknsvhp6h9jayija98pawsgd") (f (quote (("python_module" "pyo3"))))))

