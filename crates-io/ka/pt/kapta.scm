(define-module (crates-io ka pt kapta) #:use-module (crates-io))

(define-public crate-kapta-0.0.0 (c (n "kapta") (v "0.0.0") (h "0k2b564lw9vxs9spirv3cr4zmznm9hzh6ss1dhhjdwlrlz2rn3ii")))

(define-public crate-kapta-0.0.1 (c (n "kapta") (v "0.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.11") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jdqvqysv23cw6mannlfrjhb3f58wzq50lnihn4vj77bnrxyj3cq")))

(define-public crate-kapta-0.0.2 (c (n "kapta") (v "0.0.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.11") (f (quote ("serde"))) (d #t) (k 0)))) (h "10wbyl0frv4fhm7pj53dvc3x0a6c54ckzf58d0nwj7dq7z6396hf")))

(define-public crate-kapta-0.0.3 (c (n "kapta") (v "0.0.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.11") (f (quote ("serde"))) (d #t) (k 0)))) (h "1gl7b11xdla16w35c1gxzapcxah1pm2pldhg926afxh8391b2lbn")))

(define-public crate-kapta-0.0.4 (c (n "kapta") (v "0.0.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "geojson") (r "^0.24.1") (d #t) (k 0)))) (h "1xfxf8l424r3iw55z2az3cvndb28z0v8ndyj85girygw7l4djp3m")))

