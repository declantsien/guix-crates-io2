(define-module (crates-io ka si kasi-kule) #:use-module (crates-io))

(define-public crate-kasi-kule-0.1.0 (c (n "kasi-kule") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0h2npg78qgnqyx9vnws0y5d9j7vl1zqaif7mhmm1navzryamwxy4")))

(define-public crate-kasi-kule-0.2.0 (c (n "kasi-kule") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)))) (h "1xlzhd6ca14wb62sjr73vxmlx608maik42bfyqvdfharshpxq7p2")))

(define-public crate-kasi-kule-0.3.0 (c (n "kasi-kule") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)))) (h "08adah28d8paa042rdfjicjzskqplks1jw3w855qbrw3wmip8lij")))

(define-public crate-kasi-kule-0.3.1 (c (n "kasi-kule") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)))) (h "0jj5k897svki2lj8mmdsj62xd9y15j76nq4m4bqgid2nf1wi3v18")))

(define-public crate-kasi-kule-0.3.2 (c (n "kasi-kule") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)))) (h "05z5rybrj0ww8lfkg88m7vh93lsjwrpj35fdrz70zg0vg2ykdd52")))

(define-public crate-kasi-kule-0.3.4 (c (n "kasi-kule") (v "0.3.4") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 2)) (d (n "lab") (r "^0.11") (d #t) (k 2)) (d (n "micromath") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1h0d8nmli75zc81zh5kg12648vms9zy08zkinpi8amzr4zqfi304") (f (quote (("sse") ("approximate_math" "micromath"))))))

