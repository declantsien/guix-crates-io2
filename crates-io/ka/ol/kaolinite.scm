(define-module (crates-io ka ol kaolinite) #:use-module (crates-io))

(define-public crate-kaolinite-0.1.0 (c (n "kaolinite") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1whsf1iavrl23a4x14da99akqd7ri859yfpx5ljfb652729p8p6b")))

(define-public crate-kaolinite-0.1.1 (c (n "kaolinite") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0h4srrwz8xvdhmxlfgq38qcvbbchyf5cr73q6zc7czl0f3b39zkc")))

(define-public crate-kaolinite-0.1.2 (c (n "kaolinite") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1x459pcmbz5fvl0fdj9h6zdhiix4cazi1gnrdi60s35j5hn6ff5k")))

(define-public crate-kaolinite-0.1.3 (c (n "kaolinite") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1hm8iyczb90bzim4ymz1xs6vl7k1qncps3g4qy1nxmdlpj7min8j")))

(define-public crate-kaolinite-0.1.4 (c (n "kaolinite") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1sk0xyj1nlcrhn9h6v0j8ig029gcwmdixr1zz9j33plrnr06xqqd")))

(define-public crate-kaolinite-0.1.5 (c (n "kaolinite") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "02ry5d2w1qvig4kyga7pfildl22s7k2qqmm7mad4pl1ki2wns6f9")))

(define-public crate-kaolinite-0.2.0 (c (n "kaolinite") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1hn5z12l6wyda7sywkq607s34pncivrrq3mdh8ibjp63g3n5pcwx")))

(define-public crate-kaolinite-0.3.0 (c (n "kaolinite") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sugars") (r "^3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0mmh7b7r3w60vzzhg267q6islrlaid1alj6q0hr4ww6y3bibkkvj")))

(define-public crate-kaolinite-0.3.1 (c (n "kaolinite") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sugars") (r "^3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0b46r23d4jqcymvcvcb70p7rvw92r9kdbiq3ly7yzrfiqfa5prbv")))

(define-public crate-kaolinite-0.4.0 (c (n "kaolinite") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sugars") (r "^3.0.0") (d #t) (k 2)) (d (n "synoptic") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0f10cnqa0nszl1q45c69shl7s0r0ldmyfcl8kadnkx298266wli5") (f (quote (("syntax_highlighting" "synoptic"))))))

(define-public crate-kaolinite-0.4.1 (c (n "kaolinite") (v "0.4.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sugars") (r "^3.0.0") (d #t) (k 2)) (d (n "synoptic") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "16mcbxbhsidhdzkar9mi4n39dvh8i2f1g03asgqc1bppymj9vpfl") (f (quote (("syntax_highlighting" "synoptic"))))))

(define-public crate-kaolinite-0.5.0 (c (n "kaolinite") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sugars") (r "^3.0.0") (d #t) (k 2)) (d (n "synoptic") (r "^1.0.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1qkxxypvrvqqj3svcl4298ccc6r3fni8ssswnxjj5qqvzp3mhllf")))

(define-public crate-kaolinite-0.6.0 (c (n "kaolinite") (v "0.6.0") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "sugars") (r "^3.0.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1q6cmd2168i629n0z47dp7ffb17iyhjshgq047mj33zysf2p08hy")))

(define-public crate-kaolinite-0.6.1 (c (n "kaolinite") (v "0.6.1") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "sugars") (r "^3.0.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1gvxaiklg60xac23ycv6rn0fd24dqcvpw4qr4m7vsjgjnzxmj8cc")))

(define-public crate-kaolinite-0.7.0 (c (n "kaolinite") (v "0.7.0") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "ropey") (r "^1.5.0") (d #t) (k 0)) (d (n "sugars") (r "^3.0.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "03wg8a8lhv66fc1bk7dmbgcpj9l78pz6ajmg1l61kwaadwlmvws0")))

