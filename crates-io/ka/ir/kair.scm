(define-module (crates-io ka ir kair) #:use-module (crates-io))

(define-public crate-kair-0.1.0 (c (n "kair") (v "0.1.0") (d (list (d (n "lp-modeler") (r "^0.4.2") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.3.0") (k 0)))) (h "0zv7c0vsp05mk0lqqc3kmkljcwp4d8nbxcwppd2q35ckx8h6k1a4")))

(define-public crate-kair-0.2.0 (c (n "kair") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lp-modeler") (r "^0.4.2") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.3.0") (k 0)))) (h "1m9133dqpdn27xlz8i7lrpadwp8qq6vyqaw7q7lnqbkq7bps1g1p")))

(define-public crate-kair-0.3.0 (c (n "kair") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lp-modeler") (r "^0.4.2") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.3.0") (k 0)))) (h "1k72bl02qnsl3y8sn9dmka0bddc9ambnr67wz7hrr7ryacafqgph")))

(define-public crate-kair-0.3.1 (c (n "kair") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lp-modeler") (r "^0.4.2") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.4.0") (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "05m1j5w8vqkmgk6l4zmh6631iw8jw82szqiabi55fd8mcrh52ai6")))

(define-public crate-kair-0.4.0 (c (n "kair") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "good_lp") (r "^1.0.0") (k 0)) (d (n "good_lp") (r "^1.0.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.5.1") (k 0)))) (h "03zh46s2q27x0m11n1kx9xmd1ymmf9dhg0l26nmmk6006sysfcj6")))

(define-public crate-kair-0.5.0 (c (n "kair") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "custom_error") (r "^1.4.0") (d #t) (k 0)) (d (n "good_lp") (r "^1.1.0") (k 0)) (d (n "good_lp") (r "^1.1.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rust_sbml") (r "^0.5.1") (k 0)))) (h "02ifx58cyxfawgx63pf0ral6wcq51z42wr4cz3hb57k9h34rp98v")))

