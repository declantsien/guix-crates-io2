(define-module (crates-io ka ro karo) #:use-module (crates-io))

(define-public crate-karo-0.1.0 (c (n "karo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "decorum") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "183c50zhwgh1r7f5j6h9j3nj09867vnc3vidrxgc6lllicn88y9f")))

(define-public crate-karo-0.1.1 (c (n "karo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "decorum") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "066dp8l6bf3y0j2v98vdx64s1wkfv4q3m17588s01i3rc04gbk6f")))

(define-public crate-karo-0.1.2 (c (n "karo") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "decorum") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1pzsv456apvhqllmxkif03hrz8bxkb2m51816481s3wj9a6r3kv9")))

