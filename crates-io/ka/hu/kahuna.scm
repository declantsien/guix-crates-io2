(define-module (crates-io ka hu kahuna) #:use-module (crates-io))

(define-public crate-kahuna-0.1.0 (c (n "kahuna") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06pllz8xcy7wxmnv278vmjiczq9cwf2ywiwd0vb04g2nv7hqhlaf")))

(define-public crate-kahuna-0.2.0 (c (n "kahuna") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ky99bl9wmnnzj21fqmkbz9xignn3b8yidmzivsbj1fz4679i6hl")))

(define-public crate-kahuna-0.3.0 (c (n "kahuna") (v "0.3.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01qng03vkh7a3yfrncral0swlcdmzihs3h7c6yfqyhk7pcdqlx8n")))

