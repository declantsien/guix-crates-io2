(define-module (crates-io ka ff kaffe) #:use-module (crates-io))

(define-public crate-kaffe-0.1.0 (c (n "kaffe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0c5pj8zirpv33skyih5019gcffnj1d2fiwxibpyyjrvlvv9kz3aj") (y #t)))

(define-public crate-kaffe-0.1.1 (c (n "kaffe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1fsxwa42w6l4kdp771w9ji0g79xf8kvrqkkmb00yr7r7f7z2ia9y")))

(define-public crate-kaffe-0.1.2 (c (n "kaffe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1sb3h12axprl6b0p46593mxp56yfdypl90nvjkivqkb8qg2jkp13")))

(define-public crate-kaffe-0.1.3 (c (n "kaffe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "028azx0yynf37mq7b8nxkc3gxn0msqzcsad5646czc8dyiyd65lw")))

(define-public crate-kaffe-0.1.4 (c (n "kaffe") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0bvqy5v0d9rbnk7a4jcsbjsf4nayzqs4k4g5g77rd17vzq8cmzwm")))

(define-public crate-kaffe-0.2.0 (c (n "kaffe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "02gdz751fpy2yzca4891621sif74p34h9c7rnl2vfp59pqyncizl")))

