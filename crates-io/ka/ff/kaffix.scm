(define-module (crates-io ka ff kaffix) #:use-module (crates-io))

(define-public crate-kaffix-0.0.1 (c (n "kaffix") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)))) (h "14a2lwd66phyjsw69nm6w9gwys6w7dlzwrii504931xrlsgcm4if")))

