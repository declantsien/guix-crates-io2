(define-module (crates-io ka da kadabra) #:use-module (crates-io))

(define-public crate-kadabra-0.1.0 (c (n "kadabra") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "fake-tty") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1h0fcnqh45k0947sp3iq20ih87rfximjl289m3grabkjbmjmrrvy")))

