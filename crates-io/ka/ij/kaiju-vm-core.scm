(define-module (crates-io ka ij kaiju-vm-core) #:use-module (crates-io))

(define-public crate-kaiju-vm-core-0.1.0 (c (n "kaiju-vm-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "04yww5rl601cf34qnah49fk1115f0rb7jd3vg85bd5p9mjgkirbc")))

(define-public crate-kaiju-vm-core-0.1.1 (c (n "kaiju-vm-core") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0hl95yiy54dwfnprfbarl5cyw73czzk7n07mj2545066jwqvq2cz")))

(define-public crate-kaiju-vm-core-0.1.2 (c (n "kaiju-vm-core") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "06n270yzvfc9hx8sfnq0306wwl32k8zkvlydbcy6xz3mdp2imbvh")))

(define-public crate-kaiju-vm-core-0.1.3 (c (n "kaiju-vm-core") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0bv0a3v8hhx70vvc0d9x37gk33c60ymf7cs99qbgwlrafnnqvv4b")))

(define-public crate-kaiju-vm-core-0.1.4 (c (n "kaiju-vm-core") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0k4aqmqzn36jbjasap5j9b8wlrzli7b7zyjvirww97aclvflnj9h")))

