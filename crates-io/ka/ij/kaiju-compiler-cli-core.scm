(define-module (crates-io ka ij kaiju-compiler-cli-core) #:use-module (crates-io))

(define-public crate-kaiju-compiler-cli-core-0.1.0 (c (n "kaiju-compiler-cli-core") (v "0.1.0") (d (list (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "relative-path") (r "^0.4") (d #t) (k 0)))) (h "0352v1yp5fcy24065m5vs4nqsmsd8llffjz3mq58ls4swjmc2gg1")))

(define-public crate-kaiju-compiler-cli-core-0.1.1 (c (n "kaiju-compiler-cli-core") (v "0.1.1") (d (list (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "relative-path") (r "^0.4") (d #t) (k 0)))) (h "0ghgwngkwwxy0w4lnnb3crn686f9kw7y3jjwld4wk0bn3w6sipjn")))

(define-public crate-kaiju-compiler-cli-core-0.1.2 (c (n "kaiju-compiler-cli-core") (v "0.1.2") (d (list (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "relative-path") (r "^0.4") (d #t) (k 0)))) (h "0cj9qi59yp962b80ip6158nxcidwv0fgy5h426b490aa0xnl56zc")))

(define-public crate-kaiju-compiler-cli-core-0.1.3 (c (n "kaiju-compiler-cli-core") (v "0.1.3") (d (list (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "relative-path") (r "^0.4") (d #t) (k 0)))) (h "1ax7g8ywgqzviglm5isg148k959sbd8qxniyibmkvb40f24j69jq")))

(define-public crate-kaiju-compiler-cli-core-0.1.4 (c (n "kaiju-compiler-cli-core") (v "0.1.4") (d (list (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "relative-path") (r "^0.4") (d #t) (k 0)))) (h "1a391cpggn146rzbwpiywf01lr4fvws2541bwamp3dlhml35f3bm")))

