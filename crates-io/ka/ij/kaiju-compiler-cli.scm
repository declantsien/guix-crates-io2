(define-module (crates-io ka ij kaiju-compiler-cli) #:use-module (crates-io))

(define-public crate-kaiju-compiler-cli-0.1.0 (c (n "kaiju-compiler-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "1wfm1m5jczbjm3vk8c06l4pyvvmcjhxzazk4apdhliiijh5i16vm")))

(define-public crate-kaiju-compiler-cli-0.1.1 (c (n "kaiju-compiler-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0b8g611msnc7m49ipsxlqd0q04vmdih80ylfbinl291mg2c85ckg")))

(define-public crate-kaiju-compiler-cli-0.1.2 (c (n "kaiju-compiler-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0zkd6fahhi4x0rxzc2xid2101ml8n48glq9l3hcc3j2k5sw7ipk9")))

(define-public crate-kaiju-compiler-cli-0.1.3 (c (n "kaiju-compiler-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "105m5fvc4rx1l756bjxzc0pg70p3r6mfr7a2lprjsl9vhmf7jg0r")))

(define-public crate-kaiju-compiler-cli-0.1.4 (c (n "kaiju-compiler-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-compiler-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "1jvf78avg2qhp2c0xmiasj8z4868ks2w5g5fb15yanvrdi2w7s5h")))

