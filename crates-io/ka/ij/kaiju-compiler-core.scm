(define-module (crates-io ka ij kaiju-compiler-core) #:use-module (crates-io))

(define-public crate-kaiju-compiler-core-0.1.0 (c (n "kaiju-compiler-core") (v "0.1.0") (d (list (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "05q5z2f1k1q6hihfbzmhl9dg8vj04iyh2pp172mmnczaak7gpi69")))

(define-public crate-kaiju-compiler-core-0.1.1 (c (n "kaiju-compiler-core") (v "0.1.1") (d (list (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0hipwzycsh12y5kmpns16n39crsn0yx7kdxyp5ph0y97zc8qy4vr")))

(define-public crate-kaiju-compiler-core-0.1.2 (c (n "kaiju-compiler-core") (v "0.1.2") (d (list (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "0rq7inn0yl49i8bc14h6nx018ay9z2xh7f3hvhvmvn5km4ypkalc")))

(define-public crate-kaiju-compiler-core-0.1.3 (c (n "kaiju-compiler-core") (v "0.1.3") (d (list (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "1b56a4wlkzj6nqqr4d3ygsp5d52lff33m63dfaxgvxryxxkgj968")))

(define-public crate-kaiju-compiler-core-0.1.4 (c (n "kaiju-compiler-core") (v "0.1.4") (d (list (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)))) (h "10yx5jlh4clf88sn70v5bixwgr63gghk2wba6qzkhpaiyinnj5qy")))

