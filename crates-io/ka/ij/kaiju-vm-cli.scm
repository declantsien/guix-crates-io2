(define-module (crates-io ka ij kaiju-vm-cli) #:use-module (crates-io))

(define-public crate-kaiju-vm-cli-0.1.0 (c (n "kaiju-vm-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-vm-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0bnmdivjy2wsyam7wpfj4g7b3d911cm2z9pnqiy085w6gngdj0fq")))

(define-public crate-kaiju-vm-cli-0.1.1 (c (n "kaiju-vm-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-vm-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0avjm81faki6z086y2nw4nxwj5fm1hs1h90yix5r34gzxsqix7hr")))

(define-public crate-kaiju-vm-cli-0.1.2 (c (n "kaiju-vm-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-vm-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0dnzpc1fyp4ygcrxjd0dp212ns1i6k4d3x5xbvj1yg4i8l230h8v")))

(define-public crate-kaiju-vm-cli-0.1.4 (c (n "kaiju-vm-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kaiju-compiler-cli-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-core") (r "^0.1") (d #t) (k 0)) (d (n "kaiju-vm-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1ny45rs1c63l73ndsd2xdalx9knl9p12wzq6c2nkj8y92n82qgzm")))

