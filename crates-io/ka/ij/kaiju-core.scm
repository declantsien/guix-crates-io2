(define-module (crates-io ka ij kaiju-core) #:use-module (crates-io))

(define-public crate-kaiju-core-0.1.0 (c (n "kaiju-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z06q10rq45rm9dzfqqdhivflfbi7magl6mzsw4vvgiqhmlp7r05")))

(define-public crate-kaiju-core-0.1.1 (c (n "kaiju-core") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m4kps2fyh4l6fgpzyrwh4cgz9b6drwxg0kv7kp7rdhdbjjsj3xn")))

(define-public crate-kaiju-core-0.1.2 (c (n "kaiju-core") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "179g12d5hb71wwzr171kncxbvzdjsmkf3s9r20jqdfryc75arfl2")))

(define-public crate-kaiju-core-0.1.3 (c (n "kaiju-core") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zbdg2q88jvxah9kq0jdbgjxva3v28lhjxk8dpxfs4mmrqfp6ai4")))

(define-public crate-kaiju-core-0.1.4 (c (n "kaiju-core") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wcy148xi7srhw07if6ssnqb66amg9v8glg9d68ya3p6rh4ylzlf")))

