(define-module (crates-io ka yr kayrx-macro) #:use-module (crates-io))

(define-public crate-kayrx-macro-0.1.0 (c (n "kayrx-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wh0phm4r1gsi2j4285wa1cigsysydqg7jpcgkmbzq6xw4zv8rfg")))

(define-public crate-kayrx-macro-0.2.0 (c (n "kayrx-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkp009n47a5mbqjmbpf6lzrb0f4imw9vhhjy9bx7vb7c9lqs90v")))

(define-public crate-kayrx-macro-0.2.1 (c (n "kayrx-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0malbbg4v5089p2n0b2pj5nip2iqhb30wim5bi6mvcwfg57vysva")))

(define-public crate-kayrx-macro-0.3.0 (c (n "kayrx-macro") (v "0.3.0") (d (list (d (n "kayrx") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1i1g32v933misici0h07xvmb9gyj5ihfhbffvni87sa8wmpbgans")))

(define-public crate-kayrx-macro-0.4.0 (c (n "kayrx-macro") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydmbqf65bwbp896ifmcb2n34ixf2ccr8crxf9a5czi81rv9mdxj")))

(define-public crate-kayrx-macro-1.0.0 (c (n "kayrx-macro") (v "1.0.0") (d (list (d (n "kayrx") (r "^0.15") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sax43pqday8hkr7nh3j6ckz10flkg58nyj8zd024fp6lc019qci")))

(define-public crate-kayrx-macro-1.1.0 (c (n "kayrx-macro") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11iz5wpd89dpbsl4s74rbz262hl4fzkdjckqi1xd6l46q4iilwk1")))

