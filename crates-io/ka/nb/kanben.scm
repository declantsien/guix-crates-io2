(define-module (crates-io ka nb kanben) #:use-module (crates-io))

(define-public crate-kanben-0.1.0 (c (n "kanben") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "kv") (r "^0.20.2") (f (quote ("json-value"))) (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.5") (d #t) (k 0)))) (h "02q3w0afqy50bhlp7fbaz1vpgmmkgvx8rxx8daiywvwxbxcl3crp")))

(define-public crate-kanben-0.1.1 (c (n "kanben") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "kv") (r "^0.20.2") (f (quote ("json-value"))) (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.5") (d #t) (k 0)))) (h "0pmdhwbrdrc8xamcjfx1hsmvidm1hjlcz3k20kg2qzvvhnl8000n")))

(define-public crate-kanben-0.1.2 (c (n "kanben") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "kv") (r "^0.20.2") (f (quote ("json-value"))) (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.5") (d #t) (k 0)))) (h "0ynv5kl199wja0wcl7qwfqj7q1jacvgwv2crix9q1vpk6gmjvf0j")))

(define-public crate-kanben-0.1.3 (c (n "kanben") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "kv") (r "^0.20.2") (f (quote ("json-value"))) (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termimad") (r "^0.5") (d #t) (k 0)))) (h "19xyjkgr3cfszwdan3ypxf7wfi6sdy861a66fc39ln36wk2yd99r")))

