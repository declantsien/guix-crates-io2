(define-module (crates-io ka ha kahan) #:use-module (crates-io))

(define-public crate-kahan-0.1.0 (c (n "kahan") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0cb5763iwlmwz12ic9sbizg2p62iwy0r6ywr6bhd3wm1qca9gaz5")))

(define-public crate-kahan-0.1.2 (c (n "kahan") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1fbr877ip5qa68b3x2qgqfsadbsn3rblrwgqq499gl4l47h3x9g7")))

(define-public crate-kahan-0.1.3 (c (n "kahan") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1vwpcn416gpn2xxhdkmdy3v7fqh46pqzdja0mss5ww4r90q5a4b5")))

(define-public crate-kahan-0.1.4 (c (n "kahan") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0y8zgnxmy1sipyyc7av7vafvsqrmw9b289z852z88qzymw5va77j")))

