(define-module (crates-io ka kt kaktus) #:use-module (crates-io))

(define-public crate-kaktus-0.1.0 (c (n "kaktus") (v "0.1.0") (h "1vcsxhcj6v1lwjbqcs9b7xab02qdqc5gkyvfsqppxm5w3wv4rak0")))

(define-public crate-kaktus-0.1.1 (c (n "kaktus") (v "0.1.1") (h "1qyzv8p9qs8zgrvk20wvsf0ysvislpdxys33dm3i2aql3865zwi1")))

(define-public crate-kaktus-0.1.2 (c (n "kaktus") (v "0.1.2") (h "0n9pn4fhnhw4gmwf1jihyvasw8f2r6zp3svgl7zjznh2y3m13q6y")))

(define-public crate-kaktus-0.1.3 (c (n "kaktus") (v "0.1.3") (h "1574ryns057mfz0jhpna16hy6483rkwb4jjyvp7a45zkjcjj87ka")))

