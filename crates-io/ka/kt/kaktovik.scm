(define-module (crates-io ka kt kaktovik) #:use-module (crates-io))

(define-public crate-kaktovik-0.1.0 (c (n "kaktovik") (v "0.1.0") (h "1nh660bs1dm1izywnh894kr9ivnz322n2lf5i7lhskzy8ain2070")))

(define-public crate-kaktovik-0.1.1 (c (n "kaktovik") (v "0.1.1") (h "0ygs15q3v1cjd0z4h1sdgdq35ii1njykwhbrwr00pp2011il1adj")))

(define-public crate-kaktovik-0.1.2 (c (n "kaktovik") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1zw9kxf7ql6qmjvn0w3k7qjmqwmr3839h32clvg0vrfn4060zp0p")))

(define-public crate-kaktovik-0.1.3 (c (n "kaktovik") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1a7agr90h322ybbn54vfbn7zjff2aq7q77n4ivhni03kbpyxw3y9")))

(define-public crate-kaktovik-0.1.4 (c (n "kaktovik") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0vk5gp28wvdzjmzdjp14xp72w27xcwb6c5pqxcsk79k12381ibcd")))

