(define-module (crates-io ka ns kanshi) #:use-module (crates-io))

(define-public crate-kanshi-0.1.0 (c (n "kanshi") (v "0.1.0") (d (list (d (n "edid") (r "^0.1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "xmltree") (r "^0.4.0") (d #t) (k 0)))) (h "1m3h52w7hflfy087df61y1gh0hl66lqgy6zs3hjjw0f4kvp84rs3") (y #t)))

(define-public crate-kanshi-0.1.1 (c (n "kanshi") (v "0.1.1") (d (list (d (n "edid") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "xmltree") (r "^0.4.0") (d #t) (k 0)))) (h "0jf5jh3574p9hzx1j3g155bfhiwzg3qssp8acxfjfrvanzd0ga72") (y #t)))

(define-public crate-kanshi-0.2.0 (c (n "kanshi") (v "0.2.0") (d (list (d (n "edid") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.4.0") (d #t) (k 0)))) (h "0x5qb2wfb2rqdn5krlwg5ck2wasq55jwpzp8y74rbgaqs0cqs1bj") (y #t)))

(define-public crate-kanshi-0.2.1 (c (n "kanshi") (v "0.2.1") (d (list (d (n "edid") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.4.0") (d #t) (k 0)))) (h "01c1x70qx6z6glkfy0m44k0vnky25vclv2g6pnsmv0kawkli7faq") (y #t)))

