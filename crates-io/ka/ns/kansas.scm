(define-module (crates-io ka ns kansas) #:use-module (crates-io))

(define-public crate-kansas-0.1.0 (c (n "kansas") (v "0.1.0") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.17") (d #t) (k 0)) (d (n "glutin") (r "^0.9") (d #t) (k 0)))) (h "0mf7s955y6l7ad749h72zqr4arciyzbk5zmdk10n2ywbclyv1bk6")))

