(define-module (crates-io ka ss kassadin) #:use-module (crates-io))

(define-public crate-kassadin-0.1.0 (c (n "kassadin") (v "0.1.0") (d (list (d (n "kassaclient") (r "^0.1") (d #t) (k 0)) (d (n "kassapi") (r "^0.1") (d #t) (k 0)) (d (n "kassaroutes") (r "^0.1") (d #t) (k 0)) (d (n "kassatypes") (r "^0.1") (d #t) (k 0)))) (h "1fp9w1k5xbcv7gwcw6r2npyxnvaxzbw8mx0sq90k0j2j3812w49l") (f (quote (("lcu" "kassaroutes/lcu") ("default" "api" "lcu") ("api" "kassaroutes/api"))))))

