(define-module (crates-io ka ss kassapi) #:use-module (crates-io))

(define-public crate-kassapi-0.1.0 (c (n "kassapi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "kassaroutes") (r "^0.1") (f (quote ("api"))) (d #t) (k 0)) (d (n "kassatypes") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 2)))) (h "0p0f4lbk97l4s6q61av4d44bdl4h1awbxr0sxhamirqj4nmjc3vh")))

