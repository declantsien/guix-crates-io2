(define-module (crates-io ka rt kart-graphics-engine) #:use-module (crates-io))

(define-public crate-kart-graphics-engine-0.1.0 (c (n "kart-graphics-engine") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "0iwdpnn3x1gwjhh7a8qn6p06acypndim2w10wqiw7g1aw53ksbdx")))

(define-public crate-kart-graphics-engine-0.2.0 (c (n "kart-graphics-engine") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "1nx6lhrx9j2173dqza4pajifyi19wnii0xx9ch9axdsvi6qh1qmd")))

(define-public crate-kart-graphics-engine-0.2.1 (c (n "kart-graphics-engine") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "19fd3alxcp7yrnw7cq97zxwhz9v9fs4xvswchmbz5pzz0v1c5i8y")))

(define-public crate-kart-graphics-engine-0.2.2 (c (n "kart-graphics-engine") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "08ibzm9i4lpcw3did0bj9bqmdmqrw2gfsjl4rjfnma1cyfsllfna")))

(define-public crate-kart-graphics-engine-0.2.3 (c (n "kart-graphics-engine") (v "0.2.3") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "0lf3rgrf7k87arnvxsdxx3d921pj30vgvz3aljlcjpq3vrkjxzc9")))

(define-public crate-kart-graphics-engine-0.2.4 (c (n "kart-graphics-engine") (v "0.2.4") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "0388xjnbsvh7skpari6p755mzwc8cyy4mss1fvwr0hvbnlrfshd1")))

