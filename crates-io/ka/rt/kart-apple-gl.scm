(define-module (crates-io ka rt kart-apple-gl) #:use-module (crates-io))

(define-public crate-kart-apple-gl-0.3.1 (c (n "kart-apple-gl") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "14djf8kd4bczbkxvq0b0h0yq3y2ppi9zhmc9wmljldd0yyiz3s2a")))

(define-public crate-kart-apple-gl-0.4.1 (c (n "kart-apple-gl") (v "0.4.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "09vkdjc3isw2ji2mcsmz2irmwr3b08n24j1gm91386nky7mgd06y")))

(define-public crate-kart-apple-gl-0.4.2 (c (n "kart-apple-gl") (v "0.4.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "09lagbf42i2220hxvs6is2fnw955g2ipjr0zpz3j2g43y4x5m0sp")))

(define-public crate-kart-apple-gl-0.4.41 (c (n "kart-apple-gl") (v "0.4.41") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.53.0") (d #t) (k 0)))) (h "03vrx4cq2bsy4mryasr2lign9lqm69961awg9im8s88smmsh75hd")))

