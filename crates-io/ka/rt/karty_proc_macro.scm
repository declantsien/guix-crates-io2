(define-module (crates-io ka rt karty_proc_macro) #:use-module (crates-io))

(define-public crate-karty_proc_macro-0.1.0 (c (n "karty_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1g2si9wbhpig2raac7iqgjnp9jd66bqq63pwi868321wjdz0h53r")))

(define-public crate-karty_proc_macro-0.2.0 (c (n "karty_proc_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "1906vd6hzlzzbfwvh2a0yj1ffx9qnmsp8b699bhacdxc5j10njcy")))

(define-public crate-karty_proc_macro-0.2.1 (c (n "karty_proc_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1mmz37ij4qwkdf6xkas96jpb8qv0x6y2671h8f7528nyl3fp7jcj")))

