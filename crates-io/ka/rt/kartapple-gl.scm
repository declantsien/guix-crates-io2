(define-module (crates-io ka rt kartapple-gl) #:use-module (crates-io))

(define-public crate-KartApple-GL-0.3.0 (c (n "KartApple-GL") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.52.0") (d #t) (k 0)))) (h "0rqbpjhviglg3pfsp8kxqxddr36z5yfsi1c2786qr5gizf207gwm") (y #t)))

