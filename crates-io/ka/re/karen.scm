(define-module (crates-io ka re karen) #:use-module (crates-io))

(define-public crate-karen-0.1.0 (c (n "karen") (v "0.1.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "simple_logger") (r ">=4") (d #t) (k 2)))) (h "1skq3rx7rnbnvnqqppbx6rr6mdqrqhvda2zgin9mpvv10a4h12r1")))

