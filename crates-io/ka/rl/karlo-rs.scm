(define-module (crates-io ka rl karlo-rs) #:use-module (crates-io))

(define-public crate-karlo-rs-1.0.0 (c (n "karlo-rs") (v "1.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "121a9z11i54lgwm72kr82d9hs909m8j8kaqr6k10bbih67f7ndag")))

(define-public crate-karlo-rs-1.0.1 (c (n "karlo-rs") (v "1.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w3z5h1hwmlwhx8w92jbs4ibyr1d58da9xch2dfq3dkg51scwf5a")))

