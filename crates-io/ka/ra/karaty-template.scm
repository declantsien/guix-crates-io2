(define-module (crates-io ka ra karaty-template) #:use-module (crates-io))

(define-public crate-karaty-template-0.2.0 (c (n "karaty-template") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-retrouter") (r "^0.4.0") (f (quote ("web"))) (d #t) (k 0)) (d (n "karaty-blueprint") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "markdown-meta-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwasm") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1y0c4anbfgpiw854g3bj6mah3cr0rkp300x1kd3mi0mqjlb5cx8h")))

