(define-module (crates-io ka ra karaty-blueprint) #:use-module (crates-io))

(define-public crate-karaty-blueprint-0.2.0 (c (n "karaty-blueprint") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwasm") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0jpy3489wjf3yyma5p4hvrdqmyzd0zzx6703g6ldgi1i7i672cij")))

