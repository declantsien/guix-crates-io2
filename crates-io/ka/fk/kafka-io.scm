(define-module (crates-io ka fk kafka-io) #:use-module (crates-io))

(define-public crate-kafka-io-0.1.0 (c (n "kafka-io") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rdkafka") (r "^0.24") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bf3i1msg5h6xf1r3shb4irnxd8i77127ppcv24ppavvhl6pl8nw")))

(define-public crate-kafka-io-0.2.0 (c (n "kafka-io") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rdkafka") (r "^0.24") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rw51gbm26xd67wixk22adywbd2xxa9av1w9x02q5yi3v94x8b04")))

