(define-module (crates-io ka fk kafka-threadpool) #:use-module (crates-io))

(define-public crate-kafka-threadpool-1.0.0 (c (n "kafka-threadpool") (v "1.0.0") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0g4wj8wm5x0bxqapw750cyrz56ns8wjqdbvz6ga70083ladxpk9w")))

(define-public crate-kafka-threadpool-1.0.1 (c (n "kafka-threadpool") (v "1.0.1") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1s2f49hcxmrc2m6cy9lr1z2azrfway0dg035mq163dj2b0pqs5i2")))

(define-public crate-kafka-threadpool-1.0.2 (c (n "kafka-threadpool") (v "1.0.2") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1j4fgf3m363534zarix9lqd07xl99604aqyjl7fa0qkjycyjdwbp")))

(define-public crate-kafka-threadpool-1.0.3 (c (n "kafka-threadpool") (v "1.0.3") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0bpbn7p4g4ldhwpb68z892wkz8kpg0k2r58bmkn6brf5g6ls8a5q")))

(define-public crate-kafka-threadpool-1.0.4 (c (n "kafka-threadpool") (v "1.0.4") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1jrp6m4dv30mfrrkbh57lq805dzgvplvlvj1sg2vk41923dcak2m")))

(define-public crate-kafka-threadpool-1.0.5 (c (n "kafka-threadpool") (v "1.0.5") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0jk3q0srqa1nfzf8a2d8qak3jg36xsm4s16pka7xsmjdqisp5c44")))

(define-public crate-kafka-threadpool-1.0.6 (c (n "kafka-threadpool") (v "1.0.6") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0pkyrpnxypvzfxbppwsq6isfmc67528w6a1c83hiy7zjlvk52ykv")))

(define-public crate-kafka-threadpool-1.0.7 (c (n "kafka-threadpool") (v "1.0.7") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0rvsmf7227n1dn5vn26icsqha604nrc4sxfd4c1kgpgb8hgwp5i1")))

(define-public crate-kafka-threadpool-1.0.8 (c (n "kafka-threadpool") (v "1.0.8") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0xx4qw8lqjw28m79i4fdawr6fz2kfsj15wj4fzk7cgnr1mh7bkn0")))

(define-public crate-kafka-threadpool-1.0.9 (c (n "kafka-threadpool") (v "1.0.9") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1888vig202h54blz3asnjqhzzmrny3pfg3injls590bn4mfb4z6w")))

(define-public crate-kafka-threadpool-1.0.10 (c (n "kafka-threadpool") (v "1.0.10") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0w690jsm1kb8mr1c7k9fzwa35svzc67iip8xj22b82zf4zrsglw7")))

(define-public crate-kafka-threadpool-1.0.11 (c (n "kafka-threadpool") (v "1.0.11") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1frh0lxl5l744hzm9hs953zizyqphjkzcqjygsambs369hqnqkcy")))

(define-public crate-kafka-threadpool-1.0.12 (c (n "kafka-threadpool") (v "1.0.12") (d (list (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (f (quote ("cmake-build" "ssl" "ssl-vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0kymxznadhj1c8mqh06w47h25nqnr2782z7jqa618ny1xhnsnw7s")))

