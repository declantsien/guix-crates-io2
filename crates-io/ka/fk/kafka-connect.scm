(define-module (crates-io ka fk kafka-connect) #:use-module (crates-io))

(define-public crate-kafka-connect-0.0.1-alpha.0 (c (n "kafka-connect") (v "0.0.1-alpha.0") (h "1qxzpdm57j8bhaw08kz53kaq6qy93qmqlyss922fa2rzylbs3fgk") (y #t)))

(define-public crate-kafka-connect-0.0.0 (c (n "kafka-connect") (v "0.0.0") (h "0sfp8fnfg3x405fmfmmbn7h7l7irjb4ppg5z34mcf1rszgddm2rs")))

