(define-module (crates-io ka fk kafkaesque-macros) #:use-module (crates-io))

(define-public crate-kafkaesque-macros-0.0.5 (c (n "kafkaesque-macros") (v "0.0.5") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1wr4wr3ic70rkd3qm4xl7whj1dnnsxzav9r8z9wbpyb0qdg1rx20")))

(define-public crate-kafkaesque-macros-0.0.6 (c (n "kafkaesque-macros") (v "0.0.6") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "138dl17fzssqrbanvzm6x37kvk8f8qh4gsz5iby4knhffik8bidv")))

(define-public crate-kafkaesque-macros-0.0.7 (c (n "kafkaesque-macros") (v "0.0.7") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0f11x8kwm6lj1mqjisvh1j8h36z716gaz47s25y5ddvxfxxid8ml")))

(define-public crate-kafkaesque-macros-0.0.8 (c (n "kafkaesque-macros") (v "0.0.8") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1hz99mgmayxkk7nkagrf4mvzwi07xb1lxcmvhd0chjdkk8qym9m1")))

(define-public crate-kafkaesque-macros-0.0.9 (c (n "kafkaesque-macros") (v "0.0.9") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1wq272xycbn0wc7l2i1lckwcd6nq55ssbh01z444wbb53kpckxsp")))

(define-public crate-kafkaesque-macros-0.0.10 (c (n "kafkaesque-macros") (v "0.0.10") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1yl1kcr20npzi0p5k7dm0d08s867j224zpffsd0xgd672pf9srf1")))

(define-public crate-kafkaesque-macros-0.0.11 (c (n "kafkaesque-macros") (v "0.0.11") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "033qy3sa8957jbp1wghlw3radygz8qdlxx07f35l04lq9bvczh2i")))

(define-public crate-kafkaesque-macros-0.0.12 (c (n "kafkaesque-macros") (v "0.0.12") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjgs343mj5wgwncnhiywy0b20cvv7jrk0abpgwz75f5318a3v58")))

(define-public crate-kafkaesque-macros-0.0.13 (c (n "kafkaesque-macros") (v "0.0.13") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0m0i3bi6jq4y9hq4z3100khzwc0z581qv6wbg4l10jixpggipgsq")))

(define-public crate-kafkaesque-macros-0.0.14 (c (n "kafkaesque-macros") (v "0.0.14") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3n8iq6v062609x4gn480m8r6p4zf6i8zy8p3vc7hm4k1sykmzw")))

(define-public crate-kafkaesque-macros-0.0.15 (c (n "kafkaesque-macros") (v "0.0.15") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "19wzjk6lnqq4xg17m4mqq27a68v10s5lynrcm0jx65d0blxzzxf1")))

