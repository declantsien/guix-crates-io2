(define-module (crates-io ka fk kafka-serde) #:use-module (crates-io))

(define-public crate-kafka-serde-0.1.0 (c (n "kafka-serde") (v "0.1.0") (d (list (d (n "endianness") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0dfj5rxw6ccwi8iqi00ywvr3l8xxb321vjxfgnns7nxmznj5ikc3")))

