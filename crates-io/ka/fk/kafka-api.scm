(define-module (crates-io ka fk kafka-api) #:use-module (crates-io))

(define-public crate-kafka-api-0.1.0 (c (n "kafka-api") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ac97ycdc90n8fyb06b4jgii8913pdz062zn396iz91da1p2myr8")))

(define-public crate-kafka-api-0.2.0 (c (n "kafka-api") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1r8xn2yds1zqpfb0ndbcqwprqzxybkwsbxnjaykxiw8k99hans2j")))

(define-public crate-kafka-api-0.2.1 (c (n "kafka-api") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s2q4q9c2c7xq0a9cvcmnc7was4399y0ihshc7dv8n6cszg7axlr")))

(define-public crate-kafka-api-0.2.2 (c (n "kafka-api") (v "0.2.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p0fgsaqba6lqyysnmva3a3gr4la8ajpapasz6d28998nq3d09zy")))

(define-public crate-kafka-api-0.2.3 (c (n "kafka-api") (v "0.2.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "11y7kw82cgsnqz7wzjspj0ss87jpdvxrvl8bb6a027pbkh505d34")))

