(define-module (crates-io ka fk kafka-offsets-parser) #:use-module (crates-io))

(define-public crate-kafka-offsets-parser-0.1.0 (c (n "kafka-offsets-parser") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rdkafka") (r "^0.26") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "180f6ds9gbib4qfkmry0874xi6m7cv36jaz3pi8s4a6cwh09zkgi")))

(define-public crate-kafka-offsets-parser-0.1.1 (c (n "kafka-offsets-parser") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rdkafka") (r "^0.26") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "0zlkyvdlshjji8nkh9zdy4nj2565r4gm73hcwkj2g0y4s3vw7p4z")))

(define-public crate-kafka-offsets-parser-0.1.2 (c (n "kafka-offsets-parser") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rdkafka") (r "^0.26") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "09j4lwc464syl86cdj3cgx31b0h4spynk971y00xbcxnklphijg8")))

