(define-module (crates-io ka -p ka-pi) #:use-module (crates-io))

(define-public crate-ka-pi-0.0.1 (c (n "ka-pi") (v "0.0.1") (d (list (d (n "jni") (r "^0.20.0") (f (quote ("invocation"))) (d #t) (k 0)))) (h "01v2wpf3ba2mwiwzwr27h7i8pbbynlpc0006vkl6k7ph59nwaqww")))

(define-public crate-ka-pi-0.1.0 (c (n "ka-pi") (v "0.1.0") (d (list (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "035g27vpvjgi6a0gnn9fph4i89hgr3592vnwg46rhrxddw5m2w8w") (f (quote (("parse") ("generate") ("default" "parse"))))))

(define-public crate-ka-pi-0.2.0 (c (n "ka-pi") (v "0.2.0") (d (list (d (n "cfsp") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)))) (h "1rdckkfrdyirxdk2vw5xwhzwlb8qr9pxl7q157wxg4hzh556yr27") (f (quote (("default")))) (s 2) (e (quote (("cfsp" "dep:cfsp"))))))

