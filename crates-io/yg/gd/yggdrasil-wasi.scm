(define-module (crates-io yg gd yggdrasil-wasi) #:use-module (crates-io))

(define-public crate-yggdrasil-wasi-0.0.0 (c (n "yggdrasil-wasi") (v "0.0.0") (d (list (d (n "rctree") (r "^0.6.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.22.0") (d #t) (k 0)) (d (n "yggdrasil-rt") (r "^0.1.2") (d #t) (k 0)))) (h "0rz37sfg63apvm0y3jszh3x3qvh75am9r7fyfs1lg9g3vyiai3la") (f (quote (("default"))))))

