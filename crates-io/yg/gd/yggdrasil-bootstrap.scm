(define-module (crates-io yg gd yggdrasil-bootstrap) #:use-module (crates-io))

(define-public crate-yggdrasil-bootstrap-0.1.0 (c (n "yggdrasil-bootstrap") (v "0.1.0") (d (list (d (n "peginator") (r "^0.2.0") (d #t) (k 0)) (d (n "peginator") (r "^0.2.0") (d #t) (k 1)) (d (n "yggdrasil-shared") (r "^0.1") (f (quote ("lsp"))) (d #t) (k 0)))) (h "1ysrrcfyjji7gbsgxd1avsdrjgmg6vvrpvw8n5f5p434hm0fqgv7") (f (quote (("no-unnamed") ("no-ignored") ("default" "no-unnamed" "no-ignored")))) (y #t)))

