(define-module (crates-io yg gd yggdrasil-rt) #:use-module (crates-io))

(define-public crate-yggdrasil-rt-0.0.0 (c (n "yggdrasil-rt") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.4.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (f (quote ("proposed"))) (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1j9ws2279hzzwz14hz0aph4l23rxnhh2j8jyqbvs8yfnp6c6yqzb") (f (quote (("lsp" "lsp-types") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.1 (c (n "yggdrasil-rt") (v "0.0.1") (d (list (d (n "diagnostic-quick") (r "^0.4.0") (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (f (quote ("proposed"))) (o #t) (d #t) (k 0)) (d (n "pex") (r "^0.0.11") (f (quote ("ucd-trie"))) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1qfnvk0myb3x69kvysasbr9nvxin22rzlm4j5n1bga7njrh3kkin") (f (quote (("regex" "regex-automata" "pex/regex-automata") ("lsp" "lsp-types") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.2 (c (n "yggdrasil-rt") (v "0.0.2") (d (list (d (n "diagnostic-quick") (r "^0.4.0") (d #t) (k 0)) (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (f (quote ("proposed"))) (o #t) (d #t) (k 0)) (d (n "pex") (r "^0.1.0") (f (quote ("ucd-trie"))) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1chhr6f0wx8crfwv9i3cgmm8xwywc8j4aaczjddz986h9gqq8xr3") (f (quote (("regex" "regex-automata" "pex/regex-automata") ("lsp" "lsp-types") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.3 (c (n "yggdrasil-rt") (v "0.0.3") (d (list (d (n "indextree") (r "^4.6.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (f (quote ("proposed"))) (o #t) (d #t) (k 0)) (d (n "pex") (r "^0.1.6") (f (quote ("ucd-trie"))) (d #t) (k 0)) (d (n "pratt") (r "^0.4.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "13xf6q8w9is4mfsg2gdkm1pvq80m3mfphijb2rz3pbgpgljpxmi2") (f (quote (("regex" "regex-automata" "pex/regex-automata") ("lsp" "lsp-types" "url") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.4 (c (n "yggdrasil-rt") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex-automata") (r "^0.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "18vqr5k85nz7206rdmyib9g7i7j3s7wfknbljwhwilyagrc1z7gr") (f (quote (("default") ("const_prec_climber"))))))

(define-public crate-yggdrasil-rt-0.0.5 (c (n "yggdrasil-rt") (v "0.0.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex-automata") (r "^0.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (k 0)))) (h "08wjxwf1m2djf0m8ap9fb6fy2si2lkz4hw85j3dx8dl3riqc5v90") (f (quote (("default") ("const_prec_climber"))))))

(define-public crate-yggdrasil-rt-0.0.6 (c (n "yggdrasil-rt") (v "0.0.6") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0knr4zfdkzxjjn922pvjafzg0xvmifbd51amgyrn7s63zrshzmf3") (f (quote (("default") ("const_prec_climber"))))))

(define-public crate-yggdrasil-rt-0.0.7 (c (n "yggdrasil-rt") (v "0.0.7") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "052jsnnqw0pn5inp9l5sqdarfrvr546dq6ysd4fpqb020df2hja3") (f (quote (("dynamic") ("default") ("const_prec_climber"))))))

(define-public crate-yggdrasil-rt-0.0.8 (c (n "yggdrasil-rt") (v "0.0.8") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1wzhqizdg6x1im7prpgn3m0391ly3rh8hds8ib2ip1a2cj07sbz0") (f (quote (("dynamic") ("default") ("const_prec_climber"))))))

(define-public crate-yggdrasil-rt-0.0.9 (c (n "yggdrasil-rt") (v "0.0.9") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "083wg4praviwv0c547avc8yc3w0p0xayk0q3pcpagpqaczm5pngr") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.10 (c (n "yggdrasil-rt") (v "0.0.10") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1783ngxadz5q9n1n044jmwz5lgrm17mhq5iz23v3036sp13dnqpz") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.11 (c (n "yggdrasil-rt") (v "0.0.11") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "16wzkbqph2lnz3f76g89ifyvb2bfs55spc5dpicr387z6n9agy6j") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.13 (c (n "yggdrasil-rt") (v "0.0.13") (d (list (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)))) (h "1aygyj6ln606i1p5ngy88ivpad7skfikyk9hln5k1gygdwfzj4d4") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.14 (c (n "yggdrasil-rt") (v "0.0.14") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)))) (h "1vcbv8ipn7hwgdvrjk5ksyg1qsqqxmnjv6y8wvinzshyl1dhxrwd") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.15 (c (n "yggdrasil-rt") (v "0.0.15") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)))) (h "1mrn6nfl8ckxnh2cbzl8lr6a2r36286ryrgg6isyhn1rf9wsz658") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.0.16 (c (n "yggdrasil-rt") (v "0.0.16") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)))) (h "19a3y4ar90isfy744bz9ypmc24v1fz3ypw1wj6f8m3h3g1h1sqxi") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.1.0 (c (n "yggdrasil-rt") (v "0.1.0") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)))) (h "0jdc9994jpjnsqiy0v6zyyqn9c98v9qlm8qng9ypgqcy34p3g1cf") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.1.1 (c (n "yggdrasil-rt") (v "0.1.1") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)))) (h "1zsbxaq3xr72xcgkabjm1qlxiq77prd78h02wiw2g111087g9qqc") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.1.2 (c (n "yggdrasil-rt") (v "0.1.2") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)))) (h "1nlrkn7sr6q491rg8w4051xazw5vzxxxldm8dsh4c8wvcfzv92xw") (f (quote (("dynamic") ("default"))))))

(define-public crate-yggdrasil-rt-0.1.3 (c (n "yggdrasil-rt") (v "0.1.3") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)))) (h "037zzfaggxwp945mqnad9v3asnx7jqrhy88x1h7vpzg9jn4xgvwl") (f (quote (("dynamic") ("default"))))))

