(define-module (crates-io yg gd yggdrasil-viewer) #:use-module (crates-io))

(define-public crate-yggdrasil-viewer-0.0.0 (c (n "yggdrasil-viewer") (v "0.0.0") (d (list (d (n "shape-svg") (r "0.0.*") (d #t) (k 0)) (d (n "svg") (r "^0.14.0") (d #t) (k 0)) (d (n "tree-layout") (r "0.0.*") (d #t) (k 0)) (d (n "yggdrasil-parser") (r "0.1.*") (d #t) (k 2)) (d (n "yggdrasil-rt") (r "0.0.*") (d #t) (k 0)))) (h "1z94597jqskrk0gqmxhk1anq58x5nzyppgjq13aqzcqyppxv2w3b") (f (quote (("default"))))))

