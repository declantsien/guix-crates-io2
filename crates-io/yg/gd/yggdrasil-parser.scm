(define-module (crates-io yg gd yggdrasil-parser) #:use-module (crates-io))

(define-public crate-yggdrasil-parser-0.0.0 (c (n "yggdrasil-parser") (v "0.0.0") (d (list (d (n "yggdrasil-rt") (r "^0.0.2") (d #t) (k 0)))) (h "0hjmdmi7kvhlx8gc5zlh81w6qi2r7wpmpixbags3r40vv932kvim") (f (quote (("default"))))))

(define-public crate-yggdrasil-parser-0.0.1 (c (n "yggdrasil-parser") (v "0.0.1") (d (list (d (n "yggdrasil-rt") (r "^0.0.2") (d #t) (k 0)))) (h "03d43nfinzcb0wfwmyhp8g5v4qdxkm1jf0m6wiy5sbnwyw0sz2zl") (f (quote (("default"))))))

(define-public crate-yggdrasil-parser-0.0.2 (c (n "yggdrasil-parser") (v "0.0.2") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "yggdrasil-ir") (r "0.1.*") (d #t) (k 0)) (d (n "yggdrasil-rt") (r "0.0.*") (d #t) (k 0)))) (h "0m079ac58khm514lrnshrisnrj3xvvpdx6zsxr1yhyrmdq9milna")))

(define-public crate-yggdrasil-parser-0.0.3 (c (n "yggdrasil-parser") (v "0.0.3") (d (list (d (n "yggdrasil-rt") (r "0.0.*") (d #t) (k 0)))) (h "07akh1755lysr6hp8820c1py19679273w3y6d3b2dyy302n5zdm7")))

(define-public crate-yggdrasil-parser-0.0.4 (c (n "yggdrasil-parser") (v "0.0.4") (d (list (d (n "yggdrasil-rt") (r "0.0.*") (d #t) (k 0)))) (h "1d6nmpc8b7iv4ch1ljcvsjg0a52naxhk324c3kx8an93i8j3mldw")))

(define-public crate-yggdrasil-parser-0.1.0 (c (n "yggdrasil-parser") (v "0.1.0") (d (list (d (n "yggdrasil-rt") (r "0.0.*") (d #t) (k 0)))) (h "1idy23ds838v4x8m3xnyibwxasnimgygh9bdkaai55amc1x27lk1")))

