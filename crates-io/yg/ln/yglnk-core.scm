(define-module (crates-io yg ln yglnk-core) #:use-module (crates-io))

(define-public crate-yglnk-core-0.0.0 (c (n "yglnk-core") (v "0.0.0") (d (list (d (n "int-enum") (r "^0.5") (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh64"))) (d #t) (k 0)))) (h "02vpcwmakv1c4vk7vbpc5ngn79ifm1x4si2y7sk9xr3ks9n9375z")))

(define-public crate-yglnk-core-0.0.1 (c (n "yglnk-core") (v "0.0.1") (d (list (d (n "int-enum") (r "^0.5") (k 0)) (d (n "memchr") (r "^2.5") (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh64"))) (d #t) (k 0)))) (h "119x8lvfzr8nknfw6mjrzgmjk67vc2l70gzz21v7nvn1imsflv64")))

(define-public crate-yglnk-core-0.0.2 (c (n "yglnk-core") (v "0.0.2") (d (list (d (n "int-enum") (r "^0.5") (k 0)) (d (n "memchr") (r "^2.5") (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh64"))) (d #t) (k 0)))) (h "0gx8r3in83k2bpxjz1658cyhhq3q0lmwf3vmaxqkrw88ficpfsv8") (f (quote (("alloc"))))))

