(define-module (crates-io yg op ygoprodb) #:use-module (crates-io))

(define-public crate-ygoprodb-0.1.0 (c (n "ygoprodb") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("color" "suggestions" "yaml"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4303ld3j0gvb270i2ljyrzjcr3j0l6hli7dymwc2s8y5pz7f08")))

