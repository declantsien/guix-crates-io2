(define-module (crates-io ou tc outcome-sim) #:use-module (crates-io))

(define-public crate-outcome-sim-0.1.0 (c (n "outcome-sim") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0lqr9kg7xfclwzf2ai8b042s9wnq2d19h5p45njxv21mx7dsyaw4") (y #t)))

