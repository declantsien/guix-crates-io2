(define-module (crates-io ou tc outcome) #:use-module (crates-io))

(define-public crate-outcome-0.1.0 (c (n "outcome") (v "0.1.0") (h "11s7pra2vpw0608lg8vf3wafb43c9kpvj1xqviq6rwp0np5bmz5m") (y #t)))

(define-public crate-outcome-0.1.1 (c (n "outcome") (v "0.1.1") (h "1aa1d5lyrx1lgnbhan08y30cly5w840jq3n0pdfk609b1wfdcgnw")))

(define-public crate-outcome-0.1.2 (c (n "outcome") (v "0.1.2") (h "0ybm4ilm9460scy4vxs23n53alcv3kyiaxs5wi5zi3h19g2jwa1l")))

(define-public crate-outcome-0.1.3 (c (n "outcome") (v "0.1.3") (h "1cp5q8yc3hjxyzpmhv02a40z2flv6wj6j4xmknbmvjpq0wn40wjf")))

(define-public crate-outcome-0.1.4 (c (n "outcome") (v "0.1.4") (h "0c4qsvrvgcbdc41q6fkxiajv7map1djkrym7vy0xfipkfrlym3hh")))

(define-public crate-outcome-0.1.5 (c (n "outcome") (v "0.1.5") (h "0cv1gy7dqnmzhbx5bnkzmvh0j66ivvbyq0j3navb7ijmdjfgdcin")))

(define-public crate-outcome-0.1.6 (c (n "outcome") (v "0.1.6") (h "1szhjca949i9dgg3f0b0pm01mw4v6slpq0b0rgifadd0qyrwxqb5")))

(define-public crate-outcome-0.1.7 (c (n "outcome") (v "0.1.7") (h "19vbynijr40mwppbzcs2sqgxg87km0v953zg4aixrzvygfq5kzr0")))

