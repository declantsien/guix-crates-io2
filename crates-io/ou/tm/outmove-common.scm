(define-module (crates-io ou tm outmove-common) #:use-module (crates-io))

(define-public crate-outmove-common-0.1.0 (c (n "outmove-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bcs") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "mirai-annotations") (r "^1.10.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "01wm0qcrmadnxbaw2s0cw78jx4i543i50l9ky4crl4bzzxay4swq")))

