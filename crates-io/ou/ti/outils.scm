(define-module (crates-io ou ti outils) #:use-module (crates-io))

(define-public crate-outils-0.1.0 (c (n "outils") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0cb7d3k6w7aizwjyxyhvhirxxzqz8f51ppw64061dyql6dpwd28l")))

(define-public crate-outils-0.1.1 (c (n "outils") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1p8hx7nidq5nq9wyas9n7crkfnl17709ylb0qbqcka1wca0drx9c")))

(define-public crate-outils-0.1.2 (c (n "outils") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "slab") (r "^0.4.1") (d #t) (k 0)))) (h "1yvlhd1r4kyrw76scv6gq943z9y95xdxkm0aqv0gjz366i147bqg")))

(define-public crate-outils-0.1.3 (c (n "outils") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "slab") (r "^0.4.1") (d #t) (k 0)))) (h "1njvm3xkydsb2pkmpc59rhag47m1bfdfxjjklf1hgxv4lvd4a0sj")))

(define-public crate-outils-0.2.0 (c (n "outils") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0byipqxqv2vmbflv0x6siy00dzbb8rgaw5yxv5miwyj9n0lfhdjv")))

(define-public crate-outils-0.3.0 (c (n "outils") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0nry4jfmpc9475zbq0j4qfzg4v8aajjpy5a80fcxsbhr69bvrwky")))

