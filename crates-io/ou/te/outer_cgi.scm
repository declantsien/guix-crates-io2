(define-module (crates-io ou te outer_cgi) #:use-module (crates-io))

(define-public crate-outer_cgi-0.2.0 (c (n "outer_cgi") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0bbifdn9z8rsmb6zxql3lxkhnwwnrfxilb2hgb0b2dz38m3lac4k")))

(define-public crate-outer_cgi-0.2.1 (c (n "outer_cgi") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.10") (d #t) (t "cfg(unix)") (k 0)))) (h "0ax7vpfji4p8zvnyjy5lf8bgar6zz95j75f5fkz2pkzz93c1h6ww")))

(define-public crate-outer_cgi-0.2.2 (c (n "outer_cgi") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1ws215arv1asl9kqxja0b0r26r34a2mxpk0wxlin4m1pa8fr4mdd")))

(define-public crate-outer_cgi-0.2.3 (c (n "outer_cgi") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1krjmxhb9mngxy2i9gbynpmxzk87q75ys0v8c3md2bgqq9y9dl4d")))

(define-public crate-outer_cgi-0.2.4 (c (n "outer_cgi") (v "0.2.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "1r6icidfdad0ypy70qiwd0sg7xr9fldf2kilq3pwkd6b6zvaw3d6")))

(define-public crate-outer_cgi-0.2.5 (c (n "outer_cgi") (v "0.2.5") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "07qzslz36hqgdqdwk949c76m5smml3awxaza5flnzy0w0yswa02q")))

(define-public crate-outer_cgi-0.3.0 (c (n "outer_cgi") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "1ccjywlpma1q77gl5w4344m8n3776swdnmm39myj3x1py28sgcr4")))

(define-public crate-outer_cgi-0.3.1 (c (n "outer_cgi") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "013p4kardgmfk96mncq15z45b8xmscbp4arhg15qd3jrbrr87w33")))

