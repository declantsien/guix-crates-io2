(define-module (crates-io ou te outer_attribute) #:use-module (crates-io))

(define-public crate-outer_attribute-0.1.0 (c (n "outer_attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "08h8znyfb5phd032gmysss2n7xwcfa439blrkds7jzc2hbisqmk2") (f (quote (("same_layout") ("different_layout") ("default" "different_layout")))) (r "1.56.1")))

(define-public crate-outer_attribute-0.1.1 (c (n "outer_attribute") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1r0a1a707686wb6v3xx95hjcyh1synd2rpkfnsr2yi2xxfs2ky1k") (f (quote (("same_layout") ("different_layout") ("default" "different_layout")))) (r "1.56.1")))

