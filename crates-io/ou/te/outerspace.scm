(define-module (crates-io ou te outerspace) #:use-module (crates-io))

(define-public crate-outerspace-0.1.0 (c (n "outerspace") (v "0.1.0") (h "0vaywkf4na5c07i93cx45lyb027gnr5j4l0vlwbbg4cz4pj3f6qk")))

(define-public crate-outerspace-0.2.0 (c (n "outerspace") (v "0.2.0") (h "198j8q7zjnxgh0dgkjca54yc4kbn6zhj2x3ac7n6h7czm3vbplww")))

(define-public crate-outerspace-0.2.1 (c (n "outerspace") (v "0.2.1") (h "1zcqks1sx6648vqn8zmdvnckqw9zzz0zcvjp0m2jcw2rmkvgkwfi")))

