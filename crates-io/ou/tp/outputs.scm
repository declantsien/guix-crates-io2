(define-module (crates-io ou tp outputs) #:use-module (crates-io))

(define-public crate-outputs-0.0.1 (c (n "outputs") (v "0.0.1") (h "01fnkm9b04b6i4byixqwgz3l216fihlgx81b54hjpbk70q4734a5")))

(define-public crate-outputs-0.0.2 (c (n "outputs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0gms0x0ihnjzsz70xl421ri5yz27y5zvijdc7j36kpjs3gisaa4w")))

(define-public crate-outputs-0.0.3 (c (n "outputs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1vbhqv6mb3xmgbk2511qipxcwb40knn22y8sjhs5kp6rkgqs1fig")))

(define-public crate-outputs-0.0.4 (c (n "outputs") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "03jlcwmyv12mbpgpkbc6qww91vq4v1lisirh8ykwzk815g0lyphg")))

(define-public crate-outputs-0.0.5 (c (n "outputs") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0qnqdcrxj875d6pnigyj0ybny2kq0pn11i28krrdgq8j8ah6f6gg")))

(define-public crate-outputs-0.0.6 (c (n "outputs") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1nvfq19h907b2mkpk1zsw473h3pyxd06b9y3w11n75cj3hxsvq4f")))

