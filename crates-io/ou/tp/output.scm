(define-module (crates-io ou tp output) #:use-module (crates-io))

(define-public crate-output-0.1.0 (c (n "output") (v "0.1.0") (d (list (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0var2lnxg95cggg42mmx12qfbrb7qa0jzdv3bqs0bzxhv0nhf47x")))

(define-public crate-output-0.1.2 (c (n "output") (v "0.1.2") (d (list (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "164d6y7hs3gwzn2bd35al1v924l9i0547h63rw3vgfbjyi5crc04")))

(define-public crate-output-0.2.0 (c (n "output") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "1v08r0w8bzjdlly22w199k1fmwn9msxpydhk974zv3mwfkg395fd")))

(define-public crate-output-0.3.0 (c (n "output") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0k3nw302akqgvp7yk882ziq16njvyljqwcizpcfb1zaz31axm7ik")))

(define-public crate-output-0.3.1 (c (n "output") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0ljhvxk1iliwy8mxalp0x6sdbxrxhk2idjhmsz2n8hjjzq3nbi4c")))

(define-public crate-output-0.4.0 (c (n "output") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "1w47d683dwjncb33wbvkzfj8aqm38ggazfrpp55ajisnk57x16j9")))

(define-public crate-output-0.4.1 (c (n "output") (v "0.4.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0wk7cwhns3x0xd2i936w90hfr57cqy4kbfapnrzn5brxgcx5aj7s")))

(define-public crate-output-0.5.0 (c (n "output") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0y7w89av5j75bf8p3mf49bsb859bbkx85i00dra1hcyf6w3x3jbh")))

(define-public crate-output-0.6.0 (c (n "output") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0ccn0ijc05rrcnnm43b7jnw4ba49y3p9lgjgss5hhqd47a6h3g73")))

(define-public crate-output-0.6.1 (c (n "output") (v "0.6.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "0jldppzip49gwnzn2dz79d352sg3xg9xgdlj1aw3dn4xsxk5fmyr")))

(define-public crate-output-0.6.2 (c (n "output") (v "0.6.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.1") (d #t) (k 0)))) (h "1x6fdwxrhk1j1kjy6m0m4gfbwhw6j9rgjh78mz2qbvn7bjqqfarv")))

