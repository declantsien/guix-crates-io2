(define-module (crates-io ou tp output_vt100) #:use-module (crates-io))

(define-public crate-output_vt100-0.1.0 (c (n "output_vt100") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("winuser" "winbase" "consoleapi" "processenv"))) (d #t) (k 0)))) (h "14c5q6f6gwqrszjy4nx6sr6dzq0jy0b8x12i1w40vhb5irf3401y")))

(define-public crate-output_vt100-0.1.1 (c (n "output_vt100") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("winuser" "winbase" "consoleapi" "processenv"))) (d #t) (k 0)))) (h "1xxi9wvkhzbfzs4pmfgbn69i9pgdf415346nchh0kvfl12xp380s")))

(define-public crate-output_vt100-0.1.2 (c (n "output_vt100") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("winuser" "winbase" "consoleapi" "processenv"))) (d #t) (k 0)))) (h "1ygqplpxz4gg3i8f3rkan2q69pqll7gv65l2mmd8r9dphnvwbkak")))

(define-public crate-output_vt100-0.1.3 (c (n "output_vt100") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("winuser" "winbase" "consoleapi" "processenv"))) (d #t) (k 0)))) (h "0rpvpiq7gkyvvwyp9sk0zxhbk99ldlrv5q3ycr03wkmbxgx270k2")))

