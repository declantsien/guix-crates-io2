(define-module (crates-io ou ta outagefs) #:use-module (crates-io))

(define-public crate-outagefs-0.1.0 (c (n "outagefs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "shell-words") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thread-scoped") (r "^1.0.2") (d #t) (k 0)) (d (n "varbincode") (r "^0.1") (d #t) (k 0)))) (h "1pz3inv3wg82ih01m32mcq4iq6pm0psn52syfpwwh3qfg5rgi9j1")))

