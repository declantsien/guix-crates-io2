(define-module (crates-io ou td outdir-tempdir) #:use-module (crates-io))

(define-public crate-outdir-tempdir-0.1.0 (c (n "outdir-tempdir") (v "0.1.0") (d (list (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "05km0aj4qg8f7wxpj7y7xhva6fnqksaa87hxm1w16hwq83db9i63")))

(define-public crate-outdir-tempdir-0.2.0 (c (n "outdir-tempdir") (v "0.2.0") (d (list (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0bk10xwnfiv8s2xy5xxwavw3rv55b8dwn5xfrck6x3gp0ymnqchz")))

