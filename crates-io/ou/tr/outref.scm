(define-module (crates-io ou tr outref) #:use-module (crates-io))

(define-public crate-outref-0.1.0 (c (n "outref") (v "0.1.0") (h "1x61h7dl1cc6cj2f3zsalr8d98v0cw6497sykwxf74wjmqljh8kz") (r "1.62.1")))

(define-public crate-outref-0.2.0 (c (n "outref") (v "0.2.0") (h "1kjbkpxqwrpxa93s9bizpjxwnm6qfq7c1jmcks2ksqbw9xxyjvdi") (r "1.63.0")))

(define-public crate-outref-0.3.0 (c (n "outref") (v "0.3.0") (h "14valzfjsvfmqzymgq73v819qfmi9hsxcqy29gvwnxyw6m60zbb1") (r "1.63.0")))

(define-public crate-outref-0.4.0 (c (n "outref") (v "0.4.0") (h "0v10p599fdk283lj4hhmsak8lix0y61kp8iqi7rxzpdkjmmn4j6m") (r "1.63.0")))

(define-public crate-outref-0.5.0 (c (n "outref") (v "0.5.0") (h "0h7ziagd312xqn8fn25dbbd5vhjrga01pi06h3f8cqdm1rv322s6") (r "1.63.0")))

(define-public crate-outref-0.5.1 (c (n "outref") (v "0.5.1") (h "0ynw7nb89603gkfi83f9chsf76ds3b710gxfn12yyawrzl7pcc20") (r "1.63.0")))

