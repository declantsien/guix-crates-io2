(define-module (crates-io ou ra oura-api) #:use-module (crates-io))

(define-public crate-oura-api-0.1.0 (c (n "oura-api") (v "0.1.0") (d (list (d (n "mockito") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.0") (d #t) (k 0)))) (h "0xgla37z9ns0zrkyiininjl3wsagz4isxn19l97s5wp6fsshs6my")))

(define-public crate-oura-api-0.1.1 (c (n "oura-api") (v "0.1.1") (d (list (d (n "mockito") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.0") (d #t) (k 0)))) (h "11p7sx84837r5vj2yl3mvbnp3jlp1pvymlp3b3pqh4jy05xshbcx")))

(define-public crate-oura-api-0.1.2 (c (n "oura-api") (v "0.1.2") (d (list (d (n "mockito") (r "^1.1.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "typed-builder") (r "^0.16.0") (d #t) (k 0)))) (h "1bw9k4ri1ylapi9m6ljcfqyxilck6ypna5k8sp3lvwp8smji8yxw")))

