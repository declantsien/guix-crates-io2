(define-module (crates-io ou ra ourairports) #:use-module (crates-io))

(define-public crate-ourairports-0.1.0 (c (n "ourairports") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yg3ww9ldfaslg0phbhm5iaajw96xlng2s3ndwzpayidlzg3b1ns")))

