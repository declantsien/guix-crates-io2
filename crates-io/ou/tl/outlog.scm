(define-module (crates-io ou tl outlog) #:use-module (crates-io))

(define-public crate-outlog-0.1.0 (c (n "outlog") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14bvlc679fhnqk70liy83vd3h5n4avczcqgmaq4mg57zda04mhsx") (f (quote (("panic") ("default" "color") ("config-serde" "serde" "log/serde") ("color" "atty") ("all" "chrono" "color" "panic" "config-serde"))))))

