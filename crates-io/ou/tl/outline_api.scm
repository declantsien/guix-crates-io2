(define-module (crates-io ou tl outline_api) #:use-module (crates-io))

(define-public crate-outline_api-1.0.0 (c (n "outline_api") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "toml") (r "^0.7.7") (d #t) (k 0)))) (h "0k50vf3l3yxxpfk0ckc9hczli1lm1rffw2gzmfnlnyl7nwfjnizp")))

(define-public crate-outline_api-1.0.1 (c (n "outline_api") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "toml") (r "^0.7.7") (d #t) (k 0)))) (h "1s3npslr6yslng67wa59cy203j7ycyqlba00v01867yba60q73vm")))

(define-public crate-outline_api-1.0.2 (c (n "outline_api") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0hdy6na6n4alvf0pdlynzasd1hg545s6f2i3k757c5g78yi7zzk7")))

(define-public crate-outline_api-1.0.3 (c (n "outline_api") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0hjmfcx52c4a2zc7aciqxlk3kjnhqqxv5iz7ys31ml1hglnlvh00")))

(define-public crate-outline_api-2.0.0 (c (n "outline_api") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "08awk88f43k89gjn69a7ryzv1q9zfinad9vhz0hp27sk34iz88as") (y #t)))

(define-public crate-outline_api-2.0.1 (c (n "outline_api") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1ddn4ah1z3ml75rqilrvwhkcayn6zs95khf44ajwyrq689ypskvd")))

(define-public crate-outline_api-2.0.2 (c (n "outline_api") (v "2.0.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1pryvaz4v03vi7j9jz375wasrck9nrvvl6968waar95p9pk50j9q")))

(define-public crate-outline_api-2.0.3 (c (n "outline_api") (v "2.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0akvb56gyj909m46jc43hwvjzb6s33nl0j8rwcv1lglmx6xmhphq")))

(define-public crate-outline_api-2.0.4 (c (n "outline_api") (v "2.0.4") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1y4k5jib7kw9xsalpvpg8z1r3q6011g5faqx9b5vgv47yphqx6ba")))

(define-public crate-outline_api-2.1.0 (c (n "outline_api") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "06bmy5sm0y8pl51p8iw6g98l9z0jvgwq47j7mja4nhd0xk2pkrcx")))

