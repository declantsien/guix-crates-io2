(define-module (crates-io ou tl outlook-auto) #:use-module (crates-io))

(define-public crate-outlook-auto-0.1.0 (c (n "outlook-auto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0kphi2dr555avxhn1xsp9lliy8j21qvw0sidf11lkssrkshij2k9")))

(define-public crate-outlook-auto-0.1.1 (c (n "outlook-auto") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0dbv7vpk93593h7k6syn4xyd8vh2zpgmfk7b2171lp4y0yna0z0y")))

