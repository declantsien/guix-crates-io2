(define-module (crates-io ou tl outline_vpn_api) #:use-module (crates-io))

(define-public crate-outline_vpn_api-1.0.0 (c (n "outline_vpn_api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "08y1k73vj8kxvpl68cyzva8nnpf2z6inx8yyhl1k9lxnzaicrrz2")))

(define-public crate-outline_vpn_api-1.0.1 (c (n "outline_vpn_api") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "19xdsjj7vqmrwkay5d35fg7y3f2n061k9mzydgaq33kx84x83d75")))

(define-public crate-outline_vpn_api-1.0.2 (c (n "outline_vpn_api") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1zw2hj5v0bg3s7b8d1kvm6air5bsalhgdfnqb5ggm1xkh90qcd1z")))

