(define-module (crates-io ou tl outliving-deref) #:use-module (crates-io))

(define-public crate-outliving-deref-0.0.0 (c (n "outliving-deref") (v "0.0.0") (h "18gfjb457bhnrbrp51zd7b5wd0dy7kp65lk5spfk59rhr5dkz545") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-outliving-deref-0.0.1 (c (n "outliving-deref") (v "0.0.1") (h "00p63lljlwqg3sinya2xn4qh476y7kpdb6bxl6klp0yl2pn3zrv4") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-outliving-deref-0.0.2 (c (n "outliving-deref") (v "0.0.2") (h "1l2wazvvmkgxhy2sij5vamlbnzwcc7qs9lnhd10xvf2vf6zqdka3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-outliving-deref-0.0.3 (c (n "outliving-deref") (v "0.0.3") (h "00xgnyihg5kccbhh6d2ndqgc95y780agdl092bkggwlw0w48s2bv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-outliving-deref-0.0.4 (c (n "outliving-deref") (v "0.0.4") (h "0bk0kgwp13rdhm1k97g9rdnw4hpg83dlijm28brm14lb4hvsvhr1") (y #t)))

(define-public crate-outliving-deref-0.0.5 (c (n "outliving-deref") (v "0.0.5") (h "0q6nldlbd08a95ndw19ay272fz8bhl9yqj2xq9cwdgcpxc1m7g8w") (y #t)))

(define-public crate-outliving-deref-0.0.6 (c (n "outliving-deref") (v "0.0.6") (h "1nisqgkh9fc0l2vk6qag0rif6vh7v0danpc2m1zqq5bq5xjpjkw0") (y #t)))

