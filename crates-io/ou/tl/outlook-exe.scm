(define-module (crates-io ou tl outlook-exe) #:use-module (crates-io))

(define-public crate-outlook-exe-0.0.0 (c (n "outlook-exe") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (k 0)))) (h "19s0v6cn8ij4zanc8q4zk5kjhdqclr47knk164i5b2w5sk2ccfxl")))

(define-public crate-outlook-exe-0.1.0 (c (n "outlook-exe") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (k 0)))) (h "0mhdva80wlihkpdxva7bca43zvy6n045wbsvg01im6lrx0vc0c64")))

