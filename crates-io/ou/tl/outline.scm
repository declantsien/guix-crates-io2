(define-module (crates-io ou tl outline) #:use-module (crates-io))

(define-public crate-outline-0.1.0 (c (n "outline") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mdllm0nbgkcwdqmbbim56znxvwfssa48hs3ngglxp3pwdbzfq4x") (f (quote (("bin" "clap" "toml"))))))

(define-public crate-outline-0.1.1 (c (n "outline") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1q8iycixj7k246czj8jzysw3saslb7g79k4325rpy09bhmb976gj") (f (quote (("bin" "clap" "toml"))))))

(define-public crate-outline-0.1.2 (c (n "outline") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0w85i395y6aps8dlv2f69q29hiqwgqdjb0rijxm9iwdah3zw04i0") (f (quote (("bin" "clap" "toml"))))))

(define-public crate-outline-0.2.0 (c (n "outline") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1mwbs5pq1x21wc1pgqkam7y4vbzb2g9h9c4mjmiblzdh52qvlz5d") (f (quote (("bin" "clap" "toml" "either"))))))

(define-public crate-outline-0.2.1 (c (n "outline") (v "0.2.1") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "16dwm15ygqgd82s93z5zwrgvsrn9nzrcp96y78d3fv2jm99yma64") (f (quote (("bin" "clap" "toml" "either"))))))

(define-public crate-outline-0.3.0 (c (n "outline") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "049ig5lgrx3iy7cz67dwpqcx6nsmyzn0ys9k71808zn5apbb65fi") (s 2) (e (quote (("bin" "dep:clap" "dep:toml" "dep:either"))))))

