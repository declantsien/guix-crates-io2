(define-module (crates-io ou tl outlook-mapi-stub) #:use-module (crates-io))

(define-public crate-outlook-mapi-stub-0.1.0 (c (n "outlook-mapi-stub") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvl72q4gcwklrh4wvjk0kyjkqxm1phz6vzql9rxbqnfifhsysl1")))

(define-public crate-outlook-mapi-stub-0.2.0 (c (n "outlook-mapi-stub") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "00gdkblyzq3i0i2m786zr9fbdhihvib3fz0g45rr7dsnjd74iz02")))

(define-public crate-outlook-mapi-stub-0.2.1 (c (n "outlook-mapi-stub") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1644sszj8hf8805nyxa2zvimc6dq6lx3l4azwpb1h7p9h5z5m6ix")))

