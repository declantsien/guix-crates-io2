(define-module (crates-io ou tl outliers) #:use-module (crates-io))

(define-public crate-outliers-0.1.0 (c (n "outliers") (v "0.1.0") (h "046mb4snk9ka8w5y3h6hj5aba1adqik5fvgfadpg81pc2isb94a4")))

(define-public crate-outliers-0.2.0 (c (n "outliers") (v "0.2.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "075z7j85zdab5nrixv0yxhi9vfysdhkz0zb8jylhal6ifjcvyakl")))

(define-public crate-outliers-0.2.1 (c (n "outliers") (v "0.2.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "081z5njqsw3yf4mmgx121qcxhvyy15sbac7977a1k3lfv4ql639b")))

(define-public crate-outliers-0.3.0 (c (n "outliers") (v "0.3.0") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0m482yb4h4vm3hmg12sv1gncpakc93rgpkg8jiy9gkq5i00jl083")))

(define-public crate-outliers-0.4.0 (c (n "outliers") (v "0.4.0") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "1nyvg5mkxxbssvjns6fx0msmnxl4x7a2jksnjdx4q8x410w9bkrh")))

(define-public crate-outliers-0.4.1 (c (n "outliers") (v "0.4.1") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0z7yjp3ilhjm5lmmfflz5gcamhg3n21f8sgwinzkd04jymywkhwa")))

(define-public crate-outliers-0.5.0 (c (n "outliers") (v "0.5.0") (d (list (d (n "statrs") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0xjalq2lx7yxbd4vn5ffl63sgj6a2db0bcc911y3lbm40wfdfs9z")))

