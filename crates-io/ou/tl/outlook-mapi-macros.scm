(define-module (crates-io ou tl outlook-mapi-macros) #:use-module (crates-io))

(define-public crate-outlook-mapi-macros-0.1.0 (c (n "outlook-mapi-macros") (v "0.1.0") (h "0ah0dhc7qnq8kcdd2ncaz0rpxqfp50qrf05cxjd62d26dvmpavri")))

(define-public crate-outlook-mapi-macros-0.2.0 (c (n "outlook-mapi-macros") (v "0.2.0") (h "1spv2ck7dvvz6l9mqs5h1ivxhdr0m8m84dj7fn0zp00m747v8k3s")))

(define-public crate-outlook-mapi-macros-0.2.1 (c (n "outlook-mapi-macros") (v "0.2.1") (h "09m3b0ws51b0ffffkv04l0gd9m230clg8ycr5zazpsw2akg3zcvh")))

