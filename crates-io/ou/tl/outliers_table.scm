(define-module (crates-io ou tl outliers_table) #:use-module (crates-io))

(define-public crate-outliers_table-0.1.0 (c (n "outliers_table") (v "0.1.0") (h "1r1w1ralwx18ykypi1zbxb2q2b1mmq3kz5pjwci5kqv97i9yjn19") (y #t)))

(define-public crate-outliers_table-0.1.1 (c (n "outliers_table") (v "0.1.1") (h "0r87avdilw7nxm6d7a60pxygly3aag5nza83ccr5x6rm5z56l1mw") (y #t)))

(define-public crate-outliers_table-0.1.2 (c (n "outliers_table") (v "0.1.2") (h "1p0klbidw23mffxzy222h9jkf4i9v49p5avyj9y7jhw48cnsqsgg") (y #t)))

(define-public crate-outliers_table-0.1.3 (c (n "outliers_table") (v "0.1.3") (h "0av7a55akg8i0s3i1h9z5ky9bfnr0qsy64zrb3mv4f5cz3z47d42")))

(define-public crate-outliers_table-0.1.4 (c (n "outliers_table") (v "0.1.4") (h "0p8bdaml6z97787gwwfn5mib7mkxsldzr02hzkzssfqrhr1s5hj8") (y #t)))

(define-public crate-outliers_table-0.1.5 (c (n "outliers_table") (v "0.1.5") (h "1fi7z6z59ga2h2jwagvsvbz81nbzz4vp5hh896d26amk2b9ii360")))

