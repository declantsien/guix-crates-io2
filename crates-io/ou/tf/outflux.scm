(define-module (crates-io ou tf outflux) #:use-module (crates-io))

(define-public crate-outflux-0.1.0 (c (n "outflux") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11xv9v946kdyhd9y3fj8180fmsph5r04gzc9srjm17wbp73b4kg5")))

