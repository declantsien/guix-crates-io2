(define-module (crates-io ou tb outbox-relay) #:use-module (crates-io))

(define-public crate-outbox-relay-0.1.0 (c (n "outbox-relay") (v "0.1.0") (d (list (d (n "bb8-postgres") (r "^0.7") (f (quote ("with-serde_json-1"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rdkafka") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "process" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1l3n11nzn1ayvwiwghd2qmi681gn194mnhacnm31nlmmrq82njij")))

