(define-module (crates-io ou ch ouch_connect_core) #:use-module (crates-io))

(define-public crate-ouch_connect_core-5.0.1 (c (n "ouch_connect_core") (v "5.0.1") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "links_nonblocking") (r "^0.2") (d #t) (k 0)) (d (n "ouch_model") (r "~5.0.1") (o #t) (d #t) (k 0)) (d (n "soupbintcp_connect_nonblocking") (r "~4.0") (d #t) (k 0)))) (h "1131khbm9xqwivkbk4pqhplkl34y2gha2igdxrimm0w090z50557") (f (quote (("full" "unittest")))) (s 2) (e (quote (("unittest" "dep:ouch_model" "ouch_model/unittest") ("default" "dep:ouch_model")))) (r "1.69")))

