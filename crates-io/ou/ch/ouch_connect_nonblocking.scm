(define-module (crates-io ou ch ouch_connect_nonblocking) #:use-module (crates-io))

(define-public crate-ouch_connect_nonblocking-5.0.1 (c (n "ouch_connect_nonblocking") (v "5.0.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "links_nonblocking") (r "^0.2") (d #t) (k 0)) (d (n "links_nonblocking") (r "^0.2") (f (quote ("unittest"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "ouch_connect_core") (r "~5.0.1") (o #t) (d #t) (k 0)))) (h "0w15jjyjiy21jjc62x5pvb7bm9k22b4qw2xs5i3k11ly2zxc86rn") (f (quote (("full" "unittest")))) (s 2) (e (quote (("unittest" "dep:ouch_connect_core" "ouch_connect_core/unittest") ("default" "dep:ouch_connect_core")))) (r "1.69")))

