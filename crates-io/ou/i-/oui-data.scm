(define-module (crates-io ou i- oui-data) #:use-module (crates-io))

(define-public crate-oui-data-0.1.0 (c (n "oui-data") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0rs7pr82ka9c6vka73vrssrhp0f2ny34598z32dpcaf2g628n33s")))

(define-public crate-oui-data-0.1.1 (c (n "oui-data") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1vvjrm4imzxk9cs7x36hzpx1y2zgram0n360d3bqd4fwl0vmgglj")))

(define-public crate-oui-data-0.1.2 (c (n "oui-data") (v "0.1.2") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0abjqbdw7056j4q2zz1z14il95w72dfpmparhrfv6m86hiyl52y7")))

(define-public crate-oui-data-0.1.3 (c (n "oui-data") (v "0.1.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0jk7sdqa4hzwrr1v5qk1999bpax2hnqlr2h061r943nqicxdw353")))

(define-public crate-oui-data-0.2.0 (c (n "oui-data") (v "0.2.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1swlmx14v5d2dx2q1jv72yhb9mcrnbs33q3h6fp1vdxk879450fs")))

