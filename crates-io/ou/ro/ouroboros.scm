(define-module (crates-io ou ro ouroboros) #:use-module (crates-io))

(define-public crate-ouroboros-0.1.0 (c (n "ouroboros") (v "0.1.0") (d (list (d (n "ouroboros_macro") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "006646ymzj032f9b8z23hw73311a7asy2h6w9632car41hjw6dzc") (y #t)))

(define-public crate-ouroboros-0.1.1 (c (n "ouroboros") (v "0.1.1") (d (list (d (n "ouroboros_macro") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "142xk07y4wzl7gq4b0q8hjnzkm945z9i1914imgzpwk107vfg6ss") (y #t)))

(define-public crate-ouroboros-0.1.2 (c (n "ouroboros") (v "0.1.2") (d (list (d (n "ouroboros_macro") (r "^0.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1rris28hxydjyc0lsgccqlqcm725p5q7wljkcknrd34vnc1ib1bf") (y #t)))

(define-public crate-ouroboros-0.2.0 (c (n "ouroboros") (v "0.2.0") (d (list (d (n "ouroboros_macro") (r "^0.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "03ky5zbq0dm0h79pgd7vf11m223iigzzwkxpbbyh2k0m5npf6907") (y #t)))

(define-public crate-ouroboros-0.2.1 (c (n "ouroboros") (v "0.2.1") (d (list (d (n "ouroboros_macro") (r "^0.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1i6s7c30zkpr9kbh1sg9z74x5vzjnj8a7maxv1zcjxz1ca4q9a7z") (y #t)))

(define-public crate-ouroboros-0.2.2 (c (n "ouroboros") (v "0.2.2") (d (list (d (n "ouroboros_macro") (r "^0.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1nv7dq06vbc0v8r67crkysvjjp55ynmqslv64d8x9f2s9agl6s28") (y #t)))

(define-public crate-ouroboros-0.2.3 (c (n "ouroboros") (v "0.2.3") (d (list (d (n "ouroboros_macro") (r "^0.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "08sdapp1qx2ppwrqsnjb7sv10pnymhq884xz8bgal7mg4z1r8nhp") (y #t)))

(define-public crate-ouroboros-0.3.0 (c (n "ouroboros") (v "0.3.0") (d (list (d (n "ouroboros_macro") (r "^0.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1zaw1iygb54lidhnp4g0all72qj7w1dhg7ahvc4vl9qmzshns44j") (y #t)))

(define-public crate-ouroboros-0.4.0 (c (n "ouroboros") (v "0.4.0") (d (list (d (n "ouroboros_macro") (r "^0.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1aip3ibsbwyhrsqb362z97ppygg6rfhxwphsd45i7msngjcnjr3p") (y #t)))

(define-public crate-ouroboros-0.4.1 (c (n "ouroboros") (v "0.4.1") (d (list (d (n "ouroboros_macro") (r "^0.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0nr34k6bw2i67f0g0d23d3s47wifix6iqhini1zlg3w8z9l5khql") (y #t)))

(define-public crate-ouroboros-0.5.0 (c (n "ouroboros") (v "0.5.0") (d (list (d (n "ouroboros_macro") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "094kza5xyzn4zk873z93w4gd4zg6cda31aanylybz3ghhc6qxcf1") (y #t)))

(define-public crate-ouroboros-0.5.1 (c (n "ouroboros") (v "0.5.1") (d (list (d (n "ouroboros_macro") (r "^0.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "18n4ai4b3n0m3pz98w3blb95xa1n33m9hrk4pk3klv826lb5a16c")))

(define-public crate-ouroboros-0.6.0 (c (n "ouroboros") (v "0.6.0") (d (list (d (n "ouroboros_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0f8l1djwjcakkm8zhnhgfq26yib4qg77yysqlgaqcwam4w79xx8n")))

(define-public crate-ouroboros-0.7.0 (c (n "ouroboros") (v "0.7.0") (d (list (d (n "ouroboros_macro") (r "^0.7.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0a9smaa8sy60lcx88vkm5rgjjzmbizfa85brpibs6iga034nddmh")))

(define-public crate-ouroboros-0.8.0 (c (n "ouroboros") (v "0.8.0") (d (list (d (n "ouroboros_macro") (r "^0.8.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "06p4gcgxj6n4dmzpg6b1sc9cd1cvf7cyx1x2ss5dravw28zb77q6")))

(define-public crate-ouroboros-0.8.1 (c (n "ouroboros") (v "0.8.1") (d (list (d (n "ouroboros_macro") (r "^0.8.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "19c5fm2wx14mbqp84yk32lmar4ynyd4ivaxn0jckvb3zlk3qm508")))

(define-public crate-ouroboros-0.8.2 (c (n "ouroboros") (v "0.8.2") (d (list (d (n "ouroboros_macro") (r "^0.8.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "141gakw0s043bdw4i17l9cjzri2a8h99c557y0w6adlsdkm1qyr1")))

(define-public crate-ouroboros-0.8.3 (c (n "ouroboros") (v "0.8.3") (d (list (d (n "ouroboros_macro") (r "^0.8.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1nj5spsxbcq5zl6dgh0hhk7v4gpzpcy21j5yszcqdmz87wh5qvaz")))

(define-public crate-ouroboros-0.9.0 (c (n "ouroboros") (v "0.9.0") (d (list (d (n "ouroboros_macro") (r "^0.9.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0dcj5lgydcry033pjy1mcd5mgvhs90c25lr8mrgaji2m920n1y5l")))

(define-public crate-ouroboros-0.9.1 (c (n "ouroboros") (v "0.9.1") (d (list (d (n "ouroboros_macro") (r "^0.9.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0zg6g1kjx2zjq5kwi2dalk5am9s5v7pamibknqvjspa44a0fflzw")))

(define-public crate-ouroboros-0.9.2 (c (n "ouroboros") (v "0.9.2") (d (list (d (n "ouroboros_macro") (r "^0.9.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0c15dk67v70zyihpvfqc6z35fgs4qwyzsdn29is8n6n3qgzll8y8")))

(define-public crate-ouroboros-0.9.3 (c (n "ouroboros") (v "0.9.3") (d (list (d (n "ouroboros_macro") (r "^0.9.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "05fd67rj4g67iyv94bgvv4kw8mk5x3vj0360nvp4xb411cq547yc")))

(define-public crate-ouroboros-0.9.4 (c (n "ouroboros") (v "0.9.4") (d (list (d (n "ouroboros_macro") (r "^0.9.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "107zx3ibfksvmn26xip022pcfxr86adqc2624n1bhvlf973fh8x3")))

(define-public crate-ouroboros-0.9.5 (c (n "ouroboros") (v "0.9.5") (d (list (d (n "ouroboros_macro") (r "^0.9.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1jk255bliyvaxazxv306qiwfjmml8n0lbsfkxa07lh1p7q7gdvzv")))

(define-public crate-ouroboros-0.10.0 (c (n "ouroboros") (v "0.10.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.10.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1cqqv1ankd9p64h4mn8sff2pnklq0mwxyjipx61i5wj2hxy85qzh")))

(define-public crate-ouroboros-0.10.1 (c (n "ouroboros") (v "0.10.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.10.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1phbp9wjp36bvkwlyvr2zznaack6xcvg0z1869r3i33iy5j6s8w4")))

(define-public crate-ouroboros-0.11.0 (c (n "ouroboros") (v "0.11.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.11.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0xn3krpp91h1falz8mnlij7nr0x51104qp4v3jj0c8dax7l31zdc")))

(define-public crate-ouroboros-0.11.1 (c (n "ouroboros") (v "0.11.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.11.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0gw7rqffhdf26v4kkclbz88amjy3770427m3hcps9xlpqn7sc61m")))

(define-public crate-ouroboros-0.12.0 (c (n "ouroboros") (v "0.12.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1hwcjv7h65xkjgfnpc3kispknrzdb2hkm53xbr9k85c89rdz64f7")))

(define-public crate-ouroboros-0.13.0 (c (n "ouroboros") (v "0.13.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.13.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1hy9f275d94vgcjkqfrrbfzx08iwpln5938bxpxndnxls61fymzk")))

(define-public crate-ouroboros-0.14.0 (c (n "ouroboros") (v "0.14.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.14.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1ih1lna1d2is3a6aicmc7lpfhqmg046kssxpryk24kwg8j42ymh6")))

(define-public crate-ouroboros-0.14.1 (c (n "ouroboros") (v "0.14.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.14.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1dibr2xkyxih61r30658l582d3y52ilaiyh7cb74vmmbn3av3292")))

(define-public crate-ouroboros-0.14.2 (c (n "ouroboros") (v "0.14.2") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.14.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0qk63yd5iim0wavgsy7w98bfsynmw40nv1wq4nn1hvhj1llkyr3i")))

(define-public crate-ouroboros-0.15.0 (c (n "ouroboros") (v "0.15.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1gx29j6rik9hilwcy41nys9mh5ayvkf05dw2p861anv8g2va6ccz")))

(define-public crate-ouroboros-0.15.1 (c (n "ouroboros") (v "0.15.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.1") (d #t) (k 0)))) (h "182zy60izv1w37ndzn6gngakf7scs3h2s6bw5swfc026iq4fa800")))

(define-public crate-ouroboros-0.15.2 (c (n "ouroboros") (v "0.15.2") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.2") (d #t) (k 0)))) (h "1qhd9cvc4hwdbr37da1jh0k0742slchixlxn4wxgc7g3l63yl9bl") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.15.3 (c (n "ouroboros") (v "0.15.3") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.3") (d #t) (k 0)))) (h "0xw8pzl349vq20ahx7gmv66rnabzld82qfsyvc5s62aci8ahs6am") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.15.4 (c (n "ouroboros") (v "0.15.4") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.4") (d #t) (k 0)))) (h "08kq49aql3w72ld2x6j1wc67j8j4jykqagpnma3qdj2zmaqa4mkz") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.15.5 (c (n "ouroboros") (v "0.15.5") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.5") (d #t) (k 0)))) (h "1jk2n2n2fanakxhljvwaqff8qy5br7awcwf9dbdj15hmasrm1fyz") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.15.6 (c (n "ouroboros") (v "0.15.6") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.15.6") (d #t) (k 0)))) (h "1nvjra9dana2g6kxv3397qrgpyw6lknzya6lzs1s1llbap8qndg1") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.16.0 (c (n "ouroboros") (v "0.16.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0jp8vqq418yi1w9x3apfw1mp12cgw06kw44s23c2ifljka8x19p6") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.17.0 (c (n "ouroboros") (v "0.17.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.17.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0la3jpp218nix8xfrmdjip0yz1gckb0amzrhpnaaxvw23arvf4yq") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.17.1 (c (n "ouroboros") (v "0.17.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.17.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0bnm9lkcsr63wqqaq1z4qf5ksxs379d81fk7jg0xv2qhvpgisdml") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.17.2 (c (n "ouroboros") (v "0.17.2") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.17.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0m69j8288k3b3iyblngdfgraahnk9d5maw5a5y4fmprr1lr0gfp2") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.18.0 (c (n "ouroboros") (v "0.18.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.18.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1pnwcnqc1r7l34s1cfklllhly5ak3cljg6r28pn0m5sval3dx1hw") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.18.1 (c (n "ouroboros") (v "0.18.1") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.18.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0jwgbbzbp3wq9agkiphbkk0jv2w2ykm0lviwp53ivy7y3j4y7cxa") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.18.2 (c (n "ouroboros") (v "0.18.2") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.18.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "020j0vrgkdmr12ngfl0qmbnxrf9nc7xrjda8m0rjffw8zmzn62x5") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.18.3 (c (n "ouroboros") (v "0.18.3") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.18.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0g2pn23qpmm4w9my80j9hc01szrgpyrg4gxyyi9bfqili9dbxdwp") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

(define-public crate-ouroboros-0.18.4 (c (n "ouroboros") (v "0.18.4") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "ouroboros_macro") (r "^0.18.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0rsazk2hh2w626585scb7ylaf500y5insp3rnkbdwnm2jq4s4kwl") (f (quote (("std" "ouroboros_macro/std") ("default" "std"))))))

