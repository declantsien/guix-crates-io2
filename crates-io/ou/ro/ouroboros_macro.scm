(define-module (crates-io ou ro ouroboros_macro) #:use-module (crates-io))

(define-public crate-ouroboros_macro-0.1.0 (c (n "ouroboros_macro") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hyj27ajq5hhfxw4jz3azfixqgnwd3h4dq12rf15nby2fa1g87ih") (y #t)))

(define-public crate-ouroboros_macro-0.1.1 (c (n "ouroboros_macro") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06ks1clnr6lpsc3sp1sm34ziqf48c2zlpdbg50q8qdx0na5gfaq1") (y #t)))

(define-public crate-ouroboros_macro-0.1.2 (c (n "ouroboros_macro") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x62hhyvn5ksci90kxlrl3rbynipdib07k4crszy5l1vw5j3xgcj") (y #t)))

(define-public crate-ouroboros_macro-0.1.3 (c (n "ouroboros_macro") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kz8f6xl86b5fbzfzd35bar986rcilx5qifwgk47dfpzwh68s4z8") (y #t)))

(define-public crate-ouroboros_macro-0.2.0 (c (n "ouroboros_macro") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18cy69w1f71nr7ifhsrn7kik3j752znb8wsv7vp78py39jflqdqy") (y #t)))

(define-public crate-ouroboros_macro-0.2.1 (c (n "ouroboros_macro") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19slp4bgd7xlfifmkk1xivsrwx6g9jbiyd13xymsl239namd8w7v") (y #t)))

(define-public crate-ouroboros_macro-0.2.2 (c (n "ouroboros_macro") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fqp7v6krxw0c3yslrn8076w46xz97922cys2za6dl9n8b2yzhx5") (y #t)))

(define-public crate-ouroboros_macro-0.2.3 (c (n "ouroboros_macro") (v "0.2.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dnkakbmhvsy4i64nmgsvr6fjryzm35mh26035dbkzb7i2xh6kjk") (y #t)))

(define-public crate-ouroboros_macro-0.3.0 (c (n "ouroboros_macro") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07pwwlmxdfxjmzxryh4ykxk2bj2jklp70kryb1d18inrmiv9a9cl") (y #t)))

(define-public crate-ouroboros_macro-0.4.0 (c (n "ouroboros_macro") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rvcn0qyz0gbnxp34p4w49rda8h5cj9ascvfp3fqckmlbfj5n5l5") (y #t)))

(define-public crate-ouroboros_macro-0.4.1 (c (n "ouroboros_macro") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pl084m1j39dcdvg08ml2c0w9dk434g1rmbgkx4kfc1lhbi67jvp") (y #t)))

(define-public crate-ouroboros_macro-0.5.0 (c (n "ouroboros_macro") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17lq4mlr7rqa5x0rygqqvk23sc03qfkm1wpbwwid45z4lmz888zx") (y #t)))

(define-public crate-ouroboros_macro-0.5.1 (c (n "ouroboros_macro") (v "0.5.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bnq7swnjzwjijdnanggpxf3mavn1v9fjp2sx783rj5yxby3vhyf")))

(define-public crate-ouroboros_macro-0.6.0 (c (n "ouroboros_macro") (v "0.6.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "142rvljimsqv7fjs5l2zn7nzwznizqn67rc0nsbxb7j8z3idm2gg")))

(define-public crate-ouroboros_macro-0.7.0 (c (n "ouroboros_macro") (v "0.7.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "069ny8inl19kzhfp2wy45x39iwpm8nfkcx1wj8aw9bf00pr1iinz")))

(define-public crate-ouroboros_macro-0.8.0 (c (n "ouroboros_macro") (v "0.8.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ilg15kjkp1laral6hmwli55yy3f677d6hqyx7fdd6gj434qr4xd")))

(define-public crate-ouroboros_macro-0.8.1 (c (n "ouroboros_macro") (v "0.8.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1il1j1cj2w1afsnzg8cjic7jqkc8mkd0fpavg1lh6gnaylj5lpps")))

(define-public crate-ouroboros_macro-0.8.2 (c (n "ouroboros_macro") (v "0.8.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxwpvsim03nk16kfg7bd6dagyzfbrb34lxx6bdlw4cnjmxpap21")))

(define-public crate-ouroboros_macro-0.8.3 (c (n "ouroboros_macro") (v "0.8.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfzzwykwabq5jcj115f21zzwwy22gqhjnnaf1z8r876c2ll768j")))

(define-public crate-ouroboros_macro-0.9.0 (c (n "ouroboros_macro") (v "0.9.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10zhxw970sxzpnphpxqq7jz1y6g3kq7jkr9k4544dycdjky96r0q")))

(define-public crate-ouroboros_macro-0.9.1 (c (n "ouroboros_macro") (v "0.9.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dk9qx0ww80yivxx9ys1g8rlhkvmikca3hgm20638j3hx4lsz4zf")))

(define-public crate-ouroboros_macro-0.9.2 (c (n "ouroboros_macro") (v "0.9.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnqwhapp2mm54v6j36db3j5zqndv7x0brz0w9jsidn0v0n36crn")))

(define-public crate-ouroboros_macro-0.9.3 (c (n "ouroboros_macro") (v "0.9.3") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mdhd9bg88gx9cv2qiav8r2z4jyf1j8cfcxlf9xds73kz3405ns1")))

(define-public crate-ouroboros_macro-0.9.4 (c (n "ouroboros_macro") (v "0.9.4") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06b2xx03mnqcgjl0ilcabhrrfwjfzgy01ry95dfsz034ky2ln5cx")))

(define-public crate-ouroboros_macro-0.9.5 (c (n "ouroboros_macro") (v "0.9.5") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qj9mlhi7jc63xbxv40rx53d7r05w56bb87zy59dzpsv5f0cpwh3")))

(define-public crate-ouroboros_macro-0.10.0 (c (n "ouroboros_macro") (v "0.10.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04j1w2k3sldxsa4vsdrlj2dipjszl89x3yxcx8m7xb4fw1vn1w0y")))

(define-public crate-ouroboros_macro-0.10.1 (c (n "ouroboros_macro") (v "0.10.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s6aic49lyclhas6bh1f84qfy31m333mcvnmn4v02v5rdrx8aqzl")))

(define-public crate-ouroboros_macro-0.11.0 (c (n "ouroboros_macro") (v "0.11.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0584lff81fsrg3y9jznadwdk5a3kr491zrqp1j60miklshzraxp8")))

(define-public crate-ouroboros_macro-0.11.1 (c (n "ouroboros_macro") (v "0.11.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "052790wg91khm55z1azchlyc1r8awjx2p6l472l42bfb3cxq28sf")))

(define-public crate-ouroboros_macro-0.12.0 (c (n "ouroboros_macro") (v "0.12.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17a72g4fqrq3wyidksf3x8hh51ym1p19g2xv37m2f67r6pcyvgdd")))

(define-public crate-ouroboros_macro-0.13.0 (c (n "ouroboros_macro") (v "0.13.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqjkckg0kzgm58graagchmbvs5gfhra9hgylpzpvvxs5hnbb824")))

(define-public crate-ouroboros_macro-0.14.0 (c (n "ouroboros_macro") (v "0.14.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11cav04x82liknlrv50lpl1i1ln2jw4isdqzdjnjsg0pcpvwik9r")))

(define-public crate-ouroboros_macro-0.14.1 (c (n "ouroboros_macro") (v "0.14.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s01dijrfjwwdjz9dcmvl6dlfyg51pl6abcvyw024kwrl0yh2haj")))

(define-public crate-ouroboros_macro-0.14.2 (c (n "ouroboros_macro") (v "0.14.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10jdr9avpi3pg2bvzyv1qbspm20hwl8wcys9x17l4v810rr296pd")))

(define-public crate-ouroboros_macro-0.15.0 (c (n "ouroboros_macro") (v "0.15.0") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "024ljm1cbs1hwc0r83ydgs4l7fky9cggspybvhp7gcyqbmfxckq8")))

(define-public crate-ouroboros_macro-0.15.1 (c (n "ouroboros_macro") (v "0.15.1") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09878jfzjibwhisl5drdb08qmcr339fiihk46zbdlqnarpcln6fz")))

(define-public crate-ouroboros_macro-0.15.2 (c (n "ouroboros_macro") (v "0.15.2") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01lgyj5nxgr6r1l0m20pp4ilz3m14clsqg2j28hic2rrlsjafjkk") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.15.3 (c (n "ouroboros_macro") (v "0.15.3") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixy3fg0m2ayxhby9pvkgn1kzaahxn42xl0hfynmmh47pdb4av41") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.15.4 (c (n "ouroboros_macro") (v "0.15.4") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x7bylrqvxifb4d3r1cfgas2sk8h43cq3q6ywg53isyh4wg68h3c") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.15.5 (c (n "ouroboros_macro") (v "0.15.5") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k9qky6shvpf0p6n6iynhkp0jgxj8bmd26bjz28z7i4ic4d9s3aa") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.15.6 (c (n "ouroboros_macro") (v "0.15.6") (d (list (d (n "Inflector") (r "^0.11") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dsn37vds4qpkzscmwaw17dv3m5m7a7j9qby8dsac19ks3622zaz") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.16.0 (c (n "ouroboros_macro") (v "0.16.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19mc9mh0rsh5mf63mbgf5xy6vik996n0s8s9wd55a81lvh3j7g26") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.17.0 (c (n "ouroboros_macro") (v "0.17.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09j7b4b90jjmkg32cpxfvb6j01mg5v9wwq6jqcxawifxni8zcmjs") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.17.1 (c (n "ouroboros_macro") (v "0.17.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08lr4xa5vvprxmp90bgrjmizx23myr296p7lbvq10fcixl9y309b") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.17.2 (c (n "ouroboros_macro") (v "0.17.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y5nmgnng4i23g333cvp8p7j3i3438r9g85fq1wafk4vqqjn4k7c") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.18.0 (c (n "ouroboros_macro") (v "0.18.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d7iwihbing76jna1yhbk94fz43vfx1v44mpgkird5ly295hrbcc") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.18.1 (c (n "ouroboros_macro") (v "0.18.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gd5q6pwkqqpq06g9ndqrpzwlgf1zjwfv4ghaa3i01sv0d2mrpgi") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.18.2 (c (n "ouroboros_macro") (v "0.18.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzanpyh7xd4msnz8hh5bl7gp6c8045qhl1imb7rnfzihdbdccrn") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.18.3 (c (n "ouroboros_macro") (v "0.18.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cqzvszvvm7ad0m2kr4jv82v58x2f6idzl4j992jr70ibzgdqidn") (f (quote (("std"))))))

(define-public crate-ouroboros_macro-0.18.4 (c (n "ouroboros_macro") (v "0.18.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gb5njxh9clp9krjc7kfbz17g5racjlld1bsjkjx13sjs7mdxc1r") (f (quote (("std"))))))

