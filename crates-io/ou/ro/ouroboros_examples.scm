(define-module (crates-io ou ro ouroboros_examples) #:use-module (crates-io))

(define-public crate-ouroboros_examples-0.1.0 (c (n "ouroboros_examples") (v "0.1.0") (d (list (d (n "ouroboros") (r "^0.1") (d #t) (k 0)))) (h "1mmc8l097lqmva63ga050g0d4pcq91b0xc07c689m2zsciassgv1") (y #t)))

(define-public crate-ouroboros_examples-0.1.1 (c (n "ouroboros_examples") (v "0.1.1") (d (list (d (n "ouroboros") (r "^0.1") (d #t) (k 0)))) (h "1jlz2050hfjibq3qvnmyvp5k0j3m8694ns5w9pccc22lzxy70ylr") (y #t)))

(define-public crate-ouroboros_examples-0.1.2 (c (n "ouroboros_examples") (v "0.1.2") (d (list (d (n "ouroboros") (r "^0.1") (d #t) (k 0)))) (h "0rvnc5gqrlhaw3ss9xz0c4r9j584bdyw076nr8v3amfi4lagj3mj") (y #t)))

(define-public crate-ouroboros_examples-0.2.0 (c (n "ouroboros_examples") (v "0.2.0") (d (list (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19vzgjci99cv64vmz7lm9xb9ba4lvgbhrym5axm4nqsypcz4v1b8") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.2.1 (c (n "ouroboros_examples") (v "0.2.1") (d (list (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05bcbgil2596qfpmkwgk1dfjwmini3mi34wha12k0l0vmadwrwm6") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.2.2 (c (n "ouroboros_examples") (v "0.2.2") (d (list (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ac12d0ajv069c5r6n3l6d1ngk3xih3xf7dcya2wdyrhfi9yr26y") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.2.3 (c (n "ouroboros_examples") (v "0.2.3") (d (list (d (n "ouroboros") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18p00wprqzfhzg563y4h21nvkh2fmzdh0b4mq6cnxcn86lhzqivc") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.3.0 (c (n "ouroboros_examples") (v "0.3.0") (d (list (d (n "ouroboros") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00jhwnqv7x1im55dlpc2pmnxnaaddxybk4xqk8z8qfyjdv20jix4") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.4.0 (c (n "ouroboros_examples") (v "0.4.0") (d (list (d (n "ouroboros") (r "^0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1i3p5qg4nsjpic77w0j8czznf0v5ms0ny9ba4ank1cpwi7w637n0") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.4.1 (c (n "ouroboros_examples") (v "0.4.1") (d (list (d (n "ouroboros") (r "^0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0q5vcyk64vdq440jb0yzl84lb82j6x8idxj7nk8d3qagngfrbi5s") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.5.0 (c (n "ouroboros_examples") (v "0.5.0") (d (list (d (n "ouroboros") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zjzd8nxm7qqrj90jiy0f0w4yzwa0giysr6s4awjp988a9fjm48l") (f (quote (("miri") ("default")))) (y #t)))

(define-public crate-ouroboros_examples-0.5.1 (c (n "ouroboros_examples") (v "0.5.1") (d (list (d (n "ouroboros") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0x551h7259mfjhgl4q4mhf5xf69y1c7ygp7mj7n91ivfqirizymv") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.6.0 (c (n "ouroboros_examples") (v "0.6.0") (d (list (d (n "ouroboros") (r "^0.6.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06m3jn1n41ppybyivwnfsgj6g1v7qmw0hg5gw6b0jwx4bhyqjbrz") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.7.0 (c (n "ouroboros_examples") (v "0.7.0") (d (list (d (n "ouroboros") (r "^0.7.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "04m0lhk9as9d96zrnqasq18lqjrf4vfnyis1sz52kks6hrgvs6ds") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.8.0 (c (n "ouroboros_examples") (v "0.8.0") (d (list (d (n "ouroboros") (r "^0.8.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ah19rgjbpa5qha0mydpjb5256ikn0n5wpqggs7lq84x2na9c0dc") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.8.1 (c (n "ouroboros_examples") (v "0.8.1") (d (list (d (n "ouroboros") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "01q9aaz65v7m724p14q81ni8539rhs3ln9g52n8clsfb926v6psb") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.8.2 (c (n "ouroboros_examples") (v "0.8.2") (d (list (d (n "ouroboros") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0nnm14ypbp9jl8fz3hf0pg9n2wmjbllwbrv9lln68sxqgxxgaikv") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.8.3 (c (n "ouroboros_examples") (v "0.8.3") (d (list (d (n "ouroboros") (r "^0.8.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "053pkbmhbzl1x6g2x7ly6qigw841rmn363j9il929vjhw6q8q9n3") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.0 (c (n "ouroboros_examples") (v "0.9.0") (d (list (d (n "ouroboros") (r "^0.9.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1720gbbagk4sw4jjqyfzk7jyyi0gxg6rj71kg6rfivxah9n5phxg") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.1 (c (n "ouroboros_examples") (v "0.9.1") (d (list (d (n "ouroboros") (r "^0.9.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j5y8vxwqrml010hrwxhp4d2fvvq3p04xi2l5r439insyzsafgw2") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.2 (c (n "ouroboros_examples") (v "0.9.2") (d (list (d (n "ouroboros") (r "^0.9.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1gyjighkzvgsbmz9p4zlmjqmqw33z959y3y9zdjkfjxh9fbb37hp") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.3 (c (n "ouroboros_examples") (v "0.9.3") (d (list (d (n "ouroboros") (r "^0.9.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j2hmm85kylzv5aiwwxgc48wh3vvpycxhqh7p39z7id3bc9wbb30") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.4 (c (n "ouroboros_examples") (v "0.9.4") (d (list (d (n "ouroboros") (r "^0.9.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bc5k79ss748frrykz1mx6cl5q86dp4w3kkvs1qh5jq6dzjr2k9b") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.9.5 (c (n "ouroboros_examples") (v "0.9.5") (d (list (d (n "ouroboros") (r "^0.9.5") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0z9986q9fnqrikb738fp5xrzgjyprvgy9rrgvpjdmjfhqrgvrv39") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.10.0 (c (n "ouroboros_examples") (v "0.10.0") (d (list (d (n "ouroboros") (r "^0.10.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yv258qjrx9mlnhrkif4sydr2xpzd7cycv7p18mdzba82cr21y67") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.10.1 (c (n "ouroboros_examples") (v "0.10.1") (d (list (d (n "ouroboros") (r "^0.10.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s9fbvxm2qz6hih6nai130xaicib7lp817sy4fm96ckjn7dyx7v8") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.11.0 (c (n "ouroboros_examples") (v "0.11.0") (d (list (d (n "ouroboros") (r "^0.11.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15cx1ynyaqd7jzgk24780586jg2xizn4nq230jrm3f5k49r16q5v") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.11.1 (c (n "ouroboros_examples") (v "0.11.1") (d (list (d (n "ouroboros") (r "^0.11.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00xpqwrfbhwkqj6ii238c5a986f3fhnhjgz3if7q7aqjyhdmf8y8") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.12.0 (c (n "ouroboros_examples") (v "0.12.0") (d (list (d (n "ouroboros") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0jpgs1jdv3p360iw58vkgqyfc6wd9v3swfnygam5nwiks47aq8yy") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.13.0 (c (n "ouroboros_examples") (v "0.13.0") (d (list (d (n "ouroboros") (r "^0.13.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y23willfyxfacxiil81mgmddzvk60g5jg58zsqkzr5332q8whsy") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.14.0 (c (n "ouroboros_examples") (v "0.14.0") (d (list (d (n "ouroboros") (r "^0.14.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0apndiajn88macjsd4c4y2l6ciliqy59r2b2n30kxw6bngbmijcd") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.14.1 (c (n "ouroboros_examples") (v "0.14.1") (d (list (d (n "ouroboros") (r "^0.14.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18m8gjvlv0snjpgkfpdzps2jq3n03fra6axv4yy23vpjmnir2313") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.14.2 (c (n "ouroboros_examples") (v "0.14.2") (d (list (d (n "ouroboros") (r "^0.14.2") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vi8y45j13bsj6h6nxklxmdba2nnf4i8c2cbcy0cl0hh9damrjml") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.15.0 (c (n "ouroboros_examples") (v "0.15.0") (d (list (d (n "ouroboros") (r "^0.15.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cs22lvk6v0jac4yfw5p6hyjil8b1sa5wqs6c5p7hhsp3h3wjpyd") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.15.1 (c (n "ouroboros_examples") (v "0.15.1") (d (list (d (n "ouroboros") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fn4yzg3qi9gfy3xzsxhv40s9pnzrmba0w8grqfwfh18kfa20lq8") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.15.2 (c (n "ouroboros_examples") (v "0.15.2") (d (list (d (n "ouroboros") (r "^0.15.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ixfmlk5vvj90cbd64ks5c22k9vh3k7wfhfli09iffvk3vci7c55") (f (quote (("miri") ("default"))))))

(define-public crate-ouroboros_examples-0.15.3 (c (n "ouroboros_examples") (v "0.15.3") (d (list (d (n "ouroboros") (r "^0.15.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xw942szs1isgflv11681j2w5b1270nn82f8jlq8fsj030abr19s") (f (quote (("miri") ("default") ("__tokio" "tokio"))))))

(define-public crate-ouroboros_examples-0.15.4 (c (n "ouroboros_examples") (v "0.15.4") (d (list (d (n "ouroboros") (r "^0.15.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1cqdn9y1yld68bb5sm79sw02s5hqv08mz68crn74z1wgf8v4n33y") (f (quote (("miri") ("default") ("__tokio" "tokio"))))))

(define-public crate-ouroboros_examples-0.15.5 (c (n "ouroboros_examples") (v "0.15.5") (d (list (d (n "ouroboros") (r "^0.15.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0fmh8v95vplgpar2g011b18r713haplc8jjymx3zial4dpqpdwal") (f (quote (("miri") ("default") ("__tokio" "tokio"))))))

(define-public crate-ouroboros_examples-0.15.6 (c (n "ouroboros_examples") (v "0.15.6") (d (list (d (n "ouroboros") (r "^0.15.6") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1z6c58r6l4s9j3jnjmdjjfackxr690sxr02vfjv46d77c2i1m78y") (f (quote (("miri") ("default") ("__tokio" "tokio"))))))

(define-public crate-ouroboros_examples-0.16.0 (c (n "ouroboros_examples") (v "0.16.0") (d (list (d (n "ouroboros") (r "^0.16.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "0dmjidajfig3r7qv418siv0z9alw59kk6pd4ah3ygmg6ksg5fgs1") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.17.0 (c (n "ouroboros_examples") (v "0.17.0") (d (list (d (n "ouroboros") (r "^0.17.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "1p1s0sxvk4ndffhq79ahlppwcaw5zqjdz96hjjj15n99w229lq1f") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.17.1 (c (n "ouroboros_examples") (v "0.17.1") (d (list (d (n "ouroboros") (r "^0.17.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "1h22vi7a7n1v68xc0ychpxgf5nb2dcfcqr1cg66c9b1vnbc7ymmx") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.17.2 (c (n "ouroboros_examples") (v "0.17.2") (d (list (d (n "ouroboros") (r "^0.17.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "1n8m1w7hmi8ypiv7g75mdjxnzyqxj7j3g30h4m7cfsj8wnfmb89i") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.18.0 (c (n "ouroboros_examples") (v "0.18.0") (d (list (d (n "ouroboros") (r "^0.18.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "1z2clbkc30260qb9y8ad2yf4359d6x7rwz8p7qif8ljmvhnqymrn") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.18.1 (c (n "ouroboros_examples") (v "0.18.1") (d (list (d (n "ouroboros") (r "^0.18.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "14hc60rj48qdfjy6w7qnm312n1dfg7p0sagajc7m7vd89a73ijc6") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.18.2 (c (n "ouroboros_examples") (v "0.18.2") (d (list (d (n "ouroboros") (r "^0.18.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.50") (d #t) (k 2)))) (h "1d44xmc3cwdhgiwp3wappgvqnj6p3gd5gl31jarrmjyxwxmzwli1") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.18.3 (c (n "ouroboros_examples") (v "0.18.3") (d (list (d (n "ouroboros") (r "^0.18.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.85") (d #t) (k 2)))) (h "1vq5n1jb7q8pcmidwhhq0f02wp6l3mf4bkf1v5x5s80xzlsngy7a") (f (quote (("std") ("miri") ("default" "std"))))))

(define-public crate-ouroboros_examples-0.18.4 (c (n "ouroboros_examples") (v "0.18.4") (d (list (d (n "ouroboros") (r "^0.18.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "trybuild") (r "=1.0.85") (d #t) (k 2)))) (h "161ya9w9vykqhim8lr173vfdcaqrpq35jdx6yl34k7n13fsgf7ym") (f (quote (("std") ("miri") ("default" "std"))))))

