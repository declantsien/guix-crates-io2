(define-module (crates-io ou to outoforderfs) #:use-module (crates-io))

(define-public crate-outoforderfs-0.1.0 (c (n "outoforderfs") (v "0.1.0") (d (list (d (n "chan-signal") (r "^0.1.5") (d #t) (k 0)) (d (n "fuse") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1wq7zbws2yizwfs45b8iyizmh4w1d9mf7dj3w3f6z9887bri593y")))

(define-public crate-outoforderfs-0.1.1 (c (n "outoforderfs") (v "0.1.1") (d (list (d (n "chan-signal") (r "^0.1.5") (d #t) (k 0)) (d (n "fuse") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0mjw0zgx2w49vqx0q5ng9d31gxdjx8wdi8x8sr18w97vv0xhra6k")))

