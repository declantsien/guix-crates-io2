(define-module (crates-io ou t- out-reference) #:use-module (crates-io))

(define-public crate-out-reference-0.1.0 (c (n "out-reference") (v "0.1.0") (h "1kr1vnngvnf3a1al54zdy9gyqaw1sjffp0cjsbiz2ca5hmi6d2y6") (f (quote (("std") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-out-reference-0.1.1 (c (n "out-reference") (v "0.1.1") (h "1fs457jh59vym27c92gi60mrz3y5a807q3306ws15gis7n262sc5") (f (quote (("std") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-out-reference-0.2.0 (c (n "out-reference") (v "0.2.0") (h "0xgdadvwkg34d9jcxzljf0s7bqip00pma4hrfsr1iz6alkn5idkf") (f (quote (("std") ("nightly") ("default" "std"))))))

