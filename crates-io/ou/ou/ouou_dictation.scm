(define-module (crates-io ou ou ouou_dictation) #:use-module (crates-io))

(define-public crate-ouou_dictation-0.1.0 (c (n "ouou_dictation") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "lingua") (r "^1.4.0") (f (quote ("japanese" "english" "chinese"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tts") (r "^0.25") (d #t) (k 0)))) (h "0ijdr0khhdr3r3n2xv9j8ybjmbvspnyr2f6xwqnq7hpc446bs7zw")))

