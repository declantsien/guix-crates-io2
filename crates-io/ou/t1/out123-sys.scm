(define-module (crates-io ou t1 out123-sys) #:use-module (crates-io))

(define-public crate-out123-sys-0.0.1 (c (n "out123-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lgpfpnn7cfammbj1prspafhf2dfy8qbdjl9i91xbncgmacvja5x")))

(define-public crate-out123-sys-0.1.0 (c (n "out123-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wjwxbvgiirnk12lx4ylb3x7cwjcvzivnyghdll7f07a0kdr69qn")))

(define-public crate-out123-sys-0.2.0 (c (n "out123-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fnw9ndj986h86w8qj36rswc3nxnflllg0s2948ay7prl0lgrdja") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.2.1 (c (n "out123-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01rdrfxmxvirnzq5v2g766finh3dakiwj1pys8076gbab33kc15l") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.3.0 (c (n "out123-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0iqqvxygj8cdkxbaiycqbzsw8qpf10640132f0pi3rpk4g14x3sj") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.3.1 (c (n "out123-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0av2f6kz02dh8y7ybnv19gbxkr9ry2x620di4s04qg9056vgg9c7") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.4.0 (c (n "out123-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lgh9jifgblm2qiwiwva4yr0lvn5sgh45f6h06lr7rhv8xfnx36w") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.4.1 (c (n "out123-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0r9s7xzkzqyzlh4661llci36qv6mdm427mpnkws94wdwdk6n4vvm") (f (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.5.0 (c (n "out123-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rfj01xbc7mp1dvn7qz65syqj6knq6prlxxy4sny4i2bgjz7gaax") (f (quote (("static" "mpg123-sys/static"))))))

