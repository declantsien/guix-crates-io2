(define-module (crates-io cc co cccolutils) #:use-module (crates-io))

(define-public crate-cccolutils-0.0.1 (c (n "cccolutils") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "190f0f0f799f7haqixs7zkmz1j09w3iqnwc5y37r8igwzq1fbr9c")))

(define-public crate-cccolutils-0.0.2 (c (n "cccolutils") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "0c7a2g45481qbyhzi6v0rkvk6x15j09z0ii099znlc0aimwc6k7d")))

(define-public crate-cccolutils-0.0.3 (c (n "cccolutils") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "0hhicymlr1n5yifsv6y2zkxja5nwgx0s97777kffm0slp4mkfxv1") (l "krb5")))

(define-public crate-cccolutils-0.1.0 (c (n "cccolutils") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "14gbwvkc109ix9mp3vpp9gfss2mndn1995a6k4g2v5zazqh8dim6") (l "krb5")))

(define-public crate-cccolutils-0.2.0 (c (n "cccolutils") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.30") (d #t) (k 1)))) (h "0hilaxnl7faibcj3cg19c5l8k5n56qxjahncxbkncy5rrs7yj2bg") (l "krb5")))

(define-public crate-cccolutils-0.2.1 (c (n "cccolutils") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1qy103lihb6x19c6dld47r3jpsgpb62ckw433vkk2fil1dgp0pf7") (l "krb5")))

(define-public crate-cccolutils-0.2.2 (c (n "cccolutils") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (k 1)))) (h "1j2py7n8m8adq4vl4h85wdws9nmby869mgfgpmz8lxx4palda33q") (l "krb5")))

