(define-module (crates-io cc hm cchmod) #:use-module (crates-io))

(define-public crate-cchmod-0.1.0 (c (n "cchmod") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06v2kgcg3z2xj6l37wjnfpb7hnvhnvq7ii0adb3iny8y50hr67ja")))

(define-public crate-cchmod-0.1.1 (c (n "cchmod") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ald1rwdlc98af8shrjd75wxny4xj2xcib9769fsxm7qv6py6hm8")))

(define-public crate-cchmod-0.1.2 (c (n "cchmod") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "122isc7f6yww07qhnx0jh43gly5jygz5ayrv1id3j9hbgvx58qk6")))

(define-public crate-cchmod-0.1.3 (c (n "cchmod") (v "0.1.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c6wxd9whk5b6bz3v9ba8qfa3843cg4x9iqmgm1nsnkm2vb1mi30")))

