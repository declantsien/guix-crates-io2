(define-module (crates-io cc es cces) #:use-module (crates-io))

(define-public crate-cces-0.1.0 (c (n "cces") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "1mcn13w9qixfwj0iw6jjh3gvd7j9wx27dnp93ckkldpl18mgk0i3")))

(define-public crate-cces-0.1.1 (c (n "cces") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "1ilrcl13lz9c9mf0gl3wlxcv0k6zlawli15667lvxzkhiql8w4v6")))

(define-public crate-cces-0.1.2 (c (n "cces") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "08xjxr4wz1qwrmqz1zjlld6wb8y6a093r0wa7hfkj66sjaqqhjgz")))

