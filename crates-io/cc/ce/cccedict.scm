(define-module (crates-io cc ce cccedict) #:use-module (crates-io))

(define-public crate-cccedict-0.1.0 (c (n "cccedict") (v "0.1.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0zns7p76ykdjshf6bi7v0p507zf31yv93a7myk40kq13lbql1ckv")))

(define-public crate-cccedict-0.1.1 (c (n "cccedict") (v "0.1.1") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0mx4yplabhhpn89wx558kkw05lqrmvjd14jkq28z7q48jvcwhczd")))

(define-public crate-cccedict-0.1.2 (c (n "cccedict") (v "0.1.2") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "1smig1slym19waj1l8931rjpyw5baw55mh90rbncyzss1a66i4v0")))

(define-public crate-cccedict-0.1.3 (c (n "cccedict") (v "0.1.3") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0pkj35hp1bsiivyqqhr8r37sb8wwk6qzz764f4s9wxfmkkv61dw7")))

(define-public crate-cccedict-0.2.0 (c (n "cccedict") (v "0.2.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "04dyk27wx34bgkyai5fy6948xdzvmshf8is74zv6z9457x8bjs08")))

(define-public crate-cccedict-0.2.1 (c (n "cccedict") (v "0.2.1") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0v29w01c4j932fm7ri2wb8z7z3q9cr239vc2ripn2zf5g148zswf")))

