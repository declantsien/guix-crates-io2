(define-module (crates-io cc s8 ccs811-rs) #:use-module (crates-io))

(define-public crate-ccs811-rs-0.1.0 (c (n "ccs811-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0pw1df36jai7ff1mja2vbshdmqd05zl7mqwxdnwzkskccxfs5qa3")))

