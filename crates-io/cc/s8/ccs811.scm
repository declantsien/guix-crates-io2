(define-module (crates-io cc s8 ccs811) #:use-module (crates-io))

(define-public crate-ccs811-0.1.0 (c (n "ccs811") (v "0.1.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "10dm9q20gwgvpmy0v5sxpmbrs2rckldjzqnd97w51kayxlaa3d4y")))

(define-public crate-ccs811-0.1.1 (c (n "ccs811") (v "0.1.1") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1qyaxkiks53ngap0j051czdwnn2dj87jiaxy81mmfb761s7cibav")))

(define-public crate-ccs811-0.1.2 (c (n "ccs811") (v "0.1.2") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "19ak6ffp9k86q8m8m140zq3ifgv9mfrgml2k7mav69ghagbxnrkg")))

(define-public crate-ccs811-0.1.3 (c (n "ccs811") (v "0.1.3") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1n7w2ys8ivnzd804jhvy9piljgmm1imv36hhmf4i14fk08jh8cz7")))

