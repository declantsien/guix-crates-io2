(define-module (crates-io cc ap ccap) #:use-module (crates-io))

(define-public crate-ccap-0.1.0 (c (n "ccap") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1p8vgq264003dy1kbsjq8bym9jsbgrcl5j8svrv63rs87794824l")))

