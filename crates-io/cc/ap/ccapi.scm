(define-module (crates-io cc ap ccapi) #:use-module (crates-io))

(define-public crate-ccapi-0.1.0 (c (n "ccapi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (d #t) (k 0)))) (h "1k3lbpbmwm0p2qcrv3bbinvasnqa90ngpxizsjqnssgzpdnl2jdy") (r "1.43.1")))

(define-public crate-ccapi-0.2.0 (c (n "ccapi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0c7ppk92dffg7dg4b43dyzr9rqnj5cfxh8zlfa74fjfwf5nd931p") (r "1.43.1")))

(define-public crate-ccapi-0.3.0 (c (n "ccapi") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "1dh1zlf4rmsshy49zdv9rr7ww8n0x5jaddmm1ifzx6x0kk03b7hy") (r "1.43.1")))

