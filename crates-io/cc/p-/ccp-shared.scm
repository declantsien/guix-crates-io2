(define-module (crates-io cc p- ccp-shared) #:use-module (crates-io))

(define-public crate-ccp-shared-0.1.0 (c (n "ccp-shared") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qqmmcpsvrz9akkp8zdwpxgbw4wfr59xbfbg38fg6viq5b22zijr")))

(define-public crate-ccp-shared-0.3.0 (c (n "ccp-shared") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h1a6ch6bqnsgs8x1b4wjd3gpi4gw8bl9va45lpa1l62ls4z0p70")))

(define-public crate-ccp-shared-0.4.0 (c (n "ccp-shared") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p5nl4l4diy9bz6djjcj05mlw2kqc67h0wqkvppp9ihcjf1dy4ql")))

(define-public crate-ccp-shared-0.4.1 (c (n "ccp-shared") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hic6cnsl6j7i36cpa121lnd66r5hax409adjjj2ihq8z8dizx8s")))

(define-public crate-ccp-shared-0.4.2 (c (n "ccp-shared") (v "0.4.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bi2f2w1ay43abzqqr3m2cxjq4a6a07z35ha35f6y6fdgpbwl7ns")))

(define-public crate-ccp-shared-0.5.0 (c (n "ccp-shared") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1njil6l1imclg5z55i2m3mqafs8g7ffq1max0cq19n6xmihha0xy")))

(define-public crate-ccp-shared-0.6.0 (c (n "ccp-shared") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a2dz4fy3vc1ln8p390dqhcaiirgjv3nkyjx38r6zdlbpmcy38v5")))

(define-public crate-ccp-shared-0.7.0 (c (n "ccp-shared") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vrpikijab7izyazspywd2qqqbq12iiyhir614pry3aib0xfq862")))

(define-public crate-ccp-shared-0.7.1 (c (n "ccp-shared") (v "0.7.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wy5c05mzcc73k3474zjnma4vmrayxlmzqcgl96jmds4gj390n4m")))

(define-public crate-ccp-shared-0.8.0 (c (n "ccp-shared") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vdp31535flb8cnimv1swqqpkbfxczaaxj21nb1fivxchgmh2wxq")))

(define-public crate-ccp-shared-0.9.0 (c (n "ccp-shared") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qkp785xli8a9z52xp7a9ib5a36y6919b23p972hw9s4pg9wmkkw")))

(define-public crate-ccp-shared-0.10.0 (c (n "ccp-shared") (v "0.10.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i7jgzx5la42mmlp89n05f3rw63s7q6b5qngpnq8hm2cr67ymisz")))

