(define-module (crates-io cc p- ccp-randomx-types) #:use-module (crates-io))

(define-public crate-ccp-randomx-types-0.1.0 (c (n "ccp-randomx-types") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mzgnw2dpnwjwmg2g1q68jsjhqz6gdyzwd924lswzr5849r9nxmk")))

