(define-module (crates-io cc p- ccp-utils) #:use-module (crates-io))

(define-public crate-ccp-utils-0.4.0 (c (n "ccp-utils") (v "0.4.0") (d (list (d (n "ccp-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "176n02y7bhl3c4msfcax2xhwacmkb9rsxp3wwcvd8ixb23xgz4mh")))

(define-public crate-ccp-utils-0.4.1 (c (n "ccp-utils") (v "0.4.1") (d (list (d (n "ccp-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "00dsz0qnkvq59i8lslkfx5dz6a8wp499myg119vvrz1h145frham")))

(define-public crate-ccp-utils-0.4.2 (c (n "ccp-utils") (v "0.4.2") (d (list (d (n "ccp-shared") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1jlyikxlyn8p1v6qpj9v1k52lbac97crnqq11g3cs0zj35d24sg1")))

(define-public crate-ccp-utils-0.5.0 (c (n "ccp-utils") (v "0.5.0") (d (list (d (n "ccp-shared") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "110l1qr6mfxq9rwgg5ihsx7x6jndr8fzsydkqfx0wjizgjf16yfx")))

(define-public crate-ccp-utils-0.6.0 (c (n "ccp-utils") (v "0.6.0") (d (list (d (n "ccp-shared") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0z73pwsrlzxbqhv4gnsxnqkmaxblr90scam4a9p2647lc6pkzn5q")))

(define-public crate-ccp-utils-0.7.0 (c (n "ccp-utils") (v "0.7.0") (d (list (d (n "ccp-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "16d3bk432pn364cvs8w6nbg80pxk4qw3k3hs8psndb39b0fyn79g")))

(define-public crate-ccp-utils-0.7.1 (c (n "ccp-utils") (v "0.7.1") (d (list (d (n "ccp-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0y8v335l7ak3p1slpbr12n6r39mfznim1m079bgvr9c67nmgcnsf")))

(define-public crate-ccp-utils-0.8.0 (c (n "ccp-utils") (v "0.8.0") (d (list (d (n "ccp-shared") (r "^0.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0c0wp87dwb00xzy4zm9aa7mbki704lxvq3nh88ixqcc460mr96kf")))

(define-public crate-ccp-utils-0.9.0 (c (n "ccp-utils") (v "0.9.0") (d (list (d (n "ccp-shared") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1zilm6z32xm6hl1a4gj2az87cjfj67q6dkvdxs1sc3lc7yvgxdn8")))

(define-public crate-ccp-utils-0.10.0 (c (n "ccp-utils") (v "0.10.0") (d (list (d (n "ccp-shared") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "0334i1mn41gl6fjfnq6svp8al6583qagd3gi6dbj989jingmnydk")))

