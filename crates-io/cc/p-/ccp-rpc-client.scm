(define-module (crates-io cc p- ccp-rpc-client) #:use-module (crates-io))

(define-public crate-ccp-rpc-client-0.7.1 (c (n "ccp-rpc-client") (v "0.7.1") (d (list (d (n "ccp-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.21.0") (f (quote ("client" "macros" "tokio" "server"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gfvjnf2cbsbkj0f9l884h0j93w7aydga4mqh50a40x6fdlqka1r")))

(define-public crate-ccp-rpc-client-0.8.0 (c (n "ccp-rpc-client") (v "0.8.0") (d (list (d (n "ccp-shared") (r "^0.8.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.21.0") (f (quote ("client" "macros" "tokio" "server"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "104rz9b3axczwrp9yssc32l2w77zn8mmx9mj8w27pn5agqg5bjsx")))

(define-public crate-ccp-rpc-client-0.9.0 (c (n "ccp-rpc-client") (v "0.9.0") (d (list (d (n "ccp-shared") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.21.0") (f (quote ("client" "macros" "tokio" "server"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rbxknz0fhs8m2q7s07ws03fm2xvh72xycbcs1vpchw7ji9f1218")))

(define-public crate-ccp-rpc-client-0.10.0 (c (n "ccp-rpc-client") (v "0.10.0") (d (list (d (n "ccp-shared") (r "^0.10.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.21.0") (f (quote ("client" "macros" "tokio" "server"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "165jhzjh7g5cgzva5aizs060r64a044y49smc9kzkz2yli3dd15n")))

