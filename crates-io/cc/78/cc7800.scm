(define-module (crates-io cc #{78}# cc7800) #:use-module (crates-io))

(define-public crate-cc7800-0.1.0 (c (n "cc7800") (v "0.1.0") (d (list (d (n "cc6502") (r "^0.3") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pyl7nyjmp7rgfg4kv12ykik8il19miwq99qpw6afgnq2bjxspqx") (y #t)))

(define-public crate-cc7800-0.1.1 (c (n "cc7800") (v "0.1.1") (d (list (d (n "cc6502") (r "^0.3") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iv364j0gs6wba0z0839p6b2ggqqnfz4q0xr5zhm1mcgs2kjs20v") (y #t)))

(define-public crate-cc7800-0.1.2 (c (n "cc7800") (v "0.1.2") (d (list (d (n "cc6502") (r "^0.3") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "158pjkbzpfr4v2z34iai148ikp1633na618mrdkmgjwicsydr2gi")))

(define-public crate-cc7800-0.2.0 (c (n "cc7800") (v "0.2.0") (d (list (d (n "cc6502") (r "^0.4") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x1dq28bmgpmypdr42scl4kvjrairjyig28pq6g3gjx8wnrf2s66")))

(define-public crate-cc7800-0.2.1 (c (n "cc7800") (v "0.2.1") (d (list (d (n "cc6502") (r "^0.5") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0vhaslnnjylszkhmjn4gmf8nxkiirbhckwikb7z0hdcdp0zwqnyk")))

(define-public crate-cc7800-0.2.2 (c (n "cc7800") (v "0.2.2") (d (list (d (n "cc6502") (r "^0.5") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "19160036jzfqk18zbp46vsv7i7npm7j0i97km3bawi1cwdf0s83j")))

(define-public crate-cc7800-0.2.3 (c (n "cc7800") (v "0.2.3") (d (list (d (n "cc6502") (r "^0.5") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1gkhlk0v2vsav8jv92vnqs5v9bkclk2lmg34kiqhwgqg7921091s")))

(define-public crate-cc7800-0.2.4 (c (n "cc7800") (v "0.2.4") (d (list (d (n "cc6502") (r "^0.6") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1qv393zhmqki2c7k619ghqq1sigliwhx4zw8i1806ys9r6g0ki6g")))

(define-public crate-cc7800-0.2.5 (c (n "cc7800") (v "0.2.5") (d (list (d (n "cc6502") (r "^0.7") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "18vcl59vlmgapqgx4h67dxbq54l0b7r1ycz4nnhh4x0ryqym2xxb")))

(define-public crate-cc7800-0.2.7 (c (n "cc7800") (v "0.2.7") (d (list (d (n "cc6502") (r "^0.8") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0ql0ga5vzsjxlms6mypz71n8n4ddag5hv2w93r8fxpfvfgrkymys")))

(define-public crate-cc7800-0.2.9 (c (n "cc7800") (v "0.2.9") (d (list (d (n "cc6502") (r "^0.9") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hx99m9lvnp1i365vsaa2m6j7akawivxnqhdasv1r5dww1330ni3")))

(define-public crate-cc7800-0.2.10 (c (n "cc7800") (v "0.2.10") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yshc0wci0nns5rsm4ai162nz35fx5ky0whkppdfg9l4immv31ir")))

(define-public crate-cc7800-0.2.11 (c (n "cc7800") (v "0.2.11") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ylq4czk7bk74rhpcfmxc72lckjl1fxy0j2flbsjzfjbncqkzh4c")))

(define-public crate-cc7800-0.2.12 (c (n "cc7800") (v "0.2.12") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ymbczccpc8nxcdpxwh3vk9lgm6q89y29dljpcvzvh21rslc76q5")))

(define-public crate-cc7800-0.2.14 (c (n "cc7800") (v "0.2.14") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ma277fq6ya0msjf2knf51ifa54z8z5b3904zfyammcdfjpqih9k") (y #t)))

(define-public crate-cc7800-0.2.16 (c (n "cc7800") (v "0.2.16") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1y9fkhp1qs14skbxm8an1f8c76v2z93f4rm2f2vk6w6c4fsrdzcd")))

(define-public crate-cc7800-0.2.17 (c (n "cc7800") (v "0.2.17") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0acpsa6xdgv3d7in0ic1nah0dhlsjfsrxmklx3bq92ij8zl6jrlf")))

(define-public crate-cc7800-0.2.18 (c (n "cc7800") (v "0.2.18") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18nh474hsva1yvdz5hh28j9rns5n3nmm95xfzv3cx4wzqwf9acma")))

(define-public crate-cc7800-0.2.19 (c (n "cc7800") (v "0.2.19") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19pkrg77mmvvgjshdkbmxyd2kyq753msf4lkzjf3pzsc7ddmvxhd")))

(define-public crate-cc7800-0.2.21 (c (n "cc7800") (v "0.2.21") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18ikvwp2mmgdd9lf6k7x555hyccld9b468ma80vp9c6ykzkvxz69")))

(define-public crate-cc7800-0.2.24 (c (n "cc7800") (v "0.2.24") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0a3lv7qd24byjxsh1y7w734c0fz8r1s815pw0ahwchhkinwwzb9q")))

(define-public crate-cc7800-0.2.25 (c (n "cc7800") (v "0.2.25") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari7800"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jnpd56i1wg8dy2fddwsksksfksgwrikvl9x8859hvdrf17wyvpa")))

