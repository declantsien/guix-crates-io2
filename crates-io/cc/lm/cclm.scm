(define-module (crates-io cc lm cclm) #:use-module (crates-io))

(define-public crate-cclm-0.0.1 (c (n "cclm") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jq2n5q5v4qjb7dqyn5ijnm3nfiaxpwcv5xb8xd7jka7ak1hdicr") (f (quote (("default")))) (r "1.67")))

