(define-module (crates-io cc sd ccsds_primary_header) #:use-module (crates-io))

(define-public crate-ccsds_primary_header-0.1.0 (c (n "ccsds_primary_header") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0g4q3svpjnxi8274z6shvj593wbi3bamqq1agxsqx0aw7hlyx7gb")))

(define-public crate-ccsds_primary_header-0.2.0 (c (n "ccsds_primary_header") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0gp04b07jkk0zj0ipdj12dvzvy9ybbdgv8lv8bp3wdirip8n699r")))

(define-public crate-ccsds_primary_header-0.3.0 (c (n "ccsds_primary_header") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0i20xbmln5wfqx4l5iq65l4ic529n04g8zrnh6pw0d3adh1y192n")))

(define-public crate-ccsds_primary_header-0.4.0 (c (n "ccsds_primary_header") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1r755q06pfg94hbldm38yivg7a4k3jnbbr87vk8q48az5zi0gb7i")))

(define-public crate-ccsds_primary_header-0.5.0 (c (n "ccsds_primary_header") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "00wahrc7y8ram3idmv8xbzy9fy3h7sv1yrzwqhs8rfl7b8r8s2rs")))

(define-public crate-ccsds_primary_header-0.6.0 (c (n "ccsds_primary_header") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "01abml50qnj9xadnrbwxwqg8bzzpq90cy85qfyk564jay3f6xirn")))

(define-public crate-ccsds_primary_header-0.7.0 (c (n "ccsds_primary_header") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "15jhl60m28y90prvpkswcgarhrxwbf5z4ddg3zp8kyb80n8ig2n6")))

(define-public crate-ccsds_primary_header-0.8.0 (c (n "ccsds_primary_header") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "063i5s1xg3zddfsqjiq24qajj7c05lbkbz8g9k9dw5ay9vqm8clm")))

(define-public crate-ccsds_primary_header-0.9.0 (c (n "ccsds_primary_header") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0c83nwxs4b5ddcxkfiwpjlivgm9lvvjfd4yks2p2nd52p4j33bxy")))

(define-public crate-ccsds_primary_header-0.9.1 (c (n "ccsds_primary_header") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1kvzvpjz1rfk4khzcig2nqr35c6gp783ww3nmcp9vwfwwnm4j2lv")))

(define-public crate-ccsds_primary_header-0.9.2 (c (n "ccsds_primary_header") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "02cxa754l3jdgq5v1k1agq74swlih2ki3ifcb25ypxaimdpsfxvp")))

(define-public crate-ccsds_primary_header-0.10.0 (c (n "ccsds_primary_header") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1zc4yfz7z51s23k7c6c2995xb6jd580ikpa60b37fqgh2jy7j9x9")))

(define-public crate-ccsds_primary_header-0.10.1 (c (n "ccsds_primary_header") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0iwvvkz2hwpmc89d6gagg8alb8llsjkjqdji55y8i8pn48grm404")))

(define-public crate-ccsds_primary_header-0.10.2 (c (n "ccsds_primary_header") (v "0.10.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0f169i6xa4kp0azwwx8hjghziq7rjz4dcp1wbk92cfvk05dcayw7")))

(define-public crate-ccsds_primary_header-0.10.3 (c (n "ccsds_primary_header") (v "0.10.3") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1jzzlih8pz80rljjw1234sa0mcarsd55k0k3wycmbmzphdxy120r")))

(define-public crate-ccsds_primary_header-0.10.4 (c (n "ccsds_primary_header") (v "0.10.4") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "15685dm7vigl7gsxjihwykyhvycvbg97p2smc7k6f33nrzcm5fyi")))

(define-public crate-ccsds_primary_header-0.10.5 (c (n "ccsds_primary_header") (v "0.10.5") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "04707grv9bvmx0gvyfw98ac54laay497xwmnlahnhiyflcsv8yyd")))

(define-public crate-ccsds_primary_header-0.11.0 (c (n "ccsds_primary_header") (v "0.11.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1l0bcdhbxvc2hzfhxpf6n6g6jgawyjniwrjkvfwagmf6cpzi9f6x")))

(define-public crate-ccsds_primary_header-0.12.0 (c (n "ccsds_primary_header") (v "0.12.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1z9knv1s8kb1azmks7gydhc3ynyz2b1dg5dmr8d8ggyz5lqa3s3h")))

(define-public crate-ccsds_primary_header-0.13.0 (c (n "ccsds_primary_header") (v "0.13.0") (d (list (d (n "byteorder") (r "^1.2.6") (k 0)) (d (n "bytes") (r "^0.4") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1pzj1jv6lbg872kiwdw6ri37wybm1sfb7xc9xwmm2xjpblw7747s")))

(define-public crate-ccsds_primary_header-0.14.0 (c (n "ccsds_primary_header") (v "0.14.0") (d (list (d (n "byteorder") (r "^1.2.6") (k 0)) (d (n "bytes") (r "^0.4") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1m0yhwzgypz8xkh8f1nn1p54fidslxh7hjqs95s1bg4wd85c2s36")))

(define-public crate-ccsds_primary_header-0.15.0 (c (n "ccsds_primary_header") (v "0.15.0") (d (list (d (n "byteorder") (r "^1.2.6") (k 0)) (d (n "bytes") (r "^0.4") (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1d6zschl2msi33nali9y696q4qrswbsds4mysilgl66w8snzbx75")))

