(define-module (crates-io cc sd ccsds_spacepacket) #:use-module (crates-io))

(define-public crate-ccsds_spacepacket-0.1.0 (c (n "ccsds_spacepacket") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1acck6wwdff3jzsknd1n960zrr54ah0vhy2f6ifkl29dgczg4q8b")))

(define-public crate-ccsds_spacepacket-0.2.0 (c (n "ccsds_spacepacket") (v "0.2.0") (d (list (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "1wicr7dh2dmhc15i7i503wqmxrbkafv20xwdqdrk5hxvs8s1w9c2")))

