(define-module (crates-io cc le cclean) #:use-module (crates-io))

(define-public crate-cclean-0.0.1 (c (n "cclean") (v "0.0.1") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "tinyrick") (r "^0.0.9") (o #t) (d #t) (k 0)) (d (n "tinyrick_extras") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "1nj3gwjr0kn6wjkfy2pyh1rmzymqm8h5908s1wl2cwxaw0yiwnma") (f (quote (("letmeout" "tinyrick" "tinyrick_extras"))))))

(define-public crate-cclean-0.0.2 (c (n "cclean") (v "0.0.2") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tinyrick") (r "^0.0.9") (o #t) (d #t) (k 0)) (d (n "tinyrick_extras") (r "^0.0.6") (o #t) (d #t) (k 0)))) (h "1rsbs3994rp5y23kzmhpvvid36fayhpg1mqzz6mh854zxx6y2grv") (f (quote (("letmeout" "tinyrick" "tinyrick_extras"))))))

