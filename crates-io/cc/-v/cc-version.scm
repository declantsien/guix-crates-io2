(define-module (crates-io cc -v cc-version) #:use-module (crates-io))

(define-public crate-cc-version-0.1.0 (c (n "cc-version") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0p70i5nfr3azpqsjayb5rc8vq1jvwhkpicfcdhphc6hyaa52n51f")))

