(define-module (crates-io cc an ccanvas-config-derive) #:use-module (crates-io))

(define-public crate-ccanvas-config-derive-0.1.0 (c (n "ccanvas-config-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "proc-macro" "parsing"))) (k 0)))) (h "1r1ypvc6rdb54jailn51zrl15bpxgxwjx8lwqxrq86j9la4457nh")))

