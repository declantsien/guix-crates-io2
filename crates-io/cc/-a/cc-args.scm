(define-module (crates-io cc -a cc-args) #:use-module (crates-io))

(define-public crate-cc-args-0.1.0 (c (n "cc-args") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 0)))) (h "1q31qg8gjaypzjwarg0gknandfh5dqmdvaps95q7xpaajcgrx8y3") (f (quote (("default")))) (s 2) (e (quote (("pkg-config" "dep:pkg-config") ("cc" "dep:cc") ("bindgen" "dep:bindgen"))))))

(define-public crate-cc-args-0.2.0 (c (n "cc-args") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.2") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.29") (o #t) (d #t) (k 0)))) (h "1hzdhsfwmq98dhpl4c0rd6wb1h1gzc54v2z69wrdcalk0pybm6wv") (f (quote (("default")))) (s 2) (e (quote (("pkg-config" "dep:pkg-config") ("cc" "dep:cc") ("bindgen" "dep:bindgen"))))))

