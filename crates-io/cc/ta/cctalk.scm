(define-module (crates-io cc ta cctalk) #:use-module (crates-io))

(define-public crate-cctalk-0.1.0 (c (n "cctalk") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0jv80j4bsmhfwjsm22l6aypdimzbcyz5mrispj3i72kz42bs2kki")))

(define-public crate-cctalk-0.2.0 (c (n "cctalk") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "00n4l5130j8a1v1qaxn0avf4c7km84aljrxyl75hg0h2h3mhi6s6")))

