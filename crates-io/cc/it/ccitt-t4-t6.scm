(define-module (crates-io cc it ccitt-t4-t6) #:use-module (crates-io))

(define-public crate-ccitt-t4-t6-0.1.0 (c (n "ccitt-t4-t6") (v "0.1.0") (d (list (d (n "color-eyre") (r ">=0.5.0, <0.6.0") (k 2)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1jp8l8p0k2hj6a61jvazwxps9wiaicvyqff2bah7vzjhac273y6p")))

