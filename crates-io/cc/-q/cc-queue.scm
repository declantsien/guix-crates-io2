(define-module (crates-io cc -q cc-queue) #:use-module (crates-io))

(define-public crate-cc-queue-0.0.0 (c (n "cc-queue") (v "0.0.0") (h "0j4z504krjp469wbxshcgs21x9avin7grrq0xdqbmm5i9ly31zgj")))

(define-public crate-cc-queue-0.0.1 (c (n "cc-queue") (v "0.0.1") (h "0xk8cassiysng6isxd52fh6vps7h57yd9qhs4xr5msylxglampm3")))

