(define-module (crates-io cc -c cc-cli) #:use-module (crates-io))

(define-public crate-cc-cli-0.1.0 (c (n "cc-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "inquire") (r "^0.0.6") (d #t) (k 0)))) (h "12lpdg2fl1rmbh7i8y1c32q93nvxs73jap01mshsmd3x4vkpvfir")))

(define-public crate-cc-cli-0.1.1 (c (n "cc-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "inquire") (r "^0.0.6") (d #t) (k 0)))) (h "1lvpg1myj86s4axnp85rbcq6002z1p21y1p5lrgb38j83j0ndygg")))

(define-public crate-cc-cli-0.1.2 (c (n "cc-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.50") (d #t) (k 0)) (d (n "argh") (r "^0.1.6") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)))) (h "0qf2grilj6wxlwrsbmfzk5rzmhcm3j5wa8lfz02qxdgkg25v3j8q")))

(define-public crate-cc-cli-0.1.3 (c (n "cc-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)))) (h "1y7m4l0g0a2iygn7rxmcibqk25rs61slhbkhf2d2qcv2xv3wnrrr")))

(define-public crate-cc-cli-0.1.5 (c (n "cc-cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)))) (h "0wmrvv82802svrddyij3jfbwvl1hs0x2ci28ldg5nbsghff7mbnh")))

