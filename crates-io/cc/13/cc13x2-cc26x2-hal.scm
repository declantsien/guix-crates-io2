(define-module (crates-io cc #{13}# cc13x2-cc26x2-hal) #:use-module (crates-io))

(define-public crate-cc13x2-cc26x2-hal-0.1.0 (c (n "cc13x2-cc26x2-hal") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0aa8rydzik349cww5dqmdmn26vngz5920ys7gdh1zvcs53m4lahh") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

