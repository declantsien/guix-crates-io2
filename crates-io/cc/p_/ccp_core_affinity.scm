(define-module (crates-io cc p_ ccp_core_affinity) #:use-module (crates-io))

(define-public crate-ccp_core_affinity-0.8.1 (c (n "ccp_core_affinity") (v "0.8.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\"))") (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0pjxhfn9dspjdi2d7csimy8vxnsx5qlkhmiaz44l0hz9czqng8rj")))

