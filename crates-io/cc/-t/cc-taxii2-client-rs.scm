(define-module (crates-io cc -t cc-taxii2-client-rs) #:use-module (crates-io))

(define-public crate-cc-taxii2-client-rs-0.1.2 (c (n "cc-taxii2-client-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1njwl5khr45is2f900k2c4plw3lz04sfr3xrmrggq85clqmqf8rn") (y #t) (r "1.74")))

(define-public crate-cc-taxii2-client-rs-0.1.3 (c (n "cc-taxii2-client-rs") (v "0.1.3") (d (list (d (n "base64") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1xqqlv015jkn6f8pwdxj44ia1ql90y5qlpj5pd3jhxqmf2bw0q2z") (y #t) (r "1.74")))

(define-public crate-cc-taxii2-client-rs-0.1.4 (c (n "cc-taxii2-client-rs") (v "0.1.4") (d (list (d (n "base64") (r "^0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0pflnfzin416aqsrpj3dhqg8psaz7fgr0h73hb5givzix06i6jq3") (y #t) (r "1.74")))

(define-public crate-cc-taxii2-client-rs-0.1.5 (c (n "cc-taxii2-client-rs") (v "0.1.5") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0861vpsgf0xkw8l225mzrpvczxzlyvbz2ky5almyvya38k7idpk3") (r "1.74")))

