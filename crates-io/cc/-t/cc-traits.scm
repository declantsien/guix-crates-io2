(define-module (crates-io cc -t cc-traits) #:use-module (crates-io))

(define-public crate-cc-traits-0.1.0 (c (n "cc-traits") (v "0.1.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "18ybzikwb2c5ck0z9ny439k1z6jq2dgk4k7k7syhyyvjwvx823ly") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.1.1 (c (n "cc-traits") (v "0.1.1") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "11zib09mn3ngdnbngjpvgx6s4x4a3w6dxwcs91asrgxdnidswmrd") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.1.2 (c (n "cc-traits") (v "0.1.2") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0cq8b90yr4vg8dpsp842arxpp720fvl489v0arwsv9rdb0kha07r") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.2.0 (c (n "cc-traits") (v "0.2.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1jjzrwhhhgq9vd1jm10321b1z549i6mm29p5qn92gcw4dx7zcfls") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.2.1 (c (n "cc-traits") (v "0.2.1") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1knyhb1hapx7mvxvpdlnk3373lc6ri7mh0cz65j096nc3fs3yljy") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.3.0 (c (n "cc-traits") (v "0.3.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0lywcvc6wqs2q8yh3szp2kqfmvs7as1qhb9pckbp471pdzky4lds") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.4.0 (c (n "cc-traits") (v "0.4.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0v8s1jdqa5hqdaqhx3hcmfv00c3zgbja1kk1g9x8wihw71yh12lv") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.5.0 (c (n "cc-traits") (v "0.5.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1606l42h2fk1axziw4jwbnna7vc7dca1v7i8h3k8l2ja5hys3wz7") (f (quote (("nostd") ("nightly")))) (y #t)))

(define-public crate-cc-traits-0.5.1 (c (n "cc-traits") (v "0.5.1") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "169mzw0z2ld8gz4zc5ik92q89kajgv6lcidjhv7rvi8wwlkp2njz") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.5.2 (c (n "cc-traits") (v "0.5.2") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0768cp3qqnlhd2mk8k82fk7h8y3a1s7qmp7yav751h4yl55nfh27") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.6.0 (c (n "cc-traits") (v "0.6.0") (d (list (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1bfda2fwm7lwn35rwrd81w58rb9rmxznxv8n3wsg13w35vwsph6x") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.7.0 (c (n "cc-traits") (v "0.7.0") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1fbxnks79wc1d2b1sfwa396xssgay3p5iaml3krh1xfakg6zg45y") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.7.1 (c (n "cc-traits") (v "0.7.1") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "13q8h6mqny6ddqq4ldyj4chs6f0dv5ypm7zkssxnn5qfaphh4cs0") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.7.2 (c (n "cc-traits") (v "0.7.2") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0r0z6iqgwfa36rkqmlkqlwz4z0rvmw1a9mk5zy4fijhhl50m80k9") (f (quote (("nostd") ("nightly")))) (y #t)))

(define-public crate-cc-traits-0.7.3 (c (n "cc-traits") (v "0.7.3") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0cfa1xzl2rrhj30r087rlvl13ad13n5pqk5plj2apjr15ajm978f") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.8.0 (c (n "cc-traits") (v "0.8.0") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1fkj2l6ys0qpfa6h8b3z0rs305hsj2x5gpb0gfkvjza255663x4n") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-0.8.1 (c (n "cc-traits") (v "0.8.1") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1s81g27mq2d1mhpqn8gazjc8p4ji2s0sw2sf5la857hvk6sx5q90") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-1.0.0 (c (n "cc-traits") (v "1.0.0") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0k5z9vak055ailvcl5nhxv2z86r9ij80nlkxccfi9pg99bs27jxy") (f (quote (("nostd") ("nightly"))))))

(define-public crate-cc-traits-2.0.0 (c (n "cc-traits") (v "2.0.0") (d (list (d (n "ijson") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1db2m7drl9w3yda4ybxvhykz45krqrlapcg16wkm4jpg67ph60q6") (f (quote (("std") ("nightly") ("default" "alloc" "std") ("alloc") ("all-impls" "slab" "smallvec" "serde_json" "ijson"))))))

