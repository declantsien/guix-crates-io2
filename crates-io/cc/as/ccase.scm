(define-module (crates-io cc as ccase) #:use-module (crates-io))

(define-public crate-ccase-0.1.0 (c (n "ccase") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "159j6k3rmb4sbsiq8fdz7ga6f6vack6b802k1rs35j0w8zc1ngaj")))

(define-public crate-ccase-0.1.1 (c (n "ccase") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.3.2") (d #t) (k 0)))) (h "0sp7ayi0k1fkqs58ri6ahx8qiyfvpmj907ijc8a0qqmbqc26jxfq")))

(define-public crate-ccase-0.2.0 (c (n "ccase") (v "0.2.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (f (quote ("random"))) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "1a785rmq6gmlfl0jkwxfid8agwnyj7a1hzqq0rs1am2scl1bfkhb")))

(define-public crate-ccase-0.4.0 (c (n "ccase") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "color" "wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (f (quote ("random"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "0626vpdv2ah4xpj0l2n878dfman1aar8s8nwg3r5lkq46a6j8f0h")))

(define-public crate-ccase-0.4.1 (c (n "ccase") (v "0.4.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "color" "wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (f (quote ("random"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "05nnyf6qpd29rmlsx6c5jldg7l98aamcrfdwpqfr5mhgvc5a89vj")))

