(define-module (crates-io cc on cconst) #:use-module (crates-io))

(define-public crate-cconst-0.2.0 (c (n "cconst") (v "0.2.0") (h "0ffkn553sk6mjbfxb1hj0yaf03yrky1f58cps1gha6bhhbq1w9ny")))

(define-public crate-cconst-0.2.1 (c (n "cconst") (v "0.2.1") (h "0pp43cpgvcf7sxnwxfxam4wdlqh9pyvkvichdg3v2lq7ijlfjxvp")))

(define-public crate-cconst-0.2.2 (c (n "cconst") (v "0.2.2") (h "1g4w4snq1fhfj155997pqz90v44667cn2yqp8p8w6gsk7hfh2wxv")))

