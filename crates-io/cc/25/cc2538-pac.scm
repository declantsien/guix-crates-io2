(define-module (crates-io cc #{25}# cc2538-pac) #:use-module (crates-io))

(define-public crate-cc2538-pac-0.1.0 (c (n "cc2538-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16z44wy2ghl9h833g26li7jb89m2kml1q3y2028901fhb1rpjz54") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.1.1 (c (n "cc2538-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0sbh77pjm1j95g9wkrvn77sibk9rbxad8crjmz7wcg5378y8a1lw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.1.2 (c (n "cc2538-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0lrlivwnrczyflfmwlx0p7208mq3wvzhn9px8m0gpzkd99qvmsck") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.2.0 (c (n "cc2538-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0dp0y7dmnswvf32i3xc1mjlmsxm55ccdf5bv3k0wpv33mafbzg08") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.2.1 (c (n "cc2538-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s172y8fppbg9i9vd5q4xsds598z00a6x4nnlbk0xmrfv55bb93g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.3.0 (c (n "cc2538-pac") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0igpmll308whn12maisv74k6nim4isp31mmaf1085yrrcwzdgx2w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.4.0 (c (n "cc2538-pac") (v "0.4.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "059frj0m7bbca50livdnv6ypwv50k9xm300yj8n2vv7yi5gdxh1q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.4.1 (c (n "cc2538-pac") (v "0.4.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qms9ai2rqjif6x9visrifzxbsg97bvpkq7dqbn7r08mgmblqd2q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.5.0 (c (n "cc2538-pac") (v "0.5.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0vk9f146z3s1ygvcr3v2xl86bn708dpkba1n6vm6fc8d6x6flkhm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-pac-0.6.0 (c (n "cc2538-pac") (v "0.6.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ri9s1md01k55gh6f7zbghq34ixm8zwxqqgdbr12x659mp4kf11h") (f (quote (("rt" "cortex-m-rt/device"))))))

