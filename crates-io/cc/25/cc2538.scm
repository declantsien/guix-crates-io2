(define-module (crates-io cc #{25}# cc2538) #:use-module (crates-io))

(define-public crate-cc2538-0.1.0 (c (n "cc2538") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0p6fz7j6rjggbr0agc21jr7ibfjllv0wx96yxrds64y4m6kvnp1g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2538-0.2.0 (c (n "cc2538") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gixkqsry29n8609b9slkybrwsbwbjrhyamczwxv206b98h1jij8") (f (quote (("rt" "cortex-m-rt/device"))))))

