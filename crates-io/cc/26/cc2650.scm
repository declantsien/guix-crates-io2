(define-module (crates-io cc #{26}# cc2650) #:use-module (crates-io))

(define-public crate-cc2650-0.1.0 (c (n "cc2650") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xspkmmw0nz46389jzij7mpw3y28yfdp23zl83zc05h8w4fs2fk5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc2650-0.1.1 (c (n "cc2650") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1chjrd14m7s26q6idz0inmr325sdxa3kly9krmaj0mv99l5k250n") (f (quote (("rt" "cortex-m-rt/device"))))))

