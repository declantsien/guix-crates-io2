(define-module (crates-io cc #{26}# cc2600) #:use-module (crates-io))

(define-public crate-cc2600-0.1.0 (c (n "cc2600") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0mg9m3bwdnjgjrr81bydvkp80in9nvi7c75n629l47c01x4rl0y7") (y #t)))

(define-public crate-cc2600-0.1.1 (c (n "cc2600") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "05dmcj8kjppi2ifm094941i8bprcbrcgmw4kl50pqq0bj54pkcsa") (y #t)))

(define-public crate-cc2600-0.1.2 (c (n "cc2600") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0pgll3hm1xh0vkzkiyqz48l5f444qk8mjchwrv6bd3jcfv00vxqs") (y #t)))

(define-public crate-cc2600-0.1.3 (c (n "cc2600") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "10mmy1hlamn5mgj54j7a2nry8k7mias7pm4kv122swkxxgyb0shl") (y #t)))

(define-public crate-cc2600-0.1.4 (c (n "cc2600") (v "0.1.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0nbfd1jn4my4km3wa8jvgmxp8rsq43w1g8g1am9pjrbcnbajgl11") (y #t)))

(define-public crate-cc2600-0.1.5 (c (n "cc2600") (v "0.1.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "180m194klmxfqaq7sqpzcn8ij5vgdws8ivrhnmk86by24fmmnhpd") (y #t)))

(define-public crate-cc2600-0.1.6 (c (n "cc2600") (v "0.1.6") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "14g70f8q1vyp96pp3ds7lhqi1q6g986jq50rg6y8a8590rcgh4bv") (y #t)))

(define-public crate-cc2600-0.1.7 (c (n "cc2600") (v "0.1.7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0hbn4zijp516rh3rf4q2zqzxkfbc5qf58zanfirbbqj1d1g3vr62") (y #t)))

(define-public crate-cc2600-0.1.8 (c (n "cc2600") (v "0.1.8") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0390mi77pbdah9r8n2vy600figw2x2ch60pqac4rpy4khn8brwbx") (y #t)))

(define-public crate-cc2600-0.1.9 (c (n "cc2600") (v "0.1.9") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1vfi9l4xm6gs5frj7sgq9rj948zh82vpjydzp669raj0aayrdmpw") (y #t)))

(define-public crate-cc2600-0.1.10 (c (n "cc2600") (v "0.1.10") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1j9chkcgxgqh8g2askf2pp682w4g9jcnfddmpr7gcgif1fjnf1zx") (y #t)))

(define-public crate-cc2600-0.2.0 (c (n "cc2600") (v "0.2.0") (d (list (d (n "cc6502") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "051gcb7qrla5m6fnzm4y47fqj72w9njfg16wprcvzvc2144z164h") (y #t)))

(define-public crate-cc2600-0.1.11 (c (n "cc2600") (v "0.1.11") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1yc31dsppf1di11yw9zkx6h5a5daw4i82qmha3cb2007s6cfym3p") (y #t)))

(define-public crate-cc2600-0.2.1 (c (n "cc2600") (v "0.2.1") (d (list (d (n "cc6502") (r "^0.3") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x5qd71z4a0jpp0ky9nzi2w6mn9ygszbgsfj6k6ix63ymjx0318f")))

(define-public crate-cc2600-0.1.12 (c (n "cc2600") (v "0.1.12") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1xl1w3k0im6nf78i1w5xclqxcf4glm6j4z4x6z9p58g60iy2fahf") (y #t)))

(define-public crate-cc2600-0.1.13 (c (n "cc2600") (v "0.1.13") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1mvk9k7wri0zsyfpi1s5cv3xsi9nra6ikjcjld84wm50vwh4qwsc")))

(define-public crate-cc2600-0.3.0 (c (n "cc2600") (v "0.3.0") (d (list (d (n "cc6502") (r "^0.4") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01brgdzl9gzf0cl81b6blkzwjkwh1ccscqpr6i82vdlz5vj8r1bn")))

(define-public crate-cc2600-0.3.1 (c (n "cc2600") (v "0.3.1") (d (list (d (n "cc6502") (r "^0.5") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0xzgc1dysp90rzmm4warmf83p6r9qrxy85lmz950c1x11dzknmgj")))

(define-public crate-cc2600-0.4.0 (c (n "cc2600") (v "0.4.0") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0yzkzv5i7z46w5y31j8xi29ha9cnp5msqm0x1y20g748200clp45")))

(define-public crate-cc2600-0.4.1 (c (n "cc2600") (v "0.4.1") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1npmp6j9nlyp2n6wpnjlzwjainkncnqmi6smja7h4jzhnhj08xa8")))

(define-public crate-cc2600-0.4.3 (c (n "cc2600") (v "0.4.3") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cm7hnpaxvpswfvxh75fsw109xx62n28ng6mg6zi2f100f19w3q6")))

(define-public crate-cc2600-0.4.4 (c (n "cc2600") (v "0.4.4") (d (list (d (n "cc6502") (r "^1.0") (f (quote ("atari2600"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jqjz7kmmg2p4n9xrxp93bajqwrllx4hngs93x7qwr6irvvbnjsp")))

