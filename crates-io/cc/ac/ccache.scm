(define-module (crates-io cc ac ccache) #:use-module (crates-io))

(define-public crate-ccache-0.1.0 (c (n "ccache") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("raw"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fgqfyzf48lfnyhj1k0irxcrpvc2y05r281j7wv5bqg01hv2qxi9")))

