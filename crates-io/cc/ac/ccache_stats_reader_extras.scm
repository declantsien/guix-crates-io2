(define-module (crates-io cc ac ccache_stats_reader_extras) #:use-module (crates-io))

(define-public crate-ccache_stats_reader_extras-0.1.0 (c (n "ccache_stats_reader_extras") (v "0.1.0") (d (list (d (n "ccache_stats_reader") (r "^0.1") (d #t) (k 0)))) (h "0dzkyxnjc56m13c26bb31gxbiyy0fmf5aqn2g1j86vd7hcq90g7l") (f (quote (("nightly" "ccache_stats_reader/nightly") ("default"))))))

