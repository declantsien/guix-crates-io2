(define-module (crates-io cc ac ccache_stats_reader) #:use-module (crates-io))

(define-public crate-ccache_stats_reader-0.1.0 (c (n "ccache_stats_reader") (v "0.1.0") (d (list (d (n "chrono") (r "> 0.2.23") (d #t) (k 0)))) (h "1v3z347w0f7bncd8971xrbi25mzvax06h9hb2z4gy3fsjk56faqz") (f (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

(define-public crate-ccache_stats_reader-0.1.1 (c (n "ccache_stats_reader") (v "0.1.1") (d (list (d (n "chrono") (r ">= 0.4.0") (d #t) (k 0)))) (h "087r4djx86qjsg74562mcrb7yf35d4yv2km07gfnqqf26vqf2qx8") (f (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

(define-public crate-ccache_stats_reader-0.1.2 (c (n "ccache_stats_reader") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "1qsk5dg98ck9s6kydd802d0j50sk7i4xp81jzx9n0acwj3rafjhi") (f (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

