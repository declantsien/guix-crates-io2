(define-module (crates-io cc ge ccgeom) #:use-module (crates-io))

(define-public crate-ccgeom-0.1.0 (c (n "ccgeom") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "vecmat") (r "^0.7") (d #t) (k 0)))) (h "0qdniljpvyfr3nsg2g0d0ab2rjf7g37psxssqhga7x7zcjlc71fj")))

