(define-module (crates-io cc #{11}# cc1101) #:use-module (crates-io))

(define-public crate-cc1101-0.0.1 (c (n "cc1101") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "01wqpc3hcqhmkx9bhaf7a4gvhq82a6m0s9dgnzx1i99m0gdg3hpd")))

(define-public crate-cc1101-0.0.2 (c (n "cc1101") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)))) (h "10n2cydff4yjn6nvrndaawclill8aj57pz0c4lvq98ll7sz9mwam")))

(define-public crate-cc1101-0.0.3 (c (n "cc1101") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)))) (h "1q6vsii1m5jg2786948yvdpbbcvmxy0p18v4ln6hhqc7bcmzb2ha")))

(define-public crate-cc1101-0.1.3 (c (n "cc1101") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0bcfh635jbsxyp6jzjpdkhb4hdhah4n3fffjabcgji3j2yk377va") (f (quote (("std"))))))

