(define-module (crates-io cc #{11}# cc1101-rust) #:use-module (crates-io))

(define-public crate-cc1101-rust-0.1.0 (c (n "cc1101-rust") (v "0.1.0") (d (list (d (n "ioctl-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "1nn88qzqvyqm1d9mw6abamwgn5gcrpnm5gracg1cv6gdjnkhq735")))

(define-public crate-cc1101-rust-0.2.0 (c (n "cc1101-rust") (v "0.2.0") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "0i27rp3065h6qn6q5y00xazv17l4vpqyn38vgfc168fpa8fx0lic")))

(define-public crate-cc1101-rust-0.2.1 (c (n "cc1101-rust") (v "0.2.1") (d (list (d (n "ioctl-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "1linf8jpzlk0sxhssg0v1zv83ik95w35885lra8jlxg0mw0i0cwi")))

