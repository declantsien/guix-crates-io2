(define-module (crates-io cc wd ccwd) #:use-module (crates-io))

(define-public crate-ccwd-0.1.0 (c (n "ccwd") (v "0.1.0") (h "117rawhblsmgp4hrl13rh0xd399isqsiicmrnl5qb23cp113dfs3")))

(define-public crate-ccwd-0.1.1 (c (n "ccwd") (v "0.1.1") (h "0h5wjy9lwdv987466bl67zils64b9y20vzcrhrx95r5bjf61lr76")))

(define-public crate-ccwd-0.1.2 (c (n "ccwd") (v "0.1.2") (h "1pgzv0axdjcmcbfpmlgzd4lblkfgvy2sh23il02pvsbd470sf8a2")))

