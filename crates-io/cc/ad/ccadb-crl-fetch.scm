(define-module (crates-io cc ad ccadb-crl-fetch) #:use-module (crates-io))

(define-public crate-ccadb-crl-fetch-0.1.0 (c (n "ccadb-crl-fetch") (v "0.1.0") (d (list (d (n "ccadb-csv") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("rustls" "trust-dns"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0idd9aj9syckpsj6xf19dpllgcb6brkasbp12bffgsa49rlcip8y")))

