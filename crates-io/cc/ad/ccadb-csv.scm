(define-module (crates-io cc ad ccadb-csv) #:use-module (crates-io))

(define-public crate-ccadb-csv-0.1.0 (c (n "ccadb-csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cp7s8j56qfsxd8z74w8k2wimifih5962kr5nff89qlcizvpxk5r")))

