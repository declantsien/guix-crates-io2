(define-module (crates-io cc ad ccadb-csv-fetch) #:use-module (crates-io))

(define-public crate-ccadb-csv-fetch-0.1.0 (c (n "ccadb-csv-fetch") (v "0.1.0") (d (list (d (n "ccadb-csv") (r "^0.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rustls") (r "^0.20.8") (d #t) (k 0)) (d (n "rustls-webpki") (r "^0.100.1") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls" "rustls"))) (k 0)))) (h "0rsb7i9p1mp2q0ljwicp5ckjznfb2fd5mhlr33la2w7jzxrgg9gh")))

