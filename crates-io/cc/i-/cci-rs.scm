(define-module (crates-io cc i- cci-rs) #:use-module (crates-io))

(define-public crate-cci-rs-0.1.0 (c (n "cci-rs") (v "0.1.0") (d (list (d (n "md-rs") (r ">=0.1.1, <0.2.0") (d #t) (k 0)) (d (n "ta-common") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "typprice-rs") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "10wfp661h8hl9ig44jpsynsllgdzd1m6yvvamfckwn5y2vaws69r")))

