(define-module (crates-io cc l- ccl-fxhash) #:use-module (crates-io))

(define-public crate-ccl-fxhash-1.0.0 (c (n "ccl-fxhash") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "08bkzivqqa5l19rvhhn56vhv3j7abmv8d4d866p42xm3l4h4lbpy")))

(define-public crate-ccl-fxhash-2.0.0 (c (n "ccl-fxhash") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "hashmap_core") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "1i9613rblykq6gjgvymy5hrzfw9q6538jh8pw9l57kjzkb8a4k7z") (f (quote (("std" "byteorder/std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-ccl-fxhash-3.0.0 (c (n "ccl-fxhash") (v "3.0.0") (d (list (d (n "byteorder") (r "^1.0.0") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "hashmap_core") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "01icf944yryywgrmc8irhmjgi4fn5ixq4hq5xwvci57zr35hxwzf") (f (quote (("std" "byteorder/std") ("hashmap-core" "hashmap_core") ("default" "std"))))))

