(define-module (crates-io cc wc ccwc) #:use-module (crates-io))

(define-public crate-ccwc-0.1.0 (c (n "ccwc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zb76d3i9k5i4pxgjkbsv637ny7mj7fw44i4ry0hyrn6n5fpkc9s")))

