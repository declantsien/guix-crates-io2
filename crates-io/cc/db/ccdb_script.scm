(define-module (crates-io cc db ccdb_script) #:use-module (crates-io))

(define-public crate-ccdb_script-0.1.0 (c (n "ccdb_script") (v "0.1.0") (h "0yvl0978qwz9iixi215yr5zfrwj4f08kmv7hznk9j85cs3y8y9ml")))

(define-public crate-ccdb_script-1.0.0 (c (n "ccdb_script") (v "1.0.0") (h "0viyi2c9pvbsfrf3588aqbq16sqc03b3p7szii7y1r2m4r1vcn9s")))

(define-public crate-ccdb_script-1.1.0 (c (n "ccdb_script") (v "1.1.0") (h "0mmzqk4npvfiy4hzdzdn2fpj2y0h9m6l6dy543m3xh8k1k2qzn67")))

(define-public crate-ccdb_script-1.2.0 (c (n "ccdb_script") (v "1.2.0") (h "0s89zr7gnrl1671jbv9lwr1fz55jhdv6g2hzvra0k8n4n1dcy6kw")))

(define-public crate-ccdb_script-1.3.0 (c (n "ccdb_script") (v "1.3.0") (h "06wf1kap8qfb0s9kwb583z42wisdzsr6gnb0iwqhj9zjv4hfbyqm")))

(define-public crate-ccdb_script-1.3.1 (c (n "ccdb_script") (v "1.3.1") (h "0xpbqjmakwis3zbjbdkg1wph9wxv3v65jvf3im3m2803pjxhklgg") (y #t)))

(define-public crate-ccdb_script-1.3.2 (c (n "ccdb_script") (v "1.3.2") (h "1afychv5b7cd1hrzp4jbc0inzy48yv64r2df8fg35j4wjvn2cpvn")))

(define-public crate-ccdb_script-1.4.0 (c (n "ccdb_script") (v "1.4.0") (h "1y8czi93m37nj7wg52s4vd5v5nqaafg7mv4hm7znwkwd5j7d7bw9")))

