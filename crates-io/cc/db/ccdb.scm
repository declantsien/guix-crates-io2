(define-module (crates-io cc db ccdb) #:use-module (crates-io))

(define-public crate-ccdb-0.1.0 (c (n "ccdb") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^0.1.1") (d #t) (k 0)))) (h "04k7xbmhkvv2pnmjhqgzn51kf0avm1xhqbj37m6j39adw9avx9iq") (l "./src/c/")))

(define-public crate-ccdb-0.2.0 (c (n "ccdb") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^0.1.1") (d #t) (k 0)))) (h "0imvdiwpn0qnywx85kal4nv6h3771qfvdc0gjhhl024315c0b3zc") (l "./src/c/")))

(define-public crate-ccdb-0.3.0 (c (n "ccdb") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^0.1.1") (d #t) (k 0)))) (h "0x2kmgx52wx4xpbj1z779fr885h2mg2ikl4z09j6h8kjg81f6z9n") (l "./src/c/")))

(define-public crate-ccdb-0.4.0 (c (n "ccdb") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^0.1.1") (d #t) (k 0)))) (h "02kyf3f44d8h2jqa7wvh8i42j823pzvj2mvxdglfrk85qxfrsbn2") (l "./src/c/")))

(define-public crate-ccdb-0.5.0 (c (n "ccdb") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "1d55jpq165kgg9lz09kgyj6dh2franmvgl6bg34c4zv5yj2j78q1") (l "./src/c/")))

(define-public crate-ccdb-0.6.0 (c (n "ccdb") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "0ymzjixx82rfya2bb34cs5nvgqhg9l0jd9kbpmyhzki9cl096wpy") (l "./src/c/")))

(define-public crate-ccdb-0.7.0 (c (n "ccdb") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "1px91ya7q51pcf82m2sihns622y5zbcckb2rsp5imcfgr66pbg3n") (l "./src/c/")))

(define-public crate-ccdb-0.8.0 (c (n "ccdb") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "0y20icpzdmb1wnyizggxmhajrl9m7fgxsj56fhhjbwvyzwcd5rpz") (l "./src/c/")))

(define-public crate-ccdb-0.9.0 (c (n "ccdb") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "07s2n5kaam6wkfw0x1l0nw299gqsp9zizs5sz6ga1lrzyi5ar8lm") (l "./src/c/")))

(define-public crate-ccdb-1.0.0 (c (n "ccdb") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "1m1l22smil23jwndfaixmjmfnipg8q9is2cxd55as7izhkhcz3m8") (l "./src/c/")))

(define-public crate-ccdb-1.1.0 (c (n "ccdb") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "0hnjv6jkc7byspn1i4draz5j4zmmjqlvy5c3y8kacgxd7c6szvq6") (l "./src/c/")))

(define-public crate-ccdb-1.1.1 (c (n "ccdb") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "0zs90in7hqfpssj25r6z9v3s2n4ax7l9zi61p0pjra5dmb7cr2dc") (l "./src/c/")))

(define-public crate-ccdb-1.2.0 (c (n "ccdb") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "12rbi0hjnbg7hadkx38i2grhkvhdxsvz50813i7pk4vijpcjqlrh") (l "./src/c/")))

(define-public crate-ccdb-1.3.0 (c (n "ccdb") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)))) (h "1yw2wd1ydz2yd4qfv64fhrmasis02zkpgjvw3djss43wh6zw12kh") (l "./src/c/")))

(define-public crate-ccdb-1.4.0 (c (n "ccdb") (v "1.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.5") (d #t) (k 0)))) (h "1havks3c3f14fz9vk97p0wjgfwkcc8kyi81xs2i0qrs1yb6bch9j") (l "./src/c/")))

(define-public crate-ccdb-1.5.0 (c (n "ccdb") (v "1.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.5") (d #t) (k 0)))) (h "0i1lq4hdnf42iji6z52rxw284nf50zy8r7hfwdwkmk7yd0hg08cc") (l "./src/c/")))

(define-public crate-ccdb-1.6.0 (c (n "ccdb") (v "1.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.5") (d #t) (k 0)))) (h "1h4k1f7m5y91cndcxlcgc0qppbmlm081nfyxljg6r0bn3sz2fxvy") (l "./src/c/")))

(define-public crate-ccdb-1.6.1 (c (n "ccdb") (v "1.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openfile") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.5") (d #t) (k 0)))) (h "0i8frbdij8grxim18k5gb7h42ivp0zzi68hag8cjw107z524lf56") (l "./src/c/")))

