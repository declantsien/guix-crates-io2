(define-module (crates-io cc li ccli) #:use-module (crates-io))

(define-public crate-ccli-0.0.1 (c (n "ccli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1db9qkhxdp8fvn5l4xsj67gfr2bpsl17gbcvx3nizmvc1gwkxnbm")))

