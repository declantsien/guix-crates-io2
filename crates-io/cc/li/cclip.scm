(define-module (crates-io cc li cclip) #:use-module (crates-io))

(define-public crate-cclip-0.1.0 (c (n "cclip") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "copypasta") (r "^0.7") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 0)))) (h "1hj047vyascj6l3cij8p8dks0bimn68l3gdgqm2dcp5inrfggl0w")))

