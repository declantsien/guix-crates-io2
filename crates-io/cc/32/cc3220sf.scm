(define-module (crates-io cc #{32}# cc3220sf) #:use-module (crates-io))

(define-public crate-cc3220sf-0.1.0 (c (n "cc3220sf") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vbqb16dzlywgn7fvydl9yfsdr90n0rzrzxizhx5p31kmdviv6xa") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-cc3220sf-0.2.0 (c (n "cc3220sf") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0m6sj1gmf64qpgj5vdlia9zs53x023g96x8bqprzscx5bdv31rlw") (f (quote (("rt" "cortex-m-rt/device"))))))

