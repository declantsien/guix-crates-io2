(define-module (crates-io cc #{32}# cc3220sf-hal) #:use-module (crates-io))

(define-public crate-cc3220sf-hal-0.2.0 (c (n "cc3220sf-hal") (v "0.2.0") (d (list (d (n "cc3220sf") (r "^0.2.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0ni80416v1mnzcv0h3bqaq9gsz1idwzm9l4ba18fi5yqxkhx2xbb")))

