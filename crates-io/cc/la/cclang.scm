(define-module (crates-io cc la cclang) #:use-module (crates-io))

(define-public crate-cclang-0.1.0 (c (n "cclang") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "gsm") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0awqhs1v97g01lc5mfx1hd0h40y2l3lg7qafhdbmrp8rrvfjxppy")))

(define-public crate-cclang-0.2.0 (c (n "cclang") (v "0.2.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "gsm") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "02wdhqxyn9h850f7yfhnbw2nyc0j9qapfs28k6h1vid31rp68j6b")))

(define-public crate-cclang-0.2.1 (c (n "cclang") (v "0.2.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "gsm") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0xdfy91myq4990wglq1f24cvxps2arqrirl8j5cz29m1caxsl600")))

(define-public crate-cclang-0.3.0 (c (n "cclang") (v "0.3.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bs58") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "gsm") (r "^1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0xqdsc43qz5fc8pps9xr07l070cqq7y177078lg9m6dc22qmspjv")))

(define-public crate-cclang-0.4.0 (c (n "cclang") (v "0.4.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bs58") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "gsm") (r "^1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1khnhbq7xgnh3gwyly996428r35y3cb2m8282738nyq0j050vdzw")))

