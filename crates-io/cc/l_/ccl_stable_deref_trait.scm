(define-module (crates-io cc l_ ccl_stable_deref_trait) #:use-module (crates-io))

(define-public crate-ccl_stable_deref_trait-1.0.0 (c (n "ccl_stable_deref_trait") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "11cryafyqv0anvd9j1xm38q5xfvz7da1h7mz9zl9ll2gddqj41q2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-ccl_stable_deref_trait-1.0.1 (c (n "ccl_stable_deref_trait") (v "1.0.1") (d (list (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "15mf597502khjjp3178v6jz6ciy7bf27yq13y70276028i6201r3") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-ccl_stable_deref_trait-1.0.2 (c (n "ccl_stable_deref_trait") (v "1.0.2") (d (list (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)))) (h "0z1716mb10hcy2z6yvbzr8m9wfm5qzgw8dakqwfwba5fvjk0cx13") (f (quote (("std") ("default" "std") ("alloc"))))))

