(define-module (crates-io cc l_ ccl_owning_ref) #:use-module (crates-io))

(define-public crate-ccl_owning_ref-0.4.0 (c (n "ccl_owning_ref") (v "0.4.0") (d (list (d (n "ccl_stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "1qyfqmay9ary54bwi5mqxmpl6cq8j753qfa58hjizbng7kddkwh0")))

(define-public crate-ccl_owning_ref-0.4.1 (c (n "ccl_owning_ref") (v "0.4.1") (d (list (d (n "ccl_stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "0y4v4ff87alblyqxvy0hpjyqz7305cvmiw53sdxxrj9x816h6phm")))

(define-public crate-ccl_owning_ref-0.4.2 (c (n "ccl_owning_ref") (v "0.4.2") (d (list (d (n "ccl_stable_deref_trait") (r "^1.0.2") (d #t) (k 0)))) (h "13zfmxbp3bc18a0bwlvnqba553dsvkjc4x86w37szb6839m1gbrb")))

