(define-module (crates-io z4 -t z4-types) #:use-module (crates-io))

(define-public crate-z4-types-0.0.1 (c (n "z4-types") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y23qign2l2gigcjlx5m630y2l7w0lbgsaz11ww0vcwi34cm2mkw")))

