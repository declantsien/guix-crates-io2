(define-module (crates-io cp c_ cpc_logger) #:use-module (crates-io))

(define-public crate-cpc_logger-0.0.1 (c (n "cpc_logger") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1ihv3gxwxqm9l4xgp0fgi1xilqs4nynmln7q97r077z3d1m1wzsh")))

(define-public crate-cpc_logger-0.0.2 (c (n "cpc_logger") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "job_scheduler_ng") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "00xk4akrx7s29xhmaldpaay8hm0jw7hwa7an5f0vxpdby2yaiiad")))

