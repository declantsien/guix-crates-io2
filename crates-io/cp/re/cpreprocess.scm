(define-module (crates-io cp re cpreprocess) #:use-module (crates-io))

(define-public crate-cpreprocess-1.0.0 (c (n "cpreprocess") (v "1.0.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("v4"))) (d #t) (k 0)))) (h "12yxirjw3j8zqsb0ybkpdmz2svqa4279fjnrdqmzw6izh3br54n7")))

(define-public crate-cpreprocess-1.0.1 (c (n "cpreprocess") (v "1.0.1") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hhq783zs1i6gza0imns3kl778h8hagkvm0algw1rrbljb95xqza")))

(define-public crate-cpreprocess-1.0.2 (c (n "cpreprocess") (v "1.0.2") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-faithful-display") (r "^0") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z4gnzqzw56wvvvxz46wbjj5r7cla1pivpgqjvvapxgqd1rfyag5") (f (quote (("nightly" "proc-macro-faithful-display"))))))

