(define-module (crates-io cp pm cppm-generator) #:use-module (crates-io))

(define-public crate-cppm-generator-0.2.0 (c (n "cppm-generator") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "average") (r "^0.13.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zyw4m3xa7fgadjcncbz8aqyvjywi33305qiivx4z3rx2rlv3cyb")))

