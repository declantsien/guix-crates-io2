(define-module (crates-io cp pm cppmm-build) #:use-module (crates-io))

(define-public crate-cppmm-build-0.1.0 (c (n "cppmm-build") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ylx730w33k29w82qv670rswv8bcw7i7brdva86vqmphkmhnl7li")))

(define-public crate-cppmm-build-0.2.0 (c (n "cppmm-build") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1kp22s4wx0das80hyid1bygl2gqdxw5fpwryihhcbvmd7wbpvar8")))

(define-public crate-cppmm-build-0.3.0 (c (n "cppmm-build") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1cvyx41kfnmc8w2az3r6y313gpqdwr82m41zsfsg8idmhc9qzs17")))

