(define-module (crates-io cp lx cplx) #:use-module (crates-io))

(define-public crate-cplx-0.0.1 (c (n "cplx") (v "0.0.1") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "17mny9qdhd284iznp2xd51a2pyibhg02g40xawjsidfqi3frkfkd")))

(define-public crate-cplx-0.0.2 (c (n "cplx") (v "0.0.2") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "16lr7idgp2x4fyv6rakaryl6rrdrdij44wjy37nvgwc697sakl7l")))

(define-public crate-cplx-0.0.3 (c (n "cplx") (v "0.0.3") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0f002kwzlgzacfm495pdyyxhw2nl2w6wmjalzknyimp8jaiic6vm")))

(define-public crate-cplx-0.1.0 (c (n "cplx") (v "0.1.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "^1.2") (f (quote ("no_std"))) (d #t) (k 0)))) (h "19c5r0c070j9ggr670bs83dkwihqlbrhb5mn9y79ppnmgk9gv8pn")))

(define-public crate-cplx-0.2.0 (c (n "cplx") (v "0.2.0") (d (list (d (n "typenum") (r "^1.2") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1gd6gj7a6qmfwmdr43ay9y4si5y70jdg2nr1g6xc7qyq3j3krggx")))

(define-public crate-cplx-0.2.1 (c (n "cplx") (v "0.2.1") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1xhfkqwpkhy13p71ajng1cgfcyqd4bs2bg5scblh42a7iasj5n19")))

(define-public crate-cplx-0.3.0 (c (n "cplx") (v "0.3.0") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "106nafc3fj82bjxwx34qxpm32hf06w6jb8lczw9g8fbjqikr6q31")))

(define-public crate-cplx-0.3.1 (c (n "cplx") (v "0.3.1") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0yg1n03x8ikl43w7mh6czamn1v5lnkr25np6xmvhjiwsb3qqlyy4")))

(define-public crate-cplx-0.3.2 (c (n "cplx") (v "0.3.2") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "16s3ziqqslhnk7agsi1136sbva8nmqd86vzpwija8axiv7ddv6bc")))

(define-public crate-cplx-0.3.3 (c (n "cplx") (v "0.3.3") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0xg8lfqwf425gm67q3j28kfwngcjvc2fcp3k1vfn5jd7bzkwdzv7")))

