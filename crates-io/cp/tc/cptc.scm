(define-module (crates-io cp tc cptc) #:use-module (crates-io))

(define-public crate-cptc-0.1.0 (c (n "cptc") (v "0.1.0") (d (list (d (n "arboard") (r "^3.3.0") (f (quote ("wayland-data-control"))) (d #t) (k 0)))) (h "1a339pw3vhz09hyzbq5093c2vx3m5km21khalsaypgsr8rp8bmf4")))

(define-public crate-cptc-0.1.1 (c (n "cptc") (v "0.1.1") (d (list (d (n "arboard") (r "^3.3.0") (f (quote ("wayland-data-control"))) (d #t) (k 0)))) (h "0zax6nij61fzgai6nzb5szjkydcqplgrvdxcskk0fh57yg51fix7")))

(define-public crate-cptc-0.1.2 (c (n "cptc") (v "0.1.2") (d (list (d (n "arboard") (r "^3.3.0") (f (quote ("wayland-data-control"))) (d #t) (k 0)))) (h "06mg9z8vdpv932ydnfwxbzvv62gln6540lxj1k0zpgksb2fksd14")))

(define-public crate-cptc-0.1.3 (c (n "cptc") (v "0.1.3") (d (list (d (n "arboard") (r "^3.3.0") (f (quote ("wayland-data-control"))) (d #t) (k 0)))) (h "1971756bflgn11v532j1qb0kghg6a7kzlbibiyp6sdcrzhy4bifg")))

(define-public crate-cptc-0.1.4 (c (n "cptc") (v "0.1.4") (d (list (d (n "arboard") (r "^3.3.0") (f (quote ("wayland-data-control"))) (d #t) (k 0)))) (h "1plhcc8dp54l67nhf3bh2mm5gbvy9dwcmjvxm1j7dxpmavmdq3k6")))

