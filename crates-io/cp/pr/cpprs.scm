(define-module (crates-io cp pr cpprs) #:use-module (crates-io))

(define-public crate-cpprs-0.1.0 (c (n "cpprs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0402ljs0cdl8223w88c1bpk87w9j6xbiychfl0g8rbqmiwp36k15")))

