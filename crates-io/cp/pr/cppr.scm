(define-module (crates-io cp pr cppr) #:use-module (crates-io))

(define-public crate-cppr-0.1.0 (c (n "cppr") (v "0.1.0") (h "07acjqnvp2vlvmay9dhhk3cj9vdlwc9rgldjm4z8zqy88j64qwpn")))

(define-public crate-cppr-0.1.1 (c (n "cppr") (v "0.1.1") (h "1wifjrf5qrlzpiaj0il6745x1l1m4kd5r4nqrg7gvi4r938dwhid")))

(define-public crate-cppr-0.1.2 (c (n "cppr") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0wgz6r3q03nzl3bdwvzbwns9ar0ivmkz1cy1rf9v45g8kzhrfiig")))

