(define-module (crates-io cp t- cpt-hook) #:use-module (crates-io))

(define-public crate-cpt-hook-0.0.1 (c (n "cpt-hook") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "execute") (r "^0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1gf1ys2sjpdv24i37xz2hjy50bgk9r62wxgw80mk4lqq0mdnm30v")))

(define-public crate-cpt-hook-0.0.2 (c (n "cpt-hook") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "execute") (r "^0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0p792xs924sxz7v3n62l54zkg9yan0gv9cd436hcvy1yq4w31f48")))

