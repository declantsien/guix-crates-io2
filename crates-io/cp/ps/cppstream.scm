(define-module (crates-io cp ps cppstream) #:use-module (crates-io))

(define-public crate-cppStream-0.0.1 (c (n "cppStream") (v "0.0.1") (h "0rybl04c287hhi1nr2r64gx1nvhckwrd7nn5q6bfd8gds6xxi43j") (y #t)))

(define-public crate-cppStream-0.0.2 (c (n "cppStream") (v "0.0.2") (h "0c949iqghda2af43klandzqwlyjydbpx94jc1c1frcwh96c25vss")))

(define-public crate-cppStream-0.0.3 (c (n "cppStream") (v "0.0.3") (h "0x099iwgdnjgz6125im298409nb7cja9py4rf8ir664yh6yzb7gp")))

(define-public crate-cppStream-0.0.4 (c (n "cppStream") (v "0.0.4") (h "05hm8r32brw5n34v48nvxqlsirmypzsfmabfky56mhw9w90jbhsy")))

(define-public crate-cppStream-0.0.5 (c (n "cppStream") (v "0.0.5") (h "15zrr5v2qbnk3y1jvjxssmw8f5zbvkk28b5fl9zns189wgmyn6sf")))

(define-public crate-cppStream-0.1.0 (c (n "cppStream") (v "0.1.0") (h "18d87x38lq08nf5fgyj6y3vy12zdbmr8ii8j9h5rdcwbldf16sr0")))

(define-public crate-cppStream-0.1.1 (c (n "cppStream") (v "0.1.1") (h "0fr0gyjnf1pbz12s93415zk07rpcmvh03zhfby3brcc0cv6dlm12")))

