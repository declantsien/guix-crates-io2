(define-module (crates-io cp f_ cpf_util) #:use-module (crates-io))

(define-public crate-cpf_util-0.1.0 (c (n "cpf_util") (v "0.1.0") (h "1cvf9z7g98gsc7792wf0a704r9l3r79nmjjhdyxs0vnzxxfw89sf")))

(define-public crate-cpf_util-0.1.1 (c (n "cpf_util") (v "0.1.1") (h "1kf4z1gs9kh452nhhvp628zr8rpdhhfk1w7al5rsrhp7r3vyhqxw")))

