(define-module (crates-io cp f_ cpf_cnpj) #:use-module (crates-io))

(define-public crate-cpf_cnpj-0.1.0 (c (n "cpf_cnpj") (v "0.1.0") (h "14zga99lb7kp6z7i04h2fyqkrk129jh6h6zsxl64ms6n9i20qyfx")))

(define-public crate-cpf_cnpj-0.1.1 (c (n "cpf_cnpj") (v "0.1.1") (h "1ydb155cdksp0b1ads95rj0vka5vrb95q90jvnc7yn4wiwrm2kk1")))

(define-public crate-cpf_cnpj-0.2.0 (c (n "cpf_cnpj") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "023n4sbl75cx33s4my7gl542y9dr45jnr0dan49bpg8kx0yzl3b3")))

(define-public crate-cpf_cnpj-0.2.1 (c (n "cpf_cnpj") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1rmcf377flcrqi1gx3ghr3kmddbfrik760gaicjka45ir6hcr5nl")))

