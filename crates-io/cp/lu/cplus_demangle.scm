(define-module (crates-io cp lu cplus_demangle) #:use-module (crates-io))

(define-public crate-cplus_demangle-0.1.0 (c (n "cplus_demangle") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03j2vvgrz2qfqklsmjrj0q700sqyzmhyfdz4kkrx35vahdd5zclw")))

(define-public crate-cplus_demangle-0.1.1 (c (n "cplus_demangle") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rmx40zqya4mq5ba5xw64msf5599hz4ymyrawbk0p76a0vl3b18s")))

(define-public crate-cplus_demangle-0.1.2 (c (n "cplus_demangle") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04gx9d919w1i4khvzj04929kvlbfiz429m2gl6y2i7gcqg1chi3x")))

