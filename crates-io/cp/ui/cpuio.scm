(define-module (crates-io cp ui cpuio) #:use-module (crates-io))

(define-public crate-cpuio-0.1.0 (c (n "cpuio") (v "0.1.0") (h "0cznf1bizm3s3vxpah6bawy6pckqjbxz10cjvk02lgdxgsgab4vh")))

(define-public crate-cpuio-0.2.0 (c (n "cpuio") (v "0.2.0") (h "1ald0q8745vcr8qa4rxcn9613xjc8jn0x77ph8xwynpwrh4f7f12")))

(define-public crate-cpuio-0.3.0 (c (n "cpuio") (v "0.3.0") (h "19wam4sgwnkns63mqmm7hnv92ml0vr3p95k5z90j3486zd752cfm")))

