(define-module (crates-io cp ui cpuid) #:use-module (crates-io))

(define-public crate-cpuid-0.0.1 (c (n "cpuid") (v "0.0.1") (h "0b8qhi7cqcbjla2wav0dqlq1cb1yx3bsj81vhixaj1gij5fpn8ps")))

(define-public crate-cpuid-0.0.2 (c (n "cpuid") (v "0.0.2") (h "0m02fy8j0hp3sb9v63vjmm4rid09d0963harmbhlic6xw9ww8x7f")))

(define-public crate-cpuid-0.0.3 (c (n "cpuid") (v "0.0.3") (h "1di75z9vw12pqmwivfmzm12kdiq4vhx9g86ahv1prp2axgyc9ci8")))

(define-public crate-cpuid-0.0.4 (c (n "cpuid") (v "0.0.4") (h "1i65i5cq49qix77jwj42w0rycdjfa0435a5a4fsk543r9fbawp7n")))

(define-public crate-cpuid-0.0.5 (c (n "cpuid") (v "0.0.5") (h "0q68r9lxqlbiq1idq70f3qxl86rcjanw2dxfxxc5jqsmn7m9p4n0")))

(define-public crate-cpuid-0.0.6 (c (n "cpuid") (v "0.0.6") (h "1147zsi6l90rbbar58lqnn5qm0fw33fw9wfm8s1f0hyzxjnlkqsw")))

(define-public crate-cpuid-0.0.7 (c (n "cpuid") (v "0.0.7") (h "0mdizczdzjfp7b90i8y74za6p1x261i90c8nc72bxa8vji77cqg8")))

(define-public crate-cpuid-0.0.8 (c (n "cpuid") (v "0.0.8") (h "0g3gnr7l9fspkykg7vc15yqpm24c63c62zwrxifr8zypx4ncdkhq")))

(define-public crate-cpuid-0.0.9 (c (n "cpuid") (v "0.0.9") (h "0pq5xb4l8mc9q95ym0pf88zd3qm6vl7r4wp97j1qvq5n4j654y28")))

(define-public crate-cpuid-0.1.0 (c (n "cpuid") (v "0.1.0") (d (list (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1hz2fvixbbmmyarfwf4vp8y5arbpi8d855xhslsxxf0xy66b9m6c")))

(define-public crate-cpuid-0.1.1 (c (n "cpuid") (v "0.1.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1afv7gyvj5qv2lp0jzmn25fmcs6hmb27305vpz56ikls45av1i50") (f (quote (("unstable"))))))

