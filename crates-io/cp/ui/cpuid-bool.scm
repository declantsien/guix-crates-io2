(define-module (crates-io cp ui cpuid-bool) #:use-module (crates-io))

(define-public crate-cpuid-bool-0.0.0 (c (n "cpuid-bool") (v "0.0.0") (h "0g4pl47b3jygqwfi1n7sg43d0lknps430rlvf6p6srk44rm4g6ch")))

(define-public crate-cpuid-bool-0.1.0 (c (n "cpuid-bool") (v "0.1.0") (h "1r3v22cxly1shvw8qi0153708kggdqvh8jp0g82wbxi06d1mqdvd")))

(define-public crate-cpuid-bool-0.1.1 (c (n "cpuid-bool") (v "0.1.1") (h "10r1mvbacgn5qq81zjyxdsgasclirksbdlaif330vaq10g166rzc")))

(define-public crate-cpuid-bool-0.1.2 (c (n "cpuid-bool") (v "0.1.2") (h "0d16n378jl0n2dslspqxgsiw9frmjirdszhj5gfccgd0548wmswa")))

(define-public crate-cpuid-bool-0.2.0 (c (n "cpuid-bool") (v "0.2.0") (h "1fpzag3g655p1lr08jgf5n89snjc2ycqx30mm0w3irc9fc3mvcnw")))

(define-public crate-cpuid-bool-0.99.99 (c (n "cpuid-bool") (v "0.99.99") (h "0y7dkr70ahhqvzgfkf28m5q5lq2w449yj3krf85yliw5habd9bdn")))

