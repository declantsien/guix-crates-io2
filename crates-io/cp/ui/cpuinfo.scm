(define-module (crates-io cp ui cpuinfo) #:use-module (crates-io))

(define-public crate-cpuinfo-0.1.0 (c (n "cpuinfo") (v "0.1.0") (h "1m9cb71rzgv146xnkg25w106rsy87y3y5jxhcyii4vy4j1z4pgvd")))

(define-public crate-cpuinfo-0.1.1 (c (n "cpuinfo") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "101cqa9dkxkp9raqz6kp6sfqn3nnwkhfil728n9qp9vyq9q8kdlx") (f (quote (("default"))))))

