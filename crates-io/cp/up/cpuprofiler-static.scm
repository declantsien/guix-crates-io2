(define-module (crates-io cp up cpuprofiler-static) #:use-module (crates-io))

(define-public crate-cpuprofiler-static-0.1.0 (c (n "cpuprofiler-static") (v "0.1.0") (d (list (d (n "cpuprofiler") (r "^0.0.3") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "0dclmricbbmmbdpffaahm8k5nfcjgpj4kl6hcs3bvgbr1ic18wh1") (f (quote (("unwind") ("gperftools") ("default"))))))

