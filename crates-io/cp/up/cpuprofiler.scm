(define-module (crates-io cp up cpuprofiler) #:use-module (crates-io))

(define-public crate-cpuprofiler-0.0.1 (c (n "cpuprofiler") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1hdvcbrkbvjggq4w9sl41yzzhq75svk2nzipll3f6rn6hihmclgp")))

(define-public crate-cpuprofiler-0.0.2 (c (n "cpuprofiler") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0120828c25zlh490nfyiwslh446kkcfja07zls8vw83558gi4ldk")))

(define-public crate-cpuprofiler-0.0.3 (c (n "cpuprofiler") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0dbh3lf7ihf74bw53wjjqnsmq06arjbqv8fp6ab4a8b8pdv7kw1k")))

(define-public crate-cpuprofiler-0.0.4 (c (n "cpuprofiler") (v "0.0.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0az588yyl9r13w4k7xfdh5ckfaq52fwpjry2q2hblazxpjflgy23")))

