(define-module (crates-io cp u- cpu-stress-test) #:use-module (crates-io))

(define-public crate-cpu-stress-test-1.0.0 (c (n "cpu-stress-test") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "103jffqh7rvvjv19yqi0ql1gj4w2xlbm33qziwxy3irhz8dskc11")))

(define-public crate-cpu-stress-test-2.0.0 (c (n "cpu-stress-test") (v "2.0.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1zmfliclk0fdpqdn7qrgp1q7sn33z3slgm4zivd04r7clb4k0hqd")))

(define-public crate-cpu-stress-test-2.0.1 (c (n "cpu-stress-test") (v "2.0.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0s6mf1lx2xfvddzvrzc6bv2kxpcrf8y2rbrv4vvpndn1amyzpnxs")))

(define-public crate-cpu-stress-test-2.1.0 (c (n "cpu-stress-test") (v "2.1.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0qc6jn9213g8vzlvmdzwbd52qvxjsvn8vnvjqjqvlbg2d0aym27p")))

