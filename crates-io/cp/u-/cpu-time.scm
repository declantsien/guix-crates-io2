(define-module (crates-io cp u- cpu-time) #:use-module (crates-io))

(define-public crate-cpu-time-0.1.0 (c (n "cpu-time") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09k4jnybzhq0wj08kbf872i3fx89qi83m96n8nj79lpd7sb854dd")))

(define-public crate-cpu-time-1.0.0 (c (n "cpu-time") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vbxidyahd3vc91qz1ny9q5h10qqg1nbi1ah0z9zmqcgcskr7qz9")))

