(define-module (crates-io cp u- cpu-cli-controller) #:use-module (crates-io))

(define-public crate-cpu-cli-controller-0.2.0 (c (n "cpu-cli-controller") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1sdhm9ipliz4zkny3kr32v22npwk2jl5ddazm8bwfq4pbcw7sp10")))

(define-public crate-cpu-cli-controller-0.3.0 (c (n "cpu-cli-controller") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0nafqg5svgkii3v5wrpsbkd025nkj1hdbdpp2x71r7fv0vhb8kz3")))

