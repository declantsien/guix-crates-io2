(define-module (crates-io cp u- cpu-freq) #:use-module (crates-io))

(define-public crate-cpu-freq-0.0.1 (c (n "cpu-freq") (v "0.0.1") (h "1alwlf381ss65n4v9n4lnf17yxv7yizq3rrz4igvx9xrmsw5zzs5")))

(define-public crate-cpu-freq-0.0.2 (c (n "cpu-freq") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.10") (d #t) (k 0)))) (h "1iiry0f3iycq8v55skswrmwwyja1wz9hqia3w889lsfvz82cmkir")))

