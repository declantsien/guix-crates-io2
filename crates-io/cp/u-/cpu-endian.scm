(define-module (crates-io cp u- cpu-endian) #:use-module (crates-io))

(define-public crate-cpu-endian-0.1.0 (c (n "cpu-endian") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0i9k1jqwhqh5iwpkn5msnj6hyay2xnhlkwz4a9sdzaz42kz697bh")))

(define-public crate-cpu-endian-0.1.1 (c (n "cpu-endian") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1s7fdd9mjc6py69xb417c5p6j13ym9728f7fi9xnra462sblc9si")))

