(define-module (crates-io cp m- cpm-rs) #:use-module (crates-io))

(define-public crate-cpm-rs-0.1.1 (c (n "cpm-rs") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1bphd7l7xpar8w1rrf9ad18j2fmzv3ck534kf1wkbyqryr9935qx")))

(define-public crate-cpm-rs-0.1.2 (c (n "cpm-rs") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hl85d4lh5dvyx41gk6d2dm3vn395dmjw53ya4x6xq86mlyi9xa3")))

(define-public crate-cpm-rs-0.1.3 (c (n "cpm-rs") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0l8cj900x9lf7fz574vrc2zmanqv8msi66dblygwslkq1wr34b5f")))

(define-public crate-cpm-rs-0.1.4 (c (n "cpm-rs") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hi2afx52pdmd57qd3aqrc3w6lgll9lyryr81jg7pr4lvpmcfsva")))

(define-public crate-cpm-rs-0.1.5 (c (n "cpm-rs") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1l3an11m8msw70mmw4839bn8i6nd6wy875nydxkjsmc3bd2axnh1")))

(define-public crate-cpm-rs-0.1.6 (c (n "cpm-rs") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "08v3ya615mcnd2zbkdi6xh752hmkxw9idmypwcw7ccxibdihy1fb")))

