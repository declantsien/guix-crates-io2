(define-module (crates-io cp la cplat) #:use-module (crates-io))

(define-public crate-cplat-0.1.0 (c (n "cplat") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1hms4wrfxjdsz61w64n6lwyax4lnlpc66gfpv6q7hlqfhxvcar0g")))

