(define-module (crates-io cp ca cpcalendars) #:use-module (crates-io))

(define-public crate-cpcalendars-0.1.0 (c (n "cpcalendars") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0cnpi04mnlnv0yppyhbq0a6p4a4g20ccgmgwrhlxawhw4dikilwl")))

(define-public crate-cpcalendars-0.1.1 (c (n "cpcalendars") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1sqspasfrkdnqh65455imj0vglfxa01bphqjb8kq6b55p0d4330c")))

