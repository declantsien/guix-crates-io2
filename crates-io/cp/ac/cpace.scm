(define-module (crates-io cp ac cpace) #:use-module (crates-io))

(define-public crate-cpace-0.1.0 (c (n "cpace") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "hkdf") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "0sd5xjmy4a9pifvfnlyiv1jwkp8bq6d4rris3rm7808qfxwg3aks")))

