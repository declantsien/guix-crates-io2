(define-module (crates-io cp pv cppvtbl) #:use-module (crates-io))

(define-public crate-cppvtbl-0.1.0 (c (n "cppvtbl") (v "0.1.0") (d (list (d (n "cppvtbl-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0yxrxbdr3nwa0il034annm3zm3sh8gabnrg3lyq2kbwrjq5dz87z") (f (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.1.1 (c (n "cppvtbl") (v "0.1.1") (d (list (d (n "cppvtbl-macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "05wapfw6awqqmj2vy5lwi569nf5sz4bza8a8n30nj3ggcysnjyr6") (f (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.1.2 (c (n "cppvtbl") (v "0.1.2") (d (list (d (n "cppvtbl-macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "02xxjgcrndjd57137mly3qavlfzrj17q21qcgdmac1s4vag4z3gc") (f (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.2.0 (c (n "cppvtbl") (v "0.2.0") (d (list (d (n "cppvtbl-macros") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "06f756h21fk4sjsaxdx0nmqnjrc4j1m0f7i19qfd41wq6lzqnk2p") (f (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.2.1 (c (n "cppvtbl") (v "0.2.1") (d (list (d (n "cppvtbl-macros") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "01hrfwb9009gfkhn27g1270v8s084cbd6fj3wsm669nf38fcwc0p") (f (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

