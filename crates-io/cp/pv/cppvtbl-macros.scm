(define-module (crates-io cp pv cppvtbl-macros) #:use-module (crates-io))

(define-public crate-cppvtbl-macros-0.1.0 (c (n "cppvtbl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvsx09bb5j7dw0bwzxvkin9gi9xi5wjn97c8dkism2sjkhfzm4d")))

(define-public crate-cppvtbl-macros-0.1.1 (c (n "cppvtbl-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbxnmazzxa3zasgi15qvmbbrhsfnlik2h6g3f1l4h51sbzbqkc6")))

(define-public crate-cppvtbl-macros-0.2.0 (c (n "cppvtbl-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1facxminkr3xaq6rr0kg21ss80nx4axzsn76bnnbndsv3z4riayi")))

(define-public crate-cppvtbl-macros-0.2.1 (c (n "cppvtbl-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "019g2ywp29585a9170flsyahcb6z8fiyj7m652d25fsp9j9il205")))

