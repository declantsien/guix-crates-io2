(define-module (crates-io cp cl cpclib-macros) #:use-module (crates-io))

(define-public crate-cpclib-macros-0.4.1-alpha (c (n "cpclib-macros") (v "0.4.1-alpha") (d (list (d (n "cpclib-asm") (r "^0.4.1-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1k0595qpn1iyb4s2kch4a8gjvp3xw1x4zvyzv804n06wz6p8fgrh")))

(define-public crate-cpclib-macros-0.4.2-alpha (c (n "cpclib-macros") (v "0.4.2-alpha") (d (list (d (n "cpclib-asm") (r "^0.4.2-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0rq6nl9adyrgnbcdpnmhm9va48jyrflb8akhx6s0pvwzfk5fbn7z")))

(define-public crate-cpclib-macros-0.4.3-alpha (c (n "cpclib-macros") (v "0.4.3-alpha") (d (list (d (n "cpclib-asm") (r "^0.4.3-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)))) (h "0xq74yhy5vd1n7206msy2sl0ynlw1dk30j351iz12yx5msjhdks6")))

(define-public crate-cpclib-macros-0.4.4 (c (n "cpclib-macros") (v "0.4.4") (d (list (d (n "cpclib-asm") (r ">=0.4.4, <0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (d #t) (k 0)))) (h "1dk8kk90kxbaxfkflf1544h95xmakpp1399miqm4rvrv5qk3g04p")))

(define-public crate-cpclib-macros-0.5.0 (c (n "cpclib-macros") (v "0.5.0") (d (list (d (n "cpclib-asm") (r "^0.5.0") (d #t) (k 0)) (d (n "cpclib-common") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1cxwmgdbffnhm2fsk7jlxnbv4yvzdij2zdivyq8ryh1hfwmqia7j")))

(define-public crate-cpclib-macros-0.6.0 (c (n "cpclib-macros") (v "0.6.0") (d (list (d (n "cpclib-asm") (r "^0.6.0") (d #t) (k 0)) (d (n "cpclib-common") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0k0g1z0qnd8rkkqfvq21x43l7d0ab0vawzyn386ycqvnwbrm1if4")))

(define-public crate-cpclib-macros-0.7.0 (c (n "cpclib-macros") (v "0.7.0") (d (list (d (n "cpclib-asm") (r "^0.7.0") (k 0)) (d (n "cpclib-common") (r "^0.7.0") (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "12zj98mvk3n54d9gz4dpgyl0x9xwmk0bn4pgfl2f9lq12ah397ca")))

