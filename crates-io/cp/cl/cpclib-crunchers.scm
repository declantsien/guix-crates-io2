(define-module (crates-io cp cl cpclib-crunchers) #:use-module (crates-io))

(define-public crate-cpclib-crunchers-0.7.0 (c (n "cpclib-crunchers") (v "0.7.0") (d (list (d (n "built") (r "^0.6.1") (f (quote ("chrono"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0y11991mmjz5gc4av6d4kk8qsh3fhfc6wr2qi0i6w015i1l0glnw")))

