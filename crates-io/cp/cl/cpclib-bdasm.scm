(define-module (crates-io cp cl cpclib-bdasm) #:use-module (crates-io))

(define-public crate-cpclib-bdasm-0.6.0 (c (n "cpclib-bdasm") (v "0.6.0") (d (list (d (n "built") (r "^0.5.1") (f (quote ("chrono"))) (d #t) (k 1)) (d (n "clap") (r "^3.1.12") (d #t) (k 0)) (d (n "cpclib-asm") (r "^0.6.0") (d #t) (k 0)) (d (n "cpclib-disc") (r "^0.6.0") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "0sz1rmibyy3gxm6nqkqaj7pmz83fwj1aj47s0qxaxmk82yhmvl8l")))

(define-public crate-cpclib-bdasm-0.7.0 (c (n "cpclib-bdasm") (v "0.7.0") (d (list (d (n "built") (r "^0.6.1") (f (quote ("chrono"))) (d #t) (k 1)) (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "cpclib-asm") (r "^0.7.0") (k 0)) (d (n "cpclib-disc") (r "^0.7.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (d #t) (k 0)))) (h "09ic59hxxkzbjlqvxxz5l6xx6dx5kxa7q25dkcc79bs53cgm7k9j")))

