(define-module (crates-io cp ub cpubind-bash-builtin) #:use-module (crates-io))

(define-public crate-cpubind-bash-builtin-0.1.0 (c (n "cpubind-bash-builtin") (v "0.1.0") (d (list (d (n "bash-builtins") (r "^0.4.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)))) (h "0kyf1rdrs2p6szdz2z8lnmyc1pxfc8yvxx569n9zcigwjwab3x1p")))

