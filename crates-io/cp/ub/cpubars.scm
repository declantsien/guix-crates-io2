(define-module (crates-io cp ub cpubars) #:use-module (crates-io))

(define-public crate-cpubars-0.1.0 (c (n "cpubars") (v "0.1.0") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1xq99qssl3an1m6i9ppf6l740p6nxq34rafq2a3jcwdzy90bm4py")))

(define-public crate-cpubars-0.1.1 (c (n "cpubars") (v "0.1.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0h07v5rmi5f7bxmkhjf5vxksfhj8bmkl8r9ygbnhgvvx9ikmf3dy")))

(define-public crate-cpubars-0.2.0 (c (n "cpubars") (v "0.2.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0p2nl7yvmkm5q6mkcz9vrfx5wjzs10cv0zfvnk0hv7c5cyy8ffcn")))

(define-public crate-cpubars-0.2.1 (c (n "cpubars") (v "0.2.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1b345kss2lfh0d9xni1byhpgl4wqa9k0h6l3xlvp9bzyzrz7qnxa")))

(define-public crate-cpubars-0.3.0 (c (n "cpubars") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "12y0xahmvlw1q8vdqmx6cj7hqb2ipp0dn41rr3kac41dd0478skb")))

(define-public crate-cpubars-0.3.1 (c (n "cpubars") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1hppfnj4yag3xh5wlj7aih0qfbp8frzbrbccsyc12fnp338h1z1f")))

(define-public crate-cpubars-0.3.2 (c (n "cpubars") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0cb8x7ls48f4b37pn9a0967x410h0vbjjfic00yrqiy064vckpfp")))

(define-public crate-cpubars-0.4.0 (c (n "cpubars") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0fdlz7jbx5lml1a7kw304ycyq8rkx206ipk8hmp7gslpy3vk6rca")))

(define-public crate-cpubars-0.4.1 (c (n "cpubars") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0ip220blcssmh77bzbxfa6rblk8qg79a5h6sk60g2g2gnfkd9d0m")))

(define-public crate-cpubars-0.4.2 (c (n "cpubars") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0gza73vjpf2x2dkvws9ck5gy0b6iw61mm019lhdddvzd5kjrnvr1")))

(define-public crate-cpubars-0.4.3 (c (n "cpubars") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "086yf84892c7cf5x4ppw50dydgan5gyvl0vqabx3py6zsjgjm0ys")))

(define-public crate-cpubars-0.5.0 (c (n "cpubars") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "psutil") (r "^3.0.1") (d #t) (k 0)))) (h "132193llkw1dcr2xf1w3x8iifjgf5vm28y0y5mk1fxbnk00iqnmg")))

