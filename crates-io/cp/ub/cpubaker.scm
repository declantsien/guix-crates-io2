(define-module (crates-io cp ub cpubaker) #:use-module (crates-io))

(define-public crate-cpubaker-0.1.0 (c (n "cpubaker") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "16dgfwjx2d2y9dr5vzy5d2kd3c4j2idhhysqsx78ij3igd2dsdqb")))

(define-public crate-cpubaker-0.1.1 (c (n "cpubaker") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "1wymb8342nxaycb7chq3z9pic009xyl7z6qc84hyrz7sxb8gavfq")))

