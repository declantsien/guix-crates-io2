(define-module (crates-io cp ub cpuburn) #:use-module (crates-io))

(define-public crate-cpuburn-0.1.0 (c (n "cpuburn") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "1k7v5z0lf4h9rknjhd7nrryn3k9ca0yb37br7ik22yh0wqh2n4m1")))

(define-public crate-cpuburn-0.1.1 (c (n "cpuburn") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "1ckmxj2crzfl8crbmfwamrm4n765jj2mkaldzr54k74ic7wvhag4")))

