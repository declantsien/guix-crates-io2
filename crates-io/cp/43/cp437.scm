(define-module (crates-io cp #{43}# cp437) #:use-module (crates-io))

(define-public crate-cp437-0.1.0 (c (n "cp437") (v "0.1.0") (h "0nmaiq0r3jcbifvyps2bzs34p1mb5cc51ggb51k903fl3j97lpck")))

(define-public crate-cp437-0.1.1 (c (n "cp437") (v "0.1.1") (h "050wf7dcjyxnghgqhbd91m03s3gr07j3xcg08mg8sdls4sp4h6l9")))

