(define-module (crates-io cp #{43}# cp437_constants) #:use-module (crates-io))

(define-public crate-cp437_constants-1.0.0 (c (n "cp437_constants") (v "1.0.0") (h "00sxgc5jvx9n87dpvmd5mwx17csx3bkiq8wqna85gx1vj7sphw74")))

(define-public crate-cp437_constants-1.0.1 (c (n "cp437_constants") (v "1.0.1") (h "02zh4l97xr530nky8qlrg7nd4pl7nq3vs4g9k1qq63p05b6jgk8l")))

(define-public crate-cp437_constants-1.0.2 (c (n "cp437_constants") (v "1.0.2") (h "0lyr0az2ah8xy2r8dnbn0j0aw9ylmif92maw3gmahgyn9s3bpngj")))

