(define-module (crates-io cp di cpdir) #:use-module (crates-io))

(define-public crate-cpdir-0.1.0 (c (n "cpdir") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "mktemp") (r "^0.5") (d #t) (k 2)))) (h "0xmi1ra450czqpcix2rlhrxgksbk2wb9ldnnfg6g40h028911kdh") (r "1.68.2")))

