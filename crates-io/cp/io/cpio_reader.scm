(define-module (crates-io cp io cpio_reader) #:use-module (crates-io))

(define-public crate-cpio_reader-0.1.0 (c (n "cpio_reader") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "18fw8hqnv66csvicrk24clzqzw13rrmx3qz7jk20hnvcv190qhmx")))

(define-public crate-cpio_reader-0.1.1 (c (n "cpio_reader") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0p7vbabpnylwg7mv0q0znfn0sfl68j8mxqdja44p1p3qajbbfpb2")))

