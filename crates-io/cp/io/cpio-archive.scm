(define-module (crates-io cp io cpio-archive) #:use-module (crates-io))

(define-public crate-cpio-archive-0.1.0 (c (n "cpio-archive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.5.0") (d #t) (k 0)))) (h "15021rga2n79bsv4knm22dj94cxbs6zgzd69m2j3lr7371dld2py")))

(define-public crate-cpio-archive-0.2.0 (c (n "cpio-archive") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.6.0") (d #t) (k 0)))) (h "1aqaycvbdlik8azf8v411xnman78ml8218yk2gn763xg8c33504z")))

(define-public crate-cpio-archive-0.3.0 (c (n "cpio-archive") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.7.0") (d #t) (k 0)))) (h "1khjc0llbdqq8l51bh60gmjackk46f8f5kj51x7gvg51gbllqz84")))

(define-public crate-cpio-archive-0.4.0 (c (n "cpio-archive") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.9.0") (d #t) (k 0)))) (h "1h4cdbbnph1idrxn3fks14x4zkknwf38fj0nsgndcccra54gh96h")))

(define-public crate-cpio-archive-0.5.0 (c (n "cpio-archive") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tugger-file-manifest") (r "^0.10.0") (d #t) (k 0)))) (h "1zpsnvfi4q1rbbprjg2c5flxf48bhb24p7g42j474gq9a5p3yba2")))

(define-public crate-cpio-archive-0.6.0 (c (n "cpio-archive") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^1.0") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ngpk541jvv7ihdjynrfl5pliz0czbnzfmhrqz2q4yijm0f439zj")))

(define-public crate-cpio-archive-0.7.0 (c (n "cpio-archive") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1yn8jrz447wflxm756vlqdw9i2y81mzc57ms19r3qhlw8pwl9vyz")))

(define-public crate-cpio-archive-0.8.0 (c (n "cpio-archive") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0h6nhjgz6yxlxpr908vfmaby4p906wjxdia8b8ramhvpd5b24f9c")))

(define-public crate-cpio-archive-0.9.0 (c (n "cpio-archive") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "simple-file-manifest") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hdpnjw16zll757jwipi54k3wwxi1bdpsdknikd84gbdf4yi7mb3")))

