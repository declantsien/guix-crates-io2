(define-module (crates-io cp io cpio) #:use-module (crates-io))

(define-public crate-cpio-0.1.0 (c (n "cpio") (v "0.1.0") (h "1wazn6y1wbq3qjdrwlhc88n04zgprkypslvribs4991nd0x78m99")))

(define-public crate-cpio-0.2.0 (c (n "cpio") (v "0.2.0") (h "0hn2rph6c4l84r8hhhbazk6i2r3pbh90dddi0l55np5vl76pf0gh")))

(define-public crate-cpio-0.2.1 (c (n "cpio") (v "0.2.1") (h "1a326qbhzk322b1vsr373v682h1alwil7sdxynawr98ds80fglgb")))

(define-public crate-cpio-0.2.2 (c (n "cpio") (v "0.2.2") (h "1lv23p0v2f7kw1qqsw2wzpq6dbk46kaprjv2fs1v9vs38py7rrr7")))

(define-public crate-cpio-0.3.0 (c (n "cpio") (v "0.3.0") (h "134h2m2x34y01dg54psic05k3pnnhp41wmjkmw1619fcwj1y1wb0")))

(define-public crate-cpio-0.4.0 (c (n "cpio") (v "0.4.0") (h "19ilvk47k07wnk4cbvr3bzgqw62pf08kcqa694q69iwhfgnavqw0")))

