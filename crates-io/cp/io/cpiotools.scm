(define-module (crates-io cp io cpiotools) #:use-module (crates-io))

(define-public crate-cpiotools-0.1.0 (c (n "cpiotools") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cpio") (r "^0.2.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "141gsadgrvf790d592bjn7281p7ldn26v9wrsbb6mdr2lpvwxrxw")))

