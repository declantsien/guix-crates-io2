(define-module (crates-io cp _r cp_r) #:use-module (crates-io))

(define-public crate-cp_r-0.1.0 (c (n "cp_r") (v "0.1.0") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1zx9m9y4xpa9zrv29jf9j9dlsdva9irjsq5wqcqkhc3p9rzva3fy")))

(define-public crate-cp_r-0.1.1 (c (n "cp_r") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "00mb0v6k2p6m042zg5cm68rzr5i64c17cq7ikcs7z7abmqg6fvs8")))

(define-public crate-cp_r-0.2.0 (c (n "cp_r") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0vzi62fd0idwbwsmykjxls3v7sw611w6k1l93ic48yq6fqmpkl13")))

(define-public crate-cp_r-0.3.0 (c (n "cp_r") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1f67vhw12anj7ggb5khdlr2w9rq2laviwz3kshpf7p9371944g7h")))

(define-public crate-cp_r-0.3.1 (c (n "cp_r") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "18k2gv196gfx6aw6j3idkf72mc61k8nj62qy3pq3l1cxjavmksx8")))

(define-public crate-cp_r-0.4.0 (c (n "cp_r") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "18jyd95wh1bdpx45pbxi2qr1lk5cgk9g28spbdcfddjqs98drawz")))

(define-public crate-cp_r-0.5.0 (c (n "cp_r") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1i5h4g6a0f9zi1fx30l8dz0qfim13mrn6l2p5kb6iapj0bgs1dvs")))

(define-public crate-cp_r-0.5.1 (c (n "cp_r") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "12p8ccdikz7a29645xsbfxz1j5bjhd35f3wmg4qjs78nzrg307gj")))

