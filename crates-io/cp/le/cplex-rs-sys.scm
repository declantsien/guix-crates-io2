(define-module (crates-io cp le cplex-rs-sys) #:use-module (crates-io))

(define-public crate-cplex-rs-sys-0.1.0 (c (n "cplex-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "12m578mvqq16blsypxkfppf9d84ba1yj2xwqysl26ryw8jx5bz5c") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.1 (c (n "cplex-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "021h24kr5d9vny79vg7z7qxn7fxwzk6zz6d4m8j3fc0vmhidd8iw") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.2 (c (n "cplex-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0n9spdcl5m1g5f4zi3yq3bzxiy92x8vzqcpld7z030xxiim58ypi") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.3 (c (n "cplex-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 1)))) (h "1cnpzb10cb8bizxzd4666872hhbqnd2iapdkc4adan95wbpfnvv0") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.4 (c (n "cplex-rs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 1)))) (h "00dlqcgsfhkyql05c82b87p3ha9m7rla6wl80j1pgc4y6kbllchd") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.5 (c (n "cplex-rs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 1)))) (h "1qjdi2m5fsmhsf9m83vdy4f3801srpa0bqz6a6jgd7ahmg4x3fk7") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.6 (c (n "cplex-rs-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 1)))) (h "0j79pv4maz79pzssr3qy8q362jxag1icrcxr82bvshb89cym6zby") (l "static=cplex")))

(define-public crate-cplex-rs-sys-0.1.7 (c (n "cplex-rs-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 1)))) (h "0k5habrnizsjp84j08nwnms3n1h2y5vr8z3r1nziihmnj9z9fl75") (l "static=cplex")))

