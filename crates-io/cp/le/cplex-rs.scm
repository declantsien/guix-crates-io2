(define-module (crates-io cp le cplex-rs) #:use-module (crates-io))

(define-public crate-cplex-rs-0.1.0 (c (n "cplex-rs") (v "0.1.0") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "097h1gxs34yxzazaj2fb6jb5jxldkplzsfmpmlpavislks152i17")))

(define-public crate-cplex-rs-0.1.1 (c (n "cplex-rs") (v "0.1.1") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hdczp19agf0x2r62k1cbpk7lk7syjzh3sawgr5ydhbglw8hssvi")))

(define-public crate-cplex-rs-0.1.2 (c (n "cplex-rs") (v "0.1.2") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ip6s5jd5cp7c8y88vb7ix5zhrzjw91bnyysc6j5ssyzi59fqqn3")))

(define-public crate-cplex-rs-0.1.3 (c (n "cplex-rs") (v "0.1.3") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14hvvw98a78w1czc8py7y0wvm60pbwi52i32z296c8rn44b60dzn")))

(define-public crate-cplex-rs-0.1.4 (c (n "cplex-rs") (v "0.1.4") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06mck8vfq84957jr43n9q2qmx2v8khgvrrzvsp521b4cnmy5y1np")))

(define-public crate-cplex-rs-0.1.5 (c (n "cplex-rs") (v "0.1.5") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gg0vbwzss3prc7rlvky0gjq75mz6hqlvjii8gsh65nznpsr4php")))

(define-public crate-cplex-rs-0.1.6 (c (n "cplex-rs") (v "0.1.6") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03rizm0pn2s7mcqijs09ycbainlsixqyjzqh05xqmrnhj70r111x")))

(define-public crate-cplex-rs-0.1.7 (c (n "cplex-rs") (v "0.1.7") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cplex-rs-sys")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iy9r1xm049bzy5pafnh6lypn75xmf79nzfqqrhqknlrk2g9wzfq")))

