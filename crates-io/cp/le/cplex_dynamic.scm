(define-module (crates-io cp le cplex_dynamic) #:use-module (crates-io))

(define-public crate-cplex_dynamic-0.1.0 (c (n "cplex_dynamic") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0wdhml8h9kgrqvhh8pkp6brbrdcd1lbfmzaxnlxwf62d438g0pi7")))

(define-public crate-cplex_dynamic-0.1.1 (c (n "cplex_dynamic") (v "0.1.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "16xpqix7iya3ywz3s1c5b64cbnn8ws7c9c2qk3rhachfdvy0w37r")))

