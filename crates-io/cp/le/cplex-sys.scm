(define-module (crates-io cp le cplex-sys) #:use-module (crates-io))

(define-public crate-cplex-sys-0.2.3 (c (n "cplex-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "03vp9xms7grya6k483b88xl0k61477qp7jphx21wgwa6101wnssc")))

(define-public crate-cplex-sys-0.2.4 (c (n "cplex-sys") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "14bs4441a8nvz41qyza4vn37hfyzjsgx86jps9j6qvrydh26bvg2")))

(define-public crate-cplex-sys-0.3.0 (c (n "cplex-sys") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "03hirlri4gw2mxx0glx8bdfrxks6nfrdhk43v4526x845wzxq6zq")))

(define-public crate-cplex-sys-0.4.0 (c (n "cplex-sys") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "1hd8vw8ah5p8z9cflr9dblnmaqyr3yjhg7z5h2jgc20hkl0rxjjv")))

(define-public crate-cplex-sys-0.4.1 (c (n "cplex-sys") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "0k4qd4sb18spp6dqwmhcq0js8wihxx2dddmv2v315sgqwbvmprkn")))

(define-public crate-cplex-sys-0.5.0 (c (n "cplex-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1cnsmnn2f11j7ad81lcyvhvsgpnc33bwn7d4j3192azkhyxy2224")))

(define-public crate-cplex-sys-0.5.1 (c (n "cplex-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "112kki4q38ml2c8jgnw7lz39ccnilqbdg5gsa8nln50nw0sda2f0")))

(define-public crate-cplex-sys-0.5.2 (c (n "cplex-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "137h395k6pcxz6xksn82snklzbbah4y19hf0063yb4pj1mbnjj9n")))

(define-public crate-cplex-sys-0.6.0 (c (n "cplex-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "03lslfirqcqfg8sxgq654sqf8nb2zy7j34z8nlyd29bmiff3kqsh")))

(define-public crate-cplex-sys-0.6.1 (c (n "cplex-sys") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "0fjggp42lfnixnh5ghdlcavkg9kg0mav7gv3ivmc6m1splm5an6w")))

(define-public crate-cplex-sys-0.7.0 (c (n "cplex-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1lsv570r16g2nsd9pvvmal9w3bg7p19jpx1fzdi12fi4kaxra7ji")))

(define-public crate-cplex-sys-0.8.0 (c (n "cplex-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1aw5r9zkfgmwhghniz6anf54w8f5xs8hzzyxfs4907n3qfadhdfr")))

(define-public crate-cplex-sys-0.8.1 (c (n "cplex-sys") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "05ph86n1b2vlbfpa8zr27n4f4acd4vq3f2xp39al8xr5l546x2c3")))

(define-public crate-cplex-sys-0.9.0 (c (n "cplex-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1bvrh5kh9v03f520k3rk8qizq0is98lqb0phahsl28nc1x50x449")))

(define-public crate-cplex-sys-0.9.1 (c (n "cplex-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "127982h79mffa4h23wsr4bn5b3jfsc053i43cqzzvir0pxa7h34v")))

