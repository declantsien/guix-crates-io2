(define-module (crates-io cp #{21}# cp211x_uart) #:use-module (crates-io))

(define-public crate-cp211x_uart-0.1.0 (c (n "cp211x_uart") (v "0.1.0") (d (list (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)))) (h "12i61f0ap6853jg3qppry36aqlx43xsfdxg9z8pcwcaiz5hblf6b")))

(define-public crate-cp211x_uart-0.1.1 (c (n "cp211x_uart") (v "0.1.1") (d (list (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)))) (h "1shb5029lf7rhx18cqaid75gb2vknm5kwng827g2753z2cb1bm8x")))

(define-public crate-cp211x_uart-0.1.2 (c (n "cp211x_uart") (v "0.1.2") (d (list (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)))) (h "19ysn3mxrbnw00zv8sy70pkmfpmc8dnja65n59n3w0hmlznjmasc")))

(define-public crate-cp211x_uart-0.1.3 (c (n "cp211x_uart") (v "0.1.3") (d (list (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)))) (h "12d3gmr5677lym9dvf8dj4laiwy4djv2sqn8pdbzh2vx4k205k2b")))

(define-public crate-cp211x_uart-0.2.0 (c (n "cp211x_uart") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)))) (h "122jhyqsp7lsc36ns8afnc6jxvkbqrk77xsngwp6r0iihhgc7w40") (f (quote (("static" "hid/static") ("default") ("build" "static" "hid/build"))))))

(define-public crate-cp211x_uart-0.2.2 (c (n "cp211x_uart") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)))) (h "0rgsyf7qhfdvsbh2q93mdhynly1haknlw84qvdplvd2dn1mzg7zd")))

