(define-module (crates-io cp r_ cpr_bf) #:use-module (crates-io))

(define-public crate-cpr_bf-0.1.0 (c (n "cpr_bf") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "02b4kv8057vfpf7dcdbyx3y4kii3h6w8idlawnxi59pvy2g215yf")))

(define-public crate-cpr_bf-0.1.1 (c (n "cpr_bf") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1pnhfk96d0shv550b27hj16v19p7kvvafdxgxiii855fw8q041f2")))

(define-public crate-cpr_bf-0.1.2 (c (n "cpr_bf") (v "0.1.2") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0jvdiwqy1praghm87sxxgj90zhf8lvbs9l6l1ir4swvd5drl71y5")))

(define-public crate-cpr_bf-0.1.3 (c (n "cpr_bf") (v "0.1.3") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1c61phvx6p3qhwmjnyf6p1pwx568wss26cf3mrckl3cv85jr0lqw")))

(define-public crate-cpr_bf-0.1.4 (c (n "cpr_bf") (v "0.1.4") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1lsl5qf83bid6azbmhqm7d521s3x3j1wlciyqcp4ld8bj09ijlni")))

(define-public crate-cpr_bf-0.1.5 (c (n "cpr_bf") (v "0.1.5") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1xx6mc4ybs9ds24nkai2bpdxl3xj93gl09hzwhwanlgc5b4dgkgk")))

