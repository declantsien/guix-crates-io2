(define-module (crates-io cp r_ cpr_bfvm) #:use-module (crates-io))

(define-public crate-cpr_bfvm-0.1.2 (c (n "cpr_bfvm") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpr_bf") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std" "release_max_level_info"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (f (quote ("stderr"))) (d #t) (k 0)))) (h "0dasd19dc188zbm9ynm98likrc295pfdhr1xqqsl8wm2jw95akyg")))

(define-public crate-cpr_bfvm-0.1.3 (c (n "cpr_bfvm") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpr_bf") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std" "release_max_level_info"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (f (quote ("stderr"))) (d #t) (k 0)))) (h "0x8vinj5x9a5hckxv3zpadf5wq4jdd98hgv27v6znfakg5r34xpp")))

(define-public crate-cpr_bfvm-0.1.4 (c (n "cpr_bfvm") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpr_bf") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std" "release_max_level_info"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (f (quote ("stderr"))) (d #t) (k 0)))) (h "0zjbynh5my15mlrbrcwbp87ma44wv1nxv14xyyl5arcjdz8jpk3z")))

(define-public crate-cpr_bfvm-0.1.5 (c (n "cpr_bfvm") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpr_bf") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std" "release_max_level_info"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (f (quote ("stderr"))) (d #t) (k 0)))) (h "0zzhzlpmxswri1b2s2a4q65arf953h1c5iwck89na1hyg19h7ybr")))

