(define-module (crates-io cp -r cp-rs) #:use-module (crates-io))

(define-public crate-cp-rs-0.1.0 (c (n "cp-rs") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hxfb0bqanmpziy7r3f5cyp9p2k05wiyf3y1qck2rz5hp9fh5abq")))

(define-public crate-cp-rs-0.2.0 (c (n "cp-rs") (v "0.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "160vdalmsrnz895v87w89z561jjlwc3bkr8n2z18pgw016nz145v")))

(define-public crate-cp-rs-0.2.1 (c (n "cp-rs") (v "0.2.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "085l4awm46asb3gnc0x1zbx6q6hkbq0a7jbx4kgs005rzvk7qzfs")))

(define-public crate-cp-rs-0.2.2 (c (n "cp-rs") (v "0.2.2") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12ib7mplyhiaj55drng67na7rsr3lwmqdakbha6i532z672js84b")))

(define-public crate-cp-rs-0.2.3 (c (n "cp-rs") (v "0.2.3") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qz7byy434hhcxqxcyqggb4y6blhxk3jh4p03j0mvxiizg40qicn")))

