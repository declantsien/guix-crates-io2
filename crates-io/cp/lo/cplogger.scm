(define-module (crates-io cp lo cplogger) #:use-module (crates-io))

(define-public crate-cplogger-0.0.0 (c (n "cplogger") (v "0.0.0") (h "071ap0brx5czb9rhmsc3z1xy5v5h3g3syrlrwf0rq974gqcq4x3d") (y #t)))

(define-public crate-cplogger-0.0.1 (c (n "cplogger") (v "0.0.1") (h "0mv8msa7n9flg1hifgp7s2hjrk90b0zbkpcybbidr470msn44vyi") (y #t)))

(define-public crate-cplogger-0.0.2 (c (n "cplogger") (v "0.0.2") (h "0anxzz74869sfwa6gadx8pgdm2wp9l4w65ckp4y9hq3hwyqiwnf5")))

