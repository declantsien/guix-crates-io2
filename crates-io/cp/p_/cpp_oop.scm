(define-module (crates-io cp p_ cpp_oop) #:use-module (crates-io))

(define-public crate-cpp_oop-0.1.0 (c (n "cpp_oop") (v "0.1.0") (d (list (d (n "cpp_oop_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0n7m445mm2801cszm0b002xfmy541mpimzjm11j24g1a6xzvh9nh")))

(define-public crate-cpp_oop-0.1.1 (c (n "cpp_oop") (v "0.1.1") (d (list (d (n "cpp_oop_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0gyws3nlq2vqhyi32362zzyl8piv024q42g66rys52h90j0y55lb")))

(define-public crate-cpp_oop-0.1.2 (c (n "cpp_oop") (v "0.1.2") (d (list (d (n "cpp_oop_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1rm2s379mms0h20ba4m5hq8q9hf5sh4j01d6968s6yqjiqs2af16")))

(define-public crate-cpp_oop-0.1.3 (c (n "cpp_oop") (v "0.1.3") (d (list (d (n "cpp_oop_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0vj1vqzqxfhnf0yimdd5sdb6rqq3v0adalrlq1k9sw761l4qnqls")))

(define-public crate-cpp_oop-0.1.4 (c (n "cpp_oop") (v "0.1.4") (d (list (d (n "cpp_oop_macros") (r "^0.1.4") (d #t) (k 0)))) (h "0arrp1ldq9rfscqzf5lmsingk0ahn3sxricvk3nrcwlpy3v7afch")))

(define-public crate-cpp_oop-0.1.5 (c (n "cpp_oop") (v "0.1.5") (d (list (d (n "cpp_oop_macros") (r "^0.1.5") (d #t) (k 0)))) (h "1gsjdrrpdiz62jma0bdpswr4p0bbdnl9i048wdzab9gl2kwf4wha")))

(define-public crate-cpp_oop-0.1.6 (c (n "cpp_oop") (v "0.1.6") (d (list (d (n "cpp_oop_macros") (r "^0.1.6") (d #t) (k 0)))) (h "0khwyv5lwafrc2zbji6rx1y1al2kqxz95crih4as4vb5hdk07s71")))

(define-public crate-cpp_oop-0.1.7 (c (n "cpp_oop") (v "0.1.7") (d (list (d (n "cpp_oop_macros") (r "^0.1.7") (d #t) (k 0)))) (h "0xxfz8kz2gdgp4qs46w8fipday8fxlf38ysk8h39jdnbclv1pzk1")))

(define-public crate-cpp_oop-0.1.8 (c (n "cpp_oop") (v "0.1.8") (d (list (d (n "cpp_oop_macros") (r "^0.1.8") (d #t) (k 0)))) (h "0c7ahpjda8a2w0ks14j17pdw6laqhflx4yb01d4z1wh6dc10w37w")))

(define-public crate-cpp_oop-0.1.9 (c (n "cpp_oop") (v "0.1.9") (d (list (d (n "cpp_oop_macros") (r "^0.1.9") (d #t) (k 0)))) (h "1yq2i9cx4l38ni47rq79psq25l60zrnrkbanwxhg6c3qbnhz10l6")))

