(define-module (crates-io cp p_ cpp_rs) #:use-module (crates-io))

(define-public crate-cpp_rs-0.1.0 (c (n "cpp_rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (o #t) (d #t) (k 0)))) (h "1c3b2dwr9kb8ci3saj9j516mys9c6yjzr1klb0lkd45wgiqf027q") (f (quote (("build-binary" "structopt"))))))

