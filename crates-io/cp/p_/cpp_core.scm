(define-module (crates-io cp p_ cpp_core) #:use-module (crates-io))

(define-public crate-cpp_core-0.3.0 (c (n "cpp_core") (v "0.3.0") (h "10klnh9x703wf5g9p9ll2xa4wmf37di5b42bv6gxl8xipskmplgy")))

(define-public crate-cpp_core-0.4.0 (c (n "cpp_core") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ghjib0nklkacrgwnc8z9zg41shswm7f42wrkqjq1v9jlmbfjzyd")))

(define-public crate-cpp_core-0.5.0 (c (n "cpp_core") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "086vsbm95l6wppbpkj7pyn3y95f7yxq828byrdxmm5q6xda7w5j5")))

(define-public crate-cpp_core-0.5.1 (c (n "cpp_core") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12gk7x9rmaka71c8vm49k6rq0jrdximrf3jgkskrfxsw5lbzr973")))

(define-public crate-cpp_core-0.6.0-alpha.1 (c (n "cpp_core") (v "0.6.0-alpha.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zkwf2x22jm9k4ih6l2yw7xb711r12736d54yrwfwxdpgmnqgay8")))

(define-public crate-cpp_core-0.6.0 (c (n "cpp_core") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b947zz19q50k9ghqw5gic2hvn6mv72pxl72mwr8459afjlnpgay")))

