(define-module (crates-io cp p_ cpp_common) #:use-module (crates-io))

(define-public crate-cpp_common-0.2.0 (c (n "cpp_common") (v "0.2.0") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "0mvqsjk4qs9h1g4gnz24h82xd0s3wl0ajjf22km258l6nibwqfl0")))

(define-public crate-cpp_common-0.2.1 (c (n "cpp_common") (v "0.2.1") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1pqjzkcjqgs7fmx7b6qfy2igk4k96hfjvyh26y015szgnj0b1mzc")))

(define-public crate-cpp_common-0.3.0 (c (n "cpp_common") (v "0.3.0") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "079dc2brvpxqs5fn1k6krsc3n757wz2xaxibmwx4b76vr0wfiv3v")))

(define-public crate-cpp_common-0.3.1 (c (n "cpp_common") (v "0.3.1") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "03nyfy4zghrlp8rnwdr4isbnfzlypl6sfz6pha63xmb2awfad8di")))

(define-public crate-cpp_common-0.3.2 (c (n "cpp_common") (v "0.3.2") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "0whcz5mydw54z6l6bmxwpjyn4irib1mfxvjj71ysqcslrd9q19ac")))

(define-public crate-cpp_common-0.4.0 (c (n "cpp_common") (v "0.4.0") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "0a6n4x01q3wz4bp86n4dqyf142l4l9y38vjv5yhgyflllx4r3qvr")))

(define-public crate-cpp_common-0.5.0 (c (n "cpp_common") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fz104fmlz8z5p13dqa596ryydq8nz0fh8w8hfwhklncqzb5x34j")))

(define-public crate-cpp_common-0.5.1 (c (n "cpp_common") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r98bh41fj07ycdbjhmygh4idn847al40bn92lmsixa8y143f5am")))

(define-public crate-cpp_common-0.5.2 (c (n "cpp_common") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c4s7qfanfgv0j339p3nanhmwhgzs0zfxi0883nijdfa913v1gii")))

(define-public crate-cpp_common-0.5.3 (c (n "cpp_common") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02s8a3p4xqix6mgl34kf4dsjfskk2pyqj6sfayp4621v7nrd4mnd")))

(define-public crate-cpp_common-0.5.4 (c (n "cpp_common") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08wrv1cgwf2mx05jfw66fr0lnvyjj4008n48j565zh7zavnpr0kb")))

(define-public crate-cpp_common-0.5.5 (c (n "cpp_common") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gx9xkn6j44cbmpz4rsr6vp89yw6fhdn86lhmq09ahd4zdny4ibi")))

(define-public crate-cpp_common-0.5.6 (c (n "cpp_common") (v "0.5.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10zj9z639f7j1ccycix8k73j0ic77cyznyb7062l50gywllasy6z")))

(define-public crate-cpp_common-0.5.7 (c (n "cpp_common") (v "0.5.7") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18fqhzqz8dsxxk3gsgdyy4y34cdhnzm7v409n9g2ppf4r2win1vn")))

(define-public crate-cpp_common-0.5.8 (c (n "cpp_common") (v "0.5.8") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03avg6xk67dwynxzqhlagsp4hmpx7py1vkp86ql43b5jnxlbklj2")))

(define-public crate-cpp_common-0.5.9 (c (n "cpp_common") (v "0.5.9") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "189fkwbl7ryiy1vrv3pl5pism95w1nycffy164qa2kpdwhr2a6iy")))

