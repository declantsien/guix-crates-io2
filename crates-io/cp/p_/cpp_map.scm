(define-module (crates-io cp p_ cpp_map) #:use-module (crates-io))

(define-public crate-cpp_map-0.0.1 (c (n "cpp_map") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0idq23m21959a3ap28nwb9zr21lxdnsh2g1jlvya2lh4y2xrhagb")))

(define-public crate-cpp_map-0.0.2 (c (n "cpp_map") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a3vjdlsjxal1hdmqq597s5jplal8di6y5aby3kd6rj86kfyz9si")))

(define-public crate-cpp_map-0.0.3 (c (n "cpp_map") (v "0.0.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pi71c20s2l6adv3kbhm1wf0lidpzbq3cfp46dax4khp3sarazy0") (f (quote (("console_debug"))))))

(define-public crate-cpp_map-0.1.0 (c (n "cpp_map") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p3allin5gq3ryj5ghh1jg1yr43hs2aalgjm5wjls6n7fgnsk0fa") (f (quote (("console_debug"))))))

(define-public crate-cpp_map-0.1.1 (c (n "cpp_map") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a89fhs0iknd2ggwx0wcym7b8ny2hkya063icns7hhr52jzaa1mm") (f (quote (("console_debug"))))))

