(define-module (crates-io cp p_ cpp_to_rust_build_tools) #:use-module (crates-io))

(define-public crate-cpp_to_rust_build_tools-0.1.0 (c (n "cpp_to_rust_build_tools") (v "0.1.0") (d (list (d (n "cpp_to_rust_common") (r "^0.0") (d #t) (k 0)))) (h "0nav2avybjg6027mxvgyx9jwjhd47nb90b2iil4x9j9nfrs1rafy")))

(define-public crate-cpp_to_rust_build_tools-0.0.0 (c (n "cpp_to_rust_build_tools") (v "0.0.0") (d (list (d (n "cpp_to_rust_common") (r "^0.0") (d #t) (k 0)))) (h "0ca8n1zn9f0bmkmy5bl6lv5xyx19lw3wrh9nnmkzgxpclizqyr3k")))

(define-public crate-cpp_to_rust_build_tools-0.0.2 (c (n "cpp_to_rust_build_tools") (v "0.0.2") (d (list (d (n "cpp_to_rust_common") (r "^0.0") (d #t) (k 0)))) (h "0s4qpkv5ldxnzs73nx764csg3cy5vzhndk7ambnwlhw3pbvd45zf")))

(define-public crate-cpp_to_rust_build_tools-0.2.0 (c (n "cpp_to_rust_build_tools") (v "0.2.0") (d (list (d (n "cpp_to_rust_common") (r "^0.2.0") (d #t) (k 0)))) (h "0wgnfbl0rs2h8xc1426lhqbjvwhzryjsswccmprgc64qykn8yrbv")))

(define-public crate-cpp_to_rust_build_tools-0.2.1 (c (n "cpp_to_rust_build_tools") (v "0.2.1") (d (list (d (n "cpp_to_rust_common") (r "^0.2.0") (d #t) (k 0)))) (h "0cdpp5v6qz82in0amim6nrwaaczphk486l0g3il886hq85fgr9hx")))

(define-public crate-cpp_to_rust_build_tools-0.2.3 (c (n "cpp_to_rust_build_tools") (v "0.2.3") (d (list (d (n "cpp_to_rust_common") (r "^0.2.3") (d #t) (k 0)))) (h "14njdjdd9zqbj5ij7d3ydp9dmcikhkfs445p0f6gqcg5s3avvc0d")))

