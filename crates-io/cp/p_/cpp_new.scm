(define-module (crates-io cp p_ cpp_new) #:use-module (crates-io))

(define-public crate-cpp_new-0.1.0 (c (n "cpp_new") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cpp_new_simple_cpp_lib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1jl05zp6mbrzvaqzgwjvwvkxkdnhbs5d8g3r5klvddf245g6b0n5")))

