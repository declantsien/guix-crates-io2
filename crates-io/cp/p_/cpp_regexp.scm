(define-module (crates-io cp p_ cpp_regexp) #:use-module (crates-io))

(define-public crate-cpp_regexp-0.1.0 (c (n "cpp_regexp") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)))) (h "0r5mvc16lmwx3p2gq7n1xgvlpmrfkrhx5qqpm0rvckjbsgg3i3la")))

(define-public crate-cpp_regexp-0.2.0 (c (n "cpp_regexp") (v "0.2.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)))) (h "0pvns4wvcpjlk62xf9w4gsc1xgd3q3h9i7pch73nv9anf13gnc7i")))

(define-public crate-cpp_regexp-0.3.0 (c (n "cpp_regexp") (v "0.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1nh20y1f581j9sfyrg4rnz9wq114ll7bg6lk805f2mf2sgab7g3s")))

(define-public crate-cpp_regexp-0.3.1 (c (n "cpp_regexp") (v "0.3.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ghsnacmvwb524hd7xmcphjpf4rrqwpv0j3hxwdkdp0cnqa6g281")))

(define-public crate-cpp_regexp-0.3.2 (c (n "cpp_regexp") (v "0.3.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1myi901ln1x9p7l75yyywhrfsr248z7gdqzvn3wiiffws411czn5")))

(define-public crate-cpp_regexp-0.4.0 (c (n "cpp_regexp") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0kclippv00jiida2213lvnrqrk7wyrcqv4wn10ldnb4xdqcq3f99")))

(define-public crate-cpp_regexp-0.5.0 (c (n "cpp_regexp") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0cpppclhqiwx76d5flxkhrx1vip5akgg2a13hs670lja0xb4rv8f")))

(define-public crate-cpp_regexp-0.6.0 (c (n "cpp_regexp") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ip6ydhn1z2db3yw0w0yp7lkli18shm5sdc9nnqmk2ryxl2z06gp")))

(define-public crate-cpp_regexp-0.6.1 (c (n "cpp_regexp") (v "0.6.1") (d (list (d (n "cxx") (r "^1.0") (f (quote ("c++14"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "19z110lck0piq6bywjcnn3wzhpvwygxm2i9s1w3n57z52iyrn922")))

