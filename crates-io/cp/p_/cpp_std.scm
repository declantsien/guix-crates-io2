(define-module (crates-io cp p_ cpp_std) #:use-module (crates-io))

(define-public crate-cpp_std-0.1.0 (c (n "cpp_std") (v "0.1.0") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "09pg2dan637qa63xp0ar054648gh5jgww0j56v8y1hda2fac3ajm") (f (quote (("ritual_rustdoc"))))))

(define-public crate-cpp_std-0.1.1 (c (n "cpp_std") (v "0.1.1") (d (list (d (n "cpp_core") (r "^0.5.0") (d #t) (k 0)) (d (n "ritual_build") (r "^0.3.0") (d #t) (k 1)))) (h "0fakcrvdcscayapx7lvqkczwywi18rkn90yd68al3gaad7xwssnw") (f (quote (("ritual_rustdoc"))))))

