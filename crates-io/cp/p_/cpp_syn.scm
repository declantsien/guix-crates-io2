(define-module (crates-io cp p_ cpp_syn) #:use-module (crates-io))

(define-public crate-cpp_syn-0.12.0 (c (n "cpp_syn") (v "0.12.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syntex_pos") (r "^0.52") (d #t) (k 2)) (d (n "syntex_syntax") (r "^0.52") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^1.0.1") (d #t) (k 2)))) (h "0kk4y7ad8hajpxladn1y0fk3i9gmk1v0r9hjzs94v05kyndn9kd8") (f (quote (("visit") ("printing" "quote") ("parsing" "unicode-xid" "cpp_synom") ("full") ("fold") ("default" "parsing" "printing") ("aster"))))))

