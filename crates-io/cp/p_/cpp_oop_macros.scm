(define-module (crates-io cp p_ cpp_oop_macros) #:use-module (crates-io))

(define-public crate-cpp_oop_macros-0.1.0 (c (n "cpp_oop_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0ls508qf492fny2wyh9n3cg223mx0npax74nxja6ignan05n4igh")))

(define-public crate-cpp_oop_macros-0.1.1 (c (n "cpp_oop_macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0mgk8z9al8bl2j4pqbri8gh798fwk82bc21bdjsw321npi4dzxph")))

(define-public crate-cpp_oop_macros-0.1.2 (c (n "cpp_oop_macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1gj8nj0rsj05rg56g5798r0kzifrirrld643dsc7syb2dbqzzssm")))

(define-public crate-cpp_oop_macros-0.1.3 (c (n "cpp_oop_macros") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "00ns3rlgmdjwy3lskk2jbphpnj3ywpbaq7zzq9dxdsndaqfcvsn4")))

(define-public crate-cpp_oop_macros-0.1.4 (c (n "cpp_oop_macros") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1n19wrj0fmxh7w4b3gg5x4ay34d55k5gndjq3ya297l8vdw5nf35")))

(define-public crate-cpp_oop_macros-0.1.5 (c (n "cpp_oop_macros") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1qb6swfi7pz7pp0vq8d4ba8wgcg11gxn9yb5017k59cgj2qf69mf")))

(define-public crate-cpp_oop_macros-0.1.6 (c (n "cpp_oop_macros") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhk21r7q8djb8khd8ai10c3ij14hc2kn1526a3xwj2zdh60rcmb")))

(define-public crate-cpp_oop_macros-0.1.7 (c (n "cpp_oop_macros") (v "0.1.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfgg19ph52g6ad1hrrgvkdx3ml0c4hfripa2a6rgiz68lh0m58h")))

(define-public crate-cpp_oop_macros-0.1.8 (c (n "cpp_oop_macros") (v "0.1.8") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0ac8qnrmibszqn9g5dlx1zjp8ln5l612qppfh3cs0amgl9b163nl")))

(define-public crate-cpp_oop_macros-0.1.9 (c (n "cpp_oop_macros") (v "0.1.9") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1vaziiwnp68vkaq7fb2w2941mwi7g3qrvfvdr19qymfvjmkmhqgp")))

