(define-module (crates-io cp p_ cpp_synmap) #:use-module (crates-io))

(define-public crate-cpp_synmap-0.2.0 (c (n "cpp_synmap") (v "0.2.0") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "14g86ns458fm1vwhkn1ixgk6x7bhbq6x5s09xszbkb7dpc4cmz3r")))

(define-public crate-cpp_synmap-0.3.0 (c (n "cpp_synmap") (v "0.3.0") (d (list (d (n "cpp_syn") (r "^0.12") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "cpp_synom") (r "^0.12") (d #t) (k 0)) (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "1dlh5r7i051j7p4v4mq0jwm9xf6qwn772lzy7zflx1z2vff4yzl9")))

