(define-module (crates-io cp uc cpucycles-sys) #:use-module (crates-io))

(define-public crate-cpucycles-sys-0.1.0 (c (n "cpucycles-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1gafw324pvb6wha9whdfcj1zrv2vg51lamvpqzpn5689wq274jkv")))

(define-public crate-cpucycles-sys-0.1.1 (c (n "cpucycles-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1") (d #t) (k 1)))) (h "0wxi2mcsk1zcfd6jvvna01a35iffymz5kaw4rlsc857j8vlbcm19")))

(define-public crate-cpucycles-sys-0.1.2 (c (n "cpucycles-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1") (d #t) (k 1)))) (h "185hahrvjhv8iii27laddicw572d4gjrk8bkm61ss66fqgj81biz")))

