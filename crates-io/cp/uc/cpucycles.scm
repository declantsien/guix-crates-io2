(define-module (crates-io cp uc cpucycles) #:use-module (crates-io))

(define-public crate-cpucycles-0.1.0 (c (n "cpucycles") (v "0.1.0") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "cpucycles-sys")) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "0d19bdf6yg3jqsfq3zxv2551qnn9w1rqql1wfvfk8pdggvf23sz1")))

(define-public crate-cpucycles-0.1.1 (c (n "cpucycles") (v "0.1.1") (d (list (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "cpucycles-sys")) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "1h7qs5lv1a59wmf7n12i8qj2bk3whwrkj8daqpwv0nxkfydv7kpy")))

(define-public crate-cpucycles-0.2.0 (c (n "cpucycles") (v "0.2.0") (d (list (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "cpucycles-sys")) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "07m2f5a8x19z9885mlzn0l6na64bxjaracxy452y4x5zgazl43ib")))

