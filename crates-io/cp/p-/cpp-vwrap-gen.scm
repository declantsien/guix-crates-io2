(define-module (crates-io cp p- cpp-vwrap-gen) #:use-module (crates-io))

(define-public crate-cpp-vwrap-gen-0.1.0 (c (n "cpp-vwrap-gen") (v "0.1.0") (d (list (d (n "clang") (r "^0.23") (f (quote ("runtime" "clang_8_0"))) (d #t) (k 0)) (d (n "clang-sys") (r "0.*") (d #t) (k 0)))) (h "1cx6ij8m3b7jn0vs4m1xqkhsx0wcxn7yg31dk6c2gq01890qs73d")))

