(define-module (crates-io cp p- cpp-inherit) #:use-module (crates-io))

(define-public crate-cpp-inherit-0.1.0 (c (n "cpp-inherit") (v "0.1.0") (d (list (d (n "gimli") (r "^0.22") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0") (d #t) (k 0)))) (h "0ac25i6z6c96cdc3782jjf21ymhc60nfnrjpbqw3qcwsigj2mran")))

(define-public crate-cpp-inherit-0.1.1 (c (n "cpp-inherit") (v "0.1.1") (d (list (d (n "gimli") (r "^0.22") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "typed-arena") (r "^2.0") (d #t) (k 0)))) (h "1m8ir3fz7lrpv84dbmfqf21dca4wlsi82klkyr2jr6s6vd19lm8d")))

