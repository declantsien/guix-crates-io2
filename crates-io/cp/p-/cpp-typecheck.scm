(define-module (crates-io cp p- cpp-typecheck) #:use-module (crates-io))

(define-public crate-cpp-typecheck-0.2.0 (c (n "cpp-typecheck") (v "0.2.0") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "0iz2l47z6aj8w3fzdclhcxlpgbhvxa8zb82gl8h6lhkrzyhbfgf8")))

(define-public crate-cpp-typecheck-0.2.1 (c (n "cpp-typecheck") (v "0.2.1") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "0dydvfk5vczs64ljxry14pniv5y5zji7lxpa9dzsghz8ll4c0vwi")))

(define-public crate-cpp-typecheck-0.3.0 (c (n "cpp-typecheck") (v "0.3.0") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "13r28d6nsnbwd9mrcg4mbqbpi982sirv084p0ihjyzny9047rriq")))

(define-public crate-cpp-typecheck-0.3.1 (c (n "cpp-typecheck") (v "0.3.1") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "1288k9zz4jdyzjhjxm3md94h356hpvsl367m066zvf3afks24rc0")))

(define-public crate-cpp-typecheck-0.3.2 (c (n "cpp-typecheck") (v "0.3.2") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "1gx77r3zdpyxdp9idmws3fyigllfy31gvybi4vyklb5993dvpxrv")))

(define-public crate-cpp-typecheck-0.3.3 (c (n "cpp-typecheck") (v "0.3.3") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)))) (h "165blgsnlipiayvngxsp7ybaargh10gaxczah2kxcg8r7cfivwl1")))

(define-public crate-cpp-typecheck-0.3.4 (c (n "cpp-typecheck") (v "0.3.4") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)) (d (n "tempfile") (r "~2.1.4") (d #t) (k 0)))) (h "172mbx0k1gzg4sy3020344gpq5q46vh02qj4fkcm3ylsyq3jx7q2")))

(define-public crate-cpp-typecheck-0.3.5 (c (n "cpp-typecheck") (v "0.3.5") (d (list (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)) (d (n "tempfile") (r "~2.1.4") (d #t) (k 0)))) (h "0krc3gc7kzx968a0i0b6n6i7v9xnqvh23hx8vg2zj5bd6mg513sb")))

(define-public crate-cpp-typecheck-0.3.6 (c (n "cpp-typecheck") (v "0.3.6") (d (list (d (n "atomicwrites") (r "~0.0.14") (d #t) (k 0)) (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)) (d (n "tempfile") (r "~2.1.4") (d #t) (k 0)))) (h "1z8468adx3byrw0a0yxwf0rw6p05q6h20if2mnxp27c2hspjkpzp")))

(define-public crate-cpp-typecheck-0.3.7 (c (n "cpp-typecheck") (v "0.3.7") (d (list (d (n "atomicwrites") (r "~0.0.14") (d #t) (k 0)) (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)) (d (n "tempfile") (r "~2.1.4") (d #t) (k 0)))) (h "1mw5pq185pn7pkfv4lq5mwn32cd3jkjff1mzq0kba9jcmx2li8lc")))

(define-public crate-cpp-typecheck-0.3.8 (c (n "cpp-typecheck") (v "0.3.8") (d (list (d (n "atomicwrites") (r "~0.0.14") (d #t) (k 0)) (d (n "clap") (r "~2.2.6") (d #t) (k 0)) (d (n "error-type") (r "~0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "~0.8.1") (d #t) (k 0)) (d (n "tempfile") (r "~2.1.4") (d #t) (k 0)))) (h "0qd15gl50v7aj49yliz9hfbbqssbv5fz81y22n6h60lh6k602j27")))

(define-public crate-cpp-typecheck-0.4.0 (c (n "cpp-typecheck") (v "0.4.0") (d (list (d (n "atomicwrites") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "error-type") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 0)))) (h "035saj44m6wlji1iqlrlvinck7nxlv8ijb4yswl8pg3azkghwnbi")))

(define-public crate-cpp-typecheck-0.5.0 (c (n "cpp-typecheck") (v "0.5.0") (d (list (d (n "atomicwrites") (r "^0.2.5") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "error-type") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "06sbbzjz59g6c70qkfxssda4919qr7qsjfbsq2am5xqgf57mazjm")))

