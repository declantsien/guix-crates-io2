(define-module (crates-io cp p- cpp-pack) #:use-module (crates-io))

(define-public crate-cpp-pack-0.1.0 (c (n "cpp-pack") (v "0.1.0") (d (list (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "06zmn0w00p3xrcvgixd10snr8yrf9v6gjfw58hs3kazy4cdrx0js")))

(define-public crate-cpp-pack-0.1.1 (c (n "cpp-pack") (v "0.1.1") (d (list (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1fvy2y86cc29w43kxj05ra238mk09niy7gl3shs470rchfiw7vim")))

