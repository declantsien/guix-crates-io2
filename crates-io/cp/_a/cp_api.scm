(define-module (crates-io cp _a cp_api) #:use-module (crates-io))

(define-public crate-cp_api-0.1.0 (c (n "cp_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zzlxph9mw9k0mk57hln5pnd8054mv97xzcqmiwwnxjvgfl2dh7d")))

(define-public crate-cp_api-0.1.1 (c (n "cp_api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hyzb068h9wc7xw2ws1v5n5zciv340aab1707cw6605b9cy91mqj")))

(define-public crate-cp_api-0.1.2 (c (n "cp_api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06mm926vlivrpfv6rygzfvlq5026zasnxsgkylli2w9f6p2g4909")))

(define-public crate-cp_api-0.1.3 (c (n "cp_api") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "189j75w0mns9c0y544nk8wy7b1585hxw63kaj5j3p7lar9f9a1dw")))

(define-public crate-cp_api-0.1.4 (c (n "cp_api") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lf640hlk3244h05zfr0akqqnygk89wk95pcn60iw4s1yfpgqmhs")))

(define-public crate-cp_api-0.1.5 (c (n "cp_api") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ypg6dsg59pparqvn2j34ivjqh37h4fgahyvckqg7aicci5bpw4s")))

(define-public crate-cp_api-0.1.6 (c (n "cp_api") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z8cbkzsa3i9jbjca60inv26ir3hjfqjdql8i5cmiziwyb6lyfpd")))

(define-public crate-cp_api-0.2.0 (c (n "cp_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fi7qzqmv8pbj67pgjkfx9y54qfx5vrhq2n747zsvljl0s5fdxfl")))

(define-public crate-cp_api-0.3.0 (c (n "cp_api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18anpy59is6034rqqlx31lxdq52ffv41gf948465gbg3p8aah6jv")))

(define-public crate-cp_api-0.3.1 (c (n "cp_api") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qqmsngzbj8l29n80va29zhh35l76dyvm7nbxwa0173q3dyrnkfr")))

(define-public crate-cp_api-0.4.0 (c (n "cp_api") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wiwp5hfyhh77b6bm705zqv5f81f9hsn37fbwdj67cqf49y5wfw3")))

