(define-module (crates-io cp wd cpwd) #:use-module (crates-io))

(define-public crate-cpwd-1.0.0 (c (n "cpwd") (v "1.0.0") (d (list (d (n "clipboard2") (r "^0.1.1") (d #t) (k 0)))) (h "07gmpwjkkqlbzlccj5g216vwf16sq58cbpdb0y8fplg352195jiy")))

(define-public crate-cpwd-1.0.1 (c (n "cpwd") (v "1.0.1") (d (list (d (n "clipboard2") (r "^0.1.1") (d #t) (k 0)))) (h "1spqmlf2kr3y7k7kryqjrf612rlrf35rmyjkg0lypcrjz08frkqj")))

