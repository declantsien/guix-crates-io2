(define-module (crates-io cp uf cpufreq_lib) #:use-module (crates-io))

(define-public crate-cpufreq_lib-0.1.0 (c (n "cpufreq_lib") (v "0.1.0") (h "07z7fzy0qj93qvmm13rhcpg6q51kg5m7k7chq4ngys5liiq4m1j1")))

(define-public crate-cpufreq_lib-0.1.1 (c (n "cpufreq_lib") (v "0.1.1") (h "13x87fcn1sj085k11hsyg4dxwwkv6y1bfrxnrgcfr35f2zgjnxbc")))

(define-public crate-cpufreq_lib-0.1.2 (c (n "cpufreq_lib") (v "0.1.2") (h "1rmyhqf9mjv7hhaym0rpz278rnvj802ah86ajcmhk686x6hnzpf8")))

