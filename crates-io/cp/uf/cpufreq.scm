(define-module (crates-io cp uf cpufreq) #:use-module (crates-io))

(define-public crate-cpufreq-0.1.0 (c (n "cpufreq") (v "0.1.0") (d (list (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1psrdq2fmkni7nghr63p7snn0gxal8bly45wrvm1h49mm2y2sgj5")))

(define-public crate-cpufreq-0.1.3 (c (n "cpufreq") (v "0.1.3") (d (list (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "106zgigrh80xml44f1k1n11dssp6brj3ayrj9zipx01fvbgwsa4z")))

(define-public crate-cpufreq-0.1.4 (c (n "cpufreq") (v "0.1.4") (d (list (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1n3nrk2ap7vywk2vampnnjgawnnr7dq7f4ph6pgwad7yk7z2s0j7")))

(define-public crate-cpufreq-0.1.5 (c (n "cpufreq") (v "0.1.5") (d (list (d (n "errno") (r "~0.1") (d #t) (k 0)) (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "03sd5mw9g5y6wm94f49azz1nyk4g1r03jlfkyhrax2i6ijdp0mzi")))

(define-public crate-cpufreq-0.1.6 (c (n "cpufreq") (v "0.1.6") (d (list (d (n "errno") (r "~0.1") (d #t) (k 0)) (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "14zdbvhclgc3566qgmy8hi75nlw9ykzhbgb1xr2jhdnzpf0hj92x")))

(define-public crate-cpufreq-0.1.6-r1 (c (n "cpufreq") (v "0.1.6-r1") (d (list (d (n "errno") (r "~0.1") (d #t) (k 0)) (d (n "gcc") (r "~0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "19ymxs06kwq49nnqq6p01077ixar0ynhkayca1h1x76cdbjhjli9")))

