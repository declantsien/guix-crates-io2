(define-module (crates-io cp uf cpufeatures) #:use-module (crates-io))

(define-public crate-cpufeatures-0.0.0 (c (n "cpufeatures") (v "0.0.0") (h "19nc39yavr1pyvpkw5igmx39zq7zk8x88y656f413n3l5bhj82b7")))

(define-public crate-cpufeatures-0.1.0 (c (n "cpufeatures") (v "0.1.0") (h "0ajr6qfbs97i0441cyppb2zizqz62l80nqcnjqdfrrqhh9sagmaw")))

(define-public crate-cpufeatures-0.1.1 (c (n "cpufeatures") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "1x7xbqdzh140ssyabw7mz38dzwil262frig2l92wr063ha0h5hfy")))

(define-public crate-cpufeatures-0.1.2 (c (n "cpufeatures") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "0dfnbsz47jzcchrcdc6f6s5xn1y3ync9zbvj478wyd6zyigqyl2g") (y #t)))

(define-public crate-cpufeatures-0.1.3 (c (n "cpufeatures") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "14wgns2jam0rgkx9l9mdar97jichhp2x709dn5d563is5hxmc7r8")))

(define-public crate-cpufeatures-0.1.4 (c (n "cpufeatures") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "1j0i97325c2grndsfgnm3lqk0xbyvdl2dbgn8i5dd9yhnmycc07d")))

(define-public crate-cpufeatures-0.1.5 (c (n "cpufeatures") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "1vvid867wpnz5wzma3f4x2ijl83fgr6x1fc6shspzpf9ysb9djb6")))

(define-public crate-cpufeatures-0.2.0 (c (n "cpufeatures") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.101") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "083s19qg11ls5xxalycpz9qvfxhkqn5gkbn6j6i5b57y6zy7vi4w") (y #t)))

(define-public crate-cpufeatures-0.2.1 (c (n "cpufeatures") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.101") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "0sgllzsvs8hinylaiigmd9c908gd8wclxnqz8dinpxbdyql981cm") (y #t)))

(define-public crate-cpufeatures-0.2.2 (c (n "cpufeatures") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "0jsyrw41g5sqdpc9jgk57969hc0xw4c52j9amvmll4mbcwb019jr") (y #t)))

(define-public crate-cpufeatures-0.2.3 (c (n "cpufeatures") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "0h7m10c0plziz9sqzm93fd3dx6h7dqg6bap8n64ciyfr522zny8h") (y #t)))

(define-public crate-cpufeatures-0.2.4 (c (n "cpufeatures") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "04z8kxhm3gk1bsd1b79m96nzlfprv60fpzrynw5b86r4jsxqx56w") (y #t)))

(define-public crate-cpufeatures-0.2.5 (c (n "cpufeatures") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)))) (h "08535izlz4kx8z1kkcp0gy80gqk7k19dqiiysj6r5994bsyrgn98")))

(define-public crate-cpufeatures-0.2.6 (c (n "cpufeatures") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)))) (h "10g16lzbczzjig8c906sjznybrb5vf0gnila7hd8ff1sicnry2i8")))

(define-public crate-cpufeatures-0.2.7 (c (n "cpufeatures") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)))) (h "0n7y7ls0g1svrjr6ymjv338q8ajc91sv2amdpgn7pi0j42m1wk1y")))

(define-public crate-cpufeatures-0.2.8 (c (n "cpufeatures") (v "0.2.8") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)))) (h "0z4q15dc4967rzbqnnpmdnjrppp1cqlgmanvvvmpvxzpx4l9xrh3")))

(define-public crate-cpufeatures-0.2.9 (c (n "cpufeatures") (v "0.2.9") (d (list (d (n "libc") (r "^0.2.95") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)))) (h "1wg1vmsx3gd30xkc7h7r6nfx7njx063hqjimgyrb0qj17bzpcyx1")))

(define-public crate-cpufeatures-0.2.10 (c (n "cpufeatures") (v "0.2.10") (d (list (d (n "libc") (r "^0.2.149") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"loongarch64\", target_os = \"linux\"))") (k 0)))) (h "1r79n73wkvapa32i8bj0vdr5zm43pfmy227qj8j5zcs2symn1g1z")))

(define-public crate-cpufeatures-0.2.11 (c (n "cpufeatures") (v "0.2.11") (d (list (d (n "libc") (r "^0.2.149") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"loongarch64\", target_os = \"linux\"))") (k 0)))) (h "1l0gzsyy576n017g9bf0vkv5hhg9cpz1h1libxyfdlzcgbh0yhnf")))

(define-public crate-cpufeatures-0.2.12 (c (n "cpufeatures") (v "0.2.12") (d (list (d (n "libc") (r "^0.2.149") (d #t) (t "aarch64-linux-android") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_os = \"linux\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"aarch64\", target_vendor = \"apple\"))") (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (t "cfg(all(target_arch = \"loongarch64\", target_os = \"linux\"))") (k 0)))) (h "012m7rrak4girqlii3jnqwrr73gv1i980q4wra5yyyhvzwk5xzjk")))

