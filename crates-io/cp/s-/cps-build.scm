(define-module (crates-io cp s- cps-build) #:use-module (crates-io))

(define-public crate-cps-build-0.1.0 (c (n "cps-build") (v "0.1.0") (h "0hbppki0859iajxdq4rakcwm8n6hangl081f820wnmb8ilprb756") (y #t)))

(define-public crate-cps-build-0.1.1 (c (n "cps-build") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nk567d4ns1f6qfjlndwy0gdalnbj1r06y0zh4wia826iaplbazp") (y #t)))

