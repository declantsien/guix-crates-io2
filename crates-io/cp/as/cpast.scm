(define-module (crates-io cp as cpast) #:use-module (crates-io))

(define-public crate-cpast-0.2.0 (c (n "cpast") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1vvrxdd24qlda2i2qamh2rg3yc88dr2l8il0igdl7a3sd08nmz7x")))

(define-public crate-cpast-0.2.1 (c (n "cpast") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0yy92lyavvac2ww4rmmsgl0g6yn0d8w9kycaagv2504gws094cxr") (y #t)))

(define-public crate-cpast-0.2.2 (c (n "cpast") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0hs10zf9wa0jj5ki9lmk6hsjd593gc4278faqhns7d5ih2nxqyjp")))

(define-public crate-cpast-0.2.3 (c (n "cpast") (v "0.2.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0kk8vpdg07lqasqjfr4zjnwr1f49v6hjk7agzlqnfh7pgn7pfihp") (y #t)))

(define-public crate-cpast-0.2.4 (c (n "cpast") (v "0.2.4") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1knyx25v2q6cjkvsbhxq4nwx48madhad127p6vwqlk5mixb1jfvi")))

(define-public crate-cpast-0.2.5 (c (n "cpast") (v "0.2.5") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1z2cwf03vb2j1kfmkmvs2c7k7bgyxgngmjvl8spdvvggfdgxn4d8") (y #t)))

(define-public crate-cpast-0.2.6 (c (n "cpast") (v "0.2.6") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "00m4jwiz2gy5wc85hw0igx89k7qxw59qia2zm40zzdm979z2nmxw") (y #t)))

(define-public crate-cpast-0.2.7 (c (n "cpast") (v "0.2.7") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0fxffb3jrql3065pqn95c1i44pxzdad9xb9myn6qpj8q7yiqbcr3")))

(define-public crate-cpast-0.2.8 (c (n "cpast") (v "0.2.8") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "082zkzvxk66213ffzsna92rr1wccqd3w8m55v0pxl6xj8m2a32fl") (y #t)))

(define-public crate-cpast-0.2.9 (c (n "cpast") (v "0.2.9") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "0ryn4jy898jk19m5055gjal7nb7dkjpjl0ghb3bvbwkrarn8xkpb")))

(define-public crate-cpast-0.3.0 (c (n "cpast") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0wszsjk4nj0fjq88zppww02hjvwrsdzpi1i27cwf5r6lzb43b7pc")))

(define-public crate-cpast-0.3.1 (c (n "cpast") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "13zql7glryxn68gkwp9alcf97vpq4g1ysl2ksyyiwf961mw6znjp") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:cli-clipboard"))))))

(define-public crate-cpast-0.3.2 (c (n "cpast") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "04gn8bwa567rp2x4b3l5gpc7s7nwf7g5j7ajla3pvkyq77ipggr0") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:cli-clipboard"))))))

(define-public crate-cpast-0.3.3 (c (n "cpast") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "1i90s1lnfgkdmbax48i3k8drznry3lbf76isyibps87njyzbp69z") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:cli-clipboard"))))))

(define-public crate-cpast-0.3.4 (c (n "cpast") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "1vbnq6mkg01lwgvfr3sqf476ha39qkhrvqz9n0g2ian0df2gjg25") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:cli-clipboard"))))))

(define-public crate-cpast-0.4.0 (c (n "cpast") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "04g50mllv04s0dl98466i2srl5q9qq6n952rr0nwkgag4s9b58l2") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:cli-clipboard"))))))

