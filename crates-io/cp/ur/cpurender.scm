(define-module (crates-io cp ur cpurender) #:use-module (crates-io))

(define-public crate-cpurender-0.1.0 (c (n "cpurender") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "glium") (r "^0.25.1") (d #t) (k 0)) (d (n "image") (r "^0.22.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "vek") (r "^0.9.9") (f (quote ("repr_simd"))) (d #t) (k 0)))) (h "0aq4qly73f7rhwi47b9q7g7nljyks0p3w77hq0s7ik0h3p5lgq29")))

