(define-module (crates-io cp yt cpython-json) #:use-module (crates-io))

(define-public crate-cpython-json-0.1.0 (c (n "cpython-json") (v "0.1.0") (d (list (d (n "cpython") (r "^0.1") (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1sck5j9r39c1b53825053ahfz07x6vin9nxdg60skv314zz8xgxg") (f (quote (("python3-sys" "cpython/python3-sys") ("python27-sys" "cpython/python27-sys") ("default" "python3-sys"))))))

(define-public crate-cpython-json-0.1.1 (c (n "cpython-json") (v "0.1.1") (d (list (d (n "cpython") (r "^0.1") (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1cgxycwl736ggr0fivpdrv82d51mjwg84sig4c9pd7s08wxjsyc8") (f (quote (("python3-sys" "cpython/python3-sys") ("python27-sys" "cpython/python27-sys") ("default" "python3-sys"))))))

(define-public crate-cpython-json-0.2.0 (c (n "cpython-json") (v "0.2.0") (d (list (d (n "cpython") (r "^0.1") (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1741aa09p5pnzyfbl1bjpiqx9jb683374ifv41gqnmwi93q9gvpj") (f (quote (("python3-sys" "cpython/python3-sys") ("python27-sys" "cpython/python27-sys") ("default" "python3-sys"))))))

(define-public crate-cpython-json-0.3.0 (c (n "cpython-json") (v "0.3.0") (d (list (d (n "cpython") (r "^0.2") (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0313b2armb5wi9di65ar67ygbci0b80a09wpiprpi66zgjgpkw2y") (f (quote (("python3-sys" "cpython/python3-sys") ("python27-sys" "cpython/python27-sys") ("default" "python3-sys"))))))

