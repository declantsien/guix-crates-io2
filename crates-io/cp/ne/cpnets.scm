(define-module (crates-io cp ne cpnets) #:use-module (crates-io))

(define-public crate-cpnets-1.0.0 (c (n "cpnets") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "cargo_bench_support"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3" "auto-initialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09c7g12hgfmxvb39g23big5hhx3pijzw5rfmch0fsj1zpff0yvrs")))

(define-public crate-cpnets-1.1.0 (c (n "cpnets") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "cargo_bench_support"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("abi3" "auto-initialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01z2k3w5y2ggn87nwjfm09varmrx7dcmhdlchz080d0cjxdlvcwk")))

