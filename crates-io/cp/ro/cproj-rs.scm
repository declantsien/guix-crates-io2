(define-module (crates-io cp ro cproj-rs) #:use-module (crates-io))

(define-public crate-cproj-rs-0.1.0 (c (n "cproj-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02ywy5qvdnq435g4llh16lraaz5n9z36ada33f94w01yifmvyr2r")))

(define-public crate-cproj-rs-0.1.1 (c (n "cproj-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15bqjxb3523p3y5z74l6aqn9s54fyl7vcaf26kik5vqvcs901ipv")))

(define-public crate-cproj-rs-0.1.2 (c (n "cproj-rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dvl33138cl66b8hkvjr4zd9qv6pf7q5jdfnjbmnkz42nxvb2759")))

