(define-module (crates-io cp ro cprocsp-sys) #:use-module (crates-io))

(define-public crate-cprocsp-sys-0.0.1 (c (n "cprocsp-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "15nmh41rcm7swxyl6rkj40i6p62d2353lw02j0w4niswjdal22rq")))

(define-public crate-cprocsp-sys-0.0.2 (c (n "cprocsp-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "006z70xzxzg1bin3kwbiqpkp8d2i7rk00w8wgfmbmlbkk301qmq2") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-cprocsp-sys-0.0.3 (c (n "cprocsp-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "0rxvfyfz0v6kf4rrqhdj7kgz817rxl4vbbc1wvlh5cw4b4jkpkhh") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-cprocsp-sys-0.0.4 (c (n "cprocsp-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "1sf5rp0vwwvcw1wwf4cnpf3ha0bi3nszn6w3jyfmb7xg2hqr1glr") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-cprocsp-sys-0.0.5 (c (n "cprocsp-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "1r153hsf8zrikj579i8ww28g5mbylhl05fh2f4cdqw10v927ixxs") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-cprocsp-sys-0.0.6-beta.1 (c (n "cprocsp-sys") (v "0.0.6-beta.1") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "0w46avg0pfk1ycc7jffwfrp1lamw2jrja2s0wv31iiidzxj751hc") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-cprocsp-sys-0.0.6-beta.2 (c (n "cprocsp-sys") (v "0.0.6-beta.2") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (k 1)) (d (n "libloading") (r "^0.8.0") (k 0)))) (h "08cwhzbxfmdganigjw4vavp300vcazlmqg0c3rhybgic4w9cqkpq") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

