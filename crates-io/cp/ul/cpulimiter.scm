(define-module (crates-io cp ul cpulimiter) #:use-module (crates-io))

(define-public crate-cpulimiter-0.1.0 (c (n "cpulimiter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)))) (h "0h4f55wcx5ll7zqbnfhhhxnlpmaxyxznz1zvhc0bmyvy9k5yy3qx")))

(define-public crate-cpulimiter-0.1.1 (c (n "cpulimiter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)))) (h "0hz7xyzjwlggh3ndrf5zjszy061ggla5dzj9mm7wdch4cikixj8p")))

(define-public crate-cpulimiter-0.2.0 (c (n "cpulimiter") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ihvc9596lrs7xpxfcqjf6iicjxkn978znaw1rzgj89k9sr7rcli")))

