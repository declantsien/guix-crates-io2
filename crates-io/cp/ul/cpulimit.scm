(define-module (crates-io cp ul cpulimit) #:use-module (crates-io))

(define-public crate-cpulimit-0.1.1 (c (n "cpulimit") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "cpulimiter") (r "^0.1.1") (d #t) (k 0)))) (h "0rqm6c5ns77vph2dw617m7bxhrxk39ilzm9d30imxw94c8p54kj1")))

(define-public crate-cpulimit-0.2.0 (c (n "cpulimit") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "cpulimiter") (r "^0.2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (f (quote ("termination"))) (d #t) (k 0)))) (h "09w7srs7y551wj9q740q5hasl59nannsny8dwh0dwz4fr1805nw5")))

