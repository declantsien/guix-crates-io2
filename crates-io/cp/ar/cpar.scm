(define-module (crates-io cp ar cpar) #:use-module (crates-io))

(define-public crate-cpar-0.1.0 (c (n "cpar") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0l6x344n5q351jwswnl6vsn8h374x63i81c00i98zc5iw89k8lvp")))

(define-public crate-cpar-0.1.1 (c (n "cpar") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1zxx85z3wggghhldwid09fqli7wkrrawhnj7glyw6zssnbdgf8xc")))

