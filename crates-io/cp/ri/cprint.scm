(define-module (crates-io cp ri cprint) #:use-module (crates-io))

(define-public crate-cprint-0.1.0 (c (n "cprint") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "047kv1fjmy2brgn50w4w35xvdk4h25ip2s9f94a6jdk62iam4klf")))

(define-public crate-cprint-0.2.0 (c (n "cprint") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "14plpmzjdbnq3f1r82asvr3l8lii33dw9s208ysd2cpn2q9hjbs3")))

(define-public crate-cprint-0.3.0 (c (n "cprint") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0hyrnijsj3q2znmmnhf0z5xm9psi6wmwxn6liynv66pq2skzryif") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.3.1 (c (n "cprint") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11k3i2flcp2ap22h79sq69rybssyn1kg14s3jzw242mx1i6vd9qk") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.4.0 (c (n "cprint") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1c3izh5mki93f9f4x570w94dvjin43i1qnqfasxab6nacd1m33b3") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.4.1 (c (n "cprint") (v "0.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1rw1ffqwwrsn598nl6smbj3fxi24h2ldvx00j7xiqbvyk6g81jz4") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5.0 (c (n "cprint") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1zrmrn4wizb3y1xhcjahixrzjcayddv1q1y93b3m6z9b117a3gza") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5.1 (c (n "cprint") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0gav6bpb4186n62acmvscn6gflxw5isdi5ahqix826nw5wr179lx") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5.2 (c (n "cprint") (v "0.5.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "084b8vif6ppbxdk7lk1vykdcg3xrm44p1524kr4wnrlxk4sq9077") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5.3 (c (n "cprint") (v "0.5.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1mffwwj6qbk2xysjric4y1d9yvyb1cdskn0qwijlcczb07z61l42") (f (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-1.0.0 (c (n "cprint") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1s7k2vmm38r37y5q8qccli0chyrw0nbdm451msyvpfbbpq14rgv4") (f (quote (("default" "cprint") ("cprint" "cformat") ("cformat") ("ceprint" "cformat"))))))

