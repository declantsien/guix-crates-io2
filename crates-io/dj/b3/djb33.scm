(define-module (crates-io dj b3 djb33) #:use-module (crates-io))

(define-public crate-djb33-0.1.0 (c (n "djb33") (v "0.1.0") (h "1zxb49ajmvkginh3i8y7jfz511x0b5h9w3hbxi7fk9dpv6wz22bs") (y #t)))

(define-public crate-djb33-0.2.0 (c (n "djb33") (v "0.2.0") (h "1i5n49jh0qpmw059cizql5a2rplhhckl2qs1w1x186iqpgnrd6s4")))

(define-public crate-djb33-0.3.0 (c (n "djb33") (v "0.3.0") (h "1qpaaci19fzngbcq29sd7gx220b555iqc63vbmx3v8xg1jr4qir9")))

