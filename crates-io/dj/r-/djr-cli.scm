(define-module (crates-io dj r- djr-cli) #:use-module (crates-io))

(define-public crate-djr-cli-0.0.1 (c (n "djr-cli") (v "0.0.1") (d (list (d (n "cargo-lock") (r "^8.0.3") (d #t) (k 1)) (d (n "djr") (r "^0.0.1") (d #t) (k 0)))) (h "0xwkjk2imz8m7hlsch129h8fkv224nnw8gbmwg3xgrzrjcaka2yv") (y #t)))

