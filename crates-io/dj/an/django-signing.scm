(define-module (crates-io dj an django-signing) #:use-module (crates-io))

(define-public crate-django-signing-0.0.0 (c (n "django-signing") (v "0.0.0") (d (list (d (n "base62") (r "^1.1.5") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.1") (f (quote ("mac"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0sqf58ihgflyypqi9003j65ml9p7k0pwk9gl9r5np5nn35mqplmn")))

