(define-module (crates-io dj an django-auth) #:use-module (crates-io))

(define-public crate-django-auth-0.1.0 (c (n "django-auth") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pbkdf2") (r "^0.12") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fvr5bg3ycsyljdymnb7v46fp6rjlzh7dyks33x663i66ij9vj8m")))

(define-public crate-django-auth-0.1.1 (c (n "django-auth") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pbkdf2") (r "^0.12") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r6pg5rpz80k1am4kqns8vd0dw5cgsdj3ax853j1mzw9qvj0rv60")))

