(define-module (crates-io dj an django-query_derive) #:use-module (crates-io))

(define-public crate-django-query_derive-0.1.0 (c (n "django-query_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13y21ihq22wxv52z6j5n5rfpjrwa16qj2hmckqws0zfq67y0g3jy")))

(define-public crate-django-query_derive-0.1.3 (c (n "django-query_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hrkjmlhy4adxvyv02yfyyril4jxzdgap40pk8pf4lnv5yklbnr8")))

(define-public crate-django-query_derive-0.1.4 (c (n "django-query_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "190nvjsjcvhxh518irkgyls0isc2hfd98nvn3y8rwwngqcvm2dyc")))

(define-public crate-django-query_derive-0.2.0 (c (n "django-query_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1f5zhp3hjvqkw54h29n89ij2f5lrrvv7x81kg6hgxlcxfxc5isnh")))

(define-public crate-django-query_derive-0.2.1 (c (n "django-query_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l1i06kwgk660gndfgw2kwaim3y1lzczvqbmvdagng3jshckjzn3")))

