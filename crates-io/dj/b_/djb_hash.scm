(define-module (crates-io dj b_ djb_hash) #:use-module (crates-io))

(define-public crate-djb_hash-0.1.0 (c (n "djb_hash") (v "0.1.0") (h "105ydn1y8i64z66ikg5n0s636vqw0vvg06iyv7wc07pkm8dg8px1")))

(define-public crate-djb_hash-0.1.2 (c (n "djb_hash") (v "0.1.2") (h "1xl2m5gdylnfpqss68gnx37k4mdwnkcsr81bwn1ar3wixg6pv4iz")))

(define-public crate-djb_hash-0.1.3 (c (n "djb_hash") (v "0.1.3") (h "0y45y23xlnnskzbprwg4l3bwv21pgzjlfkzjz2db98r7wrhpvkz8")))

