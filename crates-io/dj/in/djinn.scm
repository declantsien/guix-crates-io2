(define-module (crates-io dj in djinn) #:use-module (crates-io))

(define-public crate-djinn-0.0.1 (c (n "djinn") (v "0.0.1") (d (list (d (n "cpython") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "redis-cluster") (r "^0.1") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serialize") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4" "rustc-serialize"))) (d #t) (k 0)) (d (n "ws") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0v35rfddpfppa3gsgmv35m0f47hgq889lrvnvxsr9i7hs2jawidk")))

