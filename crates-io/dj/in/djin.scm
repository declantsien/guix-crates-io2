(define-module (crates-io dj in djin) #:use-module (crates-io))

(define-public crate-djin-0.1.0 (c (n "djin") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "libloaderapi" "memoryapi" "minwindef" "ntdef" "processthreadsapi" "synchapi" "winbase" "winnt"))) (d #t) (k 0)))) (h "18pjg0lny1zsimd2wrnj19yj6dz2sac0drjrx3s1yydqybp99zz2")))

(define-public crate-djin-0.1.1 (c (n "djin") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "libloaderapi" "memoryapi" "minwindef" "ntdef" "processthreadsapi" "synchapi" "winbase" "winnt"))) (d #t) (k 0)))) (h "0vpa52klzd6przxhhyapddjh5s0ck36ipi60nwxy895ihf6c39yi")))

