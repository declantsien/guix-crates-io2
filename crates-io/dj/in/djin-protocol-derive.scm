(define-module (crates-io dj in djin-protocol-derive) #:use-module (crates-io))

(define-public crate-djin-protocol-derive-3.1.9 (c (n "djin-protocol-derive") (v "3.1.9") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1m3qdnxsx7nj9qyxkmyn70aq4q9n9rlh84nnaza46pkwdivpvdz8") (y #t)))

(define-public crate-djin-protocol-derive-3.2.0 (c (n "djin-protocol-derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1hijik7q5ycgmk26vwx0jcfnbrqrxf2x2h3axv08aak3wnn6rrm1")))

(define-public crate-djin-protocol-derive-3.3.1 (c (n "djin-protocol-derive") (v "3.3.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("default" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "07b7h2bq501p9djs3q5pij43znlv1xxhnqpdypz979d3l125xysa")))

