(define-module (crates-io dj ot djotters) #:use-module (crates-io))

(define-public crate-djotters-0.1.3 (c (n "djotters") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0q0wbka0killvi02pihpipcqwyw9sm4d6l8gqsa8ml532sm6r68y")))

(define-public crate-djotters-0.1.4 (c (n "djotters") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0z095vnyjhjqavvn767sygsvnnvk2zd6cywrblxrdfy3k2d3n87c")))

(define-public crate-djotters-0.1.5 (c (n "djotters") (v "0.1.5") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "1x3kj1bxsz25n6mgjg26agf26dp0m2mbwgbblj0lnh6m3p48jmwr")))

(define-public crate-djotters-0.1.6 (c (n "djotters") (v "0.1.6") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)))) (h "0ygw57cx7falj49j87bdwx87gl4hj27byl9ngssqh2wqlpfj0wvd")))

(define-public crate-djotters-0.1.7 (c (n "djotters") (v "0.1.7") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "162nw463g3fyqn5w1b3ly7dymhklbgxh1a1g2bdhnnql3iaa516z")))

(define-public crate-djotters-0.1.8 (c (n "djotters") (v "0.1.8") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "06mz5ynhy4yvxngbbxlhn849whbjzx6nbaqx917xm535ldiwvv75")))

(define-public crate-djotters-0.1.9 (c (n "djotters") (v "0.1.9") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "0kgj7byafs5ann24nmgzi1ij2l85l8mx2ps0zizl20kds7szkciw")))

(define-public crate-djotters-0.1.10 (c (n "djotters") (v "0.1.10") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "1y1z07mbl2c6s09crhjp6rnbbr9rpx9s9yynqpf0w7a5a0y031rf")))

(define-public crate-djotters-0.1.11 (c (n "djotters") (v "0.1.11") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "1mb0pmj6x640k47s00map29nc8qlvn8qhfznsnzyrcxdx0dfavv2")))

(define-public crate-djotters-0.1.12 (c (n "djotters") (v "0.1.12") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "1bdzl3ancpnb5h7498sgsmcbmb30xxb2qakmff12a2k0wfg7l937")))

(define-public crate-djotters-0.1.13 (c (n "djotters") (v "0.1.13") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)))) (h "0yb90mprplvhf9aqs7dp1dqi7jy7gn2g47875a0b74rch0xny68q")))

