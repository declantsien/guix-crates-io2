(define-module (crates-io dj ot djot) #:use-module (crates-io))

(define-public crate-djot-0.0.1 (c (n "djot") (v "0.0.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0kha1h1j4jx4hwaczj6sqsv5m95ww8y71jjq28dyfknvw6nglwi4")))

(define-public crate-djot-0.0.2 (c (n "djot") (v "0.0.2") (h "1nwhf8kh7xm9j9114arfwjjs6xm52cl5i85r0bz864nnln1sywrw")))

