(define-module (crates-io x3 #{28}# x328-proto) #:use-module (crates-io))

(define-public crate-x328-proto-0.1.0 (c (n "x328-proto") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "nom") (r "^6.1") (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("std"))) (k 0)))) (h "0jsywzy4ndy6rwzinp4nvvz2qwxgi8cbfj5sr2gm4fk130zq4rbl")))

(define-public crate-x328-proto-0.1.1 (c (n "x328-proto") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "nom") (r "^6.1") (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("std"))) (k 0)))) (h "0k2pfpa9n7qgfrmh48rcp8897fw4ysj89r7m0dh00rkcpwac3xbq")))

(define-public crate-x328-proto-0.1.2 (c (n "x328-proto") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "nom") (r "^6.1") (k 0)) (d (n "snafu") (r "^0.6") (f (quote ("std"))) (k 0)))) (h "1qdf6lawy6b75h02bdppldy94yhqllz6q0p1wfvg070hf51g9468")))

(define-public crate-x328-proto-0.1.3 (c (n "x328-proto") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.0") (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "14a2ij12a3xrmadilbsjxhr6ildq2w8ha12q6b8w7qpwbl8jw7qs") (f (quote (("std" "snafu/std") ("default" "std"))))))

(define-public crate-x328-proto-0.2.0 (c (n "x328-proto") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7.0") (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7.1") (f (quote ("rust_1_46"))) (k 0)))) (h "076hqxj3138zl8h5nryds214s9h4wzc5bbn9xy95jxhy5arjnrj3") (f (quote (("std" "snafu/std") ("default" "std"))))))

