(define-module (crates-io yi pp yippie) #:use-module (crates-io))

(define-public crate-yippie-0.1.0 (c (n "yippie") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "show-image") (r "^0.13") (f (quote ("image"))) (d #t) (k 0)))) (h "0rf3dx867ickkzfkphvp20sw4xabc5wpx3g3d4w4zz6hkfasbb5n")))

