(define-module (crates-io yi ce yices2-sys) #:use-module (crates-io))

(define-public crate-yices2-sys-0.1.0 (c (n "yices2-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 1)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.27") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "tar") (r "^0.4.40") (d #t) (k 1)) (d (n "xz") (r "^0.1.0") (d #t) (k 1)))) (h "00iwkk1d29npmcwxnbrjbjmi8yjvmgsy7207admg0ggr2m4cgqmh") (f (quote (("use-generated") ("gcc") ("default" "clang") ("clang"))))))

(define-public crate-yices2-sys-2.6.4 (c (n "yices2-sys") (v "2.6.4") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 1)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.27") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "tar") (r "^0.4.40") (d #t) (k 1)) (d (n "xz") (r "^0.1.0") (d #t) (k 1)))) (h "14wc0p4m0m1abdh9m2h874zayn2dgg2d6hd4na2yj78dvq0cx4bk") (f (quote (("use-generated") ("gcc") ("default" "clang") ("clang"))))))

(define-public crate-yices2-sys-2.6.4-patch.1 (c (n "yices2-sys") (v "2.6.4-patch.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 1)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.27") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "tar") (r "^0.4.40") (d #t) (k 1)) (d (n "xz") (r "^0.1.0") (d #t) (k 1)))) (h "1ias9wm4v5p9dnx6ccprr41i010179bhdj9ng9znlxn2aylp5p0g") (f (quote (("use-generated") ("gcc") ("default" "clang") ("clang"))))))

