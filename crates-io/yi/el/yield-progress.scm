(define-module (crates-io yi el yield-progress) #:use-module (crates-io))

(define-public crate-yield-progress-0.1.0 (c (n "yield-progress") (v "0.1.0") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros"))) (k 2)))) (h "0a0nbik38r2da53kk7sfx71lyri8q9zvjrf8aiklgxz4wdihhj25") (r "1.63.0")))

(define-public crate-yield-progress-0.1.1 (c (n "yield-progress") (v "0.1.1") (d (list (d (n "instant") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)))) (h "14yfhhsz0nd22j7v0izlbadcyz1mm58rpp2ph05wwi5s37zi1kd1") (f (quote (("sync") ("default" "sync")))) (y #t) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:instant")))) (r "1.63.0")))

(define-public crate-yield-progress-0.1.2 (c (n "yield-progress") (v "0.1.2") (d (list (d (n "instant") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)))) (h "0dxnbsasb3aqhi3iygpxz86vh5scvl41qz8z4ci6gvrjx1rk0kq0") (f (quote (("sync") ("default" "sync")))) (y #t) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:instant")))) (r "1.63.0")))

(define-public crate-yield-progress-0.1.3 (c (n "yield-progress") (v "0.1.3") (d (list (d (n "instant") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)))) (h "1vvmgblk8a7agriv5sx9narqgj5r0casydflsplvv025d58qj3hk") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:instant")))) (r "1.63.0")))

(define-public crate-yield-progress-0.1.4 (c (n "yield-progress") (v "0.1.4") (d (list (d (n "instant") (r "^0.1.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)))) (h "1kzn11yn8snx12q7lx4fkyw0myifq4gnapb88gma3spnmiishwcc") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:instant")))) (r "1.63.0")))

(define-public crate-yield-progress-0.1.5 (c (n "yield-progress") (v "0.1.5") (d (list (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)) (d (n "web-time") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1wl9wqhnfpqxhdfnmgd4aaiw6fdr3mbbgax6iq02vlnmw0ypsqa7") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:web-time")))) (r "1.63.0")))

(define-public crate-yield-progress-0.1.6 (c (n "yield-progress") (v "0.1.6") (d (list (d (n "log") (r "^0.4.17") (o #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync" "macros" "signal" "time"))) (k 2)) (d (n "web-time") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1mm7ycbnd53p5rsg8gp8d5x5n0m3xs8w3sfv4w35a3fy3l3x91v7") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("log_hiccups" "dep:log" "dep:web-time")))) (r "1.63.0")))

