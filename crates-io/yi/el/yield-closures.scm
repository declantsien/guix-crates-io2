(define-module (crates-io yi el yield-closures) #:use-module (crates-io))

(define-public crate-yield-closures-0.1.0 (c (n "yield-closures") (v "0.1.0") (d (list (d (n "waker-fn") (r "^1") (d #t) (k 0)) (d (n "yield-closures-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0j79vg35nk251lr6a4f6cy3w3bl9f4ir26pa32ac8wd2561c70kc")))

