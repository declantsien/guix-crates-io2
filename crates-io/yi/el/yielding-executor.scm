(define-module (crates-io yi el yielding-executor) #:use-module (crates-io))

(define-public crate-yielding-executor-0.10.0 (c (n "yielding-executor") (v "0.10.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("sync" "rt"))) (d #t) (k 2)))) (h "0pahr38p5rmyjps8c4hf8c0q4a7g4vvr9nfp94iga20fxmqr9628") (f (quote (("default") ("debug"))))))

