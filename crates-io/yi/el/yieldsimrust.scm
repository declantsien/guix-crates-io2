(define-module (crates-io yi el yieldsimrust) #:use-module (crates-io))

(define-public crate-YieldSimRust-0.1.0 (c (n "YieldSimRust") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "17sbnhyqqz3c4xkmz8izdj2an4cc8wfxpsqjy0inabancn2qwprh")))

(define-public crate-YieldSimRust-0.1.1 (c (n "YieldSimRust") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1a5fzvqv7470dknycp36qgl9i94jrqljj40zfi21192y1wvvp6i4")))

(define-public crate-YieldSimRust-0.1.2 (c (n "YieldSimRust") (v "0.1.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "11a7gxqfqlq47755ws1q9bifmpp4rviajz9mfv7krp77sla4rfa8")))

