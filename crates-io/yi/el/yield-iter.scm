(define-module (crates-io yi el yield-iter) #:use-module (crates-io))

(define-public crate-yield-iter-0.1.0 (c (n "yield-iter") (v "0.1.0") (h "085vkpngfjf5fcn0h93w13c5dxs6jndcx8pz1srbf42sj52h9l2m")))

(define-public crate-yield-iter-0.2.0 (c (n "yield-iter") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1z6fy63wx31m01xm2i5jbwdjvqsr5j99572gcnlbpww64gyyrpsw")))

(define-public crate-yield-iter-0.2.1 (c (n "yield-iter") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "16hzqypc7pgpvqaq5fll97k6qxjpxinavgs48pggb3r0yr6ix4nl")))

