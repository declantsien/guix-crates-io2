(define-module (crates-io oh -n oh-napi-sys) #:use-module (crates-io))

(define-public crate-oh-napi-sys-0.1.0 (c (n "oh-napi-sys") (v "0.1.0") (d (list (d (n "directories") (r "^5.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0prx3h7m92mwdlwlrccnhl5fn5bflbsihvmsllww5663acrwadyb")))

(define-public crate-oh-napi-sys-0.1.1 (c (n "oh-napi-sys") (v "0.1.1") (d (list (d (n "directories") (r "^5.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0kva9pcb920wfs6h28c7bqq9alfdpxvjpshgcwsix5q7w53qq18b")))

