(define-module (crates-io oh -n oh-napi-src) #:use-module (crates-io))

(define-public crate-oh-napi-src-0.1.0 (c (n "oh-napi-src") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "directories") (r "^5.0") (d #t) (k 1)))) (h "1g77iax151mgq22f14hx9y53f25hyb1scax66q0d0x9bp59wqjbp") (f (quote (("napi-9") ("napi-10") ("ark-ui-x")))) (y #t)))

(define-public crate-oh-napi-src-0.1.1 (c (n "oh-napi-src") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "directories") (r "^5.0") (d #t) (k 1)))) (h "15zlhg6318g0msw4j541335idlqbj2zqkb24r1wgdxayd7x0i26y") (f (quote (("napi-9") ("napi-10") ("ark-ui-x")))) (y #t)))

(define-public crate-oh-napi-src-0.1.3 (c (n "oh-napi-src") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "directories") (r "^5.0") (d #t) (k 1)))) (h "0w5hh914jj2zflc4nj38wjm4gv905kdllyjksifdyy61jixpd02z") (f (quote (("napi-9") ("napi-10") ("ark-ui-x")))) (y #t)))

