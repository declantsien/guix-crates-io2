(define-module (crates-io oh os ohos_hilog) #:use-module (crates-io))

(define-public crate-ohos_hilog-0.1.0 (c (n "ohos_hilog") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ohos_hilog-sys") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "0v0x7bkp2lcrlzr1nzsprjj409z7m4zaldz7dacfa1xm6960x07f") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-ohos_hilog-0.1.1 (c (n "ohos_hilog") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ohos_hilog-sys") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "1c816i35k9y59a0lblzd2v0h62vvlpbdgqqj53ffbs6qjp0pxvng") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-ohos_hilog-0.1.2 (c (n "ohos_hilog") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ohos_hilog-sys") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)))) (h "1x4hv3fqviizm8cq38cj7bdigqyzzwfs4pp6ipi3c0l5zygia68g") (f (quote (("regex" "env_logger/regex") ("default" "regex"))))))

