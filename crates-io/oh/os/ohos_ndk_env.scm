(define-module (crates-io oh os ohos_ndk_env) #:use-module (crates-io))

(define-public crate-ohos_ndk_env-0.1.0 (c (n "ohos_ndk_env") (v "0.1.0") (h "1yl8xh2bd1h54nbj1c597siy220bkkpnx1ig4fh7f1gwk92awlai")))

(define-public crate-ohos_ndk_env-0.1.1 (c (n "ohos_ndk_env") (v "0.1.1") (h "02yl885hba4ay8cs2a6zfg7z1k7j40wbv68wwy5lf8q8778kdjy5")))

(define-public crate-ohos_ndk_env-0.1.2 (c (n "ohos_ndk_env") (v "0.1.2") (h "1c90yh0gnrablr176nq5dprxmvl6g02csyqcxy4m4nl347lk3ic4")))

(define-public crate-ohos_ndk_env-0.1.3 (c (n "ohos_ndk_env") (v "0.1.3") (h "0klb68vf5wak7ahmh8zngz64zszk1l58m8azmm3glmrj78glyzag")))

(define-public crate-ohos_ndk_env-0.1.4 (c (n "ohos_ndk_env") (v "0.1.4") (h "0q44jil9hb9f23h05008cjbd7ic1zikxwvpsghgphx5w15199lpn")))

