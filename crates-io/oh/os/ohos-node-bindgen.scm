(define-module (crates-io oh os ohos-node-bindgen) #:use-module (crates-io))

(define-public crate-ohos-node-bindgen-6.0.2 (c (n "ohos-node-bindgen") (v "6.0.2") (d (list (d (n "nj-core") (r "^6.0.1") (o #t) (d #t) (k 0) (p "ohos-nj-core")) (d (n "nj-derive") (r "^3.2.0") (o #t) (d #t) (k 0) (p "ohos-nj-derive")) (d (n "nj-sys") (r "^0.1.1") (o #t) (d #t) (k 0) (p "oh-napi-sys")))) (h "1amcbnjm2bdw87hz5xbpbzrbn7dvmm5vvb8hs5bjrkdnydm38312") (f (quote (("uuid" "nj-core/convert-uuid") ("serde-json" "nj-core/serde-json") ("node" "nj-sys" "nj-core" "nj-derive") ("default" "node"))))))

(define-public crate-ohos-node-bindgen-6.0.3 (c (n "ohos-node-bindgen") (v "6.0.3") (d (list (d (n "nj-core") (r "^6.0.1") (o #t) (d #t) (k 0) (p "ohos-nj-core")) (d (n "nj-derive") (r "^3.2.0") (o #t) (d #t) (k 0) (p "ohos-nj-derive")) (d (n "nj-sys") (r "^0.1.1") (o #t) (d #t) (k 0) (p "oh-napi-sys")))) (h "0c1d4f5z3xd7mbi4b5f1hbqmffzdmp9gqp546g2dk9nmfj0y2zn5") (f (quote (("uuid" "nj-core/convert-uuid") ("serde-json" "nj-core/serde-json") ("node" "nj-sys" "nj-core" "nj-derive") ("default" "node"))))))

