(define-module (crates-io oh os ohos-init-binding) #:use-module (crates-io))

(define-public crate-ohos-init-binding-0.0.1 (c (n "ohos-init-binding") (v "0.0.1") (h "1zhlaimvgzhf9gaj54z3zbyvq9dv93bki2abib36k4v7qzs5i23n")))

(define-public crate-ohos-init-binding-0.0.2 (c (n "ohos-init-binding") (v "0.0.2") (h "1d7fhxg4vz3s75iwbfjaw02v4rfxry51hszh9i529lq86rzlda0k")))

