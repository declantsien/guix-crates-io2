(define-module (crates-io oh os ohos-nj-derive) #:use-module (crates-io))

(define-public crate-ohos-nj-derive-3.4.2 (c (n "ohos-nj-derive") (v "3.4.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fluvio-future") (r "^0.6.0") (f (quote ("timer"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "13n9h6yxr376gi0gc4b2d1gzxmb1gqq0gj1g98yxcschs6r0z0pl")))

