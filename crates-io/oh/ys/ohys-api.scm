(define-module (crates-io oh ys ohys-api) #:use-module (crates-io))

(define-public crate-ohys-api-0.1.0 (c (n "ohys-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "087qdf3xg68rx7jdy3vrbg187i8g739wcwmhjf1gfv7p82fihkin")))

