(define-module (crates-io oh ms ohms) #:use-module (crates-io))

(define-public crate-ohms-0.0.0 (c (n "ohms") (v "0.0.0") (h "1i7q4g0ffk20nzj5jh1rxfdh6d602ypilzi4cab537ingpiml3l0")))

(define-public crate-ohms-0.1.0 (c (n "ohms") (v "0.1.0") (h "1lr4bcshlkkykxsviwp3j4cpvijq6z9mmgpxdzpxgbahgambbzlc")))

(define-public crate-ohms-0.1.1 (c (n "ohms") (v "0.1.1") (h "1zw28z0rqmkfdbn804dqhn0zngn131mwan8s8y9gagsm35flb2k7")))

(define-public crate-ohms-0.1.2 (c (n "ohms") (v "0.1.2") (h "0y0qlzlhbiiwfwq092aligmhcksnxf6m215266imicyf61451hl4")))

(define-public crate-ohms-0.1.3 (c (n "ohms") (v "0.1.3") (h "078rkpc10f5sapwwr04q60bpqb5wxvqy01l2fp02lkydpvbik4mq")))

(define-public crate-ohms-0.1.4 (c (n "ohms") (v "0.1.4") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "13g2nc3mv3r3scg93pxcy0mbxq64nwmf44azd07lg85jsmvdnpsw")))

(define-public crate-ohms-0.2.0 (c (n "ohms") (v "0.2.0") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "19smv36xwcdrxliarh9m18pd7g443j1vy2ws8kvzlsnr5r6mpmbi")))

(define-public crate-ohms-0.3.0 (c (n "ohms") (v "0.3.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1ppa5jifgl8z0n4fwrhvs3ws9c11j7hxxpqk0s967722kpx9j8mh")))

(define-public crate-ohms-0.3.1 (c (n "ohms") (v "0.3.1") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1gh26idqpgkgq3ps9v6w15xw0i2pznwl8z3qzqb9ghyf3hyqbqg8")))

(define-public crate-ohms-0.3.2 (c (n "ohms") (v "0.3.2") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "18b8lwiw7vcjj62qpw9as5yagqrrbdjv1pprnrw03xfalg0lf31c")))

(define-public crate-ohms-0.4.0 (c (n "ohms") (v "0.4.0") (d (list (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1xb8zv9a7ymgjqpav3d4n8gam11mackr241rs28xjfwiiiy1fy99")))

