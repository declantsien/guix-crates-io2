(define-module (crates-io oh sl ohsl) #:use-module (crates-io))

(define-public crate-ohsl-0.6.0 (c (n "ohsl") (v "0.6.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "02ap33qc5c7rpg5qvmjsx0vs2gmj1b9xq7ba6lzxb86dh8r4sim3")))

(define-public crate-ohsl-0.6.1 (c (n "ohsl") (v "0.6.1") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "06hdbb2q4wcmc1jrq34wxhisd184bnirvp82ix5ja75w2dr6ljd1")))

(define-public crate-ohsl-0.6.2 (c (n "ohsl") (v "0.6.2") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "1883pjnxr583kaky9gb1a6h81d35idr4m9q9pmpwg6fp1kl2x1fx")))

(define-public crate-ohsl-0.6.3 (c (n "ohsl") (v "0.6.3") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "1qxqd8vsfij1hf2i2lzcxif7fq82n74rdh7ask68gkyy9iw4b63p")))

(define-public crate-ohsl-0.7.0 (c (n "ohsl") (v "0.7.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0dxyq6cjj02rsh0pnvn2dwmgrpm1hkk71krw22cblvh9gpdki8r6")))

(define-public crate-ohsl-0.7.1 (c (n "ohsl") (v "0.7.1") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0ww0mzxl6fabz410sxmmdbb1ypy4z07wlpdq1r7kds8xqm5bfpgp")))

(define-public crate-ohsl-0.7.2 (c (n "ohsl") (v "0.7.2") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "008i6f9v7jd3pkj469mavch570dn55n4zvng5nw5rcvvfi9jwil7")))

(define-public crate-ohsl-0.7.3 (c (n "ohsl") (v "0.7.3") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "17w9x0h72f2402iwgn4nmxkgp7nwbj8a7c5pwg5w8x7dy92k5mrl")))

(define-public crate-ohsl-0.7.5 (c (n "ohsl") (v "0.7.5") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0yj5fygk77pnapb2p8qd50b9gncq67zpl7bvjab43gnrf0fb4c8n")))

(define-public crate-ohsl-0.7.6 (c (n "ohsl") (v "0.7.6") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h6zkqyk1wxfj5fgg60bm0xx800apli59qqabwm6g7nik57d5bcd")))

(define-public crate-ohsl-0.8.0 (c (n "ohsl") (v "0.8.0") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nn7d3c82sfhrjljc94nn6v3br9rhhw1mh0883j6rj2qpr88jh10")))

(define-public crate-ohsl-0.8.1 (c (n "ohsl") (v "0.8.1") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s5dlmddfby5cqmvcyj7w79hnf948f2in13z7yb8bkj5k78b3yqd")))

(define-public crate-ohsl-0.9.0 (c (n "ohsl") (v "0.9.0") (d (list (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09ml11r4nzhy0z36xxiy56x8y2m0f759i4nar2jwk3zhiariicr5")))

