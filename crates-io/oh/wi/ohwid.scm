(define-module (crates-io oh wi ohwid) #:use-module (crates-io))

(define-public crate-ohwid-0.1.0 (c (n "ohwid") (v "0.1.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (k 0)))) (h "16xlnnsjszqq4qzk3l85cdpk2mb4szy1zqs8a0yiqpxi87d3jsz3") (y #t)))

(define-public crate-ohwid-0.1.1 (c (n "ohwid") (v "0.1.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0ns9wcpn73m2sw50bzia45bpxh128x81jr9h5yp4kns2lc0xp793") (y #t)))

(define-public crate-ohwid-0.1.2 (c (n "ohwid") (v "0.1.2") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "01ig1b708fq79vbiky18n9w1pyg18rl39cqmqhljlr5zbv9qp7li")))

