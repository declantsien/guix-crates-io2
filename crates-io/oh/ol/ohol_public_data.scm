(define-module (crates-io oh ol ohol_public_data) #:use-module (crates-io))

(define-public crate-ohol_public_data-0.1.0 (c (n "ohol_public_data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "1jgzzqyv2n6w9hg6q1q380p7qkxbh45yscyf25n4psfif2silm23")))

