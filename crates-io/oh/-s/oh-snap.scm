(define-module (crates-io oh -s oh-snap) #:use-module (crates-io))

(define-public crate-oh-snap-0.0.1 (c (n "oh-snap") (v "0.0.1") (h "1xh6kfxh2sxdp5i58s3njx0k4is3v5sf1nqh7qqixmnkxl2pq5pb")))

(define-public crate-oh-snap-0.0.2 (c (n "oh-snap") (v "0.0.2") (h "1j24c7ji56gx59qr8wymlgai45hhl336ygcsf7lirh3mcn8vgbs1")))

(define-public crate-oh-snap-0.0.3 (c (n "oh-snap") (v "0.0.3") (h "1rhcvqj8c3250pd0pryc7ji83r1p1ah9x4hhvxkcfz5bavmmidyg")))

(define-public crate-oh-snap-0.0.4 (c (n "oh-snap") (v "0.0.4") (h "0zcp7ayr8nbbba5wbxbs1z1sfzy2pi4hn0wk2s1f5vpl0wxgbr70")))

(define-public crate-oh-snap-0.0.5 (c (n "oh-snap") (v "0.0.5") (h "0d8bzhb6sb1cx06p26r78xclky0iabx4fhqj0bn395m67lrlvr9f")))

