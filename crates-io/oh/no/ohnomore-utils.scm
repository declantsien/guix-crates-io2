(define-module (crates-io oh no ohnomore-utils) #:use-module (crates-io))

(define-public crate-ohnomore-utils-0.5.0 (c (n "ohnomore-utils") (v "0.5.0") (d (list (d (n "conllu") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "ohnomore") (r "^0.5") (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)))) (h "05q9c2jqsvj57vrpagjcp62b91bk6wprls0hh0l3rh4sbr3bhi1l")))

