(define-module (crates-io oh my ohmystock) #:use-module (crates-io))

(define-public crate-ohmystock-0.1.0 (c (n "ohmystock") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0az29lnallqp35cgbvam585k449i22hs4rm29whkzva5ncyv75yb") (y #t)))

(define-public crate-ohmystock-0.1.1 (c (n "ohmystock") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "1m74zf10h34hi9rry0l4ib62qai0yw28xa4hpl8y7svwip2dbx9a") (y #t)))

(define-public crate-ohmystock-0.1.2 (c (n "ohmystock") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fvzhnigfq0zxxczs8ak14x6jiay2p5l69iaa6zxw8hawinkhciz") (y #t)))

(define-public crate-ohmystock-0.1.3 (c (n "ohmystock") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xw8cqd7j24yk8c86fn9m6y7icr57l7f0h6jkskaz61awc6s9pv2") (y #t)))

(define-public crate-ohmystock-0.1.4 (c (n "ohmystock") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0r5pqfrvh3yzylixarymfffmn415jm85xkpvfxqlj40nrdkym0rn") (y #t)))

(define-public crate-ohmystock-0.1.5 (c (n "ohmystock") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "073389j2w83cqaspjxwf1b0nkwwaw39flangp9dl5p5lgspqci0z")))

