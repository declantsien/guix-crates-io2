(define-module (crates-io oh my ohmysmtp) #:use-module (crates-io))

(define-public crate-ohmysmtp-0.1.0 (c (n "ohmysmtp") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "email-address-parser") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (d #t) (k 0)))) (h "0ssh317c0r1cif5lb8acii7mlky2051mkr0mfc0asrq90s2p3xi0") (f (quote (("email-validation" "email-address-parser"))))))

(define-public crate-ohmysmtp-0.1.1 (c (n "ohmysmtp") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "email-address-parser") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (d #t) (k 0)))) (h "1q018s782lv4n120p9636pfgmh7yin7n4sm8yim8qpmysri1cwmp") (f (quote (("email-validation" "email-address-parser"))))))

