(define-module (crates-io oh ka ohkami_macros) #:use-module (crates-io))

(define-public crate-ohkami_macros-0.1.0 (c (n "ohkami_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0da2m1vpclg6p0i59hvr2mbwdgih93h5bms2dfjl6yhpb1x2zss4")))

(define-public crate-ohkami_macros-0.1.1 (c (n "ohkami_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02dsnxylxb6jdnpdl0bb0p2q5r5hvicxiw2s4q0khzk3l48hr8d4")))

(define-public crate-ohkami_macros-0.2.0 (c (n "ohkami_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cdbxl3qhh9930ri4kh1hnpmqp2cngpciv2h86i237fipgw7vbca")))

(define-public crate-ohkami_macros-0.3.0 (c (n "ohkami_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s904jlr942ijdfjpnmrflcacnj88iyfqyqb34hs9n693hx756aj")))

(define-public crate-ohkami_macros-0.4.0 (c (n "ohkami_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sm44m6a7fi4s4mv1ppsv15dxx2ysmklhildr9yf1nphvnfkdwq3")))

(define-public crate-ohkami_macros-0.5.0 (c (n "ohkami_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17jqrylf98xi5g1ncj4b0vhja213v8piqq3bq7dxyclda4l33hll")))

(define-public crate-ohkami_macros-0.5.1 (c (n "ohkami_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dn74s242ldjb7bvqdxd9vrfchl8bfnmyzl0hq88cb9hmv4izcw6")))

(define-public crate-ohkami_macros-0.5.2 (c (n "ohkami_macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f9ziq77wj7007brqvikinqxg3qjiwx2dwhhhis6q4n03g7qc4dg")))

(define-public crate-ohkami_macros-0.6.0 (c (n "ohkami_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dvd4zx6kh8z4yshchqnkcnx9xrpx47h91dhcjprhhrwd5jrmh02")))

(define-public crate-ohkami_macros-0.7.0 (c (n "ohkami_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikhlrziya3zdgxxa5si9ff66g8yz8cm8kfly8nx1q73b418xn3c")))

(define-public crate-ohkami_macros-0.7.1 (c (n "ohkami_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06bmbkvwbn5r4h4ynmkqq4787hd40v9b2gzfy1i74kp8i1bl7nqw")))

(define-public crate-ohkami_macros-0.7.2 (c (n "ohkami_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (f (quote ("parse"))) (o #t) (k 0)))) (h "0v0d4ln1bhk40cpqljair7d0j7wp2rzfs9j42nvh7fw9zlrzi45p") (f (quote (("default" "DEBUG") ("DEBUG" "worker")))) (s 2) (e (quote (("worker" "dep:toml"))))))

(define-public crate-ohkami_macros-0.7.3 (c (n "ohkami_macros") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (f (quote ("parse"))) (o #t) (k 0)))) (h "01mvhv58bgi5nghmv3j828wwlryzzvqb0g17fz064an1x3blrvgz") (s 2) (e (quote (("worker" "dep:toml"))))))

