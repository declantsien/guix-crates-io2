(define-module (crates-io oh ka ohkami_lib) #:use-module (crates-io))

(define-public crate-ohkami_lib-0.1.0 (c (n "ohkami_lib") (v "0.1.0") (h "02zn06wv00zvaykaiyqvx6jpa24ydyw6pnrg92zj1gg2kmz4lky8")))

(define-public crate-ohkami_lib-0.1.1 (c (n "ohkami_lib") (v "0.1.1") (h "18725al90g0ip0yzrm35swas1lf86wafpiy5k8fvkhkk1ym8gwcb")))

(define-public crate-ohkami_lib-0.2.0 (c (n "ohkami_lib") (v "0.2.0") (d (list (d (n "byte_reader") (r "^3.0") (f (quote ("text"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i2xh5h3bik8hmf1m7f3ypr9c3p8hmxx1hn88pbdfl9hi57macbj")))

(define-public crate-ohkami_lib-0.2.1 (c (n "ohkami_lib") (v "0.2.1") (d (list (d (n "byte_reader") (r "^3.0") (f (quote ("text"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wkv44yx0r9j6pwwb7j3nk72d47h4d26nwbw2lvkx2wkcjlwq1m3")))

(define-public crate-ohkami_lib-0.2.3 (c (n "ohkami_lib") (v "0.2.3") (d (list (d (n "byte_reader") (r "^3.0") (f (quote ("text"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08g1fpcc5w7ia6mzqy8j2yfrrrj1qsl3qy06qfpp4yn6qv2fccs4")))

