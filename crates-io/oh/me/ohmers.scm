(define-module (crates-io oh me ohmers) #:use-module (crates-io))

(define-public crate-ohmers-0.1.0 (c (n "ohmers") (v "0.1.0") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rmp") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "stal") (r "^0.1.2") (d #t) (k 0)))) (h "0ppa525zfm4lpff300488zhqs9nxqgh1jkpdaxnvw9yh9ri318vk")))

(define-public crate-ohmers-0.1.1 (c (n "ohmers") (v "0.1.1") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rmp") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "stal") (r "^0.1.2") (d #t) (k 0)))) (h "0annr3r59vga85zwdyyygwnhvc7x1cih6z8rq4maqixj74n6chln")))

