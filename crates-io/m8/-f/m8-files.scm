(define-module (crates-io m8 -f m8-files) #:use-module (crates-io))

(define-public crate-m8-files-0.1.0 (c (n "m8-files") (v "0.1.0") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1ajppcqf718cc924405mmfqy6i7ngprq7zk74mwlcwqgqsym7mcy")))

(define-public crate-m8-files-0.1.1 (c (n "m8-files") (v "0.1.1") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0jz62xxmfpjri2aniqj8dq85a9if0k96rplvx197yjrfgfh8z0mw")))

(define-public crate-m8-files-0.1.2 (c (n "m8-files") (v "0.1.2") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1fi9i61mn8jrkwfnn8xbvp1kkl8d2gb2x4fpnv8nwb8qrm1vl5il")))

(define-public crate-m8-files-0.1.3 (c (n "m8-files") (v "0.1.3") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1z9zms3a7jyvclbbrra3ybsl3xs34yfxlq5wkigr2nr9i0a24kzc")))

(define-public crate-m8-files-0.2.0 (c (n "m8-files") (v "0.2.0") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1klw5nx9b5bb6f24k757fz62ahz8claa4s6vgj6mrks3nk6nian0")))

