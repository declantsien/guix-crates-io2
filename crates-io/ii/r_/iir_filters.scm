(define-module (crates-io ii r_ iir_filters) #:use-module (crates-io))

(define-public crate-iir_filters-0.1.0 (c (n "iir_filters") (v "0.1.0") (d (list (d (n "more-asserts") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "stdext") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1b45h6prxpkh2iraawwx8f1d1bw64dpx474ggvj1rhilaiw3b9vj")))

(define-public crate-iir_filters-0.1.1 (c (n "iir_filters") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "stdext") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0glndj925pdyykdysd8kpk9x964v0hiqglzj5rfp0wh9rkhkldqc")))

(define-public crate-iir_filters-0.1.2 (c (n "iir_filters") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "stdext") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0bfryvs90i0pgliiv5vxwhizajqnl6ya8ib4nrm53mx4j81v1dj1")))

(define-public crate-iir_filters-0.1.3 (c (n "iir_filters") (v "0.1.3") (d (list (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "stdext") (r "^0.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1ll1nvfslfw9issmvy9g18k74az4mqp4vzarvw5wwp7b2njgm36r")))

