(define-module (crates-io ii if iiif) #:use-module (crates-io))

(define-public crate-iiif-0.1.0 (c (n "iiif") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros"))) (d #t) (k 0)))) (h "1vzfhfwmdn7mwa5krr85s3xqjkkbmxpsw7lg8gpvm6w82zh3m8lg")))

(define-public crate-iiif-0.1.1 (c (n "iiif") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros"))) (d #t) (k 0)))) (h "185s9gjfh58cfchwvd6phlhjh9xz57bddycgnmvrsyc4zj71pzwk")))

