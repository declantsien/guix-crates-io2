(define-module (crates-io ii rs iirs) #:use-module (crates-io))

(define-public crate-iirs-0.9.0 (c (n "iirs") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divsufsort") (r "^2.0.0") (d #t) (k 0)) (d (n "elapsed-time") (r "^0.1.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)))) (h "0lh07v5nzvhy96q5xkz6wj2py85mrhlrld758a7nl0wyr0yda2xm")))

(define-public crate-iirs-0.9.1 (c (n "iirs") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divsufsort") (r "^2.0.0") (d #t) (k 0)) (d (n "elapsed-time") (r "^0.1.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)))) (h "010yr2mc36jp48x9z6dl63asrpqlmmzzw5z9l4mgbckl5vf4s657")))

(define-public crate-iirs-0.9.2 (c (n "iirs") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divsufsort") (r "^2.0.0") (d #t) (k 0)) (d (n "elapsed-time") (r "^0.1.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)))) (h "1zjz52765w8f01nhj05m09jmvc9cvspw7cc3l8k63iaba4svr1gs")))

