(define-module (crates-io ar a_ ara_source) #:use-module (crates-io))

(define-public crate-ara_source-0.1.0 (c (n "ara_source") (v "0.1.0") (h "12fpci7322hda227mh8w6c4l0hczzi3mdzfflq50s7x225rdl6mi")))

(define-public crate-ara_source-0.2.0 (c (n "ara_source") (v "0.2.0") (h "0yjg3bp7k1kg3r524rq9scrhg7nl8jvyqnw58z3sw0nr2yva013m")))

