(define-module (crates-io ar ae arae) #:use-module (crates-io))

(define-public crate-arae-0.1.0 (c (n "arae") (v "0.1.0") (d (list (d (n "loom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ybs31xzrwxy51sbwn1qrcwc8bb005m4vv5mr1ahpvdvlzhnrg1r") (f (quote (("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-arae-0.2.0 (c (n "arae") (v "0.2.0") (d (list (d (n "loom") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1qg2ywjgdz8ky83pgl10jyp91jifk90qrqbwrcbcn9pj3pa76y6g") (f (quote (("default" "atomic") ("atomic"))))))

