(define-module (crates-io ar cb arcball) #:use-module (crates-io))

(define-public crate-arcball-0.1.0 (c (n "arcball") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.108") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)))) (h "0mfvk182h7g1cga5zgh7nnz9zk3krfnrn4ncbay82bgzsnicbrw4") (f (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.2.0 (c (n "arcball") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.14.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.129") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)))) (h "1z8ha56rm0xgzwsc99wjkchs5rq52vp4xpxma20jmkzqg5jcrmc4") (f (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3.0 (c (n "arcball") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.148") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)))) (h "1nhqm3vylyjmf9g8b0c343rkb4bhp5bjqkm4skfjzfr4xqyiqgcf") (f (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3.1 (c (n "arcball") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.148") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)))) (h "1q00fqy6s0a5kck9jvjvznak3cx8g5wllw6p8gb62mllh6la4ma0") (f (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3.2 (c (n "arcball") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "glium") (r "^0.23.0") (d #t) (k 2)))) (h "0y4kfn7gsahzkr1jnvi3fjrdz7ppncg4xbmx3brcz73n8b8hwkl1")))

(define-public crate-arcball-0.3.3 (c (n "arcball") (v "0.3.3") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "glium") (r "^0.23.0") (d #t) (k 2)))) (h "1iy74ya7hv25064vg1ka0y56p6hjn00f18pp6sjk7570rzwgxj2p")))

(define-public crate-arcball-1.0.0 (c (n "arcball") (v "1.0.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "glium") (r "^0.21.0") (d #t) (k 2)))) (h "1bls6nb82vx4pml3rwvhc1vav04pb6c1lkqzl63yhr3yqcz47qvm")))

(define-public crate-arcball-1.1.0 (c (n "arcball") (v "1.1.0") (d (list (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "glium") (r "^0.28.0") (d #t) (k 2)))) (h "0d92hsv6nwh4jy0j5qbwzcnrn6rn53wrg3021jlhxl49ix0f2cjv")))

