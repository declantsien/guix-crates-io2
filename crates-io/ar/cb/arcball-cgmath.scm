(define-module (crates-io ar cb arcball-cgmath) #:use-module (crates-io))

(define-public crate-arcball-cgmath-0.1.0 (c (n "arcball-cgmath") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)))) (h "0q3majhj87p21gd1xxpkilbixyry2k4a18jslhzicx8zknbc08af")))

(define-public crate-arcball-cgmath-0.2.0 (c (n "arcball-cgmath") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)))) (h "1sh748ps87n8wxxxbak8dzyvblx0l6x0k3nyj9xcfq3pdl2d57wn")))

(define-public crate-arcball-cgmath-0.2.1 (c (n "arcball-cgmath") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)))) (h "10yw2gamzsidcsv0diz3mx8lgaqrbkrjibww6wy5hi5jn3kxgkdk")))

(define-public crate-arcball-cgmath-0.2.2 (c (n "arcball-cgmath") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)))) (h "0anxxf401f15484qvpmd3lqm6i378paq5q2hi5k1hrcgbspwgcgm")))

(define-public crate-arcball-cgmath-0.3.0 (c (n "arcball-cgmath") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0pnqsxldk13nczbzwhgqkn8s93f9ql1m21awliyj8dn88s4d28wz")))

(define-public crate-arcball-cgmath-0.3.1 (c (n "arcball-cgmath") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0dpgdw2ifmcpnyiq1dw4lp5zj1prs4yy1s4vs6z2mgarivx3jaxy")))

(define-public crate-arcball-cgmath-0.3.2 (c (n "arcball-cgmath") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1rmvx1amc7dr3l8fg81kdhd1swi5zd4i0q3wf6pclmg8m3hp6xgw")))

(define-public crate-arcball-cgmath-0.3.3 (c (n "arcball-cgmath") (v "0.3.3") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1qmhws9i1ndj91dm9r1ln8w5pnrvklfv12abirbkh3h9k0sipb0a")))

(define-public crate-arcball-cgmath-0.3.4 (c (n "arcball-cgmath") (v "0.3.4") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "17frkq7bqrmcll2vyc5iazdgpj6iazxb8asid51n24d6vy85vmxw")))

(define-public crate-arcball-cgmath-0.4.0 (c (n "arcball-cgmath") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0b3dcx03c6w754573020wfn75vqsqfafgj36ljrslz2n0w0x6ing")))

