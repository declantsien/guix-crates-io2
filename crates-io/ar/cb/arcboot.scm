(define-module (crates-io ar cb arcboot) #:use-module (crates-io))

(define-public crate-arcboot-0.1.0 (c (n "arcboot") (v "0.1.0") (h "0zb2mn1yzwpbsli28j03jdvmicyl9n40lc0llwxm8wmkf191cnhb")))

(define-public crate-arcboot-0.1.1 (c (n "arcboot") (v "0.1.1") (h "0zy5f6d1ngaarrjfnbs8hwx8mr9hb6i5g1pb58729lmzwz04j7pz")))

(define-public crate-arcboot-0.1.2 (c (n "arcboot") (v "0.1.2") (h "13f063mi8vvh0zahxkw2xd72yv0rjvbhrq8m3c18wmw8x93fda29")))

(define-public crate-arcboot-0.1.3 (c (n "arcboot") (v "0.1.3") (h "1sa5qxgb75v25cfj9w7bi0fap2m4ayz8bs2cbvh85s14li7apckn")))

(define-public crate-arcboot-0.1.4 (c (n "arcboot") (v "0.1.4") (h "1rp4f19f1j3pn6sd6czq0jj3r1nn4acrhs60xplm9b7z7l80av3c")))

