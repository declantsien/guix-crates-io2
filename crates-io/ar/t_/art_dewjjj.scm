(define-module (crates-io ar t_ art_dewjjj) #:use-module (crates-io))

(define-public crate-art_dewjjj-0.1.0 (c (n "art_dewjjj") (v "0.1.0") (h "1a9yl2l1bcfy3c9g5j61ndqizw20nb8ap993f467vvkyg59q66kk")))

(define-public crate-art_dewjjj-1.1.1 (c (n "art_dewjjj") (v "1.1.1") (h "1pkjvz7c9hjyxqk2gp9iizgq5wwgi8gy4967h8agk9r1b1al36bf")))

(define-public crate-art_dewjjj-1.1.8 (c (n "art_dewjjj") (v "1.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c7l1l239nmvr30drmg82zfrlz90wxi6kkplvqnch0csxzvc1wid")))

