(define-module (crates-io ar t_ art_crate_substantial) #:use-module (crates-io))

(define-public crate-art_crate_substantial-0.1.8 (c (n "art_crate_substantial") (v "0.1.8") (h "08qax8jk1hm0jy1s48lnlvhz1kf7kif814v50xzly56x00pc5525") (y #t)))

(define-public crate-art_crate_substantial-0.1.9 (c (n "art_crate_substantial") (v "0.1.9") (h "1gzvhrlrl44jbijl9r1ms39rn2kllr7lnkb6az2i45y39dnill24")))

