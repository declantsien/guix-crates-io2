(define-module (crates-io ar t_ art_xl) #:use-module (crates-io))

(define-public crate-art_xl-0.1.0 (c (n "art_xl") (v "0.1.0") (h "18qa1dm39a67y2aa6pbsysbrmdfs397g83cjc6x0b6k65azcn2z8") (y #t)))

(define-public crate-art_xl-0.2.0 (c (n "art_xl") (v "0.2.0") (h "0f2r9y9ypigpk8md7d3klix7cdpyl8q7niiszq5nsgxl730z39d9") (y #t)))

