(define-module (crates-io ar t_ art_dx) #:use-module (crates-io))

(define-public crate-art_dx-0.1.0 (c (n "art_dx") (v "0.1.0") (h "0jf1fy7jlwz9mql46wbjyzlm76vmy0w4n9yzm4y2lll8z317njpm") (y #t)))

(define-public crate-art_dx-0.2.0 (c (n "art_dx") (v "0.2.0") (h "0s8h9w8gc7a6n5p6zfkhvpf8yrp12yy30q1yg4p59q2lb93i3zm7")))

