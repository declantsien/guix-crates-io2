(define-module (crates-io ar t_ art___) #:use-module (crates-io))

(define-public crate-art___-0.1.0 (c (n "art___") (v "0.1.0") (h "0xjf0a0zq86p2i38nx4cyvvpds9460c1hcpwmxfl1md49j71pgw3")))

(define-public crate-art___-0.1.1 (c (n "art___") (v "0.1.1") (h "1ynlk53v1yqyppii643zw4zbpldr93s6j872256vww9qb3hdbkz9")))

