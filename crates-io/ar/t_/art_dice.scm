(define-module (crates-io ar t_ art_dice) #:use-module (crates-io))

(define-public crate-art_dice-0.0.1 (c (n "art_dice") (v "0.0.1") (h "1i6arx5chrwzzf7img1gr41682c9bncxcm17bhwprpl23c09p7bl")))

(define-public crate-art_dice-0.0.2 (c (n "art_dice") (v "0.0.2") (h "1gs4pqljyi12cpv8p7cp5zkd93c29w9dcdmbs96g9i0zvi8dx9sg")))

(define-public crate-art_dice-0.1.0 (c (n "art_dice") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1d9hp37ha4f6h61aq32n3gvcbvfv5d73gxssk04xx2rjrglrg78i")))

(define-public crate-art_dice-0.2.0 (c (n "art_dice") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0i3hgj1wq202l0wpflddfjyy01myzvbz8vg90fxia9fdp9h2nq1n")))

(define-public crate-art_dice-0.3.0 (c (n "art_dice") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0r7rzb815s0qpnajzn8qjinf0jhf42ypji1bixbcfgydvn414n2y")))

(define-public crate-art_dice-0.4.0 (c (n "art_dice") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1ywlan3lksmm1qhd583qxsfmdnjlivb5sb2hq7jycalfwps52dvz")))

(define-public crate-art_dice-0.4.1 (c (n "art_dice") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "04bqg4j6aw2c0pb80ax5n95w92i5xkrhqsx74jg9rchxsr2qfa55")))

