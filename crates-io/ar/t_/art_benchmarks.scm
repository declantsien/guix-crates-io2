(define-module (crates-io ar t_ art_benchmarks) #:use-module (crates-io))

(define-public crate-art_benchmarks-0.1.0 (c (n "art_benchmarks") (v "0.1.0") (h "0q0j2wd1k40wakijy7lhv8rnxdrdkc3974s04p4idl5f9dd0nyzp")))

(define-public crate-art_benchmarks-0.2.0 (c (n "art_benchmarks") (v "0.2.0") (h "0356yy10kkhzg34gd2gzhk1zzbi1rlqzk9wdjarbap76qk0k47b0")))

(define-public crate-art_benchmarks-0.3.0 (c (n "art_benchmarks") (v "0.3.0") (h "1igvpny76h0k71dgm9a4iw79lpz5akdw8r0vpm7rn5vi3gyd5zkm")))

