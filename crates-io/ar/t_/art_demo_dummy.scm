(define-module (crates-io ar t_ art_demo_dummy) #:use-module (crates-io))

(define-public crate-art_demo_dummy-0.1.0 (c (n "art_demo_dummy") (v "0.1.0") (h "13s94qb7jnfgcygycia810dz41a2sl3ac9wf65mdkhpdqprd16gs")))

(define-public crate-art_demo_dummy-0.1.2 (c (n "art_demo_dummy") (v "0.1.2") (h "1m6la51z17ngd84hash8ginmahdw9w28cylfl5bx0jbn6bj16pdl")))

(define-public crate-art_demo_dummy-0.1.3 (c (n "art_demo_dummy") (v "0.1.3") (h "1k5h5cx6da8hrkm2z23yngl9awpfid3cfxxgsd9rkq03p0xpdn2n") (y #t)))

