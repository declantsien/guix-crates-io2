(define-module (crates-io ar t_ art_lib) #:use-module (crates-io))

(define-public crate-art_lib-0.1.0 (c (n "art_lib") (v "0.1.0") (h "18s4372zx8cci8jid4bf71yf32xfxgf1dcmppzd0nm89habz4k1r")))

(define-public crate-art_lib-0.1.1 (c (n "art_lib") (v "0.1.1") (h "1zdga97c6ispw0ycr0bcy8xy4wzm8yvaf5g9q3pyak35vayhga1w")))

(define-public crate-art_lib-0.1.2 (c (n "art_lib") (v "0.1.2") (h "0w7iajs175kb0dxhx4za57q8h08n26y2j93bqmilfdsjx6wh5x2p")))

