(define-module (crates-io ar t_ art_mix_color) #:use-module (crates-io))

(define-public crate-art_mix_color-0.1.0 (c (n "art_mix_color") (v "0.1.0") (h "1a9nr67p1wqf98b0h98bvyr06rhfk2l2zin4fbw47hpijmca7gcc")))

(define-public crate-art_mix_color-0.1.1 (c (n "art_mix_color") (v "0.1.1") (h "03jsm7jpyfbn38yn4qyca31c7rhc4a50h5syfi4jahn9i7bqd20l")))

