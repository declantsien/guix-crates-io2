(define-module (crates-io ar km arkmsm) #:use-module (crates-io))

(define-public crate-arkmsm-0.3.0-alpha.1 (c (n "arkmsm") (v "0.3.0-alpha.1") (d (list (d (n "ark-bls12-381") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)))) (h "11r3jc6760ply8asyqqxx4vai7pqafvx5wf6yqql3nzbz80g9v70") (y #t) (r "1.63")))

(define-public crate-arkmsm-0.3.0-alpha.2 (c (n "arkmsm") (v "0.3.0-alpha.2") (d (list (d (n "ark-bls12-381") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)))) (h "0pvwbnbdik52jm27lqz27clzs92bq30xjnac47gzxhp40j1s04rf") (y #t) (r "1.63")))

(define-public crate-arkmsm-0.3.0-alpha.3 (c (n "arkmsm") (v "0.3.0-alpha.3") (d (list (d (n "ark-bls12-381") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)))) (h "0hgqsv3lv7lqni3p7zs0pkz4scmb7vjashbbgcd68q4wg9ajgkga") (y #t) (r "1.63")))

