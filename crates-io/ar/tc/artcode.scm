(define-module (crates-io ar tc artcode) #:use-module (crates-io))

(define-public crate-artcode-1.0.0 (c (n "artcode") (v "1.0.0") (d (list (d (n "cargo-release") (r "^0.13.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "024qfj7jmlyy7gmgfh8l0d5b5fl0apgbw6dk3xfmw202vr6gcq1n")))

(define-public crate-artcode-1.3.0 (c (n "artcode") (v "1.3.0") (d (list (d (n "cargo-release") (r "^0.13.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0jryxv04gdx8mdy4kk3yfxkm6wplz4hn4d8z8lacmlbwg66xy5x8")))

(define-public crate-artcode-1.3.1 (c (n "artcode") (v "1.3.1") (d (list (d (n "cargo-release") (r "^0.13.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0z63bx1xriwa7npfgmrsm5w1lii9s6sf56cqdg75s59hlxkl4myf")))

(define-public crate-artcode-2.0.0 (c (n "artcode") (v "2.0.0") (d (list (d (n "cargo-release") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qd0jv9kssf7czpn25f0cxpy3prqyq22xfmcn1z7qwb07v9linyc")))

(define-public crate-artcode-2.1.0 (c (n "artcode") (v "2.1.0") (d (list (d (n "cargo-release") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "161khsvzjq6i7kzxpf0hycb2210p7rdz7n0rg1ldbv1cf92j4hc4")))

(define-public crate-artcode-2.2.0 (c (n "artcode") (v "2.2.0") (d (list (d (n "cargo-release") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l5iia2iy42aycqxj9wqz5knkxdiqax5kcihagvn66krc8cfa9vw")))

(define-public crate-artcode-2.2.2 (c (n "artcode") (v "2.2.2") (d (list (d (n "cargo-release") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sn48jjh2w5m3pak5l0d5b44q7ff0lxf4jvnzbxik1dqnbyra7mv")))

