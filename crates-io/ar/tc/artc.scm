(define-module (crates-io ar tc artc) #:use-module (crates-io))

(define-public crate-artc-0.1.0 (c (n "artc") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cstr-argument") (r "^0.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (d #t) (k 0)) (d (n "gpgme") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.16") (d #t) (k 0)))) (h "14ykyhdzzkqqaw77482iny6jp6sd3ar7k2v9c84hns0qg65s6nm7")))

(define-public crate-artc-0.1.1 (c (n "artc") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cstr-argument") (r "^0.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (d #t) (k 0)) (d (n "gpgme") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.16") (d #t) (k 0)))) (h "1v0x1lx6hppf3ir82p1v2jqd2xjycrqb5m7im3z3lna2wz1jbys8")))

