(define-module (crates-io ar gd argdata) #:use-module (crates-io))

(define-public crate-argdata-0.1.0 (c (n "argdata") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)))) (h "0y0xfg3bs1gav98zl9hqnrkyx72pang8a4yww030c603m6ihay6q")))

(define-public crate-argdata-0.1.1 (c (n "argdata") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0ygps5gc36ibfbzi613fbc0marmxxws1y9i9h8jfyn1wyc5yqwp5") (f (quote (("nightly"))))))

(define-public crate-argdata-0.1.2 (c (n "argdata") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.3") (f (quote ("i128"))) (d #t) (k 0)))) (h "1yphhxbfjdl0lyvmz35q9xfzh9jbfqz4pxbj3mc0jw32j4ldawwj") (f (quote (("nightly"))))))

