(define-module (crates-io ar ti artifact) #:use-module (crates-io))

(define-public crate-artifact-0.0.1 (c (n "artifact") (v "0.0.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0zz96m4ra10dgi683bk2p4qqigassnlbhvam26373aib35rs8g1f") (f (quote (("no-failure-logs"))))))

(define-public crate-artifact-0.0.2 (c (n "artifact") (v "0.0.2") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1z4ymhvgnyxkwzd08r4ms1fgaph9g9q6136js21d42lny4d0g7wf") (f (quote (("no-failure-logs"))))))

(define-public crate-artifact-0.0.3 (c (n "artifact") (v "0.0.3") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1bp3awb6ga5m8a8rqfwhhgw5rwyqj59g5nsvbdn2nhrc3sslwx64") (f (quote (("no-failure-logs"))))))

(define-public crate-artifact-0.0.4 (c (n "artifact") (v "0.0.4") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1jg3h4ncxm6pnhzj0fwxb9k9ca29vlw12glk34fx5613qkgdgg8s") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.5 (c (n "artifact") (v "0.0.5") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0an9lh88cli3qd2m3fr511v7s3p72bys5pyv0r5kk1ayhjya4c91") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.7 (c (n "artifact") (v "0.0.7") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0jzal90zfhr63d8ysrf82gs72n4ggqxbg9b69661zx667l8qqar2") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.8 (c (n "artifact") (v "0.0.8") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "17xxnr1sk3f8kgdaxh484wpb124qnazxibsc4f1by9clg2j9g1vn") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.9 (c (n "artifact") (v "0.0.9") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0mqscb9zi1252y1bsfq7gpnll6k0w79b4wqswhcvkppl3i9w9am0") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.11 (c (n "artifact") (v "0.0.11") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "124dh9hvvhw179hfsyj7ibwkh61aklk6y5vnh5qmd2d9nfcmvln2") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.12 (c (n "artifact") (v "0.0.12") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "15qz0zv1wiqdwxz3z0hpcqa0dsa293s7b3ayfqp3iclc9xcrb4kh") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.13 (c (n "artifact") (v "0.0.13") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "0lxl4jmrqwvalvxy3w21yarf386kx9l1hd75sbnsr21d2csasvyv") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.14 (c (n "artifact") (v "0.0.14") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "01zvavc2nhnkr85kk55lmld5akmglyl9w61n49rdvhbqb0x4xlg2") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.15 (c (n "artifact") (v "0.0.15") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1szwzhil809p2kr6wdfcjji2nykcz3q7s548w2dgr9ml9vrz0pz6") (f (quote (("no-failure-logs") ("disable"))))))

(define-public crate-artifact-0.0.17 (c (n "artifact") (v "0.0.17") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)))) (h "1hwsw9vfr16jd3b7a0zimqgdsjnwwjz0jc6qxkjq110vcfdmysl1") (f (quote (("no-failure-logs"))))))

(define-public crate-artifact-0.0.18 (c (n "artifact") (v "0.0.18") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "0xff5hf65fg7r0wwvx28rnx3cygzwk5iyrk72101p5irhbhac046") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.0.19 (c (n "artifact") (v "0.0.19") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "15szw4jxqiyssslzgr0848vzwd98r4ajz00jnsvlwv367ac2cyyy") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.0.20 (c (n "artifact") (v "0.0.20") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "0mgplmqalk4dw96352486c24pgmm7iq2si4887ia2ql3xdpx8yrr") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.1.0 (c (n "artifact") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "time") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "02mdd5qszqnf14yxicljaizkdzm2i05gb6glidg87kifvwy5vp16") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.2.0 (c (n "artifact") (v "0.2.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "time") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1bn4ndhjazsizpk8pdkpq7g3b3mfxly7b9hnk9xfjd9qlim9kkdf") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.2.1 (c (n "artifact") (v "0.2.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "time") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1l102vcmrz383v61c777wpk1py07wzlxvfb4ypqqbbxbmnwndr7y") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.2.2 (c (n "artifact") (v "0.2.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "time") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "0kcmph0qv78gx87dwnl1gr1f4i3sv6i7zw43mllagpfp6s6d1phy") (f (quote (("no-failure-logs") ("default" "time"))))))

(define-public crate-artifact-0.2.4 (c (n "artifact") (v "0.2.4") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "time") (r "0.1.*") (o #t) (d #t) (k 0)))) (h "1560bdbl1vlnpbh4ydhhc9cy18ynkyhlnq96vl2in26g8ccr2k5z") (f (quote (("no-failure-logs") ("default" "time"))))))

