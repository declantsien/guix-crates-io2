(define-module (crates-io ar ti artifact_serde) #:use-module (crates-io))

(define-public crate-artifact_serde-0.1.0 (c (n "artifact_serde") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00j9ll8ncx0i2aw7ss2ml0n3v1p9s6j17fvsgd06hg63vjzclzax")))

(define-public crate-artifact_serde-0.1.1 (c (n "artifact_serde") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y44d3nlh9ff9vpp4gmmxhciyiy492rlc77wk6z03lqgd5a2vjlk")))

(define-public crate-artifact_serde-0.1.2 (c (n "artifact_serde") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cqrgjl7dy10nzy83yksnm1c3bfn0i7h3clbpc6k5rynh0x4mpzz")))

(define-public crate-artifact_serde-0.1.3 (c (n "artifact_serde") (v "0.1.3") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pk5h8l851l381ayrhzkzn84jxcvwbvf0wh0n8ivjrmr5j6hk1fj")))

(define-public crate-artifact_serde-0.1.4 (c (n "artifact_serde") (v "0.1.4") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "171kc8gmpsmbvqv6ipcz6bd8nfzfdm6sq5i6whnrk4zkbgs4jmvf")))

(define-public crate-artifact_serde-0.2.0 (c (n "artifact_serde") (v "0.2.0") (d (list (d (n "ammonia") (r "^1.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11j36ipyqwr14fd2fy4760i33q047hpjhwlx11zgw1fb2p7mniyy")))

(define-public crate-artifact_serde-0.2.1 (c (n "artifact_serde") (v "0.2.1") (d (list (d (n "ammonia") (r "^1.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wysk5pv2whhj5d1a6b24lxda0mskmjy82gn6sbysl465k6vyj7l")))

(define-public crate-artifact_serde-0.3.0 (c (n "artifact_serde") (v "0.3.0") (d (list (d (n "ammonia") (r "^1.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rh15s2knb3661l558iq7ll7wxg7wb54wzww5wsxrhn6pv5pyni4")))

(define-public crate-artifact_serde-0.3.1 (c (n "artifact_serde") (v "0.3.1") (d (list (d (n "ammonia") (r "^1.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pj54zjqk57qk7r32cvdvxnb4hy967glp790cfvgqnwjiw1hs5kv")))

