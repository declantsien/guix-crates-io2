(define-module (crates-io ar ti artifact_lib) #:use-module (crates-io))

(define-public crate-artifact_lib-0.1.0 (c (n "artifact_lib") (v "0.1.0") (d (list (d (n "artifact_serde") (r "^0.2.1") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bhwjfrj1rb4g3gcn7zryjxkmicxcyjk2mdllihc62z30nzksm50")))

(define-public crate-artifact_lib-0.2.0 (c (n "artifact_lib") (v "0.2.0") (d (list (d (n "artifact_serde") (r "^0.2.1") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q0rphd8nh22amg1jnfv9kadv0q30xjcmpyrak0pqrf2dkrgjhbp")))

(define-public crate-artifact_lib-0.2.1 (c (n "artifact_lib") (v "0.2.1") (d (list (d (n "artifact_serde") (r "^0.2.1") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jqq0q984r2jc8lr441racfbv92s04iq1m5xiz1z2828laa7r8p7")))

