(define-module (crates-io ar ti artifacts-crate) #:use-module (crates-io))

(define-public crate-artifacts-crate-0.1.0 (c (n "artifacts-crate") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full" "test-util"))) (d #t) (k 2)))) (h "0ry5fprfaysl47khbnxsi632l34p9c935dk8ix9kcq4960ss2lhp")))

