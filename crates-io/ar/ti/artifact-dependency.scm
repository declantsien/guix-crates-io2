(define-module (crates-io ar ti artifact-dependency) #:use-module (crates-io))

(define-public crate-artifact-dependency-0.1.0 (c (n "artifact-dependency") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "06zhv6ggrimnr35zlhva31w5j8xgfpck2nwppk8rwq0whdc56dkf")))

(define-public crate-artifact-dependency-0.1.1 (c (n "artifact-dependency") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "08pckgd16qk09ng9r8sh9nnzmpx9hm0hrv42qysv4lai5whg9rpk")))

(define-public crate-artifact-dependency-0.1.2 (c (n "artifact-dependency") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "0ljfn55pcd723w845b7sbxlx7j9x5mvrgpm6hk2bl3r38fd2fm0g")))

(define-public crate-artifact-dependency-0.1.3 (c (n "artifact-dependency") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "0qlcl46h7lqbjpfw2sds2c776r4dxx5dfq0yrhjmprp5wz8a8sjk")))

(define-public crate-artifact-dependency-0.1.4 (c (n "artifact-dependency") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "11d5n500k98i9574hgknak4lk34093p592nhri72kpcg4vmaahgq")))

(define-public crate-artifact-dependency-0.1.5 (c (n "artifact-dependency") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "10kp3djafw78krd49zb97qrm8p2hnnj3lrf49ym8674rrqjjqkpq")))

(define-public crate-artifact-dependency-0.1.6 (c (n "artifact-dependency") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "0cvdc7by89bbl0a8mmz88qmiqkiqml9icj6g2zp73qlq4jq97il0")))

(define-public crate-artifact-dependency-0.1.7 (c (n "artifact-dependency") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.1") (d #t) (k 0)))) (h "0qdpl74dbihv0966w17z40c0mhfkzcw25v740af5n6qlx5d2yxwk")))

