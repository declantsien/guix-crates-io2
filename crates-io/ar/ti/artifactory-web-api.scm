(define-module (crates-io ar ti artifactory-web-api) #:use-module (crates-io))

(define-public crate-artifactory-web-api-0.0.1 (c (n "artifactory-web-api") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "14pzi7ba3hggbr0ffcwpcjg5a4chz1pvhrz5znq7y5vzb7gayiwr") (f (quote (("default-tls" "reqwest/default-tls") ("default" "reqwest/rustls-tls"))))))

