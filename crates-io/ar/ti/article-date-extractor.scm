(define-module (crates-io ar ti article-date-extractor) #:use-module (crates-io))

(define-public crate-article-date-extractor-0.1.0 (c (n "article-date-extractor") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "select") (r "^0.4.0") (d #t) (k 0)))) (h "0fc5lfhar1hbkbm0kavqs8385yfl6pd0by8xihdyj029sp5h21ax")))

(define-public crate-article-date-extractor-0.1.1 (c (n "article-date-extractor") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "select") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hvav10wx22ns9mhfj4b1cw03v3f14c4s49pgcjr999fwva2y9zg")))

