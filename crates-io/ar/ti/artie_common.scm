(define-module (crates-io ar ti artie_common) #:use-module (crates-io))

(define-public crate-artie_common-0.1.0 (c (n "artie_common") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1za8j945cx6awc4q689a0vvhwz2wnpjq8mf89ia1j12px057q1y4") (r "1.60")))

(define-public crate-artie_common-0.3.0 (c (n "artie_common") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1a35wdfa1fhp4sswbbybag8rsvapvlxvxj844adx8pb85y60yl10") (r "1.60")))

