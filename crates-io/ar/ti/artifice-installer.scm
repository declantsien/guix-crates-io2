(define-module (crates-io ar ti artifice-installer) #:use-module (crates-io))

(define-public crate-artifice-installer-0.1.0-alpha1 (c (n "artifice-installer") (v "0.1.0-alpha1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "manager") (r "^0.1.1") (d #t) (k 0)) (d (n "networking") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0ardnpyabwmjdijls87gi5maznbwgq17m62ir1jvf1qgb7k8rnlv") (y #t)))

(define-public crate-artifice-installer-0.1.0-alpha2 (c (n "artifice-installer") (v "0.1.0-alpha2") (d (list (d (n "artifice-manager") (r "^0.1.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "networking") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1ks4mrvkwplf8wgk8d9s96mkgir9340023c5iksb1nhm2wvzlqai") (y #t)))

