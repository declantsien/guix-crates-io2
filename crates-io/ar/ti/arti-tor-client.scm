(define-module (crates-io ar ti arti-tor-client) #:use-module (crates-io))

(define-public crate-arti-tor-client-0.0.0 (c (n "arti-tor-client") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tor-chanmgr") (r "^0.0.0") (d #t) (k 0)) (d (n "tor-circmgr") (r "^0.0.0") (d #t) (k 0)) (d (n "tor-dirmgr") (r "^0.0.0") (d #t) (k 0)) (d (n "tor-proto") (r "^0.0.0") (d #t) (k 0)) (d (n "tor-rtcompat") (r "^0.0.0") (k 0)))) (h "0rf98ncq3j9lh8mrhx4qx3mywyv36ap9ksl2vfl14vqdqswv7497") (f (quote (("tokio" "tor-rtcompat/tokio") ("experimental-api") ("default" "tokio") ("async-std" "tor-rtcompat/async-std"))))))

