(define-module (crates-io ar ti artiaa_auth) #:use-module (crates-io))

(define-public crate-artiaa_auth-1.0.0 (c (n "artiaa_auth") (v "1.0.0") (d (list (d (n "jsonschema-valid") (r "^0.5.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0qjmg7b820srvlf1sgra7zw6s3b725wb06jdsd67szcgadgrhx94")))

