(define-module (crates-io ar ea areaportal2d) #:use-module (crates-io))

(define-public crate-areaportal2d-0.1.0 (c (n "areaportal2d") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.0") (d #t) (k 0)) (d (n "moogle") (r "^0.4.2") (d #t) (k 0)))) (h "13gwjnj79kj66099gf0q5s2kh8a039rgpl1syy68llyxb7qfxsfk")))

(define-public crate-areaportal2d-0.1.1 (c (n "areaportal2d") (v "0.1.1") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "gridd-euclid") (r "^0.1.0") (d #t) (k 0)) (d (n "moogle") (r "^0.4.2") (d #t) (k 0)) (d (n "symmetric-shadowcasting") (r "^0.2.0") (d #t) (k 0)))) (h "098k5cdzhlz192bl9l5rfr8fczhhw9lgx2xzsll6l73b9klia00y")))

