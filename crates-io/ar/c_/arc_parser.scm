(define-module (crates-io ar c_ arc_parser) #:use-module (crates-io))

(define-public crate-arc_parser-0.1.0 (c (n "arc_parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "06bzf14nqmq6gmcqf4f425azblws7z1ph4lwkgwgf3998nq0hcml")))

(define-public crate-arc_parser-0.2.0 (c (n "arc_parser") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "0mky2w2g3v42vwbbzix213npmn3kaa3rnxnk6siacc1h0mjbc0bk")))

