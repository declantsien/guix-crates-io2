(define-module (crates-io ar c_ arc_from_toml) #:use-module (crates-io))

(define-public crate-arc_from_toml-0.1.0 (c (n "arc_from_toml") (v "0.1.0") (d (list (d (n "arc_convert_lib") (r "^1.0.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.5") (d #t) (k 0)))) (h "19s9n2fl1b288622nbyq1rwck5f5dfzd23iyp1lwkwsz023xvahh") (y #t)))

