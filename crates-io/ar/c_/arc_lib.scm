(define-module (crates-io ar c_ arc_lib) #:use-module (crates-io))

(define-public crate-arc_lib-0.1.0 (c (n "arc_lib") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "opencv") (r "^0.43") (f (quote ("buildtime-bindgen"))) (d #t) (k 0)))) (h "1i9igsmznhspb2d932pfhrc5daclmmhfy5b7pb0dffmpvajkr2m1")))

(define-public crate-arc_lib-0.1.2 (c (n "arc_lib") (v "0.1.2") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "opencv") (r "^0.43") (f (quote ("buildtime-bindgen"))) (d #t) (k 0)))) (h "1gnp0nk3v33ppfd9050pcqd30c58gvkdzfzskjixc33bms8l5c0s")))

(define-public crate-arc_lib-0.1.3 (c (n "arc_lib") (v "0.1.3") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "opencv") (r "^0.43") (f (quote ("buildtime-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03wslca0sgdc6078bzbgq0qhw7kiaql2r1g8n2jv9q2rvlsap4z8")))

