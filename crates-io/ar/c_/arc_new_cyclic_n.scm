(define-module (crates-io ar c_ arc_new_cyclic_n) #:use-module (crates-io))

(define-public crate-arc_new_cyclic_n-1.0.0 (c (n "arc_new_cyclic_n") (v "1.0.0") (h "095fb4p8dwg4m9kwbinc4mi4j4m1wkpyw4snnvg9x7z27awhv36a")))

(define-public crate-arc_new_cyclic_n-1.0.1 (c (n "arc_new_cyclic_n") (v "1.0.1") (h "19ryvqnj23jgg83la1sssx6bq0nzgcp2bs1s7h8zn0pf22x6l4p1")))

(define-public crate-arc_new_cyclic_n-1.0.2 (c (n "arc_new_cyclic_n") (v "1.0.2") (h "0yqw8rfxpd98qlk00d5wv90ksxkgbdrr82ics5nf62r04wb22x3w")))

