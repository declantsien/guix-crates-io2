(define-module (crates-io ar c_ arc_number) #:use-module (crates-io))

(define-public crate-arc_number-0.0.1 (c (n "arc_number") (v "0.0.1") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "1g6kqlb7470bh2q9jfxc0p59nf15ijx393wbd8swc19fi1lgrv53")))

(define-public crate-arc_number-0.1.0 (c (n "arc_number") (v "0.1.0") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "1r0dch81g83xdv32y2rsgslwhibzwdd9idkxnjnbmp5027vbl7ci")))

(define-public crate-arc_number-0.1.1 (c (n "arc_number") (v "0.1.1") (d (list (d (n "num") (r "^0.2.1") (f (quote ("serde" "std"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "13xp3nf9pvkd3afs3jmgqbsbzmjf3hbf052ys4ih62g6wpbyxm8r")))

(define-public crate-arc_number-0.2.0 (c (n "arc_number") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0g5y3fq55pmm34xd8qnp2p6g0dvgjrpzpcldy9qcaqw6q7lj75vw")))

(define-public crate-arc_number-0.3.0 (c (n "arc_number") (v "0.3.0") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde1") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18f3gf8s7mm6g2xz89xdcb7ws7jmm4jqjqbxjhy2q5b1j7y0amf2") (f (quote (("serde" "num/serde") ("rand" "num/rand") ("default"))))))

(define-public crate-arc_number-0.3.1 (c (n "arc_number") (v "0.3.1") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde1") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ax5jkcvaq4fwcic7v4k76g6n9bpb3hsmnmxniw49a2glvp1wavw") (f (quote (("serde" "num/serde") ("rand" "num/rand") ("default"))))))

(define-public crate-arc_number-0.3.2 (c (n "arc_number") (v "0.3.2") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde1") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vhp4krfqgxlj23al6vm79ms9ahasvr1k0afr639lk0s0bafaqsf") (f (quote (("serde" "num/serde") ("rand" "num/rand") ("default"))))))

(define-public crate-arc_number-0.4.0 (c (n "arc_number") (v "0.4.0") (d (list (d (n "bigdecimal") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "serde1") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vyyzwa3qqyzxyiw2x4qz1yaw8xyg24p74qqmpyvv25w07vz1358") (f (quote (("serde" "num/serde") ("rand" "num/rand") ("default"))))))

