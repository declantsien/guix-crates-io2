(define-module (crates-io ar c_ arc_from_yaml) #:use-module (crates-io))

(define-public crate-arc_from_yaml-0.1.0 (c (n "arc_from_yaml") (v "0.1.0") (d (list (d (n "arc_convert_lib") (r "^1.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "13dqdyri3p289m478w43wvjhyb2fln0jd19ddjazkwdzwbgrfvlr") (y #t)))

