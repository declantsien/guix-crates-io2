(define-module (crates-io ar c_ arc_map) #:use-module (crates-io))

(define-public crate-arc_map-0.1.0 (c (n "arc_map") (v "0.1.0") (h "1w2l86xyixxv1m0b9xzvcgcfzmmkzs5i064yvnlf82hrxn40c1bi")))

(define-public crate-arc_map-0.1.1 (c (n "arc_map") (v "0.1.1") (h "0kn7ky2js30vvf0z5vrwszs8pz69dih2nn2zp4flaqqlm981nbxw")))

(define-public crate-arc_map-0.1.2 (c (n "arc_map") (v "0.1.2") (h "07gps5nprm9wapakw8zrnwm1hmc9j1nhbhnlmr77xv7pqmdqq9jx")))

(define-public crate-arc_map-0.1.3 (c (n "arc_map") (v "0.1.3") (h "0zp640qw1bkr1r3ij98g3j2f42gppymqvjirj32iaahybhhhd18s")))

