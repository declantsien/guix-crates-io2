(define-module (crates-io ar om aromatic) #:use-module (crates-io))

(define-public crate-aromatic-0.1.0 (c (n "aromatic") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "menva") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.0") (f (quote ("sqlite" "runtime-tokio" "tls-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0b6xgvsmv80qr7hrlbmmz5bzkq5zg9p0w4875w70ijvpjb9r3ax9")))

(define-public crate-aromatic-0.1.1 (c (n "aromatic") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "menva") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.0") (f (quote ("sqlite" "runtime-tokio" "tls-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0r343cx6hva2rv7i49962nm8swm7826k3qd0vic848f7zsc2a2cb")))

