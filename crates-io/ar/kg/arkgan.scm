(define-module (crates-io ar kg arkgan) #:use-module (crates-io))

(define-public crate-arkgan-0.0.1 (c (n "arkgan") (v "0.0.1") (h "0drgrxzi97h31i8xr7zpxcd4zpmgkvn4wg6yd73l7rpgspys79xd")))

(define-public crate-arkgan-0.0.2 (c (n "arkgan") (v "0.0.2") (h "0i2l5cgnwhzgs10pjiw46nqsasqx3ddhxra5l223niswnqm37adr")))

(define-public crate-arkgan-0.0.3 (c (n "arkgan") (v "0.0.3") (h "1ydv4a2wm80g7fwjg28mf37gz2d4rggvi8w4l711k0fl4ffw1km9")))

(define-public crate-arkgan-0.0.4 (c (n "arkgan") (v "0.0.4") (h "0lvh5cmbylir4j2lryzhwfb4rz4iip3j62ww2fs0gdcd3dvb75dv")))

(define-public crate-arkgan-0.0.5 (c (n "arkgan") (v "0.0.5") (h "0iijabz4q0j1z6jc2w3jfyi6swzw5qy5dz1rilvnv54lvp2n7m5s")))

(define-public crate-arkgan-0.0.6 (c (n "arkgan") (v "0.0.6") (h "174sh2jngnkbd5m3hyj0ymcy954cwxf124niyk1kwcx0d6z2i5kv")))

(define-public crate-arkgan-0.0.7 (c (n "arkgan") (v "0.0.7") (h "1ygqjj7xysnf5jvh4w9bm65wingcqwifip9zwsh7i7q1jv91hs2j")))

(define-public crate-arkgan-0.0.8 (c (n "arkgan") (v "0.0.8") (h "1jh7j6absng3393gihp0fz7gf3rzy9qvhyy9b06hap8dlplnw7k0")))

