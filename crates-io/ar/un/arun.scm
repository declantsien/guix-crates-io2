(define-module (crates-io ar un arun) #:use-module (crates-io))

(define-public crate-arun-0.1.0-alpha.1 (c (n "arun") (v "0.1.0-alpha.1") (d (list (d (n "aho-corasick") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0fq3zrz93qdc5r828g30lnhlsmljkzfhr6swcblirnld979dwqf7") (r "1.78")))

(define-public crate-arun-0.1.0 (c (n "arun") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1alf80lgicncs6rvszjdbjjrvhdnmd8rnwdldqng7bz1z6580fc5") (r "1.78")))

