(define-module (crates-io ar un arun-cli) #:use-module (crates-io))

(define-public crate-arun-cli-0.1.0-alpha.1 (c (n "arun-cli") (v "0.1.0-alpha.1") (d (list (d (n "arun") (r "=0.1.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wcsijkb9vhhmv5nry3rmqyvh7qd9s167bcrzs3n09z09ss16wwh") (r "1.78")))

(define-public crate-arun-cli-0.1.0 (c (n "arun-cli") (v "0.1.0") (d (list (d (n "arun") (r "=0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1lmmbbdwlly85iwcij6l8q7rcmfjfvi02zdc2pd7z362wa92in") (r "1.78")))

