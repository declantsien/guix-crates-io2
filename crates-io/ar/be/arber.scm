(define-module (crates-io ar be arber) #:use-module (crates-io))

(define-public crate-arber-0.0.1 (c (n "arber") (v "0.0.1") (d (list (d (n "blake2") (r "0.9.*") (d #t) (k 0)) (d (n "codec") (r "^2.1.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "1nsnwaw2ay2iwc192li7vcrnapjvdcszalc1qv3dzvd89bdcfy36") (f (quote (("std" "codec/std") ("default" "std")))) (y #t)))

(define-public crate-arber-0.0.2 (c (n "arber") (v "0.0.2") (d (list (d (n "blake2") (r "0.9.*") (d #t) (k 0)) (d (n "codec") (r "^2.1.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "0wxi90qnhi750lrw48ns1v36nilqaqgdkpm0vqrwi4ljjz5wis7j") (f (quote (("std" "codec/std") ("default" "std")))) (y #t)))

(define-public crate-arber-0.0.3 (c (n "arber") (v "0.0.3") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "codec") (r "^2.2.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06p426jmmjj5jbsi6nr45bqr7ifwzh8pc7lsvgl367m7qh2vxlfp") (f (quote (("std" "codec/std") ("default" "std")))) (y #t)))

(define-public crate-arber-0.2.0 (c (n "arber") (v "0.2.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "codec") (r "^2.3.1") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "displaydoc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scale-info") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "09i7i3c5ghahiffiqwpsrd72lhgc2ccl4lgzdi3jx720slwjg6w5") (f (quote (("std" "codec/std" "scale-info/std" "displaydoc/std") ("default" "std")))) (y #t)))

