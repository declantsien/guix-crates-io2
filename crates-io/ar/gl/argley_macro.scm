(define-module (crates-io ar gl argley_macro) #:use-module (crates-io))

(define-public crate-argley_macro-0.1.0 (c (n "argley_macro") (v "0.1.0") (d (list (d (n "delegate-display") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0p1cm2pg29my7dg5wvln0iy4ydrlz89fnc0rnpqrcgjifbi27fxh") (r "1.60.0")))

(define-public crate-argley_macro-1.0.0 (c (n "argley_macro") (v "1.0.0") (d (list (d (n "delegate-display") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bh1pgmd6jd0xakhsmwd6ivbphn36h3f7qfqwcxcmvy7nvj8rm57") (r "1.60.0")))

(define-public crate-argley_macro-1.1.0 (c (n "argley_macro") (v "1.1.0") (d (list (d (n "delegate-display") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "001nd5m0pd5px5xxqlmhb4kj6qxgy89hgviys52cics9li6bb035") (r "1.60.0")))

(define-public crate-argley_macro-1.1.1 (c (n "argley_macro") (v "1.1.1") (d (list (d (n "delegate-display") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1dw7n103h92sj4rv419bl6m2b1mdmf01b9jr85rrcyf3b9z841sx") (r "1.60.0")))

(define-public crate-argley_macro-1.3.0 (c (n "argley_macro") (v "1.3.0") (d (list (d (n "delegate-display") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2x3l0jrmm7kkkjvi8nz5an1ryd7q04k0zswr0zrhw3yb6qfqf5") (r "1.60.0")))

