(define-module (crates-io ar gl argley) #:use-module (crates-io))

(define-public crate-argley-0.1.0 (c (n "argley") (v "0.1.0") (d (list (d (n "argley_macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1859rb65505ammi40ldhl669g0hmllig591k1lrk51x0rrnk9krd") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:argley_macro")))) (r "1.60.0")))

(define-public crate-argley-1.0.0 (c (n "argley") (v "1.0.0") (d (list (d (n "argley_macro") (r "^1") (o #t) (d #t) (k 0)))) (h "022ggp6zrmsfbzkcs0a0ki16arzhpsknwmdvw234h6fx571pkf6f") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:argley_macro")))) (r "1.60.0")))

(define-public crate-argley-1.1.0 (c (n "argley") (v "1.1.0") (d (list (d (n "argley_macro") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1n5rlsd2rafc8czlx2jwwjbq8wlzc67r0fsjcxy2jrn34933svsi") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:argley_macro")))) (r "1.60.0")))

(define-public crate-argley-1.1.1 (c (n "argley") (v "1.1.1") (d (list (d (n "argley_macro") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1155xrb4s2zfaqfbbvai13392kjp40hhigcrl0hwl99rjiax7y99") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:argley_macro")))) (r "1.60.0")))

(define-public crate-argley-1.1.2 (c (n "argley") (v "1.1.2") (d (list (d (n "argley_macro") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "11q0p83b6a8y1zjzwjcdhpfh3f26vbsagvn7ijlsg8fxx93y2xcr") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:argley_macro")))) (r "1.60.0")))

(define-public crate-argley-1.3.0 (c (n "argley") (v "1.3.0") (d (list (d (n "argley_macro") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("unstable"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process"))) (o #t) (k 0)))) (h "0ay1y11nqlgslx9b2aqw2314c8qgv5i667rfbg9xkh3zb01xhvcd") (f (quote (("default" "derive")))) (s 2) (e (quote (("tokio" "dep:tokio") ("derive" "dep:argley_macro") ("async-std" "dep:async-std")))) (r "1.60.0")))

