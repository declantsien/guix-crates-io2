(define-module (crates-io ar ro arrow-cast-guess-precision) #:use-module (crates-io))

(define-public crate-arrow-cast-guess-precision-0.1.0 (c (n "arrow-cast-guess-precision") (v "0.1.0") (d (list (d (n "arrow-array") (r ">30") (d #t) (k 0)) (d (n "arrow-cast") (r ">30") (d #t) (k 0)) (d (n "arrow-schema") (r ">30") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0ik2r5xfv3pjmwb9b94w6xzdk08gnzy9x2kdr2gr8k8288bdprwv")))

(define-public crate-arrow-cast-guess-precision-0.2.0 (c (n "arrow-cast-guess-precision") (v "0.2.0") (d (list (d (n "arrow-array") (r "^48") (d #t) (k 0)) (d (n "arrow-cast") (r "^48") (d #t) (k 0)) (d (n "arrow-schema") (r "^48") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0gkbixjanz5hwk8iz859lrz2rb3p7v3vz2sr3bb4xvk3jrx54fn5")))

(define-public crate-arrow-cast-guess-precision-0.3.0 (c (n "arrow-cast-guess-precision") (v "0.3.0") (d (list (d (n "arrow") (r "^49") (d #t) (k 2)) (d (n "arrow-array") (r "^49") (d #t) (k 0)) (d (n "arrow-cast") (r "^49") (d #t) (k 0)) (d (n "arrow-schema") (r "^49") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "12pi4mh6xq400prk673xrvz2d50yg5n32p7ndgvq17kqjbpx9zsm")))

(define-public crate-arrow-cast-guess-precision-0.4.0 (c (n "arrow-cast-guess-precision") (v "0.4.0") (d (list (d (n "arrow") (r "^50") (d #t) (k 2)) (d (n "arrow-array") (r "^50") (d #t) (k 0)) (d (n "arrow-cast") (r "^50") (d #t) (k 0)) (d (n "arrow-schema") (r "^50") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "0ajqq62digf3mb7bzcxz7dj9l3isxl2lhxf9w8cii7s7d4mmrfaf")))

