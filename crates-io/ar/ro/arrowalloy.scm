(define-module (crates-io ar ro arrowalloy) #:use-module (crates-io))

(define-public crate-arrowalloy-0.0.1 (c (n "arrowalloy") (v "0.0.1") (d (list (d (n "arrow2") (r "^0.14.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "1cr5xkkwrdfng23v2xb0h95wzr3wd6372rpi6pa84j60nrnp7hf6")))

(define-public crate-arrowalloy-0.0.2 (c (n "arrowalloy") (v "0.0.2") (d (list (d (n "arrow2") (r "^0.14.2") (d #t) (k 0)))) (h "1rdnsk0ixq4rx81v6m2slgx332bsnaygmbc5lki01p7d7jl8nlqc")))

(define-public crate-arrowalloy-0.0.3 (c (n "arrowalloy") (v "0.0.3") (d (list (d (n "arrow2") (r "^0.14.2") (d #t) (k 0)))) (h "0gf5f1vjax0mp8g4wvnpdkd7cqq4asavm0yi9nnhmqb799rn3rqm")))

