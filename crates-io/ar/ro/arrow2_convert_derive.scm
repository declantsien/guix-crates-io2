(define-module (crates-io ar ro arrow2_convert_derive) #:use-module (crates-io))

(define-public crate-arrow2_convert_derive-0.1.0 (c (n "arrow2_convert_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1645gbf10hrhcnmj84j2nvgz30k0g142b6dq521r3l0vzzs6yfds")))

(define-public crate-arrow2_convert_derive-0.2.0 (c (n "arrow2_convert_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lb0hn8jnll05m4sd1ha3m79klar2vwj98vvzqgnwc4lnvc2ji9g")))

(define-public crate-arrow2_convert_derive-0.3.0 (c (n "arrow2_convert_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jsaqfkgd1d5g4ljg8yr44fymhamjf9wbdbyd4v55w944q0xsmdg")))

(define-public crate-arrow2_convert_derive-0.3.1 (c (n "arrow2_convert_derive") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04wf5102mc8zymmmkdsv6hslp2xddv0gaf3ync611v1qpx3rb51f") (y #t)))

(define-public crate-arrow2_convert_derive-0.3.2 (c (n "arrow2_convert_derive") (v "0.3.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06s4g1bz52gvgrjnf2740da0gp29648nx390wz49my9cp5nwbiam")))

(define-public crate-arrow2_convert_derive-0.4.0 (c (n "arrow2_convert_derive") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxy632i5dchxp7dli7gz3z2r1fi0l8jb1y9mmfmnsrj8ic36by8")))

(define-public crate-arrow2_convert_derive-0.4.1 (c (n "arrow2_convert_derive") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3ajdl4izrmnmldc98rfi6zyg4prbrj91b8mc4vkrh99dqyrfp9") (y #t)))

(define-public crate-arrow2_convert_derive-0.4.2 (c (n "arrow2_convert_derive") (v "0.4.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17ylz91dv3084x2z8nbxzyhxm8xhw6s1mxbbslq9rngwchli8iwq")))

(define-public crate-arrow2_convert_derive-0.5.0 (c (n "arrow2_convert_derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "036r52zsk4gv09hgwb5a2d1v6hwlyjvy4y9yg60fqfvwkdwrs5ab")))

