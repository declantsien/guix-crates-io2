(define-module (crates-io ar ro arroyo) #:use-module (crates-io))

(define-public crate-arroyo-0.6.0 (c (n "arroyo") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bollard") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "1br1s37mz1w574j7zkw05bx97f0cz9qyizlc0l3414smfizr0jwd")))

(define-public crate-arroyo-0.7.0 (c (n "arroyo") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bollard") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "0pjklf3ayl3f4jzymr90m35428cl54r3r6qc9gbbamd67z8nkrgi")))

