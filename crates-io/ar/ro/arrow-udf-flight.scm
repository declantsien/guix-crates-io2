(define-module (crates-io ar ro arrow-udf-flight) #:use-module (crates-io))

(define-public crate-arrow-udf-flight-0.1.0 (c (n "arrow-udf-flight") (v "0.1.0") (d (list (d (n "arrow-array") (r "^50") (d #t) (k 0)) (d (n "arrow-cast") (r "^50") (f (quote ("prettyprint"))) (d #t) (k 2)) (d (n "arrow-flight") (r "^50") (d #t) (k 0)) (d (n "arrow-schema") (r "^50") (d #t) (k 0)) (d (n "arrow-select") (r "^50") (d #t) (k 0)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0m0cr12h98jyh7gvdbhzq8hg6m7fb6fhs11xphhbabyjdc33mnsa")))

