(define-module (crates-io ar ro arrowc) #:use-module (crates-io))

(define-public crate-arrowc-0.0.1 (c (n "arrowc") (v "0.0.1") (d (list (d (n "arrow-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "07rlwbyy82drxrs2s9im4cffvhrfzkpgbq2rf416gq3qjzcw0nhf")))

(define-public crate-arrowc-0.1.0 (c (n "arrowc") (v "0.1.0") (d (list (d (n "arrow-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libarrowc") (r "^0.0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0ddddl0bp7qnzm8zj5s8lk6yr1q22ghd3aqc997xnf0sw71zcc3d")))

