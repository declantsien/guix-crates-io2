(define-module (crates-io ar ro arrow-tools) #:use-module (crates-io))

(define-public crate-arrow-tools-0.0.1 (c (n "arrow-tools") (v "0.0.1") (h "0blrq6ga2f7d5zsmwcy8i79gaas3jql38xgsbam0kc9hzb2b9pz8")))

(define-public crate-arrow-tools-0.9.0 (c (n "arrow-tools") (v "0.9.0") (h "0s4n9hifhil8bhxkbrc15iaw0dd9q8q2zja4lk6xg0b5n6f58nhq")))

(define-public crate-arrow-tools-0.10.0 (c (n "arrow-tools") (v "0.10.0") (h "02r8gknkkwld9r0cm9i7ifd5ndizzif59x9fwbcml3kqc30i5nfg")))

(define-public crate-arrow-tools-0.11.0 (c (n "arrow-tools") (v "0.11.0") (h "0hpbfq2mcc5iqpwqxskdy9avw0451gvb1yh11ak8afbp53fvfp80")))

(define-public crate-arrow-tools-0.12.0 (c (n "arrow-tools") (v "0.12.0") (h "0a8j64r1nsnb6cnyd3abbasdrgzc00paqdljpgz78ar23msiaczk")))

(define-public crate-arrow-tools-0.12.1 (c (n "arrow-tools") (v "0.12.1") (h "0nm8rbrfsl95p9p29ndi33wiqmimfvw68hs6vfi9f9bx9ikaslf6")))

(define-public crate-arrow-tools-0.13.0 (c (n "arrow-tools") (v "0.13.0") (h "19rvq0gbbq0aaz6pd7kadv880c9xmh7bw0xdmjxkrw4a1q1cs68j")))

(define-public crate-arrow-tools-0.14.0 (c (n "arrow-tools") (v "0.14.0") (h "1rk3rjba2vqy1k68nj1dhxrm0cxvf4x1mhbgcizsv0cv74wimxi1")))

(define-public crate-arrow-tools-0.14.1 (c (n "arrow-tools") (v "0.14.1") (h "1g0rrzpha872fy6jiah8sq883as5w2xfibwwsh5nz8irycrl80kx")))

(define-public crate-arrow-tools-0.15.0 (c (n "arrow-tools") (v "0.15.0") (h "038ak0h5fwwwshgar2bplambpm1r2naj0iyi2d8d2vdqq0ff492g")))

(define-public crate-arrow-tools-0.16.0 (c (n "arrow-tools") (v "0.16.0") (h "1g36nf85z9dpgcs70ijk2pdki2ixca2rrzl98gwbm8ila8rr4dda")))

(define-public crate-arrow-tools-0.17.0 (c (n "arrow-tools") (v "0.17.0") (h "18mbwikgnz0azxb1n70r4p8xpwzd1s500dyh43d30xygzxnhv3gw")))

(define-public crate-arrow-tools-0.17.1 (c (n "arrow-tools") (v "0.17.1") (h "0fhk6rfb4fdy6qfd806zxjal2zj6mkw85bfvka3814fbajjc8k98")))

(define-public crate-arrow-tools-0.17.2 (c (n "arrow-tools") (v "0.17.2") (h "167lys2p8ahfjmm948f1lfc3x2qrfxyrww7rwb8gczqy1wp6n4rn")))

(define-public crate-arrow-tools-0.17.3 (c (n "arrow-tools") (v "0.17.3") (h "1jkik8ra841s0j8484wyakd2g1azlb073scysxsgy4mld96ipa09")))

(define-public crate-arrow-tools-0.17.4 (c (n "arrow-tools") (v "0.17.4") (h "1gad8fcprnb1ikg2q260aslwvz1skd6lyhwmkxpi266vclnv5kgb")))

(define-public crate-arrow-tools-0.17.5 (c (n "arrow-tools") (v "0.17.5") (h "1cr4nggh197pi8kbxr78xc2gqfqz24ymdvfaar9zlvngd0lfznvj")))

(define-public crate-arrow-tools-0.17.7 (c (n "arrow-tools") (v "0.17.7") (h "06z97xb71zc5m0k430iirx1y7sp1lajjjv9j8q7nyc6bfi7cd12p")))

(define-public crate-arrow-tools-0.17.8 (c (n "arrow-tools") (v "0.17.8") (h "1mzdh8s76nfjsj81lawbasgrd1vmwl3mb2ab1s3nv89h6jpkdiqq")))

(define-public crate-arrow-tools-0.17.9 (c (n "arrow-tools") (v "0.17.9") (h "0pxyy04pmrnsvdwxwl2czwc1f1p7mgklsbb4vmx2cd9w22xnxg3v")))

(define-public crate-arrow-tools-0.17.10 (c (n "arrow-tools") (v "0.17.10") (h "14xs42q1ilxpxw807ffy04gs20vfz8ll9g34bl5f1vs34lnh32j0")))

(define-public crate-arrow-tools-0.18.0 (c (n "arrow-tools") (v "0.18.0") (h "1icjisg6pc3dvfiirr22fj0rx7vv6l1q2wmd330sydn0mxk0gywg")))

