(define-module (crates-io ar ro arrow-vm) #:use-module (crates-io))

(define-public crate-arrow-vm-0.0.1 (c (n "arrow-vm") (v "0.0.1") (d (list (d (n "arrow-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "arrowc") (r "^0.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)))) (h "0shw2hcgdxplxmd0jnhbv9h077szzx3lzyjdqmrncyl0575sf526") (s 2) (e (quote (("tokio" "dep:tokio"))))))

