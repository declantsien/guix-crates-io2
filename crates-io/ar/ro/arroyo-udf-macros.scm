(define-module (crates-io ar ro arroyo-udf-macros) #:use-module (crates-io))

(define-public crate-arroyo-udf-macros-0.10.0-dev (c (n "arroyo-udf-macros") (v "0.10.0-dev") (d (list (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "arroyo-udf-common") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "15ickkvp950phhdh38h6jjai8kr3yz2vjvg2q9qqs9fl2qrwi5ig") (y #t)))

(define-public crate-arroyo-udf-macros-0.1.0 (c (n "arroyo-udf-macros") (v "0.1.0") (d (list (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "arroyo-udf-common") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4ybv7971znjlnm1cdldmcgpsy5fy7qi2wr1l85hmwnglnr73qy")))

