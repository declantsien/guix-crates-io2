(define-module (crates-io ar ro arrow-schema) #:use-module (crates-io))

(define-public crate-arrow-schema-24.0.0 (c (n "arrow-schema") (v "24.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09qfqg7l8n90mgz710w2xfp0m3nyg3q8fs7shqn4bq953zqgxlc6") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-25.0.0 (c (n "arrow-schema") (v "25.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m26c9c939sv01549wn2w6ihnxmvgii5g0pqj5rhaxbpppr1hm8s") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-26.0.0 (c (n "arrow-schema") (v "26.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dxxyw8rbqgmcbxsrpby93ngrf8qbm8sl6qd6pc6pjilg3mhd50g") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-27.0.0 (c (n "arrow-schema") (v "27.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lgl2nvjndvgqd4sjj37rblirb3zc0r1wfb0fv89gqf1a12nar65") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-28.0.0 (c (n "arrow-schema") (v "28.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09lxlf6i049ysz48i8pj247za533ajcf4q4aadlf22x8h8w197ys") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-29.0.0 (c (n "arrow-schema") (v "29.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kmnfvvk5wa2b5fqb513snykdr7iqvsfi8zmcjc3n9gi8k0igvv9") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-30.0.0 (c (n "arrow-schema") (v "30.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0804khln39vway2yz4k30d930840qyqr23y0ysnvmmpbs71j6q3z") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-30.0.1 (c (n "arrow-schema") (v "30.0.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b4gvlaaq8sf9khdhszh3nkj148hslsmjb2prrg4w8p69x4wr7d9") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-31.0.0 (c (n "arrow-schema") (v "31.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08ypjxyrj37a9nf3gbgls075ay5kq681y78cyxrjszmj2384kjkk") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-32.0.0 (c (n "arrow-schema") (v "32.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10gglj137cmfm1sgamh0qkxm7x49vnkvbz6y5wpzafhpp5rz7lza") (f (quote (("default")))) (r "1.62")))

(define-public crate-arrow-schema-33.0.0 (c (n "arrow-schema") (v "33.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^1.2.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j6gaigwhhziyymkalpf8g9mjh7mhy4wpc7kxzjlpfbyv0bpfcpb") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-34.0.0 (c (n "arrow-schema") (v "34.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^1.2.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1g1z33zzqjmaz6awsjada5bj4kc7097s90ry54i8xyrv8yc1i5b4") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-35.0.0 (c (n "arrow-schema") (v "35.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^1.2.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03zj11vky0gj9i5pv9qxf119ifcrcf4d3jrijlxhwhgqlvv2csxz") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-36.0.0 (c (n "arrow-schema") (v "36.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "087jpy0c2k30zb2wrhxc67h46hqw74in3zlqmxdhpvbdp3vifkyh") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-37.0.0 (c (n "arrow-schema") (v "37.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "194phjyvvf1pb8aammwdbf91aww83xm37kdi0010ydf87alqhsx1") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-38.0.0 (c (n "arrow-schema") (v "38.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14mfvsxad4plzlyrjq1d01qycwk8hz8wf0k6mikcqqk6ilyr51dw") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-39.0.0 (c (n "arrow-schema") (v "39.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zj1gkidarvriamy0ss0c2basa9mxq3c7byz8qps3598ic2ivi0q") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-40.0.0 (c (n "arrow-schema") (v "40.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10jszxqiyfpsj89mgp4jlbxwp9dx6jg7piny9lp1l5blyjz7n615") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-41.0.0 (c (n "arrow-schema") (v "41.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jqq8qypq643j32n7xzhqn88pghqqmnqk5g3wi4fdhhbipbdhw9b") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-42.0.0 (c (n "arrow-schema") (v "42.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r921wmjnyynqvzbhn47b29vbs4n88fjijsp2jnrfz9dpm2x57ms") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-43.0.0 (c (n "arrow-schema") (v "43.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zp8jz0pcr2d020rrmkdkzg6yizb2gc0fbfgzddb2w7mxrbwpsrb") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-44.0.0 (c (n "arrow-schema") (v "44.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sba0j36vskrxa40l0f21l7cj3jfvvnrx70njsial17kpbpx1vnw") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-45.0.0 (c (n "arrow-schema") (v "45.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cz7xjngdis9mmnynwsizb521p2g95lnwa191v139dlfj53qrpsl") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-46.0.0 (c (n "arrow-schema") (v "46.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gg0j3f9s4c4s99vv77rpg6fjajsm893mh5d4bg0zw1hlzdga15i") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-47.0.0 (c (n "arrow-schema") (v "47.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jyfw40m070zj2pv8mp3gvlnzs0mavnzn6qhw19qh5bv26f1f7ax") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-48.0.0 (c (n "arrow-schema") (v "48.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0506ln19g9ngpdr2sii2jc16p8xbkbrscm91hsynmvzmjwzqqylx") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-48.0.1 (c (n "arrow-schema") (v "48.0.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jrnjj3h6ivd2is0x17zhjzd1bl7w0hyr6qj2sgsagd9pcvxgx5a") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-49.0.0 (c (n "arrow-schema") (v "49.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ry1izd9f5ygnzm8j1gjs961z47m2fnq8dikh7wv1w8vg1g8mqh9") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-50.0.0 (c (n "arrow-schema") (v "50.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a80kvnf66rkkp8500wn0jkj5rnhj9l957r6klvnklbw3z0fkwqg") (f (quote (("ffi" "bitflags")))) (r "1.62")))

(define-public crate-arrow-schema-51.0.0 (c (n "arrow-schema") (v "51.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "bitflags") (r "^2.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a75y860fl6l166x6mzpkqnzg4q3msvf2fhmh53hr4dym8x4in82") (f (quote (("ffi" "bitflags")))) (r "1.62")))

