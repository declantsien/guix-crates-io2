(define-module (crates-io ar ro arrowvortex_clipboard) #:use-module (crates-io))

(define-public crate-arrowvortex_clipboard-0.1.0 (c (n "arrowvortex_clipboard") (v "0.1.0") (h "16azmkvr1qarqh03562vf4xannjnzfs7xqcqyk4av442cbp02ijg") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrowvortex_clipboard-0.1.1 (c (n "arrowvortex_clipboard") (v "0.1.1") (h "0jga7xhcckp19jy7m1dkkxnk2j7bapxxd4kxr5c434i78glhswci") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrowvortex_clipboard-0.2.0 (c (n "arrowvortex_clipboard") (v "0.2.0") (h "1a50fk34lm7bdlgvdf3k333pj4s8dwgd6ry6952yhivpcgsssl9y")))

