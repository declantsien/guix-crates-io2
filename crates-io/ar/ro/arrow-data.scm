(define-module (crates-io ar ro arrow-data) #:use-module (crates-io))

(define-public crate-arrow-data-24.0.0 (c (n "arrow-data") (v "24.0.0") (d (list (d (n "arrow-buffer") (r "^24.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^24.0.0") (d #t) (k 0)) (d (n "half") (r "^2.0") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0igz029dbh4sp4zfvv7x830arsmza4pb6p4v7v2zv1hd0q2xy045") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-25.0.0 (c (n "arrow-data") (v "25.0.0") (d (list (d (n "arrow-buffer") (r "^25.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^25.0.0") (d #t) (k 0)) (d (n "half") (r "^2.0") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0pcli8j642mslf47c34sjvwhii0xryij29hc47bndhv0ypzlymaj") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-26.0.0 (c (n "arrow-data") (v "26.0.0") (d (list (d (n "arrow-buffer") (r "^26.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^26.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1fdvl8h0rp4ambigi7bxp26hmaf93fiwn0gpskd8bi2qw7p6z4ad") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-27.0.0 (c (n "arrow-data") (v "27.0.0") (d (list (d (n "arrow-buffer") (r "^27.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^27.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0012j8hkyfja4wwfj504k0xd1j66kpzaikncjyi3n0irc0rw2yz1") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-28.0.0 (c (n "arrow-data") (v "28.0.0") (d (list (d (n "arrow-buffer") (r "^28.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^28.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "12ma3wphfivixab8mprn9c3kj8v3vffxxdf6sm4j800pwvpq4j3y") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-29.0.0 (c (n "arrow-data") (v "29.0.0") (d (list (d (n "arrow-buffer") (r "^29.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^29.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1jq2iy7brxhq2y220dk8d35nzxvxq1hbvcl742rfdr02s7cik5p5") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-30.0.0 (c (n "arrow-data") (v "30.0.0") (d (list (d (n "arrow-buffer") (r "^30.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^30.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "04zdq42m8xflpl823r4zrnra8bhxskw4y9ypsybrhrn7k383bj5w") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-30.0.1 (c (n "arrow-data") (v "30.0.1") (d (list (d (n "arrow-buffer") (r "^30.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^30.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1ff8j51p4gbaqrizf5dr62pqh21wpl8rr01qjlabkkgd5mhghk8p") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-31.0.0 (c (n "arrow-data") (v "31.0.0") (d (list (d (n "arrow-buffer") (r "^31.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^31.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0xs485544z68x82grs387rkcyzngdri0zamlxbp5g0yrkyffdqql") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-32.0.0 (c (n "arrow-data") (v "32.0.0") (d (list (d (n "arrow-buffer") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "15b4hss2gzlw6yl03gk90misv3602kvjljvz4s7vx01vbly0w37f") (f (quote (("force_validate")))) (r "1.62")))

(define-public crate-arrow-data-33.0.0 (c (n "arrow-data") (v "33.0.0") (d (list (d (n "arrow-buffer") (r "^33.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^33.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1xaph1145bpp2pc81djsglk69vrffb9bd9yypyi2sk0y3r6xnl7a") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-34.0.0 (c (n "arrow-data") (v "34.0.0") (d (list (d (n "arrow-buffer") (r "^34.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^34.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0mhymrqzz1c852kv3w68jsh2cq70yxhx9iz8nkjad7lqwqz6gk17") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-35.0.0 (c (n "arrow-data") (v "35.0.0") (d (list (d (n "arrow-buffer") (r "^35.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^35.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1zblr0symcf35pawnqixxhk8k16533y0p1pv3ydm7yfvdiy7iiqr") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-36.0.0 (c (n "arrow-data") (v "36.0.0") (d (list (d (n "arrow-buffer") (r "^36.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^36.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1vqc0vgrn1rqc6a0q2g28d4nlwzsqa1f787nhvfdkahsz9z96gsk") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-37.0.0 (c (n "arrow-data") (v "37.0.0") (d (list (d (n "arrow-buffer") (r "^37.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^37.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1kz2nz228kawgz4r96a4ba0apn5x11x3zpx93lrnvada8wckdj5i") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-38.0.0 (c (n "arrow-data") (v "38.0.0") (d (list (d (n "arrow-buffer") (r "^38.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^38.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1zmsb7yacl02lmr51r242b3f3y8v2ln09i5j92x8pgnddqyi2s7b") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-39.0.0 (c (n "arrow-data") (v "39.0.0") (d (list (d (n "arrow-buffer") (r "^39.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^39.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "10anh0pr8v4a0czn41d1p88rxrs5009ndxdsp1lw1lk4i7ci1xy6") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-40.0.0 (c (n "arrow-data") (v "40.0.0") (d (list (d (n "arrow-buffer") (r "^40.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^40.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "15r0dplrvljrrydp0zway5q2bpm0dyfripszcr16djicj7wqvg31") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-41.0.0 (c (n "arrow-data") (v "41.0.0") (d (list (d (n "arrow-buffer") (r "^41.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^41.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0f34gkisfhsj87i4xclicj7jp5sjv7v1gbqjyx51sx1xv2zdqj2d") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-42.0.0 (c (n "arrow-data") (v "42.0.0") (d (list (d (n "arrow-buffer") (r "^42.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^42.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1hix53klh2ggnc0ifxg1rw9x7nfnc6yd61jifq3njgd5svd876hx") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-43.0.0 (c (n "arrow-data") (v "43.0.0") (d (list (d (n "arrow-buffer") (r "^43.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^43.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "12q2dvfaj78rfj5k3sfn8d3fnhwp3iizd4dbf5m14526qniz9x6l") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-44.0.0 (c (n "arrow-data") (v "44.0.0") (d (list (d (n "arrow-buffer") (r "^44.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^44.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "06j1v1fkdzqd8wli603c6sqiqnpdasd9wx369ssj1va7i6h6anj2") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-45.0.0 (c (n "arrow-data") (v "45.0.0") (d (list (d (n "arrow-buffer") (r "^45.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^45.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "13yx6gfwwgncr8230cpg1vkhxzq60r3wfh984idkxi776lk8bdn6") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-46.0.0 (c (n "arrow-data") (v "46.0.0") (d (list (d (n "arrow-buffer") (r "^46.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^46.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1261j4qr2iyy6lv80n7ayf0gkdmjfaz0j8jpl16si801zwqhz46s") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-47.0.0 (c (n "arrow-data") (v "47.0.0") (d (list (d (n "arrow-buffer") (r "^47.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^47.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0fpp09ykg6nb7jcaqnjzga242y7nlrfz3v0wlrf0kd68k4v4qnj7") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-48.0.0 (c (n "arrow-data") (v "48.0.0") (d (list (d (n "arrow-buffer") (r "^48.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0flgads6xn3bnbcyy6f92aka9wcdkckag8dggxg64zk0bqclg9fh") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-48.0.1 (c (n "arrow-data") (v "48.0.1") (d (list (d (n "arrow-buffer") (r "^48.0.1") (d #t) (k 0)) (d (n "arrow-schema") (r "^48.0.1") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0q94s7xdj5h61vj5rc2fkd6bm5nf676bhxam5rz44qs7qzzbkhkd") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-49.0.0 (c (n "arrow-data") (v "49.0.0") (d (list (d (n "arrow-buffer") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0d6611d20xk3dmdzyd40zf1zvdscrawmh64ccxs491x3h3iayzwh") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-50.0.0 (c (n "arrow-data") (v "50.0.0") (d (list (d (n "arrow-buffer") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0kn6fjjpg4lfkvr0h7jqda0qphii196iiqaw2g9klbqn3awl9mk7") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

(define-public crate-arrow-data-51.0.0 (c (n "arrow-data") (v "51.0.0") (d (list (d (n "arrow-buffer") (r "^51.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^51.0.0") (d #t) (k 0)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1idf0k6nhfllijhmhf95ih7y335n1qzxvxl8ijq6lsahcqgsqhi7") (f (quote (("force_validate") ("ffi" "arrow-schema/ffi")))) (r "1.62")))

