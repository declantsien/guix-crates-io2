(define-module (crates-io ar ro arroyo-udf-common) #:use-module (crates-io))

(define-public crate-arroyo-udf-common-0.1.0 (c (n "arroyo-udf-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "arrow") (r "^50") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "async-ffi") (r "^0.5.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "1z8a7rsd0ll07milwv77v9xm1pnklnxj9gv6nicmhf2f1gzxnz71")))

