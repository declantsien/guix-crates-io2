(define-module (crates-io ar ro arrow_extendr) #:use-module (crates-io))

(define-public crate-arrow_extendr-48.0.1 (c (n "arrow_extendr") (v "48.0.1") (d (list (d (n "arrow") (r "^48.0.1") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "extendr-api") (r ">=0.6.0") (d #t) (k 0)))) (h "1nxm7sg4zrwn1y4naslhribsj19xpi691s0jd6pzr1zi38p5xl31")))

(define-public crate-arrow_extendr-49.0.0 (c (n "arrow_extendr") (v "49.0.0") (d (list (d (n "arrow") (r "^49.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "extendr-api") (r ">=0.6.0") (d #t) (k 0)))) (h "1fq8q66nn9isgrm4dy2r1d19nflsnz9r4l6qbakvyl0s2a0xb0iv")))

(define-public crate-arrow_extendr-50.0.0 (c (n "arrow_extendr") (v "50.0.0") (d (list (d (n "arrow") (r "^50.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "extendr-api") (r ">=0.6.0") (d #t) (k 0)))) (h "15fbqzpfbig1xgc7f404q32dpf3xl0cy2lhr5jy2hp5r4qm724iv")))

(define-public crate-arrow_extendr-51.0.0 (c (n "arrow_extendr") (v "51.0.0") (d (list (d (n "arrow") (r "^51.0.0") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "extendr-api") (r ">=0.6.0") (d #t) (k 0)))) (h "1svkh9rr5aymdb3b8gkmvcfy449j55ingf89z74cr5ljydsmn5vp") (r "1.70.0")))

