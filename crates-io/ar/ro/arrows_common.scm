(define-module (crates-io ar ro arrows_common) #:use-module (crates-io))

(define-public crate-arrows_common-0.1.0 (c (n "arrows_common") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "typetag") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "10fcynbjagrflii918mw097wj0qd9pjg35xa9l2a3im0kgydnnbv")))

