(define-module (crates-io ar ro arrow-buffer) #:use-module (crates-io))

(define-public crate-arrow-buffer-23.0.0 (c (n "arrow-buffer") (v "23.0.0") (d (list (d (n "half") (r "^2.0") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1acfx1vxd1cqsh45bxshcdm1bi034bpmqlh8mlj1ma0jdq60b46j") (r "1.62")))

(define-public crate-arrow-buffer-24.0.0 (c (n "arrow-buffer") (v "24.0.0") (d (list (d (n "half") (r "^2.0") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1iscbl6a6sy9rc55wwaaf9kbcprycb4mmp4v8p18nsh2w07lsn9y") (r "1.62")))

(define-public crate-arrow-buffer-25.0.0 (c (n "arrow-buffer") (v "25.0.0") (d (list (d (n "half") (r "^2.0") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1abrsz6vazmgzvf4wpflr3sqq1p8r3fq642j3lijiydvpmqkx5mg") (r "1.62")))

(define-public crate-arrow-buffer-26.0.0 (c (n "arrow-buffer") (v "26.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "09p68ckm5lly6xp2xij9w5ikr1kksm1gwsmqrb78v01zksz6qivq") (r "1.62")))

(define-public crate-arrow-buffer-27.0.0 (c (n "arrow-buffer") (v "27.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0ij5mx9ym51basisbxpwl211kwgh0wzmlq358k8xqyjs1pslc7iv") (r "1.62")))

(define-public crate-arrow-buffer-28.0.0 (c (n "arrow-buffer") (v "28.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "05nr76mmzq15li2cnnnmqkvq1g0lhdhdjis6g0j4ns7agji69pnn") (r "1.62")))

(define-public crate-arrow-buffer-29.0.0 (c (n "arrow-buffer") (v "29.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0yd732s8d18gxnk2qpv5q124q59zbyn7k99gl4waczgdx00i0c2a") (r "1.62")))

(define-public crate-arrow-buffer-30.0.0 (c (n "arrow-buffer") (v "30.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1bqrd01lfkld7wry2dmfcsdqwpgqb964c3p54myr2rvkdbaghbjf") (r "1.62")))

(define-public crate-arrow-buffer-30.0.1 (c (n "arrow-buffer") (v "30.0.1") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1pr4413nglxbnsiwfcc53gggi6zazk9sd1gcc6kwq971ygcjicx7") (r "1.62")))

(define-public crate-arrow-buffer-31.0.0 (c (n "arrow-buffer") (v "31.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0j2p95gvq8pmsi6x984mbqil3kdp74g9qbyx7w51kh8v5ciiv8h3") (r "1.62")))

(define-public crate-arrow-buffer-32.0.0 (c (n "arrow-buffer") (v "32.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1nkv4an9w0qkjs1mqq4amvgvbv1kf1yv5anpr1p5im6cvgd6gyaf") (r "1.62")))

(define-public crate-arrow-buffer-33.0.0 (c (n "arrow-buffer") (v "33.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1wmm2rqckbb8my3fg54yffcz1f94lq8j7lvv8304h948674h23lz") (r "1.62")))

(define-public crate-arrow-buffer-34.0.0 (c (n "arrow-buffer") (v "34.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "19x0q4c3w4ff5rw3w7xj6vblk5sk1whcz5nz4xvj1h57xmrfrd38") (r "1.62")))

(define-public crate-arrow-buffer-35.0.0 (c (n "arrow-buffer") (v "35.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "07wx7ip2sbib4hq5lgikg0g8pp3iar1zaykqhh8ji4y5a959wxd3") (r "1.62")))

(define-public crate-arrow-buffer-36.0.0 (c (n "arrow-buffer") (v "36.0.0") (d (list (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1fh638d8bxbvgv9ql77apd5irsrrhdzi2irijcwvx1mij7lnlx5h") (r "1.62")))

(define-public crate-arrow-buffer-37.0.0 (c (n "arrow-buffer") (v "37.0.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1xl1wf5kcbv0ivj22yiafxbfn13fah4rrwi6ql7lmi5nqlrwfcsx") (r "1.62")))

(define-public crate-arrow-buffer-38.0.0 (c (n "arrow-buffer") (v "38.0.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0hri255vibqfp4ibr91warj7q2hqdpfb5c4gk41w83jkp6y7bj3j") (r "1.62")))

(define-public crate-arrow-buffer-39.0.0 (c (n "arrow-buffer") (v "39.0.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0axgs62rlvbi3a18l67mfhdfagmjw47afy3wp73ngmx90s0vf1cl") (r "1.62")))

(define-public crate-arrow-buffer-40.0.0 (c (n "arrow-buffer") (v "40.0.0") (d (list (d (n "criterion") (r "^0.4") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1xbqaqa9wsb70qmqg7rryprmzwzbx673cw0mnb20l4kfwr9nbxn5") (r "1.62")))

(define-public crate-arrow-buffer-41.0.0 (c (n "arrow-buffer") (v "41.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "187hia4a7wn1ddxz7dv8dximg1xpm9d42s52bfv1iq4z9fwm0d58") (r "1.62")))

(define-public crate-arrow-buffer-42.0.0 (c (n "arrow-buffer") (v "42.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "1sz9f8a4w1r594a6851qib2bplmc7gxvby6f4d700warrwp39kih") (r "1.62")))

(define-public crate-arrow-buffer-43.0.0 (c (n "arrow-buffer") (v "43.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "08m7cxfksmc2qsygba0ingr1a3acbrxx9qdr0184wj3z0yg47gmx") (r "1.62")))

(define-public crate-arrow-buffer-44.0.0 (c (n "arrow-buffer") (v "44.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0lgpj94hv20p6m39z26zb373pvpxr20nga9lw6wyx3snfbk48l7y") (r "1.62")))

(define-public crate-arrow-buffer-45.0.0 (c (n "arrow-buffer") (v "45.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "16v6ndzk12236627hnpa0a7gbxnyqh506wjrz55qf36hiqf3njr3") (r "1.62")))

(define-public crate-arrow-buffer-46.0.0 (c (n "arrow-buffer") (v "46.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "041mfr5pm3hjzfjlkd9ywlxy7zd89n3p5ib9bgrw4yfn9npl6j7w") (r "1.62")))

(define-public crate-arrow-buffer-47.0.0 (c (n "arrow-buffer") (v "47.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "15b1km7kb7cys2pdxgq1p9syiw7yzf9cch85rcw12504a8i1k8gx") (r "1.62")))

(define-public crate-arrow-buffer-48.0.0 (c (n "arrow-buffer") (v "48.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0cskdiaas474cgplcd44500m1lwrf7ll498hnxd2jkxr145gj8m7") (r "1.62")))

(define-public crate-arrow-buffer-48.0.1 (c (n "arrow-buffer") (v "48.0.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0pr0akbcyznpv97mw92sv9ssdc2hmhvzagzx2kjpdgbgijc2slam") (r "1.62")))

(define-public crate-arrow-buffer-49.0.0 (c (n "arrow-buffer") (v "49.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "0v4849lbijm7qjdhf9plh84r5pabnv0nkc374rngq02vj4hzr801") (r "1.62")))

(define-public crate-arrow-buffer-50.0.0 (c (n "arrow-buffer") (v "50.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "192s1xlvzfrfjw65has71clxa9y8hmzbqmi7qvxxzg012w35nqb9") (r "1.62")))

(define-public crate-arrow-buffer-51.0.0 (c (n "arrow-buffer") (v "51.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "half") (r "^2.1") (k 0)) (d (n "num") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (k 2)))) (h "09vvqd24grxgq5iwxiwymi5kk4b7qrlp8xfbyhmnk9fby0r282hd") (r "1.62")))

