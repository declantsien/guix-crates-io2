(define-module (crates-io ar ro arrow-parser) #:use-module (crates-io))

(define-public crate-arrow-parser-0.0.1 (c (n "arrow-parser") (v "0.0.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)))) (h "0z1d045q6i95brkii34pzx2xckb8mxak9xxqpmrv519yd3xlf5qv")))

(define-public crate-arrow-parser-0.0.2 (c (n "arrow-parser") (v "0.0.2") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mqml2kddggs3y2hxdsi3rdby1pc17bdarf103h3bqmq6k536f6i")))

