(define-module (crates-io ar ro arrow-digest) #:use-module (crates-io))

(define-public crate-arrow-digest-0.1.0 (c (n "arrow-digest") (v "0.1.0") (d (list (d (n "arrow") (r "^6") (d #t) (k 0)) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "0zpxlrp6n4d5c1r73jrk6nfzpx8ylf7dm08x41bd1w9ai4bssv8f")))

(define-public crate-arrow-digest-0.2.0 (c (n "arrow-digest") (v "0.2.0") (d (list (d (n "arrow-5") (r "^5") (o #t) (d #t) (k 0) (p "arrow")) (d (n "arrow-6") (r "^6") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "06b3x016la866bhq7n52z42fm0n85i21jzq3df3gfvwkc7mxdax5") (f (quote (("use-arrow-6" "arrow-6") ("use-arrow-5" "arrow-5") ("default" "use-arrow-6"))))))

(define-public crate-arrow-digest-0.3.0 (c (n "arrow-digest") (v "0.3.0") (d (list (d (n "arrow-5") (r "^5") (o #t) (d #t) (k 0) (p "arrow")) (d (n "arrow-6") (r "^6") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "1r3xic9wf1b1snbpgr86nsm7nfznbdf3habljdc0wmk7rjpmv1bk") (f (quote (("use-arrow-6" "arrow-6") ("use-arrow-5" "arrow-5") ("default" "use-arrow-6"))))))

(define-public crate-arrow-digest-0.4.0 (c (n "arrow-digest") (v "0.4.0") (d (list (d (n "arrow-5") (r "^5") (o #t) (d #t) (k 0) (p "arrow")) (d (n "arrow-6") (r "^6") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "0zdway9fg637kdvddssfmrdi92xapfdwwgppg8fzqma2x0sbin35") (f (quote (("use-arrow-6" "arrow-6") ("use-arrow-5" "arrow-5") ("default" "use-arrow-6"))))))

(define-public crate-arrow-digest-0.6.0 (c (n "arrow-digest") (v "0.6.0") (d (list (d (n "arrow-5") (r "^5") (o #t) (d #t) (k 0) (p "arrow")) (d (n "arrow-6") (r "^6") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "1vdq4rld1j9q0y8mwbqlqh81jg1vjb3sr93g87nci8p3i0ja5da2") (f (quote (("use-arrow-6" "arrow-6") ("use-arrow-5" "arrow-5") ("default" "use-arrow-6"))))))

(define-public crate-arrow-digest-0.6.1 (c (n "arrow-digest") (v "0.6.1") (d (list (d (n "arrow-5") (r "^5") (o #t) (d #t) (k 0) (p "arrow")) (d (n "arrow-6") (r "^6") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "0lkl3qabs8wizzg8dm5w4pcyjv5vibxcn4zsdbrgpfzz7xyf4ban") (f (quote (("use-arrow-6" "arrow-6") ("use-arrow-5" "arrow-5") ("default" "use-arrow-6"))))))

(define-public crate-arrow-digest-0.7.0 (c (n "arrow-digest") (v "0.7.0") (d (list (d (n "arrow-9") (r "^9") (o #t) (d #t) (k 0) (p "arrow")) (d (n "digest") (r "^0") (d #t) (k 0)) (d (n "parquet") (r "^9") (d #t) (k 2)) (d (n "sha3") (r "^0") (d #t) (k 2)))) (h "1fp1r252smazvs9zbjxyzwf7lc1z20853mcq2a9q548d8xfms2ib") (f (quote (("use-arrow-9" "arrow-9") ("default" "use-arrow-9"))))))

(define-public crate-arrow-digest-29.0.0 (c (n "arrow-digest") (v "29.0.0") (d (list (d (n "arrow") (r "^29") (d #t) (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^29") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "1c4kvrc1j9rgy627i9gnigv9kz1q0rg2xckm77pxwqk8b3cyangk")))

(define-public crate-arrow-digest-29.0.1 (c (n "arrow-digest") (v "29.0.1") (d (list (d (n "arrow") (r ">=28.0.0, <30.0.0") (d #t) (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r ">=28.0.0, <30.0.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "02hpdbskqzd6jnpdnb5crvqnzzwq5wisfl9yhns4i3prwjhwsmfb")))

(define-public crate-arrow-digest-29.0.2 (c (n "arrow-digest") (v "29.0.2") (d (list (d (n "arrow") (r ">=28.0.0, <30.0.0") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r ">=28.0.0, <30.0.0") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "0xiwdclli0113ixqzr3hkglln3slfyly2abav52ld8sikazbzj7m")))

(define-public crate-arrow-digest-28.0.0 (c (n "arrow-digest") (v "28.0.0") (d (list (d (n "arrow") (r "^28") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^28") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "1ls4rkcr488igyng5a2b20c77bypnvxzg0rpy93vy5f910q8hv5k")))

(define-public crate-arrow-digest-29.0.3 (c (n "arrow-digest") (v "29.0.3") (d (list (d (n "arrow") (r "^29") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^29") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "083s7pxplsxxpqh5m5rx91hzsprrfdv4gizs4vwp2phg9rkzgbqf")))

(define-public crate-arrow-digest-33.0.0 (c (n "arrow-digest") (v "33.0.0") (d (list (d (n "arrow") (r "^33") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^33") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "08c47v3fq3sxjkyxfv22h32ywc3y1cy6x029k2732d6bbaa30rf5")))

(define-public crate-arrow-digest-34.0.0 (c (n "arrow-digest") (v "34.0.0") (d (list (d (n "arrow") (r "^34") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^34") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "1k0l2knnvz4lfiwvk08j2f2zgwg5fb2kzdhnwk9sh4nxii9j23fm")))

(define-public crate-arrow-digest-35.0.0 (c (n "arrow-digest") (v "35.0.0") (d (list (d (n "arrow") (r "^35") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^35") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "04613shb0g3qydvyri2v0q42gbgdlj9kyvd2xa1j537r8j5rxjap")))

(define-public crate-arrow-digest-36.0.0 (c (n "arrow-digest") (v "36.0.0") (d (list (d (n "arrow") (r "^36") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^36") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "1qk754zbk8lr27dvncn7z3f3p3l6q2s42fnh03kb0nhjmwzgvkbk")))

(define-public crate-arrow-digest-37.0.0 (c (n "arrow-digest") (v "37.0.0") (d (list (d (n "arrow") (r "^37") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^37") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "13vpk1ihwpjd7vl19k769z74vm19z5xpa78kaaw0la1d1323cr2w")))

(define-public crate-arrow-digest-38.0.0 (c (n "arrow-digest") (v "38.0.0") (d (list (d (n "arrow") (r "^38") (k 0)) (d (n "digest") (r ">=0.10.0") (d #t) (k 0)) (d (n "parquet") (r "^38") (d #t) (k 2)) (d (n "sha3") (r ">=0.10.0") (d #t) (k 2)))) (h "01jdfhw8khi5wy0wb2mqyvxij1wpx7g6ay3iqx0kn2xilm0ilryi")))

(define-public crate-arrow-digest-39.0.0 (c (n "arrow-digest") (v "39.0.0") (d (list (d (n "arrow") (r "^39") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^39") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "1q6ypbm0riwc0fjy24krazmij1j9c9x1gmavrp816vy8c2bjdd02")))

(define-public crate-arrow-digest-40.0.0 (c (n "arrow-digest") (v "40.0.0") (d (list (d (n "arrow") (r "^40") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^40") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0pgk9x4mchh58l1cdpfwzzyv3x0qji09mh47dinff34kxm0515n6")))

(define-public crate-arrow-digest-41.0.0 (c (n "arrow-digest") (v "41.0.0") (d (list (d (n "arrow") (r "^41") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^41") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "1zgf0wai1ac5lrk2xjhs8pp4lga64lwx2v57gbla35w82q5z8a5f")))

(define-public crate-arrow-digest-42.0.0 (c (n "arrow-digest") (v "42.0.0") (d (list (d (n "arrow") (r "^42") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^42") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0w6mrsps8bndx3wpiviy7z40jc9v8yp65qrfnx408irmlc3x2zp5")))

(define-public crate-arrow-digest-43.0.0 (c (n "arrow-digest") (v "43.0.0") (d (list (d (n "arrow") (r "^43") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^43") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0aa6p8sdy876pkz0zv70isnaaw72kp6hssn264rip4w9i4zjf26v")))

(define-public crate-arrow-digest-44.0.0 (c (n "arrow-digest") (v "44.0.0") (d (list (d (n "arrow") (r "^44") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^44") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0fjdhgwxyqqawqn22cchyg393vpj4j39zgaaqc9gz0l93c6qwp4a")))

(define-public crate-arrow-digest-45.0.0 (c (n "arrow-digest") (v "45.0.0") (d (list (d (n "arrow") (r "^45") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^45") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0a1pgn87n6k7p0s4s9v57pnszxzng1rhqa9p41b4b5n921fmrsy9")))

(define-public crate-arrow-digest-46.0.0 (c (n "arrow-digest") (v "46.0.0") (d (list (d (n "arrow") (r "^46") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^46") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "11nkrc2j5y2hjbqkpkvc9hpk4il79zi5y3xnz1g0wwsyz5qnz4dd")))

(define-public crate-arrow-digest-47.0.0 (c (n "arrow-digest") (v "47.0.0") (d (list (d (n "arrow") (r "^47") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^47") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "1f7x28i4d6w7xcr342qi95fy8f1h6h45fpfdnrhrmsfsqk9k76bh")))

(define-public crate-arrow-digest-48.0.0 (c (n "arrow-digest") (v "48.0.0") (d (list (d (n "arrow") (r "^48") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^48") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "01hgwbbs43i2w49ijfz2z2q847wxabg2k7zzy9v6pwd1kxk26and")))

(define-public crate-arrow-digest-49.0.0 (c (n "arrow-digest") (v "49.0.0") (d (list (d (n "arrow") (r "^49") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^49") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "0wrr3q7d3cy7mkh48nhwbyw5qvsnp0ly0x5vsqsv69inbrp7bnbw")))

(define-public crate-arrow-digest-50.0.0 (c (n "arrow-digest") (v "50.0.0") (d (list (d (n "arrow") (r "^50") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^50") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "1vcsz75ffx0s6jzi5p303z1acprw0ylqfrr9gn23f6hg1fhyb85r")))

(define-public crate-arrow-digest-51.0.0 (c (n "arrow-digest") (v "51.0.0") (d (list (d (n "arrow") (r "^51") (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "parquet") (r "^51") (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)))) (h "1k4sfqh759bsl2ybly39zjdgjq221x6yklnv7f4p5hd7zmd2af0z")))

