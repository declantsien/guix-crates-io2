(define-module (crates-io ar ro arroyo-udf-plugin) #:use-module (crates-io))

(define-public crate-arroyo-udf-plugin-0.1.0 (c (n "arroyo-udf-plugin") (v "0.1.0") (d (list (d (n "arrow") (r "^50") (f (quote ("ffi"))) (d #t) (k 0)) (d (n "arroyo-udf-common") (r "^0.1.0") (d #t) (k 0)) (d (n "arroyo-udf-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "async-ffi") (r "^0.5.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "time" "macros"))) (d #t) (k 0)))) (h "0m9nj2gd4f7140vk0mz0ql7lllgax8fmydk094sclbhvis1g9hd1")))

