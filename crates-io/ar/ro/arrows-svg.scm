(define-module (crates-io ar ro arrows-svg) #:use-module (crates-io))

(define-public crate-arrows-svg-0.0.1 (c (n "arrows-svg") (v "0.0.1") (d (list (d (n "domrs") (r "^0.0.6") (d #t) (k 0)))) (h "051l7h4hw72spa8a0bxd79lzs2j3sydz87zx1mwwh7f85qmd6s7k")))

(define-public crate-arrows-svg-1.0.0 (c (n "arrows-svg") (v "1.0.0") (d (list (d (n "domrs") (r "^0.0.6") (d #t) (k 0)))) (h "1sb8a22r2m5q62knilzyyab60nwgh44x97r34z0m3jbna676q8is")))

