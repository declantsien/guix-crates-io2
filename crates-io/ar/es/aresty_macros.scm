(define-module (crates-io ar es aresty_macros) #:use-module (crates-io))

(define-public crate-aresty_macros-0.1.0 (c (n "aresty_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g0sg77sgva8gn7jnkjsjbhg81kq0d5yrhajhdl4x3hk305ylvif")))

