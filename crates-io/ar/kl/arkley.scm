(define-module (crates-io ar kl arkley) #:use-module (crates-io))

(define-public crate-arkley-0.0.1 (c (n "arkley") (v "0.0.1") (d (list (d (n "arkley_traits") (r "^0.0.1") (d #t) (k 0)))) (h "1ssyyaw0x9skn8s52nnaln04yr5rmdwiw2fwqyaydrd6a54530wr")))

(define-public crate-arkley-0.0.2 (c (n "arkley") (v "0.0.2") (d (list (d (n "arkley_numerics") (r "^0.0.2") (d #t) (k 0)) (d (n "arkley_traits") (r "^0.0.2") (d #t) (k 0)))) (h "18gpgmly87w2valri9y9cmjllwr885zhxbxl3bhyr4bysi1wvscv")))

