(define-module (crates-io ar kl arkley_traits) #:use-module (crates-io))

(define-public crate-arkley_traits-0.0.1 (c (n "arkley_traits") (v "0.0.1") (h "0s1v1rbkv8wz8k6jlw1slfv9nwc7h2q70w0jmgjpfn30yiirxyzj")))

(define-public crate-arkley_traits-0.0.2 (c (n "arkley_traits") (v "0.0.2") (h "0wdb4mdvsw7l24xw18zzqs1r1zcsip39wjzczc5dq13g6201yvy2")))

