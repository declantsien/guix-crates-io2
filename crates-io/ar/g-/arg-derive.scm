(define-module (crates-io ar g- arg-derive) #:use-module (crates-io))

(define-public crate-arg-derive-0.1.0 (c (n "arg-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "19q6nd5x13zm16jaq47c3zqz0wgsdwr3p3i8069zxbymafg18lqf")))

(define-public crate-arg-derive-0.2.0 (c (n "arg-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0vlb2sfi3zf59g3hsn933nbry2547ba482w3qb5sdrbv72bzhyv3")))

(define-public crate-arg-derive-0.2.1 (c (n "arg-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1qz411is3an491ixigdmi8svmkyzdndx840r9540p1fkm8vx88a3")))

(define-public crate-arg-derive-0.3.0 (c (n "arg-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1ikv3gajqpqpz9rh86jg4p5yx18mag3wi5pky95vjbhj02kxq0fh")))

(define-public crate-arg-derive-0.3.1 (c (n "arg-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "13gjcpk8ay46h3b63hbw5kza2hzx1myia54jc2sa6p238jycjyc6")))

(define-public crate-arg-derive-0.3.2 (c (n "arg-derive") (v "0.3.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "1jci9k1yjh1hp9wzc2cg0xypq704kzzq2y5r22nbd1asvdgkg9ak")))

(define-public crate-arg-derive-0.3.3 (c (n "arg-derive") (v "0.3.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0jkynk7a5nxickic9x8mwa5v27m2nkzh1114ck187ywzm4yga7sk")))

(define-public crate-arg-derive-0.3.4 (c (n "arg-derive") (v "0.3.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0sxhbkp7hgchx37x6a0yp0yx9awp33a12rk827fpv37yip9zdx7m")))

(define-public crate-arg-derive-0.3.5 (c (n "arg-derive") (v "0.3.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0m0v9n3grl98wy50b6lhwcysrcw98wwy9ls24z69bglxsyl1agcb")))

(define-public crate-arg-derive-0.4.0 (c (n "arg-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "18ibyh3jwijsz5rzwc2247v2q8yfxhm0p8ydn3w92hxylxi22l3y")))

(define-public crate-arg-derive-0.4.1 (c (n "arg-derive") (v "0.4.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)))) (h "0q1cki2awsvcx1w53z5vxfvg621ayq1ffkxz27lirr865hbgr7l4")))

