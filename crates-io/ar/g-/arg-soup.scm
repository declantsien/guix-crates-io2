(define-module (crates-io ar g- arg-soup) #:use-module (crates-io))

(define-public crate-arg-soup-0.1.0 (c (n "arg-soup") (v "0.1.0") (h "1d92r5khcafdhgyaz04kc6g62ds6mfa0xrxyrbxgbwzsg1vzgfby")))

(define-public crate-arg-soup-1.0.0 (c (n "arg-soup") (v "1.0.0") (h "1n4w0qkfq9ws2jq40qgqidhl5i1a8aikbzmaws91rj423bgsd4mk")))

(define-public crate-arg-soup-1.1.0 (c (n "arg-soup") (v "1.1.0") (h "16k76c51g00rplbkl8j5nh0x5blhmvpxy0xwgmn00j1j6kc628lc")))

(define-public crate-arg-soup-1.1.1 (c (n "arg-soup") (v "1.1.1") (h "0vvc41zs4jr76shqzns948n6y11jah9gy7si1wrr2za0ah6jynzx")))

(define-public crate-arg-soup-1.2.0 (c (n "arg-soup") (v "1.2.0") (h "16hdszci4wgxiqnxg7iy2zdfkmzrgyhhq33m4h8bzyq468z40mfy")))

(define-public crate-arg-soup-1.3.0 (c (n "arg-soup") (v "1.3.0") (h "0llcj9fv5xxcpkcy7r2hhgg58c0sljh5xcnm2b1qvz6lgy9psclv")))

(define-public crate-arg-soup-2.0.0 (c (n "arg-soup") (v "2.0.0") (h "0a3f8c7ymj2xgh3fc4prkm0yx1yfnbz851ry1f17394nkgygf7hp")))

