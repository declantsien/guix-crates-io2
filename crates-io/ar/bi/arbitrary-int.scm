(define-module (crates-io ar bi arbitrary-int) #:use-module (crates-io))

(define-public crate-arbitrary-int-1.0.0 (c (n "arbitrary-int") (v "1.0.0") (h "1f4yfvm1i323sk2cpdaik5jkd5jk02cavhvy4d833mk0698icddi")))

(define-public crate-arbitrary-int-1.1.0 (c (n "arbitrary-int") (v "1.1.0") (h "0x814r73lqzhvnmp1lq9rhz0krpam8y95qnj43qkmcwqybl2phb4")))

(define-public crate-arbitrary-int-1.2.0 (c (n "arbitrary-int") (v "1.2.0") (h "1qzh57badyzgysh1nvkdb2m6bx57xih0l4vgh5vgx8px9y7m143n")))

(define-public crate-arbitrary-int-1.2.1 (c (n "arbitrary-int") (v "1.2.1") (h "0958rmav3cgss6q6rns70y73hif1il5vkvdrwss51p00qiv005w0")))

(define-public crate-arbitrary-int-1.2.2 (c (n "arbitrary-int") (v "1.2.2") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "0xbzjl4nqcvcqadnl8agx2njmqz7igwc6wkxn802rcss66rcl7lz") (f (quote (("std"))))))

(define-public crate-arbitrary-int-1.2.3 (c (n "arbitrary-int") (v "1.2.3") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "1qjf2c9axjl961l6lmyc614wcp4zv2kwg3msfrl9is8bv61dqrr9") (f (quote (("std"))))))

(define-public crate-arbitrary-int-1.2.4 (c (n "arbitrary-int") (v "1.2.4") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "1wyv962kihnlw8qppnc83ib7lrn644s32gj93zysq0a0wijpwnq7") (f (quote (("std") ("nightly"))))))

(define-public crate-arbitrary-int-1.2.5 (c (n "arbitrary-int") (v "1.2.5") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "1ngrk2jhigldc3zxvn0hk23gvc2m8jiw66yk4615x268qy8zfjba") (f (quote (("std") ("nightly"))))))

(define-public crate-arbitrary-int-1.2.6 (c (n "arbitrary-int") (v "1.2.6") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (k 0)))) (h "0y85q886hqanfq92xj9rbvxa9k1afhcmphm0fyab4vnddrc2m3py") (f (quote (("std") ("const_convert_and_const_trait_impl"))))))

(define-public crate-arbitrary-int-1.2.7 (c (n "arbitrary-int") (v "1.2.7") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0vgl3n5zzpdn2hxpz6gqk8zg2psk5gwyjzsgpngzd9iqwc1w0ky8") (f (quote (("step_trait") ("std") ("const_convert_and_const_trait_impl")))) (s 2) (e (quote (("serde" "dep:serde") ("defmt" "dep:defmt"))))))

