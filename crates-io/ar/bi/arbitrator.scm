(define-module (crates-io ar bi arbitrator) #:use-module (crates-io))

(define-public crate-arbitrator-0.1.0 (c (n "arbitrator") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0kj2nl23f8avda1669yc57shwrhdp1pjjjbqgsmzs2p2ag45d1bb")))

(define-public crate-arbitrator-0.1.1 (c (n "arbitrator") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0dmg8x59ljgwzjw8ym75lbzsz4bwcr0pbd3kmzbs2q08zy27n9pk")))

(define-public crate-arbitrator-0.1.2 (c (n "arbitrator") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0n85bd9p0z3p0g9cl8r344blv3pz8nsqzrhq7w4wglwh4mvb3xvy")))

(define-public crate-arbitrator-0.1.3 (c (n "arbitrator") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "01sz5qyhaybpbfshm49w5gfjzmp59gw42hhrr7460f70qpja5f0q")))

