(define-module (crates-io ar bi arbitrary-chunks) #:use-module (crates-io))

(define-public crate-arbitrary-chunks-0.2.1 (c (n "arbitrary-chunks") (v "0.2.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1kll6m3l9la5n6jh77kzn3mm3l9cjxg64cvzbb452ss66ifs9wka")))

(define-public crate-arbitrary-chunks-0.3.0 (c (n "arbitrary-chunks") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1115b14yr326d6d619ighj65nfyzmmn79i6b0a30fmy1phlfvbq3")))

(define-public crate-arbitrary-chunks-0.4.0 (c (n "arbitrary-chunks") (v "0.4.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1g9844s6h2ysqsygm6pv63sd4wlvgp7a9bbc63k56yw14vq9jn4d")))

(define-public crate-arbitrary-chunks-0.4.1 (c (n "arbitrary-chunks") (v "0.4.1") (d (list (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "11j0xrjcnj8zygdpwqpc9110ac6yji35lw8mx80w85k492d6in1a")))

