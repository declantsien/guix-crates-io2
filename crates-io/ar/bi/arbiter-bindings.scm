(define-module (crates-io ar bi arbiter-bindings) #:use-module (crates-io))

(define-public crate-arbiter-bindings-0.1.0 (c (n "arbiter-bindings") (v "0.1.0") (d (list (d (n "ethers") (r "=2.0.10") (d #t) (k 0)) (d (n "revm") (r "=3.5.0") (f (quote ("ethersdb" "std" "serde"))) (d #t) (k 0)) (d (n "revm-primitives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1apjy98rg5n66rhbm42nmsr89amnzbs4c50x27h7lgygcfzj7y89")))

(define-public crate-arbiter-bindings-0.1.1 (c (n "arbiter-bindings") (v "0.1.1") (d (list (d (n "ethers") (r "=2.0.10") (d #t) (k 0)) (d (n "revm") (r "=3.5.0") (f (quote ("ethersdb" "std" "serde"))) (d #t) (k 0)) (d (n "revm-primitives") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wdp3r809frdvj7r7b4wf2kydvzk05lzmdvd728g8hqdr7z490r1")))

(define-public crate-arbiter-bindings-0.1.2 (c (n "arbiter-bindings") (v "0.1.2") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kb58q0vbnf335485b8fdw6hcl7axag9ka943zwy17cnqfhpvxxx")))

(define-public crate-arbiter-bindings-0.1.3 (c (n "arbiter-bindings") (v "0.1.3") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s4yl9vz1fm5l2hsd7pik9zy7z9jqhbzqp2x4j6w6ig8i2wnpcip")))

(define-public crate-arbiter-bindings-0.1.4 (c (n "arbiter-bindings") (v "0.1.4") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z2ipgacb3l6dhs8f34fgna9j614y1gh6jnd33qbwckgbvm360vd")))

(define-public crate-arbiter-bindings-0.1.5 (c (n "arbiter-bindings") (v "0.1.5") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mkryh3his1c1vyyiri9p1s6yycjmr1a3nycxafnammli5v54s8y")))

(define-public crate-arbiter-bindings-0.1.6 (c (n "arbiter-bindings") (v "0.1.6") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f2afnxx46myya7nf4sbn3ygyw6xx3xw5lwmwssmcghfbcr9l67p")))

(define-public crate-arbiter-bindings-0.1.7 (c (n "arbiter-bindings") (v "0.1.7") (d (list (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "09mvqg6bn3cv6hsvqraffpr6jflm09x582d5n3iwqznyszmqhlnb")))

