(define-module (crates-io ar bi arbintrary) #:use-module (crates-io))

(define-public crate-arbintrary-0.1.0 (c (n "arbintrary") (v "0.1.0") (h "1qicbf7f4mx12qmi47d8nclj1zz065ymgn5ygn0vgxiwqhzf0l5b") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.1.1 (c (n "arbintrary") (v "0.1.1") (h "1swnc7zj9cff9kszs0brcf648wg0pqbprfl5ng11j73mcg73wik1") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.1.2 (c (n "arbintrary") (v "0.1.2") (h "1fwbhp6kfjkv6fy4ll4d479d0v28ln84a705s550x4plfrcb55rr") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.2.0 (c (n "arbintrary") (v "0.2.0") (h "15vmh2mfm60h73kqjf7qz4kp3sfgdyxpv8cj4n0rf1prd8jimdn3") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.2.1 (c (n "arbintrary") (v "0.2.1") (h "0bz35njmv4qsdis1b417rpk645w09iz9lh7p5zghygikx88ppi6l") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.3.0 (c (n "arbintrary") (v "0.3.0") (h "0264qr3ivh37z5hq21p1jlcz580f0xkmfqz2h8qcbif9jmzrri4g") (f (quote (("std") ("default" "std"))))))

(define-public crate-arbintrary-0.4.0 (c (n "arbintrary") (v "0.4.0") (h "1fl9x4s7164w5iqy9vwx20y0xs8ir9b77c6kqma1mngq3821vv2k") (f (quote (("std") ("default" "std"))))))

