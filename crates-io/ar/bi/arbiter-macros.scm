(define-module (crates-io ar bi arbiter-macros) #:use-module (crates-io))

(define-public crate-arbiter-macros-0.1.0 (c (n "arbiter-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "06i1b2hg8gby26j3kar34vrfq168b6m2njdrwccd0knm26rw8ayy")))

(define-public crate-arbiter-macros-0.1.1 (c (n "arbiter-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "04g4g0xqa6rhij9wahrgw9jlyjr9gvldjy6b34wi3iwh43pak1d6")))

(define-public crate-arbiter-macros-0.1.2 (c (n "arbiter-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1gkii0xryy5nicbpysarm6rs6qxm8qvm792w8az9qgzpazlnc9v2")))

(define-public crate-arbiter-macros-0.1.3 (c (n "arbiter-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "081033fbf66p4isnx14cbb4x64wvs5q89fsqxb24aa4yl7zh8jmc")))

(define-public crate-arbiter-macros-0.1.4 (c (n "arbiter-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "02cwxig001v4i5w5zkpb93jvpds6q4lwv7zmdj0s38ld3b6q6kqx")))

