(define-module (crates-io ar bi arbitrary_ext) #:use-module (crates-io))

(define-public crate-arbitrary_ext-0.1.0 (c (n "arbitrary_ext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "055zymr49bydzcgcv0mjmkcrvcib2l11jzahgn9jj6sb8hb387ik")))

(define-public crate-arbitrary_ext-0.2.0 (c (n "arbitrary_ext") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "19v2wj2rw3dam0m8w88yprfpsr2bb4zymraw3qla42kj9bcgk4in")))

(define-public crate-arbitrary_ext-0.2.1 (c (n "arbitrary_ext") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1ly712nmlsn5ffisx5f2p8rnks79g7aka9qk15svgxbz7q6x59fj")))

(define-public crate-arbitrary_ext-0.3.0 (c (n "arbitrary_ext") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "10pjvnhj3gvf6388dnpriyz4h4hm6ggsyc757gdir8z01ij0f7jp")))

