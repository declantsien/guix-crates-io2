(define-module (crates-io ar bi arbitrary-lock) #:use-module (crates-io))

(define-public crate-arbitrary-lock-0.1.0 (c (n "arbitrary-lock") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "0kpjpibk9xrlfyw89jhsdbmcy2g38zk713mzbii92w62pkyglxs2")))

(define-public crate-arbitrary-lock-0.1.1 (c (n "arbitrary-lock") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)))) (h "088j1zic1b0ysmrk6ncv4ic6rfn5yr9vw3qdzg3k0p86isjp7v4k")))

