(define-module (crates-io ar bi arbiter-derive) #:use-module (crates-io))

(define-public crate-arbiter-derive-0.1.0 (c (n "arbiter-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.35") (d #t) (k 0)))) (h "1z7nb7746jmpcpcd4cypl51ynvpxb2kynb92c4ib0dp3b6wpr9ry")))

(define-public crate-arbiter-derive-0.1.1 (c (n "arbiter-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.38") (d #t) (k 0)))) (h "0ijhbbvbx4js1fsz884q6n1b5s78kkbhhhwls7vw48a2dpkh72kn")))

(define-public crate-arbiter-derive-0.1.2 (c (n "arbiter-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (d #t) (k 0)))) (h "02yr4sjfx3f3n1z8pfrr9vnh7wzghmd2wy3qy3c580rhfsxfva9s")))

(define-public crate-arbiter-derive-0.1.3 (c (n "arbiter-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "=1.0.69") (d #t) (k 0)) (d (n "quote") (r "=1.0.33") (d #t) (k 0)) (d (n "syn") (r "=2.0.39") (d #t) (k 0)))) (h "1hdkk1fbwmjld11hrpfc99sk9dwnibxcmyydf42ly2fg688cwc4q")))

