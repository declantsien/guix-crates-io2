(define-module (crates-io ar bi arbitrary-json) #:use-module (crates-io))

(define-public crate-arbitrary-json-0.1.0 (c (n "arbitrary-json") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rfpr2ac1qpr1dw3mi1gr34r4mnd197mfff2vvpmisr6557k1x9x")))

(define-public crate-arbitrary-json-0.1.1 (c (n "arbitrary-json") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q209aiv9jckkk53z0n16rsggacghd0njpsy0qzs7ysbbwipl488")))

