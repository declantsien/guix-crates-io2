(define-module (crates-io ar bi arbitrage) #:use-module (crates-io))

(define-public crate-arbitrage-0.0.1 (c (n "arbitrage") (v "0.0.1") (h "0cr0sggi7j1v15gskhc89a7bk4czm5yncz5nig9wvxwcf09d7x0f")))

(define-public crate-arbitrage-0.0.2 (c (n "arbitrage") (v "0.0.2") (h "1wmaw3xqgkz34wrv0wf4lhxvx38zs3r9jxj5kiyf16pq4vpghdf8")))

(define-public crate-arbitrage-0.0.3 (c (n "arbitrage") (v "0.0.3") (h "1sv5anln5rl72mdsgx4mr60mmhzwv72nvdxaawjdq7jr3j6x0qjs")))

