(define-module (crates-io ar bi arbitrary) #:use-module (crates-io))

(define-public crate-arbitrary-0.1.0 (c (n "arbitrary") (v "0.1.0") (h "1lximvzl2sq0bm7601ab17fwi9nzanmn05c6hc6klq53lgmg99ds")))

(define-public crate-arbitrary-0.1.1 (c (n "arbitrary") (v "0.1.1") (h "1vk2vylkzmj82vq9h1jd9idq44iwq0229br7iggpl4ism8iiazbc")))

(define-public crate-arbitrary-0.2.0 (c (n "arbitrary") (v "0.2.0") (h "1i3fhcdyjq4isn22xx2svmpfr5hwyzi0wavbm07fs8i2dv5pdkv4")))

(define-public crate-arbitrary-0.3.0 (c (n "arbitrary") (v "0.3.0") (d (list (d (n "derive_arbitrary") (r "= 0.3.0") (o #t) (d #t) (k 0)))) (h "1xwqmpnv59nwmbz9qqqrd8wcdvs38i5108d1gyw83mhx4siwslmy") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.3.1 (c (n "arbitrary") (v "0.3.1") (d (list (d (n "derive_arbitrary") (r "= 0.3.1") (o #t) (d #t) (k 0)))) (h "0k0s63vmhpsk13879myd754mi80nh84vy13w0yf6f18yj49cvplc") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.3.2 (c (n "arbitrary") (v "0.3.2") (d (list (d (n "derive_arbitrary") (r "= 0.3.1") (o #t) (d #t) (k 0)))) (h "0ncqwhn2y8lfg4639spp06l3j1gqqh4xr9fnpar4pwgqd9bj4fxr") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.3.3 (c (n "arbitrary") (v "0.3.3") (d (list (d (n "derive_arbitrary") (r "= 0.3.3") (o #t) (d #t) (k 0)))) (h "0pbrg6w68v63r99j0mgx9g7ykgpqnd22g870q4gzywx0n515w7a9") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.0 (c (n "arbitrary") (v "0.4.0") (d (list (d (n "derive_arbitrary") (r "= 0.4.0") (o #t) (d #t) (k 0)))) (h "193nyvymngpv6faampb5w88wnbhhmyhccv2ll7r5qp761hpiz5qn") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.1 (c (n "arbitrary") (v "0.4.1") (d (list (d (n "derive_arbitrary") (r "= 0.4.0") (o #t) (d #t) (k 0)))) (h "1vdj98qpib6hgkimdflixiz658944dqc7gyz6abxpmzdznakq5bm") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.2 (c (n "arbitrary") (v "0.4.2") (d (list (d (n "derive_arbitrary") (r "= 0.4.2") (o #t) (d #t) (k 0)))) (h "1snkr9v8xv8i0rklm301c71a2h1az0qdvx9yrk20ffirbnrcjj0i") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.3 (c (n "arbitrary") (v "0.4.3") (d (list (d (n "derive_arbitrary") (r "= 0.4.3") (o #t) (d #t) (k 0)))) (h "16px0qckivh3ijdfp17c0kgsknzzak0s2s13vyb3k6g13040ca21") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.4 (c (n "arbitrary") (v "0.4.4") (d (list (d (n "derive_arbitrary") (r "= 0.4.4") (o #t) (d #t) (k 0)))) (h "0hv13pr48rzwp6svlx278nyj0nagqdhr8av3yzr6jcwamflh3sy5") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.5 (c (n "arbitrary") (v "0.4.5") (d (list (d (n "derive_arbitrary") (r "=0.4.5") (o #t) (d #t) (k 0)))) (h "1x60qfkifl0kbm09h699ppg09kf3dzsxri7q6i5zzaky0pql9dbw") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.6 (c (n "arbitrary") (v "0.4.6") (d (list (d (n "derive_arbitrary") (r "=0.4.6") (o #t) (d #t) (k 0)))) (h "11wx9qkbsp5va1zp6rkg25cwjp2ff2vgw0sn3q8lx95m8vks68h9") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-0.4.7 (c (n "arbitrary") (v "0.4.7") (d (list (d (n "derive_arbitrary") (r "=0.4.7") (o #t) (d #t) (k 0)))) (h "0sa55cynafwzvlhyhfpm3vmi2fydj3ipdj5yfbaif7l56cixfmfv") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.0-rc1 (c (n "arbitrary") (v "1.0.0-rc1") (d (list (d (n "derive_arbitrary") (r ">=1.0.0-rc1, <2.0.0") (o #t) (d #t) (k 0)))) (h "1wx0lg12grh2zbvhh9xahddvnak4npvm9mjrb7r3ikv3qrvv1089") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.0-rc2 (c (n "arbitrary") (v "1.0.0-rc2") (d (list (d (n "derive_arbitrary") (r "^1.0.0-rc1") (o #t) (d #t) (k 0)))) (h "1pm2iawwnd9lydkns6mvff0qxk82vs75sxaz9z277gmxiacjqd8v") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.0 (c (n "arbitrary") (v "1.0.0") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0awyyh1s0yxjhaq23r0yyv4i0y1041z35c5n8pxk1mx9c6lnb2v9") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.1 (c (n "arbitrary") (v "1.0.1") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "14a6r7q9b1kf1m7810p8bcl51q11mrwc5z7fjkz0lx6kdvyk0x13") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.2 (c (n "arbitrary") (v "1.0.2") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "19zwmlbliqpjjvcyr667rhj39dqyn08h1ib3z1lriffpmjj0hysp") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.0.3 (c (n "arbitrary") (v "1.0.3") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1pjlk4kzwh09arg5bfpi57py3ljbm3wz8a57grrxmsnwxzn7c32i") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.0 (c (n "arbitrary") (v "1.1.0") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "14148yi6hmkycacdrvykkky2ymnph9xmny1y39gc4wgng5mnp2y3") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.1 (c (n "arbitrary") (v "1.1.1") (d (list (d (n "derive_arbitrary") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1kwcfc8qr2qimq2v6xwmjicrjkr946nhf469jyfa5kl6rjkqfx2g") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.2 (c (n "arbitrary") (v "1.1.2") (d (list (d (n "derive_arbitrary") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "11wsjzfn9fmingzidj1r5fchjvc7gchzidscn541y6rgy4na1q15") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.3 (c (n "arbitrary") (v "1.1.3") (d (list (d (n "derive_arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)))) (h "0q7xqpf9abj8yfq9632rbkdnkpig1ar3xw1hyq7rgc9q3x9j8yas") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.4 (c (n "arbitrary") (v "1.1.4") (d (list (d (n "derive_arbitrary") (r "^1.1.4") (o #t) (d #t) (k 0)))) (h "0bia1ibs6z8lm0p2svic7dwxc31rlldrsaybqs01kgxrd91yncc9") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.5 (c (n "arbitrary") (v "1.1.5") (d (list (d (n "derive_arbitrary") (r "^1.1.5") (o #t) (d #t) (k 0)))) (h "1m0s93b080mwc08jn1pibz23q3mbpv5wgv860yhg7adawks8f3av") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.6 (c (n "arbitrary") (v "1.1.6") (d (list (d (n "derive_arbitrary") (r "^1.1.6") (o #t) (d #t) (k 0)))) (h "1mcy8sm9vdn2lzmm3qf78gv7557mpjrhakxkzpm43fali2228hgl") (f (quote (("derive" "derive_arbitrary"))))))

(define-public crate-arbitrary-1.1.7 (c (n "arbitrary") (v "1.1.7") (d (list (d (n "derive_arbitrary") (r "^1.1.6") (o #t) (d #t) (k 0)))) (h "0pysx0vmkndnkyqpzf5ymc4lw5jzrpchfhy49iv7iarcj46x2vyq") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.2.0 (c (n "arbitrary") (v "1.2.0") (d (list (d (n "derive_arbitrary") (r "^1.1.6") (o #t) (d #t) (k 0)))) (h "106qgz0qxs202xlvjfyvw8dkb6ynr1ymmcclfh89l56mj2zpzm19") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.2.2 (c (n "arbitrary") (v "1.2.2") (d (list (d (n "derive_arbitrary") (r "^1.2.2") (o #t) (d #t) (k 0)))) (h "1rcy97lqnzn8w29a1x6xjh9mq4fiig8z4bxcbx8yyyifz4w4j8mh") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.2.3 (c (n "arbitrary") (v "1.2.3") (d (list (d (n "derive_arbitrary") (r "^1.2.3") (o #t) (d #t) (k 0)))) (h "1bdd7s3jvj02mkhy5pcfymab47jhi3zxl29dxy9v59sswr6sz41y") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.3.0 (c (n "arbitrary") (v "1.3.0") (d (list (d (n "derive_arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0km5cj0sxfzv863blfjpz49mlikaxbaslyzk463i9jn1fgzril72") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.3.1 (c (n "arbitrary") (v "1.3.1") (d (list (d (n "derive_arbitrary") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "exhaustigen") (r "^0.1.0") (d #t) (k 2)))) (h "0nf8hc9lb6jx390if0gpqgavgl3bnbc2pv0j8mq2n8daplx3gqd2") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

(define-public crate-arbitrary-1.3.2 (c (n "arbitrary") (v "1.3.2") (d (list (d (n "derive_arbitrary") (r "^1.3.2") (o #t) (d #t) (k 0)) (d (n "exhaustigen") (r "^0.1.0") (d #t) (k 2)))) (h "0471f0c4f1bgibhyhf8vnapkp158h1nkrzx0wnq97jwd9n0jcnkx") (f (quote (("derive" "derive_arbitrary")))) (r "1.63.0")))

