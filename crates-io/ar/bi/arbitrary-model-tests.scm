(define-module (crates-io ar bi arbitrary-model-tests) #:use-module (crates-io))

(define-public crate-arbitrary-model-tests-0.1.0 (c (n "arbitrary-model-tests") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06n89ih549dz5g1zp1z8p2r4j94kgb5dzmhi5pqdaak6ycl5svh6")))

