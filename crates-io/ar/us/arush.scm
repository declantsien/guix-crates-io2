(define-module (crates-io ar us arush) #:use-module (crates-io))

(define-public crate-arush-0.0.1 (c (n "arush") (v "0.0.1") (h "0q9sgjcyjjfb3451ad4fcl17w2xdx0b2ry5mrv9jgdkffalsiwl2") (y #t)))

(define-public crate-arush-0.0.5 (c (n "arush") (v "0.0.5") (h "1ihghwys2r968vzb5cgw17p5869adhcmmkq33dkf1pafcclj7hx4")))

