(define-module (crates-io ar pl arplayer) #:use-module (crates-io))

(define-public crate-arplayer-0.1.0 (c (n "arplayer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "sysctl") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "1az8mc2w7b89dn6kyij87aibdb5s830g1r2hr27hsx2q1wlalr5p")))

(define-public crate-arplayer-0.1.1 (c (n "arplayer") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "sysctl") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "1av61xl2g72i9bhw6f8v9z3iaaphzwfhws8yg74aag8wrlvzmalc")))

(define-public crate-arplayer-0.1.2 (c (n "arplayer") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "sysctl") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "02fz891slmy985sizbs30x0ipbqfn2z3110q5963wjhr0grfix30")))

