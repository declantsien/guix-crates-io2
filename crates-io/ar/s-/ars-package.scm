(define-module (crates-io ar s- ars-package) #:use-module (crates-io))

(define-public crate-ars-package-0.0.1 (c (n "ars-package") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "05zvlzlx1sq7x34isf3ag2jqxh7yw2ljk3ky5a5675glsm9i9hcf")))

(define-public crate-ars-package-0.0.2 (c (n "ars-package") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1nanjyr1yskzb8li7dzsixq8ddv3cz97m3wnd0gwzpd9dr7pzpa1")))

(define-public crate-ars-package-0.0.3 (c (n "ars-package") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mwjygbh989vxp5kd9dqzr6337aw20g7dr6ka5gyigd7kzjzcjc0")))

(define-public crate-ars-package-0.0.4 (c (n "ars-package") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0ghs0x8r81mwx7499k0a6zdx87yvs6l8c2hhsjadpn9n30kijda5")))

(define-public crate-ars-package-0.0.5 (c (n "ars-package") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0c4vvnkrgjkmajygzl04kn5jji4q92h52n3mdwivmf8yqdb1sym7")))

(define-public crate-ars-package-0.0.6 (c (n "ars-package") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1cn2cwvxjbcpwcrma9l829f3hmky96hxr3w85driaakhs6123sw4")))

(define-public crate-ars-package-0.0.7 (c (n "ars-package") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "13bigyrkm9401mb13pxfgpr94gjrd6kpqvaf9jfdnx1li0qn6j1i")))

(define-public crate-ars-package-0.0.8 (c (n "ars-package") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "14ngv3i2n70gz72qra9l0fpz9zn81pxr1pgm4dz7gfpnwil5lqbp")))

(define-public crate-ars-package-0.0.9 (c (n "ars-package") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1cwkh635v0izb8bqcsyqcz9ld8d2wknxlvy078dpyjbrns7zhf8x")))

(define-public crate-ars-package-0.0.10 (c (n "ars-package") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (f (quote ("validation"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0bkcm04ksdv36mv5a96q0spmw88wz7n22s0nca0wakpfgfrnqqyb")))

