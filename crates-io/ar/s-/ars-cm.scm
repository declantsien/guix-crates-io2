(define-module (crates-io ar s- ars-cm) #:use-module (crates-io))

(define-public crate-ars-cm-0.1.0 (c (n "ars-cm") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kcy2fdzxyzg8dxhjnfp7jz5kwyra95spy5wixl41gc9pn0293wa")))

(define-public crate-ars-cm-0.2.0 (c (n "ars-cm") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ha2ch9508zl37sy7f6wp1z2fdavqp1n8q4hi0w767vl1n6ark6a")))

(define-public crate-ars-cm-0.2.1 (c (n "ars-cm") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w1ap0015j4vk6xq1yxh08l1ycsv1y7lnggx5isb7yfvhhr0g7kn")))

