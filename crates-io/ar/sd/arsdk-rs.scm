(define-module (crates-io ar sd arsdk-rs) #:use-module (crates-io))

(define-public crate-arsdk-rs-0.0.1 (c (n "arsdk-rs") (v "0.0.1") (h "1mn98zc64apwqnq6wnjncgvmqzh8r8a4g34nbvay0i254dpsf5ih")))

(define-public crate-arsdk-rs-0.0.2 (c (n "arsdk-rs") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pnet") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fllsrbf57kxzh9kmpwy0y24vr22ckhw6m7c1q00sg1zyrmibm2f")))

(define-public crate-arsdk-rs-0.0.3 (c (n "arsdk-rs") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pnet") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kddih56kai4wcdxqp3bccibh8h28q0vc8xvqwgzna1k41006qg9")))

(define-public crate-arsdk-rs-0.0.4 (c (n "arsdk-rs") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "pnet") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wqabmhpcjlzcrjny15dx0bi814xjykim595bj0xb6wazpphqpnn")))

(define-public crate-arsdk-rs-0.0.5 (c (n "arsdk-rs") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "pnet") (r "^0.25") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0accg1n1pxr2zsn9i4a76wd87s449z00jdijmwgja4yynygv0s4y")))

