(define-module (crates-io ar _a ar_archive_writer) #:use-module (crates-io))

(define-public crate-ar_archive_writer-0.1.0 (c (n "ar_archive_writer") (v "0.1.0") (d (list (d (n "object") (r "^0.29.0") (f (quote ("std" "read"))) (k 0)))) (h "1yzhs2skyqqcl92873f02wmjqxnsxa3vk9awfipvn947lyaady0n")))

(define-public crate-ar_archive_writer-0.1.1 (c (n "ar_archive_writer") (v "0.1.1") (d (list (d (n "object") (r "^0.29.0") (f (quote ("std" "read"))) (k 0)))) (h "0cdsm6g3rfw2gb5sppri4pkqybjdgrkgq7xai3msvzan0nc82s17")))

(define-public crate-ar_archive_writer-0.1.2 (c (n "ar_archive_writer") (v "0.1.2") (d (list (d (n "object") (r "^0.30.0") (f (quote ("std" "read"))) (k 0)))) (h "1764k396k3dx3zh6n57p2yy3kcyw2cz42gpl5kxfpg1fw3689if2")))

(define-public crate-ar_archive_writer-0.1.3 (c (n "ar_archive_writer") (v "0.1.3") (d (list (d (n "object") (r "^0.30.0") (f (quote ("std" "read"))) (k 0)))) (h "1fy9mk81xx78fb1vcjsbnrijl5ycd23xra5w3iyik8qpzm0r8qxh")))

(define-public crate-ar_archive_writer-0.1.4 (c (n "ar_archive_writer") (v "0.1.4") (d (list (d (n "object") (r "^0.31.0") (f (quote ("std" "read"))) (k 0)))) (h "0k09gf5d6ipq2ryp9a9gq5c2sf5l41mv0pyb68i157m5h2cb7kvl")))

(define-public crate-ar_archive_writer-0.1.5 (c (n "ar_archive_writer") (v "0.1.5") (d (list (d (n "object") (r "^0.32.0") (f (quote ("std" "read"))) (k 0)))) (h "0w9ypimlfrmqss1s6c502mwmbl3i0sd76lz49xzpwg8plmyd74lp")))

(define-public crate-ar_archive_writer-0.2.0 (c (n "ar_archive_writer") (v "0.2.0") (d (list (d (n "cargo-binutils") (r "^0.3.6") (d #t) (k 2)) (d (n "object") (r "^0.32.0") (f (quote ("std" "read"))) (k 0)) (d (n "object") (r "^0.32.0") (f (quote ("write" "xcoff"))) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "16ikh370y20xivq73hjzq9dz6zw4pysnkkx0mpbykrbg9f4nkhph")))

(define-public crate-ar_archive_writer-0.3.0 (c (n "ar_archive_writer") (v "0.3.0") (d (list (d (n "cargo-binutils") (r "^0.3.6") (d #t) (k 2)) (d (n "object") (r "^0.35.0") (f (quote ("std" "read"))) (k 0)) (d (n "object") (r "^0.35.0") (f (quote ("write" "xcoff"))) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0cvpyryv40w4p2andg230w4yilamxprk49asp9n3aqq6d4njlhgq")))

