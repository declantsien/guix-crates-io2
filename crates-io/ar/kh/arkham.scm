(define-module (crates-io ar kh arkham) #:use-module (crates-io))

(define-public crate-arkham-0.1.0 (c (n "arkham") (v "0.1.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "0s65q7zx22qn1id4yl6vj2par71fbfjb37klfbs9r3sm1qh82r4p") (f (quote (("config_toml" "toml" "config") ("config_json" "serde_json" "config") ("config" "serde")))) (y #t)))

(define-public crate-arkham-0.1.1 (c (n "arkham") (v "0.1.1") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (o #t) (d #t) (k 0)))) (h "0fyyl6r0hc6balahh1g7yhnwgzz37f5yfd3yan885x09js2jz5bs") (f (quote (("serialize" "serde") ("config_toml" "toml" "config") ("config_json" "serde_json" "config") ("config" "serialize")))) (y #t)))

(define-public crate-arkham-0.2.0 (c (n "arkham") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ctrlc") (r "^3.3.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "18vpm02np3v7v3xk98f1mqqdnh14w6yihr4z8i2w09l8qrjl62qv")))

(define-public crate-arkham-0.2.1 (c (n "arkham") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "ctrlc") (r "^3.3.1") (d #t) (k 0)))) (h "1k83lq4qkhc2d5bnvfy6ikb97q94c48cmbhbmmiff7l262inz56r")))

