(define-module (crates-io ar -r ar-reshaper) #:use-module (crates-io))

(define-public crate-ar-reshaper-0.2.0 (c (n "ar-reshaper") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1plzijqlr3zhlvs5rc35asni96npwg3vjdmxmbj0fhvr0bak09zb") (f (quote (("default") ("config_from_font" "ttf-parser"))))))

(define-public crate-ar-reshaper-0.2.1 (c (n "ar-reshaper") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "12f1pi7kz3w6wjvhs18cgjc9ng4cq1020937vlcdwprcsbylcq80") (f (quote (("default") ("config_from_font" "ttf-parser"))))))

(define-public crate-ar-reshaper-0.2.2 (c (n "ar-reshaper") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "08gws8q2lqnvhjm6d7d2hdv901agj5zxvp0g0lhnyajdl3jv2yhi") (f (quote (("default") ("config_from_font" "ttf-parser"))))))

(define-public crate-ar-reshaper-0.2.3 (c (n "ar-reshaper") (v "0.2.3") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1s52a2x3bqrgzqmwkbraip97860ayr09c8bnipn7714hb0233l3x") (f (quote (("default"))))))

(define-public crate-ar-reshaper-0.2.4 (c (n "ar-reshaper") (v "0.2.4") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1dxk6drmdsadd6fbm9pbfiqbcacgx6cl3qrkx4v1p9anypyq8c79") (f (quote (("default"))))))

(define-public crate-ar-reshaper-0.3.0 (c (n "ar-reshaper") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "18bwrs0kbjhd64a4qk2z0vz6nhcfak5mzpq8yjkbxalzaq5rk5cf") (f (quote (("default"))))))

(define-public crate-ar-reshaper-0.3.1 (c (n "ar-reshaper") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0ina3q0dq8g8r776gggxjjx245x0j4f39k9l1cimgsc7jaycz2a0") (f (quote (("default"))))))

(define-public crate-ar-reshaper-0.3.2 (c (n "ar-reshaper") (v "0.3.2") (d (list (d (n "bitflags") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0dw2l3yjpyz320fppy4d0jr9i083k3md0pgsgnm1lj8hmsv5g67c") (f (quote (("default"))))))

(define-public crate-ar-reshaper-0.3.4 (c (n "ar-reshaper") (v "0.3.4") (d (list (d (n "bitflags") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "05lm16c4lcaggxv7wcdyc5sy60klxh8yfpnnd2y3mz2klil4w63v") (f (quote (("default"))))))

(define-public crate-ar-reshaper-1.0.0 (c (n "ar-reshaper") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "0wvbnqwcqc8ad1qcsi7bdx31jjm2lzw1zxrghbyz6kr4ivix8lay") (f (quote (("default"))))))

(define-public crate-ar-reshaper-1.1.0 (c (n "ar-reshaper") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1vb8xxcgkcf12vbg5mwzx1iqzmdp582vynl1v3k9vd9flx5shgz5") (f (quote (("default"))))))

(define-public crate-ar-reshaper-1.3.0 (c (n "ar-reshaper") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19") (o #t) (d #t) (k 0)))) (h "0rkc2asa0z56agg4pzp9ir1rpk08fgs617l1diny9h2k0hmn34rs") (f (quote (("default"))))))

(define-public crate-ar-reshaper-1.3.1 (c (n "ar-reshaper") (v "1.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19") (o #t) (d #t) (k 0)))) (h "15f2hjkq8s2ss050661gv50l2y603jlf2k8d2g44l1rfy5p2v6ib") (f (quote (("default"))))))

(define-public crate-ar-reshaper-1.5.0 (c (n "ar-reshaper") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.20") (o #t) (d #t) (k 0)))) (h "00qcqhhln6hyv9pq4dm6cbq35l1d0cnbjbjs7lady9dpaxdyinh8") (f (quote (("default"))))))

