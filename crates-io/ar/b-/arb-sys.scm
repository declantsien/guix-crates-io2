(define-module (crates-io ar b- arb-sys) #:use-module (crates-io))

(define-public crate-arb-sys-0.1.0 (c (n "arb-sys") (v "0.1.0") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l2xfsv6i0r20ad3jgii23x9vldbfawwcdfsavad8ib3f0dk0vn9")))

(define-public crate-arb-sys-0.1.1 (c (n "arb-sys") (v "0.1.1") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0blqci7yl2dyaj7cyyicilzalpyh2hjzyxh48h7v8k5nyj3ac8hv")))

(define-public crate-arb-sys-0.2.0 (c (n "arb-sys") (v "0.2.0") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cag51szrpgrkaawx4rs30an5pmvqyiqv3yj33rlxvz4cn7i5l2r")))

(define-public crate-arb-sys-0.2.1 (c (n "arb-sys") (v "0.2.1") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m4ms7jfb0mkyppqhm9b2fw36i3307hnbzi2wrhlvd0iwrsr30sm")))

(define-public crate-arb-sys-0.2.2 (c (n "arb-sys") (v "0.2.2") (d (list (d (n "flint-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10h4s1nar2szmz1crrffrn2bah3vi2yzr04vz1f99ys7x59wwwdj")))

(define-public crate-arb-sys-0.2.3 (c (n "arb-sys") (v "0.2.3") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pkbikfkpwljyayaa9mr0d8pvwld58mpvk5h0gmcdsa6pbh7m0my")))

(define-public crate-arb-sys-0.2.4 (c (n "arb-sys") (v "0.2.4") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s6dm1vkv63bzfg7qnvkhc8rcy2vh5wsa8fhjd6827a75m1rlp10")))

(define-public crate-arb-sys-0.2.5 (c (n "arb-sys") (v "0.2.5") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10dpkr1hp8s7bmycxh7kpa2gck6sc8lj58xdb5kympi934rg25as")))

(define-public crate-arb-sys-0.2.6 (c (n "arb-sys") (v "0.2.6") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q9r39s3a5h066axsvn7hk4fd5mp7qgbn82xyy05rfqwrvcz4vgc")))

(define-public crate-arb-sys-0.2.7 (c (n "arb-sys") (v "0.2.7") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hn37x11kvm426hk1sg0x7qjxqy37k9fj939hggg2wc14dd4gjn9")))

(define-public crate-arb-sys-0.2.8 (c (n "arb-sys") (v "0.2.8") (d (list (d (n "flint-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)))) (h "048zrdmp02xxy9mdq71dvb82200xsalzbl5448r01jymvkvfxsnn")))

(define-public crate-arb-sys-0.3.0 (c (n "arb-sys") (v "0.3.0") (d (list (d (n "flint-sys") (r "^0.6") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "16p38bhkk54ph4zzalsdf62yiw3af9jxvrcw92qfxjznhrlmz4c8") (f (quote (("disable-make-check"))))))

(define-public crate-arb-sys-0.3.1 (c (n "arb-sys") (v "0.3.1") (d (list (d (n "flint-sys") (r "^0.6") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "0a197xhrlx0yj3acsrby1qj0m20m7f361z72sgmn2n64id4hzzqz") (f (quote (("disable-make-check"))))))

(define-public crate-arb-sys-0.3.2 (c (n "arb-sys") (v "0.3.2") (d (list (d (n "flint-sys") (r "^0.6") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "14f9vshk1acid2r3pwbpcsgnr6d43x4advhk05pyb6ks6iy8a985") (f (quote (("disable-make-check"))))))

(define-public crate-arb-sys-0.3.3 (c (n "arb-sys") (v "0.3.3") (d (list (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "04snvvb0cz8r3m4fmj8x1dlfa4r17dc9cchsvbs0cyvf45vkfcwm") (f (quote (("disable-make-check"))))))

(define-public crate-arb-sys-0.3.4 (c (n "arb-sys") (v "0.3.4") (d (list (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "04hsjnzly77q81fj51inr2g02321sshimj4j8ia9xmr1qrs2wvrz") (f (quote (("disable-make-check") ("default" "disable-make-check"))))))

(define-public crate-arb-sys-0.3.5 (c (n "arb-sys") (v "0.3.5") (d (list (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1x7li3vd2y3f60yxpbisfp4dn4avpry2p4xj847ih1rlvcv404r1") (f (quote (("disable-make-check") ("default" "disable-make-check"))))))

(define-public crate-arb-sys-0.3.6 (c (n "arb-sys") (v "0.3.6") (d (list (d (n "derivative") (r "^2.0") (d #t) (k 0)) (d (n "flint-sys") (r "^0.7") (f (quote ("disable-make-check"))) (d #t) (k 0)) (d (n "gmp-mpfr-sys") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1nwhv1ma5qyx35wh7nr50qm76p3wplq7zc8357ibsjr9rpdr87p0") (f (quote (("disable-make-check") ("default" "disable-make-check"))))))

