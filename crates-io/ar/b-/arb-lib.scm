(define-module (crates-io ar b- arb-lib) #:use-module (crates-io))

(define-public crate-arb-lib-0.1.0 (c (n "arb-lib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "deepl") (r "^0.1") (d #t) (k 0) (p "deepl-pro")) (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.8") (d #t) (k 0)))) (h "135qybaqbrg57dfpmv53qcb2mi16i0yl11swxv1ibak2kmvqrhn7")))

