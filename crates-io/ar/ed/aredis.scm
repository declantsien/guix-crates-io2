(define-module (crates-io ar ed aredis) #:use-module (crates-io))

(define-public crate-aredis-0.0.1 (c (n "aredis") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "atoi") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k90zah8q7n1hfxn2wds9q331gfx6gcpdawp9gfgwj6ryqb8kf7s")))

(define-public crate-aredis-0.0.2 (c (n "aredis") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "atoi") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10z7485gmlxy4kk2karaw5iy1zwscmmvkc1yq1dm09n2nharx132")))

(define-public crate-aredis-0.0.3 (c (n "aredis") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "atoi") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02jgs59rjndis9m2sfz35n996rahcbp18dybca039jy1lqc4pgzy")))

(define-public crate-aredis-0.0.4 (c (n "aredis") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "atoi") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yrhdvjgj9p76063v07sn47hcb8yrl3rqkinnjnhqjb6kzmhjv1p")))

