(define-module (crates-io ar en arena-rs) #:use-module (crates-io))

(define-public crate-arena-rs-0.1.0 (c (n "arena-rs") (v "0.1.0") (h "0nlfq70w6q5fckzgwwjjynhdhfnrkpaihm0kl1j9yidcjk9564y0") (y #t)))

(define-public crate-arena-rs-0.1.1 (c (n "arena-rs") (v "0.1.1") (h "0cnss28bk0h96pyxsvb1swv1jq7rfd4m13i7678ch7if48a082im") (y #t)))

