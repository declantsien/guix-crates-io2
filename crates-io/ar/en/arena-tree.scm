(define-module (crates-io ar en arena-tree) #:use-module (crates-io))

(define-public crate-arena-tree-0.1.0 (c (n "arena-tree") (v "0.1.0") (d (list (d (n "typed-arena") (r "^1.0") (d #t) (k 0)))) (h "0zm9yj9lqvmrab07lc3bbbqhhsqh5lm2kqzdxh1dzh482nz5d58h") (y #t)))

(define-public crate-arena-tree-0.2.0 (c (n "arena-tree") (v "0.2.0") (d (list (d (n "typed-arena") (r "^1.0") (d #t) (k 2)))) (h "0lya50526yp3k6qkshzscl50q66g68h69j6j61na6f4sg82pk4lk") (y #t)))

(define-public crate-arena-tree-0.3.0 (c (n "arena-tree") (v "0.3.0") (d (list (d (n "typed-arena") (r "^1.0") (d #t) (k 2)))) (h "0q5dyp26bkg6v9pxs0im4zwfknzwzvhpwv5gby3n4a1xpgx22nxf") (y #t)))

