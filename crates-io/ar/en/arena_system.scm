(define-module (crates-io ar en arena_system) #:use-module (crates-io))

(define-public crate-arena_system-0.0.1 (c (n "arena_system") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "0bca44cbk2fxamxixcsn0g54wkvh5v8jxjbm7gnldr2ipvfpbprv")))

(define-public crate-arena_system-0.0.2 (c (n "arena_system") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "1z7b2zhiy5gvvasx82rv2dm1mj70la98bpm7l0s6kr9q38r6axkz")))

(define-public crate-arena_system-0.0.3 (c (n "arena_system") (v "0.0.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "02s14m57abb90110xwraxbslam2qms8v34p5mlyi42c3s1yil48j")))

(define-public crate-arena_system-0.0.4 (c (n "arena_system") (v "0.0.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "195cvnkdybg0wdvjqx13mx5nm0mbc5m508f7df9bqbspjfb2f0vw")))

(define-public crate-arena_system-0.0.5 (c (n "arena_system") (v "0.0.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "03a2bwzgl2zs7zd9d8dd44p8jpxrss1cbf2bnns02q9gbj3a0iv9")))

(define-public crate-arena_system-0.0.6 (c (n "arena_system") (v "0.0.6") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "1jhd35yar9gywlsdrqf20pbba422qciqg9yjsc53zlwjczgf7w8c")))

(define-public crate-arena_system-0.0.7 (c (n "arena_system") (v "0.0.7") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1") (d #t) (k 0)))) (h "00glywi01pqcxli1dir8qd00mk3z6f9afl7041v3im39gxar9q6l")))

(define-public crate-arena_system-0.0.8 (c (n "arena_system") (v "0.0.8") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1.1") (d #t) (k 0)))) (h "1n8k1a43f71np4whkj9cnn10ddc3hcvk6nl1rl337a795hg8wjb7")))

(define-public crate-arena_system-0.0.9 (c (n "arena_system") (v "0.0.9") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1.1") (d #t) (k 0)))) (h "146kcmvqi3wxlc74w8v1bhm3iry6dswdclvlvjd198lhk3cs30ic")))

(define-public crate-arena_system-0.0.10 (c (n "arena_system") (v "0.0.10") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1.1") (d #t) (k 0)))) (h "0snavh6zbar5frxmbb00jqxka9ca39zl8flvajkkmp47x2379ail")))

(define-public crate-arena_system-0.0.11 (c (n "arena_system") (v "0.0.11") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1.1") (d #t) (k 0)))) (h "1xh5vz4r4wk4j8j7px05il37yy51dpsljhkjg56959xqdw3c67wi")))

(define-public crate-arena_system-0.0.20 (c (n "arena_system") (v "0.0.20") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_cell") (r "^0.1.2") (d #t) (k 0)))) (h "1i6rgjbgv1wqg98b37nmwc9j7yhi8bxixcxbqqm7x1ww6lhmnbi0")))

