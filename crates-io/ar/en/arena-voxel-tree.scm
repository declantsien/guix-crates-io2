(define-module (crates-io ar en arena-voxel-tree) #:use-module (crates-io))

(define-public crate-arena-voxel-tree-0.0.1 (c (n "arena-voxel-tree") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "010jr0zhmfb7bd1551hs3ldz8r2w90j8rjwjndr7wz2ywi4g6iw7")))

