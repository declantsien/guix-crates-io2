(define-module (crates-io ar en arenta) #:use-module (crates-io))

(define-public crate-arenta-1.0.0 (c (n "arenta") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (f (quote ("date"))) (d #t) (k 0)))) (h "1drryri4yfnqnivqsyydxqk9qf3sxfzgxdcsc12pvm1abnmfixi3")))

(define-public crate-arenta-1.0.1 (c (n "arenta") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (f (quote ("date"))) (d #t) (k 0)))) (h "0680yhwrj1hgf2ihvms88k2jcbxp8s8ajiww9p6lh3c8va272jmn")))

(define-public crate-arenta-1.0.2 (c (n "arenta") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (f (quote ("date"))) (d #t) (k 0)))) (h "100cshr0nis7zc065y25m9f9z1qmh0xv6xr09gv94kfw4xrx6ks7")))

