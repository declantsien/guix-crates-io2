(define-module (crates-io ar en arenavec) #:use-module (crates-io))

(define-public crate-arenavec-0.1.0 (c (n "arenavec") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (t "cfg(unix)") (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("basetsd" "memoryapi" "minwindef" "sysinfoapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nvpz6sap85lvhpcy762cxiglcykfjwmxlqnxn9rl4553laqs29b")))

(define-public crate-arenavec-0.1.1 (c (n "arenavec") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.44") (d #t) (t "cfg(unix)") (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("basetsd" "memoryapi" "minwindef" "sysinfoapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cvnf4ddhinpg8nsphzzn45qk3j9akdd1jk1aqzcanvi2nlnfgka")))

