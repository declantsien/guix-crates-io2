(define-module (crates-io ar en arenas) #:use-module (crates-io))

(define-public crate-arenas-0.1.0 (c (n "arenas") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19zzz9m3dirxwy1v4fgyxl4fjp3kvawp7lw8jgk0z6kh9nbymlfw")))

(define-public crate-arenas-0.1.1 (c (n "arenas") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xvbwzk6d6cyaxypnlq1sp9rlacq58scpl1yazqx53mk1c026ylf")))

(define-public crate-arenas-0.2.0 (c (n "arenas") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0siyx8710pg0f3483452swsz3jpxr2dlcygqpz1ra6gr1p9d6ngw")))

