(define-module (crates-io ar en arenatree) #:use-module (crates-io))

(define-public crate-arenatree-0.1.0 (c (n "arenatree") (v "0.1.0") (h "08z4r9g1pf7r7czhj9s14y1c1ndz5k448nkwz0mdyqlvrqcxy8nh")))

(define-public crate-arenatree-0.1.1 (c (n "arenatree") (v "0.1.1") (h "1g0iix3pi62ziry3yd0zrvlb3hyzshjjnp1sbnl4ykfx0hhmx9q4")))

(define-public crate-arenatree-0.1.2 (c (n "arenatree") (v "0.1.2") (h "11ddl0fxc6gyi74xjdvblrp76dlw56inihjdkajgh5827hfycmpk") (y #t)))

(define-public crate-arenatree-0.1.3 (c (n "arenatree") (v "0.1.3") (h "1akpina7ma43x2h895vnbgg3qrh1cvscrjagd0570x5xdyqhrhhj")))

