(define-module (crates-io ar gi argio-macro) #:use-module (crates-io))

(define-public crate-argio-macro-0.2.0 (c (n "argio-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "17pjng0wbd2jsg1pallyrwpcai2y7avvpcwqpj9p70q62sq9vzh2")))

