(define-module (crates-io ar gi argi) #:use-module (crates-io))

(define-public crate-argi-0.1.0-beta.1 (c (n "argi") (v "0.1.0-beta.1") (h "0srr54iga2z19dx190aja4zzsybjh99va06hn3jzws4gwxm47npv") (y #t)))

(define-public crate-argi-0.1.0-beta.2 (c (n "argi") (v "0.1.0-beta.2") (h "0dlhnw54y65ccsczwg9l87a1j12p84snh2hqq1bs2wcizg4rir2r") (y #t)))

(define-public crate-argi-0.1.0-beta.3 (c (n "argi") (v "0.1.0-beta.3") (h "163n89395s5avrhcjkblfnx64g76vv825fxs5k14fyb0ckx9225i") (y #t)))

(define-public crate-argi-0.1.0-beta.4 (c (n "argi") (v "0.1.0-beta.4") (h "14682fjsa1cxaxrkbfjbv1jbwnrj4hvnws30r6x42m3yc1nyhgq8") (y #t)))

(define-public crate-argi-0.1.0-beta.5 (c (n "argi") (v "0.1.0-beta.5") (h "0i02gw9dvycw8az8xb0vxrbs6mfwidaqamqg4lkh9c790sf1lvjr")))

