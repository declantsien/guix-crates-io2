(define-module (crates-io ar gi argio) #:use-module (crates-io))

(define-public crate-argio-0.1.0 (c (n "argio") (v "0.1.0") (d (list (d (n "proconio") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0jgrxs4xsxxb9znsdy0m14lkpm3dmq6l8sk09x98bsdg860yks7n") (y #t)))

(define-public crate-argio-0.1.1 (c (n "argio") (v "0.1.1") (d (list (d (n "proconio") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0vva4g3y1s8n7g9v6xk82vcb40gb4dj7yz14x68p2c4yin0hf2l3")))

(define-public crate-argio-0.1.2 (c (n "argio") (v "0.1.2") (d (list (d (n "proconio") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "093w93162hd47ll59iwzwmdlnff9ap0vfqlhb6yicg9q5i190qfi")))

(define-public crate-argio-0.1.4 (c (n "argio") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "proconio") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "1lr4blhmqhhnynq7c3ivxpcgd53ypyfj60yz311n4sbj91x0vfvj")))

(define-public crate-argio-0.2.0 (c (n "argio") (v "0.2.0") (d (list (d (n "argio-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "proconio") (r "^0.4.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "14v3h4bfc138f48hclcshkd735pdl25mjdyp3yx4p6zhic0wbkzd")))

