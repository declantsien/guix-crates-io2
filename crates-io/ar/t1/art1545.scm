(define-module (crates-io ar t1 art1545) #:use-module (crates-io))

(define-public crate-art1545-0.1.0 (c (n "art1545") (v "0.1.0") (h "0sj7j0xcpz09lna6cx6w91jhfsd0c1mh6x08iikkqkxzzv5r7lmn") (y #t)))

(define-public crate-art1545-0.1.1 (c (n "art1545") (v "0.1.1") (h "0hc42d5qa8mmbhb2fg85qyxmfz009p6krrdc1gzryfb5q3r9gbis") (y #t)))

