(define-module (crates-io ar cc arcconfig) #:use-module (crates-io))

(define-public crate-arcconfig-0.2.0 (c (n "arcconfig") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1nx70i89s7lj3zcrjn93pj50gx1ws0c0pcv6hdn4dqjcsjvy15yj")))

(define-public crate-arcconfig-0.2.1 (c (n "arcconfig") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0619l3762kwplc3dmk9swz44yk0wav697gpfbisnv2kbhz30gsj6")))

(define-public crate-arcconfig-0.3.0 (c (n "arcconfig") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "111mfpzniwhgvzqqq1f39zd8mbhbqp6bzi556dymszgmqhwsirjb")))

(define-public crate-arcconfig-0.3.1 (c (n "arcconfig") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1899fzajc3llyn2q36rwz424kb5rhhpxz5np3lv6nvjg2dgwq4bx")))

