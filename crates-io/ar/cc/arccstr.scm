(define-module (crates-io ar cc arccstr) #:use-module (crates-io))

(define-public crate-arccstr-0.1.0 (c (n "arccstr") (v "0.1.0") (h "1abiddvzcgaslf1f2p8r1v7kjdya0wyc3crysi527y3lmzkliwk1")))

(define-public crate-arccstr-0.1.1 (c (n "arccstr") (v "0.1.1") (h "0nfmyz7vy0m5d3bz32jhszwlssi3n0f6kkj6khgsklkl1qzp4i35")))

(define-public crate-arccstr-0.2.0 (c (n "arccstr") (v "0.2.0") (d (list (d (n "serde") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9.10") (d #t) (k 2)))) (h "0caxn56mbn7fxfj8vs8av886w980vigg8jhi59vbww4pagyxl7nq") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3.0 (c (n "arccstr") (v "0.3.0") (d (list (d (n "serde") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9.10") (d #t) (k 2)))) (h "0hqxg1g1k8njnv253h1g4nlxlb8y2f3957d9srjc3zqjqp4dpmfp") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3.1 (c (n "arccstr") (v "0.3.1") (d (list (d (n "serde") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9.10") (d #t) (k 2)))) (h "04lzfhygiigb249nqcirmjbhgva4f3838rfjh6y7739brbbbj2h6") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3.2 (c (n "arccstr") (v "0.3.2") (d (list (d (n "serde") (r "^0.9.10") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9.10") (d #t) (k 2)))) (h "0z44n4hlph7apgqsmcfh6rib84rxk6kihlddiaywf20l1jhc2qvw") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4.0 (c (n "arccstr") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0kmp17hrljrwp3ga0z2wqpirnz4raypd225ypbyd9jm03fwxmhrg") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4.1 (c (n "arccstr") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0lb4w7kd801ip10ypwxrbbzl31j0cdw092k48if8m31bv4dd5ha2") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4.2 (c (n "arccstr") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05wqh371mm8c9bmgf083j7wd5zrpbrwirx0f3xdrqz7g7vsrfzps") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.0.0 (c (n "arccstr") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0xw181gnckdz881b4z1vr7r6wmm8mb8fff3yxfl31xaa776znkv6") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.0.1 (c (n "arccstr") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1840xicmwcm76p0h5i602r9mllrjcim7qkwp5m07kwrdv9h1mqx0") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.0.2 (c (n "arccstr") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0454h0zyh0fn3k32i3zwram3s8jqxv6ag51bf5a3r5m3bzjd097s") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.0.3 (c (n "arccstr") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "190pxh054gyvc164l8q3jr9iihylic4j52pqn76y3graiqrddf3i") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.0.4 (c (n "arccstr") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "17lpd7l876nkfgrckwc4ngymwvjnm941flp3xlm622qpv00y7xfh") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.1.0 (c (n "arccstr") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "15np53xvar8s7ipxpsi2nfq9x1qihh8azvb16wvbsksp0c13wkdl") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.2.0 (c (n "arccstr") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1x35ya05ysdklbqfi4fm8vwzxlykw4qxdm6ykkixb3jlmzfjwylm") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.2.1 (c (n "arccstr") (v "1.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mmcf85gnaxlcfwgj77hax80b893w4n2n5zcw8gfp00dy3y5rikp") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.2.2 (c (n "arccstr") (v "1.2.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0v2plz52nwm71jvd328kilqnwdgv57wmdp9ybx0dxw6xx99l269s") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.2.3 (c (n "arccstr") (v "1.2.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "12ldr6i5qwyx2d5kr0b2kw00l7rcfp2kiwsfcl0rl6j0sfi4qpbz") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.3.0 (c (n "arccstr") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0qbfgc6r385ipyrn0rkl8gdmsvfjz75pkl7f3nr1s0xab7b1siw2") (f (quote (("default" "serde"))))))

(define-public crate-arccstr-1.3.1 (c (n "arccstr") (v "1.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "02c42x7mdymigy8bgz4fr4mx2f30m5nc0qaf8jq0xbv913admh6n") (f (quote (("default" "serde"))))))

