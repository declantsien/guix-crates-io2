(define-module (crates-io ar r_ arr_ty) #:use-module (crates-io))

(define-public crate-arr_ty-0.1.0 (c (n "arr_ty") (v "0.1.0") (h "1iq3q64lsaj4saiwiyfyf7z2dmilgrlqk6lsy2iiqmzq34r0fkwp")))

(define-public crate-arr_ty-0.2.0 (c (n "arr_ty") (v "0.2.0") (h "1sn1zm57cm4iajrdbcs0pr2rllp59s8jnbhzzbvxqpah6qr5w44h")))

(define-public crate-arr_ty-0.2.1 (c (n "arr_ty") (v "0.2.1") (h "0403qipzdbj3q5n0g5jgmg1yqswmn83z9h9arr2by63dwwf5pm5r")))

(define-public crate-arr_ty-0.2.2 (c (n "arr_ty") (v "0.2.2") (h "0rd2dqphqb9ga1nirmfhahjp696zzmshanb10bnnc7ycqqnxzg2s")))

