(define-module (crates-io ar r_ arr_macro) #:use-module (crates-io))

(define-public crate-arr_macro-0.0.1 (c (n "arr_macro") (v "0.0.1") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1qv4kp3hvx3syn0iwdxlbhlwwgg26pkvbgipr1ycx3hmwf4lcc4x")))

(define-public crate-arr_macro-0.1.0 (c (n "arr_macro") (v "0.1.0") (d (list (d (n "arr_macro_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0m298c6g2s4756jm2adpg085c53af71ghfir90y61kgnb047q8wa")))

(define-public crate-arr_macro-0.1.1 (c (n "arr_macro") (v "0.1.1") (d (list (d (n "arr_macro_impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1x90cppfwaczsv8kgrkh9my7j4l83zdr3369506lg91pg6zwkf1c")))

(define-public crate-arr_macro-0.1.2 (c (n "arr_macro") (v "0.1.2") (d (list (d (n "arr_macro_impl") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "15rsvpwdqgchh6ycwpvfv22xy0s38k6n9ryn99aj2cap5wzvhqnj")))

(define-public crate-arr_macro-0.1.3 (c (n "arr_macro") (v "0.1.3") (d (list (d (n "arr_macro_impl") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "199086q8zva66lbg9bpz6fa67s81ra7yfa8148cwy1w7lkymn43a")))

(define-public crate-arr_macro-0.2.0 (c (n "arr_macro") (v "0.2.0") (d (list (d (n "arr_macro_impl") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)))) (h "0zp2fad99xrbiz84pqk3k8n60wqvdai03i0yar7qfk9rhsqpsfny")))

(define-public crate-arr_macro-0.2.1 (c (n "arr_macro") (v "0.2.1") (d (list (d (n "arr_macro_impl") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1") (d #t) (k 0)))) (h "14p9mx9z5562nhfgz2kyh8q5s6lw9p1rjbvsl6nfhapscbh3d4y4")))

