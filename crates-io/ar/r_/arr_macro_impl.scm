(define-module (crates-io ar r_ arr_macro_impl) #:use-module (crates-io))

(define-public crate-arr_macro_impl-0.1.0 (c (n "arr_macro_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0r80k7djzky7x6xzn8l96il1apla8l1zxyssvl7bbv7dvf5v01pl")))

(define-public crate-arr_macro_impl-0.1.1 (c (n "arr_macro_impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0y0ff1bgxb18qlf16py11d5v8nbfhsy5dcbz6gfmdqww52zs9dgd")))

(define-public crate-arr_macro_impl-0.1.2 (c (n "arr_macro_impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1z5axgkv2r7ikp0882gdqqb1dswwhb8fb4cd4929x4zczybvxv4d")))

(define-public crate-arr_macro_impl-0.1.3 (c (n "arr_macro_impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lbjilz3pvwav72dfkcbz99rsq7m04xbdpqh8g3yvx3jsn5wf286")))

(define-public crate-arr_macro_impl-0.2.0 (c (n "arr_macro_impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wmrfpcgzwaw383j18a2gk260b67hv7fdm3g95mj27n2faiwnzzf")))

(define-public crate-arr_macro_impl-0.2.1 (c (n "arr_macro_impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pkfgwbyd5ylj069bdjmaalb0dwl1jp2f0wir81w8vjwmvwnhqww")))

