(define-module (crates-io ar kn arknights) #:use-module (crates-io))

(define-public crate-Arknights-0.0.0 (c (n "Arknights") (v "0.0.0") (d (list (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "1gr5qgwfxvah68ivlnj3k3lr07l9wa9c3ymxs91w79r96zar739z") (f (quote (("default"))))))

(define-public crate-Arknights-0.1.0 (c (n "Arknights") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "01wlfqy6v2rx20d8j1bh4shnc1wal2jkmfa9mq1sjj2rl8kax1kj") (f (quote (("default"))))))

(define-public crate-Arknights-0.1.1 (c (n "Arknights") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "0r70032j7hpjb7biwf0cdkknrbim23p51ymfnkkrn48myw6rwg1j") (f (quote (("default"))))))

(define-public crate-Arknights-0.1.2 (c (n "Arknights") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)))) (h "1kljliilg32lanp0lm4ck19545idlkaqslndrabcrn27w88mjgfx") (f (quote (("default"))))))

