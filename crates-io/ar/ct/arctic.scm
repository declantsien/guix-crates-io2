(define-module (crates-io ar ct arctic) #:use-module (crates-io))

(define-public crate-arctic-0.0.1 (c (n "arctic") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1zxxhjbi10g0bhy92k380vzspzw467sfhrrs58kyd6c51zwzjdxb")))

(define-public crate-arctic-1.0.0 (c (n "arctic") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1j4gk4di2m77g5csdq95grgwj91hcs1gyc7rih6skd02s5cilf3k")))

