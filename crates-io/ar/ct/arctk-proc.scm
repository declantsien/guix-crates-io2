(define-module (crates-io ar ct arctk-proc) #:use-module (crates-io))

(define-public crate-arctk-proc-0.1.0 (c (n "arctk-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1j4s9inrcvxqnfhi9hc1xz0qw59cwp9q86fjy38f6xyzycv6g7l1")))

(define-public crate-arctk-proc-0.1.1 (c (n "arctk-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0p4jlddrdzxxh0971ipph45xlnj59myygwbrfv6lhyz6b5h002lq")))

(define-public crate-arctk-proc-0.1.2 (c (n "arctk-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9lwd5458x3fc0dpkmk33aj9lqswfhc0c9zjxysb32k6b4s2y74")))

(define-public crate-arctk-proc-0.1.3 (c (n "arctk-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "18qf9p09zgbapwhgns55r0ldqy8c66qwibd9951n8ka84yva08bs")))

(define-public crate-arctk-proc-0.1.4 (c (n "arctk-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "04n8mgv8yvf9znfgh69b4qa1adxh1vkba0nwg6zfas9bxvaslvqa")))

(define-public crate-arctk-proc-0.1.5 (c (n "arctk-proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0rchi6rw6gbiasxy7h3cgz9h9v3a4vcc9qc8i006ha806gdkyaqk")))

(define-public crate-arctk-proc-0.1.6 (c (n "arctk-proc") (v "0.1.6") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1ky5wpbrvwd71vdd197z5ag2q16nqxyzk864ivn16m9kaz4alc9l")))

(define-public crate-arctk-proc-0.1.7 (c (n "arctk-proc") (v "0.1.7") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1p69yj9l819hvlgj9qbn42v6c93dp5gkkxgca7iwyy5mw64lw7x3")))

(define-public crate-arctk-proc-0.2.0 (c (n "arctk-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "157xciwyrgw80yxg9gbvf3wigh8g41vd3r18d72b2nxaljzjs3hw")))

(define-public crate-arctk-proc-0.3.0 (c (n "arctk-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0agb2sg37f0a7y8ycrbaazji70d3n004gfbprw0l3r169k9ssz7x")))

(define-public crate-arctk-proc-0.3.1 (c (n "arctk-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "136cdlisznk8akmw3hm2s51zav9d17gdi2nvv2gnwx042riqqdsy")))

(define-public crate-arctk-proc-0.3.2 (c (n "arctk-proc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzkr3d3kb03iyvj9z20lh1zm7vp9gq8bz55mzz4mn5hy9v3m2kp")))

(define-public crate-arctk-proc-0.3.3 (c (n "arctk-proc") (v "0.3.3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1acgm9qin4qyb1cg3pgx094q4mv36ap984b42vw7g2h0gmvh0p1k")))

(define-public crate-arctk-proc-0.4.0 (c (n "arctk-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "067j5l5i7r029dhdlzr1alwx5s70mh24kq3sqgqwn9nn423irzk1")))

(define-public crate-arctk-proc-0.4.1 (c (n "arctk-proc") (v "0.4.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1m1xf4a0k4858is86i7hzd4k07cg08hqk8736w34w96648msd5yp")))

(define-public crate-arctk-proc-0.4.2 (c (n "arctk-proc") (v "0.4.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0fzqxalsplrgzlbxbs2ywa6bm43rfh6066samlfmzv3w7yllqjy6")))

(define-public crate-arctk-proc-0.4.3 (c (n "arctk-proc") (v "0.4.3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1pn369fbii20lyr18cl9w915r15zlpwlgr0zp1aiw8ckgwvmq839")))

