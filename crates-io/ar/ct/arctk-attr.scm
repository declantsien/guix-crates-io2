(define-module (crates-io ar ct arctk-attr) #:use-module (crates-io))

(define-public crate-arctk-attr-0.1.0 (c (n "arctk-attr") (v "0.1.0") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1ki1x9cg4wfc21jdfmbf97bp9n1g9h2cisdxrpb0rdhp8c10hgfb")))

(define-public crate-arctk-attr-0.1.1 (c (n "arctk-attr") (v "0.1.1") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "19vy146rr89995bxmf2mnx8k0zzhqakwk0xlzziz6nbkwwnpsi2s")))

(define-public crate-arctk-attr-0.1.2 (c (n "arctk-attr") (v "0.1.2") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "12gbi6qpw8a730nfzd9ybp345fsvhm8va1hgqjjd0ziwk9f73929")))

(define-public crate-arctk-attr-0.1.3 (c (n "arctk-attr") (v "0.1.3") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxj3wi3d8f21k63dizp797avbb1dz4afwr4ymh7xjf6mya12r89")))

(define-public crate-arctk-attr-0.1.4 (c (n "arctk-attr") (v "0.1.4") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0wx566pqh263vqx1ml42yiv6vyjaq1v2kzz1d7zcxhjhf9nigvqn")))

(define-public crate-arctk-attr-0.1.5 (c (n "arctk-attr") (v "0.1.5") (d (list (d (n "arctk-proc") (r "0.1.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "07gqh4khw76gizy82ry38swziv4z5nmnzfg1kpkjydbydk329lka")))

(define-public crate-arctk-attr-0.2.0 (c (n "arctk-attr") (v "0.2.0") (d (list (d (n "arctk-proc") (r "0.2.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0xcb6xdp6mw16fpi57nvpndnz6yim2ndzisl6zkl1vvzm1zq16y6")))

(define-public crate-arctk-attr-0.3.0 (c (n "arctk-attr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1hi09p92rsnf6zlplwwhk4vmy0a1qqyddp5y771m6rs2001dylal")))

(define-public crate-arctk-attr-0.3.1 (c (n "arctk-attr") (v "0.3.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "08hnyivd1ak1plq6bl73w0yhqykvkac8lrawnm7h1jx6y2argwb4")))

