(define-module (crates-io ar ct arctis7-controls) #:use-module (crates-io))

(define-public crate-arctis7-controls-0.1.0 (c (n "arctis7-controls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fbg1n6mqsl0978xn330w271dar0ydn4yffb1wryyyz94am7dri1")))

(define-public crate-arctis7-controls-0.1.1 (c (n "arctis7-controls") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "0fbmxj8gnc9r6i1dy6hawg0ak68wzbzcp0l83xi16gkq3nc6gkbf")))

