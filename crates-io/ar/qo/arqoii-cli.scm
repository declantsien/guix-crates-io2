(define-module (crates-io ar qo arqoii-cli) #:use-module (crates-io))

(define-public crate-arqoii-cli-0.3.0 (c (n "arqoii-cli") (v "0.3.0") (d (list (d (n "arqoii") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eframe") (r "^0.26.2") (d #t) (k 0)) (d (n "png") (r "^0.17.12") (d #t) (k 0)))) (h "11h77cgp613lfyiadz72knmky2ygsb8y2ljxv8wb9n79xhfzy0rf")))

