(define-module (crates-io ar qo arqoii-types) #:use-module (crates-io))

(define-public crate-arqoii-types-0.1.0 (c (n "arqoii-types") (v "0.1.0") (h "038jhjbdy6a9jj6pb1s2bkw3b5qzmyza9xqyrb976jqzvz1i1b69")))

(define-public crate-arqoii-types-0.2.0 (c (n "arqoii-types") (v "0.2.0") (h "1yn8vm758a4kkpgr6ahx9fl5iaiqy9c9ngh9fl44zr6i8mgs29n7")))

(define-public crate-arqoii-types-0.3.0 (c (n "arqoii-types") (v "0.3.0") (h "0z626ica9cyxkmbqblh77rdavr2kahkgizkylq57a38q0zb3mmpa")))

