(define-module (crates-io ar qo arqoii) #:use-module (crates-io))

(define-public crate-arqoii-0.1.0 (c (n "arqoii") (v "0.1.0") (d (list (d (n "arqoii-types") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "0mh84m9zwam5p3fw1br6az89ai3l3qi2n6sq3isfddkk9ndc8jl9")))

(define-public crate-arqoii-0.2.0 (c (n "arqoii") (v "0.2.0") (d (list (d (n "arqoii-types") (r "^0.2.0") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 2)))) (h "15m8yrjw5l3fv1ssm8pknqm8s6vkarp55pg6m6p3j9yx5c1jiiy0")))

(define-public crate-arqoii-0.3.0 (c (n "arqoii") (v "0.3.0") (d (list (d (n "arqoii-types") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.17.12") (d #t) (k 2)))) (h "1401i00w1d6kpfy9v7drsbm3jibs3dk3mj354k23q3vpg164i3ax")))

