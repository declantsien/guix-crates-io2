(define-module (crates-io ar rp arrpc-core) #:use-module (crates-io))

(define-public crate-arrpc-core-0.1.0 (c (n "arrpc-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)))) (h "1pgxdvdgvmy7az1g0p8k6w5gp9rxwnja29pd2bdr3cbz4pdkaj88")))

(define-public crate-arrpc-core-0.2.0 (c (n "arrpc-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)))) (h "10bwgb37xsrnlc3qlk2bcgvfw796wcwavx79vplikq2xfxlyim2l")))

