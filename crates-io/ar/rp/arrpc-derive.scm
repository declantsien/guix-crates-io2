(define-module (crates-io ar rp arrpc-derive) #:use-module (crates-io))

(define-public crate-arrpc-derive-0.1.0 (c (n "arrpc-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1j1m748mz2gyyw8wv3jrr5ws6nx5vf1rxmabxdljyjmn198vyy8k")))

(define-public crate-arrpc-derive-0.2.1 (c (n "arrpc-derive") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "017z9bmx7shp5k5srzxi6cyawwc42c12j098y797xsag3a8x537v") (f (quote (("default")))) (s 2) (e (quote (("obake" "dep:semver"))))))

