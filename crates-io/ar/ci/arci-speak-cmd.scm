(define-module (crates-io ar ci arci-speak-cmd) #:use-module (crates-io))

(define-public crate-arci-speak-cmd-0.1.0 (c (n "arci-speak-cmd") (v "0.1.0") (d (list (d (n "arci") (r "^0.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zgdy5b4zdsjj4b0p6lhbazmwgizlvkq5wpcpdl7qxi8rkl84cq0") (y #t)))

(define-public crate-arci-speak-cmd-0.0.3 (c (n "arci-speak-cmd") (v "0.0.3") (d (list (d (n "arci") (r "^0.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sgvjm177i675r2bax5zjzh93n8g0mw5wbk3v5qs86nwg1gpghwh")))

(define-public crate-arci-speak-cmd-0.0.4 (c (n "arci-speak-cmd") (v "0.0.4") (d (list (d (n "arci") (r "^0.0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1df2sp7r0pq9wp6jawir2ydrwpiypdcmr5b95f8lli1jqndl9zr2")))

(define-public crate-arci-speak-cmd-0.0.5 (c (n "arci-speak-cmd") (v "0.0.5") (d (list (d (n "arci") (r "^0.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "03p1xa8ix045bdqncip4744fal17f6klfnhgf0fq220jvyffaxdn")))

(define-public crate-arci-speak-cmd-0.0.6 (c (n "arci-speak-cmd") (v "0.0.6") (d (list (d (n "arci") (r "^0.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0np01ajkiq00wdsdcwb3qn5h0pig9z1a9b5kygfijrkml4s9v1qr")))

(define-public crate-arci-speak-cmd-0.0.7 (c (n "arci-speak-cmd") (v "0.0.7") (d (list (d (n "arci") (r "^0.0.7") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "19bk0spm2l2nvq2yzcd7ddpkk9b8in0qd23qqi5yg3ph7a7iw92n")))

(define-public crate-arci-speak-cmd-0.1.1 (c (n "arci-speak-cmd") (v "0.1.1") (d (list (d (n "arci") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter"))) (k 2)))) (h "1dlrlz489vqmhl16pz3dvr4znakyr6x9693wymyi9pphbwlr3si6")))

