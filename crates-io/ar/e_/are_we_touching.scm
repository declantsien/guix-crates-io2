(define-module (crates-io ar e_ are_we_touching) #:use-module (crates-io))

(define-public crate-are_we_touching-0.1.0 (c (n "are_we_touching") (v "0.1.0") (h "0bjw28115jyyddmhazaw7vi0mp18h3k5iskfsm1k75f087cx65g4")))

(define-public crate-are_we_touching-0.1.1 (c (n "are_we_touching") (v "0.1.1") (d (list (d (n "cubi_vectors") (r "^0.1.5") (d #t) (k 0)))) (h "0hpvrfc5bp5azh8amc0hsr50qkn5az3w79q57n93brn74g1l88pc")))

(define-public crate-are_we_touching-0.2.0 (c (n "are_we_touching") (v "0.2.0") (d (list (d (n "cubi_vectors") (r "^0.1.8") (d #t) (k 0)))) (h "17qab3iz4qh0sxf39w4blv7xzsvh4hv9j0pkll163z9iwr9x3fbj")))

(define-public crate-are_we_touching-0.2.1 (c (n "are_we_touching") (v "0.2.1") (d (list (d (n "cubi_vectors") (r "^0.1.8") (d #t) (k 0)))) (h "1hajpgkp4i5s0z0g20wp0r46hwzzipa60hyckp4hxy96igbshq91")))

(define-public crate-are_we_touching-0.2.2 (c (n "are_we_touching") (v "0.2.2") (d (list (d (n "cubi_vectors") (r "^0.1.8") (d #t) (k 0)))) (h "1jr4kc3mg5ijl40j3yjzjkjf7a4m4zd7w6q6myv42lsj4w0f1m15")))

(define-public crate-are_we_touching-0.2.3 (c (n "are_we_touching") (v "0.2.3") (d (list (d (n "cubi_vectors") (r "^0.1.8") (d #t) (k 0)))) (h "1mpj8vzknl8hn3zai14qkl1kjx204g2lbwqjx7jn3lrfrvwy1jlj")))

(define-public crate-are_we_touching-0.2.4 (c (n "are_we_touching") (v "0.2.4") (d (list (d (n "cubi_vectors") (r "^0.1.8") (d #t) (k 0)))) (h "0rjiafjp8r6brrdaydxi9fms1xnb1agscsi03d9ifzsp9g2vi7s3")))

(define-public crate-are_we_touching-0.2.5 (c (n "are_we_touching") (v "0.2.5") (d (list (d (n "cubi_vectors") (r "^0.2.0") (d #t) (k 0)))) (h "1vi5q8rh5fp4z015km9mbpcg406sib7khcicnx2pxbmn0pahccs8")))

(define-public crate-are_we_touching-0.2.6 (c (n "are_we_touching") (v "0.2.6") (d (list (d (n "cubi_vectors") (r "^0.2.0") (d #t) (k 0)))) (h "1j9i9b7qhkaxvc4lvdx0wvz6ya9pnggj9zjpk7n7drhcf7fpcp4a")))

(define-public crate-are_we_touching-0.2.7 (c (n "are_we_touching") (v "0.2.7") (d (list (d (n "cubi_vectors") (r "^0.2.0") (d #t) (k 0)))) (h "01nm7ry3a6nsningk7lgpff35llv2xmpmkglqww402193wc120db")))

(define-public crate-are_we_touching-0.2.8 (c (n "are_we_touching") (v "0.2.8") (d (list (d (n "cubi_vectors") (r "^0.2.1") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)))) (h "1j13bsazjy6s3aww8r1cd9x7p3p4va686sn1qrvd16s4dpkw5lnd")))

