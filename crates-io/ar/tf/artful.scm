(define-module (crates-io ar tf artful) #:use-module (crates-io))

(define-public crate-artful-0.1.0 (c (n "artful") (v "0.1.0") (h "1cbsx6d707zcnimyfr3lxh0a6hg1ni6ybf3s0z9ra6xswnk7yv3h") (f (quote (("simd"))))))

(define-public crate-artful-0.1.1 (c (n "artful") (v "0.1.1") (h "0378vaxcn6cgibw1rr6n2nqsixxdmi0lk6xx3x04fsj1678fvf7k") (f (quote (("simd"))))))

