(define-module (crates-io ar xi arxiv) #:use-module (crates-io))

(define-public crate-arxiv-0.1.0 (c (n "arxiv") (v "0.1.0") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros" "parsing"))) (d #t) (k 0)))) (h "07ik0aqph0lan7rnglkw9gx1mgn35x9cvphm909a9g1fm4ywc61z") (r "1.63.0")))

(define-public crate-arxiv-0.2.0 (c (n "arxiv") (v "0.2.0") (d (list (d (n "time") (r "^0.3.20") (f (quote ("macros" "parsing"))) (d #t) (k 0)))) (h "043120ywnlvknmgcydzhr7dc308bny6sw40sysfkmihcbyk3pgrn") (r "1.63.0")))

