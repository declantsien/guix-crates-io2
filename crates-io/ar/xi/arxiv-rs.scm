(define-module (crates-io ar xi arxiv-rs) #:use-module (crates-io))

(define-public crate-arxiv-rs-0.1.0 (c (n "arxiv-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "03v8kglbqs5af44461w01jsjxllv3g9fhn017kabnxgs0xpnf50z")))

(define-public crate-arxiv-rs-0.1.1 (c (n "arxiv-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0dn8g6priq70z0nvr69gid290255vjkc1dmk818vcxabzsmyswar")))

(define-public crate-arxiv-rs-0.1.2 (c (n "arxiv-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "08xsj59zzh8qpxia8aq7mk5r85n9176asjkjrx47rhd54cfvsw4s")))

(define-public crate-arxiv-rs-0.1.3 (c (n "arxiv-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0565km563fvw6jgn3620k264x5yxcfacf7i8lfsbpnpcb2iw1x8c")))

(define-public crate-arxiv-rs-0.1.4 (c (n "arxiv-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "009zf6bfmi5n7x8ry9a5qpjqh0akfqbj4yjvlji3fs21rik9v3ll")))

(define-public crate-arxiv-rs-0.1.5 (c (n "arxiv-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "00j16whi0nqj3bw579ff1x2f0b639zdji78vzk6h2cv56svbj38j")))

