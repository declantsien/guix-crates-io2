(define-module (crates-io ar ip ariprog) #:use-module (crates-io))

(define-public crate-ariprog-0.1.0 (c (n "ariprog") (v "0.1.0") (h "10qgpzapcfxvh0ww78dl8bln783shj2njpsjaxwm5mr66xdgcpxd")))

(define-public crate-ariprog-0.1.1 (c (n "ariprog") (v "0.1.1") (h "1akfm925rcqk9ildjqgcjq9b9j63vrs0108zsl0d07gifymdqkb4")))

(define-public crate-ariprog-0.1.2 (c (n "ariprog") (v "0.1.2") (h "0i36ncx6m0z6iqpkfd6g8pili5rjk0jrqa25w0m98b4km7zbs412")))

(define-public crate-ariprog-0.1.3 (c (n "ariprog") (v "0.1.3") (h "086ln9d82gxvxj9iacy223izhm9zd16k9dv0s9y00l20h75cbvip")))

(define-public crate-ariprog-0.1.4 (c (n "ariprog") (v "0.1.4") (h "18w8mqsv22cl0wxg9jmxwbzzgbi60cfvih6vr0bfcbs1gkz4mglc")))

