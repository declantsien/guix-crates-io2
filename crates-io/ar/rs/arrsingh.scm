(define-module (crates-io ar rs arrsingh) #:use-module (crates-io))

(define-public crate-arrsingh-0.1.0 (c (n "arrsingh") (v "0.1.0") (d (list (d (n "arrsingh-lists") (r "^0.1.0") (d #t) (k 0)))) (h "0l9sngk35hknfz5g9dlifgmyplsarrdzlglnk264hb5vmcp9kq2j")))

(define-public crate-arrsingh-0.2.0 (c (n "arrsingh") (v "0.2.0") (d (list (d (n "arrsingh-lists") (r "^0.2.0") (d #t) (k 0)))) (h "1gmvc1dgd5dl7z4wz05cvkxq2sp4ir9h11g1d341lxbpr9zi8w08")))

(define-public crate-arrsingh-0.3.0 (c (n "arrsingh") (v "0.3.0") (d (list (d (n "arrsingh-lists") (r "^0.3.0") (d #t) (k 0)))) (h "1gn9iqw6n95n70gzvybj0qq2fvlq61v2dd2ryvixdhfa6kai4v9h")))

(define-public crate-arrsingh-0.5.0 (c (n "arrsingh") (v "0.5.0") (d (list (d (n "arrsingh-lists") (r "^0.5.0-dev") (d #t) (k 0)))) (h "08qxy94iksk7dksq9vb74926gm7x6svhn7mlz94fv1lzn8n9zpsp")))

(define-public crate-arrsingh-0.6.0 (c (n "arrsingh") (v "0.6.0") (d (list (d (n "arrsingh-lists") (r "^0.6.0") (d #t) (k 0)))) (h "127gaw2klwcdywhfvb8qgqhwvszrmw4bmfb42fqc1xnjcnb7wjy0")))

(define-public crate-arrsingh-0.8.0 (c (n "arrsingh") (v "0.8.0") (d (list (d (n "arrsingh-lists") (r "^0.7.0") (d #t) (k 0)))) (h "1s13c6hki338jp9850554yk9444c4smhwridkq0g53snpfxrs5bq")))

(define-public crate-arrsingh-0.9.0 (c (n "arrsingh") (v "0.9.0") (d (list (d (n "arrsingh-lists") (r "^0.9.0") (d #t) (k 0)))) (h "18806h3mkjp3qmnggywyy1w4gz13pyzfdipmmb5nnamx6zw5ggla")))

(define-public crate-arrsingh-0.11.0 (c (n "arrsingh") (v "0.11.0") (d (list (d (n "arrsingh-lists") (r "^0.11.0") (d #t) (k 0)))) (h "1zqr7zxdpiidh7had29whsd6fz70ibjbp904raz1y8fycark9g2d")))

(define-public crate-arrsingh-0.14.0 (c (n "arrsingh") (v "0.14.0") (d (list (d (n "arrsingh-lists") (r "^0.14.0") (d #t) (k 0)))) (h "1nq17aivmc9ngn39i1nj20hivs8jsmc2sd2pzdab99war7wd71g7")))

(define-public crate-arrsingh-0.14.1 (c (n "arrsingh") (v "0.14.1") (d (list (d (n "arrsingh-lists") (r "^0.14.1-dev") (d #t) (k 0)))) (h "1vq7gbmw8jkvac812k1z8zmwpga40vjk66pk6mc2fjh985kq6qd5")))

(define-public crate-arrsingh-0.14.2 (c (n "arrsingh") (v "0.14.2") (d (list (d (n "arrsingh-lists") (r "^0.14.2-dev") (d #t) (k 0)))) (h "0za3219krrwif4lacnm4g98nzkhih85gic4mw2amqlnkysidjkwn")))

(define-public crate-arrsingh-0.14.3 (c (n "arrsingh") (v "0.14.3") (d (list (d (n "arrsingh-lists") (r "^0.14.3-dev") (d #t) (k 0)))) (h "0s9wakbazg10k85gjipkkxqka2pqbbj85n4bdf145jv4bwwwz4gk")))

(define-public crate-arrsingh-0.18.0 (c (n "arrsingh") (v "0.18.0") (d (list (d (n "arrsingh-lists") (r "^0.18.0") (d #t) (k 0)))) (h "1agnfz39fls7hqdv57q2bdhj4gpkdh9fwwy601l0z6wv7gdbc69q")))

(define-public crate-arrsingh-0.18.1 (c (n "arrsingh") (v "0.18.1") (d (list (d (n "arrsingh-lists") (r "^0.18.1-dev") (d #t) (k 0)))) (h "1pxcw6c6j8cq8g4cm6gfr7jd3wpjry587svspdcq3dp5lbr1axql")))

(define-public crate-arrsingh-0.19.0 (c (n "arrsingh") (v "0.19.0") (d (list (d (n "arrsingh-lists") (r "^0.19.0") (d #t) (k 0)))) (h "1haa0d7y0l1mq3by622dvjn0qcfjhidis6nfk1ca3r5njjrq5cr2")))

