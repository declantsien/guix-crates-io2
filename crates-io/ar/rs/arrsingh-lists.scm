(define-module (crates-io ar rs arrsingh-lists) #:use-module (crates-io))

(define-public crate-arrsingh-lists-0.1.0 (c (n "arrsingh-lists") (v "0.1.0") (h "0w0k2fqjzcg53ml6iil90wmv11q5jpfds69hl3mnb1vsqn2ydgpq")))

(define-public crate-arrsingh-lists-0.2.0 (c (n "arrsingh-lists") (v "0.2.0") (h "083w2z97fnv3dk7lhpzj6yf2s3zwki34ic1f35wh1yjk4r86yqlk")))

(define-public crate-arrsingh-lists-0.3.0 (c (n "arrsingh-lists") (v "0.3.0") (h "1k9mdn8v4a0ph3rslxpwi5n18mz64w52q0zdxg2wihvq1vf3jgry")))

(define-public crate-arrsingh-lists-0.5.0 (c (n "arrsingh-lists") (v "0.5.0") (h "1lhdyny71fhcqvzdp75igp1dnhg1a3bgd1harwc8qs07nnz2zwv6")))

(define-public crate-arrsingh-lists-0.6.0 (c (n "arrsingh-lists") (v "0.6.0") (h "1x9fjkaxq1r67k8d4bjvp1r5j8mk8hr05pq8ia2vnr04hlgps56p")))

(define-public crate-arrsingh-lists-0.7.0 (c (n "arrsingh-lists") (v "0.7.0") (h "03348qkqnqj4d4szag2jm7r22nmpfr8c5h9v2kxfq5rw18idmc2s")))

(define-public crate-arrsingh-lists-0.9.0 (c (n "arrsingh-lists") (v "0.9.0") (h "11phsggriwkhc7r49a98pqgml8mfzr6kngiyym3nj81y4pmdjv51")))

(define-public crate-arrsingh-lists-0.11.0 (c (n "arrsingh-lists") (v "0.11.0") (h "03swb1dk31v5ckg7ib1hpxhj4l3kkpibzx5amakw1lx98bww6yil")))

(define-public crate-arrsingh-lists-0.12.0 (c (n "arrsingh-lists") (v "0.12.0") (h "1avc4cdmg8agyjgf48p034bzvljgccfka9jixlv2mml4ychaw95a")))

(define-public crate-arrsingh-lists-0.14.0 (c (n "arrsingh-lists") (v "0.14.0") (h "0lnmzac78bhya7jc2kk3yjmcjvafrwkp6zrs5fsmsjvpj7h3d879")))

(define-public crate-arrsingh-lists-0.14.1 (c (n "arrsingh-lists") (v "0.14.1") (h "1gnnla4i94q44xqjsic0ir9b9xabrlcbjdfq97mpnsvir5hzwsff")))

(define-public crate-arrsingh-lists-0.14.2 (c (n "arrsingh-lists") (v "0.14.2") (h "0hivb1d09n3ycndarqf5pcxmhqrk2pnyl2xq9brsnhb26gwwaxcs")))

(define-public crate-arrsingh-lists-0.14.3 (c (n "arrsingh-lists") (v "0.14.3") (h "1lpnlkkb5jdknl7r5bdy7kz934kai4i8qrz4512asxin1xljrsww")))

(define-public crate-arrsingh-lists-0.18.0 (c (n "arrsingh-lists") (v "0.18.0") (h "0dq2i0qxp1k52wqn7293d02sc38gvdlp968npv12qvczpd6wa75g")))

(define-public crate-arrsingh-lists-0.18.1 (c (n "arrsingh-lists") (v "0.18.1") (h "0jcfpv2v2ypgmrxiyf2459n4r7xnyprbggic2flfa418ipjvxarn")))

(define-public crate-arrsingh-lists-0.19.0 (c (n "arrsingh-lists") (v "0.19.0") (h "068wch4z2qmzw96iak3fzb1axcyja4h03lzknni8z9rg71srdfj7")))

