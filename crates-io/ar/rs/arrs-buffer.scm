(define-module (crates-io ar rs arrs-buffer) #:use-module (crates-io))

(define-public crate-arrs-buffer-0.0.1 (c (n "arrs-buffer") (v "0.0.1") (h "0cn7mgjzg3jp013l22db373ncr41mfqhb932f54rn81bm5pv9ryw")))

(define-public crate-arrs-buffer-0.0.2 (c (n "arrs-buffer") (v "0.0.2") (h "1r6sh2sn8xznwsi5xpzsl4ygxfgwwbj7hcsa5i7iwpvwkjfss4n4")))

