(define-module (crates-io ar rs arrsac) #:use-module (crates-io))

(define-public crate-arrsac-0.1.0 (c (n "arrsac") (v "0.1.0") (d (list (d (n "derive_setters") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)) (d (n "pcg_rand") (r "^0.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (k 0)) (d (n "sample-consensus") (r "^0.1.0") (d #t) (k 0)))) (h "01q849az448mybx6wpcdylyrhxdhv255mcz9cmgagj3v3ckkcp1w")))

(define-public crate-arrsac-0.2.0 (c (n "arrsac") (v "0.2.0") (d (list (d (n "derive_setters") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)) (d (n "pcg_rand") (r "^0.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (k 0)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "0zgl1h47k5nyy2c3kylqgc1xl3l2s1jl7701b1p12y3gy07diip2")))

(define-public crate-arrsac-0.3.0 (c (n "arrsac") (v "0.3.0") (d (list (d (n "derive_setters") (r "^0.1.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "1xvdjz8s992wf2g8qwp5g0clfrxsbfgbvh0qdyasbmmwhdz8jcsa")))

(define-public crate-arrsac-0.4.0 (c (n "arrsac") (v "0.4.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^0.2.0") (d #t) (k 0)))) (h "18xds8h5s3yc5w7v0bb334vrfnmx1jsv96fs3m8bcki1ryd4cqwj")))

(define-public crate-arrsac-0.5.0 (c (n "arrsac") (v "0.5.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "1dngvc3rss33vkhvapv45ilimqghgaad5jssfhxxwnmn866dnx2w")))

(define-public crate-arrsac-0.6.0 (c (n "arrsac") (v "0.6.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "11q873d1x2fwfcqlh31b99r8f10zbvyqf6ds7zf6pjr1qw4r7pks")))

(define-public crate-arrsac-0.6.1 (c (n "arrsac") (v "0.6.1") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "1s7hn4adpzba2z3qz5isx94pzsgpvbrx535hdnavi6134qmcpvvr")))

(define-public crate-arrsac-0.7.0 (c (n "arrsac") (v "0.7.0") (d (list (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "1bhbk36jaw4b18lnh0acjpbrnb6yby8467lpd2504cki7l517lf4")))

(define-public crate-arrsac-0.7.1 (c (n "arrsac") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "0bbbzz9qxawfi2hanpz7jq2h6i53zhwimk9p6azssqk2d8asxk9k")))

(define-public crate-arrsac-0.8.0 (c (n "arrsac") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "0cxk6a0zpkh1wa2fvllkh97lg47lj95n01khk55zcw7wcbrbkbcw")))

(define-public crate-arrsac-0.9.0 (c (n "arrsac") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "03pi2qncdjqnrg9z5cndrgax9wb57sxa8d92yrcfvyai17kximwf")))

(define-public crate-arrsac-0.10.0 (c (n "arrsac") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "sample-consensus") (r "^1.0.1") (d #t) (k 0)))) (h "00j7rkzwsmvj3d7aw7y8xcrc30j6qiy7ifbxww9yjqhphgjn5gkk")))

