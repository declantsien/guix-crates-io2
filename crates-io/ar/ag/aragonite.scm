(define-module (crates-io ar ag aragonite) #:use-module (crates-io))

(define-public crate-aragonite-0.1.0 (c (n "aragonite") (v "0.1.0") (d (list (d (n "aragonite_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_LibraryLoader" "Win32_System_Kernel" "Win32_System_WindowsProgramming" "Win32_System_SystemServices" "Win32_System_Diagnostics_Debug" "Win32_System_SystemInformation"))) (d #t) (k 0)))) (h "167sm91r2ra3wnszkwrhvmhcxq8fpr02hpakmkraxbwjhm1xaqkw")))

