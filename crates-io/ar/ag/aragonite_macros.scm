(define-module (crates-io ar ag aragonite_macros) #:use-module (crates-io))

(define-public crate-aragonite_macros-0.1.0 (c (n "aragonite_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "03yihq5y399nkqx9wiqkfiwwa2rvv5v9q5k44f3cr1d4yizkg3jl")))

