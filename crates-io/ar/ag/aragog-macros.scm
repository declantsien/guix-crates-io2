(define-module (crates-io ar ag aragog-macros) #:use-module (crates-io))

(define-public crate-aragog-macros-0.1.0 (c (n "aragog-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ia2dfgxzg8gna8ccm157c1gswas00bz28c9rix8l0j4qczcxdla")))

(define-public crate-aragog-macros-0.2.0 (c (n "aragog-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c5b7xqmhqnzvar0xa9yvxq248j8mzgx66akqfyd41cnq4mdr8lw") (y #t)))

(define-public crate-aragog-macros-0.2.1 (c (n "aragog-macros") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05hbh2wikvr3qyg9hx154lnfm706v1x399m33b0153lbv9kffkvd") (y #t)))

(define-public crate-aragog-macros-0.2.2 (c (n "aragog-macros") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zwi1rl6fdmqd9iymakjjb3rqa9bl0kljx1lqp27acz2qwh4qymb")))

(define-public crate-aragog-macros-0.3.0 (c (n "aragog-macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05733pmlq3xw2jaak438g2gd7msprrnhy5criv0fhrg2lz74midq") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.4.0 (c (n "aragog-macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b4qdlswzf8nk878kzv3nz4k146qn2fqr01haaaqlza2ys65dpac") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.4.1 (c (n "aragog-macros") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h3m26pyj57rq8jxcv694si5k1shlgxb3jnqrd8wy57012hhmyy2") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.5.0 (c (n "aragog-macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ipbmaha0mc44nm4zgblxvg5qid0iq6nqv9fi6psfa0p303bsab0") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.6.0 (c (n "aragog-macros") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wl83vgn2yqx9ll65zndb1rc5ajw9rxdykhj7f86lqyd9dpvrwvj") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.7.0 (c (n "aragog-macros") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mykdn3cc5zp4fk1qjzcw76h8i1j1n89rb08ws9pa3vy8p773pl9") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.7.1 (c (n "aragog-macros") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0igv9mhh2ybgjlrg3ab2ly1f2ibi86rwmm4gr6pv0nnvrk86kz0c") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.7.2 (c (n "aragog-macros") (v "0.7.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m2wa2z15r7mlfi1ykym8a2bw1lpjrmmkri3y1avvzdycc5l7351") (f (quote (("default") ("blocking"))))))

(define-public crate-aragog-macros-0.7.3 (c (n "aragog-macros") (v "0.7.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kx29c9g5ghggj487lajb6mmq8zynl1h8shbyrmq0za9i5621ni3") (f (quote (("default") ("blocking")))) (r "1.56.0")))

(define-public crate-aragog-macros-0.8.0 (c (n "aragog-macros") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dh40dl1hcnamgfjcmdz0mjc0773abawqrbnqb0sbbczy2szfj5b") (f (quote (("default") ("blocking")))) (r "1.56.1")))

