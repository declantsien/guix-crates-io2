(define-module (crates-io ar so arson) #:use-module (crates-io))

(define-public crate-arson-0.1.0 (c (n "arson") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0dx2y10kjkfm9siy5crc5bklkl6m40hyz9yvr9aa1iylikmlg63k")))

(define-public crate-arson-0.2.0 (c (n "arson") (v "0.2.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1snh24rcyfgwdpiph4a78nrz0zqrmrl8z8ha0380973xygy7q3lc")))

(define-public crate-arson-0.2.1 (c (n "arson") (v "0.2.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0hr5fb7yxk7mjfmd3qk12hyw191n51kba83z8f2llcgh191bypkr")))

