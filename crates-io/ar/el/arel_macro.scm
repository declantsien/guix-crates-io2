(define-module (crates-io ar el arel_macro) #:use-module (crates-io))

(define-public crate-arel_macro-0.0.0 (c (n "arel_macro") (v "0.0.0") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0idrygqk5nypghd9n6z5qjyb2lb3cdah95188vssrsa9g19qkc03")))

(define-public crate-arel_macro-0.0.1 (c (n "arel_macro") (v "0.0.1") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xszzvqns7h63zphvaszvifcvaw98djalhbp1ijvch53k245yky0")))

(define-public crate-arel_macro-0.0.2 (c (n "arel_macro") (v "0.0.2") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wsw4icr510d4l5rms8b9097v31hc6g7nljjrripwj56ll4jj145")))

(define-public crate-arel_macro-0.0.3 (c (n "arel_macro") (v "0.0.3") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1cnmhwi96mqwxkxiygr7y5rs3s1brkxwyxlfgw0wr7mj0gm9hkl5")))

(define-public crate-arel_macro-0.0.4 (c (n "arel_macro") (v "0.0.4") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ybzks8rm0xpb5ik82frjhzhamx759cn8lmnqh6ra4891yxg7dpa")))

(define-public crate-arel_macro-0.0.5 (c (n "arel_macro") (v "0.0.5") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yvjmqw53xjh01c8dphncb43rj5n5d3w25pbj6xvifmfcfxsmz5w")))

(define-public crate-arel_macro-0.0.6 (c (n "arel_macro") (v "0.0.6") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0is96djzf22ssns2zqx4nz51y6hm4dbxzj3rgiar9d63jpin9pa9")))

(define-public crate-arel_macro-0.0.7 (c (n "arel_macro") (v "0.0.7") (d (list (d (n "expansion") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pb5dk4k2nv3yfl4ai3r1hk398bks5vq53acp5ryl32k1sf0cbs9")))

(define-public crate-arel_macro-0.0.8 (c (n "arel_macro") (v "0.0.8") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "expansion") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1lzglm9x2pgsczp83xhwn56q8xiaqw05vzr56rvdz1hqg9n2vbbw")))

(define-public crate-arel_macro-0.0.9 (c (n "arel_macro") (v "0.0.9") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "expansion") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "128sqsxksw0lqmv5anddqwlc6n95ch255r859gkh40wiz6hznrzf")))

