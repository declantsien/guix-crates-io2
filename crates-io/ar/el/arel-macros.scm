(define-module (crates-io ar el arel-macros) #:use-module (crates-io))

(define-public crate-arel-macros-0.0.1 (c (n "arel-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jhpp8zqkbyfc3n6mds43nsd1marg0qg1iivs00m0hrpl8bffxkr")))

(define-public crate-arel-macros-0.0.2 (c (n "arel-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "162n1lqypzqigk47i0rxklkxqgbb4c3chhr6y8pph09rmk3mrvf6")))

(define-public crate-arel-macros-0.0.3 (c (n "arel-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xy0lfkp0jbv30pzl0hkc32ajk1zbpg91cf2jl8b6kyf2dvqn3am")))

(define-public crate-arel-macros-0.0.4 (c (n "arel-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mjfcdbm9jy982ajjy82y0lg6457rbmiybs1x3vj064glxnhjf49")))

(define-public crate-arel-macros-0.0.5 (c (n "arel-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dk6mjwpq507l28rh1pkifjbxr7vwdsc9dq0blx4n57kizna850b")))

(define-public crate-arel-macros-0.0.6 (c (n "arel-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0468m7ha8fb43ic11ifhmk1kfs3dl7ik45wb64090q1cbb5gxnbq")))

(define-public crate-arel-macros-0.0.7 (c (n "arel-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0x1rfk4rc7z2fdvbkvgnhwgi88j1441cj6qymbz0rcbns4s78v8f")))

(define-public crate-arel-macros-0.0.8 (c (n "arel-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vlz5a42gr6xcpdy9lzlp5h8nh8dav2i10ahwgm42dh0hqnryfnk")))

(define-public crate-arel-macros-0.0.9 (c (n "arel-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1zgq8dmmhxmif9im8ripi4g8c2yxsy2ka9rk1shg9jgn5mvak2ni")))

(define-public crate-arel-macros-0.3.0 (c (n "arel-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0yh01izh48p5cchhlsqkiakmwnlzzw16vy1z25b8yhia8ygs2w2i")))

(define-public crate-arel-macros-0.3.1 (c (n "arel-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y7b4f7haam580v2vm0sc1hm86qys1k44wws4g8r8fb0jsrm2f56")))

(define-public crate-arel-macros-0.3.2 (c (n "arel-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hbf319bs7bgif5mwcb69cy30d0sngabg0g062ihljw67kygrw41")))

(define-public crate-arel-macros-0.3.3 (c (n "arel-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "185im7qn8qbzlly0wfdiz6m19sr2s4sljm4vqx05vlinzwvq1zz2")))

(define-public crate-arel-macros-0.3.4 (c (n "arel-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0nl0rpxz4plc665fya0qz614lxyq674z38gcsd83dmmrp49n82c5")))

(define-public crate-arel-macros-0.3.5 (c (n "arel-macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "047vnncxw74rg3i612b79q39v40pqkhpjyi4rp7ig5kcp1r4axn3")))

(define-public crate-arel-macros-0.3.6 (c (n "arel-macros") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09z1sxh9pd3hkczsnvw29y1vsghsdypxbqp7p7m7nzzfpggqcy1x")))

(define-public crate-arel-macros-0.3.7 (c (n "arel-macros") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xcalc4w5bg2p5kbhzn7d646ww428jfgay599b2nb0p4scf0a1d1")))

(define-public crate-arel-macros-0.3.8 (c (n "arel-macros") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fzzzmmdcvpm4r3nvp4cv4hm5cspi79k9k63frsgnaf4x8hvxznf")))

