(define-module (crates-io ar el arelith_combat_simulator) #:use-module (crates-io))

(define-public crate-arelith_combat_simulator-1.0.0 (c (n "arelith_combat_simulator") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13l0591n932ma43v3xlskvdprs6ylavb342arkwz3cj7jq48s7pf")))

(define-public crate-arelith_combat_simulator-1.1.0 (c (n "arelith_combat_simulator") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sc9csrnvd9ykd66frjwr7z31dhjcybcgl8r1prrkgk1bydjakqz")))

(define-public crate-arelith_combat_simulator-1.2.0 (c (n "arelith_combat_simulator") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wcv2dvb8bmaxvy4p9ax2imclicijkbkm4qwrf7wy74qn47ry56v")))

(define-public crate-arelith_combat_simulator-1.3.0 (c (n "arelith_combat_simulator") (v "1.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "070vqbhl1f7zim0mdzraxkiqf0b4i0hhf7m3dwwl0z1w4avg8dmq")))

(define-public crate-arelith_combat_simulator-1.4.0 (c (n "arelith_combat_simulator") (v "1.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qffin57cbqx5a8r8syl76ljpm378b0r7lf0gm4y39xnhnw07hnm")))

(define-public crate-arelith_combat_simulator-1.4.1 (c (n "arelith_combat_simulator") (v "1.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x3fmz5rcca5imdvas7pxfx91rmsq0ic6gab0305766h3cack4p6") (y #t)))

(define-public crate-arelith_combat_simulator-1.4.2 (c (n "arelith_combat_simulator") (v "1.4.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gnfn5xs9pqyjkb6w7xfjy22sb7nnl435q9z65b6kw85jyarz6n7") (y #t)))

(define-public crate-arelith_combat_simulator-1.4.3 (c (n "arelith_combat_simulator") (v "1.4.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h96mhkpkz2yl4xndvqnmyhc138ckrycz478yfczkk96ii0l26p2") (y #t)))

(define-public crate-arelith_combat_simulator-1.4.4 (c (n "arelith_combat_simulator") (v "1.4.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k9mh878f21dx8v5vws3l71n778n3rjv7vpppswh5gr9rpraf8vl")))

(define-public crate-arelith_combat_simulator-1.5.0 (c (n "arelith_combat_simulator") (v "1.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lmqwdm9i5mp8izic7m57awam6i6m2x61gff0rbmpmz6fhmb1l3d")))

