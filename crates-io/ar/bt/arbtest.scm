(define-module (crates-io ar bt arbtest) #:use-module (crates-io))

(define-public crate-arbtest-0.1.0 (c (n "arbtest") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.1.0") (d #t) (k 0)))) (h "16dj9nyjlf15sk37p4n86wp75k762iqq87ffz0141jyxhdzpz953") (f (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.1.1 (c (n "arbtest") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1.1.0") (d #t) (k 0)))) (h "1b689y26j4a5q43941jgj9rzw5ir9pc6cd0s3v2jwmspi659204r") (f (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.1.2 (c (n "arbtest") (v "0.1.2") (d (list (d (n "arbitrary") (r "^1.1.0") (d #t) (k 0)))) (h "110fckpjs48qw73b3vxf7g2fk7xffdlx4zlk46rml9xnlq54dn0c") (f (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.2.0 (c (n "arbtest") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1.1.0") (d #t) (k 0)))) (h "1avmdhqsm4w64f9r5qgcy940859ahmvxrx5b46s0b13b2nafx36l") (f (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.3.0-pre.1 (c (n "arbtest") (v "0.3.0-pre.1") (d (list (d (n "arbitrary") (r "^1.3.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "regex") (r "^0.1.5") (d #t) (k 2) (p "regex-lite")))) (h "179lkvdlzghdi7lzzk5qp92khpa37njlwwn6s6wqd39871cjra7y")))

(define-public crate-arbtest-0.3.0 (c (n "arbtest") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1.3.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "regex") (r "^0.1.5") (d #t) (k 2) (p "regex-lite")))) (h "0qzx35dm0bx0wg4vsa8d2bqvnp9rfy2yhgmzzn1kjxbadxqzgwsn")))

(define-public crate-arbtest-0.3.1 (c (n "arbtest") (v "0.3.1") (d (list (d (n "arbitrary") (r "^1.3.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "regex") (r "^0.1.5") (d #t) (k 2) (p "regex-lite")))) (h "1p1z3450v617axpmh539qhpd47g4vd3yi1y8ljlc5yhpnmgrv413")))

