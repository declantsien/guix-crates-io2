(define-module (crates-io ar ky arkyo) #:use-module (crates-io))

(define-public crate-arkyo-0.1.0 (c (n "arkyo") (v "0.1.0") (h "1ikyc4mf3409pxvjfkf24cmxxpif1vfpfgmp8qsf53hds7pzspah") (y #t)))

(define-public crate-arkyo-0.0.1 (c (n "arkyo") (v "0.0.1") (h "0vrcdjj838r0zx0nwzm8j87fp7zs8yz5nkm2c5mwvf744niysbkh")))

(define-public crate-arkyo-0.0.2 (c (n "arkyo") (v "0.0.2") (h "0839vys9a5jh6x0wj5rd2pfzg3wzvpvp7lzgwp5q4v9rg1f3mfmz")))

(define-public crate-arkyo-0.0.3 (c (n "arkyo") (v "0.0.3") (h "165gjr1ngpd06l9c63m4kkc55ggkhpmd7993xmw35dvdv7rrsz7y")))

(define-public crate-arkyo-0.0.4 (c (n "arkyo") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0byxaln1gxg69scxvlgh02b6pjwbkxqmx26cxnvikxbzr7mq6c90")))

(define-public crate-arkyo-0.0.5 (c (n "arkyo") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "161dk9kwkylxcrwv1cn3yqasrjrgrjprapfw4y1lgyz0ym7g5xy4")))

(define-public crate-arkyo-0.0.6 (c (n "arkyo") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0cskxjnpdaf0dkijzwqy6g4sy42mwf8nfxvl6pmghs4vdx8bg291")))

