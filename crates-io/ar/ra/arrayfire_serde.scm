(define-module (crates-io ar ra arrayfire_serde) #:use-module (crates-io))

(define-public crate-arrayfire_serde-0.1.0 (c (n "arrayfire_serde") (v "0.1.0") (d (list (d (n "arrayfire") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.18") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.18") (d #t) (k 0)))) (h "08vjb9sc1gcivycl50d3p532812wnah4d86zapd3d7al4z8vrr94")))

