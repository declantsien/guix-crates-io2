(define-module (crates-io ar ra array-concat) #:use-module (crates-io))

(define-public crate-array-concat-0.0.0 (c (n "array-concat") (v "0.0.0") (h "1i7labpggzb14gc2y6gdlky54npkkfy08saxn7n694a1qnfgxwa9")))

(define-public crate-array-concat-0.1.0 (c (n "array-concat") (v "0.1.0") (h "1f66cxsj2a4lyb38lxjvz9qlrl82wf6si277nifrhghqcmncaqd8")))

(define-public crate-array-concat-0.2.0 (c (n "array-concat") (v "0.2.0") (h "12anhzjp782d27yg81fb848xhkfwvvw7h7gig1yavvf471dwfl6j")))

(define-public crate-array-concat-0.3.0 (c (n "array-concat") (v "0.3.0") (h "1zlc9pwgh160y1h1rwpz4dhf0lbmd22n9qsm0qyj94ii9pvhw6pc")))

(define-public crate-array-concat-0.3.1 (c (n "array-concat") (v "0.3.1") (h "15bjjgcf86grk4q10vxrgv4nf4wbbakv1cbq4jwala0swmflpy1i")))

(define-public crate-array-concat-0.4.0 (c (n "array-concat") (v "0.4.0") (h "1i2ki32i0ii5nd33dx04dl7r8krdaxq07l2baz1khw5zm4achnwn")))

(define-public crate-array-concat-0.4.1 (c (n "array-concat") (v "0.4.1") (h "0v71p20adq2qsz5ci57n62whd84ifmq5wx40if6n7binxpa8y8fv")))

(define-public crate-array-concat-0.5.0 (c (n "array-concat") (v "0.5.0") (h "1snppmqd642fwjsgqpp0jwl77prlwxajray06x2igkwfnlfslgyg")))

(define-public crate-array-concat-0.5.1 (c (n "array-concat") (v "0.5.1") (h "0d4il92m2navq0niw8y7916gsh0ylyh330g5n720zx818gjgprvs")))

(define-public crate-array-concat-0.5.2 (c (n "array-concat") (v "0.5.2") (h "0zbz9slqnaf70bmgi7nwsn8199vcz2xshzzgwp7hgg6cfbmhy679")))

