(define-module (crates-io ar ra arrange) #:use-module (crates-io))

(define-public crate-arrange-0.1.0 (c (n "arrange") (v "0.1.0") (h "0vb9blfgs06j34b6g5albrj1zw3kg5hm3hirr1w4dcdbpx49xz3n") (y #t)))

(define-public crate-arrange-0.1.1 (c (n "arrange") (v "0.1.1") (h "16na78dc47i0g4ng7jv74r0i2vrhxl5qa1qac8xfpnwd2h77rn0k") (y #t)))

(define-public crate-arrange-0.1.2 (c (n "arrange") (v "0.1.2") (h "1ps8sw1dcdrcpmn7cj0l8656cj5kg0ylnp16z1vd352v8jfmf6lc") (y #t)))

(define-public crate-arrange-0.1.3 (c (n "arrange") (v "0.1.3") (h "09zhvhmpq88b5lr9p8fbqvjxm0ivy97m5x00psi345jxxkw0sb37")))

