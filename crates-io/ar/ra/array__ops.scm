(define-module (crates-io ar ra array__ops) #:use-module (crates-io))

(define-public crate-array__ops-0.1.0 (c (n "array__ops") (v "0.1.0") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "0z6n93hrgpyaxp3v88ws1ip0cm27mxz555hg0z9flqdrws54dpqd")))

(define-public crate-array__ops-0.1.1 (c (n "array__ops") (v "0.1.1") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "1mahm95jb9xl1xs8hn3q83q3n3hddqwy6dd53rs6qxpy6jz1glcc")))

(define-public crate-array__ops-0.1.2 (c (n "array__ops") (v "0.1.2") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "0qkpyb1v9qgpjkv8bqvdhaldsz6d7x2s13ywrrgs21sl8h2x8wrj")))

(define-public crate-array__ops-0.1.3 (c (n "array__ops") (v "0.1.3") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "1hmgc5mj8lgwx79sx086233lw0nvxppvz1f98mg9zi8pvsa216bc")))

(define-public crate-array__ops-0.1.4 (c (n "array__ops") (v "0.1.4") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "0p8pa5b5ff37433pd3fi89k4jfdpmwi8ygkzzc16893ycxvvmf6w")))

(define-public crate-array__ops-0.1.5 (c (n "array__ops") (v "0.1.5") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.0") (d #t) (k 0)))) (h "144lzq5ahjvi1sk14y8qbjxzg6yky8j2g58r9a2wbi884zkkf800") (f (quote (("std") ("default"))))))

(define-public crate-array__ops-0.1.6 (c (n "array__ops") (v "0.1.6") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.2") (d #t) (k 0)))) (h "0l6cm6cq5nxhbs51l06k27cmrx39cw9lr7nxqn38pwswfvmf33cl") (f (quote (("std") ("default"))))))

(define-public crate-array__ops-0.1.7 (c (n "array__ops") (v "0.1.7") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.3") (d #t) (k 0)))) (h "0x7yi4km0zmm7q53b71lb9k5bwvv3zcxvmdmi09s1zzi4z5bz8a6") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.8 (c (n "array__ops") (v "0.1.8") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.5") (d #t) (k 0)))) (h "1x5k6blpk3lhsyhailw1k9x1alrsd24wrf7knv2rjjq7c2a6y4dx") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.9 (c (n "array__ops") (v "0.1.9") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.5") (d #t) (k 0)))) (h "1b398qsb3b1hqpfhmc705jh77w0sag8da5m7dy045gincglvjk2f") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.10 (c (n "array__ops") (v "0.1.10") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.5") (d #t) (k 0)))) (h "0qxdvdz8lpzyl7r16l6lfbpi5vw33jjwab4yzv7sbiizllwlcab1") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.11 (c (n "array__ops") (v "0.1.11") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.6") (d #t) (k 0)))) (h "05ahw8abys59grrgwzkdj4j5767vsmzm9zih04bjhcdhr3d2l3g0") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.12 (c (n "array__ops") (v "0.1.12") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.6") (d #t) (k 0)))) (h "18xixfvihflqrzzlckmmnzzbxql1mnx3cs543qrjrh53c9ifclfd") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.13 (c (n "array__ops") (v "0.1.13") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.7") (d #t) (k 0)))) (h "050ii8drb3m4vap8h881ba8lpmrjdwryjzj780jssx46shkni1gw") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.14 (c (n "array__ops") (v "0.1.14") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.7") (d #t) (k 0)))) (h "041ary3f1zpvnqkdrr35d2p1gp9gbvp4kwxilhx7b703b22k7f1f") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.15 (c (n "array__ops") (v "0.1.15") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.7") (d #t) (k 0)))) (h "0fxahwzjg216n8194qklavay82m7h0sc7dv2sd23v9xka7lg0h5r") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.16 (c (n "array__ops") (v "0.1.16") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.7") (d #t) (k 0)))) (h "1a6vqr04wrbqg14a6zmcmm1jzxa5ra2zs7mqsjl8jcfabk2fs27d") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.17 (c (n "array__ops") (v "0.1.17") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.7") (d #t) (k 0)))) (h "1lbbihahd26n9yj7q6xdplldc66p5s9j3g927spwj2nphnr97yam") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.18 (c (n "array__ops") (v "0.1.18") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.8") (d #t) (k 0)))) (h "14im6bzc7a31anx3fps8mnz3qakmwk0pq2rqkyx6vajxjmsajah2") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1.19 (c (n "array__ops") (v "0.1.19") (d (list (d (n "array_trait") (r "^1.0.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_ops") (r "^0.1.10") (d #t) (k 0)))) (h "0pjxxa2r85nqv5xslac92lvqp9sl4bhp1ncmcvkzvgm467dj1mcq") (f (quote (("std" "slice_ops/std") ("default" "std"))))))

