(define-module (crates-io ar ra array-util) #:use-module (crates-io))

(define-public crate-array-util-1.0.0 (c (n "array-util") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)))) (h "1ycyck5b35w839kfsxnrhcljdr3v1wxdcx8gv7dvixxxqxywpdks") (r "1.71.1")))

(define-public crate-array-util-1.0.1 (c (n "array-util") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)))) (h "19rnc5n0zkcyzf9my1bpx3abqsq1vbf9f8gmx6qaih8kkw9xak9w") (r "1.71.1")))

(define-public crate-array-util-1.0.2 (c (n "array-util") (v "1.0.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)))) (h "1mifri4zgy8av8pkvqb02c3z95b65d56hi1l5h5bj2cgvr29hl3y") (r "1.71.1")))

