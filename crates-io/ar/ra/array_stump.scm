(define-module (crates-io ar ra array_stump) #:use-module (crates-io))

(define-public crate-array_stump-0.1.0 (c (n "array_stump") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1fk9r6y9jvi47rl72l70jcmjbihbhrzk01b8l0smjjpwzbxns31c")))

(define-public crate-array_stump-0.2.0 (c (n "array_stump") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1hmx5hih8hnllczj4vw5vxgfp5gqj6vax2df9gan1sjcs8708j8v") (f (quote (("indextrait"))))))

(define-public crate-array_stump-0.2.1 (c (n "array_stump") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "09ym0z9lc3s35p25b392hvp0qga0kn6vj6d33axgakk6n037z2wv") (f (quote (("indextrait"))))))

