(define-module (crates-io ar ra array2d) #:use-module (crates-io))

(define-public crate-array2d-0.1.0 (c (n "array2d") (v "0.1.0") (h "0j7vdcrqllc8rv8d2z3ck8z6z7dba1h588mqxqya3q2yvb8paz07")))

(define-public crate-array2d-0.2.0 (c (n "array2d") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n9c2nfwkrhyc84s6ls0m97l40fab9iirm1sby0wfzpli8nl415i")))

(define-public crate-array2d-0.2.1 (c (n "array2d") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s69ryvwpjysh0na22qdh4wcws76dq3qdc87if60dpg7sfwjsjr2")))

(define-public crate-array2d-0.3.0 (c (n "array2d") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dy9pbn5wfi5prkslhpjwqhzfqbqbfadsx5df4n87ix9s0qks2br")))

(define-public crate-array2d-0.3.1 (c (n "array2d") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rf0nf9h1rpbna9xxaipd93vmma7iy5nq8q7cm4rpzxip9155bcl")))

(define-public crate-array2d-0.3.2 (c (n "array2d") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0x4zi30jv548ac7r0a23bampvy4whnmrbalps06pqnmzq6r9rcyq")))

