(define-module (crates-io ar ra array-ops) #:use-module (crates-io))

(define-public crate-array-ops-0.1.0 (c (n "array-ops") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.4.0") (d #t) (k 0)))) (h "1jn7l9gppp6kr9kgsy22sw5p9cn1jazmqxy296gjwiwz5d11i4fc")))

