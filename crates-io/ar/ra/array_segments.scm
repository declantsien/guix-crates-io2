(define-module (crates-io ar ra array_segments) #:use-module (crates-io))

(define-public crate-array_segments-0.1.0 (c (n "array_segments") (v "0.1.0") (d (list (d (n "array__ops") (r "^0.1.10") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1mclkqqpf0qm2a498wsj6f7rmmn9v62q8rbvykf8k8pyb8xagzyj")))

(define-public crate-array_segments-0.1.1 (c (n "array_segments") (v "0.1.1") (d (list (d (n "array__ops") (r "^0.1.10") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1mysh4gj86xybrrmsh235zc7ijq3832nbfw6sc9g244mymfnxk2a")))

(define-public crate-array_segments-0.1.2 (c (n "array_segments") (v "0.1.2") (d (list (d (n "array__ops") (r "^0.1.12") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "16b50iihlim6c29110xlwcs1rk7a1jpb3w7x1frrs1dagarpg182")))

(define-public crate-array_segments-0.1.3 (c (n "array_segments") (v "0.1.3") (d (list (d (n "array__ops") (r "^0.1.13") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0xs88qpqadjjcmga2niy8zgx6fis0g78wqciy4fxk2jl0hfym8i5")))

