(define-module (crates-io ar ra arraytools) #:use-module (crates-io))

(define-public crate-arraytools-0.1.0 (c (n "arraytools") (v "0.1.0") (h "1gmzj8rw9g156gwl8ix2f4gfx7g6z8h3xwaq0q7x0bwavq12sy8r")))

(define-public crate-arraytools-0.1.1 (c (n "arraytools") (v "0.1.1") (h "17sfslnpbzmpnkc5c100v94721q85a9y1ik2djh7ywf35gi6ydgz")))

(define-public crate-arraytools-0.1.2 (c (n "arraytools") (v "0.1.2") (h "1d220mk1alfkdi1b9afy44lwnxqc2088fzhcdnr4pvn3p84bcn51")))

(define-public crate-arraytools-0.1.3 (c (n "arraytools") (v "0.1.3") (h "0p3f59z6r1vjhbfylypshrdnlcjkns794gf0f88bh7gm2vqsxin7")))

(define-public crate-arraytools-0.1.4 (c (n "arraytools") (v "0.1.4") (h "086x5whs4yixx5qz44l1jvl9sq8ivw4jiw6ywc6v3clfhkd2zwc7")))

(define-public crate-arraytools-0.1.5 (c (n "arraytools") (v "0.1.5") (h "0dzl752gd56xsg0z1bdy12xmxj3lf5ykqsig5ydq17cdjccpjdm5")))

