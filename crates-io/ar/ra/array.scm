(define-module (crates-io ar ra array) #:use-module (crates-io))

(define-public crate-array-0.0.0 (c (n "array") (v "0.0.0") (h "0xivrmd1vakh16mhj5if6qmn545858jjwkl7vb8k20sbnpc0fwmh")))

(define-public crate-array-0.0.1 (c (n "array") (v "0.0.1") (h "04gskadl6z0vbavsmk1n65yksvdd6jj3kmpsb9yjj02hwwg0a9xz")))

