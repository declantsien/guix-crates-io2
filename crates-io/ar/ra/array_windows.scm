(define-module (crates-io ar ra array_windows) #:use-module (crates-io))

(define-public crate-array_windows-0.1.0 (c (n "array_windows") (v "0.1.0") (h "12lgg673jc6slg2cmmi37wg4crpgbjfscdx4fy07laa9mj27v5al")))

(define-public crate-array_windows-0.2.0 (c (n "array_windows") (v "0.2.0") (h "0v3xm7l92b7ifhq4iw3bqz4xxmc5cwk7bgf13q888jkl120n0fwh")))

