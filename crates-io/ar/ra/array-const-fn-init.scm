(define-module (crates-io ar ra array-const-fn-init) #:use-module (crates-io))

(define-public crate-array-const-fn-init-0.1.0 (c (n "array-const-fn-init") (v "0.1.0") (h "1v1cjlnr13r1kfipaxqgn60x521xgjwfr1lspi1vzbjbqpp5q73k")))

(define-public crate-array-const-fn-init-0.1.1 (c (n "array-const-fn-init") (v "0.1.1") (h "1yxgsvzl2ijvw7sw1brjxwa7fa5sa2vldzzslrzl0pf093jqbjwb")))

