(define-module (crates-io ar ra arranged) #:use-module (crates-io))

(define-public crate-arranged-0.1.0 (c (n "arranged") (v "0.1.0") (d (list (d (n "arith_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.64") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1sxaqybkbvwikaw2ffgaj9bgpfnq5pd3kqm4fnyz4a2ybpvjyxv7")))

(define-public crate-arranged-0.1.1 (c (n "arranged") (v "0.1.1") (d (list (d (n "arith_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.64") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1cn9p1fdm9z5kqigvlam4c12m1bgd7hcz553d60yn1qmxmxmin2z")))

(define-public crate-arranged-0.1.2 (c (n "arranged") (v "0.1.2") (d (list (d (n "arith_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "backtrace") (r "^0.3.64") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0sabw0xwa36qfjlcmdd7ycs95gscx1n11z2wsv7yxpdi5rpyp5d7")))

