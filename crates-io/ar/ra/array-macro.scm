(define-module (crates-io ar ra array-macro) #:use-module (crates-io))

(define-public crate-array-macro-0.1.0 (c (n "array-macro") (v "0.1.0") (d (list (d (n "array-macro-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "16gv392zg04ai613ryn5jh5bp6d1dxyy9hglwgnkrpjcz80jhb8m") (y #t)))

(define-public crate-array-macro-0.1.1 (c (n "array-macro") (v "0.1.1") (d (list (d (n "array-macro-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "0awb5gc95dh5nizxs4x11h6a00028hfhalw9sqrk7gaf247j3bn0") (y #t)))

(define-public crate-array-macro-0.1.2 (c (n "array-macro") (v "0.1.2") (h "1im2wg49h10d5kr9vqpqybw5n0g6n3yd1i57l81wix9bglkkcv5c") (y #t)))

(define-public crate-array-macro-1.0.0 (c (n "array-macro") (v "1.0.0") (h "11z9jfmxyjd2s41qsrgi263vfir6f9dijgk6zpsnhlqz6r2gwmpg") (y #t)))

(define-public crate-array-macro-1.0.1 (c (n "array-macro") (v "1.0.1") (h "11mrcgw5pd367dgfsc1xv26i9x5xsak8vmyhz2ak19dqadaxz5jj") (y #t)))

(define-public crate-array-macro-1.0.2 (c (n "array-macro") (v "1.0.2") (h "1vs0v010rpcgq24434x52v0pbn29q8yl4l76q0n9ypi3vq01l6wb") (y #t)))

(define-public crate-array-macro-1.0.3 (c (n "array-macro") (v "1.0.3") (h "1ljjd2iqja2k4229kyrhra5mb1g6vph9nfgxxhgjli7v4mxg6kvw") (y #t)))

(define-public crate-array-macro-1.0.4 (c (n "array-macro") (v "1.0.4") (h "1pn41nrd6sbbgzw5nmkx6ga9pj7d3na00m692k1svrylfvflw0vx") (y #t)))

(define-public crate-array-macro-1.0.5 (c (n "array-macro") (v "1.0.5") (h "19mdx2xlppnqwl6rhsbzylx61a0kkp2ql8q16195b7iga977ps86")))

(define-public crate-array-macro-2.0.0 (c (n "array-macro") (v "2.0.0") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1sy5x4jdq33hf4qa7svnak3bqv9r8hxzifk2fp55hc60v1mlw7wp")))

(define-public crate-array-macro-2.1.0 (c (n "array-macro") (v "2.1.0") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0wac5di1npwm23fgldpn2ml76j55zb5ynpjfv6hi3401h3qwh22a") (y #t)))

(define-public crate-array-macro-2.1.1 (c (n "array-macro") (v "2.1.1") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1cj2mys4am91gshphmdzg3618v40pbl13lyq5fpwp6ww8xv034g3") (y #t)))

(define-public crate-array-macro-2.1.2 (c (n "array-macro") (v "2.1.2") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0dggpz0z4j7la48qqx90fw99dcljflzr7g1wm9pjclyq8b3pp40i") (y #t)))

(define-public crate-array-macro-2.1.3 (c (n "array-macro") (v "2.1.3") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "18yqz8vfw4267vajm3smya8y254lc0dcarxc77jif1242ajcbp6p")))

(define-public crate-array-macro-2.1.4 (c (n "array-macro") (v "2.1.4") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0by1fy9fswwawi896wh9l5yhqzcar890rlfj43hzajdas5s0rkmj") (r "1.56")))

(define-public crate-array-macro-2.1.5 (c (n "array-macro") (v "2.1.5") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1x3wya9qxhmyv90r8fmq4c2x5047lyl00564sxrhphqnczx62xj5") (r "1.56")))

(define-public crate-array-macro-2.1.6 (c (n "array-macro") (v "2.1.6") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0hk2kjkvpqwi6x67wr46f4fbscnvphdmj6f9byikgqrjs0hy0xmw") (r "1.56")))

(define-public crate-array-macro-2.1.7 (c (n "array-macro") (v "2.1.7") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0ismw7vcf8d2g8kha051yxbbzxwps5hk8v8f4a1b7y1n3r79078c") (r "1.56")))

(define-public crate-array-macro-2.1.8 (c (n "array-macro") (v "2.1.8") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "169wzpzzxh71jpdip2a7mbika77zzr6ykb0f3pjfyrmli9hjq2i2") (r "1.56")))

