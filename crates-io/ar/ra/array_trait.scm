(define-module (crates-io ar ra array_trait) #:use-module (crates-io))

(define-public crate-array_trait-0.1.0 (c (n "array_trait") (v "0.1.0") (h "10s0shllqcp2nzwq039iwqa6my9bw9afhf5qa6w0lcya90akp8s1")))

(define-public crate-array_trait-0.2.0 (c (n "array_trait") (v "0.2.0") (h "029j7j9bwq92i41n65n3xrnnpm3dmr5sa6m8b28v2gpcnn8c0h5s")))

(define-public crate-array_trait-0.2.1 (c (n "array_trait") (v "0.2.1") (h "0cbcn9ik2rbmclp2njjkz5n7gz0f71iigagdbfj8q33zjxv7jh8n")))

(define-public crate-array_trait-0.3.0 (c (n "array_trait") (v "0.3.0") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1kss6rjh3ydmd1f1vqzhl1mpswy5m9d5lmgmmfpqs8s5gq784m8i")))

(define-public crate-array_trait-0.3.1 (c (n "array_trait") (v "0.3.1") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0kg129iqhni4mj8c6bzrpcmc6xqrlcqjlccghmk8p2zmah3y3yhh")))

(define-public crate-array_trait-0.3.2 (c (n "array_trait") (v "0.3.2") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1gqzq4apd9qzdb5lpmzc2sx4yq8nf54pzrl7sncyzlp7gnzq74ja")))

(define-public crate-array_trait-0.3.3 (c (n "array_trait") (v "0.3.3") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "15pap9dc1jl8lvxr0m4hnlfbmvw5kr876k55gp5v1i9b484wjl0m")))

(define-public crate-array_trait-0.3.4 (c (n "array_trait") (v "0.3.4") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "17s858liv395q4w7yibk1adjia38k6dxvqdgxd416ip0fw4m2kfi")))

(define-public crate-array_trait-0.3.5 (c (n "array_trait") (v "0.3.5") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0k4fdwagv9a7w23pg67q7ddpf5zri4fdq4jszb3m62ganjh1b1nf")))

(define-public crate-array_trait-0.3.6 (c (n "array_trait") (v "0.3.6") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0xbhqyqakhhm190rsrl56f7q7bbiwvg2nfwrd0lm6br138w9qmkx")))

(define-public crate-array_trait-0.3.7 (c (n "array_trait") (v "0.3.7") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1vva9vvj0jh2f2m4s178dxdabhaz5i9li5mg2riigaj6d8l1967m")))

(define-public crate-array_trait-0.3.8 (c (n "array_trait") (v "0.3.8") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0y6syvvknb3a0d0ayashiwzzp7l3vd1n4qvn4biys30sij6kimyr")))

(define-public crate-array_trait-0.4.0 (c (n "array_trait") (v "0.4.0") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1bfkz8ykdjn5pa4l50la1a5n80kcvpkl4iizqq1d6xcrc7izxzkx")))

(define-public crate-array_trait-0.4.1 (c (n "array_trait") (v "0.4.1") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0139yrdh14dff14s5ir9hh0arrkgrfylarajkaik4yfm10hal8wn")))

(define-public crate-array_trait-0.4.2 (c (n "array_trait") (v "0.4.2") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "0hp45p08dvs5vscgwc261xivxc8yg3kswppx39y1d5gqj7zixsy5")))

(define-public crate-array_trait-0.4.3 (c (n "array_trait") (v "0.4.3") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1s38q88af4qs4r0siivqlsavjybzwkfwwxl4yw3pizhzl546a96x")))

(define-public crate-array_trait-0.4.4 (c (n "array_trait") (v "0.4.4") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1r0vlv9s93i9rqzcc62wq0pjdckn6rvpym48pszs4wjjvjpqivgq")))

(define-public crate-array_trait-0.4.5 (c (n "array_trait") (v "0.4.5") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1bf82ydvia1545bp68xv9dy33lyydmps32msabjg3gv9ilhnrk7s")))

(define-public crate-array_trait-0.4.6 (c (n "array_trait") (v "0.4.6") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "13rf1hqxqk0fgdchdh2npb728qkyjz15x81rq08niz0kagcwi4a5")))

(define-public crate-array_trait-0.5.1 (c (n "array_trait") (v "0.5.1") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1aydqxa0g3yvi3zd05dq9d390b0rgz13iccnqznfyl6125d17kiy")))

(define-public crate-array_trait-0.5.2 (c (n "array_trait") (v "0.5.2") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "17w9hp32rigilj7xqa75fh21fd498ifaf839ypg4y58dxilpd9rl")))

(define-public crate-array_trait-0.5.3 (c (n "array_trait") (v "0.5.3") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1byxjbs7iwz1am8ln74r6lgcijirvwm8l0byif08rj9gak1d1xp9")))

(define-public crate-array_trait-0.5.4 (c (n "array_trait") (v "0.5.4") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "09cq43fqg1jpfhml781069chlynxnjdmds16h02pk1cwb1x9jxzn")))

(define-public crate-array_trait-0.5.5 (c (n "array_trait") (v "0.5.5") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "10xr2mmi2j1yvdc1ypqz8f9rw4kcarkhch5cmz2silbvn7d24205")))

(define-public crate-array_trait-0.5.6 (c (n "array_trait") (v "0.5.6") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0g79yn7zm43js9l58lrjr9cga7cmsx678kkgj4blb4s83pqafrzg")))

(define-public crate-array_trait-0.5.7 (c (n "array_trait") (v "0.5.7") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1lh8sn429k992ipy5b3f3vrq4gxbhiy39c8rqfqhncn5hg062fym")))

(define-public crate-array_trait-0.5.8 (c (n "array_trait") (v "0.5.8") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0kwp5zb04dfcr12qap1hvssjaw67fskbwvafx49kkhimcyhdfpd3")))

(define-public crate-array_trait-0.5.9 (c (n "array_trait") (v "0.5.9") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "02s0hbign3g3rqn8j0gih8w2vlf976zf1l0z6dk2a656bfp5p81p")))

(define-public crate-array_trait-0.5.10 (c (n "array_trait") (v "0.5.10") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1indyg2g9jhkcgr1778lcwsv4c8gwvsgs0723q5qrhahdvpngvgv")))

(define-public crate-array_trait-0.5.11 (c (n "array_trait") (v "0.5.11") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "03ks5m72cgnqi05lah0la5wf9b285n1p478bm5c1h34cc9rhni6x")))

(define-public crate-array_trait-0.5.12 (c (n "array_trait") (v "0.5.12") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0i9pkii4cs8jbn9f8b6chfpz79kk41hhi15kbhsh94rk9mdlx74y")))

(define-public crate-array_trait-0.5.13 (c (n "array_trait") (v "0.5.13") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "05dl2di7zgzxx68irfbsyd27v4w4016lraqgzj8iby19jb9crqav")))

(define-public crate-array_trait-0.5.14 (c (n "array_trait") (v "0.5.14") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0nvxn79izkdblnlw5gb9hnaj9w2yxyhrf3lilzmdmjmca0i75yl0")))

(define-public crate-array_trait-0.5.15 (c (n "array_trait") (v "0.5.15") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1ldd50snwv9da95lsv7fi4p6hcaaxzmgxr6wzbyq4din5naxlrmd")))

(define-public crate-array_trait-0.5.16 (c (n "array_trait") (v "0.5.16") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0sf9mhrn9qs7qj3p2jrryyii420rq2rlq4ifjkjjxjw2kpfnlclx")))

(define-public crate-array_trait-0.5.17 (c (n "array_trait") (v "0.5.17") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0lmkf1hy9fiallp78q0g83534sqadh4pl6jcig0xbiphic8r725x")))

(define-public crate-array_trait-0.5.18 (c (n "array_trait") (v "0.5.18") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1hn3rxfsnq243w65xhayp6dwdaq218597al5cvb95qh4zznh4gs9")))

(define-public crate-array_trait-0.5.19 (c (n "array_trait") (v "0.5.19") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0da8v1nha0qh9k3rdivs6hlxb9hdacbaidf2vzxvx0i3c36ram22")))

(define-public crate-array_trait-0.5.20 (c (n "array_trait") (v "0.5.20") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0syqpzka29zvmjjls0f2n0a0yd60vavcg9jb619v5zvgv6rg7s70")))

(define-public crate-array_trait-0.5.21 (c (n "array_trait") (v "0.5.21") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1311ic3p8f5w98sa9x4szicd6jx7fc1n1gk2sr0vl13y7mscndjj")))

(define-public crate-array_trait-0.6.0 (c (n "array_trait") (v "0.6.0") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0vc4m2kz5dppj0mzmxjiayc4s32izwsw977w12zgqxj1rz5dsxyy")))

(define-public crate-array_trait-0.6.1 (c (n "array_trait") (v "0.6.1") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0nwbajkip73w3gsadwj7xvs4gsmz61yiqsipcmgm5a2cp69ifh2n")))

(define-public crate-array_trait-0.6.2 (c (n "array_trait") (v "0.6.2") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "02rkhxhdydcgcr5m8p7kvsz3g1k9z51xza0x9lc40d0fmbwlzk03")))

(define-public crate-array_trait-0.6.3 (c (n "array_trait") (v "0.6.3") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "03msyzxln1q5rw3bipx0fi87ccgmdi5kkj91znplnkk5973s6rp5")))

(define-public crate-array_trait-0.6.4 (c (n "array_trait") (v "0.6.4") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0lqrj5lxyvl7brycvib5wq3r0cpn2dawwfp2ygjq64ffhxnk4074")))

(define-public crate-array_trait-0.6.5 (c (n "array_trait") (v "0.6.5") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "06g75aabjvy4a5h1kvmyirnrfzlxgps5xsjv5mwilx2941n9bcyk")))

(define-public crate-array_trait-0.6.6 (c (n "array_trait") (v "0.6.6") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1fm8rcn32925918kcvhwfx1vr1xy5prk022x0rg0x102jx7b9p5k")))

(define-public crate-array_trait-0.6.7 (c (n "array_trait") (v "0.6.7") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "110jr6pl6xa6l2ji0prfl9saph8qs8gq0mndx98r5jjzpdspfh4f")))

(define-public crate-array_trait-0.6.8 (c (n "array_trait") (v "0.6.8") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "017xazpf63kvf7q5qx6b1hy9qmv8s8xi1scc35mh5z9d8bvg58n3")))

(define-public crate-array_trait-0.6.9 (c (n "array_trait") (v "0.6.9") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "16fgwjyg7jgmx91pab47xa1p3cv221nc1mkap4qn74c5lil524b3")))

(define-public crate-array_trait-0.6.10 (c (n "array_trait") (v "0.6.10") (d (list (d (n "float_approx_math") (r "^0.1.0") (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1hdyv32bwrxwdf9yh69virhx7pjjyiwmjw2fxcj27jyznsdy4cc6")))

(define-public crate-array_trait-0.6.11 (c (n "array_trait") (v "0.6.11") (d (list (d (n "float_approx_math") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0aayan0x4zglywvil4sa1c6mljxl4mdapckc1y6vi1wlzlqv228n") (s 2) (e (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6.12 (c (n "array_trait") (v "0.6.12") (d (list (d (n "float_approx_math") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0ysfiffhdcd35bpq6gbzx6hx5d23209n2rvzqicfg7v7kqlxmrc0") (s 2) (e (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6.13 (c (n "array_trait") (v "0.6.13") (d (list (d (n "float_approx_math") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1zifc8nhnmx1bghmqq6k8l31sqvs70wgd0yxqklj2cvflix3ssgq") (s 2) (e (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6.14 (c (n "array_trait") (v "0.6.14") (d (list (d (n "float_approx_math") (r "^0.1.2") (f (quote ("sqrt"))) (o #t) (d #t) (k 0)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "121lxja8w9fy04v7xk5md1cc9xn8j1p6pdx68a5bp562y8nj2b1h") (s 2) (e (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6.15 (c (n "array_trait") (v "0.6.15") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0ghzn39360xyby11ka469sndaadfkdjm4fvys1r2kis5v3gbj1x0")))

(define-public crate-array_trait-0.6.16 (c (n "array_trait") (v "0.6.16") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "1zcsl5m2rq74bnn5kc6b6c40prw0viyzjcbly5agm1ig0sakn8vq")))

(define-public crate-array_trait-0.6.17 (c (n "array_trait") (v "0.6.17") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0nk83yblm1x56y6pqj24c0vf0p9ryypbycmfpj88lijnfyls6rjg")))

(define-public crate-array_trait-0.6.18 (c (n "array_trait") (v "0.6.18") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)))) (h "0qg3axymc5maw78c5iplkd3cizbmw72y3mr0bin913ass8n6a49c")))

(define-public crate-array_trait-1.0.0 (c (n "array_trait") (v "1.0.0") (d (list (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "slice_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1ybqbpvplbkfymrf7ybi7x0dz95i71d5a2z5bikd7z3yfxmbk6xz")))

