(define-module (crates-io ar ra array-bytes) #:use-module (crates-io))

(define-public crate-array-bytes-0.1.0 (c (n "array-bytes") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "1v33dvhsdhg3mrziw3whwxccg63gxxprhrwr8vy0p63l1482rn3k")))

(define-public crate-array-bytes-0.2.0 (c (n "array-bytes") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0dq605ww6a7zi34w40mpp0nvb1vzxrqw7226kxklfa1l8rsla6vw")))

(define-public crate-array-bytes-0.3.0 (c (n "array-bytes") (v "0.3.0") (h "1pjikb8d0djc43s3c7ylns4ah0sfcx4qm95plrcpd8r9ril9yxa3")))

(define-public crate-array-bytes-1.0.0 (c (n "array-bytes") (v "1.0.0") (h "1amjyz3m839i61cyb4wi76kdby3m6s7ma3bkbipdfbhjmnf1kpx8")))

(define-public crate-array-bytes-1.1.0 (c (n "array-bytes") (v "1.1.0") (h "1dfmm6qaglx6i7xppi73bzv16aj7dcyh89s9w01ckjyfjlas22fp")))

(define-public crate-array-bytes-1.2.0 (c (n "array-bytes") (v "1.2.0") (h "1x5aflm92zjm8s785s4h302pvc0f7cww41sxlkkzwi1ywl1h9vfr")))

(define-public crate-array-bytes-1.3.0 (c (n "array-bytes") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "19nqh652jqq821rhc7mhc0whylzlgj186nbbvpgpjv3pdij1wvc0")))

(define-public crate-array-bytes-1.3.1 (c (n "array-bytes") (v "1.3.1") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "0yzbwzc9fxdg5dgnvhkimi7m56vk0icr1sly1c6qhsz44bym82v8")))

(define-public crate-array-bytes-1.3.2 (c (n "array-bytes") (v "1.3.2") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "12bhf8y4f1jhfgc2psqv1fva8v5fvw7iwymahhwfb7rmdr0ghr83")))

(define-public crate-array-bytes-1.3.3 (c (n "array-bytes") (v "1.3.3") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "0xi7r8bmhlc6vhbxnvx078ghxcv4l6bfrin5pn5jw0dhnan44rdp")))

(define-public crate-array-bytes-1.4.0 (c (n "array-bytes") (v "1.4.0") (d (list (d (n "serde") (r "^1.0.130") (o #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "036jpqzaxr83lnc04ml117k1ilv301pqy7cflcf4npd1amxjkyy8")))

(define-public crate-array-bytes-1.4.1 (c (n "array-bytes") (v "1.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "132bxyrkrqqcak08d4yz7d14gx7b42jf8k889zxz44swnjp89lls")))

(define-public crate-array-bytes-1.5.0 (c (n "array-bytes") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hrw94c9n9a3wqhwamw65gk3cmkpr8j3yi5dyn1zsq34738hg2yz")))

(define-public crate-array-bytes-1.5.1 (c (n "array-bytes") (v "1.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wyrg2zbrh0njv07g7vj4s82qz00fk4rrl5fz3ik0n240p6a654s")))

(define-public crate-array-bytes-1.5.2 (c (n "array-bytes") (v "1.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1avv52dfipgn11r966jg58yn3rgprcqzf2ahbn9a1ad47y6wg2xl")))

(define-public crate-array-bytes-1.6.0 (c (n "array-bytes") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m38j84ywvfrdiklp3d1yj0aj5jcb873jiciqlxdvg7yxgy11cn5")))

(define-public crate-array-bytes-2.0.0 (c (n "array-bytes") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ly8fh4ml0693nc02mimh63f23yzkyvz9lmcbllhj83l167zw78q") (y #t)))

(define-public crate-array-bytes-2.0.1 (c (n "array-bytes") (v "2.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vgvj7lcckd82mj2w72524z7dqwlldss3r97p5qva4gk3lh5swih")))

(define-public crate-array-bytes-2.0.2 (c (n "array-bytes") (v "2.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00h3z1dfh8axqblxbffxy4ifsrljihq4y424mli4nnrawd6a5xxp")))

(define-public crate-array-bytes-3.0.0 (c (n "array-bytes") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "158ysj629w43b349kpw5kbkx4y5c5kr95gxrw0x7wdbxaxspikfw")))

(define-public crate-array-bytes-4.0.0 (c (n "array-bytes") (v "4.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kl2hsg7ia44y7llk4y5v7hjmw1hv7493npxr9r5l4xxhnmnsh9b")))

(define-public crate-array-bytes-4.1.0 (c (n "array-bytes") (v "4.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j89jm87pz62r0m7x99ip9wfmswh1psra9q78nvyc8n9n0rkd4ba")))

(define-public crate-array-bytes-4.2.0 (c (n "array-bytes") (v "4.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1imnmw7cp9xj9il63w1klmhqj94qdavwiajynfj1csiiq72n6bzm")))

(define-public crate-array-bytes-5.0.0 (c (n "array-bytes") (v "5.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1af536fx5f395s2hhllb3mfay0lqzlrvbfl8lq52bf08z7ir1qyk")))

(define-public crate-array-bytes-5.1.0 (c (n "array-bytes") (v "5.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vyhzyhrm41i9w4iqadqzsfn8ygvjp92wfdf8h7am1ly0mk2s8c5")))

(define-public crate-array-bytes-6.0.0 (c (n "array-bytes") (v "6.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06fd0c02i6dh7w6dddrb566d523v34h0nizag03whjxcdyfjxxr2")))

(define-public crate-array-bytes-6.1.0 (c (n "array-bytes") (v "6.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "faster-hex") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z80q9q12brg68xinzxycsd4xfzibjmr9gfzv2msac7ch6jcbcfr")))

(define-public crate-array-bytes-6.2.0 (c (n "array-bytes") (v "6.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "faster-hex") (r "^0.8") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b1q62v2bxazfna7zwm845hmvffswbj4v9x1k76cbn2ajccsj5yy")))

(define-public crate-array-bytes-6.2.1 (c (n "array-bytes") (v "6.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "faster-hex") (r "^0.9") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11na5k0qq2zrqsbwj5fa8x6glkk3zy3j0jzinx0nrzir596qv4if")))

(define-public crate-array-bytes-6.2.2 (c (n "array-bytes") (v "6.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "faster-hex") (r "^0.9") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q4s8mz6a29jf0bnwil4spjrqnvdrvjjc354gvhwbksv36vhz13g")))

(define-public crate-array-bytes-6.2.3 (c (n "array-bytes") (v "6.2.3") (d (list (d (n "const-hex") (r "^1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "faster-hex") (r "^0.9") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14w23czh8s1b5ic1vfczymf674jn1jwxkcmv0blijhfk3c3dwpax")))

