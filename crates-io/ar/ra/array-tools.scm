(define-module (crates-io ar ra array-tools) #:use-module (crates-io))

(define-public crate-array-tools-0.2.4 (c (n "array-tools") (v "0.2.4") (h "1j17kdvf4apwzkbw8bsdn7v3syjr0pc5d28dgd7llac395sb7bix")))

(define-public crate-array-tools-0.2.5 (c (n "array-tools") (v "0.2.5") (h "0y4jr3rwbszb5bzrgywvgbizzix5m7sbp2gk8bgwbld67jmy0adb")))

(define-public crate-array-tools-0.2.6 (c (n "array-tools") (v "0.2.6") (h "13hc2jqz8gdbypw7ji10a1x0jm52c210pj431wp1kifzpcyjqbmz")))

(define-public crate-array-tools-0.2.7 (c (n "array-tools") (v "0.2.7") (h "0j1gw0inyx8pv4r7jahqdq6pqpknb7xv5g7273c7c21kshdvbfnq")))

(define-public crate-array-tools-0.2.8 (c (n "array-tools") (v "0.2.8") (h "1gacfy7sj4x73kb626jp3qc5805j4nsmhrzph5p2hvlwxwwbliyz")))

(define-public crate-array-tools-0.2.9 (c (n "array-tools") (v "0.2.9") (h "17mgybf8wjbq8adr97qcnnm1qm1sqw36ds4jkqyx7h8mk3ppwcfr")))

(define-public crate-array-tools-0.2.10 (c (n "array-tools") (v "0.2.10") (h "1v9i7dlz8304qhcsziifhdb1rdh56j1xialvkighr5xkls3vhgf6")))

(define-public crate-array-tools-0.3.1 (c (n "array-tools") (v "0.3.1") (h "01shhgzj2wpg598pns6py0092ys4775g45clrdg3ip868aklv35g") (y #t)))

(define-public crate-array-tools-0.3.2 (c (n "array-tools") (v "0.3.2") (h "1dpldba2j03hlr6gaj9k01nfvzlxv71i9pzjbw5p5jr36cf3nq25")))

