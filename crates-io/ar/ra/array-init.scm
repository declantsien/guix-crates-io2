(define-module (crates-io ar ra array-init) #:use-module (crates-io))

(define-public crate-array-init-0.0.1 (c (n "array-init") (v "0.0.1") (d (list (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)))) (h "121ys9l8hyy6x9ylw0m3adfxrs32cj8547jnfjxqca42y4jrhy14") (f (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.2 (c (n "array-init") (v "0.0.2") (d (list (d (n "nodrop") (r "^0.1.9") (d #t) (k 0)))) (h "0salmmba3wfz0l4270kpgflh83xrkcnbapqk1asr4s116n6m0fsd") (f (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.3 (c (n "array-init") (v "0.0.3") (d (list (d (n "nodrop") (r "^0.1.12") (k 0)))) (h "1wszqzaxwcfxcp5vz929i71lh5b079l89qsrdz3si0dfs1b89k63") (f (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.4 (c (n "array-init") (v "0.0.4") (d (list (d (n "nodrop") (r "^0.1.12") (k 0)))) (h "0wndg0qry887r61bk898irz61316a13q6y0j1wx0sikbhv5rwn13") (f (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.1.0 (c (n "array-init") (v "0.1.0") (h "0d3n40d0mhwq76brh2gy0k6x0c1l3d9j06rdbyzxmgkz5lsz0cd2")))

(define-public crate-array-init-0.1.1 (c (n "array-init") (v "0.1.1") (h "1nv5qxchqrsgxaxrbx8mkyxdx9g4j68iyylcpmapy49xbqpvw2zk")))

(define-public crate-array-init-1.0.0 (c (n "array-init") (v "1.0.0") (h "15fcfwi3c40pb2lyaizr26dvsfx4pnl5mrcily9ihsdnclyrh755") (f (quote (("const-generics"))))))

(define-public crate-array-init-1.1.0 (c (n "array-init") (v "1.1.0") (h "09rxkf6ghawzjf4vdz561x9irqjy3rd9wzinkmddhf9x905r3v00") (y #t)))

(define-public crate-array-init-2.0.0 (c (n "array-init") (v "2.0.0") (h "1ilfd4hja0i8gjm4n2p29ydcsaicin3w54750bkcavqp49acqib9")))

(define-public crate-array-init-2.0.1 (c (n "array-init") (v "2.0.1") (h "15pmzssi4nxkqdb65pjmfgj0wc60imffwvj9qw4af8nw0l8dgdmz")))

(define-public crate-array-init-2.1.0 (c (n "array-init") (v "2.1.0") (h "1z0bh6grrkxlbknq3xyipp42rasngi806y92fiddyb2n99lvfqix")))

