(define-module (crates-io ar ra array-section) #:use-module (crates-io))

(define-public crate-array-section-0.1.0 (c (n "array-section") (v "0.1.0") (h "0cccwb68lishmh4q9c7rc9fi2drjv0bhbi1lpbwp0rl8bh3c37sd") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1.1 (c (n "array-section") (v "0.1.1") (h "00hq18wraj0hyqc7mp5bgqzq93fbn0fylny45f4vf1m2xl34wjvp") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1.2 (c (n "array-section") (v "0.1.2") (h "1c16grca66wzmgh3wh37x4graffc010n1gyd7ry581xwiahmi1rz") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1.3 (c (n "array-section") (v "0.1.3") (h "05hccrv8j7vxkbhvvlspa8vs60v8n137p8nqxjkypdhzbjryliy3") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1.4 (c (n "array-section") (v "0.1.4") (h "1m18kpxiqb4h3i09lmqq1wgp62n62wx60acyx50bny5mqc5b21jy") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1.5 (c (n "array-section") (v "0.1.5") (h "1cqan7grjw6g9s14w8rh983y1sbcvziaa017cdfibcanb12ajg8q") (f (quote (("std" "alloc") ("alloc"))))))

