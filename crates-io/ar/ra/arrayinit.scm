(define-module (crates-io ar ra arrayinit) #:use-module (crates-io))

(define-public crate-arrayinit-0.1.0 (c (n "arrayinit") (v "0.1.0") (h "0pwhjh9qzj673hngchm2p6j7qw2k4w2lilwpmg9y38fjqaxc729l")))

(define-public crate-arrayinit-0.1.1 (c (n "arrayinit") (v "0.1.1") (h "0cfr3pyqfyk52967dqcmqadb3vpkv53cncp01jm22b02ak3948jd")))

