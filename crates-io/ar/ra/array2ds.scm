(define-module (crates-io ar ra array2ds) #:use-module (crates-io))

(define-public crate-array2ds-0.1.0 (c (n "array2ds") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lci1cq1gw89crb78r9shbdz7ja2s60g006lnmdaym7b83g0ha3c")))

(define-public crate-array2ds-0.1.1 (c (n "array2ds") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qwbqs5gx08ps44pbhcmz9xqk64zhgvdi67ayjsq0pgq0zimasd8")))

(define-public crate-array2ds-0.1.2 (c (n "array2ds") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lxqljl2jl509x9h6s2gbfr69s3w23vrsx9d65zm8wa01f9zphhj")))

(define-public crate-array2ds-0.1.3 (c (n "array2ds") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "14a1rcgi2hg4qlmgky4dkqj36xl8rvc4c6vj6647x2arlnb118w8")))

(define-public crate-array2ds-0.1.4 (c (n "array2ds") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jbnr32cavjfwjy39zn58iicdyc5aq6pzqckz706700drc8scxwq")))

(define-public crate-array2ds-0.2.0 (c (n "array2ds") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lfxcvpxkqlhwrmll1j94x88k0vkl32hhz1v401gzvd63r0f2mqg")))

(define-public crate-array2ds-0.2.1 (c (n "array2ds") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mz3r2gql163cp0d75dgaqiwi7mpdkmzmk72qii40hl95d08d89m")))

