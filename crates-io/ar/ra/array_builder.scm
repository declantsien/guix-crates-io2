(define-module (crates-io ar ra array_builder) #:use-module (crates-io))

(define-public crate-array_builder-0.1.0 (c (n "array_builder") (v "0.1.0") (h "0bzqjxlgmcciil81rkvjb1g35vq0ps3f0r06ziaj7djpv74ch9pg")))

(define-public crate-array_builder-0.1.1 (c (n "array_builder") (v "0.1.1") (h "0wr53l5d8x26cf82d50i5fdh9sipgr89h0hqc2790q7mv6kq0a17")))

(define-public crate-array_builder-0.1.2 (c (n "array_builder") (v "0.1.2") (h "1k0is4g1jf775a9hsp15m65xgm06yjq6qmw1i6g6y5l6p8648121")))

(define-public crate-array_builder-0.1.3 (c (n "array_builder") (v "0.1.3") (h "0gr2xhgxxqkzydz8xs81m8v8irxpx97avk8dn2wphqm729yqpr88")))

(define-public crate-array_builder-0.1.4 (c (n "array_builder") (v "0.1.4") (h "1j3wb6d0jng4llsviixcxqfax23ff2n84n2s7s0x3i5k96a8id21")))

