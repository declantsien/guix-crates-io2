(define-module (crates-io ar ra array-matrix) #:use-module (crates-io))

(define-public crate-array-matrix-0.1.0 (c (n "array-matrix") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "19wgzrgqlh35ggsk7vk09p6j6rffhmza01ws7ii8v0d0v8n9nqqh")))

(define-public crate-array-matrix-0.1.1 (c (n "array-matrix") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1figl75skabcc6bwxgj2zxn475zqkzf7593lr84lzk1d1qkamzhb")))

(define-public crate-array-matrix-0.1.2 (c (n "array-matrix") (v "0.1.2") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "19ny52fp7badivx8nddnaacvc4qsf735fhfvqmr8bjg48pgiv6bn")))

(define-public crate-array-matrix-0.1.3 (c (n "array-matrix") (v "0.1.3") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0h8vvd688rlbzkd652rahwf4wkhmsswach713y9ypmzqasfy6vi2")))

(define-public crate-array-matrix-0.1.4 (c (n "array-matrix") (v "0.1.4") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1jxcpmn59aqggwxrkhjhfvwbrcampf32r3vgfajz0pdlw0alrf5c")))

