(define-module (crates-io ar ra array_iter_tools) #:use-module (crates-io))

(define-public crate-array_iter_tools-0.1.0 (c (n "array_iter_tools") (v "0.1.0") (d (list (d (n "array_builder") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1kj7q9i1i3d72a6crrpqhdm1q1hj028ippghi7q874x2wly9slk4") (f (quote (("drop" "array_builder") ("default" "drop"))))))

(define-public crate-array_iter_tools-0.1.1 (c (n "array_iter_tools") (v "0.1.1") (d (list (d (n "array_builder") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0a1b7axnjfxjifbpx5yvcify98n1jd9id9vmwlrgqwmf6s8ddgkc") (f (quote (("drop" "array_builder") ("default" "drop"))))))

(define-public crate-array_iter_tools-0.2.0 (c (n "array_iter_tools") (v "0.2.0") (d (list (d (n "array_builder") (r "^0.1.4") (d #t) (k 0)))) (h "07hrgcb4ixrrfb75d6mxxm5iqb7d9wgbzwav0zcy6khgac7l19pc")))

