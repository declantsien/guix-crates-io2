(define-module (crates-io ar ra array_ext) #:use-module (crates-io))

(define-public crate-array_ext-0.1.0 (c (n "array_ext") (v "0.1.0") (h "10ayrpbpcm6wfwa6sl14fr74454h0hk8g86yw3y73nmi1pa7w41q")))

(define-public crate-array_ext-0.2.0 (c (n "array_ext") (v "0.2.0") (h "1p9d2ljp3micj0qyx46gimjnmlif7ih4vaq7x5wdy7g7n17817zw")))

(define-public crate-array_ext-0.3.0 (c (n "array_ext") (v "0.3.0") (d (list (d (n "seq-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0q2bmhjxwv5m3rmf64rv6x6cx6p1lw5xqg5sbq66mwyi99dm4aag")))

(define-public crate-array_ext-0.4.0 (c (n "array_ext") (v "0.4.0") (h "179yy5iyhzvzpy1mh1agv7k86r9rphq5gmzmi4rrrgr7ipv5pzbf") (f (quote (("nightly"))))))

