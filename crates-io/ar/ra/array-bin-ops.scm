(define-module (crates-io ar ra array-bin-ops) #:use-module (crates-io))

(define-public crate-array-bin-ops-0.1.0 (c (n "array-bin-ops") (v "0.1.0") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "1najn97c6vjplrmsjzsn2grzyf30n4qj5akkjvwzbk4pqb0wl1m8") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-array-bin-ops-0.1.1 (c (n "array-bin-ops") (v "0.1.1") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "06vmrl5axfxmgjlri1j6ayd4dkpqzl7w3cwqibc6phjfpbgg7j01") (f (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1.2 (c (n "array-bin-ops") (v "0.1.2") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "0i5n2yd9s6kgbapygcvx84sq1a4dnvgikm79d2wxjfzkmwci9d8z") (f (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1.3 (c (n "array-bin-ops") (v "0.1.3") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "048bl2apm1nl2641bs0vbad7aaq2ib76pxx4mdh5fy7ydi3438xs") (f (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1.4 (c (n "array-bin-ops") (v "0.1.4") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "1cmlkximkravijp0873mbm4fs27dv2l71q7k4dnaylmr2lx1il22") (f (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1.5 (c (n "array-bin-ops") (v "0.1.5") (d (list (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "1y3y4s9yx8gzgb6jnxgawpk47v14pxkg3wbgz8f0xivmm79kn97i") (f (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1.6 (c (n "array-bin-ops") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mockalloc") (r "^0.1.2") (d #t) (k 2)))) (h "1xbvqr2ya3mfqwm1x689yipb09lcx8mqwz0cckq90vmi9msp0q1x") (f (quote (("std") ("default" "std"))))))

