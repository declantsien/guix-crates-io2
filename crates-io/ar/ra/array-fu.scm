(define-module (crates-io ar ra array-fu) #:use-module (crates-io))

(define-public crate-array-fu-0.0.0 (c (n "array-fu") (v "0.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1z9l9vycsdrssfws7wcq9y40nnkashk2lz95fqmbnfpwfariipvz") (y #t)))

(define-public crate-array-fu-0.0.1-alpha (c (n "array-fu") (v "0.0.1-alpha") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lqyc99mmnrlcrgrkfklg873v0wpqhym5vnrl2xg2papp2swmiqy")))

