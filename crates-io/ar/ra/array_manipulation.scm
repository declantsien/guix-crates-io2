(define-module (crates-io ar ra array_manipulation) #:use-module (crates-io))

(define-public crate-array_manipulation-0.1.0 (c (n "array_manipulation") (v "0.1.0") (h "19gbikr9l6zdhshq1n4w4i7k00jg0gjsax5wgdbzf45mkxqx2nmr")))

(define-public crate-array_manipulation-0.2.0 (c (n "array_manipulation") (v "0.2.0") (h "12ls238f3jyba9jbgw08ydxvsycy2lcn864b4iar3gj6v3yq6qcv")))

(define-public crate-array_manipulation-0.3.0 (c (n "array_manipulation") (v "0.3.0") (h "1qmkwq45qg7pjadr3pzxzfnav490fy63g550wyf6bdwfk5yhvl44")))

(define-public crate-array_manipulation-0.4.0 (c (n "array_manipulation") (v "0.4.0") (h "01hvwv7ga1j74lay27r00ivp2jd7mgyfbhw1l9nib2zsp88nvbvk")))

(define-public crate-array_manipulation-0.4.1 (c (n "array_manipulation") (v "0.4.1") (h "0pyvdsj88qk4z35ivlmnpm5m1768qfahm79r77pdgfdni1l7zr99")))

