(define-module (crates-io ar ra arrayvec) #:use-module (crates-io))

(define-public crate-arrayvec-0.1.0 (c (n "arrayvec") (v "0.1.0") (h "104nbdkfyipwhx1ncjqphv9n7cw27kmqdn5q6wqmlx0wy98izzzp")))

(define-public crate-arrayvec-0.1.1 (c (n "arrayvec") (v "0.1.1") (h "109va9a7k1kf97fk02l0sd3pxxmcavs4cxrg2993qkq7pq2byk7j")))

(define-public crate-arrayvec-0.1.2 (c (n "arrayvec") (v "0.1.2") (h "0rxapl0jf35kfw4023wszpjarnvlyx9jkvzvn19w1lx4dzhwfzvg")))

(define-public crate-arrayvec-0.2.0 (c (n "arrayvec") (v "0.2.0") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "053a0vlz7ca1p88mpdp35jddhylm8bqxni3qpj4dm3jqn87m2lbh")))

(define-public crate-arrayvec-0.3.0 (c (n "arrayvec") (v "0.3.0") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "1adzqxwsjp2zaf095x9n7flnm7kndk1daz6cvdb0947v0lbdgg5i")))

(define-public crate-arrayvec-0.3.1 (c (n "arrayvec") (v "0.3.1") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "0m1avcjhw8c0zyj9lhj9c3jixrlwij4lnbmpk3zzh79a17922qhj")))

(define-public crate-arrayvec-0.3.2 (c (n "arrayvec") (v "0.3.2") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "09jw4dsjxjq82vf0lzdqhcvxx6fj10yqygilfkmfb111b58lwmq5")))

(define-public crate-arrayvec-0.3.3 (c (n "arrayvec") (v "0.3.3") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "1q92xvqyf2spfp5wilw3x96scahgpk5nl7bv4kjvjjjhk44s8ris")))

(define-public crate-arrayvec-0.3.4 (c (n "arrayvec") (v "0.3.4") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "0hfppqxi7wa2ak16byy93fbgcbdss3fcp57hh29a1hwv19k2vf0v")))

(define-public crate-arrayvec-0.3.5 (c (n "arrayvec") (v "0.3.5") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "odds") (r "^0.1") (d #t) (k 0)))) (h "03al12cs1h5ppl3m9iapri7q7svgk3dv7q5yfgmdslpda8ql5way")))

(define-public crate-arrayvec-0.3.6 (c (n "arrayvec") (v "0.3.6") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "1n7f5rjncy50nn8y8fmryw8aml2wa2zrwfp72hq2wbr2jcrhcnis")))

(define-public crate-arrayvec-0.3.7 (c (n "arrayvec") (v "0.3.7") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "04b9gppg625lx753r6xrxd9vyjmd3p7gfmwnpk47a2p86gadblhj")))

(define-public crate-arrayvec-0.3.8 (c (n "arrayvec") (v "0.3.8") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "099czprg3vj21f041n3gqsbp4vzzqbrkmm8pd4iigzv6gmdqfdva")))

(define-public crate-arrayvec-0.3.9 (c (n "arrayvec") (v "0.3.9") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 2)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "1gd07z35b34bn14ln75661hlcxb198743j2smm350ylhibcw5np1")))

(define-public crate-arrayvec-0.3.10 (c (n "arrayvec") (v "0.3.10") (d (list (d (n "nodrop") (r "^0.1.4") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "0adqk0p1y4jzlg84m3x3mlc9099ivx3grkcbplcgal3dmdd6cxxd")))

(define-public crate-arrayvec-0.3.11 (c (n "arrayvec") (v "0.3.11") (d (list (d (n "nodrop") (r "^0.1.4") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "1xi4g7fmfi6k7clgyxhsz4afkbdm8y36cgiycpqhx0jvwahvyx70")))

(define-public crate-arrayvec-0.3.12 (c (n "arrayvec") (v "0.3.12") (d (list (d (n "nodrop") (r "^0.1.4") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "063xjdl1nfwjbh2gl5kswdr55hlma500pwv3qpn1d64cgqj0hc5y")))

(define-public crate-arrayvec-0.3.13 (c (n "arrayvec") (v "0.3.13") (d (list (d (n "nodrop") (r "^0.1.4") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "1y1djhqrsawlccv8b6wv5a89j6qfllsw9292jn6yxf6jmlz6nhx3")))

(define-public crate-arrayvec-0.3.14 (c (n "arrayvec") (v "0.3.14") (d (list (d (n "nodrop") (r "^0.1.4") (d #t) (k 0)) (d (n "odds") (r "^0.2") (d #t) (k 0)))) (h "0szkv5bpm14w9v9fcz20p0cafiw0r7fjlx6p8vxp0njbb2hs2y32")))

(define-public crate-arrayvec-0.3.15 (c (n "arrayvec") (v "0.3.15") (d (list (d (n "nodrop") (r "^0.1.6") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1axia8yrz3hxxw3jblxqvj46567jxcd0nsj8qzzdjznq304hzk2z") (f (quote (("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.16 (c (n "arrayvec") (v "0.3.16") (d (list (d (n "nodrop") (r "^0.1.6") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "0x996nf2mwpwa736nasb55akivgqgjlmjpcphl1cwfjbynrbvqqn") (f (quote (("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.17 (c (n "arrayvec") (v "0.3.17") (d (list (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "0q68hnrwgwvscmcqfpvc88ap3m7h265djqy0hxrwx4if5qwkg8c0") (f (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.18 (c (n "arrayvec") (v "0.3.18") (d (list (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1dycl8gry7m2ikbdcrfamx86hax4202fbfrcazi6i0wffgyh23c9") (f (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.19 (c (n "arrayvec") (v "0.3.19") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "0nkmm4fya8l2vna0skcdr02p3mjhdf98cqa2z9s3ss8jh77vnni9") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.20 (c (n "arrayvec") (v "0.3.20") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "15mhzb03q8hlyjnnz3axmk6a58c2s74g12kpjyvvaw124h71p7yq") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.21 (c (n "arrayvec") (v "0.3.21") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)))) (h "1d0qxd4q01456wc3mz41alx1kigqpjq6rvgqk8i9gq50b7svhk0n") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.22 (c (n "arrayvec") (v "0.3.22") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)))) (h "1vdsfjjwbnzy28c4alh6sqp4yqj2r9k0rb2sxyfhhd814q3ykh9m") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3.23 (c (n "arrayvec") (v "0.3.23") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)))) (h "0rywr2q86719d6vjx9srkrnhs3bqnwl1psxmqgl1gmvr7fln77k9") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4.0 (c (n "arrayvec") (v "0.4.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "185j6rk1cy0fl1z23891lcdsbwi31wwf5gpdji8sk31ddhgll5fi") (f (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.1 (c (n "arrayvec") (v "0.4.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05hw95vibwp22ggvwxsjcnc7arhk2ndb0254ihas6jjf389nbiqv") (f (quote (("use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.2 (c (n "arrayvec") (v "0.4.2") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "09w40y4h96i79nk66x9175j635jwvx438ncqccam176fs13m5l11") (f (quote (("use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.3 (c (n "arrayvec") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0bnsv7rw18a805nq7gy1nns1icwb7i48ix2s5g8mn2nrmbgbd6vg") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.4 (c (n "arrayvec") (v "0.4.4") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1dhpxmv5l7z3vl9x2rgbhj7w3a1jva4b0psjvx9n6c8p7dlm00hw") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.5 (c (n "arrayvec") (v "0.4.5") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1yvjg3j4lk5arn1jf4pxcbj5nbc4sbzbb09h0bbvr4gcwq12qgpn") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.3.24 (c (n "arrayvec") (v "0.3.24") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)))) (h "1qk5kw9y04k1r8shrxmwbk9i8i8p4kmahbnzr07kmi71w3vcn0z0") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4.6 (c (n "arrayvec") (v "0.4.6") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1qmvf115sa0xviyv8j8sb2glc7f77p4ii2ci3p4s0680halz83ig") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.3.25 (c (n "arrayvec") (v "0.3.25") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.23") (k 0)))) (h "0krkhgiwsd54r11sqnx8xymgy8xd70h8rllhrpx7ifq60ghrzx86") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4.7 (c (n "arrayvec") (v "0.4.7") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1vsscw2jzkhn2lqjgz3xcp48ilj0vamh6ddlzj1q2n2dwbwn9sd1") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4.8 (c (n "arrayvec") (v "0.4.8") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1k2062b7by0p2p15x6q4y1c9bx80pkwsvhlgdi7pi2yd456cq1gl") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4.9 (c (n "arrayvec") (v "0.4.9") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "09lagldfg8fyf9k49m63rgy6vck6vi9mq72i932630idgjbi71fi") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4.10 (c (n "arrayvec") (v "0.4.10") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0wcch3ca9qvkixgdbd2afrv1xa27l83vpraf7frsh9l8pivgpiwj") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4.11 (c (n "arrayvec") (v "0.4.11") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1fmhq4ljxr954mdyazaqa9kdxryl5d2ggr5rialylrd6xndkzmxq") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5.0 (c (n "arrayvec") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05ia5hrr78x2fi7dwf0pkd8wwfbgg1dzja50l0gcjfw4p2y03n7a") (f (quote (("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4.12 (c (n "arrayvec") (v "0.4.12") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1.12") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1fdiv5m627gh6flp4mpmi1mh647imm9x423licsr11psz97d97yd") (f (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5.1 (c (n "arrayvec") (v "0.5.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1f5mca8kiiwhvhxd1mbnq68j6v6rk139sch567zwwzl6hs37vxyg") (f (quote (("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5.2 (c (n "arrayvec") (v "0.5.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "12q6hn01x5435bprwlb7w9m7817dyfq55yrl4psygr78bp32zdi3") (f (quote (("unstable-const-fn") ("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.6.0 (c (n "arrayvec") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "05rh2a04k9fdffgprybkvaq21d8zxs4jp4m83ncn9wkh2ayqhbv8") (f (quote (("unstable-const-fn") ("std") ("default" "std"))))))

(define-public crate-arrayvec-0.6.1 (c (n "arrayvec") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1jghi4x1j0gybnhcdsrnk8kqs06wd9rpq7pqhympqfimd1g0z796") (f (quote (("unstable-const-fn") ("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7.0 (c (n "arrayvec") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1xza6jbs8x51yhh4qnwjw1crm33bhl975r965fpq1hqhpfq5hbss") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7.1 (c (n "arrayvec") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1kcgsfp0ns0345g580ny7hhl9y9a6l3m0pykfa09p9pz65qw0kdy") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7.2 (c (n "arrayvec") (v "0.7.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mjl8jjqxpl0x7sm9cij61cppi7yi38cdrd1l8zjw7h7qxk2v9cd") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7.3 (c (n "arrayvec") (v "0.7.3") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "17684msnzvh4nx064x4cqqijmimqv5lyajm7kl3qpa6fz2gz0s48") (f (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7.4 (c (n "arrayvec") (v "0.7.4") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "04b7n722jij0v3fnm3qk072d5ysc2q30rl9fz33zpfhzah30mlwn") (f (quote (("std") ("default" "std"))))))

