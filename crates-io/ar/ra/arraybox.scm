(define-module (crates-io ar ra arraybox) #:use-module (crates-io))

(define-public crate-arraybox-0.0.1 (c (n "arraybox") (v "0.0.1") (h "08m8wskav3sxpnm2ll1yv0y7xr5c1vxgwayymj5f6j7s82n0k5lh")))

(define-public crate-arraybox-0.1.0 (c (n "arraybox") (v "0.1.0") (d (list (d (n "const-default") (r "^1.0.0") (d #t) (k 0)))) (h "0ah2scjmc00y7z4jwj5grf3rg8fimc258csg4k6dh15z1zzb4vqi")))

(define-public crate-arraybox-0.1.1 (c (n "arraybox") (v "0.1.1") (d (list (d (n "const-default") (r "^1.0.0") (d #t) (k 0)))) (h "1kikb85iihrn6518krvr4cvmssj40vlxz38aipkakgpj06m945iv")))

