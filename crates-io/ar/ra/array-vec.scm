(define-module (crates-io ar ra array-vec) #:use-module (crates-io))

(define-public crate-array-vec-0.1.0 (c (n "array-vec") (v "0.1.0") (h "16vmbrq5hd0cl7r0rzb5r4p0p5kbnm7s13j1d93n7q3y6h5ryxg4")))

(define-public crate-array-vec-0.1.1 (c (n "array-vec") (v "0.1.1") (h "0i958hgjzx9i9ss97ahbxlym0vyyg7frcdg7m1wvl90cd2j8qb8d")))

(define-public crate-array-vec-0.1.2 (c (n "array-vec") (v "0.1.2") (h "1cyp7h8xq8ih976lx5qa55cxjsmgzcad6pb15sa682i3zkx5csrs")))

(define-public crate-array-vec-0.1.3 (c (n "array-vec") (v "0.1.3") (h "0zwgjfrpplzk08wqqsl9xp7vhf8jlrf4lgyq40gjkmpy3k6lzr6v")))

