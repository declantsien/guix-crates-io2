(define-module (crates-io ar ra arraygen-docfix) #:use-module (crates-io))

(define-public crate-arraygen-docfix-0.1.7 (c (n "arraygen-docfix") (v "0.1.7") (d (list (d (n "arraygen") (r "^0.1.7") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)))) (h "0yll0fgav7pfw8cfcghhdmlas737n77af0iryxqpbpasa6msp5br")))

(define-public crate-arraygen-docfix-0.1.8 (c (n "arraygen-docfix") (v "0.1.8") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0avlisqdykcz9gpns1qr7pdp3z3dcyrvlxn668wkpkk81flvgf3x")))

