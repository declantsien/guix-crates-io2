(define-module (crates-io ar ra array_init_macro) #:use-module (crates-io))

(define-public crate-array_init_macro-0.1.0 (c (n "array_init_macro") (v "0.1.0") (h "1y6ldv6mcs4ia0mlv6ffm7bw331vwlwnf0bln48lh38j74c1s1vg")))

(define-public crate-array_init_macro-0.1.1 (c (n "array_init_macro") (v "0.1.1") (h "0i7b2yxnk9xm4nwvml7cf18mnz8dylpf73jzxm9j59jxahiflbl7")))

(define-public crate-array_init_macro-0.1.2 (c (n "array_init_macro") (v "0.1.2") (h "1jfp5dnw14bs7jn16901vsx0j4a3y13agzv234hv72gj813zw3s0")))

