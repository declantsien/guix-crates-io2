(define-module (crates-io ar ra arraylist) #:use-module (crates-io))

(define-public crate-arraylist-0.1.0 (c (n "arraylist") (v "0.1.0") (h "1hc9dlwrs53m3sj2qhz2jb9yyn0kp1mqip2azpfinfdyjwqwd1yx")))

(define-public crate-arraylist-0.1.2 (c (n "arraylist") (v "0.1.2") (h "1q8ygc832h1qq778gzzdqpjb3c73gq5jsbf5n8ff51mm6p477vzz") (y #t)))

(define-public crate-arraylist-0.1.3 (c (n "arraylist") (v "0.1.3") (h "0h297hm70zfk2ksazkp979ly794zaz54xw8dhwr1k3wcyr0dg33i")))

(define-public crate-arraylist-0.1.4 (c (n "arraylist") (v "0.1.4") (h "0wa9fgyjik8z6pjy8nmgq53z5ch1kbj71bx1lk6giknf8h5mbdc1") (y #t)))

(define-public crate-arraylist-0.1.5 (c (n "arraylist") (v "0.1.5") (h "1kmp5rzd58x38r24fm90286vx8nn2np17nmhmjbh5igkl4nvgqgv")))

