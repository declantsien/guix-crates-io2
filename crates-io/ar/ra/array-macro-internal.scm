(define-module (crates-io ar ra array-macro-internal) #:use-module (crates-io))

(define-public crate-array-macro-internal-0.1.0 (c (n "array-macro-internal") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "1r0qwspxw7vw0s65hskckzj82xz86rwrjz3v492vbv4n8jmn5730") (y #t)))

(define-public crate-array-macro-internal-0.1.1 (c (n "array-macro-internal") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "03y1haaxrhk0dz34i82aig62ck9a5iylj1fjkzvavcwfdrxkcc1r") (y #t)))

(define-public crate-array-macro-internal-0.1.2 (c (n "array-macro-internal") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "18r43d4yvy0slnvc1z10dirb6rq615v4lg9rjxada3j7psb7hrh9") (y #t)))

