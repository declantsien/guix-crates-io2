(define-module (crates-io ar ra array-box) #:use-module (crates-io))

(define-public crate-array-box-1.0.0 (c (n "array-box") (v "1.0.0") (h "0r4wqsxmgqzn8ld9r3yjsscxw9gwbxl37jyy04dyx288s47wnd52")))

(define-public crate-array-box-1.0.1 (c (n "array-box") (v "1.0.1") (h "1mx8gchzbdcf6zbadhmly5sl0r267shlglxl5zh4sqrdasjqvkpr")))

(define-public crate-array-box-1.0.2 (c (n "array-box") (v "1.0.2") (h "0ylm9pdddip30117ixvw6jl435l0phykfcdxasxlkvq1m5f74amy")))

