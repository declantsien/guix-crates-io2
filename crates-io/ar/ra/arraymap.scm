(define-module (crates-io ar ra arraymap) #:use-module (crates-io))

(define-public crate-arraymap-0.1.0 (c (n "arraymap") (v "0.1.0") (h "1j5kpcyadh7hhp92w0yvvzfpqhzdq4y44arj4qz184a80jlrplqg")))

(define-public crate-arraymap-0.1.1 (c (n "arraymap") (v "0.1.1") (h "05m4xygsbajbicy10ghqp3rbd8y3yas6frnp67fa2giz1n563827")))

