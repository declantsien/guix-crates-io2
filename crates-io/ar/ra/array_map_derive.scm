(define-module (crates-io ar ra array_map_derive) #:use-module (crates-io))

(define-public crate-array_map_derive-0.1.0 (c (n "array_map_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "proc-macro" "printing"))) (k 0)))) (h "03wg675djdmp9vfxvsq9l1nbz0n6clcda8ycdm1jg93yay6v0nrv")))

(define-public crate-array_map_derive-0.2.0 (c (n "array_map_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "proc-macro" "printing"))) (k 0)))) (h "0dnx4lingynhnhaf4slvixvmpvxmig8w5a0s8vk0ygp735r0p1hj")))

(define-public crate-array_map_derive-0.3.0 (c (n "array_map_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "proc-macro" "printing"))) (k 0)))) (h "0ls44zvdsmlj6fril19xadrgpmqp94li6gwb7lmgvbal2sp08z82")))

(define-public crate-array_map_derive-0.4.0 (c (n "array_map_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "derive" "proc-macro" "printing"))) (k 0)))) (h "1innhalfamgwzvvyis41w8265c5fysccyxc9k0c8qn65bvlgb01d")))

