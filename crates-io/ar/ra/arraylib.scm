(define-module (crates-io ar ra arraylib) #:use-module (crates-io))

(define-public crate-arraylib-0.1.0 (c (n "arraylib") (v "0.1.0") (h "15ajzbky3qc64j9375damkqjm7fl22fkxycm5f2w69icnc4pygbz") (f (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc")))) (y #t)))

(define-public crate-arraylib-0.2.0 (c (n "arraylib") (v "0.2.0") (h "15b44vw8qkww7q7d3zvfrfv639n86ypcppi317vsq585spk3p080") (f (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc")))) (y #t)))

(define-public crate-arraylib-0.3.0 (c (n "arraylib") (v "0.3.0") (h "1gr81zh800vqx5fbamkf3p7rv28cmwql87qbqladsxws4gaj2a49") (f (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc"))))))

