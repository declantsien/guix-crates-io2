(define-module (crates-io ar ra array-merge) #:use-module (crates-io))

(define-public crate-array-merge-0.1.0 (c (n "array-merge") (v "0.1.0") (h "1jq37qvv4z5w5rlpxpffxx7l1lm5v53sln68xxn2gpiyg146khg5")))

(define-public crate-array-merge-0.1.1 (c (n "array-merge") (v "0.1.1") (h "05im9bj5c3ngkskxy9c3g21cgd7l90mqfwkrf3mbjw98amm75mqx")))

