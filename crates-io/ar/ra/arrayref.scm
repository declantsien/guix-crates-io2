(define-module (crates-io ar ra arrayref) #:use-module (crates-io))

(define-public crate-arrayref-0.1.0 (c (n "arrayref") (v "0.1.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1jmhgx1byfsp38f8jhvmmngsbh81ls1hfzl9ln5gb3r21v4gasdv")))

(define-public crate-arrayref-0.1.1 (c (n "arrayref") (v "0.1.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0ng0jhl5pbynjf5ryjlwpvplym0xd559yjmh3g82pkckq2w1l56z")))

(define-public crate-arrayref-0.2.0 (c (n "arrayref") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0vgishy80cwvgxwrsf7d9awb136y6khh15fm5vqaj55s5sdd4kkl")))

(define-public crate-arrayref-0.2.1 (c (n "arrayref") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0bsp6y482m74vgia0824q6bfhdjfmyqjkfkxj6xcasbzmpb6lp5i")))

(define-public crate-arrayref-0.2.2 (c (n "arrayref") (v "0.2.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1ylcy2v1h0ci593vcn9n61fygbj92i38qcrbk1qm53d2gl2rrz6h")))

(define-public crate-arrayref-0.3.0 (c (n "arrayref") (v "0.3.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "01c8i780d8vi5m2r4bza4nvqrn961gkwl7b0g9r130zscidgc904")))

(define-public crate-arrayref-0.3.1 (c (n "arrayref") (v "0.3.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "18xzgc89887chahspgx3aq0bpmv3mx3dp1fdn5160z3aba1hi8r9")))

(define-public crate-arrayref-0.3.2 (c (n "arrayref") (v "0.3.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1yaqimjxdbwccr4n0szf8lalsr1v2vn6djaw1fdjwkz3sjd7403m")))

(define-public crate-arrayref-0.3.3 (c (n "arrayref") (v "0.3.3") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1r8xqn116z7343fc21f8qi9rdvggz3kk5zk0rdvsdxvr487snnxi")))

(define-public crate-arrayref-0.3.4 (c (n "arrayref") (v "0.3.4") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "13r5hyyarq53bnbqqp7jj9m6jbcjjg1bbwszsgdilr19gjdlgl8g")))

(define-public crate-arrayref-0.3.5 (c (n "arrayref") (v "0.3.5") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1vphy316jbgmgckk4z7m8csvlyc8hih9w95iyq48h8077xc2wf0d")))

(define-public crate-arrayref-0.3.6 (c (n "arrayref") (v "0.3.6") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0i6m1l3f73i0lf0cjdf5rh3xpvxydyhfbakq7xx7bkrp5qajgid4")))

(define-public crate-arrayref-0.3.7 (c (n "arrayref") (v "0.3.7") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0ia5ndyxqkzdymqr4ls53jdmajf09adjimg5kvw65kkprg930jbb")))

