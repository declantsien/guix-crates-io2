(define-module (crates-io ar ra array-utils) #:use-module (crates-io))

(define-public crate-array-utils-0.0.0 (c (n "array-utils") (v "0.0.0") (h "1yriza7pnqck6ykhh3ya8dshqnfywr450992nvdnds3hm05yxzb0")))

(define-public crate-array-utils-0.1.0 (c (n "array-utils") (v "0.1.0") (h "1k145a9xsdbs9bm1cf5av0yk4gnd6gmmpr52snysi7hwchhh5dg4") (f (quote (("superimpose") ("splice") ("slice") ("resize") ("join") ("initialize") ("drift") ("default" "initialize" "drift" "slice" "splice" "join" "resize" "superimpose"))))))

(define-public crate-array-utils-0.1.1 (c (n "array-utils") (v "0.1.1") (h "047qcqh1f3yz62waidchp4h30088krla2581vwp1kip98zb2gb62") (f (quote (("superimpose") ("splice") ("slice") ("resize") ("join") ("initialize") ("drift") ("default" "initialize" "drift" "slice" "splice" "join" "resize" "superimpose"))))))

