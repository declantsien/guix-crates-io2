(define-module (crates-io ar ra array-as-struct-derive) #:use-module (crates-io))

(define-public crate-array-as-struct-derive-0.1.0 (c (n "array-as-struct-derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "printing" "proc-macro" "derive" "clone-impls" "extra-traits"))) (k 0)))) (h "0vbdvadr67mkag453smybnq4rpq80ihi0qqsrv9f8hzb37zvmmrn")))

