(define-module (crates-io ar ra arraydeque) #:use-module (crates-io))

(define-public crate-arraydeque-0.1.2 (c (n "arraydeque") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "08q4z4sv6nh15il9a383fgj77mrlnqypj047rh0bjs00xvi5an44") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.1.3 (c (n "arraydeque") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "1qrnwy236pir4gv743h8vp0qkys7hhrwjkadq5sj6mnds0i8lm9m") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2.1 (c (n "arraydeque") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "0kfip65zk45iy4kjwf4zscs2bh49mm7sfqmz3s26hnq7g6hdbmgb") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2.2 (c (n "arraydeque") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "17g7116ri1r2lprzgrmj5v9i46zsbv5gcsx7zq9xsmbxl53290hc") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2.3 (c (n "arraydeque") (v "0.2.3") (d (list (d (n "generic-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (k 0)) (d (n "odds") (r "^0.2.12") (k 0)))) (h "05x7i5s6144lbrhpk2m532wh561zg6cnf3184m929hi4vg579rwn") (f (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.3.1 (c (n "arraydeque") (v "0.3.1") (d (list (d (n "odds") (r "^0.2.12") (k 0)))) (h "0zs2f9piz52gli2cb47xwi0i44gz86lfpxr6r5b88yb92hykhqdj") (f (quote (("std" "odds/std") ("default" "std")))) (y #t)))

(define-public crate-arraydeque-0.4.1 (c (n "arraydeque") (v "0.4.1") (h "13gvaxm521ahc56na9ll7f951l6ha7vsw8p0kk89fcb3j52y11p0") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-arraydeque-0.4.2 (c (n "arraydeque") (v "0.4.2") (h "048zn2vq0amsi8shbfjmrh6gwklfvhg0cajkvnqlz7wbb6l9l1dw") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-arraydeque-0.4.3 (c (n "arraydeque") (v "0.4.3") (d (list (d (n "generic-array") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "1imcdj47z71r1ixmy08s3gpwf6hksjr2icnc3z4gy1mqfdq34073") (f (quote (("use_generic_array" "generic-array") ("std") ("default" "std")))) (y #t)))

(define-public crate-arraydeque-0.4.4 (c (n "arraydeque") (v "0.4.4") (d (list (d (n "generic-array") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "107mg6ijwl3qpz04368r99q4dbwcqjbdg8d2lbaak97qjh7alj6x") (f (quote (("use_generic_array" "generic-array") ("std") ("default" "std"))))))

(define-public crate-arraydeque-0.4.5 (c (n "arraydeque") (v "0.4.5") (d (list (d (n "generic-array") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1n4appvjfrmxkc4x0v8ivpzwqf1z6pqx2caxk98116fqkgbd7zzh") (f (quote (("use_generic_array" "generic-array") ("std") ("default" "std"))))))

(define-public crate-arraydeque-0.5.0 (c (n "arraydeque") (v "0.5.0") (h "19h01cp55giz3064prdfr4p823j98jwjpljyms6c6pr5xw3j932w") (f (quote (("std") ("default" "std"))))))

(define-public crate-arraydeque-0.5.1 (c (n "arraydeque") (v "0.5.1") (h "0dn2xdfg3rkiqsh8a6achnmvf5nf11xk33xgjzpksliab4yjx43x") (f (quote (("std") ("default" "std"))))))

