(define-module (crates-io ar ra arraygen) #:use-module (crates-io))

(define-public crate-arraygen-0.1.0 (c (n "arraygen") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hflqi7rq6sb52vm637d8hhqpdqjjhy2i58p62mcxd8qbsnkjpnw")))

(define-public crate-arraygen-0.1.1 (c (n "arraygen") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1va5315x232z3hdw5lzx9qf0l4gn0z425vnjljp2s5wldz61v93b")))

(define-public crate-arraygen-0.1.2 (c (n "arraygen") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1dlgvbjqccp4rhqsw1kkj5vdsasf6vs4hnlkmzyic2hzjrrzxz0b")))

(define-public crate-arraygen-0.1.3 (c (n "arraygen") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1f9pghh7n07l70yzq6bicxi5gv60ywrha25asgmbfffrlfbpnwj7")))

(define-public crate-arraygen-0.1.4 (c (n "arraygen") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "09i2mkr6my4iib7mn7d8s35brw02jpn942839yfambzxgfg4mgr5")))

(define-public crate-arraygen-0.1.5 (c (n "arraygen") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1x2imfyl9vhsfj6j66xinf9xfhhlyxa2kcb5sq5jbjc8rcnmqp86")))

(define-public crate-arraygen-0.1.6 (c (n "arraygen") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1da4vhrim2shp8c6xg3m1pvy21my1vq769rivmzadbr27399s0gw")))

(define-public crate-arraygen-0.1.7 (c (n "arraygen") (v "0.1.7") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0pzx68ims0yfy6nwp2qws4mw6ii4drk53075nr193mhwvgxa14jr")))

(define-public crate-arraygen-0.1.8 (c (n "arraygen") (v "0.1.8") (d (list (d (n "arraygen") (r "^0.1.8") (d #t) (k 0) (p "arraygen-docfix")) (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0bbwph8fps640135vv6ipllnfc0hffpynx79bmw6bpqbl48s7yma")))

(define-public crate-arraygen-0.1.9 (c (n "arraygen") (v "0.1.9") (d (list (d (n "arraygen") (r "^0.1.8") (d #t) (k 0) (p "arraygen-docfix")) (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0ja1al5hhjf99fjlq9mwfk5xr18xasa3k2nc8d89l7li57sfqlq4")))

(define-public crate-arraygen-0.1.10 (c (n "arraygen") (v "0.1.10") (d (list (d (n "arraygen") (r "^0.1.8") (d #t) (k 0) (p "arraygen-docfix")) (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1mj1qkjsrbhrw4hmgn11k624qr4dfiy62bkj2hwiawswxxscjclk")))

(define-public crate-arraygen-0.1.11 (c (n "arraygen") (v "0.1.11") (d (list (d (n "arraygen") (r "^0.1.8") (d #t) (k 0) (p "arraygen-docfix")) (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1slp1vdg9pzlixpv2bvphvqyajyj8blwvfgmw335kb0b94ih2s37")))

(define-public crate-arraygen-0.1.12 (c (n "arraygen") (v "0.1.12") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1dv5wki20n0a8fbc5nxhaa36lp1q5nnkmv1xd0kl319jqznrml8w")))

(define-public crate-arraygen-0.1.13 (c (n "arraygen") (v "0.1.13") (d (list (d (n "compiletest_rs") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "18bbbqng4xhh678bzlf7dig84xqkhz7af3q0xxqc44cjq4imnxfc")))

(define-public crate-arraygen-0.1.14 (c (n "arraygen") (v "0.1.14") (d (list (d (n "compiletest_rs") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0qp513pkkr1sqjq31klfsla6j6pvnqvvh6fhpyaq4n2jbxmy06js")))

(define-public crate-arraygen-0.1.15 (c (n "arraygen") (v "0.1.15") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "0cr4gl82y2vvzxb71vxw031r0ivl5k1nb5vbl6afaavswwa74pjx")))

(define-public crate-arraygen-0.1.16 (c (n "arraygen") (v "0.1.16") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1kycd9w3hpfab6213y6j1im1x2f1jiqh0kzkqn35kd0m2rsnqfjl")))

(define-public crate-arraygen-0.2.0 (c (n "arraygen") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0v0k8rdlzx31xs1hlh87ma25ijyp6m65xxss992ws6r3rnzw7ja8")))

(define-public crate-arraygen-0.3.0-RC0 (c (n "arraygen") (v "0.3.0-RC0") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16nhxcmkg0cb74rsqshkcrfsmsxqf1d99696yjvgzg7y8f0k2mwn")))

(define-public crate-arraygen-0.3.0-RC1 (c (n "arraygen") (v "0.3.0-RC1") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mi07mksadsrz2v9filgnw3a3sfjzivdr0w2hq8f5hzyfd0m009p")))

(define-public crate-arraygen-0.3.0 (c (n "arraygen") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mpwdx6p9z82n9jc0361l7q2rra6xmqqlf4k81gqxapgkagrja9z")))

(define-public crate-arraygen-0.3.1 (c (n "arraygen") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nh8w0hrwyj0rdwl8izv1za9maarfffjbxl1py92sypz6fblxwmz")))

(define-public crate-arraygen-0.3.2 (c (n "arraygen") (v "0.3.2") (d (list (d (n "compiletest_rs") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1238askrzc2ginkbsfgv3vjj933qbiwaib7pjsky0mzxi7ivi3yi")))

