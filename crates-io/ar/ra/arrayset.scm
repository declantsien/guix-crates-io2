(define-module (crates-io ar ra arrayset) #:use-module (crates-io))

(define-public crate-arrayset-0.1.0 (c (n "arrayset") (v "0.1.0") (h "05j3mfg37amc4vfy4kvz7s5i74g5sg1hb73dbkpj7xd3wpvac55x") (y #t)))

(define-public crate-arrayset-0.1.1 (c (n "arrayset") (v "0.1.1") (h "17h7nnwdj12r7vws9h4sp2cvkgvkq2116zl4jdwa7y62kxkxsh9y") (y #t)))

(define-public crate-arrayset-1.0.0 (c (n "arrayset") (v "1.0.0") (h "1c9alr65s6ik8iq0hc4qmglyzyzjhmngxsvbyy4syjq2qlz8c9wy") (y #t)))

(define-public crate-arrayset-1.0.1 (c (n "arrayset") (v "1.0.1") (h "1fkbdi95pmiha6cy15lp8qxq16n4bn39zn35hl2n00fihkgyr890") (y #t)))

(define-public crate-arrayset-1.0.2 (c (n "arrayset") (v "1.0.2") (h "1smc8ysqszkgmhny69vr5lijmwiich8dg2y0kda0m3nisk8b7sqv") (y #t)))

(define-public crate-arrayset-1.0.3 (c (n "arrayset") (v "1.0.3") (h "0yqbwri4cn64h93n81b57nzkd58r521dg7ksmx5161fbjsqyc1xl") (y #t)))

(define-public crate-arrayset-1.0.4 (c (n "arrayset") (v "1.0.4") (h "0p4iyksj92caw2mpk4ra0y3xz67blp4xkb5g46h1lbfs912k2gsl")))

(define-public crate-arrayset-1.0.5 (c (n "arrayset") (v "1.0.5") (h "1cspznhaybr4idaia666g0hsk5f9q080f68kgbdvkjc0pkm70hjy")))

(define-public crate-arrayset-1.1.0 (c (n "arrayset") (v "1.1.0") (h "1n6r6xvyhf8a5my6gfflf91ra7fircn26ifm8ysmi04wfjd4apx3")))

(define-public crate-arrayset-1.2.0 (c (n "arrayset") (v "1.2.0") (h "0sv1xqz8ifk7rh8n45pwmljmjvbrq10ivba5a9q55i5jqaf6vczh")))

(define-public crate-arrayset-1.3.0 (c (n "arrayset") (v "1.3.0") (h "1f98fdgmasd0b8c9fcmivpacwmxml9pm0s2llbkchhq00ibfyhsl")))

(define-public crate-arrayset-1.4.0 (c (n "arrayset") (v "1.4.0") (h "0s7hsi27hqlvzrm785130vgx31yq9592klrmz7sbkc5zcrks4n5g")))

(define-public crate-arrayset-1.5.0 (c (n "arrayset") (v "1.5.0") (h "0s6lici8hss0mmznzyzbmlp36ayp5179kj72h91xlwznwqnsyxna")))

(define-public crate-arrayset-1.5.1 (c (n "arrayset") (v "1.5.1") (h "1x319zdqb0sklvlryc41vvn0l60jql2y8sd4g25m4gkbci9wjjal")))

(define-public crate-arrayset-2.0.0 (c (n "arrayset") (v "2.0.0") (h "0dywj6g8zardg047kk5ahxz9ik7ddrpsxg3gsmc3iyk7xzndmvz8")))

