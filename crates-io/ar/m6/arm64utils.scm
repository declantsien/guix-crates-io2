(define-module (crates-io ar m6 arm64utils) #:use-module (crates-io))

(define-public crate-arm64utils-0.1.0 (c (n "arm64utils") (v "0.1.0") (h "0crl6j7bxsmd2zjdq0k2ldl6pnrj63dpszmvmmqyzy3ids1jx8pa")))

(define-public crate-arm64utils-0.1.1 (c (n "arm64utils") (v "0.1.1") (h "1lifjb7yjy7qgnpnq18p7gmcbilhy0na9aj95gj7jxmix5sqzx8i")))

(define-public crate-arm64utils-0.1.2 (c (n "arm64utils") (v "0.1.2") (h "0hc1zv3cglx1ih39n23g70nhhfws0vd228q3ikyr0c6gslbjbbbm")))

(define-public crate-arm64utils-0.1.3 (c (n "arm64utils") (v "0.1.3") (h "1z8szpmz10hdxfqny3hhpwdy76h3gcb2p3zaxgh2i3hlqdz8vzzq")))

