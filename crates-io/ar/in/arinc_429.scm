(define-module (crates-io ar in arinc_429) #:use-module (crates-io))

(define-public crate-arinc_429-0.1.0 (c (n "arinc_429") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "1mbmqd3387dz9r1c0fl1bfvlwiygwzshrmis4n2r8r9a6xw4527k") (f (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1.1 (c (n "arinc_429") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "0hijhw7y8w685ziswxxrc8ngvfyr9s4m3gd1246n2gcqf61nr700") (f (quote (("default"))))))

(define-public crate-arinc_429-0.1.2 (c (n "arinc_429") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "15jgvqw5k96zzalpspxpj9vh8w1a01dnwynjm5hqkrjng82wv7yq") (f (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1.3 (c (n "arinc_429") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "10zrjxgjmsf7y1kpq892qs32r7zd9hqwnbqcrsm0pxggqf158dkn") (f (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1.4 (c (n "arinc_429") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "0fnvfq2h8a8d857igzqrnyp3rvvqild965nmacj2g2g8igky3g8r") (f (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1.5 (c (n "arinc_429") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (d #t) (k 2)))) (h "0bzyh09y5dhwn369sja20iyb582pis0jhf9xpjk202qyjkskq8d2") (f (quote (("std") ("default" "std"))))))

