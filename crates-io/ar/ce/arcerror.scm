(define-module (crates-io ar ce arcerror) #:use-module (crates-io))

(define-public crate-arcerror-0.1.0 (c (n "arcerror") (v "0.1.0") (d (list (d (n "arcstr") (r "^1.1.4") (k 0)))) (h "0wzrz21xl1figh8aqad27cqjs35wbhyvwixnhvfg364p20m4zpzf") (y #t)))

(define-public crate-arcerror-0.1.1 (c (n "arcerror") (v "0.1.1") (d (list (d (n "arcstr") (r "^1.1.4") (k 0)))) (h "07lqqs6844vv87kaxdrgmhykcjyzq10ffl53blbikl3mljac15j9") (y #t)))

(define-public crate-arcerror-0.1.2 (c (n "arcerror") (v "0.1.2") (d (list (d (n "arcstr") (r "^1.1.4") (k 0)))) (h "02al2wb8sfla36w16g3adiys34pcq16g9k3kkrqz8vy7mhdcn6h8") (y #t)))

(define-public crate-arcerror-0.1.3 (c (n "arcerror") (v "0.1.3") (d (list (d (n "arcstr") (r "^1.1.4") (k 0)))) (h "0dhnfndpvr6pvka8x3rl75pa0sf0avzfslln00n0z1ib5icrhgrl")))

(define-public crate-arcerror-0.1.4 (c (n "arcerror") (v "0.1.4") (h "0q3m7dqflxf5fd2wlnhv755wcjsiq23j6hh6lx8l38fqs99lrp3q")))

(define-public crate-arcerror-0.1.5 (c (n "arcerror") (v "0.1.5") (h "1yig8zsmwrpgppn71xlbzmmfdhin8w39w1p1vzr0sa85a9dj89qy")))

