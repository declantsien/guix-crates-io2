(define-module (crates-io ar ce arcell) #:use-module (crates-io))

(define-public crate-arcell-0.1.0 (c (n "arcell") (v "0.1.0") (h "0dllan0yn4qxflv0kh3kbn2gf71kvq17py5ydf6gqn9mav6h106j")))

(define-public crate-arcell-0.1.1 (c (n "arcell") (v "0.1.1") (h "16mp1dz9vlqcrqbccpwm7rddi61lxs47mjjcnzkslqdai2wzjfps")))

(define-public crate-arcell-0.1.2 (c (n "arcell") (v "0.1.2") (h "1kq75hvf8k9p1szshkzna6h542n5f43279y6xrz9n4g19rbsmmw9")))

