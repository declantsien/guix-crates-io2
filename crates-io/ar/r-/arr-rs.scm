(define-module (crates-io ar r- arr-rs) #:use-module (crates-io))

(define-public crate-arr-rs-0.0.0 (c (n "arr-rs") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "08lda5rzlrix4k1hbwcbn41xix5hk5pphi06nr16iaimjs6kbli9")))

(define-public crate-arr-rs-0.1.0 (c (n "arr-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "0wgm7ibrbq7frsw9ng2k4z4jcxk20s505zm4pa0iiagfziaha609") (f (quote (("macros") ("default" "macros")))) (y #t)))

(define-public crate-arr-rs-0.1.1 (c (n "arr-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "105i35ljk33gd6g06in2fhn2x13w5mx2w7h8vlp6qsbl4z0pw2x4") (f (quote (("macros") ("default" "macros")))) (y #t)))

(define-public crate-arr-rs-0.1.2 (c (n "arr-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "0ig5s167qnfj7zq925ifl8ap39rr35mvcg24ysdrkhd2vhkq8grs") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-arr-rs-0.2.0 (c (n "arr-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "0ammrgh1f4ql5bk9wqyal8n7x749xygxbr317q4j99mdfh0hjrn8") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-arr-rs-0.3.0 (c (n "arr-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "0ny0v3qlyaqp0mxa1k9k5sysdm082yacmidbi0jf44pi2g9hrdq7") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-arr-rs-0.4.0 (c (n "arr-rs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1zngcpslng0pb94big1rxcynwqzfvr65bfpw3m1w90kx04sims7z") (f (quote (("numeric") ("math" "numeric") ("macros" "numeric") ("default" "numeric" "alphanumeric" "boolean" "math" "macros") ("boolean" "numeric") ("alphanumeric"))))))

(define-public crate-arr-rs-0.5.0 (c (n "arr-rs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0b2i71r997xjmdbk29n62nw58k1kh6fd40p688zpr9pvk1dgvpw9") (f (quote (("numeric") ("math" "numeric") ("macros" "numeric") ("default" "numeric" "alphanumeric" "boolean" "math" "macros") ("boolean" "numeric") ("alphanumeric"))))))

(define-public crate-arr-rs-0.5.1 (c (n "arr-rs") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1v1xk9kvcmy34c4cll574jwp6q3kkvfj61nf0xkv5sbv5w131mn7") (f (quote (("numeric") ("math" "numeric") ("macros" "numeric") ("default" "numeric" "alphanumeric" "boolean" "math" "macros") ("boolean" "numeric") ("alphanumeric"))))))

(define-public crate-arr-rs-0.6.0 (c (n "arr-rs") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1d3b87kyf9ma0jcdwqw9a1lyzzdknnby663dcynqwbsl8zqdp0wm") (f (quote (("numeric") ("math" "numeric") ("macros" "numeric") ("linalg" "numeric") ("default" "numeric" "alphanumeric" "boolean" "linalg" "math" "macros") ("boolean" "numeric") ("alphanumeric"))))))

