(define-module (crates-io ar ie arie) #:use-module (crates-io))

(define-public crate-arie-0.1.0 (c (n "arie") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cpal") (r "^0.15.1") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.2") (d #t) (k 0)) (d (n "rubato") (r "^0.12.0") (d #t) (k 0)))) (h "1fw28kqgc57zgd5fi1bm65l1j3crcych3wsnbq5gdxln5qlr9dva")))

(define-public crate-arie-0.2.0 (c (n "arie") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.3") (d #t) (k 0)) (d (n "rubato") (r "^0.14.0") (d #t) (k 0)))) (h "0iabi4ya5k5akhxsz6bnp09wzl7vg100is7g3mrv2zcyhmnw830n")))

(define-public crate-arie-0.2.1 (c (n "arie") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.3") (d #t) (k 0)) (d (n "rubato") (r "^0.14.0") (d #t) (k 0)))) (h "0hpx5w6c551c3v5p1x7y559bc0py0nswpkf218xrg2md9rrf8lyk")))

