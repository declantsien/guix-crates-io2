(define-module (crates-io ar rr arrrg_derive) #:use-module (crates-io))

(define-public crate-arrrg_derive-0.1.0 (c (n "arrrg_derive") (v "0.1.0") (d (list (d (n "derive_util") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "055yzqrr233qa8xzbgzd12xw675lsipnagd17173dbm6clznvsrj")))

(define-public crate-arrrg_derive-0.1.1 (c (n "arrrg_derive") (v "0.1.1") (d (list (d (n "derive_util") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "158iiiriqxrj60krn6v1ggi8lynlzm428lr2f6id2y7qwdcrn8b7")))

(define-public crate-arrrg_derive-0.2.0 (c (n "arrrg_derive") (v "0.2.0") (d (list (d (n "derive_util") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10fj1jlrg6q0c08zvaqy8i5698pjijp4zc4f3nmxrq8qf2zf14zq")))

(define-public crate-arrrg_derive-0.3.0 (c (n "arrrg_derive") (v "0.3.0") (d (list (d (n "derive_util") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vj688kgx1q3yhg8n8k8vama9inr3zcymhs7hwacfr4g9xg9fz81")))

