(define-module (crates-io ar rr arrrg) #:use-module (crates-io))

(define-public crate-arrrg-0.1.0 (c (n "arrrg") (v "0.1.0") (d (list (d (n "arrrg_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "07vsbf1j8ll63cnifx3z4dkf5ky2rlzrz0irb4r1jpq9pjwr4b4g")))

(define-public crate-arrrg-0.1.1 (c (n "arrrg") (v "0.1.1") (d (list (d (n "arrrg_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0gixcw3am0acf75mp0sdpcfcign4gnzvhw1zi51hy8s84mjzv3ms")))

(define-public crate-arrrg-0.1.4 (c (n "arrrg") (v "0.1.4") (d (list (d (n "arrrg_derive") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1sg8gvqymzqbrbdpjjfz7zlzahg663b9ybiwscvmpjxhaldisdn2")))

(define-public crate-arrrg-0.2.0 (c (n "arrrg") (v "0.2.0") (d (list (d (n "arrrg_derive") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1g4zjpajswkl8y9knxdq94laqlq9ihbc7achl15bvxmzf08wp05d")))

(define-public crate-arrrg-0.3.0 (c (n "arrrg") (v "0.3.0") (d (list (d (n "arrrg_derive") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1savjjjy0xf5rppd8h17jws965xm8b9j6ykpln2f1g7zf7ggdrgl")))

