(define-module (crates-io ar m7 arm7tdmi_aeabi) #:use-module (crates-io))

(define-public crate-arm7tdmi_aeabi-0.2.0 (c (n "arm7tdmi_aeabi") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "0ja9ksggi90ns36wv4rkfqdirzm9mdsisnbsk2bzk43ah52lvwbh")))

