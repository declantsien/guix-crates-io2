(define-module (crates-io ar ig arigato) #:use-module (crates-io))

(define-public crate-arigato-0.1.0 (c (n "arigato") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util" "tracing" "sync" "net" "rt"))) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)))) (h "13zp97c3i44cvc3bfigs2i2ppii4l8k8y318xc2jxdrc1sw6i6py")))

