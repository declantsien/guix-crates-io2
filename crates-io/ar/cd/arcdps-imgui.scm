(define-module (crates-io ar cd arcdps-imgui) #:use-module (crates-io))

(define-public crate-arcdps-imgui-0.8.0 (c (n "arcdps-imgui") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.8.0") (d #t) (k 0) (p "arcdps-imgui-sys")) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0v465ck9vxsp5w5jqz5rzn7cn7fx5i6lns1jx66vjpjd37spix8j") (f (quote (("wasm" "imgui-sys/wasm") ("tables-api") ("min-const-generics") ("freetype" "imgui-sys/freetype") ("default" "min-const-generics"))))))

