(define-module (crates-io ar cd arcdps-imgui-sys) #:use-module (crates-io))

(define-public crate-arcdps-imgui-sys-0.8.0 (c (n "arcdps-imgui-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chlorine") (r "^1.0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1y4sl0n9h4q5bmf87p6v9krkl9hwx41g05rx6qgzfhafh26cwfd7") (f (quote (("wasm") ("freetype" "pkg-config") ("default")))) (l "imgui")))

