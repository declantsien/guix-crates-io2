(define-module (crates-io ar cd arcdps_codegen) #:use-module (crates-io))

(define-public crate-arcdps_codegen-0.7.0 (c (n "arcdps_codegen") (v "0.7.0") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1wkiikcry3fj4ln98d4c1n71f4lq7ghgvp4xzl8zxzbpx09m0n3n")))

(define-public crate-arcdps_codegen-0.7.1 (c (n "arcdps_codegen") (v "0.7.1") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0mhaslf2si9sqnv7vpxv0iwrxk8ai1c1qznk2nk98kqgf2h03g1k")))

(define-public crate-arcdps_codegen-0.8.0 (c (n "arcdps_codegen") (v "0.8.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "118bv1zc9xwvvqgp8ka9azfi72qq1i5k44yxyfriy35yidkyrbsw")))

(define-public crate-arcdps_codegen-0.8.1 (c (n "arcdps_codegen") (v "0.8.1") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1v734gi9mmlszygcaqlp8pi0pqhkvwd8az332x977kw2qrkkr33b")))

(define-public crate-arcdps_codegen-0.9.0 (c (n "arcdps_codegen") (v "0.9.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1viz3z6dnzin583zaj0q50sb8ggg40ymjcm2lgmyjiaq9kwccwrb") (f (quote (("imgui"))))))

(define-public crate-arcdps_codegen-0.10.0 (c (n "arcdps_codegen") (v "0.10.0") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0b1gfibyhgv3hs10ymzlcx39hi1xx1hw8kskgnmdbjmnn2lljv27") (f (quote (("imgui"))))))

