(define-module (crates-io ar te artex) #:use-module (crates-io))

(define-public crate-artex-0.1.0 (c (n "artex") (v "0.1.0") (h "1bj210840w347yyc4j0n66dzi36ljhdj5d9f1h507w6a5sfjzar0") (y #t)))

(define-public crate-artex-0.1.1 (c (n "artex") (v "0.1.1") (h "1k52h5rqqqlsn3spdx2iwmn2x94s5d42mp7dlsc0clr4razlvjyq")))

