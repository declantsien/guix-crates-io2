(define-module (crates-io ar te artery-font) #:use-module (crates-io))

(define-public crate-artery-font-1.0.0 (c (n "artery-font") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.5") (o #t) (d #t) (k 0)))) (h "0rjc8gk3rn4wiivqvjpzjr86k9gwx9qdnwrdjzkdwayczyg8fbwv") (f (quote (("no-checksum") ("double"))))))

(define-public crate-artery-font-1.0.1 (c (n "artery-font") (v "1.0.1") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.5") (o #t) (d #t) (k 0)))) (h "1w4f7knax6ngpdvnxq3qqj3wfxnrc6cjial7r2rzfqlv82dkgrgp") (f (quote (("no-checksum") ("double"))))))

