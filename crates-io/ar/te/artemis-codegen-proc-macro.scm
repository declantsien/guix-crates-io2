(define-module (crates-io ar te artemis-codegen-proc-macro) #:use-module (crates-io))

(define-public crate-artemis-codegen-proc-macro-0.1.0-alpha.0 (c (n "artemis-codegen-proc-macro") (v "0.1.0-alpha.0") (d (list (d (n "artemis_codegen") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15l8lvsmx9j2ya8z1lx68gd7pnrm9xz1wc9nb6qqksdky3dbl7rc")))

(define-public crate-artemis-codegen-proc-macro-0.1.0 (c (n "artemis-codegen-proc-macro") (v "0.1.0") (d (list (d (n "artemis_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04qfaqd6ghf3zk1c78mhccpl3frqqsjjxjbhlfx07zz7dnycbn91")))

