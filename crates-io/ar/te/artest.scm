(define-module (crates-io ar te artest) #:use-module (crates-io))

(define-public crate-artest-0.1.0 (c (n "artest") (v "0.1.0") (h "1alyg98yj2j6hipkgjry1sr54zh054iywawhkp251d3676kr14fy")))

(define-public crate-artest-0.2.0 (c (n "artest") (v "0.2.0") (h "0my48ixgjwz58559ydfzk7n0bnb0jcq8ccg9kfykxfg2llylx9jj")))

(define-public crate-artest-0.3.0 (c (n "artest") (v "0.3.0") (h "0m5axmipc9h1nhkx89rmyzjcfi12zjwpbpvyd1dpnl7zfk5b1pcv")))

