(define-module (crates-io ar gf argf) #:use-module (crates-io))

(define-public crate-argf-0.1.0 (c (n "argf") (v "0.1.0") (h "07czky9jj94kbq8rssnvykyl2rsvmhz6kwdazxh3iww8fh0ydqbi")))

(define-public crate-argf-0.2.0 (c (n "argf") (v "0.2.0") (h "1pf7pl51yd3i7asb970ny6flgh5pvb1nsx5g697vnbzjkgxwgzh9")))

