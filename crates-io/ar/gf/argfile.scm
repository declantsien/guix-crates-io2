(define-module (crates-io ar gf argfile) #:use-module (crates-io))

(define-public crate-argfile-0.1.0 (c (n "argfile") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "16rrw5nazxxzvp6707pk60kh9sn54zb8bknxa2bigm9k5jxc9rih") (f (quote (("response" "shlex") ("default"))))))

(define-public crate-argfile-0.1.1 (c (n "argfile") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "0vvii1ibkqwg5fi2cdps4gkv6a2rpd63wfybirjfm7c9al96l5ba") (f (quote (("response" "shlex") ("default"))))))

(define-public crate-argfile-0.1.2 (c (n "argfile") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "15wxmw45p7gsk6b3wwd9kzri2d719d59gqxadwbx6gvr1p8ywnhv") (f (quote (("response" "shlex") ("default"))))))

(define-public crate-argfile-0.1.3 (c (n "argfile") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "05ayzqb9v9289npldfxw4pzi1lrykls1vqlgx3cdvia0nkr558ba") (f (quote (("response" "shlex") ("default"))))))

(define-public crate-argfile-0.1.4 (c (n "argfile") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "04485v4jp8pc23dh95i5jck6mvxq2gkik8r0sczpbc78hsaqhflz") (f (quote (("response" "shlex") ("default"))))))

(define-public crate-argfile-0.1.5 (c (n "argfile") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.0") (d #t) (k 2)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.0") (d #t) (k 2)))) (h "0f6n16mdigbwpv5jlc37vp48015ncqbdi309slbs52a4jw452pr6") (f (quote (("response" "shlex") ("default")))) (r "1.60.0")))

(define-public crate-argfile-0.1.6 (c (n "argfile") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 2)) (d (n "fs-err") (r "^2.9.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "wild") (r "^2.1") (d #t) (k 2)))) (h "1xm5yq9r04k9zf0hlx47a4yv86np6iwpqczfcmg0iia15bwc91qj") (f (quote (("response" "shlex") ("default")))) (r "1.70.0")))

(define-public crate-argfile-0.2.0 (c (n "argfile") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 2)) (d (n "fs-err") (r "^2.9.0") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.0") (d #t) (k 0)) (d (n "wild") (r "^2.2") (d #t) (k 2)) (d (n "winsplit") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0d2500nw7yv6q4iib3zlp91m0vvvv8pd2f90jfmgh3h833jciidp") (f (quote (("default")))) (s 2) (e (quote (("response" "dep:winsplit")))) (r "1.74")))

