(define-module (crates-io ar ty arty) #:use-module (crates-io))

(define-public crate-arty-0.1.0 (c (n "arty") (v "0.1.0") (h "05cnn8vpary27xl0bpv75pbq6m1lfvpy2sa9hvn6cc97qy8wfg4p") (y #t)))

(define-public crate-arty-0.1.1 (c (n "arty") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "jenkins_api") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1caisxbq3gff9x7cdg5jwkn6kzgh0fxw1v7jjnsjhfivibf62pfi")))

(define-public crate-arty-0.1.2 (c (n "arty") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "jenkins_api") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0ynj70myz4i0j9rl3zx4hha33g69qlfys4k754d58vxw2cxdyhiq")))

