(define-module (crates-io ar ch archive-reader) #:use-module (crates-io))

(define-public crate-archive-reader-0.1.0 (c (n "archive-reader") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pxxazp8ki0wx8nq1xly2iacxbh770a4zby305yjmf3a4vynqm2m") (f (quote (("lending_iter"))))))

(define-public crate-archive-reader-0.2.0 (c (n "archive-reader") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09z8wpbpkxw960ick256x9d2kvqmbn09ga514f4y44g36pw3fz7k") (f (quote (("lending_iter"))))))

(define-public crate-archive-reader-0.2.1 (c (n "archive-reader") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1igj11r12xcgq4rc1qgja7vsffqigyzjdcfbn75qsngqxj1q62jr") (f (quote (("lending_iter"))))))

(define-public crate-archive-reader-0.2.2 (c (n "archive-reader") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "098zk6ijpm5irgvd5ykda2r6fzk6nrjdkcgpvz8qvbmsrd2ls18p") (f (quote (("lending_iter"))))))

(define-public crate-archive-reader-0.2.3 (c (n "archive-reader") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hhl6mjrj4sww664nzlgi8gdcd9bgba9pqwlwifn8p0h4987l0dj") (f (quote (("lending_iter"))))))

(define-public crate-archive-reader-0.3.0 (c (n "archive-reader") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kpzvmmvah7m32aary6s9hgs5vsq33qa1ibpa3ykq1yajzy3a9gh") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.3.1 (c (n "archive-reader") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11nx9r55pa7ip8r4xf7c7p5f5h2b650mkv887b27csbd5snqc43s") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.3.2 (c (n "archive-reader") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mqrh8s0fk7r70cx6clin5aqw1rmm2flnrcndp51i9spx18cs4xq") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.3.3 (c (n "archive-reader") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "127by8xkj63dm0zhpzmcfj4z49agb9jpgsbdwah325c68nhdibmy") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.4.0 (c (n "archive-reader") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06pbgcv6c212dly1hx7n840rbxwl9jd6cnz8lmspxzai5i8na36f") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.3.4 (c (n "archive-reader") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hw8pnb5zbzfrqavsrfkg6yn1aq9w419yyjzirv4r3431chi1zx3") (f (quote (("lending_iter")))) (y #t)))

(define-public crate-archive-reader-0.3.5 (c (n "archive-reader") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ijajp5sv7brlrd1f7xkyscid8yndwzpv41g16wzwryvhyixhz67") (f (quote (("lending_iter"))))))

