(define-module (crates-io ar ch archivio) #:use-module (crates-io))

(define-public crate-archivio-0.1.0 (c (n "archivio") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0d1j4h7f9avmsz05zlfqg9arag4ak3qiik4wv4byv6fn4m7y0zhf")))

(define-public crate-archivio-0.2.0 (c (n "archivio") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rssrvcdnfld7p4cskvjzshkm2rn0xz0q5r0mj9xqcv8wbn2ssbh")))

(define-public crate-archivio-0.3.0 (c (n "archivio") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dk4dzbxrycslyrqq9gswr8yc2dfag53rm08w6v5y7vrccwfzg6y")))

(define-public crate-archivio-0.4.0 (c (n "archivio") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fcpc85z987bv9bc4df1s9bl9klj78fld833zr9pq4xnzkjnc8gh")))

(define-public crate-archivio-0.5.0 (c (n "archivio") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n3qq22lkfkblz4jjlfv6akavxkak4yis6jnjcx951ybhpbp43cn")))

(define-public crate-archivio-0.6.0 (c (n "archivio") (v "0.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11mw2488kz56qrnlqwixwh5sz0588r6a7x3bk8h1m8h2id5dfkiz")))

