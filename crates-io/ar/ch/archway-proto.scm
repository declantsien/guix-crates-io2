(define-module (crates-io ar ch archway-proto) #:use-module (crates-io))

(define-public crate-archway-proto-0.1.0 (c (n "archway-proto") (v "0.1.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.18.0") (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("codegen" "prost"))) (o #t) (k 0)))) (h "18f5bkz373xjv2jy77kv7v2mrycxmmdpzk9zh8fwn33jm5w57apz") (f (quote (("grpc-transport" "grpc" "tonic/transport" "cosmos-sdk-proto/grpc-transport") ("grpc" "tonic" "cosmos-sdk-proto/grpc") ("default" "grpc-transport") ("cosmwasm" "cosmos-sdk-proto/cosmwasm"))))))

