(define-module (crates-io ar ch archivista) #:use-module (crates-io))

(define-public crate-archivista-0.1.0 (c (n "archivista") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiberius") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("compat"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1zjxzqs7rga43nwmq346lvn7rbg0f5mvfycdi86xd2ds15l54nmi")))

