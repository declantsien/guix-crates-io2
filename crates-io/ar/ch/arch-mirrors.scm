(define-module (crates-io ar ch arch-mirrors) #:use-module (crates-io))

(define-public crate-arch-mirrors-0.1.0 (c (n "arch-mirrors") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1dljl7v8knzy44zgh55wnbp54pzw45qf30jkby7v310qnmgmdhah")))

(define-public crate-arch-mirrors-0.1.1 (c (n "arch-mirrors") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yp4vr2ga7mf4gmfld0g2xbaz3whfxqjnnpd8dbs03hgwymcngxp")))

