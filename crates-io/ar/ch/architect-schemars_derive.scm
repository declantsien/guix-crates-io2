(define-module (crates-io ar ch architect-schemars_derive) #:use-module (crates-io))

(define-public crate-architect-schemars_derive-0.8.12 (c (n "architect-schemars_derive") (v "0.8.12") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17r8y872r0qkjpscb4s2vn321sny6d3jhcayk75apsvs9fixcgy5")))

(define-public crate-architect-schemars_derive-0.8.13 (c (n "architect-schemars_derive") (v "0.8.13") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bjf2np7ryicbva389wmxhl5zz9r1b3jll3fsqg4p6v7hbm383k3")))

(define-public crate-architect-schemars_derive-1.0.0 (c (n "architect-schemars_derive") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k5b6axv2rwcqshzqkv6lsgqs2p58jy8hmh57z720r69w61jqr48")))

