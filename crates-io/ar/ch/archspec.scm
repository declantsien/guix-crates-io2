(define-module (crates-io ar ch archspec) #:use-module (crates-io))

(define-public crate-archspec-0.0.0 (c (n "archspec") (v "0.0.0") (h "1cydaziaym7hhv6jnla704l8c5ibz4d7j24wflpqh6p5yshwmwkd")))

(define-public crate-archspec-0.1.0 (c (n "archspec") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysctl") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "01qf6aia5z6dvjk9h7xlfxqm9wwf7jwc8kndghm6b1ail2k6kvjg")))

(define-public crate-archspec-0.1.1 (c (n "archspec") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysctl") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1l4bm865gicc4y0mn2qvvsn5ab2f33340zsixfigfkd72f8jak6x")))

(define-public crate-archspec-0.1.2 (c (n "archspec") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysctl") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0lgnym1g3xfnqfmv0rm53cqxjjy5f6flxjb96vv7k9xw3khsingp")))

(define-public crate-archspec-0.1.3 (c (n "archspec") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysctl") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1is6yjyk38hlijhp8yyk3xq3dgg5aass7rms5h6s2mjgrzcprdlx")))

