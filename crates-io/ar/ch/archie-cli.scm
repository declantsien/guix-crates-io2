(define-module (crates-io ar ch archie-cli) #:use-module (crates-io))

(define-public crate-archie-cli-0.1.0 (c (n "archie-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "b64") (r "^0.4.0") (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)))) (h "15l8yaw7z6im7j0268abhxahkyl97qrvjyg7lg92yrjng4530dhx")))

(define-public crate-archie-cli-0.1.1 (c (n "archie-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "b64") (r "^0.4.0") (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1knh1f3yr5lk4dvmck68hdnpm0fajpnq9wsqbiiyanwpr7bz746j")))

