(define-module (crates-io ar ch arch_msgs) #:use-module (crates-io))

(define-public crate-arch_msgs-0.1.2 (c (n "arch_msgs") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "09196x3vgyvmcklg01b6vwd53drc01x49hc50xspvr95xagrl5ms")))

(define-public crate-arch_msgs-0.2.0 (c (n "arch_msgs") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0087898xmbc8495ppkf98m089swnndj1c8ch5j8f8rnhhbgg46ni")))

