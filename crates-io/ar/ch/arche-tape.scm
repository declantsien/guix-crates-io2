(define-module (crates-io ar ch arche-tape) #:use-module (crates-io))

(define-public crate-arche-tape-0.1.0 (c (n "arche-tape") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1yscldhkflx2bli7gsfkq341g0iaxdik05jycwc5chq479xb60ld")))

(define-public crate-arche-tape-0.1.1 (c (n "arche-tape") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0m98gm2yhxnd2l5r8aik3mkr69rrnaqy4yk4j7z7bq9lyris56i0")))

(define-public crate-arche-tape-0.1.2 (c (n "arche-tape") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1blpjj7jdd934kp3crlhf5rv2i1jnskhrqir0075s6s36ywrx33a")))

