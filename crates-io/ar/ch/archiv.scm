(define-module (crates-io ar ch archiv) #:use-module (crates-io))

(define-public crate-archiv-0.1.0 (c (n "archiv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 0)))) (h "0sknk67kch5mlzq78jir2pdki29czgxypb9j8nckrq7lr9nqnmxx") (f (quote (("bin" "anyhow" "clap"))))))

(define-public crate-archiv-0.1.1 (c (n "archiv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 0)))) (h "1x0af80pc26fjc109w126milqk61606fh09d5d89p2y8wkqw26h7") (f (quote (("bin" "anyhow" "clap"))))))

(define-public crate-archiv-0.1.2 (c (n "archiv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 0)))) (h "0nh8bsn4vva595dn4awdrkjl6ak4ii354iyjd62gp315gzb1rgxy") (f (quote (("bin" "anyhow" "clap"))))))

(define-public crate-archiv-0.1.3 (c (n "archiv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.13") (d #t) (k 0)))) (h "0zjklw3k9xsj0wx9byzq5bplkcw3wggp8xhl1kmqz3sldvh7j8r9") (f (quote (("bin" "anyhow" "clap"))))))

