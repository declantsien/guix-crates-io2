(define-module (crates-io ar ch arch_test_core) #:use-module (crates-io))

(define-public crate-arch_test_core-0.1.0 (c (n "arch_test_core") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "1296hz5nsn08fvadh443z14d05ggljhwfpcd3gz5xgsdix4y74jd")))

(define-public crate-arch_test_core-0.1.1 (c (n "arch_test_core") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "1pqcml77i0va0cmyxvyp75p07mfsijj1ly4d5cgb392nkd21m8jn")))

(define-public crate-arch_test_core-0.1.2 (c (n "arch_test_core") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "0p4aa2p8cchdwcjqawz1xi8fpqdngi8slyi5bshnpcygvyix21xx")))

(define-public crate-arch_test_core-0.1.3 (c (n "arch_test_core") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "1gx3wllzm3mlqg80wkixbj115x5y8h5x95k7842d49ahlhm4yb7h")))

(define-public crate-arch_test_core-0.1.4 (c (n "arch_test_core") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "100ljhlzq9dvp1xbax26vda1pdqvblb4ca3q91kw6j3phccvc6gq")))

(define-public crate-arch_test_core-0.1.5 (c (n "arch_test_core") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.59") (d #t) (k 0)) (d (n "velcro") (r "^0.5.3") (d #t) (k 0)))) (h "1x97ja9cmkqyplmbxdm8l4dgdk713d80y71m7xg0ik5lk3bgsgdp")))

