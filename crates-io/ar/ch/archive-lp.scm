(define-module (crates-io ar ch archive-lp) #:use-module (crates-io))

(define-public crate-archive-lp-0.2.2 (c (n "archive-lp") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0w8g3z9hz0vms940bxmv6zpymhfmpxbziardczzyx64d4x3x6pbd")))

(define-public crate-archive-lp-0.2.3 (c (n "archive-lp") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0f1jy19zyq60kjkndd93jmqkxdhhy6b64pqb4liapvh7r78lb9hr") (f (quote (("indicate" "indicatif"))))))

