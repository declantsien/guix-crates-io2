(define-module (crates-io ar ch archetype) #:use-module (crates-io))

(define-public crate-archetype-0.1.0 (c (n "archetype") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "similar") (r "^2.3.0") (d #t) (k 0)))) (h "1gi07pm627g17a55vqijzfy4r56sp0wz7iq668dck03024y9gj3i")))

(define-public crate-archetype-0.2.0 (c (n "archetype") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "similar") (r "^2.3.0") (d #t) (k 0)))) (h "1j11208zzvlx5f11x56dd0qvffbad5pr41ijdg4x5hx6a05b08n0")))

