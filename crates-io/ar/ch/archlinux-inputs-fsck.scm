(define-module (crates-io ar ch archlinux-inputs-fsck) #:use-module (crates-io))

(define-public crate-archlinux-inputs-fsck-0.1.0 (c (n "archlinux-inputs-fsck") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread" "process"))) (d #t) (k 0)))) (h "1vcay66nyv5phlxgd7nlzyz2166cdbb5v1kvrig8c8f203074sdn")))

