(define-module (crates-io ar ch archivelib-sys) #:use-module (crates-io))

(define-public crate-archivelib-sys-0.1.0 (c (n "archivelib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0bbf7df6iqmhzs4p9gsdvrnaf8bqilj9hiks1jz34x390q09wi33")))

(define-public crate-archivelib-sys-0.1.1 (c (n "archivelib-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0k4wfa13554x5hxh27cb5a5v44kfph7zhmfgzz21gl8kx33ds7rv")))

(define-public crate-archivelib-sys-0.2.0 (c (n "archivelib-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0l6dbhr2q6asa1rvrlwhrnqqw6pz077znw87im7h5akaab5ycipk")))

