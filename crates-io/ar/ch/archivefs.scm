(define-module (crates-io ar ch archivefs) #:use-module (crates-io))

(define-public crate-archivefs-0.1.0 (c (n "archivefs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "polyfuse") (r "^0.4.1") (d #t) (k 0)) (d (n "relative-path") (r "^1.3.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1110r4gsrxi9layn7cpybx2pcnqrgvqfwry3i2bw5lphh7sljijm")))

(define-public crate-archivefs-1.0.0 (c (n "archivefs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "polyfuse") (r "^0.4") (d #t) (k 0)) (d (n "relative-path") (r "^1.3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1y8va8hx4xfdcn0l99k2iqm3v7lq70ryz7lqv1n7prkhdrnwsz11")))

(define-public crate-archivefs-1.0.1 (c (n "archivefs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "daemonize") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "polyfuse") (r "^0.4") (d #t) (k 0)) (d (n "relative-path") (r "^1.3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0bm99vwilhq85m9dxykjl3j69q65nagdfjc7mvw8b4hfacij28h2")))

