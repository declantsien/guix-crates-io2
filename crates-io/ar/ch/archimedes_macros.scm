(define-module (crates-io ar ch archimedes_macros) #:use-module (crates-io))

(define-public crate-archimedes_macros-0.2.0 (c (n "archimedes_macros") (v "0.2.0") (d (list (d (n "archimedes_task_handler") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 2)))) (h "132pq2j44b2srq8wpp5669hc7148p8nc5lp5wgmr32z6ykz1ysyg")))

