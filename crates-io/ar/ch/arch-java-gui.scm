(define-module (crates-io ar ch arch-java-gui) #:use-module (crates-io))

(define-public crate-arch-java-gui-0.1.0 (c (n "arch-java-gui") (v "0.1.0") (d (list (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_16"))) (d #t) (k 0)))) (h "1xldl8kv93b4zwm3hsaxdah3vx2q05ii7qd1svf15yc60q3cy08x") (y #t)))

(define-public crate-arch-java-gui-0.1.1 (c (n "arch-java-gui") (v "0.1.1") (d (list (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "os_type") (r "^2.2") (d #t) (k 0)))) (h "07q8m47ni0fsgcqdrbbdk5nw317qy0rx4y4pmry0zsz4yyvwbzxv") (y #t)))

(define-public crate-arch-java-gui-0.1.2 (c (n "arch-java-gui") (v "0.1.2") (d (list (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "os_type") (r "^2.2") (d #t) (k 0)))) (h "1imq9vpa716cjv3w9gfjmia6767zbv796f5nm6ws9rp6c566cgw3") (y #t)))

(define-public crate-arch-java-gui-0.1.3 (c (n "arch-java-gui") (v "0.1.3") (d (list (d (n "gio") (r "^0.7.0") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7.1") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "os_type") (r "^2.2") (d #t) (k 0)))) (h "18mmci49g8ibzjbki0l1gdcjirwmbn2frjhm1l265sglvjsjawmc") (y #t)))

