(define-module (crates-io ar ch architect-derive) #:use-module (crates-io))

(define-public crate-architect-derive-0.2.0 (c (n "architect-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17cbq24zsna682b2gvzahafbbn4170kkb543lnv4yz92rqdb8gv0")))

(define-public crate-architect-derive-0.3.0 (c (n "architect-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ycf2mjl2sa818c578rp905r2drfhphsiv6rd79ilffq6raj0wl6")))

