(define-module (crates-io ar ch archival-dedupe) #:use-module (crates-io))

(define-public crate-archival-dedupe-1.0.0 (c (n "archival-dedupe") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liso") (r "^1.0.2") (d #t) (k 0)) (d (n "lsx") (r "^1.1.2") (f (quote ("sha256"))) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)))) (h "1zsrkz7x8iz53wbxkhcrrhfqd7f4yihrlm7c4b2y4b4vw7kf28np")))

