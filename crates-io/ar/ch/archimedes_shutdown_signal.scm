(define-module (crates-io ar ch archimedes_shutdown_signal) #:use-module (crates-io))

(define-public crate-archimedes_shutdown_signal-0.1.1 (c (n "archimedes_shutdown_signal") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "191jdscf7gr6hl510qwn9kki3lrmg6mv38fpq0fphflw5834yvda")))

(define-public crate-archimedes_shutdown_signal-0.2.1 (c (n "archimedes_shutdown_signal") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0yzvbkqwhf1pvmma3wxffx04vsyix0yck5d9ch0fc80vm5lw8n7g")))

(define-public crate-archimedes_shutdown_signal-0.2.2 (c (n "archimedes_shutdown_signal") (v "0.2.2") (d (list (d (n "archimedes_crontab_types") (r "^0.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1vpg8n26wqwm74aqnv1gqsjb9m27rhwnnjbxdxaayghhf3kj1yq4")))

(define-public crate-archimedes_shutdown_signal-0.2.3 (c (n "archimedes_shutdown_signal") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0xv2ak1cf6j8c5n5h611lvbzyhfb25k16p6lfyfvzlhw7m6nmlcl")))

(define-public crate-archimedes_shutdown_signal-0.2.4 (c (n "archimedes_shutdown_signal") (v "0.2.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "10ll2f5sgin4xkz6zng11v75py2xpqr13f4jxvri00iy1p9s1z7l")))

(define-public crate-archimedes_shutdown_signal-0.3.0 (c (n "archimedes_shutdown_signal") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "058prn3mg0mi8xjx5m4pmk877w1wx660vgiz27ig6rax22fsa2lh")))

