(define-module (crates-io ar ch archlinux-repo-parser) #:use-module (crates-io))

(define-public crate-archlinux-repo-parser-0.1.0 (c (n "archlinux-repo-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m06ia2bixq8zri1dw3b2pbg9b125d762s5az3ak1aqha0zvv9v4")))

(define-public crate-archlinux-repo-parser-0.1.1 (c (n "archlinux-repo-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s0yzcbi9lqw18i1h3wmpznfw4qf7389q2dm971a7c4mfxczq6xl")))

(define-public crate-archlinux-repo-parser-0.1.2 (c (n "archlinux-repo-parser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wx8w2pwqxmlssxvv2hqk403nh5hagjiyks1hc7hrirnfyf1zijn")))

(define-public crate-archlinux-repo-parser-0.1.3 (c (n "archlinux-repo-parser") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "10dccqja3qb88jc12wpqs4r2cy8khidrqsxv7zk44q9b7ic20g5q")))

(define-public crate-archlinux-repo-parser-0.1.4 (c (n "archlinux-repo-parser") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ds114mzwg7faz7sgknrignsxp0mmr03wsyjh1xdzrigp7pl6hlv")))

(define-public crate-archlinux-repo-parser-0.1.5 (c (n "archlinux-repo-parser") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fxcnbb2jffvayzrzzyy2ya1cmkzqc4f9nvf29h18b18nh1pgjvy")))

(define-public crate-archlinux-repo-parser-0.1.6 (c (n "archlinux-repo-parser") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "00wlpfhw9z28h4awr6s3392ghcyy734csx82qrshy3w9b78r79z9")))

