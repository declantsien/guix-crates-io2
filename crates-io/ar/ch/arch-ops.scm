(define-module (crates-io ar ch arch-ops) #:use-module (crates-io))

(define-public crate-arch-ops-0.1.0 (c (n "arch-ops") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "target-tuples") (r "^0.5.2") (d #t) (k 0)))) (h "1pmccc8zdk3wssivcadiiizgwv03bvb061jdd994wdbkc3zr8mzj") (f (quote (("x86") ("w65") ("riscv") ("propeller2") ("propeller") ("m6502") ("default-archs" "x86" "arm" "aarch64" "w65" "riscv" "propeller" "propeller2" "m6502" "clever") ("default" "default-archs") ("clever") ("arm") ("all-archs" "default-archs") ("all" "all-archs") ("aarch64")))) (r "1.56")))

(define-public crate-arch-ops-0.1.1 (c (n "arch-ops") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "target-tuples") (r "^0.5.5") (d #t) (k 0)))) (h "1mb7dw1jhfa18v6m60mrknsvlgv22hphhfjw6m075i51d5x8x1q1") (f (quote (("z80") ("x86") ("w65" "m6502") ("riscv") ("propeller2") ("propeller") ("m68k") ("m6502") ("default-archs" "x86" "arm" "aarch64" "w65" "riscv" "propeller" "propeller2" "m6502" "clever" "m68k") ("default" "default-archs") ("clever") ("arm") ("all-archs" "default-archs") ("all" "all-archs" "z80") ("aarch64")))) (r "1.56")))

