(define-module (crates-io ar ch arch-into) #:use-module (crates-io))

(define-public crate-arch-into-0.0.1-alpha.1 (c (n "arch-into") (v "0.0.1-alpha.1") (h "19dfz9igcrfdrhxg7rflcpg3wr7fx3dfydq51q2gfk98cldllvdv") (f (quote (("default") ("arch-64" "arch-32") ("arch-32"))))))

(define-public crate-arch-into-0.0.1-alpha.2 (c (n "arch-into") (v "0.0.1-alpha.2") (h "0cw240dqv88n9cjn9f2rlm6b6hyifx4nxccjzyk7hdpffzdndvwf") (f (quote (("default") ("arch-64" "arch-32") ("arch-32"))))))

(define-public crate-arch-into-0.0.1-alpha.3 (c (n "arch-into") (v "0.0.1-alpha.3") (h "1q1bcjd715hnxkc9106w878jb68lzx9wdgbvrhm7yf5cl3yjmmyr") (f (quote (("default") ("arch-64" "arch-32") ("arch-32"))))))

(define-public crate-arch-into-0.0.1-alpha.4 (c (n "arch-into") (v "0.0.1-alpha.4") (h "0xi0dlg5r1w45ji8j0sb160wfd12wp7z2xb113h860300i93g728") (f (quote (("default") ("arch-64") ("arch-32"))))))

(define-public crate-arch-into-0.0.1-alpha.5 (c (n "arch-into") (v "0.0.1-alpha.5") (d (list (d (n "const_panic") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1a2frgzrj623lrp87ma0342w2i2xpv2b0x5ikdnk3vw10zj5n9fv") (f (quote (("no-arch-64") ("no-arch-32") ("default"))))))

