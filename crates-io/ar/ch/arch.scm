(define-module (crates-io ar ch arch) #:use-module (crates-io))

(define-public crate-arch-0.0.1 (c (n "arch") (v "0.0.1") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "07kp8dw8i4mqd2jk31pyj2qz3c7jyli21hlz08zhsh6478zjf8wn")))

(define-public crate-arch-0.0.2 (c (n "arch") (v "0.0.2") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1y5wjn9aj0sdwgzwc56qi9108ypcxf9939khyabj7aq67sbcdgg0")))

(define-public crate-arch-0.1.0 (c (n "arch") (v "0.1.0") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1alhmlvj6hsfz0py9rm4q9hliwaiyl0xfqpghafzv3z0ggjzfq8f")))

(define-public crate-arch-0.1.1 (c (n "arch") (v "0.1.1") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0v5d8bvf16wis350d8x7ijycq45mnws88502ni5m2wbvq7d9nb3n")))

(define-public crate-arch-0.1.2 (c (n "arch") (v "0.1.2") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1v8nkzbh4d0aamnrw8p43m4svwpmxpdh540v334cpm365brm731s")))

(define-public crate-arch-0.3.0 (c (n "arch") (v "0.3.0") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1rdl492lksvlz0fnx16d1n2qh4p7816rj20mcnraz25vdhk1dh4j")))

(define-public crate-arch-0.4.0 (c (n "arch") (v "0.4.0") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "06kdz7wphljljyk8j4psv2jk6m6pcfswrf1ivy0p9pvghpnqgzyp")))

(define-public crate-arch-0.5.0 (c (n "arch") (v "0.5.0") (d (list (d (n "inquire") (r "^0.7.4") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0710hhdshrag9c5apzzdbwkjcynq4nxqhi9dhqq446hzm7k9367h")))

