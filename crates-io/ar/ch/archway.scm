(define-module (crates-io ar ch archway) #:use-module (crates-io))

(define-public crate-archway-0.1.0 (c (n "archway") (v "0.1.0") (h "1livfyiff0dk6k3gmh3rvcmbkra84d5a3a17r1a6c08miji9mmap") (y #t)))

(define-public crate-archway-0.1.1 (c (n "archway") (v "0.1.1") (h "1vcg6l8agfcc0bb9rs8gvzr1d9r0wfmzvf86jvn99k7hnfwp6jcj") (y #t)))

(define-public crate-archway-0.1.2 (c (n "archway") (v "0.1.2") (h "0km83yzrwnr2rdxjqki9gx5w4gc0kfq84w7l72llm3ahvrhfjcq5")))

(define-public crate-archway-0.2.0 (c (n "archway") (v "0.2.0") (h "15h1ayhi1f123zbn4jc0s7h0cx5wmaxani02pm8hslwy4i5dpzrc")))

(define-public crate-archway-0.3.0 (c (n "archway") (v "0.3.0") (h "039aakv12yyv4f9q2z6x9wcrlx8d1pnbb3w62hx4kqnyp7mpa0cs")))

(define-public crate-archway-0.3.1 (c (n "archway") (v "0.3.1") (h "1ihczdrx54w8qhdwp6bdrsjvpjarimffg40jlhw8qaln72x4w5sf")))

