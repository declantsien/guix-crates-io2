(define-module (crates-io ar ch archie) #:use-module (crates-io))

(define-public crate-archie-1.0.1 (c (n "archie") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1fy7hgc7s75l17pkqpi7v089ng5ixaisk9r8g5yhcjrs1ly99287")))

(define-public crate-archie-1.0.2 (c (n "archie") (v "1.0.2") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1hs5n7ib9rlc02nv6rfrqqa3z2qjb20my1cgdznqn2x53ysqi670")))

