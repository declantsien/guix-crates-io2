(define-module (crates-io ar ch arch_papers) #:use-module (crates-io))

(define-public crate-arch_papers-0.1.0 (c (n "arch_papers") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0jsslrzg84a2bdhb8b38qclryp86007c04rgl00rf97ajqayvj6c")))

(define-public crate-arch_papers-0.2.0 (c (n "arch_papers") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "resvg") (r "^0.35.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "usvg") (r "^0.35.0") (d #t) (k 0)))) (h "1nbciy7qzgsh6a5g5yhr7mqk7g1z8y63pmlw8wcjvwszwddzixcg")))

(define-public crate-arch_papers-0.3.0 (c (n "arch_papers") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "resvg") (r "^0.35.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.10.0") (d #t) (k 0)) (d (n "usvg") (r "^0.35.0") (d #t) (k 0)))) (h "0wxcwinf7p82wpmxx4nylakmcvzfrx7gi60jjw0lw7z0xxvf6zba")))

