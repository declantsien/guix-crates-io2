(define-module (crates-io ar ch archway-bindings) #:use-module (crates-io))

(define-public crate-archway-bindings-0.1.0 (c (n "archway-bindings") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "14kh4nlip99g2vpxlg8c2hv6jr4s8z89r4arshbxi4wsqfway57l")))

(define-public crate-archway-bindings-0.2.0 (c (n "archway-bindings") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1cvval6dk3kcdxn2wzklwni20vhhaqr28mf7dqrdszvs7l41s6nx") (f (quote (("staking" "cosmwasm-std/staking") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-archway-bindings-0.2.1 (c (n "archway-bindings") (v "0.2.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "095z4sy530r8j8pcwvqw4ac0s5zj0rrwiw0rim22ncqqlc0d2v3z") (f (quote (("staking" "cosmwasm-std/staking") ("backtraces" "cosmwasm-std/backtraces"))))))

