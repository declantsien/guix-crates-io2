(define-module (crates-io ar ch archimedes_crontab_types) #:use-module (crates-io))

(define-public crate-archimedes_crontab_types-0.1.0 (c (n "archimedes_crontab_types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1zcs725dhwyzqc4pwn3lly7vag04kj9lvdavfpv181j3kcg9mjvx")))

(define-public crate-archimedes_crontab_types-0.2.0 (c (n "archimedes_crontab_types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "11pjq9mxndy80ikz04l6v64k84h24c1x078sj6nkipzzhvswq8hv")))

(define-public crate-archimedes_crontab_types-0.3.0 (c (n "archimedes_crontab_types") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "189zd5qp4qywcr1g8h0bs0wyggwk0ni9cvwc5y75c6zk2g3aq210")))

(define-public crate-archimedes_crontab_types-0.4.0 (c (n "archimedes_crontab_types") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0m2n7fky208ikvxz33i01jdhv0p9yfvz5kwipdhzxx0rnhfsfhsd")))

(define-public crate-archimedes_crontab_types-0.5.0 (c (n "archimedes_crontab_types") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 2)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "12ng6c374snw582qw95k7n5l0rwa3a74wik6rl88yzad8xs9g428")))

