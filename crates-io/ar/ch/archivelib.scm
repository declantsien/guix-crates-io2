(define-module (crates-io ar ch archivelib) #:use-module (crates-io))

(define-public crate-archivelib-0.1.1 (c (n "archivelib") (v "0.1.1") (d (list (d (n "archivelib-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "128cjvw8883hgqz2b5i76yvqmqkv9bsk81a04y6i84iiy30v2i63")))

(define-public crate-archivelib-0.1.2 (c (n "archivelib") (v "0.1.2") (d (list (d (n "archivelib-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1cvxlb17gx977cm5yfyz4zs8xjzsmhrvq0ljybsv4y01wi3c4dxm")))

(define-public crate-archivelib-0.2.0 (c (n "archivelib") (v "0.2.0") (d (list (d (n "archivelib-sys") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "archivelib-sys") (r "^0.2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "humthreads") (r "^0.1.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1ssdz00j9ljrvvcvd9cik2cyd7vyy9ggzk43bndc57x09cahw8f9") (f (quote (("sys" "archivelib-sys") ("new_impl") ("default" "new_impl"))))))

