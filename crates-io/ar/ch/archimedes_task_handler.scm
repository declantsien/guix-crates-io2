(define-module (crates-io ar ch archimedes_task_handler) #:use-module (crates-io))

(define-public crate-archimedes_task_handler-0.2.0 (c (n "archimedes_task_handler") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "09nz664x5vzahcqk6srq5yycjah0m0b40p410cxmx4b0xrkia9mh")))

