(define-module (crates-io ar ch arch-mirrors-rs) #:use-module (crates-io))

(define-public crate-arch-mirrors-rs-0.1.0 (c (n "arch-mirrors-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0dmmxs9za7pcwjz68gwb57y5h7v7fns8cqsiy65pw41qgmb0syg8")))

(define-public crate-arch-mirrors-rs-0.1.1 (c (n "arch-mirrors-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0akiw655dwb1481m36wxc5ph389r02sippxvdjxhss4x52dmb1h8")))

