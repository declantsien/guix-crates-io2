(define-module (crates-io ar ch archive_is) #:use-module (crates-io))

(define-public crate-archive_is-0.1.0 (c (n "archive_is") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0zgdlhrxd1r9fnljyhr54x9pijjjc1pg2m9xg6yvk0ms48m5z50w")))

(define-public crate-archive_is-0.2.0 (c (n "archive_is") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1lfdqq3zw47b651yha5hk781w25d1mlh0lq6zqy3lpak6i4szjd3")))

