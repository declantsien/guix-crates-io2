(define-module (crates-io ar gy argyle) #:use-module (crates-io))

(define-public crate-argyle-0.1.0 (c (n "argyle") (v "0.1.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "169q98cw1w8n809g6ah43awq1r20d3a4s0agfvixkafxv0bidvym")))

(define-public crate-argyle-0.2.0 (c (n "argyle") (v "0.2.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "1bvwyfs84g25y328mnlxn5mwqic9q0rb9dldncn821w7n9dvq2mf")))

(define-public crate-argyle-0.3.0 (c (n "argyle") (v "0.3.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "1gapbbbzq4827x9d5s4kvh61cvd1fc6qsix1rqxb3r4xf87jmzfx")))

(define-public crate-argyle-0.3.1 (c (n "argyle") (v "0.3.1") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "01dsfmgp71aap04niir9sy4h6n0frhd8lvj487zgqpq0wrsh9y4q")))

(define-public crate-argyle-0.4.0 (c (n "argyle") (v "0.4.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "0vl7qi7w4iy0yfx49hg5zky23lr3fhq48hiswy33f36r8ivyyjah") (f (quote (("dynamic-help") ("default"))))))

(define-public crate-argyle-0.4.1 (c (n "argyle") (v "0.4.1") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "1dk40w8lcfd7ijyd4q7fkwlwa55ilcd24qyyd3c3i5knljxh2xmz") (f (quote (("dynamic-help") ("default"))))))

(define-public crate-argyle-0.4.2 (c (n "argyle") (v "0.4.2") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "0im25lnhm9rr78zwg92ginwsipm09swwx4ajqjsxadicilkgk8jw") (f (quote (("dynamic-help") ("default"))))))

(define-public crate-argyle-0.5.0 (c (n "argyle") (v "0.5.0") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1875bv7gjci9br52zv173s4i8w592dkz18wlv1xi9b17xcdq4vb6") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.1 (c (n "argyle") (v "0.5.1") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1abwanff8g830xlnra2nsa5dxcqp6nb57r5psam9ffi61afcq8m5") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.2 (c (n "argyle") (v "0.5.2") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "061w7g38yym2y597rbn0wd4h6s9wybclzq4rgm3s4j8vpadb5814") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.3 (c (n "argyle") (v "0.5.3") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "0v9pnsdd9dlm6601xpg52hh3c3ahc271fb333lnf882gmpdk8dm4") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.4 (c (n "argyle") (v "0.5.4") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "0x7xhdicvbkn35ab2sg2bybv7488smhkri23w66lz8prdynpnhz3") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.5 (c (n "argyle") (v "0.5.5") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1amxy64n5a3bzk7kfyn31yx68hklzcs0zgwz454vcsdndg1c1inv") (f (quote (("dynamic-help") ("default")))) (r "1.56")))

(define-public crate-argyle-0.5.6 (c (n "argyle") (v "0.5.6") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "0qm6ph9swp19bhdi7b7rprjb3yzibnrmyiasimd139r0bzb77biq") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.56")))

(define-public crate-argyle-0.6.0 (c (n "argyle") (v "0.6.0") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "15xca6sajxjiydwmq9ib04gza046bfr3agb49hp9i1z9jg8vmls6") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.56")))

(define-public crate-argyle-0.6.1 (c (n "argyle") (v "0.6.1") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "03hfx29bwms4zq6hws0js8n1l4i0n91qwkrg7czsll8nr6d05yc8") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.2 (c (n "argyle") (v "0.6.2") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1651xkhab5zn72dr2ylzlb4ijdl4cdpp15ycgjbz9avlvd8ysdkf") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.3 (c (n "argyle") (v "0.6.3") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "0m0wb9d85arbgq85aa0hl301brg71b8516zkl98cksg8603prqp7") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.4 (c (n "argyle") (v "0.6.4") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "0i8r8r3k9gmwzxb0scm8jr3gs4gh5hrf0wxf9c1vqz9rv7xr97qj") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.5 (c (n "argyle") (v "0.6.5") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)))) (h "0bfp921ngrf3cwzxi3p0fcf4jh14bdcqfgkw7vpq8j6in4c78dmj") (f (quote (("dynamic-help") ("docsrs") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.6 (c (n "argyle") (v "0.6.6") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)))) (h "0fpfsjr3ixfa53iyry2qcxqcqd6c3ql4d4lmp41hniqdv6p75np2") (f (quote (("dynamic-help") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.7 (c (n "argyle") (v "0.6.7") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)))) (h "1hc5r1262bp7kk0jjnfac0qmp3qn4ljxhmnhaw1ji6w22l9jabsl") (f (quote (("dynamic-help") ("default")))) (r "1.62")))

(define-public crate-argyle-0.6.8 (c (n "argyle") (v "0.6.8") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0iaijalfzqihjy5dl3rlx5s46blv8c1q2rk0y7100k346p400yvs") (f (quote (("dynamic-help") ("default")))) (r "1.62")))

(define-public crate-argyle-0.7.0 (c (n "argyle") (v "0.7.0") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0bqjxw8i7nyc4adv2jnrqyfbmsv02ilcmcgfy9vwl94zcr4za71n") (f (quote (("dynamic-help") ("default")))) (r "1.70")))

(define-public crate-argyle-0.7.1 (c (n "argyle") (v "0.7.1") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0r0ibmrjfvmc5sk03vh6aknhrcwjhcf6yj0218mrprcs9gs5p7lc") (f (quote (("dynamic-help") ("default")))) (r "1.70")))

(define-public crate-argyle-0.7.2 (c (n "argyle") (v "0.7.2") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "0sar1hq3gqcyal8pzgl80pqiv3nnvvp1rx4rd6d7xrnk2mxs8xnj") (f (quote (("dynamic-help") ("default")))) (r "1.70")))

