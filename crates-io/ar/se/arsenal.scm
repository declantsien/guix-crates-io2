(define-module (crates-io ar se arsenal) #:use-module (crates-io))

(define-public crate-arsenal-0.0.0-alpha.0 (c (n "arsenal") (v "0.0.0-alpha.0") (h "1fbiljnpxn483avmnr9mrz3anfvm0d0bw7xq2zqy3liwwa59y7ff")))

(define-public crate-arsenal-0.0.1 (c (n "arsenal") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0dcyngkkrpmlgqjdrqcvzc3yyb851h13q622x88vc0bx8kccjcc8")))

