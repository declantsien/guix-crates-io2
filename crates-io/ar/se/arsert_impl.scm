(define-module (crates-io ar se arsert_impl) #:use-module (crates-io))

(define-public crate-arsert_impl-0.0.2 (c (n "arsert_impl") (v "0.0.2") (d (list (d (n "arsert_failure") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.24") (f (quote ("printing" "parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0c1lwmvln4nryg413ahax4qvrik9ym7gkahz6cl52kdjfqii9skn") (y #t)))

