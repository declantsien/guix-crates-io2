(define-module (crates-io ar i- ari-subscriber) #:use-module (crates-io))

(define-public crate-ari-subscriber-0.0.1 (c (n "ari-subscriber") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "console-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1.34") (f (quote ("full" "tracing"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0cd3iy8s7mn876y3y6hwz4g43h2q3b83pzd90dwr51qxf7x1pv0n") (r "1.65.0")))

(define-public crate-ari-subscriber-0.0.2 (c (n "ari-subscriber") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "console-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1.34") (f (quote ("full" "tracing"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0d677s9sap0fqkzd8784mxicxah3fvjsw9gsvgd6gb9rxcd288n8") (r "1.65.0")))

