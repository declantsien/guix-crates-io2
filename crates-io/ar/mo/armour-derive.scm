(define-module (crates-io ar mo armour-derive) #:use-module (crates-io))

(define-public crate-armour-derive-0.1.0 (c (n "armour-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bd5nn2xdih4xqygc2whm8iasgj0fnyjfmwysi9hz42ingn2yr4g")))

(define-public crate-armour-derive-0.1.1 (c (n "armour-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "049bw5k0ifng26jzankdj6bfw0rvq78l329am30ls7gawdjhph87")))

(define-public crate-armour-derive-0.6.0 (c (n "armour-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ln1mjjk65hilivilmfxrvk0zdy2x68p8kfg5zd9dqlq09lgh8gb")))

(define-public crate-armour-derive-0.7.0 (c (n "armour-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0079b6si1qgjivcq6b0rh4hsqz7blpd1bzq9cqpqn5qzqsgag323")))

(define-public crate-armour-derive-0.8.0 (c (n "armour-derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06j6ik97vfnl4hjk23lv2m2drv05fmk4qzz6ipxbm2bkfvy9px5j")))

