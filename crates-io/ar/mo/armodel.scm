(define-module (crates-io ar mo armodel) #:use-module (crates-io))

(define-public crate-armodel-0.1.0 (c (n "armodel") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "02pk5lg1j2lmffz7hcsqa86lvmji57hgninqnv4ryca6cc5p46a1")))

(define-public crate-armodel-0.1.1 (c (n "armodel") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "09iw1sc5hw955xvassyzirp0nbmvbc0q7l2asga0ggrglhvmckfi")))

