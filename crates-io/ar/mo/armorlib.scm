(define-module (crates-io ar mo armorlib) #:use-module (crates-io))

(define-public crate-armorlib-0.2.0 (c (n "armorlib") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "~2.29.4") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "05z8kxjg3v46q0xpj21y7aspk87iy0g1z4bydxl2xb44m6774q8z")))

(define-public crate-armorlib-0.2.1 (c (n "armorlib") (v "0.2.1") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "~2.29.4") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "0f3a911zfcxmahh7c5s5hayzsv4672p57xvl4l4jhp99lklws5if")))

(define-public crate-armorlib-0.2.2 (c (n "armorlib") (v "0.2.2") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "~2.29.4") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "1iv7zwypvaqb4g4m1karwnbhyrfaw37jiy0ay6xl1xz87mizcv41")))

(define-public crate-armorlib-0.2.3 (c (n "armorlib") (v "0.2.3") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "clap") (r "~2.29.4") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "07qbzgm7156j2dsl9p3a59jb6h8gdnfaqsgiqx6j2n6zbmvlc8gy")))

