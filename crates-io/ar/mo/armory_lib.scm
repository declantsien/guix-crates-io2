(define-module (crates-io ar mo armory_lib) #:use-module (crates-io))

(define-public crate-armory_lib-0.1.0 (c (n "armory_lib") (v "0.1.0") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1z67i1xy1w6zdvp83hk6rxjd7prfx8cfmav6ykllka1sq334x97x")))

(define-public crate-armory_lib-0.3.2 (c (n "armory_lib") (v "0.3.2") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0m08cs5ri29alk2x7qnzlpsm7v2q6d3s572cqdrzwk9vbw0nv5ss")))

(define-public crate-armory_lib-0.3.3 (c (n "armory_lib") (v "0.3.3") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0wpsnrp3dc5fmipgppf4p1za69w19cvpnwsgfrsm3vagkngnsdsf")))

(define-public crate-armory_lib-0.3.4 (c (n "armory_lib") (v "0.3.4") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0sqpck5hpisd0ng1i7r35kdihciihxjkz54dn8119bfd2iixbik8")))

(define-public crate-armory_lib-0.3.5 (c (n "armory_lib") (v "0.3.5") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0029v6hqwynycyvj23dgypn5sp8mbrsgaylzbzkwzmmk8zbdx3gy")))

(define-public crate-armory_lib-0.3.10 (c (n "armory_lib") (v "0.3.10") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0kgy5k5ixw4sr1xavrjsxshd4g167hcwzq0i6h54j6b6ji0c1a90")))

(define-public crate-armory_lib-0.3.11 (c (n "armory_lib") (v "0.3.11") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0m0s5ry1api7530p1r2v52dqcb9m6ggw4jb6m2z3phl2dhcl6sz8")))

(define-public crate-armory_lib-0.3.13 (c (n "armory_lib") (v "0.3.13") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "07i5v7k8hr2raijsk75jfv2l97rcg18iwb5mpzacsb49xa7ipal1")))

(define-public crate-armory_lib-0.3.16 (c (n "armory_lib") (v "0.3.16") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0lz8bg66grxdfr48x1x9r0lb656lblzr8gm5mwdi3764x6j6iyy9")))

(define-public crate-armory_lib-0.3.17 (c (n "armory_lib") (v "0.3.17") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0sabj82njxnqc314aqz5m5r0d1lqqm59kd73792c8m3zxxkbs8fd")))

(define-public crate-armory_lib-0.3.18 (c (n "armory_lib") (v "0.3.18") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0ismxdyjari4f8ibb0vkk2qdh7bf0j25slrkzhnskc3jml3ljpbj")))

(define-public crate-armory_lib-0.3.19 (c (n "armory_lib") (v "0.3.19") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "1hdyqa1yhbsiscjd9nzwbpmaa5nqmax68qqdiig991a4c8vpwmyl")))

(define-public crate-armory_lib-0.3.20 (c (n "armory_lib") (v "0.3.20") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0813nr43lqqf0wlnil6pk7xlxc8m0h7lllis5vs3ippf7zc024sa")))

(define-public crate-armory_lib-0.3.21 (c (n "armory_lib") (v "0.3.21") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "1c8sjapm17p6f7rg079vsvqhy8ff8xs7xnvsbynfdv6nwlga0dnp")))

(define-public crate-armory_lib-0.3.22 (c (n "armory_lib") (v "0.3.22") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "1mqcp9mb584gkpkli2vk3b7rvy25lfm9iwffknvdn5f8bxm4r7dq")))

(define-public crate-armory_lib-0.3.23 (c (n "armory_lib") (v "0.3.23") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "15aml9z8scmqx0zklnc631880w47zxf9gg4adhhziv4qdwspczxy")))

(define-public crate-armory_lib-0.3.24 (c (n "armory_lib") (v "0.3.24") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "0jwplkbvgvi6fikgl2cj8h18cdxx0jsfjj6m0r7kyz49znq7y05j")))

(define-public crate-armory_lib-0.3.28 (c (n "armory_lib") (v "0.3.28") (d (list (d (n "cargo") (r "^0.61.1") (d #t) (k 0)) (d (n "retry") (r "^1.3.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.3") (d #t) (k 0)))) (h "16xmilp9csg6f4f2pz4gxnql03k7ngv94lvlx1wlyap4m3a04lk8")))

(define-public crate-armory_lib-0.3.29 (c (n "armory_lib") (v "0.3.29") (d (list (d (n "cargo") (r "^0.71.0") (d #t) (k 0)) (d (n "retry") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.10") (d #t) (k 0)))) (h "147iyy9qbslk16lk3ysg902p3bcninkb28m9m7j1353cdggbbssp")))

(define-public crate-armory_lib-0.3.30 (c (n "armory_lib") (v "0.3.30") (d (list (d (n "cargo") (r "^0.71.0") (d #t) (k 0)) (d (n "retry") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.10") (d #t) (k 0)))) (h "1s9gbnnc6hsfv99pb7pys12nygj5l2dcjz37yabmrh3206z3xkmp")))

(define-public crate-armory_lib-0.4.0 (c (n "armory_lib") (v "0.4.0") (d (list (d (n "cargo") (r "^0.71.0") (d #t) (k 0)) (d (n "retry") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.10") (d #t) (k 0)))) (h "1qilqq7clx9ihywzmdn7kh9dipnhi6hf38rnx0c6szr7znxgav57")))

(define-public crate-armory_lib-0.4.1 (c (n "armory_lib") (v "0.4.1") (d (list (d (n "cargo") (r "^0.71.0") (d #t) (k 0)) (d (n "retry") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.10") (d #t) (k 0)))) (h "16pif8xvaj8ly9fq5kk0hpkcc5lgsj7l8yz0kjzmhkw4pdr1p68s")))

