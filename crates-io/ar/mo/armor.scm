(define-module (crates-io ar mo armor) #:use-module (crates-io))

(define-public crate-armor-0.0.0 (c (n "armor") (v "0.0.0") (h "1slx4fjpd470gvlxz0h8qh5jwp88lz5y9qzld0pbfj2wds1ixn2x")))

(define-public crate-armor-1.0.0 (c (n "armor") (v "1.0.0") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)))) (h "12vihm3vm4a3hn0qpyg1sh09zxwyyqma4f8a9hfvh583rhnygnxm")))

(define-public crate-armor-1.1.0 (c (n "armor") (v "1.1.0") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)))) (h "0xznab5cpzj04l69yhfc2j83fybkshnlqlw1ysc7zmpdq1q3ylv7")))

(define-public crate-armor-1.2.0 (c (n "armor") (v "1.2.0") (d (list (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18mgi2aqvgc864zaabr4k888s1v6dgg134ws22bs78a37rly78zh")))

