(define-module (crates-io ar bo arborist-git) #:use-module (crates-io))

(define-public crate-arborist-git-0.1.0 (c (n "arborist-git") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pndna3z8agi818pjncfjc3hr2fnv12qxkklglnslq406f3zxf4c")))

(define-public crate-arborist-git-0.1.1 (c (n "arborist-git") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1376zw3m6j1sb081pqxjn1pxdwjpmybkacx317zqlzlwkijh00jd")))

