(define-module (crates-io ar bo arborist) #:use-module (crates-io))

(define-public crate-arborist-0.0.0 (c (n "arborist") (v "0.0.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1y56rvbyxrjrr7ydzrvsh8whnnil6g5af31df3zbldcf2pz7gb5i")))

(define-public crate-arborist-0.0.1 (c (n "arborist") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1fcl9bp7pxrfli51yrf0vawl9l6kkqv9adkndqfhyr80p39cgn51")))

(define-public crate-arborist-0.0.2 (c (n "arborist") (v "0.0.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0z86cbly4bihmdvhgp7ggb5dxm5qyfpz6ryx60pjfq7y3wqnzgwn")))

(define-public crate-arborist-0.0.3 (c (n "arborist") (v "0.0.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "053sj4xg4ng756kgs29yz8sf0c5n84kzw752q97wplx1zz2n7sav")))

(define-public crate-arborist-0.0.4 (c (n "arborist") (v "0.0.4") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0ghd5k6fd9g47mrxikvvwq5yi55qvhi6i368d12a0bbnx3rj2d4i")))

(define-public crate-arborist-0.0.5 (c (n "arborist") (v "0.0.5") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1k6hrrzgd226ybddzsa4587fmg2979hvabvhcy45v36mn5dzdpdd")))

