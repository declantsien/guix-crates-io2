(define-module (crates-io ar bo arborust) #:use-module (crates-io))

(define-public crate-arborust-0.1.0 (c (n "arborust") (v "0.1.0") (h "1n8y9ksikq8zg3bw9d0qmi3l0km3m85zs7hfcsk599ip7ykr85gc")))

(define-public crate-arborust-0.2.0 (c (n "arborust") (v "0.2.0") (d (list (d (n "ignore") (r "^0.4.20") (d #t) (k 0)))) (h "1ikhkd71jspn4mzb8r3hjb54ri1p6xmi519higfh3k9qy8i0m98j")))

