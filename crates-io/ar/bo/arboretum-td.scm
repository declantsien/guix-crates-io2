(define-module (crates-io ar bo arboretum-td) #:use-module (crates-io))

(define-public crate-arboretum-td-0.1.0 (c (n "arboretum-td") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (o #t) (d #t) (k 0)))) (h "0f91r36g5nwakw69n7fddlibb1ww6h2ifbv5g2xqxw3364wf6gis") (f (quote (("pace-logging" "log" "env_logger") ("handle-ctrlc" "ctrlc") ("cli" "structopt"))))))

