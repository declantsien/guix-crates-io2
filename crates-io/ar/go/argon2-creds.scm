(define-module (crates-io ar go argon2-creds) #:use-module (crates-io))

(define-public crate-argon2-creds-0.2.3 (c (n "argon2-creds") (v "0.2.3") (d (list (d (n "ammonia") (r "^3.1.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("perf-inline" "perf-dfa" "perf-literal" "perf-cache" "perf"))) (d #t) (k 0)) (d (n "rust-argon2") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "validator") (r "^0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1asia45a2mszr76a0z1r8ncai3h4dirbg591npk2dznscw726iyc")))

