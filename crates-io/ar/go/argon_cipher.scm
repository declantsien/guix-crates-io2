(define-module (crates-io ar go argon_cipher) #:use-module (crates-io))

(define-public crate-argon_cipher-0.1.0 (c (n "argon_cipher") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pwhash") (r "^0.3.0") (d #t) (k 0)))) (h "16jrval9kw0qwfdrncr1wng9g9q934vka564vx2p7j4l575v14d2") (y #t)))

