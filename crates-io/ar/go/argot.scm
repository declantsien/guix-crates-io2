(define-module (crates-io ar go argot) #:use-module (crates-io))

(define-public crate-argot-0.2.0 (c (n "argot") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.10.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0fjvaiw0zngl7qpgzbaznh734q8lmwyc1y0mzsv4bcj83c3w43hb")))

(define-public crate-argot-0.2.1 (c (n "argot") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "indicatif") (r "= 0.10.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03pi2gpgkzpghk8h49isvf9wnfpqpxrh070gj15bjr41nwrmdyz2")))

(define-public crate-argot-0.2.2 (c (n "argot") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "indicatif") (r "= 0.10.2") (d #t) (k 0)) (d (n "simple-error") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1g8ahjbzmd9fcnkjcbhgwqkwldcyxwi62515hjx27395a40x0fzk")))

