(define-module (crates-io ar go argone) #:use-module (crates-io))

(define-public crate-argone-0.1.0 (c (n "argone") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1419l36vb4sxgfws6rj7pyyr3zz14ryniz9m4kpmg5hii9mvd55c")))

(define-public crate-argone-0.2.0 (c (n "argone") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1v5g3zygzg629k3dgh678gbzsrh7gqkk8kp0d4885qiqscw8kp29")))

(define-public crate-argone-0.3.0 (c (n "argone") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "056k96lrlqr87kw6rlzys1l7p3l3sgrmzpqss2wycpcxvjp2vbdk")))

(define-public crate-argone-0.4.0 (c (n "argone") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1q5bgd0c0xdpww6xkihnjq6rxkc61rrffx5giqw4l131fib6vyx9")))

(define-public crate-argone-0.5.0 (c (n "argone") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0pbx0nj6i3n3xkqcwfjijv3qgq7iw0dzr9nkgr308mnlc553r5fw")))

(define-public crate-argone-0.5.1 (c (n "argone") (v "0.5.1") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fia5j0spj7azf4jl7ly67v8807hw5prsz5g0zcxf9yyqzyvkxlh")))

(define-public crate-argone-0.5.2 (c (n "argone") (v "0.5.2") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0kfkkxz9np2ssv6b46pxiz63r7xnh1jm64xwv9j4nsxlwqw5krd7")))

