(define-module (crates-io ar go argon2rs) #:use-module (crates-io))

(define-public crate-argon2rs-0.1.0 (c (n "argon2rs") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.16") (d #t) (k 0)))) (h "1xv05xvy9g6im3r0aq2vdy4pzdlwrb1jq21cxgj15lxs3341vh61")))

(define-public crate-argon2rs-0.2.0 (c (n "argon2rs") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.16") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.5") (d #t) (k 0)))) (h "0x05jj7r5m4bs1izxfd3wdaih5z0iz5acp0q5rfj2jx7hjbqq72x") (f (quote (("simd" "blake2-rfc/simd_asm") ("default" "simd"))))))

(define-public crate-argon2rs-0.2.1 (c (n "argon2rs") (v "0.2.1") (d (list (d (n "blake2-rfc") (r "^0.2.16") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.5") (d #t) (k 0)))) (h "1qrxr1yixbmdz8j3n2vfskfaljbwnaxvk6rg7995km4p3qfvi5x8") (f (quote (("simd" "blake2-rfc/simd_asm") ("default" "simd"))))))

(define-public crate-argon2rs-0.2.5 (c (n "argon2rs") (v "0.2.5") (d (list (d (n "blake2-rfc") (r "^0.2.16") (d #t) (k 0)) (d (n "cargon") (r "^0.0.1") (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "14mkgkrjd4b4zy92pflz6yb4j1wn2chbd8jczxknxbkdm2vb0rrz") (f (quote (("simd" "blake2-rfc/simd_asm") ("default"))))))

