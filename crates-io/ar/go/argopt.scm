(define-module (crates-io ar go argopt) #:use-module (crates-io))

(define-public crate-argopt-0.1.0 (c (n "argopt") (v "0.1.0") (d (list (d (n "argopt-impl") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "129klrclxbxw4jxghyigj2w75f4j73af1qiczy0bb8slfddf5drf")))

(define-public crate-argopt-0.1.1 (c (n "argopt") (v "0.1.1") (d (list (d (n "argopt-impl") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0djycwghly13bfq9iysg9k36k811kb6xvmq4z83vpfg7s4kry4ci")))

(define-public crate-argopt-0.2.0 (c (n "argopt") (v "0.2.0") (d (list (d (n "argopt-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)))) (h "1in6qqcx68m1k0x7pmwxq3pw75gk8vvjganyyvxac4zxwn13p16v")))

(define-public crate-argopt-0.3.0 (c (n "argopt") (v "0.3.0") (d (list (d (n "argopt-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0aj8mzmysnrxbdcxq8bp10j0iwagn1gn2n54mwj3lqhp05mi76an")))

