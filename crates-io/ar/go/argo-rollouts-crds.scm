(define-module (crates-io ar go argo-rollouts-crds) #:use-module (crates-io))

(define-public crate-argo-rollouts-crds-0.1.0 (c (n "argo-rollouts-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0clarax17hz1s3w8z47nmsnvs0yfi92kk0vpg1i996k83jnld0j0")))

