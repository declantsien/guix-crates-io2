(define-module (crates-io ar go argo-workflow) #:use-module (crates-io))

(define-public crate-argo-workflow-0.1.0 (c (n "argo-workflow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17rw8zw6d2ggql8q5fmpqc7ri6p7cb3nwc3x08rly3i4a4ybgwvx")))

