(define-module (crates-io ar go argo-cd-crds) #:use-module (crates-io))

(define-public crate-argo-cd-crds-0.1.0 (c (n "argo-cd-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "03fw9xbdyh5fqy390a11hgrxp1xdw7z4mna8xykcfvv4nhs279ik")))

