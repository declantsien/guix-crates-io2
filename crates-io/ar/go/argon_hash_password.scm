(define-module (crates-io ar go argon_hash_password) #:use-module (crates-io))

(define-public crate-argon_hash_password-0.1.0 (c (n "argon_hash_password") (v "0.1.0") (d (list (d (n "argon2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "04cj709lglpxccb038v5vhk7lphpd4yg4xig6x11csawpqc2sxa8")))

(define-public crate-argon_hash_password-0.1.1 (c (n "argon_hash_password") (v "0.1.1") (d (list (d (n "argon2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "09k24psyhkphn854lay1w10vsf1s5z84825pr85xdxqiwgz8gaj8")))

(define-public crate-argon_hash_password-0.1.2 (c (n "argon_hash_password") (v "0.1.2") (d (list (d (n "argon2") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "056w17br6qsazzld3kk5n1r359mlw8gd06qh63dk1a1m9fwnny2l")))

