(define-module (crates-io ar go argonfand) #:use-module (crates-io))

(define-public crate-argonfand-0.1.0 (c (n "argonfand") (v "0.1.0") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "06d1w4ndkqadvmdr3shx8gxlijpg0v982bcp4pqpxrdg1pjjl67i")))

(define-public crate-argonfand-1.0.0 (c (n "argonfand") (v "1.0.0") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1n4xl4pz054c6r8paw7vcphky916gbf0wqh1f2gl8f6shppa8y2s")))

(define-public crate-argonfand-1.0.1 (c (n "argonfand") (v "1.0.1") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "12009pw2cjdnpj3vgnczy48bmiksppr34qiqbddrzq7a11kvmf20")))

(define-public crate-argonfand-1.0.2 (c (n "argonfand") (v "1.0.2") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0hqbsi8j43fcry19w08bfbkajvxhbm64g069q35g5hnzqrxyis2g")))

(define-public crate-argonfand-1.0.3 (c (n "argonfand") (v "1.0.3") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-alloc") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "05bd46lmb7bpkyxsk66ladws3yvy9739rdbrz5y08cr1ym342ccv")))

