(define-module (crates-io ar go argo-workflows-rs) #:use-module (crates-io))

(define-public crate-argo-workflows-rs-0.0.1 (c (n "argo-workflows-rs") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ah0m3gd7fj53b33zf2xd9j1y7qad9jj0qjvzhnqrblrk24waqk8")))

(define-public crate-argo-workflows-rs-0.0.2 (c (n "argo-workflows-rs") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "09x2arvilhsn7dsx4iavjlgb3663720q7s7i5x3sb4w6anapsr3d")))

