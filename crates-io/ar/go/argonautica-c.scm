(define-module (crates-io ar go argonautica-c) #:use-module (crates-io))

(define-public crate-argonautica-c-0.1.2 (c (n "argonautica-c") (v "0.1.2") (d (list (d (n "argonautica") (r "^0.1.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bh65219m3qwvbsdfqnbmaymwqla9l54pppqmy91xi1gnaf05hp7") (f (quote (("simd" "argonautica/simd"))))))

(define-public crate-argonautica-c-0.1.3 (c (n "argonautica-c") (v "0.1.3") (d (list (d (n "argonautica") (r "^0.1.3") (d #t) (k 0)) (d (n "cbindgen") (r "^0.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wcz59c7i0n94nfhpglx4l4i5sgdm8jvx76w49jpc3mjq5nnxc8x") (f (quote (("simd" "argonautica/simd"))))))

(define-public crate-argonautica-c-0.1.4 (c (n "argonautica-c") (v "0.1.4") (d (list (d (n "argonautica") (r "^0.1.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17ikkzxyb1r4irm6zq8skmblldcd252xzh4kpls334vlndwzzwa2") (f (quote (("simd" "argonautica/simd"))))))

(define-public crate-argonautica-c-0.1.5 (c (n "argonautica-c") (v "0.1.5") (d (list (d (n "argonautica") (r "^0.1.5") (d #t) (k 0)) (d (n "cbindgen") (r "^0.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l9lrdakvv68wi99lsnb5rrfdp0rds962yi43smiq5hrz8ix91bv") (f (quote (("simd" "argonautica/simd"))))))

(define-public crate-argonautica-c-0.2.0 (c (n "argonautica-c") (v "0.2.0") (d (list (d (n "argonautica") (r "^0.2") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "118scixykvni2pk923raj3x5bnw57p625nc99n2bqgrrm5s4m39r") (f (quote (("simd" "argonautica/simd"))))))

