(define-module (crates-io ar go argon2-sys) #:use-module (crates-io))

(define-public crate-argon2-sys-0.1.0 (c (n "argon2-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "183nid18ikwfs9h5gjyijj03971wvr1x8705zwpmrlhpahdcs2qr") (f (quote (("simd")))) (l "argon2")))

