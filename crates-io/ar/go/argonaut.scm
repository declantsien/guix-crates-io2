(define-module (crates-io ar go argonaut) #:use-module (crates-io))

(define-public crate-argonaut-0.1.0 (c (n "argonaut") (v "0.1.0") (h "05hz8ys68p8fm7qbzx1m9k9k8j0h7i7vp0aas370hz0vgb5f5z1d")))

(define-public crate-argonaut-0.2.0 (c (n "argonaut") (v "0.2.0") (h "15s5fwa4v1nh4f0lnwlbhl462ag4cvrx3lw2r6hac25xi1841zg9")))

(define-public crate-argonaut-0.3.0 (c (n "argonaut") (v "0.3.0") (h "1rw765b5jfxskkml7f0rfnali5pl7adnackm073vyrhw6402q1fa")))

(define-public crate-argonaut-0.3.1 (c (n "argonaut") (v "0.3.1") (h "0w5xh6bp0r163xslxbggqvszxazpj24hpvpcmpvqvs89xjg6l3dp")))

(define-public crate-argonaut-0.4.0 (c (n "argonaut") (v "0.4.0") (h "0ry4c8awid9hf0sfw47l3xr6sd2yg99zy6xb7m3yica514qkcwfk")))

(define-public crate-argonaut-0.5.0 (c (n "argonaut") (v "0.5.0") (h "016953qap3lvvsav9j969cnpnly7700942l6b1m02g3sd03avq8w")))

(define-public crate-argonaut-0.6.0 (c (n "argonaut") (v "0.6.0") (h "0bhhhz55vhg9da79qg0hlrjcrqqyy27m18nsamzyc4hc31j5lgnd")))

(define-public crate-argonaut-0.6.1 (c (n "argonaut") (v "0.6.1") (h "0bpl2n07nwgx1dbyg94wkgaqkhflsh1qqbmk0l02pnmnzk27v9l1")))

(define-public crate-argonaut-0.7.0 (c (n "argonaut") (v "0.7.0") (h "0vvqifgvs4r5cd8vy6lx9harkxqfk1r1n07nkyfxmjb64xxqb58a")))

(define-public crate-argonaut-0.8.0 (c (n "argonaut") (v "0.8.0") (h "0gizdcq65vakk55vb5dwjcq9ml8fxaahqac9qwj1gw1fhnrcwvmp")))

(define-public crate-argonaut-0.9.0 (c (n "argonaut") (v "0.9.0") (h "098qf6rzd4p19r6jvkrw6i1spkqgyi7dcsd00qkdqawd1d4m1jqx")))

(define-public crate-argonaut-0.10.0 (c (n "argonaut") (v "0.10.0") (h "16zckfnd3gqz1pa9dv400ghr9s82qvr0pdr58s5bla22myqwp8hz")))

(define-public crate-argonaut-0.11.0 (c (n "argonaut") (v "0.11.0") (h "0rwra4c5c65yvwzdvrsq08qw5bw5xm04sh6pv6vr3vn7m32sr2v9")))

(define-public crate-argonaut-0.12.0 (c (n "argonaut") (v "0.12.0") (h "04vp3wfah6fm9r5adfmlvf6xipbcpk4kr7ip88z72702bsfp9ax8")))

