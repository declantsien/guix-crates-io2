(define-module (crates-io ar go argon) #:use-module (crates-io))

(define-public crate-argon-0.1.0 (c (n "argon") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5.4") (d #t) (k 0)) (d (n "ena") (r "^0.9.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.15.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "nan-preserving-float") (r "^0.1.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.3") (d #t) (k 2)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmi") (r "^0.2.0") (d #t) (k 2)))) (h "1wa1dblmhrfdrd18v95wicck1b36dq8qswxcq41y2gbrmxzivkrz")))

