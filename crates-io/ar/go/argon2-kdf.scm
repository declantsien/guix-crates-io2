(define-module (crates-io ar go argon2-kdf) #:use-module (crates-io))

(define-public crate-argon2-kdf-1.0.0 (c (n "argon2-kdf") (v "1.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1md93bfk2fhzxng763ff6qd18y8l4f2mkhzzq897np4zdvj1hwry")))

(define-public crate-argon2-kdf-1.1.0 (c (n "argon2-kdf") (v "1.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11kxnfrh7chly6d0g4gfpig3s1wy9bd1frjzqa5fl07mnmbwlsxb")))

(define-public crate-argon2-kdf-1.1.2 (c (n "argon2-kdf") (v "1.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1c504kw1zy8wi2dad0hyjify95pb0f4nrnw1qjzvfa1snipsbprl")))

(define-public crate-argon2-kdf-1.2.0 (c (n "argon2-kdf") (v "1.2.0") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.66.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1jysg1z247vaaqywhscgjfa82419a6jr5jwv8y3181vqb8ja3kbq") (y #t)))

(define-public crate-argon2-kdf-1.3.0 (c (n "argon2-kdf") (v "1.3.0") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.66.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0mg78jdizf1slw6q8vl7y6hw7f8b4rqkpfz7fmz9aafy3nq8dzcf")))

(define-public crate-argon2-kdf-1.4.0 (c (n "argon2-kdf") (v "1.4.0") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.68.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0mcanb88sgjknppxil5gm4z4c2zjkqrpv5brcipi9h4b8br512xk")))

(define-public crate-argon2-kdf-1.5.0 (c (n "argon2-kdf") (v "1.5.0") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.68.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "17rswgixgfccv5iajff70b5r0jghv24dn1f4l6ljk85vlyd71i33")))

(define-public crate-argon2-kdf-1.5.1 (c (n "argon2-kdf") (v "1.5.1") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.69.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0yylc0l50wnblw1k6y4x1pj09ls0x481pvgqkrzqk81vixwvqs71")))

(define-public crate-argon2-kdf-1.5.2 (c (n "argon2-kdf") (v "1.5.2") (d (list (d (n "base64") (r "0.21.*") (d #t) (k 0)) (d (n "bindgen") (r "0.69.*") (d #t) (k 1)) (d (n "cc") (r "1.0.*") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "0ykbmlz8mapxfzzg764zfclb3pmzb825p3ij43mx5j82gmvlpnc3")))

