(define-module (crates-io ar go argopt-impl) #:use-module (crates-io))

(define-public crate-argopt-impl-0.1.0 (c (n "argopt-impl") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qi1gg7hlrsp2ggfaailg7cy9jiarv4ll052vaw99mrwlshy85pl")))

(define-public crate-argopt-impl-0.2.0 (c (n "argopt-impl") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yviy00z1ms9q0x482493ngldgjfk3ryrdcwpj9zlmb4zl2c2kzq")))

(define-public crate-argopt-impl-0.3.0 (c (n "argopt-impl") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01qfipyrsmb8w8gwy45p1q9llwng15glppv5b97d46qjccg6zrf2")))

