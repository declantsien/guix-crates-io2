(define-module (crates-io ar ga argan-core) #:use-module (crates-io))

(define-public crate-argan-core-0.0.0 (c (n "argan-core") (v "0.0.0") (h "1ygv11vh03iwwvx998pd2adcg1x75hcp9kmw3y2xks0z29jnvj96") (y #t)))

(define-public crate-argan-core-0.0.1 (c (n "argan-core") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "http-body") (r "^1") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f1yblfksqc5ffy4s7wdh9dgawlb4jxfpc6fn17vqw7kvwfjgbmi")))

