(define-module (crates-io ar tw artworks) #:use-module (crates-io))

(define-public crate-artworks-0.1.0 (c (n "artworks") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1pn6blhxxqvhsapcif7wdz3al4qnp513gsr6dqfjqqs3dw4ffhl3") (y #t)))

(define-public crate-artworks-0.1.1 (c (n "artworks") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1nxcgrikhql5izznjr2c0b35ld9qshbvpfnw9r5sv4s4dbqycfmv")))

(define-public crate-artworks-0.2.0 (c (n "artworks") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1fffryb9icwx3d1sbkxx2diw3ckmv6f2pz6kh6dznf3m827fd408") (y #t)))

