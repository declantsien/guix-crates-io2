(define-module (crates-io ar im arimaa_engine_step) #:use-module (crates-io))

(define-public crate-arimaa_engine_step-1.0.0 (c (n "arimaa_engine_step") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1dyibbpsgxxfj4af95n1652n7xpan46y1lfslxk8x0a8r8xq9lvb")))

(define-public crate-arimaa_engine_step-1.0.1 (c (n "arimaa_engine_step") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("cargo_bench_support"))) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0qbqhccxnnidb4m4larghyvzy23k9gx0ayqm5sk3k9cng1np5ipi")))

