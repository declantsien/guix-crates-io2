(define-module (crates-io ar pa arpabet_types) #:use-module (crates-io))

(define-public crate-arpabet_types-2.0.0 (c (n "arpabet_types") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1r32i2q5770s4anhynhs57p9dm0q4axicsy70rfms23698h6wfmf")))

