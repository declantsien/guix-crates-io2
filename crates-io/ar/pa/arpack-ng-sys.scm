(define-module (crates-io ar pa arpack-ng-sys) #:use-module (crates-io))

(define-public crate-arpack-ng-sys-0.1.0 (c (n "arpack-ng-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0f8ljrch5ppdi4ik08kl86bf7lymac17y34zql5pia7vxqmkdhh6")))

(define-public crate-arpack-ng-sys-0.1.2-alpha.0 (c (n "arpack-ng-sys") (v "0.1.2-alpha.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "068jignz81j0d7f2g6rn9w6nz0wqmbxglvxfz7z28hp3jpa02xd9")))

(define-public crate-arpack-ng-sys-0.1.3-alpha.0 (c (n "arpack-ng-sys") (v "0.1.3-alpha.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0c1nkyi7skzv6il9vhz4mlr54hhplnwmakd0k9a25ywf6ywhxwja")))

(define-public crate-arpack-ng-sys-0.1.3-alpha.1 (c (n "arpack-ng-sys") (v "0.1.3-alpha.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "07a4y6sr7dpd9mkjv6gn41908lfc4l8824nki0vscdy3simr2734") (f (quote (("system" "pkg-config") ("static" "cmake") ("default" "static"))))))

(define-public crate-arpack-ng-sys-0.2.0 (c (n "arpack-ng-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "10fj13khac5iq2si2bxylwrz4v6drmmawvgpw988r81p6f7m9z3j") (f (quote (("system" "pkg-config") ("static" "cmake") ("default" "static"))))))

(define-public crate-arpack-ng-sys-0.2.1 (c (n "arpack-ng-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1qpx77cca19f7xv1giggkrd2366kdyhlyznm91dd37csw933xqf3") (f (quote (("system" "pkg-config") ("static" "cmake") ("default" "static"))))))

