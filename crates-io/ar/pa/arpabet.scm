(define-module (crates-io ar pa arpabet) #:use-module (crates-io))

(define-public crate-arpabet-0.1.0 (c (n "arpabet") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1r9sx7r66ifmnnbfk0pb2axm9pm9qx62j79j49s1f0iqv35mibh4")))

(define-public crate-arpabet-0.1.1 (c (n "arpabet") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0gvazplfc8hcvcbdpw1wn9a88kvx5kcisq03l6d5hddg8430lzqs")))

(define-public crate-arpabet-0.2.0 (c (n "arpabet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "06b8b6yffjrfmp9xjzmxk4d6ga18r5vvb16g0hyd1sj92hy3b0dh")))

(define-public crate-arpabet-1.0.0 (c (n "arpabet") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1paa1j5g4iskh64dfcr4h1mywjv14figh7jjghz5r8gsiiiihvf7")))

(define-public crate-arpabet-2.0.0 (c (n "arpabet") (v "2.0.0") (d (list (d (n "arpabet_cmudict") (r "^2.0.0") (d #t) (k 0)) (d (n "arpabet_parser") (r "^2.0.0") (d #t) (k 0)) (d (n "arpabet_types") (r "^2.0.0") (d #t) (k 0)))) (h "05ldgcfkx866aqfxlsxm3j9dl5q57rz6ivxbcaidydnm6nscsysd")))

