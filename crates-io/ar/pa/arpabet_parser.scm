(define-module (crates-io ar pa arpabet_parser) #:use-module (crates-io))

(define-public crate-arpabet_parser-2.0.0 (c (n "arpabet_parser") (v "2.0.0") (d (list (d (n "arpabet_types") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1ac70sgkapi6wgp2wxmax18vz9hg8xjdikg3pkx7vq0jsl4v99g1")))

