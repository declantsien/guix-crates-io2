(define-module (crates-io ar ou around) #:use-module (crates-io))

(define-public crate-around-0.1.0 (c (n "around") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1qx3iffj9dkcx7qkr7zwpahy3lgiivpa78zkls29gmxf88jgfcm0")))

