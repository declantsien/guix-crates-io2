(define-module (crates-io ar py arpy-client) #:use-module (crates-io))

(define-public crate-arpy-client-0.2.0 (c (n "arpy-client") (v "0.2.0") (d (list (d (n "arpy") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "sync"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.11") (d #t) (k 0)))) (h "0igm069fk55w2swc853i965f4yqs75kbdns9wgfn5lawnfmsac3v")))

