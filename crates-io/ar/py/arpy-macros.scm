(define-module (crates-io ar py arpy-macros) #:use-module (crates-io))

(define-public crate-arpy-macros-0.1.0 (c (n "arpy-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1bx7sx7xff9lfcx79m1wxma35qnxacp7rbqqik0xjm7pdi4h397c")))

(define-public crate-arpy-macros-0.2.0 (c (n "arpy-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "199rp0i9zqncpgz1a8amrn7s5pclmn5f9nnac0pyj2aijlg9ff56")))

