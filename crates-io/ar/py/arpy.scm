(define-module (crates-io ar py arpy) #:use-module (crates-io))

(define-public crate-arpy-0.1.0 (c (n "arpy") (v "0.1.0") (d (list (d (n "arpy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1w3rhhk3hjv3r1j0fyxxlwwasszzb61vp1vmb38bnfahfqrmxfvz")))

(define-public crate-arpy-0.2.0 (c (n "arpy") (v "0.2.0") (d (list (d (n "arpy-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "086fd199npcmj0jdrni3lvn3bs3brsdw7mvnrafwjbrhpnbgl3bi")))

