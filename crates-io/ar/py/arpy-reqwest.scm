(define-module (crates-io ar py arpy-reqwest) #:use-module (crates-io))

(define-public crate-arpy-reqwest-0.1.0 (c (n "arpy-reqwest") (v "0.1.0") (d (list (d (n "arpy") (r "^0.1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "process"))) (d #t) (k 2)))) (h "1h81gkwschc4afi1d5z6ykz524l0dik4zwbaia0iarlnrlq119z1")))

(define-public crate-arpy-reqwest-0.2.0 (c (n "arpy-reqwest") (v "0.2.0") (d (list (d (n "arpy") (r "^0.2.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "process"))) (d #t) (k 2)))) (h "12hzn40waa1shmj8j295098l9pr2fq4v2z007a70k3nryqyd76xf")))

