(define-module (crates-io ar py arpy-server) #:use-module (crates-io))

(define-public crate-arpy-server-0.1.0 (c (n "arpy-server") (v "0.1.0") (d (list (d (n "arpy") (r "^0.1.0") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0fvnnl3rk2cdra9mq6hmn2xa557bj86dyxcw45jzrxhvk7s7n1ps")))

(define-public crate-arpy-server-0.2.0 (c (n "arpy-server") (v "0.2.0") (d (list (d (n "arpy") (r "^0.2.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "181rpyhb85gm28x74ga93igbg4w4rkdrfgwba76r2ck016nnp9kw")))

