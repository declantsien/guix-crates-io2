(define-module (crates-io ar ts artsy) #:use-module (crates-io))

(define-public crate-artsy-0.1.0 (c (n "artsy") (v "0.1.0") (d (list (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "05p6qw08424995gc5hn3xm5vvfh4xhvj0m0fhp7ri1fhf1s8z37f") (f (quote (("node48") ("node4") ("node16") ("no-simd") ("default" "node4" "node16" "node48"))))))

(define-public crate-artsy-0.1.1 (c (n "artsy") (v "0.1.1") (d (list (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1v6znx6znks81w4qg52jmjwd8d42pmxgagv110am5kmwsl15migv") (f (quote (("node48") ("node4") ("node16") ("no-simd") ("default" "node4" "node16" "node48"))))))

