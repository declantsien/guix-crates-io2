(define-module (crates-io ar cy arcy) #:use-module (crates-io))

(define-public crate-arcy-0.1.0 (c (n "arcy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread" "parking_lot" "sync"))) (d #t) (k 0)))) (h "04a5xzs1l3z887s41l3r3wf5g9bfqski89c5sqm5w53xkfcgsvsr")))

