(define-module (crates-io ar na arnaddone) #:use-module (crates-io))

(define-public crate-arnaddone-0.1.0 (c (n "arnaddone") (v "0.1.0") (h "1gwsaqn5vdyf1ixsqlfivhq4g324x42a4yd0c2y0wq7z85g4v257")))

(define-public crate-arnaddone-0.2.0 (c (n "arnaddone") (v "0.2.0") (d (list (d (n "arnaddone") (r "^0.1.0") (d #t) (k 0)))) (h "0p88psvq5qwgff5rmhra1249sbawpll1xw4zbd4a7a5vc35kv9q3")))

