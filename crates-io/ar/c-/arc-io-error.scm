(define-module (crates-io ar c- arc-io-error) #:use-module (crates-io))

(define-public crate-arc-io-error-0.1.0 (c (n "arc-io-error") (v "0.1.0") (h "1vmpkcwkiiksadgdhzy4vhjpyrxmgq030xzhl24q0wjqp9mvp4xi")))

(define-public crate-arc-io-error-0.1.1 (c (n "arc-io-error") (v "0.1.1") (h "0pm9l7x08h0a4np6cyb9mv6vfdk87w0rc2ba3zrk29r73jiblvr1")))

