(define-module (crates-io ar c- arc-bytes) #:use-module (crates-io))

(define-public crate-arc-bytes-0.1.0 (c (n "arc-bytes") (v "0.1.0") (d (list (d (n "pot") (r "^1.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09y5fkxx8wfp9qjsqiy7dhz9f4qijiyb9b60wrgn1c2f031wklay") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.2.0 (c (n "arc-bytes") (v "0.2.0") (d (list (d (n "pot") (r "^1.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hppki1z3y5hqjgrf29kray03gq0v960njn309d20c58nyflzag2") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.3.0 (c (n "arc-bytes") (v "0.3.0") (d (list (d (n "pot") (r "^1.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v90ka1ihrpfh7xwl7g8sld7myxm86vxvpqbn3ipl222ydnsq1w9") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.3.1 (c (n "arc-bytes") (v "0.3.1") (d (list (d (n "pot") (r "^1.0.0-rc.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01srv697rrvkywzjrxxidv1pzr5amvc4wd4gjlla81l9lphmwhj6") (f (quote (("default" "serde")))) (y #t)))

(define-public crate-arc-bytes-0.3.2 (c (n "arc-bytes") (v "0.3.2") (d (list (d (n "pot") (r "^1.0.0-rc.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k2kcapi78xyi1afy7g118zris3lvh98ys8lb8s3k217q6cfwa6d") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.3.3 (c (n "arc-bytes") (v "0.3.3") (d (list (d (n "pot") (r "^1.0.0-rc.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1airmyz6x9w0xclx4m6xda289jfhsn83nypdhbba350r29r4g6i0") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.3.4 (c (n "arc-bytes") (v "0.3.4") (d (list (d (n "pot") (r "^1.0.0-rc.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jd6gphflzbz48scks6c8hwjn322rw8825i68pw739ih8wvfn6n0") (f (quote (("default" "serde"))))))

(define-public crate-arc-bytes-0.3.5 (c (n "arc-bytes") (v "0.3.5") (d (list (d (n "pot") (r "^1.0.0-rc.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mb72m034nrlbq7nbb03v5dkj1qgjrmd2kpd379s6qij6bmbzrrx") (f (quote (("default" "serde"))))))

