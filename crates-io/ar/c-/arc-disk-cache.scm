(define-module (crates-io ar c- arc-disk-cache) #:use-module (crates-io))

(define-public crate-arc-disk-cache-0.1.0 (c (n "arc-disk-cache") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "concread") (r "^0.3.2") (d #t) (k 0)) (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1llwq9jyid955fcpjgi9w3jvvysrmdsnsadxjyl7x5j4gpp0cfcn")))

