(define-module (crates-io ar c- arc-calc) #:use-module (crates-io))

(define-public crate-arc-calc-0.1.0 (c (n "arc-calc") (v "0.1.0") (h "0pspcy66f8y1x47am178m9x4r7a4ql8bg1djymir4fq4qvdjvzm6") (y #t)))

(define-public crate-arc-calc-0.1.1 (c (n "arc-calc") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vax05jhlw1jkn72zpqvpwpwfkkaaxahik2d4mkbnd4f99irhql5") (y #t)))

(define-public crate-arc-calc-0.1.2 (c (n "arc-calc") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qhdfa0kf5qsp12ra7nyqq53y8xhf1i6jwm5ycv9qiwzjk4bdqix") (y #t)))

(define-public crate-arc-calc-0.1.3 (c (n "arc-calc") (v "0.1.3") (h "0ags6428yla4n5v7i3ybhvzs9jnafhk1y56y4y8k7br68iq38abl") (y #t)))

(define-public crate-arc-calc-0.1.4 (c (n "arc-calc") (v "0.1.4") (h "026d0zz3c4bhcq985cq732pap53c5im811a6ri4y5h5mxg67i7la")))

