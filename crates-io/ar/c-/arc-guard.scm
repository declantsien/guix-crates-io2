(define-module (crates-io ar c- arc-guard) #:use-module (crates-io))

(define-public crate-arc-guard-0.1.0 (c (n "arc-guard") (v "0.1.0") (h "0jz9bclnxgp20xw5yhyzylcdlyfklh8jizm9bifkby90bg2mb301")))

(define-public crate-arc-guard-0.2.0 (c (n "arc-guard") (v "0.2.0") (h "0chqxyr1wjf96l2y22cysk36inzs4c9271hncy31srszcxvljjw7")))

