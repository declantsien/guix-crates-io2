(define-module (crates-io ar c- arc-isle) #:use-module (crates-io))

(define-public crate-arc-isle-0.1.0 (c (n "arc-isle") (v "0.1.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1g5kqp0sz4z9wmi60wq65738bxwvx70zjil8lvrr1z3sawhkvvx2")))

(define-public crate-arc-isle-0.1.1 (c (n "arc-isle") (v "0.1.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "14nq9axs15drwr5i21avidmmzzsybgj84hjnhaxqdkz9x6v0wm1w")))

(define-public crate-arc-isle-0.1.2 (c (n "arc-isle") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11a3w8nwsm9vq1iaglnw02crgzgcmjdz5c8hn8zykjk96nq70g11")))

