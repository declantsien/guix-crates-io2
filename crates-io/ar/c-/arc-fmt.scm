(define-module (crates-io ar c- arc-fmt) #:use-module (crates-io))

(define-public crate-arc-fmt-0.1.0 (c (n "arc-fmt") (v "0.1.0") (d (list (d (n "arc_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1riidzvjxcl8h24bk7j8m7kx3gvvsagn0ax9wc8djm8q8l5s9nca")))

