(define-module (crates-io ar c- arc-cache) #:use-module (crates-io))

(define-public crate-arc-cache-0.2.2 (c (n "arc-cache") (v "0.2.2") (d (list (d (n "xlru-cache") (r "^0.1") (d #t) (k 0)))) (h "0p2j56nldjh5dlx3k5362pb0jfrbiklr8pi541lgs7c8g9yh1w32")))

(define-public crate-arc-cache-0.2.3 (c (n "arc-cache") (v "0.2.3") (d (list (d (n "xlru-cache") (r "^0.1") (d #t) (k 0)))) (h "1nfc86d8dq67hiy9ww73ykfc8ng7cwdnbazrdsd8fsnai69kr5ip")))

(define-public crate-arc-cache-0.2.4 (c (n "arc-cache") (v "0.2.4") (d (list (d (n "xlru-cache") (r "^0.1") (d #t) (k 0)))) (h "1a6i1nkd90bxc4vng33kpmx9lfy342ygsiv6hcky36zz4dnnpzab")))

(define-public crate-arc-cache-0.2.5 (c (n "arc-cache") (v "0.2.5") (d (list (d (n "xlru-cache") (r "^0.1") (d #t) (k 0)))) (h "1vfjn50hddy8psn5vhr554n31zad8x318v5q8b4ild04m86rgbkr")))

