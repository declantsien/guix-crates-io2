(define-module (crates-io ar c- arc-cell) #:use-module (crates-io))

(define-public crate-arc-cell-0.1.0 (c (n "arc-cell") (v "0.1.0") (h "1n7k7qvy14q8hgyg2k6r9kvx0gjzlcqpwsw3xxqjgfwbl12s9vlq")))

(define-public crate-arc-cell-0.1.1 (c (n "arc-cell") (v "0.1.1") (h "05gryhvbkv6kk7jsfhfghh74hqfdqagddbbibflf9h8jbpmc3bdv")))

(define-public crate-arc-cell-0.1.2 (c (n "arc-cell") (v "0.1.2") (h "1kdlg7yp2zphln5m1wryq1w90iy0gkgayqfkwfp3czhgrxcq4bwd")))

(define-public crate-arc-cell-0.1.3 (c (n "arc-cell") (v "0.1.3") (d (list (d (n "spin") (r "^0.4.5") (d #t) (k 0)))) (h "14cgp3gi7nhs3k258xrbp4gwq977s641qkjp1wvhfx4b2dfgg5n1")))

(define-public crate-arc-cell-0.1.4 (c (n "arc-cell") (v "0.1.4") (d (list (d (n "spin") (r "^0.4.5") (k 0)))) (h "022c4a6lyyz6xgd4s4xcpprrah6a17v55bhjn9s9j0jqgdq2cyy3")))

(define-public crate-arc-cell-0.1.5 (c (n "arc-cell") (v "0.1.5") (h "00jq36lwm4qla2vnmjg7nkgs0ra1hbcmrl1hsrfyywyban5gng31")))

(define-public crate-arc-cell-0.2.0 (c (n "arc-cell") (v "0.2.0") (h "0v59i9sl8z55ds99cfr81kg494r6njbryyi11k0afg4a5nr1xlbm")))

(define-public crate-arc-cell-0.2.1 (c (n "arc-cell") (v "0.2.1") (h "0m3561p4iwsv1r1nqjyil039g2qzwl73pjnn8p72awm7an7gyrg3")))

(define-public crate-arc-cell-0.2.2 (c (n "arc-cell") (v "0.2.2") (h "1pnksd2c4cm3xs304f1yhqkvp9vm46vqbc9qr383akaz6w3jf32c")))

(define-public crate-arc-cell-0.3.0 (c (n "arc-cell") (v "0.3.0") (h "0gz27v3ijcg2d0bkwjbxaz3vswb797369yvlch9imcny03zx60r9")))

(define-public crate-arc-cell-0.3.1 (c (n "arc-cell") (v "0.3.1") (h "1vnv90dmp3nk4igfdj9ak7zi5i82z29f5m0ny8515w7s67m4df3d")))

(define-public crate-arc-cell-0.3.3 (c (n "arc-cell") (v "0.3.3") (h "1f7pwmldyzv71n5i4skb8lnm3c17lasda6qhxxnxh84lvyddmjgy") (f (quote (("const-new"))))))

