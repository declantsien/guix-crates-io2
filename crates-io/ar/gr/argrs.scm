(define-module (crates-io ar gr argrs) #:use-module (crates-io))

(define-public crate-argrs-0.1.0 (c (n "argrs") (v "0.1.0") (h "0fd9vkyc2d5vsi12hkvhidff2whfbz7igqhhri47zj48ik3y2w5g")))

(define-public crate-argrs-0.1.1 (c (n "argrs") (v "0.1.1") (h "1z7lj7sh1pkg1yhbh46r0zc8lpabxanvbqdpds3qqxfy9h25s7ja")))

(define-public crate-argrs-0.1.2 (c (n "argrs") (v "0.1.2") (h "1cbr7k5gw33frvnafxaflb5nk56asrzb9m0pza64qbvidkbnh3qw")))

