(define-module (crates-io ar ka arkaoe) #:use-module (crates-io))

(define-public crate-arkaoe-0.1.0 (c (n "arkaoe") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6") (d #t) (k 0)) (d (n "actix-web") (r "^4.5") (d #t) (k 0)) (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "askama_actix") (r "^0.13") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "docopt") (r "^1.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies" "gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vergen") (r "^7.5") (d #t) (k 1)))) (h "1klblb9cggwq8hq74ha0hf5yv6c19j5i58qka7qa9lkhwb043m6f")))

