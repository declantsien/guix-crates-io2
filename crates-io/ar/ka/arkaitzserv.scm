(define-module (crates-io ar ka arkaitzserv) #:use-module (crates-io))

(define-public crate-arkaitzserv-0.1.0 (c (n "arkaitzserv") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "07550m6hp1zfxvz7bp38brsdx2h012mvvmkfk7s6s37rv1mzh1d3") (r "1.69")))

(define-public crate-arkaitzserv-0.1.1 (c (n "arkaitzserv") (v "0.1.1") (d (list (d (n "rocket") (r "^0.5.0-rc.3") (d #t) (k 0)))) (h "0qcghwnkp6vz2my3j604ni0jwxlakryvpw1rpgv3gkn3plzqqis2") (r "1.69")))

