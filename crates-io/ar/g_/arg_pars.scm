(define-module (crates-io ar g_ arg_pars) #:use-module (crates-io))

(define-public crate-arg_pars-0.1.0 (c (n "arg_pars") (v "0.1.0") (h "15ma7j0jj51ljmvr3aagqjrzlx9n856r55yqldz99yqrc978qc1n")))

(define-public crate-arg_pars-0.1.1 (c (n "arg_pars") (v "0.1.1") (h "03cvhzppdplbqrg788cxnyfjdc6l1d3ppbmzs45xq9zissz9lcr6")))

(define-public crate-arg_pars-0.1.2 (c (n "arg_pars") (v "0.1.2") (h "1jm4fyibjl6nlcylylbnj2lnsryc5niajbjzsrz125c5b5fc4q8v")))

(define-public crate-arg_pars-0.1.3 (c (n "arg_pars") (v "0.1.3") (h "02bw1yjxhqkc7707w8k5x5j5wsvsqr7gwsihwyyshai3b3gvhc6b")))

