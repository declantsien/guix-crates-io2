(define-module (crates-io ar g_ arg_ripper) #:use-module (crates-io))

(define-public crate-arg_ripper-0.1.0 (c (n "arg_ripper") (v "0.1.0") (d (list (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0xqkxj1pijip298w9rkbvby3c5l6c5zinm94s08yy9k9kkf14l28")))

