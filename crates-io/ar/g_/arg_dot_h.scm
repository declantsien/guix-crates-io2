(define-module (crates-io ar g_ arg_dot_h) #:use-module (crates-io))

(define-public crate-arg_dot_h-0.1.0 (c (n "arg_dot_h") (v "0.1.0") (h "11qsiq56azhnz86537cyh0hjv9k9501dk7fkl8z7wpc795qvcpfv")))

(define-public crate-arg_dot_h-0.1.1 (c (n "arg_dot_h") (v "0.1.1") (h "0nwmqai635vmslkf94z3nijhgs806sxbaj1gcfydagxshwksn2ig")))

(define-public crate-arg_dot_h-0.1.2 (c (n "arg_dot_h") (v "0.1.2") (h "029fw9m8f0l87xid50yni3icpcd04pqn1ylzg5lfaggsa2b06ik5")))

