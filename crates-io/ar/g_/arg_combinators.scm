(define-module (crates-io ar g_ arg_combinators) #:use-module (crates-io))

(define-public crate-arg_combinators-0.1.0 (c (n "arg_combinators") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0qvn3s4s7sydpwafxa7smzna9jfma0jspnarqfdm9hngsgd9gk8d")))

(define-public crate-arg_combinators-0.2.0 (c (n "arg_combinators") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0h7wmga5774y7dwly9yhda7s1a8j2cz488wypdynw0kcgnrsxjgf")))

(define-public crate-arg_combinators-0.3.0 (c (n "arg_combinators") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0yqzhpnsqpz3ijh3h9pk8k3b43h6pkvvpc63f5ykz1vqd4zi5apw")))

(define-public crate-arg_combinators-0.3.1 (c (n "arg_combinators") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1b6pzdkjpci8m58svv12fz3iz5k9f2ljnzafwgblpld9ki8kxgzx")))

(define-public crate-arg_combinators-0.4.0 (c (n "arg_combinators") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0dfhi25iy0cjyy163mdllsdy6ix0g69v2s1vqnm8dcr8qp8x88mv")))

(define-public crate-arg_combinators-0.5.0 (c (n "arg_combinators") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "09fbpfv2f989iwc8faliqn43lsdw9bf9j4ckwky99nsrldqca96f")))

(define-public crate-arg_combinators-0.5.1 (c (n "arg_combinators") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0d5rr09z9qzphh6lhn4sx2fk8kkr8ymbr53hy6valydan3w7f910")))

(define-public crate-arg_combinators-0.6.0 (c (n "arg_combinators") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "13ry43gaaqkgd3l6m0c74ld7gicmp313garf51ddf3ifz508kk9l")))

