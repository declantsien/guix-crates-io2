(define-module (crates-io ar g_ arg_parse) #:use-module (crates-io))

(define-public crate-arg_parse-0.0.9 (c (n "arg_parse") (v "0.0.9") (h "1ppgjs3lz4bbsv5z7nkvcxazd12rsxqnnc4kjlwipyf1ns3kmpqd")))

(define-public crate-arg_parse-0.1.0 (c (n "arg_parse") (v "0.1.0") (h "00mm9xh1vzi8dgazimry1w00dpxgw10n4b95fis1b3iyw72q6d90")))

(define-public crate-arg_parse-0.1.1 (c (n "arg_parse") (v "0.1.1") (h "04d7y6b4l6035p0ih4wxzfqjfs16bacb1nhq7bichy0cpmj7p0wk")))

(define-public crate-arg_parse-0.2.0 (c (n "arg_parse") (v "0.2.0") (h "1krdbh10sm8hwqfg1dk5vyvkv3a9zibvk1shcj074fx0nmdchr3i") (y #t)))

(define-public crate-arg_parse-0.2.1 (c (n "arg_parse") (v "0.2.1") (h "09qdvq7i68kzybfr0khxz909dmrdakg4vv4ivqzsfynzpwmkf56r") (y #t)))

(define-public crate-arg_parse-0.2.2 (c (n "arg_parse") (v "0.2.2") (h "1x1xlmmafvhspzki7nhsx3bfvvj30ly28qk79y4klkddxa9dn80z") (y #t)))

(define-public crate-arg_parse-0.2.3 (c (n "arg_parse") (v "0.2.3") (h "0qc44vxc1c2xz26c1aq7bq7x5vky0rvr9dlz6k4c9zw00mkyw394")))

(define-public crate-arg_parse-0.3.0 (c (n "arg_parse") (v "0.3.0") (h "07c0kwih5i6i3pmfk9ws3qkmcgvrllr4s9xl9xkd0cp5736y8z7k")))

