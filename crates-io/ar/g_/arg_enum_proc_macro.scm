(define-module (crates-io ar g_ arg_enum_proc_macro) #:use-module (crates-io))

(define-public crate-arg_enum_proc_macro-0.1.0 (c (n "arg_enum_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dis2z8pc2x3mf44zj3vahqxl8f1038f27x859r6z4inlw20f29j")))

(define-public crate-arg_enum_proc_macro-0.1.1 (c (n "arg_enum_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03b9ls8wy7wqxfj0vjryp0n5dqvkw2pj36yd07m74c9b4760f6qx")))

(define-public crate-arg_enum_proc_macro-0.2.0 (c (n "arg_enum_proc_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mzf00lc902ys1n8hm1734pd1mp7lryls7855ffnkn21ckf70vlh")))

(define-public crate-arg_enum_proc_macro-0.3.0 (c (n "arg_enum_proc_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "021rr6j3n031ynfbm7kwb3j3bxvbsz40n0nqi78k47d3p92rihcv")))

(define-public crate-arg_enum_proc_macro-0.3.1 (c (n "arg_enum_proc_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qcjnkl08krm2cs3dikr8m4zxqi3pmcag55kzq6316sfc93033kw")))

(define-public crate-arg_enum_proc_macro-0.3.2 (c (n "arg_enum_proc_macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ww7gl2yn779dq7fqigrvy64z71gr8z3n0ydhw2mjm46xr1rphnp")))

(define-public crate-arg_enum_proc_macro-0.3.3 (c (n "arg_enum_proc_macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0q7g30bk6nx3hacihh2f0wsmqj11crbg96mwplqswwf9p3mv2qj0")))

(define-public crate-arg_enum_proc_macro-0.3.4 (c (n "arg_enum_proc_macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1sjdfd5a8j6r99cf0bpqrd6b160x9vz97y5rysycsjda358jms8a")))

