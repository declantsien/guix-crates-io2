(define-module (crates-io ar g_ arg_attr) #:use-module (crates-io))

(define-public crate-arg_attr-0.1.0 (c (n "arg_attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "printing"))) (d #t) (k 0)))) (h "0nh336rbizcmr22l31aiyp23k6v8qax19wdmgnwnh91n1s45gk7i")))

