(define-module (crates-io ar _r ar_row_derive) #:use-module (crates-io))

(define-public crate-ar_row_derive-0.6.0 (c (n "ar_row_derive") (v "0.6.0") (d (list (d (n "ar_row") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.30.0") (d #t) (k 2)) (d (n "rust_decimal_macros") (r "^1.30.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1.0") (d #t) (k 0)))) (h "1a43cj7pj6r6zka4dbkssgk1sqgkcxck4wpp1j3w9ybwbl8lkdjc") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.63")))

