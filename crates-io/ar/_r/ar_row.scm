(define-module (crates-io ar _r ar_row) #:use-module (crates-io))

(define-public crate-ar_row-0.6.0 (c (n "ar_row") (v "0.6.0") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (o #t) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.30.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 1)))) (h "0y20d8zr7r1rnmj8wfyjh6rxs2cfkvg6bhdjpxz4w5br6klin05n") (r "1.63")))

