(define-module (crates-io ar ma armature-macro) #:use-module (crates-io))

(define-public crate-armature-macro-0.1.0 (c (n "armature-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "016p38c48jng36jvacx55x0y6nvvqd06kiqk7ad8idvvrgylwmvb")))

(define-public crate-armature-macro-0.1.1 (c (n "armature-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1chxmih255sba25hqd7dq2gl3mwma34vn8x8d5glqb3wcj6aq0a6")))

