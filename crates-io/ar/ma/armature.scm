(define-module (crates-io ar ma armature) #:use-module (crates-io))

(define-public crate-armature-0.1.0 (c (n "armature") (v "0.1.0") (d (list (d (n "armature-macro") (r "^0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "04dkh5pkyy00nnxf4ks2v414w647pxf1aphhrkalpsyyqr4dlr0w")))

(define-public crate-armature-0.1.1 (c (n "armature") (v "0.1.1") (d (list (d (n "armature-macro") (r "^0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0kj9d84hzsjw3cwg0zfb0b7k40fs1yxfs5vbd51452xyp2qqd90b")))

