(define-module (crates-io ar ma arma_preset_parser) #:use-module (crates-io))

(define-public crate-arma_preset_parser-0.1.0 (c (n "arma_preset_parser") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)))) (h "11vxzzf4vyy0bjy6g0q9wpila86ix1f5m2vqvbq51w6m8zj0sngi") (y #t)))

(define-public crate-arma_preset_parser-0.1.1 (c (n "arma_preset_parser") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)))) (h "1z2i4bqs80m4waavbi7wwggkv9gcjmqvx0v43g9q7sy3v8xz5w5h")))

(define-public crate-arma_preset_parser-0.2.0 (c (n "arma_preset_parser") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)))) (h "0d7iagp2p8mmsh2ppqazqnxq2s7snxlrjflfavf85iwp8iff807p")))

(define-public crate-arma_preset_parser-0.3.0 (c (n "arma_preset_parser") (v "0.3.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)))) (h "1w9rdfpmjj05icnahl7af8d4653hb5xcvck2wq9nhm043dbl5yph")))

