(define-module (crates-io ar ma armalint) #:use-module (crates-io))

(define-public crate-armalint-0.1.1 (c (n "armalint") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 1)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 1)) (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "strum") (r "^0.16") (d #t) (k 0)) (d (n "strum_macros") (r "^0.16") (d #t) (k 0)))) (h "1ixvgwp8dlbjcdm5irw8qigxlc78vdd2cq148zjfda8216rn6ii0")))

