(define-module (crates-io ar ma arma-rs-proc) #:use-module (crates-io))

(define-public crate-arma-rs-proc-1.2.0 (c (n "arma-rs-proc") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zva94sa2ld7gy6qcd9swmivypad093c57w52v3rdsi9b2qs366i")))

(define-public crate-arma-rs-proc-1.3.0 (c (n "arma-rs-proc") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0acs1ngc6iyvjnaawikglr315vagn80mn91fqhyqvjvvschxa60y")))

(define-public crate-arma-rs-proc-1.5.0 (c (n "arma-rs-proc") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h3j5kaifjidvyjlkqb6ndcsaj0ny4alb5wabnazqw6k84qsd4cb")))

(define-public crate-arma-rs-proc-1.5.1 (c (n "arma-rs-proc") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02rq86rh78wxwqjm84l1xbc79m0bygxs9h2mm8r5vzkg12w4jm2d")))

(define-public crate-arma-rs-proc-1.7.2 (c (n "arma-rs-proc") (v "1.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4yc30yrrxcx2whshqffadv2aq1ypryssrg1lakml9k0w97w5vc")))

(define-public crate-arma-rs-proc-1.9.3 (c (n "arma-rs-proc") (v "1.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q8vm589rik8cjzgy68jvpyib4hs1ha0wgahzyiam84mfbs0m3dk")))

(define-public crate-arma-rs-proc-1.10.0 (c (n "arma-rs-proc") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1d6w06i90763ixgc8pp55q1azx1dd46fvzkg46b3ijbbq6f1lysi")))

