(define-module (crates-io ar du arducam_mipicamera) #:use-module (crates-io))

(define-public crate-arducam_mipicamera-0.1.0 (c (n "arducam_mipicamera") (v "0.1.0") (h "0fll861zwww4rfh2gzvmn7pnmz4vqxvclk9wmwqmx7j2mlc9lw55")))

(define-public crate-arducam_mipicamera-0.1.1 (c (n "arducam_mipicamera") (v "0.1.1") (h "1cc2yh1ipwhyssrzkr4w5vf883ydyqgpx00y4sndb1lgjxq09vz3")))

