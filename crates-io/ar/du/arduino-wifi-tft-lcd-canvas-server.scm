(define-module (crates-io ar du arduino-wifi-tft-lcd-canvas-server) #:use-module (crates-io))

(define-public crate-arduino-wifi-tft-lcd-canvas-server-1.0.0 (c (n "arduino-wifi-tft-lcd-canvas-server") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.6.1") (d #t) (k 0)) (d (n "pbr") (r "^1.1") (d #t) (k 0)))) (h "1fy16kkchsdx10d4ss0vlwgm46aapshywmlqy47s69d9ga0k3qjy")))

