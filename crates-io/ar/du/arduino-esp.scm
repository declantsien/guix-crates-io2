(define-module (crates-io ar du arduino-esp) #:use-module (crates-io))

(define-public crate-arduino-esp-0.0.1 (c (n "arduino-esp") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "148crnr1swlxkvmiaxlw971dbp6gaji7gsi0hxi7qmqzwadrm4gm") (y #t)))

(define-public crate-arduino-esp-0.0.2 (c (n "arduino-esp") (v "0.0.2") (h "16nsd87ygvpm75qv167vp6vkrk1dd2540pgja5pfb468sivjnn34")))

