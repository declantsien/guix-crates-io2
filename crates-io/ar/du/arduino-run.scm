(define-module (crates-io ar du arduino-run) #:use-module (crates-io))

(define-public crate-arduino-run-0.1.0 (c (n "arduino-run") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0b9amnhcmvcy201iqw6870hkzx0a0jaswqz6a8sms3f4nbm9gnmv")))

(define-public crate-arduino-run-0.1.1 (c (n "arduino-run") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "02r961blmj6w2py8drqq9a0j1arhgg104v1mqyhzn8rfa4hq9chw")))

(define-public crate-arduino-run-0.1.2 (c (n "arduino-run") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1lvji3yxzmbyk5c9xsr28abnmslz1f79mjzlmvy909prkm0jsss6")))

