(define-module (crates-io ar du arduino-cli-client) #:use-module (crates-io))

(define-public crate-arduino-cli-client-0.1.0 (c (n "arduino-cli-client") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "1142wpzg1z63gj11f8vqx57diyna41iaf1k5c8q9ysn1bq72rln0")))

(define-public crate-arduino-cli-client-0.1.1 (c (n "arduino-cli-client") (v "0.1.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "1351lxy5cd7ymglvad107w745kqrvr78ynycpxsj06d0rh3jbqcj")))

(define-public crate-arduino-cli-client-0.1.2 (c (n "arduino-cli-client") (v "0.1.2") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "0bybqz3b7rpxar1s6f4i1lkskgv0rmf3qprzzm2r8mykc869azlg")))

