(define-module (crates-io ar du arduino-leonardo) #:use-module (crates-io))

(define-public crate-arduino-leonardo-0.1.0 (c (n "arduino-leonardo") (v "0.1.0") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "atmega32u4-hal") (r "^0.1.2") (d #t) (k 0)))) (h "0n9lfd8cpcvyhmails8ifml6b7ygz36d1rfz1gszyk54yrrhrspa") (f (quote (("docs" "atmega32u4-hal/docs"))))))

(define-public crate-arduino-leonardo-0.1.1 (c (n "arduino-leonardo") (v "0.1.1") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "atmega32u4-hal") (r "^0.1.2") (d #t) (k 0)))) (h "17byj267n46qwkvb00jvw9inchcvvp3a71aiw1pyfmrcdpslv4j4") (f (quote (("docs" "atmega32u4-hal/docs"))))))

(define-public crate-arduino-leonardo-0.1.2 (c (n "arduino-leonardo") (v "0.1.2") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "atmega32u4-hal") (r "^0.1.3") (d #t) (k 0)))) (h "11v9ksbnsfdg3ranp9a7sg52vv4s1lwxax7p8mjqg152acx6fkp1") (f (quote (("docs" "atmega32u4-hal/docs"))))))

(define-public crate-arduino-leonardo-0.1.3 (c (n "arduino-leonardo") (v "0.1.3") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "atmega32u4-hal") (r "^0.1.4") (d #t) (k 0)))) (h "1xq5hdjhzxh7gd0grdy92m16qrwrwai6r91n9jqhl440jgjd25ii") (f (quote (("docs" "atmega32u4-hal/docs"))))))

