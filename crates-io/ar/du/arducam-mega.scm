(define-module (crates-io ar du arducam-mega) #:use-module (crates-io))

(define-public crate-arducam-mega-0.1.0 (c (n "arducam-mega") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "0g69iv4d1dbq5rq8p8xizlb2awbiafn3xbyrrqqxs92jbnx21640") (f (quote (("default" "3mp" "5mp") ("5mp") ("3mp"))))))

(define-public crate-arducam-mega-0.2.0 (c (n "arducam-mega") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "1vvv552v0aybn7j8857kdg2v4gw3bsyg1aj15ca4s613nmwsa70g") (f (quote (("default" "3mp" "5mp") ("5mp") ("3mp"))))))

(define-public crate-arducam-mega-0.3.0 (c (n "arducam-mega") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "0b1fwq66n7ffnh9cws4xbzp2vxgv8qa5m3ad0vbbzzn7nq84k00k") (f (quote (("default" "3mp" "5mp") ("5mp") ("3mp"))))))

(define-public crate-arducam-mega-0.3.1 (c (n "arducam-mega") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "1p15gk2h9w5hxanl9xn0iz1jfwg945bydjy6wqikx7z3278inmwc") (f (quote (("default" "3mp" "5mp") ("5mp") ("3mp"))))))

(define-public crate-arducam-mega-0.4.0 (c (n "arducam-mega") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "0bbdd3ii1qz8pvwqq5j13v2afl6066d0vyq40v17srv3cwvgcnng") (f (quote (("default" "3mp" "5mp") ("5mp") ("3mp"))))))

