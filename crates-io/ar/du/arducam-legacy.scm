(define-module (crates-io ar du arducam-legacy) #:use-module (crates-io))

(define-public crate-arducam-legacy-0.1.0 (c (n "arducam-legacy") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0905qplr2fyfvbg84gc3x69zbqwy5qdki3sa36bd1jf27b8z6sad")))

(define-public crate-arducam-legacy-0.1.1 (c (n "arducam-legacy") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1hckx9la8sw5af3jvr7wy895dhg5dxm0h58v64rkigwzap4150r8")))

(define-public crate-arducam-legacy-0.1.2 (c (n "arducam-legacy") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "14z3c4zrh4g3wiyidqd5rswk998iaqy6dmzzl7chakfzs85ylq1h")))

