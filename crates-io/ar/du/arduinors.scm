(define-module (crates-io ar du arduinors) #:use-module (crates-io))

(define-public crate-arduinors-0.1.0 (c (n "arduinors") (v "0.1.0") (d (list (d (n "firmata") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lxdzimpkrbyqlz2bn5d18djb7h0klcbfz7ba887vydxz0p7xkjc")))

(define-public crate-arduinors-0.1.1 (c (n "arduinors") (v "0.1.1") (d (list (d (n "firmata") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "128w1pw4gg4dx865mccnnrnwnxjppg12g98bjdqygqjvjw8qda77")))

