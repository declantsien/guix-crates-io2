(define-module (crates-io ar aw arawasu) #:use-module (crates-io))

(define-public crate-arawasu-0.1.0 (c (n "arawasu") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "handlebars") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0x712mdidvscf5g2j7mmzj9kks66by1js8bqy2acld39h52bk061")))

