(define-module (crates-io ar ia aria-of-borrow) #:use-module (crates-io))

(define-public crate-aria-of-borrow-0.2.0 (c (n "aria-of-borrow") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (o #t) (d #t) (k 0)))) (h "0mhhzpkb295kxqzxs68kcp9a56kh921499mih256idci5db3jf2d") (f (quote (("cli" "clap" "tracing-subscriber"))))))

(define-public crate-aria-of-borrow-0.3.0 (c (n "aria-of-borrow") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (o #t) (d #t) (k 0)))) (h "00vy5wrk9zj3d4csgsrf5i0szx84wm587h1mgqbyf43gp50l458d") (f (quote (("default" "cli") ("cli" "clap" "tracing-subscriber"))))))

