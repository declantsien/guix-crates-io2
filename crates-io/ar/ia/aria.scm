(define-module (crates-io ar ia aria) #:use-module (crates-io))

(define-public crate-aria-0.0.0 (c (n "aria") (v "0.0.0") (h "00q8m2nvxqq0dk0l1aprsrwwjs4hrfggvxwfz6c0n0ijl95lwsc2")))

(define-public crate-aria-0.1.0 (c (n "aria") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "096lm775n4lfpn7lmq5mhkpmx9f60i5l18aa89y46c4rfx9jiq0d") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

