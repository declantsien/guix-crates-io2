(define-module (crates-io ar gw argwerk) #:use-module (crates-io))

(define-public crate-argwerk-0.1.0 (c (n "argwerk") (v "0.1.0") (h "0szp95d023z6s805fczpl4czkg1dc5q0x2dqdbchyms2vd7dhmx0")))

(define-public crate-argwerk-0.2.0 (c (n "argwerk") (v "0.2.0") (h "0ljzalp3wyk0f04w4w0fnw51bln3xpnl36c7p972mj3slkklbxcv")))

(define-public crate-argwerk-0.3.0 (c (n "argwerk") (v "0.3.0") (h "0h7sdw3c302x4dci8f7qfp3ds4i36xmy1jqql0k0mh12r0gim6rl")))

(define-public crate-argwerk-0.4.0 (c (n "argwerk") (v "0.4.0") (h "19ax37mnvdaymcx50nplwmknq0gv6s49bp4xcaqli8kmks3b8xb4")))

(define-public crate-argwerk-0.5.0 (c (n "argwerk") (v "0.5.0") (h "0w69yl3ab2rd744msbjhdm42c73v6vi59c4ibis5vaka4i5f96l0")))

(define-public crate-argwerk-0.5.1 (c (n "argwerk") (v "0.5.1") (h "05w3mbwhm74xz4ficv5f0z8a6g0hn2ckxvhxgprng2yb6j6ndlrm")))

(define-public crate-argwerk-0.6.0 (c (n "argwerk") (v "0.6.0") (h "04wsmih18gky4xlvh6c33vpvx94d8rnsqcr11xphl1454lc0aqjr")))

(define-public crate-argwerk-0.7.0 (c (n "argwerk") (v "0.7.0") (h "0crpkza3c424ajqg97d11p056x18h1w8lbiz1r7kmqdb8pcsdm7l")))

(define-public crate-argwerk-0.8.0 (c (n "argwerk") (v "0.8.0") (h "0lmgcdllrs9p75xqv41ybldr7a5wbm3nxnkwj9wdabdjpc63nc5k")))

(define-public crate-argwerk-0.8.1 (c (n "argwerk") (v "0.8.1") (h "0lwa393r8nld6hb64wzw7784flkfpn5qyn37k8wl58xg7kgrnviz")))

(define-public crate-argwerk-0.9.0 (c (n "argwerk") (v "0.9.0") (h "0ih3nhqccyfbvd237crpsnacj034k2lrs268fjg19hi7rqgshpy1")))

(define-public crate-argwerk-0.10.0 (c (n "argwerk") (v "0.10.0") (h "0yl3lqx3jch5d70ip4apisz10l3lmlq3jr11kq2m5lhzl040lxqn")))

(define-public crate-argwerk-0.11.0 (c (n "argwerk") (v "0.11.0") (h "1xqwyp8sl5jkkgm15clg72h182qb7v54xdwxn1h90wb16gbhpgla")))

(define-public crate-argwerk-0.12.0 (c (n "argwerk") (v "0.12.0") (h "00a9jw18pap2klk196a2m7sl7knys1j4krnakzlyyqh09pnls262")))

(define-public crate-argwerk-0.13.0 (c (n "argwerk") (v "0.13.0") (h "1r419gxpysv7n8cy3q1mlzf6q6qmkxkw84k02awp1y11xxan98jk")))

(define-public crate-argwerk-0.13.1 (c (n "argwerk") (v "0.13.1") (h "16p6vi3i9nl8hd7hgpcpb9116prnpasns09ddfbkz295v6599piz")))

(define-public crate-argwerk-0.14.0 (c (n "argwerk") (v "0.14.0") (h "1ich4cghham7mzprwfzsyr994v9rz3im65b6vibvjh6z88hfk5dk")))

(define-public crate-argwerk-0.15.0 (c (n "argwerk") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1jl8sjznxri383gy5pldlns7bpajpk2z95bny6hz8n4vf8gi3vvx")))

(define-public crate-argwerk-0.16.0 (c (n "argwerk") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1nnxrdq95aj3rzfl5mxfk8pyfqhbkrmffd98v6lk6krkbwm0fy2b")))

(define-public crate-argwerk-0.16.1 (c (n "argwerk") (v "0.16.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1187pa7sfwf36ajr2rq2fcjjcnv7qkgw852zyygcvvvzh0n9whl6")))

(define-public crate-argwerk-0.17.0 (c (n "argwerk") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1n72xrkwjljgm651qbpsndfiq603kay8yy4vjgsdhrijxwb328qi")))

(define-public crate-argwerk-0.18.0 (c (n "argwerk") (v "0.18.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1pr54b4izvvh4l1mpc5jg0dldz4mmmx99wd450mrnmfywjysd5jj")))

(define-public crate-argwerk-0.20.0 (c (n "argwerk") (v "0.20.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1abaayix7wx3najf8pirw3i7w1qbl11amh58lslrg6air8krhkj9")))

(define-public crate-argwerk-0.20.1 (c (n "argwerk") (v "0.20.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0xap7fqslqs07k77ssslpqz4sd197sp30prdzs95dqi29cazp2jw")))

(define-public crate-argwerk-0.20.2 (c (n "argwerk") (v "0.20.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0qz4m31r3r3s203v4iz6hlvxqxka6ig7bb9lhadb562bgkpqn0mc")))

(define-public crate-argwerk-0.20.3 (c (n "argwerk") (v "0.20.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1waaz353s9cy9lzx9z352nlbw7i3y3icv1lfjrl3gd03vzr20378") (r "1.56")))

(define-public crate-argwerk-0.20.4 (c (n "argwerk") (v "0.20.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0v4rmscjyrh83lykfmn5gxhri3dg2zpjgnk1gqawzqxr18pkglf3") (r "1.56")))

