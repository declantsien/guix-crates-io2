(define-module (crates-io ar gp argp_derive) #:use-module (crates-io))

(define-public crate-argp_derive-0.1.0 (c (n "argp_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ngf3ilkpqs312gfpkic4rl4p8pf6363g64mxmjl87a9ljbfzwwh") (f (quote (("redact_arg_values")))) (r "1.56.0")))

(define-public crate-argp_derive-0.2.0 (c (n "argp_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c4l3kzszhbhcaz2wnx2v9ybimz11yjf9kfdxw5rbbin1pqi5a3n") (r "1.56.0")))

(define-public crate-argp_derive-0.3.0 (c (n "argp_derive") (v "0.3.0") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "168pwi2z0sx9rhygciqmv3i7bk0831kslvgj1myjzvz0np466dzy") (r "1.56.0")))

