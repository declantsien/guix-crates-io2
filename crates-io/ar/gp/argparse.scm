(define-module (crates-io ar gp argparse) #:use-module (crates-io))

(define-public crate-argparse-0.1.0 (c (n "argparse") (v "0.1.0") (h "06q1f9fqy5md3rabszzaq4ix6l0345jxpqk8jpvjypdkv5lbfvgb")))

(define-public crate-argparse-0.1.1 (c (n "argparse") (v "0.1.1") (h "0aqkfx30372mx719s0fcvvcd6d5xzx2b9fmpylvhygj1rscg31ns")))

(define-public crate-argparse-0.1.2 (c (n "argparse") (v "0.1.2") (h "1sacbvik6ca2fvlg7wis43nygx36jxmqzb42wfz48sdp4wa01l03")))

(define-public crate-argparse-0.1.3 (c (n "argparse") (v "0.1.3") (h "1mm55smfakwx9m3yzawncrq95j0z0g2dy81hbj9j923ws094gd1v")))

(define-public crate-argparse-0.2.0 (c (n "argparse") (v "0.2.0") (h "0bq1l4zabm68vdyrx9d3hfmw0h5la7lkzrsvc8ssxs1ppv2smg1b")))

(define-public crate-argparse-0.2.1 (c (n "argparse") (v "0.2.1") (h "1vv72q0jin7cfs64paj4dxhyh1s20a7mnzr2dqxv5s4ywgsrkfrp")))

(define-public crate-argparse-0.2.2 (c (n "argparse") (v "0.2.2") (h "0iqy2jkifwq0azrrh26qjssp7sknjylycq35jkalzb744xcbz3iz")))

