(define-module (crates-io ar gp argpars) #:use-module (crates-io))

(define-public crate-argpars-0.1.0 (c (n "argpars") (v "0.1.0") (h "1vgzcqbyf4pnwy38zigyz3bp64hnbx5byzk9w6bqw4jrkv27bkc7")))

(define-public crate-argpars-0.1.1 (c (n "argpars") (v "0.1.1") (h "0zfwhcdh8yfac6nsa9wz5blgd1wgfnbmx4fb4imll356sk31p5q8")))

(define-public crate-argpars-0.1.2 (c (n "argpars") (v "0.1.2") (h "108nkaxc6xpxpy7jpi2ns0546a2yfxz2kk3ryj776czj589f8m28")))

(define-public crate-argpars-0.1.3 (c (n "argpars") (v "0.1.3") (h "0hvrdw0j7rz6sp7v6hz13q013j33rn5ppdsjy5jnhnkazkdihj14")))

