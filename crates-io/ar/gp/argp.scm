(define-module (crates-io ar gp argp) #:use-module (crates-io))

(define-public crate-argp-0.1.0 (c (n "argp") (v "0.1.0") (d (list (d (n "argp_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1v7ny66dkw5akg0v3nma0awb972b8sr4sdrj9cyxkdlfkcrrmnr2") (f (quote (("redact_arg_values" "argp_derive/redact_arg_values")))) (r "1.56.0")))

(define-public crate-argp-0.2.0 (c (n "argp") (v "0.2.0") (d (list (d (n "argp_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "015cwyiv5c229lacpg8ymb10x07gsqx7x2szznhzryj3f86cgvhi") (r "1.56.0")))

(define-public crate-argp-0.3.0 (c (n "argp") (v "0.3.0") (d (list (d (n "argp_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1wdrfihpza5apaxadlb422kk05g61ayjf8gbj0574fqsg9bnrhc4") (f (quote (("term_size") ("default" "term_size")))) (r "1.56.0")))

