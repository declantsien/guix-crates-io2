(define-module (crates-io ar gp argparse-sh) #:use-module (crates-io))

(define-public crate-argparse-sh-0.1.0 (c (n "argparse-sh") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1lwx74afkx0g03r85z58ib2p6yp4jzl295d4nk27jd3xld9vzskk")))

(define-public crate-argparse-sh-0.1.1 (c (n "argparse-sh") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "02hs76almlywwd8py7jz4kl9z7a8w3shl5iplmnwkza8am2yg8hv")))

