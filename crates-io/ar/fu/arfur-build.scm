(define-module (crates-io ar fu arfur-build) #:use-module (crates-io))

(define-public crate-arfur-build-0.0.1 (c (n "arfur-build") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.1") (d #t) (k 0)))) (h "09f8gxqz33dm9l7zbgk3xx8s047i7nybkamng2skq6mlspqjh2gr") (r "1.62")))

(define-public crate-arfur-build-0.0.2 (c (n "arfur-build") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.1") (d #t) (k 0)))) (h "15ifh2j0lk3zd5y0jdmnd3gx0y8lsv16rb9hd3xyxhb25vv83pnf") (r "1.62")))

