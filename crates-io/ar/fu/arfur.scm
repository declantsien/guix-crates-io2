(define-module (crates-io ar fu arfur) #:use-module (crates-io))

(define-public crate-arfur-0.0.1 (c (n "arfur") (v "0.0.1") (d (list (d (n "arfur-wpilib") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1m0kgnrn978mxzqlw79bn3170mw09xbpaa3n9l6l5yz49rs4wivv") (f (quote (("wpilib" "arfur-wpilib") ("default" "wpilib")))) (r "1.62")))

(define-public crate-arfur-0.0.2 (c (n "arfur") (v "0.0.2") (d (list (d (n "arfur-rev") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "arfur-wpilib") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1x79x3x8fs34mmf7b97m7a1rl2bjz5ry37fd9qjaibigjd92nq3q") (f (quote (("default" "arfur-wpilib" "arfur-rev") ("bindgen" "arfur-wpilib/bindgen" "arfur-rev/bindgen")))) (r "1.62")))

