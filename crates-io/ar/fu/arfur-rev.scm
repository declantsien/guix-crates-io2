(define-module (crates-io ar fu arfur-rev) #:use-module (crates-io))

(define-public crate-arfur-rev-0.0.1 (c (n "arfur-rev") (v "0.0.1") (d (list (d (n "arfur-build") (r "^0.0.2") (d #t) (k 1)) (d (n "arfur-wpilib") (r "^0.0.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 1)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 1)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "15hzac2bg8kg671jqb5b4r3kjbyjl4lpg3sv0zxysad7db9f6amc") (f (quote (("bindgen" "arfur-build/bindgen" "arfur-wpilib/bindgen")))) (r "1.62")))

