(define-module (crates-io ar it arith_wrappers) #:use-module (crates-io))

(define-public crate-arith_wrappers-0.1.0 (c (n "arith_wrappers") (v "0.1.0") (d (list (d (n "arith_traits") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "050c68ywh52mvp23fw3mim7wkgxlf870bmp95h3i8k64pamn5q26")))

(define-public crate-arith_wrappers-0.2.0 (c (n "arith_wrappers") (v "0.2.0") (d (list (d (n "arith_traits") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1g02p3z62mv205fy6jfrbkashx5c47k9zxvvmq3jc8mpg2sc6sjj")))

