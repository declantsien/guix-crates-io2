(define-module (crates-io ar it arithmetic-coding-core) #:use-module (crates-io))

(define-public crate-arithmetic-coding-core-0.1.0 (c (n "arithmetic-coding-core") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ng9s3jwn9ydgnwsjm0jgadx6cb1s951jqn3lwnz772xc4mrb4qr")))

(define-public crate-arithmetic-coding-core-0.3.0 (c (n "arithmetic-coding-core") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1baqzjs1x3aidp6i7zxwjhzsjiw5vq2w8p3fs7jn82317014j2qf")))

