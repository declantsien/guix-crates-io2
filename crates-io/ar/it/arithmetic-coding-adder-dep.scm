(define-module (crates-io ar it arithmetic-coding-adder-dep) #:use-module (crates-io))

(define-public crate-arithmetic-coding-adder-dep-0.3.1 (c (n "arithmetic-coding-adder-dep") (v "0.3.1") (d (list (d (n "arithmetic-coding-core") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lmh4sxaasiyaiwwkfjyvrh27rp5z9plsbp3vf5lfkwc8vaw7xad")))

(define-public crate-arithmetic-coding-adder-dep-0.3.2 (c (n "arithmetic-coding-adder-dep") (v "0.3.2") (d (list (d (n "arithmetic-coding-core-adder-dep") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qh2c7235nvi7byqi7zmv1hb8p5ziy6rkdq8bkqs2z1lynwmwlcj")))

