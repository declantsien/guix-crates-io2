(define-module (crates-io ar it arithmetic_parser_kma) #:use-module (crates-io))

(define-public crate-arithmetic_parser_kma-0.1.2 (c (n "arithmetic_parser_kma") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1b2vjqbmv00sib58cy6xwrhjfk6nf5nambpd8mz8jg84i00qxzyc")))

