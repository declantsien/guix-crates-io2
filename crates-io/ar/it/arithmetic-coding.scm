(define-module (crates-io ar it arithmetic-coding) #:use-module (crates-io))

(define-public crate-arithmetic-coding-0.1.0 (c (n "arithmetic-coding") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rfwa76q7xm633sz5jlzxf9yn1l1279hvrrfan0v8491maihiv20")))

(define-public crate-arithmetic-coding-0.1.1 (c (n "arithmetic-coding") (v "0.1.1") (d (list (d (n "arithmetic-coding-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1663lq25b4b43zs3xm8c5irs8mkfrbjhc9hcxh59rppfpx9n2lzy")))

(define-public crate-arithmetic-coding-0.2.0 (c (n "arithmetic-coding") (v "0.2.0") (d (list (d (n "arithmetic-coding-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11mlvbq51d3w9m9dv6z51nvlxni1q0i8n4bwlx1i436mpzbh2dzd")))

(define-public crate-arithmetic-coding-0.3.0 (c (n "arithmetic-coding") (v "0.3.0") (d (list (d (n "arithmetic-coding-core") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0d8a1n8hh8jdx8434gp52mvmn8pid5vmyd53xp73hbpxfrdkywpw")))

(define-public crate-arithmetic-coding-0.3.1 (c (n "arithmetic-coding") (v "0.3.1") (d (list (d (n "arithmetic-coding-core") (r "^0.3.0") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16pfc8a9hvmqzv2xjlf0hb0313pr3qpyazp7ycdcjssdqxcdm2if")))

