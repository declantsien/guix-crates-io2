(define-module (crates-io ar it arithmetic_congruence_monoid) #:use-module (crates-io))

(define-public crate-arithmetic_congruence_monoid-1.0.0 (c (n "arithmetic_congruence_monoid") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "12snyvcwpmbpdap0xiby7nz1jvglkbj8h821b029ky3czbbczg3y")))

(define-public crate-arithmetic_congruence_monoid-1.0.1 (c (n "arithmetic_congruence_monoid") (v "1.0.1") (d (list (d (n "arithmetic_congruence_monoid") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1by5rldgbgk1czp714daqbf3fvcvadabjalvh8k2b18rq9jxyk4c")))

(define-public crate-arithmetic_congruence_monoid-1.0.2 (c (n "arithmetic_congruence_monoid") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09f2myfxvdk7nj8izh94fylqphr25z4m9g88729cbs6b97496x3g")))

(define-public crate-arithmetic_congruence_monoid-1.0.3 (c (n "arithmetic_congruence_monoid") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1panap2vv2nzhkl3vhzjkybfz7sxgp556jq7vk73iqbkwr3hkgdm")))

(define-public crate-arithmetic_congruence_monoid-1.0.4 (c (n "arithmetic_congruence_monoid") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0gjx3ynz03w7bq2ir2bydmyk9s78zd5cdxpij6cl0yw51gv5blxj")))

(define-public crate-arithmetic_congruence_monoid-1.1.0 (c (n "arithmetic_congruence_monoid") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (d #t) (k 0)) (d (n "egui") (r "^0.15") (d #t) (k 0)) (d (n "iced") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0q6pl2fbqwjc4q87k5p058p1xql8l8c9adpi4qi81v7ww3nzjhja")))

(define-public crate-arithmetic_congruence_monoid-1.2.0 (c (n "arithmetic_congruence_monoid") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (d #t) (k 0)) (d (n "egui") (r "^0.15") (d #t) (k 0)) (d (n "iced") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0hln2l0kj769r9pnnlnj1swqsifkfk1v7kwf254m0x0mn1jrcr5s")))

(define-public crate-arithmetic_congruence_monoid-1.2.1 (c (n "arithmetic_congruence_monoid") (v "1.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eframe") (r "^0.15") (d #t) (k 0)) (d (n "egui") (r "^0.15") (d #t) (k 0)) (d (n "iced") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0r65fvna35yi1pizyjh7kxlnz8458qcj322wkg62mzly2fynmkpy")))

