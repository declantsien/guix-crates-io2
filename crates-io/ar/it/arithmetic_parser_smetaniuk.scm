(define-module (crates-io ar it arithmetic_parser_smetaniuk) #:use-module (crates-io))

(define-public crate-arithmetic_parser_smetaniuk-0.1.1 (c (n "arithmetic_parser_smetaniuk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "06fqmpgpclcffxdm2z5i2fhf40wyqb5p2m1gxs93b3v221pgqilv")))

(define-public crate-arithmetic_parser_smetaniuk-0.1.2 (c (n "arithmetic_parser_smetaniuk") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0p2jm8zzqld8h4k33yfdvyxsdmdysb48vb0jkxzlkxx9cfkiy3b4")))

