(define-module (crates-io ar it arith) #:use-module (crates-io))

(define-public crate-arith-0.1.0 (c (n "arith") (v "0.1.0") (h "0l22kxm8q2rkz1n0i77jfaxzp8lf9yxp32rmh0zgx28h7yb8drr7")))

(define-public crate-arith-0.2.0 (c (n "arith") (v "0.2.0") (h "1lhz2k1n4iqv6nbg19igagnhjg3gjgcnaqzspc8f4r7nz8l7d4ni")))

(define-public crate-arith-0.2.1 (c (n "arith") (v "0.2.1") (h "19qrwa7hkj870aywd4p0cbjin6swll4fnsvz82k665na3889yp0v")))

(define-public crate-arith-0.2.2 (c (n "arith") (v "0.2.2") (h "1yhxdg1rbfc0k9ynpr1if4j1s5rqhzgxzlv8kzqrdlc0pnm6rb70")))

(define-public crate-arith-0.2.3 (c (n "arith") (v "0.2.3") (h "1ffasafx8gzzxnmwshshxfhlmk57ypwvvidwrxn5v7v45krsnv2m")))

