(define-module (crates-io ar it arithmancy) #:use-module (crates-io))

(define-public crate-arithmancy-0.0.1 (c (n "arithmancy") (v "0.0.1") (d (list (d (n "cargo-tinyrick") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)))) (h "0x145mc3686bscxxzgzvxpw9h6f3qsgaj2kbw7wlph3l5yzfzyw2") (f (quote (("letmeout" "cargo-tinyrick"))))))

