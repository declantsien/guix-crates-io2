(define-module (crates-io ar it arithmetic-typing) #:use-module (crates-io))

(define-public crate-arithmetic-typing-0.3.0 (c (n "arithmetic-typing") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "arithmetic-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1a79s1jgygggs07h3x4fwwdf8vhd6cpx5ynmxqid9g2mzzcyzh4q")))

