(define-module (crates-io ar it arith_traits) #:use-module (crates-io))

(define-public crate-arith_traits-0.1.0 (c (n "arith_traits") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03cvk7xbjz3660y081n3gy1lqp56sf9kxf0wgn72phq0rfw9q7bx")))

(define-public crate-arith_traits-0.2.0 (c (n "arith_traits") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "020nlfrgbprm64k59s95xjzrjs43x3ilhqfsp64z002yz8495s3y")))

(define-public crate-arith_traits-0.3.0 (c (n "arith_traits") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1jyh7cr671cv1hc7h8b7bg8c942mwp73q6by6zlv9a8blspa9xnj") (f (quote (("default")))) (y #t) (s 2) (e (quote (("xl_value_types" "dep:num"))))))

(define-public crate-arith_traits-0.3.1 (c (n "arith_traits") (v "0.3.1") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1jjygxc9in6d8rlz9pa4vfg37ay7w9sk16b2zqhnwfgfcfx5pmcv") (f (quote (("default")))) (y #t) (s 2) (e (quote (("xl_value_types" "dep:num"))))))

(define-public crate-arith_traits-0.3.2 (c (n "arith_traits") (v "0.3.2") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0dsxhwyap2y6mf9ddz4mh0kga879h2vv13gz0lmwxzjmp8pih8k0") (f (quote (("default")))) (s 2) (e (quote (("xl_types" "dep:num"))))))

