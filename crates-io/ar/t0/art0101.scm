(define-module (crates-io ar t0 art0101) #:use-module (crates-io))

(define-public crate-art0101-0.1.0 (c (n "art0101") (v "0.1.0") (h "0afhid245ac7j42c5x9arr1b31f2gsjypzrs2zxc13zs7899wv6l") (y #t)))

(define-public crate-art0101-0.1.1 (c (n "art0101") (v "0.1.1") (h "0c3ppy7jj3xsh31p3kxgp5s1dd4brg8w2zcqjl28jjq6ciqw87az")))

