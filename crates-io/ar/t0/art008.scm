(define-module (crates-io ar t0 art008) #:use-module (crates-io))

(define-public crate-art008-0.1.0 (c (n "art008") (v "0.1.0") (h "0aij1a45j8g376y7b1fvzg332pnhhkxwviifvgramsahmsjxijk6")))

(define-public crate-art008-0.1.1 (c (n "art008") (v "0.1.1") (h "0i8nbpm326a63ybdwrrqx2dvfykafky7zsbhp1vsxcnsi24p6x9b")))

