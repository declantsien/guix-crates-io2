(define-module (crates-io ar ke arkecosystem-crypto) #:use-module (crates-io))

(define-public crate-arkecosystem-crypto-0.1.0 (c (n "arkecosystem-crypto") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "ripemd160") (r "^0.7.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1fjy38n46m0k5kfcqv5kfyazd3wzwqbhh784dvr37fgl7jhqr87l")))

