(define-module (crates-io ar ke arkecosystem-client) #:use-module (crates-io))

(define-public crate-arkecosystem-client-1.0.0 (c (n "arkecosystem-client") (v "1.0.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "mockito") (r "^0.14.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1wlc4bkkby6n2nijmgyz6fp40bd4q8x9bgfphsvq8wn8fc3nwnz7")))

