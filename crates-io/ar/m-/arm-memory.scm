(define-module (crates-io ar m- arm-memory) #:use-module (crates-io))

(define-public crate-arm-memory-0.1.0 (c (n "arm-memory") (v "0.1.0") (d (list (d (n "coresight") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "jep106") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "scroll") (r "^0.9.2") (d #t) (k 0)))) (h "13ld6mipc12hgzk9k8kckqz4ais6lw7pj61zwxrfks59yn4qpqz0") (y #t)))

