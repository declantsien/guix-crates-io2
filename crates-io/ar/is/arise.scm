(define-module (crates-io ar is arise) #:use-module (crates-io))

(define-public crate-arise-0.1.0 (c (n "arise") (v "0.1.0") (h "07k19q3x1hj4z8r92chfxcsb31fh36scxy4gfkmcdnbg5szzwm9b")))

(define-public crate-arise-0.1.1 (c (n "arise") (v "0.1.1") (h "140pyx3sa8pkxq3i74zgg16haqainkdp14ca0h3bga0sp3zhii7p")))

(define-public crate-arise-0.1.2 (c (n "arise") (v "0.1.2") (h "069c2a49v8vvxb1c26jcrw660jdj5776mglf84n4pv6afrvzdkg0")))

