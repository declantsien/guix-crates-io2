(define-module (crates-io ar is aristeia) #:use-module (crates-io))

(define-public crate-aristeia-0.1.0 (c (n "aristeia") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0lwib70amhrwpmksym6dphqandg4kizjcmfg967bvj9rl2ynkgza")))

(define-public crate-aristeia-0.1.1 (c (n "aristeia") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1dvfgikjcf3f9wf8frk4r1jczw38106g6g0m3mbx996457qy6wcw")))

(define-public crate-aristeia-0.2.0 (c (n "aristeia") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1hqqnv678kb62y292danndj4qgk3jbhxw9r6xc21872kz04minqd")))

(define-public crate-aristeia-0.2.1 (c (n "aristeia") (v "0.2.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0gvhc1fx6mas1cssgay7d9xxkklv7hbqm4p9jf47xql3gv4gmhbw")))

(define-public crate-aristeia-0.2.2 (c (n "aristeia") (v "0.2.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0qx3f6wy1lb757safp1b03rsaa9hfhbblayg39cpb68kczx72ra0")))

(define-public crate-aristeia-0.2.3 (c (n "aristeia") (v "0.2.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0cx4frs07mz9njfzyk2ib3fa8wwkmcvxg7pxbh697dcllp0pisrw")))

