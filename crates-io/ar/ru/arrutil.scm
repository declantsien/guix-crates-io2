(define-module (crates-io ar ru arrutil) #:use-module (crates-io))

(define-public crate-arrutil-0.1.0 (c (n "arrutil") (v "0.1.0") (h "0s2jpws7rf53jl3svmnfvgdzb3nl9pc6cvn4a72afrg12qy02j9x")))

(define-public crate-arrutil-0.1.1 (c (n "arrutil") (v "0.1.1") (h "1l17fgd195m7isf3cx2bf1sfsvbhvkiwz7nfdn12bn5k2nk0ilic")))

(define-public crate-arrutil-0.1.2 (c (n "arrutil") (v "0.1.2") (h "1a71qh7bnpq9lmj1zy3h0fd27i1ym7xpxwpq62fs98cw158gc70n")))

(define-public crate-arrutil-1.0.0 (c (n "arrutil") (v "1.0.0") (h "197ngrklx600vymr4dn5c0q0m8vwankhby48bnqcrjv1fkj2xqx8")))

