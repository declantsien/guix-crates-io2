(define-module (crates-io ar cm arcmutex) #:use-module (crates-io))

(define-public crate-arcmutex-0.1.0 (c (n "arcmutex") (v "0.1.0") (h "1g6sqavlpq69hydakncfkl5qnzwlbhl5z4my9v8z7595sn5v5apf")))

(define-public crate-arcmutex-0.1.1 (c (n "arcmutex") (v "0.1.1") (h "18mzaimhgwv9zflwvzn3z3k9n5rjz1amjv70pddvk7h87ig72z41")))

(define-public crate-arcmutex-0.2.0 (c (n "arcmutex") (v "0.2.0") (h "1cixys988frxal8ssh4yca3cv03pnf33swwcnllsn5hbxw985hbc")))

