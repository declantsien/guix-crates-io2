(define-module (crates-io ar cm arcmut) #:use-module (crates-io))

(define-public crate-arcmut-0.1.0 (c (n "arcmut") (v "0.1.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pgxb8kijzn2wx87ng9sm0x0appk4r53412frhmcznr4bi11igxz") (f (quote (("std") ("default" "std")))) (r "1.56")))

