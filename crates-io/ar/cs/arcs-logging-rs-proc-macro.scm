(define-module (crates-io ar cs arcs-logging-rs-proc-macro) #:use-module (crates-io))

(define-public crate-arcs-logging-rs-proc-macro-0.1.0 (c (n "arcs-logging-rs-proc-macro") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0z3654jvp28kbmb9wfyda24kadi1b5apd8f1l2khvhynsigjq6pz")))

(define-public crate-arcs-logging-rs-proc-macro-0.1.1 (c (n "arcs-logging-rs-proc-macro") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0nwwllyny4vr3lclx53axknwbc37vq3s78b34bq1w2m4l2pdvimw")))

