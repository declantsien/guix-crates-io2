(define-module (crates-io ar cs arcstr) #:use-module (crates-io))

(define-public crate-arcstr-0.1.0 (c (n "arcstr") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "1zzqi4gzkmgns8mys6mm84pll7yhrdfj56zixdq6y0x6kh32bacz") (f (quote (("std") ("default"))))))

(define-public crate-arcstr-0.2.0 (c (n "arcstr") (v "0.2.0") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "0x9kid94ys312z3pd7yf1wl6wyp6cqkpzqqnk6pix9rljsnxpaf8") (f (quote (("std") ("default"))))))

(define-public crate-arcstr-0.2.1 (c (n "arcstr") (v "0.2.1") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "0azhbgzi0a9n8a9p53cw4nwid4qkmw60lyz4hadk8nmpjkjwa388") (f (quote (("std") ("default"))))))

(define-public crate-arcstr-0.2.2 (c (n "arcstr") (v "0.2.2") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "0ayswbmj6nz1x6mf1lj7ld9xav7ma4bhn5r4m49fx1kizjzm7dlg") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-0.2.3 (c (n "arcstr") (v "0.2.3") (d (list (d (n "loom") (r "^0.3") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "1gblgbc7jnciq4syayf56m93ncm0rma0jxcpcva70ybviw1gw43s") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.0.0 (c (n "arcstr") (v "1.0.0") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "1yjxvp8prjb19v6kdh9ik0ssx77q1pr5az20jd9z7a4sdhj4nksi") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.0 (c (n "arcstr") (v "1.1.0") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "1xpmjwm1i08s3naav9g1vg5yvsvq2h15fdf8ak6x2iz50mz2sa63") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.1 (c (n "arcstr") (v "1.1.1") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "13j5l5484qhg0d0xq3wkdx70pysj7avcibywzc8nv23aq2570yi0") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.2 (c (n "arcstr") (v "1.1.2") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "134fc7v5yxzb0wdjfk5yhjl8xx1h9mgl19h5a47i8h0gs0ci32lj") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.3 (c (n "arcstr") (v "1.1.3") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "18wmwd47cjnxxskcdwbgg9a4mf75arw94lnp81d7wcmdavfsbs4n") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.4 (c (n "arcstr") (v "1.1.4") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "019mzv3jnibg6pm2par33z6df2sl6wiqvibbndchvdxqc5hbspld") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.1.5 (c (n "arcstr") (v "1.1.5") (d (list (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 2)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "03j66j5wlfah21adw1jdc7qwpxhfird8bavspc906gaaan0p541z") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr"))))))

(define-public crate-arcstr-1.2.0 (c (n "arcstr") (v "1.2.0") (d (list (d (n "loom") (r "^0.7.1") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "0vbyslhqr5fh84w5dd2hqck5y5r154p771wqddfah0bpplyqr483") (f (quote (("substr-usize-indices" "substr") ("substr") ("std") ("default" "substr")))) (r "1.57.0")))

