(define-module (crates-io ar cs arcs-ctf_yaml-parser) #:use-module (crates-io))

(define-public crate-arcs-ctf_yaml-parser-0.1.0 (c (n "arcs-ctf_yaml-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1w8xgbn9iclq2l8dpjhgjnlcdc2majv8bpg4w4d34h4xnmb97say")))

