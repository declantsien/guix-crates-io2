(define-module (crates-io ar cs arcslab) #:use-module (crates-io))

(define-public crate-arcslab-0.1.0 (c (n "arcslab") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("mm"))) (t "cfg(all(not(miri), target_os = \"linux\"))") (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1z69j1lkqwddxwfqkyxq4a85n9naqmajf8sfn6nd7jv37jc4qq10")))

