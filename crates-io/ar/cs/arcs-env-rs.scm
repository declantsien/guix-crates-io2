(define-module (crates-io ar cs arcs-env-rs) #:use-module (crates-io))

(define-public crate-arcs-env-rs-0.1.0 (c (n "arcs-env-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1h2swvia7mbb1clknrrjv9gip254kqzv0pjjlkjypiksdlmgg96n")))

(define-public crate-arcs-env-rs-0.2.0 (c (n "arcs-env-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1wklrcbiiig2h78gq65xg2imjd4l65qrv12qp16zqq8lzawiz9gc")))

(define-public crate-arcs-env-rs-0.2.1 (c (n "arcs-env-rs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1icgkslg44vzn720sbkz4nhqlq6r373s1plc7z1p5d3b3qb0w5wx")))

