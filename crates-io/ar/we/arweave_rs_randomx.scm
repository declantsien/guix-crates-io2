(define-module (crates-io ar we arweave_rs_randomx) #:use-module (crates-io))

(define-public crate-arweave_rs_randomx-1.2.1 (c (n "arweave_rs_randomx") (v "1.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "056mn67543ckp5nm6ri9q1kfihlcpy02i840522yzs6bgksf3psk")))

