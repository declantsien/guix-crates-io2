(define-module (crates-io ar we arweave_rs_indexes) #:use-module (crates-io))

(define-public crate-arweave_rs_indexes-0.1.0 (c (n "arweave_rs_indexes") (v "0.1.0") (d (list (d (n "arweave_rs_types") (r "^0.1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h0zc76263y03f3qmdvaab3fm328ws0z150g01awa4d0nfjn6qdd")))

