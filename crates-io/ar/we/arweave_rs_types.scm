(define-module (crates-io ar we arweave_rs_types) #:use-module (crates-io))

(define-public crate-arweave_rs_types-0.1.0 (c (n "arweave_rs_types") (v "0.1.0") (d (list (d (n "arweave_rs_randomx") (r "^1.2.1") (d #t) (k 0)) (d (n "base64-url") (r "^2.0.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "fixed-hash") (r "^0.8.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uint") (r "^0.9.5") (d #t) (k 0)))) (h "0r4qbcw93ispfhgfc86h0ws9gck24pyqfjd6ppih7ih5ppff7vp1")))

