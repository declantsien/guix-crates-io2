(define-module (crates-io ar we arweave_rs_vdf) #:use-module (crates-io))

(define-public crate-arweave_rs_vdf-0.1.0 (c (n "arweave_rs_vdf") (v "0.1.0") (d (list (d (n "arweave_rs_types") (r "^0.1.0") (d #t) (k 0)) (d (n "base64-url") (r "^2.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1ndb2a3g44ca2kj9pqvrxmiyhbgw4d452lwzbys5c8nbyry7ib3a")))

