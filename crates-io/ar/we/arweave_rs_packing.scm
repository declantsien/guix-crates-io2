(define-module (crates-io ar we arweave_rs_packing) #:use-module (crates-io))

(define-public crate-arweave_rs_packing-0.1.0 (c (n "arweave_rs_packing") (v "0.1.0") (d (list (d (n "arweave_rs_randomx") (r "^1.2.1") (d #t) (k 0)) (d (n "arweave_rs_types") (r "^0.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)))) (h "0vcxqwjw1dy2y7iqg77i4c4frz304zz2gddgp6wh65a3bx7cl21p")))

