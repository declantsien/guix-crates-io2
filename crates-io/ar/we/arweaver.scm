(define-module (crates-io ar we arweaver) #:use-module (crates-io))

(define-public crate-arweaver-0.1.0 (c (n "arweaver") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "openssl") (r "^0.10.25") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "1dpzjr63dkjd5763qz7qwp45ipdf76ky492vwxxgsbhcy7gs5ywc")))

