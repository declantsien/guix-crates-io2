(define-module (crates-io ar sc arsc) #:use-module (crates-io))

(define-public crate-arsc-0.1.0 (c (n "arsc") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1fagxi8m4i8yp8kxbsjs4nss6bq8pm3xzjz1pfag8lbhasv07zif")))

(define-public crate-arsc-0.1.1 (c (n "arsc") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "10s813kwmyzi21fv83057vxkkvmqvzsk14s135rn0d6kxa414hr4")))

(define-public crate-arsc-0.1.2 (c (n "arsc") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "01m0vdvqncc315nrfnca68lvwy0snqlx7a47yb6hw7rszx2xc2dj")))

(define-public crate-arsc-0.1.3 (c (n "arsc") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1lb05qfyvhia6l69da9qrcvqdj2xp6vbkkii4kr64f8f67sfywhs")))

(define-public crate-arsc-0.1.4 (c (n "arsc") (v "0.1.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1i4najzav1nywlxbl58qxfsn9yd6798jrn7q8442dq8nzc8fbym4")))

(define-public crate-arsc-0.1.5 (c (n "arsc") (v "0.1.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1wb113cc32kaj3wkzq60wiv2y5sjikka3rrr772m2l2f95j15iiw")))

