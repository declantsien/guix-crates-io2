(define-module (crates-io ar _p ar_pe_ce_macro) #:use-module (crates-io))

(define-public crate-ar_pe_ce_macro-0.0.1 (c (n "ar_pe_ce_macro") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "107g707xd0n127pmx9py6dbf3i1bxfvdmc0ndy9fzbik6lh9a8z2")))

(define-public crate-ar_pe_ce_macro-0.1.0 (c (n "ar_pe_ce_macro") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fy61bparprk1zd99lis0crc14sq48gfwv0s6slw9xghwh6ay49q")))

(define-public crate-ar_pe_ce_macro-0.1.1 (c (n "ar_pe_ce_macro") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08d3g82mpyd90i2gw6jqc09h8p746cs6g9xlxnm7akkx9jkzwznk")))

