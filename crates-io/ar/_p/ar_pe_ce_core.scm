(define-module (crates-io ar _p ar_pe_ce_core) #:use-module (crates-io))

(define-public crate-ar_pe_ce_core-0.1.0 (c (n "ar_pe_ce_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-stream") (r "^0.3.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rjk2nqa862nz4c7jpq34jc20qb6qlwvxqz117jh8vybh3b3nnsf")))

