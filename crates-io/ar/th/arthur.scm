(define-module (crates-io ar th arthur) #:use-module (crates-io))

(define-public crate-arthur-0.1.0 (c (n "arthur") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "05wr9v55vpq7gizr7vgb8w5gwwm3rgmq4kgmgw5fzaqdywl17s50")))

