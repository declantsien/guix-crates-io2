(define-module (crates-io ar th artha) #:use-module (crates-io))

(define-public crate-artha-0.1.0 (c (n "artha") (v "0.1.0") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "071znj2cs08api74yvfcjfcxhvn2b2g3awl6yzbggqmq87afivq9")))

(define-public crate-artha-0.2.0 (c (n "artha") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "073x6bx135s82dcqpcivzqjr6jpgh2ac2dhmz5zx8bsfxf6csn6a")))

(define-public crate-artha-0.2.1 (c (n "artha") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "10ky0n8igiarpmydiv5iq71ib1b0sjpd8rvrwrhdkjs7rb0b7cy7")))

