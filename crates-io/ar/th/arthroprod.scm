(define-module (crates-io ar th arthroprod) #:use-module (crates-io))

(define-public crate-arthroprod-0.1.2 (c (n "arthroprod") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.3.1") (d #t) (k 2)))) (h "0fd9sly1dga8p74lv3n1y2kbm3prl4zcr15r2vm3alahnq77qbsg")))

(define-public crate-arthroprod-0.1.3 (c (n "arthroprod") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.3.1") (d #t) (k 2)))) (h "1sawsrd8j8ylz950wls6rvmzr1ickw9shy66f5jfngzy52mnasfg")))

(define-public crate-arthroprod-0.2.0 (c (n "arthroprod") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "0yzm7qv157c0nhq086l4rn9chx5nsh78w3kwckhxx0m0hzamy7x0")))

(define-public crate-arthroprod-0.2.1 (c (n "arthroprod") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "06naj081rfj7wby7djs2aq54xcym52x6pvzmc12j1a32gf49nyby")))

(define-public crate-arthroprod-0.2.2 (c (n "arthroprod") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "1wmbp0lyd3yd6lxa9gb6i3123l6yd835x42ggvzj9y9pxd4wvd4a")))

(define-public crate-arthroprod-0.2.3 (c (n "arthroprod") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "075abgkir0gxk28jhn3nysqfnwwr1ipz9l41ah5fdmq5zsgiabfn")))

(define-public crate-arthroprod-0.3.0 (c (n "arthroprod") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^1.0") (d #t) (k 2)))) (h "164arnfbz63jhpc3l0h05518akcdasl1mq3y2jcrc8zah571bjrm")))

