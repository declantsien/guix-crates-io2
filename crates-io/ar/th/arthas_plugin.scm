(define-module (crates-io ar th arthas_plugin) #:use-module (crates-io))

(define-public crate-arthas_plugin-0.1.0 (c (n "arthas_plugin") (v "0.1.0") (d (list (d (n "ecp") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nvq39zx2l7h0vm820jld4cnb54jjkjkyskw91h747bj02iv27kf")))

(define-public crate-arthas_plugin-0.1.1 (c (n "arthas_plugin") (v "0.1.1") (d (list (d (n "ecp") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "130xb72q5q91qddqhrjn120ksgnpj9gzypyhcjw0vazq78i4v98s")))

