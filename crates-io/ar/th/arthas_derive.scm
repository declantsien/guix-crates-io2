(define-module (crates-io ar th arthas_derive) #:use-module (crates-io))

(define-public crate-arthas_derive-0.1.0 (c (n "arthas_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0y5x6r4x2pvhlj98lvkd6jjicql18z286d6ajv7f5fzkicq954a3")))

