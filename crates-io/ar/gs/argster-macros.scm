(define-module (crates-io ar gs argster-macros) #:use-module (crates-io))

(define-public crate-argster-macros-0.1.0 (c (n "argster-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "02alb89azfd6d2ibzig8hp51q835ggnsd04zvmj28m73hf6kww63")))

(define-public crate-argster-macros-0.1.1 (c (n "argster-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1v3iv3527nccvic66q6bpjn88m4l7b6jxc116d4a0c6f60p3pq")))

