(define-module (crates-io ar gs args-functional) #:use-module (crates-io))

(define-public crate-args-functional-0.1.0 (c (n "args-functional") (v "0.1.0") (h "0vrfz9y8p1k6313vw4kj690f8s44rnajim13dpar50fyfj41i8x7")))

(define-public crate-args-functional-0.1.1 (c (n "args-functional") (v "0.1.1") (h "0fc09yqrxyj5ak809ax2zf32badf6xf6ji4x2blxg92jrj1pihsp")))

(define-public crate-args-functional-0.1.2 (c (n "args-functional") (v "0.1.2") (h "1pllah8dw734d3468bgp1bfa23qa63snhq2ws1bkan343fnmf233")))

