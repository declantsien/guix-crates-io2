(define-module (crates-io ar gs argsplitter) #:use-module (crates-io))

(define-public crate-argsplitter-0.4.0 (c (n "argsplitter") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)))) (h "1ra1701bga2y2jyzp6wj8lfv87krfykglklbhgvdpclds9f01d1z")))

(define-public crate-argsplitter-0.5.0 (c (n "argsplitter") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)))) (h "1ylcpi6brmc4p7dgd8jrqx2d7kjkx9yyznk5fiw14lm7z19linzg")))

