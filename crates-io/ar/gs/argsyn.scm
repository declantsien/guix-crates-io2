(define-module (crates-io ar gs argsyn) #:use-module (crates-io))

(define-public crate-argsyn-0.1.0 (c (n "argsyn") (v "0.1.0") (h "0cwp147sbmpg4fvl4nc9z8yg0421vva4c1c1lqq15xb9zkhqcbbf")))

(define-public crate-argsyn-0.1.1 (c (n "argsyn") (v "0.1.1") (h "05gdlc8zl0s3gsv5rv2dsdxnpyppqd80c6l3861s6wmlqxrisgdx")))

(define-public crate-argsyn-0.1.2 (c (n "argsyn") (v "0.1.2") (h "0i05x3f1w219155d28cz7gcpajny35a1qywpkbwadk3qhlhdjmz4")))

