(define-module (crates-io ar gs argser-macros) #:use-module (crates-io))

(define-public crate-argser-macros-0.1.0 (c (n "argser-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0gf6z50rzj2wdjqprbxh6bp4vs240wjpx551gq6dbsyg209ix66n")))

(define-public crate-argser-macros-0.2.0 (c (n "argser-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1lz03z7qpgyfgvfqsgv4mcb08yyi453m05pjah705vv47ffkysbw")))

(define-public crate-argser-macros-0.2.1 (c (n "argser-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1l6b14s5xcvca7f3yw1fpm54lgncachnjwckidwc975mgqifyy21")))

(define-public crate-argser-macros-0.2.2 (c (n "argser-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "13p7yk9zr9hsz467ywssd5rjx92zz21jb0sgl69hg3yx8a40jjj1")))

