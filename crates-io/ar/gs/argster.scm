(define-module (crates-io ar gs argster) #:use-module (crates-io))

(define-public crate-argster-0.1.0 (c (n "argster") (v "0.1.0") (d (list (d (n "argster-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h1kp4dagwlggpr7jk09bwzpl7zzzcf9fsd77x3zb204pqis9vy6")))

(define-public crate-argster-0.1.1 (c (n "argster") (v "0.1.1") (d (list (d (n "argster-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lym1bag7krhp2jkqp4icyqb0bw8kxy68fhmx7cnm06kbdncjmwj")))

