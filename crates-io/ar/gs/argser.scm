(define-module (crates-io ar gs argser) #:use-module (crates-io))

(define-public crate-argser-0.1.0 (c (n "argser") (v "0.1.0") (d (list (d (n "argser-macros") (r "^0.1") (d #t) (k 0)))) (h "1c62k9m4z45qyw88nbmn63h1wk2mbjxc3yq6rh11462k79xqdd8c")))

(define-public crate-argser-0.2.0 (c (n "argser") (v "0.2.0") (d (list (d (n "argser-macros") (r "^0.2") (d #t) (k 0)))) (h "1yfa9avrfrh3f4l4zxr0fd42n5l5xlg93qy91vbpixr37w2hqpk7")))

(define-public crate-argser-0.2.1 (c (n "argser") (v "0.2.1") (d (list (d (n "argser-macros") (r "^0.2.1") (d #t) (k 0)))) (h "0px1lpxfp6wydckadx6j32151yy7f362z0nwcw4anaff4adx0mxl")))

(define-public crate-argser-0.2.2 (c (n "argser") (v "0.2.2") (d (list (d (n "argser-macros") (r "^0.2.1") (d #t) (k 0)))) (h "12kvcdmh3ni4nsgpff2c37rmzc3skjakqfm4gk3s98jkavq7yn2m")))

(define-public crate-argser-0.2.3 (c (n "argser") (v "0.2.3") (d (list (d (n "argser-macros") (r "^0.2.2") (d #t) (k 0)))) (h "1ypvlwxrnm9cn6ryh14hzy1r7b1n9237mw336d9q3xv50y60s9br")))

