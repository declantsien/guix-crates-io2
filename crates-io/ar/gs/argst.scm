(define-module (crates-io ar gs argst) #:use-module (crates-io))

(define-public crate-argst-0.1.0 (c (n "argst") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "11mrgrp6a3sjfqcgzql6c5pdxdimqk7nwqxwb7jgrzgyglmxsqhq")))

(define-public crate-argst-0.1.1 (c (n "argst") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1h4s29p8l2apzxmfnxv3a78qyrv4zs9ql1l23cmf859py4dfyzwy")))

