(define-module (crates-io ar gs args_flags_1) #:use-module (crates-io))

(define-public crate-args_flags_1-0.1.0 (c (n "args_flags_1") (v "0.1.0") (h "00phvr2fcwicyjbc6ca3ffj0cl6cpr69wnv6f8n0c3vmxdlnndlb")))

(define-public crate-args_flags_1-0.1.1 (c (n "args_flags_1") (v "0.1.1") (h "1n9l843nycqx99bwjlwmn8m9k2imzwb67kbx8bzi1lmd1v2a17sh")))

(define-public crate-args_flags_1-0.2.0 (c (n "args_flags_1") (v "0.2.0") (h "1xjck6k2lsc23kmahzl70hylryp7p5l6wcy12i4dc2xa6vga6lrp")))

