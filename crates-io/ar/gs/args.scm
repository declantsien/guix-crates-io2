(define-module (crates-io ar gs args) #:use-module (crates-io))

(define-public crate-args-0.1.0 (c (n "args") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0kqaxhxdvm1xgb85hxn6ncg31h71v0jb14s7f2lhxh8cwxp0rdlq")))

(define-public crate-args-0.1.1 (c (n "args") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1ki2js3a25qy95gxwpwrvccjhc3bdrfrng72xvf0nd0lc9vdi1xz")))

(define-public crate-args-1.0.0 (c (n "args") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "01198dbkibaxxldjzsk42fkkmb5hc741xmmvp483fx6pmmsfbyjr")))

(define-public crate-args-1.0.1 (c (n "args") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1770g7cmiw3cnjbs093kymnfss808az5q9bzggqri0s2p3am22i3")))

(define-public crate-args-1.0.2 (c (n "args") (v "1.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0lizrzbhqwna2dz8qw1vqxl5abdmc2s7aqbyykp0s9av414657ms")))

(define-public crate-args-2.0.0 (c (n "args") (v "2.0.0") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0hapw98skbldy2drwkx0kna40jisdmc1sjm5mzczfm0by0p135vy")))

(define-public crate-args-2.0.1 (c (n "args") (v "2.0.1") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1z2wy8mnq5nm3z22q2xg21qhhdx7kj3ww1r47l9zxp4ws6v4qpn5") (y #t)))

(define-public crate-args-2.0.2 (c (n "args") (v "2.0.2") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0fvbgqly3hrc1xkv36412qbx4gikn58clmidgzrihphdv2p69iza")))

(define-public crate-args-2.0.3 (c (n "args") (v "2.0.3") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1bx71zng8v6bsk9kaf1kxkxp1i6bk6m29gdbwrk99j06jh500ki5")))

(define-public crate-args-2.0.4 (c (n "args") (v "2.0.4") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "0xv9ps04sxylz5yy17n1y9qf7zgw63ph5ningv2z8hwdb80g7b95")))

(define-public crate-args-2.1.0 (c (n "args") (v "2.1.0") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "1rwjqv7r0q4kg3mbs3j7844w8ascg35imrjmglkprpd0h1f2j44w")))

(define-public crate-args-2.2.0 (c (n "args") (v "2.2.0") (d (list (d (n "getopts") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "02wpdlzdkqh0qg49yiv8jj9pwh4dvlhf0mid0df8syqpcln47dyl")))

