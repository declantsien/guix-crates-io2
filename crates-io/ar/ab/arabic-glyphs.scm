(define-module (crates-io ar ab arabic-glyphs) #:use-module (crates-io))

(define-public crate-arabic-glyphs-0.1.0 (c (n "arabic-glyphs") (v "0.1.0") (h "1l719yz71iy2ac22145vd28cyzzak4zql14j1p991lhbknarf6ba") (y #t)))

(define-public crate-arabic-glyphs-0.1.1 (c (n "arabic-glyphs") (v "0.1.1") (h "0j2nygv5q017ra0xz3cxldjpmqv7bd2ss47y43wciap4d5k74j5j") (y #t)))

(define-public crate-arabic-glyphs-0.1.2 (c (n "arabic-glyphs") (v "0.1.2") (h "0dy6vq6mfj66d3w9klrg5rrjai5ih1pwzl7qvpnfhikjsvh52n0c") (y #t)))

