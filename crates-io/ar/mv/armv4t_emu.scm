(define-module (crates-io ar mv armv4t_emu) #:use-module (crates-io))

(define-public crate-armv4t_emu-0.1.0 (c (n "armv4t_emu") (v "0.1.0") (d (list (d (n "capstone") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3") (o #t) (d #t) (k 0)))) (h "17v80mmg9cmkj7q50b93ndw0pngimg1s7399qjqfndhf2h3k7xsx") (f (quote (("serde-serialize" "serde" "serde-big-array") ("default") ("advanced_disasm" "capstone"))))))

