(define-module (crates-io ar mv armv7) #:use-module (crates-io))

(define-public crate-armv7-0.1.0 (c (n "armv7") (v "0.1.0") (d (list (d (n "register") (r "^0.5.0") (d #t) (k 0)))) (h "0s9c3pqnjyixn69b41dzrgzxwja1ykjqh7j6h32kkghlx3flyya4")))

(define-public crate-armv7-0.2.0 (c (n "armv7") (v "0.2.0") (d (list (d (n "register") (r "^0.5.0") (d #t) (k 0)))) (h "0i894b74ffyg4nfppg28ddp78gl31jlb64vl0fszlxw7n6wxwq8w")))

(define-public crate-armv7-0.2.1 (c (n "armv7") (v "0.2.1") (d (list (d (n "register") (r "^0.5.0") (d #t) (k 0)))) (h "1sj1w25bcy49agzalwyxark6w9in4a34qkkh51v8jx01lxmhrmjl")))

