(define-module (crates-io ar mv armv5te) #:use-module (crates-io))

(define-public crate-armv5te-0.1.0 (c (n "armv5te") (v "0.1.0") (h "106wf0wlabj2izj6hnf0gpxymfif3kw30idrjf890a2jcxs3jq67")))

(define-public crate-armv5te-0.2.0 (c (n "armv5te") (v "0.2.0") (h "1hz533bn4g1mkmg2mbv7ivvhx390vy72mc503nw2g5k2iply49bl")))

(define-public crate-armv5te-0.3.0 (c (n "armv5te") (v "0.3.0") (h "012z0g1fhakc2pr49wfnmzhw46ds3mpwjhqs070vxv9lmangk07x")))

