(define-module (crates-io ar mv armv6-m-instruction-parser) #:use-module (crates-io))

(define-public crate-armv6-m-instruction-parser-0.1.0 (c (n "armv6-m-instruction-parser") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1is36g9shml872s2k1qg0hr5d2hyi9s723jna2gysmy340vx45qw")))

(define-public crate-armv6-m-instruction-parser-0.2.0 (c (n "armv6-m-instruction-parser") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0k0a8yl9jsgpqbqscjc7xrzs6w9n05v4b39my9kqhf5aj50zsi6g")))

(define-public crate-armv6-m-instruction-parser-0.3.0-rc1 (c (n "armv6-m-instruction-parser") (v "0.3.0-rc1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "04l5bghfm5hb1mrqpplh5fs8r3kmllwkbqx1k6hxcg7sgq5x3jrr")))

