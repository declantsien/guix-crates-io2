(define-module (crates-io ar ya arya) #:use-module (crates-io))

(define-public crate-arya-0.0.1 (c (n "arya") (v "0.0.1") (h "0zbdlsjz10jhb33wbpk2pkb83km9169mgizdvmacqdd33jasx6qx")))

(define-public crate-arya-0.0.2 (c (n "arya") (v "0.0.2") (h "0s5bqiwh8swgywzz0nmhc5rnpddhfdcwbqf7rhdrzr0lyzlidpws")))

(define-public crate-arya-0.0.3 (c (n "arya") (v "0.0.3") (h "1s6ris72h78ay218lminl8imljbf69bddimz56g32pgzn13w88ws")))

