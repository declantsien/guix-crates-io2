(define-module (crates-io ar p- arp-spoofer) #:use-module (crates-io))

(define-public crate-arp-spoofer-1.0.0 (c (n "arp-spoofer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atomic_enum") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_datalink") (r "^0.31.0") (d #t) (k 0)))) (h "1i5rccm2v82681z4x6ys7mwp5js6ads0iv521yc4hdpq9cb64b4l")))

