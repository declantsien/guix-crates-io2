(define-module (crates-io ar p- arp-whisper) #:use-module (crates-io))

(define-public crate-arp-whisper-0.1.0 (c (n "arp-whisper") (v "0.1.0") (d (list (d (n "pnet") (r "^0.33.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)))) (h "0fwb3wyz4xmj6gy4lcfg98v9pw8h8gn2m553xd575zff0qv79yf3")))

(define-public crate-arp-whisper-0.1.1 (c (n "arp-whisper") (v "0.1.1") (d (list (d (n "pnet") (r "^0.33.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)))) (h "0kicnpnzb4fcfawd51m5gxhs3q367ybl5iysz6giv7b8id92x1nx")))

(define-public crate-arp-whisper-0.1.2 (c (n "arp-whisper") (v "0.1.2") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (f (quote ("timestamps"))) (k 0)))) (h "1r8sril2556f6n8f5cdnj7l885p84ak6f9ybdd3abx73hbnj8k9h")))

