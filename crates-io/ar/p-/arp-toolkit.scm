(define-module (crates-io ar p- arp-toolkit) #:use-module (crates-io))

(define-public crate-arp-toolkit-0.1.0 (c (n "arp-toolkit") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)))) (h "1fv4winq8nv1szlazpj7smz4b82mj2ggi78084rghkhz73v3h6iq")))

(define-public crate-arp-toolkit-0.1.1 (c (n "arp-toolkit") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)))) (h "1hia7q550da0zygmmh1l2q9qy16bfz9j9y6b5ysnh8r8lacvjjak")))

(define-public crate-arp-toolkit-0.2.0 (c (n "arp-toolkit") (v "0.2.0") (d (list (d (n "maybe-async") (r "^0.2.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.28") (d #t) (k 0)))) (h "0s2gfxsibpf3ixf6wb923352ng843vkik5q4qbh346dlqknjk9ya") (f (quote (("sync" "maybe-async/is_sync") ("default"))))))

(define-public crate-arp-toolkit-0.3.0 (c (n "arp-toolkit") (v "0.3.0") (d (list (d (n "maybe-async") (r "^0.2.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.31") (f (quote ("std"))) (d #t) (k 0)))) (h "0fvqj1hrhwdnbypq9zn1haglgzllj4rz32vijkfil0173z30y010") (f (quote (("sync" "maybe-async/is_sync") ("default"))))))

(define-public crate-arp-toolkit-0.3.1 (c (n "arp-toolkit") (v "0.3.1") (d (list (d (n "maybe-async") (r "^0.2.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.33") (f (quote ("std"))) (d #t) (k 0)))) (h "0kiqd60aij785zfl3ygm1bry08s38j50kgdx5jfbb8hf3ykl5c8b") (f (quote (("sync" "maybe-async/is_sync") ("default"))))))

(define-public crate-arp-toolkit-0.3.2 (c (n "arp-toolkit") (v "0.3.2") (d (list (d (n "maybe-async") (r "^0.2.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (f (quote ("std"))) (d #t) (k 0)))) (h "1j08dcwrhzdf5f4sls0rgk631b5r9hk21ydhygkbkn3apy1xil4k") (f (quote (("sync" "maybe-async/is_sync") ("default"))))))

