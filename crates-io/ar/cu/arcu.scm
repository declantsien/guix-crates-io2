(define-module (crates-io ar cu arcu) #:use-module (crates-io))

(define-public crate-arcu-0.1.0 (c (n "arcu") (v "0.1.0") (h "09k4dw70s1fa2idd2309w64cyczgc821ljsk7g8mnzs4ck7yvj7w") (f (quote (("thread_local_counter" "std" "global_counters") ("std") ("global_counters" "std")))) (y #t) (r "1.65.0")))

(define-public crate-arcu-0.1.1 (c (n "arcu") (v "0.1.1") (h "17w5j6ddkvslwikfxi5f6j8p5jzdhvn7ki8iixf60dn4nh7pqwpq") (f (quote (("thread_local_counter" "std" "global_counters") ("std") ("global_counters" "std")))) (r "1.65.0")))

