(define-module (crates-io ar cu arcus) #:use-module (crates-io))

(define-public crate-arcus-0.1.0 (c (n "arcus") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0rndd484g38amqf06ampwvl5v867gjsgs4xz39mf81vbxzzn3cp6")))

