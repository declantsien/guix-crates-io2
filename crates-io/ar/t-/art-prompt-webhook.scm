(define-module (crates-io ar t- art-prompt-webhook) #:use-module (crates-io))

(define-public crate-art-prompt-webhook-1.0.0 (c (n "art-prompt-webhook") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0z1m2kgirg6cnwwxhyz3hahk669xgs56bw2hd56sk8qq69dcd11m")))

(define-public crate-art-prompt-webhook-1.1.0 (c (n "art-prompt-webhook") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0vv3ngrj3r97dnng7j3k77n1lldl48mxwjcz51w0jvi8aikyallm")))

