(define-module (crates-io ar t- art-mk) #:use-module (crates-io))

(define-public crate-art-mk-0.1.0 (c (n "art-mk") (v "0.1.0") (h "15vdb6jjmr9lfspshbbrjkqv9kc7ap1fcgrbmcinpfbaj0s5palw")))

(define-public crate-art-mk-0.1.1 (c (n "art-mk") (v "0.1.1") (h "10y9ky8kndhgg2sslxblxzc0frfidz382sy47iddh0v3i0iyrn4p")))

