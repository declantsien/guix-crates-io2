(define-module (crates-io ar t- art-dl) #:use-module (crates-io))

(define-public crate-art-dl-0.0.1 (c (n "art-dl") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0x0ijjj2vnbf2ny8997j9jh54ji72i29gc99mzi28y3jdal2zs9f")))

