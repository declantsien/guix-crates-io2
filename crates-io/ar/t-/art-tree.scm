(define-module (crates-io ar t- art-tree) #:use-module (crates-io))

(define-public crate-art-tree-0.1.0 (c (n "art-tree") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0xjvjphvqwpdarwgvndff5yr9afcbjzzkawgc9z93d8pmcnadpnj")))

(define-public crate-art-tree-0.2.0 (c (n "art-tree") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1wpiwdmg5hdcnn3ld8mlhw67a7cqvn65zpbxzrv5x5rjhnv2d15s")))

