(define-module (crates-io ar io arion) #:use-module (crates-io))

(define-public crate-arion-0.1.0 (c (n "arion") (v "0.1.0") (h "0gy9g5rj32ywmgy57hjc89ppzqlmb4qs6ypqq70cq7n0pg9ykvmv")))

(define-public crate-arion-0.2.0 (c (n "arion") (v "0.2.0") (h "148rbwla9j1nc0skj9bydl2a45kdjzka6g8v89pi2a8fn8c3b4pq")))

(define-public crate-arion-0.3.0 (c (n "arion") (v "0.3.0") (h "0bv1ifp5h6ck2zkrijn9rpcnv0b40wd5bsvsq1hhsla8x0m3y5qj")))

(define-public crate-arion-0.4.0 (c (n "arion") (v "0.4.0") (h "1vwx2gc8nbzgh9qldd5r8sph55fkxxvqsvxfvmwxafkf702kfx7r")))

(define-public crate-arion-0.5.0 (c (n "arion") (v "0.5.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1kc88lygf7s3ckkiw633nixha1yvk40n1k8n8n0857lmc7fph8lq")))

