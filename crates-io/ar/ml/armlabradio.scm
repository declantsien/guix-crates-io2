(define-module (crates-io ar ml armlabradio) #:use-module (crates-io))

(define-public crate-ArmlabRadio-0.4.0 (c (n "ArmlabRadio") (v "0.4.0") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "radio") (r "^0.4.0") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "02p6sv3np5xh29xcc5jcasvq7mrfbxy0q9cf94maf9fp5slh27yp")))

(define-public crate-ArmlabRadio-0.4.1 (c (n "ArmlabRadio") (v "0.4.1") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "radio") (r "^0.4.0") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1iymlwpnk0z8w3b14g4qqzw5ir1bq7jd49xrlf5mnx2lv6h7xxb6")))

(define-public crate-ArmlabRadio-0.4.2 (c (n "ArmlabRadio") (v "0.4.2") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1nszgi350mg94mkbgfjivqp0i7z06mgnb6csp8avpz8yv2y33hxb")))

(define-public crate-ArmlabRadio-0.4.3 (c (n "ArmlabRadio") (v "0.4.3") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "08if597chyn30djvdk3h6nh2ynr676l5rzk4ixzx3xs1j23fkyw6")))

(define-public crate-ArmlabRadio-0.4.4 (c (n "ArmlabRadio") (v "0.4.4") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0c8qp4m7fd6fs2ll1wzg3kvx0gi366fv4pm96nal4p67qhm689yn")))

(define-public crate-ArmlabRadio-0.4.5 (c (n "ArmlabRadio") (v "0.4.5") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0f6ixxz9wy7qbv5q56lz7j2ckq9drp25g365cqn1yn6g6byqh52c") (f (quote (("i2c"))))))

(define-public crate-ArmlabRadio-0.4.6 (c (n "ArmlabRadio") (v "0.4.6") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "035gm0kmd1jwzbsfh1ylg57msw5n2bks0p7kllb1f6afijlhfk5q") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4.7 (c (n "ArmlabRadio") (v "0.4.7") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0d5bcy2bnffg1305bya859jd3nb7dc30rahvcjnv0m4pdxwswpsv") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4.8 (c (n "ArmlabRadio") (v "0.4.8") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1h6w6gfm6bb3m65q04c5ci861q9bii6yfp4cpw8kwzqpbvq30x2z") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4.9 (c (n "ArmlabRadio") (v "0.4.9") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0lzdj3a4mki92vc8bggcck44snj5l6r76b6brpxsmwrxi6jhj9hs") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4.10 (c (n "ArmlabRadio") (v "0.4.10") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1x8jyxbiw9s4f0hrkxlbis1fhrdbiky2qg8ib1zz852ynidkr74w") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5.0 (c (n "ArmlabRadio") (v "0.5.0") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "138l5zhijm5n74rzgqgnjdpqfrg8yihv9sfmsmzpww5xpyxipl1m") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5.1 (c (n "ArmlabRadio") (v "0.5.1") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1b7ffyfb2ygyba6ska08j8srdair7khhslsmh043rdsp4jqwsfss") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5.2 (c (n "ArmlabRadio") (v "0.5.2") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0bmrjn0pkl883h4c0sirp56mv3w40149iz5vc0bjvr0bb17jcdrk") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.0 (c (n "ArmlabRadio") (v "0.6.0") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0jvr2mbg1i2444yrwh1d0sccl053bfkrlg9l647i1p4avpys6276") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.1 (c (n "ArmlabRadio") (v "0.6.1") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "0s71i4khzzw9x1rmkch32z5kqj4kqrz34907mdnizximx93h3l57") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.2 (c (n "ArmlabRadio") (v "0.6.2") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "150flh5bnw095xkn2lmgmh4zslb99lmvxrjljadzvjya4lxj2yk7") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.3 (c (n "ArmlabRadio") (v "0.6.3") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "1xig79ffxkijyza2x9sibiqmpxd0a8szr3lplh58qidhr9ydknpr") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.4 (c (n "ArmlabRadio") (v "0.6.4") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "08xm1r1j4l5nfandlq2h86f275zai5a31nyp6zni3qbn9whwmz19") (f (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6.5 (c (n "ArmlabRadio") (v "0.6.5") (d (list (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "17b2gsva0drfax2dcya1fa77k3g5zn32qvkgadb2k4gj9n6njv8b") (f (quote (("i2clib"))))))

