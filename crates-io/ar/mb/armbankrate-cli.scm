(define-module (crates-io ar mb armbankrate-cli) #:use-module (crates-io))

(define-public crate-armbankrate-cli-0.1.0 (c (n "armbankrate-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "armbankrate-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "09awdxp2f3hp79amrragnl8k9rrl4y7aaxk3ql1bk78b80hzxr47")))

