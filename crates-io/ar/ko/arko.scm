(define-module (crates-io ar ko arko) #:use-module (crates-io))

(define-public crate-arko-0.1.0 (c (n "arko") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.115") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1j1g5j116c9l6crxfj2l64n7gsbqdhnllz8ww0hv9c1yr120wp3j")))

(define-public crate-arko-0.1.1 (c (n "arko") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.115") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1rmam44hwdc31wsx543r9k1mhpb5aas82inzdwsjwprr0p3j3i19")))

(define-public crate-arko-0.2.0 (c (n "arko") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.115") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0q8vizjdn2pd246r2zhcl1vb1f53fc3crfjbynp1vkfhrh9z0hx8")))

(define-public crate-arko-0.2.1 (c (n "arko") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.115") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "16d3xsk6lif97yxaj2kvmirx7qj2ssgashdfm4vlqfbgq7ddqkls")))

(define-public crate-arko-0.2.2 (c (n "arko") (v "0.2.2") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0438r0qw1mvxydramsv5d6rf02vp3ylyrp9515fhwsf1y3x82ih7")))

(define-public crate-arko-0.2.3 (c (n "arko") (v "0.2.3") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xvya0qgpnj1672gigr3y6x7974121a1319j53ygj5g2d7nmb10v")))

