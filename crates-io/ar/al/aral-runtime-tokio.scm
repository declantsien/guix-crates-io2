(define-module (crates-io ar al aral-runtime-tokio) #:use-module (crates-io))

(define-public crate-aral-runtime-tokio-0.1.0-alpha.4 (c (n "aral-runtime-tokio") (v "0.1.0-alpha.4") (d (list (d (n "tokio") (r "^1.33.0") (f (quote ("fs" "time" "rt" "io-util" "net"))) (d #t) (k 0)))) (h "0ilh7xz90q2907w75185f8lj97pynmw60fvicg4ipabslp7p0agj")))

(define-public crate-aral-runtime-tokio-0.1.0-alpha.5 (c (n "aral-runtime-tokio") (v "0.1.0-alpha.5") (d (list (d (n "tokio") (r "^1.33.0") (f (quote ("fs" "time" "rt" "io-util" "net"))) (d #t) (k 0)))) (h "1s7gsvha6fpd8z5i2vr4iqpmrn2kbakrv1qf0wbrlqxmd8fby0f4")))

(define-public crate-aral-runtime-tokio-0.1.0 (c (n "aral-runtime-tokio") (v "0.1.0") (d (list (d (n "tokio") (r "^1.33.0") (f (quote ("fs" "time" "rt" "io-util" "net"))) (d #t) (k 0)))) (h "16lflrdysws30zxpy3jw8avxd7d136dl68ag6srpi0xsci63glf1") (r "1.75")))

