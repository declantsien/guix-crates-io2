(define-module (crates-io ar al aral-runtime-noop) #:use-module (crates-io))

(define-public crate-aral-runtime-noop-0.1.0-alpha.4 (c (n "aral-runtime-noop") (v "0.1.0-alpha.4") (h "07l8sjdar9axgav06kjrpjz8s2cd9azsc9x3lbz756h0ygsg0247")))

(define-public crate-aral-runtime-noop-0.1.0-alpha.5 (c (n "aral-runtime-noop") (v "0.1.0-alpha.5") (h "173r7jsv44z08d416hv61yp6zhfxn175sa7nwqnnhrmwbcvnsalr")))

(define-public crate-aral-runtime-noop-0.1.0 (c (n "aral-runtime-noop") (v "0.1.0") (h "0h5342fk21n6h3kzbifscyyyjs6jx8fv5pw8iis8px5plpq4fg54") (r "1.75")))

