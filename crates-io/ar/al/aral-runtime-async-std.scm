(define-module (crates-io ar al aral-runtime-async-std) #:use-module (crates-io))

(define-public crate-aral-runtime-async-std-0.1.0-alpha.4 (c (n "aral-runtime-async-std") (v "0.1.0-alpha.4") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)))) (h "19fcfbw77qhlgb9xjhl4wn698mwvzfyrrjf8m72fmwdh2nz4wg2i")))

(define-public crate-aral-runtime-async-std-0.1.0-alpha.5 (c (n "aral-runtime-async-std") (v "0.1.0-alpha.5") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)))) (h "1fsy3sbmjhz25nd651amfmk825ramw7jk0cz28kc98zvg8v92sj2")))

(define-public crate-aral-runtime-async-std-0.1.0 (c (n "aral-runtime-async-std") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)))) (h "12krxsnb6rdbizsiyjsq7rnzm0mwkxc2ysnywfjbm9rh5gp2jlj6") (r "1.75")))

