(define-module (crates-io ar al aral) #:use-module (crates-io))

(define-public crate-aral-0.0.0 (c (n "aral") (v "0.0.0") (h "19x12dgypa1rd8j69fbf2q4b2vn7i43sbh29kzfgjpsq2whwhza5")))

(define-public crate-aral-0.1.0-alpha.1 (c (n "aral") (v "0.1.0-alpha.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("fs" "io-util" "net" "time" "rt"))) (o #t) (d #t) (k 0)))) (h "0qhc9llg0fiaaq4fayrb4zgy74w303wd1055n2p3cb8cvn83vzra") (f (quote (("runtime-tokio-multi-thread" "tokio" "tokio/rt-multi-thread") ("runtime-tokio-current-thread" "tokio") ("runtime-async-std" "async-std"))))))

(define-public crate-aral-0.1.0-alpha.2 (c (n "aral") (v "0.1.0-alpha.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("fs" "io-util" "net" "time" "rt"))) (o #t) (d #t) (k 0)))) (h "0ingv6zy8njmw3mmg0rqhnnkaj3466jcx3v0g8dzdl5b9r2j4dm1") (f (quote (("runtime-tokio-multi-thread" "tokio" "tokio/rt-multi-thread") ("runtime-tokio-current-thread" "tokio") ("runtime-async-std" "async-std"))))))

(define-public crate-aral-0.1.0-alpha.5 (c (n "aral") (v "0.1.0-alpha.5") (d (list (d (n "aral-runtime-async-std") (r "^0.1.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "aral-runtime-noop") (r "^0.1.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "aral-runtime-tokio") (r "^0.1.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1zy17kjbxjsp9ci2b1sn7yhj017mgfrphdnjdifcripnqq5069ga") (f (quote (("runtime-tokio" "aral-runtime-tokio") ("runtime-noop" "aral-runtime-noop") ("runtime-async-std" "aral-runtime-async-std") ("default" "runtime-noop"))))))

(define-public crate-aral-0.1.0 (c (n "aral") (v "0.1.0") (d (list (d (n "aral-runtime-async-std") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "aral-runtime-noop") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "aral-runtime-tokio") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0j3zfd5sma682r21p5bnfsnif3lsdpyh062mwqs2d6v6dd6qnmh8") (f (quote (("runtime-tokio" "aral-runtime-tokio") ("runtime-noop" "aral-runtime-noop") ("runtime-async-std" "aral-runtime-async-std") ("default" "runtime-noop")))) (r "1.75")))

