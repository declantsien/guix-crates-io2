(define-module (crates-io ar as arashi) #:use-module (crates-io))

(define-public crate-arashi-0.1.0 (c (n "arashi") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "indexmap") (r "~1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "~0.21") (d #t) (k 0)) (d (n "strum_macros") (r "~0.21.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)) (d (n "ureq") (r "~2.1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1pwcd81f8lzbnh5crac5gdpy4ian0bhqfpy1lb7f0363nmbxv4mm")))

