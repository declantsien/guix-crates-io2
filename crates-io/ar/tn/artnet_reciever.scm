(define-module (crates-io ar tn artnet_reciever) #:use-module (crates-io))

(define-public crate-artnet_reciever-0.1.0 (c (n "artnet_reciever") (v "0.1.0") (d (list (d (n "artnet_protocol") (r "^0.4.1") (d #t) (k 0)) (d (n "socket2") (r "^0.5.2") (f (quote ("all"))) (d #t) (k 0)))) (h "00mccr1xa6zrzz1bgrz37gvjl8sccv7hjrvj9s5zagyjqgymxw0n")))

(define-public crate-artnet_reciever-0.2.0 (c (n "artnet_reciever") (v "0.2.0") (d (list (d (n "artnet_protocol") (r "^0.4.1") (d #t) (k 0)) (d (n "socket2") (r "^0.5.2") (f (quote ("all"))) (d #t) (k 0)))) (h "1qk889w7i7j5kia35fpkz46xmfssb46mj04av47rvnxbaafy9245")))

(define-public crate-artnet_reciever-0.3.0 (c (n "artnet_reciever") (v "0.3.0") (d (list (d (n "artnet_protocol") (r "^0.4.1") (d #t) (k 0)) (d (n "socket2") (r "^0.5.2") (f (quote ("all"))) (d #t) (k 0)))) (h "0f361ldznk01jc86ywv7hcdr32x2fqlhp5bsy73hjmf81rk561fp")))

