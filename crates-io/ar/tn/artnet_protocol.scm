(define-module (crates-io ar tn artnet_protocol) #:use-module (crates-io))

(define-public crate-artnet_protocol-0.1.0 (c (n "artnet_protocol") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1iyi8gmw4qm2ys3cm5yjazdhqf6f1jqsjkcq1qv9qg9vvix0crkk")))

(define-public crate-artnet_protocol-0.1.1 (c (n "artnet_protocol") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "15kd91nbzp8v3brvvhxc1hi231pxhdvz958pb9vr9a4j2pmjr1lx")))

(define-public crate-artnet_protocol-0.2.0 (c (n "artnet_protocol") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "1skmbpyyz7c9w4n753a32cg8rxflj8z4dflx8962xrxnsg94p2i6")))

(define-public crate-artnet_protocol-0.3.0 (c (n "artnet_protocol") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "05vdwd4d9m5zh2863bysnfriryd49f363qmi9gan9prc5dl1sf76")))

(define-public crate-artnet_protocol-0.4.0 (c (n "artnet_protocol") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1zy9k6yd7c5bfgimgd6pn9m8h5hb664lpbqfpb7506cnbs5y8alm")))

(define-public crate-artnet_protocol-0.4.1 (c (n "artnet_protocol") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "14inicb5pymrbq2cb72vfkmb77v2mrqbq8mjm79w5gqvz9xyg9if")))

(define-public crate-artnet_protocol-0.4.2 (c (n "artnet_protocol") (v "0.4.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1gi075r0rpijx80xldkyamf31sxi77ndbc8xn5f9ksvwmsf2nssk")))

