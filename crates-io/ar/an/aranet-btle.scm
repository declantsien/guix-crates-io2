(define-module (crates-io ar an aranet-btle) #:use-module (crates-io))

(define-public crate-aranet-btle-0.1.0 (c (n "aranet-btle") (v "0.1.0") (d (list (d (n "btleplug") (r "^0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.5") (d #t) (k 0)))) (h "1i1jqcvwlm5ywcgl2khqjyak5y9n8fqay227nqwrqxg1sb4dpjf1")))

(define-public crate-aranet-btle-0.1.1 (c (n "aranet-btle") (v "0.1.1") (d (list (d (n "btleplug") (r "^0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.5") (d #t) (k 0)))) (h "0h6nqlhyrlps6bzsc7yfh94lb8ppyb1aggl7rlwm8v36sazglrxv")))

(define-public crate-aranet-btle-0.1.2 (c (n "aranet-btle") (v "0.1.2") (d (list (d (n "btleplug") (r "^0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)) (d (n "uuid") (r "^1.5") (d #t) (k 0)))) (h "0w0nqbxqy86swskpxx2imay4nc12xj2vwrli4v1ym27w2sf1630j")))

