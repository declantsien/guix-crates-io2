(define-module (crates-io ar an aranet4) #:use-module (crates-io))

(define-public crate-aranet4-0.1.0 (c (n "aranet4") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "btleplug") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "13adv3jkfg4ngsbszrynr0lzxm72b15084ayqv1jxpc4sj8ysjwp")))

