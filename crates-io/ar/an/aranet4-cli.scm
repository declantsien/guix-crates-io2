(define-module (crates-io ar an aranet4-cli) #:use-module (crates-io))

(define-public crate-aranet4-cli-0.1.0 (c (n "aranet4-cli") (v "0.1.0") (d (list (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "16h0rxb1s0gxx4h8dzjrhkvx9cg6grw9hzd8ll0lgn4z9rdshv4s")))

(define-public crate-aranet4-cli-0.2.0 (c (n "aranet4-cli") (v "0.2.0") (d (list (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "15lrn8icjya3wd41mwbskiws98322j5az7srpqcvx568lwwphm8z")))

