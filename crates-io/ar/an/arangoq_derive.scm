(define-module (crates-io ar an arangoq_derive) #:use-module (crates-io))

(define-public crate-arangoq_derive-0.1.2 (c (n "arangoq_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmd8f5a74mgdgjnij6zmd08y651nqsswjgkivkz3vz8mkz7ji4h")))

