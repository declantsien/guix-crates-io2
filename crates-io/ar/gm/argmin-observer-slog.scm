(define-module (crates-io ar gm argmin-observer-slog) #:use-module (crates-io))

(define-public crate-argmin-observer-slog-0.1.0 (c (n "argmin-observer-slog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argmin") (r "^0.10.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2.7") (f (quote ("dynamic-keys"))) (d #t) (k 0)) (d (n "slog-async") (r "^2.7") (d #t) (k 0)) (d (n "slog-json") (r "^2.6") (o #t) (d #t) (k 0)) (d (n "slog-term") (r "^2.9.1") (d #t) (k 0)))) (h "1aird1g5fd32qdw8hfa6nwanrra2ijpg4x8p2rfsbrk1mg49imw3") (f (quote (("serde1" "serde" "serde_json" "slog-json") ("default"))))))

