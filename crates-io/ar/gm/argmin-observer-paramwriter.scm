(define-module (crates-io ar gm argmin-observer-paramwriter) #:use-module (crates-io))

(define-public crate-argmin-observer-paramwriter-0.1.0 (c (n "argmin-observer-paramwriter") (v "0.1.0") (d (list (d (n "argmin") (r "^0.10.0") (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r36dgvznymghadm9zvlkpfal650ix7ycxr7vyh39axhzklizq03")))

