(define-module (crates-io ar gm argmm) #:use-module (crates-io))

(define-public crate-argmm-0.1.0 (c (n "argmm") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "13cgfpi4xxrm04nhazihvn7xii89q8is0bacr16ipxfw4liw8rhi")))

(define-public crate-argmm-0.1.1 (c (n "argmm") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "0rqpb59v1qywcji3akxz16qzx8wxmwqkz6bzn7mwk922n50m9yby")))

(define-public crate-argmm-0.1.2 (c (n "argmm") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "0k2632pa57vzylpxcwyxssxf7yq7hk0x6clm8skcis4qwaknfl7v")))

