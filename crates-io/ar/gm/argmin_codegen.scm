(define-module (crates-io ar gm argmin_codegen) #:use-module (crates-io))

(define-public crate-argmin_codegen-0.1.0 (c (n "argmin_codegen") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1dklrqngs4w5p77pjm6abkf6q908r1nd89z04m93lfawyflc5h")))

(define-public crate-argmin_codegen-0.1.1 (c (n "argmin_codegen") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "15w4h1512mgbhpzw12l8bas02iryfdr7x17wdxn1s8dvgp1jn53n")))

(define-public crate-argmin_codegen-0.1.2 (c (n "argmin_codegen") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikq9bqvr5rs6719pjfw3lwsabgw76r5lk8vjfspfd9wamlk9yal")))

(define-public crate-argmin_codegen-0.1.3 (c (n "argmin_codegen") (v "0.1.3") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)))) (h "0gi9gns0my7710da74lzms5h483ndz64bv2qq2q7pgdd3wldcqlz")))

(define-public crate-argmin_codegen-0.1.4 (c (n "argmin_codegen") (v "0.1.4") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1rlw4gzjv2rmdb8lpkhlwk3809f6n6qdcarw5d0dvr12lfv6q73g")))

(define-public crate-argmin_codegen-0.1.5 (c (n "argmin_codegen") (v "0.1.5") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0hsa9gapqzikxdvpmjn1nq2kb2334sbj8wi1k8d1v52x8cm6rpjj")))

(define-public crate-argmin_codegen-0.1.6 (c (n "argmin_codegen") (v "0.1.6") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1vl3yb5dsmzkyp2q3i5fckj0f6rd7kwhnxbwk69cl6z29ic01hyz")))

(define-public crate-argmin_codegen-0.1.7 (c (n "argmin_codegen") (v "0.1.7") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "14sb8mbpgm6xav6kfr32wkyndc7anyxy68vjy2gdm00xrbhiynpv")))

(define-public crate-argmin_codegen-0.1.8 (c (n "argmin_codegen") (v "0.1.8") (d (list (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0b500fk7c92d5qn8nyf3bz43qd0lgvfik8cmm9xb66sbgq7rfg48")))

