(define-module (crates-io ar gm argmin_testfunctions) #:use-module (crates-io))

(define-public crate-argmin_testfunctions-0.1.0 (c (n "argmin_testfunctions") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0s2i5d07vf43lszc4ymmd5gxsxlc3mjqins3jyr1n1f6jqmahpxh")))

(define-public crate-argmin_testfunctions-0.1.1 (c (n "argmin_testfunctions") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0jf2is37rbvzbnhpxrl7qlfq0bylqj4v556vyw4klbbg7hc3d383")))

(define-public crate-argmin_testfunctions-0.2.0 (c (n "argmin_testfunctions") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "finitediff") (r "^0.1.4") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)))) (h "007s7qdmr2s4lpq43ippsmvjzfdgfz90yqqsx6ckb95c7n1i4sc6")))

