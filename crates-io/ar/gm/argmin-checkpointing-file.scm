(define-module (crates-io ar gm argmin-checkpointing-file) #:use-module (crates-io))

(define-public crate-argmin-checkpointing-file-0.1.0 (c (n "argmin-checkpointing-file") (v "0.1.0") (d (list (d (n "argmin") (r "^0.10.0") (k 0)) (d (n "argmin") (r "^0.10.0") (f (quote ("serde1"))) (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)))) (h "1jlzskbbgiqniziqa83r081hxg0z6js87a1apssl99hqcwd6iz9j")))

