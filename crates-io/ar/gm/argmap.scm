(define-module (crates-io ar gm argmap) #:use-module (crates-io))

(define-public crate-argmap-1.0.0 (c (n "argmap") (v "1.0.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "14m7aqdy34jp850bbdsc6fhcvsl923zax9r7jmh8166hxadp19yf")))

(define-public crate-argmap-1.1.0 (c (n "argmap") (v "1.1.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "0xxvbfpsjyz5xxir3qfjhdbkrjzi4kn1wnzhdllrylfr183kz2x8")))

(define-public crate-argmap-1.1.1 (c (n "argmap") (v "1.1.1") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1bkhixmpxvxmayb4hmqicb8202lyh1hmb951wgs0zgqmbwh00g2x")))

(define-public crate-argmap-1.1.2 (c (n "argmap") (v "1.1.2") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)))) (h "1sclg6cic344144fl44idmhzibaxbd1676cjx031wgsga9bg81y0")))

