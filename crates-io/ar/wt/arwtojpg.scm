(define-module (crates-io ar wt arwtojpg) #:use-module (crates-io))

(define-public crate-arwtojpg-0.2.0 (c (n "arwtojpg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("feature" "fs" "mman"))) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.15") (f (quote ("fs"))) (d #t) (k 0)))) (h "1r9cxfx5bl0q5ddiri13a43khx57s960v1pgngm56qwzw313sa5h")))

(define-public crate-arwtojpg-0.2.1 (c (n "arwtojpg") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("feature" "fs" "mman"))) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.15") (f (quote ("fs"))) (d #t) (k 0)))) (h "025w90lyk7dmgyascqrvkdmbf3gaf9s5mg41x4j83q30ravivl25")))

