(define-module (crates-io ar ca arcanum) #:use-module (crates-io))

(define-public crate-arcanum-0.1.0 (c (n "arcanum") (v "0.1.0") (d (list (d (n "new_mime_guess") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0jzsyisrml6vwl6rka3p8snmj2ynf80f56iavf7mdd5a8fvv2cf5")))

(define-public crate-arcanum-0.1.1 (c (n "arcanum") (v "0.1.1") (d (list (d (n "new_mime_guess") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "1xfzr5dw3zniw4zvsv78syw3q061qz13rdy7w6jq491kll67iq35")))

(define-public crate-arcanum-0.1.2 (c (n "arcanum") (v "0.1.2") (d (list (d (n "new_mime_guess") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0gid68cadfk76n2b09kyjivkqhy0rvkdns5f0k5802awi5g5d1lg")))

