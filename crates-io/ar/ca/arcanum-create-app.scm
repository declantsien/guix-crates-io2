(define-module (crates-io ar ca arcanum-create-app) #:use-module (crates-io))

(define-public crate-arcanum-create-app-0.1.0 (c (n "arcanum-create-app") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "1xbcm1a325hqmzffy65ma7gfn5mdy262js24zgzzrbj3swzx78h9")))

