(define-module (crates-io ar ca arcane) #:use-module (crates-io))

(define-public crate-arcane-0.20220721.0-dev (c (n "arcane") (v "0.20220721.0-dev") (h "0gbqnqc7m1ycy7paidwlsnwfxx2zz5c85sg9nfl8irwrbr9m5pa6") (y #t)))

(define-public crate-arcane-0.0.0-- (c (n "arcane") (v "0.0.0--") (h "0m007lqpyhfis2fzyxcrycmm6spzaybmlly356a5a7mn4mww0xbm") (y #t)))

