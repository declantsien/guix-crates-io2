(define-module (crates-io ar ca arca) #:use-module (crates-io))

(define-public crate-arca-0.1.0 (c (n "arca") (v "0.1.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "044kj4cbw3k0qkypaf8l8h0asghwzkk8wghlqzpbyygp5lnchs0h")))

(define-public crate-arca-0.1.1 (c (n "arca") (v "0.1.1") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "1r6wq5vqzp27288dzadk9rmxmkg6xgzv692g31137ybvz9rsfknq")))

(define-public crate-arca-0.1.2 (c (n "arca") (v "0.1.2") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "07y0wk49g28hdbvhcd3nv6imw8fz7z94kwyxa6ygv3kmnaay111x")))

(define-public crate-arca-0.1.3 (c (n "arca") (v "0.1.3") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0rc444qp6f8949i30xf5d6fyrkd3ypymxy37nshhss9nfdx8rqbz")))

(define-public crate-arca-0.1.4 (c (n "arca") (v "0.1.4") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "18mflalgzqfnzddqf6sq9fxbzj32gq5nx7qim0q18f06p534h0js")))

(define-public crate-arca-0.2.0 (c (n "arca") (v "0.2.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0dx8i78k10y8bamf1fqwg5i40ljwzh41nwbx7s2jalqiy5lpdsp7")))

(define-public crate-arca-0.3.0 (c (n "arca") (v "0.3.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0s8cgdga3522llxzkfm5vdm7npwvmyjmz5ig4fv17fcl2qip1ci3") (s 2) (e (quote (("napi" "dep:napi"))))))

(define-public crate-arca-0.3.1 (c (n "arca") (v "0.3.1") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "10vvk4vld762c8qs93g3mamdjkr3pywh58nwg1qmlhhfjbqiiv57") (s 2) (e (quote (("napi" "dep:napi"))))))

(define-public crate-arca-0.3.2 (c (n "arca") (v "0.3.2") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "1naamf2dfgfhd5myqjakbvhvf73lgwj2rjq4iq7q629aczg9c4f0") (s 2) (e (quote (("napi" "dep:napi"))))))

(define-public crate-arca-0.3.3 (c (n "arca") (v "0.3.3") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0728n7cipmczymxmwic42lsg7r3fypkwjpr7dv82cp3zqy9nq1fd") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.3.4 (c (n "arca") (v "0.3.4") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "14icl76cz3vqfdqiv69zbl0ywxi2p54sln5n0s8cy7x5j64clkd2") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.3.5 (c (n "arca") (v "0.3.5") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0j11sbg0zxfaad4mjww65jynh7fh3afilcihi567w2z8gas7jh33") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.3.6 (c (n "arca") (v "0.3.6") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0943pcy416dbnanrajfxpgc8kxhdh4i90gqfacaildmw3apjivyf") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.3.7 (c (n "arca") (v "0.3.7") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "17rqf95x43dgywgkmpvdr0x46ckrp9z2zyj3m7r2xzv71yhrzlsi") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.4.0 (c (n "arca") (v "0.4.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "12js0yimah0xgvhpql41gr5b4qmwwdij36lkaw4ls7pjv0qw72zg") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

(define-public crate-arca-0.5.0 (c (n "arca") (v "0.5.0") (d (list (d (n "clean-path") (r "^0.2.1") (d #t) (k 0)) (d (n "napi") (r "^2.13.1") (o #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0pmazzanbs8c8n9y7bs0880lh6nysy8q59skllvi5vj8s94z2sxh") (s 2) (e (quote (("serde" "dep:serde_derive" "dep:serde") ("napi" "dep:napi"))))))

