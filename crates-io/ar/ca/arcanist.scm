(define-module (crates-io ar ca arcanist) #:use-module (crates-io))

(define-public crate-arcanist-0.0.1 (c (n "arcanist") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12jg0s0gczrg3lnxhfbs07iz5i3xijnvn4j87g04w2bydwx8lgyf")))

(define-public crate-arcanist-0.0.2 (c (n "arcanist") (v "0.0.2") (h "0nrc7yag57lwp0caf1skhfxygh006hrnnad7pn514v8f09w4g43m")))

