(define-module (crates-io ar da ardaku) #:use-module (crates-io))

(define-public crate-ardaku-0.0.1 (c (n "ardaku") (v "0.0.1") (d (list (d (n "devout") (r "^0.2") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (f (quote ("universal" "cranelift"))) (k 0)))) (h "0zrpm1nvn3s2qr49svr70k13qgn6nw6rpxwdqpmngzgi5i6cwn4s")))

(define-public crate-ardaku-0.1.0 (c (n "ardaku") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmi") (r "^0.19") (k 0)))) (h "0j5rqij2i0wv66vzbcvk0ldlh0zvk8zvmf5nzfilmgfjzbr7g8j7")))

