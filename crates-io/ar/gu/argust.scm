(define-module (crates-io ar gu argust) #:use-module (crates-io))

(define-public crate-argust-0.1.0 (c (n "argust") (v "0.1.0") (h "1k4rriylhm46lj2j0shkdprisgmipfyf3an8dc8m1fridqhyravg") (y #t)))

(define-public crate-argust-0.0.1 (c (n "argust") (v "0.0.1") (h "07knk7cwpdah010gjsf8vdgm00y7ks7njwf9wqv0xsqzm268fa3z")))

(define-public crate-argust-0.0.2 (c (n "argust") (v "0.0.2") (h "057xidav63hn25dhbqqh9yna01kdksbf44ihzb4m28ir0l87xyhr")))

(define-public crate-argust-0.0.3 (c (n "argust") (v "0.0.3") (h "04iglb0l94ssc4sx5hm9c74044znpawa8aiqswwlmizdscxapkv4")))

