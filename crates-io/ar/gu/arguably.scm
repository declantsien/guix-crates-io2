(define-module (crates-io ar gu arguably) #:use-module (crates-io))

(define-public crate-arguably-0.1.0 (c (n "arguably") (v "0.1.0") (h "0x7bfynbgn0lvkmh2g3kx7wvszjpxcan37yjkssfx1s4zcz21pwp")))

(define-public crate-arguably-0.2.0 (c (n "arguably") (v "0.2.0") (h "1z0xba466bc25pp58jgdk3l9g9s7aq2cmwxdsmanw68cpmpxm9lg")))

(define-public crate-arguably-0.3.0 (c (n "arguably") (v "0.3.0") (h "04r93mfshc6i8fjxx3zyhmhdrx6inzsh6vzshbwizbwq1ych991y")))

(define-public crate-arguably-0.4.0 (c (n "arguably") (v "0.4.0") (h "0wz237i3a3rzlxx0d2hzw93mb2ific6hrw0bcfbmd1a0rkfsfbs4")))

(define-public crate-arguably-0.4.1 (c (n "arguably") (v "0.4.1") (h "1hzq52jayjmgibimzsjcqamrs8x5qf5zz3k47jn9vnfa2h61h2mz")))

(define-public crate-arguably-0.4.2 (c (n "arguably") (v "0.4.2") (h "0sgldr28khp7aq0ja5sigmc6b191ab34khdv5q9jppmimn0nkb6r")))

(define-public crate-arguably-0.4.3 (c (n "arguably") (v "0.4.3") (h "1lhbhgikqx52d32wbrnd0p1hi0y2ygylq5wv26qjkq46g3gmn3fw")))

(define-public crate-arguably-0.5.0 (c (n "arguably") (v "0.5.0") (h "1b6hpjw0qjgsm5kr0gmwl928prmdp22w6wxd9r21qndq1f171zky")))

(define-public crate-arguably-0.5.1 (c (n "arguably") (v "0.5.1") (h "1mhjlqlhg0d85h7fvvmpdl3k4p3wri583jzng1lba3a9k8jyxaa5")))

(define-public crate-arguably-0.6.0 (c (n "arguably") (v "0.6.0") (h "0ngryxxirn08nnqdgvnlc6v9cc7zsvnf9jddi5rj6khw4fifqn3x")))

(define-public crate-arguably-0.7.0 (c (n "arguably") (v "0.7.0") (h "0381bc28dfqcgcwddik9ad7yip26k487wgbxw435f3asc78va645")))

(define-public crate-arguably-0.8.0 (c (n "arguably") (v "0.8.0") (h "0bjlsyc9rhbx2bg3sdsdpdp8d4llx787svbrslc7656vk10hg5bg")))

(define-public crate-arguably-0.9.0 (c (n "arguably") (v "0.9.0") (h "1mz608bdd1fbv7lpsxnrdmhxp9c7xh9cv3xm5b3bq4m34i9hzwav")))

(define-public crate-arguably-0.9.1 (c (n "arguably") (v "0.9.1") (h "037wm4mwg9lfyzn94wgj7jskyr97x06i58axpm1g2jrpfr2h825p")))

(define-public crate-arguably-1.0.0 (c (n "arguably") (v "1.0.0") (h "0sd3p0ihj60s91rg44np93z9glr27nnqb51nv5xxix0qxzzan67k")))

(define-public crate-arguably-2.0.0-alpha.1 (c (n "arguably") (v "2.0.0-alpha.1") (h "0vas15aam97ch33n10c9ninr83m9jvghlffsmakmjg20wdav395h")))

(define-public crate-arguably-2.0.0-alpha.2 (c (n "arguably") (v "2.0.0-alpha.2") (h "1va26zyqgh1hawn3xralkmq7wnzswfz2zkyxwkf0jsln7b8vsh5y")))

(define-public crate-arguably-2.0.0-alpha.3 (c (n "arguably") (v "2.0.0-alpha.3") (h "0vxih9yprhqdb944f9fx3780nzg3904ibg9lbxrf223y8ldgwg61")))

(define-public crate-arguably-2.0.0 (c (n "arguably") (v "2.0.0") (h "1hvzi3nj0jyhj2xzq2y49j0ra2c7869vdq1awqs4pg76073rnndy")))

(define-public crate-arguably-2.0.1 (c (n "arguably") (v "2.0.1") (h "05h2dxrcgrhb824fb8pq823wf81g4adwhkklp374m597n1n8d93b")))

(define-public crate-arguably-2.1.0 (c (n "arguably") (v "2.1.0") (h "1zrjbbv6nj1fnxp7bbp0pz8xs8r53qj36c22b4qxpvncsnid968i")))

(define-public crate-arguably-2.1.1 (c (n "arguably") (v "2.1.1") (h "0s5q6w79p3blrh207v297bva44jrlwy55l9gbpi2vlciwrx11vxr")))

(define-public crate-arguably-2.2.0 (c (n "arguably") (v "2.2.0") (h "0ckfxqc30kfh1y9d04r3d21kvkpbra8xrmjrr0aglx7l2wynwhjp")))

