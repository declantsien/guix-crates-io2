(define-module (crates-io ar gu arguments) #:use-module (crates-io))

(define-public crate-arguments-0.0.1 (c (n "arguments") (v "0.0.1") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "074c0jzpvhdwy4i8ri9qlsk26d58gf6kcxadwdqyqp423zna3022")))

(define-public crate-arguments-0.1.0 (c (n "arguments") (v "0.1.0") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "0qyhfzirb2d90bmh4m15dwa06vyvrl3faf01pjwb1iwdwy5wl7zi")))

(define-public crate-arguments-0.1.1 (c (n "arguments") (v "0.1.1") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "1ag60mnxn9yvjjrill1n7gkcwlhbcg3lykxylrr22azdmbl562g3")))

(define-public crate-arguments-0.2.0 (c (n "arguments") (v "0.2.0") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "169im4yfwnwsnyz4iw2myj52whr7lbra1ickff0wp1xwdz7spy9a")))

(define-public crate-arguments-0.3.0 (c (n "arguments") (v "0.3.0") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "0k5pv2iks6b3ahanbgdcyx53cwch81bqqsn3l1ygvf1fdz3xbwq1")))

(define-public crate-arguments-0.4.0 (c (n "arguments") (v "0.4.0") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "0f32wn5100h4vayi3w4ki69jbga06s2hfnfn7v3ig31dm2xmlp1f")))

(define-public crate-arguments-0.5.0 (c (n "arguments") (v "0.5.0") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "1dw5ld6hwnvznfqyvxvlxydff18qlyfmikab5i4k8q2if3gvz5b7")))

(define-public crate-arguments-0.5.1 (c (n "arguments") (v "0.5.1") (d (list (d (n "options") (r "*") (d #t) (k 0)))) (h "1b724dbnkb44s2hcj218nwd6fq8hbbfrv6mcg02xbnml4vdx1cc1")))

(define-public crate-arguments-0.6.0 (c (n "arguments") (v "0.6.0") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "0snhqbmsb3aq4y4np4r6fl88pv20b2fkbv6306002gbwwl8vzi81")))

(define-public crate-arguments-0.6.1 (c (n "arguments") (v "0.6.1") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "1xg8by65505rhgn4l7p4rax1jc34jmvwnj65vw084jfapi5bmvmq")))

(define-public crate-arguments-0.6.2 (c (n "arguments") (v "0.6.2") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "1j54skslahabb534j6lann3774njsn0557vm14bbah6n9ylnni7g")))

(define-public crate-arguments-0.6.3 (c (n "arguments") (v "0.6.3") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "1pjwa44z869kafqqlykdi8b5fh463d8ljgkrrxzk3w944zligc8z")))

(define-public crate-arguments-0.7.0 (c (n "arguments") (v "0.7.0") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "0amf5llyqdrws6j4j0lz6i430n1m30lgyjlxsni320gp3gzrrs78") (y #t)))

(define-public crate-arguments-0.7.1 (c (n "arguments") (v "0.7.1") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "08ydkmmhbjqd3gf4di9p9fc4wak4gqwhdbncij2cld928f86b2wd")))

(define-public crate-arguments-0.7.2 (c (n "arguments") (v "0.7.2") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)))) (h "138iq4gy1nx06rag1z5q9x1h91zjxgmdmpkmcj41cblmw7drhf76")))

