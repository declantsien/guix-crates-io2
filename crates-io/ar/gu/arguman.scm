(define-module (crates-io ar gu arguman) #:use-module (crates-io))

(define-public crate-arguman-0.1.0 (c (n "arguman") (v "0.1.0") (h "1c1vy34s4f5vv2iw2ck0zkihq23y0rnfbwnjam021lmsdkl3zgl9")))

(define-public crate-arguman-0.1.1 (c (n "arguman") (v "0.1.1") (h "0a6rbhqws98aiydignram0zc5mscj7gaqcjk5sjhv0i8jk7sgzrz")))

