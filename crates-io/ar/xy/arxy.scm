(define-module (crates-io ar xy arxy) #:use-module (crates-io))

(define-public crate-arxy-0.0.1 (c (n "arxy") (v "0.0.1") (d (list (d (n "actix-rt") (r "^1.0.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "listenfd") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "11s6i7fqqjqn90i3szcgd1lny8x6vj22nypjkhpcjsf9znc38kz7")))

