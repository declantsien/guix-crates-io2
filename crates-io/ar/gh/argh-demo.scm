(define-module (crates-io ar gh argh-demo) #:use-module (crates-io))

(define-public crate-argh-demo-0.0.0 (c (n "argh-demo") (v "0.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)))) (h "1g4llbshr7n0zaffdwzjy2v31ys4s4nj865bybc79yj2kvqq4jw4")))

(define-public crate-argh-demo-0.0.1 (c (n "argh-demo") (v "0.0.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)))) (h "1wg8bdja3mq97cy7s8dkinaap1ahwmw02g50wbsn9xrfxbv8cy9i")))

(define-public crate-argh-demo-0.0.2 (c (n "argh-demo") (v "0.0.2") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)))) (h "1x51m8rknpyqza4mxa0vgccircqn5fkjsghvilr98gvkq17c0r15")))

