(define-module (crates-io ar gh argh) #:use-module (crates-io))

(define-public crate-argh-0.1.0 (c (n "argh") (v "0.1.0") (h "1am8inhq45dyw1lcylz6zifm3wp7dj703i72ngcdyl5wwn24yphl")))

(define-public crate-argh-0.1.1 (c (n "argh") (v "0.1.1") (d (list (d (n "argh_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.0") (d #t) (k 0)))) (h "1h34raci62111chgbgyg9qk2pdcws8m45bgs2av7l9xdvdafzw57")))

(define-public crate-argh-0.1.2 (c (n "argh") (v "0.1.2") (d (list (d (n "argh_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.0") (d #t) (k 0)))) (h "017l7zc1ggc9y7rrfd4f933jdq3c5xgx4ps7z11h4y1fldmmwphq")))

(define-public crate-argh-0.1.3 (c (n "argh") (v "0.1.3") (d (list (d (n "argh_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.1") (d #t) (k 0)))) (h "0m01hbnbxj2h7wb6flv3qmy4ky64c00nw1k9sh0dgb7c9ki7f66a")))

(define-public crate-argh-0.1.4 (c (n "argh") (v "0.1.4") (d (list (d (n "argh_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.4") (d #t) (k 0)))) (h "1gi2qp6mfc9gz2sy3w1fq8fpz3d1bvx1gmmiryicgkc7iw42yyci")))

(define-public crate-argh-0.1.5 (c (n "argh") (v "0.1.5") (d (list (d (n "argh_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0)))) (h "0gij9511j75wkvilhw2nq8d81an6c9pbnwlyilkwa5xw96jifwrf")))

(define-public crate-argh-0.1.6 (c (n "argh") (v "0.1.6") (d (list (d (n "argh_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.6") (d #t) (k 0)))) (h "1gy9y69d38q7f5147kj823swgggc3m30x7z2z1lrjpwpsxncf8zh")))

(define-public crate-argh-0.1.7 (c (n "argh") (v "0.1.7") (d (list (d (n "argh_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.7") (d #t) (k 0)))) (h "0n0r1xbhrn86r94zph2np4djdr65cp127c2sp5nbkyidv62ivd6v")))

(define-public crate-argh-0.1.8 (c (n "argh") (v "0.1.8") (d (list (d (n "argh_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)))) (h "0qks2z9qvxj72wjg3fvmfm8sjcr3nkydlq877h17wx20gsmf9rx7")))

(define-public crate-argh-0.1.9 (c (n "argh") (v "0.1.9") (d (list (d (n "argh_derive") (r "^0.1.9") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1qxb1jn3y1c3p6by1l0lhy96yjz5nrh6hf9irknxax10zpnfsxf3")))

(define-public crate-argh-0.1.10 (c (n "argh") (v "0.1.10") (d (list (d (n "argh_derive") (r "^0.1.10") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0gprrqzjigbibhvbhy7szaf6lqv4xnsif0kga9svz5llxfbpc9db")))

(define-public crate-argh-0.1.11 (c (n "argh") (v "0.1.11") (d (list (d (n "argh_derive") (r "^0.1.11") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0s9r05i25kpj5bqqcinxjc67hjkb2k8gkm05yqc3ym6my3z7c7rh")))

(define-public crate-argh-0.1.12 (c (n "argh") (v "0.1.12") (d (list (d (n "argh_derive") (r "^0.1.12") (d #t) (k 0)) (d (n "argh_shared") (r "^0.1.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "06fjmac07knqw7vahra9rkbfrrsv31yrqhf7wi623xvzjq3bmxbs")))

