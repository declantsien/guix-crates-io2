(define-module (crates-io ar gh arghsh) #:use-module (crates-io))

(define-public crate-arghsh-0.1.0 (c (n "arghsh") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0j12mlwzzw04pcb1ljk4cknz7kkg7azskfq9dack37d62y88y62z")))

(define-public crate-arghsh-0.1.1 (c (n "arghsh") (v "0.1.1") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1025mfspcqnn77ilfhrryqq2y1cjwps7r49k4nrdlwqhbangac6c")))

