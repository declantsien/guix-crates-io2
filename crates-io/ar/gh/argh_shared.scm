(define-module (crates-io ar gh argh_shared) #:use-module (crates-io))

(define-public crate-argh_shared-0.1.0 (c (n "argh_shared") (v "0.1.0") (h "0havs0w9ilvhq9xsmqmb3dwl3xjgcvssp66h5wvmn57pj72h0nhj")))

(define-public crate-argh_shared-0.1.1 (c (n "argh_shared") (v "0.1.1") (h "1x8zc5ckp7iqd54hbln8fyhz724q4s53833awf8qaxva4zs6ifp1")))

(define-public crate-argh_shared-0.1.4 (c (n "argh_shared") (v "0.1.4") (h "1cacdyl980sarjfx32v6d57snk76c7dvak3mp7fvlvc2r5n367vq")))

(define-public crate-argh_shared-0.1.5 (c (n "argh_shared") (v "0.1.5") (h "006aa509w15dyhbkr5bxicbfkfz61q9i57ybcb8ibx5qkh0ynqca")))

(define-public crate-argh_shared-0.1.6 (c (n "argh_shared") (v "0.1.6") (h "0crzkzr4mq9gyys3m0idgsfwwrwd4dk70scp7rspvb2fmgd01piq")))

(define-public crate-argh_shared-0.1.7 (c (n "argh_shared") (v "0.1.7") (h "1b1f9g3mm881bhy4anmr3cjbfjs7jq0pznbw20v1pai8za0c7y76")))

(define-public crate-argh_shared-0.1.8 (c (n "argh_shared") (v "0.1.8") (h "1npc0k84i7n0dvs5fymmmdhsqiqz9wcmjcngw6iwgbvdk2c3n9a7")))

(define-public crate-argh_shared-0.1.9 (c (n "argh_shared") (v "0.1.9") (h "1adnxljnfvgiqhrvzbizq7nz59zry1l5m1g0ca1629qqxjxpb7ql")))

(define-public crate-argh_shared-0.1.10 (c (n "argh_shared") (v "0.1.10") (h "0vshhn6znb1xywvfaj5x6gfpkpf3h9dwrrxvzwvksplnblar9jv4")))

(define-public crate-argh_shared-0.1.11 (c (n "argh_shared") (v "0.1.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h02zg29i18j5s8idr2w43dp7jwhrgc98v4xvy95rla33zgf120f")))

(define-public crate-argh_shared-0.1.12 (c (n "argh_shared") (v "0.1.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cbmf3n5fd7ha014m303f4bmsmj0v84an4a1rh77d9dx868z74sn")))

