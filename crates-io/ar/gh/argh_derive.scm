(define-module (crates-io ar gh argh_derive) #:use-module (crates-io))

(define-public crate-argh_derive-0.1.0 (c (n "argh_derive") (v "0.1.0") (d (list (d (n "argh_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0azgr1crq5jqcjlixkw4awdkcbnrdh1yn4g4bi80hq5xdd04l770")))

(define-public crate-argh_derive-0.1.1 (c (n "argh_derive") (v "0.1.1") (d (list (d (n "argh_shared") (r "^0.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r7pbqn397vabahcsfkyaa7yrlvrnb10h5w0rcmr7z231x71jhp7")))

(define-public crate-argh_derive-0.1.4 (c (n "argh_derive") (v "0.1.4") (d (list (d (n "argh_shared") (r "^0.1.4") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rxhd359d4gpac9524xx8qz13wiq3vilqnn95m0pgm0a2860rsy4")))

(define-public crate-argh_derive-0.1.5 (c (n "argh_derive") (v "0.1.5") (d (list (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g4d7fcbz963r9rzdv9p28n41b52rfq38i1m5r2fjlak6x19r530")))

(define-public crate-argh_derive-0.1.6 (c (n "argh_derive") (v "0.1.6") (d (list (d (n "argh_shared") (r "^0.1.5") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13qz9i9frdjl1v9aqw5b2cs7wn3h34x2xkpsi9wcl1hcpjd23ba8")))

(define-public crate-argh_derive-0.1.7 (c (n "argh_derive") (v "0.1.7") (d (list (d (n "argh_shared") (r "^0.1.7") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1df3ipaznhr76cw7i5m72xzqysxcqsavsl0s6fmxcza9yl7gfsdy")))

(define-public crate-argh_derive-0.1.8 (c (n "argh_derive") (v "0.1.8") (d (list (d (n "argh_shared") (r "^0.1.8") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g5l39simwnkrrkig00ksnxm8r74p84xa6sjwps18r7dyrzvvwk9")))

(define-public crate-argh_derive-0.1.9 (c (n "argh_derive") (v "0.1.9") (d (list (d (n "argh_shared") (r "^0.1.9") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00vszcr90w66hrnj48b9ycbdf4q01wml2fd006zs2281p1wk80da")))

(define-public crate-argh_derive-0.1.10 (c (n "argh_derive") (v "0.1.10") (d (list (d (n "argh_shared") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19jg69pr5bjr92q2rmiaavcb17vc23dy369ky0qk61cf539xp0mk")))

(define-public crate-argh_derive-0.1.11 (c (n "argh_derive") (v "0.1.11") (d (list (d (n "argh_shared") (r "^0.1.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08cwgrmrpn3s504xl2zz6l63m5xrc2ya636z1y18ihqzi7fqid8y")))

(define-public crate-argh_derive-0.1.12 (c (n "argh_derive") (v "0.1.12") (d (list (d (n "argh_shared") (r "^0.1.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ynq2f2f05ybhmvg5y4m1kdfihw4jsq3bnq6gp32yykbvzp0mpsn")))

