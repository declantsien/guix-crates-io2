(define-module (crates-io ar no arnold-rs) #:use-module (crates-io))

(define-public crate-arnold-rs-0.1.0 (c (n "arnold-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1c4d28nsv73mipfr847yhipffjdd7r0f425l4gz4yiqcsmqc9198")))

