(define-module (crates-io ar k- ark-serialize-zypher) #:use-module (crates-io))

(define-public crate-ark-serialize-zypher-0.4.2 (c (n "ark-serialize-zypher") (v "0.4.2") (d (list (d (n "ark-serialize-derive") (r "^0.4.2") (o #t) (d #t) (k 0) (p "ark-serialize-derive-zypher")) (d (n "ark-std") (r "^0.4.0") (k 0) (p "ark-std-zypher")) (d (n "ark-test-curves") (r "^0.4.2") (f (quote ("bls12_381_curve"))) (k 2)) (d (n "blake2") (r "^0.10") (k 2)) (d (n "digest") (r "^0.10") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "sha3") (r "^0.10") (k 2)))) (h "1b7iw4mjj86lramaalpjdkvsg889chr4gm535x6bc7lf7c0s280z") (f (quote (("std" "ark-std/std") ("derive" "ark-serialize-derive") ("default")))) (r "1.70")))

