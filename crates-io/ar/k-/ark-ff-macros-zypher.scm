(define-module (crates-io ar k- ark-ff-macros-zypher) #:use-module (crates-io))

(define-public crate-ark-ff-macros-zypher-0.4.2 (c (n "ark-ff-macros-zypher") (v "0.4.2") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "153vpfr8bwgcx0flhsx0smqk4506m4fbm2n1amf5cmw3ldh9qg6n") (r "1.70")))

