(define-module (crates-io ar k- ark-ff-asm-zypher) #:use-module (crates-io))

(define-public crate-ark-ff-asm-zypher-0.4.2 (c (n "ark-ff-asm-zypher") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0hr9594vh139bgi4wii9lzaf7aswz6bdnwwdqv39lvq50dyy7j32") (r "1.70")))

