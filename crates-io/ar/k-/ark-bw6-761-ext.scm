(define-module (crates-io ar k- ark-bw6-761-ext) #:use-module (crates-io))

(define-public crate-ark-bw6-761-ext-0.4.1 (c (n "ark-bw6-761-ext") (v "0.4.1") (d (list (d (n "ark-algebra-test-templates") (r "^0.4.2") (k 2)) (d (n "ark-bw6-761") (r "^0.4.0") (k 0)) (d (n "ark-ec") (r "^0.4.2") (k 0)) (d (n "ark-ff") (r "^0.4.2") (k 0)) (d (n "ark-models-ext") (r "^0.4.1") (k 0)) (d (n "ark-serialize") (r "^0.4.2") (k 2)) (d (n "ark-std") (r "^0.4.0") (k 0)))) (h "1wmlmnd73ic2k581q305y85pd70a0yzy33jqcw04cvr68yx5zvnc") (f (quote (("std" "ark-bw6-761/std" "ark-ec/std" "ark-ff/std" "ark-models-ext/std" "ark-serialize/std" "ark-std/std") ("parallel" "ark-ec/parallel" "ark-ff/parallel" "ark-std/parallel") ("default" "std"))))))

