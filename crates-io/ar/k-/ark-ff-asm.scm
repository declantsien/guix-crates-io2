(define-module (crates-io ar k- ark-ff-asm) #:use-module (crates-io))

(define-public crate-ark-ff-asm-0.2.0 (c (n "ark-ff-asm") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "136r6360h04jwr7bxwhbnzdngpzpndnn2ndab3q1xbrp466b531y")))

(define-public crate-ark-ff-asm-0.3.0 (c (n "ark-ff-asm") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0i3fdiw7xl9k708zkbh0jr2w8jqwxqdd68ix9m0gnhv6py8d60nv")))

(define-public crate-ark-ff-asm-0.4.0-alpha (c (n "ark-ff-asm") (v "0.4.0-alpha") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0mjlpajywqldrxnb6g68dmhbxjzvgw8pqd2ycvh16swsz2lqizyc") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0-alpha.3 (c (n "ark-ff-asm") (v "0.4.0-alpha.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "057cnz9d58q7j17pkx925f48lannl6bpv2gbm2ryyjy8r09i0ak3") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0-alpha.4 (c (n "ark-ff-asm") (v "0.4.0-alpha.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "16vf120wvskv2x0khkmyldl8swg8p2sh9k8kwjwp8xz1ngyydiml") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0-alpha.5 (c (n "ark-ff-asm") (v "0.4.0-alpha.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13ivf87w4v45037yda8nc9zzvnjrrsq4622ksmgvh1s2nnjb8rsc") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0-alpha.6 (c (n "ark-ff-asm") (v "0.4.0-alpha.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0hkabk28qf13zakmkz267n4374bx20dvxln9irgcw8y2isr0v7d5") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0-alpha.7 (c (n "ark-ff-asm") (v "0.4.0-alpha.7") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "19dbmwgzws8wlwwzqr92fp7ii40f0j39nx516gjyva44yly0mfmk") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.0 (c (n "ark-ff-asm") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "03msl8s0za8wny9cy0q0a0pv8y84v11w10q724zhzhnwgznggj1z") (r "1.57")))

(define-public crate-ark-ff-asm-0.4.1 (c (n "ark-ff-asm") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0vq5q6hqcxp0cwybzwb86d2jj8rysf0x7gmsi4ymjnbrpam76s4x") (r "1.60")))

(define-public crate-ark-ff-asm-0.4.2-alpha.1 (c (n "ark-ff-asm") (v "0.4.2-alpha.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "05wsczsayybdjvf1083wzc9g1gqxvqxgsal9ffz9sb2hcrdznwgf") (r "1.60")))

(define-public crate-ark-ff-asm-0.4.2 (c (n "ark-ff-asm") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0j13p6ymma3ypbjipfkc937pn57as8qpwgrpg5nvrl2mw97smm1y") (r "1.60")))

