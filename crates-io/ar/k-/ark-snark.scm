(define-module (crates-io ar k- ark-snark) #:use-module (crates-io))

(define-public crate-ark-snark-0.2.0 (c (n "ark-snark") (v "0.2.0") (d (list (d (n "ark-ff") (r "^0.2.0") (k 0)) (d (n "ark-relations") (r "^0.2.0") (k 0)) (d (n "ark-std") (r "^0.2.0") (k 0)))) (h "18946rv34scljvgqv8cyzs093ijy2hh9haa10w0v11755x1jdnir")))

(define-public crate-ark-snark-0.3.0 (c (n "ark-snark") (v "0.3.0") (d (list (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "ark-relations") (r "^0.3.0") (k 0)) (d (n "ark-std") (r "^0.3.0") (k 0)))) (h "12ncnz0vcga5l0yja1lx3rzx339dfmwv0cnz6h5rqypnlpqxzhqd")))

(define-public crate-ark-snark-0.4.0-alpha.1 (c (n "ark-snark") (v "0.4.0-alpha.1") (d (list (d (n "ark-ff") (r "^0.4.0-alpha") (k 0)) (d (n "ark-relations") (r "^0.4.0-alpha") (k 0)) (d (n "ark-serialize") (r "^0.4.0-alpha") (k 0)) (d (n "ark-std") (r "^0.4.0-alpha") (k 0)))) (h "135l3naargd1kfjz546hq5w9j6qvfb2bg6d32rg182fsqpyyx0sq")))

(define-public crate-ark-snark-0.4.0 (c (n "ark-snark") (v "0.4.0") (d (list (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "ark-relations") (r "^0.4.0") (k 0)) (d (n "ark-serialize") (r "^0.4.0") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)))) (h "0qys0gjhyk39nwghhj44ryiqkvk8mng8hh82c25bndd36dlcrlw4")))

