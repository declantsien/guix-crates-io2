(define-module (crates-io ar k- ark-bls12-377-ext) #:use-module (crates-io))

(define-public crate-ark-bls12-377-ext-0.4.1 (c (n "ark-bls12-377-ext") (v "0.4.1") (d (list (d (n "ark-algebra-test-templates") (r "^0.4.2") (k 2)) (d (n "ark-bls12-377") (r "^0.4.0") (f (quote ("curve"))) (k 0)) (d (n "ark-ec") (r "^0.4.2") (k 0)) (d (n "ark-ff") (r "^0.4.2") (k 2)) (d (n "ark-models-ext") (r "^0.4.1") (k 0)) (d (n "ark-serialize") (r "^0.4.2") (k 2)) (d (n "ark-std") (r "^0.4.0") (k 0)))) (h "0m8yhkrckqa0x3vyxq7zv960fmrwmz19gfhf72hbw30a30gh5ir0") (f (quote (("std" "ark-bls12-377/std" "ark-ec/std" "ark-ff/std" "ark-models-ext/std" "ark-serialize/std" "ark-std/std") ("r1cs" "ark-bls12-377/r1cs") ("parallel" "ark-ec/parallel" "ark-ff/parallel" "ark-std/parallel") ("default" "std"))))))

