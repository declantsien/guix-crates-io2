(define-module (crates-io ar k- ark-secp384r1) #:use-module (crates-io))

(define-public crate-ark-secp384r1-0.4.0 (c (n "ark-secp384r1") (v "0.4.0") (d (list (d (n "ark-ec") (r "^0.4.0") (k 0)) (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "ark-r1cs-std") (r "^0.4.0") (o #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)) (d (n "ark-algebra-test-templates") (r "^0.4.0") (k 2)) (d (n "ark-relations") (r "^0.4.0") (k 2)) (d (n "ark-serialize") (r "^0.4.0") (k 2)))) (h "1n2kydpa2gfdwsi1fbkq8a2qjkpphbn08c0glngn47fy6wwzscj0") (f (quote (("std" "ark-std/std" "ark-ff/std" "ark-ec/std") ("r1cs" "ark-r1cs-std") ("default"))))))

