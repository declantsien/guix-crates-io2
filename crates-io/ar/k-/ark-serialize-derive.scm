(define-module (crates-io ar k- ark-serialize-derive) #:use-module (crates-io))

(define-public crate-ark-serialize-derive-0.2.0 (c (n "ark-serialize-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12hxbvs78fpby2ki93w6nf9plj1ixl3d2xifbggza08bfn6dghss")))

(define-public crate-ark-serialize-derive-0.3.0 (c (n "ark-serialize-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10ly34zyx2yyjlhwd4zx11wjjghz86vzl9wdagnxb1c2pzqfbm4d")))

(define-public crate-ark-serialize-derive-0.4.0-alpha (c (n "ark-serialize-derive") (v "0.4.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17k7bnzrx33df2c6spgwsgn9my1jlx118ifmhra06qdip0m74gil") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0-alpha.3 (c (n "ark-serialize-derive") (v "0.4.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lcgk4h0i7wv6x20k4wq36h49fvs2mcwri4d58l578vg32jxqmvd") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0-alpha.4 (c (n "ark-serialize-derive") (v "0.4.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19q238zl1k12bnkg5pjw4hpzwbl1klx63n4a1jjr0sym4vzpbcka") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0-alpha.5 (c (n "ark-serialize-derive") (v "0.4.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rk1v211mnc954zzxlki7l0m4z2lclils7gffnh19gdkdfc9njsb") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0-alpha.6 (c (n "ark-serialize-derive") (v "0.4.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1azyy44yg7x3xg9z57nl993dqsj7v7na36yf79pc7iizg21ihfjk") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0-alpha.7 (c (n "ark-serialize-derive") (v "0.4.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07ffisz6z422r7fmpmdq5cgwjck9wzf6ygw6m2sx4a7bijxia5hy") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.0 (c (n "ark-serialize-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lw9sw09yfswmwlhc54hb9a4qw6cbq6m0488i61wh82059vk64qh") (r "1.56")))

(define-public crate-ark-serialize-derive-0.4.1 (c (n "ark-serialize-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k760fb0fmr505mqfxcqvsk8kpkhdx0ir1iq5y9jqpcr1n9g0d7x") (r "1.60")))

(define-public crate-ark-serialize-derive-0.4.2-alpha.1 (c (n "ark-serialize-derive") (v "0.4.2-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "082566f5fwdsnxcfkgxldrs749jaz4jjipmx15ckw0b3bqdw9xhm") (r "1.60")))

(define-public crate-ark-serialize-derive-0.4.2 (c (n "ark-serialize-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sl2yrpk88v4lfgkblwgd2yqa89hw48jbd9jmx4ybmqgdny82cmf") (r "1.60")))

