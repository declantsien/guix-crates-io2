(define-module (crates-io ar k- ark-crypto-primitives-macros-zypher) #:use-module (crates-io))

(define-public crate-ark-crypto-primitives-macros-zypher-0.4.0 (c (n "ark-crypto-primitives-macros-zypher") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xm5m2r8wihqfbvmvxrvkzsbf2gk3wrf257p6f61n8s30jy0fyg7")))

