(define-module (crates-io ar k- ark-transcript) #:use-module (crates-io))

(define-public crate-ark-transcript-0.0.1 (c (n "ark-transcript") (v "0.0.1") (d (list (d (n "ark-ed25519") (r "^0.4") (k 2)) (d (n "ark-ff") (r "^0.4") (k 0)) (d (n "ark-serialize") (r "^0.4") (k 0)) (d (n "ark-std") (r "^0.4") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "145ddxzz509lfylmc35n4ykgrzzl63i0j9691kykmjw7abv8gpih") (f (quote (("std") ("default") ("debug-transcript" "std"))))))

