(define-module (crates-io ar k- ark-msm) #:use-module (crates-io))

(define-public crate-ark-msm-0.3.0-alpha.1 (c (n "ark-msm") (v "0.3.0-alpha.1") (d (list (d (n "ark-bls12-381") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-std") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)))) (h "1a3a89icinn8k54d9z09jad49hc2rhgg9fmqin8yzxqnbi5dlc2b") (r "1.63")))

