(define-module (crates-io ar k- ark-ff-macros) #:use-module (crates-io))

(define-public crate-ark-ff-macros-0.2.0 (c (n "ark-ff-macros") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1f7fnppzz6lss92anzqdbhhkvfpsvkb1g98niiqdj3m1jdm2b70b")))

(define-public crate-ark-ff-macros-0.3.0 (c (n "ark-ff-macros") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "084bqfx24wlhbwsb67hnz3vamamw2pgyz7h0b2033jwcl2adfbyv")))

(define-public crate-ark-ff-macros-0.4.0-alpha (c (n "ark-ff-macros") (v "0.4.0-alpha") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "090305xwwr6i10xz9hyvjx26z8bcl5wjnr6qf01jq3zb5amks381") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0-alpha.3 (c (n "ark-ff-macros") (v "0.4.0-alpha.3") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1vnhn0gavrmsh5g6hcj9hsydm1dn0bjy70h39srdxv0vz3w4d112") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0-alpha.4 (c (n "ark-ff-macros") (v "0.4.0-alpha.4") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1gz2pmdlxw4n2gr225k9zlxj9blygshk9yvvm4vbnl7i0h5598ic") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0-alpha.5 (c (n "ark-ff-macros") (v "0.4.0-alpha.5") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0bir1rk63g4iq2pybdca8x5qw3mbglrixsdsil6z20bcfw7lqhjc") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0-alpha.6 (c (n "ark-ff-macros") (v "0.4.0-alpha.6") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "101f6vz88zmz7gj4pw8hril4g8dwd1gw97i4i9h1a5cyc3r8773n") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0-alpha.7 (c (n "ark-ff-macros") (v "0.4.0-alpha.7") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1yhmsbfynhrajvv5w7wks384q3bkxxvi10wn4wik2brdqhga12cr") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.0 (c (n "ark-ff-macros") (v "0.4.0") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13gznzhknwkzanqydjfwzvxgbyg5139blh4par768b8570735ahm") (r "1.56")))

(define-public crate-ark-ff-macros-0.4.1 (c (n "ark-ff-macros") (v "0.4.1") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "17cmihdn3m1klahf96v3ff8a3vg76r6wfmakjbycfz6nyb8fghpk") (r "1.60")))

(define-public crate-ark-ff-macros-0.4.2-alpha.1 (c (n "ark-ff-macros") (v "0.4.2-alpha.1") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ycg80dkmnj68c7h22x0waqrrk694sii58gwms2zaj5m4prhc5sl") (r "1.60")))

(define-public crate-ark-ff-macros-0.4.2 (c (n "ark-ff-macros") (v "0.4.2") (d (list (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ra5a4paqbqqlc9a3cnwkwxh7l1411wsnna1az28k218wjq7kgks") (r "1.60")))

