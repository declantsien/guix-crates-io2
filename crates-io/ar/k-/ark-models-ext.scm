(define-module (crates-io ar k- ark-models-ext) #:use-module (crates-io))

(define-public crate-ark-models-ext-0.4.1 (c (n "ark-models-ext") (v "0.4.1") (d (list (d (n "ark-ec") (r "^0.4.2") (k 0)) (d (n "ark-ff") (r "^0.4.2") (k 0)) (d (n "ark-serialize") (r "^0.4.2") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)) (d (n "derivative") (r "^2.2") (f (quote ("use_core"))) (k 0)))) (h "1hjyhc426p5ckw2rnh35ljq89c69mm189lv3nwlg5wjz9dfsp7iy") (f (quote (("std" "ark-ff/std" "ark-serialize/std" "ark-std/std") ("parallel" "ark-std/parallel" "std") ("default" "std"))))))

