(define-module (crates-io ar k- ark-std) #:use-module (crates-io))

(define-public crate-ark-std-0.2.0 (c (n "ark-std") (v "0.2.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0w548m4cm2ssiq729sss9ky20ggfpsbaa5l1hl8bi9xy55m8anzv") (f (quote (("std") ("print-trace" "std" "colored") ("parallel" "rayon" "std") ("default" "std"))))))

(define-public crate-ark-std-0.3.0 (c (n "ark-std") (v "0.3.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0z2fbjicajga7skmrysx0nqjivmcnbyh03npn4la1ifb569c1whx") (f (quote (("std") ("print-trace" "std" "colored") ("parallel" "rayon" "std") ("default" "std"))))))

(define-public crate-ark-std-0.4.0-alpha (c (n "ark-std") (v "0.4.0-alpha") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std"))) (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0sm0bf570dp6qw0an16c76bgpxwx45anfn7zhqwf9cp96kbicnyc") (f (quote (("std") ("print-trace" "std" "colored") ("parallel" "rayon" "std") ("getrandom" "rand/std") ("default" "std"))))))

(define-public crate-ark-std-0.4.0 (c (n "ark-std") (v "0.4.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std"))) (d #t) (k 2)))) (h "11aigd20y0x9l3mw1bxsrgklzbscn86lrp78mmjbgskf1hg3z2cl") (f (quote (("std") ("print-trace" "std" "colored") ("parallel" "rayon" "std") ("getrandom" "rand/std") ("default" "std"))))))

