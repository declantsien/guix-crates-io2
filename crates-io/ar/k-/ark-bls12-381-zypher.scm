(define-module (crates-io ar k- ark-bls12-381-zypher) #:use-module (crates-io))

(define-public crate-ark-bls12-381-zypher-0.4.0 (c (n "ark-bls12-381-zypher") (v "0.4.0") (d (list (d (n "ark-algebra-bench-templates") (r "^0.4.0") (k 2)) (d (n "ark-algebra-test-templates") (r "^0.4.0") (k 2)) (d (n "ark-ec") (r "^0.4.0") (d #t) (k 0) (p "ark-ec-zypher")) (d (n "ark-ff") (r "^0.4.0") (k 0) (p "ark-ff-zypher")) (d (n "ark-serialize") (r "^0.4.0") (k 0) (p "ark-serialize-zypher")) (d (n "ark-std") (r "^0.4.0") (k 0) (p "ark-std-zypher")) (d (n "hex") (r "^0.4.0") (d #t) (k 2)))) (h "1qd4d6dzg6rrvlmj78rj6dfayxnis1ib1wc8sgimfv974zbvq6sb") (f (quote (("std" "ark-std/std" "ark-ff/std" "ark-ec/std") ("scalar_field") ("default" "curve") ("curve" "scalar_field"))))))

