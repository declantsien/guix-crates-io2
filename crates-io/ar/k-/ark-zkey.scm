(define-module (crates-io ar k- ark-zkey) #:use-module (crates-io))

(define-public crate-ark-zkey-0.1.0 (c (n "ark-zkey") (v "0.1.0") (d (list (d (n "ark-bn254") (r "=0.4.0") (d #t) (k 0)) (d (n "ark-circom") (r "^0.1.0") (d #t) (k 0)) (d (n "ark-ec") (r "=0.4.1") (d #t) (k 0)) (d (n "ark-ff") (r "=0.4.1") (d #t) (k 0)) (d (n "ark-groth16") (r "=0.4.0") (d #t) (k 0)) (d (n "ark-relations") (r "=0.4.0") (d #t) (k 0)) (d (n "ark-serialize") (r "=0.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "flame") (r "^0.2") (d #t) (k 0)) (d (n "flamer") (r "^0.5") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)))) (h "12gr6fa3nm89ayf1yhqb9rmp8fgdvd47iyizfnv1s1qskkswjx19")))

