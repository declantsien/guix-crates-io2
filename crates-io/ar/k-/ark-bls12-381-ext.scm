(define-module (crates-io ar k- ark-bls12-381-ext) #:use-module (crates-io))

(define-public crate-ark-bls12-381-ext-0.4.1 (c (n "ark-bls12-381-ext") (v "0.4.1") (d (list (d (n "ark-algebra-test-templates") (r "^0.4.2") (k 2)) (d (n "ark-bls12-381") (r "^0.4.0") (f (quote ("curve"))) (k 0)) (d (n "ark-ec") (r "^0.4.2") (k 0)) (d (n "ark-ff") (r "^0.4.2") (k 0)) (d (n "ark-models-ext") (r "^0.4.1") (k 0)) (d (n "ark-serialize") (r "^0.4.2") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)))) (h "0733h0sg0k93wijlznci2czy8qc3bgwi55wldv08x7pi10ylpp5i") (f (quote (("std" "ark-bls12-381/std" "ark-ec/std" "ark-ff/std" "ark-models-ext/std" "ark-serialize/std" "ark-std/std") ("parallel" "ark-ec/parallel" "ark-ff/parallel" "ark-std/parallel") ("default" "std"))))))

