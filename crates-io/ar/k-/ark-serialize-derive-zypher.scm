(define-module (crates-io ar k- ark-serialize-derive-zypher) #:use-module (crates-io))

(define-public crate-ark-serialize-derive-zypher-0.4.2 (c (n "ark-serialize-derive-zypher") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0fsxi691ix0n9d25g0chjvqvfqxrwl5sxns6ky879knaqqc274wi") (r "1.70")))

