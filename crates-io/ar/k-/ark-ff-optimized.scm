(define-module (crates-io ar k- ark-ff-optimized) #:use-module (crates-io))

(define-public crate-ark-ff-optimized-0.4.0 (c (n "ark-ff-optimized") (v "0.4.0") (d (list (d (n "ark-algebra-bench-templates") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-algebra-test-templates") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-std") (r "^0.4.0") (d #t) (k 2)))) (h "18wz8n4v5bm6m3iwz879jn0f2ff4czblq0y7zw5afq0vljspzhwb") (f (quote (("asm" "ark-ff/asm"))))))

(define-public crate-ark-ff-optimized-0.4.1 (c (n "ark-ff-optimized") (v "0.4.1") (d (list (d (n "ark-algebra-bench-templates") (r "^0.4") (d #t) (k 2)) (d (n "ark-algebra-test-templates") (r "^0.4") (d #t) (k 2)) (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4") (d #t) (k 2)) (d (n "ark-std") (r "^0.4") (d #t) (k 0)) (d (n "ark-std") (r "^0.4") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "012j5s9ksx04x5bms7nzwv0dd9qaamlfmwcbidvdbkjykdxgd6y6") (f (quote (("std") ("asm" "ark-ff/asm"))))))

