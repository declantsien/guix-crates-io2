(define-module (crates-io ar k- ark-relations) #:use-module (crates-io))

(define-public crate-ark-relations-0.2.0 (c (n "ark-relations") (v "0.2.0") (d (list (d (n "ark-ff") (r "^0.2.0") (k 0)) (d (n "ark-std") (r "^0.2.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (k 0)))) (h "02rwcl6469sal7n8f7jc5j6xrxyp93mcgh2325bb1wpzim7i4bx4") (f (quote (("std" "ark-std/std" "ark-ff/std" "tracing-subscriber" "tracing/std") ("default"))))))

(define-public crate-ark-relations-0.3.0 (c (n "ark-relations") (v "0.3.0") (d (list (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "ark-std") (r "^0.3.0") (k 0)) (d (n "ark-test-curves") (r "^0.3.0") (f (quote ("bls12_381_scalar_field"))) (k 2)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (k 0)))) (h "1xiqbg3bxlg12yd17idgs9ccvhlfaxvgvxwppls6hakrk4f4rfjc") (f (quote (("std" "ark-std/std" "ark-ff/std" "tracing-subscriber" "tracing/std") ("default"))))))

(define-public crate-ark-relations-0.4.0-alpha.1 (c (n "ark-relations") (v "0.4.0-alpha.1") (d (list (d (n "ark-ff") (r "^0.4.0-alpha") (k 0)) (d (n "ark-std") (r "^0.4.0-alpha") (k 0)) (d (n "ark-test-curves") (r "^0.4.0-alpha") (f (quote ("bls12_381_scalar_field"))) (k 2)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (k 0)))) (h "06vxzsfvwqscwfcrkwr6wk1l0s131cv24z9rvva1z7siaxwvpz6q") (f (quote (("std" "ark-std/std" "ark-ff/std" "tracing-subscriber" "tracing/std") ("default"))))))

(define-public crate-ark-relations-0.4.0 (c (n "ark-relations") (v "0.4.0") (d (list (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (k 0)) (d (n "ark-test-curves") (r "^0.4.0") (f (quote ("bls12_381_scalar_field"))) (k 2)))) (h "1w5l3zri5wdahrbjv1whqgkq3257rnibd7754n1g98q5zip6ny80") (f (quote (("std" "ark-std/std" "ark-ff/std" "tracing-subscriber" "tracing/std") ("default"))))))

