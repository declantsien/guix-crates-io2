(define-module (crates-io ar k- ark-algebra-intro) #:use-module (crates-io))

(define-public crate-ark-algebra-intro-0.3.0 (c (n "ark-algebra-intro") (v "0.3.0") (d (list (d (n "ark-bls12-381") (r "^0.3") (f (quote ("curve"))) (d #t) (k 0)) (d (n "ark-ec") (r "^0.3") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.3") (d #t) (k 0)) (d (n "ark-std") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng"))) (d #t) (k 0)))) (h "1m9f15fcphw9rmx89a4fnnjpax8cqh9glld93jcdja0r4kylnmsy")))

