(define-module (crates-io ar k- ark-relations-zypher) #:use-module (crates-io))

(define-public crate-ark-relations-zypher-0.4.0 (c (n "ark-relations-zypher") (v "0.4.0") (d (list (d (n "ark-ff") (r "^0.4.0") (k 0) (p "ark-ff-zypher")) (d (n "ark-std") (r "^0.4.0") (k 0) (p "ark-std-zypher")) (d (n "ark-test-curves") (r "^0.4.0") (f (quote ("bls12_381_scalar_field"))) (k 2)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (k 0)))) (h "187z1sy3bl8pwcagx7gwkgjpw8mpq948ibvnykrk485i23fc37g0") (f (quote (("std" "ark-std/std" "ark-ff/std" "tracing-subscriber" "tracing/std") ("default"))))))

