(define-module (crates-io ar k- ark-api-macros) #:use-module (crates-io))

(define-public crate-ark-api-macros-0.6.0 (c (n "ark-api-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "03sblnrlw6pqnpxs1cdcn30vs0vwx3d73a0nhlaa4wysya1y6dn4")))

(define-public crate-ark-api-macros-0.8.1 (c (n "ark-api-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "189kfcbj4yppjk28054gqcyx9amyxzjkm7llv1ym17af6h9m4mxb")))

(define-public crate-ark-api-macros-0.9.0 (c (n "ark-api-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "13d3frmiff3ln1i09m0s76a1s7rmwdf3ygr3ajqm4p52b0cd7qw9") (r "1.56")))

(define-public crate-ark-api-macros-0.10.0 (c (n "ark-api-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "1idljjrb5cjncxxkangp52037s6ky1sajycsszkcjs9lj4isq3kv") (r "1.56")))

(define-public crate-ark-api-macros-0.11.0 (c (n "ark-api-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "063cjb7h4bmjg7lz93phhl5xgpqvxn2gl79lpypbpfrj26jfi510") (r "1.63.0")))

(define-public crate-ark-api-macros-0.12.0 (c (n "ark-api-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "11nd00npaqxb80jwsrmj8w67pq49mwdymgvan74n8pv192zzjylv") (f (quote (("ffi_profiling")))) (r "1.70.0")))

(define-public crate-ark-api-macros-0.13.0 (c (n "ark-api-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit" "visit-mut" "extra-traits" "full"))) (d #t) (k 0)))) (h "162h1v7pxm3n7y0b6ihr9vpqbxi8qhk0d5shzmwd8vsm3zy8i9ra") (f (quote (("ffi_profiling")))) (r "1.75.0")))

