(define-module (crates-io ar k- ark-poly-zypher) #:use-module (crates-io))

(define-public crate-ark-poly-zypher-0.4.2 (c (n "ark-poly-zypher") (v "0.4.2") (d (list (d (n "ark-ff") (r "^0.4.2") (k 0) (p "ark-ff-zypher")) (d (n "ark-serialize") (r "^0.4.2") (f (quote ("derive"))) (k 0) (p "ark-serialize-zypher")) (d (n "ark-std") (r "^0.4.0") (k 0) (p "ark-std-zypher")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "derivative") (r "^2") (f (quote ("use_core"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1b47wrbjhwnygca20lvdv2vx41gdabz67ynmx5vjlfahilsimn5a") (f (quote (("std" "ark-std/std" "ark-ff/std") ("parallel" "std" "ark-ff/parallel" "rayon" "ark-std/parallel") ("default")))) (r "1.70")))

