(define-module (crates-io ar k- ark-secp256r1) #:use-module (crates-io))

(define-public crate-ark-secp256r1-0.4.0 (c (n "ark-secp256r1") (v "0.4.0") (d (list (d (n "ark-ec") (r "^0.4.0") (k 0)) (d (n "ark-ff") (r "^0.4.0") (k 0)) (d (n "ark-r1cs-std") (r "^0.4.0") (o #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (k 0)) (d (n "ark-algebra-test-templates") (r "^0.4.0") (k 2)) (d (n "ark-relations") (r "^0.4.0") (k 2)) (d (n "ark-serialize") (r "^0.4.0") (k 2)))) (h "0czni3bpqfx3rkszd0arz9rgq836i9cshz7cf87swgkf18ds0x9r") (f (quote (("std" "ark-std/std" "ark-ff/std" "ark-ec/std") ("r1cs" "ark-r1cs-std") ("default"))))))

