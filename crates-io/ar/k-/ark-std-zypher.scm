(define-module (crates-io ar k- ark-std-zypher) #:use-module (crates-io))

(define-public crate-ark-std-zypher-0.4.0 (c (n "ark-std-zypher") (v "0.4.0") (d (list (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std"))) (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1ssf9hpwiqkvxznfw9illhnrmdr1g7fyi12wmql0b41k86b71hmx") (f (quote (("std") ("print-trace" "std" "colored") ("parallel" "rayon" "std") ("getrandom" "rand/std") ("default" "std"))))))

