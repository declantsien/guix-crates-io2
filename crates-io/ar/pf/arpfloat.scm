(define-module (crates-io ar pf arpfloat) #:use-module (crates-io))

(define-public crate-arpfloat-0.1.0 (c (n "arpfloat") (v "0.1.0") (h "11zr8nmarbirxrrc2gx52wxsjss5q6jcn9gpa2pwbxlp4j5ij4r8")))

(define-public crate-arpfloat-0.1.1 (c (n "arpfloat") (v "0.1.1") (h "0q5hpjf4z3k3yxpipn5kdgdm4bar1hfm9kmh9r83rgj667rglckx")))

(define-public crate-arpfloat-0.1.2 (c (n "arpfloat") (v "0.1.2") (h "150vjaza63shqgsyxf4xpkifc090iwhn6afcmxgh1qpsy18zsgci")))

(define-public crate-arpfloat-0.1.3 (c (n "arpfloat") (v "0.1.3") (h "1viw4xvizj674gv9y0k4dm4r17jfia5w05irwd112zls9jgzmgw5") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.4 (c (n "arpfloat") (v "0.1.4") (h "1ynbh9yc5gqdvv45kwyhj7hx4ccfmyrxf7mi29m6f3yvvnl34l1i") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.5 (c (n "arpfloat") (v "0.1.5") (h "18rpzavwaq045xf7pclcb6d9c29cd0kirgvqm93piskaiclgk1xd") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.6 (c (n "arpfloat") (v "0.1.6") (h "0l12c93m3cm2dy7pbkvzhfrxmj010bxlgg226hb0d3mcm2n6j74k") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.7 (c (n "arpfloat") (v "0.1.7") (h "0bzzd4grjjl0b2sn618q6yz9xv6i5mjg3yj6vnj69q3vzj1wn9aq") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.8 (c (n "arpfloat") (v "0.1.8") (h "0liwrp4fbccni4ph0r17khr6p2sqdr0dz2s04p87m4bikypyzlys") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.9 (c (n "arpfloat") (v "0.1.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0cdw2v0j6wmw0xa2544piw5bxzy586kv8lw3hmlznh4j0rxmjxvb") (f (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1.10 (c (n "arpfloat") (v "0.1.10") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1qkrk2nixwjxghwnhk62k7slafbf3yjiys0xhrwvn2z981aj6mww") (f (quote (("std") ("default" "std"))))))

