(define-module (crates-io ar _c ar_cuil_cuit_validator) #:use-module (crates-io))

(define-public crate-ar_cuil_cuit_validator-0.1.0 (c (n "ar_cuil_cuit_validator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mp1hfmnq0hyg8kxldwrs1iaj2516ll3f47ibyli2wf502midkyx")))

(define-public crate-ar_cuil_cuit_validator-0.1.1 (c (n "ar_cuil_cuit_validator") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12603y68igfzg66gw1n48pvvkb4pnf1givj767xcf3ya53ik0jlx")))

