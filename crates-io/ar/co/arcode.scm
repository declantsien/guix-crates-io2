(define-module (crates-io ar co arcode) #:use-module (crates-io))

(define-public crate-arcode-0.1.0 (c (n "arcode") (v "0.1.0") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "02bxgz0m41mzx3hfmfpgmixh5gqclc0iqj3j8hd0dvyzd5psmyw0")))

(define-public crate-arcode-0.1.1 (c (n "arcode") (v "0.1.1") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "0h6h3q8451kik47n8q8g18f9dmrv5lwjgg6bdbr59qllrh0grnh5")))

(define-public crate-arcode-0.1.2 (c (n "arcode") (v "0.1.2") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "1b5c5gmy87s4fv3qpiva49wkpf9v8mhvyis2rhqsk7h9rlz0932d")))

(define-public crate-arcode-0.2.0 (c (n "arcode") (v "0.2.0") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "0x54vq4crc3q0pw1p25sddhzm1qa7ccfj1gbhf9mji648kv6npvv") (f (quote (("binary"))))))

(define-public crate-arcode-0.2.1 (c (n "arcode") (v "0.2.1") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "1gvg423xw3bxax1mn28nk0z5mdg43lc3l6qvwkfw7034zwyb75kv") (f (quote (("binary"))))))

(define-public crate-arcode-0.2.2 (c (n "arcode") (v "0.2.2") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "fenwick") (r "^0.2.0") (d #t) (k 0)))) (h "1fsf3n6ab4n4gi9hnfv2nh35ah3vsyqsdizwk62359lc4ms9bp59") (f (quote (("binary"))))))

(define-public crate-arcode-0.2.3 (c (n "arcode") (v "0.2.3") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fenwick") (r "^1.0.0") (d #t) (k 0)))) (h "13l7nipi2y9cx43ikdvx60zfw8nr335yg1zxwcrvxf3cc4pmxmna") (f (quote (("binary"))))))

(define-public crate-arcode-0.2.4 (c (n "arcode") (v "0.2.4") (d (list (d (n "bitbit") (r "^0.2.0") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fenwick") (r "^1.0.0") (d #t) (k 0)))) (h "1jr9nryvnvkf5s4jasrgk8fharzw4l5sl7mys47wvgkgygqfrybw") (r "1.56.1")))

