(define-module (crates-io ar co arcon_macros) #:use-module (crates-io))

(define-public crate-arcon_macros-0.1.0 (c (n "arcon_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (d #t) (k 0)))) (h "0zlqn388np08ixgnqpmpvw23f96l8ip8c7s9m0wlvv7fl6xffqkm")))

(define-public crate-arcon_macros-0.1.1 (c (n "arcon_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (d #t) (k 0)))) (h "1niiaiah745r83dcd7id7d0nsskkfxz6yhmsg0igxpk7cbm9q0nf")))

(define-public crate-arcon_macros-0.1.2 (c (n "arcon_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jcffv4p8whs9nxbgn0b9csndqhk7wj8jqj62qpifwkci3wa5ky4")))

(define-public crate-arcon_macros-0.1.3 (c (n "arcon_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c67r9cb2qvczh9l6dhdfin8zszqcyb7all3b0xqw2bxf4jz529x") (f (quote (("arcon_serde"))))))

(define-public crate-arcon_macros-0.2.0 (c (n "arcon_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b3435zrkcgb82zfacq35vwi93lpsgn6qilqnyvj64qlqhkms09b") (f (quote (("unsafe_flight") ("arcon_serde"))))))

(define-public crate-arcon_macros-0.2.1 (c (n "arcon_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sc02vys6kkxpi51ji3vik2smm856gbvz81angm9b0dm0453cpnh") (f (quote (("unsafe_flight"))))))

(define-public crate-arcon_macros-0.2.2 (c (n "arcon_macros") (v "0.2.2") (d (list (d (n "arcon") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hah26adkdka7722gk2k0v18glhr4afb7ang3qw0dplc2jpxcsl9")))

