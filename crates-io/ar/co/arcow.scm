(define-module (crates-io ar co arcow) #:use-module (crates-io))

(define-public crate-arcow-1.0.0 (c (n "arcow") (v "1.0.0") (h "1jkh034873i57ggxprajc4lxcn8b177476p04180b6b22s8bcj93")))

(define-public crate-arcow-1.1.0 (c (n "arcow") (v "1.1.0") (h "0ycrhkx9nf7qgbq0r0jqp9x8bw5blyhhgmnb4splxn86d98i40l9")))

(define-public crate-arcow-1.0.1 (c (n "arcow") (v "1.0.1") (h "0yjmi6ax56yz3gncdk56wfc5906d8p5drmb3iphwrwzbzvj429hs")))

(define-public crate-arcow-1.1.1 (c (n "arcow") (v "1.1.1") (h "05yicgjz8hr1nd2p1mm1r4hjvq3wc6dggqkfrvh0x9d7qh2hn7y2")))

