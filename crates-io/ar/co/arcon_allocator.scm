(define-module (crates-io ar co arcon_allocator) #:use-module (crates-io))

(define-public crate-arcon_allocator-0.2.0 (c (n "arcon_allocator") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "0cdrvmg7xkqd713hfanbh2hw24iw79d86las5s8b737ygrgqvqxv")))

(define-public crate-arcon_allocator-0.2.1 (c (n "arcon_allocator") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "metrics") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "15lq5djd09rjz793i5sjfqn9j47s6rnax0aw1cn6631q35373fld")))

