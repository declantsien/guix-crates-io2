(define-module (crates-io ar co arcon_backend) #:use-module (crates-io))

(define-public crate-arcon_backend-0.1.0 (c (n "arcon_backend") (v "0.1.0") (d (list (d (n "arcon_error") (r "^0.1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.1") (f (quote ("lz4"))) (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1s17hcjibjsix1kshz9c6ja6x2ka7sa7h26c9w9vqw76ncf291n4") (f (quote (("default"))))))

(define-public crate-arcon_backend-0.1.1 (c (n "arcon_backend") (v "0.1.1") (d (list (d (n "arcon_error") (r "^0.1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.1") (f (quote ("lz4"))) (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0g4nkwxgh1jv0sgycb9c5dida5j4g8cw0zf7cb6rgm9yficrsfxk") (f (quote (("default"))))))

