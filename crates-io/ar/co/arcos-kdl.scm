(define-module (crates-io ar co arcos-kdl) #:use-module (crates-io))

(define-public crate-arcos-kdl-0.1.0 (c (n "arcos-kdl") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0rd7yxb7r0rqxb6afsngnwb1vy92paqxz1sv115ll8bndasxjdl2")))

(define-public crate-arcos-kdl-0.2.0 (c (n "arcos-kdl") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "02hzyh368yr3k49sqxfpcm6mk6d34f10dmxpbmva1gihjzypssv0")))

(define-public crate-arcos-kdl-0.2.1 (c (n "arcos-kdl") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.19.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gdaxr0091h2ilgk6hmc9avkqxwzmbxw1g67m75l9kav6r8swq9y")))

(define-public crate-arcos-kdl-0.2.2 (c (n "arcos-kdl") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r36yqxr1jz8jhpqfff4w9gcx7sbkgp5qrlpa8dww7d9p5nswbq8")))

(define-public crate-arcos-kdl-0.2.3 (c (n "arcos-kdl") (v "0.2.3") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x9dxj56dgbfgjfqmncm72ksz2j7dixbfkhq0h3pclc3accv76qc")))

(define-public crate-arcos-kdl-0.2.4 (c (n "arcos-kdl") (v "0.2.4") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q401bnp3w2y5pnmfm52j50qx0nsqgj4b7qxikxlnq0hs4s6jq78")))

(define-public crate-arcos-kdl-0.3.0 (c (n "arcos-kdl") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j3a3w3a5m2azkdbwi0hd3fm4pqhc03324xq1a5vlbbjqn9s8kmb")))

(define-public crate-arcos-kdl-0.3.1 (c (n "arcos-kdl") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "19kl1pi7qdkqhx8lxgrnhjcp9km9pnnlsqmwvviqx8ysklmwxgk8")))

(define-public crate-arcos-kdl-0.3.2 (c (n "arcos-kdl") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ivhg3iqjg2h1qgsp4zbv5hmx1lck1qhaq62mg881lzha66vayq")))

