(define-module (crates-io ar co arcon_messages) #:use-module (crates-io))

(define-public crate-arcon_messages-0.1.0 (c (n "arcon_messages") (v "0.1.0") (d (list (d (n "capnp") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "capnpc") (r "^0.9") (o #t) (d #t) (k 1)) (d (n "derivative") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "kompact") (r "^0.7.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (o #t) (d #t) (k 1)))) (h "0l18vzsyn1qp6r8mgk0n98idjfb2cmk7ziq1l20fdvmr1x7w8b2v") (f (quote (("proto" "protobuf" "protoc-rust") ("default" "proto") ("capnproto" "capnp" "derivative" "capnpc"))))))

(define-public crate-arcon_messages-0.1.2 (c (n "arcon_messages") (v "0.1.2") (d (list (d (n "kompact") (r "^0.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "07f8hdmcccvlpwr0mhjspzyk1v7ibqjaklc89fj3vzi2andlxh16")))

