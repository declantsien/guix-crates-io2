(define-module (crates-io ar co arcon_spec) #:use-module (crates-io))

(define-public crate-arcon_spec-0.1.0 (c (n "arcon_spec") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "015n0j2f84j5cznw8vn7xjy8s02w02vlbfdw3r6c8m3q89h3zc15")))

(define-public crate-arcon_spec-0.1.1 (c (n "arcon_spec") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00sx1c8hv7rckdxqhnpdw838j8b6z0zgk3z5h5dmzgay69krpnis")))

(define-public crate-arcon_spec-0.1.2 (c (n "arcon_spec") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15rnx8j9w03mwcqpvgak7i952iaqpv6cqn22xg9b5wm24nlxhfsa")))

(define-public crate-arcon_spec-0.1.3 (c (n "arcon_spec") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "090m02a8p75cbkvqh624h2xzci1kjpnp4h0k7lk68g5fx4d6ywyq")))

