(define-module (crates-io ar co arcon_error) #:use-module (crates-io))

(define-public crate-arcon_error-0.1.0 (c (n "arcon_error") (v "0.1.0") (h "1jyhg4vmlgd2gf13im8kr2jyiwhv9z77h6jdbqq43d8rmwdhsw00")))

(define-public crate-arcon_error-0.1.1 (c (n "arcon_error") (v "0.1.1") (h "005jmw93shp7a8xzzd7y367icsic02hw50rg63201rk5w6ds5x8l")))

