(define-module (crates-io ar co arcon_extra) #:use-module (crates-io))

(define-public crate-arcon_extra-0.1.0 (c (n "arcon_extra") (v "0.1.0") (d (list (d (n "rdkafka") (r "~0.19") (o #t) (d #t) (k 0)))) (h "0b7n8cfm425xsq86389j3dg1l5jbiqmrb42w6db6l9jafnvlr5vh") (f (quote (("kafka" "rdkafka") ("default"))))))

(define-public crate-arcon_extra-0.1.1 (c (n "arcon_extra") (v "0.1.1") (h "1vah14mbz6nc2i5x6hb1sh33x5z8lix6s8z7nqbgnmmjlmbi1c4n") (f (quote (("default"))))))

