(define-module (crates-io ar px arpx_job_parser) #:use-module (crates-io))

(define-public crate-arpx_job_parser-0.1.0 (c (n "arpx_job_parser") (v "0.1.0") (h "0710ds1pkjdbrb72kkbavbjskf8dn502xk0jqnx3m6k36d594xvh") (y #t)))

(define-public crate-arpx_job_parser-0.1.1 (c (n "arpx_job_parser") (v "0.1.1") (h "1vbiifxa0gp1akgk78sjx21djx8b7qq6c2q6m5ffkhg3249lm0nv") (y #t)))

(define-public crate-arpx_job_parser-0.1.2 (c (n "arpx_job_parser") (v "0.1.2") (h "0cdxnqpal6s63gn3jqmi1rcq2zr8rpsqrpr8d8hglzy7722i6017") (y #t)))

(define-public crate-arpx_job_parser-0.1.3 (c (n "arpx_job_parser") (v "0.1.3") (h "0r4xfsmm2csj973p20bjlf8y42ikqfaanzv3n6rvffn58c5m1xgd") (y #t)))

(define-public crate-arpx_job_parser-0.1.4 (c (n "arpx_job_parser") (v "0.1.4") (h "1k9cfgafv0kbvh86q3rhhszid6m93wddwl1pcizrsdk3p61ii47p")))

(define-public crate-arpx_job_parser-0.1.5 (c (n "arpx_job_parser") (v "0.1.5") (h "0aw8ihj68x3p091wpw2kwi1j2gddbndi61fmiyf38jwa6spmkgwh")))

(define-public crate-arpx_job_parser-0.1.6 (c (n "arpx_job_parser") (v "0.1.6") (h "07gx8hcv4vlsmk2yrcn63sf9min2ndcygxlqnrp76hlxdaa77myl")))

(define-public crate-arpx_job_parser-0.1.7 (c (n "arpx_job_parser") (v "0.1.7") (h "1w04k9ggip3w9wnp7yg04m3w2iwn0lsa58nnpqkz896wswiaf1p9")))

(define-public crate-arpx_job_parser-0.1.8 (c (n "arpx_job_parser") (v "0.1.8") (h "0zv00xgkjfcaphd276dz8p7y26d7d5rnfsm4v04yznqgdq2rkl0z")))

(define-public crate-arpx_job_parser-0.1.9 (c (n "arpx_job_parser") (v "0.1.9") (h "1pr1s6vxxk4mvx2zlhwj6zc2q0r1d4acwcbg5x84s8akxvrkgpq1")))

(define-public crate-arpx_job_parser-0.1.10 (c (n "arpx_job_parser") (v "0.1.10") (h "1zjb0gw2ir4vn4nsnc152jn6vv6jm0hqm62giiy629llnh23f3hn")))

(define-public crate-arpx_job_parser-0.1.11 (c (n "arpx_job_parser") (v "0.1.11") (h "01shz9d54skkkmn7di4ayz2ghyrrc5ypnpv35ij650f0yw639y7m")))

(define-public crate-arpx_job_parser-0.1.12 (c (n "arpx_job_parser") (v "0.1.12") (h "0ls4q9xzzvhl25gi3mzsl9xp3rnjxdn0cfc3sy2606zc27ahgdy5")))

