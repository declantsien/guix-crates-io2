(define-module (crates-io ar rf arrform) #:use-module (crates-io))

(define-public crate-arrform-0.1.0 (c (n "arrform") (v "0.1.0") (h "0cyg36b12v96bpzj6r85s161py5ymqdh4zsyj63bdq6w4dkj8qkh")))

(define-public crate-arrform-0.1.1 (c (n "arrform") (v "0.1.1") (h "1rpl9qjw6f6mab4raqpqjysck0v5pyx307g8ffwq57awrip5dkz7")))

