(define-module (crates-io ar on aronis_gps_crate) #:use-module (crates-io))

(define-public crate-AronIS_GPS_Crate-0.1.0 (c (n "AronIS_GPS_Crate") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "0c06qf7plpdjl7dccs22vrrwsk5bvfyy29zcnpc7p9smjhs7cqd1") (y #t)))

(define-public crate-AronIS_GPS_Crate-0.1.1 (c (n "AronIS_GPS_Crate") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "1wc3naj0mid975dz3m203ryiikybggz9wghx88g13mimvhd2xb97") (y #t)))

(define-public crate-AronIS_GPS_Crate-0.1.2 (c (n "AronIS_GPS_Crate") (v "0.1.2") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "0zmqfcygpmqykkr8q6pdjic3rnq0p245xvyrr3sbzsq8hbvmy8zd") (y #t)))

(define-public crate-AronIS_GPS_Crate-0.1.3 (c (n "AronIS_GPS_Crate") (v "0.1.3") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "0d0kj0bsv67k11fb9jyjw99npp6imfbwbskxy43b5rizlbz6pa45") (y #t)))

(define-public crate-AronIS_GPS_Crate-0.1.4 (c (n "AronIS_GPS_Crate") (v "0.1.4") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "1m4c33giywv3xcrfllkrn4x0kbsyfiy9pww3mi54jw7c9pbplzvf") (y #t)))

(define-public crate-AronIS_GPS_Crate-0.1.5 (c (n "AronIS_GPS_Crate") (v "0.1.5") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "1b50a407p7kanginan4c86p0jb4cx2x9s6kq8gsrazx1p2bqbzfk")))

