(define-module (crates-io ar tl artlr_syntax) #:use-module (crates-io))

(define-public crate-artlr_syntax-0.1.0 (c (n "artlr_syntax") (v "0.1.0") (d (list (d (n "artlr_lex") (r "^0.1.1") (d #t) (k 0)))) (h "1w6grq6vq9z0170xlllw0r2vhiindd40v4zx47zz47m0820s2dwi")))

(define-public crate-artlr_syntax-0.2.0 (c (n "artlr_syntax") (v "0.2.0") (h "0pmbqd02122xm2cadf59ada4pmq4z2d5k8kvyn4krd5fvc64na64")))

(define-public crate-artlr_syntax-0.3.0 (c (n "artlr_syntax") (v "0.3.0") (h "0lap7gisv7h6s94dg2vwvbmhzfa7cwri4dkhb53r73fdj838ibnn")))

