(define-module (crates-io ar tl artlr_lex) #:use-module (crates-io))

(define-public crate-artlr_lex-0.1.0 (c (n "artlr_lex") (v "0.1.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "148dgpcwl0xr3xrx1ccm364r4ha8ddz2y30b3yk48m9sys05zm7y")))

(define-public crate-artlr_lex-0.1.1 (c (n "artlr_lex") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0vqcc47m975y2rypc3w9nykkzgxhmi3fi2i4rkx5km51gb7ds8hv")))

(define-public crate-artlr_lex-0.2.0 (c (n "artlr_lex") (v "0.2.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0bqfyap28pn4psn3hnrpqy0vvnk1ziqspbm77p0wkdl9malsqwi8")))

(define-public crate-artlr_lex-0.2.1 (c (n "artlr_lex") (v "0.2.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0z9q3xlfgqck480b5pml27higyvhjia41xii1ry59vmgm6djr4ql")))

(define-public crate-artlr_lex-0.2.2 (c (n "artlr_lex") (v "0.2.2") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1lwcmmrajfa1s57si9kk7wfhlmpmgbaxhv0kdpyhlmm172d84y9l")))

