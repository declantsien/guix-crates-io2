(define-module (crates-io ar ff arff) #:use-module (crates-io))

(define-public crate-arff-0.1.0 (c (n "arff") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1d90lwrmz7clsgxggbwrb8rcbl2asrml8piizfwf5zplv9ws3hlc")))

(define-public crate-arff-0.2.0 (c (n "arff") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1an5p9j51aicp3rwr4dakrg2i3smgdiwh1g5kdrr8rpkny64jhi8")))

(define-public crate-arff-0.2.1 (c (n "arff") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "00wg7ksc87wj8xkbk2v39gji47qfnz4f8njp3mv02qk2mlfxpphz")))

(define-public crate-arff-0.3.0 (c (n "arff") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0k8xicsbbv82wmadarwmz9d8ramx89yxafnrs0ws50ddxlxcvz7z")))

