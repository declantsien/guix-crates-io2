(define-module (crates-io a4 -c a4-core) #:use-module (crates-io))

(define-public crate-a4-core-0.0.3 (c (n "a4-core") (v "0.0.3") (d (list (d (n "heapless") (r "^0.7.8") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "std"))) (k 2)))) (h "07qxb5clx0hzaw2apa2qc0w7mwamh0kmiac0ckz00pbrmzk0n6rz") (f (quote (("std" "serde/std") ("default"))))))

(define-public crate-a4-core-0.0.4 (c (n "a4-core") (v "0.0.4") (d (list (d (n "heapless") (r "^0.7.8") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (f (quote ("use-std"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "std"))) (k 2)))) (h "1jxplvz2sqvmc3m233j18v5gi1iys6if2zwfmnp3p44w4rs9y1kr") (f (quote (("std" "serde/std") ("default"))))))

