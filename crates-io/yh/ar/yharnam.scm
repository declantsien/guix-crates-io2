(define-module (crates-io yh ar yharnam) #:use-module (crates-io))

(define-public crate-yharnam-0.0.1 (c (n "yharnam") (v "0.0.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "intl_pluralrules") (r "^7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1i3p57fhvxbcv2sqi2qqaimgnbxlvia87qin5y3ydhyh9b4mdixv")))

