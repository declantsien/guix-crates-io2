(define-module (crates-io yh y- yhy-email-encoding) #:use-module (crates-io))

(define-public crate-yhy-email-encoding-0.0.1 (c (n "yhy-email-encoding") (v "0.0.1") (d (list (d (n "base64") (r "^0.22") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1wzcxa43fhf3dsw23ysjjsyz111rmci9bw85wh2ij2sjj6vqbah2") (r "1.57")))

(define-public crate-yhy-email-encoding-0.0.2 (c (n "yhy-email-encoding") (v "0.0.2") (d (list (d (n "base64") (r "^0.22") (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0zj1mpdicarjkwmk98lnbbsbalnmb894cmxl0jbq465cmnbmf8y5") (r "1.57")))

