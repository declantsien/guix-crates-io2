(define-module (crates-io hm ap hmap-serde) #:use-module (crates-io))

(define-public crate-hmap-serde-0.1.0-alpha.1 (c (n "hmap-serde") (v "0.1.0-alpha.1") (d (list (d (n "frunk_core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13nmwfz8vr5n8078l17hfl1aiirwqksr6v4rg9yqshmy88arr327")))

(define-public crate-hmap-serde-0.1.0-alpha.2 (c (n "hmap-serde") (v "0.1.0-alpha.2") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("deref" "deref_mut"))) (k 0)) (d (n "frunk_core") (r "^0.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nsllxwm07cm4j2z6z2fv5adsfsz6b3yiw3zh94xwvsw1wnapxk2")))

(define-public crate-hmap-serde-0.1.0-alpha.3 (c (n "hmap-serde") (v "0.1.0-alpha.3") (d (list (d (n "frunk_core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12h9mzwb5l0aaq3a407rpqj7acvhdwf1km278arkns4pjymm6sad")))

(define-public crate-hmap-serde-0.1.0-alpha.4 (c (n "hmap-serde") (v "0.1.0-alpha.4") (d (list (d (n "frunk_core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bfzpccn3x8jgdzfv0p4g27kf6jg8v7l9l7cyf465l3ijs94lx2b")))

