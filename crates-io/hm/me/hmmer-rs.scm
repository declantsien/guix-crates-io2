(define-module (crates-io hm me hmmer-rs) #:use-module (crates-io))

(define-public crate-hmmer-rs-0.1.0 (c (n "hmmer-rs") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "libhmmer-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0bx68ri3p073jyhr0jy1b9f0mvldnpsnjpqj0m8y1jmmn2md3an0")))

(define-public crate-hmmer-rs-0.2.0 (c (n "hmmer-rs") (v "0.2.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "libhmmer-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "02gmp595rw9xxjghgrc78h3mfparqwdff4ghafz6xk4pfz2hsqgy")))

