(define-module (crates-io hm m- hmm-rs) #:use-module (crates-io))

(define-public crate-hmm-rs-0.0.1 (c (n "hmm-rs") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0rz9pmcsqx5mbb4glmk2k0bakl1h2mcf53rzv6hi807zndwhdl49")))

