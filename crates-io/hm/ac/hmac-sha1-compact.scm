(define-module (crates-io hm ac hmac-sha1-compact) #:use-module (crates-io))

(define-public crate-hmac-sha1-compact-0.1.0 (c (n "hmac-sha1-compact") (v "0.1.0") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0sif1si40dnmfdy1r2nh1r3zdf7wf9q9hvxk05wl476j57q2g878") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha1-compact-1.0.0 (c (n "hmac-sha1-compact") (v "1.0.0") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0mw6h4zank2hm9szq8fq97m8m3z0mqb2ks3wpf47pzz821fj211a") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha1-compact-1.1.0 (c (n "hmac-sha1-compact") (v "1.1.0") (d (list (d (n "digest") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "17xjryvbm4mmk6c1qy34hpbg47jw4d4k4agdmvfnc3vmwd3flk4k") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha1-compact-1.1.1 (c (n "hmac-sha1-compact") (v "1.1.1") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "056hi626xrkk12s46vkq266hz328k57bcnkwvk8zgwzdyvncy0yi") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("default"))))))

(define-public crate-hmac-sha1-compact-1.1.2 (c (n "hmac-sha1-compact") (v "1.1.2") (d (list (d (n "digest010") (r "^0.10.0") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1bldsaac8c4d5hpfza0qlrljzxqan0ga7yd59mk7nwv89fqnhsd7") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("default"))))))

(define-public crate-hmac-sha1-compact-1.1.3 (c (n "hmac-sha1-compact") (v "1.1.3") (d (list (d (n "digest010") (r "^0.10.6") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1frr8bam0m8c42qvvh5hchzg46a2x96260fad0xhrqkq00549qh5") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("default"))))))

(define-public crate-hmac-sha1-compact-1.1.4 (c (n "hmac-sha1-compact") (v "1.1.4") (d (list (d (n "digest010") (r "^0.10.7") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "19w4iiwrprcnvq3k2gkv6xm9b11alda4w9l7vvya6bvkxh2x9yfz") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("default"))))))

