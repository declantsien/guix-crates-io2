(define-module (crates-io hm ac hmac) #:use-module (crates-io))

(define-public crate-hmac-0.0.0 (c (n "hmac") (v "0.0.0") (h "1jw4qg141xmc2vw50k3l6h8frmrj8a7ly2ilm9h2dc8gg9299yry") (y #t)))

(define-public crate-hmac-0.0.1 (c (n "hmac") (v "0.0.1") (h "07rd0y35rxlpvc578nvg951canpsiqiklzrqy8gjl2l5l3q09a76") (y #t)))

(define-public crate-hmac-0.1.0 (c (n "hmac") (v "0.1.0") (d (list (d (n "crypto-mac") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.4") (d #t) (k 2)))) (h "0yqcx08s9sgj8vvxzry9bzr7ryc56wbfwrwwppdbmzvkb2z399wi") (y #t)))

(define-public crate-hmac-0.1.1 (c (n "hmac") (v "0.1.1") (d (list (d (n "crypto-mac") (r "^0.3") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.1") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.5") (d #t) (k 2)))) (h "1zw3if5nvgz1calkqi1msjg9h8yd234c33cnsvli2ixs8ybamddx") (y #t)))

(define-public crate-hmac-0.3.1 (c (n "hmac") (v "0.3.1") (d (list (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.5") (d #t) (k 2)) (d (n "sha2") (r "^0.6") (d #t) (k 2)))) (h "0l2sx2y51v8kwpfshgidwqbgcxjnbri2h61cn9nsz38nrs7xkcr5") (y #t)))

(define-public crate-hmac-0.1.2 (c (n "hmac") (v "0.1.2") (d (list (d (n "crypto-mac") (r "^0.3") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.1") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.5") (d #t) (k 2)))) (h "1f98g60q9mvsdcdcfsq3byr243ghqqln2dba9gaipmsn5hls0450") (y #t)))

(define-public crate-hmac-0.4.0 (c (n "hmac") (v "0.4.0") (d (list (d (n "crypto-mac") (r "^0.3") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.1") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.5") (d #t) (k 2)))) (h "11c6bq3dwyr8x96kdlhv02qr4dk77bvnzkblzqb6ldfbd2mn1y60") (y #t)))

(define-public crate-hmac-0.4.1 (c (n "hmac") (v "0.4.1") (d (list (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.5") (d #t) (k 2)) (d (n "sha2") (r "^0.6") (d #t) (k 2)))) (h "1npvxjg9zs7ylhv3myglqcbyy7c415dprgs5vh6qng0fl000wjcz") (y #t)))

(define-public crate-hmac-0.4.2 (c (n "hmac") (v "0.4.2") (d (list (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.5") (d #t) (k 2)) (d (n "sha2") (r "^0.6") (d #t) (k 2)))) (h "0amfn9wggxapyva1i0j7wldy9chj487cxal4wldwmid078bg84vs") (y #t)))

(define-public crate-hmac-0.5.0 (c (n "hmac") (v "0.5.0") (d (list (d (n "crypto-mac") (r "^0.5") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.5.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "0ppm927wsczkkvybj0y7r2w2lprvy5kf5h31qxyrvnbrhnqbvws4") (y #t)))

(define-public crate-hmac-0.6.0 (c (n "hmac") (v "0.6.0") (d (list (d (n "crypto-mac") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "1qdlmamqjgychb4xyk46pm03wgwrw6r0g716hz4q70vvm6c26zhd") (y #t)))

(define-public crate-hmac-0.6.1 (c (n "hmac") (v "0.6.1") (d (list (d (n "crypto-mac") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "0q7xnz3z1q6nnxg8avvz3fzpv6sld0yph5f6iv79127myrrja2lq") (y #t)))

(define-public crate-hmac-0.6.2 (c (n "hmac") (v "0.6.2") (d (list (d (n "crypto-mac") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "11b223dd5pip9v3ldrjjlhwa0ykwc3dyr7hxdc9pv8ckh0v9bf7g") (y #t)))

(define-public crate-hmac-0.6.3 (c (n "hmac") (v "0.6.3") (d (list (d (n "crypto-mac") (r "^0.6") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "md-5") (r "^0.7") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "16prsqz0395fbrmh7f9j812pmqzmn2dpwmxv3sh1qqq6r4x1ngkk")))

(define-public crate-hmac-0.7.0 (c (n "hmac") (v "0.7.0") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.8") (k 2)) (d (n "sha2") (r "^0.8") (k 2)))) (h "0wcph449jmnm3kiryzqxf9wkqdnkal9gg1jz684g4s9mcc4aj9zi")))

(define-public crate-hmac-0.7.1 (c (n "hmac") (v "0.7.1") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.8") (k 2)) (d (n "sha2") (r "^0.8") (k 2)))) (h "15cnwpssp2n1kdm9x7abir67f2hp3q6rdfj1mcck3hm4rmj5xjsx")))

(define-public crate-hmac-0.8.0 (c (n "hmac") (v "0.8.0") (d (list (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)))) (h "1np0rkk96x1g2aldr5jvabvsqz3hvs3kazx24hiwq4b8sq5mhyxq")))

(define-public crate-hmac-0.8.1 (c (n "hmac") (v "0.8.1") (d (list (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)))) (h "0h48wc7iysh4xd6ci4prh8bb7nszijrh9w3blaaq8a6cilk8hs0j")))

(define-public crate-hmac-0.9.0 (c (n "hmac") (v "0.9.0") (d (list (d (n "crypto-mac") (r "^0.9") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)))) (h "1zqdhhf0m70lncvbm5077cn85800q2qzgf325m82rv1mpffnvbny") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-hmac-0.10.0 (c (n "hmac") (v "0.10.0") (d (list (d (n "crypto-mac") (r "^0.10") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)))) (h "0plh47p6ryab7sc2i49jg4l8apzasw48axr37ba2c2fvai72dvzf") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-hmac-0.10.1 (c (n "hmac") (v "0.10.1") (d (list (d (n "crypto-mac") (r "^0.10") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)))) (h "058yxq54x7xn0gk2vy9bl51r32c9z7qlcl2b80bjh3lk3rmiqi61") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-hmac-0.11.0 (c (n "hmac") (v "0.11.0") (d (list (d (n "crypto-mac") (r "^0.11") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (f (quote ("dev"))) (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)) (d (n "streebog") (r "^0.9") (k 2)))) (h "16z61aibdg4di40sqi4ks2s4rz6r29w4sx4gvblfph3yxch26aia") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-hmac-0.12.0 (c (n "hmac") (v "0.12.0") (d (list (d (n "digest") (r "^0.10") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (k 2)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "streebog") (r "^0.10") (k 2)))) (h "18nnjkvvpkjw6ppcyijysgxmr4a7knd98msb6vgy4b3z7qgi7jnx") (f (quote (("std" "digest/std") ("reset"))))))

(define-public crate-hmac-0.12.1 (c (n "hmac") (v "0.12.1") (d (list (d (n "digest") (r "^0.10.3") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (k 2)) (d (n "sha-1") (r "^0.10") (k 2)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "streebog") (r "^0.10") (k 2)))) (h "0pmbr069sfg76z7wsssfk5ddcqd9ncp79fyz6zcm6yn115yc6jbc") (f (quote (("std" "digest/std") ("reset"))))))

(define-public crate-hmac-0.13.0-pre.0 (c (n "hmac") (v "0.13.0-pre.0") (d (list (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "=0.11.0-pre.0") (k 2)) (d (n "sha1") (r "=0.11.0-pre.0") (k 2)) (d (n "sha2") (r "=0.11.0-pre.0") (k 2)) (d (n "streebog") (r "=0.11.0-pre.0") (k 2)))) (h "0kxjk74p3cnkm4wwxb0phx9kw7ry56hlmhjpmx238p3l78cb0b72") (f (quote (("std" "digest/std") ("reset")))) (r "1.71")))

(define-public crate-hmac-0.13.0-pre.1 (c (n "hmac") (v "0.13.0-pre.1") (d (list (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "=0.11.0-pre.1") (k 2)) (d (n "sha1") (r "=0.11.0-pre.1") (k 2)) (d (n "sha2") (r "=0.11.0-pre.1") (k 2)) (d (n "streebog") (r "=0.11.0-pre.1") (k 2)))) (h "0sh2jd41cp1byw18f3qkxs321c6am33n8phy68rp19fsqjrbvn8s") (f (quote (("std" "digest/std") ("reset")))) (r "1.71")))

(define-public crate-hmac-0.13.0-pre.2 (c (n "hmac") (v "0.13.0-pre.2") (d (list (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "=0.11.0-pre.2") (k 2)) (d (n "sha1") (r "=0.11.0-pre.2") (k 2)) (d (n "sha2") (r "=0.11.0-pre.2") (k 2)) (d (n "streebog") (r "=0.11.0-pre.2") (k 2)))) (h "0y3n04bzzpxgpkyr1a2n1rsxrarj4npwja3rffi6in8j3y4h3b0z") (f (quote (("std" "digest/std") ("reset")))) (r "1.72")))

(define-public crate-hmac-0.13.0-pre.3 (c (n "hmac") (v "0.13.0-pre.3") (d (list (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("mac"))) (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "md-5") (r "=0.11.0-pre.3") (k 2)) (d (n "sha1") (r "=0.11.0-pre.3") (k 2)) (d (n "sha2") (r "=0.11.0-pre.3") (k 2)) (d (n "streebog") (r "=0.11.0-pre.3") (k 2)))) (h "1knmjxw445zc51a38zgb29qhvdxyfyqyanc97vnk5qsyg6h91mzz") (f (quote (("std" "digest/std") ("reset")))) (r "1.72")))

