(define-module (crates-io hm ac hmac-sha256) #:use-module (crates-io))

(define-public crate-hmac-sha256-0.1.0 (c (n "hmac-sha256") (v "0.1.0") (h "0dqa6hfcdgml7psq3hrjmn42hsixs1mxyl8g564npxhb3hc0pb87")))

(define-public crate-hmac-sha256-0.1.1 (c (n "hmac-sha256") (v "0.1.1") (h "0qlf2c0i1bpqf2pg39hj3akx4p3wmw7pygwrjbhbg2n7cmxh96fa")))

(define-public crate-hmac-sha256-0.1.2 (c (n "hmac-sha256") (v "0.1.2") (h "0psjbwf77wv4ybbhpfljlmzpdh0sfrl3vgnb7gg1ndj2ch0jd5m6")))

(define-public crate-hmac-sha256-0.1.3 (c (n "hmac-sha256") (v "0.1.3") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1iq0zdyr6mnz3fraf0nlmhc71b58jkkwh4k1bj6v07cb1m7nygyk") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha256-0.1.4 (c (n "hmac-sha256") (v "0.1.4") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "19davxcwynn0i4r0qbpikw8ak0xxz5ysipwhcbs3b4yngv9m1055") (f (quote (("traits" "digest") ("default" "traits"))))))

(define-public crate-hmac-sha256-0.1.5 (c (n "hmac-sha256") (v "0.1.5") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "08zqgbgpzj7zvg2m1drhjla5mlcggshks25s2gl0jfq56s8bv9zs") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha256-0.1.6 (c (n "hmac-sha256") (v "0.1.6") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "08rlmnys40wm6gml5mimv81z7ss92ja6d5s27600jrjica63d791") (f (quote (("traits" "digest") ("default"))))))

(define-public crate-hmac-sha256-0.1.7 (c (n "hmac-sha256") (v "0.1.7") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1ik76p0raxgd2rzqadpvclbnw1n50zw0pd5ag4jm28b5aqg5gp5w") (f (quote (("traits" "digest") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.0.0 (c (n "hmac-sha256") (v "1.0.0") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0522wpmgafn2njvdxm9137ws7yjjkjrhn0zliqlmmdgh37rf6d33") (f (quote (("traits" "digest") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.0 (c (n "hmac-sha256") (v "1.1.0") (d (list (d (n "digest") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0hcrrc5qzhgxh4v94faqiavhf93zj8sqryk8q3q4d7wr1z3304il") (f (quote (("traits" "digest") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.1 (c (n "hmac-sha256") (v "1.1.1") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "0fv1b51yqk73bkwnpiawrv915hhxkx5w6lxwq8cbwg7hpfxmiwsb") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.2 (c (n "hmac-sha256") (v "1.1.2") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1xrmn1pj0w0dq0xr48w9bpdwsr1v5x9klbl31h5n3wp49nvqapml") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.3 (c (n "hmac-sha256") (v "1.1.3") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "03lnn7agnyhn639hr89ns6ah99cvnd2l0nm3z1x9bzs8wzz6bx17") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.4 (c (n "hmac-sha256") (v "1.1.4") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "11adkbv3m9725k7yy8mk8nzp4irzibbnc02pxkri8lzfb2xdnagx") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.5 (c (n "hmac-sha256") (v "1.1.5") (d (list (d (n "digest010") (r "^0.10.0") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1bvm118wxmldgffv6yg8dhmn3b8zxvgqp7x221ylb5wbp57vvv23") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.6 (c (n "hmac-sha256") (v "1.1.6") (d (list (d (n "digest010") (r "^0.10.4") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1zhvszjzmi9q4prl2sw4mh9rks917j5rixnm9jmdscfbma8n0wzw") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

(define-public crate-hmac-sha256-1.1.7 (c (n "hmac-sha256") (v "1.1.7") (d (list (d (n "digest010") (r "^0.10.7") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "0dapmabsj2mvblwjy64h518frj1cvk468kr5awayr3q172dyd21n") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("opt_size") ("default"))))))

