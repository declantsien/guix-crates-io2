(define-module (crates-io hm ac hmac-sha) #:use-module (crates-io))

(define-public crate-hmac-sha-0.2.0 (c (n "hmac-sha") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "07xj3mf3v1wx3kaz5pg4gj1qkd1wagiv48x0pcaskdzsizma2p16")))

(define-public crate-hmac-sha-0.2.1 (c (n "hmac-sha") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0a7dwf4vgj9zbzji0gfaqdgzihsp3sjmnz27y2x9kxifx7jv1zmp")))

(define-public crate-hmac-sha-0.4.0 (c (n "hmac-sha") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1yasmr1n84a4cgr52d5q955fffm9xz89ym21zqackimppfbgnhqm")))

(define-public crate-hmac-sha-0.4.1 (c (n "hmac-sha") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "11s9lh39qkcg1dzb4sgxn8kbap29km76r25mm3r7bmzf9dka5c74")))

(define-public crate-hmac-sha-0.4.2 (c (n "hmac-sha") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "101bglhdc74cf20wbam0f078ha7vyiv72qd81qxr6fl7nhhgvabv")))

(define-public crate-hmac-sha-0.4.3 (c (n "hmac-sha") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "16jwp7sk23jvq1v5646c8gw1jfvg8ng2d3jy2dhqps1p48ckwllv")))

(define-public crate-hmac-sha-0.4.4 (c (n "hmac-sha") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "13brb5kn9sxibw462ajw6qrfr9s72dkknrqshq75hy4kq66n2hy4")))

(define-public crate-hmac-sha-0.4.5 (c (n "hmac-sha") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac") (r "^0.11.0") (k 0)) (d (n "sha-1") (r "^0.9.7") (k 0)) (d (n "sha2") (r "^0.9.5") (k 0)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "1cr45w7anh19kiplvjdiw4qb41mlg14ifjc9pwpx595v6giazmpa")))

(define-public crate-hmac-sha-0.5.0 (c (n "hmac-sha") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "heapless") (r "^0.7.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac") (r "^0.11.0") (k 0)) (d (n "sha-1") (r "^0.9.7") (k 0)) (d (n "sha2") (r "^0.9.5") (k 0)) (d (n "sha3") (r "^0.9.1") (k 0)))) (h "1wpbccwzsh8lzdmqj60cmfpjfgn84ra2cmkgv8yaj31fh57sxbwj")))

(define-public crate-hmac-sha-0.6.0 (c (n "hmac-sha") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9") (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (k 0)) (d (n "sha-1") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)) (d (n "sha3") (r "^0.9") (k 2)))) (h "1fd0axr1minnvsd5i99xs9q2s1c67bgz2yy6phpfb0lc2syrjdyn")))

(define-public crate-hmac-sha-0.6.1 (c (n "hmac-sha") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.9") (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac") (r "^0.11") (k 0)) (d (n "sha-1") (r "^0.9") (k 2)) (d (n "sha2") (r "^0.9") (k 2)) (d (n "sha3") (r "^0.9") (k 2)))) (h "0blmay9gbrs2m3bsjdbn5y98wk1qnfx87l3f9azqvmzqydyc1n9m")))

