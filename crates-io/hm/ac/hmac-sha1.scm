(define-module (crates-io hm ac hmac-sha1) #:use-module (crates-io))

(define-public crate-hmac-sha1-0.1.1 (c (n "hmac-sha1") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "sha1") (r "^0.2.0") (d #t) (k 0)))) (h "101j5jyd3ps1ihwnh0ahkxd3nk7p6cszxmwaf9jlwzmq5zdpkp55")))

(define-public crate-hmac-sha1-0.1.2 (c (n "hmac-sha1") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "sha1") (r "^0.2.0") (d #t) (k 0)))) (h "0rqk5k56gx1d48fwcz446p0p910fi8jncg314mp976q7ffgxg677")))

(define-public crate-hmac-sha1-0.1.3 (c (n "hmac-sha1") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "sha1") (r "^0.2.0") (d #t) (k 0)))) (h "08k7aylc0v8x3abmxn3h73dkad3anfq2i94xk2mjrf4linnkycz1")))

(define-public crate-hmac-sha1-0.2.1 (c (n "hmac-sha1") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "0vsb6a32cs4icn8nj4wpvlgi072lfflv2vy4i1aj055p47agfnj0")))

(define-public crate-hmac-sha1-0.2.2 (c (n "hmac-sha1") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "125bkbjvfnqzian1x9sxfiq3bnj2klmvpvlinszj0isxkrdxl1bb")))

