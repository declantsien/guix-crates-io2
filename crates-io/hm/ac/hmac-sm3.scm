(define-module (crates-io hm ac hmac-sm3) #:use-module (crates-io))

(define-public crate-hmac-sm3-0.1.0 (c (n "hmac-sm3") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sm3") (r "^0.4.2") (d #t) (k 0)))) (h "0rj7rwq8hlgzqah4wpxb0yvibk41p8wajfl4xhkf1l3y82l4kld2")))

(define-public crate-hmac-sm3-0.1.1 (c (n "hmac-sm3") (v "0.1.1") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac-sha1") (r "^0.1.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 2)) (d (n "sm3") (r "^0.4.2") (d #t) (k 0)))) (h "10n1w5fcy5gg87pbrkm5a9z2z3bgprwricq074mpmhv61vlnlvz9")))

