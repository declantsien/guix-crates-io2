(define-module (crates-io hm ac hmac-predicate) #:use-module (crates-io))

(define-public crate-hmac-predicate-0.1.0 (c (n "hmac-predicate") (v "0.1.0") (d (list (d (n "form_urlencoded") (r ">=1") (d #t) (k 0)) (d (n "hex") (r ">=0") (d #t) (k 0)) (d (n "hmac") (r ">=0") (d #t) (k 0)) (d (n "http") (r ">=0") (d #t) (k 0)) (d (n "sha2") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)) (d (n "tower") (r ">=0") (f (quote ("filter"))) (d #t) (k 0)))) (h "0ln4ibc5n55y03i9filv91bd75gcyr1268r64s3i4y5yhx9nsahp")))

(define-public crate-hmac-predicate-0.2.0 (c (n "hmac-predicate") (v "0.2.0") (d (list (d (n "form_urlencoded") (r ">=1") (d #t) (k 0)) (d (n "hex") (r ">=0") (d #t) (k 0)) (d (n "hmac") (r ">=0") (d #t) (k 0)) (d (n "http") (r ">=0") (d #t) (k 0)) (d (n "sha2") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)) (d (n "tower") (r ">=0") (f (quote ("filter"))) (d #t) (k 0)))) (h "1gxpdgwsrp73qd1b16hc7jl9i2zyhvgxz2a64c5wp7w7wmyp3d5n")))

(define-public crate-hmac-predicate-0.3.0 (c (n "hmac-predicate") (v "0.3.0") (d (list (d (n "form_urlencoded") (r ">=1") (d #t) (k 0)) (d (n "hex") (r ">=0") (d #t) (k 0)) (d (n "hmac") (r ">=0") (d #t) (k 0)) (d (n "http") (r ">=0") (d #t) (k 0)) (d (n "sha2") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)) (d (n "tower") (r ">=0") (f (quote ("filter"))) (d #t) (k 0)))) (h "04rrfq93p178l0a1v2l4dl4kas9i23ndh4dqsl4ndv8q38hj2j36")))

(define-public crate-hmac-predicate-0.3.1 (c (n "hmac-predicate") (v "0.3.1") (d (list (d (n "form_urlencoded") (r ">=1") (d #t) (k 0)) (d (n "hex") (r ">=0") (d #t) (k 0)) (d (n "hmac") (r ">=0") (d #t) (k 0)) (d (n "http") (r ">=0") (d #t) (k 0)) (d (n "sha2") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)) (d (n "tower") (r ">=0") (f (quote ("filter"))) (d #t) (k 0)))) (h "0sbkfr3yv813qb0rammb5njkcr50ig72dkvdfaaky8c9pdibgzca")))

(define-public crate-hmac-predicate-0.3.2 (c (n "hmac-predicate") (v "0.3.2") (d (list (d (n "form_urlencoded") (r ">=1") (d #t) (k 0)) (d (n "hex") (r ">=0") (d #t) (k 0)) (d (n "hmac") (r ">=0") (d #t) (k 0)) (d (n "http") (r ">=0") (d #t) (k 0)) (d (n "sha2") (r ">=0") (d #t) (k 0)) (d (n "thiserror") (r ">=1") (d #t) (k 0)) (d (n "tower") (r ">=0") (f (quote ("filter"))) (d #t) (k 0)))) (h "13gnxq5qb75r003byqy60kmdm7jf75ymd7c2nh53ivy8jnxpg8lx")))

