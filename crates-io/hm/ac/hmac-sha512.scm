(define-module (crates-io hm ac hmac-sha512) #:use-module (crates-io))

(define-public crate-hmac-sha512-0.1.2 (c (n "hmac-sha512") (v "0.1.2") (h "1249qbm6i0lk3rjj5g9bpy7f0j7yw74hjagzvfmabdz7ny0i5fzi")))

(define-public crate-hmac-sha512-0.1.3 (c (n "hmac-sha512") (v "0.1.3") (h "1i8cwbq2g8gambwqgkp1dbhrspq11nb94glgng6yhlfvwxxqpq3d")))

(define-public crate-hmac-sha512-0.1.4 (c (n "hmac-sha512") (v "0.1.4") (h "10dpdxxbivn0lsppihcdc3lwns5300h8gg3f8d3g68bv261r3ria")))

(define-public crate-hmac-sha512-0.1.5 (c (n "hmac-sha512") (v "0.1.5") (h "0d12rv330zp8vxpprbyjzcabnbw8b3jfcyj4s1lhqcc2hdnfbyfv")))

(define-public crate-hmac-sha512-0.1.6 (c (n "hmac-sha512") (v "0.1.6") (h "1fr9q2dqfbli9pqwrwwjvp4kzmyx7fh1knif4i6mn90aa8l0dxb1")))

(define-public crate-hmac-sha512-0.1.7 (c (n "hmac-sha512") (v "0.1.7") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1pgl4rhm77189ra97ncr0cdck1hid7shjgrxwkcmgzfd5vnp9igw") (f (quote (("traits" "digest") ("sha384") ("default" "sha384"))))))

(define-public crate-hmac-sha512-0.1.8 (c (n "hmac-sha512") (v "0.1.8") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1bkr4ymhddf7a8zx6hyrpbgknipryckh78dvygjkpnbx5qsjiyia") (f (quote (("traits" "digest") ("sha384") ("default" "sha384"))))))

(define-public crate-hmac-sha512-0.1.9 (c (n "hmac-sha512") (v "0.1.9") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "10skv9isvx54wkd70pq9vkl3cyw42v43042lk6hx0qz6gikhds3p") (f (quote (("traits" "digest") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.0.0 (c (n "hmac-sha512") (v "1.0.0") (d (list (d (n "digest") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1xahmqg40xigaczazasgdbcsgkhbyx9hjx58a2kzpk9fmdcxazqq") (f (quote (("traits" "digest") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.0 (c (n "hmac-sha512") (v "1.1.0") (d (list (d (n "digest") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "03y7k85gkx3x8ir6h82r1pf82zp7sqrfymkbc4lz8yz3qwvfhx20") (f (quote (("traits" "digest") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.1 (c (n "hmac-sha512") (v "1.1.1") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "043vsnmljbvy8z16hcl7rvh1yd7n8d9q4fh96wh2j3q7v1vf0b3b") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.2 (c (n "hmac-sha512") (v "1.1.2") (d (list (d (n "digest010") (r "^0.10.0") (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "0r8amlfm3l644hbqqjqmb69snw2p74frjmhhl9zhny7ivw1b0a59") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.3 (c (n "hmac-sha512") (v "1.1.3") (d (list (d (n "digest010") (r "^0.10.0") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "1sy1xgx2rh632s66rh4lp053pf7y6jm3q9dq3zpr0d09qchbimpl") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.4 (c (n "hmac-sha512") (v "1.1.4") (d (list (d (n "digest010") (r "^0.10.4") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "0lfi6kvxwnm6j5m7cfm6w3421985nr8yb4awpilicrj0c0zrq32j") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("sha384") ("opt_size") ("default" "sha384"))))))

(define-public crate-hmac-sha512-1.1.5 (c (n "hmac-sha512") (v "1.1.5") (d (list (d (n "digest010") (r "^0.10.4") (f (quote ("oid"))) (o #t) (d #t) (k 0) (p "digest")) (d (n "digest09") (r "^0.9.0") (o #t) (d #t) (k 0) (p "digest")))) (h "12pp9qdf0f62lgwcb8h1xnvlb1pmkgqgjf5rzaiqkrdsar31zkp4") (f (quote (("traits09" "digest09") ("traits010" "digest010") ("traits" "traits09" "traits010") ("sha384") ("opt_size") ("default" "sha384"))))))

