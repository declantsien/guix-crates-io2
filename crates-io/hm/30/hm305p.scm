(define-module (crates-io hm #{30}# hm305p) #:use-module (crates-io))

(define-public crate-hm305p-0.1.0 (c (n "hm305p") (v "0.1.0") (d (list (d (n "serialport") (r "^4.0") (d #t) (k 0)))) (h "09zig4aql4lv6nj1cy1979j1pvyc0njqhq63vqq9hk3hj8vzfbwh")))

(define-public crate-hm305p-0.2.0 (c (n "hm305p") (v "0.2.0") (d (list (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "163fjxiacfr7xm4h1jm670piy2y28g4dpcx4vs2z99avfa8049lx")))

(define-public crate-hm305p-0.3.0 (c (n "hm305p") (v "0.3.0") (d (list (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gkrzyf6fh2ajda6rf03awmnfj88i5qhdkl3zx4ryn6p5g0zcwxn")))

(define-public crate-hm305p-1.0.0 (c (n "hm305p") (v "1.0.0") (d (list (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05yg68jcvvq393cliyhpqw28jrr8271knpgrsnijmwi0jpac4sy2")))

(define-public crate-hm305p-1.1.0 (c (n "hm305p") (v "1.1.0") (d (list (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fzjhw437l16555zyirqivqgak4v4nfq9z4iyiva8xmnk7l3xlzs")))

(define-public crate-hm305p-1.1.1 (c (n "hm305p") (v "1.1.1") (d (list (d (n "serialport") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "070jwfdpwmsjiy6aa6f171dj10v1czj508dz29dkmac1lj6188pb")))

