(define-module (crates-io hm s- hms-db) #:use-module (crates-io))

(define-public crate-hms-db-0.1.0 (c (n "hms-db") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("chrono" "returning_clauses_for_sqlite_3_35" "sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.1.0") (d #t) (k 0)) (d (n "hms-common") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0ljghsxv22hpb37xs1bkpfjrm05rri5aapwvxi9l4rgm0qzdgsj2")))

