(define-module (crates-io hm mm hmmmm) #:use-module (crates-io))

(define-public crate-hmmmm-0.1.0 (c (n "hmmmm") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m15zi8q9xpv5vh6gxamj3qnpzv1rpv6763ph4lwhm8fq1vh8sdx")))

(define-public crate-hmmmm-0.1.1 (c (n "hmmmm") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmmmm_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hmmmm_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1znmz5lanr6l9maxnmhrv0yjqip55xs5lp5zq53sgfs85iq7r2bq") (f (quote (("derive" "hmmmm_derive"))))))

(define-public crate-hmmmm-0.1.2 (c (n "hmmmm") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "hmmmm_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hmmmm_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c658v8myd921iccyg07nd4qga6h337ihzyvklx4lbk20ycdm5k6") (f (quote (("derive" "hmmmm_derive"))))))

