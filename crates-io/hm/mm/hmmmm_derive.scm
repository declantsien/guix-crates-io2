(define-module (crates-io hm mm hmmmm_derive) #:use-module (crates-io))

(define-public crate-hmmmm_derive-0.1.0 (c (n "hmmmm_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1yidf6rzhfb4nnvw3p0a1s53w1qxkf7y5hbqzv8c0nykskxv6qfq")))

