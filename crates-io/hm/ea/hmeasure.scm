(define-module (crates-io hm ea hmeasure) #:use-module (crates-io))

(define-public crate-hmeasure-0.0.1 (c (n "hmeasure") (v "0.0.1") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0rq8cfvg8gkyzn8qzfamkmm8kpk8ic3k1x79iccq9pxmd0bj68i9")))

(define-public crate-hmeasure-0.0.2 (c (n "hmeasure") (v "0.0.2") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0k4nkpy7xvmg94m35vj3ry0sm8vkfyvv17mr1a8w1p9lmhqbbbbp")))

(define-public crate-hmeasure-0.0.3 (c (n "hmeasure") (v "0.0.3") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0nimd7lcy71h41rh35ipfn3r7p6ynmvnbfbsg2qyr9w2chsim9d6")))

(define-public crate-hmeasure-0.0.4 (c (n "hmeasure") (v "0.0.4") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1aadkbrpgrq652k447dpqwr09pzlx1dbpspv3cfm584qg4a31xbd")))

(define-public crate-hmeasure-0.0.5 (c (n "hmeasure") (v "0.0.5") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0mm3qzvsq8ivr733asdc0qs5r6ysrm8b48k50r4x55zg9x2g96cb")))

(define-public crate-hmeasure-0.0.6 (c (n "hmeasure") (v "0.0.6") (d (list (d (n "array2d") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "quadrature") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "00g5b93dl90qvn2p1ifg3wvjk5ba5a9jmid197703cplkmv0ixfl")))

