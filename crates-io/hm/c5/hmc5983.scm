(define-module (crates-io hm c5 hmc5983) #:use-module (crates-io))

(define-public crate-hmc5983-0.1.2 (c (n "hmc5983") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "16asfjqygmhy7y83yv978rrd2gr9g1qanisgv06dd9in9wx6d9bb") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-hmc5983-0.1.3 (c (n "hmc5983") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "12da75f9gn1ljdsvbf6apcndc63fx395w6a4cyh6a4wqz83zzrn0") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

