(define-module (crates-io hm c5 hmc5883-async) #:use-module (crates-io))

(define-public crate-hmc5883-async-0.1.3 (c (n "hmc5883-async") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3.2") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "02ydgv4kb26ckqdi1m8hxzi5k08hxg5i5xdp1vvcny68hz1fq5ny") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

