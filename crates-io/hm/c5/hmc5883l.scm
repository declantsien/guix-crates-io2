(define-module (crates-io hm c5 hmc5883l) #:use-module (crates-io))

(define-public crate-hmc5883l-1.0.0 (c (n "hmc5883l") (v "1.0.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "1pax72pjwpnip7vhz47qa9sl8q1ci878lz3bwzimh1dml69nhlqa")))

(define-public crate-hmc5883l-1.0.1 (c (n "hmc5883l") (v "1.0.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "0i4hb9v2521611khddvyyh3zcpl03hpi2lppbi07kif1sp2pszzq")))

(define-public crate-hmc5883l-1.0.2 (c (n "hmc5883l") (v "1.0.2") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "046p530mf8fkdr2b44zp6h303w1vc87n9m25vlk3n8q5ksnm95yc")))

