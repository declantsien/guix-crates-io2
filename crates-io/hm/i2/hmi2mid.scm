(define-module (crates-io hm i2 hmi2mid) #:use-module (crates-io))

(define-public crate-hmi2mid-0.1.0 (c (n "hmi2mid") (v "0.1.0") (h "1y0s2rhksk5kplkhx3rpq9m2paiih08djrqvl380w7v2yg6yh96a")))

(define-public crate-hmi2mid-0.1.1 (c (n "hmi2mid") (v "0.1.1") (h "010ybxqha2lzbs6svjkcdbk8ygl00c0ma3phfb2qhkhqyynmc4ja")))

