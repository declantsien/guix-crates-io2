(define-module (crates-io hm de hmdee) #:use-module (crates-io))

(define-public crate-hmdee-0.1.0 (c (n "hmdee") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2") (d #t) (k 0)) (d (n "hmdee_core") (r "^0.1.0") (d #t) (k 0)) (d (n "psvr") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1ffrwh92zvx1avk5925fmr5vjjp1wnxa5hky0xbr1vigfrcvmqnf") (f (quote (("default" "psvr"))))))

