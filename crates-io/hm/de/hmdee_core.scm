(define-module (crates-io hm de hmdee_core) #:use-module (crates-io))

(define-public crate-hmdee_core-0.1.0 (c (n "hmdee_core") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)))) (h "0pcv3yy06v0kcxazxxk8k267i0p89bnf9dqq1bvyy39y8sb2310s")))

