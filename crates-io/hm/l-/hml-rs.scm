(define-module (crates-io hm l- hml-rs) #:use-module (crates-io))

(define-public crate-hml-rs-0.0.2 (c (n "hml-rs") (v "0.0.2") (d (list (d (n "utf8-read") (r "^0.4.0") (d #t) (k 0)))) (h "12lydak15x0s0hhi9r0yq5hfna8w6aiqqwshxlrkrz2grjzs5al1")))

(define-public crate-hml-rs-0.3.0 (c (n "hml-rs") (v "0.3.0") (d (list (d (n "clap") (r "~4.4") (o #t) (d #t) (k 0)) (d (n "lexer-rs") (r "^0.0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "utf8-read") (r "^0.4.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1xg0fsknc2wiq1vahppnkdv6kvyp7cjq9hwvzq513kd13hd0v5k8") (f (quote (("default" "xml")))) (s 2) (e (quote (("xml" "dep:clap" "dep:xml-rs"))))))

