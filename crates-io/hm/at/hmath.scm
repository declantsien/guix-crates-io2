(define-module (crates-io hm at hmath) #:use-module (crates-io))

(define-public crate-hmath-0.1.0 (c (n "hmath") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0gdjrwzb2n04xx66hs7g0vf2fsrz36phgs7mrqsncnigrcjnav0c") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.1 (c (n "hmath") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "169i89ii3dsbwqhw4xammvbg710w5b73qs8kg2yygjqf0v2g59mh") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.2 (c (n "hmath") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0vcpzfr4d3q055wvwkc3b6rgwq74z76h3rrrqf0szazrfzxsrl8r") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.3 (c (n "hmath") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1ks8xaivlmmyz7hzy8m84xkc7n2z6kl2kran93xcb3d5yzzcv926") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.4 (c (n "hmath") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0gb9mcxyjf9ncaim8c8bj5k1rhnfv6d0as258ai35f222b76fmw6") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.5 (c (n "hmath") (v "0.1.5") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1gva15gjgy9a3rv7bh3iqkv13k5w7g7j3dvidl160jhxy37avrp0") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.6 (c (n "hmath") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0y3v8d2pylsfkj1946plvkb05lq9ivivx0qnnqx4a762l1hbm8lx") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.7 (c (n "hmath") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1ilwazyd6a1lsr7xvw4x41k0ymjv3pv5vn50cxqjjr6c65jvpijh") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.8 (c (n "hmath") (v "0.1.8") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1b56h6h90afkjmkkbif9znsvsi8fcr3iy72dzivyafprnj6gl5yl") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.9 (c (n "hmath") (v "0.1.9") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1npyaxrc6pgx4d3ffhlb3n828181f2x0635d92n67d9wx52ij5xy") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.10 (c (n "hmath") (v "0.1.10") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "046qxqi1p14db75d2k5li7jz7d6y2djjsv2c454xg2z0vhdvzcvp") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.11 (c (n "hmath") (v "0.1.11") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0bsbl4vbv4vv82g2y3kc7i2bd8rd8j3yqxpdkgysbl0xz175s8yh") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.12 (c (n "hmath") (v "0.1.12") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1il8q40421ngjc9bf7kz4amajbxv3vkg2zd5ac7fpnksd2299vl7") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.13 (c (n "hmath") (v "0.1.13") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "13xqhghb0x9x848aj91srhvc53f6a2arzwr5b123vf9g7z1qf0fv") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.14 (c (n "hmath") (v "0.1.14") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1aqdh509kajadlg15w3bldnar2ifq0ag7rh54msh16dzqclrrv9b") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.15 (c (n "hmath") (v "0.1.15") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "11mywp3n0dy1vhlfikwy9isphxl3yhhvzswhybn3lfdrr0gnrxym") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.16 (c (n "hmath") (v "0.1.16") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1z9f7hi19l0ry8ial1nlp4nwm7nry3nawyb8zh5ncbc9mk7l5db6") (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1.17 (c (n "hmath") (v "0.1.17") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1hzv0aw78dlqpzlmrgs7n51brchnypz2wrsj82fi14vq026cg7s8") (s 2) (e (quote (("rand" "dep:rand"))))))

