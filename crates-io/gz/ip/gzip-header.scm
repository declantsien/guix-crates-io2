(define-module (crates-io gz ip gzip-header) #:use-module (crates-io))

(define-public crate-gzip-header-0.1.0 (c (n "gzip-header") (v "0.1.0") (d (list (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "129gkww02yssla5phagszj0xfjn1lv79gxh9r3ar909bi74bl5fn")))

(define-public crate-gzip-header-0.1.1 (c (n "gzip-header") (v "0.1.1") (d (list (d (n "crc") (r "^1.4.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "07jqc263zc0c128i6zdd6hh6i6sxqzgriaiq01c8b4lbq70qsh6k")))

(define-public crate-gzip-header-0.1.2 (c (n "gzip-header") (v "0.1.2") (d (list (d (n "crc") (r "^1.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "0h6dbv6g9rnfp7njgbpw4hhwppxr56y6f92v6m1564pfr7hwz7qa")))

(define-public crate-gzip-header-0.2.0 (c (n "gzip-header") (v "0.2.0") (d (list (d (n "crc") (r "^1.5.0") (d #t) (k 0)))) (h "11j5i17xkq6ad16bz5m4hwl7gxpj4wgwzwn6ns5yk7wh9995k7ig")))

(define-public crate-gzip-header-0.3.0 (c (n "gzip-header") (v "0.3.0") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0fg6vm8sgsm69szwqyz7abfbyziv6pv0jkcailimlamvsfrzwc81")))

(define-public crate-gzip-header-1.0.0 (c (n "gzip-header") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)))) (h "18lm2y96mahkmcd76pzyam2sl3v6lsl9mn8ajri9l0p6j9xm5k4m")))

