(define-module (crates-io gz ip gzip-cmp) #:use-module (crates-io))

(define-public crate-gzip-cmp-0.1.0 (c (n "gzip-cmp") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "08l518r1mkrpmgvnaw9cwkfs59lc7p30chxi33sl54zpaha5p1yw")))

