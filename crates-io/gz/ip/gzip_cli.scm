(define-module (crates-io gz ip gzip_cli) #:use-module (crates-io))

(define-public crate-gzip_cli-0.1.0 (c (n "gzip_cli") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "gzip") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1yhln5nnq64q5ddgrqh38q0fxwqa5d5vxqlhmax3x8iv8gn2qg2n")))

