(define-module (crates-io gz ip gzip) #:use-module (crates-io))

(define-public crate-gzip-0.1.1 (c (n "gzip") (v "0.1.1") (h "0c8da71ygrws3l6zr9fakrkpzgnzqj8slhhycb90wjyxvn2jzilm")))

(define-public crate-gzip-0.1.2 (c (n "gzip") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "deflate") (r "^0.8") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6.4") (d #t) (k 0)))) (h "1c3dsh1cnlvfph1wawznrac8ap8pwj971gspvc78vz3ird24azs1")))

