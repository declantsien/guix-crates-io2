(define-module (crates-io gz -t gz-transport-sys) #:use-module (crates-io))

(define-public crate-gz-transport-sys-0.0.1 (c (n "gz-transport-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "156gix7b63rvpd13mbpg4bngfryq8x8jah2s011qrxjyqbr9nv88") (y #t) (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.0.2 (c (n "gz-transport-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l5qcabzkv7mw82q3zv9y4ahbq416nvjxbbza9rxy4663gxl0c0i") (y #t) (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.0.3 (c (n "gz-transport-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18arf6ygx9dn89vafpz9dh2w9c3v4fk5pjak5vaqqp01d95066qq") (y #t) (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.1.0 (c (n "gz-transport-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0n3k1ip10w2n3sh62985q8c3mns5mg9ddmd3ahnqpxx2k6b889kd") (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.1.1 (c (n "gz-transport-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ayn3s49whb2f2yhz2scc38x9p5lwrb1559am5jghm27phqql32w") (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.2.0 (c (n "gz-transport-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09bhlmrwrc6j1kfcs6y1vqs9m3mx8lxn4f7mhx9ca9hpcp7drz46") (l "gz-transport12") (r "1.60.0")))

(define-public crate-gz-transport-sys-0.7.0 (c (n "gz-transport-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0gs3jlgqvybywk5017hbd302zpmg2an8m0aaqqw0grz7bhc06sfa") (f (quote (("harmonic") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-transport-sys-0.7.1 (c (n "gz-transport-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1acw65rj7lzny8z9bfcsk6bndvngmg6i0r6gsix0p9m5zvvyjc22") (f (quote (("harmonic") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-transport-sys-0.7.2 (c (n "gz-transport-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "08sis750v2hv72vmqcz27bsn96nccx2626w9lcimavdwhjxf38fs") (f (quote (("harmonic") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-transport-sys-0.7.3 (c (n "gz-transport-sys") (v "0.7.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "18k6zmdrjpwdwzx7bj5yagdi6aqqwic1bhf47isaa05lqfzb8hnn") (f (quote (("harmonic") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-transport-sys-0.7.4 (c (n "gz-transport-sys") (v "0.7.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "174ahfwjyvg0gidwbbddl11xwldpcizdx4hgjzbdcvj8mg3jwqqy") (f (quote (("harmonic") ("garden") ("fortress")))) (r "1.65.0")))

