(define-module (crates-io gz b_ gzb_binary_69) #:use-module (crates-io))

(define-public crate-gzb_binary_69-0.1.0 (c (n "gzb_binary_69") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "10c7r9qsls7m5dv6c5xm46js63fib1xwfc3qy09skbd2cbq1m94v")))

(define-public crate-gzb_binary_69-0.1.1 (c (n "gzb_binary_69") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0w8v3q9nc2i946fv391bv5zg3lfgv09g5jcsb21ivvn2qpdnbvas")))

(define-public crate-gzb_binary_69-0.1.2 (c (n "gzb_binary_69") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0wva3yqvzam2ycb51lqn19p63nzc6kh9z6c9hb1rfz41hwg0jqcq")))

(define-public crate-gzb_binary_69-0.1.3 (c (n "gzb_binary_69") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "13vi94f5n25aysk9r6clwbp34lfbkp5vvqmf5wpna02f9p5ryy8p")))

(define-public crate-gzb_binary_69-0.1.4 (c (n "gzb_binary_69") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1fw9nz0kpkqm8q5n75vb937k3ssgxym0s6cs2vjgwzkjw6m7q9j5")))

(define-public crate-gzb_binary_69-0.1.5 (c (n "gzb_binary_69") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0g8a8924332y58jmansgyn8q0cryh01zjbj208iljld8zssmqjjs")))

(define-public crate-gzb_binary_69-0.1.6 (c (n "gzb_binary_69") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1mqr11q4q0gq1b8rmsc47wbapcc03wkrjild8wp2p1l97dj0ldk1")))

(define-public crate-gzb_binary_69-0.2.0 (c (n "gzb_binary_69") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1wj6nimkdk2jvppv1dm5khv5r9p0qa8gla1qrbz4fls80rlfjn3s")))

(define-public crate-gzb_binary_69-0.2.1 (c (n "gzb_binary_69") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0aai6rdlnanay12gmppxihaaq5b7r0pkzkfr2vzfp8sbi1dnzzhi")))

(define-public crate-gzb_binary_69-0.2.2 (c (n "gzb_binary_69") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "13k6bvfiaa61ph0d094psym6nzc8vddrqzfhzzb3ykbds6531aad")))

