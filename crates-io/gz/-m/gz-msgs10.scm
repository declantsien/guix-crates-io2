(define-module (crates-io gz -m gz-msgs10) #:use-module (crates-io))

(define-public crate-gz-msgs10-0.5.0 (c (n "gz-msgs10") (v "0.5.0") (d (list (d (n "gz-msgs-build") (r "^0.1.0") (d #t) (k 1)) (d (n "gz-msgs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "015c3ihvvgngsmmjfd1jzqxbd8fxksg604j06qwhnf2bknb3jcdh") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs10-0.6.0 (c (n "gz-msgs10") (v "0.6.0") (d (list (d (n "gz-msgs-build") (r "^0.2.0") (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "1gd4lwq8sjcbj0fxh6zi3wd5fij23cc6aw2x0zdx029p5mss9i6r") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs10-0.0.0 (c (n "gz-msgs10") (v "0.0.0") (h "01549qbw4ghlw1lcq483cd61kfq380znkpjq8cnmzcv54nsq0i2g") (y #t)))

