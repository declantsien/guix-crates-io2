(define-module (crates-io gz -m gz-msgs-common) #:use-module (crates-io))

(define-public crate-gz-msgs-common-0.1.0 (c (n "gz-msgs-common") (v "0.1.0") (d (list (d (n "gz-msgs-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "1nfk2xlbfn54zqdg4kzgdpbc6kwid70rhs1by96qmk5z7jfmm2wd") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-common-0.7.0 (c (n "gz-msgs-common") (v "0.7.0") (d (list (d (n "gz-msgs-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "0k99xzmjfwgkx0jck8g8c6ixhhg9v3mrdh34q8s9pwzfnwshavz4") (r "1.65.0")))

(define-public crate-gz-msgs-common-0.7.1 (c (n "gz-msgs-common") (v "0.7.1") (d (list (d (n "gz-msgs-derive") (r "^0.7.1") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "123zhm5byqlhbk37sp38k4b85nxapm45qn87l5ks38vp8s05kyh4") (r "1.65.0")))

(define-public crate-gz-msgs-common-0.7.2 (c (n "gz-msgs-common") (v "0.7.2") (d (list (d (n "gz-msgs-derive") (r "^0.7.2") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "1bd3l094j96nisl9jafjdhkc5z9rfrj65zfsmphm8wzf3n01l2vm") (r "1.65.0")))

(define-public crate-gz-msgs-common-0.7.3 (c (n "gz-msgs-common") (v "0.7.3") (d (list (d (n "gz-msgs-derive") (r "^0.7.3") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "02gfpwvdrqccf4748989i8q4mw2dlgd4d9x58hw25swgq9ybbqmd") (r "1.65.0")))

(define-public crate-gz-msgs-common-0.7.4 (c (n "gz-msgs-common") (v "0.7.4") (d (list (d (n "gz-msgs-derive") (r "=0.7.4") (d #t) (k 0)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "0gmlxqnm8k3zkygf3m9xn79l12mibr3jlyw2zk8rm909p9fpxy67") (r "1.65.0")))

