(define-module (crates-io gz -m gz-msgs-build) #:use-module (crates-io))

(define-public crate-gz-msgs-build-0.1.0 (c (n "gz-msgs-build") (v "0.1.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 0)))) (h "0wnjqw7fjzl7xw80firw0ci9rhxmwxml7fnwwy110jxxcrxh64rv") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-build-0.2.0 (c (n "gz-msgs-build") (v "0.2.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 0)))) (h "0nvk3y2d33bl6yjk735i802dxl64xwjkzkayjmjjg3sykhcr9nhq") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-build-0.7.0 (c (n "gz-msgs-build") (v "0.7.0") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.3.0") (d #t) (k 0)))) (h "0wl1rvp0ngkqn5xh3d7z24g7wpl7y2daasd4al6vfngjdmbz41iy") (r "1.65.0")))

(define-public crate-gz-msgs-build-0.7.1 (c (n "gz-msgs-build") (v "0.7.1") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.3.0") (d #t) (k 0)))) (h "1r8s7j0w83zw7ya91rzmrhqn4iwk21527mhh2ck6q163acmn1nwh") (r "1.65.0")))

(define-public crate-gz-msgs-build-0.7.2 (c (n "gz-msgs-build") (v "0.7.2") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.3.0") (d #t) (k 0)))) (h "0l9mibz6nx224nmkz9bsm3a6n4cp0h69h38179vcdpf7nsjlkanw") (r "1.65.0")))

(define-public crate-gz-msgs-build-0.7.3 (c (n "gz-msgs-build") (v "0.7.3") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.3.0") (d #t) (k 0)))) (h "13b34svhy583bgfb0c825asi88ycfcdypk8fi6877layl25z8vk9") (r "1.65.0")))

(define-public crate-gz-msgs-build-0.7.4 (c (n "gz-msgs-build") (v "0.7.4") (d (list (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "=3.3.0") (d #t) (k 0)))) (h "01ifk58ygpdzaq0iap8xyr50mi9w22zcjvz42l85pwgspy3k9j93") (r "1.65.0")))

