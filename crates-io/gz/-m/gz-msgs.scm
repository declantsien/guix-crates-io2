(define-module (crates-io gz -m gz-msgs) #:use-module (crates-io))

(define-public crate-gz-msgs-0.1.0 (c (n "gz-msgs") (v "0.1.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "13k5wg0l3hhvd94kdc5d0p5l3s30c4v17w6w66kg30rx9mli2qgv") (y #t) (r "1.60.0")))

(define-public crate-gz-msgs-0.2.0 (c (n "gz-msgs") (v "0.2.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "1y4z7r1gxavv4wcm0k9yh7kivd5a5vkkvzdfgdhs37m4kw7pacgv") (y #t) (r "1.60.0")))

(define-public crate-gz-msgs-0.2.1 (c (n "gz-msgs") (v "0.2.1") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "08bfv8njhslcb6c3s8gcfalcq379s3g4flpv9x9sqzgl36979ly4") (y #t) (r "1.60.0")))

(define-public crate-gz-msgs-0.3.0 (c (n "gz-msgs") (v "0.3.0") (d (list (d (n "gz-msgs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "0xcgrv56dp1d3d5899pg7h4f75n2gji8xd84kdrj3mfm2dr0yyfi") (r "1.60.0")))

(define-public crate-gz-msgs-0.3.1 (c (n "gz-msgs") (v "0.3.1") (d (list (d (n "gz-msgs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "09z5y2xwm7lhr8kl94vxw7x3hf0sq5yq93i3dnfl23lwhz9py1d3") (r "1.60.0")))

(define-public crate-gz-msgs-0.4.0 (c (n "gz-msgs") (v "0.4.0") (d (list (d (n "gz-msgs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "1cblwkl72jx6g2995z5rym37w5fckfp8ll38d43mnzq1i81nzzhk") (f (quote (("ignition")))) (r "1.60.0")))

(define-public crate-gz-msgs-0.6.0 (c (n "gz-msgs") (v "0.6.0") (d (list (d (n "gz-msgs10") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "gz-msgs8") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "gz-msgs9") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "06agy9np59x7mcyr9v2vsklxmz1imqci90139vph7dsdxs55caw9") (f (quote (("harmonic" "gz-msgs10") ("garden" "gz-msgs9") ("fortress" "gz-msgs8") ("default" "gz-msgs9")))) (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-0.7.0 (c (n "gz-msgs") (v "0.7.0") (d (list (d (n "gz-msgs-build") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.7.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "0nllk3a5xqpq2lmasj2n8x68sc34g0mxdni1yv04lw3z0zzqv569") (f (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-msgs-0.7.1 (c (n "gz-msgs") (v "0.7.1") (d (list (d (n "gz-msgs-build") (r "^0.7.1") (o #t) (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.7.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "1x4a2p2kbnrjr13fp000qnfyk0j3alxsh9nfln4fyr6qqv5a51b7") (f (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-msgs-0.7.2 (c (n "gz-msgs") (v "0.7.2") (d (list (d (n "gz-msgs-build") (r "^0.7.2") (o #t) (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.7.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "0h6h7pfanzw69ghp1xzkl84yc59br1c6sav88ywhngczll28v01h") (f (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-msgs-0.7.3 (c (n "gz-msgs") (v "0.7.3") (d (list (d (n "gz-msgs-build") (r "^0.7.3") (o #t) (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.7.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "1s2brz6a297s3kz165rz99117yg0xchzyf06314dl4l8y7g160qb") (f (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (r "1.65.0")))

(define-public crate-gz-msgs-0.7.4 (c (n "gz-msgs") (v "0.7.4") (d (list (d (n "gz-msgs-build") (r "=0.7.4") (o #t) (d #t) (k 1)) (d (n "gz-msgs-common") (r "=0.7.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "protobuf") (r "=3.3.0") (d #t) (k 0)))) (h "1ckj922xx0z2xp43hv2z5yig2dandzkdkfcc2sy2apjk3d5bnp11") (f (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (r "1.65.0")))

