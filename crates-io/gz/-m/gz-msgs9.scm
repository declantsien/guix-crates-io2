(define-module (crates-io gz -m gz-msgs9) #:use-module (crates-io))

(define-public crate-gz-msgs9-0.5.0 (c (n "gz-msgs9") (v "0.5.0") (d (list (d (n "gz-msgs-build") (r "^0.1.0") (d #t) (k 1)) (d (n "gz-msgs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "170r9id2kpb0qgambbd1hm9xf763na34d7pvinms6wz12f49h7nw") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs9-0.6.0 (c (n "gz-msgs9") (v "0.6.0") (d (list (d (n "gz-msgs-build") (r "^0.2.0") (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "0r6cani2fc1izzdaa9795w0q8l5x0i40sq2d321yvdfirr8yyi8q") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs9-0.0.0 (c (n "gz-msgs9") (v "0.0.0") (h "1v6bq19jszqbh7z139a4bn5y70r6k4xr2rk6yl5dls4rl2fwmxq3") (y #t)))

