(define-module (crates-io gz -m gz-msgs8) #:use-module (crates-io))

(define-public crate-gz-msgs8-0.5.0 (c (n "gz-msgs8") (v "0.5.0") (d (list (d (n "gz-msgs-build") (r "^0.1.0") (d #t) (k 1)) (d (n "gz-msgs-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "0lp3dxdqbw2ydd4iy43q8j8x620gkzgpxn5mmq4f53cvi9kn37ya") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs8-0.6.0 (c (n "gz-msgs8") (v "0.6.0") (d (list (d (n "gz-msgs-build") (r "^0.2.0") (d #t) (k 1)) (d (n "gz-msgs-common") (r "^0.1.0") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)))) (h "0w961319r2kk6yhbjca23kj0lkr7iqkni9hvgdz1s1q0vb103y2n") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs8-0.0.0 (c (n "gz-msgs8") (v "0.0.0") (h "02va3skbw96q58dkq3703s1zdlrljh6c5afdgq670749vafma8im") (y #t)))

