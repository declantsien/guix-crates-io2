(define-module (crates-io gz -m gz-msgs-derive) #:use-module (crates-io))

(define-public crate-gz-msgs-derive-0.1.0 (c (n "gz-msgs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03wzwq4cgrz6g18gyi0izfwr6ckj8jqjbqkj804096n5y4gkwljg") (r "1.60.0")))

(define-public crate-gz-msgs-derive-0.2.0 (c (n "gz-msgs-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1nz30dp433w9n54vrxw1i8430xs9d8a6c1nh3g5c09gbnlpgi7x4") (r "1.60.0")))

(define-public crate-gz-msgs-derive-0.3.0 (c (n "gz-msgs-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bdhwlzlynd0bw47rjdycfrg9sfbz9bpxdziniw956z4w6w2xrkr") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-derive-0.4.0 (c (n "gz-msgs-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05s47nwswm6p25mx1pqmihjs3g31jrfkdjljyma8n9wv1x7g2ihg") (y #t) (r "1.64.0")))

(define-public crate-gz-msgs-derive-0.7.0 (c (n "gz-msgs-derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "03r9xs90mjcrbqqy51nn7z6h8h167sn21iy38mc49d6vdircf3gp") (r "1.65.0")))

(define-public crate-gz-msgs-derive-0.7.1 (c (n "gz-msgs-derive") (v "0.7.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0mjkndjjmcg72qy0vcdknrrn1yvj7bd5k93l9q0f6w61m51fjjh4") (r "1.65.0")))

(define-public crate-gz-msgs-derive-0.7.2 (c (n "gz-msgs-derive") (v "0.7.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "188p4zhylx8x8617rhsabafjgji3d3nb6a0w55z94mqbmr1qambl") (r "1.65.0")))

(define-public crate-gz-msgs-derive-0.7.3 (c (n "gz-msgs-derive") (v "0.7.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1fza6scwgxlc20n9cb357k3gh9jrmmrfkgazk49j539371xvwvy3") (r "1.65.0")))

(define-public crate-gz-msgs-derive-0.7.4 (c (n "gz-msgs-derive") (v "0.7.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0g8nn0dxc6zy5sqjd5y9zkz3wsmkd37lmns303l2n0l3jz72712m") (r "1.65.0")))

