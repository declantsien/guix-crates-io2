(define-module (crates-io jc c- jcc-cli) #:use-module (crates-io))

(define-public crate-jcc-cli-0.1.4 (c (n "jcc-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "jcc") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0sa99izxc8983499m0n31i5b6bihdpgm9m9bhp1d9wh0jhj6l7nw")))

(define-public crate-jcc-cli-0.1.5 (c (n "jcc-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "jcc") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0k3zg5rngzbyf03b835vf5qrrnfyh2dwri2954w1zpafa8jzpahg")))

(define-public crate-jcc-cli-0.1.6 (c (n "jcc-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "jcc") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0zivlmjfkdlmj7i295bq9sbd2qmlx538f04m22vznlpbijmyp18h")))

(define-public crate-jcc-cli-0.1.7 (c (n "jcc-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "jcc") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0vij4zyvcb5kxik3g1knqfy3y2xb5ybq8s81cqmi0i2fdlzghfsf")))

