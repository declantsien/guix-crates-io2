(define-module (crates-io jc om jcompiler) #:use-module (crates-io))

(define-public crate-jcompiler-0.1.0 (c (n "jcompiler") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "llvm-sys") (r "^70") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)))) (h "00pg6zq2y4ivk2a4ds9qf29jf2n0jink2azqfvj5k73k6yh4c1l3")))

