(define-module (crates-io jc er jcers) #:use-module (crates-io))

(define-public crate-jcers-0.1.0 (c (n "jcers") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "jcers_proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1yigmxldy4d087az650b2w1c8mkr6h97vrzm1xr2xymbm0442njl") (f (quote (("derive" "jcers_proc"))))))

(define-public crate-jcers-0.1.1 (c (n "jcers") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "jcers_proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "06dhvzq3lsf4njfn4lzsxjxnsvnrx9b42agx4qb9aynn3wi0cxzi") (f (quote (("derive" "jcers_proc"))))))

(define-public crate-jcers-0.1.2 (c (n "jcers") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "jcers_proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1rkbl8f5yraya1il5f6k6481bj5vqh9fyn7q16yil3r0i01xilkn") (f (quote (("derive" "jcers_proc"))))))

