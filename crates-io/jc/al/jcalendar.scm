(define-module (crates-io jc al jcalendar) #:use-module (crates-io))

(define-public crate-jcalendar-0.1.0 (c (n "jcalendar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "jput") (r "^0.1") (d #t) (k 0)) (d (n "koyomi") (r "^0.4") (d #t) (k 0)))) (h "1mcf1wl2cfm2iajc5cpn354wviyd9nncn11b0fri9157j63v2p1q") (y #t)))

(define-public crate-jcalendar-0.1.1 (c (n "jcalendar") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "jput") (r "^0.1") (d #t) (k 0)) (d (n "koyomi") (r "^0.4") (d #t) (k 0)))) (h "0wqw7s1in2pzxjz3kq2xsw4kgw0sj6170n76z09ra46z886f4x3g") (y #t)))

(define-public crate-jcalendar-0.1.2 (c (n "jcalendar") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "jput") (r "^0.1") (d #t) (k 0)) (d (n "koyomi") (r "^0.4") (d #t) (k 0)))) (h "1bi69akrssfv26dm6if302apinn94y480xz61mimkwwv1hqx726v")))

