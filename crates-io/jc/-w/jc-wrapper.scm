(define-module (crates-io jc -w jc-wrapper) #:use-module (crates-io))

(define-public crate-jc-wrapper-0.1.0 (c (n "jc-wrapper") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lg2bk6n7v714cg5yddymz8cyq9mc1p1ba4xj5bygw0lv42v1zn4")))

(define-public crate-jc-wrapper-0.1.1 (c (n "jc-wrapper") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mbki30rrlr4ds2libyxl3mm8b3da4qwbp12zdqriny8aw1bk133")))

(define-public crate-jc-wrapper-0.2.0 (c (n "jc-wrapper") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x2l0dy4afw8vg1l1lkmzrv9l8fhhn7kmphd1p3hf7fcgim02yvz")))

(define-public crate-jc-wrapper-0.2.1 (c (n "jc-wrapper") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hw3p762lwaix2is99k01kmax1my0kqfvwrm7wx8c5riw10ivaz5")))

