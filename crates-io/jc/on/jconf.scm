(define-module (crates-io jc on jconf) #:use-module (crates-io))

(define-public crate-jconf-0.1.0 (c (n "jconf") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0vasp0icqd0k26yzf7ly84q3573412agzg46lpspdm72gimgm1qr")))

(define-public crate-jconf-0.1.1 (c (n "jconf") (v "0.1.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0q59kl73d218074fy7sz6zbjg0f0skj6x7znknilqwi5ccfra3qm")))

(define-public crate-jconf-0.1.2 (c (n "jconf") (v "0.1.2") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1v7a8rdzid0wwv1hgxymal6xcdpwmvwpvca4pfrlijrnbl9ss8jz")))

