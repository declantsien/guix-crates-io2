(define-module (crates-io jc on jconfig) #:use-module (crates-io))

(define-public crate-jconfig-0.1.0 (c (n "jconfig") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "traitobject") (r "^0.0") (d #t) (k 0)) (d (n "unicase") (r "^1.4") (d #t) (k 0)))) (h "05kdkr2zb3dmxgl34n71gh6al3r89sn34j0zxvgnc6wplwy5inmf")))

