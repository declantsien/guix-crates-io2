(define-module (crates-io jc -p jc-protobuf-cognito) #:use-module (crates-io))

(define-public crate-jc-protobuf-cognito-1.0.32 (c (n "jc-protobuf-cognito") (v "1.0.32") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0plwmrf2h8qd9zm4140x6pzalqh2r473x8x6kp7xf20dm2yd1ihy")))

(define-public crate-jc-protobuf-cognito-1.0.33 (c (n "jc-protobuf-cognito") (v "1.0.33") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0zwnrwn2q19bkhxr3pafialidbww90hdr1vzsw00w90s79373qvs")))

(define-public crate-jc-protobuf-cognito-1.0.34 (c (n "jc-protobuf-cognito") (v "1.0.34") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1x1h5y4am3n2ql627p8y5vhx4r2y80kv0j07snsqs42z01nik7xp")))

(define-public crate-jc-protobuf-cognito-1.0.35 (c (n "jc-protobuf-cognito") (v "1.0.35") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "1i817rbrb0lk72p8ijdqxr9p5r223q2j7vfhih4bql129m9495wq")))

(define-public crate-jc-protobuf-cognito-1.0.36 (c (n "jc-protobuf-cognito") (v "1.0.36") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "00fdasw75n0xwhvbndfxd4i8nmpw094vylxd9z5is541jckxy0a7")))

(define-public crate-jc-protobuf-cognito-1.0.37 (c (n "jc-protobuf-cognito") (v "1.0.37") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "0h7l1a2xr0diqym74zdxbc7fhcl253wi55aifmdwdgb22f47i1hb")))

(define-public crate-jc-protobuf-cognito-1.0.38 (c (n "jc-protobuf-cognito") (v "1.0.38") (d (list (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "grpc-build") (r "^4.2.0") (d #t) (k 1)))) (h "03nfkzj2llz6vifddd2y2yk9m8gfz0lb0vk58aq0n494ssv88prc")))

