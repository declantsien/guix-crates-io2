(define-module (crates-io jc e- jce-derive) #:use-module (crates-io))

(define-public crate-jce-derive-0.1.0 (c (n "jce-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b4bpiwlk88krdk1z0zdmangrk2dsqf3mmhp5zk7wsvgvhd5xl2f")))

(define-public crate-jce-derive-0.1.1 (c (n "jce-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "185vg72m2pjjk7hqxmbxkn75ayg6achjhrx55n0fw087pz9r222i")))

