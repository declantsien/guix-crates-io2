(define-module (crates-io r0 ke r0ket-l0dable) #:use-module (crates-io))

(define-public crate-r0ket-l0dable-0.0.0 (c (n "r0ket-l0dable") (v "0.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "lpc13xx") (r "^0.0.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "0bqlvimnk5v4cwlajv17fwlv1szzgdck3qjgmc20r201r4lslij6")))

(define-public crate-r0ket-l0dable-0.1.0 (c (n "r0ket-l0dable") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "lpc13xx") (r "^0.0.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "028avmyjz7dvvcgks76x58wda91zgl5z1vnxl1idfrbdpfaqjl4n")))

