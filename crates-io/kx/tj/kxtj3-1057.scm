(define-module (crates-io kx tj kxtj3-1057) #:use-module (crates-io))

(define-public crate-kxtj3-1057-0.1.0 (c (n "kxtj3-1057") (v "0.1.0") (h "00b74zqvi3vch9fmiyjbs9kqyvkr2rqpvk1c9vd4yaa912kqahia")))

(define-public crate-kxtj3-1057-0.2.0 (c (n "kxtj3-1057") (v "0.2.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "esp-idf-hal") (r "^0.39.3") (d #t) (k 2)) (d (n "esp-idf-sys") (r "^0.31.11") (d #t) (k 2)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "08ii6q5mlm99b4329rxpn8air52n1598rx4vjmcqng08lawj89yg")))

(define-public crate-kxtj3-1057-0.3.0 (c (n "kxtj3-1057") (v "0.3.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "esp-idf-svc") (r "^0.48.0") (k 2)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "17psv3gh0a62b1z4a3x34b4jwa8an4f2sz91956wf2lp7bqjy3yv")))

(define-public crate-kxtj3-1057-0.3.1 (c (n "kxtj3-1057") (v "0.3.1") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "esp-idf-svc") (r "^0.48.0") (k 2)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "1g8x7wdcsvd1kp2hb5ibqz9fsfyi5srfh9nx7cq925k60xrqayl1")))

