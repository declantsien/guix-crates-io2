(define-module (crates-io kx cj kxcj9) #:use-module (crates-io))

(define-public crate-kxcj9-0.1.0 (c (n "kxcj9") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1yxkzl2vjnkk9352jw95ic964c7j05nmajwrz3yb9qrn18kf56gn")))

(define-public crate-kxcj9-0.2.0 (c (n "kxcj9") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "11dr6l7n09yidl1aa6zzvidv8797038wzr23jgriarn5d8ywka2p")))

