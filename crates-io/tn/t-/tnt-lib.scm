(define-module (crates-io tn t- tnt-lib) #:use-module (crates-io))

(define-public crate-tnt-lib-0.1.1 (c (n "tnt-lib") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.32") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.32") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1dbx473pjsaxfpj7m2pwnayc1yxl3wmyviglv5wijy3n6gnw201n") (l "NTL")))

(define-public crate-tnt-lib-0.1.2 (c (n "tnt-lib") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0.32") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.32") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "180yx6n6s1znqr5xhx1i0sjbbs56ys61jjdpa8aw918w8xlmwnay") (l "NTL")))

(define-public crate-tnt-lib-0.1.3 (c (n "tnt-lib") (v "0.1.3") (d (list (d (n "cxx") (r "^1.0.32") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.32") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "08x2h7gzij9q87hgdlnv5p64zj228rs5i0cxcn95rc4pklj60kmz") (l "NTL")))

(define-public crate-tnt-lib-0.1.4 (c (n "tnt-lib") (v "0.1.4") (d (list (d (n "cxx") (r "^1.0.32") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.32") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "04w1rx7dzdxjyjsk68d3zzhi3q1s3l02cr50iav0pf4iba4pbzvh") (l "NTL")))

