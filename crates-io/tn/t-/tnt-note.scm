(define-module (crates-io tn t- tnt-note) #:use-module (crates-io))

(define-public crate-tnt-note-0.1.0 (c (n "tnt-note") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)))) (h "13ak1s1birp5g7lk2qc3d9j3wmpsxg2zgwir0vgm1zg2gdfkr83q")))

(define-public crate-tnt-note-0.1.1 (c (n "tnt-note") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "lexopt") (r "^0.2.1") (d #t) (k 0)))) (h "01awmbgnzb9m8lq55z2mawk3lyx68s7a14a3jhhwirdbrywh6zar")))

