(define-module (crates-io tn il tnil) #:use-module (crates-io))

(define-public crate-tnil-0.1.0 (c (n "tnil") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "1bzhgwb4zivnnxm34kgkqzhxbgvsw0l8axzcf2icja9j6z6lrq5f") (f (quote (("sheet-root-data") ("sheet-affix-data") ("lexicon-json-root-data"))))))

(define-public crate-tnil-0.1.1 (c (n "tnil") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "0lhm58wg15sbf0nnx6bg3vi37ahh3nj7m00kk7k9yv6ffyjk61cn") (f (quote (("sheet-root-data") ("sheet-affix-data") ("lexicon-json-root-data"))))))

(define-public crate-tnil-0.1.2 (c (n "tnil") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "0gnc7z0zywf0pcl55mb6zsnjcxymvllgiyr4c3abs48nq33437v2") (f (quote (("sheet-root-data") ("sheet-affix-data") ("lexicon-json-root-data"))))))

(define-public crate-tnil-0.1.3 (c (n "tnil") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "vec1") (r "^1.10.1") (d #t) (k 0)))) (h "09bf4micannqc7588v4y51f3xn1fym9yckspihmakia1i1nam5m1") (f (quote (("sheet-root-data") ("sheet-affix-data") ("lexicon-json-root-data"))))))

