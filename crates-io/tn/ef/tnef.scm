(define-module (crates-io tn ef tnef) #:use-module (crates-io))

(define-public crate-tnef-0.1.0 (c (n "tnef") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "codepage") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "07vi7bcwspjrf0sq9a63jfrbqxgrmaxkd5ybssrjfrs2i5vb2wqb") (y #t)))

(define-public crate-tnef-0.1.1 (c (n "tnef") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "codepage") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1cknwd0dn5inm9ldmi2kqzzsw71fjc3qzwk1a1qc17wz2x99kxhn")))

