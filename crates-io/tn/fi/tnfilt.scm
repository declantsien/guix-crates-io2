(define-module (crates-io tn fi tnfilt) #:use-module (crates-io))

(define-public crate-tnfilt-0.1.0 (c (n "tnfilt") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "0ky5fqzz9jal5gjkwwg9c42y4p3iikb4h75y3wc3kq85ay4yrs04")))

(define-public crate-tnfilt-0.1.1 (c (n "tnfilt") (v "0.1.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "13jm2xbpvb9jbp0l5bmcyx6g22d46c4qi6y10y9khp1x9swqj7rd")))

