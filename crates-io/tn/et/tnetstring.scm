(define-module (crates-io tn et tnetstring) #:use-module (crates-io))

(define-public crate-tnetstring-0.1.0 (c (n "tnetstring") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "05na1pk3p765jr66yr8qhk6agq3sbh2z0lyrix29ikjywsa0r78s")))

(define-public crate-tnetstring-0.2.0 (c (n "tnetstring") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "008bglc47chw1a73krkkb2gci8p94zgkpc04py66rdhd29hvyzh8")))

