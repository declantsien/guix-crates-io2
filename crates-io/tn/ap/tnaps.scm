(define-module (crates-io tn ap tnaps) #:use-module (crates-io))

(define-public crate-tnaps-0.1.0 (c (n "tnaps") (v "0.1.0") (d (list (d (n "arrrg") (r "^0.3") (d #t) (k 2)) (d (n "guacamole") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "statslicer") (r "^0.1") (d #t) (k 2)))) (h "09zyxj526hgn2bnjdl1kj21gh1kxpxpizl79y0fz4cndyjw2gjaz")))

