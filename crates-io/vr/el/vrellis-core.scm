(define-module (crates-io vr el vrellis-core) #:use-module (crates-io))

(define-public crate-vrellis-core-0.1.0 (c (n "vrellis-core") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "imageproc") (r "^0.22") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11l6f7bsah1qiq5psccykwbwyzhazg1zgdm4d7nxxh1yx3d3a8x8") (f (quote (("default"))))))

