(define-module (crates-io vr id vride-api) #:use-module (crates-io))

(define-public crate-vride-api-0.1.0 (c (n "vride-api") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1grl79r33bnzyq7aqxfbydd4wmq6zm8m6449fki9ryzl4zizwzsb")))

