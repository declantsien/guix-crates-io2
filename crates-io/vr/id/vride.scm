(define-module (crates-io vr id vride) #:use-module (crates-io))

(define-public crate-vride-0.1.0 (c (n "vride") (v "0.1.0") (d (list (d (n "vride-api") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vride-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vride-runtime") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0aiszb7g1hga0v5ciabs1542a0v6havv0lm3nb47sl126z93dgfd") (f (quote (("runtime" "vride-runtime") ("macros" "vride-macros") ("full" "macros" "runtime" "api") ("api" "vride-api"))))))

