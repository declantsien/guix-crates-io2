(define-module (crates-io vr id vride-macros) #:use-module (crates-io))

(define-public crate-vride-macros-0.1.0 (c (n "vride-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1blb0dx3rh423ry6pipjhvsnlc3dkijkjcqjfi32ll4incavg27v")))

