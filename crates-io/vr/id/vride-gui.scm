(define-module (crates-io vr id vride-gui) #:use-module (crates-io))

(define-public crate-vride-gui-0.1.0 (c (n "vride-gui") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0fip2988xg2xcsb53wdcda0gb8fhm6yr8dyqmfpczl610bk42h92")))

