(define-module (crates-io vr sh vrsh) #:use-module (crates-io))

(define-public crate-vrsh-0.1.0 (c (n "vrsh") (v "0.1.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0xlq5gnqlv83k82vy3l1ykqdzsl1pf91fgcarc45f8fy6bhzln3f")))

(define-public crate-vrsh-0.1.1 (c (n "vrsh") (v "0.1.1") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "03kcarrz617bznskd031fwsr6xcg1vxdiyx75vwdj3cl8dh8c6w6")))

(define-public crate-vrsh-0.1.2 (c (n "vrsh") (v "0.1.2") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0l5hdyxl84rb10sxihprx98hk4sfaw7hpxd87n7b98lmsgq14jpq")))

