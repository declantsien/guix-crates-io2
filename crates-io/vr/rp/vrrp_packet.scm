(define-module (crates-io vr rp vrrp_packet) #:use-module (crates-io))

(define-public crate-vrrp_packet-0.1.0 (c (n "vrrp_packet") (v "0.1.0") (d (list (d (n "pnet_base") (r "^0.34.0") (d #t) (k 0)) (d (n "pnet_macros") (r "^0.34.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.34.0") (d #t) (k 0)))) (h "0s5a1qd5jrqxbsdpxj0h2ziynj5g1gx38q684ckfkcwz5rx11kn4")))

