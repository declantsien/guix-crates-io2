(define-module (crates-io vr u- vru-noise-macros) #:use-module (crates-io))

(define-public crate-vru-noise-macros-1.0.0 (c (n "vru-noise-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0asgbgz4c5w3g0mrgri0a5yl8davsax9jxj5nzlvc9nji5pzz23m")))

(define-public crate-vru-noise-macros-1.0.1 (c (n "vru-noise-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10r5pn6cd7bpzayi1byqpwq3pik2yl1c2l4jc79f3zcsmzw4wf30")))

