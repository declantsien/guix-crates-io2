(define-module (crates-io vr p- vrp-solver) #:use-module (crates-io))

(define-public crate-vrp-solver-1.0.0 (c (n "vrp-solver") (v "1.0.0") (d (list (d (n "vrp-core") (r "^1.0.0") (d #t) (k 0)))) (h "0a3i1l8h9svjfvpy464xkiaxm9v5vvk3z9c735hlgcb9hl4vhw7d")))

(define-public crate-vrp-solver-1.0.1 (c (n "vrp-solver") (v "1.0.1") (d (list (d (n "vrp-core") (r "^1.0.1") (d #t) (k 0)))) (h "14gzc6k19fdyxf872xhl6z0f4qaijmwr7n4pyr493lzlba6y83rs")))

(define-public crate-vrp-solver-1.0.2 (c (n "vrp-solver") (v "1.0.2") (d (list (d (n "vrp-core") (r "^1.0.2") (d #t) (k 0)))) (h "163yh6wixbz57742jn46w0v4pwnmp43na861ib8rbhwnw0fspb17")))

(define-public crate-vrp-solver-1.0.3 (c (n "vrp-solver") (v "1.0.3") (d (list (d (n "vrp-core") (r "^1.0.3") (d #t) (k 0)))) (h "0mlxhp90dsbhnc5wmgb0hwiw6npilb1qpiynkr0rb5iqq5gbnjb7")))

