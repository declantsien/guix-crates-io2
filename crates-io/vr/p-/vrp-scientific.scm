(define-module (crates-io vr p- vrp-scientific) #:use-module (crates-io))

(define-public crate-vrp-scientific-1.0.0 (c (n "vrp-scientific") (v "1.0.0") (d (list (d (n "vrp-core") (r "^1.0.0") (d #t) (k 0)))) (h "07fwymnjdll12sbx5dn2ln376qw408xi86fblxkisai418s7sbln")))

(define-public crate-vrp-scientific-1.0.1 (c (n "vrp-scientific") (v "1.0.1") (d (list (d (n "vrp-core") (r "^1.0.1") (d #t) (k 0)))) (h "1ixnhm8rw92az050mw6p8lrakwa8zg7p7lmh78c424hyfc0qbm4g")))

(define-public crate-vrp-scientific-1.0.2 (c (n "vrp-scientific") (v "1.0.2") (d (list (d (n "vrp-core") (r "^1.0.2") (d #t) (k 0)))) (h "0ir633gvk7khhc9aih23cii8jjxrnmmqz1adv3y4pmqhpfqcg96z")))

(define-public crate-vrp-scientific-1.0.3 (c (n "vrp-scientific") (v "1.0.3") (d (list (d (n "vrp-core") (r "^1.0.3") (d #t) (k 0)))) (h "06xbkn9yxpahpv3qfgiasf94nqpkjg54b3vzaxbali1iwfls2zpv")))

(define-public crate-vrp-scientific-1.1.0 (c (n "vrp-scientific") (v "1.1.0") (d (list (d (n "vrp-core") (r "^1.1.0") (d #t) (k 0)))) (h "1xg6dqq98c10z7879z89xwabi32q8wi17a7vc31556sm42ds5615")))

(define-public crate-vrp-scientific-1.1.1 (c (n "vrp-scientific") (v "1.1.1") (d (list (d (n "vrp-core") (r "^1.1.1") (d #t) (k 0)))) (h "1qhiclwvhqcl3dzi1rpkqwfdffij59s8fjhs24mxp59v71kan4g7")))

(define-public crate-vrp-scientific-1.1.2 (c (n "vrp-scientific") (v "1.1.2") (d (list (d (n "vrp-core") (r "^1.1.2") (d #t) (k 0)))) (h "0vgwxcxhyimdjrr48hhifwcab9qlzi395nlpsddg6kg07zb5c9kz")))

(define-public crate-vrp-scientific-1.2.0 (c (n "vrp-scientific") (v "1.2.0") (d (list (d (n "vrp-core") (r "^1.2.0") (d #t) (k 0)))) (h "0ab4mzljs35h47if6vrawj5p4im28hxmlr4if88ziv0p4n6v9hkk")))

(define-public crate-vrp-scientific-1.2.1 (c (n "vrp-scientific") (v "1.2.1") (d (list (d (n "vrp-core") (r "^1.2.1") (d #t) (k 0)))) (h "119c9q4a2qhlsh6rj9bkz3cycjc4b4qj2x9sq22708zmg39an7ly")))

(define-public crate-vrp-scientific-1.2.2 (c (n "vrp-scientific") (v "1.2.2") (d (list (d (n "vrp-core") (r "^1.2.2") (d #t) (k 0)))) (h "07rnsgdxx0v7gr9irk1ykmp9g27y55jxwb0xa9g1wy809cp11q42")))

(define-public crate-vrp-scientific-1.2.3 (c (n "vrp-scientific") (v "1.2.3") (d (list (d (n "vrp-core") (r "^1.2.3") (d #t) (k 0)))) (h "1vb9lb4qkfx4cir5rbrc0vn36hv2vs7wvmm6qvf8fd3ykzzl71ig")))

(define-public crate-vrp-scientific-1.2.4 (c (n "vrp-scientific") (v "1.2.4") (d (list (d (n "vrp-core") (r "^1.2.4") (d #t) (k 0)))) (h "0m6fm0s06lzrpqk9p6cv2fvcj7whssmdbjqk43h6smi0hv4dwsz3")))

(define-public crate-vrp-scientific-1.3.0 (c (n "vrp-scientific") (v "1.3.0") (d (list (d (n "vrp-core") (r "^1.3.0") (d #t) (k 0)))) (h "1f2gaiqck57xvg3vrwn4l1pmhc71gfp4z1vlsdgzc62j4s19ly5j")))

(define-public crate-vrp-scientific-1.4.0 (c (n "vrp-scientific") (v "1.4.0") (d (list (d (n "vrp-core") (r "^1.4.0") (d #t) (k 0)))) (h "0cm0m0djjiyyx4l2cdym31ngccjdf5dm6zj6rwzssajhdm7xh294")))

(define-public crate-vrp-scientific-1.4.1 (c (n "vrp-scientific") (v "1.4.1") (d (list (d (n "vrp-core") (r "^1.4.1") (d #t) (k 0)))) (h "0d633s0abkld6q8vj9dnvz325f5216j4g160w9if5iqmgavlhxgl")))

(define-public crate-vrp-scientific-1.4.2 (c (n "vrp-scientific") (v "1.4.2") (d (list (d (n "vrp-core") (r "^1.4.2") (d #t) (k 0)))) (h "188070yb7ck78pd5r9wjpwa4sclzc1ps3kb5wn59ricbcqs3rv8j")))

(define-public crate-vrp-scientific-1.4.3 (c (n "vrp-scientific") (v "1.4.3") (d (list (d (n "vrp-core") (r "^1.4.3") (d #t) (k 0)))) (h "0xrb4r39y7cgfbnxlfd7570n42x76dihjfjfx94z0nn2bparh1sy")))

(define-public crate-vrp-scientific-1.5.0 (c (n "vrp-scientific") (v "1.5.0") (d (list (d (n "vrp-core") (r "^1.5.0") (d #t) (k 0)))) (h "1vl50rimk316089fghmj9q6rvrsphyc6d5fw4nkld9i8vjpz0whk")))

(define-public crate-vrp-scientific-1.5.1 (c (n "vrp-scientific") (v "1.5.1") (d (list (d (n "vrp-core") (r "^1.5.1") (d #t) (k 0)))) (h "12ssz9njfvkqrjdj1gc5ayc9clsvpfziijbbck7yf0mdv5a1xgmz")))

(define-public crate-vrp-scientific-1.5.2 (c (n "vrp-scientific") (v "1.5.2") (d (list (d (n "vrp-core") (r "^1.5.2") (d #t) (k 0)))) (h "0rszq8sv399n3fbwsnqdwlgwai45sfhhljwr9md5aamp27d35vcb")))

(define-public crate-vrp-scientific-1.5.3 (c (n "vrp-scientific") (v "1.5.3") (d (list (d (n "vrp-core") (r "^1.5.3") (d #t) (k 0)))) (h "0l3piqnvg2jr5yw1b4zsmmbpz0dn4a9jxwl2sr0dsvxr1s2s3z38")))

(define-public crate-vrp-scientific-1.5.4 (c (n "vrp-scientific") (v "1.5.4") (d (list (d (n "vrp-core") (r "^1.5.4") (d #t) (k 0)))) (h "10zy37im9nlk7kc735q5mgxhq9x6pxl0zcdhxwh5zilac5xjg03s")))

(define-public crate-vrp-scientific-1.5.5 (c (n "vrp-scientific") (v "1.5.5") (d (list (d (n "vrp-core") (r "^1.5.5") (d #t) (k 0)))) (h "1qkwn8ywbq02347mhhzd0ckci57yq4x6l7q51y44glla6vynacmg")))

(define-public crate-vrp-scientific-1.5.6 (c (n "vrp-scientific") (v "1.5.6") (d (list (d (n "vrp-core") (r "^1.5.6") (d #t) (k 0)))) (h "0d7b8q6g0pcmgkrnvsf7qwxwwy5r6766gjgs2xsj7cwg1g2yyr83")))

(define-public crate-vrp-scientific-1.5.7 (c (n "vrp-scientific") (v "1.5.7") (d (list (d (n "vrp-core") (r "^1.5.7") (d #t) (k 0)))) (h "0ckf8j3jy3w29d9lsad90iww74kh0wgd3096abhaw13kywnkjdk6")))

(define-public crate-vrp-scientific-1.5.8 (c (n "vrp-scientific") (v "1.5.8") (d (list (d (n "vrp-core") (r "^1.5.8") (d #t) (k 0)))) (h "0r4m5l7dsk01rxhll44nr3i99mmn8hq9a58a2nbg899yzhz7gk8p")))

(define-public crate-vrp-scientific-1.6.0 (c (n "vrp-scientific") (v "1.6.0") (d (list (d (n "vrp-core") (r "^1.6.0") (d #t) (k 0)))) (h "03az6hapbhwclnl4jmwwxbhpdjlkrx3cv0wbqq7gxjp943s5fhjq")))

(define-public crate-vrp-scientific-1.6.1 (c (n "vrp-scientific") (v "1.6.1") (d (list (d (n "vrp-core") (r "^1.6.1") (d #t) (k 0)))) (h "16khkbf4c7vm5i6s1yhdsil37p9h16wbpm6srg74wxd3y6ky7h2z")))

(define-public crate-vrp-scientific-1.6.2 (c (n "vrp-scientific") (v "1.6.2") (d (list (d (n "vrp-core") (r "^1.6.2") (d #t) (k 0)))) (h "1wsv3xxm1bvnc1id6bpvw8g24nvkijvfb08lr8jj49i98dcnw02z")))

(define-public crate-vrp-scientific-1.6.3 (c (n "vrp-scientific") (v "1.6.3") (d (list (d (n "vrp-core") (r "^1.6.3") (d #t) (k 0)))) (h "0z87xhl66lmb8c311qifpzl2kfbjmrxgwvaf9yrh9xcz59ykn7hh")))

(define-public crate-vrp-scientific-1.6.4 (c (n "vrp-scientific") (v "1.6.4") (d (list (d (n "vrp-core") (r "^1.6.4") (d #t) (k 0)))) (h "0mhx6l9k6gdr5q035pzssycpxw2g96ayv59wdf8rcpz8rv1kkpb2")))

(define-public crate-vrp-scientific-1.7.0 (c (n "vrp-scientific") (v "1.7.0") (d (list (d (n "vrp-core") (r ">=1.7.0, <2.0.0") (d #t) (k 0)))) (h "1dn3aab86xmqaccq0m5dnd7pfqcln3630crf533vnkwc7vjfi1y0")))

(define-public crate-vrp-scientific-1.7.1 (c (n "vrp-scientific") (v "1.7.1") (d (list (d (n "vrp-core") (r "^1.7.1") (d #t) (k 0)))) (h "1p7m50kq5xxjdn4p07mw8h3f313clf1fni8bf063m3qrz7ns4g93")))

(define-public crate-vrp-scientific-1.7.2 (c (n "vrp-scientific") (v "1.7.2") (d (list (d (n "vrp-core") (r "^1.7.2") (d #t) (k 0)))) (h "036awbfvnv8i5szj2j3lngsscc57i1ddpm6rpd8g6qndcizwqzi4")))

(define-public crate-vrp-scientific-1.7.3 (c (n "vrp-scientific") (v "1.7.3") (d (list (d (n "vrp-core") (r "^1.7.3") (d #t) (k 0)))) (h "09ia70j0pvjw89m06l437f1d3yqqzyr85hb5ws8aycvvjmw6wxx7")))

(define-public crate-vrp-scientific-1.7.4 (c (n "vrp-scientific") (v "1.7.4") (d (list (d (n "vrp-core") (r "^1.7.4") (d #t) (k 0)))) (h "1c9pcxgw3fspadjih827vdx4v05mgmkn5aqvni6n3mnn6wngxixi")))

(define-public crate-vrp-scientific-1.8.0 (c (n "vrp-scientific") (v "1.8.0") (d (list (d (n "vrp-core") (r "^1.8.0") (d #t) (k 0)))) (h "057dxdfzn220i0np23pldgwy0a7r9zycagcwjnc21isk1ld9jl87")))

(define-public crate-vrp-scientific-1.8.1 (c (n "vrp-scientific") (v "1.8.1") (d (list (d (n "vrp-core") (r "^1.8.1") (d #t) (k 0)))) (h "11fqxalfw0dkcnxg13f5f0gd257vj8mkpxxfcnagj1pik2s18rph")))

(define-public crate-vrp-scientific-1.9.0 (c (n "vrp-scientific") (v "1.9.0") (d (list (d (n "vrp-core") (r "^1.9.0") (d #t) (k 0)))) (h "1y7l5lbsw2dvja4xnwn4qjwzjr2xdf2zyzgwqdlgl60srw5kcvyf")))

(define-public crate-vrp-scientific-1.9.1 (c (n "vrp-scientific") (v "1.9.1") (d (list (d (n "vrp-core") (r "^1.9.1") (d #t) (k 0)))) (h "1r0k55xnfnx0dbwz7y93sv9nf70cvsmbq5467cgazj8agryrvjbk")))

(define-public crate-vrp-scientific-1.10.0 (c (n "vrp-scientific") (v "1.10.0") (d (list (d (n "vrp-core") (r "^1.10.0") (d #t) (k 0)))) (h "00al2kaidrfhjgmx5mp1xz5wfckjgm8416pmb9mqyjrk987f311n")))

(define-public crate-vrp-scientific-1.10.1 (c (n "vrp-scientific") (v "1.10.1") (d (list (d (n "vrp-core") (r "^1.10.1") (d #t) (k 0)))) (h "117rarpcqg1jha70h6kaz1j1bmh3rrwvp15p66na0a1wmynizvmf")))

(define-public crate-vrp-scientific-1.10.2 (c (n "vrp-scientific") (v "1.10.2") (d (list (d (n "vrp-core") (r "^1.10.2") (d #t) (k 0)))) (h "0y8jbgjbsc3r3iq06m552rxmpjirmqa2kixqwfv518x9lkyhnb1a")))

(define-public crate-vrp-scientific-1.10.3 (c (n "vrp-scientific") (v "1.10.3") (d (list (d (n "vrp-core") (r "^1.10.3") (d #t) (k 0)))) (h "0n5k46pl66jnj59zhn6cj8k81myrnsbhr9lj9p1c7zfls5g8jfwq")))

(define-public crate-vrp-scientific-1.10.4 (c (n "vrp-scientific") (v "1.10.4") (d (list (d (n "vrp-core") (r "^1.10.4") (d #t) (k 0)))) (h "0qini7ddcsx9yw0m6lx4hxry27mdar38sqgj08jmn4lvnx1h2zra")))

(define-public crate-vrp-scientific-1.10.5 (c (n "vrp-scientific") (v "1.10.5") (d (list (d (n "vrp-core") (r "^1.10.5") (d #t) (k 0)))) (h "1jwqs9v7falzrq7n2pxy6hn95z4ll35gv62x79w5i263fdgcv671")))

(define-public crate-vrp-scientific-1.10.6 (c (n "vrp-scientific") (v "1.10.6") (d (list (d (n "vrp-core") (r "^1.10.6") (d #t) (k 0)))) (h "0c7jlqv3y4kn0n5vhd32yd5q37p7z9ry614sx31jvlfx9mrw73qq")))

(define-public crate-vrp-scientific-1.10.7 (c (n "vrp-scientific") (v "1.10.7") (d (list (d (n "vrp-core") (r "^1.10.7") (d #t) (k 0)))) (h "15jb5jll2y7pp08f2m4asw7jqmj9579caf15mrj1jxnjd1xnaszj")))

(define-public crate-vrp-scientific-1.11.0 (c (n "vrp-scientific") (v "1.11.0") (d (list (d (n "vrp-core") (r "^1.11.0") (d #t) (k 0)))) (h "0i84ymckbcjj9fjccyw1y63gni3znihxhkxq156jkc2sbvvsbj69")))

(define-public crate-vrp-scientific-1.11.1 (c (n "vrp-scientific") (v "1.11.1") (d (list (d (n "vrp-core") (r "^1.11.1") (d #t) (k 0)))) (h "1fjznp1rnii8987nzn4jngq1n96ysgfkq6q4zfn8abpbh61gl1kl")))

(define-public crate-vrp-scientific-1.11.2 (c (n "vrp-scientific") (v "1.11.2") (d (list (d (n "vrp-core") (r "^1.11.2") (d #t) (k 0)))) (h "1qahsfqrisbkyr3zxnwwg0pr7qizv04za8xdswp8v3hf1hb49yvg")))

(define-public crate-vrp-scientific-1.11.3 (c (n "vrp-scientific") (v "1.11.3") (d (list (d (n "vrp-core") (r "^1.11.3") (d #t) (k 0)))) (h "1hjlra5w2p5cg3mkb2cl3p6s5gi4s1chx6xa3ngr0ap0bdhcgs68")))

(define-public crate-vrp-scientific-1.11.4 (c (n "vrp-scientific") (v "1.11.4") (d (list (d (n "vrp-core") (r "^1.11.4") (d #t) (k 0)))) (h "0m2kjbxmvs5h9y9kfz8h31xxrc977zavg6h169v1cawq1cw4rn2r")))

(define-public crate-vrp-scientific-1.11.5 (c (n "vrp-scientific") (v "1.11.5") (d (list (d (n "vrp-core") (r "^1.11.5") (d #t) (k 0)))) (h "02n1njn5ra1r686qasmviip98m4hl1xzp8gn92486zls8j5q9a2i")))

(define-public crate-vrp-scientific-1.12.0 (c (n "vrp-scientific") (v "1.12.0") (d (list (d (n "vrp-core") (r "^1.12.0") (d #t) (k 0)))) (h "09f0wl8im6cvbcfl42hw0q8wfnvnil51hg16ib6i1pw5ij3jci4k")))

(define-public crate-vrp-scientific-1.13.0 (c (n "vrp-scientific") (v "1.13.0") (d (list (d (n "vrp-core") (r "^1.13.0") (d #t) (k 0)))) (h "0hqn4pq504cyx4r7vfql3smil9f5p367kdfqrhr9w58qr5v195l3")))

(define-public crate-vrp-scientific-1.14.0 (c (n "vrp-scientific") (v "1.14.0") (d (list (d (n "vrp-core") (r "^1.14.0") (d #t) (k 0)))) (h "1gcjr17n048hrn95g6xhg9ia09m4gsid5cdcxs8lrv793lh23rl8")))

(define-public crate-vrp-scientific-1.15.0 (c (n "vrp-scientific") (v "1.15.0") (d (list (d (n "vrp-core") (r "^1.15.0") (d #t) (k 0)))) (h "05k64yxz7i6118n9faypvxvjxwgaw41km64xkzwzdacl8mbpa148")))

(define-public crate-vrp-scientific-1.16.0 (c (n "vrp-scientific") (v "1.16.0") (d (list (d (n "vrp-core") (r "^1.16.0") (d #t) (k 0)))) (h "1ivcgigrav8pkj7z4ncxm0cfd65h639hbln0mk0a3nwpwz2nk69x")))

(define-public crate-vrp-scientific-1.16.1 (c (n "vrp-scientific") (v "1.16.1") (d (list (d (n "vrp-core") (r "^1.16.1") (d #t) (k 0)))) (h "1cd8cb0sqriyrnpi6v9w6w74f9svbkjh6b8krv4hkj1lh29vxkjd")))

(define-public crate-vrp-scientific-1.17.0 (c (n "vrp-scientific") (v "1.17.0") (d (list (d (n "vrp-core") (r "^1.17.0") (d #t) (k 0)))) (h "1c1wilbhjk5sk2jg2mvzix5qmz7pm80wqx39zxlf6283b60l94mc")))

(define-public crate-vrp-scientific-1.18.0 (c (n "vrp-scientific") (v "1.18.0") (d (list (d (n "vrp-core") (r "^1.18.0") (d #t) (k 0)))) (h "1398ir2xc9nn43rgq3r05c4p7wsz3xb06q9f6ddkin2a7xn7fxf5")))

(define-public crate-vrp-scientific-1.18.1 (c (n "vrp-scientific") (v "1.18.1") (d (list (d (n "vrp-core") (r "^1.18.1") (d #t) (k 0)))) (h "1g8dkddmmxj537dk0hbqjrlz8ls7cm99v9ki1zfkmdmr6a002h8q")))

(define-public crate-vrp-scientific-1.18.2 (c (n "vrp-scientific") (v "1.18.2") (d (list (d (n "vrp-core") (r "^1.18.2") (d #t) (k 0)))) (h "0q54nvhsfvlz8w64f081k9mfkjb6k7fvjwp1l4v4w27jcrz8i6fk")))

(define-public crate-vrp-scientific-1.18.3 (c (n "vrp-scientific") (v "1.18.3") (d (list (d (n "vrp-core") (r "^1.18.3") (d #t) (k 0)))) (h "15xn1kz9w3aapwfv1j0anj5syvj66h0n66nr5qpjwi2dzjnvr3jx")))

(define-public crate-vrp-scientific-1.18.4 (c (n "vrp-scientific") (v "1.18.4") (d (list (d (n "vrp-core") (r "^1.18.4") (d #t) (k 0)))) (h "1w07yl05m1d6i0c3zxvypjlwgcwsd8qjnfa6bbvpdh8lxxq4h5sc")))

(define-public crate-vrp-scientific-1.19.0 (c (n "vrp-scientific") (v "1.19.0") (d (list (d (n "vrp-core") (r "^1.19.0") (d #t) (k 0)))) (h "014mpn73rrri4yqjyzyfj3xzlxd9m7az87pl6hzf1ls3cv69ka0z")))

(define-public crate-vrp-scientific-1.19.1 (c (n "vrp-scientific") (v "1.19.1") (d (list (d (n "vrp-core") (r "^1.19.1") (d #t) (k 0)))) (h "1fpf96d2diqlf50ljjv8p6ll8q7nlhgbl21sp363x0a0dhdcr9k9")))

(define-public crate-vrp-scientific-1.19.2 (c (n "vrp-scientific") (v "1.19.2") (d (list (d (n "vrp-core") (r "^1.19.2") (d #t) (k 0)))) (h "18hc83v3x560x5y92ha4czx32ysyid9fbk5dapx3xkk3sq6pr8yi")))

(define-public crate-vrp-scientific-1.20.0 (c (n "vrp-scientific") (v "1.20.0") (d (list (d (n "vrp-core") (r "^1.20.0") (d #t) (k 0)))) (h "177gnng7i9sas82pzcg5dvbgw4cawiwmnfi14kbg1fsap6dyfbz1")))

(define-public crate-vrp-scientific-1.21.0 (c (n "vrp-scientific") (v "1.21.0") (d (list (d (n "vrp-core") (r "^1.21.0") (d #t) (k 0)))) (h "1fs8ss0j2caha0nk0x4gc80q1fpfxg2a7nd5q3dx2rjc0g3ahz2x")))

(define-public crate-vrp-scientific-1.21.1 (c (n "vrp-scientific") (v "1.21.1") (d (list (d (n "vrp-core") (r "^1.21.1") (d #t) (k 0)))) (h "1810niyjzlwn1rv5jg6v1751wv65a5z0brwkimk1ayhvmfxlfxbq")))

(define-public crate-vrp-scientific-1.22.0 (c (n "vrp-scientific") (v "1.22.0") (d (list (d (n "vrp-core") (r "^1.22.0") (d #t) (k 0)))) (h "15vzh3yllg9c1mr3yln510sqs4j4pp5w2b455zjbmq4kcw3dbkx7")))

(define-public crate-vrp-scientific-1.22.1 (c (n "vrp-scientific") (v "1.22.1") (d (list (d (n "vrp-core") (r "^1.22.1") (d #t) (k 0)))) (h "0ld915591lycay16i0nk9pybpw13xf5an3fv85zwc453ck5lrn34")))

(define-public crate-vrp-scientific-1.23.0 (c (n "vrp-scientific") (v "1.23.0") (d (list (d (n "vrp-core") (r "^1.23.0") (d #t) (k 0)))) (h "18fjaiq72ycp0zp79qbad50r0ryhqddarfdrxdzsrwcp772wvb3k")))

