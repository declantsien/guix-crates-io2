(define-module (crates-io vr gi vrgit) #:use-module (crates-io))

(define-public crate-vrgit-0.1.0 (c (n "vrgit") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (f (quote ("win32"))) (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0063gf3p006c43zd6251w12v0aqa4ldq4mm6g5swr9w2s2pcka8a")))

