(define-module (crates-io vr ch vrchat-log) #:use-module (crates-io))

(define-public crate-vrchat-log-0.1.0 (c (n "vrchat-log") (v "0.1.0") (d (list (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "10asj17rx7dgy9c6prsykwad5lnwk6kix9d458fdyqhvc9pmzw3i")))

(define-public crate-vrchat-log-0.1.1 (c (n "vrchat-log") (v "0.1.1") (d (list (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "1wcajzzl3cd2p34y4zm5f6r8wzql2n6pgclavncbaj8szv1vmvln")))

(define-public crate-vrchat-log-0.1.2 (c (n "vrchat-log") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "0q29l94xwhwx5x5lhix2wvvr9kaj7z6q8qk0d8mhk55r23w0a1bi")))

(define-public crate-vrchat-log-0.1.3 (c (n "vrchat-log") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "0p3xk0yq41hxaz53zy1zz9n1ww82wb5lckhbcidc9sbzdmv8q1cl")))

(define-public crate-vrchat-log-0.1.4 (c (n "vrchat-log") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)))) (h "06k9ap2v2zjv78cbd5jxwck0kawimjflfy0nk8dsnbsxmw2c90wv")))

