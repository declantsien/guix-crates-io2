(define-module (crates-io vr f- vrf-r255) #:use-module (crates-io))

(define-public crate-vrf-r255-0.0.0 (c (n "vrf-r255") (v "0.0.0") (h "1zlk71cqzrhaa6krj6c28dwh672hkvpwiampgpi83c3vs9z3kw27")))

(define-public crate-vrf-r255-0.1.0 (c (n "vrf-r255") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^4") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "0yc3gbxaml6fxq9akn5jhgnc0khfmy98zijbd8zzrf677h47qbpc") (r "1.60")))

