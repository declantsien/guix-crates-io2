(define-module (crates-io vr f- vrf-mod) #:use-module (crates-io))

(define-public crate-vrf-mod-0.1.0 (c (n "vrf-mod") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qqlnacg6vr1x3vk50wjrzfky8hj9zamh9aa4fz55cnzw80m9pdx") (y #t)))

(define-public crate-vrf-mod-0.1.1 (c (n "vrf-mod") (v "0.1.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gb1rdy8lwkh925ap1rpmy2s26vl0m0d5hpclyida2xdnfrl3xsw")))

(define-public crate-vrf-mod-0.1.2 (c (n "vrf-mod") (v "0.1.2") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vz912hm69rm9zs137vafv1zyqkwk550c9lrlihc6ar0dcy8gcyj")))

