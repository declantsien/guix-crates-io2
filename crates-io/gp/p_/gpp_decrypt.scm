(define-module (crates-io gp p_ gpp_decrypt) #:use-module (crates-io))

(define-public crate-gpp_decrypt-0.1.0 (c (n "gpp_decrypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.35") (d #t) (k 0)))) (h "0pnx2fvszz0z0sfaw41hqia8lgrzwdflx4wqf7m22dikw5a4vdlx")))

(define-public crate-gpp_decrypt-0.1.1 (c (n "gpp_decrypt") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.35") (d #t) (k 0)))) (h "0zmajmq6fb7ibjx9idmw9s9q1ywimd0ni6bjndx5v91vff0rah0r")))

