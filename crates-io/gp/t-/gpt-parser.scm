(define-module (crates-io gp t- gpt-parser) #:use-module (crates-io))

(define-public crate-gpt-parser-0.0.1 (c (n "gpt-parser") (v "0.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "simple_endian") (r "^0.2") (d #t) (k 0)))) (h "1mx4wxn5fzahlkhhpfs1dqapdy4fxyzqcgyfihmcdwkxxq4vry7i") (f (quote (("no_std") ("default" "no_std"))))))

(define-public crate-gpt-parser-0.0.5 (c (n "gpt-parser") (v "0.0.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "simple_endian") (r "^0.2") (d #t) (k 0)))) (h "1vs1qilg8hpnbbrmzlsmbv8vpf7m8qx99f5amjyp94dy47d5fv7i") (f (quote (("no_std") ("default" "no_std"))))))

(define-public crate-gpt-parser-0.0.6 (c (n "gpt-parser") (v "0.0.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "002q6pk05h5iry4rxwx10pqrm89rldaxci92h7ksg6zkdc0hlbcp") (f (quote (("no_std"))))))

(define-public crate-gpt-parser-0.0.7 (c (n "gpt-parser") (v "0.0.7") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "explicit-endian") (r "^0.1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "116dzbz9wrgwzz2zgrvm04ri1cm7m6f008vh4dj5bppmw3wmca2z") (f (quote (("no_std"))))))

(define-public crate-gpt-parser-0.0.8 (c (n "gpt-parser") (v "0.0.8") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "explicit-endian") (r "^0.1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "0xi44cjydpfpqp4s6v7hxrrs2yqpv1ipqflfqhvcj0a4yp4jqqn7") (f (quote (("no_std"))))))

(define-public crate-gpt-parser-0.0.9 (c (n "gpt-parser") (v "0.0.9") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "explicit-endian") (r "^0.1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "02wam3xjgvc9f9bn1hj6klm3qlhvjva3y3k9ll9aan0bsin5gixh") (f (quote (("no_std"))))))

(define-public crate-gpt-parser-0.0.10 (c (n "gpt-parser") (v "0.0.10") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "explicit-endian") (r "^0.1.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "0z8nrg607hfj6a3yivcygja6pkwipwvdl421r9klxz0s5mc8mbn9") (f (quote (("no_std"))))))

