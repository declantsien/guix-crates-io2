(define-module (crates-io gp t- gpt-doc-gen) #:use-module (crates-io))

(define-public crate-gpt-doc-gen-0.1.0 (c (n "gpt-doc-gen") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.10.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06mmf6rrfqswjr9nlphinia7mnnaagqx1rards7a8j90p0yqbg3m")))

