(define-module (crates-io gp t- gpt-partition) #:use-module (crates-io))

(define-public crate-gpt-partition-0.1.0 (c (n "gpt-partition") (v "0.1.0") (d (list (d (n "gpt") (r "^3.0.0") (d #t) (k 0)))) (h "0jfd2vf81563dfflwwnsdvdzb1llr6l6yhdk32ckgab7vdw9jpbi")))

(define-public crate-gpt-partition-0.1.1 (c (n "gpt-partition") (v "0.1.1") (d (list (d (n "gpt") (r "^3.0.0") (d #t) (k 0)))) (h "0iypqn35agcl2kbk9rsvy12lcjkh6islsgbjhfszj0nsxjnc0mry")))

(define-public crate-gpt-partition-0.1.2 (c (n "gpt-partition") (v "0.1.2") (d (list (d (n "crc32fast") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "gpt") (r "^3.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0") (o #t) (d #t) (k 0)) (d (n "log") (r ">=0") (o #t) (d #t) (k 0)))) (h "00hfzx2mnwrw7j26yn5jfbqg7irw4hc89vf4zgyig9nxkarxg4rw") (f (quote (("gpt_header_fixup" "crc32fast" "log" "libc"))))))

