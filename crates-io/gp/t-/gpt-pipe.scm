(define-module (crates-io gp t- gpt-pipe) #:use-module (crates-io))

(define-public crate-gpt-pipe-0.1.0 (c (n "gpt-pipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)))) (h "1xr3jqqghmd0gkpkwr8p2wbxflr0c9km2n2kl5w6nars2xnkvq7h")))

(define-public crate-gpt-pipe-0.1.1 (c (n "gpt-pipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)))) (h "1l76ylrpsx8sdbl1kpq1s565nixhhds4vnxb1b70scb5qa3sd59x")))

(define-public crate-gpt-pipe-0.1.2 (c (n "gpt-pipe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "170dcdfwhsw7ilcdp1h8n0yv414i71nkngzdj4ha2p0zdrgjgy4h")))

(define-public crate-gpt-pipe-0.1.3 (c (n "gpt-pipe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "1mw0d3zybk8r56bw4jx8zs67lk3l9x9lgfbbb8y8pz5kfvx8i4mj")))

(define-public crate-gpt-pipe-0.2.0 (c (n "gpt-pipe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "0xvaqgzz78nbwxzj2yz69npwm67p0fs4zhmx36sg2xq8blh71i8r")))

