(define-module (crates-io gp t- gpt-model) #:use-module (crates-io))

(define-public crate-gpt-model-0.1.0 (c (n "gpt-model") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.18.4") (d #t) (k 0)))) (h "0xjcnnbxrnwf34rj5wa5cia5mjv6mr8ypnsr0iqkk885dvgb8df5")))

