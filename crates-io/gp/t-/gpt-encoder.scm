(define-module (crates-io gp t- gpt-encoder) #:use-module (crates-io))

(define-public crate-gpt-encoder-0.1.0 (c (n "gpt-encoder") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0dy0k1v8956pn1d2f8rjqfs67n7j2d7iw7r1sadvvmqlk1h894ly") (y #t)))

(define-public crate-gpt-encoder-0.1.1 (c (n "gpt-encoder") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "12whl8qz8fninvz9cizg9k0alzkp5h96fs52802skihdckmc175v")))

