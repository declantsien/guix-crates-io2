(define-module (crates-io gp t- gpt-feeder) #:use-module (crates-io))

(define-public crate-gpt-feeder-0.1.0 (c (n "gpt-feeder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1b8gzqvdd03x69gk40nx6ya15hy2nj9bz3ays98w38bvdfyqb6bz")))

(define-public crate-gpt-feeder-1.0.0 (c (n "gpt-feeder") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "18n1inblahsrkr3rrxvgkr1gldb05158aq4xwxwsf892vqj0iqai")))

(define-public crate-gpt-feeder-1.0.1 (c (n "gpt-feeder") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1q2b5k13v31bh7npkmdkmbrjnvg6zxfvfrrc4zyknqidj0lbmk1a")))

