(define-module (crates-io gp t- gpt-fat-disk-image-tools) #:use-module (crates-io))

(define-public crate-gpt-fat-disk-image-tools-0.1.0 (c (n "gpt-fat-disk-image-tools") (v "0.1.0") (d (list (d (n "mini_fat") (r "^0.1") (d #t) (k 0)) (d (n "mini_gpt") (r "^0.1") (d #t) (k 0)) (d (n "simon") (r "^0.4") (d #t) (k 0)))) (h "0zclg375dj4sk9zibr6dx5hn57d98ls7jds2ff9g7xb6hhdm5ydw")))

