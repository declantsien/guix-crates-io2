(define-module (crates-io gp kg gpkg-core) #:use-module (crates-io))

(define-public crate-gpkg-core-0.1.0 (c (n "gpkg-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.4") (d #t) (k 0)) (d (n "gpkg-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wkb") (r "^0.7.1") (d #t) (k 0)))) (h "1dzwwv7xc60mm5v6g9y05hdmpfsc9378ff2dzhbqx4bf1kjfx6b2") (y #t)))

