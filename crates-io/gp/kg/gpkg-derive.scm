(define-module (crates-io gp kg gpkg-derive) #:use-module (crates-io))

(define-public crate-gpkg-derive-0.1.0 (c (n "gpkg-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1q23z9dn3m5fwnj5wb31gwrk3kvijp5mc3087h96zagxvqx7ykbj")))

(define-public crate-gpkg-derive-0.2.0 (c (n "gpkg-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1k8nqambcq7m3g2bv3c46zvqdwmm4x5n8b9c4djvlss9ykkqhcc7")))

