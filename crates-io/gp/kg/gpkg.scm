(define-module (crates-io gp kg gpkg) #:use-module (crates-io))

(define-public crate-gpkg-0.1.0 (c (n "gpkg") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.4") (d #t) (k 0)) (d (n "gpkg-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wkb") (r "^0.7.1") (d #t) (k 0)))) (h "1yym6frhxmly6panzqga01q7851nzhv7s0yhzw3jpbk5lxnnxwba")))

(define-public crate-gpkg-0.2.0 (c (n "gpkg") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.4") (d #t) (k 0)) (d (n "gpkg-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "wkb") (r "^0.7.1") (d #t) (k 0)))) (h "1j9fis1m6flhnpmqdykznqr52pr85i167yhl4m6f7027x2kc3qdk") (y #t)))

