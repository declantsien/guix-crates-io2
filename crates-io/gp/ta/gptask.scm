(define-module (crates-io gp ta gptask) #:use-module (crates-io))

(define-public crate-gptask-0.1.0 (c (n "gptask") (v "0.1.0") (d (list (d (n "bat") (r "^0.22.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "06sqfrj4j41kl4yjkfm764n3h7nadcs69hf3kgb61d0z4iq4483v")))

(define-public crate-gptask-0.2.0 (c (n "gptask") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "02f7c2g8klqbziilmi8l06grzbq8wvwn9xkjsvjshy3fh2i23z2h")))

