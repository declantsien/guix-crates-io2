(define-module (crates-io gp tx gptx) #:use-module (crates-io))

(define-public crate-gptx-0.1.0 (c (n "gptx") (v "0.1.0") (d (list (d (n "gptrust_api") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vh098llc1s3v59ql3rlhhig35dd0nqv9cqa47mmm3c27rk404fi")))

