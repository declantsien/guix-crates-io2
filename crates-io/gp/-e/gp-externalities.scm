(define-module (crates-io gp -e gp-externalities) #:use-module (crates-io))

(define-public crate-gp-externalities-0.13.0 (c (n "gp-externalities") (v "0.13.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "environmental") (r "^1.1.3") (k 0)) (d (n "sp-std") (r "^5.0.0") (k 0)) (d (n "sp-storage") (r "^7.0.0") (k 0)))) (h "0wsz74p4krqv917sahlqlfb1naajckgx6rxycgls6jffq1ki70dk") (f (quote (("std" "codec/std" "environmental/std" "sp-std/std" "sp-storage/std") ("default" "std"))))))

