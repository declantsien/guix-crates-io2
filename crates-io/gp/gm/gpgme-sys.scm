(define-module (crates-io gp gm gpgme-sys) #:use-module (crates-io))

(define-public crate-gpgme-sys-0.1.0 (c (n "gpgme-sys") (v "0.1.0") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1anlp1mk74jcbyfnc3f6s3xp7rw29dinqm97w1z4257w70baqasf")))

(define-public crate-gpgme-sys-0.1.1 (c (n "gpgme-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0zhvvrqnry8kjs809wjgsvilq5hlbwwfhdlhkdfnlgh8dzkrdrqh")))

(define-public crate-gpgme-sys-0.2.0 (c (n "gpgme-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0blsgiibp9dla642y1cxqisr4y2lgryywa1ipgg8nbiwr73lhch6")))

(define-public crate-gpgme-sys-0.5.0 (c (n "gpgme-sys") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "0m8bfr16r91myckhimb1klbl89haijwwr56lc22g2j9q54yw34gq") (f (quote (("v1_8_0" "v1_7_1") ("v1_7_1" "v1_7_0") ("v1_7_0" "v1_6_0") ("v1_6_0" "v1_5_1") ("v1_5_1" "v1_5_0") ("v1_5_0" "v1_4_3") ("v1_4_3" "v1_4_2") ("v1_4_2" "v1_4_0") ("v1_4_0" "v1_3_1") ("v1_3_1" "v1_3_0") ("v1_3_0") ("default" "v1_8_0"))))))

(define-public crate-gpgme-sys-0.6.0 (c (n "gpgme-sys") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "0vv5bkzcgblmk0rxpab1p27ybw0fgcmvpcwvxwnapfgkjh2s2js4") (f (quote (("v1_9_0" "v1_8_0") ("v1_8_0" "v1_7_1") ("v1_7_1" "v1_7_0") ("v1_7_0" "v1_6_0") ("v1_6_0" "v1_5_1") ("v1_5_1" "v1_5_0") ("v1_5_0" "v1_4_3") ("v1_4_3" "v1_4_2") ("v1_4_2" "v1_4_0") ("v1_4_0" "v1_3_1") ("v1_3_1" "v1_3_0") ("v1_3_0") ("default" "v1_9_0"))))))

(define-public crate-gpgme-sys-0.6.1 (c (n "gpgme-sys") (v "0.6.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "0f3qdnxgqcixk7qmkf1hddmdp97qafi627n0mjzhvmz46673hx1d") (f (quote (("v1_9_0" "v1_8_0") ("v1_8_0" "v1_7_1") ("v1_7_1" "v1_7_0") ("v1_7_0" "v1_6_0") ("v1_6_0" "v1_5_1") ("v1_5_1" "v1_5_0") ("v1_5_0" "v1_4_3") ("v1_4_3" "v1_4_2") ("v1_4_2" "v1_4_0") ("v1_4_0" "v1_3_1") ("v1_3_1" "v1_3_0") ("v1_3_0") ("default" "v1_9_0"))))))

(define-public crate-gpgme-sys-0.7.0 (c (n "gpgme-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "0q6m9jjh99w1yzq755sa3cmgrbfg697jy24pnwmw8dmaavi203wn")))

(define-public crate-gpgme-sys-0.8.0 (c (n "gpgme-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.4") (k 0)))) (h "1jsaxrzc3ay78y324ds2nh40znm9n81nmjfag8xx0b0kg6yz4z3w") (f (quote (("default" "bundled") ("bundled" "libgpg-error-sys/bundled")))) (l "gpgme")))

(define-public crate-gpgme-sys-0.9.0 (c (n "gpgme-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.5") (d #t) (k 0)))) (h "0srdhmwikyap070pw6h4gqpmib5d2w7vi53ijgidg0xcskxyxmhl") (l "gpgme")))

(define-public crate-gpgme-sys-0.9.1 (c (n "gpgme-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.5.1") (d #t) (k 0)))) (h "07z3y9baalsc10kc56nb3xcbklhhgk48pz5yrpa01kv71bj3na8b") (l "gpgme")))

(define-public crate-gpgme-sys-0.10.0 (c (n "gpgme-sys") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "winreg") (r "^0.9") (d #t) (t "cfg(windows)") (k 1)))) (h "0mjp15x3gqvipv4yfvbad1y1mk3vym6zfsblnmdnqk7gsy35jmgr") (l "gpgme")))

(define-public crate-gpgme-sys-0.11.0 (c (n "gpgme-sys") (v "0.11.0") (d (list (d (n "build-rs") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 1)))) (h "1xqwfilq63nmxk38cz4i64nz08vpj6ndcdwl48k4lvn0b7b274jh") (l "gpgme") (r "1.64")))

