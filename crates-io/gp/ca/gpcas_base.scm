(define-module (crates-io gp ca gpcas_base) #:use-module (crates-io))

(define-public crate-gpcas_base-0.1.0 (c (n "gpcas_base") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gjmwg1y54q8pspyb1qriivf90pxcw8g4r3pl6ahqai7g73n0fbg")))

(define-public crate-gpcas_base-0.1.1 (c (n "gpcas_base") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08yadk2r5z45kpj2ly0hnmpfd9ldshbhzwcc439v7d6q4z911al9")))

(define-public crate-gpcas_base-0.2.0 (c (n "gpcas_base") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b7q8g8zkyf5f20qnwx8ifh2qz585a7ds9myq8l5j0ix2iq1r20q")))

(define-public crate-gpcas_base-0.3.0 (c (n "gpcas_base") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fm2kypjw7rpa0qxn80v778n70nri6d8znn0bvpfajk2vrz2cjlc")))

(define-public crate-gpcas_base-0.3.1 (c (n "gpcas_base") (v "0.3.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02r8cdr47wibxrpjgg4fi9n0ln1zp7b4r8agnglvgpdbgcqjygva") (y #t)))

