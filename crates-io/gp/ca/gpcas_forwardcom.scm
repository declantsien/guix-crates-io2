(define-module (crates-io gp ca gpcas_forwardcom) #:use-module (crates-io))

(define-public crate-gpcas_forwardcom-0.1.0 (c (n "gpcas_forwardcom") (v "0.1.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "half") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "142ly1a3iykbva3lpc55c43zm6c8wiwzlajgirmnq7clr6qbmk5j")))

(define-public crate-gpcas_forwardcom-0.1.1 (c (n "gpcas_forwardcom") (v "0.1.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "half") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17l47ddg6pf1d84m39qw9xm9kpnb8bm5scgsl2l1x2sla0kfn7pm")))

(define-public crate-gpcas_forwardcom-0.1.2 (c (n "gpcas_forwardcom") (v "0.1.2") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "half") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15vmr02x7yyb2jbw2gid9w07ppp98yzkrmzjgwr2n77yhy6qg4v0")))

(define-public crate-gpcas_forwardcom-0.2.0 (c (n "gpcas_forwardcom") (v "0.2.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "half") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "167jdanlabic92rbiprgagbazlyd78pqzdy9mv87yrnwkpfcva9r")))

(define-public crate-gpcas_forwardcom-0.2.1 (c (n "gpcas_forwardcom") (v "0.2.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "half") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04lb95l633swwdf1by43dspi6hywa7a1141vnb7s9q0w15p54vm2") (r "1.67")))

