(define-module (crates-io gp ca gpcas_cpu_model) #:use-module (crates-io))

(define-public crate-gpcas_cpu_model-0.1.0 (c (n "gpcas_cpu_model") (v "0.1.0") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j53bq4abpdixrf73wnp91p06sxsy4jchh7qkk8hnq2ns9ckihy9")))

(define-public crate-gpcas_cpu_model-0.1.1 (c (n "gpcas_cpu_model") (v "0.1.1") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z0rv8nrjpjhwlpx8lcmm6f046ap4n3f3wh9jzy7lah1ris3gjbx")))

(define-public crate-gpcas_cpu_model-0.2.0 (c (n "gpcas_cpu_model") (v "0.2.0") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cjlvnc54yldhj1p8phm4dr5x3hghkz3s183cfcrdamzxk63vzk8") (y #t)))

(define-public crate-gpcas_cpu_model-0.2.1 (c (n "gpcas_cpu_model") (v "0.2.1") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aa91gf51f9q4w8nfh9y40ixmsb102jfds569rijjc0ravzc25y0")))

(define-public crate-gpcas_cpu_model-0.3.0 (c (n "gpcas_cpu_model") (v "0.3.0") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07k5knf724230j8hjjh5gr0zp0qdjxhnwgh6fq06r694wjm27w8k") (y #t)))

(define-public crate-gpcas_cpu_model-0.3.1 (c (n "gpcas_cpu_model") (v "0.3.1") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a77bayrr67wn48qiwd26v07z5rqq4m4ycv2kmdd9h8jsimw803b")))

(define-public crate-gpcas_cpu_model-0.3.2 (c (n "gpcas_cpu_model") (v "0.3.2") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rqkmg9hkkywdj8nkh48jhcymjqgb62zlvzxarxr5vs1d50jqdxm")))

(define-public crate-gpcas_cpu_model-0.3.3 (c (n "gpcas_cpu_model") (v "0.3.3") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sa1d0bvn7z7z13h63h5bzm4661fg3k2ixbjfjqkivxhylysprkg")))

(define-public crate-gpcas_cpu_model-0.4.0 (c (n "gpcas_cpu_model") (v "0.4.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1djh360028kxck5qmh6kps0lq4g8gbyslk5d9hflndplizapwpid")))

(define-public crate-gpcas_cpu_model-0.4.1 (c (n "gpcas_cpu_model") (v "0.4.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j7sfqyp7g48mvf6l8miwika5ymyqh1rcbh2cgpc0f5xzs8da9fr")))

(define-public crate-gpcas_cpu_model-0.5.0 (c (n "gpcas_cpu_model") (v "0.5.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0whd7snahip1182b1wgw15kml3nhwz51j5jkhwb2x96rysk50h4j")))

(define-public crate-gpcas_cpu_model-0.5.1 (c (n "gpcas_cpu_model") (v "0.5.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g2hzg8h3qws1f4d2rw8fxmhw3fkm7xpf679zyj13k7smi2ns3c5")))

(define-public crate-gpcas_cpu_model-0.6.0 (c (n "gpcas_cpu_model") (v "0.6.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13gmxbxg1p3vcixa8f66xjvz3bldi4b2qvjy8njs6dsh6jf15psg")))

(define-public crate-gpcas_cpu_model-0.6.1 (c (n "gpcas_cpu_model") (v "0.6.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ip8f4lzvhrv738gpl8q3rw96ybxjh1nnvi5pidh335mrkq2rn14")))

(define-public crate-gpcas_cpu_model-0.7.0 (c (n "gpcas_cpu_model") (v "0.7.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "193yvipzqgzygyvfgyzq0hafvxd448nx674bdlih1siz3fxy44iy")))

(define-public crate-gpcas_cpu_model-0.7.1 (c (n "gpcas_cpu_model") (v "0.7.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19gv1xj8pyqbhw0prrv7falf65gyxrfnf3qw1kiifrwjgq9jkx2y")))

