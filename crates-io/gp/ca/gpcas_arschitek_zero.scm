(define-module (crates-io gp ca gpcas_arschitek_zero) #:use-module (crates-io))

(define-public crate-gpcas_arschitek_zero-0.1.0 (c (n "gpcas_arschitek_zero") (v "0.1.0") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.1") (d #t) (k 0)))) (h "1s06049278mm3frb49m04is5sw96glwq6b2xz9kpvl2gjx1dvabv")))

(define-public crate-gpcas_arschitek_zero-0.1.1 (c (n "gpcas_arschitek_zero") (v "0.1.1") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.1") (d #t) (k 0)))) (h "1rd9smyv9xav13a5nqk6340yx6847dsz9ibpa9cgjb10nx8vqbj5")))

(define-public crate-gpcas_arschitek_zero-0.1.2 (c (n "gpcas_arschitek_zero") (v "0.1.2") (d (list (d (n "gpcas_base") (r "^0.1") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.1") (d #t) (k 0)))) (h "0sanx5hniv0np77l8pp0w9gwladqklyv92ciz55c6yrcgw55flh9")))

(define-public crate-gpcas_arschitek_zero-0.1.3 (c (n "gpcas_arschitek_zero") (v "0.1.3") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.1") (d #t) (k 0)))) (h "1lbzx53j1qrw4cwyd377x2798yqzp6x8jlvqxawd3a9m60fv395q")))

(define-public crate-gpcas_arschitek_zero-0.1.4 (c (n "gpcas_arschitek_zero") (v "0.1.4") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.1.2") (d #t) (k 0)))) (h "1l52lpcs3cf8nlgkrlid0lkx989w4pkawg7dqr68gby9x2kblsnc")))

(define-public crate-gpcas_arschitek_zero-0.2.0 (c (n "gpcas_arschitek_zero") (v "0.2.0") (d (list (d (n "gpcas_base") (r "^0.2") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.2.2") (d #t) (k 0)))) (h "006228qcwfh958gzcsv9q5qp9gbj5s63jmgnz7rmkqlgqlvb4555")))

(define-public crate-gpcas_arschitek_zero-0.3.0 (c (n "gpcas_arschitek_zero") (v "0.3.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.3") (d #t) (k 0)))) (h "0pdvpmr0gkhxjdkqqjms64j6m6p6lqbhiy6ls2s9frgn2z677028")))

(define-public crate-gpcas_arschitek_zero-0.3.1 (c (n "gpcas_arschitek_zero") (v "0.3.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.3") (d #t) (k 0)))) (h "1w9x9f97wqywgdwmvfghah17a7ww9sf0rblb8yqrg79i03gqymz6")))

(define-public crate-gpcas_arschitek_zero-0.4.0 (c (n "gpcas_arschitek_zero") (v "0.4.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.4") (d #t) (k 0)))) (h "1jv7fnpfa7l106h0b6y05ipc34p3rmrfgmwwjvb6gryz3axaznkm")))

(define-public crate-gpcas_arschitek_zero-0.4.1 (c (n "gpcas_arschitek_zero") (v "0.4.1") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.5") (d #t) (k 0)))) (h "1l5lqmwncp39l6b8y060zf86ikp62gkpw5mg4pds3pv9j03ww8gn")))

(define-public crate-gpcas_arschitek_zero-0.5.0 (c (n "gpcas_arschitek_zero") (v "0.5.0") (d (list (d (n "gpcas_base") (r "^0.3") (d #t) (k 0)) (d (n "gpcas_isa") (r "^0.6") (d #t) (k 0)))) (h "0hn1al78vv1d1wik9n2x1bykzzband0977pr6lyzda5zd4y6nsgs")))

