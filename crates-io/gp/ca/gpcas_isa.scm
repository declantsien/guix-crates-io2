(define-module (crates-io gp ca gpcas_isa) #:use-module (crates-io))

(define-public crate-gpcas_isa-0.1.0 (c (n "gpcas_isa") (v "0.1.0") (h "0yqi6bjrl2m83fb14hz14l6rxm11b2r4784mscr7nxjl0amv4xjg")))

(define-public crate-gpcas_isa-0.1.1 (c (n "gpcas_isa") (v "0.1.1") (h "1cc0rg8frq1v7ylq3z6mjzx4fwqx9kfx8mc8srdry1lbjm1nda8i")))

(define-public crate-gpcas_isa-0.1.2 (c (n "gpcas_isa") (v "0.1.2") (h "00n5sd0v3llhw04cp9956v9py28z5sxcpxv8i690v4vfpshjhdn3")))

(define-public crate-gpcas_isa-0.2.0 (c (n "gpcas_isa") (v "0.2.0") (h "0ccmvs61yvrnq8d1zxs6pbkb16fpav3va4kfkgrpr194awa3wwvg") (y #t)))

(define-public crate-gpcas_isa-0.2.1 (c (n "gpcas_isa") (v "0.2.1") (h "1ysfmr7hw3s1jgbk8w6irmfrnwwf783blgn3l8m5f0jvs707z9v5")))

(define-public crate-gpcas_isa-0.2.2 (c (n "gpcas_isa") (v "0.2.2") (h "0vlvk0ds4d7xky6ykjpjw8jlh7zpdvkw5i9c71vyzsnq76wgn41v")))

(define-public crate-gpcas_isa-0.3.0 (c (n "gpcas_isa") (v "0.3.0") (h "0ngv3n0c6azddlqs4cjg7fm93yy355hh30pc70jah2izi9jg8py6")))

(define-public crate-gpcas_isa-0.4.0 (c (n "gpcas_isa") (v "0.4.0") (h "0hgdix5g0j01aky3i78wl7bqgxpd13q91j5lifmqh4qhza881yqb")))

(define-public crate-gpcas_isa-0.5.0 (c (n "gpcas_isa") (v "0.5.0") (h "096hgavcjk9cm1s07dmi2wclva8hpcfmbpdlzsdj8cfwwyv3jpfs")))

(define-public crate-gpcas_isa-0.6.0 (c (n "gpcas_isa") (v "0.6.0") (h "1siyp7fv28w9f19ghzdnn3lsszjbyphl8al9sii1dxmm2rgfa7ng")))

(define-public crate-gpcas_isa-0.6.1 (c (n "gpcas_isa") (v "0.6.1") (h "1f1l7pma679x5cy7lhhyid0f8jb5r811lsj97if633g7r32p2qrg")))

