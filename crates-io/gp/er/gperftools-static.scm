(define-module (crates-io gp er gperftools-static) #:use-module (crates-io))

(define-public crate-gperftools-static-0.1.0 (c (n "gperftools-static") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "gperftools") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "09nxrvlkxxbdvm11jqkpza8m47p1gap9gq25j87gs5q9j6qkbw20") (f (quote (("static_unwind") ("static_gperftools") ("default"))))))

(define-public crate-gperftools-static-0.1.1 (c (n "gperftools-static") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "gperftools") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "0y6s2fggrrm4sc2mhqkh174l38zpc0h6y2kg670yw5xh3xwy426y") (f (quote (("static_unwind") ("static_gperftools") ("default"))))))

(define-public crate-gperftools-static-0.1.2 (c (n "gperftools-static") (v "0.1.2") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "gperftools") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "05idhpzhkmbisn9pssfgkd4bhmr4v0r2w2qpzqyalj8ld4phrw3p") (f (quote (("static_unwind") ("static_gperftools") ("default"))))))

