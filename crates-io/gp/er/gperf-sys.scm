(define-module (crates-io gp er gperf-sys) #:use-module (crates-io))

(define-public crate-gperf-sys-0.1.0 (c (n "gperf-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2.1") (d #t) (k 1)))) (h "1jqq0fwgz1ws5wkc84hkib0nyzbsivh0rnl7la6a08p13n0z6xxz") (y #t)))

(define-public crate-gperf-sys-0.2.0 (c (n "gperf-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2.1") (d #t) (k 1)))) (h "0s23jf0831blhfxnz3c7l8ihwz6p46w9hp1d9jd4zx3imaf098yb") (y #t) (l "gp")))

