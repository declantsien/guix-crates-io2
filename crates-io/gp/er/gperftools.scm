(define-module (crates-io gp er gperftools) #:use-module (crates-io))

(define-public crate-gperftools-0.2.0 (c (n "gperftools") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17dw7sc2zdfl01sw0y3zay5j11h4j349hsgw5333w8mi31cgr8r0") (f (quote (("heap") ("default"))))))

