(define-module (crates-io gp #{2y}# gp2y1014au) #:use-module (crates-io))

(define-public crate-gp2y1014au-0.1.0 (c (n "gp2y1014au") (v "0.1.0") (d (list (d (n "embedded-hal") (r "0.2.*") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "1.0.*") (d #t) (k 0)))) (h "0x97hiby1n9khkwh4c405ijczqw60z888zrizl390fh65rfsr7iz")))

