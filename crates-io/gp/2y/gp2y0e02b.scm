(define-module (crates-io gp #{2y}# gp2y0e02b) #:use-module (crates-io))

(define-public crate-gp2y0e02b-0.1.0 (c (n "gp2y0e02b") (v "0.1.0") (d (list (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1zr38gas1dllbiip4331f92nq11xnnlsk63hizbl72klj60kc4lz")))

(define-public crate-gp2y0e02b-0.1.1 (c (n "gp2y0e02b") (v "0.1.1") (d (list (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "13li8b68jxzzypsl085sad4wrsicsli5gz0zy4hj42425wdhqdnk")))

(define-public crate-gp2y0e02b-0.2.0 (c (n "gp2y0e02b") (v "0.2.0") (d (list (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0gz4ir7mi4qigjbrl5nvypgcd76wp49kbd3ppsfh796gg5hh6ais")))

(define-public crate-gp2y0e02b-0.2.1 (c (n "gp2y0e02b") (v "0.2.1") (d (list (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1d0phzbypv6kh6i2y10ahj8aaq7nkrsp84phqxfay5nwb6qz5sji")))

