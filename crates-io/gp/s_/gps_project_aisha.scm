(define-module (crates-io gp s_ gps_project_aisha) #:use-module (crates-io))

(define-public crate-GPS_project_aisha-0.1.0 (c (n "GPS_project_aisha") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1xh5aqdnnjcbr5hi3iw66vml86icq3q8y0a9ld5wapnwmx841fbi") (y #t)))

(define-public crate-GPS_project_aisha-0.1.1 (c (n "GPS_project_aisha") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "08akzxmvcs0801sdpb33lf9jqf1vrgv7mf0khkllsd74nfhg4aw7") (y #t)))

(define-public crate-GPS_project_aisha-0.1.2 (c (n "GPS_project_aisha") (v "0.1.2") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0p55gfk1p47qrg8zs7rr1i67k4lxll151f8r2xdyx3vinyj3ik4c") (y #t)))

(define-public crate-GPS_project_aisha-0.1.3 (c (n "GPS_project_aisha") (v "0.1.3") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0xvjsdcrzwjxr0nq7lqkkg51bklvw30zl1l4408a57c9y8ml61cy") (y #t)))

(define-public crate-GPS_project_aisha-0.1.4 (c (n "GPS_project_aisha") (v "0.1.4") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1i515da2dznbxhyby4g1zaamdr34637rixvx61vyilf24s3kslkn")))

