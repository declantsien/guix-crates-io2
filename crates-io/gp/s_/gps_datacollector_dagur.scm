(define-module (crates-io gp s_ gps_datacollector_dagur) #:use-module (crates-io))

(define-public crate-GPS_datacollector_dagur-0.1.0 (c (n "GPS_datacollector_dagur") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1v3zi2bx4y5rqhsy891pgp87vwwvd2a1s9giisrkd7qvwrjklbc2")))

(define-public crate-GPS_datacollector_dagur-0.1.1 (c (n "GPS_datacollector_dagur") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0c9lmqvba0nn3cz9mmy9inks5aqipvkxz2gmjybgvszh9gafldxm")))

