(define-module (crates-io gp s_ gps_data_reader_bjarki18) #:use-module (crates-io))

(define-public crate-gps_data_reader_bjarki18-0.1.0 (c (n "gps_data_reader_bjarki18") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "00hmplxvlid2zh04wivsa2n5mdarz4wmwc5a9vd3va7l3arm5hwa") (y #t)))

(define-public crate-gps_data_reader_bjarki18-0.1.1 (c (n "gps_data_reader_bjarki18") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "063fskgw4chz2wpvz65l5i35lv5jqf1rdblqwflxy2yi3mjhigvm") (y #t)))

(define-public crate-gps_data_reader_bjarki18-0.1.2 (c (n "gps_data_reader_bjarki18") (v "0.1.2") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1hlmgjgrdghr68lwr0iyrw98cd64m2nijczc7pr8n7nx739fk212") (y #t)))

(define-public crate-gps_data_reader_bjarki18-0.1.3 (c (n "gps_data_reader_bjarki18") (v "0.1.3") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1f5im4nwskcpqr91s78zyrcq677rkmsabqwqk9sv2icdzcd991v3") (y #t)))

(define-public crate-gps_data_reader_bjarki18-0.1.4 (c (n "gps_data_reader_bjarki18") (v "0.1.4") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "06pzmxg5kl57yh3h3l8y6jkiq1scc4vrrw7y15hwlngbnxvd1yp4")))

