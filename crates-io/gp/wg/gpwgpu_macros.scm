(define-module (crates-io gp wg gpwgpu_macros) #:use-module (crates-io))

(define-public crate-gpwgpu_macros-0.1.0 (c (n "gpwgpu_macros") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "gpwgpu_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.16.1") (d #t) (k 0)))) (h "0s7m4n55yx2kp35mwbxkbq06yxlw8ysb14mf99m8bhxjqb0679md") (f (quote (("nightly"))))))

