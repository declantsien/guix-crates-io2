(define-module (crates-io gp wg gpwg) #:use-module (crates-io))

(define-public crate-gpwg-1.0.0 (c (n "gpwg") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2") (d #t) (k 0)))) (h "1xdp8cs8y9fas3jr5q0mr951gqja2q1ma3gr72zrjw2y5a8qmpyd")))

(define-public crate-gpwg-1.2.0 (c (n "gpwg") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.3.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "zxcvbn") (r "^2") (d #t) (k 0)))) (h "1y5vl2f8q0ab1n3khi2pfafxglpz97qjg9vqqknnzzs3yngmicix")))

