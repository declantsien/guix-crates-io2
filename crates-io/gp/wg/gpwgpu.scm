(define-module (crates-io gp wg gpwgpu) #:use-module (crates-io))

(define-public crate-gpwgpu-0.1.0 (c (n "gpwgpu") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gpwgpu_core") (r "^0.1.0") (d #t) (k 0)) (d (n "gpwgpu_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "slow_primes") (r "^0.1.14") (d #t) (k 2)) (d (n "tiff") (r "^0.9.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.16.1") (d #t) (k 0)))) (h "0q5m9q8qpicaz22nvb7q0hls5b7z37skcp4cb247kdavmjbi7ngf") (f (quote (("nightly" "gpwgpu_macros/nightly"))))))

