(define-module (crates-io gp s- gps-gunnlaug_18) #:use-module (crates-io))

(define-public crate-GPS-Gunnlaug_18-0.1.0 (c (n "GPS-Gunnlaug_18") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0gbw8dbzcfz3klf896dbrbspan4d72p69rynyvdkhzis0nmdmsxn")))

(define-public crate-GPS-Gunnlaug_18-0.1.1 (c (n "GPS-Gunnlaug_18") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1mbkrvkz3d977jq16ynmif9d9x3rdhi5qf81fyg8rgjcpbb5na2q")))

(define-public crate-GPS-Gunnlaug_18-0.1.2 (c (n "GPS-Gunnlaug_18") (v "0.1.2") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1dkn90xy41k6zkch73s3vsr7c5syd0drfvvvjsczhy7bvnqw5sn7")))

(define-public crate-GPS-Gunnlaug_18-0.1.3 (c (n "GPS-Gunnlaug_18") (v "0.1.3") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0z6g039zm0ak0xfbl9r3v8ys4gr8w9s3dj7zmy7ch3njwss80kki")))

(define-public crate-GPS-Gunnlaug_18-0.1.4 (c (n "GPS-Gunnlaug_18") (v "0.1.4") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1a83qxgxr5l2p9f2m6d6xl7zl7lar467w0b2shj71xaywrm737kk")))

(define-public crate-GPS-Gunnlaug_18-0.1.5 (c (n "GPS-Gunnlaug_18") (v "0.1.5") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "14qahy5qx3k7nraah9msqsdy0zg0jd6492dfsm50va8wb8blxs2b")))

(define-public crate-GPS-Gunnlaug_18-0.1.6 (c (n "GPS-Gunnlaug_18") (v "0.1.6") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0h4cgxzvl0mrpcaddlw2l1gxzagxrd9zj1cxhdb0ghvxm62865an")))

