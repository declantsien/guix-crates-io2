(define-module (crates-io gp ro gprofile) #:use-module (crates-io))

(define-public crate-gprofile-0.1.0 (c (n "gprofile") (v "0.1.0") (h "0g813m9dwaw7ayrh8qqmcdq251q5lj2vda1gs59w34jkyfb464i8")))

(define-public crate-gprofile-0.2.0 (c (n "gprofile") (v "0.2.0") (h "1lqmarp5m0np1yyqzryk4xaylh55j843mkyknd0drnbs8p3q453f")))

