(define-module (crates-io gp l- gpl-session-macros-attribute) #:use-module (crates-io))

(define-public crate-gpl-session-macros-attribute-0.1.0 (c (n "gpl-session-macros-attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1wkmk7xkvczjyyjfjrrc5832afnywq29m8ra44fy5pli23kna6fn")))

