(define-module (crates-io gp l- gpl-math) #:use-module (crates-io))

(define-public crate-gpl-math-0.1.0 (c (n "gpl-math") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gemachain-program-test") (r "^1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "^1.8.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "08gqf99v4np7a8wv96y79mrwf1kk29bbfbzsrrjr3ypfaq1f9g57") (f (quote (("test-bpf") ("no-entrypoint"))))))

