(define-module (crates-io gp l- gpl-core) #:use-module (crates-io))

(define-public crate-gpl-core-1.0.0 (c (n "gpl-core") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "gpl-session") (r "^0.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "082b4ji8x189km9ngqgmp72q8l85hvxx8xhrx2apxbaabfymgz81") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

