(define-module (crates-io gp l- gpl-metaplex-token-vault) #:use-module (crates-io))

(define-public crate-gpl-metaplex-token-vault-0.0.1 (c (n "gpl-metaplex-token-vault") (v "0.0.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.7.11") (d #t) (k 0)) (d (n "gpl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xgs6b1d2vjsj39s0sa0gv7wjp14s89ib0lbpv78ps5cjkl4wfdc") (f (quote (("test-bpf") ("no-entrypoint"))))))

