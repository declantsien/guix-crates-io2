(define-module (crates-io gp l- gpl-governance-tools) #:use-module (crates-io))

(define-public crate-gpl-governance-tools-0.1.0 (c (n "gpl-governance-tools") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gpl-token") (r "^3.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06a7x80xw8q0k2fc85bnqp3akbzv86lnxs2bkfrsr4pi2w0qiclh")))

