(define-module (crates-io gp l- gpl-token) #:use-module (crates-io))

(define-public crate-gpl-token-3.2.0 (c (n "gpl-token") (v "3.2.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gemachain-sdk") (r "^1.8.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pp00i2vhnh8zzga4rwmwdkrw8cms3awy3j3chrys8rgbc4j3av8") (f (quote (("no-entrypoint"))))))

