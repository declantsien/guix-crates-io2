(define-module (crates-io gp l- gpl-memo) #:use-module (crates-io))

(define-public crate-gpl-memo-3.0.1 (c (n "gpl-memo") (v "3.0.1") (d (list (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gemachain-program-test") (r "^1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "^1.8.2") (d #t) (k 2)))) (h "02cps12r3g9f4bar8dbdz5sgv76a12sgvw5p7chdabcj0l0hl5zj") (f (quote (("test-bpf") ("no-entrypoint"))))))

