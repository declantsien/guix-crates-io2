(define-module (crates-io gp l- gpl-compression) #:use-module (crates-io))

(define-public crate-gpl-compression-0.1.1 (c (n "gpl-compression") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "gpl-core") (r "^1.0.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)) (d (n "spl-account-compression") (r "^0.1.7") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1vlrq521a37bcl110wasnglj4xzlfbgrjhap6rfndzaniprxx0px") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

