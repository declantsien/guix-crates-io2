(define-module (crates-io gp l- gpl-feature-proposal) #:use-module (crates-io))

(define-public crate-gpl-feature-proposal-1.0.0 (c (n "gpl-feature-proposal") (v "1.0.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gemachain-program-test") (r "^1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "^1.8.2") (d #t) (k 2)) (d (n "gpl-token") (r "^3.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1fk07vdmajk2gsdqr7fy3fkfpwy8c07jjjzd3zls7mn8w82vk6r3") (f (quote (("test-bpf") ("no-entrypoint"))))))

