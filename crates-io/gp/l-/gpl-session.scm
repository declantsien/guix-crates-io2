(define-module (crates-io gp l- gpl-session) #:use-module (crates-io))

(define-public crate-gpl-session-0.2.0 (c (n "gpl-session") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "gpl-session-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "0gjwzh7jrmcqv49dkfcx9shg5ksv869bwg0m9ll8cm6z2ah87j56") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "gpl-session-macros") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-gpl-session-1.0.0 (c (n "gpl-session") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (d #t) (k 0)) (d (n "gpl-session-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "1538w08z0gy0aiykp7vh47a553fzzd2kv21na03vl452sidcibqq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "gpl-session-macros") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-gpl-session-2.0.0 (c (n "gpl-session") (v "2.0.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "gpl-session-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.1.0") (d #t) (k 0)))) (h "0h0dbdijmzcs588bz1wnr38rq09336rv0g2bifbb343mzjjvb4m7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint" "gpl-session-macros") ("default") ("cpi" "no-entrypoint"))))))

