(define-module (crates-io gp l- gpl-associated-token-account) #:use-module (crates-io))

(define-public crate-gpl-associated-token-account-1.0.4 (c (n "gpl-associated-token-account") (v "1.0.4") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "gemachain-program") (r "^1.8.2") (d #t) (k 0)) (d (n "gemachain-program-test") (r "^1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "^1.8.2") (d #t) (k 2)) (d (n "gpl-token") (r "^3.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0s3s7hazx7lafdrwdvxrd01zwspvffbjl3b3n5fpvbxh2yan8y17") (f (quote (("test-bpf") ("no-entrypoint"))))))

