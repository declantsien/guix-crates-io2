(define-module (crates-io gp #{2d}# gp2d12) #:use-module (crates-io))

(define-public crate-gp2d12-0.1.0 (c (n "gp2d12") (v "0.1.0") (d (list (d (n "adc-interpolator") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0zdfkwsh119lxn5n20829xv85i5dga2dxa37nhgq9ajcrijkcsw2")))

(define-public crate-gp2d12-0.2.0 (c (n "gp2d12") (v "0.2.0") (d (list (d (n "adc-interpolator") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0zkpsa9rif6rfdl6028pfmf58r96h0r38p86149flavyrjcqchik")))

(define-public crate-gp2d12-0.3.0 (c (n "gp2d12") (v "0.3.0") (d (list (d (n "adc-interpolator") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "00ngi6q6rjrzkkl4d3a3wmh4rf1y7racqpsr7ccsllcj955as8qp")))

