(define-module (crates-io gp u- gpu-alloc-types) #:use-module (crates-io))

(define-public crate-gpu-alloc-types-0.1.0 (c (n "gpu-alloc-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)))) (h "1szbsdrz3hxrmbaqp44skkaz5arx8sz836xx0i6qfnx1840d79vb")))

(define-public crate-gpu-alloc-types-0.2.0 (c (n "gpu-alloc-types") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)))) (h "19b7zqg0va1lqcr7sj2z66cvasgg1p8imv7aninz5my9dc6lv02l")))

(define-public crate-gpu-alloc-types-0.3.0 (c (n "gpu-alloc-types") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.0") (k 0)))) (h "190wxsp9q8c59xybkfrlzqqyrxj6z39zamadk1q7v0xad2s07zwq")))

