(define-module (crates-io gp u- gpu-checker) #:use-module (crates-io))

(define-public crate-gpu-checker-0.1.0 (c (n "gpu-checker") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "04d45qf6asnm2rbri00lwh84wlrn2qak29vhj46iiqdnsydk29cy") (y #t)))

(define-public crate-gpu-checker-0.1.1 (c (n "gpu-checker") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wysixhqlgrd2l091an6kfc52d15i1sgw2mqs6254bvc9s0ps50i")))

(define-public crate-gpu-checker-0.1.2 (c (n "gpu-checker") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fcs9l058mrnmzsycr9gdvz7c5mnvla9j3x1banlgkxjzzqw39ka")))

