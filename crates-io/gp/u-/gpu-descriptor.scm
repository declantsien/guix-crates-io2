(define-module (crates-io gp u- gpu-descriptor) #:use-module (crates-io))

(define-public crate-gpu-descriptor-0.1.0 (c (n "gpu-descriptor") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1ij5bqbhsvjv0sq6gcjadykr5q67z1zd99hjy8ljw87hlsk6hinp") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.1.1 (c (n "gpu-descriptor") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1qpwdksaz99fyhw7xl7h4vgxpr31p31057p9lgb0x153hwg0z9z8") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.2.0 (c (n "gpu-descriptor") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1yvcml3mrwwklwn2c7ifglpgnxbnh793jacyf1kfjcivswqvbvnr") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.2.1 (c (n "gpu-descriptor") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "02sbing2879np0w3ypf7aazsd2gnqhmccmbd00bhvccs87q3g8np") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.2.2 (c (n "gpu-descriptor") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1nhxr0hyh0bmm2m5pjiy02227k13hfk3qa4sf7s5yh2dpqbz4f55") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.2.3 (c (n "gpu-descriptor") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0ynmd3phm3kj448xyq570zjzh860157a7305cpli9nqbpbhh430b") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.2.4 (c (n "gpu-descriptor") (v "0.2.4") (d (list (d (n "bitflags") (r "^2.4") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0b38pi460ajx8ksb61zxardwkpa27qgz8fpm252mczlfrqddy4fc") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-descriptor-0.3.0 (c (n "gpu-descriptor") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.4") (k 0)) (d (n "gpu-descriptor-types") (r "^0.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0mw5vp9fs77fvdnv3lzh30b2lrxsn0g857xrp0ibgl584gvc224w") (f (quote (("std") ("default" "std"))))))

