(define-module (crates-io gp u- gpu-alloc-ash) #:use-module (crates-io))

(define-public crate-gpu-alloc-ash-0.2.0 (c (n "gpu-alloc-ash") (v "0.2.0") (d (list (d (n "ash") (r "^0.33") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "03imywbd00fl151b3n7azq328hz70fmp0kc6jcj26k7l947gqazv")))

(define-public crate-gpu-alloc-ash-0.3.0 (c (n "gpu-alloc-ash") (v "0.3.0") (d (list (d (n "ash") (r "^0.35") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0ns3axgskmnflqnnqh2m6mibh67y4y56igqmxzscgp3a3vl9sacc")))

(define-public crate-gpu-alloc-ash-0.4.0 (c (n "gpu-alloc-ash") (v "0.4.0") (d (list (d (n "ash") (r "^0.36") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1i133p1m0dc3zm8fqishjnkr9dyj5265xvmv51yakjmdqdyshhff")))

(define-public crate-gpu-alloc-ash-0.5.0 (c (n "gpu-alloc-ash") (v "0.5.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1hxj62na42cx8m9njyap2kpkdaq8sr4cfcakp6pwadp922ys57cy")))

(define-public crate-gpu-alloc-ash-0.6.0 (c (n "gpu-alloc-ash") (v "0.6.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "gpu-alloc-types") (r "=0.3.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0cjp86hlvsvag29fha7kx0idvpz2s39jaz75aqd0w5w8pv4lnhnj")))

(define-public crate-gpu-alloc-ash-0.7.0 (c (n "gpu-alloc-ash") (v "0.7.0") (d (list (d (n "ash") (r "^0.38") (k 0)) (d (n "gpu-alloc-types") (r "=0.3.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "16ppnrwicgxps35x0dcr91cvydgrgls5qhz01lp8rjcvl8c7mnnb")))

