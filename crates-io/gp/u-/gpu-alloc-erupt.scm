(define-module (crates-io gp u- gpu-alloc-erupt) #:use-module (crates-io))

(define-public crate-gpu-alloc-erupt-0.1.0 (c (n "gpu-alloc-erupt") (v "0.1.0") (d (list (d (n "erupt") (r "^0.15") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "10cs4a8b6bxsqy4l80flglj8g7xy3x6j3rins0yyzxfhcn4ag0iz")))

(define-public crate-gpu-alloc-erupt-0.1.1 (c (n "gpu-alloc-erupt") (v "0.1.1") (d (list (d (n "erupt") (r "^0.15") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0fwl0646qvvqy10pzlvrc8vib1sghmv983685l3id4s8ncsnx4sh")))

(define-public crate-gpu-alloc-erupt-0.2.0 (c (n "gpu-alloc-erupt") (v "0.2.0") (d (list (d (n "erupt") (r "^0.16") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "08dinahslln14hsy3fqbl2z2cqfv03ni21bcdh9b97psc3dlqg31")))

(define-public crate-gpu-alloc-erupt-0.3.0 (c (n "gpu-alloc-erupt") (v "0.3.0") (d (list (d (n "erupt") (r "^0.17") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0vkpfzyjky1sk9m1svg8jd3d76dw3wfpyy9vl3lhlih95p9r5fal")))

(define-public crate-gpu-alloc-erupt-0.4.0 (c (n "gpu-alloc-erupt") (v "0.4.0") (d (list (d (n "erupt") (r "^0.18") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1flryxb322hq4pg4z49yplym1w55djq9gf3pdahgfm6a7gbl5b9w")))

(define-public crate-gpu-alloc-erupt-0.5.0 (c (n "gpu-alloc-erupt") (v "0.5.0") (d (list (d (n "erupt") (r "^0.19") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "02708cxsfxzdhwdcblh5qayx97ai9d1z4vm7v7s0ck01amjv7cw1")))

(define-public crate-gpu-alloc-erupt-0.6.0 (c (n "gpu-alloc-erupt") (v "0.6.0") (d (list (d (n "erupt") (r "^0.21") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "05wphwklgpnr7hyh5rcc8vxr9f2x76mi213m96x61ak1w59gl724")))

(define-public crate-gpu-alloc-erupt-0.7.0 (c (n "gpu-alloc-erupt") (v "0.7.0") (d (list (d (n "erupt") (r "^0.22") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0ykiywpfsf7dhz4xxvgyp0q81nxhfg597skg7qq8g3mikvyyxvwx")))

(define-public crate-gpu-alloc-erupt-0.8.0 (c (n "gpu-alloc-erupt") (v "0.8.0") (d (list (d (n "erupt") (r "^0.23.0") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "1dkdsa3gak6h598052zahfm3wn3hk4467a8y9jh18scgdiz02srf")))

(define-public crate-gpu-alloc-erupt-0.9.0 (c (n "gpu-alloc-erupt") (v "0.9.0") (d (list (d (n "erupt") (r "^0.23.0") (f (quote ("loading"))) (k 0)) (d (n "gpu-alloc-types") (r "=0.3.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)))) (h "0lw29303njik8361hlg49sj210p299xs1vz48bpmgm46xp46rzz0")))

