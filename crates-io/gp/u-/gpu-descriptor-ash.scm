(define-module (crates-io gp u- gpu-descriptor-ash) #:use-module (crates-io))

(define-public crate-gpu-descriptor-ash-0.1.0 (c (n "gpu-descriptor-ash") (v "0.1.0") (d (list (d (n "ash") (r "^0.35") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0js0qsqr76gs2mad6yvb51favvf9vdby0fxr6l25gyzkclr4k5z5")))

(define-public crate-gpu-descriptor-ash-0.2.0 (c (n "gpu-descriptor-ash") (v "0.2.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1nqwg36mc28m6xiac4ldd3q180smz813pqz7yxf7x23blcvp5zr7")))

(define-public crate-gpu-descriptor-ash-0.3.0 (c (n "gpu-descriptor-ash") (v "0.3.0") (d (list (d (n "ash") (r "^0.38") (k 0)) (d (n "gpu-descriptor-types") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0m410r4gkp36sria4wy0krbx1rc0db0li9grpm6rnfarz417n287")))

