(define-module (crates-io gp u- gpu-alloc) #:use-module (crates-io))

(define-public crate-gpu-alloc-0.1.0 (c (n "gpu-alloc") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0b3qliqkkcfv19b0srmlfpapcqdqhjy6ylw20ngf44cqgl7kk7kj") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.1.1 (c (n "gpu-alloc") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1phv1c457isnymbjj9hdrzhnyi17d7471xpzmwzkcvn9kc2dyg18") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.2.0 (c (n "gpu-alloc") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "192g8dfayp3z7wc612633r0zvy8jw4ymnqpkwzbnrycgg5122i0i") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.3.0 (c (n "gpu-alloc") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0sz101dspppg6f1yssz13pj8cpr6wq74xxgsf1ns6zpmmswj8xqy") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.3.1 (c (n "gpu-alloc") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "18ax9j0cdaf7ixy2mlpxh2qkbd96asairdp3lx8hhn1nxg8h84vv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.4.1 (c (n "gpu-alloc") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1i8giv7l65hrwf9bfkj079j61ifxiy7903pr9cbliij9bzm7x6is") (f (quote (("std") ("freelist") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.4.2 (c (n "gpu-alloc") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1pr7nlmcrg68p0l7hbmb1wz8c2chd3jhj486bfl4wr5vm8q14173") (f (quote (("std") ("freelist") ("default" "std"))))))

(define-public crate-gpu-alloc-0.4.3 (c (n "gpu-alloc") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0dh4kxrlzb4mixa18cafdw7hjn26lz7ng5dq6vg696rfxl13y5pl") (f (quote (("std") ("freelist") ("default" "std"))))))

(define-public crate-gpu-alloc-0.4.4 (c (n "gpu-alloc") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1nmcrj90339zj1ajrnahw0f4mpn8g3a43fd8frs99ip236qwlnc1") (f (quote (("std") ("freelist") ("default" "std" "freelist"))))))

(define-public crate-gpu-alloc-0.4.5 (c (n "gpu-alloc") (v "0.4.5") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1fpxx4p2i4d6mxgi0lxlxcjzpvysdwqakgl0p3rscrdv0j40hxmw") (f (quote (("std") ("freelist") ("default" "std" "freelist"))))))

(define-public crate-gpu-alloc-0.4.6 (c (n "gpu-alloc") (v "0.4.6") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0c4jq2ksz6qc9ywflh3y9slgvx0ldcrg4lv5wgdf308q18rkfgn8") (f (quote (("std") ("freelist") ("default" "std" "freelist"))))))

(define-public crate-gpu-alloc-0.4.7 (c (n "gpu-alloc") (v "0.4.7") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0fhzp4md1bya60bpdf6idrq07kjp4fs9qvbq4qjqd0af6z5bdhfb") (f (quote (("std") ("freelist") ("default" "std" "freelist"))))))

(define-public crate-gpu-alloc-0.5.0 (c (n "gpu-alloc") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0y9fllq9fz7dsy31z1ypfygjrq8j76xm1mivsbz1sjih8jf4b0f4") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.5.1 (c (n "gpu-alloc") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (k 0)))) (h "1gs2ni10y8lz9f3q424dkzpc0kgnziwdy6l9bii0b7gwqpm291db") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gpu-alloc-0.3.2 (c (n "gpu-alloc") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0c5s6zx5ni9jjv7ckacxqvh2hgg5d2wbqlz788rhkdg4y59ix7kk") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.5.2 (c (n "gpu-alloc") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (f (quote ("attributes"))) (o #t) (k 0)))) (h "19lahfc0f00hq0n1z24hq57nfkx8jvhmcpm9kphx6235sfwcnr0f") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.5.3 (c (n "gpu-alloc") (v "0.5.3") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (f (quote ("attributes"))) (o #t) (k 0)))) (h "13c1vqbzgch32w9sjvc7m1lnyr6xc72qczvhwrv0wc8ff5grxibz") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.5.4 (c (n "gpu-alloc") (v "0.5.4") (d (list (d (n "bitflags") (r "^1.2") (k 0)) (d (n "gpu-alloc-types") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0qkzv19k8ls20nx13qw63gfy9jc4gbxzcc50gr2h90mk57yamgi2") (f (quote (("std") ("default" "std"))))))

(define-public crate-gpu-alloc-0.6.0 (c (n "gpu-alloc") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.0") (k 0)) (d (n "gpu-alloc-types") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1.27") (f (quote ("attributes"))) (o #t) (k 0)))) (h "0wd1wq7qs8ja0cp37ajm9p1r526sp6w0kvjp3xx24jsrjfx2vkgv") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde"))))))

