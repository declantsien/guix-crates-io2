(define-module (crates-io gp u- gpu-descriptor-gfx) #:use-module (crates-io))

(define-public crate-gpu-descriptor-gfx-0.1.0 (c (n "gpu-descriptor-gfx") (v "0.1.0") (d (list (d (n "gfx-hal") (r "^0.6") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "039i0yksjbc7m2g977kd0dv5icar20zawjck4sx54qs825g6g458")))

(define-public crate-gpu-descriptor-gfx-0.2.0 (c (n "gpu-descriptor-gfx") (v "0.2.0") (d (list (d (n "gfx-hal") (r "^0.7") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1aj8axml99lc5kv8wzxmrh4z1xz7axwy9jx0r8h3qsdl3mmr3zvy")))

