(define-module (crates-io gp u- gpu-sw) #:use-module (crates-io))

(define-public crate-gpu-sw-1.0.0 (c (n "gpu-sw") (v "1.0.0") (d (list (d (n "arrayfire") (r "^3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "095a5lg4j3sn7ayp08vc8xxaib30zckrjvr9sjbbmdz92233m1m1")))

(define-public crate-gpu-sw-1.0.1 (c (n "gpu-sw") (v "1.0.1") (d (list (d (n "arrayfire") (r "^3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "0p2m50bnxzykik8ppfk5h06ymwlfrvsh9myqjmn2dp116n2z44xp")))

(define-public crate-gpu-sw-1.0.2 (c (n "gpu-sw") (v "1.0.2") (d (list (d (n "arrayfire") (r "^3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "01b209xp7sl64a4bcgpxgd2kxilgdkjccllcd2i7cbdi1y00jca3")))

(define-public crate-gpu-sw-1.0.3 (c (n "gpu-sw") (v "1.0.3") (d (list (d (n "arrayfire") (r "^3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "0bxrgzfy36j1pqn6l16mfr47dhhfs2mkqmyshd0npgrrd5k44yb7")))

(define-public crate-gpu-sw-1.0.4 (c (n "gpu-sw") (v "1.0.4") (d (list (d (n "arrayfire") (r "^3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "02r0dmid3jhca0106mhll7lyxhfq9k2z7m4by8jr3wwvsz9dsl6b")))

