(define-module (crates-io gp u- gpu-info) #:use-module (crates-io))

(define-public crate-gpu-info-0.1.0 (c (n "gpu-info") (v "0.1.0") (d (list (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.0") (d #t) (k 0)))) (h "07a964lpnbdhj932mbz7b4l2rvyq34gqic03awvn1mzmlghl803y") (y #t)))

