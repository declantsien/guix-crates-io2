(define-module (crates-io gp u- gpu-descriptor-erupt) #:use-module (crates-io))

(define-public crate-gpu-descriptor-erupt-0.1.0 (c (n "gpu-descriptor-erupt") (v "0.1.0") (d (list (d (n "erupt") (r "^0.18.0") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "157m7w9r29606ssfny9mqrjvg7f96lp15pbwi2srbc7fxfsdbgdk")))

(define-public crate-gpu-descriptor-erupt-0.1.1 (c (n "gpu-descriptor-erupt") (v "0.1.1") (d (list (d (n "erupt") (r "^0.18.0") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0i1ck9m1nmshw7g0p5irw2101ablagzkg46ay5dbmjmzldzsxlwh")))

(define-public crate-gpu-descriptor-erupt-0.2.0 (c (n "gpu-descriptor-erupt") (v "0.2.0") (d (list (d (n "erupt") (r "^0.19") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1x284rqhibb3y2mh5146qpc5syv8wa1v344xqf8ir17cddai9wa7")))

(define-public crate-gpu-descriptor-erupt-0.3.0 (c (n "gpu-descriptor-erupt") (v "0.3.0") (d (list (d (n "erupt") (r "^0.22") (d #t) (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "185x3ghs56ync3hmsgl5pjzn6a9sxs5b298b3lqkqj4ram622ij1")))

(define-public crate-gpu-descriptor-erupt-0.4.0 (c (n "gpu-descriptor-erupt") (v "0.4.0") (d (list (d (n "erupt") (r "^0.23") (k 0)) (d (n "gpu-descriptor-types") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1jw87dd6q42qzw8r6w0f348rwr1k2mrc0nnl9g2a7qjbqlrwsqml")))

(define-public crate-gpu-descriptor-erupt-0.5.0 (c (n "gpu-descriptor-erupt") (v "0.5.0") (d (list (d (n "erupt") (r "^0.23") (k 0)) (d (n "gpu-descriptor-types") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1aznbrcnzbvchix10q52sv5w0w24pac9qgf5axvl45dpl0k7f4gb")))

