(define-module (crates-io gp u- gpu-descriptor-types) #:use-module (crates-io))

(define-public crate-gpu-descriptor-types-0.1.0 (c (n "gpu-descriptor-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (k 0)))) (h "0rj5020v24vcg7k30dcidpd5b02ncy6gk66z2ja58rdzq6r5h9hq") (y #t)))

(define-public crate-gpu-descriptor-types-0.1.1 (c (n "gpu-descriptor-types") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (k 0)))) (h "09i17z9943npl59lqy2y9h9562ri98xdxyccyvz6ilaswmvkcgin")))

(define-public crate-gpu-descriptor-types-0.1.2 (c (n "gpu-descriptor-types") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4") (k 0)))) (h "135pp1b3bzyr7bfnb30rf9pkgy61h75w0jabi8fpw2q9dxpb7w3b")))

(define-public crate-gpu-descriptor-types-0.2.0 (c (n "gpu-descriptor-types") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4") (k 0)))) (h "14ab90klss7w0ybj95fcnqxjsjya17xjhf576dpvi4zq5ml45wpx")))

