(define-module (crates-io gp u- gpu-alloc-vulkanalia) #:use-module (crates-io))

(define-public crate-gpu-alloc-vulkanalia-0.1.0 (c (n "gpu-alloc-vulkanalia") (v "0.1.0") (d (list (d (n "gpu-alloc-types") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "const_generics"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "vulkanalia") (r "^0.22") (d #t) (k 0)))) (h "1mqgaah1z7y7yhd0xkvrq4639nccrxa7mj6b8wcjs3g5ss63rb55") (y #t) (r "1.65")))

(define-public crate-gpu-alloc-vulkanalia-0.1.1 (c (n "gpu-alloc-vulkanalia") (v "0.1.1") (d (list (d (n "gpu-alloc-types") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "const_generics"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "vulkanalia") (r "^0.22") (d #t) (k 0)))) (h "0zbnafipj0as7vzghjdpf6gm2rfqbndsbnlcp3g4ccic2c4kkpll") (r "1.65")))

(define-public crate-gpu-alloc-vulkanalia-0.1.2 (c (n "gpu-alloc-vulkanalia") (v "0.1.2") (d (list (d (n "gpu-alloc-types") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "const_generics"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "vulkanalia") (r "^0.23") (d #t) (k 0)))) (h "0mxpnw65nmnmj782sdq7qinwx6xm202447zbbn2r6x91ng30jgvg") (r "1.65")))

