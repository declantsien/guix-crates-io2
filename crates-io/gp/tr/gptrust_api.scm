(define-module (crates-io gp tr gptrust_api) #:use-module (crates-io))

(define-public crate-gptrust_api-0.1.0 (c (n "gptrust_api") (v "0.1.0") (d (list (d (n "gptrust_http") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13bz7s0xal5irkqlws1kwzj7mfifni38jlpnsqbxh29k44nncsa9")))

(define-public crate-gptrust_api-0.1.1 (c (n "gptrust_api") (v "0.1.1") (d (list (d (n "gptrust_http") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18z6mfa79cd15jfdzxlv3afw0wdkkxwcam5v58ss3lvzpd4s4hxp")))

(define-public crate-gptrust_api-0.1.2 (c (n "gptrust_api") (v "0.1.2") (d (list (d (n "gptrust_http") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y68m1y4wn68619cxqp006jh0424yp0rb64slyzlbgpwv1r13d87")))

(define-public crate-gptrust_api-0.1.3 (c (n "gptrust_api") (v "0.1.3") (d (list (d (n "gptrust_http") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nlsgi5292lwd6v4gk7x57zci5qr5gngsvs002pz272ghawd5aqm")))

(define-public crate-gptrust_api-0.1.4 (c (n "gptrust_api") (v "0.1.4") (d (list (d (n "gptrust_http") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0syq15g2cz1fklhwkg5bls1fcaxmzqsr8n8ivncrp3hn7n4qdkbn")))

