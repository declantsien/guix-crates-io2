(define-module (crates-io gp tr gptrust_http) #:use-module (crates-io))

(define-public crate-gptrust_http-0.1.0 (c (n "gptrust_http") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)))) (h "0v3f6a9lckhg6jag51vnqgjrdljvnlzqiia4dsapjq649gv61jgb")))

(define-public crate-gptrust_http-0.1.1 (c (n "gptrust_http") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)))) (h "07d1cjfp76fgk34mn41cb7bjd7899xaq2b5b644xkmbwirfwql62")))

(define-public crate-gptrust_http-0.1.2 (c (n "gptrust_http") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)))) (h "0dfkx4qlp71v71rykmd2zvls9l5xpjsachbl238nh177plv53axr")))

(define-public crate-gptrust_http-0.1.3 (c (n "gptrust_http") (v "0.1.3") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)))) (h "069n7iv07a82rn7s9g205rjhqz0gda0d91zcw7kmmy1lkdj6if39")))

(define-public crate-gptrust_http-0.1.4 (c (n "gptrust_http") (v "0.1.4") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)))) (h "05niafqjmmawfv2bc2zs9nwblshh5dnf73yygscdwvav3snbhjia")))

