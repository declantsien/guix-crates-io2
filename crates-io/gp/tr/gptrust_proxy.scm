(define-module (crates-io gp tr gptrust_proxy) #:use-module (crates-io))

(define-public crate-gptrust_proxy-0.1.0 (c (n "gptrust_proxy") (v "0.1.0") (d (list (d (n "gptrust_api") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1al2jdp5mp80wgsq1c893xlyvna0qwvz2wwqw8pfgkwandw304m7")))

