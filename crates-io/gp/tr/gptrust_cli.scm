(define-module (crates-io gp tr gptrust_cli) #:use-module (crates-io))

(define-public crate-gptrust_cli-0.1.0 (c (n "gptrust_cli") (v "0.1.0") (d (list (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "=4.0.21") (d #t) (k 0)) (d (n "clap_lex") (r "=0.3.0") (d #t) (k 0)) (d (n "gptrust_api") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0idvlq1gbb44hvdiaq208qmnzs69rcbvbw5l7m5pbp9dr4qvv7pi")))

(define-public crate-gptrust_cli-0.1.1 (c (n "gptrust_cli") (v "0.1.1") (d (list (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "=4.0.21") (d #t) (k 0)) (d (n "clap_lex") (r "=0.3.0") (d #t) (k 0)) (d (n "gptrust_api") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g2zxxxx9yf088pm4gyjigpd2fz3sagbbq0zypw1kismyz38q4qf")))

(define-public crate-gptrust_cli-0.1.2 (c (n "gptrust_cli") (v "0.1.2") (d (list (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "=4.0.21") (d #t) (k 0)) (d (n "clap_lex") (r "=0.3.0") (d #t) (k 0)) (d (n "gptrust_api") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b34i6604iwg8d0jzv61ybkcm9m1wxwyq82vq0ak49f8ms7ipimw")))

(define-public crate-gptrust_cli-0.1.3 (c (n "gptrust_cli") (v "0.1.3") (d (list (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "=4.0.21") (d #t) (k 0)) (d (n "clap_lex") (r "=0.3.0") (d #t) (k 0)) (d (n "gptrust_api") (r "^0.1.3") (d #t) (k 0)) (d (n "gptrust_http") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13hl5q9zjizkak0yrdr6b46xhsdp3mjd6vlriic1g1vhz3qq3b8l")))

(define-public crate-gptrust_cli-0.1.4 (c (n "gptrust_cli") (v "0.1.4") (d (list (d (n "clap") (r "=4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "=4.0.21") (d #t) (k 0)) (d (n "clap_lex") (r "=0.3.0") (d #t) (k 0)) (d (n "gptrust_api") (r "^0.1.4") (d #t) (k 0)) (d (n "gptrust_http") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a6xjh9hiqzhvzi9hvhzvw383898pjwqckjapd4m7258xqwj0n9s")))

