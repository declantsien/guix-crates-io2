(define-module (crates-io gp ui gpuinfo) #:use-module (crates-io))

(define-public crate-gpuinfo-0.1.0 (c (n "gpuinfo") (v "0.1.0") (d (list (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.0") (d #t) (k 0)))) (h "1bya9gb2q1glzwxcbsfci18vci4x8fpdrsjmjbsq16si7gxbhghr")))

(define-public crate-gpuinfo-0.1.1 (c (n "gpuinfo") (v "0.1.1") (d (list (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.0") (d #t) (k 0)))) (h "15lmcwjycyqlyqdfp9npg13sjhm84p52kfl0j071976m316a7gk7")))

(define-public crate-gpuinfo-0.1.2 (c (n "gpuinfo") (v "0.1.2") (d (list (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.0") (d #t) (k 0)))) (h "1r35b1sm2grb2hazjic7v6gm14965zwnrwv2r22k4xndbjcfzdkr")))

(define-public crate-gpuinfo-0.1.3 (c (n "gpuinfo") (v "0.1.3") (d (list (d (n "nvml-wrapper") (r "^0.7.0") (d #t) (k 0)) (d (n "nvml-wrapper-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.19.0") (d #t) (k 0)))) (h "0k88m6nn418if50qyxf7cnkrc8d5c0wfpxfi1ny8b3mf7cgbgnm8")))

