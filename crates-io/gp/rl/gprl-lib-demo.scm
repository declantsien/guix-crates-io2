(define-module (crates-io gp rl gprl-lib-demo) #:use-module (crates-io))

(define-public crate-gprl-lib-demo-0.1.0 (c (n "gprl-lib-demo") (v "0.1.0") (h "1h1qcdmizzy3w8i5ffq9xdvhvkkp7a625sibj024c54d6n7sy0gd") (y #t)))

(define-public crate-gprl-lib-demo-0.1.1 (c (n "gprl-lib-demo") (v "0.1.1") (h "0kwkjp22850ikn5cxb55k3a7zfbcziyamv1iwgsia33mz2crydrf")))

(define-public crate-gprl-lib-demo-0.1.2 (c (n "gprl-lib-demo") (v "0.1.2") (h "1dws8wrznl9nj48ybgn75ng3bjqfbgvvnid8f8jb7np34rnjk7zv")))

