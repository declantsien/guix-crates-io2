(define-module (crates-io gp ho gphoto2-sys) #:use-module (crates-io))

(define-public crate-gphoto2-sys-0.1.0 (c (n "gphoto2-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "17wkc8qyr2spw2p060cc4i2l68d28jb81yjydj9bfyh4vwrf0n4q")))

(define-public crate-gphoto2-sys-0.1.1 (c (n "gphoto2-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "18445knq48pbr81bypsv8y6spkzpjd2lylwzvzp1176gmxdcxxp1")))

(define-public crate-gphoto2-sys-0.1.2 (c (n "gphoto2-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "167qsw8qanbj020rjij11vmpwl8lxp0ni176c9vx2nvns45janmy")))

