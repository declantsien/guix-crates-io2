(define-module (crates-io gp ho gphoto) #:use-module (crates-io))

(define-public crate-gphoto-0.1.0 (c (n "gphoto") (v "0.1.0") (d (list (d (n "gphoto2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "14l8lcl3r5zzjwvsfb7iv9knppm1wv4n8bvjq2q88y93bf7ndycf")))

(define-public crate-gphoto-0.1.1 (c (n "gphoto") (v "0.1.1") (d (list (d (n "gphoto2-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cbm780jfq6i9kxam051a0kislbhasbqkacdqvn35as6hpg3d35w")))

(define-public crate-gphoto-0.1.2 (c (n "gphoto") (v "0.1.2") (d (list (d (n "gphoto2-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g0kg7qf0caaix5d7ym475qhxaiwr59782n2cxzp755n3gdyhhym")))

