(define-module (crates-io gp ho gphoto2) #:use-module (crates-io))

(define-public crate-gphoto2-0.1.0 (c (n "gphoto2") (v "0.1.0") (d (list (d (n "libgphoto2_sys") (r "^2.5.10") (d #t) (k 0)))) (h "00ki2l1i88i4w4fpvzc47y3naq8n8qys287m5vcjbjkjk321zna1")))

(define-public crate-gphoto2-0.1.1 (c (n "gphoto2") (v "0.1.1") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "0fj6h2m52g5lk1wbjsbwang38lr91zcmc395cjsz6h36ivv97dq1")))

(define-public crate-gphoto2-0.2.0 (c (n "gphoto2") (v "0.2.0") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "10l70jw6vxx7skx0aknrkh5k40bzhsym55aybj2bg7iim9x19wil")))

(define-public crate-gphoto2-0.3.0 (c (n "gphoto2") (v "0.3.0") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "0asa6xxg26nppizw294gz63ipm8n4nzn5x3vi244swnigs6gaxmn")))

(define-public crate-gphoto2-0.3.1 (c (n "gphoto2") (v "0.3.1") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "139nycbrci4i8gkqpn6f9q0154vihql712nqv2gnab1y2kndf0x8")))

(define-public crate-gphoto2-1.0.0-rc1 (c (n "gphoto2") (v "1.0.0-rc1") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "18ywy182w7l8apx9pyrc3h2k0mqyc3nl3v68fif7p96jzmgkkbk3")))

(define-public crate-gphoto2-1.0.0 (c (n "gphoto2") (v "1.0.0") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "0yzhyyz18y2blyyaz7pga972hj0fr4f0ngzhiibdrm9jizrgydri")))

(define-public crate-gphoto2-1.0.1 (c (n "gphoto2") (v "1.0.1") (d (list (d (n "libgphoto2_sys") (r "^1.0.0") (d #t) (k 0)))) (h "015bd76mfs4ilzczx9p4gyczvn4fpfkwyzl9qll4kyg0zz57gnj4")))

(define-public crate-gphoto2-1.0.2 (c (n "gphoto2") (v "1.0.2") (d (list (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "0j18vxj56v0xm0gnacbmb84c9wbnm6hv2w1lc8vw169a25r6sv50")))

(define-public crate-gphoto2-1.1.0 (c (n "gphoto2") (v "1.1.0") (d (list (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "17hq13yj92s57xxds3a1dfkx5zjzb0ng17cjds6x3n4zzbzg9rb9")))

(define-public crate-gphoto2-1.1.1 (c (n "gphoto2") (v "1.1.1") (d (list (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "1cvrckzdac9l17bbfcapszn69ahs5ngcm92s3dbbw6q42x98dcdx")))

(define-public crate-gphoto2-1.2.0 (c (n "gphoto2") (v "1.2.0") (d (list (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "09bdqk78aalccz4x05rvs7h4jyphnmklylmpq0wa0azy6d3rvanw")))

(define-public crate-gphoto2-1.3.0 (c (n "gphoto2") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "0vq1mk0dwdyj8q5dpydqh8pfn6g2v21hkxnaajrqr25wgq8ka2r0")))

(define-public crate-gphoto2-1.3.1 (c (n "gphoto2") (v "1.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "0g93bdyphpywhhk9j4qjw8432kqqplb4116l0g770af6nxyhp9nx")))

(define-public crate-gphoto2-1.3.2 (c (n "gphoto2") (v "1.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "1f744cagicm6bshs593gdc45xhw55k5fgrb18x59h1i6flp319p1")))

(define-public crate-gphoto2-1.3.3 (c (n "gphoto2") (v "1.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "10a64y2lagjdm5v2y82gmlhwrgwpprbrb18q7lyjnwlirnqdih23")))

(define-public crate-gphoto2-1.3.4 (c (n "gphoto2") (v "1.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.0.1") (d #t) (k 0)))) (h "0r68a80wjjgk4pmg1107rxvvz8w36q7g89cw6kl0ak2yx5qg4m27")))

(define-public crate-gphoto2-1.4.0 (c (n "gphoto2") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)))) (h "18z390afh1f6xm5njjh92wygqxqsnx5vnfbk1j8n77svf41wc66c")))

(define-public crate-gphoto2-1.4.1 (c (n "gphoto2") (v "1.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)))) (h "0308ywcvq31063j70vb7a22mmn7aaaiinf5ysr8pc4lqyzlaz3g8")))

(define-public crate-gphoto2-1.5.0 (c (n "gphoto2") (v "1.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)))) (h "1d9jdwacanjjwxzgnldx7fms2kwf5v0b92vg5p5lnhgk69002w19")))

(define-public crate-gphoto2-2.0.0 (c (n "gphoto2") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hg3csj046ip70r5ldh9q9173lbz0myar38rp7vk9nb5a204d1ha") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-2.0.1 (c (n "gphoto2") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0w04d3qdx4l8h5g3pk4dpcc988xqzv1fhm0yw47917lxxfnfbw8z") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3.0.0 (c (n "gphoto2") (v "3.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g3lyqmphxbxja10mhzdmjvmjxfba4zgqlnwnq8qr934xnaxavwy") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3.0.1 (c (n "gphoto2") (v "3.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a6lqg2zh6bz49y05r496hbkdyrvsr6aazkjcmmck0jn5c54mwcy") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3.1.0 (c (n "gphoto2") (v "3.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qqljcg5mky6l5fmn0yalx2k57wjl79b49qhv8cdm0r04qgbdjvd") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3.2.0 (c (n "gphoto2") (v "3.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yjjyg2gq2haqyz33dipmvmcbghb8a7nnzchpabv5i7rbgvyxbkg") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3.2.1 (c (n "gphoto2") (v "3.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pdidrhrzzwkkjmwfp8qxqr1jcrws23rh85bsnkbq9rr5mkavsqv") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3.2.2 (c (n "gphoto2") (v "3.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1y7pk6lrw5i5cncw4ixk659l62dgzak4gihbv87qipxp9rvrkca2") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3.2.3 (c (n "gphoto2") (v "3.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qs4nldpnjagxdgb18p72kx8mbwh7s5idiiwc7gyd79cg3qcvpi1") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3.3.0 (c (n "gphoto2") (v "3.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgphoto2_sys") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gdhp3nbj37arpyh5wyc0322df1yxbvwmyfq72gfl0gmf53j7kz8") (f (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (s 2) (e (quote (("serde" "dep:serde"))))))

