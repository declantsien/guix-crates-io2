(define-module (crates-io gp io gpiod) #:use-module (crates-io))

(define-public crate-gpiod-0.2.0 (c (n "gpiod") (v "0.2.0") (d (list (d (n "gpiod-core") (r "^0.2.0") (k 0)))) (h "184lkvrslh595a1h3c9wg7fnzv9c0hnjpak6gnsq784bikvr4cz1") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-gpiod-0.2.1 (c (n "gpiod") (v "0.2.1") (d (list (d (n "gpiod-core") (r "^0.2.1") (k 0)))) (h "0p9pms258k0w8ibcqi3dp2a1wsrsva6cgiv9z2b4pbwip535k8sn") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-gpiod-0.2.2 (c (n "gpiod") (v "0.2.2") (d (list (d (n "gpiod-core") (r "^0.2.2") (k 0)))) (h "0bisbavdgj71lly2h4dgvgdyjrshklvxl7jzx1538aj3pwkn7rxd") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-gpiod-0.2.3 (c (n "gpiod") (v "0.2.3") (d (list (d (n "gpiod-core") (r "^0.2.3") (k 0)))) (h "0lvd9y01i07zrkbnyaczhk1ml0wdkq0wlw4g30qm0iqryi3lyai6") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-gpiod-0.3.0 (c (n "gpiod") (v "0.3.0") (d (list (d (n "gpiod-core") (r "^0.3") (k 0)))) (h "0jfsz1j85988l1kwcx10f52l3xrvd7c81vs86i4llsxk0z3b2lgq") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

