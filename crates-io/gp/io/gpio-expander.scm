(define-module (crates-io gp io gpio-expander) #:use-module (crates-io))

(define-public crate-gpio-expander-0.1.0 (c (n "gpio-expander") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "rppal") (r "^0.13") (f (quote ("hal"))) (d #t) (k 2)) (d (n "shared-bus") (r "^0.2") (d #t) (k 0)))) (h "0li8q4ivzvxf7ydk2na2lbyamrf8vcv8w3klx1c4vk7q2fafdmps")))

