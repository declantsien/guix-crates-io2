(define-module (crates-io gp io gpio-rust) #:use-module (crates-io))

(define-public crate-gpio-rust-0.1.0 (c (n "gpio-rust") (v "0.1.0") (d (list (d (n "inline-python") (r "^0.6.0") (d #t) (k 0)))) (h "1g42bnk6jb4c8n4blbjx7rkgvvpzqcx87z6jcp54b44qdxgmfcsc")))

(define-public crate-gpio-rust-0.1.1 (c (n "gpio-rust") (v "0.1.1") (d (list (d (n "inline-python") (r "^0.6.0") (d #t) (k 0)))) (h "03gg43q15g9v3v751iqyf30k8pawz4hdphm83nj2c28lp7q96121")))

(define-public crate-gpio-rust-0.1.2 (c (n "gpio-rust") (v "0.1.2") (d (list (d (n "inline-python") (r "^0.6.0") (d #t) (k 0)))) (h "1dvmi8gy8s7jkz3lmrc9dqpc3cvbynm1pcb109pif68h9z4lsdrq")))

(define-public crate-gpio-rust-0.1.3 (c (n "gpio-rust") (v "0.1.3") (d (list (d (n "inline-python") (r "^0.6.0") (d #t) (k 0)))) (h "1frxkkg1b9payfxj4jvqs5sgpjdx99dwm895024ll9r12nf05lkx")))

(define-public crate-gpio-rust-0.1.4 (c (n "gpio-rust") (v "0.1.4") (d (list (d (n "inline-python") (r "^0.6.0") (d #t) (k 0)))) (h "0ixk9dmzp5k72p5my3wyrl6djlh6qhxcyp5awsad00x564m49sfn")))

