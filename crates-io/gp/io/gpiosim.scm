(define-module (crates-io gp io gpiosim) #:use-module (crates-io))

(define-public crate-gpiosim-0.1.0 (c (n "gpiosim") (v "0.1.0") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rmgh39rllj9bj4jq8a7m9p3mkv8byddpmnjcrbfxhwqlxw07drk")))

(define-public crate-gpiosim-0.1.1 (c (n "gpiosim") (v "0.1.1") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hx2yagiakwhyz9vvg81vj7wggcff6cb1nw6digpmqf2909zd5a2")))

(define-public crate-gpiosim-0.1.2 (c (n "gpiosim") (v "0.1.2") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y1g1zp08jbv620gcv5av347islajw7r778miz0sh8fdbx7w7799")))

(define-public crate-gpiosim-0.2.0 (c (n "gpiosim") (v "0.2.0") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qrzgpd9fg3lil0k5nrdl5gy54b9gjyppy70c8hjj1nvkylriyiq")))

(define-public crate-gpiosim-0.2.1 (c (n "gpiosim") (v "0.2.1") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.3.0") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rrsr8q8qi5g4gn8y7ki1dqkjqfxw80szdjafmddzjhc3l9gsj4f") (r "1.56")))

(define-public crate-gpiosim-0.2.2 (c (n "gpiosim") (v "0.2.2") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.3.0") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jczmikf1n5vsn5g5ljhfrfcxf9xgf4brvy5vdilsavr57fjvk3y") (r "1.56")))

(define-public crate-gpiosim-0.2.3 (c (n "gpiosim") (v "0.2.3") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.3.0") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qa3jv3kck8ksp63cf88gy620lqncfm22l742pdy4qlip7g84qb6") (r "1.56")))

(define-public crate-gpiosim-0.3.0 (c (n "gpiosim") (v "0.3.0") (d (list (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.4") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "148r9xaxh2h6nznflvlaxxfg0kahd2mnl0g3qi1zqh9rbb4nndi5") (r "1.56")))

(define-public crate-gpiosim-0.3.1 (c (n "gpiosim") (v "0.3.1") (d (list (d (n "cap-std") (r "^2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.4") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "106pz8kz5npmnxlv6965mvjwda2s8akxfpv58y3yp1gyz4imlhvw") (r "1.63")))

(define-public crate-gpiosim-0.3.2 (c (n "gpiosim") (v "0.3.2") (d (list (d (n "cap-std") (r "^3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "global_counter") (r "^0.2") (d #t) (k 0)) (d (n "gpiocdev") (r "^0.7") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k6n6ml9vwyd3qvsl3jzhpppm4z8i9g1f12x9g2cfq2k9wrdn0rw") (r "1.63")))

(define-public crate-gpiosim-0.4.0 (c (n "gpiosim") (v "0.4.0") (d (list (d (n "cap-std") (r "^3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gpiocdev") (r "^0.7") (f (quote ("uapi_v1" "uapi_v2"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ax3j5r37ml2wjpp5lnvqdfrmhp01qjm4zrdh37vshgpzkh55gbl") (r "1.63")))

