(define-module (crates-io gp io gpio) #:use-module (crates-io))

(define-public crate-gpio-0.2.0 (c (n "gpio") (v "0.2.0") (h "06rcp7cv6gmi2aphkw8i5l9i8alzj7imqhbdgazwwxj8ds4cjcww")))

(define-public crate-gpio-0.2.1 (c (n "gpio") (v "0.2.1") (h "0ydl9a5dlagisiqxphgi19dppy17l5sfgdppl5hnwy0ikdyym1j9")))

(define-public crate-gpio-0.3.0 (c (n "gpio") (v "0.3.0") (h "0s1n3ky2lcz39cp97a82qz7izhnlkz0gmsp1i9vm6gzax5gcmka7")))

(define-public crate-gpio-0.4.0 (c (n "gpio") (v "0.4.0") (h "0p3z9aq9nfnsfbpj61grds5pjnlz562adyggyl62l46xfvlp1nsk")))

(define-public crate-gpio-0.4.1 (c (n "gpio") (v "0.4.1") (h "1iyjf89irgbzsk70947j8gm1brz0lzjjm79w8yn4frakf0r7irjg")))

