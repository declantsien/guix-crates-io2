(define-module (crates-io gp io gpiod-core) #:use-module (crates-io))

(define-public crate-gpiod-core-0.2.0 (c (n "gpiod-core") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "09l1f2yb7qfnx91r80y7y4jpz4hk1941hk8bmx1ifrnzr5l0bnhb") (f (quote (("v2") ("default" "v2"))))))

(define-public crate-gpiod-core-0.2.1 (c (n "gpiod-core") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl" "fs"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1p9f2w5ym3prfackxpga3rg6s1lyprq2hmmn1ialbc0s5w4vqv9x") (f (quote (("v2") ("default" "v2"))))))

(define-public crate-gpiod-core-0.2.2 (c (n "gpiod-core") (v "0.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("ioctl" "fs"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1khclwp7hr95klniz3mdj3yfczyb8ivhm34288payff661h486if") (f (quote (("v2") ("default" "v2"))))))

(define-public crate-gpiod-core-0.2.3 (c (n "gpiod-core") (v "0.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl" "fs"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (k 0)))) (h "1gwq39v17swg6zzda302z3jjr4r8hr6x2800bnviyssy8njsjh0y") (f (quote (("v2") ("default" "v2"))))))

(define-public crate-gpiod-core-0.3.0 (c (n "gpiod-core") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl" "fs"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "043ilibpsh8yxldcvrjp3zn4dqkwnilqfgws0hyn8i2lxcxhx9hm") (f (quote (("v2") ("default" "v2"))))))

