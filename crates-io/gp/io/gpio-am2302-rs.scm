(define-module (crates-io gp io gpio-am2302-rs) #:use-module (crates-io))

(define-public crate-gpio-am2302-rs-0.1.0 (c (n "gpio-am2302-rs") (v "0.1.0") (d (list (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1dz91sba2x0zbv5irh6nqipjwammv1nma9x30w6jf9qa9xs7r7fz")))

(define-public crate-gpio-am2302-rs-1.0.0 (c (n "gpio-am2302-rs") (v "1.0.0") (d (list (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0mgpdzgzpg6p0dw60h3yxmdcxsxpmv4vz7whrzr2z7m0mcdikavl")))

(define-public crate-gpio-am2302-rs-1.1.0 (c (n "gpio-am2302-rs") (v "1.1.0") (d (list (d (n "gpio-cdev") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0fnjrmlq8hbqv01k91arspsv1sr0hqgycpih1b68405ky1qygcm1")))

