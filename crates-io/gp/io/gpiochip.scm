(define-module (crates-io gp io gpiochip) #:use-module (crates-io))

(define-public crate-gpiochip-0.1.0 (c (n "gpiochip") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0vsazqcdkpx1q242ksr76hzsmdwwwf7zcjwwxwgi3glwmzfqlj13")))

(define-public crate-gpiochip-0.1.1 (c (n "gpiochip") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0mxlaw3g41v02jx2xhk3wxbf91wsdcd2m6vlfvar1h176qwlmzwi")))

