(define-module (crates-io gp io gpio-mqtt-bridge) #:use-module (crates-io))

(define-public crate-gpio-mqtt-bridge-0.2.0 (c (n "gpio-mqtt-bridge") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "cargo-release") (r "^0.17.1") (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rppal") (r "^0.12.0") (d #t) (k 0)) (d (n "rumqttc") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xgx2df2wgkspqdx9hdsdakj8rih2kw06qs6xbjdnb7xpqswqiwc")))

