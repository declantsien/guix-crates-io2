(define-module (crates-io gp sc gpscandump) #:use-module (crates-io))

(define-public crate-gpscandump-1.0.0 (c (n "gpscandump") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "nmea") (r "^0.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "0ac6cjd59azdsla5k6fia1q3abmmxpglzfs2bcrrvi8l9shmm40f")))

(define-public crate-gpscandump-1.0.1 (c (n "gpscandump") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "nmea") (r "^0.0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "045f03jc8v28kaly8fr9ws7d55wp6ll32jgvql31jb1k14r1ny56")))

