(define-module (crates-io gp ti gptinfo) #:use-module (crates-io))

(define-public crate-gptinfo-1.0.0 (c (n "gptinfo") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1y4vk7307ggy8pvivsmwmwf0kav1kmpj66c0g1wc183vwjz7620b")))

(define-public crate-gptinfo-1.0.1 (c (n "gptinfo") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11if6nii4zi6d4mas7jbixa3rx8s7l8xqzmyl4qzwl3nv0bk923c")))

(define-public crate-gptinfo-1.0.2 (c (n "gptinfo") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "117w1kw8hll3icm948gshnqn8djvn9wyfhrhz9d425h1w7pyap9d")))

(define-public crate-gptinfo-1.0.3 (c (n "gptinfo") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0h28ww26dbyjhr53zbnq0hni5b1c0fnz9qbwk6kw0hig02hqskv2")))

(define-public crate-gptinfo-1.0.4 (c (n "gptinfo") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "04nas1nix0ddgafmff90zlhj3bsywnqk4y25hv1dxs0ryqdnlpq7")))

(define-public crate-gptinfo-2.0.0 (c (n "gptinfo") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "1r20fj545vjl00bb3bkagj26qxcwarij25lv95jzq64hfcjnzi1j")))

(define-public crate-gptinfo-2.1.0 (c (n "gptinfo") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "const_format") (r "^0.2.10") (d #t) (k 0)))) (h "14xpfd3bq7q4cqhsw55s3sc92hndbaxdnydx9i3vnn9lldr0wfmy")))

