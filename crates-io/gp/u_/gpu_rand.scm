(define-module (crates-io gp u_ gpu_rand) #:use-module (crates-io))

(define-public crate-gpu_rand-0.1.0 (c (n "gpu_rand") (v "0.1.0") (d (list (d (n "cuda_std") (r "^0.1") (d #t) (t "cfg(target_os = \"cuda\")") (k 0)) (d (n "cust") (r "^0.1") (d #t) (t "cfg(not(target_os = \"cuda\"))") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "18lw351s94i9vzirxn10qjkfs1c97yxaqw7wrblhblgcm8nzxxwm")))

(define-public crate-gpu_rand-0.1.1 (c (n "gpu_rand") (v "0.1.1") (d (list (d (n "cuda_std") (r "^0.1") (d #t) (t "cfg(target_os = \"cuda\")") (k 0)) (d (n "cust") (r "^0.2") (d #t) (t "cfg(not(target_os = \"cuda\"))") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "03wlhs9153s9wdyrsqirvmpvfycb03xnx74axlz2fl9ssbwzxbxb")))

(define-public crate-gpu_rand-0.1.2 (c (n "gpu_rand") (v "0.1.2") (d (list (d (n "cuda_std") (r "^0.2") (d #t) (t "cfg(target_os = \"cuda\")") (k 0)) (d (n "cust") (r "^0.2") (d #t) (t "cfg(not(target_os = \"cuda\"))") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "108afdnvhkc4cagdgpxvkgcl9sgbsacbxbpd4w41qjb7yxbr2gyz")))

(define-public crate-gpu_rand-0.1.3 (c (n "gpu_rand") (v "0.1.3") (d (list (d (n "cuda_std") (r "^0.2") (d #t) (t "cfg(target_os = \"cuda\")") (k 0)) (d (n "cust_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0748p5wmyrwifgyjgczh8apkvq0djs6jq03xy7rkvgshm4003k2r")))

