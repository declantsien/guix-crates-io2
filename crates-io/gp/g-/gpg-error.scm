(define-module (crates-io gp g- gpg-error) #:use-module (crates-io))

(define-public crate-gpg-error-0.1.0 (c (n "gpg-error") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1iyhkrirkalw4psib9gq6fiavwcick89vk2782y7nalyf0vy1sry")))

(define-public crate-gpg-error-0.1.1 (c (n "gpg-error") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1w9n8grw0xjc2qlkn43gravnp8d5zxdjpp5vgn03in3sjinfq1i9")))

(define-public crate-gpg-error-0.1.2 (c (n "gpg-error") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0lzwb0j8qbr6jzpy3xyhbn62sxm8avdbzvvx7853jq551zjhzyl0")))

(define-public crate-gpg-error-0.1.3 (c (n "gpg-error") (v "0.1.3") (d (list (d (n "libgpg-error-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0z6d3raj7ihgh0kwf0vbmflyg6sx1bgwi3nf57l96k3na7xixhlp")))

(define-public crate-gpg-error-0.1.4 (c (n "gpg-error") (v "0.1.4") (d (list (d (n "libgpg-error-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1pa72xw3pdzly4f85v0dkf1hg9g81wmdrffkn9f4ph8x015gak23")))

(define-public crate-gpg-error-0.1.5 (c (n "gpg-error") (v "0.1.5") (d (list (d (n "libgpg-error-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0ghmviaz6fac1bb47y178nzk0553k1cmryk2jnljq6h5nwcd0qpy")))

(define-public crate-gpg-error-0.2.0 (c (n "gpg-error") (v "0.2.0") (d (list (d (n "libgpg-error-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0bqngx2d1lb2qrgx1gcgcyp0g86risx8ja709zkmwvxg9aphip78")))

(define-public crate-gpg-error-0.2.1 (c (n "gpg-error") (v "0.2.1") (d (list (d (n "libgpg-error-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0xa2a7f467bvis39b6nb79902ymabii7x2h8lvw2yc0rg1hiz3qd")))

(define-public crate-gpg-error-0.2.2 (c (n "gpg-error") (v "0.2.2") (d (list (d (n "libgpg-error-sys") (r "^0.2.3") (d #t) (k 0)))) (h "13z02cg4r98zjcc26981i5i24g2xcj6xrk73icy2lbjvn7qyfcn7")))

(define-public crate-gpg-error-0.3.0 (c (n "gpg-error") (v "0.3.0") (d (list (d (n "libgpg-error-sys") (r "^0.3.0") (d #t) (k 0)))) (h "124g39xsvl4a8183zch254bgjhzwbkh2ah7g425j88p32qidrhm8")))

(define-public crate-gpg-error-0.3.1 (c (n "gpg-error") (v "0.3.1") (d (list (d (n "libgpg-error-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0bb1zfhi1vvn4zd7c1g9fdpqfjh287qw9krr12f5hkmljpk1gxbv")))

(define-public crate-gpg-error-0.3.2 (c (n "gpg-error") (v "0.3.2") (d (list (d (n "libgpg-error-sys") (r "^0.3.2") (d #t) (k 0)))) (h "19w00j79jvb3h8jbvzk2l76j24i49p7jb9xdv70pqzxnwg9h9rah")))

(define-public crate-gpg-error-0.4.0 (c (n "gpg-error") (v "0.4.0") (d (list (d (n "libgpg-error-sys") (r "^0.4.0") (k 0)))) (h "01xsmp19jwwasspvnf5jsyz8161qs49dfx9fc136h3b24gi7f2vp") (f (quote (("default" "bundled") ("bundled" "libgpg-error-sys/bundled"))))))

(define-public crate-gpg-error-0.4.1 (c (n "gpg-error") (v "0.4.1") (d (list (d (n "libgpg-error-sys") (r "^0.4.1") (k 0)))) (h "0w9ylx7p56d4xh072l23xl3s5hfx714sjhan4kpqrv47afwpp4al") (f (quote (("default" "bundled") ("bundled" "libgpg-error-sys/bundled"))))))

(define-public crate-gpg-error-0.5.0 (c (n "gpg-error") (v "0.5.0") (d (list (d (n "ffi") (r "^0.5") (d #t) (k 0) (p "libgpg-error-sys")))) (h "0fnswwi1jczjz0x0w02mj1nn3062mj62z3slnfxlnjsf3v0ldwih")))

(define-public crate-gpg-error-0.5.1 (c (n "gpg-error") (v "0.5.1") (d (list (d (n "ffi") (r "^0.5.1") (d #t) (k 0) (p "libgpg-error-sys")))) (h "0dsij2a9pbrmkkkkbh7knsliffrcr9grlzq5m751548f1bvdibqy")))

(define-public crate-gpg-error-0.5.2 (c (n "gpg-error") (v "0.5.2") (d (list (d (n "ffi") (r "^0.5.2") (d #t) (k 0) (p "libgpg-error-sys")))) (h "1wb7yz8h0cr5qnw72wv7w9y4lsadjip0h5c7c1rswd1lhanbjwvh")))

(define-public crate-gpg-error-0.6.0 (c (n "gpg-error") (v "0.6.0") (d (list (d (n "ffi") (r "^0.6.0") (d #t) (k 0) (p "libgpg-error-sys")))) (h "0b2rpl4dmvkfaxax9wam70mhhd5avax8msaqihvi68xrpzfsx6nq") (r "1.64")))

(define-public crate-gpg-error-0.6.1 (c (n "gpg-error") (v "0.6.1") (d (list (d (n "ffi") (r "^0.6.1") (d #t) (k 0) (p "libgpg-error-sys")))) (h "1fnahi5k316dvr81mzbs7n9xk12413rl43m7i28pbl987nm7kp99")))

