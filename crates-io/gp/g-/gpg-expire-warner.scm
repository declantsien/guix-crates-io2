(define-module (crates-io gp g- gpg-expire-warner) #:use-module (crates-io))

(define-public crate-gpg-expire-warner-0.1.0 (c (n "gpg-expire-warner") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0qnyrk14kj19b43885f8p5qwrs2vb6fspldvxwpr2x5k2kw7gn01")))

(define-public crate-gpg-expire-warner-0.1.1 (c (n "gpg-expire-warner") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ghcd4kcaai580mrbd35sqkwvc52jvm1xidv7ky8q4q5bcg416fr")))

(define-public crate-gpg-expire-warner-0.1.2 (c (n "gpg-expire-warner") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0yipqzn4iwz7jnf7mf8dxiqq7p0f5bjq49l0g3gl48i49zf90yrq")))

(define-public crate-gpg-expire-warner-0.2.0 (c (n "gpg-expire-warner") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11v7slaqyml55fiksdyrwrvrygip3akrh0v9q0c1vyczgpbpifd8")))

(define-public crate-gpg-expire-warner-0.2.1 (c (n "gpg-expire-warner") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zj2m661pcdmd3571k3qshklj6xmyagpq9b4vambckq8czsmb9m1")))

