(define-module (crates-io gp ac gpac-sys) #:use-module (crates-io))

(define-public crate-gpac-sys-0.9.0 (c (n "gpac-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0qihddmr92pbjkcbashwayxrql0kf53l9m1cz7kr53v34zab8fdl")))

(define-public crate-gpac-sys-0.9.2 (c (n "gpac-sys") (v "0.9.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "10hqj2wwfgj223j26wwhrvh90dcw5g3x564n7izmg7ak6wr5hmcb")))

(define-public crate-gpac-sys-0.9.3 (c (n "gpac-sys") (v "0.9.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "17k0rp92ax27js9kyp7i1vygrnzix82ff1y8hvkiq9h2sxli22vi") (f (quote (("static"))))))

