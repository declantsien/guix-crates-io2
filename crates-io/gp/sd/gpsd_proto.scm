(define-module (crates-io gp sd gpsd_proto) #:use-module (crates-io))

(define-public crate-gpsd_proto-0.1.0 (c (n "gpsd_proto") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "177wc05ch5ds9aazmw9s9g9i3ayvrlnivcyxnvz6zv366hbwch2c")))

(define-public crate-gpsd_proto-0.1.1 (c (n "gpsd_proto") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ikjb3fj44j33lsx98b8ci0agfpny69rzdwqbinpbs7dcqf0r2ps")))

(define-public crate-gpsd_proto-0.1.2 (c (n "gpsd_proto") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ggyqb463p75x0al5acgs8b50qx7cq4s3cf36jxj0ngb4j0ra2sm")))

(define-public crate-gpsd_proto-0.1.3 (c (n "gpsd_proto") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16y19c9dyd8cg45bpglrhn94k7swjx8gw6ks9d2kx4pq63298sr7")))

(define-public crate-gpsd_proto-0.1.4 (c (n "gpsd_proto") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zbl3am9mn42wdm546avyyx4jr14c0c2pav1bpls5faqaq1r42fa")))

(define-public crate-gpsd_proto-0.2.0 (c (n "gpsd_proto") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ms1pj31k5lp2fbsk72lxaa3nfsqrzgg84ld07bk7jszl60dn0hq")))

(define-public crate-gpsd_proto-0.2.1 (c (n "gpsd_proto") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kjw6vls8ihg3m9dbh64sq1bfpgb1yaz10nsni7xsdxbah8qkfiv")))

(define-public crate-gpsd_proto-0.3.0 (c (n "gpsd_proto") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0kq6mzn96ixsmd27dn16hbwyghydk370xvmi2444k0j28gqbi4f3") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.4.0 (c (n "gpsd_proto") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0jxm1cb6l0ncy9p0hw6yg2c6a4iws7wbw9cc27ppp8y75nmhpqy0") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.4.1 (c (n "gpsd_proto") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1p9y3gy60cwajismwwcm3dwhwdrlx5rh7p5mpf62ik98xfskc8d4") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.5.0 (c (n "gpsd_proto") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "10j1nk031r362lvli6i27j569j3rba9421ys6c20b3g2pq9fy2y6") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.5.1 (c (n "gpsd_proto") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1lvyrd9ymw8dpi6v281i78wzszzdrz7imzphf5wbas3iqrc39rqf") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.6.0 (c (n "gpsd_proto") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "06f1n9ks0frrrixr9w0rvb15c3chkzb2qxzj6kab71h7jzi7cixw") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-0.7.0 (c (n "gpsd_proto") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.1.27") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "08fn95wycgjz7z5rv3sx07nv149sl8fsgswdiafd7x391g5iqxlx") (f (quote (("serialize") ("default"))))))

(define-public crate-gpsd_proto-1.0.0 (c (n "gpsd_proto") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("rt" "macros" "net"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 2)))) (h "19awz9whkhf3fgqy5jf1lsa23vwaq3d99y3cglvpihvbzvzz2z94") (f (quote (("serialize") ("default"))))))

