(define-module (crates-io gp sd gpsd_client) #:use-module (crates-io))

(define-public crate-gpsd_client-0.1.0 (c (n "gpsd_client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0pykps7h884b2vwq2lv92a8ryy2g66chvm8pjl6kzmyhv31avkr0") (y #t)))

(define-public crate-gpsd_client-0.1.1 (c (n "gpsd_client") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "09x71bqaak0pz2p3akfp926k9vfw3p8qab0r10psgy7ms87imdr5") (y #t)))

(define-public crate-gpsd_client-0.1.2 (c (n "gpsd_client") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0a6yidwl302mnks16idgvaxzdd8ixvdrqfa1m9lrkhmv64xhdzfn") (y #t)))

(define-public crate-gpsd_client-0.1.3 (c (n "gpsd_client") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "18ldc3fvalp4s8kigc4jj1f552y0j85c9s684pvs7ivpylikkg6n")))

(define-public crate-gpsd_client-0.1.4 (c (n "gpsd_client") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1a92q9hmzvjc43z0w5fiy2aflsj24jf9ay03fxlrsha756533580") (y #t)))

(define-public crate-gpsd_client-0.1.5 (c (n "gpsd_client") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "round") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1n6v5gnqa66l77zzpc934w5jmnik7k5qgndb67qiqzp8a0sscpy7")))

