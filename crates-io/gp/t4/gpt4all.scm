(define-module (crates-io gp t4 gpt4all) #:use-module (crates-io))

(define-public crate-gpt4all-0.1.0 (c (n "gpt4all") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "11cz2gis1qnc16ks6m63z3p0dajjpqmp8qrfkra2pdam8wvcy8ah")))

