(define-module (crates-io gp ur gpurs) #:use-module (crates-io))

(define-public crate-gpurs-0.1.0 (c (n "gpurs") (v "0.1.0") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "156mjsyay67x8da0aj990k6gm5x0k70301s6ap03nzb3synhjk85") (y #t)))

(define-public crate-gpurs-0.1.1 (c (n "gpurs") (v "0.1.1") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "1hd1a5qhqr9pqj8pvq8rwcgrhg1mr0459xdcbgja9ycrxdl6x8ir")))

(define-public crate-gpurs-0.2.0 (c (n "gpurs") (v "0.2.0") (d (list (d (n "opencl3") (r "^0.9.3") (d #t) (k 0)))) (h "0srf8dmcmzs83jf8sap7xnx6zyhj2sd8d9xq4mfyvjihdyl6vqvs")))

(define-public crate-gpurs-0.3.0 (c (n "gpurs") (v "0.3.0") (d (list (d (n "opencl3") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "10i8l8v9sjwxbch2zcjh3h4l3k0mdzqc212l803hfwhpn1q7sxg7") (f (quote (("default" "gpu_accel")))) (s 2) (e (quote (("gpu_accel" "dep:opencl3"))))))

(define-public crate-gpurs-0.3.1 (c (n "gpurs") (v "0.3.1") (d (list (d (n "opencl3") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "02f90l9svjcixnbrrk989y0vqb0xpi33xgy7l26v4751y2lpz0k7") (f (quote (("default" "gpu_accel")))) (s 2) (e (quote (("gpu_accel" "dep:opencl3"))))))

(define-public crate-gpurs-0.4.0 (c (n "gpurs") (v "0.4.0") (d (list (d (n "opencl3") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "070paxx8bkyq9752rjjp5wnd1jqjdgp1s39qfma05fhwqdnqghpx") (f (quote (("default" "gpu_accel")))) (s 2) (e (quote (("gpu_accel" "dep:opencl3"))))))

