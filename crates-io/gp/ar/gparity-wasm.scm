(define-module (crates-io gp ar gparity-wasm) #:use-module (crates-io))

(define-public crate-gparity-wasm-0.45.0 (c (n "gparity-wasm") (v "0.45.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1mb1any62r0qklafjkc1ina2xqg4wy5sm00fgcjgax47ji251v2k") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

