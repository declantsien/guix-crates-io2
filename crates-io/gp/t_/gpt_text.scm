(define-module (crates-io gp t_ gpt_text) #:use-module (crates-io))

(define-public crate-gpt_text-0.1.0 (c (n "gpt_text") (v "0.1.0") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)))) (h "1k2kc74bank3nn8q6yidf1526bq9c9mm1lx7l15qdgml5k8my970")))

(define-public crate-gpt_text-0.1.1 (c (n "gpt_text") (v "0.1.1") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)))) (h "1v74rjgvw9wk2gs86hhwv2kkcirsxgjb7gma9a0viic8abywgvzl")))

(define-public crate-gpt_text-0.1.2 (c (n "gpt_text") (v "0.1.2") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1ngk6ah10ifx7qbcx89gxrrmaai9b2h375h6dl80dp1scdy9ywiz")))

(define-public crate-gpt_text-0.1.3 (c (n "gpt_text") (v "0.1.3") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0iais771a0hj482g50yxs15ghjc96i7s21b6q83bpx1rabch6g6n")))

(define-public crate-gpt_text-0.1.4 (c (n "gpt_text") (v "0.1.4") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0gv0z989f1i9pk4rlwsdqr2iiprhxxkqg71y2m4hnqs3f81cs7aw")))

(define-public crate-gpt_text-0.1.5 (c (n "gpt_text") (v "0.1.5") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0jlsd65kblx9dzkmwsrgp4b45vhp5any0lyf0bmaidfilif95lif")))

(define-public crate-gpt_text-0.1.6 (c (n "gpt_text") (v "0.1.6") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1x237d6pn9nvlaqs8f38ljssjhbsqk1r4l45p2s04wv2mqgigy79")))

(define-public crate-gpt_text-0.1.7 (c (n "gpt_text") (v "0.1.7") (d (list (d (n "fieri") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0mr6a5kpdp072g4l9xmnh1al8ikvpz539mnz714q1jng699vd09h")))

