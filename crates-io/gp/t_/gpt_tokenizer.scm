(define-module (crates-io gp t_ gpt_tokenizer) #:use-module (crates-io))

(define-public crate-gpt_tokenizer-0.1.0 (c (n "gpt_tokenizer") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0mxmgnmqr9h84zwgkafwqhliyla37n2ps24pixaa5plgmxinn4iv")))

