(define-module (crates-io gp t_ gpt_disk_types) #:use-module (crates-io))

(define-public crate-gpt_disk_types-0.5.0 (c (n "gpt_disk_types") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)))) (h "08bralx62bqmp4d6vhpz8cffc5k8wqqy1nq6cb4vgnmbqzjkjhra") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.6.0 (c (n "gpt_disk_types") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)))) (h "1ggmm75j523rd91g0bpanqidda1j8l0k0vhxa612xia1wfqjq120") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.7.0 (c (n "gpt_disk_types") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)))) (h "1s134yp9axc9mzjlpzy2qs4al3bpi920ldnp9bh34y87phhxz5f0") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.8.0 (c (n "gpt_disk_types") (v "0.8.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)))) (h "1jy8iw5vzaa6wyal8yy871m9achpm9rlz6shvm1y22h8is798bdz") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.9.0 (c (n "gpt_disk_types") (v "0.9.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^0.5.0") (d #t) (k 0)))) (h "09fdhyk8a82srjjx7siakmbjy46p5g09r916251y1arp6776ysjr") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.10.0 (c (n "gpt_disk_types") (v "0.10.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^0.6.0") (d #t) (k 0)))) (h "0238f5aa8hbcalvy14mxdhqrpk39lvgzm2i9nhfsd4iwn2jma4wi") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.11.0 (c (n "gpt_disk_types") (v "0.11.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^0.7.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "03520w9xbgmnmcvvrc62gid4x58af5mmnhw2dk6xvd1p9hd6d8c8") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.12.0 (c (n "gpt_disk_types") (v "0.12.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^1.0.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0vhwfvm7cnxw4cfxj2f208mv7fs74nv9fw84b1rdcv67yyyz2xj6") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.13.0 (c (n "gpt_disk_types") (v "0.13.0") (d (list (d (n "bytemuck") (r "^1.9.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^1.0.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1hyjb0fsj1bjimm19390yzgdnpaa40zs019xsqsab7cq7l2f10k1") (f (quote (("std"))))))

(define-public crate-gpt_disk_types-0.14.0 (c (n "gpt_disk_types") (v "0.14.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^1.0.0") (d #t) (k 0)))) (h "0f575byxapjln0dj12p7fsa15dixs2jpaz0qhb7r0mi20qxfhg1a") (f (quote (("std" "uguid/std")))) (s 2) (e (quote (("bytemuck" "dep:bytemuck" "uguid/bytemuck"))))))

(define-public crate-gpt_disk_types-0.15.0 (c (n "gpt_disk_types") (v "0.15.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^2.0.0") (d #t) (k 0)))) (h "0ixzgxwys2pnplqmmdigjl1dd90r0lqlr0vq05g31dwvri9fr70w") (f (quote (("std" "uguid/std")))) (s 2) (e (quote (("bytemuck" "dep:bytemuck" "uguid/bytemuck")))) (r "1.60")))

(define-public crate-gpt_disk_types-0.16.0 (c (n "gpt_disk_types") (v "0.16.0") (d (list (d (n "bytemuck") (r "^1.4.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)) (d (n "uguid") (r "^2.1.0") (d #t) (k 0)))) (h "08ch2j58b81h9bc8gm6z4hrpap4zk5ahfh9sfhvh9qxl7ifg4rci") (f (quote (("std" "uguid/std")))) (s 2) (e (quote (("bytemuck" "dep:bytemuck" "uguid/bytemuck")))) (r "1.68")))

