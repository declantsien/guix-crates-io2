(define-module (crates-io gp t_ gpt_fn_macros) #:use-module (crates-io))

(define-public crate-gpt_fn_macros-1.0.0 (c (n "gpt_fn_macros") (v "1.0.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "1ljkia6g2sxakhm81z65b41ql1y9ys37aqca3v7yl1a0c94qxqix")))

