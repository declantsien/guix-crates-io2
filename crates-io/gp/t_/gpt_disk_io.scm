(define-module (crates-io gp t_ gpt_disk_io) #:use-module (crates-io))

(define-public crate-gpt_disk_io-0.5.0 (c (n "gpt_disk_io") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "02a533vxvb44fln87hn4gxvhqmjxczw327srfb72nk3wggdzw1gg") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.6.0 (c (n "gpt_disk_io") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.6.0") (d #t) (k 0)))) (h "120hj20vkx2953rxpijvjkmh6zlwbl8dql68fn66nmvb1m6laypc") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.7.0 (c (n "gpt_disk_io") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.7.0") (d #t) (k 0)))) (h "19fyh53asczrjn7anjcliad25qm5hgh178lyv5s63wvaxjqj29hq") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.8.0 (c (n "gpt_disk_io") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.8.0") (d #t) (k 0)))) (h "1piq2cg7jwiwbhfv7rcvf4ri2225ac5g3jkqplfgpqgka42n59dh") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.9.0 (c (n "gpt_disk_io") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.9.0") (d #t) (k 0)))) (h "1k9pw3h70r0q1cgrhssgqbyyy0jrvb5lzgsrn9h2rc9r67dapdqb") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.10.0 (c (n "gpt_disk_io") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.10.0") (d #t) (k 0)))) (h "1j65p1jd6gr1j99ml1xn7wsvcsqmsw9zq88z2i4y2j6p6smvqmf3") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.11.0 (c (n "gpt_disk_io") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bytemuck") (r "^1.9.1") (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.11.0") (d #t) (k 0)))) (h "17h53vixkif2dcy3pr0ln3c19iy9vnsis03w23zp97l47c51h1c7") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.12.0 (c (n "gpt_disk_io") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bytemuck") (r "^1.9.1") (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.12.0") (d #t) (k 0)))) (h "0rgzd0yf9vwir7yn4yn7v2s5h915glv3pm2zagckcqgwiinshrqh") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.13.0 (c (n "gpt_disk_io") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bytemuck") (r "^1.9.1") (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.13.0") (d #t) (k 0)))) (h "070ywa45bsi7vmvaiw4kdp8rpj1yfb3zmszbssgj3i4z42gdpddv") (f (quote (("std"))))))

(define-public crate-gpt_disk_io-0.14.0 (c (n "gpt_disk_io") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.4.0") (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.14.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "12izkwi46jfrn4fzidpp97ksmg2dmci310v74a3z184bw8c8lpx0") (f (quote (("std" "gpt_disk_types/std"))))))

(define-public crate-gpt_disk_io-0.15.0 (c (n "gpt_disk_io") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.4.0") (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 2)) (d (n "gpt_disk_types") (r "^0.15.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1p63azmfvrkilmd6zc8k15vcyk55jw3ry56k7ml42kyirq908wn0") (f (quote (("std" "gpt_disk_types/std")))) (r "1.60")))

(define-public crate-gpt_disk_io-0.16.0 (c (n "gpt_disk_io") (v "0.16.0") (d (list (d (n "bytemuck") (r "^1.4.0") (k 0)) (d (n "gpt_disk_types") (r "^0.16.0") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1ham2ljw9masn3r1f8k9s61q20zyivycr0piqb9s0v6ssa38k59p") (f (quote (("std" "alloc" "gpt_disk_types/std") ("alloc")))) (r "1.68")))

