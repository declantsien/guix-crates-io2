(define-module (crates-io gp t_ gpt_commit) #:use-module (crates-io))

(define-public crate-gpt_commit-0.1.0 (c (n "gpt_commit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09bhqrb8z512yplggcqbalflxl1wcznggz68icl1q8pjjydi27fk")))

(define-public crate-gpt_commit-0.1.1 (c (n "gpt_commit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qris06r6q80awca1nd4np6wdhqhjfl5gl7l01fs2xxm89wnlvs7")))

(define-public crate-gpt_commit-0.1.2 (c (n "gpt_commit") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a015b5xf1vxmscqsb8hxnh46m7yqg9yvb9rm1qdsv95dgq2awy4")))

