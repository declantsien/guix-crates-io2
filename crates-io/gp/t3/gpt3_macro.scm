(define-module (crates-io gp t3 gpt3_macro) #:use-module (crates-io))

(define-public crate-gpt3_macro-0.1.0 (c (n "gpt3_macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0rjg2bi8q4vwaspxf872nn1sbcaqjzpvz84k741rg8d8i2js62a4")))

(define-public crate-gpt3_macro-0.2.0 (c (n "gpt3_macro") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0h6rabw40sa57m5jh9jsjxw5jg6c816pfhmnc5nwlpylh8dilrhp")))

(define-public crate-gpt3_macro-0.2.1 (c (n "gpt3_macro") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "0l018ggqr4xvxxcfwpfpgmc6alyaij2irmz01hr7bhqldqivk5fp")))

(define-public crate-gpt3_macro-0.2.2 (c (n "gpt3_macro") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1psblky6h4qcmvz2f2ad6nhb48byq2nbs4kmhnc0npld9bph5gsn")))

(define-public crate-gpt3_macro-0.3.1 (c (n "gpt3_macro") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1gjvbpv6wx5hw72kbc2lfjllvykvd6169qdw0bjkgbf3a4q4m17z")))

