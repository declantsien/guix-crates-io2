(define-module (crates-io gp tc gptcli) #:use-module (crates-io))

(define-public crate-gptcli-0.1.0 (c (n "gptcli") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0x3l1jvkh3kj6yvvr1k22jjdw0l4k1rgrrcpmbxdx2khiyxw3k20")))

