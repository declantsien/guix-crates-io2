(define-module (crates-io n8 he n8henrie) #:use-module (crates-io))

(define-public crate-n8henrie-0.1.0 (c (n "n8henrie") (v "0.1.0") (h "1wvam3d52bm3blk1qs0n13xwpb3437p9iwdrjqw64nhgi49nxpj3")))

(define-public crate-n8henrie-0.1.1 (c (n "n8henrie") (v "0.1.1") (h "07mlx76wr397j37699xjxnmxw6jdnccphb2dq32nba6d7c3pdx0b") (f (quote (("pola-rs-polars-issues-6123"))))))

(define-public crate-n8henrie-0.1.2 (c (n "n8henrie") (v "0.1.2") (h "076p74ybdf7fpsmbksdh6fmmm260s6d6cz4gs60d7250lp0lb81w") (f (quote (("pola-rs-polars-issues-6123"))))))

(define-public crate-n8henrie-0.1.3 (c (n "n8henrie") (v "0.1.3") (h "042jxai1cb8h4j2p8qg42j99vympc1msn0lbnc57qkamz9353r87") (f (quote (("pola-rs-polars-issues-6123"))))))

(define-public crate-n8henrie-0.1.4 (c (n "n8henrie") (v "0.1.4") (h "1xfpi7rm2qbqc9dniah85k89njj1bglarlh1nympv05838f6lqi7") (f (quote (("pola-rs-polars-issues-6123"))))))

(define-public crate-n8henrie-0.1.5 (c (n "n8henrie") (v "0.1.5") (h "039la5dg87f5bdz1703la7yqblg89qh5vdvk7qvgaggw4gk8b2vk") (f (quote (("foo"))))))

(define-public crate-n8henrie-0.1.6 (c (n "n8henrie") (v "0.1.6") (h "1nhjgb5mvxy7820vrqwkaaa6sr6qlcmsy5gv0vz1b6bl9f37w53s") (f (quote (("foo"))))))

(define-public crate-n8henrie-0.1.8 (c (n "n8henrie") (v "0.1.8") (h "000dhj6fjz0nb3zwpfnfbwi77wi4rnixr8rqsbgdjjn8kn78j8wj") (f (quote (("foo"))))))

