(define-module (crates-io wn g- wng-lib) #:use-module (crates-io))

(define-public crate-wng-lib-0.1.0 (c (n "wng-lib") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1lrzf3gqsrv9slajb53nq2v6h3v656jx5yr9l9j6xwkvgy1nwm80")))

(define-public crate-wng-lib-0.1.1 (c (n "wng-lib") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0f1gbiyvc0bjdbdrzmb12by842k16lmjii9lan2ifbhhi1hcn111")))

(define-public crate-wng-lib-0.1.2 (c (n "wng-lib") (v "0.1.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0aa38r4y9n04n87lz5dy6148qsdm4hv2syzmdjdsvg4dqfgnxsr6")))

(define-public crate-wng-lib-0.1.3 (c (n "wng-lib") (v "0.1.3") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1w1pf53rdmgc0aj24x55y4k76caq5cqgyydf4f790dykdfn9ah1a")))

