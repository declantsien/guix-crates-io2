(define-module (crates-io wn ck wnck-sys) #:use-module (crates-io))

(define-public crate-wnck-sys-0.1.0 (c (n "wnck-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "fragile") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gdk-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nbjx5h00zvgabkk1c98qjxmnixf4shdp30b9ij29y99zd5cs41f")))

