(define-module (crates-io dm s- dms-coordinates) #:use-module (crates-io))

(define-public crate-dms-coordinates-0.1.0 (c (n "dms-coordinates") (v "0.1.0") (h "0wjzc5bi6dj94qgg0wia308fi8x62ag3kplbp36qp7adkh0fw01w")))

(define-public crate-dms-coordinates-0.1.1 (c (n "dms-coordinates") (v "0.1.1") (h "0sl59n879zlb7mlkx2mf50jg535yp08sfd2w94nanzkjz7pbki91")))

(define-public crate-dms-coordinates-0.1.2 (c (n "dms-coordinates") (v "0.1.2") (h "0gp0wm25rs8wvbcaydbnzprkglgvw40xzijp84kyvprwzwxvniy1")))

(define-public crate-dms-coordinates-0.1.3 (c (n "dms-coordinates") (v "0.1.3") (h "0sznyg6n4d5dgxvixlv5ny9knz9500shrn7n8x1m3ja3bpf4hg87")))

(define-public crate-dms-coordinates-0.1.4 (c (n "dms-coordinates") (v "0.1.4") (h "0hd91idvgd629sv39y1rdayxpgl4spd92gsv4vjsinmyx9z83z8h")))

(define-public crate-dms-coordinates-0.1.5 (c (n "dms-coordinates") (v "0.1.5") (h "0nnzmi78am727iw5zd7891ikl1j9p39dm55p01rjzslrc0rqykil")))

(define-public crate-dms-coordinates-1.0.0 (c (n "dms-coordinates") (v "1.0.0") (d (list (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dav93qhwvvnz9x9hj1fy0hvg6yywnv739psbss6ikjq8sm1xnk6")))

(define-public crate-dms-coordinates-1.0.1 (c (n "dms-coordinates") (v "1.0.1") (d (list (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wa8hxwvaj67ii1izsw1bv22kqf8apcm3zg58ap7fwymic08pbcw")))

(define-public crate-dms-coordinates-1.0.2 (c (n "dms-coordinates") (v "1.0.2") (d (list (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "126bg2d7aqvssxisah525dmznyb3vq25kmilzfwqaz19ri9md7jw")))

(define-public crate-dms-coordinates-1.1.0 (c (n "dms-coordinates") (v "1.1.0") (d (list (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (d #t) (k 0)) (d (n "initial_conditions") (r "^0.4.0") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "rust-3d") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03fr60p8xdy6dcnqhpgsb63jg5g1j89pdzqmlxik0rsfqf646szc")))

(define-public crate-dms-coordinates-1.2.0 (c (n "dms-coordinates") (v "1.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (o #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-3d") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ws0yrabbrz95r5ji975zjs2fxcxdy85j83d4ndkw2wjhpicmqbq") (f (quote (("std" "serde" "serde_derive") ("default"))))))

(define-public crate-dms-coordinates-1.3.0 (c (n "dms-coordinates") (v "1.3.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "gpx") (r "^0.8.1") (o #t) (k 0)) (d (n "map_3d") (r "^0.1.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-3d") (r "^0.34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wg6j8xn9ifrc7q26liwdqxwi653q8s327wx540p93a9ci7aia3v") (f (quote (("std" "serde" "serde_derive") ("default"))))))

