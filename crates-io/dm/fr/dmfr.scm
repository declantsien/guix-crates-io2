(define-module (crates-io dm fr dmfr) #:use-module (crates-io))

(define-public crate-dmfr-0.1.0 (c (n "dmfr") (v "0.1.0") (d (list (d (n "regress") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0a57c3rw2zaka44qjk0l8w5hxhayil4gsj1bqa50fy3qd93i4zxv")))

