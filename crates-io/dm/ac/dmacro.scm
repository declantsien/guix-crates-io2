(define-module (crates-io dm ac dmacro) #:use-module (crates-io))

(define-public crate-dmacro-0.1.0 (c (n "dmacro") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "01hpfvqk1y6s57pmq6jpzjsr996abi9rf35if2xr9nn2jm3jh2pm") (y #t)))

(define-public crate-dmacro-0.1.1 (c (n "dmacro") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1l31is7w6pw1mbxdg6ymjis8wpbvy911q2g8mdazqplkryab204h") (y #t)))

(define-public crate-dmacro-0.1.2 (c (n "dmacro") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0p42p5rzhybgm2j6ax3bj4i5kz53jwa38xcls5llpbfg3hymgpvl") (y #t)))

(define-public crate-dmacro-0.1.3 (c (n "dmacro") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1l4zdrgppz3j61cx8d73474z0ig0xz4icc19ld40vjaabrbydjgm") (y #t)))

(define-public crate-dmacro-0.1.4 (c (n "dmacro") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1ga2vc76cyfbmplbvy9hmqdksri6fxirbgzryjajv3danvhvhwy7") (y #t)))

(define-public crate-dmacro-0.1.5 (c (n "dmacro") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1l2wszw2fmmjy1kxm16zpx4vk3qxzj5wrf65v7ff79mrx9xk9vya") (y #t)))

(define-public crate-dmacro-0.1.6 (c (n "dmacro") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1sfm5krhn01jz2lhmw4w74p4zbg9sxvja0fwlzzl9wsnw360590g")))

(define-public crate-dmacro-0.1.7 (c (n "dmacro") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0n1ddc1kypp1vxs9c77nhxyyj1flzvq2mcpd7q2jx2fpdkz38cai")))

(define-public crate-dmacro-0.1.8 (c (n "dmacro") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "132ijwm2qwkqadd0vlhszca7d72j47k8qhklkd5rj6m0gn61d97c")))

(define-public crate-dmacro-0.1.9 (c (n "dmacro") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0qmqm8sazicscdv670jm68a23dfhw7zxh85xzxfi68wgkn4s2l6x")))

