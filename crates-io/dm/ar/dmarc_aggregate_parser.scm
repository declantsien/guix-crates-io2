(define-module (crates-io dm ar dmarc_aggregate_parser) #:use-module (crates-io))

(define-public crate-dmarc_aggregate_parser-0.1.0 (c (n "dmarc_aggregate_parser") (v "0.1.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0ppwb98xdv8fbvb8brkxvli56sn98b2jpghh31mb4cwxrxgfqkyp") (y #t)))

(define-public crate-dmarc_aggregate_parser-0.1.1 (c (n "dmarc_aggregate_parser") (v "0.1.1") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "185bhypfk3pxfvlapw29853nhpazcppi404czvnggkdc29887pxl")))

(define-public crate-dmarc_aggregate_parser-0.1.2 (c (n "dmarc_aggregate_parser") (v "0.1.2") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0ggqilmswd8wmyhzahvqqd3yygyhwlfsvbmy3gvaz7hm7vzl1253")))

(define-public crate-dmarc_aggregate_parser-0.1.3 (c (n "dmarc_aggregate_parser") (v "0.1.3") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "00x3hyqkh033cph7c3z4bwchb7rp0cnw16npx957q26iy2cjyl32")))

(define-public crate-dmarc_aggregate_parser-0.1.4 (c (n "dmarc_aggregate_parser") (v "0.1.4") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1rwpd9kxwpp1ykp71v7cqk0vpy3czjcfh9bmg78xcyf2mxjmrg4j")))

