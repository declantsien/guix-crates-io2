(define-module (crates-io dm nt dmntk-feel-parser) #:use-module (crates-io))

(define-public crate-dmntk-feel-parser-0.0.5 (c (n "dmntk-feel-parser") (v "0.0.5") (d (list (d (n "dmntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.5") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.5") (d #t) (k 1)))) (h "0zgvxw334gwgs7w5fgklq3ni8xxzsjn1ya1q6slk9pfjl30b2g2p")))

(define-public crate-dmntk-feel-parser-0.0.6 (c (n "dmntk-feel-parser") (v "0.0.6") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.6") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.6") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.6") (d #t) (k 1)))) (h "07l35b2n3lb6c5c8y4dw39znhrszgkag82qvvagi64q6icpv8xwf")))

(define-public crate-dmntk-feel-parser-0.0.7 (c (n "dmntk-feel-parser") (v "0.0.7") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.7") (d #t) (k 1)))) (h "1rnjmp9rbgifw9l5cbp9f6ic2dbgrx3ggfy22vamfd5vf2k7b53a")))

(define-public crate-dmntk-feel-parser-0.0.9 (c (n "dmntk-feel-parser") (v "0.0.9") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.9") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.9") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.9") (d #t) (k 1)))) (h "19b4w18c4blg9w5difs9jgkx3an4sg9ncvg8x9niscszlfwfg4q1") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.10 (c (n "dmntk-feel-parser") (v "0.0.10") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-feel") (r "^0.0.10") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.10") (d #t) (k 1)))) (h "07gbg8pf9x1f34wq51gl57yvkpw6vz80zawhcxmx9mwrfmnbyk7v") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.11 (c (n "dmntk-feel-parser") (v "0.0.11") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-feel") (r "^0.0.11") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.11") (d #t) (k 1)))) (h "1mwki5lslzn73d5yb91wlxp0i1qid6323l7blqbilpd27kjz380c") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.20 (c (n "dmntk-feel-parser") (v "0.0.20") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.20") (d #t) (k 1)))) (h "0264zwhqm0xzcwbqa4fqpyr3bmm646733ivrxv5v4sl76ig65614") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.35 (c (n "dmntk-feel-parser") (v "0.0.35") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.35") (d #t) (k 1)))) (h "08imdwc5ip3mfwisy99ydmpwmvgarji510cxm0mwg9cj5ands67k") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.36 (c (n "dmntk-feel-parser") (v "0.0.36") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.36") (d #t) (k 1)))) (h "1gknnx19yjs5vs2lqcc9dx670507riwpbwmgn955pgqgp9qkngwg") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.37 (c (n "dmntk-feel-parser") (v "0.0.37") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.37") (d #t) (k 1)))) (h "1p7q84b462w7fr3hjmssc1yma5rfj3j76442mgmjqh8zq0r52wwi") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.38 (c (n "dmntk-feel-parser") (v "0.0.38") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.38") (d #t) (k 1)))) (h "1kkqg62ckfv3hdz4l6yaci9hmw483vrl54z570fc2b9ys0b4z802") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.40 (c (n "dmntk-feel-parser") (v "0.0.40") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.40") (d #t) (k 1)))) (h "11l5bn09lziwixbmy68w6i78h2l2zrjv8qcxwkqzgwn1kvcjvpf0") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.41 (c (n "dmntk-feel-parser") (v "0.0.41") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.41") (d #t) (k 1)))) (h "10h34pn5d81n2phhr8sqmplir8099gvskz5672lf764f0bghnyp6") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.42 (c (n "dmntk-feel-parser") (v "0.0.42") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.42") (d #t) (k 1)))) (h "0lffn8fdqlmmqj3njdglyyadm4ykgwx0zhmv26v400m4w37i8v5r") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.43 (c (n "dmntk-feel-parser") (v "0.0.43") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.43") (d #t) (k 1)))) (h "1m8mkdhxf1s8n2r5gnaljfpzjzn7nrnnccbmyllf8vygw7s9bmlb") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.44 (c (n "dmntk-feel-parser") (v "0.0.44") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.44") (d #t) (k 1)))) (h "0bxpylz2y3j83mrwkwh73njwm453zan8lr7m4x4yg7cajvrz79bd") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.45 (c (n "dmntk-feel-parser") (v "0.0.45") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.45") (d #t) (k 1)))) (h "1yb0hiqq04d925h1whwyn8ysr0lzymdwvl1zl8brd7vj74q6i9cp") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.46 (c (n "dmntk-feel-parser") (v "0.0.46") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.46") (d #t) (k 1)))) (h "0xy3hi16h2jqfyh161fdz6v330wz2h2q9nhnwzidxnn8zh3b0vwj") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.47 (c (n "dmntk-feel-parser") (v "0.0.47") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.47") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.47") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.47") (d #t) (k 1)))) (h "0d1jp1lh7jbalqh71hxl5j94y9y80f2g9a4miqlknckhablakg0a") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.48 (c (n "dmntk-feel-parser") (v "0.0.48") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "=0.0.48") (d #t) (k 1)))) (h "0jg9q92sz795swqancrnbh1270x60hr4mjcylg65b40v39x8ra95") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.49 (c (n "dmntk-feel-parser") (v "0.0.49") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "=0.0.49") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.49") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "=0.0.49") (d #t) (k 1)))) (h "1kg4mfgsql18gqjwky8zq6h1ld6gk8n6v6325vxncx0s3nsjgvb3") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.50 (c (n "dmntk-feel-parser") (v "0.0.50") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "=0.0.50") (d #t) (k 1)))) (h "0d3gn13yja9shdwih4mzn9xg3gzadm63cxfa07xp7qg2r0fq9y43") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.52 (c (n "dmntk-feel-parser") (v "0.0.52") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.52") (d #t) (k 1)))) (h "0apikzl1jljh6lg9as67l65s3f8b4dr0r9rmqa89s8dcgdkmn1zc") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.53 (c (n "dmntk-feel-parser") (v "0.0.53") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.53") (d #t) (k 1)))) (h "0q7ma4qkhrr2vnpw4vx95kkmvw9q68bipb91jzsmykrxz61kjdxd") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.0.54 (c (n "dmntk-feel-parser") (v "0.0.54") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.54") (d #t) (k 1)))) (h "1f84b7pqll2vsrk071qh5g9sdf5y09h6wh1kfz48j9kik0kfzhyd") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.1.0 (c (n "dmntk-feel-parser") (v "0.1.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.1.0") (d #t) (k 1)))) (h "1y728k1zsmik4hnwm04s38kfadi4w7f1gpnw73aa23hbdd4qc9ck") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.1.1 (c (n "dmntk-feel-parser") (v "0.1.1") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.1.0") (d #t) (k 1)))) (h "0b9r93r2kwc51sc06xlcdbxwcb4hinp6klx8rd7x8m52z2i13wgj") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.1.2 (c (n "dmntk-feel-parser") (v "0.1.2") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.1.2") (d #t) (k 1)))) (h "1hl285qyrysy1hmbfc9q44q2l6hnw5si2zg1an9q1fyjrxchgbhg") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.2.0 (c (n "dmntk-feel-parser") (v "0.2.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.2.0") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0pvahjj4kkm59jicrqqyaxirs2sfaz0p0a37dkia8p7m8y53wkks") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.2.5 (c (n "dmntk-feel-parser") (v "0.2.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.2.5") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.2.5") (d #t) (k 0)))) (h "0dj96smg0dypqbm0lvphx551z97vg6jrapq8gkhcn9665abxzm93") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.0 (c (n "dmntk-feel-parser") (v "0.3.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.0") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1i3bbf4wgla92yrjykk8h2mgbzwqlcdac90jp8n0w9a7pjwwjmk3") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.1 (c (n "dmntk-feel-parser") (v "0.3.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common-1") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.0") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.1") (d #t) (k 0)))) (h "0qh0hnh7k3z0jjzmnnbsrpcd2q83ac3l3h3m56n1rpfy7dxvn52b") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.2 (c (n "dmntk-feel-parser") (v "0.3.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.2") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1nlf31d6l8qfwlwai4fsdlkchh8lhiim0a0nd2pis4p9jha65xz8") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.3 (c (n "dmntk-feel-parser") (v "0.3.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.3") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.3") (d #t) (k 0)))) (h "1jfc6pa8cl1xhf88qn489mhslhkvj0zp7wlwbzfbb7d9s4rm1fjx") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.4 (c (n "dmntk-feel-parser") (v "0.3.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.4") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.4") (d #t) (k 0)))) (h "1gsbd6wl0wikwy1340qi02sk5mziaqh68zznxm7al1nqlric93d6") (f (quote (("parsing-tables"))))))

(define-public crate-dmntk-feel-parser-0.3.5 (c (n "dmntk-feel-parser") (v "0.3.5") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "dmntk-common") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.3.5") (d #t) (k 1)) (d (n "dmntk-macros") (r "^0.3.5") (d #t) (k 0)))) (h "02rphspksg309zl353syjwl3iacgn8b5my90qm4qfc26cd4986n5") (f (quote (("parsing-tables"))))))

