(define-module (crates-io dm nt dmntk-feel-number) #:use-module (crates-io))

(define-public crate-dmntk-feel-number-0.0.41 (c (n "dmntk-feel-number") (v "0.0.41") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.41") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hy5bwbcz1vmzcw2lzb4s69aj8lldcqk5417jjmj6wyq60621lw7") (l "decnumber")))

(define-public crate-dmntk-feel-number-0.0.42 (c (n "dmntk-feel-number") (v "0.0.42") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.42") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1k81bgifw7n4sjr6ysk2r0pyd2p9rsvxac53bg003sjyaxwjv2jw")))

(define-public crate-dmntk-feel-number-0.0.43 (c (n "dmntk-feel-number") (v "0.0.43") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.43") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12285pphbrrbwfid72jgzd6l1nbf9rzd37gn4vxrk53vnh1rclxa")))

(define-public crate-dmntk-feel-number-0.0.44 (c (n "dmntk-feel-number") (v "0.0.44") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0clrc27ny5ddjjgl0nk9jlj6ykyjfkd8g81idrlir7iwy424f403")))

(define-public crate-dmntk-feel-number-0.0.45 (c (n "dmntk-feel-number") (v "0.0.45") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.45") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qm5k81qmj5hcpf9xinixxps03as41bq9l3zf16224z356yxcy2c")))

(define-public crate-dmntk-feel-number-0.0.46 (c (n "dmntk-feel-number") (v "0.0.46") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "dmntk-common") (r "^0.0.46") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)))) (h "0bkkc00pby14yy6bcpsqny0cdhbw0906y3jvg8zvwqym4y93agz6")))

(define-public crate-dmntk-feel-number-0.0.47 (c (n "dmntk-feel-number") (v "0.0.47") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.0.47") (d #t) (k 0)))) (h "0k7m0xr2sbphy5gfqabnmhsszpv63hwrx4y5i1j75k7bvclshsg8")))

(define-public crate-dmntk-feel-number-0.0.48 (c (n "dmntk-feel-number") (v "0.0.48") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "=0.0.48") (d #t) (k 0)))) (h "00n7jjrbrabjdfzfdj2ksk3aa5k1x6yvijzdbqgypiwr6rg5ygdi")))

(define-public crate-dmntk-feel-number-0.0.49 (c (n "dmntk-feel-number") (v "0.0.49") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "=0.0.49") (d #t) (k 0)))) (h "154hk1gvib6mq9m81a5zdw3zlmd83ammmpapgr5afwydsgqgdxn4")))

(define-public crate-dmntk-feel-number-0.0.50 (c (n "dmntk-feel-number") (v "0.0.50") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "=0.0.50") (d #t) (k 0)))) (h "11grkv3jsmy5sl81ba9m5iminkjhcmq0cvil0a1l6jiw9zq9w0py")))

(define-public crate-dmntk-feel-number-0.0.52 (c (n "dmntk-feel-number") (v "0.0.52") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "=0.0.52") (d #t) (k 0)))) (h "0z6804r7dlhd8pq1qad8141my6bwf8k7r5x3p621ykz0gr2swlxf")))

(define-public crate-dmntk-feel-number-0.0.53 (c (n "dmntk-feel-number") (v "0.0.53") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.0.53") (d #t) (k 0)))) (h "1b7wj5s5dhvni16nf22bvzhdnkw6j15jw4vhpnv8fljiaynh8rf7")))

(define-public crate-dmntk-feel-number-0.0.54 (c (n "dmntk-feel-number") (v "0.0.54") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.0.54") (d #t) (k 0)))) (h "1l0nwcglaj1fgag16rpgnq3fcz179xyb0hbf95nkzpvpn8q71vd1")))

(define-public crate-dmntk-feel-number-0.1.0 (c (n "dmntk-feel-number") (v "0.1.0") (d (list (d (n "dfp-number-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)))) (h "17abssmvdwiga80klysi4ja2im51myljn92xmxy7r5syn84liq32")))

(define-public crate-dmntk-feel-number-0.1.1 (c (n "dmntk-feel-number") (v "0.1.1") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)))) (h "11zh34mm80f0qslwi1c1iszbyzhr21892ijz1jg0v7swnx5xvi7y")))

(define-public crate-dmntk-feel-number-0.1.2 (c (n "dmntk-feel-number") (v "0.1.2") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.1.2") (d #t) (k 0)))) (h "1p2mvflf15zsz30bpmy4pm0r3hkjrp585zbr2vnc7mlyfpfkqqvq")))

(define-public crate-dmntk-feel-number-0.2.0 (c (n "dmntk-feel-number") (v "0.2.0") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0ccyaxkm52cc1mmc3slhij4dg6hys92c2w7amzw33mk52paxb1vb")))

(define-public crate-dmntk-feel-number-0.2.5 (c (n "dmntk-feel-number") (v "0.2.5") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.2.5") (d #t) (k 0)))) (h "0is6zgcxxdl385i7b4rwxm3z0m6dgg6y885xs3hh1s6f1yv8yw3d")))

(define-public crate-dmntk-feel-number-0.3.0 (c (n "dmntk-feel-number") (v "0.3.0") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.0") (d #t) (k 0)))) (h "12f8ql4nx709j4cbjqlh0f179ivlbswxkibql559w2iy3mmws55s")))

(define-public crate-dmntk-feel-number-0.3.1 (c (n "dmntk-feel-number") (v "0.3.1") (d (list (d (n "dfp-number-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "dmntk-common-1") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1llq4q19n7k0qhr9lmq7mnsgpd6as1z9rv23y47976g480kjp6pb")))

(define-public crate-dmntk-feel-number-0.3.2 (c (n "dmntk-feel-number") (v "0.3.2") (d (list (d (n "dfp-number-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1x25bc8vrrgbdd625542y28wimi4lfq8n16k811jf40zvrq90p6b")))

(define-public crate-dmntk-feel-number-0.3.3 (c (n "dmntk-feel-number") (v "0.3.3") (d (list (d (n "dfp-number-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.3") (d #t) (k 0)))) (h "0rwcrxssgslmn9q7x6zsikk7xy57wlpydrj3zjspawpc8774wn5s")))

(define-public crate-dmntk-feel-number-0.3.4 (c (n "dmntk-feel-number") (v "0.3.4") (d (list (d (n "dfp-number-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.4") (d #t) (k 0)))) (h "1xvqw2dw5q1rd2ag0gizx1qmhldgagywhsxa5951parhj58iikpw")))

(define-public crate-dmntk-feel-number-0.3.5 (c (n "dmntk-feel-number") (v "0.3.5") (d (list (d (n "dfp-number-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-common") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.5") (d #t) (k 0)))) (h "08mfz4bd4xcrrvs9ynl6wqnr1xn1j0kaa3lvhbzgvj9nfdxhrycp")))

