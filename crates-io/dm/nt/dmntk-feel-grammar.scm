(define-module (crates-io dm nt dmntk-feel-grammar) #:use-module (crates-io))

(define-public crate-dmntk-feel-grammar-0.0.1 (c (n "dmntk-feel-grammar") (v "0.0.1") (h "19w3mqwn8nbakz2dp8mmr3j1vsi2hix0blr15pv81sqvpbib6awl")))

(define-public crate-dmntk-feel-grammar-0.0.2 (c (n "dmntk-feel-grammar") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0zq67cz1xnrp3x8dcj7rf1fm5hh5s0fr6b1bn34dp4lfz9p3cn9z")))

(define-public crate-dmntk-feel-grammar-0.0.3 (c (n "dmntk-feel-grammar") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "08q4gpiqm4sl8cn87l36p1dkyn0v0j7wwxfhbw0vqch7rij8i2ly")))

(define-public crate-dmntk-feel-grammar-0.0.5 (c (n "dmntk-feel-grammar") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1rjsqzgc1jm8mhza3r92zxh294dzgz7r1asppv32876g35xn77ld")))

(define-public crate-dmntk-feel-grammar-0.0.6 (c (n "dmntk-feel-grammar") (v "0.0.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1s4xy3ng6wa2z6y8vbnrnkz1yb1ra17r5aci0cvbzsbcix2zrj10")))

(define-public crate-dmntk-feel-grammar-0.0.7 (c (n "dmntk-feel-grammar") (v "0.0.7") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "060z5inf4gp3cdsg36lv4ic6s4s4bbswbdl7yc6g5xp6qh6d7cym")))

(define-public crate-dmntk-feel-grammar-0.0.8 (c (n "dmntk-feel-grammar") (v "0.0.8") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "09s76qwhv69sxqdlqwbib80ij28d46p9ap32f7vancy67crr90z6")))

(define-public crate-dmntk-feel-grammar-0.0.9 (c (n "dmntk-feel-grammar") (v "0.0.9") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1piq8j17q09ljhzcg96606mhx1949yvfyavqqjpan2xxm1l01xpj")))

(define-public crate-dmntk-feel-grammar-0.0.10 (c (n "dmntk-feel-grammar") (v "0.0.10") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0s36zs1vgs95fbicg50x856fk6x32fzircrj3j7h5s9q6hclbfqr")))

(define-public crate-dmntk-feel-grammar-0.0.11 (c (n "dmntk-feel-grammar") (v "0.0.11") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hr1mrv9s75kc02nvp0bcqcva52z3rgy9l0lmy0m2sv79ilyyn5c")))

(define-public crate-dmntk-feel-grammar-0.0.12 (c (n "dmntk-feel-grammar") (v "0.0.12") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "111pibnga0ni1pbqj27lrkycy3fs6dp05sg05l5mkvr1s5d44whp")))

(define-public crate-dmntk-feel-grammar-0.0.13 (c (n "dmntk-feel-grammar") (v "0.0.13") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0xz91v1fbwsxnk0km95kkzwa242nyxgn8saa60vkqx97krda689l") (f (quote (("default") ("bin"))))))

(define-public crate-dmntk-feel-grammar-0.0.14 (c (n "dmntk-feel-grammar") (v "0.0.14") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1zlpx8vx8w1lijgc8j97gb1kcnmbg6hxqnwfsqvlhfxvxy5fvb6b") (f (quote (("default") ("bin"))))))

(define-public crate-dmntk-feel-grammar-0.0.20 (c (n "dmntk-feel-grammar") (v "0.0.20") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0cmnw2xzzpw1xdk8a8h8j18gyjxq22kyc23db0y5d3cr307n30d0")))

(define-public crate-dmntk-feel-grammar-0.0.35 (c (n "dmntk-feel-grammar") (v "0.0.35") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "129dywdzhvbqbpi203agpc72kywmyy912sr0zrx7mfwh14prqgf7")))

(define-public crate-dmntk-feel-grammar-0.0.36 (c (n "dmntk-feel-grammar") (v "0.0.36") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1k0dxm61zrf8v52yrf18cc6fdym9zlrfa8ailvz4452g8gcwicz7")))

(define-public crate-dmntk-feel-grammar-0.0.37 (c (n "dmntk-feel-grammar") (v "0.0.37") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0mx6aadj1w9aggnplh3ncn40cq8mcanjyj4vwq3hcxxlipl908rj")))

(define-public crate-dmntk-feel-grammar-0.0.38 (c (n "dmntk-feel-grammar") (v "0.0.38") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1kzjwniws4337b78wg86h4a4961612h5aa9bx0wrf9lncpihvp6c")))

(define-public crate-dmntk-feel-grammar-0.0.40 (c (n "dmntk-feel-grammar") (v "0.0.40") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0789gis8d7538zwrmsh405y45p0bzrawrpal0zb7snkpr35jzi9f")))

(define-public crate-dmntk-feel-grammar-0.0.41 (c (n "dmntk-feel-grammar") (v "0.0.41") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0cjisg7pa8x1h16h78i66cmk2ixxm96x8zrr4vcjbbgv6r7236ha")))

(define-public crate-dmntk-feel-grammar-0.0.42 (c (n "dmntk-feel-grammar") (v "0.0.42") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "17clm1vziwnpw299d7f4pk1yb4hkvdfy00jb64srrjdk7lrhzg3h")))

(define-public crate-dmntk-feel-grammar-0.0.43 (c (n "dmntk-feel-grammar") (v "0.0.43") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1ajh8qqmqvrjl8i2n2r703ry21d8g4i9hrgk554030v08fkjj0ci")))

(define-public crate-dmntk-feel-grammar-0.0.44 (c (n "dmntk-feel-grammar") (v "0.0.44") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "16kyfjnwslgw8fgnjym5xfv3lzzcln5cx0d3pkps8dd4hp6cyfrc")))

(define-public crate-dmntk-feel-grammar-0.0.45 (c (n "dmntk-feel-grammar") (v "0.0.45") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vj8wl43m4ws9raxlmqmnx7mcsn1q0l26k0q7xql99k2bigd1l8b")))

(define-public crate-dmntk-feel-grammar-0.0.46 (c (n "dmntk-feel-grammar") (v "0.0.46") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hnk5zn738nvkhxb283p57lpqy3bp27c9afvv4rvafigp4ns4k24")))

(define-public crate-dmntk-feel-grammar-0.0.47 (c (n "dmntk-feel-grammar") (v "0.0.47") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0707w078sazhw09pbpm2b7jnfm2xd4f17rxmk3vy8glxg2gw4v9r")))

(define-public crate-dmntk-feel-grammar-0.0.48 (c (n "dmntk-feel-grammar") (v "0.0.48") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0aah6xgz5n106398d5wi668bw25a14zgw41c9pcqcb9gfgjw5n11")))

(define-public crate-dmntk-feel-grammar-0.0.49 (c (n "dmntk-feel-grammar") (v "0.0.49") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0lv9r50499nsssy638drc1mdc9jlzpbqwbrvjaiwrvp4iwwbxmcl")))

(define-public crate-dmntk-feel-grammar-0.0.50 (c (n "dmntk-feel-grammar") (v "0.0.50") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0p8m0sjrfjfaf0viww5ha1806dfp2av11gg1irxpqb18z51l5sq7")))

(define-public crate-dmntk-feel-grammar-0.0.52 (c (n "dmntk-feel-grammar") (v "0.0.52") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1gnbd5af4q0pjzijzglc56vybq722qi8sk5jmzwkbapc91mnkc6n")))

(define-public crate-dmntk-feel-grammar-0.0.53 (c (n "dmntk-feel-grammar") (v "0.0.53") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0qhh6xkv1sjskzvzd3v7jwak7q6mk5i09fgj4hivwdrfs9dqv2hn")))

(define-public crate-dmntk-feel-grammar-0.0.54 (c (n "dmntk-feel-grammar") (v "0.0.54") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1iby2cym6y64g6lwzgfij7py27dk6fy1s6gd96m1aj8gy575j3kg")))

(define-public crate-dmntk-feel-grammar-0.1.0 (c (n "dmntk-feel-grammar") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0hd5pgqhyg9v5h69ak2g4m4x8kdz829vb8l481jwr6pw61va0ib9")))

(define-public crate-dmntk-feel-grammar-0.1.1 (c (n "dmntk-feel-grammar") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1lp0qhpkynqlrbcg6x437m0yhl7sl257gx4igabmkqvy0102zirp")))

(define-public crate-dmntk-feel-grammar-0.1.2 (c (n "dmntk-feel-grammar") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1cj4qy984s0ghsmc5y6bcig1hkdic3xccm30dbrskknn9nk3p2jf")))

(define-public crate-dmntk-feel-grammar-0.2.0 (c (n "dmntk-feel-grammar") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "18n3hdkw6hl88bdpb4kbgk7gqc9v4wz2gr8djvjaq37spknmigc1")))

(define-public crate-dmntk-feel-grammar-0.2.5 (c (n "dmntk-feel-grammar") (v "0.2.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0zv0bfsr4r8i4gkdgqk63svykgkg8nqhzhxfhglh8lgdcbdxln2n")))

(define-public crate-dmntk-feel-grammar-0.3.0 (c (n "dmntk-feel-grammar") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "16jgsbnj0k0qax2yl3g5qmf4dz9cxs4ainxwzmplspjkvn6g5pyw")))

(define-public crate-dmntk-feel-grammar-0.3.1 (c (n "dmntk-feel-grammar") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0ggx4m5lvynm3c8vxm47hpllb1fdjjhrrxc4y1ppdkjnbq59343z")))

(define-public crate-dmntk-feel-grammar-0.3.2 (c (n "dmntk-feel-grammar") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "16xp2c338ykflq1mz8h52km3qh03nspjj658kikvac145ad6h38y")))

(define-public crate-dmntk-feel-grammar-0.3.3 (c (n "dmntk-feel-grammar") (v "0.3.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1mbjgzwpnslds88q4a0ny938kc0y3v9rq67zii9j1dpvfvaj5d6d")))

(define-public crate-dmntk-feel-grammar-0.3.4 (c (n "dmntk-feel-grammar") (v "0.3.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "12is5vpnhnkcnasn0sz997zmz9nhgxqq85ky122pcpy2mbxfxric")))

(define-public crate-dmntk-feel-grammar-0.3.5 (c (n "dmntk-feel-grammar") (v "0.3.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1vn07s1caj25xf32bbzg4pi65x0hlph7x2az2h17mc7wn777m97a")))

