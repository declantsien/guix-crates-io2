(define-module (crates-io dm nt dmntk-evaluator) #:use-module (crates-io))

(define-public crate-dmntk-evaluator-0.0.6 (c (n "dmntk-evaluator") (v "0.0.6") (d (list (d (n "dmntk-common") (r "^0.0.6") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.6") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pdciv2gqamyi11ps94cwfv85y2mnr3gn638gh1bcwmp3i85vkkc")))

(define-public crate-dmntk-evaluator-0.0.7 (c (n "dmntk-evaluator") (v "0.0.7") (d (list (d (n "dmntk-common") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hs3rv5z0lj34x5ywss8ykmgbwp1p54hxvqyll32n5cidglxlklp")))

(define-public crate-dmntk-evaluator-0.0.10 (c (n "dmntk-evaluator") (v "0.0.10") (d (list (d (n "dmntk-examples") (r "^0.0.10") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.10") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.10") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hz866v5fwi3ldhakgynzpdbwnxx8pfsbki7kp2hsnv475dyzimr")))

(define-public crate-dmntk-evaluator-0.0.11 (c (n "dmntk-evaluator") (v "0.0.11") (d (list (d (n "dmntk-recognizer") (r "^0.0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10yhwf48p6b8d2jhmkppwjhs3vqd5ckdy4dfgy8nn48lpr6dfy5f")))

(define-public crate-dmntk-evaluator-0.0.20 (c (n "dmntk-evaluator") (v "0.0.20") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.20") (d #t) (k 0)))) (h "0y0pyqf4khmbvl3a48n4ylmhx1i6f8jwkinqrcb2s3ly4d4wmmvf")))

(define-public crate-dmntk-evaluator-0.0.35 (c (n "dmntk-evaluator") (v "0.0.35") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.35") (d #t) (k 0)))) (h "0m939kmn1yfjxyqh7zrkq427cyn73n41jsqivsx19rjpnr834kcb")))

(define-public crate-dmntk-evaluator-0.0.36 (c (n "dmntk-evaluator") (v "0.0.36") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.36") (d #t) (k 0)))) (h "0ndzrxkhc7mdajc4l8q6qcnjlj896r53n3idy9hiqnm28rm4519l")))

(define-public crate-dmntk-evaluator-0.0.37 (c (n "dmntk-evaluator") (v "0.0.37") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.37") (d #t) (k 0)))) (h "13bvd049l7ll08x8z96rjkjf4933f5ajjkibihv91kq5mfyf7in9")))

(define-public crate-dmntk-evaluator-0.0.38 (c (n "dmntk-evaluator") (v "0.0.38") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.38") (d #t) (k 0)))) (h "1f0wscj7zlfis1kcdfyxlig4ddk7zifjjqzkbmmsk9vqa2f6zprn")))

(define-public crate-dmntk-evaluator-0.0.40 (c (n "dmntk-evaluator") (v "0.0.40") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.40") (d #t) (k 0)))) (h "08mwd2lry4f0qbhqq8qw1yfwxhyw2m5d2j0md0frqzpdykxcqdpk")))

(define-public crate-dmntk-evaluator-0.0.41 (c (n "dmntk-evaluator") (v "0.0.41") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.41") (d #t) (k 0)))) (h "16wv226zq7i28m3cqk8say2ai4wgxqjpm4fi8y6jb8ylp5gn789x")))

(define-public crate-dmntk-evaluator-0.0.42 (c (n "dmntk-evaluator") (v "0.0.42") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.42") (d #t) (k 0)))) (h "1fwaizkijnjmj4399ky8mvgpy3wnh32l3y5w90ycbb6lpyqhjilv")))

(define-public crate-dmntk-evaluator-0.0.43 (c (n "dmntk-evaluator") (v "0.0.43") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.43") (d #t) (k 0)))) (h "05sm6fcqs9bmfc203jzgkwn83jr1ghgsqw1qhqbch2d625g1pkqg")))

(define-public crate-dmntk-evaluator-0.0.44 (c (n "dmntk-evaluator") (v "0.0.44") (d (list (d (n "dmntk-feel-evaluator") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.44") (d #t) (k 0)))) (h "1azk91wlziv2djgbp7x8g9nc2vbkh7nxr9mwj2wdrv91rjyq9sx2")))

(define-public crate-dmntk-evaluator-0.0.45 (c (n "dmntk-evaluator") (v "0.0.45") (d (list (d (n "dmntk-common") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.45") (d #t) (k 0)))) (h "15pmsbwq43irjaxxr2y6y53rnjp06ffnpkwbzgmgnvc89vxahfd0")))

(define-public crate-dmntk-evaluator-0.0.46 (c (n "dmntk-evaluator") (v "0.0.46") (d (list (d (n "dmntk-common") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.46") (d #t) (k 0)))) (h "1jihc3mhiv923xd6x7r2vq6pi5r9d4dg8801xljy5xml60a6r3ll")))

(define-public crate-dmntk-evaluator-0.0.47 (c (n "dmntk-evaluator") (v "0.0.47") (d (list (d (n "dmntk-common") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "=0.0.47") (d #t) (k 0)))) (h "1z624fkmx6d3a91kwkm85i3b4930rr8nzsdlvqnf1s6rd774x0j6")))

(define-public crate-dmntk-evaluator-0.0.48 (c (n "dmntk-evaluator") (v "0.0.48") (d (list (d (n "dmntk-common") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "=0.0.48") (d #t) (k 0)))) (h "1wrny2wsgjga6pm8919qf4c7j4rmgpg1vd2k4ar340abhg8jbipm")))

(define-public crate-dmntk-evaluator-0.0.50 (c (n "dmntk-evaluator") (v "0.0.50") (d (list (d (n "dmntk-common") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "=0.0.50") (d #t) (k 0)))) (h "1jlc6jdn07j8k99afr17a2jmmy1g6naaf4wxs1xa2ky75h5ih9yz")))

(define-public crate-dmntk-evaluator-0.0.52 (c (n "dmntk-evaluator") (v "0.0.52") (d (list (d (n "dmntk-common") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.52") (d #t) (k 0)))) (h "1blsgf5748vj9wygplbqf55xlbx3kq1pn91641083xknlshibr8m")))

(define-public crate-dmntk-evaluator-0.0.53 (c (n "dmntk-evaluator") (v "0.0.53") (d (list (d (n "dmntk-common") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.53") (d #t) (k 0)))) (h "1i07py1qvnnvdbjlc1283v3lm5jm1vn02qz165hcjljcjk8m27l0")))

(define-public crate-dmntk-evaluator-0.0.54 (c (n "dmntk-evaluator") (v "0.0.54") (d (list (d (n "dmntk-common") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.0.54") (d #t) (k 0)))) (h "1xb8bgars1lpwfw15215fgbhfjq432d8hgmf4srw0cmmy6ip81j5")))

(define-public crate-dmntk-evaluator-0.1.0 (c (n "dmntk-evaluator") (v "0.1.0") (d (list (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.1.0") (d #t) (k 0)))) (h "015lwcsf78awqf1r8yx4bbxxm3ysjidmx4scdj9ii89vkswdh8g8")))

(define-public crate-dmntk-evaluator-0.1.1 (c (n "dmntk-evaluator") (v "0.1.1") (d (list (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.1.0") (d #t) (k 0)))) (h "0h70jcim9dvfb214akvs3l76py1f0kd68nas6f0y6w3r9zx26pfj")))

(define-public crate-dmntk-evaluator-0.1.2 (c (n "dmntk-evaluator") (v "0.1.2") (d (list (d (n "dmntk-common") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.1.2") (d #t) (k 0)))) (h "172b99xbmgqf499vbmghrgwx40bi6pv59c1m3qhnf1zh825448s6")))

(define-public crate-dmntk-evaluator-0.2.0 (c (n "dmntk-evaluator") (v "0.2.0") (d (list (d (n "dmntk-common") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.2.0") (d #t) (k 0)))) (h "01j3xcyzd7cw210vpkwm2jkxksyajibnki18hb4shw57kb9pj9pz")))

(define-public crate-dmntk-evaluator-0.2.5 (c (n "dmntk-evaluator") (v "0.2.5") (d (list (d (n "dmntk-common") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.2.5") (d #t) (k 0)))) (h "0ph5dw33hfwm9xi22vjvaclv11i8iq9lnb542lnzgbs7a4l1kg60")))

(define-public crate-dmntk-evaluator-0.3.0 (c (n "dmntk-evaluator") (v "0.3.0") (d (list (d (n "dmntk-common") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.0") (d #t) (k 0)))) (h "1m341q9fbdyln2663cpd6cfqg2rb9yfxqyxq38ddqpinfsmcfr70")))

(define-public crate-dmntk-evaluator-0.3.1 (c (n "dmntk-evaluator") (v "0.3.1") (d (list (d (n "dmntk-common-1") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.1") (d #t) (k 0)))) (h "1qj6asblkfdwdgrpvgcbq2vl5zr9gprg2m3mdyi261y1mi8hdn8f")))

(define-public crate-dmntk-evaluator-0.3.2 (c (n "dmntk-evaluator") (v "0.3.2") (d (list (d (n "dmntk-common") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.2") (d #t) (k 0)))) (h "0kl6bpmhk3ak4haf7fldmcj2hl5igd13f1an532bdhaz145m9j35")))

(define-public crate-dmntk-evaluator-0.3.3 (c (n "dmntk-evaluator") (v "0.3.3") (d (list (d (n "dmntk-common") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.3") (d #t) (k 0)))) (h "1in7g7nayggsbc815954bcpr3a6h8sbrl952c985c7yph55skiqa")))

(define-public crate-dmntk-evaluator-0.3.4 (c (n "dmntk-evaluator") (v "0.3.4") (d (list (d (n "dmntk-common") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.4") (d #t) (k 0)))) (h "02li9n3n3qsbbvkf6gn9mb0kqgj7vb4wmhx7jn7nn8lqzyxh6z60")))

(define-public crate-dmntk-evaluator-0.3.5 (c (n "dmntk-evaluator") (v "0.3.5") (d (list (d (n "dmntk-common") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel-evaluator") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-model-evaluator") (r "^0.3.5") (d #t) (k 0)))) (h "1n4hbkrr0g7qxym25z2fg6iv9jgwaacw3sy2v549kfj47v63g7ly")))

