(define-module (crates-io dm nt dmntk-common-1) #:use-module (crates-io))

(define-public crate-dmntk-common-1-0.3.1 (c (n "dmntk-common-1") (v "0.3.1") (d (list (d (n "dmntk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z26kpgjg7llwsfnnfb5qf0kc2p8lzcp668xnjzcg8yyl2ni5nk9") (y #t)))

