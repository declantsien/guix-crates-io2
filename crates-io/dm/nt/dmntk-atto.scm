(define-module (crates-io dm nt dmntk-atto) #:use-module (crates-io))

(define-public crate-dmntk-atto-0.0.1 (c (n "dmntk-atto") (v "0.0.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "0h6940dxwl05mfk0wvvrvj0kvard85r648b0c4fmzaw9csqr74bx")))

(define-public crate-dmntk-atto-0.0.2 (c (n "dmntk-atto") (v "0.0.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "1qavacqq1hvj0khyxpa1cs6c0r102wcls6s91ch14f0smlm2il33")))

(define-public crate-dmntk-atto-0.0.3 (c (n "dmntk-atto") (v "0.0.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "0wwkp4qarybkssj3s6cz26cxydj13qrj4l3sa9haadkvvz63ir01")))

(define-public crate-dmntk-atto-0.0.4 (c (n "dmntk-atto") (v "0.0.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "0gvppg0ihg7vhnrydi2dyrskxl7kcjqzrzkz8f7vra8jj1bhjv1c")))

(define-public crate-dmntk-atto-0.0.6 (c (n "dmntk-atto") (v "0.0.6") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "1xc62l1b9c01s00drkd6fa88ky57iazjsrzms4jcdzrz2s3zwnrr")))

(define-public crate-dmntk-atto-0.0.7 (c (n "dmntk-atto") (v "0.0.7") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "1783ck7wv3h5slfy47y2nw57dizk7j6vpb57hfvyxyrxzjlfqxzf")))

(define-public crate-dmntk-atto-0.0.8 (c (n "dmntk-atto") (v "0.0.8") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "03akl9c2v6lb6w52pcycmya0yq3vlb01dsmd3x7qwkjs79qa84g5")))

(define-public crate-dmntk-atto-0.0.9 (c (n "dmntk-atto") (v "0.0.9") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^5.101.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "1wba3yladi1v3yvq4k8kkz8p2wdq2v74awmlsiq1g0g7drrd7a5j")))

(define-public crate-dmntk-atto-0.3.5 (c (n "dmntk-atto") (v "0.3.5") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "ncurses") (r "^6.0.0") (f (quote ("wide" "extended_colors"))) (d #t) (k 0)))) (h "1898sawyhdqvrx3xypdbp13lgi22c40cqg967kyv013rgsj0idfq")))

