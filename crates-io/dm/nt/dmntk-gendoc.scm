(define-module (crates-io dm nt dmntk-gendoc) #:use-module (crates-io))

(define-public crate-dmntk-gendoc-0.0.20 (c (n "dmntk-gendoc") (v "0.0.20") (d (list (d (n "dmntk-examples") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.20") (d #t) (k 0)))) (h "16jj7ynzp7vs7rw27123vagcsmvvg1pxr5d5152y16rmvdcdjqyg")))

(define-public crate-dmntk-gendoc-0.0.35 (c (n "dmntk-gendoc") (v "0.0.35") (d (list (d (n "dmntk-examples") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.35") (d #t) (k 0)))) (h "1mnx69fyvpxlc363n82anmblm4x143xj2qaa07vpd0y064syilg8")))

(define-public crate-dmntk-gendoc-0.0.36 (c (n "dmntk-gendoc") (v "0.0.36") (d (list (d (n "dmntk-examples") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.36") (d #t) (k 0)))) (h "0msiza9plw3la3iswx3n4dn1hig4ysi6290sgdappnf4wpjscvd8")))

(define-public crate-dmntk-gendoc-0.0.37 (c (n "dmntk-gendoc") (v "0.0.37") (d (list (d (n "dmntk-examples") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.37") (d #t) (k 0)))) (h "0liihiprzfkrad60clilpbxmsn5xxn0drrxm7msx9x113jg3zi3v")))

(define-public crate-dmntk-gendoc-0.0.38 (c (n "dmntk-gendoc") (v "0.0.38") (d (list (d (n "dmntk-examples") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.38") (d #t) (k 0)))) (h "1m46gih06zwnwvpwp2c2ha25r09p2a7wbj0zrgh5c372wxwdgn8r")))

(define-public crate-dmntk-gendoc-0.0.40 (c (n "dmntk-gendoc") (v "0.0.40") (d (list (d (n "dmntk-examples") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.40") (d #t) (k 0)))) (h "1xxwj2ds0bcxjspmf7f86m4lz8yyynim7hgqkdawg9n70n6ryjpr")))

(define-public crate-dmntk-gendoc-0.0.41 (c (n "dmntk-gendoc") (v "0.0.41") (d (list (d (n "dmntk-examples") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.41") (d #t) (k 0)))) (h "0fsnz33l6klq29kzz7fy2gy3l8jnm85ql4a0jazbpyzxjj42scz5")))

(define-public crate-dmntk-gendoc-0.0.42 (c (n "dmntk-gendoc") (v "0.0.42") (d (list (d (n "dmntk-examples") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.42") (d #t) (k 0)))) (h "1g6v3p3jlpanq8xad04klszjxjs8ibvgxvqhqhcj3mh9v6mihnpr")))

(define-public crate-dmntk-gendoc-0.0.43 (c (n "dmntk-gendoc") (v "0.0.43") (d (list (d (n "dmntk-examples") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.43") (d #t) (k 0)))) (h "02af0p1nl6qhqzzx958brbipl0j47ds0vbhf1fsgcphpz0vkwz9k")))

(define-public crate-dmntk-gendoc-0.0.44 (c (n "dmntk-gendoc") (v "0.0.44") (d (list (d (n "dmntk-examples") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.44") (d #t) (k 0)))) (h "1nrzf97dgrq9vzdxql3qr3ngn9g9y0l8p2l8x5ascm0b5madz7ad")))

(define-public crate-dmntk-gendoc-0.0.45 (c (n "dmntk-gendoc") (v "0.0.45") (d (list (d (n "dmntk-examples") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.45") (d #t) (k 0)))) (h "0kqpy0c8ssncc3277pkr2k60n1y2j157brkxrjiq4ph94j0jbygv")))

(define-public crate-dmntk-gendoc-0.0.46 (c (n "dmntk-gendoc") (v "0.0.46") (d (list (d (n "dmntk-examples") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.46") (d #t) (k 0)))) (h "1vikr570ps2a9hw36himzjsbfwvn6d9yf0a56yrvrlkkfh50vq5j")))

(define-public crate-dmntk-gendoc-0.0.47 (c (n "dmntk-gendoc") (v "0.0.47") (d (list (d (n "dmntk-examples") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-model") (r "=0.0.47") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "=0.0.47") (d #t) (k 2)))) (h "0n96s58qd1xx44wrbbggm6fisrdilk2hkjdcxgalkivg0r2vs6zd")))

(define-public crate-dmntk-gendoc-0.0.48 (c (n "dmntk-gendoc") (v "0.0.48") (d (list (d (n "dmntk-examples") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-model") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "=0.0.48") (d #t) (k 2)))) (h "0im1icsmj3lqwkba5sal0ilkfhy6bcibmgicjw2j2lrgf46rhz4m")))

(define-public crate-dmntk-gendoc-0.0.50 (c (n "dmntk-gendoc") (v "0.0.50") (d (list (d (n "dmntk-examples") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-model") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "=0.0.50") (d #t) (k 2)))) (h "1zg5w31ad7q43qjs2fj73ww31dg4ay95np8g1nz6bbqg12z3z4l6")))

(define-public crate-dmntk-gendoc-0.0.52 (c (n "dmntk-gendoc") (v "0.0.52") (d (list (d (n "dmntk-examples") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.0.52") (d #t) (k 2)))) (h "0i7lyw7mfp9bvkr7va67zs6p8a2zlwicswf005gh6a2i0l9nc20d")))

(define-public crate-dmntk-gendoc-0.0.53 (c (n "dmntk-gendoc") (v "0.0.53") (d (list (d (n "dmntk-examples") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.0.53") (d #t) (k 2)))) (h "1wj4xn96pnabn2w6lki3aws4ry7hs4vmagw2x7i5fwp6ill67k1w")))

(define-public crate-dmntk-gendoc-0.0.54 (c (n "dmntk-gendoc") (v "0.0.54") (d (list (d (n "dmntk-examples") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.0.54") (d #t) (k 2)))) (h "0prnd98sqml5qclpkb97nnkdqzx8j8nynyyid49f2x2z1lrh2nj5")))

(define-public crate-dmntk-gendoc-0.1.0 (c (n "dmntk-gendoc") (v "0.1.0") (d (list (d (n "dmntk-examples") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "12z56chs48gxnz1n6bz17r760nps2bm3ra06v1c8gyg7v9lhmwpp")))

(define-public crate-dmntk-gendoc-0.1.1 (c (n "dmntk-gendoc") (v "0.1.1") (d (list (d (n "dmntk-examples") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "14zv12lyxrmc99xm5y629viksbm6a8c2wlplg8ii2n2s7y7bq5nj")))

(define-public crate-dmntk-gendoc-0.1.2 (c (n "dmntk-gendoc") (v "0.1.2") (d (list (d (n "dmntk-examples") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.1.2") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "157zzmin7khlvd5ymmp1146bmh624bgr6m2vkas6yxpbs1ah9czd")))

(define-public crate-dmntk-gendoc-0.2.0 (c (n "dmntk-gendoc") (v "0.2.0") (d (list (d (n "dmntk-common") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.2.0") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "0jgnn9vm3hx6qjd34czfvhpbg72fkj0g4pf0285jgd5hcyskv0iz")))

(define-public crate-dmntk-gendoc-0.2.5 (c (n "dmntk-gendoc") (v "0.2.5") (d (list (d (n "dmntk-common") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.2.5") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "1lmcjng726m7ihr40viif3b6jgi6ifszx2h31gh9anksq7glp3lz")))

(define-public crate-dmntk-gendoc-0.3.0 (c (n "dmntk-gendoc") (v "0.3.0") (d (list (d (n "dmntk-common") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.0") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "1164bjw03xiplc2gh5y6fkbxmkrzfkgv0hpwb6xg66l4l4bg32np")))

(define-public crate-dmntk-gendoc-0.3.1 (c (n "dmntk-gendoc") (v "0.3.1") (d (list (d (n "dmntk-common-1") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.1") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "16mafiqh37rhnsxjnbzcp8i49vsw2xkfd112iw5ax22ii9kcss33")))

(define-public crate-dmntk-gendoc-0.3.2 (c (n "dmntk-gendoc") (v "0.3.2") (d (list (d (n "dmntk-common") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.2") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "15rp0kwckbnva6frbj06r5sf1dmqjrgjfx5ds97j0qxk03smc8x5")))

(define-public crate-dmntk-gendoc-0.3.3 (c (n "dmntk-gendoc") (v "0.3.3") (d (list (d (n "dmntk-common") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.3") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0ckk228j41zddwndlkj9drs299z1f5jgj44yl0y6b2704bbm68zp")))

(define-public crate-dmntk-gendoc-0.3.4 (c (n "dmntk-gendoc") (v "0.3.4") (d (list (d (n "dmntk-common") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.4") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0r9h9pn3glyn0z2l1vswc4jfsvk37nj9l45vxv3kjffp2590rn90")))

(define-public crate-dmntk-gendoc-0.3.5 (c (n "dmntk-gendoc") (v "0.3.5") (d (list (d (n "dmntk-common") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-model") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-recognizer") (r "^0.3.5") (d #t) (k 2)) (d (n "domrs") (r "^0.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "15n48zyd2f2qnls0l6pss7qcr3sg6jnmmskmvkxrbrg2qjkranz1")))

