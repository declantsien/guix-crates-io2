(define-module (crates-io dm nt dmntk-common) #:use-module (crates-io))

(define-public crate-dmntk-common-0.0.1 (c (n "dmntk-common") (v "0.0.1") (h "0fsqkwin6gqspfhcmi3x4v2f964y996v6kg94rwr2wga0z44nav4")))

(define-public crate-dmntk-common-0.0.5 (c (n "dmntk-common") (v "0.0.5") (h "17vny19zmc4hnw3hc199ic90mbmrr3snikca04mzg0mls5zfhf21")))

(define-public crate-dmntk-common-0.0.6 (c (n "dmntk-common") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0nr80yhmrf33wly3mg7ridpdj2jkn2ymhw6rm6c8vlbr35iz2wb7")))

(define-public crate-dmntk-common-0.0.7 (c (n "dmntk-common") (v "0.0.7") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1786l0wi957mh4mcvi6z57vxd52l3j74hwh8prfzy871kqd9bhiw")))

(define-public crate-dmntk-common-0.0.9 (c (n "dmntk-common") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1d6gy02yzg6jwsjywjrwswykh6889z0f7905qgrpifj1v2yf5vwd")))

(define-public crate-dmntk-common-0.0.10 (c (n "dmntk-common") (v "0.0.10") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1h976xdfmhaj50dz37dai2lgpz8aqcz3b9fkg13fbcs3symczyls")))

(define-public crate-dmntk-common-0.0.11 (c (n "dmntk-common") (v "0.0.11") (d (list (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "03gd92fxif7ykf13ib6q3pk3rsgg6l5ngmh7r0nvvgqmw07pjxak")))

(define-public crate-dmntk-common-0.0.20 (c (n "dmntk-common") (v "0.0.20") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1k9mngs49xs1072b1zc8yi22x4rryl91qr59cvcsjpnqcy49fkr3")))

(define-public crate-dmntk-common-0.0.25 (c (n "dmntk-common") (v "0.0.25") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1vhr08flnnzgbhjg73pllzvlk30ck0rjpmc91992xslidk44v0f2")))

(define-public crate-dmntk-common-0.0.26 (c (n "dmntk-common") (v "0.0.26") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "17sl01g4q2p1s15z64zqz0kgzznqksjvwmh2qsf99zg5wxv0r3dz")))

(define-public crate-dmntk-common-0.0.35 (c (n "dmntk-common") (v "0.0.35") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "0rzxi2jkn73j2ikjfpsnllvqrpw3iy3qqfbpbld3x8i0508azxk8")))

(define-public crate-dmntk-common-0.0.36 (c (n "dmntk-common") (v "0.0.36") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "13pbyxg1xj85mv3kydsmyklj2bdw5wzfa5w6qi002vddjyjg3976")))

(define-public crate-dmntk-common-0.0.37 (c (n "dmntk-common") (v "0.0.37") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1kyrmpva8hl0wayld6pcgifyg7nrm7d9ifp7zp9rhq89sxvhm2c4")))

(define-public crate-dmntk-common-0.0.38 (c (n "dmntk-common") (v "0.0.38") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "09jr4ypjvm5d3d6ssbbca1kaqq9ik2iv0xf5dsx41lya6nqfirl5")))

(define-public crate-dmntk-common-0.0.39 (c (n "dmntk-common") (v "0.0.39") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "12dp26gc9pyvd2sjl05ax9j2xqqrmv9zdp6rf2hjw0k7x8m0d5qj")))

(define-public crate-dmntk-common-0.0.40 (c (n "dmntk-common") (v "0.0.40") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1fxrsp4z2rsw9kzyrs54cy9iafy5kaxmbpb1yxn9cbq53ykn5wvs")))

(define-public crate-dmntk-common-0.0.41 (c (n "dmntk-common") (v "0.0.41") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1y9x9q1mclfvhacdz01nn9m6mfqsnp92cswy3kg31q3wvq5l9n6l")))

(define-public crate-dmntk-common-0.0.42 (c (n "dmntk-common") (v "0.0.42") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "0z90jnaa6g01zmdx62vpy8zyvivp398832mxys9rfsyriaiqqizv")))

(define-public crate-dmntk-common-0.0.43 (c (n "dmntk-common") (v "0.0.43") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "0f5nv4q2jqmlq3vs7qmbphmysyl362bywv9ydsclmfaw4qxlry24")))

(define-public crate-dmntk-common-0.0.44 (c (n "dmntk-common") (v "0.0.44") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "1adllax57ycqzdpy07dmq56pv4y6m48mzp3782jpi9mqjwkc6d3g")))

(define-public crate-dmntk-common-0.0.45 (c (n "dmntk-common") (v "0.0.45") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "0bhhhzm07663dq75hwnm833amrxvaqghg38avx237jvjpdlz1cvf")))

(define-public crate-dmntk-common-0.0.46 (c (n "dmntk-common") (v "0.0.46") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.3") (d #t) (k 0)))) (h "0dsjs18q7rq219qm0j37nssqfwa1nsiyklw952h7dlvi7c90l905")))

(define-public crate-dmntk-common-0.0.47 (c (n "dmntk-common") (v "0.0.47") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "09spjdq4mdh3vcxnj0l77acilfqw2xm3mdqafa2k08s74g1j751p")))

(define-public crate-dmntk-common-0.0.48 (c (n "dmntk-common") (v "0.0.48") (d (list (d (n "serde") (r "=1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "0hcz7hxkbsyh9h77w7d91ih9lbaj4l3khilf8wc0bawpxi89jd7l")))

(define-public crate-dmntk-common-0.0.49 (c (n "dmntk-common") (v "0.0.49") (d (list (d (n "serde") (r "=1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "0dldbw9hj78kavhb640qybsq7lkcxz3lkqd2pvfv24g9j6ngh8n0")))

(define-public crate-dmntk-common-0.0.50 (c (n "dmntk-common") (v "0.0.50") (d (list (d (n "serde") (r "=1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "1qa6ac23msrhyz8hpg8fnf8w0zdvwvgrsa7rjp1jzk4rya0i5iyj")))

(define-public crate-dmntk-common-0.0.52 (c (n "dmntk-common") (v "0.0.52") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "0il5snv4l0bmq02bsdchvccqkpklafp2wbvyhspyn35hlrd5za6q")))

(define-public crate-dmntk-common-0.0.53 (c (n "dmntk-common") (v "0.0.53") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "0m2jymgrymp6v236xy4yajynvfww19icalhc5h4ylzq3ks6z3va3")))

(define-public crate-dmntk-common-0.0.54 (c (n "dmntk-common") (v "0.0.54") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)))) (h "175qvgb1ba6dy9cycy9cidyfqpxkkb5zvk40z7wl96446gypq06m")))

(define-public crate-dmntk-common-0.1.0 (c (n "dmntk-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.150") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nsvjm0s5fkndqy6yprdac68da194z9mrh6m9q3mc4whz8iaby3s")))

(define-public crate-dmntk-common-0.1.1 (c (n "dmntk-common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0468v5wg498xbp3krgpvjwfklggcjg72vwfvmkgl03sn6yal6k27")))

(define-public crate-dmntk-common-0.1.2 (c (n "dmntk-common") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jywbbw0yqfjibhsdsmzpqj36k2kc5b6ckzq70280y0lw8r9rlan")))

(define-public crate-dmntk-common-0.2.0 (c (n "dmntk-common") (v "0.2.0") (d (list (d (n "dmntk-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nq3hi8i5zhxv5vfxrp05540l4w6qa7vx519aybb5j0w5zfgwgxw")))

(define-public crate-dmntk-common-0.2.5 (c (n "dmntk-common") (v "0.2.5") (d (list (d (n "dmntk-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lgdzg7wd3xa18vl2f4miilpvskvgx2sr4kynj0fs1war9m5xi2k")))

(define-public crate-dmntk-common-0.3.0 (c (n "dmntk-common") (v "0.3.0") (d (list (d (n "dmntk-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dm3wq3a2nhcpld6nqfff2kq37zv45fgy3lh4mafapj8hn6xf0f1")))

(define-public crate-dmntk-common-0.3.1 (c (n "dmntk-common") (v "0.3.1") (d (list (d (n "dmntk-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "15h1c3vbzsadv416xxnnwfp1jh6jlhfx9kp54ii0s6d3abx1dn1k")))

(define-public crate-dmntk-common-0.3.2 (c (n "dmntk-common") (v "0.3.2") (d (list (d (n "dmntk-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dm5frdnz29migfbj2i8gpi3pmm87zdxm0ima0r1iyw1wl1rvvdv")))

(define-public crate-dmntk-common-0.3.3 (c (n "dmntk-common") (v "0.3.3") (d (list (d (n "dmntk-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ksi74hq143p17bfcgjz6mr52337994sv4i16hw0krqq0xfnz756")))

(define-public crate-dmntk-common-0.3.4 (c (n "dmntk-common") (v "0.3.4") (d (list (d (n "dmntk-macros") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g16r14ns526wv8b7bpicz3ksivvcmbz6r4p3yikff3nlwk402am")))

(define-public crate-dmntk-common-0.3.5 (c (n "dmntk-common") (v "0.3.5") (d (list (d (n "dmntk-macros") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "11k0cwazql2ccfq0sd50i627j1j9j7srfha76nx9yhsqi3n9fvgn")))

