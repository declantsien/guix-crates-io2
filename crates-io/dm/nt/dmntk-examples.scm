(define-module (crates-io dm nt dmntk-examples) #:use-module (crates-io))

(define-public crate-dmntk-examples-0.0.6 (c (n "dmntk-examples") (v "0.0.6") (h "0i99ymrq85bvq80qqknxnygzkbc44c4wv188h6ac5pg9xx83jj87")))

(define-public crate-dmntk-examples-0.0.7 (c (n "dmntk-examples") (v "0.0.7") (h "0ni4g423s3n2878nwa0sw7r6qm33v620yln307gffdf59hr0cprq")))

(define-public crate-dmntk-examples-0.0.9 (c (n "dmntk-examples") (v "0.0.9") (h "04cgzi7qyr7l2gya06rlwz2z8xkp1h6ix2xxwr87gx24j67inwdy")))

(define-public crate-dmntk-examples-0.0.10 (c (n "dmntk-examples") (v "0.0.10") (h "14hmgbz367918865518ivva9y7bjwdxv65r0hqj5ygk6g9jbigj7")))

(define-public crate-dmntk-examples-0.0.11 (c (n "dmntk-examples") (v "0.0.11") (h "0gii61132d3qz7d3n3ksgrnp8ykkx35r24n0a52wmv06dv7mw235")))

(define-public crate-dmntk-examples-0.0.20 (c (n "dmntk-examples") (v "0.0.20") (h "1h8c0rslxzzw7wfyn1k8qdlq0lxjk77c3a7pk5824yv9556m58rq")))

(define-public crate-dmntk-examples-0.0.35 (c (n "dmntk-examples") (v "0.0.35") (h "1zvhs82h6lcbyhnirz9i7rc4a9rjz5jaql1yx9lmgmjkf2hyby5h")))

(define-public crate-dmntk-examples-0.0.36 (c (n "dmntk-examples") (v "0.0.36") (h "06fqwgpwd6gr6d5kpi9fvknmzxklgijfg46kj04867kvwnlzlqzs")))

(define-public crate-dmntk-examples-0.0.37 (c (n "dmntk-examples") (v "0.0.37") (h "1fdrlq4vqip5q5isya5pads9nff3h761v5lxqjb080h4977qmg6c")))

(define-public crate-dmntk-examples-0.0.38 (c (n "dmntk-examples") (v "0.0.38") (h "180iwy84h4qsiy6pvk8g7rvrqjaxlvbxzfs102r3rpya2g0hp8np")))

(define-public crate-dmntk-examples-0.0.40 (c (n "dmntk-examples") (v "0.0.40") (h "12wgrn3vmhw93hxhgc7nqgqb4h5d0f2drf31hf1xgddpxfwkfdxb")))

(define-public crate-dmntk-examples-0.0.41 (c (n "dmntk-examples") (v "0.0.41") (h "0jx57pkanssbfspcjgw4d67mwpvf5i84x71im12vbphrdxq380da")))

(define-public crate-dmntk-examples-0.0.42 (c (n "dmntk-examples") (v "0.0.42") (h "0x9cqr46z98x5g1jdxhx3qbv7v954ghfx8sl5wr0ii40z9zybqv2")))

(define-public crate-dmntk-examples-0.0.43 (c (n "dmntk-examples") (v "0.0.43") (h "11irg2f7vr2nqsbmvxcr91s162rpka6afdws1b0smjbjf5zkryqg")))

(define-public crate-dmntk-examples-0.0.44 (c (n "dmntk-examples") (v "0.0.44") (h "0m6nqbagz6yhkcq4fkm8zc67j8vagwn1i4y4ipid4mbk6hr40wjx")))

(define-public crate-dmntk-examples-0.0.45 (c (n "dmntk-examples") (v "0.0.45") (h "1r4bzbzm9k2ah881li7fpm4fmb6y8fyxfzpm0fsrr4kqvpsl5rkv")))

(define-public crate-dmntk-examples-0.0.46 (c (n "dmntk-examples") (v "0.0.46") (h "0042lnxvs1za1ingwb3bkpzhyfbsp8nxq5wkaj4yzdf0f08swm8s")))

(define-public crate-dmntk-examples-0.0.47 (c (n "dmntk-examples") (v "0.0.47") (h "05w969wp6lkinfg8kc8f8wbkd2sbv3dd7j41i91db6rn62gdkjl0")))

(define-public crate-dmntk-examples-0.0.48 (c (n "dmntk-examples") (v "0.0.48") (h "0w0jfjb2m46z9n93xjz5b4m1irxyxlnrvx992pn470n23fm4hyb9")))

(define-public crate-dmntk-examples-0.0.49 (c (n "dmntk-examples") (v "0.0.49") (h "0cmla9bxckqlf2nnm88g9y04z27hh0yxgfzqiasf4cizxi3dywlq")))

(define-public crate-dmntk-examples-0.0.50 (c (n "dmntk-examples") (v "0.0.50") (h "1ns9vh78pcbwpyjmqi7dlcag24q1nmcni9cxsq1bh3cd2mj32a1d")))

(define-public crate-dmntk-examples-0.0.52 (c (n "dmntk-examples") (v "0.0.52") (h "1gbyl6jfb3ilfj20d9mx9pip39dz13h4r5pa8j6jy3zzvzykh81v")))

(define-public crate-dmntk-examples-0.0.53 (c (n "dmntk-examples") (v "0.0.53") (h "01bidcwf0qxx8cgngspqaia35rl0wfh08vn711aimgkj1lps5mz3")))

(define-public crate-dmntk-examples-0.0.54 (c (n "dmntk-examples") (v "0.0.54") (h "1hi31f30pdkxag3x84z1p6fv5v69y45rgpi76nh253dzf9fgflx2")))

(define-public crate-dmntk-examples-0.1.0 (c (n "dmntk-examples") (v "0.1.0") (h "0nx81lmb21alxrw6k80bpyw3b9366nxwxqch1z1sfayz0nvjz7wj")))

(define-public crate-dmntk-examples-0.1.1 (c (n "dmntk-examples") (v "0.1.1") (h "07pkkwj6c1y6c7bjj07f434r9xrrzpd5a9xz2g5y6z6dw167sayx")))

(define-public crate-dmntk-examples-0.1.2-dev (c (n "dmntk-examples") (v "0.1.2-dev") (h "1j8cpaza5qg3j5i63mwywawr5gjsi5vlds83lr2rdrr4zmckyfq9")))

(define-public crate-dmntk-examples-0.1.2 (c (n "dmntk-examples") (v "0.1.2") (h "1ph75ha6xqwxxbyzr18rb07b8v6yy843j9h67jdx5myvlp60x5vp")))

(define-public crate-dmntk-examples-0.2.0 (c (n "dmntk-examples") (v "0.2.0") (h "1aj4fhz8bjvghk11d7c48gwmdgr2plndg8vwq62xxwknhkhagcr5")))

(define-public crate-dmntk-examples-0.2.5 (c (n "dmntk-examples") (v "0.2.5") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "16sxbcrxng661fkyddfqhgswcb379fv6iypr6nmmq6820ygkapdg")))

(define-public crate-dmntk-examples-0.3.0 (c (n "dmntk-examples") (v "0.3.0") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "18dzbwgkkw3j12mpf065fjm93cgcg7bdbyzhv24b8766a1j4n6nk")))

(define-public crate-dmntk-examples-0.3.1 (c (n "dmntk-examples") (v "0.3.1") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 2)))) (h "1lzxzshbdyq2lc5pgbdn44cxwsqmr7b1pplj7r71yjzyi99wpwn1")))

(define-public crate-dmntk-examples-0.3.2 (c (n "dmntk-examples") (v "0.3.2") (d (list (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "1n3y4284j0a1fqa2n5g41n8r5r5igv1h8zcd5gdf9sj34l4hzvps")))

(define-public crate-dmntk-examples-0.3.3 (c (n "dmntk-examples") (v "0.3.3") (d (list (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "0sw6k0c6i0xm5ipsbr5lgqj3ic1yzz5ansic5nhm1bni4rlbw6gh")))

(define-public crate-dmntk-examples-0.3.4 (c (n "dmntk-examples") (v "0.3.4") (d (list (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "1lvma76yc8ygb0913zv5yqap453jbv96k7zb5y80wd40aygy1ys7")))

(define-public crate-dmntk-examples-0.3.5 (c (n "dmntk-examples") (v "0.3.5") (d (list (d (n "walkdir") (r "^2.5.0") (d #t) (k 2)))) (h "0cqrsgfbdgfw7znmzv9gphw9jki94pipv6lh9ljwxyv2g1mahr1v")))

