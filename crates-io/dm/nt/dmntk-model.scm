(define-module (crates-io dm nt dmntk-model) #:use-module (crates-io))

(define-public crate-dmntk-model-0.0.6 (c (n "dmntk-model") (v "0.0.6") (d (list (d (n "dmntk-common") (r "^0.0.6") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.6") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "183s18hmwqbzxp3fy5wqgi5kxrc79mdnp975cxzdbbs59dx176py")))

(define-public crate-dmntk-model-0.0.7 (c (n "dmntk-model") (v "0.0.7") (d (list (d (n "dmntk-common") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "132bz6gvgnyizswp2cgh92wryd1j16l87ah39qsdq337xcdwfpjl")))

(define-public crate-dmntk-model-0.0.8 (c (n "dmntk-model") (v "0.0.8") (d (list (d (n "dmntk-common") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.7") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.7") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1xw8ynhpnxani4qsqlk2rylrmk8jm3d8g4xw169q97yxqcpg4k01")))

(define-public crate-dmntk-model-0.0.10 (c (n "dmntk-model") (v "0.0.10") (d (list (d (n "dmntk-examples") (r "^0.0.10") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.10") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0zbcc5665nlzminnbckfba2nv7x1q6cps3xa903mdkwrc30jaqcn")))

(define-public crate-dmntk-model-0.0.11 (c (n "dmntk-model") (v "0.0.11") (d (list (d (n "dmntk-examples") (r "^0.0.11") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0wxb976s1cwsasr768p3kz01880y7f505mqx9dmlksfnr0x31ym1")))

(define-public crate-dmntk-model-0.0.20 (c (n "dmntk-model") (v "0.0.20") (d (list (d (n "dmntk-common") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.20") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.20") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0gspwgbkm3nlssp1pjmhmaqfkwjg5xm242r0x4hzqrgx1x5q3hd1")))

(define-public crate-dmntk-model-0.0.35 (c (n "dmntk-model") (v "0.0.35") (d (list (d (n "dmntk-common") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.35") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.35") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1xg2vj7qxqwgfd4i5fh3jnyqkxyapyj3gkq2lfpxm71jhdhsg9gn")))

(define-public crate-dmntk-model-0.0.36 (c (n "dmntk-model") (v "0.0.36") (d (list (d (n "dmntk-common") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.36") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.36") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0mbff350xsjv1nd9vjgdiax4l4iha1jhkfbyc8dy5b05gamiafxy")))

(define-public crate-dmntk-model-0.0.37 (c (n "dmntk-model") (v "0.0.37") (d (list (d (n "dmntk-common") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.37") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.37") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0wmv37bhfh2sf1xqf29fd90hj5rsld48wb2n1q64ggh25gf2b70y")))

(define-public crate-dmntk-model-0.0.38 (c (n "dmntk-model") (v "0.0.38") (d (list (d (n "dmntk-common") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.38") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.38") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1pbz9d7gxnml5qpaxcpajk2rx6waj18ic7pv4i27gh6136wsc2mg")))

(define-public crate-dmntk-model-0.0.40 (c (n "dmntk-model") (v "0.0.40") (d (list (d (n "dmntk-common") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.40") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.40") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1n77w3caqmqn4svghfxqx2pfmcflwwkim0r00hm2xd1dmsnjj9w7")))

(define-public crate-dmntk-model-0.0.41 (c (n "dmntk-model") (v "0.0.41") (d (list (d (n "dmntk-common") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.41") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.41") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0r394jx4fv1s8z3pccf3c4a2b60v0irhgv2wpbb0r1xv4kj4xrrl")))

(define-public crate-dmntk-model-0.0.42 (c (n "dmntk-model") (v "0.0.42") (d (list (d (n "dmntk-common") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.42") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.42") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1ahj1wgcpk6blf0rbz8gnbzmaqc9lgc2yi7hy6v8mfw5kpjfsmkn")))

(define-public crate-dmntk-model-0.0.43 (c (n "dmntk-model") (v "0.0.43") (d (list (d (n "dmntk-common") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.43") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.43") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0j8k06sshfzfsqvjxigaqnrp2l69csjrg7h4zv8y5vg7lyh1jh1g")))

(define-public crate-dmntk-model-0.0.44 (c (n "dmntk-model") (v "0.0.44") (d (list (d (n "dmntk-common") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.44") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.44") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "0ihffqr9qb0rppqlw3d25qh65kp85wh044ykp79rpvnva09nn8ms")))

(define-public crate-dmntk-model-0.0.45 (c (n "dmntk-model") (v "0.0.45") (d (list (d (n "dmntk-common") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.45") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.45") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "1fyhqsdvmajk4phxcpk897i6nb8d5cp51rrqjnm1d4wla3i31qf0")))

(define-public crate-dmntk-model-0.0.46 (c (n "dmntk-model") (v "0.0.46") (d (list (d (n "dmntk-common") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.46") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.46") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)))) (h "10q31b3rm5kg214cdwgz7gnjraqraq5rvc8qyx3q7inw9qml3s3x")))

(define-public crate-dmntk-model-0.0.47 (c (n "dmntk-model") (v "0.0.47") (d (list (d (n "dmntk-common") (r "^0.0.47") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.47") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.47") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.47") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "1dlm9afqmfx3a04fkkrjinqcfwk2xqmkqg35kd3g4azwnm8s5prh")))

(define-public crate-dmntk-model-0.0.48 (c (n "dmntk-model") (v "0.0.48") (d (list (d (n "dmntk-common") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-examples") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.48") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.48") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "0hcfx55nlm67qlx2grr9z1rmwffckk040s7w4953wv6xsr0grx8y")))

(define-public crate-dmntk-model-0.0.49 (c (n "dmntk-model") (v "0.0.49") (d (list (d (n "dmntk-common") (r "=0.0.49") (d #t) (k 0)) (d (n "dmntk-examples") (r "=0.0.49") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.49") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.49") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "12g1cns0wx8kszph5fzi5k6i2c43lwn9z0f61b5ppmg5zxh4gcni")))

(define-public crate-dmntk-model-0.0.50 (c (n "dmntk-model") (v "0.0.50") (d (list (d (n "dmntk-common") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-examples") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel") (r "=0.0.50") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "=0.0.50") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "05bqhk7r7vg1s6b4yf68ikqd5r4awl9yk8jqyxb8lf8fm13k77pi")))

(define-public crate-dmntk-model-0.0.52 (c (n "dmntk-model") (v "0.0.52") (d (list (d (n "dmntk-common") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.52") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.52") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "16r2hrgm1cg9mm0p00lzirg6dggzid3wwd11z5gmsbbvsns6l5rs")))

(define-public crate-dmntk-model-0.0.53 (c (n "dmntk-model") (v "0.0.53") (d (list (d (n "dmntk-common") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.53") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.53") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "0h6hars410r22bvmczk9l5qgmghvh8pk05s0g2khp7qq9jhhrpxr")))

(define-public crate-dmntk-model-0.0.54 (c (n "dmntk-model") (v "0.0.54") (d (list (d (n "dmntk-common") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.54") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.0.54") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "144s19862nppvryzp5bhrxnc5i64znqi4fwry2q2srj9v35rcwk0")))

(define-public crate-dmntk-model-0.1.0 (c (n "dmntk-model") (v "0.1.0") (d (list (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "1pgqjgfdbxl76r5jzr51gjdyfavnjgmsblixp3j0kdrh1vghns6w")))

(define-public crate-dmntk-model-0.1.1 (c (n "dmntk-model") (v "0.1.1") (d (list (d (n "dmntk-common") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "1ssfbhdkh6hl8032nasss5vh0s4bbb6711sjlc4y2iq8mj8fklrz")))

(define-public crate-dmntk-model-0.1.2 (c (n "dmntk-model") (v "0.1.2") (d (list (d (n "dmntk-common") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.1.2") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "0qn2ppd827x6h0f8ggr4zrxpmxllzpl7zjs2wsq66ig6r1c7h7lb")))

(define-public crate-dmntk-model-0.2.0 (c (n "dmntk-model") (v "0.2.0") (d (list (d (n "dmntk-common") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "1hqrxxaakxdq34ncp5dsihqmd19acp01nd1nm1k21d346nw6mn4s")))

(define-public crate-dmntk-model-0.2.5 (c (n "dmntk-model") (v "0.2.5") (d (list (d (n "dmntk-common") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.2.5") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.2.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "0ki5bwb9jf8cyaj47hnldk1gdnnyz9qdsnnfb3m6cgcsa83hvawz")))

(define-public crate-dmntk-model-0.3.0 (c (n "dmntk-model") (v "0.3.0") (d (list (d (n "dmntk-common") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "1jk9lv6l6sx0rx3g1p0f22wmbx2njsqwkwp90x0cqbj6jfh88gs8")))

(define-public crate-dmntk-model-0.3.1 (c (n "dmntk-model") (v "0.3.1") (d (list (d (n "dmntk-common-1") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.0") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "19r682k15swqfpx4px9rrhyyq91lr7pahfd8wlw9b55b8xbv0hfr")))

(define-public crate-dmntk-model-0.3.2 (c (n "dmntk-model") (v "0.3.2") (d (list (d (n "dmntk-common") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.2") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)))) (h "0qxknyj66mcnzma4qjscrpf6cpgizx2wajiqp4kfschdaya1sdqv")))

(define-public crate-dmntk-model-0.3.3 (c (n "dmntk-model") (v "0.3.3") (d (list (d (n "dmntk-common") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.3") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)))) (h "1i0csj1w87rh1h3ybvvgkwanz7g5fzlpb0f5p8nx0vzq0pd97li2")))

(define-public crate-dmntk-model-0.3.4 (c (n "dmntk-model") (v "0.3.4") (d (list (d (n "dmntk-common") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.4") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)))) (h "12dhhzdrkb9xmcrfa89rk60j9qjsgig5m6x9k8qfqcqrphw4ljln")))

(define-public crate-dmntk-model-0.3.5 (c (n "dmntk-model") (v "0.3.5") (d (list (d (n "dmntk-common") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-examples") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-feel-parser") (r "^0.3.5") (d #t) (k 0)) (d (n "dmntk-macros") (r "^0.3.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)))) (h "0my7v2ghws64r0h4dp97d9gsaj9y7anbz4xm408wa6gbq6mys9i4")))

