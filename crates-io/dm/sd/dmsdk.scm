(define-module (crates-io dm sd dmsdk) #:use-module (crates-io))

(define-public crate-dmsdk-0.1.0 (c (n "dmsdk") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "dmsdk_ffi") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0b4883nmkxvpi40qdfrdxjnaj82j4ah1np5c4ljjfar7l5z8b03x")))

(define-public crate-dmsdk-0.1.1 (c (n "dmsdk") (v "0.1.1") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "dmsdk_ffi") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0hv7n5k0d15gh23ykcfxkv39a0gmpm8vlm2kalqcda98zh1r8q95")))

(define-public crate-dmsdk-0.1.2 (c (n "dmsdk") (v "0.1.2") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "dmsdk_ffi") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "195ygw75wii7d3sp0149jwh6qsjpw9d0shdkn6kqgq17sdbaxgqr")))

(define-public crate-dmsdk-0.1.3 (c (n "dmsdk") (v "0.1.3") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "dmsdk_ffi") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1m2rz3dlhsxz2wv19inh4yqffwc95qbapz0pb07ca9g4jsilils6")))

(define-public crate-dmsdk-0.2.0 (c (n "dmsdk") (v "0.2.0") (d (list (d (n "ctor") (r "^0.1.23") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "dmsdk_ffi") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "08hkfa83qj660yr4ml83i1z9l4n4jzgn05q5f0xfwmh8d2lwcv9f")))

