(define-module (crates-io dm sd dmsdk_ffi) #:use-module (crates-io))

(define-public crate-dmsdk_ffi-0.1.0 (c (n "dmsdk_ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "10glif1drh3az62nsd6d6q3zhb9h6brr9pvhdd89vnhchnnn318c")))

(define-public crate-dmsdk_ffi-0.1.1 (c (n "dmsdk_ffi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)))) (h "0dib91ny834rbpfh684wpjn6xbq0bi3axi3wb6bsx3lzdrida7c1")))

