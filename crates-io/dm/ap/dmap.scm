(define-module (crates-io dm ap dmap) #:use-module (crates-io))

(define-public crate-dmap-0.1.0 (c (n "dmap") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0pqi1b44yxchns3la9bn3w52n0bfh824b9dhhnmw5pkxhv3rydw5")))

(define-public crate-dmap-0.2.0 (c (n "dmap") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16nwiq3xxcxymvl57mls88sr5qda0camq8cs90wy7zjpp0mcb9rh")))

(define-public crate-dmap-0.2.1 (c (n "dmap") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0cqzydswf0nqqc4bm2h30aici4mnfg34xhncl53fihg71s2mhawj")))

