(define-module (crates-io dm at dmatcher) #:use-module (crates-io))

(define-public crate-dmatcher-0.1.0 (c (n "dmatcher") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0k02p7wfssnz9i7a90xsf0783x1m6az9l8gsvivv17jdnjch2ny0")))

(define-public crate-dmatcher-0.1.1 (c (n "dmatcher") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jrgsi0fkfx72l04a3g76wadbgnmmrh97pbnwipncaqyxqz54vjd")))

(define-public crate-dmatcher-0.1.2 (c (n "dmatcher") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mxjxgxfaqn95fjsdjnw74nrwg310bsm2fwqiymgf2mc5lgm5vql")))

(define-public crate-dmatcher-0.1.3 (c (n "dmatcher") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0fyicb1v56fl19gpp0cp6r20x5sqiph34q0lcb6mymap6kmmk26n")))

(define-public crate-dmatcher-0.1.4 (c (n "dmatcher") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "13iyd46fb7b5kfc6yyl539bj1sf3xacbckyfj91h9j5lw69ns1g0")))

(define-public crate-dmatcher-0.1.6 (c (n "dmatcher") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.19") (k 0)))) (h "027j8k9l17c0ws8j0862d50m3i5vz0h80jxs4nw5r9q52dl4d58k")))

(define-public crate-dmatcher-0.1.7 (c (n "dmatcher") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.19") (k 0)))) (h "10d41yrxdqa9hchl7bn7np5gbwxqn4cm7ay4h5fbnc38b51sbi5n")))

(define-public crate-dmatcher-0.1.8 (c (n "dmatcher") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.19") (k 0)))) (h "1j50x4rjkg09y8xpc4g9dv2lqnh9h3khpjm2l3mw92vqan1ksj2n")))

(define-public crate-dmatcher-0.1.9 (c (n "dmatcher") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.19") (k 0)))) (h "1zhipqrvgdwmipi9mhnk2z40b3lsalk41fndmbpj1jy96hw63wnb")))

(define-public crate-dmatcher-0.1.11 (c (n "dmatcher") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "trust-dns-proto") (r "^0.20.0-alpha.3") (k 0)))) (h "00rjrrx33i0kfrwr8awk6xrw6lkhzbsbjym91pjndb4m9fx28p1y")))

