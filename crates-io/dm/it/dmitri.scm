(define-module (crates-io dm it dmitri) #:use-module (crates-io))

(define-public crate-dmitri-0.1.0 (c (n "dmitri") (v "0.1.0") (d (list (d (n "breadx") (r "^2.0.0") (d #t) (k 0)) (d (n "font-loader") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gluten-keyboard") (r "^0.1.2") (d #t) (k 0)) (d (n "hex_color") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1wiyfw3b7mjpmwarvrbccj1fjchgqrn41316d8k2vkij7d3dblza")))

(define-public crate-dmitri-0.1.1 (c (n "dmitri") (v "0.1.1") (d (list (d (n "breadx") (r "^2.0.0") (d #t) (k 0)) (d (n "font-loader") (r "^0.11.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gluten-keyboard") (r "^0.1.2") (d #t) (k 0)) (d (n "hex_color") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-fuzzy-search") (r "^0.1.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)))) (h "1mcmn8f6991qh4k8r3v5m3izhfy1qbydfivics7kq98niyf6hdsq")))

