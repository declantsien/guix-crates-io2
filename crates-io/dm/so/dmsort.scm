(define-module (crates-io dm so dmsort) #:use-module (crates-io))

(define-public crate-dmsort-0.1.0 (c (n "dmsort") (v "0.1.0") (d (list (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 2)) (d (n "quickersort") (r "^2.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)))) (h "1g1yb54r1p8h8ad3yzvmxgpjdv6wyycfr30a11nfqvxl6x3pqbr6")))

(define-public crate-dmsort-0.1.1 (c (n "dmsort") (v "0.1.1") (d (list (d (n "gnuplot") (r "^0.0.21") (d #t) (k 2)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 2)) (d (n "quickersort") (r "^2.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)))) (h "0zyiikxl2iwqglfq06dfpn9yzia8x5ml368xs4id1iqxl1zs7q59")))

(define-public crate-dmsort-0.1.2 (c (n "dmsort") (v "0.1.2") (d (list (d (n "gnuplot") (r "^0.0.22") (d #t) (k 2)) (d (n "pbr") (r "^1.0.0-alpha.2") (d #t) (k 2)) (d (n "pdqsort") (r "^0.1.1") (d #t) (k 0)) (d (n "quickersort") (r "^2.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 2)))) (h "0gmbdv1cgcv7h1cfivjs217bn01j3acwwr0aphx67p86b4zqqn4p")))

(define-public crate-dmsort-0.1.3 (c (n "dmsort") (v "0.1.3") (d (list (d (n "gnuplot") (r "^0.0.22") (d #t) (k 2)) (d (n "pbr") (r "^1.0.0") (d #t) (k 2)) (d (n "pdqsort") (r "^0.1.2") (d #t) (k 0)) (d (n "quickersort") (r "^2.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "16j926yhpsyj3w4mpy7sb3zsn49j4sq6yhjqwqyx8xvpjsjrpffq")))

(define-public crate-dmsort-1.0.0 (c (n "dmsort") (v "1.0.0") (d (list (d (n "gnuplot") (r "^0.0.22") (d #t) (k 2)) (d (n "pbr") (r "^1.0.0") (d #t) (k 2)) (d (n "quickersort") (r "^2.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "time") (r "^0.1.37") (d #t) (k 2)))) (h "0i3lxid9sxvwkjsi0xmva0dl9kc5ygh2m5y35zqd5kwmr6blpjdz")))

(define-public crate-dmsort-1.0.1 (c (n "dmsort") (v "1.0.1") (d (list (d (n "gnuplot") (r "^0.0.22") (d #t) (k 2)) (d (n "pbr") (r "^1") (d #t) (k 2)) (d (n "quickersort") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1z140g8rynvim8aq7li9krbl5p3crvkc3zbzfjdhk3v7nxf9ysfl")))

(define-public crate-dmsort-1.0.2 (c (n "dmsort") (v "1.0.2") (d (list (d (n "gnuplot") (r "^0.0.22") (d #t) (k 2)) (d (n "pbr") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1lqh9yfj268l938axk31lil6rnqxz8kzwpgp8sgprha1jjz8zg7h")))

