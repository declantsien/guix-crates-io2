(define-module (crates-io dm xp dmxparser) #:use-module (crates-io))

(define-public crate-dmxparser-0.1.0 (c (n "dmxparser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1idjp0rv5cqkbmlzsf9j1vd3ciy34ynkxrvp0wdag4gxvynvyijp")))

