(define-module (crates-io dm g- dmg-oxide) #:use-module (crates-io))

(define-public crate-dmg-oxide-0.1.0 (c (n "dmg-oxide") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fatfs") (r "^0.3.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.23") (d #t) (k 0)) (d (n "fscommon") (r "^0.1.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (d #t) (k 0)) (d (n "gpt") (r "^3.0.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "109hp70kla94xjn11ia44p47pl91d551279493xswbvxnwrc7flc")))

