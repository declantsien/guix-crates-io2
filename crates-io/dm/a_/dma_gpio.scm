(define-module (crates-io dm a_ dma_gpio) #:use-module (crates-io))

(define-public crate-dma_gpio-0.1.0 (c (n "dma_gpio") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1rj78qk0gznskyif9q25v6f9392b48a1masdn1cz7p3jkwym2xc6") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.1 (c (n "dma_gpio") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "158yr5c2ddqkfslnkgdx8rvgazyx5m7hnsacmmw47d7wyqxv5jaq") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.2 (c (n "dma_gpio") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1azwsmbzf2blz4c23gbvxphjlrn3f5xj3xryklprp0rga1y4mrmr") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.3 (c (n "dma_gpio") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0srx1fpcllkpr5i2ivsv88l97dw4xrn57c55iw6bhkiras653zmk") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.4 (c (n "dma_gpio") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0d67a0skb77plifc6rwn58w9j0h6r5sc1353nnrasm0j18iwmqy0") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.5 (c (n "dma_gpio") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1gh8sjsixxpc1226c5cdkma1fh1g2dixz77b7y4dvs1wf4l06sq7") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.6 (c (n "dma_gpio") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "17y36dfymbpd7n9ivjnkfnyd58q8994pdm28kkfqla4pdrf429j3") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.7 (c (n "dma_gpio") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1398zm4fqxs9pxi4anm52ggij29wjssqwfanpnihxs18fdfy66ra") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.8 (c (n "dma_gpio") (v "0.1.8") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "10lbvzrrhrdz3wh4kl6yaky4i7w2gwifz5arw42v2rpmcdn8r6bc") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.1.9 (c (n "dma_gpio") (v "0.1.9") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hwloc") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1axzbvkd14gsc92a27y50li7jq4lb47jvj707imh6wc5m5znrvaz") (f (quote (("debug") ("bind_process" "hwloc")))) (y #t)))

(define-public crate-dma_gpio-0.2.0 (c (n "dma_gpio") (v "0.2.0") (h "1ky5320jf2jc4v80i46allspqpdrbkp7dbqixsbhc4njijv0gz1x") (y #t)))

