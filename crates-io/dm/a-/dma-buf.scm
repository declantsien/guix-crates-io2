(define-module (crates-io dm a- dma-buf) #:use-module (crates-io))

(define-public crate-dma-buf-0.1.0 (c (n "dma-buf") (v "0.1.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "mmap") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.20") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "1l37bkf6gmv6gwxb90dfqvha4wfxkssghnvsar7y1aikq20xw4i4")))

(define-public crate-dma-buf-0.2.0 (c (n "dma-buf") (v "0.2.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "memmap") (r "~0.3") (d #t) (k 0) (p "memmap2")) (d (n "nix") (r "~0.20") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "0lra8l94pii8p457mw5wp1y05r7hxn00k66vsmls4dp09wwhp0sv")))

(define-public crate-dma-buf-0.3.0 (c (n "dma-buf") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "memmap") (r "^0.5.8") (d #t) (k 0) (p "memmap2")) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0qmiamr8k593z7zksy04d3c3pd7csnwhg456bq3jhqwv0pzmbjw4")))

(define-public crate-dma-buf-0.2.1 (c (n "dma-buf") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "memmap") (r "^0.9.3") (d #t) (k 0) (p "memmap2")) (d (n "nix") (r "^0.27.1") (f (quote ("ioctl" "fs"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1k531ivxvx7nb68qj89ijgihva6bla8grks8lgsf1b8dv0yr54mz")))

(define-public crate-dma-buf-0.3.1 (c (n "dma-buf") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "memmap") (r "^0.9.3") (d #t) (k 0) (p "memmap2")) (d (n "nix") (r "^0.27.1") (f (quote ("ioctl" "fs"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1an7vfnikqzrl8bxxw49695fwmc6pkn9jcf4pk7xq1f21mgyhqha")))

(define-public crate-dma-buf-0.4.0 (c (n "dma-buf") (v "0.4.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustix") (r "^0.38.31") (f (quote ("fs" "mm" "param"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "03q9k703hg21z1nr0x0gwbyc1mcd91pziwqcvw5ijra7w3z2zli6") (f (quote (("nightly"))))))

