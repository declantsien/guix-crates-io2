(define-module (crates-io dm oj dmoj) #:use-module (crates-io))

(define-public crate-dmoj-0.1.0 (c (n "dmoj") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1df3bpb57gybnrpv2ar167kw2599b6rms51nrg1zzvs6lqf440ij")))

(define-public crate-dmoj-0.1.1 (c (n "dmoj") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0m20g0hq18i5mmwz2irzlwv0sk782gjc3mvdj0sq61d2pd6vwqga")))

(define-public crate-dmoj-0.1.2 (c (n "dmoj") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0f4m0232mzzl7hilyglw4vwiw8ir0hq1zfli73i5v7phcb5naxqh")))

(define-public crate-dmoj-0.1.3 (c (n "dmoj") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1awiwqs3j2saq663sfw6srap4b15j3fgwfi9yfi19fn2n3i0mbaj")))

(define-public crate-dmoj-0.1.4 (c (n "dmoj") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1csyvx6ng9a01b8x57gqjgsnfylj6x9c2sgaak4dr9fq814riaf5")))

(define-public crate-dmoj-0.1.5 (c (n "dmoj") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1wc41q83c81hysqyx4y2xrwg2bj8vasdjpmkinwjlmhxfxas3y51")))

