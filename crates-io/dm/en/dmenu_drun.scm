(define-module (crates-io dm en dmenu_drun) #:use-module (crates-io))

(define-public crate-dmenu_drun-0.1.0 (c (n "dmenu_drun") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10wb20a8kdgn41hw8ny8mj2cmgkiq81s14q3qp3z3fa3xkbaf4kw")))

(define-public crate-dmenu_drun-0.1.1 (c (n "dmenu_drun") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05y72chr55a2kvd1vlq3far7cqifzgay60fzh6p7qpnxp10gjskf")))

(define-public crate-dmenu_drun-0.1.2 (c (n "dmenu_drun") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14bgnnar6lfkd28jl3dbmpd41c0aik4nvx32wk392695ixvk202x")))

(define-public crate-dmenu_drun-0.1.3 (c (n "dmenu_drun") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jq9k1981ivjwjn54cwyr4yh9za5rxgys4rgfkx8mq2q1sy4v6gj")))

(define-public crate-dmenu_drun-0.1.4 (c (n "dmenu_drun") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.18") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ra1gml55v060r6gxf4jz6jdl6xwacifsakkqslsgh09jdm42d4g")))

