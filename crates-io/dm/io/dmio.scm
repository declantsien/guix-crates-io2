(define-module (crates-io dm io dmio) #:use-module (crates-io))

(define-public crate-dmio-0.1.0 (c (n "dmio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kp6lq2slyw7rrxhqmxj1c37ipjh9f924ij5n4962agdp8w4znvi")))

(define-public crate-dmio-0.1.2 (c (n "dmio") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gryp9i7zhx208nj8z4mh9xhzl5jr5zigfw0v4kkpq62vq1bag4f")))

(define-public crate-dmio-0.1.4 (c (n "dmio") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0y8b5vvqsc0rm3g6sj1ly2f8y0ln4zwkzqid50c6p92p5f06d98h")))

(define-public crate-dmio-0.1.5 (c (n "dmio") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "14n3vbrjrz6g67i26s1n3nprfwv0rlzpyrpjdgg94c2ikva86qly")))

