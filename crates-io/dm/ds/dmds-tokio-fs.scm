(define-module (crates-io dm ds dmds-tokio-fs) #:use-module (crates-io))

(define-public crate-dmds-tokio-fs-0.1.0 (c (n "dmds-tokio-fs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "dmds") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("fs" "io-util" "time" "rt"))) (d #t) (k 0)))) (h "1v7njysmczdrbqxpjqsdy2j055h7619p6jblxvbq3b4a2aisa74q")))

(define-public crate-dmds-tokio-fs-0.2.0 (c (n "dmds-tokio-fs") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "dmds") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("fs" "io-util" "time" "rt"))) (d #t) (k 0)))) (h "13yaik31bs01yi6f8l558464zw531czl6k9la6wrag6plmadi3nx")))

(define-public crate-dmds-tokio-fs-0.3.0 (c (n "dmds-tokio-fs") (v "0.3.0") (d (list (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "dmds") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("fs" "io-util" "time" "rt"))) (d #t) (k 0)))) (h "0ncjsxzhglzz6vhzb26gszpzm05a47r131rsgrb0b3z7pwllh2ml")))

