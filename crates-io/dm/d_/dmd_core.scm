(define-module (crates-io dm d_ dmd_core) #:use-module (crates-io))

(define-public crate-dmd_core-0.2.0 (c (n "dmd_core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "15550r3sb4anv5fbkb7585jd8d23vivr9ymgfl56ysb9vz41cc0d")))

(define-public crate-dmd_core-0.2.1 (c (n "dmd_core") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0xj0fgbjw36iyy3gg87p75lqb0ds5x9bn3im7730r33bqi6n85f7")))

(define-public crate-dmd_core-0.3.0 (c (n "dmd_core") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1jkw78bf8lnqmj2vj3792294h0kpk64l4vzzmm8fgf0h47jdjp5l")))

(define-public crate-dmd_core-0.3.1 (c (n "dmd_core") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "15wb91vvglbk252yhr3kr4rcl7127yfj0k2j67j8gmzashkzjjbr")))

(define-public crate-dmd_core-0.4.0 (c (n "dmd_core") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "02mnbz4ld01j292f5n54mhv6lkmsamss8arbl0jbwj04gc3kr2ns")))

(define-public crate-dmd_core-0.6.2 (c (n "dmd_core") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0bhr7d0ml9rdminp5jldvcdzrhjc8qpk0x7xc2r9irswfvbbq4dv")))

(define-public crate-dmd_core-0.6.3 (c (n "dmd_core") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0mx33vqnq41hcm2bn2drk439arniszsdsa01caikw2ybxyb29wck")))

