(define-module (crates-io dm v_ dmv_derive) #:use-module (crates-io))

(define-public crate-dmv_derive-0.0.0 (c (n "dmv_derive") (v "0.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sdz5s7hkw2nj5azy6z0v281q67k162f0gcky2x92984hxrgq7qh") (y #t)))

(define-public crate-dmv_derive-0.1.0 (c (n "dmv_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fjwp1dviaypx98i9c35z0847zlsaw2jy029m1aysml1nvf5h309") (y #t)))

(define-public crate-dmv_derive-0.2.0 (c (n "dmv_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cybglk8iwgrn8dlfyjr7s97zf85kjk2v4mk8ps2z1xs8baaps8v")))

(define-public crate-dmv_derive-0.2.1 (c (n "dmv_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ksmrsq9bl7h4n2i2y3fnsqzds7s64ag2gqr9ixsqmbmvalfm6fb")))

(define-public crate-dmv_derive-0.2.2 (c (n "dmv_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15l1lvv0y69852cmqadlr3d4660hzn1pyhx15ij02ixr4s1aj77y")))

