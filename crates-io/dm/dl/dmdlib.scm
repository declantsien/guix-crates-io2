(define-module (crates-io dm dl dmdlib) #:use-module (crates-io))

(define-public crate-dmdlib-0.1.0 (c (n "dmdlib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "requestty") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "10n1z5ipg49v187yxycjhdsid74bkzcmfzykwvvhqw62frljj07a") (y #t)))

