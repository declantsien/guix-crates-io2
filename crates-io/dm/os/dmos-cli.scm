(define-module (crates-io dm os dmos-cli) #:use-module (crates-io))

(define-public crate-dmos-cli-0.2.1 (c (n "dmos-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dmos") (r "^0") (k 0)))) (h "08jx2qrskbpx7r597y259r3zkm2pcz63kyc5w5r9zgjnwchwk8vd") (f (quote (("syntect" "dmos/syntect") ("inkjet" "dmos/inkjet") ("default" "syntect"))))))

(define-public crate-dmos-cli-0.3.0 (c (n "dmos-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dmos") (r "^0") (k 0)))) (h "08v9yf8s3gqq9qawc5wxpabzmwmhjrqgpzhilng3hcsmkz391lqn") (f (quote (("syntect" "dmos/syntect") ("inkjet" "dmos/inkjet") ("default" "syntect"))))))

