(define-module (crates-io dm os dmos) #:use-module (crates-io))

(define-public crate-dmos-0.2.1 (c (n "dmos") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "inkjet") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "jotdown") (r "^0.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ksnn6710y9ir4p23ga21a3wx9d1l9priqksr01dw5wvrpf2psmh") (f (quote (("default" "syntect")))) (s 2) (e (quote (("syntect" "dep:syntect") ("inkjet" "dep:inkjet"))))))

(define-public crate-dmos-0.3.0 (c (n "dmos") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "inkjet") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "jotdown") (r "^0.4.0") (d #t) (k 0)) (d (n "syntect") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19cs1yzp33gzyk6s59ffi3s9vv7pdzry2j5xi1sd9zn18bn5rn22") (f (quote (("default" "syntect")))) (s 2) (e (quote (("syntect" "dep:syntect") ("inkjet" "dep:inkjet"))))))

