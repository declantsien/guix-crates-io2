(define-module (crates-io dm ut dmutils_derive) #:use-module (crates-io))

(define-public crate-dmutils_derive-0.1.0-alpha.0 (c (n "dmutils_derive") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "148vw5jw2yn5k07i3w2wldxa5ca6bnmi6v3l87jnr2i8nvy17wz0")))

