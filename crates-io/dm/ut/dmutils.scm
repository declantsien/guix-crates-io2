(define-module (crates-io dm ut dmutils) #:use-module (crates-io))

(define-public crate-dmutils-0.1.0-alpha.0 (c (n "dmutils") (v "0.1.0-alpha.0") (h "1xw8d5s3dlrql7x1xdjlcwascz65qy6vnn77nffasyvxy6dddx0n")))

(define-public crate-dmutils-0.1.0-alpha.1 (c (n "dmutils") (v "0.1.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zaf28v34b3rz9jyir08ccv7hkh259g96xsq76i2fj7b530ndwvn")))

