(define-module (crates-io dm id dmidecode-rs) #:use-module (crates-io))

(define-public crate-dmidecode-rs-0.1.0 (c (n "dmidecode-rs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smbios-lib") (r "^0.7.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1kh2g3r23vxr2sdinr4rsrs8jy09wxnpdg99hvzb8dpp76k774yl")))

(define-public crate-dmidecode-rs-0.2.0 (c (n "dmidecode-rs") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smbios-lib") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1r78fg6wc3wjxf675c086m9j3di65594pc9qjylh7gjv9cwwasl1")))

(define-public crate-dmidecode-rs-0.2.1 (c (n "dmidecode-rs") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smbios-lib") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0g8bg736m18gijslbxvc5nsiq3wpllbnz4ssa74akp40sqfh778a")))

(define-public crate-dmidecode-rs-0.2.2 (c (n "dmidecode-rs") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smbios-lib") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "10kg4xy7rdfhw2c21raib5lc8iqjysp6ij12br0mpvzn736sxg7w")))

