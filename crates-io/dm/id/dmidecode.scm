(define-module (crates-io dm id dmidecode) #:use-module (crates-io))

(define-public crate-dmidecode-0.1.0 (c (n "dmidecode") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0617bfxgxl7ny4syy737r6741nfx3sagz76jfiy4gv7mjrgq0ajd")))

(define-public crate-dmidecode-0.2.0 (c (n "dmidecode") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0psyrxmdraqifi4mkihqg8yb2ayimhxcg870q56q0s9xv4drqljf")))

(define-public crate-dmidecode-0.3.0 (c (n "dmidecode") (v "0.3.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "06wsdfn4ffmdw9vvj973jijc0nj2ab8crk6mfzsjl75jjfd8p0kj")))

(define-public crate-dmidecode-0.3.1 (c (n "dmidecode") (v "0.3.1") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "076hqxgz5sf50sbl82b33096h883zb365616pc53y4r5zvfli510")))

(define-public crate-dmidecode-0.4.0 (c (n "dmidecode") (v "0.4.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0zqm5b1kkdq1y1finava76pa7mjpqqsch3x5in9865dls61l0ns4")))

(define-public crate-dmidecode-0.4.1 (c (n "dmidecode") (v "0.4.1") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0bqcvdljc81sd47qz503dlz8kqmnnhxn9lm21l1850jymr7a7f2d")))

(define-public crate-dmidecode-0.4.2 (c (n "dmidecode") (v "0.4.2") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0ymv3vfapc1yrk4n71c5sjvjqwgpy4gxj8h1byvfx6m2lzm06lk2")))

(define-public crate-dmidecode-0.4.3 (c (n "dmidecode") (v "0.4.3") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0bx4zfiviligjdyc92jx33gw6vm4wgj94gmkfryrv77laanznh11")))

(define-public crate-dmidecode-0.5.0 (c (n "dmidecode") (v "0.5.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0sc5pwysl4sgws8x83kfa7xkndj1wa4i7pfjamkd9pll4kjkvnhj")))

(define-public crate-dmidecode-0.6.0 (c (n "dmidecode") (v "0.6.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1x9vpx19ik7mss9cbwfkfl545gh5blzsr9j0yg2s0yxagqw86saq")))

(define-public crate-dmidecode-0.7.0 (c (n "dmidecode") (v "0.7.0") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1qqc30mll1cbg4nx7l44fvab1rjqyl723f1mriwiggvjkqlplkjm")))

(define-public crate-dmidecode-0.7.1 (c (n "dmidecode") (v "0.7.1") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "13jqv0h6szhlvda4dhvqssx41657jdjjg22in848ysm45qlgwzsc")))

(define-public crate-dmidecode-0.7.2 (c (n "dmidecode") (v "0.7.2") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0g17y2rsngi68c3yvgidymh3pcyl9ax6hl8mbdv3bw40vn7izp0p")))

(define-public crate-dmidecode-0.7.3 (c (n "dmidecode") (v "0.7.3") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0pk0vv20g08g7vz3b8bffr7j11v852bw6b1bcs7r7gz3z6krmk03")))

(define-public crate-dmidecode-0.7.4 (c (n "dmidecode") (v "0.7.4") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1rx5s0j1x0n9wphpajz3zq9kgfzjxhwkdv7dlg1ninry3m9nvi93")))

(define-public crate-dmidecode-0.7.5 (c (n "dmidecode") (v "0.7.5") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0iq2hidw0w4j24414if0d7dh0c0xqj30bv2lljpvq538w21wrfrm")))

(define-public crate-dmidecode-0.7.6 (c (n "dmidecode") (v "0.7.6") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0nj46mcmb966q24vw4pbsgiwkjkqrf0xwr7ij61c0pa39v9krk1s") (r "1.62")))

(define-public crate-dmidecode-0.7.7 (c (n "dmidecode") (v "0.7.7") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1p96s1xns579gihnvdbfg4l857i8ymfsdqg51pxlql8lpb791bgh") (r "1.62")))

(define-public crate-dmidecode-0.7.8 (c (n "dmidecode") (v "0.7.8") (d (list (d (n "aho-corasick") (r "^0.6.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0zb8hhi1pihh1za8r2riaq1sggxqdw6b4xa7bazcwxw45084vx5i") (r "1.62")))

(define-public crate-dmidecode-0.8.0 (c (n "dmidecode") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0vf1njpvhgi2dq4l1af83zawfq498gjbd9pk7v7cyl4y44v0j9id") (f (quote (("std") ("default")))) (r "1.62")))

(define-public crate-dmidecode-0.8.1 (c (n "dmidecode") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0siq328nlgrwn7zsq9aahbd25xhjvydjcl8fhs1z0xl6xly4z96b") (f (quote (("std") ("default")))) (r "1.62")))

(define-public crate-dmidecode-0.8.2 (c (n "dmidecode") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0l3zmldj8311npz018783l2vffk5ir1ax9dv6vys3wjf3jjgk1ps") (f (quote (("std") ("default")))) (r "1.62")))

(define-public crate-dmidecode-0.8.3 (c (n "dmidecode") (v "0.8.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "15gvpags63a1nhk5jyybnn8h5g4gw7wbx3wfkga2kxyzzzalrx15") (f (quote (("std") ("default")))) (r "1.62")))

