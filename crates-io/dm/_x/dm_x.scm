(define-module (crates-io dm _x dm_x) #:use-module (crates-io))

(define-public crate-dm_x-0.1.0 (c (n "dm_x") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02wz9vn6xfrwn4jvkiav388d5c2hfgdxklzhxdf9jgznfp0z9hx5")))

(define-public crate-dm_x-0.1.1 (c (n "dm_x") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b5bhiqi4jnqsn57facmij8s9k7ym5qxgyjmzbqnzmbiq32fqf5h")))

(define-public crate-dm_x-0.2.0 (c (n "dm_x") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "05vba4v21a1wygq8vhx7p7zmywgvhb1l0kx5c0y8y73fy79zlj0d") (y #t) (s 2) (e (quote (("config" "dep:serde" "dep:toml"))))))

(define-public crate-dm_x-0.2.1 (c (n "dm_x") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0z936d68aybkr4b3wzzxlxk8r0vb5fch333vy7wdcxkkb441amd2") (s 2) (e (quote (("config" "dep:serde" "dep:toml"))))))

(define-public crate-dm_x-0.2.3 (c (n "dm_x") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "147sns9n72q5vc3z5j6xnfaz3dh1mn318ybq83lkxkv8i09hispy") (s 2) (e (quote (("config" "dep:serde" "dep:toml"))))))

