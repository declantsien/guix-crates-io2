(define-module (crates-io dm x- dmx-rdm) #:use-module (crates-io))

(define-public crate-dmx-rdm-0.0.8-alpha (c (n "dmx-rdm") (v "0.0.8-alpha") (d (list (d (n "binary-layout") (r "^3.2") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fugit") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "12n2vrk9jagdx69hrwfdqij53ryqp6zmzq7yapplharq3wc8prmn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-dmx-rdm-0.0.9-alpha (c (n "dmx-rdm") (v "0.0.9-alpha") (d (list (d (n "binary-layout") (r "^4.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "01zv29q4imdimszsdq2xrb5hxiny4w7a4nr5jam8vpj073c0kyhc") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-dmx-rdm-0.0.10-alpha (c (n "dmx-rdm") (v "0.0.10-alpha") (d (list (d (n "binary-layout") (r "~4.0.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "1lf3ab7n8wq0ln9xwjxca92nnvn8pi000nm8krdphw2zmbxgzkyk") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-dmx-rdm-0.0.11-alpha (c (n "dmx-rdm") (v "0.0.11-alpha") (d (list (d (n "binary-layout") (r "~4.0.2") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "1i0ibkfqzbzi7pjqxwyq7g4xmdj94059yq7b09i66a3q1zaq41sr") (f (quote (("std" "binary-layout/std") ("default" "std")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-dmx-rdm-0.0.12-alpha (c (n "dmx-rdm") (v "0.0.12-alpha") (d (list (d (n "binary-layout") (r "~4.0.2") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "0zlwz3ygnslq8hd174khaa78bc3f0qalccgh66szm5dhl7adgdhj") (f (quote (("std" "binary-layout/std") ("default" "std")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

