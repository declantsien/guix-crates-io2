(define-module (crates-io dm x- dmx-termios) #:use-module (crates-io))

(define-public crate-dmx-termios-0.3.0 (c (n "dmx-termios") (v "0.3.0") (d (list (d (n "ioctl-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ifaw0nsk96mwsks9nr9n3gv1p0swka7gy8lkgdsi8j7pvimayfg")))

