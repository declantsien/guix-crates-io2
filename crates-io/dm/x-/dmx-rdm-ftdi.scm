(define-module (crates-io dm x- dmx-rdm-ftdi) #:use-module (crates-io))

(define-public crate-dmx-rdm-ftdi-0.0.2-alpha (c (n "dmx-rdm-ftdi") (v "0.0.2-alpha") (d (list (d (n "dmx-rdm") (r "^0.0.8-alpha") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lil7ikjprjlwww7k9qbahyihkh456i770svcrmd1jmcda5i85xs")))

(define-public crate-dmx-rdm-ftdi-0.0.3-alpha (c (n "dmx-rdm-ftdi") (v "0.0.3-alpha") (d (list (d (n "dmx-rdm") (r "^0.0.9-alpha") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0djvn7shl6ppam70wsgmfl4skwic213z3aywgl6zrjdnvljqvqy6") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-dmx-rdm-ftdi-0.0.4-alpha (c (n "dmx-rdm-ftdi") (v "0.0.4-alpha") (d (list (d (n "dmx-rdm") (r "~0.0.10-alpha") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0iqgwx2nxjqv0pxs10qrn12jx95f83ai6jf7x76rg264z3g12zxf") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-dmx-rdm-ftdi-0.0.5-alpha (c (n "dmx-rdm-ftdi") (v "0.0.5-alpha") (d (list (d (n "dmx-rdm") (r "^0.0.11-alpha") (d #t) (k 0)) (d (n "libftd2xx") (r "^0.32") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17dv9s9lhas9ndjn85hf9jv9j0iqmwc6nnvdqp1hn6ldl8yq4m36") (f (quote (("default" "log")))) (s 2) (e (quote (("log" "dep:log"))))))

