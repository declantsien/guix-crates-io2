(define-module (crates-io e3 #{10}# e310x) #:use-module (crates-io))

(define-public crate-e310x-0.1.0 (c (n "e310x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.1.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1ivjjjm2q4ivkpqm2bd2n8mpwnmjxma66965mk70ff1xkvi3r70k") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-e310x-0.1.1 (c (n "e310x") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.1.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0d0qmj8a1llfg4lj8q45fxbjm1gcqbwpfgwccxdwn0a9bnj0fslx") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-e310x-0.2.0 (c (n "e310x") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.3.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1np77cii5inmbpgd98dd353v5sby3nlbwlspppmm1qgjkaj3nld4") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-e310x-0.3.0 (c (n "e310x") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.4.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "106jdjljqldgba8hk0prcms6wcml8cahk3awv8v9k2nn81zcpmya") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-e310x-0.4.0 (c (n "e310x") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0mhrhwc5d3w3krib98vdlpxb8gxgb7isf1z2h8gmln3lnflcxvfg") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-e310x-0.5.0 (c (n "e310x") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "189wfdv3qys5mrd3372pf4jjj63z3rmbylm988ajm5n8a2757wd9") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.5.1 (c (n "e310x") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "08ccsmkdn8xla7gmbhyyknwc0jklfwjbhs094i9c93s4b92g0pb1") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.5.2 (c (n "e310x") (v "0.5.2") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yd2rs78rrqp3h06vvs09bpivhya0lfsvpqfgdw6sj7fclzzcj0h") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.5.3 (c (n "e310x") (v "0.5.3") (d (list (d (n "bare-metal") (r "^0.2.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1dm6ckrlj23rxywyyv887igyx1bj024avicr3nq730qsf9ly8nc7") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.6.0 (c (n "e310x") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0pqsm66ndmdl9xcyyk1kgwgwpky3k9n8khz1470xgh7l1bc1i6hh") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.7.0 (c (n "e310x") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0n0xc2ij864d9dali0rr6qsn2rl2i97f8n8g29cyl0i8ai2pp1ii") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.8.0 (c (n "e310x") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0pspqxqnjf5gp7fzzbfgqd783wpmw1glydl37139lv3grmlvy0cr") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.8.1 (c (n "e310x") (v "0.8.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1wq5mx5vw6gdn2hwhi6prkp7wpx8l9pa3906syd7rp2vigx2rqfn") (f (quote (("rt" "riscv-rt") ("g002"))))))

(define-public crate-e310x-0.9.0 (c (n "e310x") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fd8shvy682ksphw5xmf9jl4c4y7j2hmhc1c7cab2l4818z6skrb") (f (quote (("rt") ("g002"))))))

(define-public crate-e310x-0.9.1 (c (n "e310x") (v "0.9.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xn7dqnhvk2sijx4ymsbjz1nkp09qwk2n7nrs8sga9cd7c06ldxy") (f (quote (("rt") ("g002")))) (y #t)))

(define-public crate-e310x-0.10.0 (c (n "e310x") (v "0.10.0") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1k3zd0v36c5mbgka7ipvf1lsddwkc69k2llix4qbxwphp0y669a7") (f (quote (("rt") ("g002")))) (r "1.59")))

(define-public crate-e310x-0.11.0 (c (n "e310x") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ljxxnxgdgaj0iwm2pl7y5f5n2c61z2sqfikmra6gi6knwymb8rb") (f (quote (("rt") ("g002")))) (r "1.59")))

