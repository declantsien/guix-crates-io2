(define-module (crates-io e3 #{10}# e310x-hal) #:use-module (crates-io))

(define-public crate-e310x-hal-0.2.0 (c (n "e310x-hal") (v "0.2.0") (d (list (d (n "e310x") (r "^0.2.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.3.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.3.0") (d #t) (k 0)))) (h "0zgznhpb3v44sjikvfs5y8ad5gn64gic2j0zz22vzi5db2g830zb") (f (quote (("pll") ("lfaltclk") ("hfxosc") ("default" "pll" "hfxosc" "lfaltclk"))))))

(define-public crate-e310x-hal-0.3.0 (c (n "e310x-hal") (v "0.3.0") (d (list (d (n "e310x") (r "^0.3.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.4.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1xfx0npppwzz3vlv3im5dqhs4mwxhdzq6b1z5l30pwrxckvrhzbb")))

(define-public crate-e310x-hal-0.3.1 (c (n "e310x-hal") (v "0.3.1") (d (list (d (n "e310x") (r "^0.3.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.4.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.4.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "05j940vcm3x26lg6x1k5115jd8m4fpqj2c112qxrvjx86qla5q90")))

(define-public crate-e310x-hal-0.4.0 (c (n "e310x-hal") (v "0.4.0") (d (list (d (n "e310x") (r "^0.4.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "09igxanjp5f16kdv1m323ch54bv7kwigg1hy0xw9hmf1xdnqz4wv")))

(define-public crate-e310x-hal-0.5.0 (c (n "e310x-hal") (v "0.5.0") (d (list (d (n "e310x") (r "^0.5.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)))) (h "1sf4761x5yg56l52pz87mkc0bzqgkk2kad4nd2lbl496gvfxwqhy") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.5.1 (c (n "e310x-hal") (v "0.5.1") (d (list (d (n "e310x") (r "^0.5.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)))) (h "0v1h6d10dnr2aa9g6r1130c9cr0pyv06iniib6d9qva20c63gm4r") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.6.0 (c (n "e310x-hal") (v "0.6.0") (d (list (d (n "e310x") (r "^0.5.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "riscv") (r "^0.5.0") (d #t) (k 0)))) (h "0982w8fzsiq55qxmb3vv5vzb8gxb852jy41zma3fcnz66fda9qgf") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.6.1 (c (n "e310x-hal") (v "0.6.1") (d (list (d (n "e310x") (r "^0.5.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.2") (d #t) (k 0)))) (h "1wnwf00pysfkb0m8q07av35hrchhaqvpr7ams3r9zlw974396jyn") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.7.0 (c (n "e310x-hal") (v "0.7.0") (d (list (d (n "e310x") (r "^0.5.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.2") (d #t) (k 0)))) (h "01vxy0q0h0q83963pjaigah8fqyz5d1j3ma1n86wbpw3x03s7154") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.8.0 (c (n "e310x-hal") (v "0.8.0") (d (list (d (n "e310x") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)))) (h "0jvzjica4y0awhg0x3rrlmg09xsm4vp4zkz7lm265nqd06jiz6c2") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.8.1 (c (n "e310x-hal") (v "0.8.1") (d (list (d (n "e310x") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)))) (h "0fkn0zmywbvk8crfnla72pn2afz6q5pj8z0n171xr1nzx99yksz4") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.8.2 (c (n "e310x-hal") (v "0.8.2") (d (list (d (n "e310x") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)))) (h "0fjxwp59hla24rkcq4w1nqpwjpp79xsg64mb0h5gpjzm7lkijd6g") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.8.3 (c (n "e310x-hal") (v "0.8.3") (d (list (d (n "e310x") (r "^0.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)))) (h "1fx5nv8r7yd9np9bxg50qnyc0gb43rhsjha6wq8ra4fjhddnimmi") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.9.0 (c (n "e310x-hal") (v "0.9.0") (d (list (d (n "e310x") (r "^0.9.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "0x8mjgkmlz82dqr0h7wfdx1acpgcxypq5gj2b2l4bg42hkf1y2rp") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.9.1 (c (n "e310x-hal") (v "0.9.1") (d (list (d (n "e310x") (r "^0.9.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "1bfp06spv4i0zwjl1v0s9m7vas209m2jlgsrd5zaiwkf57kgklhz") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.9.2 (c (n "e310x-hal") (v "0.9.2") (d (list (d (n "e310x") (r "^0.9.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "1z34wbhcjqicrxzv4g70qmznj4qdpa2x2va62bdqwihlb3xslh1k") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.9.3 (c (n "e310x-hal") (v "0.9.3") (d (list (d (n "e310x") (r "^0.9.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "0c90w8ls6w7d4xycynd5bi6glardi25y2hblh8xcnkbbzvzdnqdj") (f (quote (("g002" "e310x/g002"))))))

(define-public crate-e310x-hal-0.9.4 (c (n "e310x-hal") (v "0.9.4") (d (list (d (n "e310x") (r "^0.9.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)))) (h "0chi69qdqx0q4f5igb0cdbjp87k8rzswscfwk3xqvwf46ygshm69") (f (quote (("g002" "e310x/g002")))) (y #t)))

(define-public crate-e310x-hal-0.11.0 (c (n "e310x-hal") (v "0.11.0") (d (list (d (n "e310x") (r "^0.11.0") (f (quote ("rt" "critical-section"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (f (quote ("critical-section-single-hart"))) (d #t) (k 0)))) (h "1k5vc1byz1kwxqj2nj1jmbi1vjcykxp9qksimysa5vwfvpamndh2") (f (quote (("virq") ("g002" "e310x/g002")))) (r "1.59")))

