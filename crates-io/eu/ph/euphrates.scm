(define-module (crates-io eu ph euphrates) #:use-module (crates-io))

(define-public crate-euphrates-0.1.0 (c (n "euphrates") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0s77zhl8ca4iqdzzbjq804yhcn3pch5gnjppd27ni1qrpblrs5v0")))

