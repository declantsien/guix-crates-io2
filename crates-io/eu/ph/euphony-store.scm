(define-module (crates-io eu ph euphony-store) #:use-module (crates-io))

(define-public crate-euphony-store-0.1.1 (c (n "euphony-store") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "euphony-compiler") (r "^0.1") (d #t) (k 0)) (d (n "euphony-mix") (r "^0.1") (d #t) (k 0)) (d (n "euphony-node") (r "^0.1") (d #t) (k 0)) (d (n "euphony-units") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0w7dvbi5m24q2rqfnqq7b63zb50ijbziz5lg1bvsdscymzh1h6zn") (f (quote (("default"))))))

