(define-module (crates-io eu ph euphony-graph) #:use-module (crates-io))

(define-public crate-euphony-graph-0.1.0 (c (n "euphony-graph") (v "0.1.0") (d (list (d (n "bolero") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "slotmap") (r "^1") (k 0)))) (h "0j4a6iwlxllfaikh6596083gf1zif3znssmnk9dkw76d0r118664")))

(define-public crate-euphony-graph-0.1.1 (c (n "euphony-graph") (v "0.1.1") (d (list (d (n "bolero") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "slotmap") (r "^1") (k 0)))) (h "1023i2vx0l4103xi93szrgfgb3a2adnmch088g3brisfj9y54qss")))

