(define-module (crates-io eu ph euphrates_cli) #:use-module (crates-io))

(define-public crate-euphrates_cli-0.1.0 (c (n "euphrates_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "euphrates") (r "^0.1.0") (d #t) (k 0)) (d (n "euphrates_sdl2") (r "^0.1.0") (d #t) (k 0)) (d (n "euphrates_virtual_memory") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "euphrates_x64") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "13zwj9k5iq36f4x5v1c3qrrm75jvwmxvwa3mg71wffncybbpqmiw") (f (quote (("state_memory"))))))

