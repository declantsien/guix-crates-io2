(define-module (crates-io eu ph euphony-node) #:use-module (crates-io))

(define-public crate-euphony-node-0.1.0 (c (n "euphony-node") (v "0.1.0") (d (list (d (n "euphony-graph") (r "^0.1") (d #t) (k 0)) (d (n "euphony-macros") (r "^0.2") (d #t) (k 0)) (d (n "euphony-units") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0fzd7hvkdpgjnnkzah6n0hi8pgfkc159mn5lpis2a4yrm95gv4nl") (f (quote (("reflect" "heck" "serde" "serde_json"))))))

(define-public crate-euphony-node-0.1.1 (c (n "euphony-node") (v "0.1.1") (d (list (d (n "euphony-graph") (r "^0.1") (d #t) (k 0)) (d (n "euphony-macros") (r "^0.2") (d #t) (k 0)) (d (n "euphony-units") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0g0nz3bmkpxjsxj87vk2jv6f83w2m7xqxhqnsqfif3v8pbmqbnm1") (f (quote (("reflect" "heck" "serde" "serde_json"))))))

