(define-module (crates-io eu ph euphony-samples) #:use-module (crates-io))

(define-public crate-euphony-samples-0.1.0 (c (n "euphony-samples") (v "0.1.0") (d (list (d (n "blake3") (r "^1") (d #t) (k 2)) (d (n "euphony-buffer") (r "^0.1") (k 0)) (d (n "euphony-buffer") (r "^0.1") (f (quote ("resample"))) (d #t) (k 2)) (d (n "hound") (r "^3") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "rubato") (r "^0.12") (d #t) (k 2)))) (h "0f90xw4c25z9z0kh0v4yq7is4a126a0rpzbhgj5zji9pj1a72vbs")))

(define-public crate-euphony-samples-0.1.1 (c (n "euphony-samples") (v "0.1.1") (d (list (d (n "blake3") (r "^1") (d #t) (k 2)) (d (n "euphony-buffer") (r "^0.1") (k 0)) (d (n "euphony-buffer") (r "^0.1") (f (quote ("resample"))) (d #t) (k 2)) (d (n "hound") (r "^3") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "rubato") (r "^0.12") (d #t) (k 2)))) (h "1bwir02hxbzm8wsgyaw8qsn5g27nzhq7w4q6qnj2l2ldzg0kfdha")))

