(define-module (crates-io eu ph euphrates_virtual_memory) #:use-module (crates-io))

(define-public crate-euphrates_virtual_memory-0.1.0 (c (n "euphrates_virtual_memory") (v "0.1.0") (d (list (d (n "euphrates") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1z0l7rcsw5sj8cvjr7mqbnh0p27115b465iq6mn9p2sbvs3cswzc")))

