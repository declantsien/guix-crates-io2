(define-module (crates-io eu ph euphony-macros) #:use-module (crates-io))

(define-public crate-euphony-macros-0.1.0 (c (n "euphony-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r07n8ff8xiv0rjghx4scc4c6a5lgn0swmqzbm7s3jmvyr5rd3b1")))

(define-public crate-euphony-macros-0.2.0 (c (n "euphony-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19dk0690ddhx9fyy8wn9aiigsaxwm8ak58r6ayp5bwifsz0x4j2c")))

(define-public crate-euphony-macros-0.2.1 (c (n "euphony-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8q83c5d6vzi54h8a6l0zda1qacfl711z63dijsphg0jps9v3f6")))

