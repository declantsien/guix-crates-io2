(define-module (crates-io eu ph euphony-core) #:use-module (crates-io))

(define-public crate-euphony-core-0.1.0 (c (n "euphony-core") (v "0.1.0") (d (list (d (n "euphony-core-macros") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-rational") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "18q2mh3gkamlnsnlrf2nxzakhhgsc609k5ikhfrwxdcbm5zwn0kj")))

