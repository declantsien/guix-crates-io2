(define-module (crates-io eu ph euphony-core-macros) #:use-module (crates-io))

(define-public crate-euphony-core-macros-0.1.0 (c (n "euphony-core-macros") (v "0.1.0") (d (list (d (n "num-rational") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18gyv3dpxmprxxlbghcqjm34mijyxsf8rf32pc49rqycn4sqpmrx")))

