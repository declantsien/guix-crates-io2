(define-module (crates-io eu ph euphrates_sdl2) #:use-module (crates-io))

(define-public crate-euphrates_sdl2-0.1.0 (c (n "euphrates_sdl2") (v "0.1.0") (d (list (d (n "euphrates") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1x2vyc5f7b9109qv5frk93svgcaf8dwhdigir9wjxp8vr9vdksa4")))

