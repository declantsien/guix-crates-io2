(define-module (crates-io eu ph euphoria) #:use-module (crates-io))

(define-public crate-euphoria-0.1.0 (c (n "euphoria") (v "0.1.0") (h "1pg16bfma1qymqnf7fmq2r5yvd2g5l29rsfi8p40x8ibl56kcnyr")))

(define-public crate-euphoria-0.1.1 (c (n "euphoria") (v "0.1.1") (h "0licppq0qmqdm66w6wsav3z0p0xjkmz1ls8ys371sf0205dip2jd")))

