(define-module (crates-io eu ph euphony-command) #:use-module (crates-io))

(define-public crate-euphony-command-0.1.0 (c (n "euphony-command") (v "0.1.0") (d (list (d (n "bolero") (r "^0.6") (d #t) (k 2)))) (h "1ma74lfny08gnimhjs4qsmx7cnijgggdgx8192drjdc17yvq9sbi")))

(define-public crate-euphony-command-0.1.1 (c (n "euphony-command") (v "0.1.1") (d (list (d (n "bach") (r "^0.0.6") (d #t) (k 0)) (d (n "bolero") (r "^0.7") (d #t) (k 2)))) (h "1yj40isyvk0gm5fjfwk8z12mgv37dkfj7yh1zac89w6imwsfmxnh")))

