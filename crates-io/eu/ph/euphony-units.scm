(define-module (crates-io eu ph euphony-units) #:use-module (crates-io))

(define-public crate-euphony-units-0.1.0 (c (n "euphony-units") (v "0.1.0") (d (list (d (n "euphony-macros") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-rational") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0b89573y56xrjmvfmcpi9ivag9039z44l1vbwgbviiakimm7pr1a") (f (quote (("std"))))))

(define-public crate-euphony-units-0.1.1 (c (n "euphony-units") (v "0.1.1") (d (list (d (n "euphony-macros") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-rational") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11cmh56vapinqjzd3ms2nw9wn0xbxv1zfbbgh0bhc6gzfwy018qw") (f (quote (("std"))))))

