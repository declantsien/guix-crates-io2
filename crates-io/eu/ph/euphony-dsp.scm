(define-module (crates-io eu ph euphony-dsp) #:use-module (crates-io))

(define-public crate-euphony-dsp-0.1.1 (c (n "euphony-dsp") (v "0.1.1") (d (list (d (n "dasp_sample") (r "^0.11") (d #t) (k 0)) (d (n "euphony-graph") (r "^0.1") (d #t) (k 0)) (d (n "euphony-node") (r "^0.1") (d #t) (k 0)) (d (n "euphony-units") (r "^0.1") (d #t) (k 0)) (d (n "fastapprox") (r "^0.3") (d #t) (k 0)) (d (n "fundsp") (r "^0.9") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)))) (h "1y810v53jggdanyr08dg96j2ql2ign4f80da2v07xnl1gf8xjbmm")))

