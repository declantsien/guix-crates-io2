(define-module (crates-io eu c_ euc_lib) #:use-module (crates-io))

(define-public crate-euc_lib-0.1.0 (c (n "euc_lib") (v "0.1.0") (h "0i2hf2l7bav3zwj98jq1zp5zc4dixvh27gcw8n46460dk4nhm7rf") (y #t)))

(define-public crate-euc_lib-0.1.1 (c (n "euc_lib") (v "0.1.1") (h "042wma9kg0cjvf0rrss65lfgnw0rn3z7p86b0hwf63bz00ahqqm8") (y #t)))

(define-public crate-euc_lib-0.1.2 (c (n "euc_lib") (v "0.1.2") (h "0rchw0irnbp7afqasymhbnkx6rp9w4p83ccfsx3w7vf72szp1m3p") (y #t)))

(define-public crate-euc_lib-0.1.3 (c (n "euc_lib") (v "0.1.3") (h "098ggxkqqgqvl3kji0h704zjrskly986m2rmvwpwj82qqdhdnki5") (y #t)))

(define-public crate-euc_lib-0.1.4 (c (n "euc_lib") (v "0.1.4") (h "1qcpmxizl4fd08jzxwbrm1b30mspvlijmk85bhqymhpx0zp8wfx0") (y #t)))

(define-public crate-euc_lib-0.1.5 (c (n "euc_lib") (v "0.1.5") (h "1ma2wq2qsl2xjwy4vkb6sakn9cz705dpcpb6sr6050m4p4aj5iy0") (y #t)))

(define-public crate-euc_lib-0.1.6 (c (n "euc_lib") (v "0.1.6") (h "0jdc15b6kynz59q2i7yxl6xhfbdjvmz4pqz8fc1fh0b9svpffkjx")))

(define-public crate-euc_lib-0.1.7-rc1 (c (n "euc_lib") (v "0.1.7-rc1") (h "0npg175s8llmwp56g1i5crrr7vx655rwvcffba3g7mr2lw924lmn") (y #t)))

(define-public crate-euc_lib-0.1.7-rc2 (c (n "euc_lib") (v "0.1.7-rc2") (h "1i25syn52cbbx3npvb1k3zgyvnx6aql99aj333fa3vdz6achpi9s") (y #t)))

(define-public crate-euc_lib-0.1.7 (c (n "euc_lib") (v "0.1.7") (h "13ziij2h73vzki9km80j8q8qa0ag4dm0qain81gkw414fbsrwgaa") (y #t)))

(define-public crate-euc_lib-0.1.8-rc1 (c (n "euc_lib") (v "0.1.8-rc1") (h "1mgsp182ac103ribjjhpp5w14hdmbvd9gn660fz9mhg851ra30zv") (y #t)))

(define-public crate-euc_lib-0.1.8 (c (n "euc_lib") (v "0.1.8") (h "1q893vc969sf7yp9lad80p7pvrbvm1j3i0yx0a09qp062lmh3x2b") (y #t)))

(define-public crate-euc_lib-0.1.9 (c (n "euc_lib") (v "0.1.9") (h "0mqv0lscd3r570s1izm4z8w0sgprlhpf7da5f708axjm37w5s02g")))

(define-public crate-euc_lib-0.2.1 (c (n "euc_lib") (v "0.2.1") (h "0496j5bdwnayzfwwj1br3pwn9azmpaza7l8hgjih4khsjdcwkk6v") (y #t)))

(define-public crate-euc_lib-0.2.2 (c (n "euc_lib") (v "0.2.2") (h "1fhhiwjaywqqgbiz39rfaa5xr5w9q0a90f2b4p7d42m2pa4slkyi") (y #t)))

(define-public crate-euc_lib-0.2.3 (c (n "euc_lib") (v "0.2.3") (h "1yaf6w2ivkqck834fhw8xv7w4816k1l40hrqlrxcc3h81a1436h7") (y #t)))

(define-public crate-euc_lib-0.2.4 (c (n "euc_lib") (v "0.2.4") (h "0a33wh1qy03rjym0rxkblnmk6vi1qrcc7yvd91qp47awkkcf0096") (y #t)))

(define-public crate-euc_lib-0.2.5 (c (n "euc_lib") (v "0.2.5") (h "0iw1gy5ffqsj4w457z285z275xj8q77f139jk2lnchzs5b0is6l6") (y #t)))

(define-public crate-euc_lib-0.2.6 (c (n "euc_lib") (v "0.2.6") (h "1r11q8gdvwjal000vqxkp0q1y65r6vqwcydxi4ni1nhbyqvm0k25")))

(define-public crate-euc_lib-0.3.0 (c (n "euc_lib") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1d0jmhl6lkh56w64nmg01dzv5p10ab6a02kzmglk7k0qn5sba3qg") (y #t)))

(define-public crate-euc_lib-0.3.1 (c (n "euc_lib") (v "0.3.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1irxbvbwd4bywlyxvfsaa6iyrhq70yd6gkr3ac26pn176yl6ichs") (y #t)))

(define-public crate-euc_lib-0.3.2 (c (n "euc_lib") (v "0.3.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "04vai89y49pv1rpqbphyd4z38j66scdfxb9nllsb25lsh07vgvkp")))

(define-public crate-euc_lib-0.4.0 (c (n "euc_lib") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "061xrdwwpny90ikllcr3ifk5vm7sa1xc2wjjl7gr597sh6nhyj1w")))

