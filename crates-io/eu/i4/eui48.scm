(define-module (crates-io eu i4 eui48) #:use-module (crates-io))

(define-public crate-eui48-0.1.0 (c (n "eui48") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1ayw4vpkjg974bqbixhc8l1fcdd473hc01vsjm5mwg69nb2y48km")))

(define-public crate-eui48-0.3.0 (c (n "eui48") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "~1") (o #t) (d #t) (k 0)))) (h "1ng06z6v5zj3bh905v082n5kdr5y3civsy2dvcxx4p3n8mkxxf62")))

(define-public crate-eui48-0.3.1 (c (n "eui48") (v "0.3.1") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "015wn3hsf9ng85vgja4vagplkdspkh1sigcw86a5wzaqwv7fwf9j")))

(define-public crate-eui48-0.3.2 (c (n "eui48") (v "0.3.2") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "0mmdhczfdxwv5v5h90ydqkx0mdqiv0h2clshm2cm4qlwp0gacw29")))

(define-public crate-eui48-0.4.0 (c (n "eui48") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "0xp44gnx80zcb6dz4msrjgzwmix9fyxl1vkvahncpwjdfj6h2lax")))

(define-public crate-eui48-0.4.1 (c (n "eui48") (v "0.4.1") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "13hillaards606s515yhmbcq2fkprrswdnif0zdsl5nw2fqsv7b4")))

(define-public crate-eui48-0.4.2 (c (n "eui48") (v "0.4.2") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "0sx8pwvbwvgmi989wpczx3mw9fk0f13z1a14hmgnr14g2x6k5z1z")))

(define-public crate-eui48-0.4.3 (c (n "eui48") (v "0.4.3") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "0kg6hlxmcj7j1afkax6a5c9ma3phniplhscasqfmwxjp38ajgq06")))

(define-public crate-eui48-0.4.4 (c (n "eui48") (v "0.4.4") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "0vryczmj5irhc2lin4glnhrygkvddyzajl308vkxvfs2ln1yqp5x")))

(define-public crate-eui48-0.4.5 (c (n "eui48") (v "0.4.5") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (o #t) (d #t) (k 0)))) (h "1qb32nxrmjx84878yp887rn9b8ny35gfnd29zl11lch6h49q39c1")))

(define-public crate-eui48-0.4.6 (c (n "eui48") (v "0.4.6") (d (list (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (o #t) (d #t) (k 0)))) (h "0sqbmcnvilanzjagknmpf85pnji2b9hn2pqzd5rygrfkwikghk4c") (f (quote (("disp_hexstring") ("default"))))))

(define-public crate-eui48-0.5.0 (c (n "eui48") (v "0.5.0") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (o #t) (d #t) (k 0)))) (h "1pdrcqz5917dy7rq2i4i6d42m5dk3rfhb5b0mfq7yvkhljirw8wx") (f (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1.0.0 (c (n "eui48") (v "1.0.0") (d (list (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (o #t) (d #t) (k 0)))) (h "1xqbs1jl17zv1xc9w9393rywncpyy66vlrwdpzzm7875m47b3cp3") (f (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1.0.1 (c (n "eui48") (v "1.0.1") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "0lliq2ssnj9l8x6whl6rmdr5y3gw1l9kzqy2r5ba58qayd7j28x9") (f (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-0.5.1 (c (n "eui48") (v "0.5.1") (d (list (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "19yfy6yphbvlh70qjclf68nrsi9yf5w407i5612mh44davrpkn5f") (f (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1.1.0 (c (n "eui48") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "00cpf25kc3mxhqnahm0bw9xl19gr2pzc5g84dvkc4mwdbsn1hx48") (f (quote (("serde_bytes" "serde") ("disp_hexstring") ("default" "rustc-serialize"))))))

