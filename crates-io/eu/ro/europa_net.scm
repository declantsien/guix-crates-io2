(define-module (crates-io eu ro europa_net) #:use-module (crates-io))

(define-public crate-europa_net-0.1.0 (c (n "europa_net") (v "0.1.0") (h "00n0ig9z70a02iaax8rnp7cx7hj3mg9622gh935ci8r07xaxwplx")))

(define-public crate-europa_net-0.1.1 (c (n "europa_net") (v "0.1.1") (h "1bf7hf3kqw7xvy5w8iv7r9qk8p8nw26g5x8hca7p7x5q7sxawl44")))

