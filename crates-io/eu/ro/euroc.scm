(define-module (crates-io eu ro euroc) #:use-module (crates-io))

(define-public crate-euroc-0.1.0 (c (n "euroc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1rn8d8kw5fi5qs4n2mwkd40qq4jqlz2rb1byvpk4lcxz8s9zj3pa")))

