(define-module (crates-io eu ro europe-elects-csv) #:use-module (crates-io))

(define-public crate-europe-elects-csv-0.0.1 (c (n "europe-elects-csv") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0fpk4gsrl4qy7l1w3lch6rnmy48rpfjvlf7jalqjkxy25k0v1h1k")))

