(define-module (crates-io eu ro eurorack-oxide-utils) #:use-module (crates-io))

(define-public crate-eurorack-oxide-utils-0.1.0 (c (n "eurorack-oxide-utils") (v "0.1.0") (h "16ycdxwgg5g8pkwfv15vf29ncn0x5mbshx31imv2sf97i51a4z45")))

(define-public crate-eurorack-oxide-utils-0.1.1 (c (n "eurorack-oxide-utils") (v "0.1.1") (h "1hkjqd65d9hbgnxhwwjcfnk1m907pamdlifz9qap7smxzl04xzai")))

(define-public crate-eurorack-oxide-utils-0.1.2 (c (n "eurorack-oxide-utils") (v "0.1.2") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)))) (h "0g743bjqv2wk3agaj2pd3xrvb2zb189sh93ngq7cxzi74ikf43bs")))

(define-public crate-eurorack-oxide-utils-0.1.3 (c (n "eurorack-oxide-utils") (v "0.1.3") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)))) (h "1jdghir1rqdagnp5w9z8d5s9bbhcpkh3dj2y08pzjw63m20bf12b")))

(define-public crate-eurorack-oxide-utils-0.1.4 (c (n "eurorack-oxide-utils") (v "0.1.4") (d (list (d (n "micromath") (r "^1.0.0") (d #t) (k 0)))) (h "0lm4lbzj7lqqyk7hp1644n68nvzcyw6dijlc4439q0cmn3ly3rk1")))

