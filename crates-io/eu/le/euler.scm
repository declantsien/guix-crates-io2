(define-module (crates-io eu le euler) #:use-module (crates-io))

(define-public crate-euler-0.1.0 (c (n "euler") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.4.2") (d #t) (k 0)))) (h "06liym923jp0ly2ip0yc1is9n246pc5cwlzhxinf2fqmz0gnwrpr")))

(define-public crate-euler-0.2.0 (c (n "euler") (v "0.2.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15.0") (d #t) (k 0)) (d (n "mint") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0qngpzk8vqyc6rs1hx67kjyc7bdpnr9kb6d21mr8x506fw7ga98d") (f (quote (("default" "cgmath/mint"))))))

(define-public crate-euler-0.2.1 (c (n "euler") (v "0.2.1") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15.0") (d #t) (k 0)) (d (n "mint") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0n2j2i6nj42087sggkwaxd9ym8v65rc14yg61z9pipiamc9jjns8") (f (quote (("default" "cgmath/mint"))))))

(define-public crate-euler-0.3.0 (c (n "euler") (v "0.3.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15.0") (d #t) (k 0)) (d (n "mint") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1ngcvypz9vfyyix2frzy5xr4xklmf23kd1ipadlh5rpfd251nigc") (f (quote (("default" "cgmath/mint"))))))

(define-public crate-euler-0.4.0 (c (n "euler") (v "0.4.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "14zmc3pn2r2viiayq0gnahi6rhmyf8gsqfbdawkcb5l3y1rcxvpg") (f (quote (("default" "cgmath/mint"))))))

(define-public crate-euler-0.4.1 (c (n "euler") (v "0.4.1") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1h8b5jlcxpgaph5am4b0zly2qpiyjsib7nlb93p6m954d0ax269z") (f (quote (("default" "cgmath/mint"))))))

