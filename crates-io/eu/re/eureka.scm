(define-module (crates-io eu re eureka) #:use-module (crates-io))

(define-public crate-eureka-1.0.0 (c (n "eureka") (v "1.0.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~0.8") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "1ba6k5jpyp6wr6zps4xl6p7fvfr2h6rc5skchnmj29wfmhwv5sfa") (f (quote (("default"))))))

(define-public crate-eureka-1.1.0 (c (n "eureka") (v "1.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~0.8") (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "0vcbjzrfd2rbdab3n854w2273is4jjzzz9l6pimjsvxhqsdx4igr") (f (quote (("default"))))))

(define-public crate-eureka-1.2.0 (c (n "eureka") (v "1.2.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "1chl8k0pqnacksziah3p8c2h74r983lqksavfbl502646qii2f4p") (f (quote (("default"))))))

(define-public crate-eureka-1.3.0 (c (n "eureka") (v "1.3.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "0yhc09sv86hbn7hmgxj3y6kxwrrdvw4gpvz7qvjh21m4k5ajfdm0") (f (quote (("default"))))))

(define-public crate-eureka-1.4.0 (c (n "eureka") (v "1.4.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "04swz4k03qqdm70d7gxmvnkkcvya9v32dgagpd26zyzwdx4fznsv") (f (quote (("default"))))))

(define-public crate-eureka-1.5.0 (c (n "eureka") (v "1.5.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "1x4a2qbam28bbp7hymrwgdfk9qhywwbqihdbby901l40ha9svmnk") (f (quote (("default"))))))

(define-public crate-eureka-1.5.1 (c (n "eureka") (v "1.5.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "12470zskw453l0nhxggfxafaylfglfvriai6ai981pwfaj5wqaml") (f (quote (("default"))))))

(define-public crate-eureka-1.5.2 (c (n "eureka") (v "1.5.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "text_io") (r "~0.1.5") (d #t) (k 0)))) (h "18hzcbhymqvcp91ghwzs9m4bml6izgz0b3v9k3x00h9h089h9cwq") (f (quote (("default"))))))

(define-public crate-eureka-1.6.0 (c (n "eureka") (v "1.6.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1dwpkxn7n8jblmy4kcj8li4pn89kqmb31yf9x1ds0fyf7jxlyyqz") (f (quote (("default"))))))

(define-public crate-eureka-1.6.1 (c (n "eureka") (v "1.6.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "15iksn9ynik98yjqakkfl3p9wpaqn2xdfbgy7xzc02xf33xc8yr5") (f (quote (("default"))))))

(define-public crate-eureka-1.6.2 (c (n "eureka") (v "1.6.2") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)) (d (n "which") (r "^2.0.1") (d #t) (k 0)))) (h "1dajf4maavfi53mmdy24lwl60ks9agvznhrgm7a70acxvvm8868s") (f (quote (("default"))))))

(define-public crate-eureka-1.6.3 (c (n "eureka") (v "1.6.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0.0") (d #t) (k 0)))) (h "1lf70f2b8pla5xqz163an1wpnrc4idz0n9s77zs2wlxb4mgb3ihl")))

(define-public crate-eureka-1.7.0 (c (n "eureka") (v "1.7.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0.0") (d #t) (k 0)))) (h "1yxinich5p3c36h8dd977b1y71grxp403h2agk4n6pxdwbd2664i")))

(define-public crate-eureka-1.8.0 (c (n "eureka") (v "1.8.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "0bcg5jgfa48910ik0zijgvzkf10pahakyd6sjgs2qnlwjj48pw79")))

(define-public crate-eureka-1.8.1 (c (n "eureka") (v "1.8.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "1hbjm69af9y8a8vsch9xvkxdlgjckh6nl3zv92g8v4z0jnbv0y46")))

(define-public crate-eureka-2.0.0 (c (n "eureka") (v "2.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0l5fkg80zalkik1kwklmzzm6fg54am8i1ndxrjj2dw38x885qxn3")))

(define-public crate-eureka-2.0.1 (c (n "eureka") (v "2.0.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("std" "cargo"))) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (f (quote ("default"))) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1s1ky974c0q519l36h9h8py5kzwxzdms0qqq4ly5flcy3zzr2wzy") (y #t)))

