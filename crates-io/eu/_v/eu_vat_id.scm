(define-module (crates-io eu _v eu_vat_id) #:use-module (crates-io))

(define-public crate-eu_vat_id-0.1.0 (c (n "eu_vat_id") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.11") (d #t) (k 0)))) (h "1d51yh3ps596cmkcvcibybyd5i0lk9ab36psw8jhrfz826k1ajgl")))

