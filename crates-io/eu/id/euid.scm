(define-module (crates-io eu id euid) #:use-module (crates-io))

(define-public crate-euid-0.1.0 (c (n "euid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1radgpzxbrgi8mnq6hrjzw8a2jqlpsysrcdpyh19v4jq8ykqf0cx") (f (quote (("default" "checkmod_128") ("checkmod_64") ("checkmod_128"))))))

(define-public crate-euid-0.1.1 (c (n "euid") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pzzfvpcihy17yv1nr2j3vvd2gmwbbvssc9fmrxaf6hrh3jnnhax") (f (quote (("std") ("default" "std"))))))

(define-public crate-euid-0.1.2 (c (n "euid") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1avsq13908fr1qyf0iywp0y2l8qvxlv8xfzfa39qvakn1j1q0f6s") (f (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1.3 (c (n "euid") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c3p4ymb066d1bw44ba72kzxla0kys0k500kq5fyi6y41di64i36") (f (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1.4 (c (n "euid") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0z8qbkn6swrs2kvq70sj0k1l3ymjnkqj67w51hcbwvmarpn6c3zq") (f (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1.5 (c (n "euid") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p43m900qpp05zx5p0zpaq0ndgh2j4gaw9d4xm2xc7dmhmjifzq9") (f (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1.6 (c (n "euid") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17v4588qc3rxnm0pyhd7mjvcdgkdgb9hp56891c7mg29vndva71c") (f (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1.7 (c (n "euid") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "036jkla0h7gynlyay6s25qx4hsqz3r98vy4kjhj1cfkpc81q0ljm") (f (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

