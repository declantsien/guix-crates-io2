(define-module (crates-io eu id euid-toys) #:use-module (crates-io))

(define-public crate-euid-toys-0.1.0 (c (n "euid-toys") (v "0.1.0") (d (list (d (n "euid") (r "^0.1.0") (d #t) (k 0)))) (h "0kyl36kyk8081fhj4pv1xl1ryh9ihckx8fpcii2vzvh6zrf5hq0j")))

(define-public crate-euid-toys-0.1.3 (c (n "euid-toys") (v "0.1.3") (d (list (d (n "euid") (r "^0.1.3") (d #t) (k 0)))) (h "0rzkmb4psg06byd9fq6gd2dy7rnfnpsq1dcl64316msx5q9v7hq6")))

(define-public crate-euid-toys-0.1.5 (c (n "euid-toys") (v "0.1.5") (d (list (d (n "euid") (r "^0.1.5") (d #t) (k 0)))) (h "1mlh9qhdsklbm1w53zxihs99bc9kfh2p6a66sqf0xq3a4ndbbndm")))

(define-public crate-euid-toys-0.1.6 (c (n "euid-toys") (v "0.1.6") (d (list (d (n "euid") (r "^0.1.6") (d #t) (k 0)))) (h "00vkl20llnkf2s3s3w1gp6gk12qkayv726mwgdyf342am0xiln1d")))

(define-public crate-euid-toys-0.1.7 (c (n "euid-toys") (v "0.1.7") (d (list (d (n "euid") (r "^0.1.7") (d #t) (k 0)))) (h "128l1l0h1rp44s56bwjg5kw354m3sg9w8nm6mql15wh4n961hmvf")))

