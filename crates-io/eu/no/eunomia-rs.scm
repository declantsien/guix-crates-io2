(define-module (crates-io eu no eunomia-rs) #:use-module (crates-io))

(define-public crate-eunomia-rs-0.1.0 (c (n "eunomia-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0") (d #t) (k 0)))) (h "0vzmp1k9rzz8qdzpfwhxiziarixcwb7344cylhkbhy1qfic2a9zk")))

(define-public crate-eunomia-rs-0.1.1 (c (n "eunomia-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.6.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1qvcxfcwpxh66ap5193r89n27s60kyr0v84ji136qkh281xcqhjk")))

