(define-module (crates-io eu cl euclidean_algo) #:use-module (crates-io))

(define-public crate-euclidean_algo-0.2.0 (c (n "euclidean_algo") (v "0.2.0") (h "087xzlanw7nik8nsz7vg5ailri34kdi5ciz3d6y7rbw5rrpb6mmk")))

(define-public crate-euclidean_algo-0.2.1 (c (n "euclidean_algo") (v "0.2.1") (h "0w75q5m6n8rmv0rnijc6q0inrd28jgxay2b4z194f49yzxkh1qi0")))

(define-public crate-euclidean_algo-0.3.0 (c (n "euclidean_algo") (v "0.3.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "16kpc18i8yhxrfm6mr70kchrh1xz22c3y3jm4anjfh0gh5qxwc9g")))

(define-public crate-euclidean_algo-0.3.1 (c (n "euclidean_algo") (v "0.3.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1li921rcpypz08l40bglw2adhvrgvflx45lhfcis7dji6m2a90ff")))

