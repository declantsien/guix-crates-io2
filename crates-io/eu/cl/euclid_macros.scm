(define-module (crates-io eu cl euclid_macros) #:use-module (crates-io))

(define-public crate-euclid_macros-0.1.0 (c (n "euclid_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("visit"))) (d #t) (k 0)))) (h "05dy60mxw2yg26m1ssqd5v7an0wly97rn0r3b8f7l0x5iv0q9jzx")))

