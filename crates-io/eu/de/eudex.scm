(define-module (crates-io eu de eudex) #:use-module (crates-io))

(define-public crate-eudex-0.1.0 (c (n "eudex") (v "0.1.0") (h "06nfzh7kyc3fz662znzd620zzim5sldm76zak4ag4bnr70niffb1")))

(define-public crate-eudex-0.1.1 (c (n "eudex") (v "0.1.1") (h "11xcx6q242viciaw96gw8jhg548lpsbm3pdfh9b28rlkjvq9pjwm")))

