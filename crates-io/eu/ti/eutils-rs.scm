(define-module (crates-io eu ti eutils-rs) #:use-module (crates-io))

(define-public crate-eutils-rs-0.1.0 (c (n "eutils-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s5yv59xl8636s8jag066x52rzz2q9kax49wmhm7iv2rpv33q09c")))

(define-public crate-eutils-rs-0.1.1 (c (n "eutils-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13sx3x6zhc3pq0cyq5yppwrbjancpai77pmbc8bhnsy1i4mibcmk")))

(define-public crate-eutils-rs-0.1.2 (c (n "eutils-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05rl19f303126g6bj77079136svd6218wr70vnx0lj406mnffdmj")))

(define-public crate-eutils-rs-0.1.3 (c (n "eutils-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1xb02xqi1zig547xkicmy5aqhmgaikab3m4vmx8rzm7fhh7bxgyj")))

(define-public crate-eutils-rs-0.1.4 (c (n "eutils-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1rkdvhzrzr6pijizw0lq1a85pkl6hfndifvzkis5598kxy8bgfsk")))

(define-public crate-eutils-rs-0.1.5 (c (n "eutils-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "093k60bvyj7q2z6bf2yl545w5246p6d55j98an4iffrgj80r9zav")))

(define-public crate-eutils-rs-0.1.6 (c (n "eutils-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0qlam5lp3gv6fg3af6q8g9wgk1xvyn86bmp6q66wd1z4f265341m")))

