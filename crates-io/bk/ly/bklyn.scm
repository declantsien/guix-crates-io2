(define-module (crates-io bk ly bklyn) #:use-module (crates-io))

(define-public crate-bklyn-0.1.0 (c (n "bklyn") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.30") (d #t) (k 1)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "17j4jq59pfbsmc2zdv95a2gnsa8wvza186447vgxl00b2lhrpbds")))

(define-public crate-bklyn-0.1.1 (c (n "bklyn") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.32") (d #t) (k 1)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "11x9392mhh5bw5lsf70vxwwrz2ws2w7cwb6kgrjbj7fy0nm6avl7")))

