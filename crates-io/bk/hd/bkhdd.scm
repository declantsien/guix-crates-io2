(define-module (crates-io bk hd bkhdd) #:use-module (crates-io))

(define-public crate-bkhdd-0.1.0 (c (n "bkhdd") (v "0.1.0") (h "1nf5073ndaf20lf0fxzfrmk0nnwmmwx2w5wincfarm7kzkng6966")))

(define-public crate-bkhdd-0.1.1 (c (n "bkhdd") (v "0.1.1") (h "0nczy809kcrmyzaa59k85i8p34shawhi1r7k0a7h6nmf8i3qh2kx")))

(define-public crate-bkhdd-0.1.2 (c (n "bkhdd") (v "0.1.2") (h "0cv7vrfx41nwmw5mij3nz4m92bizp4g1hnsmc65an9s7vf4xsql0")))

(define-public crate-bkhdd-0.2.4 (c (n "bkhdd") (v "0.2.4") (d (list (d (n "binrw") (r "^0.8.0") (d #t) (k 0)) (d (n "byteordered") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "fuser") (r "^0.9.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.107") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0d8b09gqk8nxc5abr48bl67wry5dkm8k313nmzqx4yz7l99zny4f")))

(define-public crate-bkhdd-0.2.5 (c (n "bkhdd") (v "0.2.5") (d (list (d (n "binrw") (r "^0.9.2") (d #t) (k 0)) (d (n "byteordered") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "fuser") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0ddh7131hcr2h3lfx2c3hfyschq292bph5izyssgdjl8c2vc8vki")))

