(define-module (crates-io bk -t bk-tree) #:use-module (crates-io))

(define-public crate-bk-tree-0.1.0 (c (n "bk-tree") (v "0.1.0") (h "1j7j1pvpg95hnshkd17pva2kw5k4gzlx0mxaynags7vn8i3hf4nh")))

(define-public crate-bk-tree-0.1.1 (c (n "bk-tree") (v "0.1.1") (h "0x7b4z0h0f0k7k3n8pm5sb2zxizc5xzr38xcl6b9d7nd2m432y17")))

(define-public crate-bk-tree-0.1.2 (c (n "bk-tree") (v "0.1.2") (h "0rzr57dpnd80lhs5yk9isnvhcxrn9adhrfm3lqv9cpwly8gsvxn6")))

(define-public crate-bk-tree-0.1.3 (c (n "bk-tree") (v "0.1.3") (h "16i2ag9z4r8mmvakc18mbndi6sv72f4y77adhdv3lbx9xrh18cji")))

(define-public crate-bk-tree-0.1.4 (c (n "bk-tree") (v "0.1.4") (h "027glnwb4i2xr7y5qq2hks28p14k6rchh88kjh71kdvxl7ka9iv8")))

(define-public crate-bk-tree-0.1.5 (c (n "bk-tree") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1scplzirk0612sk37wj7f209pywwja495b557li2vdjj8npsw1kb")))

(define-public crate-bk-tree-0.1.6 (c (n "bk-tree") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1zbq53pzbh4pdzn774qb2hfpsvxhj9nh8nrnr58ic7czi611z9n0")))

(define-public crate-bk-tree-0.2.0 (c (n "bk-tree") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0pkncz3a81y4c6ywqqngpsng4g1gh0bv0gc7iyjpxjm3szfriz6v")))

(define-public crate-bk-tree-0.3.0 (c (n "bk-tree") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1125hlh3lafi077z1kxdzxdrba0q0adf658l6ml8dpn6lag0722l")))

(define-public crate-bk-tree-0.4.0 (c (n "bk-tree") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "triple_accel") (r "^0.3.4") (d #t) (k 0)))) (h "0gzg40a6q7fs9dxvhbhvl911bvf2wxb1sqx2bixp2kg90zhzc8b1") (f (quote (("enable-fnv" "fnv") ("default" "enable-fnv"))))))

(define-public crate-bk-tree-0.5.0 (c (n "bk-tree") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "triple_accel") (r "^0.3.4") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ihmq13ws2f9m58ag3p4jri9ad7zdbx7wlmwz0c3k1sbwsw3ya58") (f (quote (("enable-fnv" "fnv") ("default" "enable-fnv")))) (s 2) (e (quote (("serde" "dep:serde"))))))

