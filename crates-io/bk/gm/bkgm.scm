(define-module (crates-io bk gm bkgm) #:use-module (crates-io))

(define-public crate-bkgm-0.1.0 (c (n "bkgm") (v "0.1.0") (h "1nx37jcbm992a4p3k4ibd45zb034j8zalprxhyhjcby4rwdy9089") (y #t)))

(define-public crate-bkgm-0.2.0 (c (n "bkgm") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)))) (h "17qa10qnqj18m6hpmqp2y7rj3jj49why5195dnjd7yyb28gzl06f")))

(define-public crate-bkgm-0.3.0 (c (n "bkgm") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)))) (h "1j9m1ky1zr7x4gvl60s3nibvkbvggx2qzd5vzvh2f4m5skbgsgzy")))

(define-public crate-bkgm-0.3.1 (c (n "bkgm") (v "0.3.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)))) (h "0i44g5nr874dnj9x5fz21jqbf43nkpf5lwq3hcpfbzqvaxbcsjps")))

