(define-module (crates-io bk _a bk_allocator) #:use-module (crates-io))

(define-public crate-bk_allocator-0.1.0 (c (n "bk_allocator") (v "0.1.0") (d (list (d (n "bk_primitives") (r "^0.1.0") (d #t) (k 0)))) (h "1h5f3mgkskycx8l5102vsv99czcz0b4nd49v9vk9cgfgjlgclhcj")))

(define-public crate-bk_allocator-0.2.0 (c (n "bk_allocator") (v "0.2.0") (d (list (d (n "bk_primitives") (r "^0.1.0") (d #t) (k 0)))) (h "0mqc6f2acj18vw6xsf8wkzwd37pdqr7kfxhizlcl7k9fzawzkgf6") (y #t)))

