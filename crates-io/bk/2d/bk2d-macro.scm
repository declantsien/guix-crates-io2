(define-module (crates-io bk #{2d}# bk2d-macro) #:use-module (crates-io))

(define-public crate-bk2d-macro-0.1.0 (c (n "bk2d-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qw1vcpql60mn8p0kxzzwjh2wam2wjg9aadb6vnz7y15i9wyn6q6")))

(define-public crate-bk2d-macro-0.2.0 (c (n "bk2d-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ij4rhylsvgsd8j6cnnd1blsfrgn9cjba9kmdfwqwsbrm1l0m8ll")))

(define-public crate-bk2d-macro-0.2.1 (c (n "bk2d-macro") (v "0.2.1") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0v09s7dhdanh6xp1w7aagra38c17h8xc4fhl2mkjkykp3zl7szqs")))

(define-public crate-bk2d-macro-0.3.0 (c (n "bk2d-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "041vcbffjl2p0xzc7lb72xcz15ahawxpmvsyzj3alzi9ap310im6")))

