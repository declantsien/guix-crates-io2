(define-module (crates-io bk #{2d}# bk2d) #:use-module (crates-io))

(define-public crate-bk2d-0.1.0 (c (n "bk2d") (v "0.1.0") (d (list (d (n "bk2d-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0xg3xdlqqgx0l6vy8h4ab3s9hwqkqsgqcb2hqjciflpmx77sl690")))

(define-public crate-bk2d-0.2.0 (c (n "bk2d") (v "0.2.0") (d (list (d (n "bk2d-macro") (r "^0.2.0") (d #t) (k 0)))) (h "02l9i66bcclpqfwbrfj1h34vx70qs4wx7mrrsng2s043gw9dx7ck")))

(define-public crate-bk2d-0.3.0 (c (n "bk2d") (v "0.3.0") (d (list (d (n "bk2d-macro") (r "^0.3.0") (d #t) (k 0)))) (h "00457ybvnrn2gyvjfw8ffy6vcsay6za6nvr83sf69mdyvff8ix99")))

