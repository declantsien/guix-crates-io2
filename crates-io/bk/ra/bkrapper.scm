(define-module (crates-io bk ra bkrapper) #:use-module (crates-io))

(define-public crate-bkrapper-0.1.0 (c (n "bkrapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "diff-struct") (r "^0.4.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07rimdjj79q0z7blrsvk9fv804pfvjpzrvd22rqbsfwi9qy888kx") (y #t)))

