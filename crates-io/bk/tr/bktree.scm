(define-module (crates-io bk tr bktree) #:use-module (crates-io))

(define-public crate-bktree-1.0.0 (c (n "bktree") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1a5zr6irwdsdbi9q07gm47jwanmv6p47fq0m615i6z711a7x50zq")))

(define-public crate-bktree-1.0.1 (c (n "bktree") (v "1.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "075j035k2ii5xivmg6dc9nfrbrgkcwc0k1i1jsg3nskgh52fgc8b")))

