(define-module (crates-io xo of xoofff) #:use-module (crates-io))

(define-public crate-xoofff-0.1.0 (c (n "xoofff") (v "0.1.0") (d (list (d (n "criterion") (r "=0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.1.0") (d #t) (k 2)))) (h "05d10pvppqn2dvyp2sbscwxzsvbmxq7sd9hlv81303bh8b8gx3q0") (f (quote (("dev"))))))

(define-public crate-xoofff-0.1.1 (c (n "xoofff") (v "0.1.1") (d (list (d (n "criterion") (r "=0.4.0") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.1.0") (d #t) (k 2)))) (h "0wsqial5pvdqy6vs4q9f9fk16gs80zy399bbpvqig422c39q3131") (f (quote (("dev"))))))

(define-public crate-xoofff-0.1.2 (c (n "xoofff") (v "0.1.2") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.3.1") (d #t) (k 2)))) (h "09w0x8adfxcaqp2kdfd2rwzjh01jsppm4h155c6x463wq595rfrd") (f (quote (("dev"))))))

(define-public crate-xoofff-0.1.3 (c (n "xoofff") (v "0.1.3") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "=0.6.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\", target_arch = \"aarch64\", target_arch = \"loongarch64\"))") (k 2)) (d (n "crunchy") (r "=0.2.2") (d #t) (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 2)) (d (n "rand") (r "=0.8.5") (d #t) (k 2)) (d (n "test-case") (r "=3.3.1") (d #t) (k 2)))) (h "09824adjb2fygi2q7424dig5w3xf0wzjnmqcx54v26nqgai7yj8v") (f (quote (("dev"))))))

