(define-module (crates-io xo r- xor-genkeys) #:use-module (crates-io))

(define-public crate-xor-genkeys-0.1.0 (c (n "xor-genkeys") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)))) (h "15bz70c6i8scxq50ib7w5307jw8d9fqx08s5v0w3x033q76klwk0")))

(define-public crate-xor-genkeys-0.2.0 (c (n "xor-genkeys") (v "0.2.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)))) (h "05aqc3x6djfzijjkgbg82sgfijn2dkphx9kq5wxivlf9ifxg89ck")))

(define-public crate-xor-genkeys-0.3.0 (c (n "xor-genkeys") (v "0.3.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "xor-utils") (r "^0.2.0") (d #t) (k 0)))) (h "1j00mb2v0q0yf7yybgmp2ws6wdkpb7zw6pv1fbcz7977hj0f3krj")))

