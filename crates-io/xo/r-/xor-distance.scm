(define-module (crates-io xo r- xor-distance) #:use-module (crates-io))

(define-public crate-xor-distance-0.1.0 (c (n "xor-distance") (v "0.1.0") (h "13lzz0vc2nqm0i1nisj98m6ax4lk7lkna9i50kl33jdjf44s7xii")))

(define-public crate-xor-distance-0.2.0 (c (n "xor-distance") (v "0.2.0") (h "1zmrpfpg56m7j5gfp93yc5x4nmmrnn9hrnqldb45zd2yn05k55ig")))

