(define-module (crates-io xo r- xor-keysize-guess) #:use-module (crates-io))

(define-public crate-xor-keysize-guess-1.0.0 (c (n "xor-keysize-guess") (v "1.0.0") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "1qdd01aybdhy1yg6yqjqv4lfddpilp47n1dxnc25576kmagbl4im")))

(define-public crate-xor-keysize-guess-1.0.1 (c (n "xor-keysize-guess") (v "1.0.1") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.3.0") (d #t) (k 0)))) (h "1yfra27i821d1r8lf4zs1iwwlm4bn8lvyqbpnk8r0gi9b0g6xhbk")))

