(define-module (crates-io xo r- xor-distance-exercise) #:use-module (crates-io))

(define-public crate-xor-distance-exercise-0.1.0 (c (n "xor-distance-exercise") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1jjy1l14gfj561ism04nyna70qqlkpw3mm6na8im93m5jnw8848k")))

(define-public crate-xor-distance-exercise-0.1.1 (c (n "xor-distance-exercise") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0fcyz4wgf69j0d4sm0xg0z1qiw1ka9zcddlnzjsq1x5np6f73b1c")))

(define-public crate-xor-distance-exercise-0.2.0 (c (n "xor-distance-exercise") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0h23izl9s6360rhgsl1jyywbazsfnsns0nd482wrn1a4g2lds9r5")))

(define-public crate-xor-distance-exercise-0.2.1 (c (n "xor-distance-exercise") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0agwpyfg0ns64bl8b4d1fpy7hmn0vksp8hjxir07xq4diqn7fcfj")))

(define-public crate-xor-distance-exercise-0.2.2 (c (n "xor-distance-exercise") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "14gn47jlbjjlkk9fywwsz8yb1vjgnk8jxg736mqq33510249dqf7")))

(define-public crate-xor-distance-exercise-0.3.0 (c (n "xor-distance-exercise") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0hlxyw81wkan2viyan7b1ias0mp166dr93rgynsm2x5l597xvjaj")))

(define-public crate-xor-distance-exercise-0.3.1 (c (n "xor-distance-exercise") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0za3561fwh55l684ag1fk746d78kbll4nsjqlqcxgyq5l7ps4s35")))

(define-public crate-xor-distance-exercise-0.3.5 (c (n "xor-distance-exercise") (v "0.3.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0p6mbv1nbapq7cb12pqdyb7mxvrci4qpl6zlwmry9nd12kbl2ybh")))

(define-public crate-xor-distance-exercise-0.3.6 (c (n "xor-distance-exercise") (v "0.3.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0rxd7ql22yhqmwyaqk1cfvc1vn3fxjprqii8fmd2wfzshi040l8v")))

