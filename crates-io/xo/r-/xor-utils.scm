(define-module (crates-io xo r- xor-utils) #:use-module (crates-io))

(define-public crate-xor-utils-0.1.0 (c (n "xor-utils") (v "0.1.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1f9d174wn1ixwdkr9x3wdi46ir89n7f4y4rhmw481d4dr32771pl")))

(define-public crate-xor-utils-0.1.1 (c (n "xor-utils") (v "0.1.1") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1xxx85vwwjvpd6wrs0bf86lbvfmwhxfalj61nydyfyksj8nd9n6k")))

(define-public crate-xor-utils-0.1.2 (c (n "xor-utils") (v "0.1.2") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0ryng8fglmncvnbq6sm9jrdflzirsnkapdfs4asj1bz0fswvq5vp")))

(define-public crate-xor-utils-0.1.3 (c (n "xor-utils") (v "0.1.3") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0n6hfkq0ardqpdmcq489qld82wnyvxmlbv4616clj9z1bi4km0l2")))

(define-public crate-xor-utils-0.2.0 (c (n "xor-utils") (v "0.2.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0w12qb01zxacv8sgbxwybjciig9hppdr71vw6xky0kxy51jdiz7j")))

(define-public crate-xor-utils-0.3.0 (c (n "xor-utils") (v "0.3.0") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0sa8vhv9nzdcwijf36m82rhhxfl67sfid7h7kfx2z6n9m2347fxx")))

(define-public crate-xor-utils-0.4.0 (c (n "xor-utils") (v "0.4.0") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1nb131i67yjym24wwbz9r7b83hbsfs1bgh0qsbkd3fwds529z8w5")))

(define-public crate-xor-utils-0.4.1 (c (n "xor-utils") (v "0.4.1") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1wx6haijp88a6vb1br2asdcwd5pdwr1ffx5d8p8x6fhc0bxybwnc")))

(define-public crate-xor-utils-0.4.2 (c (n "xor-utils") (v "0.4.2") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1npf7b7063vqs8z8w42vi74akb5zim76hafwy94qxjv34r3414pl")))

(define-public crate-xor-utils-0.5.0 (c (n "xor-utils") (v "0.5.0") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1zcajk0cgarlgwcn643dmhrszd2rvbv5bx2gcdx9slxi03rinhpi")))

(define-public crate-xor-utils-0.6.0 (c (n "xor-utils") (v "0.6.0") (d (list (d (n "hamming") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0br0p7ndsz3vpisjpcw64kxf71xk2niy21amm593xvyg469khg1w")))

