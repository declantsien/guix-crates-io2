(define-module (crates-io xo r- xor-str-encode) #:use-module (crates-io))

(define-public crate-xor-str-encode-0.1.0 (c (n "xor-str-encode") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)))) (h "1nlihr8nlmpdqgp7kr6qdpvcdgwnwlswd97nzxm1vafifj76ddaj")))

(define-public crate-xor-str-encode-0.1.1 (c (n "xor-str-encode") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)))) (h "088zynzvz22dhb53r1zad0ilm0q6ppi2sh4mmdmazid4i6qqnqyr")))

