(define-module (crates-io xo r- xor-str) #:use-module (crates-io))

(define-public crate-xor-str-0.1.0 (c (n "xor-str") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "xor-str-encode") (r "^0.1.0") (d #t) (k 0)))) (h "14r190g7q3222zc0z54xyza4zz5997x16bd42jdcbspi0snvq2m8")))

(define-public crate-xor-str-0.1.1 (c (n "xor-str") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "xor-str-encode") (r "^0.1.0") (d #t) (k 0)))) (h "0y2h54isbazw610qm8ck5j3w20ysfmwand4c7fi8rai84s92llw6")))

(define-public crate-xor-str-0.1.2 (c (n "xor-str") (v "0.1.2") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "xor-str-encode") (r "^0.1.0") (d #t) (k 0)))) (h "1k1shbm3cxw6h48dvmln3gzfq0f7lyirw7gv27dkpx6i275m4pvb")))

(define-public crate-xor-str-0.1.3 (c (n "xor-str") (v "0.1.3") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "xor-str-encode") (r "^0.1.0") (d #t) (k 0)))) (h "1zdv87wcavlsilbzyk95w4w6qg9j6rawgh16ly86rhjvriapighn")))

(define-public crate-xor-str-0.1.4 (c (n "xor-str") (v "0.1.4") (d (list (d (n "syn") (r "^2.0.64") (d #t) (k 0)) (d (n "xor-str-encode") (r "^0.1.1") (d #t) (k 0)))) (h "004z29a7q8jsvl5p4b97njmvi8wxgac34z6qw1whlpk63c3wnfhl")))

