(define-module (crates-io xo de xodec-rs) #:use-module (crates-io))

(define-public crate-xodec-rs-0.1.0 (c (n "xodec-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 1)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1mgj2hnd210a322rdmbv3dcgg6ps5g37zswa1r8k4z188bv5gr1n")))

(define-public crate-xodec-rs-0.1.1 (c (n "xodec-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 1)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)))) (h "0czl3m2p9hggi0s96pyismmj1z4c7bx8r8izwvp4h4d4018nrr2w") (l "ffmpeg")))

(define-public crate-xodec-rs-0.1.2 (c (n "xodec-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 1)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)))) (h "0d6ppah5dg6dx0ipp6pyv6gb3fajbdd9g8hzi5xnz44laamnnr6i")))

(define-public crate-xodec-rs-0.1.3 (c (n "xodec-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 1)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)))) (h "0la8vdf94v89ifzqbw71xsfkz8xdjz2jhi8rw9h56hw7c71flvcm") (l "faac")))

(define-public crate-xodec-rs-0.1.4 (c (n "xodec-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.2") (d #t) (k 1)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1m8y56giwn04bw4r0028yz29yqq7mrp9b2d08fr90dxdq08h608i")))

(define-public crate-xodec-rs-0.1.6 (c (n "xodec-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.11") (d #t) (k 0)) (d (n "cyberex") (r "^0.1.11") (d #t) (k 1)) (d (n "ffprobe") (r "^0.3.3") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "xux-rs") (r "^0.1") (d #t) (k 0)))) (h "0kjxgcvlbijvalk5n1sbfvcs4z37czxyp350n5z7v4bxc1r3mh7r")))

(define-public crate-xodec-rs-0.1.7 (c (n "xodec-rs") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.3.6") (d #t) (k 0)) (d (n "cyberex") (r "^0.3.6") (d #t) (k 1)) (d (n "ffprobe") (r "^0.3.3") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "xux-rs") (r "^0.1.36") (d #t) (k 0)))) (h "01dd1ab8fy436nnjl8vpjghgx4xgy8v8g48dchaixqfyk11214xk")))

(define-public crate-xodec-rs-0.1.8 (c (n "xodec-rs") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cyberex") (r "^0.3.7") (d #t) (k 0)) (d (n "cyberex") (r "^0.3.7") (d #t) (k 1)) (d (n "ffprobe") (r "^0.3.3") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "th_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "xux-rs") (r "^0.1.36") (d #t) (k 0)))) (h "1l52ky47p719dc8lb2jkqic35ixxxh4ybyi41c9gxjyrz76c71wg")))

