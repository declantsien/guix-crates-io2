(define-module (crates-io xo rf xorfilter-rs) #:use-module (crates-io))

(define-public crate-xorfilter-rs-0.1.0 (c (n "xorfilter-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0zwavhi3vqb16ddw1p31ia4g4ynjq95xagshw2jfviwlmzq60vq9")))

(define-public crate-xorfilter-rs-0.2.0 (c (n "xorfilter-rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "171fbbhggz0k53cv2yshxhpc82136l9yk7yzlz4zixdnkr46chwh")))

(define-public crate-xorfilter-rs-0.3.0 (c (n "xorfilter-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0whmazql0p4bdzhpfx7fcrzyr211dnfh3q7jhzcqdnhccfkr3w1m")))

(define-public crate-xorfilter-rs-0.4.0 (c (n "xorfilter-rs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1jhbv3s903zj9pcfsc9s7kx6l6clf5j5yr7nfsnmzw3appjq4f80")))

(define-public crate-xorfilter-rs-0.5.0 (c (n "xorfilter-rs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "08w1b7337849df3s4x6mamqrzi89ab53nh6bnzs7mmi9znxdg98z")))

(define-public crate-xorfilter-rs-0.5.1 (c (n "xorfilter-rs") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "091gs6rir2n0m48fvw7ccdpxrm3219vifvc91cavrdl8d8lxmya7")))

