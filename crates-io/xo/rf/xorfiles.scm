(define-module (crates-io xo rf xorfiles) #:use-module (crates-io))

(define-public crate-xorfiles-0.1.0 (c (n "xorfiles") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "10hhqn70ccb1i9cxqsgk11a7hc85yrn3bm9hw5s3xfp11drscz4m")))

(define-public crate-xorfiles-0.1.1 (c (n "xorfiles") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "0xxabjgyvz91p0i92iniaams5jhkaddzfsf21l3n9m1a2h2i3c8r")))

