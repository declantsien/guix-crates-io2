(define-module (crates-io xo rf xorfilter) #:use-module (crates-io))

(define-public crate-Xorfilter-0.0.1 (c (n "Xorfilter") (v "0.0.1") (h "1f2iqn84qkq8z4740jay8lbjmyva3qdb7j4j4l12i8b6jiq2s75r")))

(define-public crate-Xorfilter-0.0.2 (c (n "Xorfilter") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0fagm5kyhglfsgashbqa1y709njlai7w194c262di9njz28j9c39")))

(define-public crate-Xorfilter-0.0.3 (c (n "Xorfilter") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1prikvd7a60gy3qh900c7mw823126nzwz9i6irr5c5gr45gdi5a8")))

(define-public crate-Xorfilter-0.0.4 (c (n "Xorfilter") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0p5r31bxgkz7iq2lffsc302b1f1bwqaa6zkk2ljb009g87qwff9x")))

(define-public crate-Xorfilter-0.0.6 (c (n "Xorfilter") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "183q6mafigmfd3lr4b8qcnwx0arc96sf2bsb9m4rghhil3hy5g04")))

(define-public crate-Xorfilter-0.0.7 (c (n "Xorfilter") (v "0.0.7") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1w3wdp4lxy3cfswazs9ik5dv8rmql3z2q3sjjp0p1i3sqd7prx3n")))

(define-public crate-Xorfilter-0.0.8 (c (n "Xorfilter") (v "0.0.8") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0ji4ghw7jhnnfcnx134gq9qg5igjc70ba9s0ybn1kmg4d0dpzjx7")))

(define-public crate-Xorfilter-0.0.9 (c (n "Xorfilter") (v "0.0.9") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1wfw2mq2wizw77scyjjf1vpj30wghvjhj8x2nhj6cym199qkpsba")))

(define-public crate-Xorfilter-0.0.10 (c (n "Xorfilter") (v "0.0.10") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "12ghp696k53i7v7xyjxrjgqa2yfn9i59dggk7avflxj662ihw3g3")))

(define-public crate-Xorfilter-0.1.0 (c (n "Xorfilter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0bq54b9q8iwfaq4xsq6hj4sqk7h578l2x95mihncrc9ydb69m8mx")))

(define-public crate-Xorfilter-0.1.1 (c (n "Xorfilter") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "11rpsvc0n4jmbkn8400nm1mwx2ca46zma98gnivzq0ix5znxrsg8")))

(define-public crate-Xorfilter-0.1.2 (c (n "Xorfilter") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "11rhk9j9l4wdwcab92k68y298lw4pd6bcl3h2lqnv2xv9ikbyvf6")))

(define-public crate-Xorfilter-0.1.3 (c (n "Xorfilter") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1823px0l7lcqa9svcrcg39pn08c2gjxamk9nx8n0vnrgdrqqk36n")))

(define-public crate-Xorfilter-0.1.4 (c (n "Xorfilter") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0204wb56hqybf8pnzw11glw7lfkr6dksxw9kil4rifg1qxacfhyx")))

(define-public crate-Xorfilter-0.1.5 (c (n "Xorfilter") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1dxj3jl4imcm5yvqwk8iklqgc81v97acajv37kfbqqdax08k3b4g")))

(define-public crate-Xorfilter-0.1.6 (c (n "Xorfilter") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "16fhdnndv8hl2qmv67shmfwxfryxgxkfdsyma2di1wrk8wi9i530")))

(define-public crate-Xorfilter-0.2.0 (c (n "Xorfilter") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "16syiifv0vgwbjvvkyplqyrxpqrwj7kqr9g72q8hghhivgijs4sc")))

(define-public crate-Xorfilter-0.2.1 (c (n "Xorfilter") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "0i6wqm26ka3rwsbp5gfhpq4qnwfc5dv3a2w2v2nnzv4hgh4zhn2s")))

(define-public crate-Xorfilter-0.2.2 (c (n "Xorfilter") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.4.1") (d #t) (k 2)))) (h "1p2khgb2zsq70bjgphcs6aw4ib6wpmyd1n6d4pf8b7whcjcz3173")))

