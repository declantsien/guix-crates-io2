(define-module (crates-io xo sd xosd-rs) #:use-module (crates-io))

(define-public crate-xosd-rs-0.1.0 (c (n "xosd-rs") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "xosd-sys") (r "^2.2.14") (d #t) (k 0)))) (h "19065rlz4xbqxm2azxwvh7c6ixmvx46ny7xylzz7z6h2rxf6ljyq")))

(define-public crate-xosd-rs-0.2.0 (c (n "xosd-rs") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "xosd-sys") (r "^2.2.14") (d #t) (k 0)))) (h "0slk03sjf9dk80kbqb24s5vxsjqg2jc6gx53xdps7dwzw7g84lhc")))

