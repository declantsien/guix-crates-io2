(define-module (crates-io xo ro xoroshiro) #:use-module (crates-io))

(define-public crate-xoroshiro-0.0.1 (c (n "xoroshiro") (v "0.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vsmi6b9fbw66svq0pxp73n4fmyq7fb2yz6vbl2pyv2c8mrqy23v")))

(define-public crate-xoroshiro-0.1.0 (c (n "xoroshiro") (v "0.1.0") (d (list (d (n "aesni") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1l511c1bdfrxh92qmim2kwdpjcxgcbdbl26xcq8l98fzmblryv0w")))

(define-public crate-xoroshiro-0.2.0 (c (n "xoroshiro") (v "0.2.0") (d (list (d (n "aesni") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hh1zs7a324qixid715fkzydag0gg71klzrs0m5r7wsr3c2fnfqv") (f (quote (("unstable" "aesni") ("default"))))))

(define-public crate-xoroshiro-0.3.0 (c (n "xoroshiro") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "1qprykl0l9x5andaddyyn2j6v1n2vlnm90x1kc9gwkz2fyag1blf")))

