(define-module (crates-io xo ro xoroshiro128) #:use-module (crates-io))

(define-public crate-xoroshiro128-0.1.0 (c (n "xoroshiro128") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00dg9vdprvk8fg38adk5dvcbi20kfg3rv0kyqjpp606kwdx429l4")))

(define-public crate-xoroshiro128-0.2.0 (c (n "xoroshiro128") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0yblkf84a9lzdddnmxxkimhskjm51c3da6kwq9jqjhgq49mb9dpf")))

(define-public crate-xoroshiro128-0.3.0 (c (n "xoroshiro128") (v "0.3.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1kfzanis2v39r8q963q161izv0hmfsdxa11cxgqw8jgcp8sdmvp0")))

(define-public crate-xoroshiro128-0.4.0 (c (n "xoroshiro128") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0dd0qjdymxhzdgkxqn1w9vvmbxhjqsjx5lavc14vxz47v482v024")))

(define-public crate-xoroshiro128-0.0.0 (c (n "xoroshiro128") (v "0.0.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1fxydvs4r7y9dzd5bwci6n23crkgxp91h644cyvkna8w71jpsasj") (f (quote (("std")))) (y #t)))

(define-public crate-xoroshiro128-0.5.0 (c (n "xoroshiro128") (v "0.5.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0zll2cy4lw3h4hv17s9si5fina3zf2724w8f774011d9943hmw5l") (f (quote (("std"))))))

