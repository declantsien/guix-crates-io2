(define-module (crates-io xo rs xorstring-procmacro) #:use-module (crates-io))

(define-public crate-xorstring-procmacro-0.1.0 (c (n "xorstring-procmacro") (v "0.1.0") (d (list (d (n "lazy_static") (r "~1") (d #t) (k 0)) (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (d #t) (k 0)))) (h "0zl7h6mq1xww9jlsr023kbjkpp19qqfnhnlkkx64adw2fsssljiq")))

