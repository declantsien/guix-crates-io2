(define-module (crates-io xo rs xorshift) #:use-module (crates-io))

(define-public crate-xorshift-0.1.0 (c (n "xorshift") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b31q3siyxv731y2bcw3583qjycrk31vr36hnvvk0452qr51za5r")))

(define-public crate-xorshift-0.1.1 (c (n "xorshift") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "11vkawfbmdfmn3dqgqki42ymamfyx9pfcdydignjnpfzgdcnirbk")))

(define-public crate-xorshift-0.1.2 (c (n "xorshift") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "029vm9p6w2qn6bcvdiwy66bxh4kl7cvd6r1ngfagn4mizy8zifwi")))

(define-public crate-xorshift-0.1.3 (c (n "xorshift") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1vd1xjnrli1qkx8s5i7gilq6rizx1nvwcg5jrbm0np6l9dal46fs")))

