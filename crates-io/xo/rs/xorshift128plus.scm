(define-module (crates-io xo rs xorshift128plus) #:use-module (crates-io))

(define-public crate-xorshift128plus-0.1.0 (c (n "xorshift128plus") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1p0qv4dlrif7b783r8b662rj42bsm7fhhl16gpcmlz0f9yazwdzq")))

(define-public crate-xorshift128plus-0.1.1 (c (n "xorshift128plus") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0xxr7lqb0ygf2nxxahp5dza6fq3cidcr1frz9xkkax2fyil607bi")))

