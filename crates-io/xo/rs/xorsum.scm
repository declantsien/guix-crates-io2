(define-module (crates-io xo rs xorsum) #:use-module (crates-io))

(define-public crate-xorsum-0.0.0 (c (n "xorsum") (v "0.0.0") (h "074034ywa3chx9jh6y3g57agb7m83r79ka1ml6hxjsfrmd7xx3jd")))

(define-public crate-xorsum-0.0.1 (c (n "xorsum") (v "0.0.1") (h "1rj9v3f31pylbphv40dscrh8hjd2qbrmlscbgz8chdwdhpkh87rn")))

(define-public crate-xorsum-1.0.0 (c (n "xorsum") (v "1.0.0") (h "1fq5ljyyygvgq1wyrwyp4nglp3k2ipa7485czrph19wbn0qrablp")))

(define-public crate-xorsum-2.0.0 (c (n "xorsum") (v "2.0.0") (h "1irswwgil5fyy2b86xb9h8ljafds7ly0w4zxraqb7sj7fdq946g9")))

(define-public crate-xorsum-3.0.0 (c (n "xorsum") (v "3.0.0") (h "1yhg8zvfw537j69r331sngf2ibj5p4cpki7izpg3malm2m8j36g8")))

(define-public crate-xorsum-3.1.0 (c (n "xorsum") (v "3.1.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j723fcyrjnv6ldww0pdn570va3blb6lkivksj3ral8n2gk72dra")))

(define-public crate-xorsum-3.1.1 (c (n "xorsum") (v "3.1.1") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "0a3zkz6dvv3cn6vcqi4bvz22wcx9r8drls20iaqi1j5z55hzdmmn")))

(define-public crate-xorsum-3.1.3 (c (n "xorsum") (v "3.1.3") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rb0m9n3sj9l1xqisxnh0mbkxjwikirxfxmy5fif27l69zky4a4p")))

(define-public crate-xorsum-3.1.5 (c (n "xorsum") (v "3.1.5") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dj35lijnnbwsics35ihpwbbmchbgpd9ymq3h22k8xh178h4gvx4")))

(define-public crate-xorsum-3.1.6 (c (n "xorsum") (v "3.1.6") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "06j2pxhxkr5v5yk4bs73x3rm0j9742lyy97mkqw2mnhgayz1338c")))

(define-public crate-xorsum-3.1.7 (c (n "xorsum") (v "3.1.7") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lzia6qrzhdnkb0ij83qmn3i71wscxb8sl77ns9zvxbz2sfnd6vj")))

(define-public crate-xorsum-4.0.0 (c (n "xorsum") (v "4.0.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xi3ysggq1cl6sbqzfif8nklc4yn1dbavcmc1vx7dvw5xki5a8rj")))

