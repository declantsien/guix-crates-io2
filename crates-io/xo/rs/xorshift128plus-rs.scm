(define-module (crates-io xo rs xorshift128plus-rs) #:use-module (crates-io))

(define-public crate-xorshift128plus-rs-0.1.0 (c (n "xorshift128plus-rs") (v "0.1.0") (h "0p35n5c5rry8gmpc8lrm42d0mb6b2clc9mdnx825xq3l04vc2jbx")))

(define-public crate-xorshift128plus-rs-0.1.1 (c (n "xorshift128plus-rs") (v "0.1.1") (h "0ih5fhyzab3mcg2r521a4vvmvf7b8i6ny8hlqlvyw6zq2qq0ylyz")))

(define-public crate-xorshift128plus-rs-0.1.2 (c (n "xorshift128plus-rs") (v "0.1.2") (h "0m10x9bx4742b0k16gqiphw6hqylflrziqddxw18mpi9yqvfbicb")))

(define-public crate-xorshift128plus-rs-0.1.3 (c (n "xorshift128plus-rs") (v "0.1.3") (h "1ab4vhzb78j6rf1jlf7nxb9v44higsy2xby8i8iq6wffh3kfkav0")))

(define-public crate-xorshift128plus-rs-0.1.4 (c (n "xorshift128plus-rs") (v "0.1.4") (h "18hkxsrxspf14j51v8g099y5lsgqdh3z58cwqs7pvmgz2kl2hlqa")))

(define-public crate-xorshift128plus-rs-0.1.5 (c (n "xorshift128plus-rs") (v "0.1.5") (h "0jy61kdj83nxmkqjanwqcd2z6i02y4xdk7lvznzgqm3pciy7dply")))

(define-public crate-xorshift128plus-rs-0.1.6 (c (n "xorshift128plus-rs") (v "0.1.6") (h "0cv6vvmkrhh6bxzv731vnwxqvmp3rsyd48fhmhp2k934x8gg7546")))

