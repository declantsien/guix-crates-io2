(define-module (crates-io xo sh xoshiro) #:use-module (crates-io))

(define-public crate-xoshiro-0.0.1 (c (n "xoshiro") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "039dfwwf2knmyvq94w2zabvd2fdzcabm578pya62cnrnshpyhhhw")))

(define-public crate-xoshiro-0.0.2 (c (n "xoshiro") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "0d8nggam8d33kv81kydsbhscxpayjw4y6wcp5r1nqi5vnh57skxa")))

(define-public crate-xoshiro-0.0.3 (c (n "xoshiro") (v "0.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "01izwznmasar87kglpqna9knk926qwkgyxq95f7cv70nsxj78v9k")))

(define-public crate-xoshiro-0.0.4 (c (n "xoshiro") (v "0.0.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2") (d #t) (k 0)))) (h "0qnyr90h6r6w6cdg6arjgig37sxyr20adg6ahza4ymcqi68l1rxl")))

(define-public crate-xoshiro-0.0.5 (c (n "xoshiro") (v "0.0.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (d #t) (k 0)))) (h "1wwh4kxrsqxpzpd7y9c3pi9w0s9pp7qpsv4l265705kd4d12ic4q")))

