(define-module (crates-io xo tp xotp) #:use-module (crates-io))

(define-public crate-xotp-0.1.0 (c (n "xotp") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "1zhplwf66x0pkfhn5mw9gvi5mzzfps06ssdfnmmvazcjiy73akkc")))

(define-public crate-xotp-0.2.0 (c (n "xotp") (v "0.2.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1d1kxrwf2c8k8gfhwzyf1bkakpdy0khahpvp883lvynlg4d83hj4") (f (quote (("ffi") ("default")))) (y #t)))

(define-public crate-xotp-0.3.0 (c (n "xotp") (v "0.3.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0khw4qjsnb5lwzi00z4zw1w8477344xr3k3hx8zx4qqaf36jyg90")))

(define-public crate-xotp-0.4.0 (c (n "xotp") (v "0.4.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0zj6kb098ia39a8xnjmy9yx0xlszhzxx1a6xnqvrf5q22f2dq889")))

