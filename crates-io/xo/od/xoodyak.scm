(define-module (crates-io xo od xoodyak) #:use-module (crates-io))

(define-public crate-xoodyak-0.1.0 (c (n "xoodyak") (v "0.1.0") (d (list (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1x1pis4w2h9ajvcr22w6dpsnf8wyvr2rc1yh6vsarmkmqy14yy9y") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.1.1 (c (n "xoodyak") (v "0.1.1") (d (list (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0d4crxpj4145h7fpja4xdrjbsdcqgyjfixykdrb3vvl2nlzi4196") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.1.2 (c (n "xoodyak") (v "0.1.2") (d (list (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0ry70jng4pp17yll8i1l8z3jpb1hsrvqqfbvjs698wyf7fyrcdl7") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.2.0 (c (n "xoodyak") (v "0.2.0") (d (list (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0a46ix8gj423n5ijsnnh1dr2v4hxnici151pnnf03y0f9b7qfrhw") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.0 (c (n "xoodyak") (v "0.3.0") (d (list (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0ibxqif8mvr0lvpf20ph3gnhbwvlc0nzkxdp11j0278xjsnnyvhm") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.1 (c (n "xoodyak") (v "0.3.1") (d (list (d (n "rawbytes") (r "^0.1.0") (d #t) (k 0)) (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0z770758d1bndq6j25vlzs04fg7vv096caa5365970jkm63nlqbr") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.2 (c (n "xoodyak") (v "0.3.2") (d (list (d (n "rawbytes") (r "^0.1.0") (d #t) (k 0)) (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0msxwbd2w6gd5qhzmw5a8j5cjvvycaz2h7yxb45grys02q9hz75s") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.3 (c (n "xoodyak") (v "0.3.3") (d (list (d (n "rawbytes") (r "^0.1.0") (d #t) (k 0)) (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1cjigk7ydnrdhg5awb7aqxl5wsd8176rvfhx9m5w3izfnmlh63z5") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.4 (c (n "xoodyak") (v "0.3.4") (d (list (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "06cvrrvh2n7rq5s4yid1lzqrrvbjl6arg5wbw0bll713lh8hq4mi") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.5 (c (n "xoodyak") (v "0.3.5") (d (list (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "unroll") (r "^0.1.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0wgwymxwq2l5g4nmyqhqwdyzb1c2lllzihv6m8yhxdwnmjsc0njl") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.6 (c (n "xoodyak") (v "0.3.6") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0s2mh71zg12l4fnslckzqbi6w8bzc4kj0zbhzz0zmqfh85nmvkx5") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3.7 (c (n "xoodyak") (v "0.3.7") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0298wj1axm9qgxxk96cnvlnpyc3dj2f397xmf6arxfp21qgavjgl") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4.0 (c (n "xoodyak") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0j5ln3qlxvyids3qcksq2hch8slpfry4j2h12la3hy8fmzqi5141") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4.1 (c (n "xoodyak") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1fd7ihp30lsiw1543f1m1w1pdy59qywqxj527cq0br70zbsrkq33") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4.2 (c (n "xoodyak") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "05hjapd0kxj88jxf8p5r81gg11jhwbfhx5icqrmb51kjkph9fk97") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.5.0 (c (n "xoodyak") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0bdilj2qm9wid9x1va132x4k9lwvj6jyxzjqkk0kv2bvfxyrmpr2") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.5.1 (c (n "xoodyak") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "17ksfv9bpyqrka2fgphgw7z6pzk3dx13sv6l6cdqgp2l0azw9bkz") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7.0 (c (n "xoodyak") (v "0.7.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "rawbytes") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "zeroize") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "10d4fj2g2vkxkkqdvbmd8fbkalzlhk9gg6c8hpn6qdrsbih8b74g") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7.1 (c (n "xoodyak") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.2") (d #t) (k 0)))) (h "12nvq6v9ssyhw5xxx78v14mmwyzq0ihzwl0n59dvza8hs4jlykh9") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7.2 (c (n "xoodyak") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.3") (d #t) (k 0)))) (h "0h5n0sswyi7mqnqhh8hgcxfnafanfizli7b8cyrh7ky9yhzb8gjw") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7.3 (c (n "xoodyak") (v "0.7.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.4") (d #t) (k 0)))) (h "1vgfnpxbsarv78z81q41s0l2znqh2spq6cz3spf64j6ap6xjvdcl") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8.0 (c (n "xoodyak") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1dhwxjridyzpy3vn25ildimdcaylmwf4rwcnhfshds8khd3qibqy") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8.1 (c (n "xoodyak") (v "0.8.1") (d (list (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "0wvr1wp31849npvkg3bspdl3bs9i9v77sbxq1f9l0z8pd0jpxigx") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8.2 (c (n "xoodyak") (v "0.8.2") (d (list (d (n "benchmark-simple") (r "^0.1.7") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6") (d #t) (k 0)))) (h "0xal85dj1lmc7kh2lcv4msd3xvgh0v7bdngmqa18y3rk826yinc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8.3 (c (n "xoodyak") (v "0.8.3") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6") (d #t) (k 0)))) (h "1q515cjyapgdhvl2bdri68zzdd07mxcr9122k78nvfrs7267ki49") (f (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8.4 (c (n "xoodyak") (v "0.8.4") (d (list (d (n "benchmark-simple") (r "^0.1.8") (d #t) (k 2)) (d (n "zeroize") (r "^1.6") (d #t) (k 0)))) (h "0szi326zgf66asmy2pmq1ni99syjdy3m2pqm8603kmaq40dpdb8d") (f (quote (("std") ("default" "std"))))))

