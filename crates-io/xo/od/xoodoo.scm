(define-module (crates-io xo od xoodoo) #:use-module (crates-io))

(define-public crate-xoodoo-0.1.0 (c (n "xoodoo") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rawbytes") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.2") (d #t) (k 0)))) (h "01v1jbh6x2zpsq1ck2l57kj1vbizxxfaramnq35dxkx9p8ncdly1") (f (quote (("std") ("default" "std"))))))

