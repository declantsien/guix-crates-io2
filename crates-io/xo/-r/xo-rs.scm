(define-module (crates-io xo -r xo-rs) #:use-module (crates-io))

(define-public crate-xo-rs-0.1.0 (c (n "xo-rs") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)))) (h "0sjl7mwy8n7fv3f7aair0gxdrd1xg8vwjz84vcm7qqiy35ynla39") (y #t)))

(define-public crate-xo-rs-0.2.0 (c (n "xo-rs") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)))) (h "0zs8mx50f8pyb54jcjm7qpvb6hy8asgramkqbqdcdb9vy2p7fpn7") (y #t)))

(define-public crate-xo-rs-0.3.0 (c (n "xo-rs") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)))) (h "01vm7bq20llgix2mximgib0fxyc3fiwsc1cdw1i9682bmilgckd8") (y #t)))

(define-public crate-xo-rs-0.4.0 (c (n "xo-rs") (v "0.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1kmnd8hs08cnpicpjahycqhwcvdpf9hpm2l7r8s2wj4d42i4fcag") (y #t)))

(define-public crate-xo-rs-0.4.1 (c (n "xo-rs") (v "0.4.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1yb226cz63a7cg3bzqk9pb25f8xhqp0jjp18xsk7d6fqwbfslnca") (y #t)))

(define-public crate-xo-rs-0.5.0 (c (n "xo-rs") (v "0.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0ibahh3p34qx4ri6qr9p7pfb1d4z3c9jladv5pc6dp71b1nifjqz")))

(define-public crate-xo-rs-0.5.1 (c (n "xo-rs") (v "0.5.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1hdm0kfczwchsah2hgjc97w5sknj7ri3m95rbg0dxvvd9qhn2mw8")))

