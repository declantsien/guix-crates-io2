(define-module (crates-io xo ps xops_core) #:use-module (crates-io))

(define-public crate-xops_core-0.1.0 (c (n "xops_core") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zk9w3hlxxj8k6d9zann0wv2c73nkxlcaxv4v5jlydi1dggyry3r")))

(define-public crate-xops_core-0.1.1 (c (n "xops_core") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1h0l2g3fm2iagqlcmd7sv539vcyzi9gnm4sy1lw4cbn7pczhx41d")))

