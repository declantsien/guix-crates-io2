(define-module (crates-io xo ps xops_macros) #:use-module (crates-io))

(define-public crate-xops_macros-0.1.0 (c (n "xops_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "xops_core") (r "^0.1.0") (d #t) (k 0)))) (h "03934rpkd0szbfrvnzn99wq6kkc5jivh84x1dsj92ifksk6b2fd9")))

(define-public crate-xops_macros-0.1.1 (c (n "xops_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "xops_core") (r "^0.1.1") (d #t) (k 0)))) (h "1fg1nkx2slrp60f4vyyi9d8cilhbrr4g2ksb4dlvn097pd11nqwz")))

