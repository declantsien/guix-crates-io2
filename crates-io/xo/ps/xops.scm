(define-module (crates-io xo ps xops) #:use-module (crates-io))

(define-public crate-xops-0.1.0 (c (n "xops") (v "0.1.0") (d (list (d (n "xops_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0bc3lzbr3p3s571bx2v280j5kld82za9gwmbcgqpikl77szzrnsd")))

(define-public crate-xops-0.1.1 (c (n "xops") (v "0.1.1") (d (list (d (n "xops_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0wk6zm4j28dix7l453jh3qvc23mhcfjp2mjlrhs9ha9sldk448vi")))

