(define-module (crates-io xo r_ xor_cryptor) #:use-module (crates-io))

(define-public crate-xor_cryptor-1.0.0 (c (n "xor_cryptor") (v "1.0.0") (h "0dg9jjczfb3ys5hkra899svbjdvrrjc2f16h95diz60i0i0iqbys")))

(define-public crate-xor_cryptor-1.0.1 (c (n "xor_cryptor") (v "1.0.1") (h "0ymsvak1abzxnqiq4pmb8v7dqhirgminldh9w0xh75mnykg5mihd")))

(define-public crate-xor_cryptor-1.0.2 (c (n "xor_cryptor") (v "1.0.2") (h "1r6880p5gf5rhphchyrvxlb778jda8yr03pswk95y6pbsdhzabay")))

(define-public crate-xor_cryptor-1.1.0 (c (n "xor_cryptor") (v "1.1.0") (h "1z2y69bhakrq9jvph912rh0920cccq27m96wrz09f9y9fx3c5yxh")))

(define-public crate-xor_cryptor-1.2.0 (c (n "xor_cryptor") (v "1.2.0") (d (list (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "058m3dw1xsbdzbfv48r881kjyjcvnfr6c3nxqv9mhbi1k6kqrnw7")))

(define-public crate-xor_cryptor-1.2.1 (c (n "xor_cryptor") (v "1.2.1") (d (list (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "1swbl1d3aywa9dhq1dlz9pbyzng3xarcdllmkcihxz0q9h79iygk")))

(define-public crate-xor_cryptor-1.2.2 (c (n "xor_cryptor") (v "1.2.2") (d (list (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "03vpmq7vawp121yxbirpvz14gp0rbf7snwrjqb0cncnkir2mcprc")))

(define-public crate-xor_cryptor-1.2.3 (c (n "xor_cryptor") (v "1.2.3") (d (list (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "02cj2dsyr6r9n5p2y7dn9r69vmv5n3d2gsl38x1hiw5zxzsavrbj")))

