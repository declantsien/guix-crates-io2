(define-module (crates-io xo r_ xor_rand) #:use-module (crates-io))

(define-public crate-xor_rand-0.1.0 (c (n "xor_rand") (v "0.1.0") (h "16sjj2hsd77ff9qkknp44y9sm4mm45hivc21zpqcl7k863xkh9pg")))

(define-public crate-xor_rand-0.1.1 (c (n "xor_rand") (v "0.1.1") (h "0p4d42iq4q04pwir1nwj19rrc3z4wpnrpvvsmwy1bfl5hyzx01ga")))

(define-public crate-xor_rand-0.1.2 (c (n "xor_rand") (v "0.1.2") (h "075iay0lc1nrmrrcz23c23lk3p3j1fybf64gn2phbz433c4jcrjb")))

(define-public crate-xor_rand-0.1.21 (c (n "xor_rand") (v "0.1.21") (h "0p454wpkkz2zzs9j7ddc4i52nfymc3w5n9qr03mc8pxibapaldc1") (f (quote (("std") ("default" "std"))))))

(define-public crate-xor_rand-0.1.22 (c (n "xor_rand") (v "0.1.22") (h "0vv3wkjgqkxfgly1xsxxxdpx7wqy5lip2fmd2aqydpbl59g869hg") (f (quote (("std") ("default" "std"))))))

(define-public crate-xor_rand-0.1.2201 (c (n "xor_rand") (v "0.1.2201") (h "1mpm3c2nhf1ckspdw771ksfv2b9imn3zmpmn6ghn94mmihpsa3hi") (f (quote (("std") ("default" "std"))))))

(define-public crate-xor_rand-0.1.221 (c (n "xor_rand") (v "0.1.221") (h "1qk9jmwlzs5kv6zvk7cl0d8hwzzwnmpxsb9l69vgqa8r7z9x3g69") (f (quote (("std") ("default" "std"))))))

(define-public crate-xor_rand-0.1.222 (c (n "xor_rand") (v "0.1.222") (h "1hm3dlbdhwiblr4jsj1y6g2mgqdsg1l0lahjxf6j7yx88h2a1521") (f (quote (("std") ("default" "std"))))))

(define-public crate-xor_rand-0.1.3 (c (n "xor_rand") (v "0.1.3") (h "1yi25hs0iqd8sm9fdwmw84m5423siy9i5nivbh2d27jn2qcykb8x")))

(define-public crate-xor_rand-0.1.3000 (c (n "xor_rand") (v "0.1.3000") (h "0rmwxbyn2rf5b8yhyqqkm1rabcin7mn9792llign72317r631jg0")))

(define-public crate-xor_rand-0.1.3100 (c (n "xor_rand") (v "0.1.3100") (h "0d8clv9m83fn6xidwc9a59afkphlk283856g4f3naxgyvkp1qx5j")))

(define-public crate-xor_rand-0.1.3200 (c (n "xor_rand") (v "0.1.3200") (h "1lh06sd2dbcmk8q4d4izkd6ppn5f3g1x2dfc6rx5g5sl5l1cwb73")))

(define-public crate-xor_rand-0.1.3300 (c (n "xor_rand") (v "0.1.3300") (h "1xjdbi157bncvc68i2h7sva3brnsvfd01riiyxqivig0imwi0ari")))

(define-public crate-xor_rand-0.1.3350 (c (n "xor_rand") (v "0.1.3350") (h "04m69ni5h4vy9z6h2cwc2kgila4yccb57sbh4ia4nhrcir4gc6yv")))

(define-public crate-xor_rand-0.1.3355 (c (n "xor_rand") (v "0.1.3355") (h "18l78xvgawx39p20y0wn9zih1f0yw3sa9zrjzv6qjhchq3lrcafr")))

(define-public crate-xor_rand-0.1.3400 (c (n "xor_rand") (v "0.1.3400") (h "1cjgvc6b9xg3fbs1z1j3llbj6c3b9sv4307ilai9kvh7a0rdrkr6")))

