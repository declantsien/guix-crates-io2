(define-module (crates-io xo rl xorlist) #:use-module (crates-io))

(define-public crate-xorlist-0.1.0 (c (n "xorlist") (v "0.1.0") (h "1jv4p5zpzgyzr4cvn96bhs2pjzhlh974264vsmq3fjwk46ywfrr3")))

(define-public crate-xorlist-0.1.1 (c (n "xorlist") (v "0.1.1") (h "0zj2bygli2073cvkmkbk17sqmbmyxzp41mr8fhvmg425b9dl64ir")))

