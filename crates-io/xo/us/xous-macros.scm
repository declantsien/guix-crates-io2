(define-module (crates-io xo us xous-macros) #:use-module (crates-io))

(define-public crate-xous-macros-0.1.0 (c (n "xous-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (f (quote ("small_rng"))) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1h2cpxndba8azkfbzhn8v8dmc5zg6r6lwrlqvgw6r9ky9virmkrz")))

