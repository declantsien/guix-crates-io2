(define-module (crates-io xo us xous-ipc) #:use-module (crates-io))

(define-public crate-xous-ipc-0.8.0 (c (n "xous-ipc") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.8.0") (d #t) (k 0)))) (h "0sj2img316jsdirjk7rj120g1m3n32sgbf0bjn684v9r4ifm7ska")))

(define-public crate-xous-ipc-0.9.7 (c (n "xous-ipc") (v "0.9.7") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.7") (d #t) (k 0)))) (h "11xxd64cp9fi8961lqnfb1dvzhccw0kvpq2i6gkqyp6fvjsc8x4q")))

(define-public crate-xous-ipc-0.9.9 (c (n "xous-ipc") (v "0.9.9") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.9") (d #t) (k 0)))) (h "1js33rghphdjhm04x8hajyzv084rhalsidw707qkl2vqq10a09hb")))

(define-public crate-xous-ipc-0.9.10 (c (n "xous-ipc") (v "0.9.10") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.10") (d #t) (k 0)))) (h "1ps3678a8hx226cqymm772730k9zc29knj2qnjdraa52zh43afhy")))

(define-public crate-xous-ipc-0.9.11 (c (n "xous-ipc") (v "0.9.11") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.11") (d #t) (k 0)))) (h "0j1dy6fdywm6fihvcf3rbxb7srcpmgca99qhrixlcj0bz5zz38m6")))

(define-public crate-xous-ipc-0.9.12 (c (n "xous-ipc") (v "0.9.12") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.12") (d #t) (k 0)))) (h "0gllz2b7wsyw6ygvk1gp90g78sknic1vmypqypfi7458jz8gblii")))

(define-public crate-xous-ipc-0.9.13 (c (n "xous-ipc") (v "0.9.13") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.13") (d #t) (k 0)))) (h "0wjfr2f3n7s21dpsc957gs0bksazzj4w80gd3nhqdihq910lg2a9")))

(define-public crate-xous-ipc-0.9.14 (c (n "xous-ipc") (v "0.9.14") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.14") (d #t) (k 0)))) (h "0hanqnf8m8q5xkhqwyjpadvsc81gnw8mjjc0grqs3pqf32lshj5d")))

(define-public crate-xous-ipc-0.9.15 (c (n "xous-ipc") (v "0.9.15") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.15") (d #t) (k 0)))) (h "0vlnka4y6nrgykwrd01xdx8iwplxbnq5jxrrm1ch5qbsa2dqv9sh")))

(define-public crate-xous-ipc-0.9.16 (c (n "xous-ipc") (v "0.9.16") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.16") (d #t) (k 0)))) (h "1hmrvlszjnvi6md0dc1zghyq4yz9dcbnafxfxpv7w10vmjfpaw2g")))

(define-public crate-xous-ipc-0.9.17 (c (n "xous-ipc") (v "0.9.17") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.17") (d #t) (k 0)))) (h "0jgn5mlgn3kmgvv6pm48aqqikhnwkdxny34qgl4qca9w1q06sx16")))

(define-public crate-xous-ipc-0.9.18 (c (n "xous-ipc") (v "0.9.18") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.18") (d #t) (k 0)))) (h "02cgjbnbjagah5dp7fdlzakw25yviwrw40b31nx4mvqg0iq86nnv")))

(define-public crate-xous-ipc-0.9.19 (c (n "xous-ipc") (v "0.9.19") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.19") (d #t) (k 0)))) (h "0jc7rdk31s9w8s0m6nnw1vqcvk46r47ji2xavsl2pfdl2is96ars")))

(define-public crate-xous-ipc-0.9.20 (c (n "xous-ipc") (v "0.9.20") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.20") (d #t) (k 0)))) (h "0q6hskbpcfalfix8jq7gnazzfard9czga5wf0f4q55lazxr6fhid")))

(define-public crate-xous-ipc-0.9.21 (c (n "xous-ipc") (v "0.9.21") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.21") (d #t) (k 0)))) (h "1g00bf5scfs4acqp135prwrrkwr3kw1s1kbmnwlf6fr22l1ylm0j")))

(define-public crate-xous-ipc-0.9.22 (c (n "xous-ipc") (v "0.9.22") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.22") (d #t) (k 0)))) (h "1wy1kwl8lfhyqjy6dc6h7w734hygyyqxdl718i934zga69yxkisy")))

(define-public crate-xous-ipc-0.9.23 (c (n "xous-ipc") (v "0.9.23") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.23") (d #t) (k 0)))) (h "0ccrc564yjlw2q4v2g1hk8bzzdgv9ccywwi7jaiim283xmmhagpd")))

(define-public crate-xous-ipc-0.9.24 (c (n "xous-ipc") (v "0.9.24") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.24") (d #t) (k 0)))) (h "0p5mhsrd6fh31813vaxn1yi4x3796a78v79l4ygb2bmbkpszz0iz")))

(define-public crate-xous-ipc-0.9.25 (c (n "xous-ipc") (v "0.9.25") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.25") (d #t) (k 0)))) (h "1kkszaggmj61z5dv1mxyk0i3vhc30y0wjdwjqycsdd55ranjnfkj")))

(define-public crate-xous-ipc-0.9.26 (c (n "xous-ipc") (v "0.9.26") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.26") (d #t) (k 0)))) (h "1xmkgd23gjb18j765vsvbc8p2slqlbp8c3ci6l9rz8nfqm0jak78")))

(define-public crate-xous-ipc-0.9.27 (c (n "xous-ipc") (v "0.9.27") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.27") (d #t) (k 0)))) (h "1l2d31jwr9r0wgbp7zmbhrblw518qybm06bvblcr221mw5rlc3sk")))

(define-public crate-xous-ipc-0.9.28 (c (n "xous-ipc") (v "0.9.28") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.28") (d #t) (k 0)))) (h "1nla020gv7i5z6as26fsws9scfbk214b1w12fy0i7868f68w8j1m")))

(define-public crate-xous-ipc-0.9.29 (c (n "xous-ipc") (v "0.9.29") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.29") (d #t) (k 0)))) (h "0pqfc0y7gzgy0s4zl21vjryql5fy4xlxdgc746p72f49y07gnc45")))

(define-public crate-xous-ipc-0.9.30 (c (n "xous-ipc") (v "0.9.30") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.30") (d #t) (k 0)))) (h "0rkhqjx5645vyfxkp0ccf8rq95abxbri5g9xnqkhlz4n9vkgjbq6")))

(define-public crate-xous-ipc-0.9.31 (c (n "xous-ipc") (v "0.9.31") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.31") (d #t) (k 0)))) (h "16jgz5dvkjfa5hycsqi96v1wc451qfy1q8ap8cf9sc6cc1zhb9g1")))

(define-public crate-xous-ipc-0.9.32 (c (n "xous-ipc") (v "0.9.32") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.32") (d #t) (k 0)))) (h "11r4agwik8ysizkxljl28rvkh64ghh940s03s6axh0ayrfpa30bz")))

(define-public crate-xous-ipc-0.9.33 (c (n "xous-ipc") (v "0.9.33") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.33") (d #t) (k 0)))) (h "1xw4djklddd45mi2ps1dlqvbq7chyv9q6ddzf7v88xnjaiwhxc0z")))

(define-public crate-xous-ipc-0.9.34 (c (n "xous-ipc") (v "0.9.34") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.34") (d #t) (k 0)))) (h "07mn5kkm6hgkqbqkzv0bvlil9y5rgyn03l13h09avqbdjfffdjxh")))

(define-public crate-xous-ipc-0.9.35 (c (n "xous-ipc") (v "0.9.35") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.35") (d #t) (k 0)))) (h "1zb62y8h3v63y1mrbhaza4mczc5c49ski7c9iwvydw18yrsbp2il")))

(define-public crate-xous-ipc-0.9.36 (c (n "xous-ipc") (v "0.9.36") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.36") (d #t) (k 0)))) (h "0s496lslcf5qq08kgi6yrqf1jrcii8zjdbwa4fzi3phqm2jzfcs7")))

(define-public crate-xous-ipc-0.9.37 (c (n "xous-ipc") (v "0.9.37") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.37") (d #t) (k 0)))) (h "1slpkhbh85p0jx19mdfm4hd4q6yc290763f90g7x31n60x7d7x9v")))

(define-public crate-xous-ipc-0.9.38 (c (n "xous-ipc") (v "0.9.38") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.38") (d #t) (k 0)))) (h "0sn8vr26wps7bgngv290bzi5bvgkrzwdy1fr2dc8wmwj3lbmcznc")))

(define-public crate-xous-ipc-0.9.39 (c (n "xous-ipc") (v "0.9.39") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.39") (d #t) (k 0)))) (h "0nxzqvsdbp3sn8165ys7lb555yqc0ns1wjh6zjfdq7krsrnza69l")))

(define-public crate-xous-ipc-0.9.40 (c (n "xous-ipc") (v "0.9.40") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.40") (d #t) (k 0)))) (h "0861bjwr326s3f3i77a2iajhqp7la8816jq4ydmxf2syqnyvgy48")))

(define-public crate-xous-ipc-0.9.41 (c (n "xous-ipc") (v "0.9.41") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.41") (d #t) (k 0)))) (h "1nsxk4pb2r1p2z64pagkyi6cjmz09iqck1k9003ympcw2bvmhcc4")))

(define-public crate-xous-ipc-0.9.42 (c (n "xous-ipc") (v "0.9.42") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.42") (d #t) (k 0)))) (h "13ynlkixp3hcgip4hl1mwdf531naizmf1b9ikpma019klrfghlmw")))

(define-public crate-xous-ipc-0.9.43 (c (n "xous-ipc") (v "0.9.43") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.43") (d #t) (k 0)))) (h "0610pxa2zdz2bb02jx2gsm0akbgfh80qmbzzisnr5k21jhqhr5vm")))

(define-public crate-xous-ipc-0.9.44 (c (n "xous-ipc") (v "0.9.44") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.44") (d #t) (k 0)))) (h "0rsdxmwhppkdhnhdkrf3wwc8iwsnlp3k0v8bf1psam9dch4gw86m")))

(define-public crate-xous-ipc-0.9.45 (c (n "xous-ipc") (v "0.9.45") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.45") (d #t) (k 0)))) (h "1pjz9gm8fsniiqcz284fm35y72bsbrhv89znc05yvvjlwqvyj2si")))

(define-public crate-xous-ipc-0.9.46 (c (n "xous-ipc") (v "0.9.46") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.46") (d #t) (k 0)))) (h "06mcb6dxkf5fr79c1zcd755cmig4s3zdbzy4xsy4cvm1m8a0k020")))

(define-public crate-xous-ipc-0.9.47 (c (n "xous-ipc") (v "0.9.47") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.47") (d #t) (k 0)))) (h "0py6fmk7kpz63mhc7wbj3vknqrdsfcsyk3j8l96phgi6g4ma1ngg")))

(define-public crate-xous-ipc-0.9.48 (c (n "xous-ipc") (v "0.9.48") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.48") (d #t) (k 0)))) (h "0d1jjq36aaf2jqy3c8wli3vm3jkjsh2njy86jrnljv9hswndvhcv")))

(define-public crate-xous-ipc-0.9.49 (c (n "xous-ipc") (v "0.9.49") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.49") (d #t) (k 0)))) (h "0hf2lrkn5kyj18c3rybbpnznzdaaawcj7f2h8r7dzn6rljmr9r83")))

(define-public crate-xous-ipc-0.9.50 (c (n "xous-ipc") (v "0.9.50") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.50") (d #t) (k 0)))) (h "10j3618dq8g0g3dplarh0iy8kwjva5fbyf22a6dchcihgihh7awi")))

(define-public crate-xous-ipc-0.9.51 (c (n "xous-ipc") (v "0.9.51") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.51") (d #t) (k 0)))) (h "0462kgpydhx99plpmljv18n9lvpiqwzgqh4qam4lqpids34ar7jl")))

(define-public crate-xous-ipc-0.9.52 (c (n "xous-ipc") (v "0.9.52") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.52") (d #t) (k 0)))) (h "01kpadhhsdrg47yf39rzq3388xsqlvrlkak19ig1g3b2d2rawa1b")))

(define-public crate-xous-ipc-0.9.53 (c (n "xous-ipc") (v "0.9.53") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.53") (d #t) (k 0)))) (h "11z6xw59m5qhi5kc40d832z8zk868xrmzw0wjbvjjcdlpmm5cfwb")))

(define-public crate-xous-ipc-0.9.54 (c (n "xous-ipc") (v "0.9.54") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.54") (d #t) (k 0)))) (h "06ha3a3zq82c4rfy0pc4l3jlj8jkqkkxx4929qp29mnbv8cd7qay")))

(define-public crate-xous-ipc-0.9.55 (c (n "xous-ipc") (v "0.9.55") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.55") (d #t) (k 0)))) (h "1dmv5a3vz9yk17g546nnlk1wrykwh2blcbq5mj4z4y60fs9xb1jc")))

(define-public crate-xous-ipc-0.9.56 (c (n "xous-ipc") (v "0.9.56") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.56") (d #t) (k 0)))) (h "0j41pgcnw34brvm51jv6wd7hn4ri9f5mvp6nwj8isayr63n3qnws")))

(define-public crate-xous-ipc-0.9.57 (c (n "xous-ipc") (v "0.9.57") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.57") (d #t) (k 0)))) (h "0rrsspf5jg8489yfyn82msmnldbj7444465if4nmxgb5m8m8hk4k")))

(define-public crate-xous-ipc-0.9.58 (c (n "xous-ipc") (v "0.9.58") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.58") (d #t) (k 0)))) (h "0pbc49ncybsxn3lgdva9zk5qczcmq0nfw0sl03i1bw8l2dc82l1g")))

(define-public crate-xous-ipc-0.9.59 (c (n "xous-ipc") (v "0.9.59") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.59") (d #t) (k 0)))) (h "081i8480lj2xzj38rk73jpxr88g8c64p1incfh2bggl5il7n5hzr")))

(define-public crate-xous-ipc-0.9.60 (c (n "xous-ipc") (v "0.9.60") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.60") (d #t) (k 0)))) (h "01qaaqwnvmsyr4jrgb6iaznb4b991c7y351yaafab5y8zsijhcdk")))

(define-public crate-xous-ipc-0.9.61 (c (n "xous-ipc") (v "0.9.61") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "rkyv") (r "^0.4.3") (f (quote ("const_generics"))) (k 0)) (d (n "xous") (r "^0.9.61") (d #t) (k 0)))) (h "1xm4jpykqnf395klhab187ifns1bdrw5r036iwgjvwhbhfirg5sg")))

