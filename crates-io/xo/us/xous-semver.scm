(define-module (crates-io xo us xous-semver) #:use-module (crates-io))

(define-public crate-xous-semver-0.1.0 (c (n "xous-semver") (v "0.1.0") (h "1w0b81swy2vsd2j5ahj61a5r02pvbsra2gkhnvxcjfw4dclj32xa") (f (quote (("std") ("default" "std"))))))

(define-public crate-xous-semver-0.1.1 (c (n "xous-semver") (v "0.1.1") (h "0xyw8h58hz93xdmxf0pbwwddkl84871vws6bcb9nvsyyw0q1mk8f") (f (quote (("std") ("default" "std"))))))

(define-public crate-xous-semver-0.1.2 (c (n "xous-semver") (v "0.1.2") (h "1b8mi80bp6039xjbq3l0zdzkbxv9a97475y5q6aligp7nj5jglvf") (f (quote (("std") ("default" "std"))))))

(define-public crate-xous-semver-0.1.3 (c (n "xous-semver") (v "0.1.3") (h "130p8nizrpjp5176kjgs6snngbmxi0xqsb3zdxvdy3xbmrzdjyab") (f (quote (("std") ("default" "std"))))))

