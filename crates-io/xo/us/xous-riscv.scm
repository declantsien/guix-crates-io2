(define-module (crates-io xo us xous-riscv) #:use-module (crates-io))

(define-public crate-xous-riscv-0.5.6 (c (n "xous-riscv") (v "0.5.6") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.2.5") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)))) (h "06j61dgylcf3ahs5lq0kgmlpx3g2jfxcihjw6yy49yb9asym3v33") (f (quote (("inline-asm"))))))

