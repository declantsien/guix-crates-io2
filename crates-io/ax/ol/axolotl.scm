(define-module (crates-io ax ol axolotl) #:use-module (crates-io))

(define-public crate-axolotl-0.2.0 (c (n "axolotl") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "sailfish") (r "^0.3.3") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "02xw0zj653h87q1iln4rdpj3jkajsjqib7lhl6h9947kkq201sp0")))

