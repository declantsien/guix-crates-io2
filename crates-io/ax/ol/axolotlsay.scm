(define-module (crates-io ax ol axolotlsay) #:use-module (crates-io))

(define-public crate-axolotlsay-0.0.0 (c (n "axolotlsay") (v "0.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "11sfjnhd5izq7338x7pl84j2asxf5ydg595i6kgfgwdvpvl7dxd5")))

(define-public crate-axolotlsay-0.1.0 (c (n "axolotlsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1z54gv59mglcrakqpynyqx1q0hzdwxg2d3y0qmk0sich4a1k0b5m")))

(define-public crate-axolotlsay-0.2.0 (c (n "axolotlsay") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0gbis1sg4fjapj82d19hh1x7ipgmnzw4nzqvc6n5qyncg00pdfc1")))

(define-public crate-axolotlsay-0.2.2 (c (n "axolotlsay") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "18nipjh8zxs98h19g675w3gqkp2m6dcj8ia0g77pkvf3khcajbi8")))

