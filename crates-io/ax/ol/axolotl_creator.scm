(define-module (crates-io ax ol axolotl_creator) #:use-module (crates-io))

(define-public crate-axolotl_creator-0.1.0 (c (n "axolotl_creator") (v "0.1.0") (h "1fwvimmdabw6n0k0lklh105gajka3x863s6m6amhvm432dnn0jhs")))

(define-public crate-axolotl_creator-0.1.1 (c (n "axolotl_creator") (v "0.1.1") (h "1mx2qhjkmkrc2ynj2vzmy4f74cfagpq9im3g52j0kgghbwg88hrp")))

(define-public crate-axolotl_creator-0.1.2 (c (n "axolotl_creator") (v "0.1.2") (h "18qs32sdmqnh04cgwanwxkhbi8fard90xrk8l52rrkj197nyfn7y")))

(define-public crate-axolotl_creator-0.1.3 (c (n "axolotl_creator") (v "0.1.3") (d (list (d (n "savefile") (r "^0.7.4") (d #t) (k 0)) (d (n "savefile-derive") (r "^0.7") (d #t) (k 0)))) (h "014ff5rrjyr3mx6pcgm7mvgwbzfinyxqzl6m73ciyyamdr1hnqby")))

(define-public crate-axolotl_creator-0.1.4 (c (n "axolotl_creator") (v "0.1.4") (d (list (d (n "savefile") (r "^0.7.4") (d #t) (k 0)) (d (n "savefile-derive") (r "^0.7") (d #t) (k 0)))) (h "033d5gcb9bcr2mm7cq0gz9y91mdhfd2fsda776plf4vni3hcd5jq")))

(define-public crate-axolotl_creator-0.1.5 (c (n "axolotl_creator") (v "0.1.5") (d (list (d (n "savefile") (r "^0.7.4") (d #t) (k 0)) (d (n "savefile-derive") (r "^0.7") (d #t) (k 0)))) (h "1a4hqswf1fycf16xq878xqx00xnp7vliqj038n6628k05vy7zh14")))

