(define-module (crates-io ax op axoprocess) #:use-module (crates-io))

(define-public crate-axoprocess-0.1.0 (c (n "axoprocess") (v "0.1.0") (d (list (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "07gvi4nprhf36ifr8413rsxjyl6gbbkgz96bk80ih43x15j8yja1") (f (quote (("stdout_to_stderr_modern") ("default"))))))

(define-public crate-axoprocess-0.2.0 (c (n "axoprocess") (v "0.2.0") (d (list (d (n "miette") (r "^7.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0zfs9fkn04jj8r30i470mqv28m6dp8q636cphxjrbvwab0h6kr2d") (f (quote (("stdout_to_stderr_modern") ("default"))))))

