(define-module (crates-io ax um axum-server-timing) #:use-module (crates-io))

(define-public crate-axum-server-timing-0.1.0 (c (n "axum-server-timing") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0w6xfp48lalx96rzmb9lmg9ikw4xlk15ziirxzsqk5id12brz5fc")))

(define-public crate-axum-server-timing-0.2.0 (c (n "axum-server-timing") (v "0.2.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0bwg1p48siinffig44mm91jn4s5w19sapn54dqbhkrhjnhskdfv8")))

(define-public crate-axum-server-timing-0.3.0 (c (n "axum-server-timing") (v "0.3.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0bnmzm48qbc29q73m1w7g1ajqbvyq2d87ly20a9333al1y8b7ppc")))

(define-public crate-axum-server-timing-0.3.1 (c (n "axum-server-timing") (v "0.3.1") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0bp35hmf3lni7bmwclv49hcn35shf4ca865cxyz0r4isz4jx7wsg")))

