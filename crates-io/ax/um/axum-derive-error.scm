(define-module (crates-io ax um axum-derive-error) #:use-module (crates-io))

(define-public crate-axum-derive-error-0.1.0 (c (n "axum-derive-error") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls"))) (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "06v3pcwvbgrrmpl9j94sgsg2z075lyagvg2a6pmh51vpy4cdwzpp")))

