(define-module (crates-io ax um axum-helmet) #:use-module (crates-io))

(define-public crate-axum-helmet-0.1.0 (c (n "axum-helmet") (v "0.1.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "axum-test") (r "^14.2") (d #t) (k 2)) (d (n "helmet-core") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1a2k3v68n8ii8707apwyngnxwjhgzrzbpr1nn1567r3x4r83k29r")))

