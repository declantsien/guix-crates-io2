(define-module (crates-io ax um axum-trace-id) #:use-module (crates-io))

(define-public crate-axum-trace-id-0.1.0 (c (n "axum-trace-id") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0h9r8bzpw40ggw1w8bbwsz5h9lv7avri3mq909arw5b1b19ji50p")))

