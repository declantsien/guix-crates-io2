(define-module (crates-io ax um axum-forwarded-header) #:use-module (crates-io))

(define-public crate-axum-forwarded-header-0.1.0 (c (n "axum-forwarded-header") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.0") (k 0)))) (h "1qy1wv2xcmb99zqky2l9w59al6na3b58ddvy3m0kk4k9hqcnidri") (r "1.66")))

