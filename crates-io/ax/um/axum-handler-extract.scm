(define-module (crates-io ax um axum-handler-extract) #:use-module (crates-io))

(define-public crate-axum-handler-extract-0.1.0 (c (n "axum-handler-extract") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("matched-path"))) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 2)) (d (n "axum") (r "^0.6") (f (quote ("http1" "tokio"))) (k 2)) (d (n "hyper") (r "^0.14") (k 2)) (d (n "isahc") (r "^1") (k 2)) (d (n "portpicker") (r "^0.1") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "08zgr6fcvi5fb7hllc1wg264248cx2b539yhy80d0aw4n0bxbv4c")))

