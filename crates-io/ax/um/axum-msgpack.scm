(define-module (crates-io ax um axum-msgpack) #:use-module (crates-io))

(define-public crate-axum-msgpack-0.1.0 (c (n "axum-msgpack") (v "0.1.0") (d (list (d (n "axum") (r "^0.2.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sxpr3m49dy8q3p8viplq26ki91jbpza2bm3r6gc7bl2jawgj8lk")))

(define-public crate-axum-msgpack-0.2.1 (c (n "axum-msgpack") (v "0.2.1") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "~1.15") (f (quote ("full"))) (d #t) (k 2)))) (h "0vn423r8vzm9jmj4wqkqvsml8lf6pd7br0m9j34vxv9ydzl14gxl")))

(define-public crate-axum-msgpack-0.2.2 (c (n "axum-msgpack") (v "0.2.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "~1.17") (f (quote ("full"))) (d #t) (k 2)))) (h "03lpb4j5fykylrv8lb8142p07nzzsig5r6r8nvzjsvm0mfpimbj5")))

(define-public crate-axum-msgpack-0.3.0 (c (n "axum-msgpack") (v "0.3.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("full"))) (d #t) (k 2)))) (h "1yw64dqgq2wjzcgn6rrppf3ykdw42mvgqczzdyszcl2hnkhgqfj5")))

(define-public crate-axum-msgpack-0.4.0 (c (n "axum-msgpack") (v "0.4.0") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^1.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0nn55m0id6nxx6q87cbi9jv87gl05np1ln86fd7cqmdm5dr2ilqx")))

