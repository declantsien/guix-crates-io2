(define-module (crates-io ax um axum-jwt-auth) #:use-module (crates-io))

(define-public crate-axum-jwt-auth-0.1.1 (c (n "axum-jwt-auth") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers" "macros"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "0zn0p4j74driihjg09hb7y8kj7kq5qhl8yhd7rls12b3fy1jiymb")))

