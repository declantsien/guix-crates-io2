(define-module (crates-io ax um axum-prehandle) #:use-module (crates-io))

(define-public crate-axum-prehandle-0.1.0 (c (n "axum-prehandle") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "resp-result") (r "^0.2") (f (quote ("for-axum"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0zs4rss5j5k2kjj1adkrx8wv1d5l7f2xgs3rs7vz5i18i7wjhafr")))

(define-public crate-axum-prehandle-0.1.1 (c (n "axum-prehandle") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "resp-result") (r "^0.3") (f (quote ("for-axum"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1hwfmicywv3hh1zq0yhgb9sgdr36kacszmrc8jgp33gr45n4aqdm")))

(define-public crate-axum-prehandle-0.1.2 (c (n "axum-prehandle") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "resp-result") (r "^0.4") (f (quote ("for-axum"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "11i4n4mcqcplsw6hbr7mkps9k6in20wp7f9qy8g2n72ml1kghxyg")))

