(define-module (crates-io ax um axum-sqlite) #:use-module (crates-io))

(define-public crate-axum-sqlite-0.1.0 (c (n "axum-sqlite") (v "0.1.0") (d (list (d (n "axum") (r "~0.5") (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "r2d2") (r "~0.8") (k 0)) (d (n "r2d2_sqlite") (r "~0.20") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17fkrdrnw1i8ala23n9kmm9lsnsh34rlbpp5f5q0jgv0jdzp3xk5")))

