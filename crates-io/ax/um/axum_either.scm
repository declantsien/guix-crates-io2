(define-module (crates-io ax um axum_either) #:use-module (crates-io))

(define-public crate-axum_either-0.1.0 (c (n "axum_either") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "axum") (r "^0.5.13") (d #t) (k 2)) (d (n "axum-core") (r "^0.2.7") (d #t) (k 0)) (d (n "either") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "tokio-rustls" "rustls-tls"))) (k 2)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fryvwl2x6i0g2b8pdr0p1xz60g0yac3lrllzvg6v0l7px4c37ba") (f (quote (("into_either" "either") ("default" "into_either"))))))

