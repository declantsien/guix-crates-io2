(define-module (crates-io ax um axum-starter-macro) #:use-module (crates-io))

(define-public crate-axum-starter-macro-0.1.0 (c (n "axum-starter-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0icz6r81x3pkd8w0z421jylw320wgbmcvjrgglp69zxd3d5m8k8g")))

(define-public crate-axum-starter-macro-0.2.0 (c (n "axum-starter-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0l59vlvjh0f1ghgvrb1mkb0ak9vrc7gjgkzs7slc92vy37ixpfv6")))

(define-public crate-axum-starter-macro-0.2.1 (c (n "axum-starter-macro") (v "0.2.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1vixw4gh4rl1g0kxl95wmwhg4cizqc5ybm1dx5adkmmdid0c9fk4")))

(define-public crate-axum-starter-macro-0.3.0 (c (n "axum-starter-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0wkprfmqjsf02yh8lbmjv9ra2jxs8qkd02vfzq31hs17m8ldl7hr")))

(define-public crate-axum-starter-macro-0.4.0 (c (n "axum-starter-macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0g6icjc74hidgh4zhb9x07mybmzi2ccicfh58zbhnjdm8zcxq78q")))

(define-public crate-axum-starter-macro-0.5.0 (c (n "axum-starter-macro") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0zqmhzgmapjwhiyn5r1f93lynifca2a5768hvkf7qzbcs4q1d6wc")))

(define-public crate-axum-starter-macro-0.5.1 (c (n "axum-starter-macro") (v "0.5.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0i7afh96rjhnrknggdsiwznxkyrw9al3rfxc38d9rjdcdk8h95jn")))

(define-public crate-axum-starter-macro-0.6.0 (c (n "axum-starter-macro") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0jrnp6p1ngdj2skzaap1288gra9ap44z6hsgfrv97d7pvlg2cc6j")))

(define-public crate-axum-starter-macro-0.6.1 (c (n "axum-starter-macro") (v "0.6.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "12a4q03v7ybbwaiq16xgq5ncx9l4ggi7zjsrrv9janmissx3bwig")))

(define-public crate-axum-starter-macro-0.7.0 (c (n "axum-starter-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0c4gbjnm3kxcfdhv7bnjhfakxjznv4vwfq3fax0qschs59p2ygqh")))

(define-public crate-axum-starter-macro-0.8.0 (c (n "axum-starter-macro") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "visit"))) (d #t) (k 0)))) (h "1xl17hf5hn7qzgcdzzbr69sdjwj8rl91bcgmyjbdgyrdhwxg4bjl")))

(define-public crate-axum-starter-macro-0.9.0 (c (n "axum-starter-macro") (v "0.9.0") (d (list (d (n "axum-starter") (r "^0.8.0") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "visit"))) (d #t) (k 0)))) (h "0xnz5cx66xzibbcdp0x78d2yj60m16lhcr621742m4nalmblvqhf")))

(define-public crate-axum-starter-macro-0.10.0 (c (n "axum-starter-macro") (v "0.10.0") (d (list (d (n "axum-starter") (r "^0.8.0") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit-mut" "visit"))) (d #t) (k 0)))) (h "1p0fnihjicam9y10r4mc7dh4c79q4y2razh591hz5r3l5dymx0xg")))

