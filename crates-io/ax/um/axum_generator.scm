(define-module (crates-io ax um axum_generator) #:use-module (crates-io))

(define-public crate-axum_generator-0.1.0 (c (n "axum_generator") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "16qjn6nqbb1b39gmzk6apcl128qnzz8yl9iqw1wlmawqycg5vyba") (f (quote (("default" "axum") ("cli" "atty" "structopt") ("axum" "cli")))) (y #t)))

