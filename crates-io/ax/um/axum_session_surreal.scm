(define-module (crates-io ax um axum_session_surreal) #:use-module (crates-io))

(define-public crate-axum_session_surreal-0.1.0 (c (n "axum_session_surreal") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "axum_session") (r "^0.14.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock" "serde"))) (k 0)) (d (n "surrealdb") (r "^1.2.2") (d #t) (k 0)))) (h "0g3vjiylh77nki5965x1ls3g7y6mcid54yr6izyi2z3a7yhrcn6w")))

