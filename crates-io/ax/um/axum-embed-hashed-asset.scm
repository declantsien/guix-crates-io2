(define-module (crates-io ax um axum-embed-hashed-asset) #:use-module (crates-io))

(define-public crate-axum-embed-hashed-asset-0.1.0 (c (n "axum-embed-hashed-asset") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0") (f (quote ("mime-guess"))) (d #t) (k 0)))) (h "13c1wprpgh0qnzn9agapiqiq29pv2y7r97bxfirxadj7ssf58gk6")))

