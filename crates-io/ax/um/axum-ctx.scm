(define-module (crates-io ax um axum-ctx) #:use-module (crates-io))

(define-public crate-axum-ctx-0.1.0 (c (n "axum-ctx") (v "0.1.0") (d (list (d (n "axum-core") (r "^0.3.4") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1q6jsabd6zxm83p4smmn78was96v7c22a1fgaxqg02qpg8hw1r68")))

(define-public crate-axum-ctx-0.2.0 (c (n "axum-ctx") (v "0.2.0") (d (list (d (n "axum-core") (r "^0.4.3") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "05zcxcmlpqsfbpcj751dijp7fyw4pq7n5y45w5qyh030sly35w63")))

(define-public crate-axum-ctx-0.3.0 (c (n "axum-ctx") (v "0.3.0") (d (list (d (n "axum-core") (r "^0.4.3") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0j9yyncimckf5iik68mzzbbhjg9xk48nchw46nv8g0lvg1d467k0")))

(define-public crate-axum-ctx-0.4.0 (c (n "axum-ctx") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "axum") (r "^0.7.5") (f (quote ("json"))) (k 2)) (d (n "axum-core") (r "^0.4.3") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "15kjs5z2rgs5g4qr6y701y5gp1qhgkb5j2r5zzv5galm880sq0s4")))

