(define-module (crates-io ax um axum-embed-files) #:use-module (crates-io))

(define-public crate-axum-embed-files-0.1.0-beta.0 (c (n "axum-embed-files") (v "0.1.0-beta.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "axum-embed-files-core") (r "^0.1.0-beta.0") (o #t) (d #t) (k 0)) (d (n "axum-embed-files-macros") (r "^0.1.0-beta.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0hpapb5fzwpz7b221kfb93m5396gpgcy6l9379yf54hv8kj0jfkl") (f (quote (("serve-from-fs" "axum-embed-files-core" "axum-embed-files-macros/serve-from-fs") ("default"))))))

