(define-module (crates-io ax um axum-enum-response) #:use-module (crates-io))

(define-public crate-axum-enum-response-0.1.0 (c (n "axum-enum-response") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1jnx9kn2vra62787cgpky1qqf1y27f9zglxknpcb43l5fqbxm728")))

(define-public crate-axum-enum-response-0.1.1 (c (n "axum-enum-response") (v "0.1.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1k8ccvkw9x7njmh81iillnj285syryr5yzcysgw7j9y5gw1snkv2")))

(define-public crate-axum-enum-response-0.1.2 (c (n "axum-enum-response") (v "0.1.2") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1c9b497ri1m5nd6xwmw63426qf910fwyjck9fdlkw4r5j2hw8xrr")))

