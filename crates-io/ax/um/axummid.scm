(define-module (crates-io ax um axummid) #:use-module (crates-io))

(define-public crate-axummid-0.1.1 (c (n "axummid") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0shl2isg3nwkw37vwyb2nby92nsf7nxpns2gnrs6h8h1dspi873y")))

