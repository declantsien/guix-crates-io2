(define-module (crates-io ax um axum-cloudflare-adapter-macros) #:use-module (crates-io))

(define-public crate-axum-cloudflare-adapter-macros-0.1.0 (c (n "axum-cloudflare-adapter-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "09jqbjy0gh5ccawyhfcdrmcm6hv8cx50zl4aqk4n3gqsmzlwb02v") (r "1.67.1")))

(define-public crate-axum-cloudflare-adapter-macros-0.1.1 (c (n "axum-cloudflare-adapter-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19p00rh1znr3lavxndn6gjmnx66k6ccz7m588r700pa0cn26b6ix") (r "1.67.1")))

(define-public crate-axum-cloudflare-adapter-macros-0.2.0 (c (n "axum-cloudflare-adapter-macros") (v "0.2.0") (h "10hvba37cipshv4flix6p21487c8rzfz3chjd6vsgsfnfa0wgk0d") (r "1.63")))

