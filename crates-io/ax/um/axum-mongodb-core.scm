(define-module (crates-io ax um axum-mongodb-core) #:use-module (crates-io))

(define-public crate-axum-mongodb-core-0.1.0 (c (n "axum-mongodb-core") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00gyhvj4wfbccmxx1pq92hampklnd3ghiarfl7hcclfbwakb0vas")))

(define-public crate-axum-mongodb-core-0.1.1 (c (n "axum-mongodb-core") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "142cxmm9yarg4mayl2lc4yp8h447gvyqhjrb2wx3i3n65sl6c0vj")))

(define-public crate-axum-mongodb-core-1.0.0 (c (n "axum-mongodb-core") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00qfiknr6m63b9lq22rja9qyw1k8zliwj0rg7hmv98x166hj9sa7") (y #t)))

(define-public crate-axum-mongodb-core-1.0.1 (c (n "axum-mongodb-core") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12ryp5wgp6wmyqrmdb0l4h1d0cj43rl0gqq40700cbzf6fdi95kb") (y #t)))

(define-public crate-axum-mongodb-core-1.0.3 (c (n "axum-mongodb-core") (v "1.0.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1plnasqrrr82xlgcc67m41wvawqgkfcnh98ay88v98b7fpnln4h3") (y #t)))

(define-public crate-axum-mongodb-core-0.2.0 (c (n "axum-mongodb-core") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1fg7vzlh6sh6vmkapgybmmq5d89d1z5s69dr2qjwr06m4dscpfzw")))

(define-public crate-axum-mongodb-core-0.2.1 (c (n "axum-mongodb-core") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "019g4is7h9q6jjjs0446cqkiv9zxjxcjsxh515dzp5f9vhg0vlkg")))

(define-public crate-axum-mongodb-core-0.2.2 (c (n "axum-mongodb-core") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zkqy12kix9rwri1xzh4nxxmh2wwdkafppsgbhhxlswl4knp5l08")))

