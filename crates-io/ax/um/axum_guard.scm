(define-module (crates-io ax um axum_guard) #:use-module (crates-io))

(define-public crate-axum_guard-0.1.0 (c (n "axum_guard") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "18xm5j7ksk9fbpbdh2gsqyy4fwdjh5z5igpbirp9id6ri2g7yacd")))

