(define-module (crates-io ax um axum_session_mongo) #:use-module (crates-io))

(define-public crate-axum_session_mongo-0.1.0 (c (n "axum_session_mongo") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "axum_session") (r "^0.14.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock" "serde"))) (k 0)) (d (n "mongodb") (r "^2.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "01yjym02byra18fn6mqhlrw9a120k4z3852gaq8r2g19jbswa6dd")))

