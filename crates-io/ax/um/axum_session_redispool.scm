(define-module (crates-io ax um axum_session_redispool) #:use-module (crates-io))

(define-public crate-axum_session_redispool-0.1.0 (c (n "axum_session_redispool") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "axum_session") (r "^0.14.0") (d #t) (k 0)) (d (n "redis") (r "^0.25.0") (f (quote ("aio" "tokio-comp"))) (d #t) (k 0)) (d (n "redis_pool") (r "^0.4.0") (d #t) (k 0)))) (h "115kpavacwj1vghwkc8ijpl6nx9cz7nzkzi1hj64811sd0zx1sg3") (f (quote (("redis-clusterdb" "redis_pool/cluster" "redis/cluster-async"))))))

