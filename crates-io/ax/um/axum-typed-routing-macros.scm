(define-module (crates-io ax um axum-typed-routing-macros) #:use-module (crates-io))

(define-public crate-axum-typed-routing-macros-0.1.0 (c (n "axum-typed-routing-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04mgbd9fvwxdswd9axzzss41aib6gwgw43m73kjzjk0yxi5zwcjz")))

(define-public crate-axum-typed-routing-macros-0.1.1 (c (n "axum-typed-routing-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yap17r6k5k7g19m5mqazlgac3dxjajk0hglq4kkv4j2bg1l0p0c")))

(define-public crate-axum-typed-routing-macros-0.1.2 (c (n "axum-typed-routing-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hpk7cys9g7j0iii85ca1abl1wa4wycz48iq5qbrszmb3ah46bj3")))

(define-public crate-axum-typed-routing-macros-0.1.3 (c (n "axum-typed-routing-macros") (v "0.1.3") (d (list (d (n "aide") (r "^0.12") (f (quote ("axum"))) (d #t) (k 2)) (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gbvs44vj2lrqkjzkwpkh8krscm914ak5qy375l6098b676xyd9a")))

(define-public crate-axum-typed-routing-macros-0.2.0 (c (n "axum-typed-routing-macros") (v "0.2.0") (d (list (d (n "aide") (r "^0.13") (f (quote ("axum"))) (d #t) (k 2)) (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0582i11zm6x337v5fygnrbqbvsikkc85s911w72b4qj9hcf2q48r")))

(define-public crate-axum-typed-routing-macros-0.2.1 (c (n "axum-typed-routing-macros") (v "0.2.1") (d (list (d (n "aide") (r "^0.13") (f (quote ("axum"))) (d #t) (k 2)) (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yk2b4gdgvfcbdra8n6z6lnnf6pm2baxbn5rjzmp59ikn6i0rs0x")))

