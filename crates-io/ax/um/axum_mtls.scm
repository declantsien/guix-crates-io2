(define-module (crates-io ax um axum_mtls) #:use-module (crates-io))

(define-public crate-axum_mtls-1.0.0 (c (n "axum_mtls") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12ivhcshh453mi1mdxsiakwbja3i3qyw7yi8a5026c9k94py8hxx") (f (quote (("safer")))) (y #t)))

(define-public crate-axum_mtls-2.0.0 (c (n "axum_mtls") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "17306lccdhap9zrs8i7721nzz6nxjzd44ksynvqx6px6jgn8kbm1") (f (quote (("safer")))) (y #t)))

(define-public crate-axum_mtls-3.0.0 (c (n "axum_mtls") (v "3.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "08cx1kbsgyqhk4ad35fwhz87q2kk82lw3szghzq7jpwqkqigwlk5") (f (quote (("safer")))) (y #t)))

(define-public crate-axum_mtls-4.0.0 (c (n "axum_mtls") (v "4.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "08sqnnavqvhqr3r5rap5nlbfvlcjm1xfgjz7y1g5cr4vdmykcd7n") (f (quote (("safer")))) (y #t)))

(define-public crate-axum_mtls-5.0.0 (c (n "axum_mtls") (v "5.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1jlivkj0mna7j63dpsdmmgclkp3sywxkggpxm9rrbjln367cm4pz") (f (quote (("safer")))) (y #t)))

(define-public crate-axum_mtls-5.0.1 (c (n "axum_mtls") (v "5.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "axum-server") (r "^0.5") (f (quote ("tls-rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0pas9wqfdx6pkwfwi812634s0ipkp1mq3lpbn55yxljx8kvqmjmx") (f (quote (("safer")))) (y #t)))

