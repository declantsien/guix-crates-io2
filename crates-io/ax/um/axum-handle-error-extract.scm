(define-module (crates-io ax um axum-handle-error-extract) #:use-module (crates-io))

(define-public crate-axum-handle-error-extract-0.1.0 (c (n "axum-handle-error-extract") (v "0.1.0") (d (list (d (n "axum") (r "^0.3.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util" "timeout"))) (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1fdf5nhjph8lwff6wp27m8a1n9cmr840lgcpbb6b1cicxlv57yw8")))

