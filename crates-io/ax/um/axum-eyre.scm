(define-module (crates-io ax um axum-eyre) #:use-module (crates-io))

(define-public crate-axum-eyre-0.5.0 (c (n "axum-eyre") (v "0.5.0") (d (list (d (n "axum") (r "~0.5") (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "eyre") (r "~0.6") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1rkr20w353gbjvs4f90z46ns7fpwr8xbs986qrnsvxqjrw24zz6q") (y #t)))

(define-public crate-axum-eyre-0.5.1 (c (n "axum-eyre") (v "0.5.1") (d (list (d (n "axum") (r "~0.5") (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "eyre") (r "~0.6") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1p4373scghl4qbkvjcrj5if951c4r9qbcap84fkbyzw785a366hi") (y #t)))

