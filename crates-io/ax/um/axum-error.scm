(define-module (crates-io ax um axum-error) #:use-module (crates-io))

(define-public crate-axum-error-0.1.0 (c (n "axum-error") (v "0.1.0") (d (list (d (n "axum") (r "~0.5") (k 0)) (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "eyre") (r "~0.6") (k 0)) (d (n "fehler") (r "^1.0.0") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1zr3z7iysjqkxcc34g205g8liypqmaf2g4w94w5m2ifxxp8829mn")))

(define-public crate-axum-error-0.2.0 (c (n "axum-error") (v "0.2.0") (d (list (d (n "axum") (r "~0.6") (k 0)) (d (n "eyre") (r "~0.6") (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qd62zlzb0zd18w6iy215b966z5f9bkmvapsnlzjd8ff4gagb30g")))

