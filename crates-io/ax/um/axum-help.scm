(define-module (crates-io ax um axum-help) #:use-module (crates-io))

(define-public crate-axum-help-0.1.0 (c (n "axum-help") (v "0.1.0") (d (list (d (n "axum") (r "^0.5.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tower") (r "^0.4.12") (d #t) (k 0)))) (h "0ynq3mplab0hbdfz047yv1kab584kaz6mgq2nv54vl4x0z4qcb8z")))

(define-public crate-axum-help-0.1.1 (c (n "axum-help") (v "0.1.1") (d (list (d (n "axum") (r "^0.5.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tower") (r "^0.4.12") (d #t) (k 0)))) (h "0kp6mvq459jrya468rxjx2vshmnyirib271i09m3vqpvdiamp91z")))

(define-public crate-axum-help-0.1.2 (c (n "axum-help") (v "0.1.2") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tower") (r "^0.4.12") (d #t) (k 0)))) (h "01scv7zszdd3f14zswgqr9appfwci1p65frshrdl239dpwqdvg0g")))

(define-public crate-axum-help-0.1.3 (c (n "axum-help") (v "0.1.3") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tower") (r "^0.4.12") (d #t) (k 0)))) (h "0f936yi87jcjbhqshwms98gxk5l07prqddr5i1s1pagn55xa3j7c")))

