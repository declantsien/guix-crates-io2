(define-module (crates-io ax um axum-embed-files-core) #:use-module (crates-io))

(define-public crate-axum-embed-files-core-0.1.0-beta.0 (c (n "axum-embed-files-core") (v "0.1.0-beta.0") (d (list (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "13wdxfyl80a9zd1k4c7g936ss89qsngrfrxn05ffkl9avnp5h92j")))

