(define-module (crates-io ax um axum_static_include) #:use-module (crates-io))

(define-public crate-axum_static_include-0.2.0 (c (n "axum_static_include") (v "0.2.0") (d (list (d (n "direx") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "sha256") (r "^1.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11izzvyaxqslpqkwcp0q3vi049cmdy2indkkkmva7hihmpmgy525")))

(define-public crate-axum_static_include-0.3.0 (c (n "axum_static_include") (v "0.3.0") (d (list (d (n "direx") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "sha256") (r "^1.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xyirqy2qlkmrsqp9hxy09p62cgq7a7cm9yfsym2rifh63lqywsz")))

