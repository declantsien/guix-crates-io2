(define-module (crates-io ax um axum_static) #:use-module (crates-io))

(define-public crate-axum_static-0.1.0 (c (n "axum_static") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "16vscjqxx61rkn2c9hlz03gg1z9ia342savw1n2gq7fn3cqdw3ph")))

(define-public crate-axum_static-0.1.1 (c (n "axum_static") (v "0.1.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "1z8gj53pkwz7zc7p9nf36abdkdmbyq45mwc3is0wq780wnjyyqhn")))

(define-public crate-axum_static-0.1.2 (c (n "axum_static") (v "0.1.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "1vpmkmvdnzv2ci3wgjzpfzchhdzydbdhg31j5jszc4983cvmq5fn")))

(define-public crate-axum_static-1.0.0 (c (n "axum_static") (v "1.0.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "1a4w6hmjx6dzhwz8mrl905apj7shdn2r692xxhjziz2hwncrkg8i")))

(define-public crate-axum_static-1.1.0 (c (n "axum_static") (v "1.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "05kl6jidwb3bmyi7k9fgngfzl7gr7n19akfdvcwa4nhlcbdwmiif")))

(define-public crate-axum_static-1.2.0 (c (n "axum_static") (v "1.2.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "13q4288h714blvjsh00zqim3vf3mibsdqsl5d6r922dnj5zfklzm")))

(define-public crate-axum_static-1.2.1 (c (n "axum_static") (v "1.2.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "0l57ql8xl7pv62ghl241k8k99nx1jzkcr0bnaav4ni7xbvswwkfx")))

(define-public crate-axum_static-1.2.2 (c (n "axum_static") (v "1.2.2") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "1rf2hq408svx8rfs3343cvi3vwqpsmqphkgwm6xk771pgvmmgll5")))

(define-public crate-axum_static-1.2.3 (c (n "axum_static") (v "1.2.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "011cdgf38hsxkq9f3cx8zqq7rmvajw5azp1k1qy2ikk19yk6axhq")))

(define-public crate-axum_static-1.6.0 (c (n "axum_static") (v "1.6.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.5") (f (quote ("fs"))) (d #t) (k 0)))) (h "181pp9i5lppqsgi6h6va1kh4536x2lcbs55g65nkj4sbayg9lxh7")))

(define-public crate-axum_static-1.7.0 (c (n "axum_static") (v "1.7.0") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0b40g6lcwz3cs5dcjd0s2h1wf6m757i9n86h903kqa7kh4d6v59i")))

(define-public crate-axum_static-1.7.1 (c (n "axum_static") (v "1.7.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0vjx8k9fc3fzc9nb6v3mgdd563lvkjcqyx245v46nndxx8m4s4rx")))

