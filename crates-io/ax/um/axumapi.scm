(define-module (crates-io ax um axumapi) #:use-module (crates-io))

(define-public crate-axumapi-0.1.0 (c (n "axumapi") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "^0.20.0") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0m7q3yahl3jylkqn1gavbz6042imhri8s8plpkp7b19s95s7g2pj")))

(define-public crate-axumapi-0.1.1 (c (n "axumapi") (v "0.1.1") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "11qa77sfm1zs1caiq2xfi5wy74fnzammj33hvnwc7kskcbggdm5b")))

(define-public crate-axumapi-0.1.2 (c (n "axumapi") (v "0.1.2") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0v9grr7sb0bhk6j9l020hqnp4b0drwjbhlv233pxpmlpafpf81hx")))

(define-public crate-axumapi-0.1.3 (c (n "axumapi") (v "0.1.3") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "1qv4fp5yx0fs3yg5gscjqkzwzwj87fly7g5cqlgqi6n18r73kvqp")))

(define-public crate-axumapi-0.1.4 (c (n "axumapi") (v "0.1.4") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "142gg0a4vhya2ivcfbywd7d5585hfmbf8f8xximdcj2lrn6z2whd")))

