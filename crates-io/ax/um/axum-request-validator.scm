(define-module (crates-io ax um axum-request-validator) #:use-module (crates-io))

(define-public crate-axum-request-validator-0.1.0 (c (n "axum-request-validator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.6") (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http-request-validator") (r "^0.1") (d #t) (k 0)) (d (n "hyper-request-validator") (r "^0.1") (d #t) (k 0)))) (h "0k3qbja4jr1r3ybspxlgmyhl6vm4znp5mwc0qpsh5cwam2vgz705")))

