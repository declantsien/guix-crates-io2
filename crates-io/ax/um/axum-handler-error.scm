(define-module (crates-io ax um axum-handler-error) #:use-module (crates-io))

(define-public crate-axum-handler-error-0.1.0 (c (n "axum-handler-error") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "07s2df0xc544xk8mlh985jix574adai0ssq8zb7xndvsgsnlr57b") (y #t)))

(define-public crate-axum-handler-error-0.1.1 (c (n "axum-handler-error") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0w5ckkgdqrd0wbsf6f2aw5k0mi01li24wpp0lq5vs3jm8qyc1drr")))

