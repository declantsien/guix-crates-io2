(define-module (crates-io ax um axum-embed-files-macros) #:use-module (crates-io))

(define-public crate-axum-embed-files-macros-0.1.0-beta.0 (c (n "axum-embed-files-macros") (v "0.1.0-beta.0") (d (list (d (n "axum-embed-files-core") (r "^0.1.0-beta.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1m66zz1bdpqi1q6xxnf2233r93sgpninw85v7d8cgch2fkrblsm0") (f (quote (("serve-from-fs") ("default"))))))

