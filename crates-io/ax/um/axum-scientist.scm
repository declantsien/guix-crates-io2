(define-module (crates-io ax um axum-scientist) #:use-module (crates-io))

(define-public crate-axum-scientist-0.1.0 (c (n "axum-scientist") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "axum") (r "^0.6.1") (d #t) (k 2)) (d (n "axum-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)))) (h "0f52i25jq01dgbmgx6i69i17kyb6mnnn0886wnh0zmn7y3h2g2dc")))

