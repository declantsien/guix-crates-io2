(define-module (crates-io ax um axum-resp-result-macro) #:use-module (crates-io))

(define-public crate-axum-resp-result-macro-0.6.0 (c (n "axum-resp-result-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "10h95nl2arfyq1smknm0vfsk9rsc41fadnqjm1cbhz3cgywxf60l")))

(define-public crate-axum-resp-result-macro-0.6.1 (c (n "axum-resp-result-macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "01m8c0pwiram45qbydfk74vf12a95y0qi8wnkkn8mq0pmjx1wq98")))

(define-public crate-axum-resp-result-macro-0.6.2 (c (n "axum-resp-result-macro") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "16n5w1xhl31saxbjkvswgyyskramyxw1mrb61yvjrhjc744piqj5")))

(define-public crate-axum-resp-result-macro-0.6.3 (c (n "axum-resp-result-macro") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "05qmr7aib8jiv996yxnz517xf0ah22jwh2hh0zbqdbamaprvfjp9")))

(define-public crate-axum-resp-result-macro-0.7.0 (c (n "axum-resp-result-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w0rwk4s9qmx8m5l3rjvdlwhvxc66qbz0g9sjy1bf5kx54kn0xrm") (f (quote (("extra-error"))))))

(define-public crate-axum-resp-result-macro-0.7.1 (c (n "axum-resp-result-macro") (v "0.7.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1iwqrxz780c8vpc0nbf6h9vqzqr4g1vbkfn4q0czahb00zpbdxbw") (f (quote (("extra-error"))))))

