(define-module (crates-io ax um axum-option) #:use-module (crates-io))

(define-public crate-axum-option-0.1.0 (c (n "axum-option") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.12") (d #t) (k 0)) (d (n "axum-debug") (r "^0.3.3") (d #t) (k 2)) (d (n "axum-extra") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "axum-macros") (r "^0.3.7") (d #t) (k 2)) (d (n "jwt-authorizer") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "jwt-authorizer") (r "^0.8.1") (d #t) (k 2)) (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1mr0vibrxrz7qmk15zk89wlvhnvrg78fc2vbpnr5977bm58w99l6") (f (quote (("headers" "axum/headers"))))))

