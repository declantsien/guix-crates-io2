(define-module (crates-io ax um axum-htmx) #:use-module (crates-io))

(define-public crate-axum-htmx-0.1.0 (c (n "axum-htmx") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (k 0)))) (h "1zmr1h8clfkjas987zagfbkifb900xv9wrm0ijal3c4h9q71hixs")))

(define-public crate-axum-htmx-0.2.0 (c (n "axum-htmx") (v "0.2.0") (d (list (d (n "axum") (r "^0.6") (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (k 0)))) (h "1bxhfx802zbr4cmfn1gpa00dsmmkb1jm2dn8qnj3w0rzh8ag4mh2") (f (quote (("guards" "tower" "futures-core" "pin-project-lite") ("default"))))))

(define-public crate-axum-htmx-0.3.0 (c (n "axum-htmx") (v "0.3.0") (d (list (d (n "axum") (r "^0.6") (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (k 0)))) (h "0vxsq4pwxm2jcjk4gii8bwp590yrv65qaw8lrk6jx7f6as07x71b") (f (quote (("guards" "tower" "futures-core" "pin-project-lite") ("default"))))))

(define-public crate-axum-htmx-0.3.1 (c (n "axum-htmx") (v "0.3.1") (d (list (d (n "axum") (r "^0.6") (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (k 0)))) (h "0d9mdjvdldd31k7bk29n346askihfp4fmmghxdrw8aa8n46m72jq") (f (quote (("guards" "tower" "futures-core" "pin-project-lite") ("default"))))))

(define-public crate-axum-htmx-0.4.0 (c (n "axum-htmx") (v "0.4.0") (d (list (d (n "axum") (r "^0.6") (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (k 0)))) (h "0iv7p4yiqgjixqhc6s87yfdiqm8hamqa8mcikpivp89wmi59wjgy") (f (quote (("guards" "tower" "futures-core" "pin-project-lite") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-axum-htmx-0.5.0 (c (n "axum-htmx") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum") (r "^0.7") (k 2)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "http") (r "^1.0") (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tower") (r "^0.4") (o #t) (k 0)))) (h "07lh36gz23sz2lcr8aj7wkg5930r9g59pavcl1g6wjq9vhghbxs0") (f (quote (("unstable") ("guards" "tower" "futures-core" "pin-project-lite") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

