(define-module (crates-io ax um axum_tool) #:use-module (crates-io))

(define-public crate-axum_tool-0.0.1 (c (n "axum_tool") (v "0.0.1") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "04fvvnm8lsyjrfdz6y77l7yvrgqvnds84k2aw3h0igcga0lw6wy8")))

