(define-module (crates-io ax um axum-toml) #:use-module (crates-io))

(define-public crate-axum-toml-0.1.0 (c (n "axum-toml") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("headers"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "01yrffnv1135rmbcby5rayrxqr57ndq02z3l4c0hgl7svxg321z0")))

