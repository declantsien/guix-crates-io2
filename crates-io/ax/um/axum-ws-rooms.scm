(define-module (crates-io ax um axum-ws-rooms) #:use-module (crates-io))

(define-public crate-axum-ws-rooms-0.1.0 (c (n "axum-ws-rooms") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0k488qisnz51mjj2yddbna0kgjw2fmjsc2pg2kcsvp7kpkc8b43w")))

(define-public crate-axum-ws-rooms-0.2.0 (c (n "axum-ws-rooms") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "17iwkcp111qls5qmcij56ni24vyg574wm2fbpxpiyc4jmhyw36jk")))

(define-public crate-axum-ws-rooms-0.3.0 (c (n "axum-ws-rooms") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xnrxjz48lpv02hj9aldsqa1wvhavgxzr6kzjl2xw2xr20bwjg0f")))

(define-public crate-axum-ws-rooms-0.4.0 (c (n "axum-ws-rooms") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "18yh8i3sq141ik5dkmrym669l65b898iir0iakiy33dx2nhpdvik")))

(define-public crate-axum-ws-rooms-0.5.0 (c (n "axum-ws-rooms") (v "0.5.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "0dg5zzqb52fhfh57gs58ngiqs3fq712jhhzk9mvb5il4bkvk0md7")))

(define-public crate-axum-ws-rooms-0.5.1 (c (n "axum-ws-rooms") (v "0.5.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "14zzx6kjsdcw9liz2cnpl6fsrvl6s0kalg0rbkr3mhlanbbbknlj")))

(define-public crate-axum-ws-rooms-0.5.2 (c (n "axum-ws-rooms") (v "0.5.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "16s6scwm93dhjx3a3y1h388z8c456qf4zkwmvl1c5zny5s2cbi44")))

(define-public crate-axum-ws-rooms-0.6.0 (c (n "axum-ws-rooms") (v "0.6.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "012l884nj7n5i2cvzpypzhfvnk893msgmga0d4jbj2gyjm4f46rn")))

