(define-module (crates-io ax um axum-auth) #:use-module (crates-io))

(define-public crate-axum-auth-0.1.0 (c (n "axum-auth") (v "0.1.0") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "19f4g693xklqsk512pmn8syiizfzpn1wm7ziqwa1h3j1zszsxf6z") (y #t)))

(define-public crate-axum-auth-0.1.1 (c (n "axum-auth") (v "0.1.1") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0xjjc99har234y3qy5jy1cm6p6a9f37jyglii0iqpj0ril5cv8dr")))

(define-public crate-axum-auth-0.2.0 (c (n "axum-auth") (v "0.2.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0x4g4cjg4vw18xgphd0vihjk1xyv447cq3wfz2nxfbdgyacw7357")))

(define-public crate-axum-auth-0.3.0 (c (n "axum-auth") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "axum-core") (r "^0.2.7") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "01wlsrzbln16p8znca9ijxgv532kjmacp6k0cr035cj7j6d0yxzr") (f (quote (("default" "auth-basic" "auth-bearer") ("auth-bearer") ("auth-basic"))))))

(define-public crate-axum-auth-0.4.0 (c (n "axum-auth") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "axum-core") (r "^0.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "163d3xhyzi5iisrzlq51l1fx4gbvimb22d7rc20spfkpbdj3f2v2") (f (quote (("default" "auth-basic" "auth-bearer") ("auth-bearer") ("auth-basic"))))))

(define-public crate-axum-auth-0.4.1 (c (n "axum-auth") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6") (d #t) (k 2)) (d (n "axum-core") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05m06pv9wicj0cjp11fvgjq3s4ka3mp7rssw5yg4w96x7p8cshpy") (f (quote (("default" "auth-basic" "auth-bearer") ("auth-bearer") ("auth-basic"))))))

(define-public crate-axum-auth-0.7.0 (c (n "axum-auth") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.7") (d #t) (k 2)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "02hjwhji03lrjsabkg22hnzk0paqvy0kbz7w2j3gcm2z30x12sc1") (f (quote (("default" "auth-basic" "auth-bearer") ("auth-bearer") ("auth-basic"))))))

