(define-module (crates-io ax um axum-bson) #:use-module (crates-io))

(define-public crate-axum-bson-0.1.0 (c (n "axum-bson") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "bson") (r "^2.9") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09zn6jqzijm4qgddww61s48p299g8x7nns70bq2cwvr2gqxn1q6j")))

(define-public crate-axum-bson-0.1.1 (c (n "axum-bson") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "axum-core") (r "^0.4") (d #t) (k 0)) (d (n "bson") (r "^2.9") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "http") (r "^1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07inl57q9f933hn772xv2sinkjkvc66bl8gqhrn0kzvpi9gnl6cj")))

