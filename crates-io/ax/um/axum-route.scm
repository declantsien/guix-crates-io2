(define-module (crates-io ax um axum-route) #:use-module (crates-io))

(define-public crate-axum-route-0.1.0 (c (n "axum-route") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03phynaq67r7jryxiq6vir9g3z21hhqvq0zcra8qsi6ahvyn6hjw")))

(define-public crate-axum-route-0.1.1 (c (n "axum-route") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mp6xzg67hydwrqpz67ywl9xqphqd6fr5w87v9sygqx65q9fsryy")))

