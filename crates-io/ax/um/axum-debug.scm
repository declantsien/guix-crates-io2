(define-module (crates-io ax um axum-debug) #:use-module (crates-io))

(define-public crate-axum-debug-0.0.0 (c (n "axum-debug") (v "0.0.0") (h "0ih6516i9q7gisgzr1zbbhyqxqh3mig0k0rmncf64bclb991rz46")))

(define-public crate-axum-debug-0.1.0 (c (n "axum-debug") (v "0.1.0") (d (list (d (n "axum") (r "^0.2") (d #t) (k 0)) (d (n "axum-debug-macros") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1ii92rkfspp5bp4xn8pn0nmx4fh0dq1rixjd68hh0ywcih9w5dq8")))

(define-public crate-axum-debug-0.2.0 (c (n "axum-debug") (v "0.2.0") (d (list (d (n "axum") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yr369zs4pjs7m1gn36v1cl95lqc667p9fr4fl06d8c4v5iqwz7c")))

(define-public crate-axum-debug-0.2.1 (c (n "axum-debug") (v "0.2.1") (d (list (d (n "axum") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yi1wrn9x2f61f1a82rrjb0qd5i769f6ha9238yiz6gsyfxy2mjf")))

(define-public crate-axum-debug-0.2.2 (c (n "axum-debug") (v "0.2.2") (d (list (d (n "axum") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "141j5ijsw0rbn588vsqs940a04hlr2hvrjw5fibr96sjsd4n0rpr")))

(define-public crate-axum-debug-0.3.0 (c (n "axum-debug") (v "0.3.0") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17kqrbsj592mnvd2h0ximjnyk0c1ikizm1wncjprd7s0739w3349")))

(define-public crate-axum-debug-0.3.1 (c (n "axum-debug") (v "0.3.1") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1n54arny6p4bzv6a29f3rfsy1gkww1dxzz5268zy67rdqhzvswyp")))

(define-public crate-axum-debug-0.3.2 (c (n "axum-debug") (v "0.3.2") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13vv6axznfz7c093c08whcx6vlki0bz5l26z2ndpydcdhb25vny9")))

(define-public crate-axum-debug-0.3.3 (c (n "axum-debug") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrqmrnyvwa6pih3ym71cdnfqsc3h5h309y2qglv73k0fvm1wj46")))

