(define-module (crates-io ax um axum_thiserror) #:use-module (crates-io))

(define-public crate-axum_thiserror-0.1.0 (c (n "axum_thiserror") (v "0.1.0") (d (list (d (n "axum") (r "0.7.*") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "0wxx00vvk8nclz8qdr2lx34p5zlgnq4nzp458clrm17qbhigns08")))

