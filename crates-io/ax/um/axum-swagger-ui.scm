(define-module (crates-io ax um axum-swagger-ui) #:use-module (crates-io))

(define-public crate-axum-swagger-ui-0.1.0 (c (n "axum-swagger-ui") (v "0.1.0") (d (list (d (n "axum") (r "^0.5.17") (d #t) (k 0)))) (h "1qxwnjjas6my6jg0kdrwk8xhvgcwz3s6ahnn2im26jzsb4mkm4f1")))

(define-public crate-axum-swagger-ui-0.2.0 (c (n "axum-swagger-ui") (v "0.2.0") (d (list (d (n "axum") (r "^0.6.0-rc.5") (d #t) (k 0)))) (h "0c6jj6a3sf8mdr0wwfah8hgavdrc8wzf5ahdyqa4r258amx93nyj")))

(define-public crate-axum-swagger-ui-0.2.1 (c (n "axum-swagger-ui") (v "0.2.1") (d (list (d (n "axum") (r "^0.6.0") (d #t) (k 0)))) (h "0znhqgm0j9slkwf94hg1b9a47j9rjkh7dpjyv4yjq38pz2wnwvv7")))

(define-public crate-axum-swagger-ui-0.2.2 (c (n "axum-swagger-ui") (v "0.2.2") (d (list (d (n "axum") (r "^0.6.1") (d #t) (k 0)))) (h "1a0x2p0ki4aq4dl699m78f1w7cbcczsh99n7nk8xgaf63c2809m4")))

(define-public crate-axum-swagger-ui-0.2.3 (c (n "axum-swagger-ui") (v "0.2.3") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)))) (h "0x5hdb38vkhp3fk6id83ywc5wp0ymlklm3fggn46p647kamlsizb")))

(define-public crate-axum-swagger-ui-0.3.0 (c (n "axum-swagger-ui") (v "0.3.0") (h "13wli39l0hz8ss5c0ifn6437nmb563hfl3h0pyk8pyay1qr7fn30")))

