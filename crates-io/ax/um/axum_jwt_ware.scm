(define-module (crates-io ax um axum_jwt_ware) #:use-module (crates-io))

(define-public crate-axum_jwt_ware-0.1.0 (c (n "axum_jwt_ware") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jp1hphziq24q9xh136i19fpjbcxq63j99ywq17h575360181vnv")))

(define-public crate-axum_jwt_ware-0.1.1 (c (n "axum_jwt_ware") (v "0.1.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1mv6agk2ggjnn0ladymm5jx0wbrv863hnc800wyb201yq8pkpr")))

(define-public crate-axum_jwt_ware-0.1.2 (c (n "axum_jwt_ware") (v "0.1.2") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01pw41rb418ahmz9al3barqb9q2zkpnh5m037lbd2wjyff7sqly2")))

(define-public crate-axum_jwt_ware-0.1.3 (c (n "axum_jwt_ware") (v "0.1.3") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kpcwpv2wjfq6j3zi050chyc5kknqm5qd3bm7azazhlal0spi6y6")))

(define-public crate-axum_jwt_ware-0.1.4 (c (n "axum_jwt_ware") (v "0.1.4") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07i538dfdf17v5zb8dqmydrl4gh3f8psn368i9d2bn2hi21hd3jl")))

(define-public crate-axum_jwt_ware-0.1.5 (c (n "axum_jwt_ware") (v "0.1.5") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vl64a7l1zzy1h4nv6ds3nlsc1jd8x6ngdbic8yfpvs42zr8pqy2")))

(define-public crate-axum_jwt_ware-0.1.6 (c (n "axum_jwt_ware") (v "0.1.6") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gd2fr7grpfz9wdph8wf93n2bdhqgzz3hkvag7q4h08djvcrldxz")))

(define-public crate-axum_jwt_ware-0.1.7 (c (n "axum_jwt_ware") (v "0.1.7") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8802spxapddxprbsigsl3bcii9kxscqzcjwsrvyynyqd01hv8p")))

