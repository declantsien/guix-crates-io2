(define-module (crates-io ax um axum_static_macro) #:use-module (crates-io))

(define-public crate-axum_static_macro-1.0.0 (c (n "axum_static_macro") (v "1.0.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0axfz8yg86h1fy0ylnlnwpqmixsdpgp12vlxnlvxlylxss84n9sc") (y #t)))

(define-public crate-axum_static_macro-1.0.1 (c (n "axum_static_macro") (v "1.0.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "146by20q0r5h0l7i3k4ig9l6viavxsy3a3km93c7dpfpzdfrw0p1") (y #t)))

(define-public crate-axum_static_macro-1.0.2 (c (n "axum_static_macro") (v "1.0.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0br7yks7xawaih7j8ha29bdvrg1rlwxfbarf05hgnyfq568frzws") (y #t)))

(define-public crate-axum_static_macro-1.0.3 (c (n "axum_static_macro") (v "1.0.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j4agqhpvfdpmvdbbyzllhyc01vmp6r35i0vwqb8xvrn7klwinm9") (y #t)))

(define-public crate-axum_static_macro-1.0.4 (c (n "axum_static_macro") (v "1.0.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10v3zp9sgp05hl29sw6b6h00q70djdwv2vpjp4d8sin4p3sdkgz1") (y #t)))

(define-public crate-axum_static_macro-1.0.5 (c (n "axum_static_macro") (v "1.0.5") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1d517sksa7bbczs443n95mbrphzvkzkin23yp4amf3ipm6xgvndw") (y #t)))

(define-public crate-axum_static_macro-1.0.6 (c (n "axum_static_macro") (v "1.0.6") (h "0s2vrdsn3wl50kw1fp6pbz6f4zqfvrscambaslgqcbrcl2phjbjb") (y #t)))

(define-public crate-axum_static_macro-1.0.7 (c (n "axum_static_macro") (v "1.0.7") (h "07y00hrjhji5sivxjgka7slycs7vqsb5ibsnvcdk2nm69fyrmm1g") (y #t)))

(define-public crate-axum_static_macro-1.0.8 (c (n "axum_static_macro") (v "1.0.8") (h "14dj2fjsyk2ik884m3xlw1hq95m56w7lfid060cjdx5by2j0wlkg") (y #t)))

(define-public crate-axum_static_macro-1.1.0 (c (n "axum_static_macro") (v "1.1.0") (h "0214m0529y83r3g7rw361ggdn14la1l0x2sz0rkglkwyrj0pnxng") (y #t)))

(define-public crate-axum_static_macro-1.1.1 (c (n "axum_static_macro") (v "1.1.1") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "032cj9l25583wdj2gxmhcq7b08lw04wgkjb4c1kcd23x6xg4mimg") (y #t)))

(define-public crate-axum_static_macro-1.1.2 (c (n "axum_static_macro") (v "1.1.2") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0p8ch10ck6m6sz476vg57lmwsr3gfcwbdcxh91z1nxax0qsxqsw5") (y #t)))

(define-public crate-axum_static_macro-1.1.3 (c (n "axum_static_macro") (v "1.1.3") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "content-types") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0h4z4w5frys8l0m7r4ry1shmwnjvph5mjjp3vfn319gbx1hckbsk") (y #t)))

(define-public crate-axum_static_macro-1.2.0 (c (n "axum_static_macro") (v "1.2.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 2)))) (h "1c5dj7affn0m3ncr6m38cmgv7jx4ypzaysg5k7p0ba8kn5c1bphl") (y #t)))

(define-public crate-axum_static_macro-1.2.1 (c (n "axum_static_macro") (v "1.2.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 2)))) (h "1378dpmmlhpz8zq0qql5pk1air1yj48xm5568kpddnn26pld0w4w") (y #t)))

