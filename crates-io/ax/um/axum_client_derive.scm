(define-module (crates-io ax um axum_client_derive) #:use-module (crates-io))

(define-public crate-axum_client_derive-0.1.0 (c (n "axum_client_derive") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.0-rc.1") (f (quote ("headers"))) (d #t) (k 2)) (d (n "bytes") (r "^1.2.1") (d #t) (k 2)) (d (n "headers") (r "^0.3.8") (d #t) (k 2)) (d (n "http") (r "^0.2.8") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05mbwhccbdldb14120x8bby0pj7z009pqz7dxc34r81yj4ab9p89") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

