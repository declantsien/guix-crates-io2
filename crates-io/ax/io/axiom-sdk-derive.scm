(define-module (crates-io ax io axiom-sdk-derive) #:use-module (crates-io))

(define-public crate-axiom-sdk-derive-0.1.0 (c (n "axiom-sdk-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1w1fhpa42a17ab1qi1a3vjh6g629b30hn04mqlndzl0m9psfbgd7")))

(define-public crate-axiom-sdk-derive-0.0.1 (c (n "axiom-sdk-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpmikf3ryghjm45z5r1s1rcdqpzmxj9i0b10rs0qnz5ac50pnlj") (y #t)))

(define-public crate-axiom-sdk-derive-0.1.1 (c (n "axiom-sdk-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "19syan1dlmdw57h8cfy1bz8gprfi7p4dr20a59p2m4347pa42x7l")))

