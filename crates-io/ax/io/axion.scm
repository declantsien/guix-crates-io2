(define-module (crates-io ax io axion) #:use-module (crates-io))

(define-public crate-axion-0.1.0 (c (n "axion") (v "0.1.0") (d (list (d (n "axion-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1vi270g1xn0qwm4xpf7r3mn6222w2y799svhmlj4g2fazqgqlc0a") (f (quote (("simd") ("math") ("default" "math" "simd"))))))

