(define-module (crates-io ax le axleharp) #:use-module (crates-io))

(define-public crate-axleharp-0.0.2 (c (n "axleharp") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "1x3m8bz6f63j5643mdl2hri6iz6484nkcxmh9xv4jvznsbk03bq3")))

