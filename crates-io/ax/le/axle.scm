(define-module (crates-io ax le axle) #:use-module (crates-io))

(define-public crate-axle-0.1.0 (c (n "axle") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "09b07yz3fxi8c75cgyjpyml57g8sygygm6498issaqh8x786380p") (f (quote (("toml_cfg" "toml" "serde_derive" "serde") ("default"))))))

(define-public crate-axle-0.1.1 (c (n "axle") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1m6pv9qj78hgmih9lmvlqndfrs5sc62xm21b5a6063mkg7a7768w") (f (quote (("toml_cfg" "toml" "serde_derive" "serde") ("default"))))))

