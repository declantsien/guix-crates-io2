(define-module (crates-io ax ge axgeom) #:use-module (crates-io))

(define-public crate-axgeom-0.1.0 (c (n "axgeom") (v "0.1.0") (h "1vz20p8mb5pn9zq22dnjkwf4igl18dmh7148cchxn2fviapn2li6")))

(define-public crate-axgeom-0.2.0 (c (n "axgeom") (v "0.2.0") (h "0c8r97d8q2avnfx4yrnvs0x3vlhah9qhra9p6dwwhqdq7nxarg30")))

(define-public crate-axgeom-1.0.0 (c (n "axgeom") (v "1.0.0") (h "1qvx2cw0sn3hp2zdd0pqx5wm3h8iyqf8w1vpxii88xj2q54qzza0")))

(define-public crate-axgeom-1.0.1 (c (n "axgeom") (v "1.0.1") (h "07w06gimxp4nxbljlf61cg2ingqr7afzla8cybw11yzf468c83s0")))

(define-public crate-axgeom-1.0.2 (c (n "axgeom") (v "1.0.2") (h "1gnpn19wsnfski3nq7l5i0qfpq9c4qgsgi632h3894q66s6612f2")))

(define-public crate-axgeom-1.0.3 (c (n "axgeom") (v "1.0.3") (h "02vr0vsdbzvkvzv8h6j86scdl8cqj0dqlln40zk7sa9bwj7ba8dp")))

(define-public crate-axgeom-1.0.4 (c (n "axgeom") (v "1.0.4") (h "0b7sg6a0ai4ll7s0dqdmlh15ik464370b0dlgyrccmgz9asil989")))

(define-public crate-axgeom-1.0.5 (c (n "axgeom") (v "1.0.5") (h "11ihyy89pi0f8jvfi08mibljnyr9cf6mljalb82kjb0bpm5vdr34")))

(define-public crate-axgeom-1.0.6 (c (n "axgeom") (v "1.0.6") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)))) (h "1lb2gjv109czl82l9nmlrmcg2ayihvk7kh8x1aidvy6gjnc21njm")))

(define-public crate-axgeom-1.0.7 (c (n "axgeom") (v "1.0.7") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)))) (h "13gy4v3n36906pj01z89vjblw1012bq9g9swyva5p7abrijb1wzf")))

(define-public crate-axgeom-1.1.0 (c (n "axgeom") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0kxhnwbm31yz644w92db1vkncq1s9cwi548sb01lrcg7parh5dxb")))

(define-public crate-axgeom-1.1.1 (c (n "axgeom") (v "1.1.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "02za132skk14kbylax22ixcckchmcz0n2zppgs7943qf1sp1vzxi")))

(define-public crate-axgeom-1.1.2 (c (n "axgeom") (v "1.1.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "1nqqy54lzpdk6d20f65c6phadm51gcgxvbbh2fc8iy397g6aqas4")))

(define-public crate-axgeom-1.2.0 (c (n "axgeom") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "1j1zs3748cv5fc462aq0fk5k537m9kpq6fa4l0ms5ypl8d8474ax")))

(define-public crate-axgeom-1.2.1 (c (n "axgeom") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0rlhsz4q05aqrl5fih05gzpv4rsjha2q5j464vd4kb9n98bc58dn")))

(define-public crate-axgeom-1.2.2 (c (n "axgeom") (v "1.2.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "1dkgb1waq92ykjsr7q942h2x575rvv9z6h71kc8rbm181nv91v0n")))

(define-public crate-axgeom-1.2.3 (c (n "axgeom") (v "1.2.3") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0j4wiaq6kpym01m7mfpgkc3v3j181lvj3vf8aifhxf0f2hg34wxb")))

(define-public crate-axgeom-1.2.4 (c (n "axgeom") (v "1.2.4") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "10fsrnf61aarkfhii3w7ykqfnrq8lgznndcx3drsq6di810rhis9")))

(define-public crate-axgeom-1.2.5 (c (n "axgeom") (v "1.2.5") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0x5v1w451k8rai6qripyfz5k517lr1sg3d65bi57lynwk8d784hw")))

(define-public crate-axgeom-1.2.6 (c (n "axgeom") (v "1.2.6") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "11a48c35fz055rb08cin5pd7rjs6cbw5bn7vhf66gmvvqmf5svh7")))

(define-public crate-axgeom-1.3.0 (c (n "axgeom") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0kag3g2yffb5ryh4ccdbfxh5kb9jx3c5f12a8swjki4fhcdi13ny")))

(define-public crate-axgeom-1.3.1 (c (n "axgeom") (v "1.3.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0ij3gi2w38z9h4kwml8hbjx4m4kaivj2rwds86cri46scs69vlid")))

(define-public crate-axgeom-1.3.2 (c (n "axgeom") (v "1.3.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)))) (h "0ymv9nmvsb259z95cvsmkn837ybh0p392pbbj9h7w7j941rjcinh")))

(define-public crate-axgeom-1.4.0 (c (n "axgeom") (v "1.4.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "07wpg5gcy53iiqnckrb1qbychl201ajgps92a7y6cykz4qfhgcz0")))

(define-public crate-axgeom-1.4.1 (c (n "axgeom") (v "1.4.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "0zfadq8q596w9y0sc6pl2an0mximykjm5fmdvqxgld618fbgj1kq")))

(define-public crate-axgeom-1.4.2 (c (n "axgeom") (v "1.4.2") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "0cllcddx145riyv2mdf3wi7j134q1299gbjsaxlns7n7s8lsvhhk")))

(define-public crate-axgeom-1.4.3 (c (n "axgeom") (v "1.4.3") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1f1xrh5iqdr7yysbprr1138pl5sx6k89nh0mjcylrlw5acf3bkqf")))

(define-public crate-axgeom-1.4.4 (c (n "axgeom") (v "1.4.4") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1wakb0zz159r3yr77w8v4w4aig4k1ky9immi1zdxhgna1d2rajh6")))

(define-public crate-axgeom-1.4.5 (c (n "axgeom") (v "1.4.5") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "02yx7vpral18j87qqv64dr536ggjffpblvasqqvg6c4948pdfr3z")))

(define-public crate-axgeom-1.5.0 (c (n "axgeom") (v "1.5.0") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "037z8j51r711w82ld0fr495hxk9icnyfay1qpj7j9nzy0p92xxbc")))

(define-public crate-axgeom-1.5.1 (c (n "axgeom") (v "1.5.1") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "090gflmyqqmkigmch93gziscskapk7iwj36w6j7xrmh0myalbg79")))

(define-public crate-axgeom-1.5.2 (c (n "axgeom") (v "1.5.2") (d (list (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1bn2f0b5f1d4ds2f3qpa73y2jg89dpx6yv1298jb00wimdkn9ma2")))

(define-public crate-axgeom-1.6.0 (c (n "axgeom") (v "1.6.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0fvslix6al7ijxcn7655wyqdgkjb6lvahkcdmxy9faykb1sihdgc")))

(define-public crate-axgeom-1.6.1 (c (n "axgeom") (v "1.6.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0mfjbiqiqalqgsckwhv9vbp8lbxd6cb194nyp8l64371wpk3qgrv")))

(define-public crate-axgeom-1.6.2 (c (n "axgeom") (v "1.6.2") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "1i67b8k8zf5sr6zcildq96agr6fpkr53l72fyifwlzfkri2ralcw")))

(define-public crate-axgeom-1.6.3 (c (n "axgeom") (v "1.6.3") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0ffav44mng0gd105ry49qqg9jazj6q482qgpm1rlvqvns4s6gkrh")))

(define-public crate-axgeom-1.6.4 (c (n "axgeom") (v "1.6.4") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0dzn1r8v6bs1h1wkxd6mfrwkwqmxh4wbnp96j86qjncsg8p5qnd7")))

(define-public crate-axgeom-1.6.5 (c (n "axgeom") (v "1.6.5") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "primitive-from") (r "^1.0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0lk5vg4kmb0jvd30c407sd7ys11qwm5hm8hh945zmhpspjms8cz5")))

(define-public crate-axgeom-1.6.6 (c (n "axgeom") (v "1.6.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "primitive-from") (r "^1.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ghkxzvs20aghpzwhsybfba2wffwhijnxq50vkcgmvc2ahia42ny")))

(define-public crate-axgeom-1.6.9 (c (n "axgeom") (v "1.6.9") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "primitive-from") (r "^1.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xsvsi7c9bd8rx7gzxkypyfbcxch45xciq15qxxm6wgrks1s5lym")))

(define-public crate-axgeom-1.7.0 (c (n "axgeom") (v "1.7.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0hzyc79h7rzwmgc12qhb8ss22mbsgc0ry9waariiz5xvwnbvk7ww") (f (quote (("std") ("default" "std"))))))

(define-public crate-axgeom-1.7.1 (c (n "axgeom") (v "1.7.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0x939p498965v3m4aimqvzjkcivsbp5i6gac8jn9c54h1kfl3if7") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-axgeom-1.7.2 (c (n "axgeom") (v "1.7.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0ijpd0z1jz720gjd9dl0fyz1b7y55psg0x0gbhspi7zhxcvrxby0") (f (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-axgeom-1.7.3 (c (n "axgeom") (v "1.7.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1jrw0i7armyhdhzrp6fm0pk4kdbsc5dfi6n9fwkmyzbk9mik1br2") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.7.4 (c (n "axgeom") (v "1.7.4") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1pnpy9dyxw8c20wdz8pk4vjdbal8ck02gs504wkqh1jcfbp1fw8d") (f (quote (("std" "num-traits/std" "roots" "ordered-float/serde") ("default" "std"))))))

(define-public crate-axgeom-1.7.5 (c (n "axgeom") (v "1.7.5") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "ordered-float") (r "^2.0") (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1qvv8kq3rl9blxc35lg0rxldm9fl1r2vnb0ki1cbba1qjv7iraql") (f (quote (("std" "num-traits/std" "roots" "ordered-float/serde") ("default" "std"))))))

(define-public crate-axgeom-1.7.6 (c (n "axgeom") (v "1.7.6") (d (list (d (n "num-traits") (r ">=0.2.0, <0.3.0") (k 0)) (d (n "ordered-float") (r ">=2.0.0, <3.0.0") (k 0)) (d (n "roots") (r ">=0.0.6, <0.0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (k 0)))) (h "0c1lickn3qxcrlhiq985r9vv5fpzfgmbmpspcm0m4dz87il1fjr6") (f (quote (("std" "num-traits/std" "roots" "ordered-float/serde") ("default" "std"))))))

(define-public crate-axgeom-1.7.7 (c (n "axgeom") (v "1.7.7") (d (list (d (n "num-traits") (r ">=0.2.0, <0.3.0") (k 0)) (d (n "ordered-float") (r ">=2.0.0, <3.0.0") (k 0)) (d (n "roots") (r ">=0.0.6, <0.0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (k 0)))) (h "002adz22mvs7c0vndyplvh22bnkpj5mi7mnw5l3l84f711l6mcw6") (f (quote (("std" "num-traits/std" "roots" "ordered-float/serde") ("default" "std"))))))

(define-public crate-axgeom-1.8.0 (c (n "axgeom") (v "1.8.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0gifi662771kczwl32n9a37chkb9wqbm8mddc6ska9yn975w26r3") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.0 (c (n "axgeom") (v "1.9.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "18ynpz7x1ja74bmj0pwa2wwwwka8f69miqx4p31gqq5k463khn8p") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.1 (c (n "axgeom") (v "1.9.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "01xxdff34kbs69r7c9fv2l3b7fgih5bmvfdmwbc8jy58gawf93in") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.2 (c (n "axgeom") (v "1.9.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "096ky2n3i7hllcv8l6rjwa2iqc2mv411pf3g2s7yksig0s5imr0v") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.3 (c (n "axgeom") (v "1.9.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0gppwnvfnk7d2gsk4zb993sjsp1479sgy92nksfa3cy7jah3x6m4") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.4 (c (n "axgeom") (v "1.9.4") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1dvr3h4cqj7m6hncgfjiz2md346s3ixjv2py87d41n5c1722gyzr") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.5 (c (n "axgeom") (v "1.9.5") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "020z5h2v0wc5776z4l6masrfgkh20nfr555xs2mhfrcjmlrf10h4") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.6 (c (n "axgeom") (v "1.9.6") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0xwhb51qhn7bgrd4gqmxy7dxcdcfaq61qy6by4sdzdjg4aafywm2") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.9.7 (c (n "axgeom") (v "1.9.7") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1y4m5cz9kyn2n2433f4jnjwwm8x94p4d8bd7s8kgwap7fva7kjvx") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.10.0 (c (n "axgeom") (v "1.10.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "11qy4dfb0sjzj46sa16riagk483hpidrrqrgadf7vgmsm072inrv") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.11.0 (c (n "axgeom") (v "1.11.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0vlcnh65pyix67jxlgwgwz6j9yqrby39rpf02vddr72rlrh6sy2a") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.12.0 (c (n "axgeom") (v "1.12.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1y92h1i1vvyc7f23pv3w9cmvv3yl8mrk0sd5rx3jidmkpl3fdk7h") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.12.1 (c (n "axgeom") (v "1.12.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "19fw1jlqzwfj5790jdr1x6qknj0pdq6gxc3qn85v2k1zdj17q8fv") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.12.2 (c (n "axgeom") (v "1.12.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1hihz3sf4xzh01ajwa64nn4mn2mk2ywn8i2sv0728bb3zv4v86n0") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.13.0 (c (n "axgeom") (v "1.13.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1csvgicp0z0yw47qgkbbpf1lyf41f5kgq3ch8brvb5000dzz7ajs") (f (quote (("std" "num-traits/std" "roots") ("default" "std"))))))

(define-public crate-axgeom-1.13.1 (c (n "axgeom") (v "1.13.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1hzss15s0j5r80y4kx2mdk1czf76bx6nyvidc50sxywddbd27wz4") (f (quote (("std" "num-traits/std" "roots") ("default" "std" "serde")))) (y #t)))

(define-public crate-axgeom-1.13.2 (c (n "axgeom") (v "1.13.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "17bqn3qzx7h9i22y63q0bnp4333qzzmcp5z7hglm765lf6ym9m5s") (f (quote (("std" "num-traits/std" "roots") ("default" "std" "serde"))))))

