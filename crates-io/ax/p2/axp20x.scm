(define-module (crates-io ax p2 axp20x) #:use-module (crates-io))

(define-public crate-axp20x-0.0.1 (c (n "axp20x") (v "0.0.1") (d (list (d (n "bitmask-enum") (r "^1.1.3") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "0z91r4y7l6mq37d9ksa0rfx318qdw8q44zg76232cvyzh3jszy2b") (f (quote (("std") ("default"))))))

(define-public crate-axp20x-0.0.2 (c (n "axp20x") (v "0.0.2") (d (list (d (n "bitmask-enum") (r "^1.1.3") (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1w9dhv1m0ls720fbjbv9hdj2ba7dcwbpyjp1sgg5ryyn7fpi7hlx") (f (quote (("std") ("default"))))))

