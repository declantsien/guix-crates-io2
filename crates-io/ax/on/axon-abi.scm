(define-module (crates-io ax on axon-abi) #:use-module (crates-io))

(define-public crate-axon-abi-0.0.1 (c (n "axon-abi") (v "0.0.1") (d (list (d (n "axon-tools") (r "^0.0.11") (f (quote ("hash"))) (d #t) (k 0)) (d (n "ckb-jsonrpc-types") (r "^0.110") (d #t) (k 0)) (d (n "ckb-types") (r "^0.110") (d #t) (k 2)) (d (n "ethers") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q3n5ilkqbn2nh2rml3rf6mx3av0zl4bhvbd57x38c643ls6n03q")))

