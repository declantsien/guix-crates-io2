(define-module (crates-io ax on axon_parser) #:use-module (crates-io))

(define-public crate-axon_parser-0.1.0 (c (n "axon_parser") (v "0.1.0") (d (list (d (n "axon_parseast_parser") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mfz2glqky0w38y8yfcvg264ri434lb49qkhh7q1fys7ikr29lmq")))

(define-public crate-axon_parser-0.1.1 (c (n "axon_parser") (v "0.1.1") (d (list (d (n "axon_parseast_parser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1shs5f6q0gz0nn2b3aw8ldvv0hvsz3wxfs5mlwqyb3fjwspwsc1v")))

(define-public crate-axon_parser-0.2.0 (c (n "axon_parser") (v "0.2.0") (d (list (d (n "axon_parseast_parser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gijdjq4v32785512z7jvjrrks2151rqmkdwlxgv49caiwwx4yka")))

(define-public crate-axon_parser-0.2.1 (c (n "axon_parser") (v "0.2.1") (d (list (d (n "axon_parseast_parser") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jpn5xfk19r2gj20gng21vxl65kxcc0nhx9vy85afjxhxv6h9hfy")))

(define-public crate-axon_parser-0.2.2 (c (n "axon_parser") (v "0.2.2") (d (list (d (n "axon_parseast_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rddwlavb450nm2di44lrff9xr2phb28abmcp5v36r3vrk2bgljw")))

(define-public crate-axon_parser-0.2.3 (c (n "axon_parser") (v "0.2.3") (d (list (d (n "axon_parseast_parser") (r "^0.4.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01g25zqldhys8lzml91aqd8ciz4vfxldq7ibhwyz6hqwwladw7zy")))

(define-public crate-axon_parser-0.3.0 (c (n "axon_parser") (v "0.3.0") (d (list (d (n "axon_parseast_parser") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hi0af8ban9wb3fpc2ggjdf9bvkmb6fzycjyfrs86cn9daxd1hcc")))

(define-public crate-axon_parser-0.4.0 (c (n "axon_parser") (v "0.4.0") (d (list (d (n "axon_parseast_parser") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ixh7alk34z49dsalnqz6gmnqqy7kh6g3kcl21j9jipakrnmw664")))

(define-public crate-axon_parser-0.4.1 (c (n "axon_parser") (v "0.4.1") (d (list (d (n "axon_parseast_parser") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jn34486y2n03csm879ylhrl9i75r3l5pvbv376dlacyhq3lwk28")))

(define-public crate-axon_parser-0.5.0 (c (n "axon_parser") (v "0.5.0") (d (list (d (n "axon_parseast_parser") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p0ci4hav53q6d0ailjk2zndiyvinbhad0y3rcrsxqdhrj278y6v")))

(define-public crate-axon_parser-0.5.1 (c (n "axon_parser") (v "0.5.1") (d (list (d (n "axon_parseast_parser") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cb5yxi1h1lnzlgg844l7sqb7hw0a1imiim3zjvqxfmkbqp5wppd")))

(define-public crate-axon_parser-0.5.2 (c (n "axon_parser") (v "0.5.2") (d (list (d (n "axon_parseast_parser") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ckr28c4vbycsfvkx78z3k11w7ik87kr8cr82rlalahbndj8cspl")))

(define-public crate-axon_parser-0.5.3 (c (n "axon_parser") (v "0.5.3") (d (list (d (n "axon_parseast_parser") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "139bavc4mf6g86awqvl4ajk3jfwj034vm07kp574q0sbfs69601g")))

(define-public crate-axon_parser-0.5.4 (c (n "axon_parser") (v "0.5.4") (d (list (d (n "axon_parseast_parser") (r "^0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l8866i6vvsxh0w07c8liqj1kv5in0wbmryqm6axi09ssklwj27n")))

(define-public crate-axon_parser-0.6.0 (c (n "axon_parser") (v "0.6.0") (d (list (d (n "axon_parseast_parser") (r "^0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m9wy3qa7g1xjh4qjv3kwnn9qhcapd8y6063qp3jcp7zzf3wxj8q")))

(define-public crate-axon_parser-0.6.1 (c (n "axon_parser") (v "0.6.1") (d (list (d (n "axon_parseast_parser") (r "^0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19ksaz3f2ar9l2h4ifdc1bdk8q3hm0iqbn651dbf558ddfr7jjzq")))

(define-public crate-axon_parser-0.7.0 (c (n "axon_parser") (v "0.7.0") (d (list (d (n "axon_parseast_parser") (r "^0.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01313bccq69vb5sd6y6ggyw6r65rdr9sa4v22my5rhwjmvnr9qmc")))

(define-public crate-axon_parser-0.8.0 (c (n "axon_parser") (v "0.8.0") (d (list (d (n "axon_parseast_parser") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03qah219jvwyj73f6ylvrbphjllxk9vzdqx4ijybs0qhvy74p133")))

(define-public crate-axon_parser-0.8.1 (c (n "axon_parser") (v "0.8.1") (d (list (d (n "axon_parseast_parser") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kj2by2f1f4krimzk6lyadxs9jw3zrds7j8yl7s4vchdayxgv0x7")))

(define-public crate-axon_parser-0.8.2 (c (n "axon_parser") (v "0.8.2") (d (list (d (n "axon_parseast_parser") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raystack_core") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r446cy6ggnvc13wqyb77qhvyx18yg1ydghszh3xgqak3lagyyc1")))

