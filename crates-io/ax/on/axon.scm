(define-module (crates-io ax on axon) #:use-module (crates-io))

(define-public crate-axon-0.1.0 (c (n "axon") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0axnxq1rdr2wfw91qrpy2l4wswl5fcr14nkdb8r235jiqfbp2hnn")))

(define-public crate-axon-0.1.1 (c (n "axon") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "15gdbjqxrwl609jyz4b0di8s50qr8sp0aabg8f6a5wgzndf8qxny")))

(define-public crate-axon-0.1.2 (c (n "axon") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0x54m4nsv78pfb7xrj2169fwpi102wg1yvqsjbh31zd1929hw969")))

(define-public crate-axon-0.1.3 (c (n "axon") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0hm1mq230qzn4fr70kqxiyg6vi9avf1ahnkgf855s9fws2fxrx4x")))

(define-public crate-axon-0.1.4 (c (n "axon") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0biys7qpjhc3dlgvn63zw72j9xlkdv9adbka381i4685z3p6w0cd")))

(define-public crate-axon-0.1.5 (c (n "axon") (v "0.1.5") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "03k911kxavjgy0ap9i29c03rvh6y4hzy8zmj5bh66d0f59d289sf")))

(define-public crate-axon-0.1.6 (c (n "axon") (v "0.1.6") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1db0vpqzm5rfg737sp2xv2bn2wvcyvaadqfhs4znvis9vxm44gwm")))

(define-public crate-axon-0.1.7 (c (n "axon") (v "0.1.7") (d (list (d (n "futures") (r "^0.1.24") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0c0crflr40pgc23c4h7zdrgcsi62wdm7cpqw6gqf205yqwvz6fqr")))

(define-public crate-axon-0.2.0 (c (n "axon") (v "0.2.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "osaka") (r "^0.2") (d #t) (k 0)))) (h "1bz3f26sg6azjwvibdmkldv1mhpyb61dmrwymyakq6khqrx1g3dq")))

(define-public crate-axon-0.2.1 (c (n "axon") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "osaka") (r "^0.2.10") (d #t) (k 0)))) (h "15civx2mhndwxxhwc2s2y4h6lbbg5crqp5wk00wdf47kvm1m9f04")))

(define-public crate-axon-0.3.0 (c (n "axon") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "osaka") (r "^0.3.0") (d #t) (k 0)))) (h "148vk825d211d4p3vxrjr4609i2mkbqf3rj0528fwpp8jgxwvs79")))

