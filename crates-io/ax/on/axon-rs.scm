(define-module (crates-io ax on axon-rs) #:use-module (crates-io))

(define-public crate-axon-rs-0.1.0 (c (n "axon-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "19b8hdd6ppk8wvkjb2jaihbc6lky2x1d5b7gzinrsr7h8yjn5zga")))

