(define-module (crates-io ax on axon_parseast_parser) #:use-module (crates-io))

(define-public crate-axon_parseast_parser-0.1.0 (c (n "axon_parseast_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cavv0jhgq6l783fn2wwiy4fjak1avw1jzvgv9j752z8bim0q80i")))

(define-public crate-axon_parseast_parser-0.1.1 (c (n "axon_parseast_parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vh38z1x3mqfca4l3562n9g8cha0pc4wyp86gvc7i0j1fw3q1qj5")))

(define-public crate-axon_parseast_parser-0.2.0 (c (n "axon_parseast_parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1aqds31ai9fhxahsg4pl8wa5ggcqzs86lrkf6lzcy7whfbv195ba")))

(define-public crate-axon_parseast_parser-0.3.0 (c (n "axon_parseast_parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08dnqy2wv271rrs4l7qldi0jpdm0vsqlas5j4ap05x9kfldjblkf")))

(define-public crate-axon_parseast_parser-0.4.0 (c (n "axon_parseast_parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mprq460ivz0dvz1f9i9jyv9f76p3rxgazy29lp17r228i960cm9")))

(define-public crate-axon_parseast_parser-0.4.1 (c (n "axon_parseast_parser") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1phnibv0i2lg6975j2zb18l4m4rbc4xl94073nl9xnbpfw7bk6ax")))

(define-public crate-axon_parseast_parser-0.4.2 (c (n "axon_parseast_parser") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack_core") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sbpps6s71fdf6c0aaqgd7f3ydaxzlzkrz7bdjr4l0ingky6yjpv")))

(define-public crate-axon_parseast_parser-0.5.0 (c (n "axon_parseast_parser") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zdlr16hm5f5qb4znm2w7w4z0cwa994yfyrvi0gqyn28amngnanh")))

(define-public crate-axon_parseast_parser-0.5.1 (c (n "axon_parseast_parser") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r3jh5vkl5i4nbx1fl3f00sy9clkfcrfzbs3zv9lqqirflqdxm8m")))

(define-public crate-axon_parseast_parser-0.5.2 (c (n "axon_parseast_parser") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x6lqx82jxwin2ihbhkfja7qghj8zd11gpplr7x9x6a8hvlgkgz1")))

(define-public crate-axon_parseast_parser-0.5.3 (c (n "axon_parseast_parser") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x63dzvzvq1x7wnc3rv69gwsfkqbc75rbhcngf564zwdrpxsgww8")))

(define-public crate-axon_parseast_parser-0.6.0 (c (n "axon_parseast_parser") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09fr4pj9pjpf76ayp8i35ichnw170mbhrvn8ra7i0x2x8v2ni7rs")))

(define-public crate-axon_parseast_parser-0.6.1 (c (n "axon_parseast_parser") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06z029wi1j37kbfhb4ql53g61n7skd0mb9d9j7c8dqqa8ccma5av")))

(define-public crate-axon_parseast_parser-0.6.2 (c (n "axon_parseast_parser") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fgnp8kny2z8y3q3zifm8ncskzggjk7ryznyjx3fv56lxk94xdak")))

(define-public crate-axon_parseast_parser-0.7.0 (c (n "axon_parseast_parser") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06sdhhq7qk07zh9vpia7l7cblwqj5shd6311m41d7gqjdr2c0jxa")))

(define-public crate-axon_parseast_parser-0.8.0 (c (n "axon_parseast_parser") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "raystack_core") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17iqgczm2h7d5l63zgn9g0y2xhiiddkpgxnyzysza3w536dp58b1")))

