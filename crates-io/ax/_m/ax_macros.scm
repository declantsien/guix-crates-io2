(define-module (crates-io ax _m ax_macros) #:use-module (crates-io))

(define-public crate-ax_macros-0.1.0 (c (n "ax_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qixradlamqn432x546ibwcp5j37xcj2326k01kxwk1wh4hbzp01")))

