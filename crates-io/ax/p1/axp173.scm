(define-module (crates-io ax p1 axp173) #:use-module (crates-io))

(define-public crate-axp173-0.1.0 (c (n "axp173") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0qd2mhgf7f1yh3ybrpk5vrmdq42q9qvxz50z2nxall4kd2w8l4p7")))

(define-public crate-axp173-0.1.1 (c (n "axp173") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0dclf9j5214hhzsjzkzd5npwrgiwk3g6rcj609ld2rrvsn019421")))

(define-public crate-axp173-0.1.2 (c (n "axp173") (v "0.1.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1ykik64gnk76pmswkhgpczp3flmxpf3gh6h9rvypdxvma8wjns14")))

(define-public crate-axp173-0.1.3 (c (n "axp173") (v "0.1.3") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0hzaakmiqvwv3l9rdlhnlmq53aclajhbz7alid6h4xm58pkcf23n")))

(define-public crate-axp173-0.1.4 (c (n "axp173") (v "0.1.4") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1rxc7lw6acvmm78mkja53ad8nwv1h09pcdbpgivwaw2w2g7qwzw5")))

(define-public crate-axp173-0.1.5 (c (n "axp173") (v "0.1.5") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "13nyvnrfxsyhdx6cmjliqsp8girwxr6vqj64z64q1f69dmabfznl")))

(define-public crate-axp173-0.1.6 (c (n "axp173") (v "0.1.6") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0panj9h9lpphhqimvblavnyvcw8347lww9qmwrilgiyprabzwadw")))

(define-public crate-axp173-0.1.7 (c (n "axp173") (v "0.1.7") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1bvqqfl611pc9ikqxgrmii6xhiqixqkl5045wki95jwnx55yrdmc")))

