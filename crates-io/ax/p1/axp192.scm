(define-module (crates-io ax p1 axp192) #:use-module (crates-io))

(define-public crate-axp192-0.1.0 (c (n "axp192") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "esp-backtrace") (r "^0.8.0") (f (quote ("esp32" "panic-handler" "exception-handler" "print-uart"))) (d #t) (k 2)) (d (n "esp-println") (r "^0.6") (f (quote ("esp32"))) (d #t) (k 2)) (d (n "esp32-hal") (r "^0.15.0") (d #t) (k 2)))) (h "0cf2c57l03p4g4q0lzx4im882mhqdsc2l93sdwzbnzfanr9i51ml")))

(define-public crate-axp192-0.1.1 (c (n "axp192") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "esp-backtrace") (r "^0.8.0") (f (quote ("esp32" "panic-handler" "exception-handler" "print-uart"))) (d #t) (k 2)) (d (n "esp-println") (r "^0.6") (f (quote ("esp32"))) (d #t) (k 2)) (d (n "esp32-hal") (r "^0.15.0") (d #t) (k 2)))) (h "1vqbslc7zh9p5l7l6czmj9cp7rpgglxpd42xki3mg57fs0m8m8s4")))

