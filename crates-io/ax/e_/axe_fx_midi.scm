(define-module (crates-io ax e_ axe_fx_midi) #:use-module (crates-io))

(define-public crate-axe_fx_midi-0.1.0 (c (n "axe_fx_midi") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "coremidi") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0y6nhzzyrf0by55izak5dn7jiwzlkyfjs1aid2xnlbmi18kq87sq")))

