(define-module (crates-io ax os axosnake) #:use-module (crates-io))

(define-public crate-axosnake-0.1.0 (c (n "axosnake") (v "0.1.0") (d (list (d (n "quicksilver") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0im3m265cw8agsw4rpfcfzcfc82h1a38805xki88clma6gc0b3cg")))

(define-public crate-axosnake-0.1.1 (c (n "axosnake") (v "0.1.1") (d (list (d (n "quicksilver") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1w1d7l9rh7650fywp1igmc6hpsc1gxsfpa8szsx7dpfi2y51c153")))

(define-public crate-axosnake-0.1.2 (c (n "axosnake") (v "0.1.2") (d (list (d (n "quicksilver") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0nm0kxzpx769qil1w6gp0s8wi207ri31gyhfq3vbn1ggj97d71li")))

(define-public crate-axosnake-0.1.3 (c (n "axosnake") (v "0.1.3") (d (list (d (n "quicksilver") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "11s23bzaaqcvskzkv7z4my3srvgj7fl1qv3x83qhjkh3f1km9d73")))

(define-public crate-axosnake-0.1.4 (c (n "axosnake") (v "0.1.4") (d (list (d (n "quicksilver") (r "^0.3.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1jbvcb3gwvy73p6visfmh770angj26714knis19jb46ya6bw55w8")))

(define-public crate-axosnake-0.1.5 (c (n "axosnake") (v "0.1.5") (d (list (d (n "quicksilver") (r "^0.3.14") (f (quote ("fonts"))) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "17any65aywapvx9q99rbr043lyxiyfjkiqqjkk0000g9pygnwxy6")))

(define-public crate-axosnake-0.1.6 (c (n "axosnake") (v "0.1.6") (d (list (d (n "quicksilver") (r "^0.3.14") (f (quote ("fonts"))) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1c74xmvbxs5lr25y4j6lp3q44m2v8yhliz658x2mk7zi3k6id6rg")))

(define-public crate-axosnake-0.1.7 (c (n "axosnake") (v "0.1.7") (d (list (d (n "instant") (r "^0.1.1") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.16") (f (quote ("fonts"))) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1qg8530x4kccd56h095zmxkdiq2y04aqic4dmha1yi4sjq1jb1l6")))

