(define-module (crates-io ax os axos-primitives) #:use-module (crates-io))

(define-public crate-axos-primitives-0.1.0 (c (n "axos-primitives") (v "0.1.0") (d (list (d (n "alloy-primitives") (r "^0.4.2") (f (quote ("hex-compat" "serde"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1mqanlbvf2jiw5k2gvm40b6ljjvay9ma293cw69sg66zgbd9v1pg")))

