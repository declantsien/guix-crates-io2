(define-module (crates-io ax os axos) #:use-module (crates-io))

(define-public crate-axos-0.1.0 (c (n "axos") (v "0.1.0") (d (list (d (n "axos-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "07smsdwvixrbimpn5zli55ys1qb1jyq7d9srvj0cc20dylqig1wc")))

