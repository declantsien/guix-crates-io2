(define-module (crates-io ax ot axotag) #:use-module (crates-io))

(define-public crate-axotag-0.1.0 (c (n "axotag") (v "0.1.0") (d (list (d (n "miette") (r "^5.6.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1wy7q0mm3p5cribvvri19dd2f63rq93mg2bb8r0rgw3wx92vcaf8")))

(define-public crate-axotag-0.2.0 (c (n "axotag") (v "0.2.0") (d (list (d (n "miette") (r "^7.0.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0m3c38gbz2dk42s5qlspqdayjnmg4mdgqhx76vgwnr1ynz0gm26q")))

