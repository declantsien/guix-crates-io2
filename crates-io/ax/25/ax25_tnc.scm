(define-module (crates-io ax #{25}# ax25_tnc) #:use-module (crates-io))

(define-public crate-ax25_tnc-0.3.0 (c (n "ax25_tnc") (v "0.3.0") (d (list (d (n "ax25") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("local-offset"))) (d #t) (k 2)))) (h "1g8rk137s4fj76141q9cq5igxfnjgdicapaf4x3kh0hiqcld1cm6")))

