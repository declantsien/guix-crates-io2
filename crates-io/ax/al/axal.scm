(define-module (crates-io ax al axal) #:use-module (crates-io))

(define-public crate-axal-0.1.0 (c (n "axal") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "080mqrv6k62aryqa3x615fx697lzzwjl8xwnw1dx07x689mr70m6")))

