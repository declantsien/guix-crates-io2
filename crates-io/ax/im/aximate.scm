(define-module (crates-io ax im aximate) #:use-module (crates-io))

(define-public crate-aximate-0.1.0 (c (n "aximate") (v "0.1.0") (h "130clvv9qxbs8dhpr9lh0x4hl8wy7bd3zn4vpaigrb3m3zfk4shm")))

(define-public crate-aximate-0.1.1 (c (n "aximate") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n0qk7jxsm02n6kgvmmkj030bw3xx2dxcy5y9n27kbpsn2kgffsf")))

(define-public crate-aximate-0.1.2 (c (n "aximate") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18ikxvyj0i5amyks6g3qy1g09lrrvvw3bx0rfk9jh1q01nd7wywc")))

(define-public crate-aximate-0.1.3 (c (n "aximate") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bwy698wlgh7dgv03hazvcbhni4ns618k67qzwi9nyg7drdz5466")))

(define-public crate-aximate-0.1.4 (c (n "aximate") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1plg8ns34kymkh4azvdyll32bhi06bwby07b4acdxbqc3ijdlyja")))

