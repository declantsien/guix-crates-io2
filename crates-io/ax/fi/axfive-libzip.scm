(define-module (crates-io ax fi axfive-libzip) #:use-module (crates-io))

(define-public crate-axfive-libzip-0.1.0 (c (n "axfive-libzip") (v "0.1.0") (d (list (d (n "axfive-libzip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0azdw5q7ldv1ml0b5j1djsvgr5c7w82lm0r2xx40q6g92axg50m1")))

(define-public crate-axfive-libzip-0.1.1 (c (n "axfive-libzip") (v "0.1.1") (d (list (d (n "axfive-libzip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0ii7c29p568691s1lw8d4h3qcsvp1m6waa7ja1slf4wzi0kys914")))

(define-public crate-axfive-libzip-0.1.2 (c (n "axfive-libzip") (v "0.1.2") (d (list (d (n "axfive-libzip-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a73yswfn5wm9c75ks4gh6bx9gbf146fazqbk46dm5wybkw6vqpi")))

(define-public crate-axfive-libzip-0.1.3 (c (n "axfive-libzip") (v "0.1.3") (d (list (d (n "axfive-libzip-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0npkllvszybkn4jhzkq6ph0bq43gp74210mdkhml8s3gp7crnq2p")))

(define-public crate-axfive-libzip-0.1.5 (c (n "axfive-libzip") (v "0.1.5") (d (list (d (n "axfive-libzip-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15kxv0lcxmqg8x5x27igl7nrqylyhqlfjpa741dqdl9x7j90mqkg") (f (quote (("static" "axfive-libzip-sys/static") ("default"))))))

