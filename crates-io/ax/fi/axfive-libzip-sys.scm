(define-module (crates-io ax fi axfive-libzip-sys) #:use-module (crates-io))

(define-public crate-axfive-libzip-sys-0.1.0 (c (n "axfive-libzip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "18daib2gdjs49z36p9kwg8p3fn59371q7843n2p0ib2q18k4yqm3")))

(define-public crate-axfive-libzip-sys-0.1.1 (c (n "axfive-libzip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06v8ixi51ia3nqjmh14mr95fc4nkbgkkrjalbq568v84rsc9vd0d")))

(define-public crate-axfive-libzip-sys-0.1.5 (c (n "axfive-libzip-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libz-sys") (r "^1") (o #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1h86r7axhl00w5g0qgmln8sssaczwq3bgps8s5anll3zdg468vdv") (f (quote (("static" "openssl-sys/vendored" "libz-sys/zlib-ng") ("default")))) (l "zip")))

