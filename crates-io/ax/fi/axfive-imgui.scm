(define-module (crates-io ax fi axfive-imgui) #:use-module (crates-io))

(define-public crate-axfive-imgui-0.1.0 (c (n "axfive-imgui") (v "0.1.0") (h "1mvng6ypgnj5cjqd522wfbqafn6kwdax00b9yfy7d0kmac0kvvr0")))

(define-public crate-axfive-imgui-0.2.0 (c (n "axfive-imgui") (v "0.2.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glium") (r "^0.25.1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0wxdp9zq68m4jiqchg6kz2h55z3jmlbghbv5axbb7cniyb3pl4lp")))

(define-public crate-axfive-imgui-0.2.1 (c (n "axfive-imgui") (v "0.2.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glium") (r "^0.25.1") (d #t) (k 0)) (d (n "imgui-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0igsg3gnk9b1jvsixjmq1h2xa01zf7180np899kx7ynnlvyw3iwl")))

