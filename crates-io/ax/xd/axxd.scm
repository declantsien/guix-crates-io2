(define-module (crates-io ax xd axxd) #:use-module (crates-io))

(define-public crate-axxd-0.1.0 (c (n "axxd") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0na3vridylqw5w659a2hyl5s9ym9d529csaqp5d2bds9qvq3lnjk")))

