(define-module (crates-io ax ma axmac) #:use-module (crates-io))

(define-public crate-axmac-0.1.0 (c (n "axmac") (v "0.1.0") (h "0sb1y1wydra4bssa8z27zc67gasv6yw9348ffrrdgk9fs11ryzw6")))

(define-public crate-axmac-0.1.1 (c (n "axmac") (v "0.1.1") (h "0ysq02qm0k1rdi3i0h7z5xly6fymmrrd1yqmy2z2rh1gsvpl0a45")))

