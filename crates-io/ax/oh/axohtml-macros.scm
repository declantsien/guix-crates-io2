(define-module (crates-io ax oh axohtml-macros) #:use-module (crates-io))

(define-public crate-axohtml-macros-0.1.0 (c (n "axohtml-macros") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "05z8b9xrbl80fpsd3jww2pmzrk5xggv069cnwmsmpmmkbpsl5vjr")))

(define-public crate-axohtml-macros-0.2.0 (c (n "axohtml-macros") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "0xbz5m2lr816j6wswx5nbyi6f8jf77g0z4vxw4sa0a9mznhghls1")))

(define-public crate-axohtml-macros-0.3.0 (c (n "axohtml-macros") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "1jhbf9jpl5zpiv1dwbm4kxhasgjl8imdb1s8gggk1cn0c6qw2cn7")))

(define-public crate-axohtml-macros-0.4.0 (c (n "axohtml-macros") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "0c2iq8nd9p6wc85aqh5lyx9j458h3bxizby2602fm53y63frya12")))

(define-public crate-axohtml-macros-0.4.1 (c (n "axohtml-macros") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "version_check") (r "^0.9.1") (d #t) (k 1)))) (h "02y4b4fc0dyn3bbxz9x8vxmaxnaw8ndnpxm4v0xlks3hrjps8bsh")))

(define-public crate-axohtml-macros-0.5.0 (c (n "axohtml-macros") (v "0.5.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.9") (d #t) (k 1)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1gan1w6ivycblf4376c6wv0mwq5r7drn7d4jgz4917caiwlpwrfa")))

