(define-module (crates-io pt rp ptrplus) #:use-module (crates-io))

(define-public crate-ptrplus-0.1.0 (c (n "ptrplus") (v "0.1.0") (h "0gkxf41acrqahk6439jw4zlhps0gpj9pij92018fli4aplmwvrzx")))

(define-public crate-ptrplus-0.2.0 (c (n "ptrplus") (v "0.2.0") (h "0vin5g6dvqzzvn2xq965jx1i6xq4h5cd4chz5zf3h8vq4r0mq21z")))

(define-public crate-ptrplus-1.0.0 (c (n "ptrplus") (v "1.0.0") (h "1mn21air79q1x7xdwkhs56adyk4v6ih07s3q2fc2n1yl7il91yak")))

(define-public crate-ptrplus-1.1.0 (c (n "ptrplus") (v "1.1.0") (h "1lqqy89342234ylgiwx06lkav17kpqm9mlym4hhjb5w7gil6zhlj") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-ptrplus-2.0.0 (c (n "ptrplus") (v "2.0.0") (h "0m42mzpb9dg85jqkg0yr7k3x89v2jldvlil209c35amvjxmjdvm3") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-ptrplus-2.1.0 (c (n "ptrplus") (v "2.1.0") (h "10psihxhxwbh7njmnbf87yjxzsbzzw08xllyrw592asnmd6a6kzi") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (r "1.65")))

