(define-module (crates-io pt ox ptoxide) #:use-module (crates-io))

(define-public crate-ptoxide-0.1.0 (c (n "ptoxide") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0adir17zl2sknxyn3hmajxcd1nfpcbn1r3y73lj4qvg6yl8bwvrn")))

