(define-module (crates-io pt ri ptrie) #:use-module (crates-io))

(define-public crate-ptrie-0.5.0 (c (n "ptrie") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g76g3r0jqm073dsarn6dngm6j3hy8hgf5g778n7z4jq2j4p6c4w") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.5.2 (c (n "ptrie") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1shji41qawmzb0jr1v2b9i89s3dpnpxafmikp8qxj8zcpcr20ml4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.5.4 (c (n "ptrie") (v "0.5.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08pfj7gg5hyp4r797cj0bx78344n1wb4khfzf501r9wln2nr60bd") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.6.0 (c (n "ptrie") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xhwpagcd2cxd1svp9zbg6mwxl8i20cfc3l2jrc5iz3nx5zyxnjw") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.7.0 (c (n "ptrie") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11wmpjd98invwjrhnz8azjrz7pxa9sw0gwxfrzpchi5ycvslqq2s") (s 2) (e (quote (("serde" "dep:serde"))))))

