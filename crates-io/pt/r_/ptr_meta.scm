(define-module (crates-io pt r_ ptr_meta) #:use-module (crates-io))

(define-public crate-ptr_meta-0.1.0 (c (n "ptr_meta") (v "0.1.0") (d (list (d (n "ptr_meta_derive") (r "=0.1.0") (d #t) (k 0)))) (h "0abpyc795gfjpkqzwdzzzbzw57y7phc2xqw62kpa2g9xw2v1qgcp")))

(define-public crate-ptr_meta-0.1.1 (c (n "ptr_meta") (v "0.1.1") (d (list (d (n "ptr_meta_derive") (r "=0.1.1") (d #t) (k 0)))) (h "1651cw06zszrd965hvjz6kggywcc981805ivfgaanls0pgs81nda")))

(define-public crate-ptr_meta-0.1.2 (c (n "ptr_meta") (v "0.1.2") (d (list (d (n "ptr_meta_derive") (r "=0.1.2") (d #t) (k 0)))) (h "04m6jg7isay9wrgphnp3gvjq9ci9y60qfp7hrgiiy9pzfqvp1r3d")))

(define-public crate-ptr_meta-0.1.3 (c (n "ptr_meta") (v "0.1.3") (d (list (d (n "ptr_meta_derive") (r "=0.1.2") (d #t) (k 0)))) (h "0601nvrjw9kyl9admybqqnkmj0jyz7bgw6ik0g24cahq419ja93l") (f (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.1.4 (c (n "ptr_meta") (v "0.1.4") (d (list (d (n "ptr_meta_derive") (r "=0.1.4") (d #t) (k 0)))) (h "1wd4wy0wxrcays4f1gy8gwcmxg7mskmivcv40p0hidh6xbvwqf07") (f (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.2.0 (c (n "ptr_meta") (v "0.2.0") (d (list (d (n "ptr_meta_derive") (r "=0.2.0") (d #t) (k 0)))) (h "01w6w8k9bf53zl4w16mz9k25vspdclw096lcykajxi06m86sibdw") (f (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.3.0-pre1 (c (n "ptr_meta") (v "0.3.0-pre1") (d (list (d (n "ptr_meta_derive") (r "^0.3.0-pre1") (d #t) (k 0)))) (h "13hi8xczm084zlgv6nwmp3lxrikv1yqc6p7hszn8x91km1r64pih") (f (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.3.0-alpha.2 (c (n "ptr_meta") (v "0.3.0-alpha.2") (d (list (d (n "ptr_meta_derive") (r "^0.3.0-alpha.2") (d #t) (k 0)))) (h "0mkg2x6warj82mk6i7dr4dayx11k5brmcfdrgm9jrjdqpryarbcx") (f (quote (("std") ("default" "std"))))))

