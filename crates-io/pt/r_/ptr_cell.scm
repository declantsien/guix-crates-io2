(define-module (crates-io pt r_ ptr_cell) #:use-module (crates-io))

(define-public crate-ptr_cell-1.0.0 (c (n "ptr_cell") (v "1.0.0") (h "0csczpcbmgpwb5yskr0a7bwv78arshff9jrlvkf54557b1kygwsf")))

(define-public crate-ptr_cell-1.0.1 (c (n "ptr_cell") (v "1.0.1") (h "0x5bpjl0dj2ig56lpp7wiq7yd42k19hswrrmdhwjj0zj4h204bbg")))

(define-public crate-ptr_cell-1.1.0 (c (n "ptr_cell") (v "1.1.0") (h "0ya5gz044yfqpvm26c4lnj15fgwn74x1wld6gbzjwj27rfdisana")))

(define-public crate-ptr_cell-1.2.0 (c (n "ptr_cell") (v "1.2.0") (h "0vsgr27vww0jaa25bwkir92wvjqizfy5as7y876pg0v9dvb1mwvp")))

(define-public crate-ptr_cell-1.2.1 (c (n "ptr_cell") (v "1.2.1") (h "1d2b1vbjcrq40j9k4xvifm6l71mig30qcfycy5mv1iqv0sk8r4kn")))

(define-public crate-ptr_cell-2.0.0 (c (n "ptr_cell") (v "2.0.0") (h "1imppvpkj13ciyi0x266zjc1nqzqza83px6rwf0w0qc4x21ygh92")))

(define-public crate-ptr_cell-2.1.0 (c (n "ptr_cell") (v "2.1.0") (h "1155anw19p7acjkd4q83bisb8709rnp588xzfbysndm666ghpwxk")))

(define-public crate-ptr_cell-2.1.1 (c (n "ptr_cell") (v "2.1.1") (h "09lajzl08cy8lghlh08kgkj338v2xych3k986x4450h5yd23smy1")))

(define-public crate-ptr_cell-2.2.0 (c (n "ptr_cell") (v "2.2.0") (h "0ymrcrcf8sjzqwf44r4pymkyralj9j76kknhw3f6blhyddi4v7kg")))

