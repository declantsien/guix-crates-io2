(define-module (crates-io pt r_ ptr_info_lib) #:use-module (crates-io))

(define-public crate-ptr_info_lib-0.2.1 (c (n "ptr_info_lib") (v "0.2.1") (h "1zvj24rvysfwbc2l5qbx4wchb8digms0dknslfizw321j7xgxj6i")))

(define-public crate-ptr_info_lib-0.2.2 (c (n "ptr_info_lib") (v "0.2.2") (h "1lgzgviyffr0pi43xgxbj901kcnr4zv7a2abnvjdgm39a0xlhcnd")))

