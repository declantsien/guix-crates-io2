(define-module (crates-io pt r_ ptr_iter) #:use-module (crates-io))

(define-public crate-ptr_iter-0.1.0 (c (n "ptr_iter") (v "0.1.0") (h "1irm9xddjjcfpxck1zabvzyylqpwcm7m14gcizs918yy31ykaj3i")))

(define-public crate-ptr_iter-0.1.1 (c (n "ptr_iter") (v "0.1.1") (h "0a7z7b51vna7hx03yhfajgdzmr8vp4l514wv64pzblzr1z3d471k")))

