(define-module (crates-io pt cr ptcrab) #:use-module (crates-io))

(define-public crate-ptcrab-0.0.0 (c (n "ptcrab") (v "0.0.0") (h "0glgcl1fq0pwr76dsr559rfz4hsx1whj77hvx6bf26h1bdj9ijj2")))

(define-public crate-ptcrab-0.0.1 (c (n "ptcrab") (v "0.0.1") (d (list (d (n "duplicate") (r "^1.0.0") (d #t) (k 0)))) (h "0d4z7qbzraz7im37apwh2jqdw1zkzmm5d9rn1v3vfqbmhvnpmhia")))

(define-public crate-ptcrab-0.1.0 (c (n "ptcrab") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "duplicate") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "01c1mmv9i7wb10yihqj1wnd402ydy4k2rcb2qk7x1wyf6y0ns0nm")))

