(define-module (crates-io pt ra ptrace-do-rs) #:use-module (crates-io))

(define-public crate-ptrace-do-rs-0.1.0 (c (n "ptrace-do-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0ix0cg31xys17d47803zk4l7cbgn3is4arwy8p35y7yrd4vv18v8")))

