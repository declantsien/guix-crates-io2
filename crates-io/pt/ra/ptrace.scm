(define-module (crates-io pt ra ptrace) #:use-module (crates-io))

(define-public crate-ptrace-0.0.1 (c (n "ptrace") (v "0.0.1") (d (list (d (n "posix-ipc") (r "*") (d #t) (k 0)))) (h "1v1z96qka05ygkxacyglwjnagcdpdshg4x1fcgaaw1f5hkgjgd5m")))

(define-public crate-ptrace-0.0.2 (c (n "ptrace") (v "0.0.2") (d (list (d (n "posix-ipc") (r "*") (d #t) (k 0)))) (h "00a4mpvpdzf8s3bzf7qqk1q16m74w4prxsy1hy33v93vnm4dznc7")))

(define-public crate-ptrace-0.0.3 (c (n "ptrace") (v "0.0.3") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "posix-ipc") (r "^0.0") (d #t) (k 0)))) (h "1qi3p0qbj6xr7flb1i4sxljaw5axx60k2dxrp708vd2nraz5rg78")))

(define-public crate-ptrace-0.1.0 (c (n "ptrace") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "posix-ipc") (r "^0.0") (d #t) (k 0)))) (h "0qfxg74mijdddyr3cwkpp82lwdipsxs80cpn6jig2nnrxbhkahrk")))

(define-public crate-ptrace-0.1.1 (c (n "ptrace") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "posix-ipc") (r "^0.0") (d #t) (k 0)))) (h "1hwlrdd0v24wdnczmjdkfjdj2a8myyyg15avwf758lcbaxavlzxy")))

(define-public crate-ptrace-0.1.2 (c (n "ptrace") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "posix-ipc") (r "^0.0") (d #t) (k 0)))) (h "12j1r5akqnlyjqfyg5gy3vcw85si3vcc62mi3nrlvwspzrhcgwrg")))

