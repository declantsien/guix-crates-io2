(define-module (crates-io pt ra ptracer) #:use-module (crates-io))

(define-public crate-ptracer-0.1.0 (c (n "ptracer") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.47") (d #t) (k 0)))) (h "041n3p996jdblai5ibk3d9ddw87s31way88b3jkgjknxmwvbpfz2")))

(define-public crate-ptracer-0.3.0 (c (n "ptracer") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "goblin") (r "^0.1.3") (f (quote ("std" "endian_fd" "elf32" "elf64"))) (k 2)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "procfs") (r "^0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "09f910rivyhg64bqw2g0lgqdinkddxhahi2hhshma393g8nbpfr3")))

(define-public crate-ptracer-0.3.1 (c (n "ptracer") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "goblin") (r "^0.1.3") (f (quote ("std" "endian_fd" "elf32" "elf64"))) (k 2)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "procfs") (r "^0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1xxx3a09lhicdsfnrl8szlw24p35awlsscjbc70ppvlixzsrlqnj")))

