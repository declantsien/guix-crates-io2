(define-module (crates-io pt ra ptrace-do) #:use-module (crates-io))

(define-public crate-ptrace-do-0.1.0 (c (n "ptrace-do") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "proc-maps") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)))) (h "0hid1kjhailanqxff5qxjygbpvfzyw2n4vh0fsmb66y35gm8l7mq")))

(define-public crate-ptrace-do-0.1.1 (c (n "ptrace-do") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "proc-maps") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)))) (h "1ypxkb1qnwwc3vmzsldv6880qvf3fv2cjbfdwg4hzcsfs3dnc2yg")))

