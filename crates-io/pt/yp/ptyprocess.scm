(define-module (crates-io pt yp ptyprocess) #:use-module (crates-io))

(define-public crate-ptyprocess-0.1.0 (c (n "ptyprocess") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "0wa4xs38m6vfsj8wd5wyfq6ykmhl1ab2xp3k455qyvli1n193spk") (f (quote (("sync") ("default" "sync") ("async" "async-fs" "futures-lite"))))))

(define-public crate-ptyprocess-0.1.1 (c (n "ptyprocess") (v "0.1.1") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0c8wszdmq3w4yagfyn3vr0pdkg15dmc4nq9igng768phrbksa4k1") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.2 (c (n "ptyprocess") (v "0.1.2") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0ba7v5x9pbd18brmmr6zd62qs7yq9x244xij3jxqbrl4n690a848") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.3 (c (n "ptyprocess") (v "0.1.3") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0xkxfbay4j9ajjsgd0zxj8pwa6fg6z9xkjkg0f33nxmawkmsp77m") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.4 (c (n "ptyprocess") (v "0.1.4") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0i4ihr11ci9m0wdkn2wn59ksd9rzp54n6ngz1mgg0malis3l0v3d") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.5 (c (n "ptyprocess") (v "0.1.5") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "1bhbaimigndl3n3p18hhkr4wh4yz6ld25jpdaklc2k5d2vl1rw4p") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.6 (c (n "ptyprocess") (v "0.1.6") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "06phn2lii300glblpzyh8iv8iqym3whs0cv8iqr01l741mksws4y") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.7 (c (n "ptyprocess") (v "0.1.7") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "19hz8az55c830jbcjg12a4x7r5bja8acc8wg49qw9bh64azs2lgb") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.1.9 (c (n "ptyprocess") (v "0.1.9") (d (list (d (n "async-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0vry9z3a1xh4ardb88h5vi9rfdyhs780fsh30pj2grkb5kn07app") (f (quote (("sync") ("default" "sync") ("async" "async-io" "futures-lite" "pin-project"))))))

(define-public crate-ptyprocess-0.2.0 (c (n "ptyprocess") (v "0.2.0") (d (list (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "0ykqgvrqdfa7j9xp0arlg8giczfslfj0zj9q0vvqsfy56i6bdyxi")))

(define-public crate-ptyprocess-0.3.0 (c (n "ptyprocess") (v "0.3.0") (d (list (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "05qrrrhwg1b4j1sg4cdypv0sh3i3471ryh39kphvyhnqpz78zhk9")))

(define-public crate-ptyprocess-0.4.0 (c (n "ptyprocess") (v "0.4.0") (d (list (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "0qk9ahicg02r7dc320g3jias6blk0kc521d93zym5vasachlz0ip")))

(define-public crate-ptyprocess-0.4.1 (c (n "ptyprocess") (v "0.4.1") (d (list (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1kz8gzsr2pjpxcxy708338wcdqnxg3cpfbca8q8a44gvpvvsw1by")))

