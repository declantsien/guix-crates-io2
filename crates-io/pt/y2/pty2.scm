(define-module (crates-io pt y2 pty2) #:use-module (crates-io))

(define-public crate-pty2-0.1.0 (c (n "pty2") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r ">=0.2") (d #t) (k 0)))) (h "1m8m5avvlihb7v6b6ib5sz0c9iwv108ya8762a8lnrwrcgwyfqa4") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

