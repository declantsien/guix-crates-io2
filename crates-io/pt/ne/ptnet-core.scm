(define-module (crates-io pt ne ptnet-core) #:use-module (crates-io))

(define-public crate-ptnet-core-0.1.0 (c (n "ptnet-core") (v "0.1.0") (h "096vxsrwk9a679hgfyrwf4xmc5g81m6rcp9r58497l1s208kik7k")))

(define-public crate-ptnet-core-0.1.1 (c (n "ptnet-core") (v "0.1.1") (h "1vj6fr044gwzry5vs678m62anda389gw8n9z3hq7k1g53sfq1r7c")))

(define-public crate-ptnet-core-0.1.2 (c (n "ptnet-core") (v "0.1.2") (h "12lh0jzgx27gghp42ax8zrb30q9pb4vd80jlyarhvi0a514ly1sm")))

