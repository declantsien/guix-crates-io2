(define-module (crates-io pt ne ptnet-elementary) #:use-module (crates-io))

(define-public crate-ptnet-elementary-0.1.0 (c (n "ptnet-elementary") (v "0.1.0") (d (list (d (n "ptnet-core") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dh1z389nx7gp23fghnr14aj66vlamvr7i8sddvradgw7i2x0raa")))

(define-public crate-ptnet-elementary-0.1.1 (c (n "ptnet-elementary") (v "0.1.1") (d (list (d (n "ptnet-core") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zc6ralxgbjwiizpf4cm4yh2hs0zzdi8gky53gxq7w22iq591bwc")))

(define-public crate-ptnet-elementary-0.1.2 (c (n "ptnet-elementary") (v "0.1.2") (d (list (d (n "ptnet-core") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08w1rpa1vvn7wyy2719ky7mqf49wrg9cs01cjc4rgzs1nylf2mad")))

