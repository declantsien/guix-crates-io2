(define-module (crates-io pt ai ptail) #:use-module (crates-io))

(define-public crate-ptail-0.1.0 (c (n "ptail") (v "0.1.0") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)))) (h "1sj9z3ccxnssqn2ka94snrjl5pfadlqblfj9k8zp12swbc38ssby")))

(define-public crate-ptail-0.2.0 (c (n "ptail") (v "0.2.0") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "duct") (r "^0.13.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)))) (h "16xrq2mac2p4b550kc621wcqay97hf0nq2f2lvjqbzws3nyb5b0s")))

(define-public crate-ptail-0.2.1 (c (n "ptail") (v "0.2.1") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "duct") (r "^0.13.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)))) (h "17fgh4i6xsrzcmlni9l8nc25l22fxr1fhvd57c45b7dvcn1sz0l9")))

(define-public crate-ptail-0.2.2 (c (n "ptail") (v "0.2.2") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "duct") (r "^0.13.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)))) (h "0n84lndzsikkv27cjsp5m2rv7q6rj6f7r689rsviq72my77j7xin")))

