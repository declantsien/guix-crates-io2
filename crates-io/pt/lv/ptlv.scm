(define-module (crates-io pt lv ptlv) #:use-module (crates-io))

(define-public crate-ptlv-0.1.0 (c (n "ptlv") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "156vkvl214im21saq93ax0by3rv1cgs62dy091miaq4g7mwqsnhf")))

(define-public crate-ptlv-0.1.1 (c (n "ptlv") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1i10yqr5by553zdya3sc280b9da6nnak8za94aq1d0y154xplj75")))

