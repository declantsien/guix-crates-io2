(define-module (crates-io pt r- ptr-origin-tracker) #:use-module (crates-io))

(define-public crate-ptr-origin-tracker-0.1.0 (c (n "ptr-origin-tracker") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)))) (h "016rbkpybsafgxiwqrgfjca5b8xgrmwzsz2iirrbhf807qjlz046")))

(define-public crate-ptr-origin-tracker-0.1.1 (c (n "ptr-origin-tracker") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)))) (h "0f7k48xwg78bqqlp0kd1np08qbg6h94311ywa0hyh3kyg3hyvp64")))

(define-public crate-ptr-origin-tracker-0.2.0 (c (n "ptr-origin-tracker") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "02wg90lvrqi9194d0nnxvf3xiy52gvlc6rgmb1r06glhcwrim4w5")))

