(define-module (crates-io pt r- ptr-union) #:use-module (crates-io))

(define-public crate-ptr-union-1.0.0 (c (n "ptr-union") (v "1.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "02diz0kqvz2c5s2lj2ailkjipq9ydzfrb19r52pds7i1lvlw74g1") (f (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (y #t)))

(define-public crate-ptr-union-1.0.1 (c (n "ptr-union") (v "1.0.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)))) (h "0v2gh7pkh1xgw58aw5w4d8qaffdalxvhavw03lvlly085kmaxbfh") (f (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (y #t)))

(define-public crate-ptr-union-1.2.0 (c (n "ptr-union") (v "1.2.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "0gddd02m2bh2zxpksq194b8knmragj60a0ksg77na0zpbm00zpgx") (f (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (y #t)))

(define-public crate-ptr-union-2.0.0-pre (c (n "ptr-union") (v "2.0.0-pre") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "09xdsrpm01xhxlzpla4q8m8rjw18j96vwlws59gs01pa1a459x8i") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2.0.0 (c (n "ptr-union") (v "2.0.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "17swjfmkccsc73zfif4k0l6bnrsg6rnbjqx84wgl2xsyvqk9m1vj") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2.0.1 (c (n "ptr-union") (v "2.0.1") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "1b6dsspp7nvisjisk7bcjzpx20wjy547im614rj3pb6kb80dyzkv") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2.1.0 (c (n "ptr-union") (v "2.1.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "1qnfm1m2qry9nfw423v16c435l7055m2cnzs2qp8x5q1i6v8y3kl") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2.2.0 (c (n "ptr-union") (v "2.2.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0cja6xm05lzykxhkgz7304vax10yf4g4kb0dsrm89piv5b8zw2x9") (f (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (y #t)))

(define-public crate-ptr-union-2.2.1 (c (n "ptr-union") (v "2.2.1") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1khfcyfka97nzzrd03633587p6781x70xld3vrhrbjvd2s1nmfgn") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2.2.2 (c (n "ptr-union") (v "2.2.2") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "erasable") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0fipvkkjc9pdwx4ghjv77v20yx5k98gd4xdw09qs5pk1y6jbav13") (f (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

