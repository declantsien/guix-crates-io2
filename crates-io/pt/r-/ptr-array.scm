(define-module (crates-io pt r- ptr-array) #:use-module (crates-io))

(define-public crate-ptr-array-0.1.0 (c (n "ptr-array") (v "0.1.0") (h "01snfn26szsdwc1fza358c4sqwhb99v07pbwvxqmbhnw1sx1ji9n")))

(define-public crate-ptr-array-0.1.1 (c (n "ptr-array") (v "0.1.1") (h "18dwjs210q9186dlniz3rvqd8igcvkk4449sy28mzgpddj8rmmvc")))

