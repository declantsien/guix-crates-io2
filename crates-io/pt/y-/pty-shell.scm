(define-module (crates-io pt y- pty-shell) #:use-module (crates-io))

(define-public crate-pty-shell-0.1.0 (c (n "pty-shell") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "mio") (r "^0.5.0") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "18zyxqylx1mn0pmrqxbk90swf34p7r7yr9hlxa777llqchp7z0h3")))

(define-public crate-pty-shell-0.1.1 (c (n "pty-shell") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "mio") (r "^0.5.0") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "0fnh6qv3pbj6dsjwg7xjl383aixz13rb3i7pcx0d6pqyydd0c3v7")))

(define-public crate-pty-shell-0.1.2 (c (n "pty-shell") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "mio") (r "^0.5.0") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "1rqbs9z4sbm8nmnizmbxxbachngxljzl114r5rjq1bp7irr820g9")))

(define-public crate-pty-shell-0.1.3 (c (n "pty-shell") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "0ccf0yf2m8zjzi5qima8gnzi5wcsxk1lk6fjrfkf0pji6c2krc4i")))

(define-public crate-pty-shell-0.1.4 (c (n "pty-shell") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)) (d (n "pty") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "1b36zv7cr2fxfgwfamqdk1r6hkaqf1fp4rrsbsawwwwchlzvi5cn")))

(define-public crate-pty-shell-0.2.0 (c (n "pty-shell") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)) (d (n "pty") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2.0") (d #t) (k 0)))) (h "1x6bwl75bwby5c37rq18kidc90i6mbyhbr67xx6xy1xm32iavn83")))

