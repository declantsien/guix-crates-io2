(define-module (crates-io pt ah ptah_derive) #:use-module (crates-io))

(define-public crate-ptah_derive-0.1.0 (c (n "ptah_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02ahnmd39ls1fai3pxflscdm4xb4bpy1d14cr5kl7jv39475y30j")))

(define-public crate-ptah_derive-0.2.0 (c (n "ptah_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0xnm3wfwivd9wbqbicnmxvdv0yk8s085sn0ds06ypdlkqz5f7rx6")))

