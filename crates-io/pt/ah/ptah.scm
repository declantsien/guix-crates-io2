(define-module (crates-io pt ah ptah) #:use-module (crates-io))

(define-public crate-ptah-0.0.0 (c (n "ptah") (v "0.0.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "0zdwgxlz4l7zl13vcryhzsjq11qr9b4q3z8fvi4zsa5jvw7jnw9i")))

(define-public crate-ptah-0.1.0 (c (n "ptah") (v "0.1.0") (d (list (d (n "ptah_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1w5996d0g7j7kvbx358mf09iafnrm8ys4kccla304klb4mciadm2") (f (quote (("derive" "ptah_derive") ("default" "alloc" "derive") ("alloc"))))))

(define-public crate-ptah-0.2.0 (c (n "ptah") (v "0.2.0") (d (list (d (n "ptah_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1gdfzri4c75vqd4pki151cw0ncgfk0vdfmyl6pfq5rsmqisrqclv") (f (quote (("derive" "ptah_derive") ("default" "alloc" "derive") ("alloc"))))))

