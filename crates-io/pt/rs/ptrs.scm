(define-module (crates-io pt rs ptrs) #:use-module (crates-io))

(define-public crate-ptrs-0.1.0 (c (n "ptrs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "10f6mx5ppd3gs6hdxciwvs0z6mflnx3n0wmhc7fdsncha3n8yqjh") (f (quote (("default") ("debug")))) (r "1.70")))

