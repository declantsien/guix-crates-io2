(define-module (crates-io pt as ptask) #:use-module (crates-io))

(define-public crate-ptask-0.1.0 (c (n "ptask") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1zyi887ic256ba014aij619nda10w51198pjdq4899cq6rlxqk0i") (f (quote (("inlining") ("default" "inlining"))))))

