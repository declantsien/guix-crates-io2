(define-module (crates-io pt sd ptsd) #:use-module (crates-io))

(define-public crate-ptsd-0.1.0 (c (n "ptsd") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2i32k6pgr06xljjnk5mxiway9k8h9h7mzw04h8wyckjg40znn9")))

