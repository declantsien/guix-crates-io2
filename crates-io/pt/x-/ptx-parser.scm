(define-module (crates-io pt x- ptx-parser) #:use-module (crates-io))

(define-public crate-ptx-parser-0.1.0 (c (n "ptx-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "15rpk6k6k50m3gdcjfwq9bjh4r5kdb527768321q50r2dw72c0va")))

(define-public crate-ptx-parser-0.1.1 (c (n "ptx-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "1i4mfi8fxizv443qs4acigvlz6rl60nr1lkm2dm6jly18gc9n72a") (f (quote (("std" "nom/std"))))))

(define-public crate-ptx-parser-0.1.2 (c (n "ptx-parser") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "170cks2dvkv873pycx721z9d4w648qfgr40bwfj8501kd6lh28w1") (f (quote (("std" "nom/std"))))))

(define-public crate-ptx-parser-0.1.3 (c (n "ptx-parser") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "16kc4gysad7ivpbzbw6k0nf9j22fddv1pyg7np9gmp2qgcj6a1j0") (f (quote (("std" "nom/std"))))))

(define-public crate-ptx-parser-0.1.4 (c (n "ptx-parser") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "1xysjag5hb8pcb2s0q25yrxgaqh4sg1p5avhzp68lamjjvx1ssf6") (f (quote (("std" "nom/std"))))))

(define-public crate-ptx-parser-0.1.5 (c (n "ptx-parser") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "0iw9mc5vzy86j0p3xvzas1z2xi18vgq7652k8zwf8cnksa4kxb1w") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-ptx-parser-0.1.6 (c (n "ptx-parser") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (k 0)))) (h "1ia4hwq4d4ck1jdf7ajlh3i1gn2077dk8z563bfzhykl7ywq8vpy") (f (quote (("std" "nom/std") ("default" "std"))))))

