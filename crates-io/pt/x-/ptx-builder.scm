(define-module (crates-io pt x- ptx-builder) #:use-module (crates-io))

(define-public crate-ptx-builder-0.1.0 (c (n "ptx-builder") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1m0b8p21sb2cfxl2cca41qqrinwd0xnzm4l0zsrkghb6jxg0p4pi")))

(define-public crate-ptx-builder-0.2.0 (c (n "ptx-builder") (v "0.2.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1yncy6w4gvz2rs517x71bwp4pwg19i9gacmbx25x5plvj7bfgv26")))

(define-public crate-ptx-builder-0.2.1 (c (n "ptx-builder") (v "0.2.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "164p2nk6zgf317z0jmjycdf8w6wr9hzr5fg58aqxh6861y09bm1m")))

(define-public crate-ptx-builder-0.3.0 (c (n "ptx-builder") (v "0.3.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1lgrqpjs5rkjvk80l6v41hd1qw44nddybacakv3n69rdkvimyhay")))

(define-public crate-ptx-builder-0.3.1 (c (n "ptx-builder") (v "0.3.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1m13sqixinr2izrqidl5lxws055avb2k47wsa56ac0zw8ps0v2m1")))

(define-public crate-ptx-builder-0.3.2 (c (n "ptx-builder") (v "0.3.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1nzp0zc0cr8zvz9waf3shrglv72qna49bizxlccfyy5s0xfr4jrr")))

(define-public crate-ptx-builder-0.3.3 (c (n "ptx-builder") (v "0.3.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1b7mq1wflm4d200hf1dyqqrqfx4m9ia8m3ca9gdjdcx5m6xb3kvb")))

(define-public crate-ptx-builder-0.3.4 (c (n "ptx-builder") (v "0.3.4") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1rbj6g389rxgcvh94n9f57k4zpdxnnxhf3y6mpqvsdipbqfq09ma")))

(define-public crate-ptx-builder-0.3.5 (c (n "ptx-builder") (v "0.3.5") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "08xkk6dwl9zccccag6793d99sdadnxc8i6qq2gmzyfsvgfzw8kfc")))

(define-public crate-ptx-builder-0.3.6 (c (n "ptx-builder") (v "0.3.6") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1wbcc0fnf04l42fgwgy28m5mz48hbj3b2vhc0h1a6hjx7bf07ccd")))

(define-public crate-ptx-builder-0.3.7 (c (n "ptx-builder") (v "0.3.7") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1bj279v1b2578qils7vjbxjq257rsgv977kzcnggdw9lvnhdnaym")))

(define-public crate-ptx-builder-0.3.8 (c (n "ptx-builder") (v "0.3.8") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1wb3wsjxjidrr4yn4frnkzgc9ps0g2dc39lch7x96aw2kk9n5pph")))

(define-public crate-ptx-builder-0.4.0 (c (n "ptx-builder") (v "0.4.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1kscnw6wfanqq636gzi0pcgaaz872mn91146jiiny1lgzysyimqr")))

(define-public crate-ptx-builder-0.4.1 (c (n "ptx-builder") (v "0.4.1") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0pdsw809ryacs5sxrlqdkgp107iwk36386advgp4w1zp6q92c2by")))

(define-public crate-ptx-builder-0.5.0 (c (n "ptx-builder") (v "0.5.0") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0xa9wnl0nxm51vc73hpxlg0i78lvhh21lz8lm70zs2i7cay6zcln")))

(define-public crate-ptx-builder-0.5.1 (c (n "ptx-builder") (v "0.5.1") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1xwwh85yb3y99zcbflcix58ddxh1h5aipqrxiisr93c9igipbnbh")))

(define-public crate-ptx-builder-0.5.2 (c (n "ptx-builder") (v "0.5.2") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1sz0v316kw3aaycxsqdyq07xmz7395is29g59x8x06bi2n388s6p")))

(define-public crate-ptx-builder-0.5.3 (c (n "ptx-builder") (v "0.5.3") (d (list (d (n "antidote") (r "^1.0") (d #t) (k 2)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "085b3hg93pza8zq4blpnhlf3klizm3j3wwakaq69qwn0arsqwhkg")))

