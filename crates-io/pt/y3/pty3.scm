(define-module (crates-io pt y3 pty3) #:use-module (crates-io))

(define-public crate-pty3-0.1.0 (c (n "pty3") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7603qss04yxpvkw2gjfb88l5di2264y5hddfbj8jk9viw59jzk")))

(define-public crate-pty3-0.1.1 (c (n "pty3") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x71lqv4p4qc2hg0baxw638qld5dpqj8y5r8innqkzspg8jsp0vj")))

