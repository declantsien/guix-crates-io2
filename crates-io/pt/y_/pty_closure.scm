(define-module (crates-io pt y_ pty_closure) #:use-module (crates-io))

(define-public crate-pty_closure-0.1.0 (c (n "pty_closure") (v "0.1.0") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gkx4cn47dsqnn7xvsar0h22wg94dmqqgvxxq0a4fc3fyipazj62")))

(define-public crate-pty_closure-0.1.1 (c (n "pty_closure") (v "0.1.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zfxhic3xkx1ay02sa8z41y7hcm9lfn3avrwvwlb1s79ry2md50q")))

(define-public crate-pty_closure-0.1.2 (c (n "pty_closure") (v "0.1.2") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lgnqv14fl4yvvkh8bfrhk602xpwv7n427kr0j1ykhn29jr57zkz")))

