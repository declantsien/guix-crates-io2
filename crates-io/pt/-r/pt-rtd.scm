(define-module (crates-io pt -r pt-rtd) #:use-module (crates-io))

(define-public crate-pt-rtd-0.1.0 (c (n "pt-rtd") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)))) (h "1qhb5g0b020w4b4bwr2g4xaqsjihf2mch0b20x570547a4hxyn35")))

(define-public crate-pt-rtd-0.1.1 (c (n "pt-rtd") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)))) (h "0m58krg6553cyyl6h6hzvfpmk8ywqgmr07sz9acaj4avzqvys4qm")))

