(define-module (crates-io pt yt ptytest) #:use-module (crates-io))

(define-public crate-ptytest-0.1.0 (c (n "ptytest") (v "0.1.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "rustyline") (r "^5.0.5") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1") (f (quote ("mio-support"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "terminfo") (r "^0.6.1") (d #t) (k 0)) (d (n "vt100") (r "^0.8") (d #t) (k 0)))) (h "10qr5g5d4xf8xlb9r4p51pqgc0sh1am1rpcwxc5v0hs4kbvb8501")))

