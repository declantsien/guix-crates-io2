(define-module (crates-io pt ex ptex-sys) #:use-module (crates-io))

(define-public crate-ptex-sys-0.0.0 (c (n "ptex-sys") (v "0.0.0") (h "1nydh9571xnm9hq88k55i29m9xp929pgvvrccvvb5xz3x7n03d44")))

(define-public crate-ptex-sys-2.4.1 (c (n "ptex-sys") (v "2.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "1ka5w7w7b3w4ihqk3jzqzvhgyd3qrpzr37jhmpr5wyn5zl1nprn0") (y #t)))

(define-public crate-ptex-sys-0.0.1 (c (n "ptex-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "040fl21fcqbb9fwgs303y993zzsnaaazdli2l7qbg98y1rvf7pcy") (y #t)))

(define-public crate-ptex-sys-0.0.2 (c (n "ptex-sys") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "1n0l04zhn7vrwdxq8r1sbh8cfq42zz0gkwsa6vqz3hpdsgsqfv58") (y #t)))

(define-public crate-ptex-sys-0.0.3 (c (n "ptex-sys") (v "0.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "1s4vkgkmfvxxq4607rnv3l347q3yfi18f75s871pil15mmbzb12i") (y #t)))

(define-public crate-ptex-sys-0.0.4 (c (n "ptex-sys") (v "0.0.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "03803xirhlgr8pib8zixrv8dyp5b24ga87d4985wdb0y85y7kp17") (y #t)))

(define-public crate-ptex-sys-0.0.5 (c (n "ptex-sys") (v "0.0.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "11w92vw8kf3k2cys1g0a4xh83rpakhgdc12jc7dadxjid0yczpz3")))

(define-public crate-ptex-sys-0.0.6 (c (n "ptex-sys") (v "0.0.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "quick-xml") (r "^0.22") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "0r08l36qmmrz82ban1bl33gs1xi56y3c5574ay8pxysgvarjh9yh")))

(define-public crate-ptex-sys-0.2.0 (c (n "ptex-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (f (quote ("c++17"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0wrqxw91rrjqad8z4wax32gyw4gfq71s7r7jfzvqsqm0yz3gr1c4") (l "Ptex")))

(define-public crate-ptex-sys-0.3.0 (c (n "ptex-sys") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (f (quote ("c++17"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1qm5l1szsshzyrbx7lal2x85wbv1fzkrb963jmv1lp7sajpwf7sj") (l "Ptex")))

