(define-module (crates-io pt ex ptex) #:use-module (crates-io))

(define-public crate-ptex-0.0.0 (c (n "ptex") (v "0.0.0") (h "1lnl6k6qlcwmcg8vd0znqncn751wvwfv8132xznvwsl652bsd8ag")))

(define-public crate-ptex-0.0.1 (c (n "ptex") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ptex-sys") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16j6a3fgb9bg5xm0aarjncvdrk4l87w69rfybqzy6yvcgabxw64q")))

(define-public crate-ptex-0.0.6 (c (n "ptex") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_float_eq") (r "^1") (d #t) (k 0)) (d (n "ptex-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g27bm0jq8y186jfz6pjv3ynnr0rllaib2mzwz9wfsxsh5zy2nrw")))

(define-public crate-ptex-0.2.0 (c (n "ptex") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "assert_float_eq") (r "^1.0") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (f (quote ("c++17"))) (d #t) (k 0)) (d (n "ptex-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cbn4ylsdi5y9n02pgi8v95p0aa8s9daynzxb4psal2rnpz9icm8")))

(define-public crate-ptex-0.3.0 (c (n "ptex") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "assert_float_eq") (r "^1.0") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (f (quote ("c++17"))) (d #t) (k 0)) (d (n "half") (r "^2.4.0") (d #t) (k 0)) (d (n "ptex-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rplc6m2v4xv2jpkp9ixvbky92ixj7fl5j1j3kv81hf3arfj1p9n")))

