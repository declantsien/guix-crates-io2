(define-module (crates-io pt im ptime) #:use-module (crates-io))

(define-public crate-ptime-0.1.0 (c (n "ptime") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02mq50b5ahdd4mpg4kmz015xva6zb550ajb2kbj214k64iwv9ksf")))

(define-public crate-ptime-0.1.1 (c (n "ptime") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1506svb3vxw1mnqm7j75di810yk3xicxk9wyx59isvxf8z7q6b14")))

