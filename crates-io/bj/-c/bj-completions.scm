(define-module (crates-io bj -c bj-completions) #:use-module (crates-io))

(define-public crate-bj-completions-0.1.2 (c (n "bj-completions") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "0369459q77ya57ix0jlcphqjp87iip64hhnqq9d7w0nf4p1m7cbk") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

(define-public crate-bj-completions-0.1.3 (c (n "bj-completions") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "1bl0n2y8020cl9wkrbd7rs865b8s89p0d9bwj84rxil1j23p066r") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

(define-public crate-bj-completions-0.1.4 (c (n "bj-completions") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "1k2vkchn023zjngv1pg288d0k7prpj9ryb43ldryh5aaar9lg24m") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

(define-public crate-bj-completions-0.1.5 (c (n "bj-completions") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "05fdrnp9c77nfxjpbfjc5zq57km9lk5l8yryiyv3sqda40shp7h4") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

(define-public crate-bj-completions-0.1.6 (c (n "bj-completions") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "0sk916cgxdz7f3z6q4c58va0xm6x1mhxq5nrz2rp8hih05yvcrrx") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

(define-public crate-bj-completions-0.2.0 (c (n "bj-completions") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bj") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)))) (h "1mx2w5yyfxv7y7a5h3v81chi11f6074xpimf5kcklij933ah2i1w") (f (quote (("default") ("address32" "bj/address32") ("address20" "bj/address20"))))))

