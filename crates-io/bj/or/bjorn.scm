(define-module (crates-io bj or bjorn) #:use-module (crates-io))

(define-public crate-bjorn-0.1.0 (c (n "bjorn") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.1.0") (d #t) (k 0)) (d (n "regex") (r "~1.0.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "1lfsyilphwy4gsavwppn3z5ggx8v1xzyhkl82m8c7ysdzr6kj2w0")))

(define-public crate-bjorn-0.2.0 (c (n "bjorn") (v "0.2.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "~1.1.0") (d #t) (k 0)) (d (n "regex") (r "~1.0.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "19gb87yjdhig8n8aqcc4x53yf5rh5pd1djr3v3r8gh6zbxjpvg3z")))

