(define-module (crates-io bv rs bvrs) #:use-module (crates-io))

(define-public crate-bvrs-0.1.0 (c (n "bvrs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dil1qk803d039qwha5vnhra0z43mzng0x78gbrbnrg48y74hkk2")))

(define-public crate-bvrs-0.1.1 (c (n "bvrs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ymcxpfjc0p7zyq81cv0zxshpk2sc2x76dmk2bmyf7ws4yc5k4qa")))

