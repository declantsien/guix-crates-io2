(define-module (crates-io bv h- bvh-arena) #:use-module (crates-io))

(define-public crate-bvh-arena-1.0.0-beta.1 (c (n "bvh-arena") (v "1.0.0-beta.1") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "13p1sxqhz5nfa1maqg6ghf2rmi37hg3rwdvhp3k1359wp97dcpl1") (r "1.58")))

(define-public crate-bvh-arena-1.0.0-beta.2 (c (n "bvh-arena") (v "1.0.0-beta.2") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "0la91ja0d5aj159hayshf4f3xav8i3pzbqc5lmqaxwkj33mw999l") (r "1.58")))

(define-public crate-bvh-arena-1.0.0 (c (n "bvh-arena") (v "1.0.0") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "1aw7vnx8mizcb4v9si56khjbxw96i47h63z6vwvajjnxxycjyh5x") (r "1.58")))

(define-public crate-bvh-arena-1.1.0 (c (n "bvh-arena") (v "1.1.0") (d (list (d (n "glam") (r "^0.20.5") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "00sq08y6bqz8hw9wgmgkkdc8n8hahnvf1m6x5pqlxh1bi09gfnaq") (f (quote (("std" "slotmap/std") ("default" "std")))) (r "1.58")))

(define-public crate-bvh-arena-1.1.1 (c (n "bvh-arena") (v "1.1.1") (d (list (d (n "glam") (r "^0.20.5") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "0z6yxz8q7synmapcwa1ls6sxadpm6mra4n3vhnc3022r9cvzmmvm") (f (quote (("std" "slotmap/std") ("default" "std")))) (r "1.58")))

(define-public crate-bvh-arena-1.1.2 (c (n "bvh-arena") (v "1.1.2") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "0fcqi8plq28gfydrqvmhakbq38nyk28pa1pc7hgbqj95348y2ni8") (f (quote (("std" "slotmap/std") ("default" "std")))) (r "1.58")))

(define-public crate-bvh-arena-1.1.3 (c (n "bvh-arena") (v "1.1.3") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 2)) (d (n "slotmap") (r "^1.0") (k 0)))) (h "1g8ynfqbhigwyn3y5x4n1dn3gr6z8q9kfj9068adw63dy223sdqa") (f (quote (("std" "slotmap/std") ("default" "std")))) (r "1.56")))

