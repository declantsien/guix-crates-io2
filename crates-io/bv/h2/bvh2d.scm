(define-module (crates-io bv h2 bvh2d) #:use-module (crates-io))

(define-public crate-bvh2d-0.1.0 (c (n "bvh2d") (v "0.1.0") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "17dmk73mn73azad6yw6d8zb5ii39jiy56f7jp3icz1gf2z6ay9gh") (y #t)))

(define-public crate-bvh2d-0.1.1 (c (n "bvh2d") (v "0.1.1") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "1r3xrbi3cj8gp5wkiz8h5akkq7hsdlf1mp19sig6rqhn1az7wzi5")))

(define-public crate-bvh2d-0.1.2 (c (n "bvh2d") (v "0.1.2") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)))) (h "0jhwhni62hibyimxkh8kw7sdh9wy288pas3asd8jyfahy967zk2v")))

(define-public crate-bvh2d-0.2.0 (c (n "bvh2d") (v "0.2.0") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)))) (h "1flnnl52vfl7plpwcq8y7ryqmiv14zr2ygchbfzl73kfb0pj0w65")))

(define-public crate-bvh2d-0.3.0 (c (n "bvh2d") (v "0.3.0") (d (list (d (n "glam") (r "^0.23") (d #t) (k 0)))) (h "08nrv3x4h87n7gxmad3sd6bwlqk4i2y86383x358h1wmbqmwywa8")))

(define-public crate-bvh2d-0.3.1 (c (n "bvh2d") (v "0.3.1") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16p8hx2id65brj2xj3lwj6s372mynwdxpnr6zzczv3a3825yh75j") (s 2) (e (quote (("serde" "glam/serde" "dep:serde"))))))

(define-public crate-bvh2d-0.4.0 (c (n "bvh2d") (v "0.4.0") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g4rwjv8px28ishy0in08p902vpmy3m4av0xr9vmhlz3nyj6ya4l") (s 2) (e (quote (("serde" "glam/serde" "dep:serde"))))))

