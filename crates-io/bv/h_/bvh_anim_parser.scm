(define-module (crates-io bv h_ bvh_anim_parser) #:use-module (crates-io))

(define-public crate-bvh_anim_parser-1.0.0 (c (n "bvh_anim_parser") (v "1.0.0") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking"))) (o #t) (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "cargo") (r "^0.78.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1gka8m0bhzyjgdhq0gybbgn3vg5hw6rn5r4v00yfldqvcsi0lrva") (s 2) (e (quote (("visualize" "dep:bevy" "dep:bevy_panorbit_camera"))))))

(define-public crate-bvh_anim_parser-1.0.1 (c (n "bvh_anim_parser") (v "1.0.1") (d (list (d (n "bevy") (r "^0.13.2") (f (quote ("dynamic_linking"))) (o #t) (d #t) (k 0)) (d (n "bevy_panorbit_camera") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "cargo") (r "^0.78.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0lg0jwxjpdm6fvzf82igppf3di3wx5xl63sllvkfxkg4gfzdxbiy") (s 2) (e (quote (("visualize" "dep:bevy" "dep:bevy_panorbit_camera"))))))

