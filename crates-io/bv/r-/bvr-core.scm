(define-module (crates-io bv r- bvr-core) #:use-module (crates-io))

(define-public crate-bvr-core-0.0.2 (c (n "bvr-core") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dmgy2bq75w3sxnhig3ydhd4vdbylijd21rqqgwq0vadj9irh7hb")))

(define-public crate-bvr-core-0.0.3 (c (n "bvr-core") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arc-swap") (r "^1.6") (d #t) (k 0)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b7s4rva8r56b21zsd4g0i9x2h465a9ycqrjq12pni4di3blbyky")))

(define-public crate-bvr-core-0.0.4 (c (n "bvr-core") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arc-swap") (r "^1.6") (d #t) (k 0)) (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "memchr") (r "^2.6") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z35yg7fpr3sxwsf9pkfyd9ncsr7m8hp6q749dzg1j05gbsd6bkr")))

