(define-module (crates-io bv a_ bva_cli) #:use-module (crates-io))

(define-public crate-bva_cli-0.1.0 (c (n "bva_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "16h8znxlcpc0ksnwbgv6i4ai97z4wjfc1hz6vz0r1v8s3cigr5ry")))

(define-public crate-bva_cli-0.1.1 (c (n "bva_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0h3ax8xs6ib9ax075s89jpg4sipd2xl01v3bshwv13ahvyz1cz40")))

(define-public crate-bva_cli-0.1.2 (c (n "bva_cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0ppg0n297yyf6cwy9r0vyfqpn25bkf6dmg7m2vf6p5340ylknljq")))

(define-public crate-bva_cli-0.1.3 (c (n "bva_cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1njk7q87s7fw1n95xr17x6ivqwxhk5an0l8gxb4w4jdzpzhzjp1b")))

(define-public crate-bva_cli-0.1.4 (c (n "bva_cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "normpath") (r "^1.1.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0yckmw3mjvsph29r8jxpsibcfrwkgxw4ixhh1cralg0yagbggrai")))

(define-public crate-bva_cli-0.1.5 (c (n "bva_cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bevy_vach_assets") (r "^0.1.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "05szpcdmhmmir5rdifpw1knjpl6xz97cikdr0b7sr04yq2lsikza")))

