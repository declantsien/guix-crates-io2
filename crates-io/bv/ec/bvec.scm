(define-module (crates-io bv ec bvec) #:use-module (crates-io))

(define-public crate-bvec-0.1.0 (c (n "bvec") (v "0.1.0") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "1phlc46506ciy0g1ymd77yl1d2bv2pb5xbl5pbyrrmn13gcachix")))

(define-public crate-bvec-0.1.1 (c (n "bvec") (v "0.1.1") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "1ar2sxlccg51plbw9kysm00v5ygzklzwxkyzh41c7armscsbxr91")))

(define-public crate-bvec-0.1.2 (c (n "bvec") (v "0.1.2") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)))) (h "09zynsvg295fxhwi33iah0x1qynwvf3741gh6gwlz996llkr4p6n")))

