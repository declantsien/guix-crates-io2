(define-module (crates-io bv m_ bvm_filter) #:use-module (crates-io))

(define-public crate-bvm_filter-0.0.0 (c (n "bvm_filter") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "elfkit") (r "^0.0.6") (d #t) (k 0)) (d (n "hash32") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0v74mvvcc9csl9pkh8lgnhq95k03ma2fydj5ir7hrrrvqrbm6cl6")))

(define-public crate-bvm_filter-1.0.0 (c (n "bvm_filter") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "elf") (r "^0.0.10") (d #t) (k 2)) (d (n "elfkit") (r "^0.0.6") (d #t) (k 0)) (d (n "hash32") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rr3g647zkp2a221v5lzr6ymcpg2x51is8s5k4mni5gpdxva5lig")))

