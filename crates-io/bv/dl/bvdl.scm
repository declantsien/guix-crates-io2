(define-module (crates-io bv dl bvdl) #:use-module (crates-io))

(define-public crate-bvdl-0.1.0 (c (n "bvdl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03dmvdczzsyi5vr5rb6pryqw1xqhxwghpzm6ncbpm07k7gm26fzw")))

