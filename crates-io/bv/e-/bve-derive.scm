(define-module (crates-io bv e- bve-derive) #:use-module (crates-io))

(define-public crate-bve-derive-0.0.1 (c (n "bve-derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bsj48q5gbin7ficlfnmnpnbl58bjadn7l1h8f67qn07x50fv8a0")))

