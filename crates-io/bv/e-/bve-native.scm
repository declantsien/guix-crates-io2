(define-module (crates-io bv e- bve-native) #:use-module (crates-io))

(define-public crate-bve-native-0.0.0-Placeholder (c (n "bve-native") (v "0.0.0-Placeholder") (h "0inhrncvhb8i43fig5r6px6vdssijd6hrj1vmk2fsdfbk9zjf797")))

(define-public crate-bve-native-0.0.1 (c (n "bve-native") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3.42") (d #t) (k 0)) (d (n "bve") (r "^0.0.1") (d #t) (k 0)) (d (n "bve-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1k7k8a8ymh8z7pdg06wndbbpwljqclwrm7hgsw6826v3j57spj6m")))

