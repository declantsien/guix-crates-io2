(define-module (crates-io bv re bvreader) #:use-module (crates-io))

(define-public crate-bvreader-0.1.0 (c (n "bvreader") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "07vw1p75cqbbfhhjff62fax4ca7acins9nsjhj571lpvifi58hps")))

(define-public crate-bvreader-0.1.1 (c (n "bvreader") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0fmvzi06i15iks4ln5jiax28f9qaznm1zkiyklp87368f75dh6ck")))

(define-public crate-bvreader-0.1.2 (c (n "bvreader") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0wygklkn6zbprl0fnqqf8lpyzl3wwa5rqfz88b25q5ibygynag4p")))

(define-public crate-bvreader-0.1.3 (c (n "bvreader") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1dn00yc50akj8g3z09j5x0f73h3ihd18r62c0v1935d1jy6p8iwl")))

(define-public crate-bvreader-0.1.4 (c (n "bvreader") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0zll70m79lkyj58zwjkpwgahv401xmzaw897swk7bkg1w1vizna9")))

(define-public crate-bvreader-0.1.5 (c (n "bvreader") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "11a09rdzrd20g926c6g2z9vfll5m29fvgg9zshb18yjxfhv1bbmj")))

