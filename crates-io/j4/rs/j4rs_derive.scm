(define-module (crates-io j4 rs j4rs_derive) #:use-module (crates-io))

(define-public crate-j4rs_derive-0.1.0 (c (n "j4rs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13d7n3vx2krazz0yh7ajl33qpbzfsv0ymdz3v7l7vnn8v4pdridk")))

(define-public crate-j4rs_derive-0.1.1 (c (n "j4rs_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ir2a6dmgbg6ark7qf8vc54rygmpahx3i9lzv11kdlj53nc4jvkf")))

