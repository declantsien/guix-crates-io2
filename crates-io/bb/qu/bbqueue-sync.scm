(define-module (crates-io bb qu bbqueue-sync) #:use-module (crates-io))

(define-public crate-bbqueue-sync-0.5.1 (c (n "bbqueue-sync") (v "0.5.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1igfx3aj12c3gjn1mxzh34p8y306i1n1szgv845zvk7hqyh26npc") (f (quote (("thumbv6" "cortex-m") ("defmt_0_3" "defmt"))))))

