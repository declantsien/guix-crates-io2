(define-module (crates-io bb qu bbqueue) #:use-module (crates-io))

(define-public crate-bbqueue-0.0.1 (c (n "bbqueue") (v "0.0.1") (h "0cj10fiaakdnjz01w5hnlp07vcinqp4kiiymdnhq6j5a74vgl8cb")))

(define-public crate-bbqueue-0.0.2 (c (n "bbqueue") (v "0.0.2") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "17v069wvh7xdk93cwam8s7vga73myfv34xjmpqpv0qx056sirph8")))

(define-public crate-bbqueue-0.1.0 (c (n "bbqueue") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0lpg7hh3ylbpf14y9g9c34x567kbcxhy0ard4qd1dpshx7x8nvcr")))

(define-public crate-bbqueue-0.1.1 (c (n "bbqueue") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0z17g8x30man8i3dr44hhq3p8yayyg7s41v00yxqlfwmp2rvv7k3")))

(define-public crate-bbqueue-0.2.0 (c (n "bbqueue") (v "0.2.0") (h "0gvsrj2ppvsh6i7i650hisji72bcvskrmqq8araidf82w9f71gg0") (f (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.2.1 (c (n "bbqueue") (v "0.2.1") (h "10b2hswxd3vdhc2d3svy15y9v2zn4vp1gl0k2j7vcacx48739ghv") (f (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3.0 (c (n "bbqueue") (v "0.3.0") (h "06s647k7p8vxnkc05gqmp3kxaynjyjdfk6cfxh11pb6iyyvwhllj") (f (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3.1 (c (n "bbqueue") (v "0.3.1") (h "1a5wmyadd1ngzpw0ciil4mw48hkda2glykmbq48kyhb22q8xlvdn") (f (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3.2 (c (n "bbqueue") (v "0.3.2") (h "1x0h8gpvl51sv3b3cdmcj2n303x3zny37sk1bblnsvqysbrd26gx") (f (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.4.0-alpha1 (c (n "bbqueue") (v "0.4.0-alpha1") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "19yzq00dc61hlqjgh2z5mc084zkwmk1gh4j8zm898d62xp3357wq") (f (quote (("std") ("default") ("cortex-m")))) (y #t)))

(define-public crate-bbqueue-0.4.0-alpha2 (c (n "bbqueue") (v "0.4.0-alpha2") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "1l347928yi8xpknwg0jkkfpxz21pw02rcyvdyxlgzjm25hwyvccq") (f (quote (("std") ("default") ("cortex-m")))) (y #t)))

(define-public crate-bbqueue-0.4.0-alpha3 (c (n "bbqueue") (v "0.4.0-alpha3") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "19lkp9f5jvvb8n5c8h1zdyya27y9v4rha98lcklvar6am9c7kz8i") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.0-alpha4 (c (n "bbqueue") (v "0.4.0-alpha4") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "00fh9lli4kick7qq9j7piymr5zszkd2b72nvas9z2ylid7376amh") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.0-alpha5 (c (n "bbqueue") (v "0.4.0-alpha5") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "1rs545knajjr2255vlpwhdf9g4bx82jjv0ak2v9frvf0yryb8ni6") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.0 (c (n "bbqueue") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "10ki7yqqilkfqy869vn2k3z06l3s4wi8k0yxszi2x4mazmyn8b15") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.1 (c (n "bbqueue") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "0yq5qhg5ka9f21n9zcwabp8q4c3d6rrvjfp8h1v5jvsb06rclydk") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.2 (c (n "bbqueue") (v "0.4.2") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "0ch9p2arf1my3zadfwkj9b81s17nfwmpj6fhv78ibl334g94p7bx") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.3 (c (n "bbqueue") (v "0.4.3") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "0pfndvkvphbj0wl0nyh0s7x1x8lkvv208i35l77a6qnnjfh319wq") (f (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.4 (c (n "bbqueue") (v "0.4.4") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "1pj50cnc06b0y5p42r5inm4grg7kchk5pyyzr0j7z6n7fqvih603") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.5 (c (n "bbqueue") (v "0.4.5") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "0zgf4vc4h201qh3k18wjx6nf2kwyv2gmrq5ka5aanxna7pprks9m") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.6 (c (n "bbqueue") (v "0.4.6") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "1sb5vqja2br1ng1lznm9hpcwm3s72d8983454njpf28h4227pfjr") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.7 (c (n "bbqueue") (v "0.4.7") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "0y1favc4kqgq0nzhwxhim531zq8vrvfs40w7iq1ms90vkm3xhszk") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.8 (c (n "bbqueue") (v "0.4.8") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "10lf2nr25wypqnj7imfjn22qnpqrnbwwz0piikd8axyskzrqrlr7") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.9 (c (n "bbqueue") (v "0.4.9") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "09y9rz46zp68kgvhbhgpxzvixqs04v96pqnm1xhr36iziczr3d3z") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.10 (c (n "bbqueue") (v "0.4.10") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "02lc52kiwbgycfi8mfmmd0a9jbwcwkyx1ijsbnsqwa84dndgsvrf") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4.11 (c (n "bbqueue") (v "0.4.11") (d (list (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r ">=0.13.0, <0.14.0") (d #t) (k 0)))) (h "1k147b7g5dvlwkjrwb57pxd0pzk2vdxnvddhxp2mm0w50dlcf1jk") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic")))) (y #t)))

(define-public crate-bbqueue-0.4.12 (c (n "bbqueue") (v "0.4.12") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)))) (h "1icl5xx6q316j7rfg6ajb95n6mxm90dmz3ypxy2gp0lx8mqjm88s") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.5.0 (c (n "bbqueue") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0n0y8j2ycyvla4rbpp1vkjanlgqmm9kapzaa4h96d7pnbanw1045") (f (quote (("thumbv6" "cortex-m"))))))

(define-public crate-bbqueue-0.5.1 (c (n "bbqueue") (v "0.5.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0m8azrn0xc158cg83z58r7qhwr54k42mr9rr210wg96ib64alfzx") (f (quote (("thumbv6" "cortex-m") ("defmt_0_3" "defmt"))))))

