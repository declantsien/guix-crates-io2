(define-module (crates-io bb qu bbqueue-ng) #:use-module (crates-io))

(define-public crate-bbqueue-ng-0.4.11 (c (n "bbqueue-ng") (v "0.4.11") (d (list (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r ">=0.13.0, <0.14.0") (d #t) (k 0)))) (h "0f7jxkcni9359pf5w58h02425g989dzbfb4ra868pwn5db9qhkja") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.100.0 (c (n "bbqueue-ng") (v "0.100.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "17n0zfhsd0vc8b8jnpmlkq4x0v8hcdsyxkjl5rn40c8z0bydy0zs") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.100.1 (c (n "bbqueue-ng") (v "0.100.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1zvr3zx5li4r07w0xawlaxm4s2maks4a3n3y6l5vnhknq12swcwp") (f (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.101.0 (c (n "bbqueue-ng") (v "0.101.0") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1vy8jrfiywsa6mkxnsrjwq1rwlxh1wv2abigdb3v70c99ahg9lra") (f (quote (("thumbv6" "cortex-m"))))))

(define-public crate-bbqueue-ng-0.101.1 (c (n "bbqueue-ng") (v "0.101.1") (d (list (d (n "cortex-m") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0wx24j5bsw9m30kzp3m6z2lxbabhnsa9cvzr3rja0qgf5c84gsgm") (f (quote (("thumbv6" "cortex-m"))))))

