(define-module (crates-io bb q- bbq-rs) #:use-module (crates-io))

(define-public crate-bbq-rs-0.1.0 (c (n "bbq-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)))) (h "1iws63d0ykhx6s2xnfanm9nmm4y7b8yddi5nk01n61v8dc30biip")))

(define-public crate-bbq-rs-0.1.1 (c (n "bbq-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)))) (h "1vrzwf6hb11yin3xn7qyw0ja5xzsshl5l2gmfci3psfpjgijxndd")))

