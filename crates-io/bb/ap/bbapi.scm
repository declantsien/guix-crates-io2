(define-module (crates-io bb ap bbapi) #:use-module (crates-io))

(define-public crate-bbapi-0.1.0 (c (n "bbapi") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)))) (h "0xysng5nk42d49gj60nxnhvy6ag0spc09s1z15l9bz376bh0d3fj")))

