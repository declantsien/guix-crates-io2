(define-module (crates-io bb cl bbclash) #:use-module (crates-io))

(define-public crate-bbclash-0.1.0 (c (n "bbclash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.2.1") (d #t) (k 0)))) (h "01ss4kwc1s9x3nymh07v1gbgf2ld1nhp8c6kz14q7kv6anp43ybb")))

(define-public crate-bbclash-0.1.1 (c (n "bbclash") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.2.1") (d #t) (k 0)))) (h "1vcsmnn3nw1dgrjx879ys59qjbzv34j6p3qx4vi3ws7rp3m1kyqz")))

(define-public crate-bbclash-0.1.2 (c (n "bbclash") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.2.1") (d #t) (k 0)))) (h "1md7pzm0vcymirwif41xdq37ln7q3nljrif115d9kgj9v4pv5ahz")))

(define-public crate-bbclash-1.0.0 (c (n "bbclash") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.3.3") (d #t) (k 0)))) (h "1m30jfd831yzs8xbk2yc7z3m320wr50ahrk574drqw28y89pwlvv")))

(define-public crate-bbclash-1.1.0 (c (n "bbclash") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.3.3") (d #t) (k 0)))) (h "1vr9v8j6anbwpid03klik88i4zmfcd2319fg2yjfz19wpjgldx52")))

(define-public crate-bbclash-1.1.1 (c (n "bbclash") (v "1.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rctree") (r "^0.3.3") (d #t) (k 0)))) (h "05isb2i1m9csplxc6sq0wzrxrz7b68b51wbfp9n6dxn94qvlwvin")))

