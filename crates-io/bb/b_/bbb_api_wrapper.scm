(define-module (crates-io bb b_ bbb_api_wrapper) #:use-module (crates-io))

(define-public crate-bbb_api_wrapper-1.0.2 (c (n "bbb_api_wrapper") (v "1.0.2") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_qs") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (d #t) (k 0)))) (h "01kyh85ddbmxi3qq6pvz2q2prskkd61ffmqz6zbs8k3v1jw1lqr8")))

