(define-module (crates-io bb fp bbfpromix) #:use-module (crates-io))

(define-public crate-bbfpromix-0.1.0 (c (n "bbfpromix") (v "0.1.0") (d (list (d (n "alsa-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "gio-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1kzk5k6y6gygskylfib9rj96xlmfvz82d5ym3dxvwsndzq4sfcpd")))

