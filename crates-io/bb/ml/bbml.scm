(define-module (crates-io bb ml bbml) #:use-module (crates-io))

(define-public crate-bbml-0.0.1 (c (n "bbml") (v "0.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "190w51fnpf6z9xzafbpdirvxjs55614jq8209gbdv1g34bjq9r44")))

(define-public crate-bbml-0.1.0 (c (n "bbml") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1bgqqas8ai6dwzy4mv5jiwgh3jglzppn131p5d0xip424kf1qa0b")))

(define-public crate-bbml-0.1.1 (c (n "bbml") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0lvs9mailzzanblrk4jkblkl6bhlnn3wgj8x519gv4r4m803zhbn")))

(define-public crate-bbml-0.2.0 (c (n "bbml") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.25.0") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "03r4579wh4bv5shcwa1k9n1vljz1wx8782qklmlyji6z9yba4m07")))

