(define-module (crates-io bb ec bbecs_tutorial) #:use-module (crates-io))

(define-public crate-bbecs_tutorial-0.1.0 (c (n "bbecs_tutorial") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1dbf5c4g4l9pjx1hpm3rbyfirlqz9wph5msy6v89slbzsz7aj21g")))

(define-public crate-bbecs_tutorial-1.0.0 (c (n "bbecs_tutorial") (v "1.0.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "19i1awb155kvcf1xgf6qj54wm87fjxra7113sy7y586ws3y6hzcb")))

(define-public crate-bbecs_tutorial-1.0.1 (c (n "bbecs_tutorial") (v "1.0.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1miaxjn42aghr8xpgm0mvwaa9iwrwk927fdhl5viykkhwynhsc0a")))

(define-public crate-bbecs_tutorial-1.0.2 (c (n "bbecs_tutorial") (v "1.0.2") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0ykk90sagk7mpvifg2b9dfyv9js637hbjl19k87hxbpjk2snxw53")))

(define-public crate-bbecs_tutorial-1.0.3 (c (n "bbecs_tutorial") (v "1.0.3") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "15zhl2vy2vy943dr49drnxw674vh8a1dwhmdw3sy7n21l77y29yh")))

