(define-module (crates-io bb ec bbecs) #:use-module (crates-io))

(define-public crate-bbecs-0.1.0 (c (n "bbecs") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1pm75wpkgqbrp12zllb9jgd9xsmxhiqjx1rimi3wfn0prmbvf8kf")))

(define-public crate-bbecs-0.1.1 (c (n "bbecs") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "187l72q8ymk7ali9n73v9cc23912414yjf97ryq7nkhgv7cx60m8")))

(define-public crate-bbecs-0.1.2 (c (n "bbecs") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1yx3lzb7ql83vqflnkvs6ck8304b55y2k6isynglkh4rnz1ga64q")))

(define-public crate-bbecs-0.1.3 (c (n "bbecs") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0nyak79ggjsszc5mwqqm9n902yjqvvg9xn33scc1939xyyvz6l32")))

(define-public crate-bbecs-0.1.4 (c (n "bbecs") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1ic8lzrh4w505lv0hlqg3r9nksss97db56vrxsigxwq75rln93b3")))

(define-public crate-bbecs-0.1.5 (c (n "bbecs") (v "0.1.5") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0vccis27psv29v82x260az3y4i33zprg0bd428m89p9hyrka436x")))

(define-public crate-bbecs-0.1.6 (c (n "bbecs") (v "0.1.6") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1mdqjvmhna76a9wc39hpjmaq02vv8jzpppcn2xs7k8dimhbl315c")))

(define-public crate-bbecs-0.1.7 (c (n "bbecs") (v "0.1.7") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1vk2w6nv92i295297c0k587j6bik11y000rrjca60hx64v6adnfk")))

(define-public crate-bbecs-0.1.8 (c (n "bbecs") (v "0.1.8") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ls3y7ca0y4475kmvcx547lc7d0n56k076s9b4m19nay68rmlrgf")))

(define-public crate-bbecs-0.1.9 (c (n "bbecs") (v "0.1.9") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0j2bqb6qfmz8rjl7gvqrn01vdrw2qx140dysfz9vw5y1aj5r82ir")))

(define-public crate-bbecs-0.1.10 (c (n "bbecs") (v "0.1.10") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1k1pajn14hd9747r5rm9pi0wh2amw45b9vyhh4k15v8cr4ljzba9")))

(define-public crate-bbecs-0.1.11 (c (n "bbecs") (v "0.1.11") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "04slmaa18hgalc0wx88kvjq9b4bj45iz5fngcr7ps7dgxghw8rf9")))

(define-public crate-bbecs-0.1.12 (c (n "bbecs") (v "0.1.12") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0v3pds2yx1xc5wammcprsga2r5gzhypjwyzkw7dp80cbzd0n9k3y")))

(define-public crate-bbecs-0.1.14 (c (n "bbecs") (v "0.1.14") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "119j1wqvzwxbb0r1vl7c97c5974q63hx47k1lhb2p4m3vz6c53gx")))

(define-public crate-bbecs-0.2.0 (c (n "bbecs") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ba1kdg44bgazbmxxniz66dbxdbxjk97807h2fjy05kqcjvykrny")))

(define-public crate-bbecs-0.3.0 (c (n "bbecs") (v "0.3.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "08j8kij62hhbshq695x881yf86v9jadf32d1rrbfrpbcx9j5kdsr")))

