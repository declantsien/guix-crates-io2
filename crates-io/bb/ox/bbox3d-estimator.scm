(define-module (crates-io bb ox bbox3d-estimator) #:use-module (crates-io))

(define-public crate-bbox3d-estimator-0.1.0 (c (n "bbox3d-estimator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "geo") (r "^0.22.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0r0ip1j4zz533qa7zx83d50indxiw6l10v7pi6mfjdqqk4v4jr0j")))

(define-public crate-bbox3d-estimator-0.2.0 (c (n "bbox3d-estimator") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "geo") (r "^0.24.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "189cdj1mz6r82js9xi7mwnx8wbbfz13yl36265rig3h4xq8p48v1")))

(define-public crate-bbox3d-estimator-0.3.0 (c (n "bbox3d-estimator") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "geo") (r "^0.24.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0mxasgwdsvhx6wi801c82pks86ii66v80j9jvfcnpy3v2zrcgnn5")))

