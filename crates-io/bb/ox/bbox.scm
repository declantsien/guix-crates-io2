(define-module (crates-io bb ox bbox) #:use-module (crates-io))

(define-public crate-bbox-0.7.0 (c (n "bbox") (v "0.7.0") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18jrnikck6xrfkxjws1nr76hbrcbjjbfjv9h5mghwmkn39b6r8s9")))

(define-public crate-bbox-0.7.1 (c (n "bbox") (v "0.7.1") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1myxfl5qimlacsarpiggynjdn70vs54951dbxsiq5mz2qjq9qcp0")))

(define-public crate-bbox-0.7.2 (c (n "bbox") (v "0.7.2") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n0ks1l00s1nqxmb7qwq3cyyg3z4zmlkf9n00ml45b9mgbfj3aw9")))

(define-public crate-bbox-0.7.3 (c (n "bbox") (v "0.7.3") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ckd2878ikbgji3znrcy9yfvv6c16hx1a5apxf7mdb72ljlyw8a3")))

(define-public crate-bbox-0.7.4 (c (n "bbox") (v "0.7.4") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "03qsr8k052g3s31hql70000bfbprmys93wpnxbm59wajhcsi6865")))

(define-public crate-bbox-0.7.5 (c (n "bbox") (v "0.7.5") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "118nakbnpy6h17n1jny2r6gz7qcarymbbs4xqfbs085h0qdw21hv")))

(define-public crate-bbox-0.7.6 (c (n "bbox") (v "0.7.6") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gbbn4nm5sljh11lqq2l8xmzkg8kkzj3xgvalplzl724v0y02yca")))

(define-public crate-bbox-0.8.1 (c (n "bbox") (v "0.8.1") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "06vw07iqqm8834xs0d534w87nxlyw16zf4bccdq4bxmbklgshv38")))

(define-public crate-bbox-0.8.2 (c (n "bbox") (v "0.8.2") (d (list (d (n "alga") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0y5jgvraf8pa7g2kr74nlwvwirqjxr3r0c9vm0wir70h59yvbxy1")))

(define-public crate-bbox-0.9.0 (c (n "bbox") (v "0.9.0") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zx0jq0c2lfiw175mqv5ljswf9z3kzkzicxcz60xp4sbk5d7r404")))

(define-public crate-bbox-0.9.1 (c (n "bbox") (v "0.9.1") (d (list (d (n "alga") (r "^0.7") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0min6svnp914hkrssa2zk32khafypsmbzd97n4gwfb753n6751i7")))

(define-public crate-bbox-0.9.2 (c (n "bbox") (v "0.9.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16m05hywz8l367m15n6d323lb70lrnyh7ddjnsacgkvkg0wg18ml")))

(define-public crate-bbox-0.10.0 (c (n "bbox") (v "0.10.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fd1wlnxsk7brq3sxa4awjqhrpsaqad5sh7sp4z8dlpzzngr50js")))

(define-public crate-bbox-0.11.0 (c (n "bbox") (v "0.11.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (f (quote ("alga"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pwmp67jv8c2fg3mrn3lprdx0djdiwqhmla8wjh0d1xgq96qgyxc")))

(define-public crate-bbox-0.11.1 (c (n "bbox") (v "0.11.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (f (quote ("alga"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10flkw0wpsy06lnahj0dgb08vvf154qn5gzka170zkzspjd9avaz")))

(define-public crate-bbox-0.11.2 (c (n "bbox") (v "0.11.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.22") (f (quote ("alga"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0v11qlj9gmdaqsn1msk1nydgik1rjjzydsc3k341xsnikhqyw4lp")))

