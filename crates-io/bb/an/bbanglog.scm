(define-module (crates-io bb an bbanglog) #:use-module (crates-io))

(define-public crate-bbanglog-0.1.0 (c (n "bbanglog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0rh28qhiwj9jq8kms1gq53smmrxpali6wnimy1f763skvsnq1409")))

