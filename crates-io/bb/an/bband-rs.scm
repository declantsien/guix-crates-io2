(define-module (crates-io bb an bband-rs) #:use-module (crates-io))

(define-public crate-bband-rs-0.1.0 (c (n "bband-rs") (v "0.1.0") (d (list (d (n "sd-rs") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "ta-common") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "009nyra14wd8jmvdy417gsbmwgkhgi6z8iz8nab6154b1rh4w8hz")))

