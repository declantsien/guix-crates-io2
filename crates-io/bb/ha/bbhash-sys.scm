(define-module (crates-io bb ha bbhash-sys) #:use-module (crates-io))

(define-public crate-bbhash-sys-0.1.0 (c (n "bbhash-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1kn6q9vxg5awn9vlrhp1bj1v6ij8s1q0036ik5wia50gjpj9427h") (l "bbhash")))

