(define-module (crates-io bb ha bbhash) #:use-module (crates-io))

(define-public crate-bbhash-0.1.0 (c (n "bbhash") (v "0.1.0") (d (list (d (n "bbhash-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)))) (h "0c46bj2n54lhvaw7bxys37xm9cd6xj9mlfbm1f7cprzjaj61iahy")))

(define-public crate-bbhash-0.1.1 (c (n "bbhash") (v "0.1.1") (d (list (d (n "bbhash-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)))) (h "0hpy9n1x49gywqcpp45mn2jia0vjqqmid4zx078ivi0f08smfb2a")))

