(define-module (crates-io bb gg bbggez) #:use-module (crates-io))

(define-public crate-bbggez-0.0.0 (c (n "bbggez") (v "0.0.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "021lfxhrm3s9q7k3zpxa1z8pnb5v8n3grrqhp6wxzpf2c6x5khw0")))

(define-public crate-bbggez-0.1.0 (c (n "bbggez") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1qqcd7d1wbxs2w50gq12dp924nrs9d0f56pkzzk65nmss3gkmmbm")))

(define-public crate-bbggez-0.2.0 (c (n "bbggez") (v "0.2.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "04n6ssahk0z9614j44q5yvgy7lkr48yb5pz1sm828cbri5zfp9a1")))

(define-public crate-bbggez-0.3.0 (c (n "bbggez") (v "0.3.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1gwjrjykbcrpqnpr8nf21jin4gsflw48wqjlqqa5wfmgg0h3ic5s")))

(define-public crate-bbggez-0.4.0 (c (n "bbggez") (v "0.4.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1i267mq08wc3nlnjgl34ljfnlgdxv6kaqfix2zx0l54jxkayn2kd")))

(define-public crate-bbggez-0.5.0 (c (n "bbggez") (v "0.5.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "094jqs02xsa0q1fkm2r8ihbpvlgxq52s18qfdy3d223pl4pn2qd1")))

(define-public crate-bbggez-0.5.1 (c (n "bbggez") (v "0.5.1") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1bj2b64xr8jy7cfhpdq0gld27v8v75fvll96wlxc3hhx2i6f2325")))

(define-public crate-bbggez-0.6.0 (c (n "bbggez") (v "0.6.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "11rfnn1l5sdj08ac4nw1q80d5pxm7082qzclfxbxycldj2llarq2")))

(define-public crate-bbggez-0.7.0 (c (n "bbggez") (v "0.7.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1aymhcxkk2bl9wgfsg626vh0h95f4zk4wvnrhk8ygbl59k47f2rc")))

(define-public crate-bbggez-0.8.0 (c (n "bbggez") (v "0.8.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "12qd7sg9rzizvvp7dmqb3n3v1mq74jnilw5h2yad1z1mc4k8j57z")))

(define-public crate-bbggez-0.8.1 (c (n "bbggez") (v "0.8.1") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1vqji2rwci9qlcaqnhgbaphmkvq5wjpg5ba9qx329wsb3b2azflr")))

(define-public crate-bbggez-1.0.0 (c (n "bbggez") (v "1.0.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0czb77kfpwplrdgr6cii78nksvgwibmcw85plvywh26ykyaxxx8r")))

(define-public crate-bbggez-1.1.0 (c (n "bbggez") (v "1.1.0") (d (list (d (n "ggez") (r "^0.5.0") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0qnywxy17dwa9jw8vvh1cfpr0gyjqc1xiyzf7jq2pfbxw8sbzxw2")))

