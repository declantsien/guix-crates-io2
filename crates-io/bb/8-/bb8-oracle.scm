(define-module (crates-io bb #{8-}# bb8-oracle) #:use-module (crates-io))

(define-public crate-bb8-oracle-0.1.0 (c (n "bb8-oracle") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "oracle") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (d #t) (k 0)))) (h "0kppxyn7sw976lc3fmashwchyp7xqxq6rnd6j4q9l199ywbcszjk") (f (quote (("chrono" "oracle/chrono"))))))

