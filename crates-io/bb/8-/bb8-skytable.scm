(define-module (crates-io bb #{8-}# bb8-skytable) #:use-module (crates-io))

(define-public crate-bb8-skytable-0.2.0 (c (n "bb8-skytable") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "skytable") (r "^0.5.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "148dsvjydrnfbd6yxdv79byjbwcg0cn4g6gymj58v5ffdsv6s9xw")))

(define-public crate-bb8-skytable-0.3.0 (c (n "bb8-skytable") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "skytable") (r "^0.6.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zshgc1yakfrlivjzvj9yjzzcpc633zrwsqx0fy1rlfbhdvisf8i")))

(define-public crate-bb8-skytable-0.3.1 (c (n "bb8-skytable") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "skytable") (r "^0.6.0") (f (quote ("async"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1w95ifhd94kvph9rn4hbbx4c60i66lrm8sk9q4mniaj0611zhs22")))

