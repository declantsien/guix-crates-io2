(define-module (crates-io bb #{8-}# bb8-tivk) #:use-module (crates-io))

(define-public crate-bb8-tivk-0.0.99 (c (n "bb8-tivk") (v "0.0.99") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tikv-client") (r "^0.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0acy7ys49944pmmkck11606nnmfnalmbax6s7lv5jaxc60icxhzk") (y #t)))

