(define-module (crates-io bb #{8-}# bb8-mongodb) #:use-module (crates-io))

(define-public crate-bb8-mongodb-0.1.0 (c (n "bb8-mongodb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1kvj73lgc8d84dh6xx6vcqs4sb030mwjrgdc6m3f5ijs8ycjfbsr")))

(define-public crate-bb8-mongodb-0.2.0 (c (n "bb8-mongodb") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1l6nqk6xl20qi8wna568yvhk6s9ddvmdgy6w6y5kxjbr8v84ldz9")))

(define-public crate-bb8-mongodb-0.2.1 (c (n "bb8-mongodb") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bb8") (r "=0.8.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0as9xqa973i7vwh5pfdkjmhk2w95397wfvzyrzfadji44pfaqk7z")))

(define-public crate-bb8-mongodb-0.2.2 (c (n "bb8-mongodb") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bb8") (r "=0.8.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ay2a93akjhhr0i6ins0czi3krdm8b4j503n6g4igys0fjsawqhz")))

(define-public crate-bb8-mongodb-0.2.3 (c (n "bb8-mongodb") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bb8") (r "^0.8.3") (d #t) (k 0)) (d (n "mongodb") (r "^2.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s17dyh2jvdan7ck2awx2gnblywyra1rqgfj6nrgqbv6asg3db16")))

