(define-module (crates-io bb #{8-}# bb8-surrealdb) #:use-module (crates-io))

(define-public crate-bb8-surrealdb-0.1.0 (c (n "bb8-surrealdb") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (d #t) (k 0)))) (h "0n67150d2258cwkcbqzrd77qjfikdzg936ls04xj114vair1sbv5") (f (quote (("tikv" "surrealdb/kv-tikv"))))))

(define-public crate-bb8-surrealdb-0.1.1 (c (n "bb8-surrealdb") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (d #t) (k 0)))) (h "1gs3l2ybfsbd4kdvl6b560ngp0lfi5pp3j8sjjz7jx0wc2xc170h") (f (quote (("tikv" "surrealdb/kv-tikv"))))))

(define-public crate-bb8-surrealdb-0.1.2 (c (n "bb8-surrealdb") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (d #t) (k 0)))) (h "1r8xxj6y5pidwx6bagfya40qfis1j6svc08wd2yn5nnnhr1yqqjw") (f (quote (("tikv" "surrealdb/kv-tikv"))))))

(define-public crate-bb8-surrealdb-0.1.3 (c (n "bb8-surrealdb") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (d #t) (k 0)))) (h "0g3ra569krw38kbwhppgl7jcz3qa7870ry47v29v5v10l6gsvd89") (f (quote (("tikv" "surrealdb/kv-tikv"))))))

