(define-module (crates-io bb #{8-}# bb8-rusqlite) #:use-module (crates-io))

(define-public crate-bb8-rusqlite-0.1.0 (c (n "bb8-rusqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "19zvpdcrkiid92ix6klqlh5ys1nbvfa75k895qfpj7crzdbpaz1v")))

