(define-module (crates-io bb #{8-}# bb8-failsafe) #:use-module (crates-io))

(define-public crate-bb8-failsafe-0.1.0 (c (n "bb8-failsafe") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.1") (d #t) (k 0)))) (h "0361zh57kdx94dwyxkwh472rm2qr8fwgdy1hbir11qkihm2xw078")))

(define-public crate-bb8-failsafe-0.1.1 (c (n "bb8-failsafe") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "05sy9hd904m424qphdf56hpwp1qjl9bv17w9nk9zg79c6qd8ix4c")))

(define-public crate-bb8-failsafe-0.1.2 (c (n "bb8-failsafe") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0gxl3wqpifx707hadjyk6hj2l84sxwi9fpfr1ppjhprfch678j52")))

(define-public crate-bb8-failsafe-0.1.3 (c (n "bb8-failsafe") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0icgfgympb2wb41z1kii32pmh9yahif7jgdrmmc58ggv5znxry6g")))

(define-public crate-bb8-failsafe-0.1.4 (c (n "bb8-failsafe") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0lkpmr1fayf1jp2x4irmhsv2iib5higrimfabsskj6l7w57mdwga")))

(define-public crate-bb8-failsafe-0.1.5 (c (n "bb8-failsafe") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z04zk9sj3wbq5x9a04g4hk1f7i5lg5qmfrpqbdhj8nh1nc498v4")))

(define-public crate-bb8-failsafe-0.1.6 (c (n "bb8-failsafe") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1q6rir892x3fhh1pb4bvgyz665imkkm7q5znzlh16b6xwifjmp3k")))

(define-public crate-bb8-failsafe-0.1.7 (c (n "bb8-failsafe") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1d39rf7mgb6xxq8mi9y6asvzwwlpmzniclw01gr7d6426wcba8c0")))

(define-public crate-bb8-failsafe-0.1.8 (c (n "bb8-failsafe") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "08arjridp3x8k5a0bd8wjvhk6l34n2q3s4px5awrj5azakhd73vy")))

(define-public crate-bb8-failsafe-0.1.9 (c (n "bb8-failsafe") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0a010qq7aa1lv8ij2mqckg0rd3i53cm3qq374jxarmd3vn9lq7jz")))

(define-public crate-bb8-failsafe-0.1.10 (c (n "bb8-failsafe") (v "0.1.10") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "failsafe") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "05d7w40xpd04f8grd376iwc85bwlmwf940qahbfvx6vjl6d88p15")))

