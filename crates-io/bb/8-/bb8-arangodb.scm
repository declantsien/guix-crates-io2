(define-module (crates-io bb #{8-}# bb8-arangodb) #:use-module (crates-io))

(define-public crate-bb8-arangodb-0.1.0 (c (n "bb8-arangodb") (v "0.1.0") (d (list (d (n "arangors") (r "^0.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1g3l4a2xhj7idc75lylkh7as4ncp8gjwn7fqxq2wcyfx5c52y4nv")))

(define-public crate-bb8-arangodb-0.2.0 (c (n "bb8-arangodb") (v "0.2.0") (d (list (d (n "arangors") (r "^0.5") (k 0)) (d (n "arangors") (r "^0.5") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0zblpc4kl22k95gbgrc2wfqbrbfk1inl0qibzj49jjfi9ffps5nv")))

