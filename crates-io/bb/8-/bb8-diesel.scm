(define-module (crates-io bb #{8-}# bb8-diesel) #:use-module (crates-io))

(define-public crate-bb8-diesel-0.1.0 (c (n "bb8-diesel") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking"))) (k 0)))) (h "0dcyzj94kdammvnvjm2ayjnf78f1k350lz34czqfny7ndfi00642")))

(define-public crate-bb8-diesel-0.1.1 (c (n "bb8-diesel") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking"))) (k 0)))) (h "17hdaki2qp8f0brqs4y4scm6g2hvy31a0b5dnr5v84rmcb756g7p") (y #t)))

(define-public crate-bb8-diesel-0.1.2 (c (n "bb8-diesel") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking"))) (k 0)))) (h "0qm3g1vpf0ly78xnlg01kfzfirvp3p2mkkiy61sb37z8aiw3lrw0")))

(define-public crate-bb8-diesel-0.1.3 (c (n "bb8-diesel") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "rt-threaded"))) (k 0)))) (h "1k4s60as54wajhfij52xqjqkngj01v0hiydih1wdgc86n42wcxgb")))

(define-public crate-bb8-diesel-0.2.0 (c (n "bb8-diesel") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "rt-threaded"))) (k 0)))) (h "0las7r4sab57gm73i3d1727ibbxf8b23q1q7mpm31gss0f0c7vns")))

(define-public crate-bb8-diesel-0.2.1 (c (n "bb8-diesel") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("r2d2"))) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "09h8mlb2x3jda6cx84il8qdr1zgmf0m9v8cq1n2zfvq8n097xj3r")))

