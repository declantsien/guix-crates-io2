(define-module (crates-io bb #{8-}# bb8-tikv) #:use-module (crates-io))

(define-public crate-bb8-tikv-0.1.0 (c (n "bb8-tikv") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tikv-client") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1g0bpw6fbdggnsr3m2lymjhhxjwhkclbn0hybazdf7aa1pgja2hc")))

