(define-module (crates-io bb #{8-}# bb8-sqlite) #:use-module (crates-io))

(define-public crate-bb8-sqlite-0.1.0 (c (n "bb8-sqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1dvagxfrg5y23npazgsghaypa4wqp31dwpdibavrqs69fx2pxg04")))

