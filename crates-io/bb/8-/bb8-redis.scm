(define-module (crates-io bb #{8-}# bb8-redis) #:use-module (crates-io))

(define-public crate-bb8-redis-0.3.0 (c (n "bb8-redis") (v "0.3.0") (d (list (d (n "bb8") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r ">= 0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0xmlp90l3mjs1nc20ynn790r182hczk90fwj3dca239z2y6g65wq")))

(define-public crate-bb8-redis-0.3.1 (c (n "bb8-redis") (v "0.3.1") (d (list (d (n "bb8") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "107zmhr7yfdjq4qm68f49xyi7qk13690ncrx6n6bmpryq0rmm51r")))

(define-public crate-bb8-redis-0.4.0 (c (n "bb8-redis") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)))) (h "0rjls3ga2f4q9npc76rkm6dhkcpfli5g6sm8sy32gjdr0nph6047")))

(define-public crate-bb8-redis-0.5.0 (c (n "bb8-redis") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0z9pdm3wgxzcp8psypfwwdcjkgays5bkfvj1f2352l4yvl17mpag")))

(define-public crate-bb8-redis-0.6.0 (c (n "bb8-redis") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.2") (d #t) (k 2)) (d (n "redis") (r "^0.17") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1gv2nphry4xck859x1h7pqq26wqnbv5fdhdn9cgf0di3h6mfgzmr")))

(define-public crate-bb8-redis-0.7.0 (c (n "bb8-redis") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.6.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.2") (d #t) (k 2)) (d (n "redis") (r "^0.18") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0h72dv83grv7kvv69n860vcp9xk7pxvx5d2j2awdkwj8yi5ymlpr")))

(define-public crate-bb8-redis-0.8.0 (c (n "bb8-redis") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.2") (d #t) (k 2)) (d (n "redis") (r "^0.19") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13x2lk68x25m864x85rl51ch8d44q6g0dqs321x748z0bva63q1s")))

(define-public crate-bb8-redis-0.8.1 (c (n "bb8-redis") (v "0.8.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.2") (d #t) (k 2)) (d (n "redis") (r "^0.19") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1mdrsbxlzrbjj2ax1jcrzvjz25cf3x7yd5a5ckqcnhh49qkbyxwa")))

(define-public crate-bb8-redis-0.9.0 (c (n "bb8-redis") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.2") (d #t) (k 2)) (d (n "redis") (r "^0.20") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jm688h98rp0f23p77sdcs9bc61papp5ww886arrwi45jx36bdlc")))

(define-public crate-bb8-redis-0.10.0 (c (n "bb8-redis") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.21") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "098v7ih71rphpr998i91mls7ls41kw14nnfidd8rvwbj7mhmix56")))

(define-public crate-bb8-redis-0.10.1 (c (n "bb8-redis") (v "0.10.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.7") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.21.1") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1plvn15y31zji91lh0prya21rpmizd28ibljr77b6sfb8majjh64")))

(define-public crate-bb8-redis-0.11.0 (c (n "bb8-redis") (v "0.11.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.21.1") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0x2crp34irdhvbyp3c061159r5yxp0zxdzj38md6qh80823f50dh")))

(define-public crate-bb8-redis-0.12.0 (c (n "bb8-redis") (v "0.12.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.22") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ckz3nh12m85ga966zwmxkrl0hs17bbsnq9240k07yi639qxl443") (r "1.56")))

(define-public crate-bb8-redis-0.13.0 (c (n "bb8-redis") (v "0.13.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.23") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0csmh5v0gbragnwq2f8limhi5pwmbazr8jkp67hsgi79z09a92v8") (r "1.60")))

(define-public crate-bb8-redis-0.13.1 (c (n "bb8-redis") (v "0.13.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.23") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hy9dxb3yga8dffby4yfxf2rq54a0z79f5lfwmgpyklfp9hn6ifd") (r "1.60")))

(define-public crate-bb8-redis-0.14.0 (c (n "bb8-redis") (v "0.14.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.24") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07vx1660yljci6mlngaqk74pxmh1vf35lcalxpyhq29kp4bvr520") (r "1.63")))

(define-public crate-bb8-redis-0.15.0 (c (n "bb8-redis") (v "0.15.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.25") (f (quote ("tokio-comp"))) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1bjkzd3af304pjmd4p045f7hpqcdb3c5ni3wcrghnx9snd0z3d3y") (r "1.63")))

