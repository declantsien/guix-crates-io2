(define-module (crates-io bb #{8-}# bb8-redis-cluster-async) #:use-module (crates-io))

(define-public crate-bb8-redis-cluster-async-0.1.0 (c (n "bb8-redis-cluster-async") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("cluster-async" "tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1fkbv37zcpw5f1za88yiqjg2dxjr5rwbh0ziyjjr8ij6qrabb3qv")))

