(define-module (crates-io bb #{8-}# bb8-redis-cluster) #:use-module (crates-io))

(define-public crate-bb8-redis-cluster-0.1.0 (c (n "bb8-redis-cluster") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.21.6") (f (quote ("tokio-comp"))) (k 0)) (d (n "redis_cluster_async") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "05i10gc3zxmqm63i3cqln0ywzj05yh9lna103il52h97k7vwbsg7") (r "1.56")))

(define-public crate-bb8-redis-cluster-0.1.1 (c (n "bb8-redis-cluster") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bb8") (r "^0.8.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 2)) (d (n "redis") (r "^0.23.0") (f (quote ("tokio-comp"))) (k 0)) (d (n "redis_cluster_async") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0mik0j4s445mqi2bfqq4miydy0zd6md78d57hd9n51f6pzbswzmi") (r "1.56")))

