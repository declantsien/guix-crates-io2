(define-module (crates-io bb #{8-}# bb8-nebula-graph) #:use-module (crates-io))

(define-public crate-bb8-nebula-graph-0.1.0 (c (n "bb8-nebula-graph") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.1") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.1") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "1qrr5xd3kxm2h37xgs8m5qya5ifnkp438nyj2hbwzrx02svym7hz")))

(define-public crate-bb8-nebula-graph-0.2.0 (c (n "bb8-nebula-graph") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.1") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.1") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "0ha3k8mk0hafdxpfjx2a45cwi4yz88hfd4lgz0mdv2lgprz0chhs")))

(define-public crate-bb8-nebula-graph-0.2.1 (c (n "bb8-nebula-graph") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.1") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.1") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "0fsjiaiivs5sz4jzm57wi2gqj9ds14jq7026jp71ak3q31bk0jbp")))

(define-public crate-bb8-nebula-graph-0.3.0 (c (n "bb8-nebula-graph") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.1") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.2") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "05f1ph58mic0f0ly0idbhln15x1z1p5lhq3a7jn4wk31y8aklx83")))

(define-public crate-bb8-nebula-graph-0.4.0 (c (n "bb8-nebula-graph") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.2") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.2") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "1d8nkw2m883vqlch6dj42mdk538qqfwcbjv52w5zfnv6acvdjrd3")))

(define-public crate-bb8-nebula-graph-0.4.1 (c (n "bb8-nebula-graph") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.3") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "0287fhmjws46mn1lxcqpwcxsyyxc8l2w61lya4r4pdb16aw7phbg")))

(define-public crate-bb8-nebula-graph-0.4.2 (c (n "bb8-nebula-graph") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.3") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "1w1ix7lz5kgx6sir2k6cp8iijv997w79hagic2fki06vgg9w1z74")))

(define-public crate-bb8-nebula-graph-0.4.3 (c (n "bb8-nebula-graph") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "^0.3") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "198ckind3qfgiidic99bc8gs2y6m3gd7xx84nmzcmmqpdrsw2ji3")))

(define-public crate-bb8-nebula-graph-0.4.4 (c (n "bb8-nebula-graph") (v "0.4.4") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bb8") (r "^0.4") (k 0)) (d (n "fbthrift-transport") (r "=0.4.3") (f (quote ("tokio_io"))) (k 0)) (d (n "nebula-graph-client") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net"))) (k 0)))) (h "128lf49skjhbw50bach63wbnq8lh474hjfc8rq9346bzb4jaawrp")))

