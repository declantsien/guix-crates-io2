(define-module (crates-io bb co bbcode-tagger) #:use-module (crates-io))

(define-public crate-bbcode-tagger-0.1.0 (c (n "bbcode-tagger") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0f1yzr9xxy0qrfnyk738r8q57bzlw86yznv89a4xdcjdnmnccqlk")))

(define-public crate-bbcode-tagger-0.1.1 (c (n "bbcode-tagger") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0adc7lgsj7lyxyg7v26gxh5wz172dmi84lmgxdm4xyikyr443c40")))

(define-public crate-bbcode-tagger-0.1.2 (c (n "bbcode-tagger") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0my8czkcah7ndm3vmdk7cx1jpgdx2sli8qqdc81iagldypqxp3dw")))

(define-public crate-bbcode-tagger-0.1.3 (c (n "bbcode-tagger") (v "0.1.3") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0898ic12papfrqd41bi4b80ym9jh05qkp17zzw5vlx424q4sci4j")))

(define-public crate-bbcode-tagger-0.1.4 (c (n "bbcode-tagger") (v "0.1.4") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1vz9yb4s2k2a1982mkhisrh4inwacwyz9x5qaf57glca4jb32z8w")))

(define-public crate-bbcode-tagger-0.2.0 (c (n "bbcode-tagger") (v "0.2.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "19x05wa4c3mn55mzhs12n48021gmz2iwsigbmhazzi46gml35lj9")))

