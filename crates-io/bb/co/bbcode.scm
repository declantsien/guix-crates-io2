(define-module (crates-io bb co bbcode) #:use-module (crates-io))

(define-public crate-bbcode-1.0.0 (c (n "bbcode") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0scf00ssclw95zkkxymv4azdm0ll7hzfharppmwn8wgk782ah57c")))

(define-public crate-bbcode-1.0.1 (c (n "bbcode") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0f5rnll5yv1fj6cpbdg9kq4ya3m8dwr6dc3awf4ifda8cmzgrqrf")))

(define-public crate-bbcode-1.0.2 (c (n "bbcode") (v "1.0.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0w8gqa7vm557gpwmkn6fk8qgq3xqxz175fcgh9ick4m30xrv4p6w")))

