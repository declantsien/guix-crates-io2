(define-module (crates-io bb d- bbd-lib) #:use-module (crates-io))

(define-public crate-bbd-lib-0.1.0 (c (n "bbd-lib") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1va8vjsahx48lc45bsirwzj1pr4xpfv6v1g2bfjcf24v7fibmcb1")))

(define-public crate-bbd-lib-0.1.1 (c (n "bbd-lib") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0d50gnz2s8pjfxhl61qp0y3cdbwc1jbkxcv0lchs8mn5vm6v2wnx")))

(define-public crate-bbd-lib-0.2.0 (c (n "bbd-lib") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1l5hd2pw7x09x5lk9pyasa3n85w576p25s9c63g57kgkyw97448g")))

