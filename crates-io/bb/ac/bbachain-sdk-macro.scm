(define-module (crates-io bb ac bbachain-sdk-macro) #:use-module (crates-io))

(define-public crate-bbachain-sdk-macro-0.1.1 (c (n "bbachain-sdk-macro") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15jcmpdwfl34i597pv4rac2crns0sxvapnq1wb09l8qmg0bbaj5w")))

