(define-module (crates-io bb ac bbachain-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-bbachain-frozen-abi-macro-0.1.1 (c (n "bbachain-frozen-abi-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mvz3agzcxgsq1b0dpxxkza0wfy8wvkrp8n2qcv40pr8lq412smm")))

