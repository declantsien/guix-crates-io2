(define-module (crates-io bb ac bbachain-notifier) #:use-module (crates-io))

(define-public crate-bbachain-notifier-0.1.1 (c (n "bbachain-notifier") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "brotli" "deflate" "gzip" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wz6h86dkf15ai0d2zhsjpf216acnjaa4zgfcrs6w94hf6d4wmg0")))

