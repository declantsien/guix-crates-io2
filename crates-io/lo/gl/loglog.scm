(define-module (crates-io lo gl loglog) #:use-module (crates-io))

(define-public crate-loglog-0.1.0 (c (n "loglog") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1i43knci4islszmgd3cw2mkcbqvlzgsmcshmsx8sawmfwv73a1ky")))

(define-public crate-loglog-0.2.0 (c (n "loglog") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0744z2fipvcpwlg1x4am7ybbji1877p862l6vjvfbhp0ibn7bbla")))

(define-public crate-loglog-0.3.0 (c (n "loglog") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0c3yb6xzjg4hwwwpkx9s1mg2przk164archs2a5sbf9cq5isgrqc")))

(define-public crate-loglog-0.3.1 (c (n "loglog") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1rg49lj08i8h6xqbirjdai7i8pawvfkba0hmc4lhw7cd0k6019xq")))

(define-public crate-loglog-0.3.2 (c (n "loglog") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1d67fm3b471fyckjv5r0jm8dhdd9g6y1hh5w5s9l9q2i3i7nbc87")))

