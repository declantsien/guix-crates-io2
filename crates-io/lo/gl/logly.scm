(define-module (crates-io lo gl logly) #:use-module (crates-io))

(define-public crate-logly-0.0.1 (c (n "logly") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorama") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "12fvyqydh48a96yvjksls19ngcpwnf31mx1sx6lyl7lz78vqqkdp")))

(define-public crate-logly-0.0.2 (c (n "logly") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorama") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "03nqhw8mb5cypg759dqhd5rj0l6qf64r4jah1rp2dw9zf0iq8vrh")))

(define-public crate-logly-0.0.3 (c (n "logly") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorama") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0apslji2qf20p899gacbhjnqskkvnxrvyac0701rjxg3p60v53fx")))

