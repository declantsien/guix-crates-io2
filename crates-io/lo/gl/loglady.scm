(define-module (crates-io lo gl loglady) #:use-module (crates-io))

(define-public crate-loglady-0.0.0 (c (n "loglady") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tui") (r "^0.3.0") (d #t) (k 0)))) (h "17l6qmfddwwkmv3jh7h8ydwwzb9fckybpkghmyn3cm6ar82qsvfa")))

