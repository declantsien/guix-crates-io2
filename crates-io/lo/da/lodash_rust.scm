(define-module (crates-io lo da lodash_rust) #:use-module (crates-io))

(define-public crate-lodash_rust-0.1.0 (c (n "lodash_rust") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l8qxs98f81lj4d0h4p0bpjj3kqjg80wrk96kiasi046wb7bqa5g")))

(define-public crate-lodash_rust-0.1.1 (c (n "lodash_rust") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gsgpx9yiwqis9wbl87xmcilxs5gxb4iscdq2bdqa60zpky09csk")))

(define-public crate-lodash_rust-0.1.2 (c (n "lodash_rust") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m7vx0vsiclzrvrra1cfzg69vbx75p87gxmqv335fqaahv1klavi")))

