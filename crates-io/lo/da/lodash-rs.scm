(define-module (crates-io lo da lodash-rs) #:use-module (crates-io))

(define-public crate-lodash-rs-0.1.0 (c (n "lodash-rs") (v "0.1.0") (h "0wpj4byq4ppvnqjrivm0acrzx2al169wgm0km5h6j2v4jwzdhz2p")))

(define-public crate-lodash-rs-0.1.1 (c (n "lodash-rs") (v "0.1.1") (h "02jsbzg363mgjbkyj4hjwdmacp2563s18scdl3x71c5ba5l7c89d")))

