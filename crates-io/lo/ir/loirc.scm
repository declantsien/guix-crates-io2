(define-module (crates-io lo ir loirc) #:use-module (crates-io))

(define-public crate-loirc-0.1.0 (c (n "loirc") (v "0.1.0") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0j8rqgqykbglj8m86qvrv6cyx4rjrhh4phhrpqsik3fbpnjh822v")))

(define-public crate-loirc-0.1.1 (c (n "loirc") (v "0.1.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1xrg8slb3qinpwbqjipm1c289vc07la23lli0bvc5577df59b09c")))

(define-public crate-loirc-0.1.2 (c (n "loirc") (v "0.1.2") (d (list (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "05p5vff6vgscwnyb7371fmv34zln9mg7qz5w1i68ia8p4341ipj1")))

(define-public crate-loirc-0.1.3 (c (n "loirc") (v "0.1.3") (d (list (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "1qkq6vsw9xkz6p8d8a56f9pi3xpvlx58ldff7350jrm0rv6grshd")))

(define-public crate-loirc-0.1.4 (c (n "loirc") (v "0.1.4") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "0l7cd584bbrbxcxb7j4n12mkg744wbq7a82vjzw8vmsvxnr0d6ij")))

(define-public crate-loirc-0.2.0 (c (n "loirc") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)))) (h "1jqr6ak3rgmwxvdmf0fng54zh1k86adxg23lld0f51d0ln36ji90")))

