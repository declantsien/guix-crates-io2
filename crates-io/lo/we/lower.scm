(define-module (crates-io lo we lower) #:use-module (crates-io))

(define-public crate-lower-0.1.0 (c (n "lower") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0hj36z12k6aldjc8i4cmci1lj1yc92za4gw4g6clmzzk8wmjmwpm") (y #t)))

(define-public crate-lower-0.1.1 (c (n "lower") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1975hm8mzi0slrf735lj93sjrd9xh90h6rlwywy5b3kl7w88dhhi")))

