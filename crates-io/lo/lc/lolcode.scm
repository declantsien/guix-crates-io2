(define-module (crates-io lo lc lolcode) #:use-module (crates-io))

(define-public crate-lolcode-0.1.0 (c (n "lolcode") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1q0ypff070r43bv745w2g50kw2c6fanmy8iavpxym0gx7312pr84")))

(define-public crate-lolcode-0.2.0 (c (n "lolcode") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1pwhgz9mc8h2sy5lqg39l5vnkwi45kylh28fgnmfsa6cpzk9pygr")))

(define-public crate-lolcode-0.3.0 (c (n "lolcode") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1i3j3cwv9df4swsknsc6cq2p2fw9zr29cjvpchx5w50w6qh7rr9s")))

(define-public crate-lolcode-0.3.1 (c (n "lolcode") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qk7hmvvrlli1bckrg3znm46bqyi07sdpzzsnrfp4ii6dbn7x4j2")))

