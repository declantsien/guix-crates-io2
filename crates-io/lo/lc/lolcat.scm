(define-module (crates-io lo lc lolcat) #:use-module (crates-io))

(define-public crate-lolcat-0.1.0 (c (n "lolcat") (v "0.1.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1mzh23hbvrbq2y0nf3hismspspjl8yzxkg796wf8007vs8c9r2nm")))

(define-public crate-lolcat-0.1.1 (c (n "lolcat") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "04bapxynzzxsphgzhd6wmbanxknz2bm7kwi600wd4dsq0dwn2a3s")))

(define-public crate-lolcat-1.0.0 (c (n "lolcat") (v "1.0.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1wzzi72mpw9jx5rpn0lwvmhnk1p0bz9am934ivfy37as34xi2qrb")))

(define-public crate-lolcat-1.0.1 (c (n "lolcat") (v "1.0.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1cr8y4vyzx83005vaspvcmms8r2np58r3si1nr6kzak7xbxnrxz4")))

(define-public crate-lolcat-1.1.0 (c (n "lolcat") (v "1.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0hm1k3fi0wqvqbwx7n6p24w95y5dsk2zidd5rv0l4ygmq8s6xjqg")))

(define-public crate-lolcat-1.2.0 (c (n "lolcat") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1fj1vyhg3siscdgvg73w31mm5gis4ngnqclp0n7psfcs770kry68")))

(define-public crate-lolcat-1.3.0 (c (n "lolcat") (v "1.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1zfhdn05xkwdk6y7xl9kc0ni26v0z0jm72g7a22zlajp8gn6skcf")))

(define-public crate-lolcat-1.3.1 (c (n "lolcat") (v "1.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ill7dyx6p0zwdjqns90kyd6rbcvnbkh3db4p3g4zgq6hi48h34z") (y #t)))

(define-public crate-lolcat-1.3.2 (c (n "lolcat") (v "1.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vxdqrmypki2k9fjs6v8h137myg5vnikflkb38xdakpqgh8agzmx")))

(define-public crate-lolcat-1.4.1 (c (n "lolcat") (v "1.4.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "utf8-chars") (r "^1.0.2") (d #t) (k 0)))) (h "1544mfykkrdr5mbr53b9k99qrqd57rcp0b8y5b5sz1ar12vc5g1v")))

(define-public crate-lolcat-1.5.0 (c (n "lolcat") (v "1.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)) (d (n "utf8-chars") (r "^1.0.2") (d #t) (k 0)))) (h "07b57y5zihppabphkg5rzmnmmimym42a6jkv0gpn311w70viwjja")))

