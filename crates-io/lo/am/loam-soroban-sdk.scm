(define-module (crates-io lo am loam-soroban-sdk) #:use-module (crates-io))

(define-public crate-loam-soroban-sdk-0.6.2 (c (n "loam-soroban-sdk") (v "0.6.2") (d (list (d (n "loam-sdk-macro") (r "^0.6.2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^0.9.2") (d #t) (k 0)))) (h "12dd392md6875icl6cxpj44ha447dl20c429cvpj7027kzi11ixk") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.3 (c (n "loam-soroban-sdk") (v "0.6.3") (d (list (d (n "loam-sdk-macro") (r "^0.6.2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^0.9.2") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1ad7ly67wri9xkfqzlyxf2yira2armh9y9h3irz9rq3iav7f2gx6") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.4 (c (n "loam-soroban-sdk") (v "0.6.4") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^0.9.2") (f (quote ("alloc"))) (d #t) (k 0)))) (h "11mn8z10n1dhryfxwghg5xh6w2rwk99yas581v54cv8ggd12anyq") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.5 (c (n "loam-soroban-sdk") (v "0.6.5") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)))) (h "02v4g6zzs0clw821r6j55hbfwc32m5z0mml1vs22b2dzm3gm0hhs") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.6 (c (n "loam-soroban-sdk") (v "0.6.6") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.3") (d #t) (k 0)))) (h "1xky12qykfy0rwpmpjsd18qmhd64m2xy6l8g707xgd5bh6gks6l1") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.7 (c (n "loam-soroban-sdk") (v "0.6.7") (d (list (d (n "loam-sdk-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)))) (h "0jhzp9rkqb7syiypvwxpadlapxpgl5545ax2w0nq5wdk94wqfkwv") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

(define-public crate-loam-soroban-sdk-0.6.8 (c (n "loam-soroban-sdk") (v "0.6.8") (d (list (d (n "loam-sdk-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^21.0.1-preview.2") (d #t) (k 0)))) (h "1d7r6bzjb5bl8lkc48yki76fcy1z0rral2a4b0x1zm00k7pi631x") (f (quote (("testutils" "soroban-sdk/testutils") ("default"))))))

