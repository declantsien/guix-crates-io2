(define-module (crates-io lo am loam-sdk) #:use-module (crates-io))

(define-public crate-loam-sdk-0.6.2 (c (n "loam-sdk") (v "0.6.2") (d (list (d (n "loam-sdk-macro") (r "^0.6.2") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0kaf1ibvzb5fpj4ibfl4gai2jq49rs5p17fr5v2ykajxvcdzhxc4") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.3 (c (n "loam-sdk") (v "0.6.3") (d (list (d (n "loam-sdk-macro") (r "^0.6.2") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "00m94cxb7jfdiy3db5v3xqwpwd6gzbz4cwwa2w9z4vxvpihdmqdq") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.4 (c (n "loam-sdk") (v "0.6.4") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1227rm58ydw7k5py7ix44acjh848ysgwjiwrfbi4ndr10wmm56qz") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.5 (c (n "loam-sdk") (v "0.6.5") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.5") (o #t) (d #t) (k 0)))) (h "0smz9qg9h8aq3kgx4711x0f5n0d3pzz80xmfpjqfz8jx75s6d221") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.6 (c (n "loam-sdk") (v "0.6.6") (d (list (d (n "loam-sdk-macro") (r "^0.6.4") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "02k0fccv0p499mrqphlvwrxcfiyppl9cvnv52xra9jk9xks28f7p") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.7 (c (n "loam-sdk") (v "0.6.7") (d (list (d (n "loam-sdk-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0h58l1qji2avsgslkhj3i8jhg6hfrrllfpjhjxrsy4zqjjp7xvk7") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

(define-public crate-loam-sdk-0.6.8 (c (n "loam-sdk") (v "0.6.8") (d (list (d (n "loam-sdk-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "loam-soroban-sdk") (r "^0.6.8") (o #t) (d #t) (k 0)))) (h "1liyciwc7papp6nzhvrr0y5kj7fcnb4jnh42kqjb0pf9lgmz4zsy") (f (quote (("soroban-sdk-testutils" "loam-soroban-sdk/testutils") ("default" "loam-soroban-sdk"))))))

