(define-module (crates-io lo am loam-build) #:use-module (crates-io))

(define-public crate-loam-build-0.1.0 (c (n "loam-build") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0kpyfr5pj1cwnak1y9hw99g3mvvrkazpv19vzq3kx3dcflyl334m")))

(define-public crate-loam-build-0.6.2 (c (n "loam-build") (v "0.6.2") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "11rjmac6gk7miq5qxcw5i11ir9zbfpcxrz3i59wh031w6llpakp0")))

(define-public crate-loam-build-0.6.3 (c (n "loam-build") (v "0.6.3") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0qn8x1w93ri8hkvfr94ybgm3fgwqmf2vp0d83vd4a3qq5fkif4dr")))

(define-public crate-loam-build-0.6.4 (c (n "loam-build") (v "0.6.4") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1lc8dlyj51mxprhm626kcl821fj4s5zfr0rqpqxcipz4i3q7cn7v")))

(define-public crate-loam-build-0.6.5 (c (n "loam-build") (v "0.6.5") (d (list (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0mks76ggdaqvifr4blqrcwl6ib6nczg09cq632i0gjpvn3p1ka5h")))

(define-public crate-loam-build-0.7.0 (c (n "loam-build") (v "0.7.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0g1xcn0hqs26fb9m0hkn5k6v3fxm1pwcfd7w9fpqwkc7jjj6kq1g")))

