(define-module (crates-io lo am loam) #:use-module (crates-io))

(define-public crate-loam-0.1.0 (c (n "loam") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06kjm58npzb5kfss85w5k9vvv03ib61i9ra0d5drnljfi6kxz2mf") (f (quote (("default") ("crc" "crc32fast"))))))

(define-public crate-loam-0.2.0 (c (n "loam") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gdkfpmair4vdxv151zb5c2nfpc7787gi066qxidlx5yl2zaa6af") (f (quote (("default") ("crc" "crc32fast"))))))

(define-public crate-loam-0.3.0 (c (n "loam") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k6wpwmy4kwrvak1h0fwb455v899710lrd5rhjpcrwhr5wf2dmdr") (f (quote (("default") ("crc" "crc32fast"))))))

