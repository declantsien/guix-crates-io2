(define-module (crates-io lo am loam-sdk-ft) #:use-module (crates-io))

(define-public crate-loam-sdk-ft-0.6.2 (c (n "loam-sdk-ft") (v "0.6.2") (d (list (d (n "loam-sdk") (r "^0.6.2") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "1vy5r6yh373ddky3plkmcrlk5smq2d9xfi8786hmaqkpfbggb7mz")))

(define-public crate-loam-sdk-ft-0.6.3 (c (n "loam-sdk-ft") (v "0.6.3") (d (list (d (n "loam-sdk") (r "^0.6.2") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "09k4hxkpv1s7a2a80mb70kr2znglhk3bmli3mfw3h9p1jvqzvm83")))

(define-public crate-loam-sdk-ft-0.6.4 (c (n "loam-sdk-ft") (v "0.6.4") (d (list (d (n "loam-sdk") (r "^0.6.3") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "0rg048507kqvk6wakljbi94bmddl43l1gagkkidj948zykpl22lp")))

(define-public crate-loam-sdk-ft-0.6.5 (c (n "loam-sdk-ft") (v "0.6.5") (d (list (d (n "loam-sdk") (r "^0.6.5") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "0zxj8c4rrr8wz48wmp91z4cx96r05cx1sfx8anh8k7grb4by4j9j")))

(define-public crate-loam-sdk-ft-0.6.6 (c (n "loam-sdk-ft") (v "0.6.6") (d (list (d (n "loam-sdk") (r "^0.6.7") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "0d4f7pap3ahlm5gs9xfwh3a0synvk5bf074kny9mgfchzf9c3n8p")))

