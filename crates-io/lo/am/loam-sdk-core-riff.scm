(define-module (crates-io lo am loam-sdk-core-riff) #:use-module (crates-io))

(define-public crate-loam-sdk-core-riff-0.6.2 (c (n "loam-sdk-core-riff") (v "0.6.2") (d (list (d (n "loam-sdk") (r "^0.6.2") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "009wariw0clzcp88137qrd93p7m6i7fd789rv2k7zmjxx7s323ka")))

(define-public crate-loam-sdk-core-riff-0.6.3 (c (n "loam-sdk-core-riff") (v "0.6.3") (d (list (d (n "loam-sdk") (r "^0.6.2") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "1gp191848cik6rxj3k8qq302xsmnpzn2d1bb1r9dh6hxaxfyrdnn")))

(define-public crate-loam-sdk-core-riff-0.6.4 (c (n "loam-sdk-core-riff") (v "0.6.4") (d (list (d (n "loam-sdk") (r "^0.6.3") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "10kc2cl18fd4989z6c68h8f8l0gdf0l0riaxdjcrwf7xjm69zykh")))

(define-public crate-loam-sdk-core-riff-0.6.5 (c (n "loam-sdk-core-riff") (v "0.6.5") (d (list (d (n "loam-sdk") (r "^0.6.5") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "18m20m3ll1a6vsz1bdpqi1phmz4blm41q1w746fkgawhs4lzicrx")))

(define-public crate-loam-sdk-core-riff-0.7.0 (c (n "loam-sdk-core-riff") (v "0.7.0") (d (list (d (n "loam-sdk") (r "^0.6.7") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)))) (h "0i7c824mvyf17jq07yrqyybrc722rm5ysc282dyrbc4i03dsb1hk")))

