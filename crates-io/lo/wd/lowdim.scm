(define-module (crates-io lo wd lowdim) #:use-module (crates-io))

(define-public crate-lowdim-0.2.0 (c (n "lowdim") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1adi7bbaxn1hapfj193shfsa274zzm616p0k4gm39wl8lgj1ad60") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.2.1 (c (n "lowdim") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "17vgdqdyim1awn9vsf5mil8d62m2wry1rgwpcqgl91ywq28vjlgx") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.2.2 (c (n "lowdim") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0lwjqp3bmgq3y64yv5ckyb9wj6li42v7v2gqm03r5fyq9rrxkj9s") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.2.3 (c (n "lowdim") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1yfpl5j962lvb7jqkdpnd29iq0nvszv9s9si2qjdb8fl7nn7dhzf") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.2.4 (c (n "lowdim") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1c470fkd39ypz90cmnq9885n9rsgrdy07bkark3hxgyndvbjz9yr") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.3.0 (c (n "lowdim") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0ndfxs4psx03j6pfilw8yf6wbjr5nw5356lwf3q5a7s0pklvj2ax") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.3.1 (c (n "lowdim") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "10r65901y1xamnjf5yz9vyab5d7wnvs3shrdapsy97q9qfdclz4a") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.3.2 (c (n "lowdim") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0rkay27cxw74yaklpqx0lclvx7vhwac1h3g4s401d58ps71b09bh") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.3.3 (c (n "lowdim") (v "0.3.3") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "10n677xnqjapn3svk3ywl4mcf52vm5l0pcnicyknfkm6ryq2d5nh") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.4.0 (c (n "lowdim") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1rrzwc3awxaramczj5n4sqmyk8lj3yd1qyz4dmi2gsnq0ip1g9ld") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.4.1 (c (n "lowdim") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1csji1yvwb76993afqkz1baaqy33pzqqfj9dkgdw75dzxzvwxqhp") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.4.2 (c (n "lowdim") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1s9fzmmigadbqzifgxi74g2axcsyin76ig1v9nirp9lixc6jl6v0") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.5.0 (c (n "lowdim") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0w14qpckcrjpvlpqii7lc8zbaahmq9q4qyv9bkax57n6c8njlxm1") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.5.1 (c (n "lowdim") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0yba0yf9bk8v271zw7zzhc676dcs2njdsrb89pph4sdnddnxiibi") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.5.2 (c (n "lowdim") (v "0.5.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1snp8h8wvlhyr6vgvzwlbqw77kzrg1z9mm5aaikbrdckfarbalpz") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.5.3 (c (n "lowdim") (v "0.5.3") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0did4n6rh11picihaqkb6zwbxxa2ndhk3q82ipw8gi58jyh5jr0q") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.5.4 (c (n "lowdim") (v "0.5.4") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0pgcs169agm817bzpxnwhy0zqjcf43knj0kjzr687zs8k82r179g") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.6.0 (c (n "lowdim") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1nlmzzdkpkh69cb1li22ckcz1rq4r3h9iix81fjrz6fbg0dxrfjk") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.6.1 (c (n "lowdim") (v "0.6.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1783a5k2s0v1ph6yvkn8im29j0s7vkvmd7pcmigqzzzzgzc6jjh1") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.7.0 (c (n "lowdim") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0aqnd0kr6fgm59vyrxh6ni01b22acr8pgv2psz3cfpmfgrdxq3a0") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.7.1 (c (n "lowdim") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0sccyq5zjqxslq151bbsg9shq0rwayfirypi4zgp4d4iinw5ij6v") (f (quote (("random" "rand") ("default"))))))

(define-public crate-lowdim-0.7.2 (c (n "lowdim") (v "0.7.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "15xggcijp0gcnnn3l357fxpzjginvsya52vjzmdjlnliq2y139pd") (f (quote (("random" "rand") ("default"))))))

