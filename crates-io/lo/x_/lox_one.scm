(define-module (crates-io lo x_ lox_one) #:use-module (crates-io))

(define-public crate-lox_one-0.1.0 (c (n "lox_one") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "parameterized") (r "^1") (d #t) (k 2)) (d (n "parse-display") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0afgljhwh17cihdfhwbvv0vf0rz30l0vjv02m58b6c32fwz20c9c")))

