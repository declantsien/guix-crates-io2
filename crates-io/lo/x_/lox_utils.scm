(define-module (crates-io lo x_ lox_utils) #:use-module (crates-io))

(define-public crate-lox_utils-0.1.0 (c (n "lox_utils") (v "0.1.0") (d (list (d (n "lox-library") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "1lzaab4zvybsz70wgp5zbvkrq6sndd6dx3mgklnh5wp8f1vry6bk") (f (quote (("full")))) (r "1.65")))

