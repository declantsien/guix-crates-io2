(define-module (crates-io lo pd lopdf_bugfix_19072017) #:use-module (crates-io))

(define-public crate-lopdf_bugfix_19072017-0.9.0 (c (n "lopdf_bugfix_19072017") (v "0.9.0") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "05pgnnd497aqsrairlbb3pfzmlvx85c4kd6iykclma5jxcs47rw4")))

