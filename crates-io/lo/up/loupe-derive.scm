(define-module (crates-io lo up loupe-derive) #:use-module (crates-io))

(define-public crate-loupe-derive-0.1.0 (c (n "loupe-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yvcqklrca886zf9p4k6pfisraqv64mmqgqyz803cx9979sjz6ns")))

(define-public crate-loupe-derive-0.1.1 (c (n "loupe-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d28684f5drl9r94n607i2ch1xld7i4iy9n0yjq2c7v8873q2274")))

(define-public crate-loupe-derive-0.1.2 (c (n "loupe-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6wzdvxnaksj9pbg59bsrmr85sc73ynl8263klnvhzfrqi5nm0a")))

(define-public crate-loupe-derive-0.1.3 (c (n "loupe-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ljrdhp4yk87xnbdq53f679yzm7yghanxq4s5sgjfs3i6f4gryy0")))

