(define-module (crates-io lo up loupe) #:use-module (crates-io))

(define-public crate-loupe-0.1.0 (c (n "loupe") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "loupe-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0ryadqd9bzvigy575i0igcyj54xcdlf6ydvgm8aki9n2lsz0ww7h") (f (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1.1 (c (n "loupe") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "loupe-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "18y9sniwvmz1qcyhkxd9779ha8hlcgn2fz50csyann7ba5dhdkqs") (f (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1.2 (c (n "loupe") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "loupe-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "1rphd6x1hnc2h856m85b71a7z6s1wyka0hpnfj9aalkmmb1hr6yp") (f (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1.3 (c (n "loupe") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "loupe-derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0kb3bc62xh20i405afr8bf65m6gznbp0fhxrfrg5pqaglkgp4slv") (f (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

