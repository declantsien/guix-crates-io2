(define-module (crates-io lo tr lotr-api) #:use-module (crates-io))

(define-public crate-lotr-api-0.1.0 (c (n "lotr-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1wjqxbzdqs7sy94km4icjwl1qbnrgwxb1pfl1dpv9aicpcchcqwr")))

(define-public crate-lotr-api-0.1.1 (c (n "lotr-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "001w7n9lzsc96mja4z38xgcjqzj4j6hmyxlh8drc0hgqmkik6390")))

(define-public crate-lotr-api-0.1.2 (c (n "lotr-api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "15z2vvfbyjdn09kbfvc8ifa1agwyl98sw5a4kgf6xs9c95p87m7z")))

