(define-module (crates-io lo gs logscreen) #:use-module (crates-io))

(define-public crate-logscreen-0.0.0-development (c (n "logscreen") (v "0.0.0-development") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0b31516w0yp7yp1vnkg7kx7x9nll8xpr4ncpv3qj8w1s0nbn3y08") (f (quote (("coverage")))) (r "1.76.0")))

