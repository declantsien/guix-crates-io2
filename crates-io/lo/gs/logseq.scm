(define-module (crates-io lo gs logseq) #:use-module (crates-io))

(define-public crate-logseq-0.2.1 (c (n "logseq") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1ly1yc701bl74ycxc898s9271icz5vqf1w8q7id8xf0mp2yn2llj")))

(define-public crate-logseq-0.3.0 (c (n "logseq") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0qf4199d2z4dg26xffvyi02f8qfqpn8ih19kn8ma8vnbqrpkkq01")))

