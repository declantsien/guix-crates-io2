(define-module (crates-io lo gs logstr) #:use-module (crates-io))

(define-public crate-logstr-0.1.0 (c (n "logstr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "0px13j22zcwsnhrn6y4n8nks5gfniazc8bj64ikp7l4vzal136cj")))

(define-public crate-logstr-0.1.1 (c (n "logstr") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "1pa74vgh40nxv86bb8q5z0vrnwlnhnk0wyaiaqb738b2dps1b274")))

(define-public crate-logstr-0.1.2 (c (n "logstr") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "08q2qjl3mz9ls3yy5l6r6h3w47acv19vw8xd9vl5dp01lmj146sc")))

(define-public crate-logstr-0.1.3 (c (n "logstr") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)))) (h "1bfgsqxz7xdmwcmzl6c6f7ck7l67xfiqx781g910b637m9828a5w")))

(define-public crate-logstr-0.1.5 (c (n "logstr") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1xi5wivr8pzad9mkw83sh1v5ag5cmgnj47b8zzacb3qyjwixml48")))

(define-public crate-logstr-0.1.6 (c (n "logstr") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0g9hslrw4633sszfs0ln66z2y3vxh250512y6b3h2zy8qsbdslgy")))

