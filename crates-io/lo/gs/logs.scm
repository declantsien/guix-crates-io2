(define-module (crates-io lo gs logs) #:use-module (crates-io))

(define-public crate-logs-0.0.1 (c (n "logs") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "time") (r "^0.1.43") (o #t) (k 0)))) (h "0a0yng34zn32kj2xjcqg2qhjc7in218ryx8klw7lpfa5vqvb3fxk") (f (quote (("warn") ("trace") ("info") ("error") ("debug")))) (y #t)))

(define-public crate-logs-0.1.0 (c (n "logs") (v "0.1.0") (d (list (d (n "cfg-if") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "time") (r ">=0.1.44, <0.2.0") (d #t) (k 0)))) (h "15lmrzl26ahlbk5w2j0kgywhwaw344h28k60sb8ik8qdvqck751n") (f (quote (("warn") ("trace") ("info") ("error") ("debug")))) (y #t)))

(define-public crate-logs-0.2.0 (c (n "logs") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0gql1qs2gl31jixqqzrx881lgsfqxcmfm61jqmglksr4s2j2qgcw") (f (quote (("warn") ("trace") ("info") ("error") ("debug")))) (y #t)))

(define-public crate-logs-0.3.0 (c (n "logs") (v "0.3.0") (d (list (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0az5d5fh3c837wcfncdvj00w8djbdcv53ivqy40v3aykwfqwzbzh") (y #t)))

(define-public crate-logs-0.3.1 (c (n "logs") (v "0.3.1") (d (list (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "1zxzwch68jv1arsiivlk0m7wig853kdl7dclvl92k738gdsnm0gp") (y #t)))

(define-public crate-logs-0.3.2 (c (n "logs") (v "0.3.2") (d (list (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "1iigw5mc49ns2pdw8ygyqj17b6w95wyd711mm3n0vm9sgj9a7s86") (y #t)))

(define-public crate-logs-0.4.0 (c (n "logs") (v "0.4.0") (d (list (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "1sdm6zid9qdszy70hpvr11swv3b6ns6grga5f3gph27a08qm2x88") (y #t)))

(define-public crate-logs-0.5.0 (c (n "logs") (v "0.5.0") (d (list (d (n "time") (r "^0.1.44") (d #t) (k 0)))) (h "0sy4adxllazcryzbhg5hsn613qg6inlj8k4245rwyxrdfr9a6003") (y #t)))

(define-public crate-logs-0.6.0 (c (n "logs") (v "0.6.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "033pd2l97l7p32xknr4sxmkaybw7f47ara6sgils5mr50i5nd8dj") (y #t)))

(define-public crate-logs-0.6.1 (c (n "logs") (v "0.6.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1yds4i8i1d062llnk1fw2yqanvi3ansnr5z2dhclmnganc9qh56c") (y #t)))

(define-public crate-logs-0.6.2 (c (n "logs") (v "0.6.2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1f6n73ywrjgqiiy9vlbqc510narbblqgpkiz3af9z1slk3856p2d") (y #t)))

(define-public crate-logs-0.6.3 (c (n "logs") (v "0.6.3") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "05sbc2a3x991y22pkqxsnf7r6ppr3zyrc5jffvi0j3bbpz1lyzqr") (y #t)))

(define-public crate-logs-0.7.0 (c (n "logs") (v "0.7.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0snv1pq1yx5dd6ln973jqzaxh4nywhikxwkccnlhspz2f3mbxp09") (y #t)))

(define-public crate-logs-0.7.1 (c (n "logs") (v "0.7.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0vb8qgywgfwm39px6sc89mxpksk0z4dvdz6bad9f3mrfdxgfi3i1")))

