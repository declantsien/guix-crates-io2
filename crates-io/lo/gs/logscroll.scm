(define-module (crates-io lo gs logscroll) #:use-module (crates-io))

(define-public crate-logscroll-0.1.0 (c (n "logscroll") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1d3ywmj3xx7a0jid28n052wshw668a42a7vsa3m5qlx3vy21r32d")))

(define-public crate-logscroll-0.1.1 (c (n "logscroll") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "14c92mckdaj1hga29z02kyga1dhacycazvg8da9y119mg9j0bahx")))

(define-public crate-logscroll-0.2.4 (c (n "logscroll") (v "0.2.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0b3yvd506fn77sjj3m2xfwal2islxhh7kgk1pdn6afc4534vqp6y")))

