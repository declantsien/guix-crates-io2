(define-module (crates-io lo gs logs-narrator) #:use-module (crates-io))

(define-public crate-logs-narrator-0.4.0 (c (n "logs-narrator") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1krgqp0qzi4ga5k3k61rwpprj8i5kkixcgwjig6n566bwcf629df")))

