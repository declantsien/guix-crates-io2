(define-module (crates-io lo gs logstash-rs) #:use-module (crates-io))

(define-public crate-logstash-rs-0.1.0 (c (n "logstash-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustls-crate") (r "^0.20.2") (o #t) (d #t) (k 0) (p "rustls")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "0gxvkic7117db8z4g2gr3bbhydknbxg23yvkkzh68bmpp3cbdcb2") (f (quote (("tls" "native-tls") ("rustls" "rustls-crate" "webpki-roots") ("default"))))))

