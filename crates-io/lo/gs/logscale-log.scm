(define-module (crates-io lo gs logscale-log) #:use-module (crates-io))

(define-public crate-logscale-log-0.1.0 (c (n "logscale-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (f (quote ("std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "logscale-client") (r "^0.1.0") (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlpkgaypwsahq0wq9m4aj4kg4bc14z8cp8ydx8zar0klvzgm88w")))

(define-public crate-logscale-log-0.1.1 (c (n "logscale-log") (v "0.1.1") (d (list (d (n "log") (r "^0.4.19") (f (quote ("std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "logscale-client") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ksr6i6akvbkwz213m916p79xhaf2lsrxjb5psz1793bjjz83ljz")))

(define-public crate-logscale-log-0.1.2 (c (n "logscale-log") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("std" "kv_unstable_serde"))) (d #t) (k 0)) (d (n "logscale-client") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "structured-logger") (r "^1.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1swr95wq9n7ni93pnz5js8wzv70v5mdrx8zy6k5wl677rikgh7x6")))

