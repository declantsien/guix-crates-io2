(define-module (crates-io lo gs logscale-client) #:use-module (crates-io))

(define-public crate-logscale-client-0.1.0 (c (n "logscale-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1mzx67n7rb9ydxy82fv8sj21sqpj47v19m3q9snmg0syfndncii8")))

(define-public crate-logscale-client-0.1.1 (c (n "logscale-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1r8inffsn4iz1nl8fddmx8810mhj36mcvm1md7ihk8dgdwsxkf6p")))

(define-public crate-logscale-client-0.1.2 (c (n "logscale-client") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s78n16mgpj3s1fwv73idzpwyn89izk848k3y342qa5ky83drkfr")))

