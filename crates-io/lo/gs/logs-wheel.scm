(define-module (crates-io lo gs logs-wheel) #:use-module (crates-io))

(define-public crate-logs-wheel-0.1.0 (c (n "logs-wheel") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1f136hk18ghj4dq2kvc0shmmkkrqcbdv44mc2np764lvhhcyfy3z")))

(define-public crate-logs-wheel-0.2.0 (c (n "logs-wheel") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1c9aiji9gr4v3cbcxwfn1h4h8pnzxl7n20fxya7q2xh5vzhq3yk3")))

(define-public crate-logs-wheel-0.3.0 (c (n "logs-wheel") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (k 2)))) (h "1dllrscz19q8ry6cxk49wg9hxiy9x1q94akj8hklbj8rnbi5iw4v")))

(define-public crate-logs-wheel-0.3.1 (c (n "logs-wheel") (v "0.3.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (k 2)))) (h "1v3n6dnmcla0pbfzg5y452jgis90knb0ybpfrwdkb6bxbvkk15ib")))

