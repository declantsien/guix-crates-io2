(define-module (crates-io lo se loser_case) #:use-module (crates-io))

(define-public crate-loser_case-0.1.0 (c (n "loser_case") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1z0ckp558q38yvib7mgd2ikmj33xzsymmpx81vvkfkcd4gglnlb5")))

(define-public crate-loser_case-0.2.0 (c (n "loser_case") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1dqzmqad3r4fqyckdna5y2j8v78zabg4z608sw2qi5nd5hwpzqf1")))

(define-public crate-loser_case-0.2.1 (c (n "loser_case") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0alqgxm0xf70gbci0ilkip0vg45jw4nms6n3ddcihkfqv3hb8kjz")))

