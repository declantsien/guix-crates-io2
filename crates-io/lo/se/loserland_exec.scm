(define-module (crates-io lo se loserland_exec) #:use-module (crates-io))

(define-public crate-loserland_exec-0.1.0 (c (n "loserland_exec") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "pelite") (r "^0.7.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "sysinfoapi" "winnt" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "11ilbkdc4rgcbnrkj8jhy88ljj38w66pg1fa0dyi54lgay6x9b74")))

