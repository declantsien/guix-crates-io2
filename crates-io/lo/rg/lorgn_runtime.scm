(define-module (crates-io lo rg lorgn_runtime) #:use-module (crates-io))

(define-public crate-lorgn_runtime-0.1.0 (c (n "lorgn_runtime") (v "0.1.0") (d (list (d (n "gc") (r "^0.4") (d #t) (k 0)) (d (n "gc_derive") (r "^0.4") (d #t) (k 0)) (d (n "lorgn_lang") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)))) (h "03yvzh0g2nx75sk3il5m4baacymhzm08whaisv259wbb0d3hpnhh")))

