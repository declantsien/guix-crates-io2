(define-module (crates-io lo wp lowpass-filter) #:use-module (crates-io))

(define-public crate-lowpass-filter-0.1.0 (c (n "lowpass-filter") (v "0.1.0") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "0z4xqgsm3y3cdy44lynxy9vqim5wiapmkazgk1iyqx5mp3scmfqc")))

(define-public crate-lowpass-filter-0.1.1 (c (n "lowpass-filter") (v "0.1.1") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "11s9ram2236wdqm1c5w90mhz6vj972cgg15mn4n6xav8gnf9a3qs")))

(define-public crate-lowpass-filter-0.1.2 (c (n "lowpass-filter") (v "0.1.2") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "1cxqx7873vkdmkx36qc89yvb17hcn1c2x5i02mc4cmw7ac79a9rl")))

(define-public crate-lowpass-filter-0.2.0 (c (n "lowpass-filter") (v "0.2.0") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "0fjg1xwz55cycjwvrdpy9f2ffp175qcq2njphnk2dabys1gi3vci") (y #t)))

(define-public crate-lowpass-filter-0.2.1 (c (n "lowpass-filter") (v "0.2.1") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "0w99y0mh26dilwkiihnxiw2wdlji2v0v393mv4bnjrn695vn5kwl")))

(define-public crate-lowpass-filter-0.2.2 (c (n "lowpass-filter") (v "0.2.2") (d (list (d (n "audio-visualizer") (r "^0.1.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)))) (h "1f29hpvwjx6azkg7g4mx8c40iaz7hfgy8wf17ag6sz41r35mzmd8")))

(define-public crate-lowpass-filter-0.2.3 (c (n "lowpass-filter") (v "0.2.3") (d (list (d (n "audio-visualizer") (r "^0.2.1") (d #t) (k 0)) (d (n "audio-visualizer") (r "^0.2.1") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^0.2.2") (d #t) (k 2)) (d (n "wav") (r "^0.6.0") (d #t) (k 2)))) (h "0pp2vwf41p6w0wnfdf4y5jl6830k10axwd7abr7vjc1c6gznsay8")))

(define-public crate-lowpass-filter-0.2.4 (c (n "lowpass-filter") (v "0.2.4") (d (list (d (n "audio-visualizer") (r "^0.2.2") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^0.2.2") (d #t) (k 2)) (d (n "wav") (r "^0.6.0") (d #t) (k 2)))) (h "0ypc5cvyihra3qhk283fp7mw6wha9rkdz7gc8ij01npfk9l2y37w")))

(define-public crate-lowpass-filter-0.2.5 (c (n "lowpass-filter") (v "0.2.5") (d (list (d (n "audio-visualizer") (r "^0.2.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5.1") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^1.1.2") (f (quote ("rustfft-complex"))) (k 2)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1y6sm3ckbg14p08z3l8cv690jn4yznrj3da5q9xh3yhg6kw2j3gn")))

(define-public crate-lowpass-filter-0.3.0 (c (n "lowpass-filter") (v "0.3.0") (d (list (d (n "audio-visualizer") (r "^0.3") (d #t) (k 2)) (d (n "biquad") (r "^0.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^1.1") (f (quote ("rustfft-complex"))) (k 2)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0i1x1914i85wf3if1y7rd3pxx5s0ajmnrnm5442yk4i30p4bbm6p")))

(define-public crate-lowpass-filter-0.3.1 (c (n "lowpass-filter") (v "0.3.1") (d (list (d (n "audio-visualizer") (r "^0.3") (d #t) (k 2)) (d (n "biquad") (r "^0.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^1.1") (f (quote ("rustfft-complex"))) (k 2)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "03f5l1wl9rc7imcrd1j0j470cs78zmg3dbywkkp09fjsp7blx19r")))

(define-public crate-lowpass-filter-0.3.2 (c (n "lowpass-filter") (v "0.3.2") (d (list (d (n "audio-visualizer") (r "^0.3") (d #t) (k 2)) (d (n "biquad") (r "^0.4") (d #t) (k 2)) (d (n "minimp3") (r "^0.5") (d #t) (k 2)) (d (n "spectrum-analyzer") (r "^1.1") (f (quote ("rustfft-complex"))) (k 2)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0vf03flqbrqbr3h7md78bawl20vk275ap8795cpmn509xqghdins")))

