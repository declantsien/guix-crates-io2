(define-module (crates-io lo uv louvre) #:use-module (crates-io))

(define-public crate-louvre-0.1.0 (c (n "louvre") (v "0.1.0") (h "1zyya1gkm00m2fc9lx96jd4421gr9bcpn52xky6bnws516q1c9n4") (y #t)))

(define-public crate-louvre-0.1.1 (c (n "louvre") (v "0.1.1") (h "010jl3gsnzwfr0x1jr1r98h37v4mx5lc6mqy7zr6073ivn8344rm") (y #t)))

(define-public crate-louvre-0.1.2 (c (n "louvre") (v "0.1.2") (h "1jvz361b60mfahqm6fdnl4brx01l40mkmgc608xzkiqaa1i66783")))

(define-public crate-louvre-0.2.0 (c (n "louvre") (v "0.2.0") (d (list (d (n "gloo-utils") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("HtmlCanvasElement" "CanvasRenderingContext2d" "DomRect" "Element" "Window" "HtmlInputElement" "Event" "EventTarget" "MouseEvent" "KeyboardEvent" "InputEvent"))) (o #t) (d #t) (k 0)))) (h "1vcbm1jwjpix0k9nd5yd1zzi6nicsyjfdpfp4l4s7fcwkg1c8plw") (f (quote (("default")))) (s 2) (e (quote (("html" "dep:web-sys" "dep:gloo-utils" "dep:js-sys" "dep:wasm-bindgen"))))))

