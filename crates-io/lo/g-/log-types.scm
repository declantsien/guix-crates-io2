(define-module (crates-io lo g- log-types) #:use-module (crates-io))

(define-public crate-log-types-0.0.0 (c (n "log-types") (v "0.0.0") (h "18xp45isp7cmz39lpixpdn7dp7wqq13z1ljslasixfcg1n9syy0r")))

(define-public crate-log-types-1.0.0 (c (n "log-types") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "19hp4smz8gj4zdvm7wq8305hkadlpab3j862qjqhzjyp956r62lk")))

(define-public crate-log-types-1.0.1 (c (n "log-types") (v "1.0.1") (d (list (d (n "log") (r "^0.4.8") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "1b1pf3lwxyrrwr3vv3xd9gzvp7hrzdpnqkmmp3im4a7y5z62vzv6")))

