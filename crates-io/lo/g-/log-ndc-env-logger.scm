(define-module (crates-io lo g- log-ndc-env-logger) #:use-module (crates-io))

(define-public crate-log-ndc-env-logger-0.2.0 (c (n "log-ndc-env-logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-ndc") (r "= 0.2.0") (d #t) (k 0)))) (h "0k7yp272liy2wzchl37ras7icp7wwf1rndhz78v45cj80wg9fblm")))

(define-public crate-log-ndc-env-logger-0.3.0 (c (n "log-ndc-env-logger") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-ndc") (r "=0.3.0") (d #t) (k 0)))) (h "03cr3px3rka74l2lgz7lc4qlqrc31g5s9bqcg0lbi90lbjrc6vgi")))

