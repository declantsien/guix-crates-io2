(define-module (crates-io lo g- log-reroute) #:use-module (crates-io))

(define-public crate-log-reroute-0.1.0 (c (n "log-reroute") (v "0.1.0") (d (list (d (n "arc-swap") (r "~0.1") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "lazy_static") (r "~1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0nw1k7mk1bg3qp39qz31l459b55az368s3s2q8jbl9iv0bf4lj7c")))

(define-public crate-log-reroute-0.1.1 (c (n "log-reroute") (v "0.1.1") (d (list (d (n "arc-swap") (r "~0.3") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "lazy_static") (r "~1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "0pqpsf0kdml709k99918mh0xylkp5qsad31aynchb4mzp50q411j")))

(define-public crate-log-reroute-0.1.2 (c (n "log-reroute") (v "0.1.2") (d (list (d (n "arc-swap") (r "~0.3") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~0.1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.5") (d #t) (k 2)))) (h "12m28c5yq4q0nmf9mk9phf31ib1m54lr0yanqq96m0a4rz8gd6ms")))

(define-public crate-log-reroute-0.1.3 (c (n "log-reroute") (v "0.1.3") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~0.2") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "106avjbmg3n3j3qjcc2fhlv0k2a14427xb14m0bxr0xac2npv428")))

(define-public crate-log-reroute-0.1.4 (c (n "log-reroute") (v "0.1.4") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1vidd0ra51zpl32ifgjhkfxnqavi0g8pyijymkjxjpwrm4c0cvqs")))

(define-public crate-log-reroute-0.1.5 (c (n "log-reroute") (v "0.1.5") (d (list (d (n "arc-swap") (r "~0.4") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1jnjvxrafznckl7r6m5mllkkcqidwm3ym2p3i2ji43vs8bm745rc")))

(define-public crate-log-reroute-0.1.6 (c (n "log-reroute") (v "0.1.6") (d (list (d (n "arc-swap") (r "~1") (d #t) (k 0)) (d (n "fern") (r "~0.5") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "12whmmp322hdd0yapf76k5zsg8arfxspdjqvqqlk0ggi3xcnvps7")))

(define-public crate-log-reroute-0.1.7 (c (n "log-reroute") (v "0.1.7") (d (list (d (n "arc-swap") (r "~1") (d #t) (k 0)) (d (n "fern") (r "~0.6") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)))) (h "0h22n0sgzy7sgr2igfa18pv315gv5vz8i9dwnm9a61xs83l7s96w")))

(define-public crate-log-reroute-0.1.8 (c (n "log-reroute") (v "0.1.8") (d (list (d (n "arc-swap") (r "~1") (d #t) (k 0)) (d (n "fern") (r "~0.6") (d #t) (k 2)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "once_cell") (r "~1") (d #t) (k 0)) (d (n "tempfile") (r "~3") (d #t) (k 2)))) (h "00mw91qd2ibaawl7x1pxc1kryki0ixyirnlx64qx78d9g6k3n6kl")))

