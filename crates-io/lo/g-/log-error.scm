(define-module (crates-io lo g- log-error) #:use-module (crates-io))

(define-public crate-log-error-0.1.0 (c (n "log-error") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (d #t) (k 2)))) (h "0w0pc1y1r6zwacmy5r2wz419q8k1lcpqfixwq385xy40jfw5g4r6")))

(define-public crate-log-error-0.1.1 (c (n "log-error") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "1bwaj48az63wid0v4lbyh2hjhl60dnv5i1v7w8ajaqyhz93ljbsn")))

