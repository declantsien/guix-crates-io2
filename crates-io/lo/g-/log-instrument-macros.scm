(define-module (crates-io lo g- log-instrument-macros) #:use-module (crates-io))

(define-public crate-log-instrument-macros-0.1.0 (c (n "log-instrument-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fvb3yfgmc5213jzg7wbn03y3rg9vvxmg94x2ri4qg1883b58bi0")))

(define-public crate-log-instrument-macros-0.1.1 (c (n "log-instrument-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wk3i4v07ik4abrjnrg51gii87gfqznfa5gycfyj9fqs3vgjsbg5")))

(define-public crate-log-instrument-macros-0.2.0 (c (n "log-instrument-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g62ins6yzjkslsr5kcgi9z3x7q6r9xb3zji4crqsy2i1gk0m1k4") (f (quote (("on") ("default" "on"))))))

