(define-module (crates-io lo g- log-relativizer) #:use-module (crates-io))

(define-public crate-log-relativizer-0.1.0 (c (n "log-relativizer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "0jb96j1x4v5zjhvhzl42jz4q0plh90aqhds2wm6diqnv87wrhnix")))

