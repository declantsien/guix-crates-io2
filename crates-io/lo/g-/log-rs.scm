(define-module (crates-io lo g- log-rs) #:use-module (crates-io))

(define-public crate-log-rs-0.1.0 (c (n "log-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "140frfjgwk9xf7hqd0f5d5ldvavzv6ch65ak46afnsb4i4fmayqb")))

(define-public crate-log-rs-0.1.1 (c (n "log-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1ddmlb0wabazkn8wb30zw85d9a817lw8ymh209vxli7pslakksqq")))

