(define-module (crates-io lo g- log-to-defmt) #:use-module (crates-io))

(define-public crate-log-to-defmt-0.1.0 (c (n "log-to-defmt") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ikkj4va912pxmg6rbxq67rj7ym27m0szyq2swarl259g26i2m4l")))

