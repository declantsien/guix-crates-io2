(define-module (crates-io lo g- log-update) #:use-module (crates-io))

(define-public crate-log-update-0.1.0 (c (n "log-update") (v "0.1.0") (d (list (d (n "ansi-escapes") (r "~0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)))) (h "1rcksfn6gk603w5kin02zxvazn61jqd7bsd6y1xybvj34h6r813g")))

