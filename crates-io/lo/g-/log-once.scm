(define-module (crates-io lo g- log-once) #:use-module (crates-io))

(define-public crate-log-once-0.1.0 (c (n "log-once") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1mb5vg8qyx6aax7mik4plg2nw2ramni6dskjhmygpgprdjpbsqi2")))

(define-public crate-log-once-0.1.1 (c (n "log-once") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0z2ybkx9pk6pn5ci3wa5msxr2izlhykdd60czkm2hbki3sylk04v")))

(define-public crate-log-once-0.1.2 (c (n "log-once") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "10b5pl4avsh4cxryhl6j91mdglkdzjhnh85l8942nxb9zii9561d")))

(define-public crate-log-once-0.2.0 (c (n "log-once") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ig28q9xz55cfy4qnavlfynbmpx4p01xb1m3h86ghn1n8hz9j84p")))

(define-public crate-log-once-0.3.0 (c (n "log-once") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1395db1wbjk3s2n3qmd0vh85pd95i7a47kdzc7x4vsdl5yy8g516")))

(define-public crate-log-once-0.3.1 (c (n "log-once") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "132a4szdxd2h3h0bx73qn4x597r489hdzqkqjzgkfkr5j04g19mb")))

(define-public crate-log-once-0.4.0 (c (n "log-once") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dcww3g7lay9zx6llsg9dhvjnznv5gnajhmbqp7yj4mccivp7s0s")))

(define-public crate-log-once-0.4.1 (c (n "log-once") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w7bdibhraiyn9xjc5cqqpfbwqkhp9dkwddzdldpnccvhzihb2kd") (r "1.65")))

