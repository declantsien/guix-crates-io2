(define-module (crates-io lo g- log-termination) #:use-module (crates-io))

(define-public crate-log-termination-0.1.0 (c (n "log-termination") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ihyjjv68jg5c6m07cpsycz57bfm6arpah17rqka5c57p01b3175")))

