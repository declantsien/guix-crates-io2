(define-module (crates-io lo g- log-instrument) #:use-module (crates-io))

(define-public crate-log-instrument-0.1.0 (c (n "log-instrument") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wykh3n5prrlqpv4f2w67w41q9sky6jm8m5wfibck99ixrm2cxdl")))

(define-public crate-log-instrument-0.1.1 (c (n "log-instrument") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15y8mkzmfp0y60snnmqnc6fhzmr9bd486x2rcijjqlpqcgl2pmcz")))

(define-public crate-log-instrument-0.1.2 (c (n "log-instrument") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qz09nhnkpnapnlzvgvyfd2mmnyi0nd602548faivxd8cmlmr7gs")))

(define-public crate-log-instrument-0.1.3 (c (n "log-instrument") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log-instrument-macros") (r "^0.1.0") (d #t) (k 0)))) (h "14lydmwm043bznbw4vrph1q4c76b8ypdy0b41aiwx9lycvqya3r0")))

(define-public crate-log-instrument-0.1.4 (c (n "log-instrument") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log-instrument-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1dffyc5wmsfpy1b7479iljh2f7hwrnpf24dn98wh8kghcqr99p5h")))

(define-public crate-log-instrument-0.2.0 (c (n "log-instrument") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log-instrument-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1vnvxrwmn4rwz1r59nvgbk7vl8idcb0i2jf922a2mljvsl1ff6yk") (f (quote (("on" "log-instrument-macros/on") ("default" "on"))))))

(define-public crate-log-instrument-0.3.0 (c (n "log-instrument") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log-instrument-macros") (r "^0.2.0") (d #t) (k 0)))) (h "09sqaq2ajicc55jlhaspg93jlpqd5cvwkcain66m2ib0f9zylfx5") (f (quote (("on" "log-instrument-macros/on") ("default" "on"))))))

