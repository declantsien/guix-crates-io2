(define-module (crates-io lo g- log-print-panics) #:use-module (crates-io))

(define-public crate-log-print-panics-2.1.1 (c (n "log-print-panics") (v "2.1.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g9az8iqkvpx07wsq5w70p3wimz39jad19y5grsrxjssdvicv58g") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-print-panics-2.1.2 (c (n "log-print-panics") (v "2.1.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cpa5p3rjr4ng2wp6mq2sqpb1idai61cjpa1y5a074xv3qpylbrj") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-print-panics-2.1.3 (c (n "log-print-panics") (v "2.1.3") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fhgwjfp72gp36364s0x1xiy4jgpqz8raxakws8q7ykrq8fhrlk5") (f (quote (("with-backtrace" "backtrace"))))))

