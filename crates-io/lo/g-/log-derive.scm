(define-module (crates-io lo g- log-derive) #:use-module (crates-io))

(define-public crate-log-derive-0.1.0 (c (n "log-derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sjcl8dsd33v5a4xa6mycxsagi6mvri1sq84vn32hskzbpmvhl06")))

(define-public crate-log-derive-0.2.0 (c (n "log-derive") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zv9axxqql22qdzsfhjjq50ha07mq9gws2x4hjicbfhhl1vb8f5i")))

(define-public crate-log-derive-0.2.1 (c (n "log-derive") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "155743nqmpqrzpyhkab0h59ya6fgf6nnnak3w89ns866wgzpd6xk")))

(define-public crate-log-derive-0.2.2 (c (n "log-derive") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "07q95di02vcr8b8qqfdsr5p8yk94m59dn3kaj3356phribvsfhxa")))

(define-public crate-log-derive-0.2.3 (c (n "log-derive") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ww74m8zg19c7bcfnw6wivsnd56d48gnc9rrmdc42xlhzmxjxp47")))

(define-public crate-log-derive-0.2.4 (c (n "log-derive") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ca1x5il6pcslmc348zpg6w279s6ci0qam58899w252zzld7abjw")))

(define-public crate-log-derive-0.3.0 (c (n "log-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1z03p2xgd1vndyzpblkilvy40fapjqn0wr4ydba6kgyly99y3dwh")))

(define-public crate-log-derive-0.3.1 (c (n "log-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1irl6j7l895l6v8857gkymz21xvk72zl55sqwqpi5g9048cw8nmg")))

(define-public crate-log-derive-0.3.2 (c (n "log-derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "07hk59bg8ibh4axl4gz9s8vxv20d7agh0xah2ixqalav7dnl6zrc")))

(define-public crate-log-derive-0.4.0 (c (n "log-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0dz7b32g4rxgx3jx9wk757sivgjrmsz2dcvmjmmmfv0zp6l0knh6")))

(define-public crate-log-derive-0.4.1 (c (n "log-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.8") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1ksvsmvhakd588r1m7x215aa5zsfk1iz3mbi6nsd3g1jnimm4hka") (f (quote (("async_test" "futures-executor"))))))

