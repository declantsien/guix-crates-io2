(define-module (crates-io lo g- log-wrap) #:use-module (crates-io))

(define-public crate-log-wrap-0.1.0 (c (n "log-wrap") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h5gk05f9dhwg4209a52zp6wq8z5np107lsq3mnraa1ha44gazn9") (f (quote (("std" "log/std") ("default" "std"))))))

