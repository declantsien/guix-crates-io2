(define-module (crates-io lo g- log-writer) #:use-module (crates-io))

(define-public crate-log-writer-0.1.0 (c (n "log-writer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0frrsyx2v7h0zzisrx2b6g4sd4wx4v1dlaji2hfi7gdpm5vxfjg1")))

(define-public crate-log-writer-0.2.0 (c (n "log-writer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ppbvadanxs7yisaim960va2hza7ajyqvhlm5kw0kic2438r7p01")))

(define-public crate-log-writer-0.1.1 (c (n "log-writer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10a4s776z9gdbsing0ynz5193kmdw41llb101n0dkxnasr32n2ql") (y #t)))

(define-public crate-log-writer-0.2.1 (c (n "log-writer") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w4f58gqgh8150dqlkp88q1yjqavxyk0d5vc5nwsh8sj44y8fk86")))

(define-public crate-log-writer-0.2.2 (c (n "log-writer") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11hd332w92bxbqmk5d853kfwsvm9amyr7yc81lpm91ch9z8nwk8v")))

(define-public crate-log-writer-0.3.0 (c (n "log-writer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p73d91nsappq09b7nr01ay649injhhnmzs52mw7bv8504kjm5f0")))

(define-public crate-log-writer-0.4.0 (c (n "log-writer") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nn32258pjcrsfhqszr7qi59y2myb6wiz6arw34kspsqq6md6cbl")))

(define-public crate-log-writer-0.5.0 (c (n "log-writer") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l45inzdxzdxf2cag5c92q33n4gd9wmc6j8dyjwpaq74rw6rm37m")))

(define-public crate-log-writer-0.6.0 (c (n "log-writer") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j5chkcfmj26mmw8986v8dqyhkl0lkpxhnvbz45igf8g0jf9mcn3")))

(define-public crate-log-writer-0.6.1 (c (n "log-writer") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rff56wzzarrba05rn8ibmm3kvsinb5m76mg8l23dyr1nvnk1dch")))

(define-public crate-log-writer-0.6.2 (c (n "log-writer") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wpn7l0mavh7d2d5r20jxpkprz3pn71ziw5fpajispmbgvjazgxw")))

