(define-module (crates-io lo g- log-panics) #:use-module (crates-io))

(define-public crate-log-panics-1.0.0 (c (n "log-panics") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "045rb7isjq9yagjwfdr2n48vl4hbhkz7x3zbbrdna7hjfjdp7961") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-panics-1.1.0 (c (n "log-panics") (v "1.1.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "19sn02m1d0frzqdf1h1ig53yj61c1z33zfigg4hbjccavmbvdfr6") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-panics-1.2.0 (c (n "log-panics") (v "1.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "08vy9in7i6zwcxf1q8mwpyxync0ipfh8wy8pv35ylivn7nfc5yxm") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-panics-2.0.0 (c (n "log-panics") (v "2.0.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vrb118v123pn3cayyp26bh66z3mjiiw3mhqm8fjc2gjgljkc0df") (f (quote (("with-backtrace" "backtrace"))))))

(define-public crate-log-panics-2.1.0 (c (n "log-panics") (v "2.1.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pxcq9f4jmcs2l9lgs6hj2w3ba80zzsj4zgnxi81h70r8s2xvyb8") (f (quote (("with-backtrace" "backtrace"))))))

