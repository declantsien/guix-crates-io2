(define-module (crates-io lo g- log-attributes) #:use-module (crates-io))

(define-public crate-log-attributes-0.1.0 (c (n "log-attributes") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.52") (d #t) (k 2)))) (h "15y4z4jv118n3qgyr87v9hzr8bqk3ycv7kydr71z8aa1hqqkcnl1")))

