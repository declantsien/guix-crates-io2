(define-module (crates-io lo g- log-analyzer-pro) #:use-module (crates-io))

(define-public crate-log-analyzer-pro-1.0.0 (c (n "log-analyzer-pro") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.53") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "flume") (r "^0.10.12") (d #t) (k 0)) (d (n "log-analyzer") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (d #t) (k 0)) (d (n "tui-input") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0i3nm49ffviqjdq30wkgaxxzdyma0vkjy12wqs61081qv8fjyfrv")))

(define-public crate-log-analyzer-pro-1.0.1 (c (n "log-analyzer-pro") (v "1.0.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "terminal-ui") (r "^0.1.1") (d #t) (k 0)))) (h "0qw58yxlsk35pfijasr28zysf70a4cn7m0prx6ikj7yk6ppynfa3")))

(define-public crate-log-analyzer-pro-1.0.2 (c (n "log-analyzer-pro") (v "1.0.2") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "terminal-ui") (r "^0.1.2") (d #t) (k 0)))) (h "1yxdwd1xwczrgiyyfrvp5cx49fdyrzal7fgab29l9qd1m5559k1g")))

(define-public crate-log-analyzer-pro-1.0.3 (c (n "log-analyzer-pro") (v "1.0.3") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "terminal-ui") (r "^0.1.3") (d #t) (k 0)))) (h "1jncyi7hasiy7frmhx7grrlcidv9jqvgfxnjxdb2xr0kml4znzyz")))

