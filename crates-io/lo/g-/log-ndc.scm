(define-module (crates-io lo g- log-ndc) #:use-module (crates-io))

(define-public crate-log-ndc-0.1.0 (c (n "log-ndc") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nc384hx81zmc3lv6qk5pyskxdffzw5jjm31nnk8vfhshidflc48")))

(define-public crate-log-ndc-0.2.0 (c (n "log-ndc") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1gf81cv50cxwpg53ywsajyr63rm1x6p8wllb94kpv2xmqxbr1c7d")))

(define-public crate-log-ndc-0.3.0 (c (n "log-ndc") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "10cv7rlb3p5wdcap5ijqpbv26364ll6w3i32y56nww2q233kmnkw")))

