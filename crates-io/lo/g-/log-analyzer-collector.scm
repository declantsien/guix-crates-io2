(define-module (crates-io lo g- log-analyzer-collector) #:use-module (crates-io))

(define-public crate-log-analyzer-collector-0.1.0 (c (n "log-analyzer-collector") (v "0.1.0") (d (list (d (n "log-analyzer-transient-types") (r "^0.1") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "09hxj2av9s14iqm00h2cp97x396wqmsm2zvimbsv1n7k54kgjfhd")))

(define-public crate-log-analyzer-collector-0.1.1 (c (n "log-analyzer-collector") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log-analyzer-transient-types") (r "^0.1") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-serde") (r "^0.8") (f (quote ("cbor"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1zdfyfzlb9yw0q54labcq5s55b47z120fimy66rri9xyf6gk3q29")))

