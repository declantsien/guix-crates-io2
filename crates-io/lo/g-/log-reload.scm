(define-module (crates-io lo g- log-reload) #:use-module (crates-io))

(define-public crate-log-reload-0.1.0 (c (n "log-reload") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "16crx10l9dcl03f29wf1sfxb7zxks1m5fbddr4m08ag8pxczg7g9") (r "1.71")))

