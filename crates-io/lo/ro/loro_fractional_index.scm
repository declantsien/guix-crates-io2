(define-module (crates-io lo ro loro_fractional_index) #:use-module (crates-io))

(define-public crate-loro_fractional_index-0.16.2 (c (n "loro_fractional_index") (v "0.16.2") (d (list (d (n "criterion") (r "^0.5.0") (d #t) (k 2)) (d (n "fraction_index") (r "^2.0") (d #t) (k 2) (p "fractional_index")) (d (n "imbl") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0admqsj35ql1p21r6qn789yx6g8pag4d92zl1ws72b3xasndbsjf")))

