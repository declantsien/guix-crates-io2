(define-module (crates-io lo ro loro) #:use-module (crates-io))

(define-public crate-loro-0.1.0 (c (n "loro") (v "0.1.0") (h "183bnz9fw16r89d3vapbnma3qfpnhhg2gs72v38wjrvlrww03bii")))

(define-public crate-loro-0.2.0 (c (n "loro") (v "0.2.0") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "loro-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "06i9d3vqzhc268bssxvhfyb01il54ycx875j3jmqyqh6xfagjq51")))

(define-public crate-loro-0.2.1 (c (n "loro") (v "0.2.1") (d (list (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "loro-internal") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "0v5bg5gpnw2dvdvbi3pvjbg850wyylzggzs2z1r0xjnwhkq8yhlk") (f (quote (("test_utils" "loro-internal/test_utils"))))))

(define-public crate-loro-0.2.2 (c (n "loro") (v "0.2.2") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "loro-internal") (r "^0.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "01wyd6dzhh58vv032vyj33g84vs5cqxaw2mf6awwm937al96g6q1") (f (quote (("test_utils" "loro-internal/test_utils"))))))

(define-public crate-loro-0.3.0 (c (n "loro") (v "0.3.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "loro-internal") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "147j37piljb9hjzndq0jbis9d8k361qld9bcn3c66dyd5s3xakmv") (f (quote (("test_utils" "loro-internal/test_utils"))))))

(define-public crate-loro-0.4.0 (c (n "loro") (v "0.4.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "loro-internal") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "0gns1khqy17cb1xn2sbjp3mz09qxn4bl8m50dsjf8w9llqplwpgr") (f (quote (("test_utils" "loro-internal/test_utils"))))))

(define-public crate-loro-0.5.0 (c (n "loro") (v "0.5.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "delta") (r "^0.5.0") (d #t) (k 0) (p "loro-delta")) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-btree") (r "^0.10.4") (d #t) (k 0)) (d (n "loro-internal") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11199bx50zrwbkvp1nvjxkh1vw98cyi234mzcfjm3j97xkydqqx8")))

(define-public crate-loro-0.5.1 (c (n "loro") (v "0.5.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "delta") (r "^0.5.1") (d #t) (k 0) (p "loro-delta")) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-btree") (r "^0.10.5") (d #t) (k 0)) (d (n "loro-internal") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0capzb2zh6gcg0f55mgcsv286anwi2cbcf54lzsd02sn5yfdc4lm")))

(define-public crate-loro-0.15.4 (c (n "loro") (v "0.15.4") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "delta") (r "^0.15.4") (d #t) (k 0) (p "loro-delta")) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-btree") (r "^0.10.5") (d #t) (k 0)) (d (n "loro-internal") (r "^0.15.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11hxiwm1hr84fy04p62ms278ikisqf4byy8vfjxm79y8agd3kdk6")))

(define-public crate-loro-0.16.2 (c (n "loro") (v "0.16.2") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "delta") (r "^0.16.2") (d #t) (k 0) (p "loro-delta")) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-btree") (r "^0.10.5") (d #t) (k 0)) (d (n "loro-internal") (r "^0.16.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1d1a46c0gbyswbmql8sy24ppdfggvi9n1nyamwq49pqpdasrzj47") (f (quote (("counter" "loro-internal/counter"))))))

