(define-module (crates-io lo ro loro-preload) #:use-module (crates-io))

(define-public crate-loro-preload-0.1.0 (c (n "loro-preload") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "loro-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_columnar") (r "^0.3.2") (d #t) (k 0)))) (h "0aa271xyrl6ld7kxgq4bwz2w9axc18gyk64a4wk1z9i39i9ljlls")))

(define-public crate-loro-preload-0.2.0 (c (n "loro-preload") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "loro-common") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_columnar") (r "^0.3.2") (d #t) (k 0)))) (h "1xmd35wm9d4p9gwvwqj7nnmws9cb7fxi0dhiasg56b6ai2p476k0")))

(define-public crate-loro-preload-0.4.0 (c (n "loro-preload") (v "0.4.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "loro-common") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_columnar") (r "^0.3.4") (d #t) (k 0)))) (h "0xnxvk4l4a59894f5ldmw1krz4jnry95y482fhmqaaplccwwlypk")))

