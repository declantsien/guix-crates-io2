(define-module (crates-io lo dg lodge) #:use-module (crates-io))

(define-public crate-lodge-0.1.0 (c (n "lodge") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)))) (h "1z56679c26jipc012c991hvxgkdzzqxm9ab6xl33hbg4ahbc8iv0")))

(define-public crate-lodge-0.2.0 (c (n "lodge") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)))) (h "0lsxv7463j2lg1kj63c3nd7n3jj5mkjq0rbdd9w6nc0nbwymv8wv")))

