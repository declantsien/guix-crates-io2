(define-module (crates-io lo gr logrs) #:use-module (crates-io))

(define-public crate-logrs-0.1.0 (c (n "logrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "0yhldw1d9n646hyby9r5wvgsrwbv8hv9dsndmhp56j37ziv7b0ya")))

