(define-module (crates-io lo gr logru) #:use-module (crates-io))

(define-public crate-logru-0.2.0 (c (n "logru") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 2)) (d (n "dirs") (r "^4.0") (d #t) (k 2)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.29") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2.25") (d #t) (k 2)))) (h "0dk17d6b8zbbsvnkhs06bpq7rmkpn37jvp0l9ddjldrql5718ya9")))

