(define-module (crates-io lo gr logreduce-tokenizer) #:use-module (crates-io))

(define-public crate-logreduce-tokenizer-0.1.0 (c (n "logreduce-tokenizer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10c57dmmpr13ab0v1c2l5b7gwr7a6bik5dg12ff5w2ygx33ynh51")))

