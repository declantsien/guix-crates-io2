(define-module (crates-io lo gr logr) #:use-module (crates-io))

(define-public crate-logr-0.1.0 (c (n "logr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "1a0zk79phplis7w2wl9fsygp0f4fmllbzb73h079i01a6lr9wazy")))

(define-public crate-logr-0.1.1 (c (n "logr") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "1vywadzdq5m1py98phm6n5d68qcgwnps65bw1hkkijg3kb1wcv4x")))

(define-public crate-logr-0.1.2 (c (n "logr") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "15ydx1s42g0ssydpk714valvw3jg5kxjcjdbq0zm86mkad0vgz9h")))

(define-public crate-logr-0.1.3 (c (n "logr") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "12c7y2zmvxjdf1681cngwh3bqrhzxiavsvyxhhyzzx9q86lshwpp")))

(define-public crate-logr-0.1.4 (c (n "logr") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "1fyya38w30nrqxn7naqbm67i22y92858m6gpf79nfs8hya8vmabx")))

(define-public crate-logr-0.1.5 (c (n "logr") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "1m420bzdgqvx9z8dga3qrl5brcc4jiili8q1ij2cy3a0zplm6acp")))

(define-public crate-logr-2.0.0 (c (n "logr") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "1bjrpp4mlklxy351gw26w407blyl11r5w8skvdn5f1zs6bnhb1gy")))

(define-public crate-logr-1.0.0 (c (n "logr") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "008j230gczzskc2xccvfsgl1d13ab8d1x4bhwq6g03nfbbmzx0yf")))

(define-public crate-logr-1.0.1 (c (n "logr") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)))) (h "03010s07fdbiih812p0rhjd2z4q3r94j6gl4y0db9y8jjbl7s3ny")))

