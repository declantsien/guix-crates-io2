(define-module (crates-io lo mb lombok) #:use-module (crates-io))

(define-public crate-lombok-0.1.0 (c (n "lombok") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a6zyg8nl73fr17yzqqcijni3znff0xyazsjzj3k1a4bclnwaybm")))

(define-public crate-lombok-0.1.1 (c (n "lombok") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pjh9hk3l4dk51kknimhfjck4dxymg22m7zx64w0sj38d14n5zlk")))

(define-public crate-lombok-0.2.0 (c (n "lombok") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "161igps4s94xmmg3dzvsisagqh26129l7jxbsvz2cha7w2pfnzq2")))

(define-public crate-lombok-0.3.0 (c (n "lombok") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pn75nrhpygbjayxx8chf1lj1brp6al5bfrp4akivaw422dkr7fv")))

(define-public crate-lombok-0.3.1 (c (n "lombok") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q4bv0qxsd4jwcpjlswcj6nzf4x9bn2a37nd746p1wf8dafln62x")))

(define-public crate-lombok-0.3.2 (c (n "lombok") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bl983dhfigq07p8akgbrcpbg6idyjr9dys5wvixvvjwbc0bv5ml")))

(define-public crate-lombok-0.3.3 (c (n "lombok") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sldg5dn2pqz9qvp749vs0acw7cn187zgiyyxkzjnql728nn91si")))

(define-public crate-lombok-0.4.0 (c (n "lombok") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xxxbba65v56wiwdw0bk5jbgyg0lgr2kgp1xwqh79pb9gk4yrlqg")))

