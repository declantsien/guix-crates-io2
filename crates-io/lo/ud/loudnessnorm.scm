(define-module (crates-io lo ud loudnessnorm) #:use-module (crates-io))

(define-public crate-loudnessnorm-1.2.0 (c (n "loudnessnorm") (v "1.2.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zk254rxj412fhxsjvygbbkyyrsdphr9alm04zrslsl1ckh56fj7")))

(define-public crate-loudnessnorm-1.1.2 (c (n "loudnessnorm") (v "1.1.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0mwzh1jj1fpgayiyscqz8lvz78c9fvihnmsn8ajclhx0v2aqpkln")))

(define-public crate-loudnessnorm-1.1.3 (c (n "loudnessnorm") (v "1.1.3") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dlpz2q2q2j7naw9dc15gpgqb1piyprbsh69aqmgjpfq1h55xn14")))

(define-public crate-loudnessnorm-1.3.0 (c (n "loudnessnorm") (v "1.3.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02z6pbglwddslmv8dyfh0v1hvfxanddhjk3skj0h6adlssh9f94z")))

(define-public crate-loudnessnorm-1.3.1 (c (n "loudnessnorm") (v "1.3.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0idjw1fqwlykjfq8fhr90qxqm2cs508lzpgjfm90yzy3bi1h6p2h")))

(define-public crate-loudnessnorm-1.3.2 (c (n "loudnessnorm") (v "1.3.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xg60askdfyh45nb3798qgakq93wsxkd0hm194w28niad42zx84f")))

(define-public crate-loudnessnorm-1.4.0 (c (n "loudnessnorm") (v "1.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "12lcbqh6w5qs2ab5w32jq4vanhid5d4xvi7215prk3dc8ars95x8")))

