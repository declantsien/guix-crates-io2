(define-module (crates-io lo ud louds) #:use-module (crates-io))

(define-public crate-louds-0.1.0 (c (n "louds") (v "0.1.0") (d (list (d (n "fid") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "128xwb2nk8alz84vxdb5s2cggl8vm7489k6hyr1aapp2p3k5mf1c")))

(define-public crate-louds-0.1.1 (c (n "louds") (v "0.1.1") (d (list (d (n "fid") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0x9ksbn3c19jx42b6iclxymjnznn4xq9kgcyx44bf9zvgv16b9yw")))

