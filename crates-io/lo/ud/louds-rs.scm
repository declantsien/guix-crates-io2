(define-module (crates-io lo ud louds-rs) #:use-module (crates-io))

(define-public crate-louds-rs-0.1.0 (c (n "louds-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1axllwjzcr613nw85yhfyb84ysny5b74b1vccgx41jycyyij6da1")))

(define-public crate-louds-rs-0.1.1 (c (n "louds-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0zbzpxgp1qha38hicn8ikbbm4p90d52jm1d4maqg1k9plk2vnydx")))

(define-public crate-louds-rs-0.2.0 (c (n "louds-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1k5a80v5m1rh37kg2rwrmmfilhmcrdpxi6mv18wh06589wja265d")))

(define-public crate-louds-rs-0.3.0 (c (n "louds-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0lhncdgq6nh4inkf7k38qfmrab0889s6v7jkyr8cb841w7a9cn0s")))

(define-public crate-louds-rs-0.4.0 (c (n "louds-rs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1sal98m4iidkyhczxyrlzai2bxaai2i060cafnd6sjzp43xr2sp1")))

(define-public crate-louds-rs-0.5.0 (c (n "louds-rs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fid-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0pbhnzmmjpyw54bz32v8dzx52l7ffmsi9hlfpl53sf76zj7w6ajh")))

(define-public crate-louds-rs-0.6.0 (c (n "louds-rs") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fid-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "mem_dbg") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13yniawr8jpaqx7xsys53n747z440l2n2x8pam5x1kj55705g6xq") (f (quote (("rayon" "fid-rs/rayon")))) (s 2) (e (quote (("serde" "fid-rs/serde" "dep:serde") ("mem_dbg" "dep:mem_dbg" "fid-rs/mem_dbg"))))))

(define-public crate-louds-rs-0.7.0 (c (n "louds-rs") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fid-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "mem_dbg") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zz0v0fryjv33xcvahjg1j48h3fdga8dm2hzj9d17rq85z1fcvck") (f (quote (("rayon" "fid-rs/rayon")))) (s 2) (e (quote (("serde" "fid-rs/serde" "dep:serde") ("mem_dbg" "dep:mem_dbg" "fid-rs/mem_dbg"))))))

