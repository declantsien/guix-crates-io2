(define-module (crates-io lo ui louis-sys) #:use-module (crates-io))

(define-public crate-louis-sys-0.1.0 (c (n "louis-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "0wrf1i1z57ncz079v436ba30801ybhs37cqyszipwzbdncbd9sfb")))

(define-public crate-louis-sys-0.1.1 (c (n "louis-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "02p93f1bldixj9imglkc89m33mnji30wggk6i4xfy2iw4kz6cb0n") (l "louis")))

(define-public crate-louis-sys-0.2.0 (c (n "louis-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "1c461rbcmc998qchkasikh5fgyz2yvf6ghf1dci340kc660gvcyg") (l "louis")))

(define-public crate-louis-sys-0.3.0 (c (n "louis-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "1w6whfvzmna38agbl275ilc8b018ma53hys1jxhvfalbazp4cb10") (l "louis")))

(define-public crate-louis-sys-0.3.1 (c (n "louis-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "07cpw8n8b1irk5y4wpg16azdlnp6vggy70psd4qwpbs86mlapc3g") (l "louis")))

(define-public crate-louis-sys-0.3.2 (c (n "louis-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "05k8xqjdkgk14ag2kvvypjm83b9bihr422g32967cf08fvqv1mxf") (l "louis")))

(define-public crate-louis-sys-0.4.0 (c (n "louis-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "13mmbavmpdi0f0gygi7dfgq11ihjik474sqmmp30rkjnznjsn2ls") (l "louis")))

(define-public crate-louis-sys-0.5.0 (c (n "louis-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "0afa94px8n06r1bsk2kgqhpcnvhhqz7mf5lyr0aaza4mwrvh4hhz") (l "louis")))

(define-public crate-louis-sys-0.6.0 (c (n "louis-sys") (v "0.6.0") (d (list (d (n "autotools") (r "^0.1.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "1ih3f3k5hi4xwwaj79ry6v01y4k8p70anv3knvqkgbsil3zvk4zf") (l "louis")))

(define-public crate-louis-sys-0.6.1 (c (n "louis-sys") (v "0.6.1") (d (list (d (n "autotools") (r "^0.1.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "semver-parser") (r "^0.9.0") (d #t) (k 2)))) (h "1srm3a9ri29gaim33c2jfisx9g4jsx0p66f22cfrxw5z08adky7s") (l "louis")))

