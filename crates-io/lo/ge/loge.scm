(define-module (crates-io lo ge loge) #:use-module (crates-io))

(define-public crate-loge-0.1.0 (c (n "loge") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0n34a16r6p5s4avf2l66i8rr1186gzb1czlpkvdzs7lwrxs2nb2v")))

(define-public crate-loge-0.1.1 (c (n "loge") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1dkvvm8wm2iy96fbzj0phq7bb8pmbdxga8is5a549aljv4g4lj4q")))

(define-public crate-loge-0.1.2 (c (n "loge") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0r8rvmppy3mm4w8257ymxrq38dx1ciq6ldv12mjgd8b1hkp14hal")))

(define-public crate-loge-0.2.0 (c (n "loge") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1aa33adjj1wskq6816aa9s1j1ygpac3q89hfwa79lny2g4r13pmf")))

(define-public crate-loge-0.2.1 (c (n "loge") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "16r1h3q77nng0ivwa1lh529c8wznv9d5sxmmdwk3jnb2qp1lqrnp")))

(define-public crate-loge-0.2.2 (c (n "loge") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0phsr7pizilb6r6lli0fbbdzkylfcy4rh21cw4vds1kjkyh2iw1r")))

(define-public crate-loge-0.2.3 (c (n "loge") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1936m40czhpibamw1r575iv5z59ny0dlb8swmd302013mzxqlzlc")))

(define-public crate-loge-0.3.0 (c (n "loge") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fnwiisvdnsii3v7xv1l7l2ghnrww0kdzvplqb3nf4z0lzkd390f")))

(define-public crate-loge-0.3.1 (c (n "loge") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xdi4a1cb0s1g2fn92sfx6azc26nvs430x21zj8x9k219czh7xk8")))

(define-public crate-loge-0.4.0 (c (n "loge") (v "0.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "json-color") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "051lfjhrf49ymfsi06wcpq3546y1kz2n003h9pwvc9nvpsla4av0") (f (quote (("json" "serde_json" "json-color") ("default" "chrono" "colored" "json"))))))

(define-public crate-loge-0.4.1 (c (n "loge") (v "0.4.1") (d (list (d (n "atty") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "json-color") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rg4m8s99rhf5p670aq7l57cz1xbcwpjghdgzq43fvcsw2mrahxy") (f (quote (("json" "serde_json" "json-color") ("default" "chrono" "colored" "json"))))))

(define-public crate-loge-0.4.2 (c (n "loge") (v "0.4.2") (d (list (d (n "atty") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "json-color") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jschpzgivkx14qa21rwnbsn7i6fqrbcp47hjg8dnq0dj5iyji6w") (f (quote (("json" "serde_json" "json-color") ("file") ("default" "chrono" "colored" "json"))))))

