(define-module (crates-io lo ge logerr) #:use-module (crates-io))

(define-public crate-logerr-0.1.0 (c (n "logerr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1dsw173a2irinykmqpqwqdvmd2cg9nvl4sji4m6j5rkanb61di5q") (f (quote (("std_errors") ("generic") ("default" "log" "generic"))))))

(define-public crate-logerr-0.2.0 (c (n "logerr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1qinq4fxm2plba6y5l9g4znhz14qna3pxfr211gx94vys8a68ir3") (f (quote (("generic") ("default" "log" "generic")))) (s 2) (e (quote (("log" "dep:log") ("anyhow" "dep:anyhow"))))))

