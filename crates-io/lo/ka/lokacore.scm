(define-module (crates-io lo ka lokacore) #:use-module (crates-io))

(define-public crate-lokacore-0.0.1 (c (n "lokacore") (v "0.0.1") (h "07qsx15ipabvzmyjbkipj7mnrp4sdjjxg6bk9hrdckb5raa3j3xl")))

(define-public crate-lokacore-0.0.2 (c (n "lokacore") (v "0.0.2") (h "1cn698swwjxfffshyg8vr420dgd0f2c0gc5k5i4lzq4x8i5ip1j2")))

(define-public crate-lokacore-0.1.0 (c (n "lokacore") (v "0.1.0") (h "0bvmrmm0y4vksf2qnfvn3hvn66sahdijrpx7qbc6l9x4alxp34zd")))

(define-public crate-lokacore-0.1.1 (c (n "lokacore") (v "0.1.1") (h "1b762909az875832kkrc9ynia9gjzyhc33s8ii2wzr8xpqh4h6vp")))

(define-public crate-lokacore-0.1.2 (c (n "lokacore") (v "0.1.2") (h "12sm7ig577wri73qddwgsn6m885qfncs8kb8vzbl2zmb33cf03np")))

(define-public crate-lokacore-0.1.3 (c (n "lokacore") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.4") (d #t) (t "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1q4bxkpw9ln721k2wvf8qsqzya6qjpjpj4rpvcqmck6nz2mwgmph") (f (quote (("default" "serde"))))))

(define-public crate-lokacore-0.1.4 (c (n "lokacore") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.4") (d #t) (t "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "11bxrwjkb1k4jfw1x3cl637rn7ysr6kwvjvrf11miajpz9i6hkbn") (f (quote (("default" "serde"))))))

(define-public crate-lokacore-0.1.5 (c (n "lokacore") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.4") (d #t) (t "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "064jmbgassm1ss7vb2k221m1kvgp6ddy9z743ibgw4qwapn5svmv") (f (quote (("default"))))))

(define-public crate-lokacore-0.2.0 (c (n "lokacore") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "libm") (r "^0.1.4") (d #t) (t "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0r4wklgw30nnmm0qcrxdinnijwqpsm1yyzgvx4wzr022sg5kl41a") (f (quote (("default"))))))

(define-public crate-lokacore-0.3.0 (c (n "lokacore") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "libm") (r "^0.1.4") (d #t) (t "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0rslkh4i9bih35qa3k67j3l51mjiv8pqfm9lfswhj6xnba2ricgf") (f (quote (("extern_crate_alloc" "bytemuck/extern_crate_alloc") ("default"))))))

