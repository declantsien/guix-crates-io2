(define-module (crates-io lo re lore) #:use-module (crates-io))

(define-public crate-lore-1.0.0 (c (n "lore") (v "1.0.0") (h "0m3dmfcy7a6l33lknmpns6j47saf11wz75sx78hz0acg2b59dm7a") (y #t)))

(define-public crate-lore-1.0.1 (c (n "lore") (v "1.0.1") (h "1lzal167rj002kb5mqf3h6h04hsc7rgqlnmnbwx8w5c4prxaf3m7")))

(define-public crate-lore-2.0.0 (c (n "lore") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "11x3zrfn32ygayvy048yl0r81xklvda71zh5ya8bfharvb90garn")))

(define-public crate-lore-2.0.1 (c (n "lore") (v "2.0.1") (h "0lpzr2v5rrj6b429h972rj6ywkxj0vqzczxfhvqbrhpkz80jzf3l")))

(define-public crate-lore-2.1.0 (c (n "lore") (v "2.1.0") (d (list (d (n "serde") (r ">=1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vgb8wihmszj186wx8k7gp5pw0f3046zppyqk30n7dnqjvkyhfnz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

