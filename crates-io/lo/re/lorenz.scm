(define-module (crates-io lo re lorenz) #:use-module (crates-io))

(define-public crate-lorenz-0.1.0 (c (n "lorenz") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "x25519-dalek") (r "^0.5.2") (d #t) (k 0)) (d (n "zeroize") (r "^0.9.1") (d #t) (k 0)))) (h "1qw3lwbhc5g071cz8pwml0adl66kw5r5hylkprgvy9f2lvbvnmy7")))

(define-public crate-lorenz-0.1.1 (c (n "lorenz") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "x25519-dalek") (r "^0.5.2") (d #t) (k 0)) (d (n "zeroize") (r "^0.9.1") (d #t) (k 0)))) (h "0ljmxj2dwz8igk0g95a20csqjf434ivzvifajpvbks86yv4p0qvd")))

