(define-module (crates-io lo re lorem-ipsum) #:use-module (crates-io))

(define-public crate-lorem-ipsum-0.1.0 (c (n "lorem-ipsum") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0nzrsvwnsc1xn77p34fbk1nz90zxrxjyr6ny8qf2c0wafhk8nv2w")))

(define-public crate-lorem-ipsum-0.1.1 (c (n "lorem-ipsum") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "09k6v8mfiiqhwigivb59i9vb557i502h219kisap160a9va4q0c0")))

(define-public crate-lorem-ipsum-0.1.2 (c (n "lorem-ipsum") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1ca85dmsl78frwgs4p2yli9g40qg89y0z1zp74x6r4f0v8n02bim")))

