(define-module (crates-io lo re lorem) #:use-module (crates-io))

(define-public crate-lorem-1.0.0 (c (n "lorem") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "18fqsg4ac4gmih1ibd192ccqxpn9r79irp0q7yfzv2fzxzr0czli")))

(define-public crate-lorem-1.1.0 (c (n "lorem") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.0") (d #t) (k 0)))) (h "12rbyglf8s0izja91mnvdqrbq1fvprhqq0zq3chayykljfrbv4wd")))

