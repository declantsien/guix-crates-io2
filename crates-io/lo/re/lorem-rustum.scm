(define-module (crates-io lo re lorem-rustum) #:use-module (crates-io))

(define-public crate-lorem-rustum-0.0.1 (c (n "lorem-rustum") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f8xjqnfxh3264p86wf4xa5gr7c4kvpavdwn7b5d5n4nhspfmhag")))

(define-public crate-lorem-rustum-0.0.2 (c (n "lorem-rustum") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14l647irz557nd2h4jnrsijlqqzsvhhj6ds7h4wbc9xlka7jp8q5")))

(define-public crate-lorem-rustum-0.0.3 (c (n "lorem-rustum") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vncr5bpg25a0xl1mcqafcy3bivdsrwfi7wlabyzsc6bz1njp74m")))

(define-public crate-lorem-rustum-0.0.4 (c (n "lorem-rustum") (v "0.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jrmf8qavrw8g5ij9i3y545qs9gjygsjv0wpah5ah07njgkza0z0")))

(define-public crate-lorem-rustum-0.0.5 (c (n "lorem-rustum") (v "0.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11zdzaki3xi3sl78lwj9l3bvadvh0skn58rx34sxyxpyw1g36mlx")))

