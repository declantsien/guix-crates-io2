(define-module (crates-io lo lg lolgrep) #:use-module (crates-io))

(define-public crate-lolgrep-1.0.0 (c (n "lolgrep") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08r5ayrlxzhlghybz7v3gcgpsbfkdr6j19cwhrl0spbqljd4hv7p")))

(define-public crate-lolgrep-1.5.0 (c (n "lolgrep") (v "1.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0zczv5zxa8vz9hy1jjs99736qadqgali4y55lpag2b223kkjphal")))

