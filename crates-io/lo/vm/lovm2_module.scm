(define-module (crates-io lo vm lovm2_module) #:use-module (crates-io))

(define-public crate-lovm2_module-0.1.0 (c (n "lovm2_module") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "18325xwapslwapvsk9nix576gx91183xc20x6ys85ngi4k01isc7")))

