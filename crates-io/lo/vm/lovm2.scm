(define-module (crates-io lo vm lovm2) #:use-module (crates-io))

(define-public crate-lovm2-0.2.4 (c (n "lovm2") (v "0.2.4") (d (list (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)))) (h "1vnwin7hsg2bcwy8bdcw1rqazhnxs7648j3iyskvy5ryxlbxh08c")))

(define-public crate-lovm2-0.2.7 (c (n "lovm2") (v "0.2.7") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "0fa6cix7v184mhmxrijrajx3l7bvpih8rdmr4sapcqgfd9glxy7s")))

(define-public crate-lovm2-0.2.8 (c (n "lovm2") (v "0.2.8") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yby94gmxc6s8brl4x45s1k6cjcx1pja56p44p2f1ylc6krhsydz")))

(define-public crate-lovm2-0.2.9 (c (n "lovm2") (v "0.2.9") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0clj9c13sxlafzvf56ai27nr0vwg91qxa3z28finb4m7p2nc1psz")))

(define-public crate-lovm2-0.2.10 (c (n "lovm2") (v "0.2.10") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ir997xryhf8wznmi041fmfajhdh4yw3l5xcs6xi0yayyr9sryyk")))

(define-public crate-lovm2-0.2.11 (c (n "lovm2") (v "0.2.11") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jxfr5alvnm21p4qyrxpmkc37r65x43wmlyfn7afd6svsvq4p2b5")))

(define-public crate-lovm2-0.2.2 (c (n "lovm2") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ac2sanhzd2fb1sk4q55hqa74jj50h7l7g31ad2gjh1fc1mz2mn9")))

(define-public crate-lovm2-0.3.7 (c (n "lovm2") (v "0.3.7") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_error") (r "^0.1.0") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k9v2mcyb3r2gi6hx4v1n399h8jvhmgbkll8hr8lzl3jmf9s26mq")))

(define-public crate-lovm2-0.3.8 (c (n "lovm2") (v "0.3.8") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_error") (r "^0.1.0") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "119f8bbjjwkqwmnz98kigf74c4l0j59fddbapnvygmam1r840sa4")))

(define-public crate-lovm2-0.4.0 (c (n "lovm2") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "lovm2_error") (r "^0.1.0") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m7sxhaqvg57s6207bfq838kqi02xgrna3bqhmswxkpr9cm98ryv")))

(define-public crate-lovm2-0.4.5 (c (n "lovm2") (v "0.4.5") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.5") (d #t) (k 0)) (d (n "lovm2_error") (r "^0.1.1") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "070d48rrckcsnm4i6w3blry5vpivwpr0v6hzxb7ixk9c8whibldl")))

(define-public crate-lovm2-0.4.7 (c (n "lovm2") (v "0.4.7") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)) (d (n "lovm2_error") (r "^0.1.1") (d #t) (k 0)) (d (n "lovm2_internals") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ps69sqyai9c2sz33xy5nihyxn17abqyj1hr41hrjrrd0xis9sqb")))

(define-public crate-lovm2-0.4.8 (c (n "lovm2") (v "0.4.8") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)) (d (n "lovm2_core") (r "^0.4.8") (d #t) (k 0)) (d (n "lovm2_std") (r "^0.4.8") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cd0qwh87q2jmizwyqal6zibqmigwm7ch58x3z7xjbqhsl6x9jr2") (f (quote (("stdlib-ssl" "lovm2_std" "lovm2_std/net" "lovm2_std/ssl") ("stdlib-net" "lovm2_std" "lovm2_std/net") ("stdlib" "lovm2_std") ("default" "stdlib" "stdlib-net" "stdlib-ssl"))))))

