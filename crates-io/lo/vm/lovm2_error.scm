(define-module (crates-io lo vm lovm2_error) #:use-module (crates-io))

(define-public crate-lovm2_error-0.1.0 (c (n "lovm2_error") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.53") (o #t) (d #t) (k 0)))) (h "1s8msxalcibj1cz6l4aq8cyi1c6mp2gmd6m15z3cysgvg7anh84w") (f (quote (("default" "backtracing") ("backtracing" "backtrace"))))))

(define-public crate-lovm2_error-0.1.1 (c (n "lovm2_error") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.53") (d #t) (k 0)))) (h "0cp8kzcd3kypla3k77h3pqfpdi6hd09z46zw1i9ng99y2f2x2x37")))

