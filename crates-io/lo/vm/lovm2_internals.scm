(define-module (crates-io lo vm lovm2_internals) #:use-module (crates-io))

(define-public crate-lovm2_internals-0.2.0 (c (n "lovm2_internals") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1gagkmd67hyfazl0y8ks0f33rn0hx5hb6vr0iv8h7izvm3nl11qv")))

(define-public crate-lovm2_internals-0.2.1 (c (n "lovm2_internals") (v "0.2.1") (d (list (d (n "lovm2_error") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "189jsbhvw73dci5wm8wrlrvaqb0bn3ypisf2s0sx3k45mp5yjvya")))

(define-public crate-lovm2_internals-0.2.2 (c (n "lovm2_internals") (v "0.2.2") (d (list (d (n "lovm2_error") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1mrqkgwyan1vf4ya8cdncf3zhpdiy59md5b5m5n1d07kk1vqcyvk")))

