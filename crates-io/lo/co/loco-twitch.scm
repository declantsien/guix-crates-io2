(define-module (crates-io lo co loco-twitch) #:use-module (crates-io))

(define-public crate-loco-twitch-0.1.0 (c (n "loco-twitch") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "00pzkyil89d775nav9h7lxysyay4ipd3dpsfiz5hzfgqs1swhq21") (y #t)))

(define-public crate-loco-twitch-0.1.1 (c (n "loco-twitch") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)))) (h "0wabrpb94q1ljrnlcixxw0fghj4nyxal8j9xs290gsqimp8kiyc0")))

