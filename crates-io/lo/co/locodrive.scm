(define-module (crates-io lo co locodrive) #:use-module (crates-io))

(define-public crate-locodrive-0.1.0 (c (n "locodrive") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "io-util" "macros" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1xgsxjw0fwvmk3zag5awm34ziqmkma796zqh2zy4rgslzbgvzkxd") (f (quote (("control") ("all" "control"))))))

(define-public crate-locodrive-0.1.1 (c (n "locodrive") (v "0.1.1") (d (list (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("rt" "rt-multi-thread" "io-util" "macros" "sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.4") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "199sh63vkw5wcmrfi2il149v60lkkrzh93w0y167fkk55bkf4dfd") (f (quote (("control" "tokio" "tokio-serial" "tokio-util" "bytes") ("all" "control"))))))

(define-public crate-locodrive-0.1.2 (c (n "locodrive") (v "0.1.2") (d (list (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "rt-multi-thread" "io-util" "macros" "sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.4") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0frqhzc4igcpgpyxrq41g8r0dsqqbzdrmsmiphb9ik2z2y0c455d") (f (quote (("control" "tokio" "tokio-serial" "tokio-util" "bytes") ("all" "control"))))))

