(define-module (crates-io lo co locot) #:use-module (crates-io))

(define-public crate-locot-0.1.3 (c (n "locot") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0cqq3bdls810n7j3rzmxwrslh367nx8hnk7sqkps8h048knzr51m")))

