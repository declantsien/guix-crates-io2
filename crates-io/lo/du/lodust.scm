(define-module (crates-io lo du lodust) #:use-module (crates-io))

(define-public crate-lodust-0.1.0 (c (n "lodust") (v "0.1.0") (h "1dsvwj2svfrixqmqhv1zi6r8hmn6140a75jflwp6klf0vgxzhggd")))

(define-public crate-lodust-0.1.1 (c (n "lodust") (v "0.1.1") (h "1k5ijp35wvc0326hik1rzxgpmfag4x7smh8jm2n95xb9q916w1w0") (f (quote (("string") ("default" "string"))))))

(define-public crate-lodust-0.1.2 (c (n "lodust") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "08bz73lyi2gbzry6zwzn3d0vcd2q9bgqxfl5rm63bklyfydy6li8") (f (quote (("string") ("default" "string"))))))

