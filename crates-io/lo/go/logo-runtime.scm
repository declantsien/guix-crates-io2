(define-module (crates-io lo go logo-runtime) #:use-module (crates-io))

(define-public crate-logo-runtime-0.1.0 (c (n "logo-runtime") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "logo-interp") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "16cml30rh3y1wb21lh78bfj82vb8154binx8mb3q2wfla9r0fk1l")))

