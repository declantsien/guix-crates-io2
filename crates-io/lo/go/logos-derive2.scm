(define-module (crates-io lo go logos-derive2) #:use-module (crates-io))

(define-public crate-logos-derive2-0.13.0 (c (n "logos-derive2") (v "0.13.0") (d (list (d (n "logos-codegen2") (r "^0.13.0") (d #t) (k 0)))) (h "1qspiknkbsyfiq2vzrk900hswb34qfjk7q2qc09x4a8zlpy4xnx9") (y #t) (r "1.65.0")))

(define-public crate-logos-derive2-0.13.1 (c (n "logos-derive2") (v "0.13.1") (d (list (d (n "logos-codegen2") (r "^0.13.1") (d #t) (k 0)))) (h "10w6kc4z7rq8j2mhvyzksljfn87yvispqgzgjzap0cz74dl30rcw") (y #t) (r "1.65.0")))

(define-public crate-logos-derive2-0.13.2 (c (n "logos-derive2") (v "0.13.2") (d (list (d (n "logos-codegen2") (r "^0.13.2") (d #t) (k 0)))) (h "1frqh1j454959g0ymcq1mkxy6v0yxx2d6wknzq9nfpbvrhf4s6wa") (y #t) (r "1.65.0")))

(define-public crate-logos-derive2-0.13.3 (c (n "logos-derive2") (v "0.13.3") (d (list (d (n "logos-codegen2") (r "^0.13.3") (d #t) (k 0)))) (h "0iqzmxzv05ga0zmfkacb1a8sjj7h1mjr0ixqwrz9xb4zsxql34my") (y #t) (r "1.65.0")))

(define-public crate-logos-derive2-0.13.4 (c (n "logos-derive2") (v "0.13.4") (d (list (d (n "logos-codegen2") (r "^0.13.4") (d #t) (k 0)))) (h "1k3snsnvspb8fyb88dhpm91is2xmbh24j46vyxgjfxfm257zjfsn") (y #t) (r "1.65.0")))

(define-public crate-logos-derive2-0.14.0 (c (n "logos-derive2") (v "0.14.0") (d (list (d (n "logos-codegen2") (r "^0.14.0") (d #t) (k 0)))) (h "10j9v62xvm4jpq673hw9wmrzjvpidg1zdz2s3dwab2wgz6iynax1") (y #t) (r "1.65.0")))

