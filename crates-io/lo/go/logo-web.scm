(define-module (crates-io lo go logo-web) #:use-module (crates-io))

(define-public crate-logo-web-0.1.0 (c (n "logo-web") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "logo-renderer") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "06alzn0r47xnnl0blb2sypm12mvkr9h47qllbrwbf9alnfpjfcbf")))

