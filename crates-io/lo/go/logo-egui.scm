(define-module (crates-io lo go logo-egui) #:use-module (crates-io))

(define-public crate-logo-egui-0.1.0 (c (n "logo-egui") (v "0.1.0") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.22.0") (d #t) (k 0)) (d (n "logo-renderer") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0047a1vhwwz5ll3hv62dssw59dil6h79sszw3f0rlm9yp81damjk")))

