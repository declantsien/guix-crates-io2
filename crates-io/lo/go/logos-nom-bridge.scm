(define-module (crates-io lo go logos-nom-bridge) #:use-module (crates-io))

(define-public crate-logos-nom-bridge-0.1.0 (c (n "logos-nom-bridge") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0cp0fam96szx0iy9jyp0cybjlfskvxr73yh3dxshm3szbzn9npw9")))

