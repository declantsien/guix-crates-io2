(define-module (crates-io lo go logos) #:use-module (crates-io))

(define-public crate-logos-0.1.0 (c (n "logos") (v "0.1.0") (h "1a7hd4y96i0c9ypax797c21sa19drihkwnd7y34iklqkb04dxmfl")))

(define-public crate-logos-0.2.0 (c (n "logos") (v "0.2.0") (d (list (d (n "logos-derive") (r "^0.2") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1pc8x91cmvyq5lahz9y2vjrnnh5f197f8d0qqz47kqqwwfwch0f7") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.2.1 (c (n "logos") (v "0.2.1") (d (list (d (n "logos-derive") (r "^0.2") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "07440gbgwfjax1q3zj5f2nli246f1m5lxx8bya068r2kghsnfpgv") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.2.2 (c (n "logos") (v "0.2.2") (d (list (d (n "logos-derive") (r "^0.2.2") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0ac618hwqi4lswcb336y24xbrhw6kjarz64bkils8pzs5dfnj2a0") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.2.3 (c (n "logos") (v "0.2.3") (d (list (d (n "logos-derive") (r "^0.2.3") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "08lls3jhz58cal0qf1ybin2yb52a1bfdjgvrgfvyhihgzabp7r8s") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.3.0 (c (n "logos") (v "0.3.0") (d (list (d (n "logos-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1vm7mjiax6gg9djxajisl7yym4r34hq9jankrd2801zlb66vzcyj") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.3.1 (c (n "logos") (v "0.3.1") (d (list (d (n "logos-derive") (r "^0.3") (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1xk3l6lm47zxpmc10smsh7vj9f8rqqbyj3sr3qcr3yz3yss76wzh") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.4.0 (c (n "logos") (v "0.4.0") (d (list (d (n "logos-derive") (r "^0.4") (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1h26qnb925dc7x55sa4233vjb49w3fhvaym6bif6bzq0j1sahm7j") (f (quote (("nul_term_source" "toolshed") ("default"))))))

(define-public crate-logos-0.5.0 (c (n "logos") (v "0.5.0") (d (list (d (n "logos-derive") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1mcr1ri52ncn4xnyr6ah4dphff4rkhwgmybxm58p2x7z3pxk3vk5") (f (quote (("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive"))))))

(define-public crate-logos-0.6.0 (c (n "logos") (v "0.6.0") (d (list (d (n "logos-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "08s56b7fzinzifk8ramld0a0wsjq91j16z148h6hf85cqfx2maij") (f (quote (("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive"))))))

(define-public crate-logos-0.6.1 (c (n "logos") (v "0.6.1") (d (list (d (n "logos-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0l7qix8krggkkvzfnw6r3rs97wbg3sz5mjrh4s9mj6d6jkfvczgh") (f (quote (("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive"))))))

(define-public crate-logos-0.6.2 (c (n "logos") (v "0.6.2") (d (list (d (n "logos-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1za0ln8g260ip91cbivvsm7d2f74xvj23vv8khqpibfrgaz6vidj") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.0 (c (n "logos") (v "0.7.0") (d (list (d (n "logos-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0qvy925lsnh2rg9pa1ra876hrznd7ncwx27i8d59wac6ib6kx86r") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.1 (c (n "logos") (v "0.7.1") (d (list (d (n "logos-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1kyq3ild663bacjydddcqia98lzjyb8qgply9yw31ci4cg1q8s7d") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.2 (c (n "logos") (v "0.7.2") (d (list (d (n "logos-derive") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1hy7f0rphyqnkzvgfxlf5lbi96czqbf7hvk46f4xh98bisgi8mg5") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.3 (c (n "logos") (v "0.7.3") (d (list (d (n "logos-derive") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0r17havnmybb8qd31jsp6ip13imx5g7zfpkl0143gszrmks2hfqs") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.4 (c (n "logos") (v "0.7.4") (d (list (d (n "logos-derive") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0sqdcwvcgx398yw4db04k4563jszfjy58ll73qp767jb7yy8dvzp") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.5 (c (n "logos") (v "0.7.5") (d (list (d (n "logos-derive") (r "^0.7.5") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0vy1z40w64s1sgh7ix1634zpi4bnrgagwq0sxj71gnzhf6i1v6v8") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.6 (c (n "logos") (v "0.7.6") (d (list (d (n "logos-derive") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0f2parlg434njgykp3m37pi8yg8gn4fb4p4xw8n9r9blmiw5w68g") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.7.7 (c (n "logos") (v "0.7.7") (d (list (d (n "logos-derive") (r "^0.7.7") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1l0jgay9aiqssfr5bppzx7jdxp0ymvlam2ky5j1k52sjj436kjk0") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.8.0 (c (n "logos") (v "0.8.0") (d (list (d (n "logos-derive") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.6") (o #t) (d #t) (k 0)))) (h "08jzqpc4i58hrpb9n1pvqll1bp5p3aa9pmqjq327dixnk84vjm0h") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.9.0 (c (n "logos") (v "0.9.0") (d (list (d (n "logos-derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "02krc38285laxb35vz0barvgds36k80nl35dhckhs0cf2f9b7kil") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.9.1 (c (n "logos") (v "0.9.1") (d (list (d (n "logos-derive") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1xk82ja66xb8qfddzy4r8j228hnc1rm4ccwp03hlrz4d4ppqkr86") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.9.2 (c (n "logos") (v "0.9.2") (d (list (d (n "logos-derive") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0idayqbcf4a30xzw1mn17zlip20i1giky9j4s7lwysqqb2xw3517") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.9.3 (c (n "logos") (v "0.9.3") (d (list (d (n "logos-derive") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1yvxf93jsv3314a34f0fxsilg25s5lgdaaizcdlxwrzqgxfdyhy1") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.9.4 (c (n "logos") (v "0.9.4") (d (list (d (n "logos-derive") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1dlv5svcv4vmszi39cs0gq09mm9kdnijwbnapjyw2x9zriy2qb6r") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std")))) (y #t)))

(define-public crate-logos-0.9.5 (c (n "logos") (v "0.9.5") (d (list (d (n "logos-derive") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1w5p0svvzi3c9g3nls4xi1c5pyrcd7yw1jlvcyk74wkx8z34j6s1") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std")))) (y #t)))

(define-public crate-logos-0.9.6 (c (n "logos") (v "0.9.6") (d (list (d (n "logos-derive") (r "^0.9.6") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "032ay118gfpdma9iwg5rqvxnnbxnhmpi0wrsgp2mj3k2fcq19p80") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std")))) (y #t)))

(define-public crate-logos-0.9.7 (c (n "logos") (v "0.9.7") (d (list (d (n "logos-derive") (r "^0.9.7") (o #t) (d #t) (k 0)) (d (n "toolshed") (r "^0.8") (o #t) (d #t) (k 0)))) (h "14vbmvxxk68g4mpwwmibcyszw2cl1srmzn9z1fv5v81z54ny557l") (f (quote (("std") ("nul_term_source" "toolshed") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0-rc1 (c (n "logos") (v "0.10.0-rc1") (d (list (d (n "logos-derive") (r "^0.10.0-rc1") (o #t) (d #t) (k 0)))) (h "12ahhl569m1sgcwb6184dqc9l1x3v11c4fvh8d9z1gb464miz8y6") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0-rc2 (c (n "logos") (v "0.10.0-rc2") (d (list (d (n "logos-derive") (r "^0.10.0-rc2") (o #t) (d #t) (k 0)))) (h "07mlcz9m3xccfafrsbzqgdabmzbqq3x0d1ysv38qz90214p9cdp1") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0-rc3 (c (n "logos") (v "0.10.0-rc3") (d (list (d (n "logos-derive") (r "^0.10.0-rc2") (o #t) (d #t) (k 0)))) (h "0b52nykmyavbvl9lxhaqskk4190khdrgbm309zlk60kfjyvspz8f") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std")))) (y #t)))

(define-public crate-logos-0.10.0-rc4 (c (n "logos") (v "0.10.0-rc4") (d (list (d (n "logos-derive") (r "^0.10.0-rc3") (o #t) (d #t) (k 0)))) (h "0pdwy2a2my1fnn21rvjd21mahn1xaqlc7qrrm1inw2qjsp1fdnz3") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0-rc5 (c (n "logos") (v "0.10.0-rc5") (d (list (d (n "logos-derive") (r "^0.10.0-rc5") (o #t) (d #t) (k 0)))) (h "1a4igxlhjr12imgi7za4qxw5idxzmrh271yl070jkrphw5r4w5b7") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0-rc6 (c (n "logos") (v "0.10.0-rc6") (d (list (d (n "logos-derive") (r "^0.10.0-rc6") (o #t) (d #t) (k 0)))) (h "0dwqxp0nz498725p68yilrg355dgy1mjxinm1ja0hmfl6ibi95j0") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.10.0 (c (n "logos") (v "0.10.0") (d (list (d (n "logos-derive") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "02pgf8i73mrc18f4cmxk3g311cz3mxc6i0z8q4m7xgqzms5q63ww") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0-rc1 (c (n "logos") (v "0.11.0-rc1") (d (list (d (n "logos-derive") (r "^0.11.0-rc1") (o #t) (d #t) (k 0)))) (h "1a2vkkj1n8dia79zc1smy5qin7aa612dz1pprzj2gwsi3sc65wb0") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0-rc2 (c (n "logos") (v "0.11.0-rc2") (d (list (d (n "logos-derive") (r "^0.11.0-rc2") (o #t) (d #t) (k 0)))) (h "059w1dvrai3ns32whni1npacdngyy13a098cmbk46a32y8a80xk0") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0-rc3 (c (n "logos") (v "0.11.0-rc3") (d (list (d (n "logos-derive") (r "^0.11.0-rc3") (o #t) (d #t) (k 0)))) (h "172i8ypby4d2pb8a905mf9dz8j7zzxkazg1jnyjvmhy7xg9gbg08") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0-rc4 (c (n "logos") (v "0.11.0-rc4") (d (list (d (n "logos-derive") (r "^0.11.0-rc4") (o #t) (d #t) (k 0)))) (h "0d81cnxaadd8z6xlgblchj9ip3pxjxzvxjwn21wz0djwrp2lj7ba") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0-rc5 (c (n "logos") (v "0.11.0-rc5") (d (list (d (n "logos-derive") (r "^0.11.0-rc5") (o #t) (d #t) (k 0)))) (h "11w9d7z8bfb83kpbbcx4kgd9rffn4qrgqnzai7a76p0fyzj72jk5") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.0 (c (n "logos") (v "0.11.0") (d (list (d (n "logos-derive") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0k3w3dq2apvnkjm096aaxrvmgz3x3x0vvxq5ndssi5wwjag8ywa2") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.1 (c (n "logos") (v "0.11.1") (d (list (d (n "logos-derive") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0f1291kzkkfcirk07yp65grb8m5mr2alvn1aay0s5505rk0x49zl") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.2 (c (n "logos") (v "0.11.2") (d (list (d (n "logos-derive") (r "^0.11.4") (o #t) (d #t) (k 0)))) (h "12y0hjpwk5xdh5xhhk1nxs0d8d6xvqplcxs3zi3ynjavvbz0lnk7") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.3 (c (n "logos") (v "0.11.3") (d (list (d (n "logos-derive") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "12l5syf9pbc6d22jg0bnc2fvv0x09ii47dfmgw55aaq43ygwbh7m") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.11.4 (c (n "logos") (v "0.11.4") (d (list (d (n "logos-derive") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "1r07w56424yk8rjixj38vg01bv8zpcbic0zrjk0dd9cp6mblj75r") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.12.0 (c (n "logos") (v "0.12.0") (d (list (d (n "logos-derive") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0idklzd8afh87c82n3yp3l0djjmkwrsginxgm5ni64xylny2lzj2") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.12.1 (c (n "logos") (v "0.12.1") (d (list (d (n "logos-derive") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "1w82qm3hck5cr6ax3j3yzrpf4zzbffahz126ahyqwyn6h8b072xz") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.13.0 (c (n "logos") (v "0.13.0") (d (list (d (n "logos-derive") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1hfjqmmcq6fbfwpca6874b1k3lsqi75n584kkg4qmwcgj16wl060") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std"))))))

(define-public crate-logos-0.14.0 (c (n "logos") (v "0.14.0") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1s227bgfl528vfw11dxngxz75df5cws0dq9kqgh7mnm0i3mp268n") (f (quote (("std") ("export_derive" "logos-derive") ("default" "export_derive" "std")))) (r "1.65.0")))

