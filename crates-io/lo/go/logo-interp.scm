(define-module (crates-io lo go logo-interp) #:use-module (crates-io))

(define-public crate-logo-interp-0.1.0 (c (n "logo-interp") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h4sah094rir16jslnliyxwaixw44fisz2mhqmw1rfzk98asfqgf")))

