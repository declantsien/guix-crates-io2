(define-module (crates-io lo go logo) #:use-module (crates-io))

(define-public crate-logo-0.1.0 (c (n "logo") (v "0.1.0") (d (list (d (n "ftlog") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1qvsqmw7ss812jidmvi6fffpr63qqdx5xhjjkn263zm6a63ghyaq")))

(define-public crate-logo-0.1.1 (c (n "logo") (v "0.1.1") (d (list (d (n "ftlog") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "08as4jnhnibm9198r14a1a8sqxh5c53jq0v1fh4ljwm6v858grmi")))

(define-public crate-logo-0.2.0 (c (n "logo") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("local-time" "env-filter"))) (d #t) (k 0)))) (h "0gh2wcr84jb852yi5dy2vhpgg7155i1y5svrfijax6z9x3zpfkb0")))

(define-public crate-logo-0.2.1 (c (n "logo") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("local-time" "env-filter"))) (d #t) (k 0)))) (h "1yjpljqx9gdi4d1bnh8zl5n5vgxp6yk9sl128h7pxq9ai204f3a0")))

(define-public crate-logo-1.1.1 (c (n "logo") (v "1.1.1") (d (list (d (n "ftlog") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1vxaw54asv9vcni3vdny5ll1njpa4cyfvinnackw203xm46gaddl")))

