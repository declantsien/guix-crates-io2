(define-module (crates-io lo go logos2) #:use-module (crates-io))

(define-public crate-logos2-0.13.0 (c (n "logos2") (v "0.13.0") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0wk253ia9xmhi2694l4d8ilxm3byf2a3y0x42n722nk434z14bjh") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

(define-public crate-logos2-0.13.1 (c (n "logos2") (v "0.13.1") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.13.1") (o #t) (d #t) (k 0)))) (h "1ixf7jijh70x0xxh5sn521jnymibybqyfnlgf322jlni428naq19") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

(define-public crate-logos2-0.13.2 (c (n "logos2") (v "0.13.2") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.13.2") (o #t) (d #t) (k 0)))) (h "1c9qml03f7hvnskhddr789nkc705vjqvfh96dbyjw5012nh5hc9g") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

(define-public crate-logos2-0.13.3 (c (n "logos2") (v "0.13.3") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.13.3") (o #t) (d #t) (k 0)))) (h "0afgi8sd30g94pda4d79dk8b8x055lp5zfdadppbhflkdq2xrl48") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

(define-public crate-logos2-0.13.4 (c (n "logos2") (v "0.13.4") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.13.4") (o #t) (d #t) (k 0)))) (h "0x5sivdn1cv0cxmnkb3p39d2xh0sj07s79zhvqr0b9m1xqglzgr6") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

(define-public crate-logos2-0.14.0 (c (n "logos2") (v "0.14.0") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("auto-color"))) (d #t) (k 2)) (d (n "logos-derive2") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "146hrj1vzibk44qsa5qhm736mv461niaviv4whdir0ifavacknk6") (f (quote (("std") ("export_derive" "logos-derive2") ("default" "export_derive" "std")))) (y #t) (r "1.65.0")))

