(define-module (crates-io lo go logos-codegen) #:use-module (crates-io))

(define-public crate-logos-codegen-0.13.0 (c (n "logos-codegen") (v "0.13.0") (d (list (d (n "beef") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0s7drl8vfp9viw9mfyz8dll1gfvp1dc6np82abj0402y548p6j6w")))

(define-public crate-logos-codegen-0.14.0 (c (n "logos-codegen") (v "0.14.0") (d (list (d (n "beef") (r "^0.5.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0amixv1zfk8fnp793w5p25dxhgby8lylgxi197giy4z5kpfvlccf") (r "1.65.0")))

