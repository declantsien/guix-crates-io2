(define-module (crates-io lo go logos-display) #:use-module (crates-io))

(define-public crate-logos-display-0.1.0 (c (n "logos-display") (v "0.1.0") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1xz2a8c14103wlxg7fjg8rza2d7c0d4qnnzj8z7sbyr1gzzdka5d")))

(define-public crate-logos-display-0.1.1 (c (n "logos-display") (v "0.1.1") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0xhcw0ac2s6g3d55c1hk10b4k7izsyjb718g2n7kyyp3csl2bgl0") (y #t)))

(define-public crate-logos-display-0.1.2 (c (n "logos-display") (v "0.1.2") (d (list (d (n "assert-tokenstreams-eq") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1ywn6hm2x0n2hal2n5j06hndjvd5g5b0dz4cq5jqchb3y2y2yb11")))

