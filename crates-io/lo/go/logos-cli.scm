(define-module (crates-io lo go logos-cli) #:use-module (crates-io))

(define-public crate-logos-cli-0.13.0 (c (n "logos-cli") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "logos-codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)))) (h "0cnr2asgknsk2lwsancnvjj5ddbzkbb321mph902cgnzsdsxvgrw")))

(define-public crate-logos-cli-0.14.0 (c (n "logos-cli") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "logos-codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)))) (h "0f27hlbcrz4c7b0fvzmcd1mfh1y25g0dzglx5psnh1igmqb4sa85") (r "1.65.0")))

