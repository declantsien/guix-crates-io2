(define-module (crates-io lo go logos-iter) #:use-module (crates-io))

(define-public crate-logos-iter-0.1.0 (c (n "logos-iter") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "0n06j7zbwx82why7sq98jqzk4ymwl626whsg7y8qlwmhn4hw9vd1") (f (quote (("std" "logos/std") ("default" "std"))))))

(define-public crate-logos-iter-0.1.1 (c (n "logos-iter") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.0") (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1s54glh389bfm4p9r1p4qdkm5fqqpk4j0gmbhqigyg2j3n00524c") (f (quote (("std" "logos/std") ("default" "std"))))))

(define-public crate-logos-iter-0.1.2 (c (n "logos-iter") (v "0.1.2") (d (list (d (n "logos") (r "^0.12.0") (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1y6jw1pjcxcvfyp3bmyvbqk8jxbn0xf4mhygpsq18a3ix9ysn5sn") (f (quote (("std" "logos/std") ("default" "std"))))))

(define-public crate-logos-iter-0.1.3 (c (n "logos-iter") (v "0.1.3") (d (list (d (n "logos") (r "^0.12.0") (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1579i830k2d0g53f59qpr86grvp6yn8z7023z050ppqaa7p99m0i") (f (quote (("std" "logos/std") ("default" "std"))))))

