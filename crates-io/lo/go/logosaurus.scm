(define-module (crates-io lo go logosaurus) #:use-module (crates-io))

(define-public crate-logosaurus-0.1.0 (c (n "logosaurus") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0r24gkpi1x6i3gxwgr8n8xkb37vgz36982flngrsvycw2iwig4w1")))

(define-public crate-logosaurus-0.1.1 (c (n "logosaurus") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "17pvp5mxlfnd9frvycxvc6qr0f4a254zc4173zy3v0zi1s1mcasx")))

(define-public crate-logosaurus-0.2.0 (c (n "logosaurus") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "14bbkgl4bk6g9ldry2zn305g3pmz0dqjyma05m1accscc54jhmz1")))

(define-public crate-logosaurus-0.3.0 (c (n "logosaurus") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "09cirndvw4diia5pxgkp22czrz7v8fwnrppmxar0v1qb3q03b8x1")))

(define-public crate-logosaurus-0.3.1 (c (n "logosaurus") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1sagjy2dh5w8iind47p4d6dbvmcdqj6ykq1g9n7r3b7xr9nskzxl")))

(define-public crate-logosaurus-0.3.3 (c (n "logosaurus") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1rwmvck90rj43cb1irp7jjaialiw0pdqmc6nv3k8j6v1b8kkl7br")))

(define-public crate-logosaurus-0.3.4 (c (n "logosaurus") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1bysa848i3garr02y873ps9wyyr4mn0cipa893wphv556gwz1y14")))

(define-public crate-logosaurus-0.4.0 (c (n "logosaurus") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1yjhdy7pfzwnkjplxnjr5mrvjqwlkf2aq46qh9w0scirm18bm08v")))

(define-public crate-logosaurus-0.4.1 (c (n "logosaurus") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0m7kmdljjiy746kmjmmqcwbdn9jim3mw1xhxa8mcgq5wq8n80b4a")))

(define-public crate-logosaurus-0.5.0 (c (n "logosaurus") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1yq57859vzrwa61xbd99y6dc2j7zm2xkvl6793ca44ayhvh61nsi")))

