(define-module (crates-io lo l_ lol_alloc) #:use-module (crates-io))

(define-public crate-lol_alloc-0.1.0 (c (n "lol_alloc") (v "0.1.0") (d (list (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0yg1kx6vm7pnglj3dlrahi9gl60k8na1flwaf3hg0gmniv4n1f88")))

(define-public crate-lol_alloc-0.1.1 (c (n "lol_alloc") (v "0.1.1") (d (list (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "082kg8dkrsl5rw3k7ys5bhk05rcfzy6qdx0wpbbxhrgnzkg7nzix")))

(define-public crate-lol_alloc-0.1.2 (c (n "lol_alloc") (v "0.1.2") (d (list (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "08v4c97rvm4cm6in7m13fswcd1jwcjrr663npb267fap8gb4jnwb")))

(define-public crate-lol_alloc-0.1.3 (c (n "lol_alloc") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0d3d9cv9k1vl8208yz2n2bbbhk42hxd7nlwkad1fjw09llihnnc8")))

(define-public crate-lol_alloc-0.2.0 (c (n "lol_alloc") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "051mml6xxylyzy4vn0q3fd4vg7ybnmw8iwws1zlqag28kchzgixv")))

(define-public crate-lol_alloc-0.3.0 (c (n "lol_alloc") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1armgjrc19f9f9ivnnrbnp7rf20aldff6nfs1ikvxgi7h1hr6dm7")))

(define-public crate-lol_alloc-0.4.0 (c (n "lol_alloc") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0x2ryh7c4psvskdg5cfp942m10z90gxf145vw03abwcinwrbrain")))

(define-public crate-lol_alloc-0.4.1 (c (n "lol_alloc") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1p65qh6i41myl8nghlk64zrnmbk0fpsm9pya5margg6aaiji1rc3")))

