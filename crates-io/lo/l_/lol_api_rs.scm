(define-module (crates-io lo l_ lol_api_rs) #:use-module (crates-io))

(define-public crate-lol_api_rs-0.2.0 (c (n "lol_api_rs") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.0.6") (d #t) (k 0)) (d (n "slog-term") (r "^2.0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1khj4h70dir90c3c82cfjif9n18xrb9gi6i2iv1yp90rdq84pha5")))

