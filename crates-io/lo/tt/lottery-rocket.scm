(define-module (crates-io lo tt lottery-rocket) #:use-module (crates-io))

(define-public crate-lottery-rocket-1.0.0 (c (n "lottery-rocket") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "rocket") (r "~0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (f (quote ("serve"))) (k 0)))) (h "03wnma70nwbxprqckl5b4x04wlqldkz3aj596h9z97rrdmwrd8k6")))

(define-public crate-lottery-rocket-1.0.1 (c (n "lottery-rocket") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)) (d (n "rocket") (r "~0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (f (quote ("serve"))) (k 0)))) (h "1daraf5yfc4cgavxxlwg2lfqn64nn52y735nxqwqwlbfzw38xcgn")))

