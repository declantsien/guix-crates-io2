(define-module (crates-io lo tt lottie2webp) #:use-module (crates-io))

(define-public crate-lottie2webp-0.1.0 (c (n "lottie2webp") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.3.0") (d #t) (k 0)) (d (n "webp-animation") (r "^0.5.0") (d #t) (k 0)))) (h "0ff4qip57a8267wgnzv4vxbqgc9c6y42s5fib8l2y6dssd6xw8d0") (r "1.56")))

(define-public crate-lottie2webp-0.2.0 (c (n "lottie2webp") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.4") (d #t) (k 0)) (d (n "webp-animation") (r "^0.5") (d #t) (k 0)))) (h "0r0cc21z5r63fbqxv4x273p7dw3i6c5vrh13614apzlrn4cqmily") (r "1.56")))

(define-public crate-lottie2webp-0.2.1 (c (n "lottie2webp") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.4.1") (d #t) (k 0)) (d (n "webp-animation") (r "^0.5") (d #t) (k 0)))) (h "0759jvfsa2cianj1wwjyshlcc33lzvkzsr8i5pgid08kw1hnyw84") (r "1.56")))

