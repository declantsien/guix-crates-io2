(define-module (crates-io lo tt lottie2gif) #:use-module (crates-io))

(define-public crate-lottie2gif-0.1.0 (c (n "lottie2gif") (v "0.1.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rlottie") (r "^0.1") (d #t) (k 0)))) (h "09wmiaqjsg0fvvnbn83n0k6valgb78zpxj6c23i1c4hnz2m5arrw") (r "1.56")))

(define-public crate-lottie2gif-0.1.1 (c (n "lottie2gif") (v "0.1.1") (d (list (d (n "clap") (r "=3.0.0-beta.5") (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rlottie") (r "^0.1") (d #t) (k 0)))) (h "11md3jxzg4qpkc4d8vmwx43dch7y82li53b6dzvnnlgyvl5x2c5r") (r "1.56")))

(define-public crate-lottie2gif-0.2.0 (c (n "lottie2gif") (v "0.2.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rlottie") (r "^0.2") (d #t) (k 0)))) (h "1ibixb1ic7d69i7n6sdi4va63n933g5ajpv3xqyf0d93cix1cn42") (r "1.56")))

(define-public crate-lottie2gif-0.2.1 (c (n "lottie2gif") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rlottie") (r "^0.2.1") (d #t) (k 0)))) (h "177fgz7czic45mrm2mgqpjp0pjln98w5mwj6kkd2rq66smxizipb") (r "1.56")))

(define-public crate-lottie2gif-0.3.0 (c (n "lottie2gif") (v "0.3.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.3.0") (d #t) (k 0)))) (h "0i0j86r0zdlhg3sxp5mmcrrjlaix7wx2hv2an8b061rpa0264n8y") (r "1.56")))

(define-public crate-lottie2gif-0.3.1 (c (n "lottie2gif") (v "0.3.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.3.0") (d #t) (k 0)))) (h "0ssn8sjmyvyjx9q4cxl5czzra53n9idzkr6hyx5wq2gb8q6avhch") (r "1.56")))

(define-public crate-lottie2gif-0.4.0 (c (n "lottie2gif") (v "0.4.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.4") (d #t) (k 0)))) (h "0d5plyvvxq6gp0rmm9n2vz8cd2k40nxsvs5fdnppmal4avy6hzky") (r "1.56")))

(define-public crate-lottie2gif-0.4.1 (c (n "lottie2gif") (v "0.4.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (k 0)) (d (n "rlottie") (r "^0.4.1") (d #t) (k 0)))) (h "1n5581gim2i6709kfw807bsbjhf124jc7zrs71h18c8v2vh33wl3") (r "1.56")))

