(define-module (crates-io lo tt lottie-renderer-service) #:use-module (crates-io))

(define-public crate-lottie-renderer-service-0.1.0 (c (n "lottie-renderer-service") (v "0.1.0") (d (list (d (n "actix-cors") (r "^0.6") (d #t) (k 0)) (d (n "actix-utils") (r "^3") (d #t) (k 0)) (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "gif") (r "^0.11") (d #t) (k 0)) (d (n "lottieconv") (r "^0.2.0") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "webp-animation") (r "^0.7.0") (d #t) (k 0)))) (h "0cxvimmc2zf93nsb93gbnnzvj67rnlv5s1qqgks5d463glmw88lj")))

