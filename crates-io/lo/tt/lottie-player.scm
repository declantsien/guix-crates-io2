(define-module (crates-io lo tt lottie-player) #:use-module (crates-io))

(define-public crate-lottie-player-0.1.0 (c (n "lottie-player") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lottie") (r "^0.1.0") (d #t) (k 0)) (d (n "lottie-renderer-bevy") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 0)) (d (n "webp-animation") (r "^0.9.0") (d #t) (k 0)))) (h "1hfxig7q2fv289xr2aw00lmv8pwfd75rhzwg9pk3bl9q83scsmmq") (f (quote (("default" "debug") ("debug" "lottie-renderer-bevy/egui"))))))

