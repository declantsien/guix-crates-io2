(define-module (crates-io lo de lodepng) #:use-module (crates-io))

(define-public crate-lodepng-0.2.1 (c (n "lodepng") (v "0.2.1") (h "1r20yr11scgzyvf1ihp8vl2d7cm0x7s0zx45cnlwwg052hp9m19c") (y #t)))

(define-public crate-lodepng-0.2.2 (c (n "lodepng") (v "0.2.2") (h "1jpwn9v84ipkinm87nhik9cw76g6lqb4yb6s4vrwrhn3bvk2r2s0") (y #t)))

(define-public crate-lodepng-0.3.0 (c (n "lodepng") (v "0.3.0") (d (list (d (n "c_vec") (r "^1.0.1") (d #t) (k 0)))) (h "0k1yiknfjq21ppyqxgbrzm25wz405n4xa05m72q3m4s4vsq31nc5") (y #t)))

(define-public crate-lodepng-0.3.1 (c (n "lodepng") (v "0.3.1") (d (list (d (n "c_vec") (r "^1.0.1") (d #t) (k 0)))) (h "0wibd2yx0qnbqlwcaynx5y3n889pm66d9gy07wn3nr3lw5sv06ym") (y #t)))

(define-public crate-lodepng-0.4.0 (c (n "lodepng") (v "0.4.0") (d (list (d (n "c_vec") (r "^1.0.11") (d #t) (k 0)))) (h "1dpn592z2kvpgyrxa789qy7r201qk4579nrvy0hby1zqw1dcrpbq") (y #t)))

(define-public crate-lodepng-0.4.1 (c (n "lodepng") (v "0.4.1") (d (list (d (n "c_vec") (r "^1.0.11") (d #t) (k 0)))) (h "1396ijq3zgfnjgk2qlz38ywy2rmqr5d8jk0w2yhzc2pzv2qk0pb3") (y #t)))

(define-public crate-lodepng-0.4.2 (c (n "lodepng") (v "0.4.2") (d (list (d (n "c_vec") (r "^1.0.11") (d #t) (k 0)))) (h "0bsz525m4q9n3g0kmbv42jscjs66jww5nkijdl759y5dajlssvzv") (y #t)))

(define-public crate-lodepng-0.5.0 (c (n "lodepng") (v "0.5.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1v4vi05q8prb1hs7l4qvjvhh620bv0dlskajn4pa9djlbhhw4i2p") (y #t)))

(define-public crate-lodepng-0.5.1 (c (n "lodepng") (v "0.5.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0rn5ybjps62nw93x88nhaf7acxlxhkgp2vpd40gzv4nsr1bg0yvq") (y #t)))

(define-public crate-lodepng-0.5.2 (c (n "lodepng") (v "0.5.2") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1f62rz3pj4zsq3fsp853pwldvrjalrjbmkqmqmm2gbq47s398j39") (y #t)))

(define-public crate-lodepng-0.5.3 (c (n "lodepng") (v "0.5.3") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1pdzxwgdhj999iigzkx1dyb1wyk6aignnjrvx052abq264l9zyi0") (y #t)))

(define-public crate-lodepng-0.6.0 (c (n "lodepng") (v "0.6.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.1.0") (d #t) (k 0)))) (h "0g4ppnbasx40qd5bm3d3xy44sywbafzdz12dnnawqiwacfcac3ba") (y #t)))

(define-public crate-lodepng-0.6.1 (c (n "lodepng") (v "0.6.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.2.0") (d #t) (k 0)))) (h "1kqihicjkbd104zdf87bny4sql34h085zfaqm625aj624m9i7dj5") (y #t)))

(define-public crate-lodepng-0.6.2 (c (n "lodepng") (v "0.6.2") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.2.0") (d #t) (k 0)))) (h "1m8zl9v1w10dw89zlc54q08s4y00s6xhy866svyc6xnvns37nca5") (y #t)))

(define-public crate-lodepng-0.7.0 (c (n "lodepng") (v "0.7.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "01ka03crv9w5z0hq8jc5ddh687ibzrrrn98y7604d9n6p1hn2xah") (y #t)))

(define-public crate-lodepng-0.8.0 (c (n "lodepng") (v "0.8.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "0id80829zs3cs3c3hn9702fx3bgpz3d2inckkzhviv6bnd3dr6zl") (y #t)))

(define-public crate-lodepng-0.8.1 (c (n "lodepng") (v "0.8.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "0yc7gx97hxwi2l9nrmbkk1rk5qrr1safpg58fawarpf60q0k8j99") (y #t)))

(define-public crate-lodepng-0.9.0 (c (n "lodepng") (v "0.9.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "035zw403014m3wnzz5whh5jq7m494l4881h7djqlq4vz5ii16wxy") (y #t)))

(define-public crate-lodepng-0.10.0 (c (n "lodepng") (v "0.10.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "0sgd73gs094q5x3m3dx9bpxjz9h6ml539fxp77j546xkdgl3i8i5") (y #t)))

(define-public crate-lodepng-0.10.1 (c (n "lodepng") (v "0.10.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.2.0") (d #t) (k 0)))) (h "1wixlif78slx3wg7nfq84awh2ywjhfv94918l9aknigar9c6iw72") (y #t)))

(define-public crate-lodepng-0.10.2 (c (n "lodepng") (v "0.10.2") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.4.0") (d #t) (k 0)))) (h "1k08mr895cvzadh05zv47g9lkfhs1qwwhhrar4hga1d36i8afc53") (y #t)))

(define-public crate-lodepng-0.11.0 (c (n "lodepng") (v "0.11.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.4.0") (d #t) (k 0)))) (h "140j7wkw364l2c7f5yg2k9l07fc4pqz22sr3mz1lz1bk0jhjyqyb") (y #t)))

(define-public crate-lodepng-0.12.0 (c (n "lodepng") (v "0.12.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.4.0") (d #t) (k 0)))) (h "0zdrm5wkfxlfch33n1hjhgxqrj9fiq1xa7ny6iib6hzfkcs99r49") (y #t)))

(define-public crate-lodepng-0.12.1 (c (n "lodepng") (v "0.12.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.4.0") (d #t) (k 0)))) (h "05qjx3za2xij5crjw9jpsl7s6j6kk4ky88y8fcm835k7hzy2dx0v") (y #t)))

(define-public crate-lodepng-0.12.2 (c (n "lodepng") (v "0.12.2") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.4.0") (d #t) (k 0)))) (h "0znvy6sg7q1y3pbbdaba15wzaibc7sivcf0sjzxmljbjhl39lwwb") (y #t)))

(define-public crate-lodepng-0.12.3 (c (n "lodepng") (v "0.12.3") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.5.0") (d #t) (k 0)))) (h "1ryqpzkvj5bl9dzyqwjpcfdhyc7vdrf0dlbnb2z0865l4fg5yrk1") (y #t)))

(define-public crate-lodepng-0.12.4 (c (n "lodepng") (v "0.12.4") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.5.1") (d #t) (k 0)))) (h "0b2ixsn6gjynxg8q1wf5zp79gk290nnp1avy27qdzx6f50nay9fx") (y #t)))

(define-public crate-lodepng-0.13.0 (c (n "lodepng") (v "0.13.0") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.5.1") (d #t) (k 0)))) (h "0cjbq5q17si15rdjz9dw1xf7qk9si4szaig54dw85p13fb0yqqn7") (y #t)))

(define-public crate-lodepng-0.13.1 (c (n "lodepng") (v "0.13.1") (d (list (d (n "c_vec") (r "= 1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r ">= 0.5.2") (d #t) (k 0)))) (h "12q70v6x99v5w6dl8nq17kmi464d7cx4yij1fapi943yhz6n1rw2") (y #t)))

(define-public crate-lodepng-0.13.2 (c (n "lodepng") (v "0.13.2") (d (list (d (n "c_vec") (r "^1.0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.4") (d #t) (k 0)))) (h "0dw3m0l6nxrq86q24sxasfy10bi97gkv105l8gdyw145cx6c30ww") (y #t)))

(define-public crate-lodepng-1.0.0 (c (n "lodepng") (v "1.0.0") (d (list (d (n "c_vec") (r "^1.0.12") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.5") (d #t) (k 0)))) (h "1kzj2jjyx548ynvxm00ys1pr199lad59hqsc87lgzwkw3am2vrhz") (y #t)))

(define-public crate-lodepng-1.0.1 (c (n "lodepng") (v "1.0.1") (d (list (d (n "c_vec") (r "^1.0.12") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.5") (d #t) (k 0)))) (h "0j51gwd9w1dlayy51phlnk2yzn9nca0d8c7b4vkkmkga2kirnk7j") (y #t)))

(define-public crate-lodepng-1.0.2 (c (n "lodepng") (v "1.0.2") (d (list (d (n "c_vec") (r "^1.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.5") (d #t) (k 0)))) (h "0rkvqv7ybnrmznn0zczikmfjm1gprscf0d9fb7cqjqn63g2kynx2") (y #t)))

(define-public crate-lodepng-1.1.0 (c (n "lodepng") (v "1.1.0") (d (list (d (n "c_vec") (r "^1.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.5") (d #t) (k 0)))) (h "1sbjzpb4349yqlwciiv0hfilsapd7asphgh64q0nn0825am2hm0g") (y #t)))

(define-public crate-lodepng-1.1.1 (c (n "lodepng") (v "1.1.1") (d (list (d (n "c_vec") (r "^1.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.5.8") (d #t) (k 0)))) (h "19hsz26qv04rxf09jpg0p011mvgknpmcijq4fkx7y3r2pn135b4l") (y #t)))

(define-public crate-lodepng-1.1.2 (c (n "lodepng") (v "1.1.2") (d (list (d (n "c_vec") (r "^1.2.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "1vx4nvy3v6r8yjhfl9krcqprfj80pjprxnxp0l7wljsjacfk6z0d") (y #t)))

(define-public crate-lodepng-1.1.3 (c (n "lodepng") (v "1.1.3") (d (list (d (n "c_vec") (r "^1.3.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.43") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "02sz4ba511nig0dibyxa4j2326ddb5x1wnwhn9hhhqw1zra6n0fw") (y #t)))

(define-public crate-lodepng-0.8.2 (c (n "lodepng") (v "0.8.2") (d (list (d (n "c_vec") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "09kyanhh8v4xj0lmnal42zva1k8gxywiafcwydsjx8l1wmjncyq4") (y #t)))

(define-public crate-lodepng-1.2.0 (c (n "lodepng") (v "1.2.0") (d (list (d (n "c_vec") (r "^1.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "1zx5hadyfcspdgdxqdbjh5j1w2lirv1b79gxnz67rlji8slk9xfx") (y #t)))

(define-public crate-lodepng-1.2.1 (c (n "lodepng") (v "1.2.1") (d (list (d (n "c_vec") (r "^1.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "0vdyk5a95nwd1y8gf9354lqqyp051qc2z7pjkn0gl8yib71nzq1h") (y #t)))

(define-public crate-lodepng-2.0.0 (c (n "lodepng") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "05iagyg8l444056s6w7qxas4ka7sz4z16dhcmk42ipyxd13mnwa2") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.1 (c (n "lodepng") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "1qvn2bjildz8mmxsyh78g5fgyfkvn25dyp57qhgngyw2yfzzwigw") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.2 (c (n "lodepng") (v "2.0.2") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "03dnvdjdscr43skh9y6v5x84za7ak0mia9pa63h7lrgc8dsngkji") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.3 (c (n "lodepng") (v "2.0.3") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.7.2") (d #t) (k 0)))) (h "1anhzsnjavmjkhircsdirdd5c9c9lrzr6qmhxmnn62fwpnblcj0x") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.4 (c (n "lodepng") (v "2.0.4") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1lm6rc8jc61h5n05yyvyz540gn7pmbsl836nb2v79xiq1px45r2g") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.5 (c (n "lodepng") (v "2.0.5") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "14w4xnih2679pcvhqvyp3kc0781nzzjhipvvcx8vlly7n26l8fl8") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.6 (c (n "lodepng") (v "2.0.6") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "14majy895rvxq1wb8f725arcvq6qqmyh19npky98h4h57hmrinv1") (f (quote (("c_statics")))) (y #t)))

(define-public crate-lodepng-2.0.7 (c (n "lodepng") (v "2.0.7") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "10934v3c1ayqix0c6s0f84088rmqz5pi9ccn97d6b9wx8y1gjqrj") (y #t)))

(define-public crate-lodepng-2.1.0 (c (n "lodepng") (v "2.1.0") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "06mljfi2qw0c734axm1z74n8x8qnixgyn47rskhzq89kpg3n0ci9") (y #t)))

(define-public crate-lodepng-1.2.2 (c (n "lodepng") (v "1.2.2") (d (list (d (n "c_vec") (r "^1.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "rgb") (r "^0.8.6") (d #t) (k 0)))) (h "0bzggn2307ikmg0b7836jcc7h857a70143k23akdxmdphpgxzhca")))

(define-public crate-lodepng-2.1.1 (c (n "lodepng") (v "2.1.1") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "163k18k74r10c6vz61vyr8kbkg8p694a7d59bparha3x9nlak7qq") (y #t)))

(define-public crate-lodepng-2.1.2 (c (n "lodepng") (v "2.1.2") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "11dnryw7x927y76wibyxxjl1cjvz6n61mcza0sacn4c604gv4fz2")))

(define-public crate-lodepng-2.1.3 (c (n "lodepng") (v "2.1.3") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1rykfl6wvypnvh2rbbvj12ndgmf6va6s1h1ij31b9szgj6x6g3mw")))

(define-public crate-lodepng-2.1.4 (c (n "lodepng") (v "2.1.4") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0kcd6g6p8cp800b3h2ykzkyr9lxbl56vyz6myr7qszdzbwbsyj3w")))

(define-public crate-lodepng-2.1.5 (c (n "lodepng") (v "2.1.5") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0lhflyri3xfg15b2wg7lin6r9khzdakh6glm854bh1f5dwwzbf90")))

(define-public crate-lodepng-2.2.0 (c (n "lodepng") (v "2.2.0") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "0zl4zw1whb5rj7bj7qp3vx5ikslzg4q75laipwvgf58al2jc60m6")))

(define-public crate-lodepng-2.3.0 (c (n "lodepng") (v "2.3.0") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "1rvcggjd60hwfhr042dslc3aq225m7963318kxnygc7mk7fbkw2g")))

(define-public crate-lodepng-2.4.0 (c (n "lodepng") (v "2.4.0") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "06xgj078g8a0qbr9faq5zasklzghc3nwqzb0bx484m5m9r5mnsfn")))

(define-public crate-lodepng-2.4.1 (c (n "lodepng") (v "2.4.1") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "0v0ky3zap9v6ary3dk2j07gslybh2cp84x7pf4qnsrrb8j3dmpxv")))

(define-public crate-lodepng-2.4.2 (c (n "lodepng") (v "2.4.2") (d (list (d (n "libc") (r "^0.2.33") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "1jvbyclvzb6scawlg66ygx8kmd6r55b4zr2b3sca0mzlwl55r7mw")))

(define-public crate-lodepng-2.5.0 (c (n "lodepng") (v "2.5.0") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "rgb") (r "^0.8.14") (d #t) (k 0)))) (h "0c6lccaibskkg790ag5zz9xgdfn5ak82mwr88m4rn02yn5criva0")))

(define-public crate-lodepng-2.6.0 (c (n "lodepng") (v "2.6.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rgb") (r "^0.8.17") (d #t) (k 0)))) (h "1x9g5rkkwy48pmy370ay9csa2bckzhqzscdahsl6h7zq0g83f4lv")))

(define-public crate-lodepng-2.7.0 (c (n "lodepng") (v "2.7.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rgb") (r "^0.8.17") (d #t) (k 0)))) (h "1a824ywdhmhxyw5db81q9xqzf38ad7brim6vnxvhwjr621l6z6aj")))

(define-public crate-lodepng-2.7.1 (c (n "lodepng") (v "2.7.1") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rgb") (r "^0.8.17") (d #t) (k 0)))) (h "1aj7jxdkjiqv0p3ayaasks3p745kfznww5bxh2a4n8cwwmk4kdna")))

(define-public crate-lodepng-3.0.0-alpha.1 (c (n "lodepng") (v "3.0.0-alpha.1") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "rgb") (r "^0.8.17") (d #t) (k 0)))) (h "0jvzb98xp7x5z3h1xjyilhs0p8wz96b84lpxagraxw823d3b4bvw") (y #t)))

(define-public crate-lodepng-2.7.2 (c (n "lodepng") (v "2.7.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1hlwjgxqkzy1vgxnpjb8s5viyj2xwrv7c7xv0qbmwz6i86ddia79")))

(define-public crate-lodepng-3.0.0 (c (n "lodepng") (v "3.0.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "0ps3yfkqla6ky3rl4nk7ivxf8gd0cnq3m7lfci44p9ndal3wfnzy") (y #t)))

(define-public crate-lodepng-3.1.0 (c (n "lodepng") (v "3.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1lb677fp9yjdand35fy0llfs0f6vdj7r8wwcxd9cn1rbd87iri48")))

(define-public crate-lodepng-3.2.0 (c (n "lodepng") (v "3.2.0") (d (list (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1qrmhhlja6mb0255pfk38j447qyzrpzlj1sicixzbhkkpfnq1p5b") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.2.1 (c (n "lodepng") (v "3.2.1") (d (list (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1np1d6kmqr9mq7g3z5ayhzr58i81k61mi74msix3fih153haqvqx") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-2.7.3 (c (n "lodepng") (v "2.7.3") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "036msl3jgrk0cqfn0f52yp2pky39azrhnbkcinv3hmljpnvvmv3p")))

(define-public crate-lodepng-3.2.2 (c (n "lodepng") (v "3.2.2") (d (list (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1avywrsbmj5sdr728695pfp66hddnych3f36dp3qjfr2hj8r1sxn") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.0 (c (n "lodepng") (v "3.4.0") (d (list (d (n "fallible_collections") (r "^0.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "0prmhhq29w57mfnaxvixsgiqqfyf353niv90br4c402mw0z8p2b1") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.1 (c (n "lodepng") (v "3.4.1") (d (list (d (n "fallible_collections") (r "^0.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1xv6xlilk1zri5469gml1rshd2famhpzrrmrzbkjix1v3ggvh341") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.2 (c (n "lodepng") (v "3.4.2") (d (list (d (n "fallible_collections") (r "^0.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "rgb") (r "^0.8.20") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1l9immcqs4bf8xq6yxp7axlalhjz31m4wcnp50w819wq2f1yadzn") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.3 (c (n "lodepng") (v "3.4.3") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "04sn6q2kaiwbzlnwchyl0w3w5d6ypdlng5fcwn74dp4pglbksi0i") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.4 (c (n "lodepng") (v "3.4.4") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1s96cjjqf25ja210wx7y2wwy7d6m7g7a2s1y2wly31zgpg659m8b") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.5 (c (n "lodepng") (v "3.4.5") (d (list (d (n "fallible_collections") (r "^0.4.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1mrn9hx3r08wbi2pmpa7diwagbp8p83x1n2zhx4fmahg72i7h1y6") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.6 (c (n "lodepng") (v "3.4.6") (d (list (d (n "fallible_collections") (r "^0.4.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "15pv83j49zdp6s1nw5x5vn0h4c7qvv0mzbf6rh0r0cka2hdyzxz1") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.4.7 (c (n "lodepng") (v "3.4.7") (d (list (d (n "fallible_collections") (r "^0.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)) (d (n "rgb") (r "^0.8.29") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "08x3q57v07kpcfs1ph3fjjd7k3kz9i59dw2vzd9dsbcj1df4v114") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.5.0 (c (n "lodepng") (v "3.5.0") (d (list (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1hzxrs13kwwcyw9v6477gd9frahi917sw8va1wg0ym98rj3gm6zf") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.5.1 (c (n "lodepng") (v "3.5.1") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "18kqc72bv17g0k5c1rr3iy01d8nia64cnqc217aw8mgh5qi44m2r") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.5.2 (c (n "lodepng") (v "3.5.2") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1ji6x0l1f6j25rvyk7d50fgjzqaiaiz5gvgsj362f9bcy2qgs190") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.6.0 (c (n "lodepng") (v "3.6.0") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "0jivm7iimwg7ppww79sikacmzlmr4ls8nx0104m9x348501a4rpv") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.6.1 (c (n "lodepng") (v "3.6.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1iwyk1xq7xl60sknwgwrdrw4f1hfdnwbdbdakh7kz2xyrpyy313z") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.6.2 (c (n "lodepng") (v "3.6.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "rgb") (r "^0.8.31") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "11mqnp2w9hjb5g0kq3fzcfvfj51mhnd2j3zacawvz7blvm2rdaam") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib"))))))

(define-public crate-lodepng-3.7.0 (c (n "lodepng") (v "3.7.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.4") (f (quote ("std_io" "rust_1_57"))) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.33") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1izbyggpr5d7vvd9zzd860g51nzdb488ciyd9w22qicpqx756igz") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (r "1.57")))

(define-public crate-lodepng-3.7.1 (c (n "lodepng") (v "3.7.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1s6gi8ra9bgwlf2nsypiy5wcxlcgrd7slr5ps9664lyffkhq0jxa") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (y #t) (r "1.57")))

(define-public crate-lodepng-3.7.2 (c (n "lodepng") (v "3.7.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1y24y0hhnwkrim48bizrpjjld01acd1nacgjnq5v395sbgvkkbgh") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (r "1.57")))

(define-public crate-lodepng-3.8.0 (c (n "lodepng") (v "3.8.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1f5d9bva17sq7npw9qh7h6sh02k0ycrjx5hr147q0jv4m0qd2970") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (r "1.57")))

(define-public crate-lodepng-3.8.1 (c (n "lodepng") (v "3.8.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "0ppnnf7ia2m1hz8k857n42vp6n0vwhlqs9k5x56qfpg55n6cj4z1") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (r "1.57")))

(define-public crate-lodepng-3.8.2 (c (n "lodepng") (v "3.8.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1azynhq99d2avc882y1wzc9qyxbrjfb60lfy64v98sp1r5i1ij3k") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend") ("cfzlib" "flate2/cloudflare_zlib")))) (r "1.64")))

(define-public crate-lodepng-3.9.0 (c (n "lodepng") (v "3.9.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1jp74acma77r473hlfs1hmday0lwrqa02x1qcaxkrjr5zd6vh7d2") (f (quote (("rust_backend" "flate2/rust_backend") ("default" "rust_backend" "c_ffi") ("cfzlib" "flate2/cloudflare_zlib")))) (y #t) (s 2) (e (quote (("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.9.1 (c (n "lodepng") (v "3.9.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "01h6vhm830j3p3rzn6ky501rbqf2fbzvqvk5y1bd99aprz8crkd3") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (y #t) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.9.2 (c (n "lodepng") (v "3.9.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "04vyz5yvx38pcvk831d98ygl7sfllz4flwrb2ymj2mydkgzmc3xh") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (y #t) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.9.3 (c (n "lodepng") (v "3.9.3") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "04hxw43g3l9hd0wx8jh0xcqs5gd2k1rlypvmwdx48k4jv4lfi57l") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.9.4-rc (c (n "lodepng") (v "3.9.4-rc") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "0xgvl4xh8qdnw9mikhzmmady2xp91f56zy4z0barf619x2va77qp") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (y #t) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.9.4 (c (n "lodepng") (v "3.9.4") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "11g77vzcc2b4il3dzbyrigpwfi5hxsj5z018vqm139sy9xfncsg4") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.10.0 (c (n "lodepng") (v "3.10.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1kib455m5c52s094ah5px2p7ymr4p99fglcckkmrrqrgik5p2pab") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.64")))

(define-public crate-lodepng-3.10.1 (c (n "lodepng") (v "3.10.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fallible_collections") (r "^0.4.5") (f (quote ("rust_1_57"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (k 0)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (f (quote ("as-bytes"))) (d #t) (k 0)))) (h "1n020101h85pqzm6slcyk0pb8y7jmm2bv6p2gn202i5ijj32jbd4") (f (quote (("rust_backend" "flate2/rust_backend") ("ngzlib" "flate2/zlib-ng-compat") ("default" "rust_backend" "c_ffi" "deprecated_back_compat_error_type") ("cfzlib" "flate2/cloudflare_zlib")))) (s 2) (e (quote (("deprecated_back_compat_error_type" "dep:fallible_collections") ("c_ffi" "dep:libc")))) (r "1.65")))

