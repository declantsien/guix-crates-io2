(define-module (crates-io lo de lodestone_core) #:use-module (crates-io))

(define-public crate-lodestone_core-0.2.3 (c (n "lodestone_core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1rb5x7cmq7f0hgkkk63miasj16sk3vlg1rkf38fv7ii21qj38i8i")))

