(define-module (crates-io lo de lodestone) #:use-module (crates-io))

(define-public crate-lodestone-0.1.0 (c (n "lodestone") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "1bz17arqigy4a5kn9yqjlwra7kak0ga2w9848bx9ic86bjrzxdsl")))

(define-public crate-lodestone-0.1.1 (c (n "lodestone") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0yk1z8vnqkidv7pniw0l3gkmrq126nkjpjik7gq1z3flmxj2pbav")))

(define-public crate-lodestone-0.1.2 (c (n "lodestone") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0mx84z6pznwsg0nvmn846kjrfdlgpmwi318bfnqs8vz1093n31yb")))

(define-public crate-lodestone-0.2.0 (c (n "lodestone") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "1l8z5gm0kkw14mlkz68vg2lmc8vscrl88rcnmvn414k4zvn289kd")))

(define-public crate-lodestone-0.3.0 (c (n "lodestone") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)))) (h "0bs0wcrv3a4v41db6vf9a74b368mz0izg2dn4fhb0w05sxqqvhdi")))

(define-public crate-lodestone-0.4.0 (c (n "lodestone") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "008frnw40sm7qz93wsids0dj30kbqgzqf2czz1qixkkaiwxq2v2d")))

