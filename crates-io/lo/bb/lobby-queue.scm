(define-module (crates-io lo bb lobby-queue) #:use-module (crates-io))

(define-public crate-lobby-queue-0.1.0 (c (n "lobby-queue") (v "0.1.0") (h "1p9xs99kcicrpwlkq7y033w2f9whjpjs2kzznzskg0ka9m858y7w")))

(define-public crate-lobby-queue-0.2.0 (c (n "lobby-queue") (v "0.2.0") (h "1m631qhnlrbs9av069w87r6iqcb6qy3imxwc6z115h2421vb7bgs")))

