(define-module (crates-io lo g4 log42navi) #:use-module (crates-io))

(define-public crate-log42navi-0.1.0 (c (n "log42navi") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "0227ghhhsprfarald64mfsfbl02h00vbjwpilf7nm3r0f5f2jdfz") (y #t)))

(define-public crate-log42navi-0.1.1 (c (n "log42navi") (v "0.1.1") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "1pm2rxq286dc3ydjdsz2m7j4yszjhxs2agyammd7pg2xg8cs1cm9") (y #t)))

(define-public crate-log42navi-0.1.2 (c (n "log42navi") (v "0.1.2") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "1wkfap7f45i6bxrh9y3m2087pgbvdcn29mknjajma7r1wjalazlx")))

