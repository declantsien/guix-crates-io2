(define-module (crates-io lo g4 log4r) #:use-module (crates-io))

(define-public crate-log4r-0.1.0 (c (n "log4r") (v "0.1.0") (h "1vaw43x85nxb2k7z77p12hn4d8wcpzghllxgzj46xgp2vvimrxxr")))

(define-public crate-log4r-0.1.1 (c (n "log4r") (v "0.1.1") (h "041b825js1ksvqlpn4p4rmbc5lqf3wr10rr036l6wpwn9nnyhj10")))

