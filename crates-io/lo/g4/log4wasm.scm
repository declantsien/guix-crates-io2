(define-module (crates-io lo g4 log4wasm) #:use-module (crates-io))

(define-public crate-log4wasm-0.1.1 (c (n "log4wasm") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (k 0)))) (h "1b1fjbnb02w1qc8zx7123mvziy69mdjv5xk9hkl8k2q443zq236y") (f (quote (("default" "web-sys/console")))) (r "1.60")))

(define-public crate-log4wasm-0.1.2 (c (n "log4wasm") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (k 0)))) (h "0fahy9cm6sbr9fxk8l9fdvifvvcp9nmhsynrzqgxfaapddpj40bj") (f (quote (("default" "web-sys/console")))) (r "1.60")))

(define-public crate-log4wasm-0.1.3 (c (n "log4wasm") (v "0.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (k 0)))) (h "0jxfgml6f0d4ziwhmfdz4mjxdii0s8nj8vn9cavwja1p4b5mg04y") (f (quote (("default" "web-sys/console")))) (r "1.60")))

