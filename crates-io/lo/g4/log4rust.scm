(define-module (crates-io lo g4 log4rust) #:use-module (crates-io))

(define-public crate-log4rust-0.1.0 (c (n "log4rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "047wv2qf3c6ips5l70cx5bdm2nnlxdj3kcc2dkldmr38m18jsgk5")))

(define-public crate-log4rust-0.1.1 (c (n "log4rust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "1h66zv78zy1z6m0r99hnkj0r98vfn1aljibpjdqzcf2arli33kw1")))

(define-public crate-log4rust-0.1.2 (c (n "log4rust") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "004fk878mv27z4mhrz41fkl3rd2dywacj426m6k3kp1vg89pjm2w")))

(define-public crate-log4rust-0.1.3 (c (n "log4rust") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "028dln5y7nmcpk28c07z9qyp9ig73dwc22wpmjb0fyhy1lf7w8ry")))

(define-public crate-log4rust-0.1.4 (c (n "log4rust") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (d #t) (k 0)))) (h "19gw3nfs1isgg5h7i6b6mfwabmvwcl2h4phzcnq3k5hjqfw1wjrc")))

(define-public crate-log4rust-0.1.5 (c (n "log4rust") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "18valf93lai4b58cgy2kfqxl8yl5iqfb9bvv5yhi24iwwwxk6zfj") (s 2) (e (quote (("web" "dep:ureq"))))))

(define-public crate-log4rust-0.1.6 (c (n "log4rust") (v "0.1.6") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "1zx1xk9sysb8b0j8dsbbhpfsnrf3myg71cvshdbcgvmyabxi4j35") (s 2) (e (quote (("web" "dep:ureq"))))))

