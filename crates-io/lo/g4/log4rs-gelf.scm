(define-module (crates-io lo g4 log4rs-gelf) #:use-module (crates-io))

(define-public crate-log4rs-gelf-0.1.0 (c (n "log4rs-gelf") (v "0.1.0") (d (list (d (n "gelf_logger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_gelf") (r "^0.1") (d #t) (k 0)))) (h "11rsw21m4gqisca9rm9g3nvkf971iyqg6wdsh6jd5n60kf93aza0") (f (quote (("ovh-ldp" "gelf_logger/ovh-ldp" "serde_gelf/ovh-ldp"))))))

(define-public crate-log4rs-gelf-0.1.1 (c (n "log4rs-gelf") (v "0.1.1") (d (list (d (n "gelf_logger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_gelf") (r "^0.1") (d #t) (k 0)))) (h "1j9cw5kbnn5qy3qnr13sk299j2m67nrj7kfzyn4m2y3d6mr5pxrd") (f (quote (("ovh-ldp" "gelf_logger/ovh-ldp" "serde_gelf/ovh-ldp"))))))

(define-public crate-log4rs-gelf-0.1.2 (c (n "log4rs-gelf") (v "0.1.2") (d (list (d (n "gelf_logger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde_gelf") (r "^0.1") (d #t) (k 0)))) (h "0hmg110vkg5j587z0zj1p66f33h6dpy15rdza6yaw7kxmjqn11rd") (f (quote (("ovh-ldp" "gelf_logger/ovh-ldp" "serde_gelf/ovh-ldp"))))))

(define-public crate-log4rs-gelf-0.1.3 (c (n "log4rs-gelf") (v "0.1.3") (d (list (d (n "gelf_logger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde_gelf") (r "^0.1") (d #t) (k 0)))) (h "1rkw4bsd1axmxc3fzvpjdzq879v8g2y1kyc1n624laqr7x6jsv11") (f (quote (("ovh-ldp" "gelf_logger/ovh-ldp" "serde_gelf/ovh-ldp"))))))

(define-public crate-log4rs-gelf-0.1.4 (c (n "log4rs-gelf") (v "0.1.4") (d (list (d (n "gelf_logger") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "serde_gelf") (r "^0.1") (d #t) (k 0)))) (h "0xiz7ji3699ligxhl0qxw3mycfb2awk35s9wpqrzxkhp4raw2rvk") (f (quote (("ovh-ldp" "gelf_logger/ovh-ldp" "serde_gelf/ovh-ldp"))))))

