(define-module (crates-io lo g4 log4rs-macros) #:use-module (crates-io))

(define-public crate-log4rs-macros-1.1.1 (c (n "log4rs-macros") (v "1.1.1") (d (list (d (n "log4rs") (r "^1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)))) (h "1p62xxwm71l50gwsydx65pk4fim3af5m4r2s9cam0qj2529y7j16")))

