(define-module (crates-io lo g4 log4rs_dynamic_filters) #:use-module (crates-io))

(define-public crate-log4rs_dynamic_filters-0.1.0 (c (n "log4rs_dynamic_filters") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "log4rs_test_utils") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "01fka3i7jil9nl25p25ndzhwwg1b22652mh9gi295zpdpmlm9b9s")))

(define-public crate-log4rs_dynamic_filters-0.1.1 (c (n "log4rs_dynamic_filters") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "log4rs_test_utils") (r "^0.2.0") (f (quote ("log_testing"))) (k 2)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "0hfd2b75z1pnarb72m1cw0jy6c7sdb3d4zbwqa9c979qvx7j9xy9")))

(define-public crate-log4rs_dynamic_filters-0.1.2 (c (n "log4rs_dynamic_filters") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (f (quote ("config_parsing"))) (k 0)) (d (n "log4rs_test_utils") (r "^0.2.1") (f (quote ("log_testing"))) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1rw50vpig1cmrhnaipnskqcpvz9biyp8g66bzvgpzbiyzc7i9ql8")))

(define-public crate-log4rs_dynamic_filters-0.1.3 (c (n "log4rs_dynamic_filters") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (f (quote ("config_parsing"))) (k 0)) (d (n "log4rs_test_utils") (r "^0.2.1") (f (quote ("log_testing"))) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0af9lpvadfr1aa0xv0jr03bd989p8j4h580haqx9hyvybra4zqlg")))

