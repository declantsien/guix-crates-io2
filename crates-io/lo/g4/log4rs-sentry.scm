(define-module (crates-io lo g4 log4rs-sentry) #:use-module (crates-io))

(define-public crate-log4rs-sentry-1.0.0 (c (n "log4rs-sentry") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "sentry") (r "^0.15") (d #t) (k 0)))) (h "131nikp2czgckcw09iwm5scx12k5djkzmh7a2w0pib7riy446zdh")))

(define-public crate-log4rs-sentry-1.0.1 (c (n "log4rs-sentry") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (f (quote ("simple_writer"))) (k 0)) (d (n "sentry") (r "^0.15") (d #t) (k 0)))) (h "0ii56q2a3pbxqmgx325ywwhbs6hkhycpa2c3nwq234cvnm8iqng9")))

