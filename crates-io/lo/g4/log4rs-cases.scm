(define-module (crates-io lo g4 log4rs-cases) #:use-module (crates-io))

(define-public crate-log4rs-cases-0.1.0 (c (n "log4rs-cases") (v "0.1.0") (h "1dsx6gzsgi5rf1v74y34m9nhl32rhxca98vciwlli0m9aj2cq3dj")))

(define-public crate-log4rs-cases-0.1.1 (c (n "log4rs-cases") (v "0.1.1") (d (list (d (n "failure") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "log4rs") (r "0.8.*") (d #t) (k 0)))) (h "1qi60lqspqpragq4xz3yd68m84djk42h11qqqw6g91z3kaxph335")))

(define-public crate-log4rs-cases-0.2.0 (c (n "log4rs-cases") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.13") (d #t) (k 0)))) (h "0lzgp7id81i0hh4y703zzq9p3jfna7vbvpr791416mgin87pnmhs")))

