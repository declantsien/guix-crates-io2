(define-module (crates-io lo g4 log4rs_test_utils) #:use-module (crates-io))

(define-public crate-log4rs_test_utils-0.1.0 (c (n "log4rs_test_utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)))) (h "1i5qbbxc87w8bcikwhz9n0pcix7010aa2fg76d0sj8fgjifw9p9r")))

(define-public crate-log4rs_test_utils-0.2.0 (c (n "log4rs_test_utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)))) (h "1j5zkp92p9mrw22dwnpqzgdfyxi755n3gdcgfd3ril2a9w6ry9n4") (f (quote (("test_logging") ("log_testing" "lazy_static") ("default" "log_testing" "test_logging"))))))

(define-public crate-log4rs_test_utils-0.2.1 (c (n "log4rs_test_utils") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (f (quote ("pattern_encoder"))) (k 0)))) (h "11mchw1fqpsg3zkf8sn32m9qlhwlr934jvalrgc5ndh76y6vg7yr") (f (quote (("test_logging") ("log_testing" "lazy_static") ("default" "log_testing" "test_logging"))))))

(define-public crate-log4rs_test_utils-0.2.2 (c (n "log4rs_test_utils") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (f (quote ("pattern_encoder"))) (k 0)))) (h "1ichxvw12hs9j177br5k80ml52nbkz6x6d9vwlda5g3h6nkdx8ya") (f (quote (("test_logging") ("log_testing" "lazy_static") ("default" "log_testing" "test_logging"))))))

(define-public crate-log4rs_test_utils-0.2.3 (c (n "log4rs_test_utils") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (f (quote ("pattern_encoder"))) (k 0)))) (h "1xf4q28b919qip1y0257h9zvv3kmdjj5qh3mqvqnsq0qmxvbqgsw") (f (quote (("test_logging") ("log_testing" "lazy_static") ("default" "log_testing" "test_logging"))))))

