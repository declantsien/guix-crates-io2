(define-module (crates-io lo g4 log4rs-logstash) #:use-module (crates-io))

(define-public crate-log4rs-logstash-0.1.0 (c (n "log4rs-logstash") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "humantime-serde") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "logstash-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 2)))) (h "1zibdfvs3ai3hzajrqsj76p8x9iiakplhkq0l14xphs6xwlcmnqi") (f (quote (("tls" "logstash-rs/tls") ("rustls" "logstash-rs/rustls"))))))

