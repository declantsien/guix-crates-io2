(define-module (crates-io lo g4 log4s-mqtt) #:use-module (crates-io))

(define-public crate-log4s-mqtt-1.0.0 (c (n "log4s-mqtt") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.12") (f (quote ("bundled" "build_bindgen"))) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "05080kx96lb6c7w935jpj44vxnb994m4ij5prqzypvfvifxnip08") (f (quote (("default" "config_parsing") ("config_parsing" "log4rs/config_parsing" "log/serde" "serde" "serde_derive")))) (y #t)))

