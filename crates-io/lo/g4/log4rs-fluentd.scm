(define-module (crates-io lo g4 log4rs-fluentd) #:use-module (crates-io))

(define-public crate-log4rs-fluentd-0.1.0 (c (n "log4rs-fluentd") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "log4rs") (r "^0.8") (f (quote ("simple_writer"))) (d #t) (k 0)) (d (n "poston") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1cixl4jcisli8hfh391cyzaj5v8640zda8drxp3s5agwhsj35qjn")))

(define-public crate-log4rs-fluentd-0.2.0 (c (n "log4rs-fluentd") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "log4rs") (r "^0.8") (f (quote ("simple_writer"))) (d #t) (k 0)) (d (n "poston") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1il7jfa8qyr4gqqslyjvl5nyz2k7syww9fm7awv826sv6pjs13j6")))

