(define-module (crates-io lo g4 log4rs-syslog-net) #:use-module (crates-io))

(define-public crate-log4rs-syslog-net-0.1.0 (c (n "log4rs-syslog-net") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^0.13.0") (f (quote ("simple_writer" "pattern_encoder"))) (k 0)))) (h "1dv5kib66dbmbwg6skapwlib70dcxp2xd234p6lxpl2mrs3hgwr9")))

(define-public crate-log4rs-syslog-net-0.2.0 (c (n "log4rs-syslog-net") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^0.13.0") (f (quote ("simple_writer" "pattern_encoder"))) (k 0)))) (h "1kiw1j3sbc47c5sf9r3hcygfka5dapsvs5d7lrdr1cdympk6m9j6")))

(define-public crate-log4rs-syslog-net-0.2.1 (c (n "log4rs-syslog-net") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^0.13.0") (f (quote ("simple_writer" "pattern_encoder"))) (k 0)))) (h "0klyqvj56vmzv3lzzx1ha1k5ay92klgjd8q799hcabn21zqwi8l5")))

(define-public crate-log4rs-syslog-net-0.2.2 (c (n "log4rs-syslog-net") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "log4rs") (r "^0.13.0") (f (quote ("simple_writer" "pattern_encoder"))) (k 0)))) (h "0b6asaqkdfzx4yq9bilpykncgzsc035v9p2rq6r5mfk4i87l5mpd")))

