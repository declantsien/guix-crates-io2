(define-module (crates-io lo g4 log4j) #:use-module (crates-io))

(define-public crate-log4j-0.1.0 (c (n "log4j") (v "0.1.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "blocking"))) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1gfi6ci58k4q2j35bbbq082fr1n4d5bgyx3gbdsrspfcf8i391mi")))

