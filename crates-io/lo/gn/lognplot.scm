(define-module (crates-io lo gn lognplot) #:use-module (crates-io))

(define-public crate-lognplot-0.1.0 (c (n "lognplot") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "1mxc5s52ml4n1s20652zikvh0rh03d9ff2ivv8kqhbjq63wl4y35") (f (quote (("server" "tokio" "futures" "serde" "serde_cbor") ("cairo" "cairo-rs"))))))

