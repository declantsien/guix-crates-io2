(define-module (crates-io lo g_ log_buffer) #:use-module (crates-io))

(define-public crate-log_buffer-0.1.0 (c (n "log_buffer") (v "0.1.0") (h "1sg5hh0lrs7zza6ni1kl0mgw693hps6wn8s19z029n15a5iarzzd")))

(define-public crate-log_buffer-1.0.0 (c (n "log_buffer") (v "1.0.0") (h "0npj6jqwypqyp1x6pv51h9z6l22yvv24ai47b2cjz9gc4jxbbgpq")))

(define-public crate-log_buffer-1.1.0 (c (n "log_buffer") (v "1.1.0") (h "14x45b5qq3rarf1kmpn62wrlfrwynp4njcd9dbvvvrxvhhxp4mzc") (f (quote (("const_fn"))))))

(define-public crate-log_buffer-1.2.0 (c (n "log_buffer") (v "1.2.0") (h "069497i5128721frhk3lnrd3kbl36h55r2cwlybzxdw6jhy1fczh") (f (quote (("const_fn"))))))

