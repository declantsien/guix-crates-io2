(define-module (crates-io lo g_ log_domain) #:use-module (crates-io))

(define-public crate-log_domain-0.3.0 (c (n "log_domain") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)))) (h "0lpszrfhv7dvcv42ljzdxi0gk85l0vif9cq3lwgrdavi62wqqdmb")))

(define-public crate-log_domain-0.4.0 (c (n "log_domain") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j681n5qqbsg691f8ffxmmla18d7qrn44phcdpwl3wmqcci9s0a5")))

(define-public crate-log_domain-0.4.1 (c (n "log_domain") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04c4fp3v8aaki286x63cg0xz1y4qvns0x8a4w419zjy1kxi7qddb")))

