(define-module (crates-io lo g_ log_wrapper) #:use-module (crates-io))

(define-public crate-log_wrapper-0.1.0 (c (n "log_wrapper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1kv8cgy98bd5rghb5hm1g3w484lpm7knq07vsbzr3syhhckisf1l")))

(define-public crate-log_wrapper-0.1.1 (c (n "log_wrapper") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r3l3vs6c6jgxsajpqfp4fdlvsyj9jxgb7l1gzjjh0qrf42wsnpm")))

