(define-module (crates-io lo g_ log_settings) #:use-module (crates-io))

(define-public crate-log_settings-0.1.0 (c (n "log_settings") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "09s7pvyvnm632bgjrxi0bkr4ll0kxkv48097lwn6ji1c54755zdv")))

(define-public crate-log_settings-0.1.1 (c (n "log_settings") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1dhm75hbnqwas55j7gslig3hzsmxalrdp6a81iwhkg0gx8r2ff1x")))

(define-public crate-log_settings-0.1.2 (c (n "log_settings") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1kg7cf5ll4a1cznhdgsdxj4wxmfl885sslw10nr1jz2xavq43bqr")))

