(define-module (crates-io lo g_ log_err) #:use-module (crates-io))

(define-public crate-log_err-1.0.0 (c (n "log_err") (v "1.0.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.10.*") (d #t) (k 2)))) (h "080ma11yz4hml0vnn8qy8231h1m5kd6j039d9dzpsj47q27maakk")))

(define-public crate-log_err-1.0.1 (c (n "log_err") (v "1.0.1") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.10.*") (d #t) (k 2)))) (h "11axyxykbij3sifffskpg13gzldsglbsq3m31dnjifnr1l8457dz")))

(define-public crate-log_err-1.1.0 (c (n "log_err") (v "1.1.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.10.*") (d #t) (k 2)))) (h "0lq0n0v2y14rh4ikiwk2mjk9b42qcl4iglvg58dvn5wyc51pks38")))

(define-public crate-log_err-1.1.1 (c (n "log_err") (v "1.1.1") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "simplelog") (r "0.10.*") (d #t) (k 2)))) (h "1xv0m3xfjncrphws9fq0ykrhd0q136z1ny9swlkq58cda209x0p2")))

