(define-module (crates-io lo g_ log_macro) #:use-module (crates-io))

(define-public crate-log_macro-0.1.0 (c (n "log_macro") (v "0.1.0") (h "17dfljkg48i801z5zq1jcqd75c92kv9yrmr811k0m25hz5ribvjb")))

(define-public crate-log_macro-0.1.1 (c (n "log_macro") (v "0.1.1") (h "0sv5zdrhngkh5mqwi97p1pky6lviass5f0fivs59krhxw7r47ig3")))

(define-public crate-log_macro-0.1.2 (c (n "log_macro") (v "0.1.2") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "16nj2sgs6viy2kqnkhna0h2vgpd017fl56yl9f9kb078xjlh1kgs")))

(define-public crate-log_macro-0.1.3 (c (n "log_macro") (v "0.1.3") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "1rwfmvv3nl04bgf1gb6inmfzbvy7lr3rspgh73kffs38khm0mlvg")))

(define-public crate-log_macro-0.1.4 (c (n "log_macro") (v "0.1.4") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "0j64wlavbab2p7cdvsws733ckrrzfkxdg4y9gkvwdxnv7z5g0mh9")))

(define-public crate-log_macro-0.1.5 (c (n "log_macro") (v "0.1.5") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "1wi3cjqvf7gnq7d52rq80gpfwjcwyg0j4k14fp8x1nx020q58iki")))

(define-public crate-log_macro-0.1.6 (c (n "log_macro") (v "0.1.6") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 2)))) (h "00a30azn9skpx0jbsf3n3a70bmn856zj7sqv4dcwm4780v2wbsj1")))

