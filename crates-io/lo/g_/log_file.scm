(define-module (crates-io lo g_ log_file) #:use-module (crates-io))

(define-public crate-log_file-0.1.0 (c (n "log_file") (v "0.1.0") (h "1kyrd55zljyhn7sma548w23a5y9g62c0018nmzs9mmgfki0rpn4h")))

(define-public crate-log_file-0.1.1 (c (n "log_file") (v "0.1.1") (h "163mx1gx1kdiamk165fgp4cy7cpl0hk4c0fi1wawqzjridlvwmnr")))

