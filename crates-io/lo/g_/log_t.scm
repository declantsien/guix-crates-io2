(define-module (crates-io lo g_ log_t) #:use-module (crates-io))

(define-public crate-log_t-0.1.0 (c (n "log_t") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "contracts") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0hl9hwlkh0gigpgn9bnspv3aph6f21fcx92nfh97v5x3dx0gjg99")))

