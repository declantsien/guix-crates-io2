(define-module (crates-io lo g_ log_std) #:use-module (crates-io))

(define-public crate-log_std-0.1.0 (c (n "log_std") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "log_hal") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0n4qhxjglr0cx1yjwc3sx92lq1rx5b2b997sgnh70827mznpqghv")))

