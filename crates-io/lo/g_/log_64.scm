(define-module (crates-io lo g_ log_64) #:use-module (crates-io))

(define-public crate-log_64-0.1.0 (c (n "log_64") (v "0.1.0") (d (list (d (n "log") (r "^0.4.0") (k 0)))) (h "13yx31f0yqkq464byyhla4na8dnc696dnma5kxfz8kfi5vpkngvb") (y #t)))

(define-public crate-log_64-0.1.1 (c (n "log_64") (v "0.1.1") (d (list (d (n "log") (r "^0.4.0") (k 0)))) (h "1bikjv7n9y5015ap2v5zrfca49zp6k9fpkp8snr11yxbibqa1zw0")))

