(define-module (crates-io lo ca locator) #:use-module (crates-io))

(define-public crate-locator-0.1.0 (c (n "locator") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1j2icqfcb411rxdh9syg14vr91nl13m8z3hbc98dyqwjrnzbybis") (y #t)))

(define-public crate-locator-0.1.1 (c (n "locator") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1azj7da2hbidacjcd8hassis204w1blhccbr8wj5b9mk5c9ysyk1") (y #t)))

(define-public crate-locator-0.2.0 (c (n "locator") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "0zn5d0x3jrnbvx308dip0783s6wb8bvlik66k9j8zhfb80567mvr") (y #t)))

(define-public crate-locator-0.2.1 (c (n "locator") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "188m0qiw9w49vfnb6wq2i597xyxvgc3w517vphsl5gf5z4hpz8g4") (y #t)))

(define-public crate-locator-0.2.2 (c (n "locator") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (d #t) (k 0)))) (h "1sd8dwask5drmcf1mn0si7sbh18qh4rwsm9czk4lsvzwiwclnfbz") (y #t)))

