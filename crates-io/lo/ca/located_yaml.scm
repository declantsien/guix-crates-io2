(define-module (crates-io lo ca located_yaml) #:use-module (crates-io))

(define-public crate-located_yaml-0.1.0 (c (n "located_yaml") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "07i2jrh945846h9wf1wqqw25r5v5wkbwv0w8lsj0ygbm2sj2k7zb")))

(define-public crate-located_yaml-0.2.0 (c (n "located_yaml") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "0s1n6399fals9xxszmx641fc4j4w9ipw6n1wp94f5rzc6rn5b475")))

(define-public crate-located_yaml-0.2.1 (c (n "located_yaml") (v "0.2.1") (d (list (d (n "linked-hash-map") (r "^0") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0") (d #t) (k 0)))) (h "0xnx5al5v7d9syspj0irm22alwc3a9adikqxpbyyf6vsz3k8xilv")))

