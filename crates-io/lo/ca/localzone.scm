(define-module (crates-io lo ca localzone) #:use-module (crates-io))

(define-public crate-localzone-0.1.0 (c (n "localzone") (v "0.1.0") (d (list (d (n "chrono-tz") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_System_Time"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lmc7agx4wj3bkgshq7hbddavz49wnf0wsm0jxazmhf97vym8zdh") (f (quote (("win_zones") ("auto_validation" "chrono-tz"))))))

(define-public crate-localzone-0.2.0 (c (n "localzone") (v "0.2.0") (d (list (d (n "chrono-tz") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.55") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_System_Time"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vvc457j3nqnkagfyklqvfgk544hff6m6zpyp2082m6b7shzarbq") (f (quote (("win_zones") ("auto_validation" "chrono-tz"))))))

(define-public crate-localzone-0.3.1 (c (n "localzone") (v "0.3.1") (d (list (d (n "chrono-tz") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.69") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Foundation" "Win32_System_Time"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kbv9zsxndv9f12spf0k0lkbgrmm3pm92ir2h8ppahiqgfm311bi") (f (quote (("win_zones") ("auto_validation" "chrono-tz"))))))

