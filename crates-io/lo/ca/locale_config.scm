(define-module (crates-io lo ca locale_config) #:use-module (crates-io))

(define-public crate-locale_config-0.1.0 (c (n "locale_config") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1vnak228kl8nsh9q0nj8fpqfx5arjqqhhfx3k0s6b2ffhhrikv7x")))

(define-public crate-locale_config-0.1.1 (c (n "locale_config") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0bkkhlnhrc550fpc6ssxplbjh91kv2g6rd9w8p2y3x9x7hg9b7s5")))

(define-public crate-locale_config-0.2.0 (c (n "locale_config") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "10l807yd2hkghyarf0gh0r68rnyh83c0msavn1fxsw9j4wb6ip1n")))

(define-public crate-locale_config-0.2.1 (c (n "locale_config") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0n3nzripgdrg5958k1qr6g6fqi1a7l5cg2qqq0wi3izd0l8w5w3y")))

(define-public crate-locale_config-0.2.2 (c (n "locale_config") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1aqxzrp5z7rma1shn2lvs0gwq9lqdskdwkvw8aidcbdw747fxyql")))

(define-public crate-locale_config-0.2.3 (c (n "locale_config") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p2kdgc1c9cq5bi2rpszbhkh7pdk1fwxhij37gayb2alwkmikb3k")))

(define-public crate-locale_config-0.3.0 (c (n "locale_config") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d399alr1i7h7yji4vydbdbzd8hp0xaykr7h4rn3yj7l2rdw7lh8")))

