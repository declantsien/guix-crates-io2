(define-module (crates-io lo ca localization-macros) #:use-module (crates-io))

(define-public crate-localization-macros-0.1.0 (c (n "localization-macros") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0zdfpjaq00gm7bb7sc0ba0xf1040dmmfyd56y4njvc36h6gm5mzj")))

(define-public crate-localization-macros-0.1.1 (c (n "localization-macros") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "193rkxc7p40g2ybab5jrisf4rj5jsk508nm7c2zvr0psrhs06rlw")))

(define-public crate-localization-macros-0.1.2 (c (n "localization-macros") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0nw919fv0mnic5hz7d4d4h1qlxdx9dchwd9xh57fi12nkm4msrkq")))

(define-public crate-localization-macros-0.1.3 (c (n "localization-macros") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1fz0202bwykr2c4pcfapdydx8izljiqrlmidz6iwb51di3r165p9")))

