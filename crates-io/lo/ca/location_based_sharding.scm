(define-module (crates-io lo ca location_based_sharding) #:use-module (crates-io))

(define-public crate-location_based_sharding-0.1.0 (c (n "location_based_sharding") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "s2") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0s30lbns4pfixv5xqqd0d9037scgh0qg3200qqwjbfdsxmivq9ps")))

(define-public crate-location_based_sharding-0.1.1 (c (n "location_based_sharding") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "s2") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "00a4m7kwfjyq4l7nl5bs8w16p0i0j9i91iqkhd9xjwyl9c37iypb")))

(define-public crate-location_based_sharding-0.1.2 (c (n "location_based_sharding") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "s2") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "12pqf0b30lvmpmah2v8v1lpkv74xf2gxdvmwv3ld612xdmy5rpjf")))

(define-public crate-location_based_sharding-0.1.3 (c (n "location_based_sharding") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "s2") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "1rw6pzrszrmvk5y6367zcn5vwjchl2v8whalrlv5n4x3sk9adqnl")))

(define-public crate-location_based_sharding-0.1.4 (c (n "location_based_sharding") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "s2") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0sg8f36l5naiqxl2jcc3ls8mfb1a719iczk34jsp7n4q33a3v2k3")))

