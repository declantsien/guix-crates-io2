(define-module (crates-io lo ca localize) #:use-module (crates-io))

(define-public crate-localize-0.1.0 (c (n "localize") (v "0.1.0") (d (list (d (n "json") (r "^1.0.0") (d #t) (k 0) (p "serde_json")) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1066hr5w79by45m837j5y7ys15ixcp176gz13s459fhmwcz7yykg")))

(define-public crate-localize-0.1.1 (c (n "localize") (v "0.1.1") (d (list (d (n "json") (r "^1.0.0") (d #t) (k 0) (p "serde_json")) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1h9xpmk0i9kaqaynsj68zzj0y1lvdn559cr26chsv0ll6a6rbjv8")))

(define-public crate-localize-0.2.0 (c (n "localize") (v "0.2.0") (d (list (d (n "json") (r "^1.0.0") (d #t) (k 0) (p "serde_json")) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0vrkfnmq7ljrdc6ialjr0cnm492r9n32ax4qcahsvq4a7wc40vry")))

