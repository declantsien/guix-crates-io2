(define-module (crates-io lo ca localtunnel-cli) #:use-module (crates-io))

(define-public crate-localtunnel-cli-0.0.1 (c (n "localtunnel-cli") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "localtunnel") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0v5gw1bd8xjg5xnh0qy9v4g3jr0pplakjlzgnmv83az7pzkp71qk")))

(define-public crate-localtunnel-cli-0.0.2 (c (n "localtunnel-cli") (v "0.0.2") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "localtunnel") (r "^0.0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0id4v6j9bkxka2xkd9fl2lpfwghy0zpm66m4jy537k11viv5910g")))

(define-public crate-localtunnel-cli-0.0.3 (c (n "localtunnel-cli") (v "0.0.3") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "localtunnel") (r "^0.0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11k1jjnlj25wm2kmlp3dxk6ylzdq3gwgs0m9jahrfhadr26flr1j")))

