(define-module (crates-io lo ca localforage-stdweb) #:use-module (crates-io))

(define-public crate-localforage-stdweb-0.1.0 (c (n "localforage-stdweb") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.6") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (f (quote ("experimental_features_which_may_break_on_minor_version_bumps" "nightly"))) (d #t) (k 0)))) (h "1i5npw0w7bjdy2rm26w784jz1s25p2hy5qmrywhfhjfq8k0sprrz")))

(define-public crate-localforage-stdweb-0.2.0 (c (n "localforage-stdweb") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.6") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (f (quote ("experimental_features_which_may_break_on_minor_version_bumps" "nightly"))) (d #t) (k 0)))) (h "0asii371dv18nlvclcqb87fwb502yvfbpnv2cv3inixkrjzq7hz2")))

(define-public crate-localforage-stdweb-0.2.1 (c (n "localforage-stdweb") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.6") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (f (quote ("experimental_features_which_may_break_on_minor_version_bumps" "nightly"))) (d #t) (k 0)))) (h "0016paiwx5nk4sxra7cir39w6pdgfd9jb23jjn2fj4cbqbnwcxyj")))

