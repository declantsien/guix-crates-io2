(define-module (crates-io lo ca local-impl) #:use-module (crates-io))

(define-public crate-local-impl-0.1.0 (c (n "local-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1rbymnayywz8rjias9rjvlaig9gyyp2j4cjkk0q5vrgzaqxa662c")))

(define-public crate-local-impl-0.1.1 (c (n "local-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1xwcy15rpfqn6az88jqrsn3ks1xjlqcgp5z0ign5z87z9z9mnj74")))

(define-public crate-local-impl-0.1.2 (c (n "local-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "12s1f1izml0if16r31mggp987r3i3j9rcxp09g4cb6icc9bdcdjy")))

