(define-module (crates-io lo ca local_vec) #:use-module (crates-io))

(define-public crate-local_vec-0.1.0 (c (n "local_vec") (v "0.1.0") (h "0d3z5adavmi9mqjhvkmphcjf7d0606dxkybvvljk6mkzzdza5xz0")))

(define-public crate-local_vec-0.1.1 (c (n "local_vec") (v "0.1.1") (h "04kgrxadl6v8wrrv33wglgp73k6prgfmpbr5jkbxbrb4xcgv2p19")))

(define-public crate-local_vec-0.2.0 (c (n "local_vec") (v "0.2.0") (h "0imx51wz2s8yimgv5ap0yw9nmdl0gj6zrsbgac0a8c4w6lpi0pjx")))

(define-public crate-local_vec-0.2.1 (c (n "local_vec") (v "0.2.1") (h "1z3cgf40z4aasqw9n9hqyga5ycyikbpnnflzll7idviyw6hpfriv")))

(define-public crate-local_vec-0.3.0 (c (n "local_vec") (v "0.3.0") (h "18h398bq233g2ka5j8c25s215m5ch9rf3wcanbzxsvm2s6mfsh4j")))

(define-public crate-local_vec-0.4.0 (c (n "local_vec") (v "0.4.0") (h "0x5n6fjgr7r127g5kqd9hgrdkhghb312pi4dkyg5l270prfaznga")))

(define-public crate-local_vec-0.5.0 (c (n "local_vec") (v "0.5.0") (h "10c067gl87lkm0vb3sj7zdkvz9r8qk82d9j473z96yhvnvh3vg4r")))

