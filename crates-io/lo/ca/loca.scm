(define-module (crates-io lo ca loca) #:use-module (crates-io))

(define-public crate-loca-0.1.0 (c (n "loca") (v "0.1.0") (h "0ifnmv21pi3ki1kgvr5mbi8nzpni5af7n9insf3jfj0jj6xlm9jf")))

(define-public crate-loca-0.2.0 (c (n "loca") (v "0.2.0") (h "0dwn4cha5nm1vg3haimbms2v4y726fx2kxsn38i5b8lindfmv378")))

(define-public crate-loca-0.3.0 (c (n "loca") (v "0.3.0") (h "04bl7l16b2qshflqrkcbh9qps8jdixyir1w5z3ppf96m5wv26vqm")))

(define-public crate-loca-0.4.0 (c (n "loca") (v "0.4.0") (h "0j1zinnd67k9jwcbm7dqvhqrgq7gd3zc2k2f0d5wmiv3xmgf7kca")))

(define-public crate-loca-0.4.1 (c (n "loca") (v "0.4.1") (h "194amxc9nxkk06ysikislc44gnqn51f2px7gbykr4b2zksaszvbj")))

(define-public crate-loca-0.5.0 (c (n "loca") (v "0.5.0") (h "1jq9wihxa6w9iaj04c23ir856ba8rn6gq1lkwn644648jm7j0m1x") (y #t)))

(define-public crate-loca-0.5.1 (c (n "loca") (v "0.5.1") (h "0nyh4n6fzniaydw4qlnd1493897165r8gmhhkvh8q4hp2wva5cyh") (y #t)))

(define-public crate-loca-0.5.2 (c (n "loca") (v "0.5.2") (h "0ab4sf92kkng42fgsnj5qkk792xvqjrbrx72kf76szypm5vlnjff")))

(define-public crate-loca-0.5.3 (c (n "loca") (v "0.5.3") (d (list (d (n "ptr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ykzn6kf4jrj8vfijkkkq7w5fairccd2qhrdq4lsi2lkzgbqwkx0") (f (quote (("stable-rust" "ptr"))))))

(define-public crate-loca-0.5.4 (c (n "loca") (v "0.5.4") (d (list (d (n "ptr") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vbs3h3ma9f064n1mwmgq8aw7ffm3gwmki8hmzfaiw5if8aj1gqz") (f (quote (("stable-rust" "ptr"))))))

(define-public crate-loca-0.6.0 (c (n "loca") (v "0.6.0") (d (list (d (n "abort") (r "^0.1") (d #t) (k 0)) (d (n "ptr") (r "^0.1.2") (d #t) (k 0)))) (h "010ngaa0865xyqwqk7df8gmq4jrm0jgdyz0g0af5y7xrb3jx6ppr")))

(define-public crate-loca-0.6.1 (c (n "loca") (v "0.6.1") (d (list (d (n "ptr") (r "^0.1.2") (d #t) (k 0)))) (h "0412dwm08zzj912q5if3cbnhfd7mkk6rgk37ilsv0n39yrls5z3f")))

(define-public crate-loca-0.7.0 (c (n "loca") (v "0.7.0") (d (list (d (n "ptr") (r "^0.2.1") (d #t) (k 0)))) (h "0in42h7s87rqk2m6pyyjzxzaa748vq9nmhakmkwpbdja7nyihy6s")))

(define-public crate-loca-0.7.1 (c (n "loca") (v "0.7.1") (d (list (d (n "ptr") (r "^0.2.1") (d #t) (k 0)))) (h "0w6ikv02fyh4h4qw8ll1925fjkddsb0qsm0m8g30vfy8xw0l2k8n")))

(define-public crate-loca-0.7.2 (c (n "loca") (v "0.7.2") (d (list (d (n "ptr") (r "^0.2.1") (d #t) (k 0)))) (h "1xb23ckhxd2z5gfjaf7g0lic7v2xpscdq46l540wras6npf6pjpx")))

(define-public crate-loca-0.7.3 (c (n "loca") (v "0.7.3") (d (list (d (n "ptr") (r "^0.2.1") (d #t) (k 0)))) (h "1y4rx9sr2dliia3x66m7qbdhp0x0b2wy2faw2623zz3i151h753l")))

(define-public crate-loca-0.7.4 (c (n "loca") (v "0.7.4") (d (list (d (n "ptr") (r "^0.2.1") (d #t) (k 0)))) (h "09z5lnvmpixrdp9wdzshgsnn7cqa1wkd04qps7h9h2kk9ck8k4v6")))

