(define-module (crates-io lo ca locale-decoder) #:use-module (crates-io))

(define-public crate-locale-decoder-0.1.0 (c (n "locale-decoder") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jcz855qzsrz5jq4jhmssfi2q4mdsca6r5d8dw5g5rlyd36sbqwa")))

(define-public crate-locale-decoder-0.1.1 (c (n "locale-decoder") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dm4sjcnmq5k9y4gqyq4hkhspayy66ikkzll1iphf222av4p4pv9")))

