(define-module (crates-io lo ca location_history) #:use-module (crates-io))

(define-public crate-location_history-0.1.0 (c (n "location_history") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "0rd42kcjqpahm475vna5bnmvrxhg1paxa3va66j7crjl4z6yf5kd")))

(define-public crate-location_history-0.1.1 (c (n "location_history") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)))) (h "0wi1fs48ld613am1cyfsirfs9sycazdxg80z3fq4c3wacswrjq4a")))

(define-public crate-location_history-0.2.0 (c (n "location_history") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)))) (h "1axqmiii4g711krhcikg2w9nih1lh07f5xi9bs7sbqv3wrwnpnix")))

(define-public crate-location_history-0.2.1 (c (n "location_history") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "1zz472w54g4y135bad2204cgyhbrk0lhlyk3js5lmna5j9rmbsvw")))

(define-public crate-location_history-0.2.2 (c (n "location_history") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "15x7k5c8fmfc0xzhhqy7h846jcwrk01s5chzk3p17gddyhkcgw10")))

