(define-module (crates-io lo ca locale-strict) #:use-module (crates-io))

(define-public crate-locale-strict-0.1.0 (c (n "locale-strict") (v "0.1.0") (d (list (d (n "locale-codes") (r "^0.3.0") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)))) (h "03as4918i7sv929s82h3v84df4j4p34pan24vnns21ilamkfv25j")))

