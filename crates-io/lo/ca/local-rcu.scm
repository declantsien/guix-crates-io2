(define-module (crates-io lo ca local-rcu) #:use-module (crates-io))

(define-public crate-local-rcu-1.0.0 (c (n "local-rcu") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)))) (h "116fw4jwz4sfha57ixd1cmbklphrgy8hcavbvqinpq606afi21ci") (y #t)))

(define-public crate-local-rcu-1.0.1 (c (n "local-rcu") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)))) (h "0cyfsp1ylc4145117sgmyasjlss980bg48pj1glwidf5n0byy1xq") (y #t)))

(define-public crate-local-rcu-1.0.2 (c (n "local-rcu") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "04rz6zvj45l4dd85ych6ambz43sal05damqpd4pm4i17a714dmxq")))

(define-public crate-local-rcu-1.0.3 (c (n "local-rcu") (v "1.0.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0dcmmr7xrw1s36hxrd0zhwjha7zkim5zbcr3fm934n0slmi6fva1")))

(define-public crate-local-rcu-1.0.4 (c (n "local-rcu") (v "1.0.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1h3bc62nm9jfddax3r6z7gxr45j2rr597hhp4km13kxw2d7h190n")))

