(define-module (crates-io lo ca locales) #:use-module (crates-io))

(define-public crate-locales-0.0.3 (c (n "locales") (v "0.0.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 1)))) (h "0vq4kvcbwgixlwwfa7i21vgckngjs1b6j9xqgbwz258qh0vqqb3m")))

(define-public crate-locales-0.1.0 (c (n "locales") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 1)))) (h "0sxpn8aay8jpsdpxxfwb7s6hfrcyigg4f80rl2mh9i17zjq3rkcd")))

