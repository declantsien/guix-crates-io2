(define-module (crates-io lo ca location-parser) #:use-module (crates-io))

(define-public crate-location-parser-0.1.0 (c (n "location-parser") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1chmmc263r91kpc8by5v4sz562xjxh7h11a46w3xaqc4w4aw34hm")))

