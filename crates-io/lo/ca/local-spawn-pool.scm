(define-module (crates-io lo ca local-spawn-pool) #:use-module (crates-io))

(define-public crate-local-spawn-pool-0.1.0 (c (n "local-spawn-pool") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "0gsaw3821mwf3hqz5rj1rn1scblbs79khlq9mq0n03vz22my04yp")))

