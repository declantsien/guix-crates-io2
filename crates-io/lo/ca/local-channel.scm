(define-module (crates-io lo ca local-channel) #:use-module (crates-io))

(define-public crate-local-channel-0.0.1 (c (n "local-channel") (v "0.0.1") (h "08vnijnwxxgqyv3jrm9qnp5y84b49fjlx2x1r7mpp78iavd7vp05")))

(define-public crate-local-channel-0.1.0 (c (n "local-channel") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-sink") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "1whc92636khyc01qif0c3mzigmagw5ss98ma2jga1q2fpr06s5sk")))

(define-public crate-local-channel-0.1.1 (c (n "local-channel") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-sink") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "0qpiqlwvhyl50n5cyzqy7v394ixrig61xp6rby0hlkvsladydnxj")))

(define-public crate-local-channel-0.1.2 (c (n "local-channel") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-sink") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "17zy0g5wv4i0crnj03mzk18zkshmgv4mj9ail02p424my66ccik2")))

(define-public crate-local-channel-0.1.3 (c (n "local-channel") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3.7") (k 0)) (d (n "futures-sink") (r "^0.3.7") (k 0)) (d (n "futures-util") (r "^0.3.7") (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "177wqgadrlw6m7r6xxafkj58asgpgbpv1ww4gx258v2cx703wc3z")))

(define-public crate-local-channel-0.1.4 (c (n "local-channel") (v "0.1.4") (d (list (d (n "futures-core") (r "^0.3.17") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.17") (d #t) (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "0y5iw3x96j5qcpmr0120viicbzrjhnxyx2dszj7qrwg5im497970") (r "1.65")))

(define-public crate-local-channel-0.1.5 (c (n "local-channel") (v "0.1.5") (d (list (d (n "futures-core") (r "^0.3.17") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.17") (d #t) (k 0)) (d (n "local-waker") (r "^0.1") (d #t) (k 0)))) (h "1j1ywn459kl4fdmjfyljm379k40qwwscd7mqp25lppxqd5gcijxn") (r "1.65")))

