(define-module (crates-io lo ca localstoragefs) #:use-module (crates-io))

(define-public crate-localstoragefs-0.1.0 (c (n "localstoragefs") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.17") (d #t) (k 0)))) (h "1pvcwxh05a84mr1g980ny8j8crx974i9axfk3ak6wzmydgpcp9kb")))

