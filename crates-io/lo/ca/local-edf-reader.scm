(define-module (crates-io lo ca local-edf-reader) #:use-module (crates-io))

(define-public crate-local-edf-reader-0.1.0 (c (n "local-edf-reader") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "edf-reader") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.2") (d #t) (k 0)))) (h "09jndmdirkq7vwl7wdqx04v32zhi91rkih6zid0pgn5kz0ryg416")))

