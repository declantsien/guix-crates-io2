(define-module (crates-io lo ca local-encoding-ng) #:use-module (crates-io))

(define-public crate-local-encoding-ng-0.1.0 (c (n "local-encoding-ng") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winnls" "stringapiset"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k7d5a4b43w7z1zx90p2qyr1r8d4k6hh7jqsrs23sllq4hl0zbf3")))

