(define-module (crates-io lo ca locate-locale) #:use-module (crates-io))

(define-public crate-locate-locale-0.1.0 (c (n "locate-locale") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "~0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1kkckn0n1ihkwmvz5rshd55vbny5ilhx4m0q7g2xqj4rglgzlri5")))

(define-public crate-locate-locale-0.2.0 (c (n "locate-locale") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winnls"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "06a6rcn1qrkha723ppsql166gmx3vka7gwmg88a534lssfpfld98")))

