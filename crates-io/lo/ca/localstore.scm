(define-module (crates-io lo ca localstore) #:use-module (crates-io))

(define-public crate-localstore-0.1.0 (c (n "localstore") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0dm6plkkmdn16mb4fzzmg24aw4pydn96kyhbv2psqhlwcjmvjycl")))

