(define-module (crates-io lo ca localghost-macros) #:use-module (crates-io))

(define-public crate-localghost-macros-0.0.0 (c (n "localghost-macros") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w3vwp3fhnc76h4ldszmj8vykajw1w770q4z79kcamsvwjp18pff")))

(define-public crate-localghost-macros-0.1.0 (c (n "localghost-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vfhh70yvqqxkh0nyf3cn4q7rx1m7lw049c938xx6j5qm67kfr34")))

