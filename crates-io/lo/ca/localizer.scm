(define-module (crates-io lo ca localizer) #:use-module (crates-io))

(define-public crate-localizer-0.2.3 (c (n "localizer") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1lfll1kwcxhm8nnwjfxkf4f338bnkg9h1zwdw9nnnh0bg0icjfdk")))

(define-public crate-localizer-0.2.4 (c (n "localizer") (v "0.2.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0x64c4f1xg6gklvmhfm1i137mggir719sdmlypbqbsd3zn1qfffy")))

(define-public crate-localizer-0.3.0 (c (n "localizer") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "051lk686hkwrjxfhhy25x9s102gj2f3yfacqq3hmi5mb9pfk0wrq") (f (quote (("no_std"))))))

(define-public crate-localizer-0.3.5 (c (n "localizer") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (o #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (k 0)))) (h "0jgfw1pwlrbjl51zgky7k9646k2il2bjivlk6bbh02z280718279") (f (quote (("std" "toml" "serde_json/std" "lazy_static" "serde/std") ("no_std" "serde/alloc" "serde_json/alloc" "lazy_static/spin_no_std") ("format") ("default" "std" "format"))))))

