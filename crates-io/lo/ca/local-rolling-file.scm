(define-module (crates-io lo ca local-rolling-file) #:use-module (crates-io))

(define-public crate-local-rolling-file-1.1.0 (c (n "local-rolling-file") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zy1kmp21hbpsnf8lmldiz3p55np3krhaai27f2qkqam3nhr377m")))

(define-public crate-local-rolling-file-1.1.1 (c (n "local-rolling-file") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "symlink") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0hpnk7m4pz4flf89x63dva92v47vn3psapkzn295kvmq4sm72avr")))

