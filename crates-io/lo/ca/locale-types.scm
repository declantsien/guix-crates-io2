(define-module (crates-io lo ca locale-types) #:use-module (crates-io))

(define-public crate-locale-types-0.1.0 (c (n "locale-types") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qs7fvakw9sxdqwsqfxhd6arclm3wx6gdx4wrg61k3q286z27gbw")))

(define-public crate-locale-types-0.3.0 (c (n "locale-types") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d6pgfiq2a3wmd98zwx8ccdai24x1qwf9s8a4593sl7r3gyayjdj")))

(define-public crate-locale-types-0.4.0 (c (n "locale-types") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15vsbv501hfk546wpz5xrqpcqhlj797jm568vq7sy1pzkcairgh2")))

