(define-module (crates-io lo ca localoco) #:use-module (crates-io))

(define-public crate-localoco-0.1.0 (c (n "localoco") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16rn9p1s1nw1k73sg7h4zf2i0650ps680w27133k7bnwqb0mg5a5")))

(define-public crate-localoco-0.1.1 (c (n "localoco") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13n717xz5y6d1xsql8n6lgvkxyfyyxm0lnzfxlhfzng5ffxhr4jx")))

(define-public crate-localoco-0.1.2 (c (n "localoco") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1dw3z2da7dvapdysw325jfqqfby1nwjwm6jy1ajnwnpyxck63j4a")))

(define-public crate-localoco-0.1.3 (c (n "localoco") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0jgbbwpzhzxb2kicsxiw5gm7zs74rzcfljlzjxlsyi9vhy1k5rm3")))

(define-public crate-localoco-0.1.4 (c (n "localoco") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1zpvcw7a1pdinpz5a1g2jlh79hlaywmcwzgbs8pkdvaxgddkr2cq")))

(define-public crate-localoco-0.2.0 (c (n "localoco") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1n0jgjnca02272pyf18mvbz6wsk0ak8j4vnq73ickkc67m1c8pb1")))

