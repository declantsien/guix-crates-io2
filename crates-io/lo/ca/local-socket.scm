(define-module (crates-io lo ca local-socket) #:use-module (crates-io))

(define-public crate-local-socket-0.1.0 (c (n "local-socket") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "message-sink") (r "^0.1.0") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.13") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("compat"))) (d #t) (k 0)))) (h "0dcs5xqpqwzs6qfn5gwvcpkg8rd2965z7nfij15zg97dlbzzfcd3")))

