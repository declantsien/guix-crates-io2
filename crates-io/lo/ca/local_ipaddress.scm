(define-module (crates-io lo ca local_ipaddress) #:use-module (crates-io))

(define-public crate-local_ipaddress-0.1.0 (c (n "local_ipaddress") (v "0.1.0") (h "0f09qcj3v085rv5khl7lrqlc4f7fkqd7bnn7bhcmiifq3h6cjqxj")))

(define-public crate-local_ipaddress-0.1.1 (c (n "local_ipaddress") (v "0.1.1") (h "14calby2km0s9cq7906dnp9mn0nqwwp26mxlhn0igqcclqqlzn84")))

(define-public crate-local_ipaddress-0.1.2 (c (n "local_ipaddress") (v "0.1.2") (h "1cmkmys5n7b3l8yw29nmjs8ajx0xmg6h49c7wdjjipj6nskmqqgd")))

(define-public crate-local_ipaddress-0.1.3 (c (n "local_ipaddress") (v "0.1.3") (h "1adg4rsxglij22v3d2c1x41al3yad7bqxaagiv3w9ys915rh98gn")))

