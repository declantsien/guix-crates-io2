(define-module (crates-io lo ca local-pool-with-id) #:use-module (crates-io))

(define-public crate-local-pool-with-id-0.1.0 (c (n "local-pool-with-id") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0jyjxl0r3dam7krdgkn6hzs3v24lwi90i1sdwl7179d24934x4xl")))

(define-public crate-local-pool-with-id-0.1.1 (c (n "local-pool-with-id") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "03wlgnqbq5n8gcbhzk743cvpv3z4fj934chk1cd34myfprzdzzqd")))

