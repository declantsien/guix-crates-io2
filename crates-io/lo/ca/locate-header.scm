(define-module (crates-io lo ca locate-header) #:use-module (crates-io))

(define-public crate-locate-header-0.1.0 (c (n "locate-header") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)))) (h "0v9s4fmypl86r8l9vkm5vhgxhj3qhkqirakw42ihzfzc9zrkdsgk")))

(define-public crate-locate-header-0.1.1 (c (n "locate-header") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)))) (h "1awhkq4k2q0c00a4id2lwrrp0ska53lyndpc78g8qjr0c8a251m0")))

