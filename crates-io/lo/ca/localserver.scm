(define-module (crates-io lo ca localserver) #:use-module (crates-io))

(define-public crate-localserver-0.1.0 (c (n "localserver") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1zxcdxgc64044mz47p7ybp4qawdbaw9whliy6wlys00cyi4n4x1b")))

(define-public crate-localserver-0.1.1 (c (n "localserver") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1wsc512gzpbfryv796plms8inw69yhxbigpcgh1khmpnx4qp5cyy")))

