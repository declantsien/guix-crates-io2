(define-module (crates-io lo ca local-ip-addr) #:use-module (crates-io))

(define-public crate-local-ip-addr-0.1.0 (c (n "local-ip-addr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "02hca2yw5ip93c7h6kc5wgsw4lhc0xygwva00q0wf2q13z4hbbk1")))

(define-public crate-local-ip-addr-0.1.1 (c (n "local-ip-addr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "socket2") (r "^0.4.7") (d #t) (k 0)))) (h "056m7li5335rmvaig84jh6gn723ng0qh3fqwcdly77n6v8xv0pnb")))

