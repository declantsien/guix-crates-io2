(define-module (crates-io lo ca locale-codes) #:use-module (crates-io))

(define-public crate-locale-codes-0.2.0 (c (n "locale-codes") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gs32b14f7r91xk8j5g5f1zrhmbdd6sf5l7d93gk925jpcc06457")))

(define-public crate-locale-codes-0.3.0 (c (n "locale-codes") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09138389lpn7g32m6gfafcacrwl8yfx9ilsx7mv1j3545nfwrx6i")))

