(define-module (crates-io lo ca local_timestamps) #:use-module (crates-io))

(define-public crate-local_timestamps-0.1.0 (c (n "local_timestamps") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "1by7am0n8admzpg6m6k2bb3wi3axgd2l227l8lkzi3hcbbs89vcq") (y #t)))

(define-public crate-local_timestamps-0.1.1 (c (n "local_timestamps") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "0y83vxq407mpb68d4a6mm1p2n16pvf4p1m5ixv8kpra3rik08fby") (y #t)))

(define-public crate-local_timestamps-0.1.2 (c (n "local_timestamps") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "1sxabl8pfsc61myb8mz1m9plrs5gx5a4acshk3bbcirvacghgwkx") (y #t)))

(define-public crate-local_timestamps-0.1.3 (c (n "local_timestamps") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "0s3b2h8lng7f4bsii9id56qpgfgmpvr1z3ih0p8ls8srjg3ld8gf")))

(define-public crate-local_timestamps-0.1.4 (c (n "local_timestamps") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "17zqx312v4lcbsasds07sp8dvn9jxi56s9804yx97n9d8dbq7z37")))

(define-public crate-local_timestamps-0.1.5 (c (n "local_timestamps") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "0gka8xzdfjdyc1q3igdswxj9pw78gha5vyisiwp829gpax48zi7z")))

