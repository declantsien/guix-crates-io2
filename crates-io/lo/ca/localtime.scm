(define-module (crates-io lo ca localtime) #:use-module (crates-io))

(define-public crate-localtime-1.0.0 (c (n "localtime") (v "1.0.0") (h "06fvygx3wg1aiz1jbbgsjlv3bldkzv2987jr3pn0x4mh4qk1m2xm")))

(define-public crate-localtime-1.1.0 (c (n "localtime") (v "1.1.0") (h "11z55gxz79bxdw0c4srmyb5jz52rfasi4vjlg6c51saxds1djlpp")))

(define-public crate-localtime-1.2.0 (c (n "localtime") (v "1.2.0") (h "0pmihflbnv2idqxbw46g1fl0x3wjbba6603nrwpl7p2w25zy7wmj")))

(define-public crate-localtime-1.3.0 (c (n "localtime") (v "1.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "069q02zppli97iz5q6ffapds0jca9cqhpvra2cqvnd1ln21ppiki")))

(define-public crate-localtime-1.3.1 (c (n "localtime") (v "1.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "11vdz49sqqw8ydgg6vnc892z4vpym08vyx7v54r6xfmq1fg00sh1")))

