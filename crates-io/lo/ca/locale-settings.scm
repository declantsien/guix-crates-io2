(define-module (crates-io lo ca locale-settings) #:use-module (crates-io))

(define-public crate-locale-settings-0.1.0 (c (n "locale-settings") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "locale-codes") (r "^0.2") (d #t) (k 2)) (d (n "locale-types") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kiwpkinnd5l2ykc5hrq1hmi8h2jgzl1lpszc01dp4x53w3y14nz")))

(define-public crate-locale-settings-0.3.0 (c (n "locale-settings") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "locale-codes") (r "^0.3") (d #t) (k 2)) (d (n "locale-types") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05avvl0m22wjkvis87wd4skflp946prysqkpa861j1y7pfbn1kgc")))

