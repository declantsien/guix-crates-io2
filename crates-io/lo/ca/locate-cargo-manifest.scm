(define-module (crates-io lo ca locate-cargo-manifest) #:use-module (crates-io))

(define-public crate-locate-cargo-manifest-0.1.0 (c (n "locate-cargo-manifest") (v "0.1.0") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1pmqc29imvg74xsnhija4kcb4vc64jq707lniwk3c9kdrb0x51wx")))

(define-public crate-locate-cargo-manifest-0.2.0 (c (n "locate-cargo-manifest") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1qdp2xz7d2zdgdkw0v1idhph0lhk5q3z8ichxfic081varrdcr6j")))

(define-public crate-locate-cargo-manifest-0.2.1 (c (n "locate-cargo-manifest") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1zzdscsn4z6gnqyh95v201kc5q8i2js08qhr9kw4flvkx9rcbx0b") (y #t)))

(define-public crate-locate-cargo-manifest-0.2.2 (c (n "locate-cargo-manifest") (v "0.2.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0rl469d4q24dyg9dms3bp06747n3zkpyq2pmf66rxq0z8dimp66v")))

