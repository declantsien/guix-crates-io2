(define-module (crates-io lo ca locale_name_code_page) #:use-module (crates-io))

(define-public crate-locale_name_code_page-0.1.0 (c (n "locale_name_code_page") (v "0.1.0") (d (list (d (n "ahash") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19bapz05n73hah3mwwvw1hrgsw1bg1m2m22x04kfs7zzbyac8im8")))

