(define-module (crates-io lo ca localization) #:use-module (crates-io))

(define-public crate-localization-0.1.0 (c (n "localization") (v "0.1.0") (d (list (d (n "localization-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0n9vpqpxf8cvd5bdgi0625m8c5r1yh9mhjas8sh8s2bbm0xwaka0")))

(define-public crate-localization-0.1.1 (c (n "localization") (v "0.1.1") (d (list (d (n "localization-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1jk3nw5ds9li1kn854gwwgxhp1wx1dm6nzc5pkfz6hmhjab5g312")))

(define-public crate-localization-0.1.2 (c (n "localization") (v "0.1.2") (d (list (d (n "localization-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1i7h5b1snh4xs5wyvxdxz0676syknh2c72yc9mkz84x3fjaic3h6")))

(define-public crate-localization-0.1.3 (c (n "localization") (v "0.1.3") (d (list (d (n "localization-macros") (r "^0.1.3") (d #t) (k 0)))) (h "11xns28d03rxmwhq3nirddpaksyw5wjicfimw28h25cx7p020ics")))

