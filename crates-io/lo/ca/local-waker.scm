(define-module (crates-io lo ca local-waker) #:use-module (crates-io))

(define-public crate-local-waker-0.1.0 (c (n "local-waker") (v "0.1.0") (h "15wyy7hkvk66a4c5d038m6ahv8y0x4lk4fiddld1xs4kbn14xnr4")))

(define-public crate-local-waker-0.1.1 (c (n "local-waker") (v "0.1.1") (h "194dvpx4drizq4jgcawvxa9n54bv3adv1l5aqgi9rsbwwb9s5yc4")))

(define-public crate-local-waker-0.1.2 (c (n "local-waker") (v "0.1.2") (h "1yy41aivbkp6gzlwa7x60nk441ji8bbzdyyb8d2qd485xfavcblh")))

(define-public crate-local-waker-0.1.3 (c (n "local-waker") (v "0.1.3") (h "1w9zqlh18mymvb82ya0sailiy5d3wsjamaakgl70x50i6vmpckz3")))

(define-public crate-local-waker-0.1.4 (c (n "local-waker") (v "0.1.4") (h "11vlcm8q6dhdf0srkgjnwca48dn9zcz820fq20hv82ffcxy3v1sd") (r "1.65")))

