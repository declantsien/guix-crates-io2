(define-module (crates-io lo ca localization-build) #:use-module (crates-io))

(define-public crate-localization-build-0.1.0 (c (n "localization-build") (v "0.1.0") (h "1ihlvly66l50ib7cqh97kmdjmf68wqizpyfkylby736s0w6dd47m")))

(define-public crate-localization-build-0.1.2 (c (n "localization-build") (v "0.1.2") (h "1d6snqm4m56b74gjbmn5nf35brwmhc02qijqhizp1hxsr44wihck")))

(define-public crate-localization-build-0.1.3 (c (n "localization-build") (v "0.1.3") (h "1lwb409ybcx9p1qqlvbjwpb7wm9nn8nbm1hw1drx8g09l6h71svj")))

