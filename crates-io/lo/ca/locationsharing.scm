(define-module (crates-io lo ca locationsharing) #:use-module (crates-io))

(define-public crate-locationsharing-0.1.0 (c (n "locationsharing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xflc94nm3d2mrwy0yrw7rs4j41nqs2hmxd4jq1mkkfyv0v1qk2m")))

(define-public crate-locationsharing-0.1.2 (c (n "locationsharing") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gi6aicak2prch8w4m24ys7i0fs965jqlb625f6w0d6f6y19jgvw")))

