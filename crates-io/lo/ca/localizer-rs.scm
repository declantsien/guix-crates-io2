(define-module (crates-io lo ca localizer-rs) #:use-module (crates-io))

(define-public crate-localizer-rs-1.0.0 (c (n "localizer-rs") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1xn4clni1l255i4lifqiccqhq43b9rll5wh6fy04pnp2fq7cqqlm") (r "1.69")))

(define-public crate-localizer-rs-1.1.0 (c (n "localizer-rs") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1gfp297i559ik145mnz1mcw7hsgpyrcv5m9abn0md7l61hglp9ws") (r "1.69")))

(define-public crate-localizer-rs-1.1.1 (c (n "localizer-rs") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1idfvgvrnv3c55hdpf82c8m97bw6r4cwcvcrhy1afs9kb1hyf24f") (r "1.69")))

(define-public crate-localizer-rs-1.2.0 (c (n "localizer-rs") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "166np1l808n6gxs8ccgcbbjrrnvgpw1q35a7266p3zq3651iah4s") (r "1.69")))

