(define-module (crates-io lo ca locate) #:use-module (crates-io))

(define-public crate-locate-0.1.0 (c (n "locate") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1fqvnj2b54wmdv6xrz3hkx4yrwan1f67h64bc144sd0rhkh3y2qh")))

(define-public crate-locate-0.1.1 (c (n "locate") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "fstream") (r "^0.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0f7s1nzph9s970s138fd2vdydy07hr6frya4a858f1z4nzfgxrkp")))

