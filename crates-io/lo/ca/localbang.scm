(define-module (crates-io lo ca localbang) #:use-module (crates-io))

(define-public crate-localbang-0.1.0 (c (n "localbang") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "15yzz8ksc81w0i3kbv6dgnwsqvz9zxxrghqs0aagc99p0xh4s8cf")))

(define-public crate-localbang-0.1.1 (c (n "localbang") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1y54j9kkmiwymnzp9dlp8hvqy9f806jk3b3flni4vkg335xz67cx")))

(define-public crate-localbang-0.2.0 (c (n "localbang") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "10vn42db6lz6dk83vpn0pfdn4ys9vbvb8qrsrxjdrmw58ij50n6j")))

(define-public crate-localbang-0.3.0 (c (n "localbang") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "06lwl8mpc6pk3050xbixjfxypqn164pqdxxx487rxxxcjbcjhhqj")))

(define-public crate-localbang-0.3.1 (c (n "localbang") (v "0.3.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0np2q8xvndvqxar8wpd7qzg41n7izhx2ifscyj5jxrsxmjmcm6v4")))

(define-public crate-localbang-0.3.2 (c (n "localbang") (v "0.3.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0q8p9blnirq8w4nzmv3lil6d4nj2jy5nn4aq6vdilmqk4j5asbh6")))

(define-public crate-localbang-0.3.3 (c (n "localbang") (v "0.3.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1x3w4xr0gfq41i5j79ikxbdck9kw4xlw8klviyr8h14avji0z3ni")))

(define-public crate-localbang-0.4.0 (c (n "localbang") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "18iv89xw51dgpadcn8c97dp36sk8c8j9ssnvc47kxy7bnb11s5fn")))

