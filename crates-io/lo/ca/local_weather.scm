(define-module (crates-io lo ca local_weather) #:use-module (crates-io))

(define-public crate-local_weather-0.1.0 (c (n "local_weather") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r2iw4v4vzry0bcm92y2pabk19q3ndfzi3gmz3cqyaxj0s03spfj")))

(define-public crate-local_weather-0.1.1 (c (n "local_weather") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vjqqwqrl9pq3aj67bfzfx2a9qyf398f1zkh8l9hbahv5cm9zqjz")))

