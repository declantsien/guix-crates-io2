(define-module (crates-io lo ca local-sync) #:use-module (crates-io))

(define-public crate-local-sync-0.0.1 (c (n "local-sync") (v "0.0.1") (d (list (d (n "frosty") (r "^0.0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0ymsa01sfy6yirbmwf7jw5kjxq1aqs330xdhsn6330n0r77bnbd8") (y #t)))

(define-public crate-local-sync-0.0.2 (c (n "local-sync") (v "0.0.2") (d (list (d (n "frosty") (r "^0.0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0kd39czplz6qz7xzfjpsha7xas5q38qvbi1igvrqvc7zbjp9m05q") (y #t)))

(define-public crate-local-sync-0.0.3 (c (n "local-sync") (v "0.0.3") (d (list (d (n "frosty") (r "^0.0.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "02j3mddxdca37axmchy8lzzzg0v52r34zs5i4zpnr79g4xyp4y3p") (y #t)))

(define-public crate-local-sync-0.0.4 (c (n "local-sync") (v "0.0.4") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "monoio") (r "^0.0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "11zskzy4qhbqn76ys9sddzrhv6m16r9wnn0qcgj1vpgxb8bvrgmv") (y #t)))

(define-public crate-local-sync-0.0.5 (c (n "local-sync") (v "0.0.5") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "monoio") (r "^0.0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "1zvycqjz2d0nddhb5y96ar28aig0hmz0g5bw9x2mxllqfbg89bjn")))

(define-public crate-local-sync-0.1.0 (c (n "local-sync") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "monoio") (r "^0.1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ybycsrwx38vlsr70vl9qfl57fd1h22vvjclh6r4mxvmkw1gqwaj")))

(define-public crate-local-sync-0.1.1 (c (n "local-sync") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-sink") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "monoio") (r "^0.1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1vsh02l5vmv7g1fv9w65dlpmm2jmg9j7i109i4xrznbds4zcqr9q")))

