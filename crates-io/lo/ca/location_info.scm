(define-module (crates-io lo ca location_info) #:use-module (crates-io))

(define-public crate-location_info-0.1.0-alpha (c (n "location_info") (v "0.1.0-alpha") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "14mkz33k1k13ys9lycravkzbic9ac3qdxp5qv0r1g9yr8620vn5w")))

(define-public crate-location_info-0.1.0-alpha-1 (c (n "location_info") (v "0.1.0-alpha-1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i8vzb5m00by6iwbzzwlmihld4xgvh6jhfia9rrgrm3kb2gyg574")))

