(define-module (crates-io lo ca localnative_cli) #:use-module (crates-io))

(define-public crate-localnative_cli-0.0.1 (c (n "localnative_cli") (v "0.0.1") (d (list (d (n "localnative_core") (r "^0.0.1") (d #t) (k 0)))) (h "1dd12vbm89dkf2dkcilv174ccpn177vvm2yrx4m3z2dwixag7vm4")))

(define-public crate-localnative_cli-0.0.2 (c (n "localnative_cli") (v "0.0.2") (d (list (d (n "localnative_core") (r "^0.0.2") (d #t) (k 0)))) (h "0pj2glrv4k0ldhrxwp8z4fl8qlb5snmfazaizc13i2d40sjcp2ag")))

(define-public crate-localnative_cli-0.1.0 (c (n "localnative_cli") (v "0.1.0") (d (list (d (n "localnative_core") (r "^0.1.0") (d #t) (k 0)))) (h "0v4y3i60yi2z5p1r5n67gh7j35dnm1b2llkbrhvmy2zkk0ix5dhw")))

(define-public crate-localnative_cli-0.1.1 (c (n "localnative_cli") (v "0.1.1") (d (list (d (n "localnative_core") (r "^0.1.1") (d #t) (k 0)))) (h "1yddc91ais7bbzaqb7q14mngqgkmjzzgvkijwivfmcj5p7bkn15d")))

(define-public crate-localnative_cli-0.1.2 (c (n "localnative_cli") (v "0.1.2") (d (list (d (n "localnative_core") (r "^0.1.2") (d #t) (k 0)))) (h "0khcygwdmag4284k8szyscjbkgnk7651mvppzpsqz7wrdnirnkpn")))

(define-public crate-localnative_cli-0.1.3 (c (n "localnative_cli") (v "0.1.3") (d (list (d (n "localnative_core") (r "^0.1.3") (d #t) (k 0)))) (h "00h93k36p47lh014qlxa3gqvcd0a19b24i97wfsm85d1l4sp5awf")))

(define-public crate-localnative_cli-0.1.4 (c (n "localnative_cli") (v "0.1.4") (d (list (d (n "localnative_core") (r "^0.1.4") (d #t) (k 0)))) (h "0hqifzra3b188bpanapsh0ldy20y078s9gvc3jq4g4dn5ah6mb2j")))

(define-public crate-localnative_cli-0.1.5 (c (n "localnative_cli") (v "0.1.5") (d (list (d (n "localnative_core") (r "^0.1.5") (d #t) (k 0)))) (h "0n6ysh1xs55l6b2ssfmxsan7rhygs81lq11c29z37inqsl2kc672")))

(define-public crate-localnative_cli-0.2.0 (c (n "localnative_cli") (v "0.2.0") (d (list (d (n "localnative_core") (r "^0.2.0") (d #t) (k 0)))) (h "08q6qmh7qgkjd2isddc75653ysahfq6hp7m10vk8m2wyd4xv3q7w")))

(define-public crate-localnative_cli-0.2.4 (c (n "localnative_cli") (v "0.2.4") (d (list (d (n "localnative_core") (r "^0.2.4") (d #t) (k 0)))) (h "12kvf7q227ahgknig5nzl2pd7m9pkz5p1ai10y5x6jr65k31mq9p")))

(define-public crate-localnative_cli-0.3.0 (c (n "localnative_cli") (v "0.3.0") (d (list (d (n "localnative_core") (r "^0.3.0") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.0") (d #t) (k 0)))) (h "1spgd0wd5cr4x5m3j573609iw742v9gpwh16r0jz3nx5irc49y28")))

(define-public crate-localnative_cli-0.3.1 (c (n "localnative_cli") (v "0.3.1") (d (list (d (n "localnative_core") (r "^0.3.1") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.1") (d #t) (k 0)))) (h "0zam278p869139p44r53vjdblhwrlmcgipjrk3342pzss6kqjphp")))

(define-public crate-localnative_cli-0.3.2 (c (n "localnative_cli") (v "0.3.2") (d (list (d (n "localnative_core") (r "^0.3.2") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.2") (d #t) (k 0)))) (h "124jxp1hb0af53hgsd1waav0c3i5ajir99pyykkql9rscxs55n20")))

(define-public crate-localnative_cli-0.3.3 (c (n "localnative_cli") (v "0.3.3") (d (list (d (n "localnative_core") (r "^0.3.3") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.3") (d #t) (k 0)))) (h "1j6rfyh692m8p2yizg29c2mlpnh5fqbfvdjqp4l26kqx5snd4c60")))

(define-public crate-localnative_cli-0.3.4 (c (n "localnative_cli") (v "0.3.4") (d (list (d (n "localnative_core") (r "^0.3.4") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.4") (d #t) (k 0)))) (h "156xn2fdgib9m6cyssk7p896856rr4znjzbljd0nk5jxn3vvilr1")))

(define-public crate-localnative_cli-0.3.5 (c (n "localnative_cli") (v "0.3.5") (d (list (d (n "localnative_core") (r "^0.3.5") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.5") (d #t) (k 0)))) (h "11i4b0kiksn8wnjkijkpkq12sv4aldi8927gl62sjlwmq5j1bdn0")))

(define-public crate-localnative_cli-0.3.7 (c (n "localnative_cli") (v "0.3.7") (d (list (d (n "localnative_core") (r "^0.3.7") (d #t) (k 0)) (d (n "localnative_ssb") (r "^0.3.7") (d #t) (k 0)))) (h "0694qh2w0hpgczhz3y4c6gyzk6vhqqqx367cr78v01f2nwc019qg")))

