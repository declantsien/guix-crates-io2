(define-module (crates-io lo ca local-ex) #:use-module (crates-io))

(define-public crate-local-ex-0.1.0 (c (n "local-ex") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1") (d #t) (k 0)))) (h "1l31p8h1bdvnwiyzj5vynas5714vjprv5lnkpx11aagwjb1971ab")))

