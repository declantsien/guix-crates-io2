(define-module (crates-io lo ca local-type-alias) #:use-module (crates-io))

(define-public crate-local-type-alias-0.1.0 (c (n "local-type-alias") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gcqjg6rq28kxpxi6gqin5bhsh60v3np07zqlw1r5ycb2nw7na93") (r "1.56")))

(define-public crate-local-type-alias-0.1.1 (c (n "local-type-alias") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bfgx8ky9l4ssqpxnwhssnzkn4pcpyd44hyahnf7ivw48pnlzyg1") (y #t) (r "1.56")))

(define-public crate-local-type-alias-0.1.2 (c (n "local-type-alias") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1f0dq536dbf095hk6lkw1p7gzj6yhk1zrhfnpf0s63x3d299abbj") (r "1.56")))

(define-public crate-local-type-alias-0.1.3 (c (n "local-type-alias") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18zqcrh4xl3pdhkmk4dhvvlv0xm7jgza6s3s8y7wx5npw7vadi6j") (r "1.56")))

(define-public crate-local-type-alias-0.1.4 (c (n "local-type-alias") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jjrc9wjbmcg1s1wdjkqrrz3falj8fryvpjc55qzd1h1g5dxr9l3") (r "1.56")))

(define-public crate-local-type-alias-0.1.5 (c (n "local-type-alias") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jy61aqdi4sl2g70ivchp0dba9pi30a85vdq4bbfb74if7y9nkrc") (r "1.56")))

