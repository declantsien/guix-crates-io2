(define-module (crates-io lo ca local_file_cache) #:use-module (crates-io))

(define-public crate-local_file_cache-0.1.1-beta.1 (c (n "local_file_cache") (v "0.1.1-beta.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "0mcifnw1bbwgf4v099kn59pk3pqggmk26avf24gkwpi4nshd1hh7")))

(define-public crate-local_file_cache-0.1.1 (c (n "local_file_cache") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "1hka6m48lcdivvf8g9jcwkp1ryfp0i81gz06hc5fr38340kwypig")))

(define-public crate-local_file_cache-0.1.2-beta.1 (c (n "local_file_cache") (v "0.1.2-beta.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "1vpjab7gvq50rcw38iq8mh5cq6sjk463ahx2nxf8gm9ji107a386")))

(define-public crate-local_file_cache-0.1.2-beta.2 (c (n "local_file_cache") (v "0.1.2-beta.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "01cmhvl7l427qh8x8ny406srx0rl3bhhysq3bjn938778qpiyiks")))

(define-public crate-local_file_cache-0.1.2-beta.3 (c (n "local_file_cache") (v "0.1.2-beta.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "14xyn73hy177p8dnbgnil7s9n11xwai6a47h59j7d7as64wfwxnn")))

(define-public crate-local_file_cache-0.1.2 (c (n "local_file_cache") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "1n58k9ws85v4xl0g2hlpm04mg26gq8cfarc7f5wgccyy0h4nn7nh")))

(define-public crate-local_file_cache-0.1.3-beta.1 (c (n "local_file_cache") (v "0.1.3-beta.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "0dbgj9pbciq5qsh218klgj3rrb4wm7223064dhpy5gbq7lhvb47w")))

(define-public crate-local_file_cache-0.1.3 (c (n "local_file_cache") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)))) (h "013byyqs39zkpz9apxjz9ffxrzc36l36dqia5qcmxrlacpwbvahd")))

(define-public crate-local_file_cache-0.1.4-beta.1 (c (n "local_file_cache") (v "0.1.4-beta.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0ar45ncx9bqa8higz5g9azpw7jmp33f3yh2cwgf34l93l8sllz9p")))

(define-public crate-local_file_cache-0.1.4 (c (n "local_file_cache") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "06g23aw00h0zj3xhj8y2088lhvn19wb0m6h86qs5pmqn004jx17y")))

