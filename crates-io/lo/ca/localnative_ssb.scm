(define-module (crates-io lo ca localnative_ssb) #:use-module (crates-io))

(define-public crate-localnative_ssb-0.3.0 (c (n "localnative_ssb") (v "0.3.0") (d (list (d (n "localnative_core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "1xs2lzhxbdj7f95q50r6s4015bq2yh5rv1bfzl3vp82p9mf8kq8b")))

(define-public crate-localnative_ssb-0.3.1 (c (n "localnative_ssb") (v "0.3.1") (d (list (d (n "localnative_core") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "05b7nrxq7fpdzdb4cm698jk5s2hsrvy3hhbzv63bcj5ycpqh0nfd")))

(define-public crate-localnative_ssb-0.3.2 (c (n "localnative_ssb") (v "0.3.2") (d (list (d (n "localnative_core") (r "^0.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)))) (h "0dp6m34m2kjvwrjxz3nhmcbwxhajb1mdb14fxglvy840n622iy2c")))

(define-public crate-localnative_ssb-0.3.3 (c (n "localnative_ssb") (v "0.3.3") (d (list (d (n "localnative_core") (r "^0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)))) (h "1r0306p76yyxc1xkikmnn0rmv9f6257gwwz73kxi9bgrhgapky71")))

(define-public crate-localnative_ssb-0.3.4 (c (n "localnative_ssb") (v "0.3.4") (d (list (d (n "localnative_core") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)))) (h "1lnqdhzl8l3bf9nwbmwfyp3ciixp2y8pbxvmaq2qsg3njg6qm33y")))

(define-public crate-localnative_ssb-0.3.5 (c (n "localnative_ssb") (v "0.3.5") (d (list (d (n "localnative_core") (r "^0.3.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)))) (h "07khgzskq4psmhsrlqiy05q3fwfiaprv80qpm9p7cxxk4s72cglg")))

(define-public crate-localnative_ssb-0.3.7 (c (n "localnative_ssb") (v "0.3.7") (d (list (d (n "localnative_core") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "^0.1.41") (d #t) (k 2)))) (h "1wh0h7zaww5b9dsv6rl6gkd62y5lm0vdggf3aqais6ckw41g5ng0")))

