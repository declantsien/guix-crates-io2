(define-module (crates-io lo ca local-encoding) #:use-module (crates-io))

(define-public crate-local-encoding-0.1.0 (c (n "local-encoding") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "skeptic") (r "^0.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.2.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0xwkfi7gp73fmamrlmfvhl6szmac51b8ypp2bkibhfzn9pgi0mq1")))

(define-public crate-local-encoding-0.2.0 (c (n "local-encoding") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "skeptic") (r "^0.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.2.7") (d #t) (t "cfg(windows)") (k 0)))) (h "0rhsb8x10i0959ry38da3j1avnmihqwmyygr7wpy8ypz747v5kp1")))

