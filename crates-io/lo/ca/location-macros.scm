(define-module (crates-io lo ca location-macros) #:use-module (crates-io))

(define-public crate-location-macros-0.1.0 (c (n "location-macros") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.4") (f (quote ("parsing"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "019gvrr5ap7y4530vw9hwaicl09xfka4zhzp6a6yg3j8km32h0bm")))

(define-public crate-location-macros-0.1.1 (c (n "location-macros") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.4") (f (quote ("parsing"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03kjkpp3a2n9nmhpw449xp4y1jgk5d7f96m80a3vnv18rfrwxbhz")))

(define-public crate-location-macros-0.1.2 (c (n "location-macros") (v "0.1.2") (d (list (d (n "syn") (r "^2.0.4") (f (quote ("parsing"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04wb5j14yvai7xazrqp4v40g98rsjfmbky6vbakflyalqf9wq0by")))

