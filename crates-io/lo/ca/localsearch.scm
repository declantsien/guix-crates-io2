(define-module (crates-io lo ca localsearch) #:use-module (crates-io))

(define-public crate-localsearch-0.7.0 (c (n "localsearch") (v "0.7.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1vq04w904k2i3k6asc3q4zlw95bznq0nfk02gza465z3f9rlq0h6")))

(define-public crate-localsearch-0.8.0 (c (n "localsearch") (v "0.8.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1sch0pf3bgvy9p7d47fhppzsljn8mbkad514cca10cl0fqndi2jz")))

(define-public crate-localsearch-0.9.0 (c (n "localsearch") (v "0.9.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "1h4gz4b51wff60splpwvrqlykc5wrsal6wh703vnc6nq8d29cc0m")))

(define-public crate-localsearch-0.10.0 (c (n "localsearch") (v "0.10.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "0bqgp83ashql9wqq2yzyrhjmr2y8h8z1p5zpa2dnsfvvnkbw6n3h")))

(define-public crate-localsearch-0.11.0 (c (n "localsearch") (v "0.11.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "auto_impl") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "0i4r62hp9xmpsg6jvjw70nm2ffi1vzxg6pzqb7kj8ibl5bckjgk4")))

(define-public crate-localsearch-0.12.0 (c (n "localsearch") (v "0.12.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "auto_impl") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "0qi49s2gzzzqx4fk5qyg631zg2rr47kl2bk383gcv3yrdm4ml321")))

(define-public crate-localsearch-0.12.1 (c (n "localsearch") (v "0.12.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "auto_impl") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)) (d (n "web-time") (r "^1.1.0") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "0jqk0i0harz1h8bn4g357pz4jqczpvjckw5ziyyriwd4dhj973qc")))

