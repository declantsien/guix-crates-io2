(define-module (crates-io lo cd locdev) #:use-module (crates-io))

(define-public crate-locdev-0.1.0 (c (n "locdev") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "04mqcy8chfvr5s56jl95hxr78kjprsfa5kd67ilgmy7c3y3pi738")))

(define-public crate-locdev-0.1.1 (c (n "locdev") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "12i8gb3lwl75aidxx2gvx4j5l8r8rb018jwgflmxzddpis2idsz1")))

(define-public crate-locdev-0.1.2 (c (n "locdev") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0albacx3xv6w7s32p2165ffwjf2f8fhkcbrkx1xdwq3wdp2gfxl7")))

