(define-module (crates-io lo g2 log2graph) #:use-module (crates-io))

(define-public crate-log2graph-0.1.0 (c (n "log2graph") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "0f83fq4mji48h5bykk118mwz8v9k4skzslsp3vl1mmy0l5rrykcr")))

(define-public crate-log2graph-0.1.1 (c (n "log2graph") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "15wh0rr3yvr12w5r39g452yqs08hficbzi7sxpw2gw5qyfw7ak4m")))

