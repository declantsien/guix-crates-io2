(define-module (crates-io lo g2 log2) #:use-module (crates-io))

(define-public crate-log2-0.1.0 (c (n "log2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "1p5p48b6qkwz2jni8siqvac2r1dvaj1d8kini19chkgicip0b4im")))

(define-public crate-log2-0.1.1 (c (n "log2") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "0xsgwnjgh7q3p7chdqrx32afdxwh65y0hsrdwicih5z7j3yyx4k5")))

(define-public crate-log2-0.1.2 (c (n "log2") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "1gksja7gy51g0nmz49jrl0zkxap819c1g2bw231hixswwamg9g6k")))

(define-public crate-log2-0.1.3 (c (n "log2") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "1417z2b9n8ashxbbgx04yym7hcqd2bm0iy604jiir25fgzjpsd9x")))

(define-public crate-log2-0.1.4 (c (n "log2") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "15flkh4c8bggwlhmi9lywlga43qgjxycxpcdycr49j0zzk7r8fx1")))

(define-public crate-log2-0.1.5 (c (n "log2") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "09srd8fv3hgb4y15x4rndlvkjyifaif4dc63aybxy4k2fmz59rs3")))

(define-public crate-log2-0.1.6 (c (n "log2") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "0ig540i792bzn8ir4hq56smk95wcf1zy5p0mba75834867k7wyam")))

(define-public crate-log2-0.1.7 (c (n "log2") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "1bqhpvvpx70j2bvx8fnxlpv6lz0a033m567gqgsidi50ymm3vv91")))

(define-public crate-log2-0.1.8 (c (n "log2") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "015jiww32x366pb5n4kd00w4kdy0f6fnpk7gj7k0gxw8b07769w5")))

(define-public crate-log2-0.1.9 (c (n "log2") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "06c0c86v26s6c87yx6395bb4sjs41fgj0g02xy3zbl20mn0fhz27")))

(define-public crate-log2-0.1.10 (c (n "log2") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "0bkww9d8lq53dv52jcqsg3xyvcqimj816rl7axh9hvnbqsb4ihwd")))

(define-public crate-log2-0.1.11 (c (n "log2") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (f (quote ("std"))) (d #t) (k 0)))) (h "0rhlnxab6pjlsav06jvazd42nh8jvakc43w9i78m7jj0xik6lrb3")))

