(define-module (crates-io lo rr lorry) #:use-module (crates-io))

(define-public crate-lorry-0.1.0 (c (n "lorry") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1j20rcxb3gsdhf2qwp28v768imf9cbp22jjwvmxk0zjacd67n6i9")))

(define-public crate-lorry-0.1.1 (c (n "lorry") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0r6jx09fzkw0m6c0804ycb6drajk32gs1h0mdzzbm6n6xcra299d")))

(define-public crate-lorry-0.1.2 (c (n "lorry") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "04slri3p9j4vlp0m4p4b118nfcs5v9c5g5f67i4sj8fc4kqvrfkh")))

(define-public crate-lorry-0.1.3 (c (n "lorry") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "09zf6lqyvrzdngn0vpfdixrm2p35l33kdcsjma4hrx3m3f7jj55s")))

(define-public crate-lorry-0.1.4 (c (n "lorry") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "16jccr1f3d8za62iy2zr3sj7pf1nxzg3gnvmdf5js5cqzhhkjqkf")))

(define-public crate-lorry-0.1.5 (c (n "lorry") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0rv6q2prm4qm4dg94krc34rpgnaxnxl1b5p19xqqz80f0p7dha9m")))

(define-public crate-lorry-0.1.6 (c (n "lorry") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "1ab4z6rarn8mn8i39vfxr0pqvqa0jqp88fci7c97srck2fzl8nv9")))

(define-public crate-lorry-0.1.7 (c (n "lorry") (v "0.1.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0b6yczsjgg8laljcb8rm9hxw794wps6sh8nxcs9fnz6npij1x28c")))

(define-public crate-lorry-0.1.8 (c (n "lorry") (v "0.1.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "029gd3jfs83yhd9n24nalfkin211qgk1g55m00qzqncnm25hv84w")))

