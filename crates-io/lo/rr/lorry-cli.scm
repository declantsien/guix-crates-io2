(define-module (crates-io lo rr lorry-cli) #:use-module (crates-io))

(define-public crate-lorry-cli-0.1.4 (c (n "lorry-cli") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "lorry") (r "^0.1.5") (d #t) (k 1)))) (h "0qpg4yvis6gna7f3xas0pl62wbifj071fvjg9rfqgndd3fs06ivz")))

(define-public crate-lorry-cli-0.1.5 (c (n "lorry-cli") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "lorry") (r "^0.1.6") (d #t) (k 1)))) (h "1pw6scxcpdc9zq130xswr7299h30b55awnjv4l3an8rfnbs4p4cd")))

