(define-module (crates-io lo rd lordeckcodes) #:use-module (crates-io))

(define-public crate-lordeckcodes-0.1.0 (c (n "lordeckcodes") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "16acpa355ck1ld6jwl70n02cp78ky4y33g1qq5dh4ghcjrbl1wz0")))

(define-public crate-lordeckcodes-0.1.1 (c (n "lordeckcodes") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "0wp8rxlrrz4n2fgxpn2gxysr8vqlndzzsvnrfa9wr5bdwb0wndb8")))

(define-public crate-lordeckcodes-0.2.0 (c (n "lordeckcodes") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "0am3pqwbm2w9swhsja3qkbq70gcc8nfv064y03hh80a0kpbinrp9") (f (quote (("default" "serde"))))))

(define-public crate-lordeckcodes-0.3.0 (c (n "lordeckcodes") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "09kxgm9h8bf3ld6l64790s8zwyiq4wl7cs8dh3x2bj88pq66h3gc") (f (quote (("default" "serde"))))))

(define-public crate-lordeckcodes-0.4.0 (c (n "lordeckcodes") (v "0.4.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varint") (r "^0.9") (d #t) (k 0)))) (h "1g35h0sr5z1wj4jci1l3lcwz3k9i2dkjdrwq8xpzmi8913rmffil") (f (quote (("default" "serde"))))))

(define-public crate-lordeckcodes-1.0.0 (c (n "lordeckcodes") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varint") (r "^0.9") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2") (d #t) (k 0)))) (h "02qdpaqlwgk1nxp5n7llbpaf5igmrp1w66pckzfzgb3rbdh6jbih") (f (quote (("default" "serde"))))))

(define-public crate-lordeckcodes-1.1.0 (c (n "lordeckcodes") (v "1.1.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2") (d #t) (k 0)))) (h "0r034af1bfjjbd82xy0gap4mzc0wm5mgh4g41f1h4qvcj9v9hscn") (f (quote (("default" "serde"))))))

