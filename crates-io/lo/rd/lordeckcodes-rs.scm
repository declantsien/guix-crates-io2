(define-module (crates-io lo rd lordeckcodes-rs) #:use-module (crates-io))

(define-public crate-lordeckcodes-rs-0.1.0 (c (n "lordeckcodes-rs") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "13m2x2krdrqyqcz537v8j5j0gymsjyr06l06a790gyra9vdagll1") (y #t)))

