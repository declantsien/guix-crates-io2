(define-module (crates-io lo li loli_lib_dev) #:use-module (crates-io))

(define-public crate-loli_lib_dev-0.0.1 (c (n "loli_lib_dev") (v "0.0.1") (h "04xnhqrgd3nrag0ia3jgi2dgzqxkbdavglph5sw3qlfxh9wqiwba") (r "1.68.0")))

(define-public crate-loli_lib_dev-0.1.1 (c (n "loli_lib_dev") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0wlckm7lr80africrxkw0a13jvsg37zw4h904wl9pn1bvdn735pj") (r "1.68.0")))

