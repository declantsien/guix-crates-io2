(define-module (crates-io lo li lolicon) #:use-module (crates-io))

(define-public crate-lolicon-0.1.0 (c (n "lolicon") (v "0.1.0") (d (list (d (n "lolicon_api") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1nsxxc88gk87agcrzz1xiv9fal2fcmx795dk7v98p71rjh253c19") (y #t)))

(define-public crate-lolicon-0.1.1 (c (n "lolicon") (v "0.1.1") (d (list (d (n "lolicon_api") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1ppcvl0rp9kwwawxrizh922csiaims4rziz0vm19662wppvi6s72")))

(define-public crate-lolicon-0.2.0 (c (n "lolicon") (v "0.2.0") (d (list (d (n "lolicon_api") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1mjag7c6imrh0wiy07967671hahc1w5g3zq0n4r2cmjkj0f5ipd3") (y #t)))

(define-public crate-lolicon-0.2.2 (c (n "lolicon") (v "0.2.2") (d (list (d (n "lolicon_api") (r "^0.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0kzm3jyy0r0385j9q7grxj2nx6fg8395wy10yrfhjd1c3nxblprz") (y #t)))

(define-public crate-lolicon-0.2.3 (c (n "lolicon") (v "0.2.3") (d (list (d (n "lolicon_api") (r "^0.3.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0slipnn5zhpihw46i4899bj145lm32jpph1k0zh30yczfipprins") (y #t)))

(define-public crate-lolicon-0.2.4 (c (n "lolicon") (v "0.2.4") (d (list (d (n "lolicon_api") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1m50ddmdywg6bmisqp54vcx6g5cgrvf324sbwl1crmwx0vk89vis")))

(define-public crate-lolicon-0.3.0 (c (n "lolicon") (v "0.3.0") (d (list (d (n "lolicon_api") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "08rjhxw5gy895ypwzvqfkpaa1gpgnxiy66kh1fwa3rj5r0y3bysm") (y #t)))

(define-public crate-lolicon-0.3.1 (c (n "lolicon") (v "0.3.1") (d (list (d (n "lolicon_api") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1bn62yqfw2vx2h68hspdd71argj3b09jzp3blmi23x1gxp8j9gwc")))

(define-public crate-lolicon-0.4.0 (c (n "lolicon") (v "0.4.0") (d (list (d (n "lolicon_api") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "18xxg763lqgxz3zxcp9fx4cwi4f39r834anx668jidfk4bpba0x7")))

