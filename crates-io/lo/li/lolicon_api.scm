(define-module (crates-io lo li lolicon_api) #:use-module (crates-io))

(define-public crate-lolicon_api-0.1.0 (c (n "lolicon_api") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0qp2zb4dfkd7wn1xn0l4wckg5pqbr56hyxcrqzlm73djhj0nql6x")))

(define-public crate-lolicon_api-0.1.1 (c (n "lolicon_api") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "19d01dmah50aslzibr0ilzsjxm5df2kswajis6amvwihqn3i9rjq")))

(define-public crate-lolicon_api-0.2.0 (c (n "lolicon_api") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0n1x2hbg35bflz1blqggvxi01k067ka5bhmavxbyhybvcnp9gp77")))

(define-public crate-lolicon_api-0.2.1 (c (n "lolicon_api") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1bl257yh51bpvkr67qlzbhk73k5biac4skdjfv9byn3mfcml160c")))

(define-public crate-lolicon_api-0.3.1 (c (n "lolicon_api") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0pri593722l121hfz0ncj4dyxjnpx29l941g920xpc6v8zq4mxhl")))

(define-public crate-lolicon_api-0.3.2 (c (n "lolicon_api") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0qml0ds959g6b16q2kx1kibn7p7847h12h1hlw24rpjn2f0grdbw")))

(define-public crate-lolicon_api-0.3.3 (c (n "lolicon_api") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0apq9rnqbmvq7aa10cvakr7v02gfn2kcmnzmv28ivw7ky01sl1y6")))

(define-public crate-lolicon_api-1.0.0 (c (n "lolicon_api") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "14062y09zy5iky9kl6mqlg261aivxacphqssy74zfm879z6sw1db")))

(define-public crate-lolicon_api-1.1.0 (c (n "lolicon_api") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1v389z7pp3zxb9ywhabr5maiyvl4756gcs19d7fcs6pw7f7wfarb")))

(define-public crate-lolicon_api-1.2.0 (c (n "lolicon_api") (v "1.2.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0q84n727xa7ph8nnf3rhad60m220269rra1rymwmf5x0phapqw58")))

(define-public crate-lolicon_api-1.3.0 (c (n "lolicon_api") (v "1.3.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "079grnz1hk4yg0ms7z8jb3xn2p9ngf01qq1q5r6akxgzcwv9x9zy")))

(define-public crate-lolicon_api-1.3.1 (c (n "lolicon_api") (v "1.3.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1mbzmvdf3g7dws89kxfk1x3b541wxv2a0vq4c4jc2vxg6klzw67k")))

(define-public crate-lolicon_api-1.4.0 (c (n "lolicon_api") (v "1.4.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1avgxs8rzzdq0ww359mk8lcz0cahzppkrqr7i6siwisrcq8137cd") (y #t)))

(define-public crate-lolicon_api-1.4.1 (c (n "lolicon_api") (v "1.4.1") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "16zv2z9n50ip8f07d2n6axrndz5rkyns046pkphmm2gz35fm9lf7")))

(define-public crate-lolicon_api-1.4.2 (c (n "lolicon_api") (v "1.4.2") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1irqb6fbcg37mj3yqmsz8bqxrm8hvmi940kgw0xaw31hcj9299kh")))

