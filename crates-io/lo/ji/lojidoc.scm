(define-module (crates-io lo ji lojidoc) #:use-module (crates-io))

(define-public crate-lojidoc-0.1.0 (c (n "lojidoc") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "08a0dbhcdf58w5n27hvyf5d280lmf44ppxymz1kjzkmsxqkax9fl")))

(define-public crate-lojidoc-0.1.1 (c (n "lojidoc") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0k0zmsb28il8q92f08zw9lniwad7b65k17xjnjvpqpyxyav003dy")))

(define-public crate-lojidoc-0.2.0 (c (n "lojidoc") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "mdbook") (r "^0.2.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1rz5y20n38avgzwkf0q47jmk2rlll70cz6pmgb0b2j1kgih50q9v")))

(define-public crate-lojidoc-0.3.0 (c (n "lojidoc") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (k 0)) (d (n "mdbook") (r "^0.2.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "046pz53d3xfqnr4mpciwzpcrx5pabnasn6993rk6dr387115qw2q")))

(define-public crate-lojidoc-0.3.1 (c (n "lojidoc") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (k 0)) (d (n "mdbook") (r "^0.2.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "075wyj4w7vhbb0nq30xbrv7j81ax2babjx95p4gpcvxg8mqq6y2c")))

