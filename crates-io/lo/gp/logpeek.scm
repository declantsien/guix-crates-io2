(define-module (crates-io lo gp logpeek) #:use-module (crates-io))

(define-public crate-logpeek-0.1.0 (c (n "logpeek") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("formatting" "local-offset"))) (d #t) (k 0)))) (h "14hzlv74wih551p8q6zqn05y8ph594qg90zqqa8b97rnd3mnzam6")))

(define-public crate-logpeek-0.2.0 (c (n "logpeek") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("formatting" "local-offset"))) (d #t) (k 0)))) (h "125y7kxrlbvc6mpa36wjjsvysgks3qkwvx77r2ha720qjzwni04w")))

(define-public crate-logpeek-0.2.1 (c (n "logpeek") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("formatting" "local-offset"))) (d #t) (k 0)))) (h "1izwyhfkivynz1y7kh4am9162xzp7lx41m1x3cmx1s429lszdiad")))

