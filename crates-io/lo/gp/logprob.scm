(define-module (crates-io lo gp logprob) #:use-module (crates-io))

(define-public crate-logprob-0.1.0 (c (n "logprob") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "divan") (r "^0.1.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1yxl9176dw4yi7lf6dzgrlkzkqw1h8ihps0hdj1c510xid76yzd7")))

(define-public crate-logprob-0.2.0 (c (n "logprob") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "divan") (r "^0.1.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0ala1vd98qb3dc3lgfz26f8q4q348d7nb74vqlb4irswp7kwlngb")))

