(define-module (crates-io lo gt logtra) #:use-module (crates-io))

(define-public crate-logtra-0.1.1 (c (n "logtra") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "contra") (r "^5.0.1") (d #t) (k 0)) (d (n "lib-contra") (r "^5.0.1") (d #t) (k 0)) (d (n "proc-contra") (r "^5.0.1") (d #t) (k 0)))) (h "1chkfcd5xnlszx4aig6fbiiy9bfyr6x33bzrhjdq379pfnjwqk58")))

(define-public crate-logtra-0.2.0 (c (n "logtra") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "contra") (r "^5.0.1") (d #t) (k 0)) (d (n "lib-contra") (r "^5.0.1") (d #t) (k 0)) (d (n "proc-contra") (r "^5.0.1") (d #t) (k 0)))) (h "0gil10ys9gal1iqdh468l39pwb0l8c8gzihqhqrlinqfn8s53jz5")))

(define-public crate-logtra-0.3.0 (c (n "logtra") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "contra") (r "^5.0.1") (d #t) (k 0)) (d (n "lib-contra") (r "^5.0.1") (d #t) (k 0)) (d (n "proc-contra") (r "^5.0.1") (d #t) (k 0)))) (h "1iaz47sbqdwylvkfj0clnqnbc20a4zwald021yk8jjxygj44g8nd")))

(define-public crate-logtra-0.3.1 (c (n "logtra") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "contra") (r "^5.0.2") (d #t) (k 0)))) (h "0ljiidx0s5b9kd3izs1b2an6yl4ww5bk4kw6fwh5l5b4f0hq1v3s")))

(define-public crate-logtra-0.3.2 (c (n "logtra") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "contra") (r "^5.0.2") (d #t) (k 0)))) (h "12zrvzbbmpxpwxh10xig9yii918iy3bm0gjykxigs1waxqcwyvja")))

