(define-module (crates-io lo gt logto-rs) #:use-module (crates-io))

(define-public crate-logto-rs-0.1.0 (c (n "logto-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "josekit") (r "^0.8.4") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "mockito") (r "^1.2.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.62") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)))) (h "0cxrircpvs7kmn9hjy6lsl6l98a6198j72b9jmcbg07wsplw4l2v")))

