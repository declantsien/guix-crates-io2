(define-module (crates-io lo gt logtest) #:use-module (crates-io))

(define-public crate-logtest-1.0.0 (c (n "logtest") (v "1.0.0") (d (list (d (n "kv-log-macro") (r "^1.0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "1fmy5x75dwgj8jl542fx99zvsvapr8kcqk2j8qx8sw04x6277f51")))

(define-public crate-logtest-2.0.0 (c (n "logtest") (v "2.0.0") (d (list (d (n "kv-log-macro") (r "^1.0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("kv_unstable"))) (d #t) (k 0)))) (h "09ihwkq6z7xm6wdwxmc9mz74lsl20g5bi7fcdm8n87bwcnl46gpb")))

