(define-module (crates-io lo gt logtea) #:use-module (crates-io))

(define-public crate-logtea-1.0.0 (c (n "logtea") (v "1.0.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "rettle") (r "^1") (d #t) (k 0)))) (h "0qcbn85bj3i67ifr0aai15z85zd5c6z1mbx663b5kklhqc6fbd9d")))

(define-public crate-logtea-1.0.1 (c (n "logtea") (v "1.0.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "rettle") (r "^1") (d #t) (k 0)))) (h "1gymawhwa8167jqawpnalqgnk2g2wncjwakqkh5x15lykxkbncxs")))

(define-public crate-logtea-2.0.0 (c (n "logtea") (v "2.0.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "rettle") (r "^2") (d #t) (k 0)))) (h "0mabzkpsa0xqdrhj09jarxn1n3as7npv73p0rz2x1s5h3qzdwx5z")))

