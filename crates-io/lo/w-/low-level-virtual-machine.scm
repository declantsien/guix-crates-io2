(define-module (crates-io lo w- low-level-virtual-machine) #:use-module (crates-io))

(define-public crate-low-level-virtual-machine-0.0.1 (c (n "low-level-virtual-machine") (v "0.0.1") (h "0236n1j4dn1g4aiacrs6flmmsqwiyqg871bmkhblyd6704kv80qn")))

(define-public crate-low-level-virtual-machine-0.1.0 (c (n "low-level-virtual-machine") (v "0.1.0") (d (list (d (n "llvm-sys") (r "^100.1.0") (d #t) (k 0)))) (h "1frdi51bqc367hj0xpxhqai0zcf13cs7vhix13ymcdy0cigpvjwc")))

(define-public crate-low-level-virtual-machine-0.1.1 (c (n "low-level-virtual-machine") (v "0.1.1") (d (list (d (n "llvm-sys") (r "^100.1.0") (d #t) (k 0)))) (h "1b3baqr8fj0fny2m3xjjpvvrxwzy2d6fwqx3niypdvbnq3r96mvc")))

(define-public crate-low-level-virtual-machine-0.1.2 (c (n "low-level-virtual-machine") (v "0.1.2") (d (list (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "1nb65qh073d503xxgrnnxqrgdlb558jrqybhaj7f3l6bfkdxp2wy")))

