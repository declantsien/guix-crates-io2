(define-module (crates-io lo gq logql) #:use-module (crates-io))

(define-public crate-logql-0.1.0 (c (n "logql") (v "0.1.0") (h "1p8vidvzvwnx7wl4anxaynvdlgrq3dmm6v7jn5rs4f8hrc11fm08")))

(define-public crate-logql-0.2.0 (c (n "logql") (v "0.2.0") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0awrhrdx9wqbfqzmr7zm6rjkhyms8w0zx29bfqipzpf1khb2qihf")))

(define-public crate-logql-0.2.1 (c (n "logql") (v "0.2.1") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0sk7zp5ffrfxqzypsihx7qpqsa9wnwk2qzj094p9mm7yi89a8inr")))

(define-public crate-logql-0.2.2 (c (n "logql") (v "0.2.2") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1r149znqc2zldy6dsliw9j1l7afdwkjj3475x5hqgb7pgk98kkxl")))

(define-public crate-logql-0.2.3 (c (n "logql") (v "0.2.3") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "01ryn9x5frzbyn549xzslj9798ddvgy6nw01dvfq78293hkcss1a")))

(define-public crate-logql-0.2.4 (c (n "logql") (v "0.2.4") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0s5v43a2dij8dxj38h03ln5cc6ddvlm35wp0b7ca4b9zc764qfv1")))

(define-public crate-logql-0.2.5 (c (n "logql") (v "0.2.5") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0sy3ya7lli7fxl21hd2k2mfwr2dg14aynggpnd68f24r6i8gnsfm")))

(define-public crate-logql-0.2.6 (c (n "logql") (v "0.2.6") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1xfaikmb569k1dabvxd7qrl793lcgg0yb2nk4dld3bjsszq7z1dz")))

(define-public crate-logql-0.2.7 (c (n "logql") (v "0.2.7") (d (list (d (n "derive_more") (r "^0") (f (quote ("from" "add" "iterator"))) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "17l4bpd9c73s5r06pbcrk57fv5hnbdlqpii6vw6lbrl8b45dkqfi")))

