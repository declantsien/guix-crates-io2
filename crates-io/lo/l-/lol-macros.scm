(define-module (crates-io lo l- lol-macros) #:use-module (crates-io))

(define-public crate-lol-macros-0.0.1 (c (n "lol-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0c6bgxd44hlwd71wmkk463a14x26wn0i5wdxxdxqswvv3nrpns93")))

