(define-module (crates-io lo ry lory) #:use-module (crates-io))

(define-public crate-lory-0.1.0 (c (n "lory") (v "0.1.0") (h "137jxl4dk2dx0qnzsc3cbdis5j3y38mlsmihs5zzswr6sndscsm0")))

(define-public crate-lory-2020.3.16 (c (n "lory") (v "2020.3.16") (h "1gyi3xiibmwrysj9138krdy2npjn0lkgla5d8sjgdk4vnhx5j6kf")))

