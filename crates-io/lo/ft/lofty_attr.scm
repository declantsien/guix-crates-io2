(define-module (crates-io lo ft lofty_attr) #:use-module (crates-io))

(define-public crate-lofty_attr-0.1.0 (c (n "lofty_attr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0pl1csq14rpi7kl0p1l9yw9gilijxf2ayaagphi80fqq8dfsnaym") (y #t)))

(define-public crate-lofty_attr-0.1.2 (c (n "lofty_attr") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "06kb6q8yyw76nxd5fgrcjnc1nabz7qn0ikxgcc36r80hnnf74yx9") (y #t)))

(define-public crate-lofty_attr-0.1.3 (c (n "lofty_attr") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "18iyfi0pfqgpawk8rb2z50g1vg3xf2r5jn5npsdxlamnw41m2wi8") (y #t)))

(define-public crate-lofty_attr-0.1.4 (c (n "lofty_attr") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1vicqa0bz7rnpyidvdwyszxp9n5dx9qcsrwsczcslk68n7dna2bk") (y #t)))

(define-public crate-lofty_attr-0.1.5 (c (n "lofty_attr") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqk1s4jxgrm88kbxvl654mm3d7gi4x5s8pdbrd5cdq11n8pynzm") (y #t)))

(define-public crate-lofty_attr-0.1.6 (c (n "lofty_attr") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1yipinlzrbism2677ybdl6qnss0qwgqfb8cci7pfizw5m8p6yrrk") (y #t)))

(define-public crate-lofty_attr-0.1.7 (c (n "lofty_attr") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1bg3dha1z0rslyhhj0y8ln2fy2zyz2ivjhpp8kza6sgwr7xcfmwm") (y #t)))

(define-public crate-lofty_attr-0.1.8 (c (n "lofty_attr") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jjw2y0ss9m2ya7cj2yrfn67mfyvpn5n3j4kqm7sdaszlilw57cn") (y #t)))

(define-public crate-lofty_attr-0.1.9 (c (n "lofty_attr") (v "0.1.9") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mwfxxwvxskjfkvg2p10yck3brqnywg0h6kjpp9h01vr3243v4vj") (y #t)))

(define-public crate-lofty_attr-0.2.0 (c (n "lofty_attr") (v "0.2.0") (h "08rlvgcs6p7i5q5sj2x1270fp8aqxsdi2amilphah26fq192lpk4")))

(define-public crate-lofty_attr-0.3.0 (c (n "lofty_attr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1y43hgbpbgsl8nx12pfsj3277pf52g8ndklfp7gqivd9rm16d44d")))

(define-public crate-lofty_attr-0.4.0 (c (n "lofty_attr") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1iwcmrnf20l8444lss2r3fffx2f0ni790vnbvkn6idzx7vg1j39x")))

(define-public crate-lofty_attr-0.5.0 (c (n "lofty_attr") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1qkxgbk6g586amb0xmbwy0wkyfiyv9fjxq1b962q5as3nind9wh2")))

(define-public crate-lofty_attr-0.6.0 (c (n "lofty_attr") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0rprfjgvylbsqjkxm7dfyr4hvb5bazyd19fapz735nfz5yxzlv9k")))

(define-public crate-lofty_attr-0.7.0 (c (n "lofty_attr") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1p016fydjj9ciq2wl9awry29qa5cd9bzz2k55hvghxxk3n8sv0ll")))

(define-public crate-lofty_attr-0.8.0 (c (n "lofty_attr") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1cisfz4zpjf3kqw2cp4xzf13pzzmn28bj1d614fcizjkgkyf34p9")))

(define-public crate-lofty_attr-0.9.0 (c (n "lofty_attr") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1kzkf9gx0y99qdlkll3kdjwg7n6pbjlkcxm1lrjmcznhvphn0jvn")))

(define-public crate-lofty_attr-0.10.0 (c (n "lofty_attr") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1lqpfp58fcyzr62g833jf8ydnlkvn4d400kbjg7c60aalvdxrql3")))

