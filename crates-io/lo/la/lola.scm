(define-module (crates-io lo la lola) #:use-module (crates-io))

(define-public crate-lola-0.1.0 (c (n "lola") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "06dlwl46iwjfx1kgwrajl7wc5jwqc0c82cvnywyc1gb07iwh0b0a")))

(define-public crate-lola-0.1.1 (c (n "lola") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "192fq8nzgklkj2ly6q5inzg5qsnklhn4lg1kzf348xdbci58n1dc")))

