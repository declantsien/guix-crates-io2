(define-module (crates-io lo gi logi) #:use-module (crates-io))

(define-public crate-logi-0.0.1 (c (n "logi") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0vanw5fmkazm1bwqkr0jjyggmwmc43lsgcyw9dg4r8cvcgjsl4a2")))

(define-public crate-logi-0.0.2 (c (n "logi") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "07g4yzl487nkxi5w0935c8w3rdn4n390l1y2a7dxnznkmfpd7027")))

(define-public crate-logi-0.0.3 (c (n "logi") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1hkybgnmjy5ghlm55m4r4lg5s1bpcd0j52m6v22cvhc6hsg5bymx")))

(define-public crate-logi-0.0.4 (c (n "logi") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1x90f0mrvbydqzh5w25h0kfsp09zly88gnrmxlwwjr34p81n0lhm")))

(define-public crate-logi-0.0.5 (c (n "logi") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1g272krjflv5k283ngwgpaxd5d5h70027vmr6px95rkx0cybj9s8")))

(define-public crate-logi-0.0.6 (c (n "logi") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0ihwr6725pwiy3jv84kgqkq623al2ndgh51l5n65rv58rj1yyqvf")))

(define-public crate-logi-0.0.7 (c (n "logi") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "14kqx83j73ghj1vz04fgp3jqhsj7vakvdvr7q18ffpy49qg5ddi1")))

