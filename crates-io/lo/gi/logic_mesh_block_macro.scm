(define-module (crates-io lo gi logic_mesh_block_macro) #:use-module (crates-io))

(define-public crate-logic_mesh_block_macro-0.1.0 (c (n "logic_mesh_block_macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1378mg6d1z0nj17304khvvljp28lpcsgb11ry2c89y6ann544zks")))

(define-public crate-logic_mesh_block_macro-0.1.1 (c (n "logic_mesh_block_macro") (v "0.1.1") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "06kxgpc77hy4mkqfk5kwd75csdp16y7s817r050fx9zcx5nsqacl")))

(define-public crate-logic_mesh_block_macro-0.1.2 (c (n "logic_mesh_block_macro") (v "0.1.2") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1zpl2sdd2shv2zdz1i7wq7rw3h768cnjv9f68az637x643nwnjfd")))

(define-public crate-logic_mesh_block_macro-0.1.3 (c (n "logic_mesh_block_macro") (v "0.1.3") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0iis6sghq2kgzm7xa9blq8hwnlfsd65h99xfbc7ji4lr97h2y5k6")))

(define-public crate-logic_mesh_block_macro-0.1.4 (c (n "logic_mesh_block_macro") (v "0.1.4") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1cwb2dbnq211xg8dmq7wg9qa9j9zjjjhiyy8aw48lfqwi9342wkh")))

(define-public crate-logic_mesh_block_macro-0.1.5 (c (n "logic_mesh_block_macro") (v "0.1.5") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "06bdmsmm4f6xswkba20h2rssdnaigylxhklz53qxga1yfmp593q4")))

