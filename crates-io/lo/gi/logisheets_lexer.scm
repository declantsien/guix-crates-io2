(define-module (crates-io lo gi logisheets_lexer) #:use-module (crates-io))

(define-public crate-logisheets_lexer-0.1.0 (c (n "logisheets_lexer") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "067ibmcwq2rmlwyx8z27axviri9015cqcxch8r5p4yvwlpprmi6r")))

(define-public crate-logisheets_lexer-0.2.0 (c (n "logisheets_lexer") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0f9040r78sc4534fgsbc2wq5rxd1lfpnpiavv5blfjhbdikq7y4x")))

(define-public crate-logisheets_lexer-0.2.1 (c (n "logisheets_lexer") (v "0.2.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0n9lxd32n0pfhs11951d8aymwbbdjf2yqwgxba1p2zyb4gbk32y1")))

(define-public crate-logisheets_lexer-0.3.0 (c (n "logisheets_lexer") (v "0.3.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0ck3yrx3nd7nylvj5isdmrhha2k1c1g6paj3nnwxmx687gsknyq9")))

(define-public crate-logisheets_lexer-0.4.0 (c (n "logisheets_lexer") (v "0.4.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yipy6pmajgj49cpf2l32xc8gii7pchqrj7cr89r1w614qfqpz4k")))

(define-public crate-logisheets_lexer-0.5.0 (c (n "logisheets_lexer") (v "0.5.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fn0fq2adzcqgfc4sbqmjfk9ap17mvyy1zsxz79g0pprnk7s02q3")))

(define-public crate-logisheets_lexer-0.6.0 (c (n "logisheets_lexer") (v "0.6.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "16fiwrxfs90axvgz3nygsyymrnxrpaiz5mm7i4xlbkhibb4xywym")))

