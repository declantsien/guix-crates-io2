(define-module (crates-io lo gi logitech-led-sdk-sys) #:use-module (crates-io))

(define-public crate-logitech-led-sdk-sys-0.1.0 (c (n "logitech-led-sdk-sys") (v "0.1.0") (h "1sg3pvin1aa0r205fqs1iwc7479xx20ix6d3k04idbn9lbd3ga4r") (f (quote (("rustdoc") ("default"))))))

(define-public crate-logitech-led-sdk-sys-0.1.1 (c (n "logitech-led-sdk-sys") (v "0.1.1") (h "0vg9k7xa7brw5jmx4fy5fcxlash0wm02mnnmm21yjihdskcwrwf0") (f (quote (("rustdoc") ("default"))))))

