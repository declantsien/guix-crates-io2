(define-module (crates-io lo gi logical_solver) #:use-module (crates-io))

(define-public crate-logical_solver-1.0.2 (c (n "logical_solver") (v "1.0.2") (d (list (d (n "lalrpop") (r "^0.19.9") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1796dall6fj2kdzrkcv8wcaq6hr99dyqxm1vbwifb76bvnx621dv")))

