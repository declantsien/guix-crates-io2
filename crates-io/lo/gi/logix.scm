(define-module (crates-io lo gi logix) #:use-module (crates-io))

(define-public crate-logix-0.1.0 (c (n "logix") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0alzplb4jic7xin4255pakkadminrckhghz3yk4k7i4asy7jsn6n")))

(define-public crate-logix-0.2.0 (c (n "logix") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "logix-type") (r "^0.4.1") (d #t) (k 0)) (d (n "logix-vfs") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "06bx1s4x9g0vaadi22bnnimbkb77z8gbvbv8hl5c6harg7r56z32")))

(define-public crate-logix-0.2.1 (c (n "logix") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "logix-type") (r "^0.6.0") (d #t) (k 0)) (d (n "logix-vfs") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "19raqmi9khkb338k11p73ps3yfs88c07vgkg2vldbjyd524znxlk")))

