(define-module (crates-io lo gi logistics) #:use-module (crates-io))

(define-public crate-logistics-0.1.0 (c (n "logistics") (v "0.1.0") (h "17hqyrdm2mxqnp4297jk7rzvixhq89y4rcbqyvscwjby8qwzfm0i") (y #t)))

(define-public crate-logistics-0.0.0 (c (n "logistics") (v "0.0.0") (h "1xz1ck6xzq3964157pq1cni9rp91zsvkf8ljp7axkrnp7049gsl8")))

(define-public crate-logistics-0.0.1 (c (n "logistics") (v "0.0.1") (h "09s1nbyhw5p97681b0d0zzlx0wr7yg0ggi5l53aasjhn25n6vc88") (y #t)))

(define-public crate-logistics-0.0.2 (c (n "logistics") (v "0.0.2") (h "09fybmsp15ajij0wyx0lnxsxavgpqkd3nmv3vk7dra5d40z47r21")))

(define-public crate-logistics-0.0.4 (c (n "logistics") (v "0.0.4") (d (list (d (n "solver") (r "^0.0.2") (d #t) (k 0)))) (h "046vivw54rwbikjpv4bqgyvx5rqszrf62rkwpazzy39ai5qgy0b1")))

