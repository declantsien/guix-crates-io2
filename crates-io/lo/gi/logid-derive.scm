(define-module (crates-io lo gi logid-derive) #:use-module (crates-io))

(define-public crate-logid-derive-0.7.1 (c (n "logid-derive") (v "0.7.1") (d (list (d (n "logid") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03avnl0bf1170wwgyfn4gv18m1ilzag3cyc0c6yw3v0rjzvmp2hg")))

(define-public crate-logid-derive-0.8.0 (c (n "logid-derive") (v "0.8.0") (d (list (d (n "logid-core") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5agc5wqwyj06p13n69z0xb4ir2ry6kvk8myz3k77782s74znh2")))

(define-public crate-logid-derive-0.8.1 (c (n "logid-derive") (v "0.8.1") (d (list (d (n "logid-core") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02mr789agyfcabki8i8z0h5v5cblpcw59qz0y3w99ijpiw5ymjk7")))

(define-public crate-logid-derive-0.9.0 (c (n "logid-derive") (v "0.9.0") (d (list (d (n "logid-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dahx38xjh8l9qx5jx57lzkh2q7azdiq323qdz36h4b3vzrxmrwm")))

(define-public crate-logid-derive-0.11.0 (c (n "logid-derive") (v "0.11.0") (d (list (d (n "logid-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15f71pmiqbdbga3ad0d72zl7vahkfypf0whxad2kcq12ww2zb5rs")))

(define-public crate-logid-derive-0.12.0 (c (n "logid-derive") (v "0.12.0") (d (list (d (n "logid-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12wmm7yc8qkl5isaca1cwz3lylv629mywwr44xk0p1ihk9pg9zjx")))

(define-public crate-logid-derive-0.12.1 (c (n "logid-derive") (v "0.12.1") (d (list (d (n "logid-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q4k7hakxxmkzdh24sm04146cnfxvcam84v05nqpvdmlqsqbfqax")))

(define-public crate-logid-derive-0.12.2 (c (n "logid-derive") (v "0.12.2") (d (list (d (n "logid-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "006hclqsh0ryx8kp3r9bbg9614vznwkk931llazz63kncqqmpn3b")))

