(define-module (crates-io lo gi logic-lang) #:use-module (crates-io))

(define-public crate-logic-lang-0.0.1 (c (n "logic-lang") (v "0.0.1") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1bby7q6y7nh3zsdkbd0p3pvww5m50hgwz6gl03a4b0k95yq4rcvw") (f (quote (("logic_lang") ("default"))))))

(define-public crate-logic-lang-0.0.2 (c (n "logic-lang") (v "0.0.2") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0h2k90s48a92lyx9s4f8gjfnsjdjlh6yvdf56rhk65and4n2nm6n") (f (quote (("logic_lang") ("default"))))))

