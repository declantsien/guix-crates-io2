(define-module (crates-io lo gi logind-dbus) #:use-module (crates-io))

(define-public crate-logind-dbus-0.1.0 (c (n "logind-dbus") (v "0.1.0") (d (list (d (n "cascade") (r "^0.1.2") (d #t) (k 0)) (d (n "dbus") (r "= 0.6.2") (d #t) (k 0)))) (h "1zga9vrjn2lcbx0k4i4x95iik7qyrw25j6vcjzkyi93g4q9j5adv")))

(define-public crate-logind-dbus-0.1.1 (c (n "logind-dbus") (v "0.1.1") (d (list (d (n "cascade") (r "^0.1.2") (d #t) (k 0)) (d (n "dbus") (r "^0.6") (d #t) (k 0)))) (h "0swzbgk197vap3930ynsa9mq8j31i1bbqfnkqfbsv72cg2yhw9fs")))

