(define-module (crates-io lo gi logiscript) #:use-module (crates-io))

(define-public crate-logiscript-0.1.0 (c (n "logiscript") (v "0.1.0") (d (list (d (n "logisheets_controller") (r "^0.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)))) (h "0an5c1kj2kdf4mbrg52nj6v43pnlnlyr9wmnz5ydm415lg45i81l")))

