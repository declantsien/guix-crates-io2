(define-module (crates-io lo gi logit) #:use-module (crates-io))

(define-public crate-logit-0.0.0 (c (n "logit") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v6df650l1k9253138f8jr7apflhwzsaaksm8l2i9k5n7klaxi88")))

