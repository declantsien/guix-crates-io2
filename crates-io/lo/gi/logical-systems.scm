(define-module (crates-io lo gi logical-systems) #:use-module (crates-io))

(define-public crate-logical-systems-0.3.0 (c (n "logical-systems") (v "0.3.0") (d (list (d (n "fltk") (r "^1.2.27") (d #t) (k 0)) (d (n "fltk-theme") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "redis") (r "^0.21.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)))) (h "0p144f01bdb9sjxpbiz9aghj971b36mq951xzry46q6hq0ghk8vz")))

