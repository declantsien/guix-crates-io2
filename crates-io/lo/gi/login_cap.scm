(define-module (crates-io lo gi login_cap) #:use-module (crates-io))

(define-public crate-login_cap-0.0.1 (c (n "login_cap") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "login_cap-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "163nfgcgnqgav80fl0qmws84rxpahfr73lk83rk3y0b7gc77cby7")))

(define-public crate-login_cap-0.0.2 (c (n "login_cap") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "login_cap-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "1pbd88r2vaw70482fr29m0nz1yz2xc5d903gfgfpfj11ilyd8kgc")))

