(define-module (crates-io lo gi logind-zbus) #:use-module (crates-io))

(define-public crate-logind-zbus-0.1.0 (c (n "logind-zbus") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "1mazhgbinlk8clpvm3bc1z6f05ibfkn1v8xzq1yq7jmrjbgp3xam")))

(define-public crate-logind-zbus-0.1.1 (c (n "logind-zbus") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "1i3022v3a2dhvx460qgwfhvmh7sbvzz3xcap5j8392v6jfi9212f")))

(define-public crate-logind-zbus-0.1.2 (c (n "logind-zbus") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "1qnn0bazlx0i5af5s8gwbhgcazlrqnw2n6gdnjs8asjskj4n9akq")))

(define-public crate-logind-zbus-0.1.3 (c (n "logind-zbus") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "0hd7a26zka7xnr0r0awa40r312988nbr57n212yxiarbg87rq9lj")))

(define-public crate-logind-zbus-0.1.4 (c (n "logind-zbus") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "135xykwzxx3qnj4llm5nhl90s1lsz1a3gpzdqvh8qak57z27ji11")))

(define-public crate-logind-zbus-0.1.5 (c (n "logind-zbus") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "05g2sa4z1qlqlmvsb3flb19nriwcj3apc2x40xzfnxawhphjh48p")))

(define-public crate-logind-zbus-0.1.6 (c (n "logind-zbus") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "11dawc2bllig60acp51k1l6pph2d6v7ckc9ni8prywa4lrhgbnjl")))

(define-public crate-logind-zbus-0.1.7 (c (n "logind-zbus") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "1clnic2j73arwrkax2mqvryvljnr4pdxd9widxvnyzn81wh18qyy")))

(define-public crate-logind-zbus-0.1.8 (c (n "logind-zbus") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "0p03ylfn34bi7y54zzqh7hkd3m5lzif49skdxj7wzhlmlbxrl9kk")))

(define-public crate-logind-zbus-0.2.0 (c (n "logind-zbus") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.8") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.8") (d #t) (k 0)) (d (n "zvariant") (r "^2.5") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.5") (d #t) (k 0)))) (h "1q0pd6mwc7qshqbcdqk5xc7yfvjmd5bfmkzm73apvgb707a9lddk") (f (quote (("azync"))))))

(define-public crate-logind-zbus-0.4.0 (c (n "logind-zbus") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "1xb3b5kis0vqbg60lqdz64s06cp7l5pyjlwrxl6gd8c6hc4z3c4x") (f (quote (("azync"))))))

(define-public crate-logind-zbus-0.5.0 (c (n "logind-zbus") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "0ab0mcmv3g4iipanvkz8q5nd7b9hsnpbw6v83m0nm3rrczy6jqj1") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-0.6.0 (c (n "logind-zbus") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "1sqlqgjb3df6kx1gzqkk83pq8rblh0z7haclw821pz5ci6g70qgp") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-0.6.1 (c (n "logind-zbus") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "1fyzw11xfqqjvk3y8xwr3b2nbhpw4s46ykwz01irsgmni37zcwb6") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-0.7.0 (c (n "logind-zbus") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "0f4wsvbsfsx737hsjigggwc23k68qcvvl675jgwsjlcm4dajmgqp") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-1.0.0-beta1 (c (n "logind-zbus") (v "1.0.0-beta1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "1mrmc5ad5b4p70h07i0iscndsy09mkdjf373gajwidi0zx1qzxnl") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-0.7.1 (c (n "logind-zbus") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^1.9.1") (d #t) (k 0)) (d (n "zbus_macros") (r "^1.9.1") (d #t) (k 0)) (d (n "zvariant") (r "^2.6") (d #t) (k 0)) (d (n "zvariant_derive") (r "^2.6") (d #t) (k 0)))) (h "18jknfni9lz1n3d8g75048l1sb8jy11966ja1hz96rg327m4p9nw") (f (quote (("default") ("azync"))))))

(define-public crate-logind-zbus-2.0.0 (c (n "logind-zbus") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.0.0") (d #t) (k 0)) (d (n "zvariant_derive") (r "^3.0.0") (d #t) (k 0)))) (h "1c9647ja7889xddhl2mdxqd8la64q96k25crxry14n52jz3kj93r") (f (quote (("non_blocking") ("default"))))))

(define-public crate-logind-zbus-2.1.0 (c (n "logind-zbus") (v "2.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0") (d #t) (k 0)) (d (n "zbus_macros") (r "^2.0.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.0.0") (d #t) (k 0)) (d (n "zvariant_derive") (r "^3.0.0") (d #t) (k 0)))) (h "0k805yb6165i0mlpwhrrsd97jxxq8agxqpqbrjai707fglx878ka")))

(define-public crate-logind-zbus-3.0.0 (c (n "logind-zbus") (v "3.0.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.1.2") (d #t) (k 0)))) (h "1027wyim4axgivs973f55id0zddbcv6gvwa6lzdgsfvbycgrs4hp")))

(define-public crate-logind-zbus-3.0.1 (c (n "logind-zbus") (v "3.0.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.1.2") (d #t) (k 0)))) (h "09h2bgff7c58r90gn5q07vdf46i79zd6mhghv9ihk8hq03r5hff0")))

(define-public crate-logind-zbus-3.0.2 (c (n "logind-zbus") (v "3.0.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.1.2") (d #t) (k 0)))) (h "05vqjjnmbxq49qmjiy93hhql2jqzdc0jrb11dzk4dc1lg9kdaaqw")))

(define-public crate-logind-zbus-3.0.3 (c (n "logind-zbus") (v "3.0.3") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.5.0") (d #t) (k 0)))) (h "1csm3fnbdd82z5nahf1zrrnywahlkcpb99y155rlc6ygylwifjr1")))

(define-public crate-logind-zbus-3.1.0 (c (n "logind-zbus") (v "3.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6.2") (d #t) (k 0)))) (h "1qwgn7j931kha2cabpjnvsr6khhjwn6c0kilswm013awaragqb1z")))

(define-public crate-logind-zbus-3.1.1 (c (n "logind-zbus") (v "3.1.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~3.13.1") (d #t) (k 0)))) (h "0my1vvmick2z1vkq164wn1vvmw2dxshw388i8jr20lp09cv6pcql")))

(define-public crate-logind-zbus-3.1.2 (c (n "logind-zbus") (v "3.1.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~3.14.0") (d #t) (k 0)))) (h "1cvyrf8wdl29sq09sb931fxb5vch462m91hmh0ksj7p9yr12ayn0")))

(define-public crate-logind-zbus-4.0.0 (c (n "logind-zbus") (v "4.0.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~4.0.1") (d #t) (k 0)))) (h "1qpak27rzzkqwz3faz90j6fkg6cmmpbhq3bnzfg9827f0f6ygzaa")))

(define-public crate-logind-zbus-4.0.1 (c (n "logind-zbus") (v "4.0.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~4.0.1") (d #t) (k 0)))) (h "1qzsz3x4l4yd5spffppzfghp3gmc2dswm5bxz67yhkvcyp77lgxl")))

(define-public crate-logind-zbus-4.0.2 (c (n "logind-zbus") (v "4.0.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "~4.1.2") (d #t) (k 0)))) (h "0am4gpy0nfg78py99l7ryh2iisl9m8hbycx3fh3isxxyx9d3gchm")))

(define-public crate-logind-zbus-4.0.3 (c (n "logind-zbus") (v "4.0.3") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "zbus") (r "^4.2") (d #t) (k 0)))) (h "07kjlln6dpv4rqkc31fk0vpwq57jm5bwys415lkj6r95v8fj6xwf")))

