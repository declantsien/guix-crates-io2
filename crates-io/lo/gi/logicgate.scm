(define-module (crates-io lo gi logicgate) #:use-module (crates-io))

(define-public crate-logicgate-0.1.0 (c (n "logicgate") (v "0.1.0") (h "1pbiyb2wq8ybyiyw5591wlwi2bbpvawnjzma7lh9jmw9s822q3bi")))

(define-public crate-logicgate-0.2.0 (c (n "logicgate") (v "0.2.0") (h "02r74fh2340waywclbvbpmqia0jzgs0hjf9fsxkgwwzqmhc1r4j5")))

(define-public crate-logicgate-0.2.1 (c (n "logicgate") (v "0.2.1") (h "004frah6vkmvjxaffagsjkpisx53jp86yi1a3vd6mj3f39b6ysrk")))

(define-public crate-logicgate-0.2.2 (c (n "logicgate") (v "0.2.2") (h "0a7k6q87m39gjx9hndjafjqnbnwqmiyfs50q2pwcxndgq2njlyhg")))

(define-public crate-logicgate-0.3.2 (c (n "logicgate") (v "0.3.2") (h "0w5ja4l5x0ygm1b3j85gf3c0v5m0klwkbjhy7ppggyjssk89vfya")))

