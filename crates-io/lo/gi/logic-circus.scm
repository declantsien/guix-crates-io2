(define-module (crates-io lo gi logic-circus) #:use-module (crates-io))

(define-public crate-logic-circus-0.1.0 (c (n "logic-circus") (v "0.1.0") (h "1c3hy5fmigijz984sh501f7ypi5pxn81091n400j8047pgsmai38")))

(define-public crate-logic-circus-0.2.0 (c (n "logic-circus") (v "0.2.0") (h "0yw1a5ai4a5i2gmqz86af9jk5zk9vwzzpb4mqambqqm7ryk747zi")))

(define-public crate-logic-circus-0.2.1 (c (n "logic-circus") (v "0.2.1") (h "0k8986y1fdh52dr84r878p94gn3i3b6k9107zlhas0rh9a3fvpzd")))

(define-public crate-logic-circus-0.3.0 (c (n "logic-circus") (v "0.3.0") (h "191l3i814hbj77yl11jdd1hcxkk3av5hh1ji1ynjxnry67n0xwdy")))

