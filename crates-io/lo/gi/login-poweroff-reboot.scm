(define-module (crates-io lo gi login-poweroff-reboot) #:use-module (crates-io))

(define-public crate-login-poweroff-reboot-0.2.1 (c (n "login-poweroff-reboot") (v "0.2.1") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)))) (h "1z6bwyl071b7vrd658gnn000bb8jslgcw141a1sfws154bzvl6jp")))

(define-public crate-login-poweroff-reboot-0.2.2 (c (n "login-poweroff-reboot") (v "0.2.2") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)))) (h "01wbabr8g668w0gxhrr2sx244n8kjidljsqaf66ml3h2qcsb3hqd")))

(define-public crate-login-poweroff-reboot-0.2.3 (c (n "login-poweroff-reboot") (v "0.2.3") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)))) (h "194x1a6l85z097cs2pw2k3p7gqw63j0xrwnip2pyrjc5bdcny1w2")))

(define-public crate-login-poweroff-reboot-0.2.4 (c (n "login-poweroff-reboot") (v "0.2.4") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)))) (h "047nckkfisv0ihp3klv88lqm9mxm6ma5rnacdxqf7d4ac7mxsbm1")))

(define-public crate-login-poweroff-reboot-0.3.0 (c (n "login-poweroff-reboot") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)))) (h "1jqgnnl15dbqhf8bls1gjgdr03adf6h6c0f922209wbdibfbsmh0")))

(define-public crate-login-poweroff-reboot-0.4.0 (c (n "login-poweroff-reboot") (v "0.4.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "14l80bgwjk8lq98l3x0y7giwr2bnrnn2f4phby9ii08c98cab0zs")))

(define-public crate-login-poweroff-reboot-0.4.4 (c (n "login-poweroff-reboot") (v "0.4.4") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "05lj427hc7iz2bbcs88wybr56lw3inqznflwys9mhx3xji6nkfrs")))

(define-public crate-login-poweroff-reboot-0.4.5 (c (n "login-poweroff-reboot") (v "0.4.5") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c1ghdkny8sc53pf3ww99p4wp2lml697ar2nb3lwhcnrv1l7ga4r")))

