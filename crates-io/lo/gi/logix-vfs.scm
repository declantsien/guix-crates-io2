(define-module (crates-io lo gi logix-vfs) #:use-module (crates-io))

(define-public crate-logix-vfs-0.1.0 (c (n "logix-vfs") (v "0.1.0") (h "1afh3xs8nxm7047l0qg3jcs2j0w4s3868n4y6rjpl7d0zm8fvslj")))

(define-public crate-logix-vfs-0.1.1 (c (n "logix-vfs") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "031z9x33h14k3xxh83sla6482k3b2nin7992a1b001r6xixc7ca4")))

(define-public crate-logix-vfs-0.2.0 (c (n "logix-vfs") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0j6y2kx62y5xzsbqz1z0ci3wq9gj837zxp49hycx1yp1j0siv2di")))

(define-public crate-logix-vfs-0.3.0 (c (n "logix-vfs") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "099133jh5x8d1rs9gzsjw1yr7311izd6v90mwrvkzh9fvf56p0li")))

(define-public crate-logix-vfs-0.4.0 (c (n "logix-vfs") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1ilgwc29ql4lix8znxk9f0zs6w6w3r5ics2ldsih0pq3y0j6m5db")))

(define-public crate-logix-vfs-0.5.0 (c (n "logix-vfs") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1drr20aq25zfc6wdgqv9vwg0f8mj3n67jvxf159cf255k6whpmys")))

(define-public crate-logix-vfs-0.5.1 (c (n "logix-vfs") (v "0.5.1") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "097zmyp29jqaam2vgws6z0pm80r598y1sbqhflwkwzbqbhsslzrd")))

(define-public crate-logix-vfs-0.6.0 (c (n "logix-vfs") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0sbp2prxvvqxwnx4a9c79hlax7z8wl3b052xr39f13n48b9a73qj")))

(define-public crate-logix-vfs-0.6.1 (c (n "logix-vfs") (v "0.6.1") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05jswwylkfhp2ng7n7z1bh5zzkzfgx1h5bcj5gi5ngjpzam46jh0")))

(define-public crate-logix-vfs-0.7.0 (c (n "logix-vfs") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1fpfwrddva8x7w3cprw6r85dbwxvzxdczjdv77dqqc9qgjfj1cr2")))

