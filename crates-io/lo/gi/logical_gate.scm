(define-module (crates-io lo gi logical_gate) #:use-module (crates-io))

(define-public crate-logical_gate-0.1.0 (c (n "logical_gate") (v "0.1.0") (h "0mps48b78mpk7535rlz7wy0zmdxlsdky7nhsd7ji86i9xdbrrbp7")))

(define-public crate-logical_gate-0.1.0-bool (c (n "logical_gate") (v "0.1.0-bool") (h "0nqn36dgzgxhf13nmpkskwwpf7vhl1h86hfihjwy2x6gybgyl3ha")))

(define-public crate-logical_gate-0.1.0-fast (c (n "logical_gate") (v "0.1.0-fast") (h "1algl6lv6gki3ma0cqnp3byk9x8wn3m41i34r77m2fcbnxqcfayw")))

