(define-module (crates-io lo gi logical_expression_parser) #:use-module (crates-io))

(define-public crate-logical_expression_parser-0.1.0 (c (n "logical_expression_parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0vqsydv8hmg03sy2dr7bn6y7ar6i1v4iafvsg6hc2pgsgrhpgbax")))

(define-public crate-logical_expression_parser-0.1.1 (c (n "logical_expression_parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1x4m7l93xw6c5a0bjm56lfgrhs91n6vv1qqr2kd1kvg3yqdysqp2")))

