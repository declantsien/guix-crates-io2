(define-module (crates-io lo gi logical_clock) #:use-module (crates-io))

(define-public crate-logical_clock-0.1.0 (c (n "logical_clock") (v "0.1.0") (h "11c7n26jk1d41grsc4m2svq06msivyyh3fmcziwvq0zz722qq2a9")))

(define-public crate-logical_clock-0.1.1 (c (n "logical_clock") (v "0.1.1") (h "17ni9n382b4glqdn2pkpkhv7r4pcnwj15bl59m33f24p5r7226rn")))

(define-public crate-logical_clock-0.1.2 (c (n "logical_clock") (v "0.1.2") (d (list (d (n "nanoid") (r "^0.2.0") (d #t) (k 0)))) (h "1wa5k3k3n9vwayi9jyqrx6lg3zphjvma1fggl8iqgp8pbaszw0sx")))

(define-public crate-logical_clock-0.1.3 (c (n "logical_clock") (v "0.1.3") (d (list (d (n "nanoid") (r "^0.2.0") (d #t) (k 0)))) (h "0q97kg01mhg72k6v2srgkx931k1m5mhmbr75f9ccy39drx53jz7l")))

