(define-module (crates-io lo gi logic-long) #:use-module (crates-io))

(define-public crate-logic-long-0.1.0 (c (n "logic-long") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "000q783crkl6hy11d3rhilx0pfif0cp2kl37bpva2xdf95pr6p2b")))

(define-public crate-logic-long-0.1.1 (c (n "logic-long") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yi9mkyzabsx94vc1qhcb362i1ridlchmw22vrp359zsqni9x3kx")))

(define-public crate-logic-long-0.1.2 (c (n "logic-long") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zyg0br75imd82wawmdjvijni7lf8xjncl8jv16lwvkgm6y4w97r")))

(define-public crate-logic-long-0.1.3 (c (n "logic-long") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yjv0c97adqxwnbqca6jh97sm47h9s06lf2g83aqsvqp467hgind")))

(define-public crate-logic-long-0.1.4 (c (n "logic-long") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fkbvfm4inznkdmzaxyysvy20jlz56rqi0lma2cpd5kqm8y4hcbk")))

(define-public crate-logic-long-0.1.5 (c (n "logic-long") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ivayxm633q6v3d7n6m0dbilybavb9fnfq7vymgcwdhzw16kxwk0")))

(define-public crate-logic-long-0.1.6 (c (n "logic-long") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14rxfv2blbf4vdm6ipr316yp5bd499rkvvd03lshxgaly3j9na04")))

(define-public crate-logic-long-0.1.8 (c (n "logic-long") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r14958plawd677x195d6ap7cmci8svrbhj9aq0kjasalk1yhzgm")))

(define-public crate-logic-long-0.2.0 (c (n "logic-long") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ngnmv4abhshy0jv5nsn2x5vsg6zc88dzfr1igfmn66xkjjkd2np")))

