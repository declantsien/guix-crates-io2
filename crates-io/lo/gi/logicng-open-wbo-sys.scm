(define-module (crates-io lo gi logicng-open-wbo-sys) #:use-module (crates-io))

(define-public crate-logicng-open-wbo-sys-0.1.0-alpha.1 (c (n "logicng-open-wbo-sys") (v "0.1.0-alpha.1") (d (list (d (n "cxx") (r "^1.0.107") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.107") (d #t) (k 1)))) (h "059r8jdavwq447dbrfbfj10vvwc4n8pdz3x24mzv51hfqgmgjnkl")))

(define-public crate-logicng-open-wbo-sys-0.1.0-alpha.2 (c (n "logicng-open-wbo-sys") (v "0.1.0-alpha.2") (d (list (d (n "cxx") (r "^1.0.107") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.107") (d #t) (k 1)))) (h "0z18pz3z21ijnjin4wrnmma2z99c9lgik01b3fpvkz31azyigd4v")))

(define-public crate-logicng-open-wbo-sys-0.1.0-alpha.3 (c (n "logicng-open-wbo-sys") (v "0.1.0-alpha.3") (d (list (d (n "cxx") (r "^1.0.107") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.107") (d #t) (k 1)))) (h "0vbxshpkiw3k6hzy98lj3k8m2j69znx2gy6y825zq8x2s5ijks52")))

