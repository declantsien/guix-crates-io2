(define-module (crates-io lo gi logic-form) #:use-module (crates-io))

(define-public crate-logic-form-0.1.0 (c (n "logic-form") (v "0.1.0") (h "17vas58ajmwm0ap35i6667p8pwlbr31h445grlv26k49f9fs3j3n")))

(define-public crate-logic-form-0.1.1 (c (n "logic-form") (v "0.1.1") (h "1ph8f26wcdnncwsq54zdjb0w7xblrzm2p26225qm3338aaa2fi3j")))

