(define-module (crates-io lo gi logic-rs) #:use-module (crates-io))

(define-public crate-logic-rs-0.1.0 (c (n "logic-rs") (v "0.1.0") (d (list (d (n "approx") (r "~0.5.1") (d #t) (k 2)) (d (n "csv") (r "~1.1.6") (d #t) (k 2)) (d (n "ordered-float") (r "~2.8.0") (d #t) (k 0)))) (h "0s5ywl8z2f2rk0awr75xdbcdf5cy8gzf6vpzq8pwvga8j4v2rcj5")))

(define-public crate-logic-rs-0.1.1 (c (n "logic-rs") (v "0.1.1") (d (list (d (n "approx") (r "~0.5.1") (d #t) (k 2)) (d (n "csv") (r "~1.1.6") (d #t) (k 2)) (d (n "ordered-float") (r "~2.8.0") (d #t) (k 0)))) (h "007i2fcgjk43y445k8y8nhf7y35x636gwbs44j5xaxp67wq54qlc")))

