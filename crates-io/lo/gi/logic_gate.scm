(define-module (crates-io lo gi logic_gate) #:use-module (crates-io))

(define-public crate-logic_gate-0.1.0 (c (n "logic_gate") (v "0.1.0") (h "05zs3pj03pn0svh9g2sh86q9yjr0ss4i37wrzd4w0jry0ldgh8hm")))

(define-public crate-logic_gate-0.2.0 (c (n "logic_gate") (v "0.2.0") (h "1jh0n4n8jp0dhsk85hiy5g7nz8ddn4fqym5w3mvshby1n9162bm0")))

(define-public crate-logic_gate-0.2.1 (c (n "logic_gate") (v "0.2.1") (h "0xv89hwhp46l9schmcr87h7ckfji845b1cb7cs73szkqvav4jaz7")))

(define-public crate-logic_gate-0.2.2 (c (n "logic_gate") (v "0.2.2") (h "1ppcdmsaqsklfy2qfzknpcv2ilrnwlkwrd9isx0vs1w263md2bkr")))

(define-public crate-logic_gate-0.3.0 (c (n "logic_gate") (v "0.3.0") (h "040jfyhdsd7233cxw987djvrmjn1dfdlzjkwnsxb9ncz53qlkjnn")))

(define-public crate-logic_gate-0.4.0 (c (n "logic_gate") (v "0.4.0") (h "0n8f9j739sj0vwsn525xkdyk4qyykhjjapgvkgjg437vyp45d0p4")))

