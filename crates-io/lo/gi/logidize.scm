(define-module (crates-io lo gi logidize) #:use-module (crates-io))

(define-public crate-logidize-0.5.1 (c (n "logidize") (v "0.5.1") (d (list (d (n "const_format") (r "^0.2.30") (d #t) (k 0)))) (h "07is5p5my3mx5i96rj4vk8ky2ghiqkvd5zlgzlkasq6pjxgkhxr8") (y #t)))

(define-public crate-logidize-0.5.2 (c (n "logidize") (v "0.5.2") (d (list (d (n "const_format") (r "^0.2.30") (d #t) (k 0)))) (h "1dhgjv0yjkw84nxr7giy06i40s8aaw14sih4vnvaagcgpx499bdq") (y #t)))

