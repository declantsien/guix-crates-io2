(define-module (crates-io lo gi logical) #:use-module (crates-io))

(define-public crate-logical-0.2.0 (c (n "logical") (v "0.2.0") (h "08hcffpm320w7681bivqvxis0npf0c2b874cax90dr3shqxiy27b")))

(define-public crate-logical-0.2.1 (c (n "logical") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0bzp09p17a9jl09xf520dwcccdxymvijyh6miybpyp3b9izzlv6l")))

