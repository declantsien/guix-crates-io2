(define-module (crates-io lo gi logisheets_xmlserde) #:use-module (crates-io))

(define-public crate-logisheets_xmlserde-0.1.0 (c (n "logisheets_xmlserde") (v "0.1.0") (d (list (d (n "logisheets_derives") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "06dhr9wjldl011ac5zqag6mjj01bn69lnbxq2fw5f3jv5lmyhcd2")))

(define-public crate-logisheets_xmlserde-0.2.0 (c (n "logisheets_xmlserde") (v "0.2.0") (d (list (d (n "logisheets_derives") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "ts-rs") (r "^6.1.2") (d #t) (k 0)))) (h "0xk8af04m7k5vs60rjwn2nqrq4l0c8ki47firi0rhl6wv59xiw1h")))

(define-public crate-logisheets_xmlserde-0.2.1 (c (n "logisheets_xmlserde") (v "0.2.1") (d (list (d (n "logisheets_derives") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "ts-rs") (r "^6.1.2") (d #t) (k 0)))) (h "0nz8r098gl99asr55ab6pgkp7vrg0wkh1q0lr9q1qmgyx7zsfy3k")))

