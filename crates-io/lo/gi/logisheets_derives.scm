(define-module (crates-io lo gi logisheets_derives) #:use-module (crates-io))

(define-public crate-logisheets_derives-0.1.0 (c (n "logisheets_derives") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "06h2phkdmb5ywmlp56n5gxiambqjjqpcd3iq6libi11gx0savn8f")))

(define-public crate-logisheets_derives-0.2.0 (c (n "logisheets_derives") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0x84ynd8kg22zascpn41b4vlq1ng6vwpg4mmpqwgazil4s0whq0r")))

(define-public crate-logisheets_derives-0.2.1 (c (n "logisheets_derives") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "09vyjp8gxzcmsiszkd0xii847gswmpxynxz8sm7w24pqrbh4kjzv")))

(define-public crate-logisheets_derives-0.3.0 (c (n "logisheets_derives") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0j3lv9rgfp5k93hn7jxk9ln8vj3946vp7c11gxk06wsjmh9s9avl")))

