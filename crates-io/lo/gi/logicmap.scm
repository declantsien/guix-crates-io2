(define-module (crates-io lo gi logicmap) #:use-module (crates-io))

(define-public crate-logicmap-0.0.0 (c (n "logicmap") (v "0.0.0") (d (list (d (n "jsonpath_lib") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q9mdi5swn28har933krqjq0sz7jvb9amwrv56hn499vwzgnkhdf")))

(define-public crate-logicmap-0.0.1 (c (n "logicmap") (v "0.0.1") (d (list (d (n "jsonpath_lib") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pwzp8imva8kpajzrqdf2sl338vpa2hcq5hglxpn0fhkxlg7rrpz")))

