(define-module (crates-io lo gi login_cap-sys) #:use-module (crates-io))

(define-public crate-login_cap-sys-0.0.1 (c (n "login_cap-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0ywq8smdjajmw9lgfkbdcy5rr7gcwhl7d31300vzc4sbjgjjq2gz")))

(define-public crate-login_cap-sys-0.1.0 (c (n "login_cap-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "10macscirhfg6qrdl0b17dadfgx3cnjgcfwhzsznjkk07i7c3mfj")))

