(define-module (crates-io lo gi logisheets_astchecker) #:use-module (crates-io))

(define-public crate-logisheets_astchecker-0.4.0 (c (n "logisheets_astchecker") (v "0.4.0") (d (list (d (n "logisheets_base") (r "^0.4.0") (d #t) (k 0)) (d (n "logisheets_parser") (r "^0.4.0") (d #t) (k 0)))) (h "10r0yysi4d4qxbaphgvv2zp3avifyljvkra65sz6kjvql63hp0g1")))

(define-public crate-logisheets_astchecker-0.5.0 (c (n "logisheets_astchecker") (v "0.5.0") (d (list (d (n "logisheets_base") (r "^0.5.0") (d #t) (k 0)) (d (n "logisheets_parser") (r "^0.5.0") (d #t) (k 0)))) (h "0nb15avmhb67m4mq52my3gsg9bqn54hn3qixm7g62zkgi19n9nzc")))

(define-public crate-logisheets_astchecker-0.6.0 (c (n "logisheets_astchecker") (v "0.6.0") (d (list (d (n "logisheets_base") (r "^0.6.0") (d #t) (k 0)) (d (n "logisheets_parser") (r "^0.6.0") (d #t) (k 0)))) (h "10h4bpvjb80ird2dck9zinyi38aj50gy2fqv42kk8j7zymsnb5pd")))

