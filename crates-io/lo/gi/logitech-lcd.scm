(define-module (crates-io lo gi logitech-lcd) #:use-module (crates-io))

(define-public crate-logitech-lcd-1.0.0 (c (n "logitech-lcd") (v "1.0.0") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 2)) (d (n "logitech-lcd-sys") (r "^1.0.2") (d #t) (k 0)))) (h "0hm3prgr9nfjl325d69v8ja1lbz8ap35l2wscrcnaihwl5jrhl8z") (y #t)))

(define-public crate-logitech-lcd-1.0.1 (c (n "logitech-lcd") (v "1.0.1") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 2)) (d (n "logitech-lcd-sys") (r "^1.0.4") (d #t) (k 0)))) (h "0cx6nr5p4kki78xc40iy28bxqhbivfzfajvara9iqp50jkzaqnls")))

(define-public crate-logitech-lcd-1.0.2 (c (n "logitech-lcd") (v "1.0.2") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 2)) (d (n "logitech-lcd-sys") (r "^1.0.4") (d #t) (k 0)))) (h "0mk5p405xzz8vx4bz7vj15zpfxhack850fwmzxmlplxhajgirhji")))

(define-public crate-logitech-lcd-2.0.0 (c (n "logitech-lcd") (v "2.0.0") (d (list (d (n "image") (r "^0.13") (d #t) (k 2)) (d (n "logitech-lcd-sys") (r "^2.0.0") (d #t) (k 0)))) (h "1q430by6aa8vrnpvl6pbvagjz6k2v9njgc9l290dyhi7y2xymi7g")))

