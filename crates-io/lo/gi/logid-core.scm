(define-module (crates-io lo gi logid-core) #:use-module (crates-io))

(define-public crate-logid-core-0.8.0 (c (n "logid-core") (v "0.8.0") (d (list (d (n "evident") (r "^0.5") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04lxrl6i6qkiin3hr8lh43bksb9m13wd6b6ky2i43ci7f2q8dmfg") (f (quote (("payloads" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.8.1 (c (n "logid-core") (v "0.8.1") (d (list (d (n "evident") (r "^0.6") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mgrg8yf9l1a19hpaxb0a48qmh4i846av93pgahic466n55amfjx") (f (quote (("payloads" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.9.0 (c (n "logid-core") (v "0.9.0") (d (list (d (n "evident") (r "^0.6") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "074b6kkk06q6wwrads3mi56xc6pa16pc6ivbcmwylwl5xasbasyc") (f (quote (("payloads" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.11.0 (c (n "logid-core") (v "0.11.0") (d (list (d (n "evident") (r "^0.12") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fq62il62hkywr8469r2qcb0vs4nr7v7z62068n5sjbaa3cvswkk") (f (quote (("test_filter") ("payloads" "serde_json") ("log_traces") ("log_debugs") ("hint_note") ("fmt" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.12.0 (c (n "logid-core") (v "0.12.0") (d (list (d (n "evident") (r "~0.12") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01shqhczr2plk5f147x7xcwmzllpv1gqjpzw7z67dfc0ycxz8fiy") (f (quote (("test_filter") ("payloads" "serde_json") ("log_traces") ("log_debugs") ("hint_note") ("fmt" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.12.1 (c (n "logid-core") (v "0.12.1") (d (list (d (n "evident") (r "~0.12") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1j1rc3sqk2y6v0gf96cxg7s84ac1ma1xb7hnm91dfpkq293i6cib") (f (quote (("test_filter") ("payloads" "serde_json") ("log_traces") ("log_debugs") ("hint_note") ("fmt" "serde_json") ("diagnostics" "lsp-types"))))))

(define-public crate-logid-core-0.12.2 (c (n "logid-core") (v "0.12.2") (d (list (d (n "evident") (r "~0.12") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17in7388ry3178qh2yvggfj5bq53lrjyxszphrlkqnk399r1i4cq") (f (quote (("test_filter") ("payloads" "serde_json") ("log_traces") ("log_debugs") ("hint_note") ("fmt" "serde_json") ("diagnostics" "lsp-types"))))))

