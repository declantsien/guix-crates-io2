(define-module (crates-io lo gi logix-type-derive) #:use-module (crates-io))

(define-public crate-logix-type-derive-0.1.0 (c (n "logix-type-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "18ppljcr0qyqwsrlzaahc6jff89dcq37f0zmmbqc4cmn88c7saqa")))

(define-public crate-logix-type-derive-0.1.1 (c (n "logix-type-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "19gv5qc8hv7zz4glqfxb335amlipfyad0lw9sn2s1mpkiiz16rc4")))

(define-public crate-logix-type-derive-0.2.0 (c (n "logix-type-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0i9m3617xyxm1x72zpjm3mbs4iz30s66x95v6gm0n12kspkf03p8")))

(define-public crate-logix-type-derive-0.3.0 (c (n "logix-type-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0518lvh0zg2vcpdi8nizcwlqmb0rjjir4rswjwl2790j36zh1xn7")))

(define-public crate-logix-type-derive-0.3.1 (c (n "logix-type-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "13wsprhlwbwinabhlwwhbr7clgy2vc83c5d2p0mk73iqhm65h9yn")))

(define-public crate-logix-type-derive-0.3.2 (c (n "logix-type-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0nz6fa2kdglj0hyj9jhqljn3kl6p6c9hygjrzli1mvmykm34bizd")))

(define-public crate-logix-type-derive-0.4.0 (c (n "logix-type-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0algbcgyvr0x5vp3k925cznnh7mmajacyw6ad6xm8b20asb93qb0")))

(define-public crate-logix-type-derive-0.4.1 (c (n "logix-type-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0xn3ixw0g23941gnknb42mqijaaimnq7i5zv6zmw5qff6ajfnh2k")))

(define-public crate-logix-type-derive-0.4.2 (c (n "logix-type-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1xdwbn7fjlj0pp7q9683f7mffb49jz78m3sv60d8ic9c1l2inilh")))

(define-public crate-logix-type-derive-0.5.0 (c (n "logix-type-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "00fd7pic5mywspbx6bfrf9dgg7ik821bkrlqg4xfvrzlcrrwz1kf")))

(define-public crate-logix-type-derive-0.6.0 (c (n "logix-type-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1yvgw6mficm8krsckw94m14yf1dckjrmdrslysy05i3q18d2np9y")))

(define-public crate-logix-type-derive-0.6.1 (c (n "logix-type-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1q0893kxgpfr5fd5l94iiy34mpmg5mjcil480azqn6grzxzh9ivs")))

(define-public crate-logix-type-derive-0.7.0 (c (n "logix-type-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1fkg72n8k8mlsljrrsnd94pi6a5n8a5y26zy5pc6b2jmzdgqhzr2")))

(define-public crate-logix-type-derive-0.8.0 (c (n "logix-type-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0dr37cwzxiybdl44a0ihswnx17hc39mfvkirdafz0yw9a026zp8x")))

(define-public crate-logix-type-derive-0.8.1 (c (n "logix-type-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1nrv2wcwmidr6asjxwfkcjn7b888qg1ysxbgm87cvrzsbw40abvd")))

(define-public crate-logix-type-derive-0.9.0 (c (n "logix-type-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1xh6mrcxl3q84ifwz7pnw67ik4x8vaq7q1vqixcv88rvzngc3sl2")))

