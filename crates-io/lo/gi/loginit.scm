(define-module (crates-io lo gi loginit) #:use-module (crates-io))

(define-public crate-loginit-0.1.0 (c (n "loginit") (v "0.1.0") (d (list (d (n "tracing-stackdriver") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1gsr8n49j31i2n80yh77gn5jps7q91jrmdkm0cwkfyk08jsz6ix2") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.2 (c (n "loginit") (v "0.1.2") (d (list (d (n "tracing-stackdriver") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0n9czq63f62g873dbackawshlzgs6jk3mksaaqn05664g4jp4i4b") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.3 (c (n "loginit") (v "0.1.3") (d (list (d (n "tracing-stackdriver") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1x1inl1n6yddji5xs70v36rrdnwpwxhfk1rsfl0nsmlfzf7i8n0p") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.4 (c (n "loginit") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0pyzc286mqxmnyc0n5db1bc5dsbflkcdfd56lyghgf5a0fchqpg0") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.5 (c (n "loginit") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "07hbkpsr9p8z49y4vvrzbfgad1m7dlkxyf7fj8d5y78vqqvp9lf2") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.6 (c (n "loginit") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "08yhm2pdkai8gf26jcn1s50fmsgj5z2dmnkx2m0hjgflmkfdadd4") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.7 (c (n "loginit") (v "0.1.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0yf80x7p6fr1a4h6pfpwnic6ngnd59dqzhp29wdgqg0q13zjswb4") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.8 (c (n "loginit") (v "0.1.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0j9kygqysglsf646bj5wb532aifl9bmyyikpvrm5ii0nhmxr67rk") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.9 (c (n "loginit") (v "0.1.9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0xk4kpybp4prnilrfk381jv3vc8xwvqxqazrh6mjfyj2ylqm3yil") (f (quote (("stackdriver" "tracing-stackdriver"))))))

(define-public crate-loginit-0.1.10 (c (n "loginit") (v "0.1.10") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.50.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-core") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing-stackdriver") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "058kqzr94n7n8wswwld0csxy8d4h5xqpqzabrdvrgpha3kq0ifmj") (f (quote (("stackdriver" "tracing-stackdriver"))))))

