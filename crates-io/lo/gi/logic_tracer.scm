(define-module (crates-io lo gi logic_tracer) #:use-module (crates-io))

(define-public crate-logic_tracer-0.0.1 (c (n "logic_tracer") (v "0.0.1") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1qai4q706xg2aw7fph9hrahzk2dx6pipb6yfi6g6c25yz45ah8cc")))

(define-public crate-logic_tracer-0.0.2 (c (n "logic_tracer") (v "0.0.2") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0xn3g8g3kfj4p3jmfz1f8kcr0lygqlm86mwq1ig49lsfda9nqsji")))

(define-public crate-logic_tracer-0.0.3 (c (n "logic_tracer") (v "0.0.3") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1bfh9dvlnckcg23v3720ga7hkgdwj84hisgwz8x4mlim55dvywca")))

(define-public crate-logic_tracer-0.0.4 (c (n "logic_tracer") (v "0.0.4") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1956zn2fsqq50cifh2mxgc7mxagm4az7svy3hl6hia5m32yy32f3")))

(define-public crate-logic_tracer-0.0.5 (c (n "logic_tracer") (v "0.0.5") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "1n1i161zfp7pwdhsgiyxbsh7zhxbv4hsppqgw6slwgl24wxxfjz4")))

(define-public crate-logic_tracer-0.0.6 (c (n "logic_tracer") (v "0.0.6") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0l625qipkwwyj64821q499v21iwnmrb87n6q80kqdcaz0zp46g52")))

(define-public crate-logic_tracer-0.0.7 (c (n "logic_tracer") (v "0.0.7") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0230f7h16fnyzibr25sw2lgysf1b5af4sb9qzrw8w6xyrahpnnz6")))

(define-public crate-logic_tracer-0.0.8 (c (n "logic_tracer") (v "0.0.8") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (o #t) (d #t) (k 0)))) (h "02vlq56c6nlkx9djl5g85c9m5qibs631z7br9m3ldkk1s05xzxf8") (f (quote (("sequential") ("regex_test" "regex") ("full" "regex_test") ("default" "full") ("combinational"))))))

(define-public crate-logic_tracer-0.0.9 (c (n "logic_tracer") (v "0.0.9") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (o #t) (d #t) (k 0)))) (h "0jjaqr2qgbcc6h4zszvw3rmd78rx9v4w6gn3n2k1d2fjfk6jlldd") (f (quote (("sequential") ("regex_test" "regex") ("full" "regex_test") ("default" "full") ("combinational"))))))

(define-public crate-logic_tracer-0.0.10 (c (n "logic_tracer") (v "0.0.10") (d (list (d (n "dev_utils") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (o #t) (d #t) (k 0)))) (h "1n2izkbvzh0y2mx0iv6rxxqyfq6m1nrw31dp0ak8shzl7dc4ir7s") (f (quote (("sequential") ("regex_test" "regex") ("full" "regex_test") ("default" "full") ("combinational"))))))

