(define-module (crates-io lo gi logitech-lcd-sys) #:use-module (crates-io))

(define-public crate-logitech-lcd-sys-1.0.0 (c (n "logitech-lcd-sys") (v "1.0.0") (d (list (d (n "enumflags") (r "^0.1.2") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)) (d (n "winreg") (r "^0.4") (d #t) (k 0)))) (h "0f3vwxbqpsq41rdkk1bl10c2pa6v3dqj5xbcq9v4pi2df6z33386") (y #t)))

(define-public crate-logitech-lcd-sys-1.0.1 (c (n "logitech-lcd-sys") (v "1.0.1") (d (list (d (n "enumflags") (r "^0.1.2") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)) (d (n "winreg") (r "^0.4") (d #t) (k 0)))) (h "0cixj1330h236hliafsg3k42awziww5pbsqywrsydsmzpcyklikl") (y #t)))

(define-public crate-logitech-lcd-sys-1.0.2 (c (n "logitech-lcd-sys") (v "1.0.2") (d (list (d (n "enumflags") (r "^0.1.2") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1l398icvcvsgwk59zjzilhv59zs29sij6xrclwp7iwz2kq5ws20y") (y #t)))

(define-public crate-logitech-lcd-sys-1.0.4 (c (n "logitech-lcd-sys") (v "1.0.4") (d (list (d (n "enumflags") (r "^0.1.2") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1n1j5r85bp7lmc4k8qw26ppg71hdfx47n9k4smhnq8ygj7mwlkqp")))

(define-public crate-logitech-lcd-sys-2.0.0 (c (n "logitech-lcd-sys") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0rcn44s8bjr2qisn539fg1pa824zbg0wjh4xl5g6n7wzs544zk1r")))

