(define-module (crates-io lo gi logisheets) #:use-module (crates-io))

(define-public crate-logisheets-0.1.0 (c (n "logisheets") (v "0.1.0") (d (list (d (n "logisheets_controller") (r "^0.1.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.1.0") (d #t) (k 0)))) (h "1kxhs86a3xiax9raz72m5rm98kzdj2vm4ywd6phfvcnncx2z3cxp")))

(define-public crate-logisheets-0.2.0 (c (n "logisheets") (v "0.2.0") (d (list (d (n "logisheets_controller") (r "^0.2.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.2.0") (d #t) (k 0)))) (h "101jsxla3gwkbgxpkl5b44yii7inhl8g0hr92czq70zhy58cafi2")))

(define-public crate-logisheets-0.2.1 (c (n "logisheets") (v "0.2.1") (d (list (d (n "logisheets_controller") (r "^0.2.1") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.2.1") (d #t) (k 0)))) (h "0k3cv668z46cdyzfs1v14k1ma1vsw1hvmh4mpa8c4k8an3abdvm6")))

(define-public crate-logisheets-0.3.0 (c (n "logisheets") (v "0.3.0") (d (list (d (n "logisheets_controller") (r "^0.3.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.3.0") (d #t) (k 0)))) (h "1m3n1scrgfdrgrd128kx4y5jl8g5iqwwpxsjf19j82937z6wpicz")))

(define-public crate-logisheets-0.4.0 (c (n "logisheets") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "logiscript") (r "^0.1.0") (d #t) (k 2)) (d (n "logisheets_controller") (r "^0.4.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.4.0") (d #t) (k 0)))) (h "1qxh1i757a4sml45cj703iq59yvhs11f2zh5q6xv02qa2bksrc89")))

(define-public crate-logisheets-0.5.0 (c (n "logisheets") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "logiscript") (r "^0.1.0") (d #t) (k 2)) (d (n "logisheets_controller") (r "^0.5.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.5.0") (d #t) (k 0)))) (h "0ybin62pmm757csm04sq7y70k45yihfjhsv783sadnxypk39kxaz")))

(define-public crate-logisheets-0.6.0 (c (n "logisheets") (v "0.6.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "logiscript") (r "^0.1.0") (d #t) (k 2)) (d (n "logisheets_controller") (r "^0.6.0") (d #t) (k 0)) (d (n "logisheets_workbook") (r "^0.6.0") (d #t) (k 0)))) (h "1q90wgbh9aigz0mi9s8scbpbmmh0j4rbgn450n3plhlk7zwjwsvs")))

