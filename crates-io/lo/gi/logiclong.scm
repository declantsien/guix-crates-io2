(define-module (crates-io lo gi logiclong) #:use-module (crates-io))

(define-public crate-logiclong-0.1.0 (c (n "logiclong") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y4crdwj46323g0lw6dhgs8hk3rnh8d0is3c62x2a9nvn030v0jm")))

