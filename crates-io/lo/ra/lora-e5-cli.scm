(define-module (crates-io lo ra lora-e5-cli) #:use-module (crates-io))

(define-public crate-lora-e5-cli-0.1.1 (c (n "lora-e5-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "lora-e5") (r "^0.1.1") (f (quote ("runtime"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "sync" "signal" "rt" "process" "time"))) (k 0)))) (h "122rmimw69kv99r3x0h7wgdrd1p36j0i57kzvj73fld4xzpy752g")))

