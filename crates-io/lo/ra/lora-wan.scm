(define-module (crates-io lo ra lora-wan) #:use-module (crates-io))

(define-public crate-lora-wan-0.1.1 (c (n "lora-wan") (v "0.1.1") (d (list (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "rfm9x") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 2)))) (h "1jb20ncbn1i8q995rp6iyvajyjv9hlpd6y7w156xpc6ypzcd2civ")))

(define-public crate-lora-wan-0.1.2 (c (n "lora-wan") (v "0.1.2") (d (list (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "rfm9x") (r "^0.1.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 2)))) (h "0hsnizwwn1k5qmcb030r1s9vd5yl98bz9mivi4sgssq9njjr2vsh")))

