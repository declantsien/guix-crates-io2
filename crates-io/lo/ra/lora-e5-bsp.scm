(define-module (crates-io lo ra lora-e5-bsp) #:use-module (crates-io))

(define-public crate-lora-e5-bsp-0.2.0 (c (n "lora-e5-bsp") (v "0.2.0") (d (list (d (n "stm32wlxx-hal") (r "=0.2.0") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "0xl00gkzgqsbglvbaribak8s9y9j2hwz2v3bcjv6mzxkmrzxl2n7") (f (quote (("rt" "stm32wlxx-hal/rt") ("defmt" "stm32wlxx-hal/defmt")))) (r "1.56")))

(define-public crate-lora-e5-bsp-0.2.1 (c (n "lora-e5-bsp") (v "0.2.1") (d (list (d (n "stm32wlxx-hal") (r "=0.2.1") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "1yvmnhkyzqcgq59zk817zwr8pz5ywnjs48hhdngkqgmp3ha1h6wf") (f (quote (("rt" "stm32wlxx-hal/rt") ("defmt" "stm32wlxx-hal/defmt")))) (r "1.56")))

(define-public crate-lora-e5-bsp-0.3.0 (c (n "lora-e5-bsp") (v "0.3.0") (d (list (d (n "dfmt") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "stm32wlxx-hal") (r "=0.3.0") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "163lpfr99l6piay3fgagjv261ziyn25vwj56wqsyl508njqw3aai") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("defmt" "stm32wlxx-hal/defmt" "dfmt") ("chrono" "stm32wlxx-hal/chrono")))) (r "1.57")))

(define-public crate-lora-e5-bsp-0.4.0 (c (n "lora-e5-bsp") (v "0.4.0") (d (list (d (n "dfmt") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "stm32wlxx-hal") (r "=0.4.0") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "0w8abya4ik7yr4wyg0hmys7db8nczc2zkkyxxc8skz1019v8rw1l") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("defmt" "stm32wlxx-hal/defmt" "dfmt") ("chrono" "stm32wlxx-hal/chrono")))) (r "1.57")))

(define-public crate-lora-e5-bsp-0.4.1 (c (n "lora-e5-bsp") (v "0.4.1") (d (list (d (n "dfmt") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "stm32wlxx-hal") (r "=0.4.1") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "07fy3i35bxj2ck14vmg3v5alyn32qsrq8szg1sklfw96bxpyk8bm") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("defmt" "stm32wlxx-hal/defmt" "dfmt") ("chrono" "stm32wlxx-hal/chrono")))) (r "1.57")))

(define-public crate-lora-e5-bsp-0.5.0 (c (n "lora-e5-bsp") (v "0.5.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stm32wlxx-hal") (r "=0.5.0") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "1dggs010dyx94qs4cmycw3bbc41lbnqdz84a3wfkyw90lcq0rxqa") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("chrono" "stm32wlxx-hal/chrono")))) (s 2) (e (quote (("defmt" "stm32wlxx-hal/defmt" "dep:defmt")))) (r "1.60")))

(define-public crate-lora-e5-bsp-0.5.1 (c (n "lora-e5-bsp") (v "0.5.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stm32wlxx-hal") (r "=0.5.1") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "0dx70kbzz1990mf2m9rmv5yc1bz1a2wy6cf7kwkdzafvjcsm2wp5") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("chrono" "stm32wlxx-hal/chrono")))) (s 2) (e (quote (("defmt" "stm32wlxx-hal/defmt" "dep:defmt")))) (r "1.60")))

(define-public crate-lora-e5-bsp-0.6.0 (c (n "lora-e5-bsp") (v "0.6.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stm32wlxx-hal") (r "=0.6.0") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "0nghjrg78y8060df3zswy1f90285aga1fv9jymwhhwzhyxincwf6") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("chrono" "stm32wlxx-hal/chrono")))) (s 2) (e (quote (("defmt" "stm32wlxx-hal/defmt" "dep:defmt")))) (r "1.60")))

(define-public crate-lora-e5-bsp-0.6.1 (c (n "lora-e5-bsp") (v "0.6.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stm32wlxx-hal") (r "=0.6.1") (f (quote ("stm32wle5"))) (d #t) (k 0)))) (h "0hfqvq8zaxwpn8da517rbigs8dk0g6aqsla9pq9cggxx35044pbh") (f (quote (("rt" "stm32wlxx-hal/rt") ("embedded-time" "stm32wlxx-hal/embedded-time") ("chrono" "stm32wlxx-hal/chrono")))) (s 2) (e (quote (("defmt" "stm32wlxx-hal/defmt" "dep:defmt")))) (r "1.60")))

