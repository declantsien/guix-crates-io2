(define-module (crates-io lo ra lorawan-encoding) #:use-module (crates-io))

(define-public crate-lorawan-encoding-0.6.1 (c (n "lorawan-encoding") (v "0.6.1") (d (list (d (n "aes") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "cmac") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)))) (h "1w1j1axsrabm6qclbsvzimp1wf1b5ki07b32ddih99c3afmj9844") (f (quote (("with-to-string") ("with-downlink") ("full" "with-to-string" "with-downlink" "default-crypto") ("default-crypto" "aes" "cmac") ("default" "full"))))))

(define-public crate-lorawan-encoding-0.6.2 (c (n "lorawan-encoding") (v "0.6.2") (d (list (d (n "aes") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "cmac") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)))) (h "06d7p1w8pc7qflz4gd5d4bifvh9mbrrg0kypc52zj3v9cp3czc5x") (f (quote (("with-to-string") ("with-downlink") ("full" "with-to-string" "with-downlink" "default-crypto") ("default-crypto" "aes" "cmac") ("default" "full"))))))

