(define-module (crates-io lo ra lora-modulation) #:use-module (crates-io))

(define-public crate-lora-modulation-0.1.0 (c (n "lora-modulation") (v "0.1.0") (d (list (d (n "defmt") (r "^0") (o #t) (d #t) (k 0)) (d (n "lora-phy") (r "^1") (o #t) (d #t) (k 0)))) (h "194lrsdm99fx8qzw3ygvhy6qfbpi014cpispa2mrl5c9b15hnh5v") (s 2) (e (quote (("external-lora-phy" "dep:lora-phy") ("defmt" "dep:defmt"))))))

(define-public crate-lora-modulation-0.1.1 (c (n "lora-modulation") (v "0.1.1") (d (list (d (n "defmt") (r "^0") (o #t) (d #t) (k 0)) (d (n "lora-phy") (r "^1") (o #t) (d #t) (k 0)))) (h "09isv1cy636n9jwy8ljad3w24jxvny8spjjgimpdsfd7dl641p6p") (s 2) (e (quote (("external-lora-phy" "dep:lora-phy") ("defmt" "dep:defmt"))))))

(define-public crate-lora-modulation-0.1.2 (c (n "lora-modulation") (v "0.1.2") (d (list (d (n "defmt") (r "^0") (o #t) (d #t) (k 0)) (d (n "lora-phy") (r "^1") (o #t) (d #t) (k 0)))) (h "0rhmx423mfp8d2wp9q0v21hj6lf95a9d6fwzkdbx9ljmf876b8rc") (s 2) (e (quote (("external-lora-phy" "dep:lora-phy") ("defmt" "dep:defmt"))))))

(define-public crate-lora-modulation-0.1.3 (c (n "lora-modulation") (v "0.1.3") (d (list (d (n "defmt") (r "^0") (o #t) (d #t) (k 0)))) (h "0g6qadapdxc9845f479ny6z7p4ns3n479fydq1rm2ijkib51qbxg") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-lora-modulation-0.1.4 (c (n "lora-modulation") (v "0.1.4") (d (list (d (n "defmt") (r "^0") (o #t) (d #t) (k 0)))) (h "075lcgb9g8fwiigg259m3ih25rmqzn9v0vm7wxa9s6y7vakd357y") (s 2) (e (quote (("defmt" "dep:defmt"))))))

