(define-module (crates-io lo ra lora-e5) #:use-module (crates-io))

(define-public crate-lora-e5-0.1.0 (c (n "lora-e5") (v "0.1.0") (d (list (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "serialport") (r "^4") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "sync" "signal" "rt" "process" "time"))) (o #t) (k 0)))) (h "0kd4jy8kd5cxs9mm8ms0w7l4hgckvf8qvvnl5sl5vrqwmshhp0mi") (f (quote (("runtime" "tokio") ("libudev" "serialport/libudev") ("default"))))))

(define-public crate-lora-e5-0.1.1 (c (n "lora-e5") (v "0.1.1") (d (list (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "serialport") (r "^4") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "sync" "signal" "rt" "process" "time"))) (o #t) (k 0)))) (h "1xpnyj7zhn2yxhyxgggmh1z9050w9ks0jkw4imw968mndxmhikfp") (f (quote (("runtime" "tokio") ("libudev" "serialport/libudev") ("default"))))))

