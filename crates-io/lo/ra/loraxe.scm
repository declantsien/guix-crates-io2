(define-module (crates-io lo ra loraxe) #:use-module (crates-io))

(define-public crate-loraxe-0.1.0 (c (n "loraxe") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1ghz6vk1m23mkmj9vjbq4ngsicc30xmd5p99cgnnn5nyzhflnq1z")))

