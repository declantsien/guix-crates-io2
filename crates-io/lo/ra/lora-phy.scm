(define-module (crates-io lo ra lora-phy) #:use-module (crates-io))

(define-public crate-lora-phy-1.0.0 (c (n "lora-phy") (v "1.0.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "00d14q0smmz0ypvjwk8mz30nnpywhlp7l6nv5ms47z8i84igv5kf")))

(define-public crate-lora-phy-1.0.1 (c (n "lora-phy") (v "1.0.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "01jca5mpb2bv58zl0algihyn1gp5wkwa1zhig814jrrfry7lxxds")))

(define-public crate-lora-phy-1.0.2 (c (n "lora-phy") (v "1.0.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fsk1qr06hpgbly7h6mkdryzgqzc44m3dsmk684q6zps84rn2x6j")))

(define-public crate-lora-phy-1.1.0 (c (n "lora-phy") (v "1.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "18cagvqg0ckyxciqkd0xnz51jnypq0rzrx8rydww8fl8bipb2xxb")))

(define-public crate-lora-phy-1.2.0 (c (n "lora-phy") (v "1.2.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1hy6kng27s4j5c13yzray7dyvm3qcv1a8w1gymmifpgfdklygl6q")))

(define-public crate-lora-phy-2.0.0 (c (n "lora-phy") (v "2.0.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1px253dpkz86xw8yl9c28rcyng08y0hc2s0yw4p2wwwcl53brxkl")))

(define-public crate-lora-phy-2.1.0 (c (n "lora-phy") (v "2.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1ms3ara2g4s1kzw8cbqdnkvypr8ails2qz6im3zr4wlmp3npdmr7")))

(define-public crate-lora-phy-1.2.1 (c (n "lora-phy") (v "1.2.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=0.2.0-alpha.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0kckw62bzkjrj3fjs7smsdzgzy906ifxlnii6kd6557m0zr41hhv")))

(define-public crate-lora-phy-2.1.1 (c (n "lora-phy") (v "2.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lora-modulation") (r ">=0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0dkj97fz3i9ra4gc6fis0lw91q9db4zkx8k40d6ig1p24vqcig5n")))

(define-public crate-lora-phy-2.1.2 (c (n "lora-phy") (v "2.1.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lora-modulation") (r ">=0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "05ay505d35fblay6zjhkcyfcrwwjsjnrqv3wisimvzv5wk5cgkz2")))

(define-public crate-lora-phy-3.0.0 (c (n "lora-phy") (v "3.0.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "lora-modulation") (r ">=0.1.2") (d #t) (k 0)) (d (n "lorawan-device") (r "^0.12") (f (quote ("defmt"))) (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "04l82xgi7pj3zvqf4sf6c2nfrqsxf37hkr7vav3nzknn7pq30bjr") (s 2) (e (quote (("lorawan-radio" "dep:lorawan-device"))))))

