(define-module (crates-io lo cu locutus-macros) #:use-module (crates-io))

(define-public crate-locutus-macros-0.0.1 (c (n "locutus-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6bw282xczl008gd5z6yjsa2mgw2j53phi6iq0cx15wy9ij3n6n") (r "1.58.0")))

(define-public crate-locutus-macros-0.0.2 (c (n "locutus-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rl03x50pkd0348d6njv855wkwz9hbjwzrp25y6l3laxapa6cyjc") (r "1.58.0")))

(define-public crate-locutus-macros-0.0.3 (c (n "locutus-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r094knzng9f5xdj67ap28r17kgqvmfb3p42hkjyij39nglw5l9b") (r "1.58.0")))

