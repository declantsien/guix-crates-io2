(define-module (crates-io lo fi lofi) #:use-module (crates-io))

(define-public crate-lofi-0.0.9 (c (n "lofi") (v "0.0.9") (d (list (d (n "coco") (r "^0.1") (d #t) (k 0)) (d (n "context") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "0gn1sq4b54hfi71wih5xbxz3hwh9hv32ar8bc3z49sb5dx5s4g79")))

(define-public crate-lofi-0.0.10 (c (n "lofi") (v "0.0.10") (d (list (d (n "coco") (r "^0.1") (d #t) (k 0)) (d (n "context") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1gbfvznibg9zp9ww3s7qr52fzqs0xc7bbylivjg7zgjqx3rdampi")))

(define-public crate-lofi-0.0.11 (c (n "lofi") (v "0.0.11") (d (list (d (n "coco") (r "^0.1") (d #t) (k 0)) (d (n "context") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "07m2kc1s7052w2s1r36zw1swi4q5la4xmha2nj0qqqh1pja25c8k")))

(define-public crate-lofi-0.1.0 (c (n "lofi") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.0") (f (quote ("boxed"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0z3xfz36lglclawkpm65srbnxcacid5fvh66pvby60bidslqh630") (f (quote (("fallback-alloc-always" "fallback-alloc") ("fallback-alloc") ("default" "fallback-alloc") ("dealloc-on-drop"))))))

(define-public crate-lofi-0.2.0 (c (n "lofi") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.0") (f (quote ("boxed"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1l1l5xrh57ihcpjgh1fvrzzci2khwpshj8nh4qid9vnvymqqhc1y") (f (quote (("fallback-alloc-always" "fallback-alloc") ("fallback-alloc") ("default" "fallback-alloc") ("dealloc-on-drop"))))))

