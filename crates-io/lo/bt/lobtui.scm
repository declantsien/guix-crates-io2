(define-module (crates-io lo bt lobtui) #:use-module (crates-io))

(define-public crate-lobtui-0.1.0 (c (n "lobtui") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^5") (d #t) (k 0)) (d (n "ratatui") (r "^0.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qak565b14njvx6di9f9y5r0fgdc8vb74nik8v34bm1ki6hw44x9")))

(define-public crate-lobtui-0.2.0 (c (n "lobtui") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05ljihqydbk6z9zpngd0hbck2wpwcln1ics30vqw7w0iabwfk7r9")))

