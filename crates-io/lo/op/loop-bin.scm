(define-module (crates-io lo op loop-bin) #:use-module (crates-io))

(define-public crate-loop-bin-0.1.0 (c (n "loop-bin") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0vvfks468fg8cj8cyp74j7fkx02f6bai612lygnwlkk1m0v3v9vj")))

(define-public crate-loop-bin-0.1.1 (c (n "loop-bin") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1f06f6wj6zlafdc47qs3j30wgcqnfqimr3swc0mriqrpfmaw972s")))

(define-public crate-loop-bin-0.1.2 (c (n "loop-bin") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (f (quote ("termination"))) (d #t) (k 0)))) (h "11j89m0f4hgyp9q466i08a1gi0aa8iaihwz3qdjdxw2i4nk5k7gk")))

(define-public crate-loop-bin-0.1.3 (c (n "loop-bin") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("string"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (f (quote ("termination"))) (d #t) (k 0)))) (h "07fm7p8j7c4nz2wyxvmnmc4ad7lwaiijwkvg62v4mddx1156ajv1")))

