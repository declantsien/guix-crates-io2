(define-module (crates-io lo op loopdev-erikh) #:use-module (crates-io))

(define-public crate-loopdev-erikh-0.5.0 (c (n "loopdev-erikh") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.63") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "gpt") (r "^3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13cv67d5igd28777n3cywj65q28kcsp87ix7ld5nncm3hg3ay65c") (f (quote (("direct_io"))))))

