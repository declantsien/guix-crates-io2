(define-module (crates-io lo op looper) #:use-module (crates-io))

(define-public crate-looper-0.0.1 (c (n "looper") (v "0.0.1") (h "1a114d8j5sb51zxik9z4k2xvhnxbdp55kvhd3g2fxv4xb4af08f8")))

(define-public crate-looper-0.0.2 (c (n "looper") (v "0.0.2") (h "0fwslhdkmvgpc5vx4r41198z7mazymi6fzky8y2fxbqwabndq800")))

