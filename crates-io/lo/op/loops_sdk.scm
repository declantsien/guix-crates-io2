(define-module (crates-io lo op loops_sdk) #:use-module (crates-io))

(define-public crate-loops_sdk-1.0.0 (c (n "loops_sdk") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1v8p5c8jbhk44fs5a0h72z01mli8mbsawyhi5r29clqkws0vrbd5")))

