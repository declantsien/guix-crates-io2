(define-module (crates-io lo op loop_chain) #:use-module (crates-io))

(define-public crate-loop_chain-0.1.0-beta.0 (c (n "loop_chain") (v "0.1.0-beta.0") (h "0j9na08lj49m6ansd8xx6m1diwbgcdr33ylwv2kcsgcm8gajbvqj")))

(define-public crate-loop_chain-0.1.0 (c (n "loop_chain") (v "0.1.0") (h "0mn6shsqw683pyqvy367r54v0iq6sixf4d25k12d85r24xw7gff2")))

(define-public crate-loop_chain-0.1.1 (c (n "loop_chain") (v "0.1.1") (h "1cbbswbl74s1bnnjx2npgyra3c63jdc1rvadyamrpkqcv56bwbj5")))

