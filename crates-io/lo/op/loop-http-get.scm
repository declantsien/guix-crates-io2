(define-module (crates-io lo op loop-http-get) #:use-module (crates-io))

(define-public crate-loop-http-get-1.0.0 (c (n "loop-http-get") (v "1.0.0") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "1dy9wsw6i3aj6xmi76w905jzrvj04137f0b1hb3vf9g0zji5y7wc")))

(define-public crate-loop-http-get-1.0.1 (c (n "loop-http-get") (v "1.0.1") (d (list (d (n "minreq") (r "^2.8.1") (d #t) (k 0)))) (h "10xba70gnc4mv50fx85pa251nnl763g57nqqf14c0dqa7v952km5") (r "1.60")))

(define-public crate-loop-http-get-1.0.2 (c (n "loop-http-get") (v "1.0.2") (d (list (d (n "minreq") (r "^2.9.0") (d #t) (k 0)))) (h "0y85m02qg3l1141zyz0rn0nrbd41xb3fn65biw28rkll6dqhb684") (r "1.56")))

