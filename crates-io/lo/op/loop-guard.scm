(define-module (crates-io lo op loop-guard) #:use-module (crates-io))

(define-public crate-loop-guard-0.1.0 (c (n "loop-guard") (v "0.1.0") (h "1gabjkf96gikk8xinbchh1vz6475zbrkblfbfr89vk8badq0zi7v") (y #t)))

(define-public crate-loop-guard-0.1.1 (c (n "loop-guard") (v "0.1.1") (h "1lkh7war49hwk9qji83jy73lz1r6165bg6zz6x5c0plm3rv4r54l") (y #t)))

(define-public crate-loop-guard-0.1.2 (c (n "loop-guard") (v "0.1.2") (h "1bg9aix5kqxy2z354vp3ccn2bdggrhjcw80ihfc29mknlhgrrpy6") (y #t)))

(define-public crate-loop-guard-0.1.3 (c (n "loop-guard") (v "0.1.3") (h "1gsm5ih7s660nnblgl3mz81jcc4fa5ixmfj5idlcsxd856mmxih8") (y #t)))

(define-public crate-loop-guard-0.1.4 (c (n "loop-guard") (v "0.1.4") (h "0akzp2m2zjfq5y7djqalqql6a3b0xgvc8s3x9f9d68cq41ciwn5d")))

(define-public crate-loop-guard-1.0.0 (c (n "loop-guard") (v "1.0.0") (h "1vvcxcjrzgzjfvn8w0n35jbn8nqdk6b7y08rvfrn8hildibz48is")))

