(define-module (crates-io lo op loopdev) #:use-module (crates-io))

(define-public crate-loopdev-0.1.0 (c (n "loopdev") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0sg6m5x4rhqqxnwg203g6lphfmzlpss27jpnypw59vd5bxzkbm66")))

(define-public crate-loopdev-0.1.1 (c (n "loopdev") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1is0badxbjvhlczizqwspw97jqppk3b8lfqxp6bpllisp758kgby")))

(define-public crate-loopdev-0.1.2 (c (n "loopdev") (v "0.1.2") (d (list (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19l11q1fqjpr39xp6cgzm5mq519p6ps9cjy7y4pnb5qv26avqiky")))

(define-public crate-loopdev-0.1.3 (c (n "loopdev") (v "0.1.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jgqc937q58z3bj97bh3pv3r990vpx4p56ia9vnxssix7y41mzx2")))

(define-public crate-loopdev-0.2.0 (c (n "loopdev") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fmyhs8xcsz885yxhibq7x1skjm3cgaq3z0n73f32gz0yjfymkgc")))

(define-public crate-loopdev-0.2.1 (c (n "loopdev") (v "0.2.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k3g7prrfllp4gs3s41w0lrhdip6j0x93a1ckw2nfvb4nv7kb7mc")))

(define-public crate-loopdev-0.2.2 (c (n "loopdev") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "11c48vay2h3sy49x5rnmzfp69fxkmrqm4gg82p0vznh5k62vnns0")))

(define-public crate-loopdev-0.3.0 (c (n "loopdev") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "16lb5mvb3air1n1hmj42s6ii7hx8rpsdhf6bs34nqq1ync3fzh4l") (f (quote (("direct_io"))))))

(define-public crate-loopdev-0.4.0 (c (n "loopdev") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "123kkyqllscwkngx6db5q7gws2g8x518awgzra5f64a6n1ahiyjv") (f (quote (("direct_io"))))))

