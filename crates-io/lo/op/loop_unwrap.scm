(define-module (crates-io lo op loop_unwrap) #:use-module (crates-io))

(define-public crate-loop_unwrap-0.1.0 (c (n "loop_unwrap") (v "0.1.0") (h "10b9m4f2xrp769b95xla5q7faknnq45d7vb4r33109vcpcp2mjhv") (y #t)))

(define-public crate-loop_unwrap-0.1.1 (c (n "loop_unwrap") (v "0.1.1") (h "0mcrpczm8zwaj8aajjm8gk831cwww18mwnyzckhymm6fss3mkx29") (y #t)))

(define-public crate-loop_unwrap-0.1.2 (c (n "loop_unwrap") (v "0.1.2") (h "1x42yk9b3sfxvwrg47yah3g5qh8fd0mmp8kdcl6i1m7dnaknj7lc") (y #t)))

(define-public crate-loop_unwrap-0.1.3 (c (n "loop_unwrap") (v "0.1.3") (h "16l65z08rx99zzyhn034mkp1i1pw5n4izqr0ijqcfw97l3k27rin")))

(define-public crate-loop_unwrap-0.2.0 (c (n "loop_unwrap") (v "0.2.0") (h "17lz7c4rgpv7d29f32p9sgbjn4ljv8nrwxs897m29bww9qqfc109")))

