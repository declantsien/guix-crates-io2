(define-module (crates-io lo op loop-forever) #:use-module (crates-io))

(define-public crate-loop-forever-0.2.0 (c (n "loop-forever") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "12nbmpmfzvc3y66knfjvlayhpy4kw5ihh2w73z83d9f23yfjrf68")))

(define-public crate-loop-forever-0.2.1 (c (n "loop-forever") (v "0.2.1") (h "1vz8dvq6q64r9z1vsyxqh1c7fjh4vbvygxw4vm5bgkjiw7cz7v6v")))

(define-public crate-loop-forever-1.0.0 (c (n "loop-forever") (v "1.0.0") (d (list (d (n "spin") (r "^0.3") (d #t) (k 0)))) (h "0a9np8r2a0mplnw02z6z6k1cix83y8m7ylkhx30nzi9aa0hpzxcx")))

(define-public crate-loop-forever-1.0.2 (c (n "loop-forever") (v "1.0.2") (h "0b0rvbzfhydmp1b5v15wc9yj21a1friq6418gkfal1pbn3dy5g44")))

