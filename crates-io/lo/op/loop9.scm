(define-module (crates-io lo op loop9) #:use-module (crates-io))

(define-public crate-loop9-0.1.1 (c (n "loop9") (v "0.1.1") (d (list (d (n "imgref") (r "^1.2.0") (d #t) (k 0)))) (h "0x56c7g3ik7l2xcf891dp9m8mk6myhq1divaksislkm6hh900ymr")))

(define-public crate-loop9-0.1.2 (c (n "loop9") (v "0.1.2") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)))) (h "1jg0fcz2pq9yqd7vg2gdgq9h742p05rhx06jpzbs21zc73ghf67w")))

(define-public crate-loop9-0.1.3 (c (n "loop9") (v "0.1.3") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)))) (h "0h4rys8001cdq4l2f30k66wmvscm4lb2laxgpia794p5652800x7")))

(define-public crate-loop9-0.1.4 (c (n "loop9") (v "0.1.4") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)))) (h "17b9mbdy01m7crm0vasw6818s80k35fjbfiinlddchfy2zwkga41")))

(define-public crate-loop9-0.1.5 (c (n "loop9") (v "0.1.5") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)))) (h "0qphc1c0cbbx43pwm6isnwzwbg6nsxjh7jah04n1sg5h4p0qgbhg")))

