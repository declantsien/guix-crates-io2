(define-module (crates-io lo op loop-stream) #:use-module (crates-io))

(define-public crate-loop-stream-1.0.0 (c (n "loop-stream") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fzjy88m2jkfgbirrmaq2r64gaayr908p9vr0hy8pldly0qd99z9")))

(define-public crate-loop-stream-1.0.1 (c (n "loop-stream") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dgixghr2yg1xbxn58i998wk2cscmf659j35i1yds548s4dh16s6")))

