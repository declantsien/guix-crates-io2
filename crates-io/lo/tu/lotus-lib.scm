(define-module (crates-io lo tu lotus-lib) #:use-module (crates-io))

(define-public crate-lotus-lib-0.1.0 (c (n "lotus-lib") (v "0.1.0") (d (list (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0wpymi8j9rxlgpk1wg2a8q74w1xf9q6c8i95n1mkldaqacb1rkr6") (y #t)))

(define-public crate-lotus-lib-0.2.0 (c (n "lotus-lib") (v "0.2.0") (d (list (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0f5195k404y7vjld5dlk3xvxv42fazdpacdbi995c8fvqf8zkjqg") (y #t)))

(define-public crate-lotus-lib-0.3.0 (c (n "lotus-lib") (v "0.3.0") (d (list (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0645751zkb180i5r96rfdwv79xnhdsa1aa8gdj0d3qc11dkf3lvz") (y #t)))

(define-public crate-lotus-lib-0.4.0 (c (n "lotus-lib") (v "0.4.0") (d (list (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "052fdayijr4m623n1xwzawzdcns41h63gx24qj79jrk38xypl4pz") (y #t)))

(define-public crate-lotus-lib-0.5.0 (c (n "lotus-lib") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "08mdx3q812d0337r4zniymajvwf9cwm1ihx80slafp5dfxhdwf8c") (y #t)))

(define-public crate-lotus-lib-0.5.1 (c (n "lotus-lib") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1gmbs1br1ci0lnxspvrr91g9zzbc3gw2cr7qvndnvnm9a883kqlp") (y #t)))

(define-public crate-lotus-lib-0.5.2 (c (n "lotus-lib") (v "0.5.2") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1wpkv6jrhjxjggac6w5hh8ijq3z92vd4jc4m851pzb9npscdizcf") (y #t)))

(define-public crate-lotus-lib-0.5.3 (c (n "lotus-lib") (v "0.5.3") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1hwbccdfwgdb8xprwh1arj6f7p747bhhr5qmrcxf6pad4nck1yfc")))

(define-public crate-lotus-lib-0.6.0 (c (n "lotus-lib") (v "0.6.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "ooz-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0j26y7gapn72y2v30wavinj9iz1ankq38pyhdrr40szadz10kgfw")))

(define-public crate-lotus-lib-0.6.1 (c (n "lotus-lib") (v "0.6.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1.0") (d #t) (k 0)))) (h "0xm9yimd41m7qv3wnsdsq9mbkd2krnxfpdhny5026h2lxi1ndhsg")))

(define-public crate-lotus-lib-1.0.0 (c (n "lotus-lib") (v "1.0.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1.0") (d #t) (k 0)))) (h "029sj6jzz256hyc22g1wzhk5hw5mn7pna37ap1kzv9wg093kacb9")))

(define-public crate-lotus-lib-1.0.1 (c (n "lotus-lib") (v "1.0.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1.1") (d #t) (k 0)))) (h "1l8n07xmag502hap37ch5bqsr0jniz5vg5ksdp8vw0j58jfr2zzi")))

(define-public crate-lotus-lib-1.0.2 (c (n "lotus-lib") (v "1.0.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "1aswg2mjg27q1p0pslk3syjzqsdbm07szpl9hk82fqzf49ms3450")))

(define-public crate-lotus-lib-2.0.0 (c (n "lotus-lib") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "0jh1dx4292zvfgjacjaminwp441wmqy1q387685b7g9gpxnpj2p7")))

(define-public crate-lotus-lib-2.1.0 (c (n "lotus-lib") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "0kgxv6c3cwzm69qzl4g0wg3kp2106wvaqlkvmyjmhsxngz3zhwxq")))

(define-public crate-lotus-lib-2.2.0 (c (n "lotus-lib") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "18gz8rgnwpinwirnzcii99ivhl7hgqg33mm9qirsi5cdzmpwdl4m")))

(define-public crate-lotus-lib-2.2.1 (c (n "lotus-lib") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "1ylahqf03nzy6kjvkykbfpwnv2cvdyfcgf82vwl5r04nvwhphjya")))

(define-public crate-lotus-lib-2.2.2 (c (n "lotus-lib") (v "2.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oozle") (r "^0.1") (d #t) (k 0)))) (h "0crjrid37fy89dxqgxs86psdh11kvwwc0cx5dpqc45h1ln7dsrmz")))

(define-public crate-lotus-lib-3.0.0 (c (n "lotus-lib") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oodle-sys") (r "^0.1") (d #t) (k 0)) (d (n "rctree") (r "^0.6.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1virdxkhdbiqgmgl07bq2z7as5w3b91p3gakkjzmlzrrplcq4fhc")))

(define-public crate-lotus-lib-3.0.1 (c (n "lotus-lib") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oodle-safe") (r "^0.1") (d #t) (k 0)) (d (n "rctree") (r "^0.6.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "18k6d66w3gir08x6h5z3il892zji0rapmyjvx9ajs1cgh268dkzp") (f (quote (("pre_ensmallening") ("post_ensmallening") ("internal" "post_ensmallening" "pre_ensmallening"))))))

(define-public crate-lotus-lib-4.0.0 (c (n "lotus-lib") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oodle-safe") (r "^0.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f5sycr6p80qihv4yr7qpnnbk6riz0l54xb3qbaz22msf6p2f633") (f (quote (("pre_ensmallening") ("post_ensmallening") ("internal" "post_ensmallening" "pre_ensmallening"))))))

(define-public crate-lotus-lib-4.0.1 (c (n "lotus-lib") (v "4.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "arctree") (r "^0.1.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9.5") (d #t) (k 0)) (d (n "oodle-safe") (r "^0.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a2nwcq57hz2ml0r5n9ksgbrb9c6i80ghn0m7jn3f24p8s3ysj8z") (f (quote (("pre_ensmallening") ("post_ensmallening") ("internal" "post_ensmallening" "pre_ensmallening"))))))

