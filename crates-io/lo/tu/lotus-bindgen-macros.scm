(define-module (crates-io lo tu lotus-bindgen-macros) #:use-module (crates-io))

(define-public crate-lotus-bindgen-macros-0.1.0 (c (n "lotus-bindgen-macros") (v "0.1.0") (h "15vd61qrspc53wxf63m83y7ym05ph21w3d2r444qzg9qc4vifga8")))

(define-public crate-lotus-bindgen-macros-0.1.1 (c (n "lotus-bindgen-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1wz5zrydh8bc3zwfmrgff0mxnkciicwmyrssqszwx7a6qzw61p0w")))

