(define-module (crates-io lo tu lotus-audio-utils) #:use-module (crates-io))

(define-public crate-lotus-audio-utils-0.1.0 (c (n "lotus-audio-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "3.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "1sl73f5f6jfmk8rs614829h0sxz7chp4hl3pn4agjs79yki40yw3") (y #t)))

