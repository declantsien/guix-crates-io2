(define-module (crates-io lo tu lotus-utils-audio) #:use-module (crates-io))

(define-public crate-lotus-utils-audio-0.1.0 (c (n "lotus-utils-audio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "3.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "0asp7z014s5xdpsdk7nvr9v8aw1j2sdqf789zc29430jm5pkcr4f")))

(define-public crate-lotus-utils-audio-0.1.1 (c (n "lotus-utils-audio") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "0bgvwv5lqzjfp0l5h7p73fhnjh74l01dmb31f290gix2m1pikkh3")))

(define-public crate-lotus-utils-audio-0.2.0 (c (n "lotus-utils-audio") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "1vfs0f5ysg3ycmr4pbnrm3kiqzdv4azjq0gqccfbvipnnrhyaqn4")))

(define-public crate-lotus-utils-audio-0.2.1 (c (n "lotus-utils-audio") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "^4.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)))) (h "08b4cwy20vkf53qbw3azvjxg2ngkrav036j966sswn80i4dz7bik")))

