(define-module (crates-io lo tu lotus) #:use-module (crates-io))

(define-public crate-Lotus-0.1.1 (c (n "Lotus") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.7.1") (d #t) (k 0)))) (h "15gqplsmwl38gw38vhf20pmi0fb1qn489xc103pr0z7wkn1wsz2b")))

(define-public crate-Lotus-0.2.1 (c (n "Lotus") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.7.1") (d #t) (k 0)))) (h "05gi7lkfjlzlkhz1sppqg6xhmpnafac4v1vq5vlzi3lk102dm6zh")))

