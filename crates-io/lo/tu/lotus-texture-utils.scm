(define-module (crates-io lo tu lotus-texture-utils) #:use-module (crates-io))

(define-public crate-lotus-texture-utils-0.1.0 (c (n "lotus-texture-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "ddsfile") (r "^0.5.2") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lotus-lib") (r "3.*") (f (quote ("internal"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c04g257y669v7sw292jxly4ppai020cfknyqsw4rfvgjf6kdrsl") (y #t)))

