(define-module (crates-io lo tu lotus-script) #:use-module (crates-io))

(define-public crate-lotus-script-0.1.0 (c (n "lotus-script") (v "0.1.0") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12dbq9jy1s5psbnxcj1lm41p7yxrrr2ib24l3f9m31w4ff9zzyi2")))

(define-public crate-lotus-script-0.1.1 (c (n "lotus-script") (v "0.1.1") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08xaab0rk9qcydcwnb863qgv8bb4mymbrzn1ad7zzzs4rzmiqcqz")))

(define-public crate-lotus-script-0.2.0 (c (n "lotus-script") (v "0.2.0") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "146hb1vhdnpbrabg9a5dmm0hqqsrjzd6fw7m81x0rq3pgrb486y5")))

(define-public crate-lotus-script-0.2.1 (c (n "lotus-script") (v "0.2.1") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gl3njnf33prnrf7v1fj9kvvzg57sjw9aw2g6ava5wj2byfn4jq")))

(define-public crate-lotus-script-0.2.2 (c (n "lotus-script") (v "0.2.2") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qb98kvfpa06l18frj2wx9j3gpr1w66dj7fhcdpk5p1mj4l3bhgy")))

(define-public crate-lotus-script-0.2.3 (c (n "lotus-script") (v "0.2.3") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r1jmgpw45x044f9ggxhwwysw6g5nqdm9g6gnrhri06bdvbyxvr2")))

(define-public crate-lotus-script-0.2.4 (c (n "lotus-script") (v "0.2.4") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "09xy4zcj8kky16h35wlvflfsgnyf49hwx4ncwxnwi9973ggkmb85")))

(define-public crate-lotus-script-0.2.5 (c (n "lotus-script") (v "0.2.5") (d (list (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0zhq8zf1998n5lbrkx8wax02biikw83s0hq9fkj3g0m3xy6ns39s")))

(define-public crate-lotus-script-0.2.6 (c (n "lotus-script") (v "0.2.6") (d (list (d (n "glam") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lotus-bindgen-macros") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1vkavn3zmxy0nby7ka98srabylqbf17968ay2mm3aig7pk374cmh")))

