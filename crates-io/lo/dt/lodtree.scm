(define-module (crates-io lo dt lodtree) #:use-module (crates-io))

(define-public crate-lodtree-0.1.0 (c (n "lodtree") (v "0.1.0") (d (list (d (n "glium") (r "^0.30") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0m5i0pq1990ww6bfn2yb629kwaj2sgdjgzxl92wll3smka6qr9g3")))

(define-public crate-lodtree-0.1.1 (c (n "lodtree") (v "0.1.1") (d (list (d (n "glium") (r "^0.30") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0ph40n0k8qrg3wl3ikvfjy5qg2wyi3wx3kxxgfj4pkyygw4i30bi")))

(define-public crate-lodtree-0.1.2 (c (n "lodtree") (v "0.1.2") (d (list (d (n "glium") (r "^0.30") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "1w3cmixm77gc9mn7244b4lq5x25bicnbsncra5wr1ywg3byrybfn")))

(define-public crate-lodtree-0.1.3 (c (n "lodtree") (v "0.1.3") (d (list (d (n "glium") (r "^0.30") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "02kdl3l30mdizfw5983vn9545z0dshi69bk9vz8pnq1xk4cmvhql")))

(define-public crate-lodtree-0.1.4 (c (n "lodtree") (v "0.1.4") (d (list (d (n "glium") (r "^0.30") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0kkd3mjp6yz1j7s3rkmpk4mggi67qwz00vzpkc9ddkzk4cmlgb3b")))

