(define-module (crates-io lo ch lochnes) #:use-module (crates-io))

(define-public crate-lochnes-0.1.0 (c (n "lochnes") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "enum-kinds") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1v2b1jav1f4z5nw912zawm52n1chkqjfn1c5jgd57p0pb5cnq5an") (f (quote (("easter-egg") ("default" "easter-egg"))))))

(define-public crate-lochnes-0.1.1 (c (n "lochnes") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enum-kinds") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (d #t) (k 0)))) (h "1r2b8kaxd65vqxa856pkdm58pgwfi9n3kr7lnhlqln4ll1x6psd5") (f (quote (("easter-egg") ("default" "easter-egg"))))))

