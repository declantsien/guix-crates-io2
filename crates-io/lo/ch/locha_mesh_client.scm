(define-module (crates-io lo ch locha_mesh_client) #:use-module (crates-io))

(define-public crate-locha_mesh_client-0.0.1 (c (n "locha_mesh_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1msqsxcgz0fmakqm0c8gwz49imlhbl1090gxfzk0b29lxji1j4s3")))

(define-public crate-locha_mesh_client-0.0.2 (c (n "locha_mesh_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "0kx2a9i856gk0jjvbhmkkv69hbr9q0sanzi7n998ajblw5vqv3aw")))

