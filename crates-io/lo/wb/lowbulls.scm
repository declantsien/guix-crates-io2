(define-module (crates-io lo wb lowbulls) #:use-module (crates-io))

(define-public crate-lowbulls-0.1.0 (c (n "lowbulls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "18vrx8f41fz5caslahl9f6awkq3vq2svra1wqb0szdl6cyx4ymq4")))

(define-public crate-lowbulls-0.1.1 (c (n "lowbulls") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1jcvji0i875ffx6s9bmssiz729v0nksj07lpq3vn6r4aj3agw5j0")))

(define-public crate-lowbulls-0.1.11 (c (n "lowbulls") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0vs0x101ad08kxw0w9cigf270y5nhzl7iwwg69ljc30kb2ngr8jv")))

(define-public crate-lowbulls-0.1.12 (c (n "lowbulls") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0nswz5chdhimfwyclp8zi01n7vg08mmlac5bm5i3ykdjdgn3k7xi")))

(define-public crate-lowbulls-0.1.13 (c (n "lowbulls") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "14adcg5r3sh7ycmr5x5ivbd3h1byl5pfjmhl7mwqgy4yl2h1xm97")))

(define-public crate-lowbulls-0.1.14 (c (n "lowbulls") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0lfv3f44dxlf32ml5dzz1nmrh2dz3cfhias8vjsix9qvrvla2abg")))

(define-public crate-lowbulls-0.1.15 (c (n "lowbulls") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0jrdv3n6zzzp74d3bmdjyw5v0gfc1mymh314bcs76r9nnx0rg4sl")))

(define-public crate-lowbulls-0.1.2 (c (n "lowbulls") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1dbdpkxv58zg3ccyidgcz690h5qr1n7l8lf11zvfwkqxxzr2pzjl")))

(define-public crate-lowbulls-0.1.21 (c (n "lowbulls") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1ppk8d6ldq737pj531alv4pnfdjjwmwjpy05js7k7dkvwb0nl11p")))

(define-public crate-lowbulls-0.1.22 (c (n "lowbulls") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0m6pzi2pcb7fq1birav18swhj5ag902a43i52d5wahgvg7i3mh31")))

