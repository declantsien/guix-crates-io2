(define-module (crates-io lo ne lonelyradio_types) #:use-module (crates-io))

(define-public crate-lonelyradio_types-0.4.0 (c (n "lonelyradio_types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i2s8gbgjr5l5404is9vx4i25zir4338kpamak4wwclvd0qp96iq")))

(define-public crate-lonelyradio_types-0.4.1 (c (n "lonelyradio_types") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "027i3da6cv0s4jvj5m3b2mgkqgscwvw3vwwgxxnaw85cb26iwyqx")))

