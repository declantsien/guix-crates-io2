(define-module (crates-io lo gv logv) #:use-module (crates-io))

(define-public crate-logv-0.2.0 (c (n "logv") (v "0.2.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xqx2x2xl4vyhjsq5bzjkjza7r9jyy20xgj7ryijrq0a0xhs1zlw")))

(define-public crate-logv-0.2.1 (c (n "logv") (v "0.2.1") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)))) (h "13h7b8gr74gbgk87bxdk6pdmwr52r8g9vl7mhzwzf49j7iqm2rvi")))

(define-public crate-logv-0.2.2 (c (n "logv") (v "0.2.2") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)))) (h "140n8c7h103qdl606yb7irvhbyazwmpijy7903h51jl2k5vp3cls")))

(define-public crate-logv-0.3.0 (c (n "logv") (v "0.3.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (d #t) (k 0)))) (h "067lvavz2669az4xlaw4chb243yvwnlwdni92k2l1a56lrcjsxy5")))

