(define-module (crates-io lo ga logaddexp) #:use-module (crates-io))

(define-public crate-logaddexp-0.1.0 (c (n "logaddexp") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fjvn9awilqkrmb39x737v6pv14m9g5lsqi9ygrnh3glv2b6zp32")))

(define-public crate-logaddexp-0.1.1 (c (n "logaddexp") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kg2pxfn97jb2l74kh9xa8cz4g3n7ci58bcxix4swba4szm8z595")))

(define-public crate-logaddexp-0.1.2 (c (n "logaddexp") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zmp2g2bh6v4l62zvp2wd2lxx4zqvl3hpxdfbddpskip98mybkmq")))

(define-public crate-logaddexp-0.1.3 (c (n "logaddexp") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cyl5f98hwd5bmra771krrgy6rw8kvnpj9b4cgq94k49mmxrqns1")))

