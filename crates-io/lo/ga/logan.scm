(define-module (crates-io lo ga logan) #:use-module (crates-io))

(define-public crate-logan-0.0.0 (c (n "logan") (v "0.0.0") (d (list (d (n "derive_more") (r "^0.9") (d #t) (k 0)))) (h "0qg8rwsdzjiwaczgi0gs96pp4v2rzab38dxwg2lyvjj9nn53jwbb")))

(define-public crate-logan-0.0.1 (c (n "logan") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.9") (d #t) (k 0)))) (h "0k1z7jcf7dda3g9vgfj934cm517r4jhvlhj511k11na41hb5gdx7")))

