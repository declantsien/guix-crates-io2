(define-module (crates-io lo ga loga) #:use-module (crates-io))

(define-public crate-loga-0.1.0 (c (n "loga") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1akwwb3ghgd4kj7bwr6cbz0d7vvs9vwr9ijkbm0nbqvv2dqb08b6")))

(define-public crate-loga-0.1.1 (c (n "loga") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "13dvw248vqi2cps7ryfcng11xq0sb4srz0vdn38ppkar0b44r97a")))

(define-public crate-loga-0.1.2 (c (n "loga") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "06m2fgscm891h0xr2j0783dfjwp5h3xxkfggwfska5408gv0bk6k")))

(define-public crate-loga-0.1.3 (c (n "loga") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "11j5bgjmhbc4z257qwmpy810lk7h49vb5i33ip2yyvidx4l3b62c")))

(define-public crate-loga-0.1.4 (c (n "loga") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "02xlimmmzmjjrmksd59qr608vwwinlbim3il5sl4fxfi1a3xil2f")))

(define-public crate-loga-0.1.5 (c (n "loga") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1zg2kk8adda02b64yg8bcvz8jlg4nlbh29xcxmlyn7v6mh665rsl")))

(define-public crate-loga-0.1.6 (c (n "loga") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1h8w70man3hsiyrv0sg7q7s3mr18ghh74qfxidgzwxyv6dkh752v")))

(define-public crate-loga-0.1.7 (c (n "loga") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1gblj3qlp882x0szmlgwdzviw2p54c6cfpgqksv059rgajm8qkpw")))

(define-public crate-loga-0.1.8 (c (n "loga") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1qnwwq5lc6rd116k6i8mbqd0c6sfpkkiis06wrvz5czj1na93kmr") (y #t)))

(define-public crate-loga-0.2.0 (c (n "loga") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1jzwjk8v428prcf5hc05b3qi8q2q01crl3v69l28ig2i94v1a2wy")))

(define-public crate-loga-0.3.0 (c (n "loga") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1064rrihmni7p0rkr2hkxnvnc8mmi6ic17gwnzk4rhw1387g4c35")))

(define-public crate-loga-0.3.1 (c (n "loga") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0j5h0dr0vq3yk0lqlhnlak5l1qi5zwqv3aip8glh1rv9b0szc8id")))

(define-public crate-loga-0.3.2 (c (n "loga") (v "0.3.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1j189idkjn23kj5jxjwlmgxhxk7n2ld4aaszqww6jlala80l9psz")))

(define-public crate-loga-0.3.3 (c (n "loga") (v "0.3.3") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1qm0f41w4magl9c8r5gzc8cpw49a22r4j0vhk7dm432qgspnf0n3")))

(define-public crate-loga-0.3.4 (c (n "loga") (v "0.3.4") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "01plah48r9qnm1wkqmb36730idgp1l37lgiikrxdh6xgg7r3y5dg")))

(define-public crate-loga-0.3.5 (c (n "loga") (v "0.3.5") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "015yrkpc447swblw3ihs0dzh02fphbbvb5fmsdsgzb3xn1axrns7")))

(define-public crate-loga-0.3.6 (c (n "loga") (v "0.3.6") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "08ri9lj1z6bpa3ckqm5qbk53m1b0nfa2j0yyrx0zjcn6kd1jh5pb")))

(define-public crate-loga-0.3.7 (c (n "loga") (v "0.3.7") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0cby08sf2iwp03i9820hdx09nvb8ya4ybfq5hp4s7hqimcm3v3k1")))

(define-public crate-loga-0.3.8 (c (n "loga") (v "0.3.8") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1h9rc387sh38llf29w07iiawzlfjvfd7y2jvl4gnyjvgy46vvigx")))

(define-public crate-loga-0.3.9 (c (n "loga") (v "0.3.9") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "03wbl31sa0fv4a047ryak8w5wnvqj4rcibx270y1f6dq3fk2zchr")))

(define-public crate-loga-0.3.10 (c (n "loga") (v "0.3.10") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "12pc6pjwdkh9146bvwphrd7hzakbfda8fzpgr3128mi9bfl1czym")))

(define-public crate-loga-0.4.0 (c (n "loga") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0x3bj6f25bbdzf8xlhif2hgsfsanxszpcgw9plvjf92mllx84k0z")))

