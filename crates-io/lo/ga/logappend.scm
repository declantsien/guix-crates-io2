(define-module (crates-io lo ga logappend) #:use-module (crates-io))

(define-public crate-logappend-0.1.0 (c (n "logappend") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ylgh1r83fx55kk0h6bgan02vcrmi8d6g1i0fila5qszc9mzh5y1")))

(define-public crate-logappend-0.2.0 (c (n "logappend") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "10srliavxrdgv7afydhlf5g1d7fmbljxydx0yp62hg6rmp0kf4ji")))

(define-public crate-logappend-0.2.1 (c (n "logappend") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ip5j93i8y2jidhr2inbjzmhaz060snas0j9dfhdfqraqyp16fsk")))

(define-public crate-logappend-0.3.0 (c (n "logappend") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ph9b8kjhh6292vnvvizlv2j4yjn861fwz8q8wc6nwk0fbwwpp2g")))

(define-public crate-logappend-0.3.2 (c (n "logappend") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1v2ph992pj7c6b78f99rky70qixbxpj3y554v4saafjk8sql01rw")))

