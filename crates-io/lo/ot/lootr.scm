(define-module (crates-io lo ot lootr) #:use-module (crates-io))

(define-public crate-lootr-0.3.0 (c (n "lootr") (v "0.3.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dv0h1m2i5bk4as3yybdspz9hnawdssyhcbdbvl5g9h345vbhh95")))

(define-public crate-lootr-0.4.0 (c (n "lootr") (v "0.4.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1x5r2mii9fdyxf97bn32x13m3lgaclyq0mx4152yrh85mgf3v7sg")))

(define-public crate-lootr-0.7.0 (c (n "lootr") (v "0.7.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "06nlk8pz2qaksxy4z7ixjmqffy97b9sqxdcvmn2hr3a8kfp27583")))

