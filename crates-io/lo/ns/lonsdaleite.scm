(define-module (crates-io lo ns lonsdaleite) #:use-module (crates-io))

(define-public crate-lonsdaleite-0.0.1 (c (n "lonsdaleite") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 2)) (d (n "cocoa") (r "^0.19.0") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17.0") (d #t) (k 0)) (d (n "metal") (r "^0.17.1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (d #t) (k 2)) (d (n "winit") (r "^0.21.0") (d #t) (k 0)))) (h "1c2jrlr5c6rybc32bxx69qlig1fjl02aakjg7imhb905m082qszi") (f (quote (("mtl") ("empty") ("default" "mtl")))) (y #t)))

