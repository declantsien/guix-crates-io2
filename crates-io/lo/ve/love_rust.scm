(define-module (crates-io lo ve love_rust) #:use-module (crates-io))

(define-public crate-love_rust-0.1.0 (c (n "love_rust") (v "0.1.0") (h "1i1smh9mb9jdnkwdsk5r1pqdfdva3yp1ja7dra9w0nbl6ck56wba")))

(define-public crate-love_rust-0.2.0 (c (n "love_rust") (v "0.2.0") (h "1yvxpqb4v3q3m5qnk7dk3phinzm2jwd402xcx0qb8zq26mfchi7w")))

(define-public crate-love_rust-0.2.1 (c (n "love_rust") (v "0.2.1") (h "0788nzbz7lry644s7kfsy02kyg8rirxpqck47v14yxi7nf21vxi3")))

(define-public crate-love_rust-0.2.2 (c (n "love_rust") (v "0.2.2") (h "04428c3j8pqr17847fsyk2cgnj18v1qzkv8ndvvnfanyy8vaa4h2")))

(define-public crate-love_rust-0.3.0 (c (n "love_rust") (v "0.3.0") (h "11xqs25rrckyiqjcmdc0gdf90lf9mcxylrg11l7rncw8m4bjjsbf")))

