(define-module (crates-io lo ve love-fetch) #:use-module (crates-io))

(define-public crate-love-fetch-1.0.0 (c (n "love-fetch") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.5") (k 0)) (d (n "whoami") (r "^1.1.1") (d #t) (k 0)))) (h "0bxw6yjcbl0d1ch7cpqfb8scdkn1xp6rcad8bznb428x3imndld2")))

(define-public crate-love-fetch-1.0.1 (c (n "love-fetch") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.5") (k 0)) (d (n "whoami") (r "^1.1.1") (d #t) (k 0)))) (h "08ng62c4dkhxl6wwaxzj4zzniasp9br9d48h020l4hjsn0x1n5za")))

(define-public crate-love-fetch-1.1.1 (c (n "love-fetch") (v "1.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.5") (k 0)) (d (n "whoami") (r "^1.1.1") (d #t) (k 0)))) (h "1j3x80byvkgixmznk5p0b4m64mjbzwkahhncgzycnlsjirywqdiq")))

(define-public crate-love-fetch-1.1.2 (c (n "love-fetch") (v "1.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.5") (k 0)) (d (n "whoami") (r "^1.1.1") (d #t) (k 0)))) (h "197qy1ghc6rkn7bx3pbri8hkf694h502zkn1cvb0fni0y9dk86p5")))

