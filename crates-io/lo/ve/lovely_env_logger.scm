(define-module (crates-io lo ve lovely_env_logger) #:use-module (crates-io))

(define-public crate-lovely_env_logger-0.1.0 (c (n "lovely_env_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17iidk35as6vlxpdawh6na4pn5pw6hgncbgsvbzjy2lddcsz9x7i")))

(define-public crate-lovely_env_logger-0.5.0 (c (n "lovely_env_logger") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x1d4xq17j1q2l8la42jb6bbzw3djsh0nwzdxzc35wi1qa6gycr1")))

(define-public crate-lovely_env_logger-0.6.0 (c (n "lovely_env_logger") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (f (quote ("auto-color"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19k6av04na4g45zjg189186aq80r5zf2jqf6jil91l38jr2ni3g9") (f (quote (("regex" "env_logger/regex") ("humantime" "env_logger/humantime") ("default" "humantime" "reltime" "regex")))) (s 2) (e (quote (("reltime" "dep:chrono"))))))

(define-public crate-lovely_env_logger-0.6.1 (c (n "lovely_env_logger") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (f (quote ("auto-color"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00hmdlvx23glw96l8fc97aghimv2ph5md2gssszbxdmb6n7n34kj") (f (quote (("regex" "env_logger/regex") ("humantime" "env_logger/humantime") ("default" "humantime" "reltime" "regex")))) (s 2) (e (quote (("reltime" "dep:chrono"))))))

