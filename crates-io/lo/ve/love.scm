(define-module (crates-io lo ve love) #:use-module (crates-io))

(define-public crate-love-0.2.0 (c (n "love") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "platform-lp") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "version-lp") (r "^0.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1rwrird1nj77xnsb90sh027lk9clylgikyzn6j8978nrygrr1riv")))

