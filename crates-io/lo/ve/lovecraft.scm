(define-module (crates-io lo ve lovecraft) #:use-module (crates-io))

(define-public crate-lovecraft-0.1.0 (c (n "lovecraft") (v "0.1.0") (h "0pjmnsfwn4dvmwrdwqs2wy3nbakdad0b8lg29qa9d4b9hsbwrk9b")))

(define-public crate-lovecraft-0.2.0 (c (n "lovecraft") (v "0.2.0") (h "15vknp54i2nprn4njhbywrk7ll4fwghksbmwxxj7prk5shzqhrbq")))

