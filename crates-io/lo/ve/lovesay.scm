(define-module (crates-io lo ve lovesay) #:use-module (crates-io))

(define-public crate-lovesay-0.1.0 (c (n "lovesay") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1q08kiw7c3h0gjz12g8ky1s82d9bsfxh83d53xkbvbngbs3n0c3p")))

(define-public crate-lovesay-0.1.1 (c (n "lovesay") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1bj5cpap24xqp4fsilj5si94p2mn7zfasihjclm4jng7gg3gwp1c")))

(define-public crate-lovesay-0.2.0 (c (n "lovesay") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1r6d5c9r5122cyqg8pa316lkglrl78671gw2424kp187f21ajf5b")))

(define-public crate-lovesay-0.3.0 (c (n "lovesay") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1ppk438rzmgf6f0zda2a17ij94irwxc9dh2zjl3q2gbrh0q7c6h6")))

(define-public crate-lovesay-0.4.0 (c (n "lovesay") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1dl5f5snvh5mhld72lv2zc9bpld07n4chbv15grwkh3ln957pfgj")))

(define-public crate-lovesay-0.4.1 (c (n "lovesay") (v "0.4.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1rzcc27ifcm3fgvg8zrw4g2mpbg066jlzpjn1b6r3fgm3xyns908")))

(define-public crate-lovesay-0.5.0 (c (n "lovesay") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0aicbnnk4svd0ifl8qqfd542mzs8m9ssql7wqlx9h2cc6591ni5q")))

(define-public crate-lovesay-0.5.1 (c (n "lovesay") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.3.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "00ssksrk4s5wa42r92rnp8ybk7gx3aaxbdcf4d8b77x0y9hylvgp")))

(define-public crate-lovesay-0.5.2 (c (n "lovesay") (v "0.5.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "12c1x7kr3zpnv3zgwdn7x4famsj3wlb1amfy7km1hqmf3d1k6k3z")))

(define-public crate-lovesay-0.5.3 (c (n "lovesay") (v "0.5.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.5.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1zagar11l9raipkh8l744wrydzdg4zra3g01mkravp4cwgv72g4l")))

(define-public crate-lovesay-0.5.4 (c (n "lovesay") (v "0.5.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1b1g7d18sjhafiidl0xqzzjp3s2n7phg8z5pck55gzkrbar8w29h")))

(define-public crate-lovesay-0.5.5 (c (n "lovesay") (v "0.5.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0j56p50552ffs32pk2rr5yvaa0g3wyhk9lizsywj7nf3xz85zj4y")))

(define-public crate-lovesay-0.5.6 (c (n "lovesay") (v "0.5.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1vnz4g62mg24m8slkhrkcgfy0njx2l9amhfpsq7vmvcs8byfwhf1")))

(define-public crate-lovesay-0.5.7 (c (n "lovesay") (v "0.5.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "kolorz") (r "^0.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0nimlkbvb4m6lnw7pj5g5bnb3c3pivhvnwy1zwdxdxr7flf7qvcv")))

(define-public crate-lovesay-0.6.0 (c (n "lovesay") (v "0.6.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.4.9") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "kolorz") (r "^0.6.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "076myn8j348xsksjzdjmdngzkhazfrndk5f7rshrabaagm5h8768")))

