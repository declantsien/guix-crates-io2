(define-module (crates-io lo gb logbar) #:use-module (crates-io))

(define-public crate-logbar-0.1.0 (c (n "logbar") (v "0.1.0") (h "1lwhp2acd9gnjilnazz875kqfh4m2mlm70qngvnpzym2bqnq11xf")))

(define-public crate-logbar-0.1.1 (c (n "logbar") (v "0.1.1") (h "0kdy8b9i821jf16sy9lbl4mhq1l8wgw2na360xn9gd94nz9xq1gf")))

