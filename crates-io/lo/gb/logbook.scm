(define-module (crates-io lo gb logbook) #:use-module (crates-io))

(define-public crate-logbook-0.1.0 (c (n "logbook") (v "0.1.0") (h "0p7q3v7b2c1k0fhsaj3ai3p5wlgcnxh9qagqb10fh93v78gcmvjs")))

(define-public crate-logbook-0.1.0-alpha.1 (c (n "logbook") (v "0.1.0-alpha.1") (h "0nbm7kn7w9r4k1jfr8d9gmkk1y4hfl763jn6d2bf2y8fpjs3ziby")))

