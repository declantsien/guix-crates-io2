(define-module (crates-io lo gw logwatch) #:use-module (crates-io))

(define-public crate-logwatch-0.1.0 (c (n "logwatch") (v "0.1.0") (h "1phv25hmaalx283159sq6ah8whdi5k9ips4hb6gzbpyvxvsvhgp9")))

(define-public crate-logwatch-0.1.1 (c (n "logwatch") (v "0.1.1") (h "1fa2wh2fzb40h3xq264dmy77qxkr3sz8basbdi10cl7dq4w2v570")))

(define-public crate-logwatch-0.1.2 (c (n "logwatch") (v "0.1.2") (h "0ghca8akcgyyjc7sd59nynmw93nwkkwqjcl5l67nkw4vb0hmhqvw")))

(define-public crate-logwatch-0.1.3 (c (n "logwatch") (v "0.1.3") (h "0qimpwilypzbn7y924jy6lfgw3rljwn16srfbrprwxhyy35m293j")))

(define-public crate-logwatch-0.1.4 (c (n "logwatch") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17kd163d9i1468zhhw2j1az2nfnlxapb4cw4xx3qw5z3jmw4clgq")))

