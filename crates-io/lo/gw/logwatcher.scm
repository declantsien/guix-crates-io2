(define-module (crates-io lo gw logwatcher) #:use-module (crates-io))

(define-public crate-logwatcher-0.1.0 (c (n "logwatcher") (v "0.1.0") (h "19fy30h4gk74i6df04ani701pipn82crvvy03c9g9s8gmqxiccd8")))

(define-public crate-logwatcher-0.1.1 (c (n "logwatcher") (v "0.1.1") (h "00y99v2gdaz7w5xr01s1rpdm3w0cj5dd4f7laycdabmisnp2a29x")))

