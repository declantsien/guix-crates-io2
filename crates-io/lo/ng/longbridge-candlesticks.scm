(define-module (crates-io lo ng longbridge-candlesticks) #:use-module (crates-io))

(define-public crate-longbridge-candlesticks-0.2.21 (c (n "longbridge-candlesticks") (v "0.2.21") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1rczn2qcjr9jafv7m6w55w8mzk8xa3gnbk088avb2gk2mmvgvfx5")))

(define-public crate-longbridge-candlesticks-0.2.22 (c (n "longbridge-candlesticks") (v "0.2.22") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1bkkispmgjzssq5wzhbmfx4c5c6a3sgs1aj9fhcvw4igg4gx913v")))

(define-public crate-longbridge-candlesticks-0.2.23 (c (n "longbridge-candlesticks") (v "0.2.23") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0zwcwcm1pf9cmpvkg8lw8f7439j6z6smpmm37832njrzlr49yr32")))

(define-public crate-longbridge-candlesticks-0.2.24 (c (n "longbridge-candlesticks") (v "0.2.24") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "14zlql6yx74ms98dckfym5qp0ysr97wj55lla33rz3jgnbzlcx46")))

(define-public crate-longbridge-candlesticks-0.2.25 (c (n "longbridge-candlesticks") (v "0.2.25") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0k3nyl0bnrsi71abr693az7px18q2fypsq5f1h1335qi1k42pvpz")))

(define-public crate-longbridge-candlesticks-0.2.26 (c (n "longbridge-candlesticks") (v "0.2.26") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "11yda3in2hrhz231z53szcj4n1bcbbkd3g5wdbjbvx0ywjrk143s")))

(define-public crate-longbridge-candlesticks-0.2.27 (c (n "longbridge-candlesticks") (v "0.2.27") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "07h6p9d6pipnmjz1b8x4hvx1xdzqpr6bdhzm82ryq4j9fa4baxni")))

(define-public crate-longbridge-candlesticks-0.2.28 (c (n "longbridge-candlesticks") (v "0.2.28") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "06f7s89f9jzm03mf4r9hgz0jsvb9r4hry4kwrkfhprs8jkav0q19")))

(define-public crate-longbridge-candlesticks-0.2.29 (c (n "longbridge-candlesticks") (v "0.2.29") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1hn2l13pqw7wi2m5mxxr64mkhbmfgdsq8dp91gn7m9vs48afz1kk")))

(define-public crate-longbridge-candlesticks-0.2.31 (c (n "longbridge-candlesticks") (v "0.2.31") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0d2xp3ha0ph7idw8s8yq10aal0z2ap19zwciz3sb5hf6v18risxn")))

(define-public crate-longbridge-candlesticks-0.2.32 (c (n "longbridge-candlesticks") (v "0.2.32") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "07nca945rvgqy56aw34vdmr9fxch1pw9d3qf5q0y16dlsgkh01a4")))

(define-public crate-longbridge-candlesticks-0.2.33 (c (n "longbridge-candlesticks") (v "0.2.33") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1xa5pqjnw4hyb2x6d5hkwcdsz8kw3fflbpnk306p79a58nn1n0j9")))

(define-public crate-longbridge-candlesticks-0.2.34 (c (n "longbridge-candlesticks") (v "0.2.34") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0ijf198hgggp5lp707x85w82m0z2d9xxwgswrnl4qwlkzgr5xf8p")))

(define-public crate-longbridge-candlesticks-0.2.35 (c (n "longbridge-candlesticks") (v "0.2.35") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1iyrld0xg71qqwjsjyg055nwds5ah6swby0q67p2g7raizl5rwln")))

(define-public crate-longbridge-candlesticks-0.2.36 (c (n "longbridge-candlesticks") (v "0.2.36") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "18a4iv8j0kzfgnh62shx8221vfnnnrp3dg0ihc113qinxrrq01n5")))

(define-public crate-longbridge-candlesticks-0.2.37 (c (n "longbridge-candlesticks") (v "0.2.37") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "06jy3iqvcz5rzy4a2dic3i6x936rr06320m73hr67y2bbcp6znvh")))

(define-public crate-longbridge-candlesticks-0.2.38 (c (n "longbridge-candlesticks") (v "0.2.38") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "12arl8h1xcic6x0c7cs3fn4h80zc7pl4cxz728glv6595s3k0jp8")))

(define-public crate-longbridge-candlesticks-0.2.39 (c (n "longbridge-candlesticks") (v "0.2.39") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0kfagcvm63yg9ap2v5lrv2cak5xg10l1q3w94gn7i7wx8q6q5ivm")))

(define-public crate-longbridge-candlesticks-0.2.40 (c (n "longbridge-candlesticks") (v "0.2.40") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0w2hh8qp5hddlwv5cm9kibg7yn55780dwqf0kv111b0179ymzy63")))

(define-public crate-longbridge-candlesticks-0.2.41 (c (n "longbridge-candlesticks") (v "0.2.41") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1zg6chm96c4ci24yq2dfqhw2vzsb90x0apmkpnqnsqg2916f77vb")))

(define-public crate-longbridge-candlesticks-0.2.42 (c (n "longbridge-candlesticks") (v "0.2.42") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "01ihqx4saf931d135mjl6d5ghfpchj926bvc0bjdh34qwv25gq0x")))

(define-public crate-longbridge-candlesticks-0.2.43 (c (n "longbridge-candlesticks") (v "0.2.43") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1c5pn4z2wxiar3ghrxcf572wfyvbgmvran3x3cs32bcbns5n65nq")))

(define-public crate-longbridge-candlesticks-0.2.44 (c (n "longbridge-candlesticks") (v "0.2.44") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "14sivijzkgipgz08pqa3x33ig3g19ai6dsv61nnn3hwndmvgcc04")))

(define-public crate-longbridge-candlesticks-0.2.45 (c (n "longbridge-candlesticks") (v "0.2.45") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1kp28mpy822d1lbq5kr55vh10i06kipcqj3ppgs6igz997dxy56y")))

(define-public crate-longbridge-candlesticks-0.2.46 (c (n "longbridge-candlesticks") (v "0.2.46") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0wd1pa781g2ki8mikjm53rk7ipmrv5lqmbljdr6qyxfgc643g3rf")))

(define-public crate-longbridge-candlesticks-0.2.47 (c (n "longbridge-candlesticks") (v "0.2.47") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0j62xkv4j81ixz6ap4jzglklck0igvlwr3vfkdzh7fqag9wa92dk")))

(define-public crate-longbridge-candlesticks-0.2.48 (c (n "longbridge-candlesticks") (v "0.2.48") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1icx238qd2vf7314kps2fdpypsq0nwyzhgk3bbri0g9ag5bwbp78")))

(define-public crate-longbridge-candlesticks-0.2.49 (c (n "longbridge-candlesticks") (v "0.2.49") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0vpmwy395m0qj2qsjz9141qbc133xfqs2jk510m71hh7j99yg3dp")))

(define-public crate-longbridge-candlesticks-0.2.50 (c (n "longbridge-candlesticks") (v "0.2.50") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1ml5ppnb49fy47931v1fkdsj6n41iywyf2q7vdq1ihjgj7bz61s6")))

(define-public crate-longbridge-candlesticks-0.2.51 (c (n "longbridge-candlesticks") (v "0.2.51") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1cm0a96mzxlfddfljm7pq0s5b8iqp682dc61793mzrpy4bnwf27x")))

(define-public crate-longbridge-candlesticks-0.2.52 (c (n "longbridge-candlesticks") (v "0.2.52") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1mswxi4dm5rw90nlygb9hmz52rm3hxmz309xp1damk8z85zx7jgp")))

(define-public crate-longbridge-candlesticks-0.2.53 (c (n "longbridge-candlesticks") (v "0.2.53") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0k3d63z79750gazza2y55wh06dgalpmgi4hln9wh2r5765x14byq")))

(define-public crate-longbridge-candlesticks-0.2.54 (c (n "longbridge-candlesticks") (v "0.2.54") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "098vawwmissjpy258drph2njfz0dq9s263yz4xs177f0wwdxangl")))

(define-public crate-longbridge-candlesticks-0.2.55 (c (n "longbridge-candlesticks") (v "0.2.55") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1hdjbfzcsg0c16lp2gmyhd579vr82r9n64jaqmv4vm0lqb9qfd89")))

(define-public crate-longbridge-candlesticks-0.2.56 (c (n "longbridge-candlesticks") (v "0.2.56") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1nfqk9l64qz2xsry3njyyvkqffppcpl1vp5kix7n2a5vfnf4gipy")))

(define-public crate-longbridge-candlesticks-0.2.57 (c (n "longbridge-candlesticks") (v "0.2.57") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1q4gd395nyqw06bj5x9wadrsi7n0z2xygk60s2sz9gn8j798jaxs")))

(define-public crate-longbridge-candlesticks-0.2.58 (c (n "longbridge-candlesticks") (v "0.2.58") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "100inqqvv1ca29i27kk90qzsvp1xsj23pyh7n2qxhrihc2f0261a")))

(define-public crate-longbridge-candlesticks-0.2.59 (c (n "longbridge-candlesticks") (v "0.2.59") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1wj1nvj8h5vxi4vi6nf07nakxv3crqldma4w8dl8x76v7r4ckw6d")))

(define-public crate-longbridge-candlesticks-0.2.60 (c (n "longbridge-candlesticks") (v "0.2.60") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0pnzb04h9b3lvygjyaryp4y86lqv8d580xp118w6l8fx7a3nkbzr")))

(define-public crate-longbridge-candlesticks-0.2.61 (c (n "longbridge-candlesticks") (v "0.2.61") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "00g1riaswly0ygdi0ys6c8d2mwvpi892556s2gkyhkllmji8kyha")))

(define-public crate-longbridge-candlesticks-0.2.63 (c (n "longbridge-candlesticks") (v "0.2.63") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1i1iii99rz92ax6m3gmnlnxpy374z0wz43s3zxpjby8x7nvb8rjv")))

(define-public crate-longbridge-candlesticks-0.2.64 (c (n "longbridge-candlesticks") (v "0.2.64") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1jys9awdq6kj1n79bbqgjy0ds07ry939g0m1bpywzlr2bsgq5lvv")))

(define-public crate-longbridge-candlesticks-0.2.65 (c (n "longbridge-candlesticks") (v "0.2.65") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0c21ssyvws1nmgl67in89mnqvy6hd5y512h85czaf72mbmifpw6h")))

(define-public crate-longbridge-candlesticks-0.2.66 (c (n "longbridge-candlesticks") (v "0.2.66") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1h7cwpipwp26fv1w8ddmgqja2x7pn1xbfi285c341729vzp5qrby")))

(define-public crate-longbridge-candlesticks-0.2.67 (c (n "longbridge-candlesticks") (v "0.2.67") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1p1xpscvawar879malp44dvf7rszq274dz8yjvvl003prx651c1a")))

(define-public crate-longbridge-candlesticks-0.2.68 (c (n "longbridge-candlesticks") (v "0.2.68") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1wgsf6z9g90fhhjkdqk356sif0srfhmfbbv606xg5n14259g9m45")))

(define-public crate-longbridge-candlesticks-0.2.69 (c (n "longbridge-candlesticks") (v "0.2.69") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1nq33xh4gn752nrb3jl9gkb4ash7cbc4hb92alr7gcz3a3xrp06b")))

(define-public crate-longbridge-candlesticks-0.2.70 (c (n "longbridge-candlesticks") (v "0.2.70") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "15y7yj805zild79s676kwd2vx3x619rfadxaj43ac8594cyzyp30")))

(define-public crate-longbridge-candlesticks-0.2.71 (c (n "longbridge-candlesticks") (v "0.2.71") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1dryyadfzpr5pp6jnpbc7hg16ghcc2madmpl9a3fazsd9bvzb0hy")))

(define-public crate-longbridge-candlesticks-0.2.72 (c (n "longbridge-candlesticks") (v "0.2.72") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1kfpx6qz0sd1hfs9yzidkicvpdv0lpncgia1lq1661y5h0lvcnpm")))

(define-public crate-longbridge-candlesticks-0.2.73 (c (n "longbridge-candlesticks") (v "0.2.73") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0zb7chmy7njvkly1j4q0xxj3pylq64x5v6mvcgng08vyclwyhxj4")))

(define-public crate-longbridge-candlesticks-0.2.74 (c (n "longbridge-candlesticks") (v "0.2.74") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0i6f3fx1q75q63ggr8wdm33ci8y90mp7ksfayyjf4zp3nqpkhflc")))

(define-public crate-longbridge-candlesticks-0.2.75 (c (n "longbridge-candlesticks") (v "0.2.75") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0z8ahgrpbyy0rvcm6xl4shf4n026nmbr2mr49552xgp691nzbc1h")))

(define-public crate-longbridge-candlesticks-0.2.76 (c (n "longbridge-candlesticks") (v "0.2.76") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0dd1cr54am97zz5i7xadx5g5xmdkia1y85azwk11f07fdryrh39g")))

(define-public crate-longbridge-candlesticks-0.2.77 (c (n "longbridge-candlesticks") (v "0.2.77") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "04wmpllw5xp36si30axckc0k1cr7syvbpnp9i7ppd6z34j3pndw9")))

