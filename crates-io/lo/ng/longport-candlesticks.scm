(define-module (crates-io lo ng longport-candlesticks) #:use-module (crates-io))

(define-public crate-longport-candlesticks-1.0.0 (c (n "longport-candlesticks") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "14qs44a5gbvqx6dbz14hjrjmsamlj7s93mqlq3dahrr1xsp67ir1")))

(define-public crate-longport-candlesticks-1.0.1 (c (n "longport-candlesticks") (v "1.0.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1gjr2aqw6qc4lzcwz3r9r3s0q5c36gx9f1fhqr7vm3n14dx9x351")))

(define-public crate-longport-candlesticks-1.0.2 (c (n "longport-candlesticks") (v "1.0.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "16dnpa960y85ldh14wzqgzm8gkr4773kp9wa03rrcwym600wgs91")))

(define-public crate-longport-candlesticks-1.0.3 (c (n "longport-candlesticks") (v "1.0.3") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "17qf3gmq34zh1gh6g1pp5jjhnxshyma76rwqck85ggsi0vcbl2s4")))

(define-public crate-longport-candlesticks-1.0.4 (c (n "longport-candlesticks") (v "1.0.4") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1fzb4pkld02pszq24kwvslg114jybf1g66gq7dyrjpxcal83yc8y")))

(define-public crate-longport-candlesticks-1.0.5 (c (n "longport-candlesticks") (v "1.0.5") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "06bkr1bh3j10pla7mykj9jq6p7hs72875pprwgb0bwwqq4wqqjvd")))

(define-public crate-longport-candlesticks-1.0.6 (c (n "longport-candlesticks") (v "1.0.6") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1b3vp66k3d2zqpf5vwjayv0ny3s85bl0kmryfyb05crjb4bpvlgp")))

(define-public crate-longport-candlesticks-1.0.7 (c (n "longport-candlesticks") (v "1.0.7") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0qaziarkgqpkz5h9p6qx2a12dmsx7hkdk31m8iwwm8h6xzc9hyw3")))

(define-public crate-longport-candlesticks-1.0.8 (c (n "longport-candlesticks") (v "1.0.8") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0awl73q6n7x9nn508yn25h94i1h3zgqkrryn0wr22n1hh1y96l7d")))

(define-public crate-longport-candlesticks-1.0.9 (c (n "longport-candlesticks") (v "1.0.9") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "18hcbbywsr998xib75vvh4nss1jihbrqzmipxxq8iwgd4w5hvfga")))

(define-public crate-longport-candlesticks-1.0.10 (c (n "longport-candlesticks") (v "1.0.10") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0m6rsdk7wf5g89xlrs3zbvpdfw35r97h4c0abddzg3ww5kpidnhw")))

(define-public crate-longport-candlesticks-1.0.11 (c (n "longport-candlesticks") (v "1.0.11") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1365951q0jm96212q78fhsfmzgb3353fnflx2297nygabz6ak56f")))

(define-public crate-longport-candlesticks-1.0.13 (c (n "longport-candlesticks") (v "1.0.13") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1sc7gx1iq3hj36j6rzfry3vpc1s82d5nsp737z6qs7kbnhrmwkwj")))

(define-public crate-longport-candlesticks-1.0.14 (c (n "longport-candlesticks") (v "1.0.14") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "04337zmqdf939db38vx4k0rqz8khykf780b7hzrqd8skmx8ysn5n")))

(define-public crate-longport-candlesticks-1.0.15 (c (n "longport-candlesticks") (v "1.0.15") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1wk1z8r4z9h7vrm169ks17i65dw4my4h3q1nqad0xwkmdk4m9141")))

(define-public crate-longport-candlesticks-1.0.16 (c (n "longport-candlesticks") (v "1.0.16") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0j47sg0zp8kfjiv3pn4c308jv5ywiayigiaj9rb05qwlk3kvj115")))

(define-public crate-longport-candlesticks-1.0.18 (c (n "longport-candlesticks") (v "1.0.18") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0yx93lwyns53lq6s8m2skdr8fwrzh4hca0pl7xxxlqnlgbyd9j7g")))

(define-public crate-longport-candlesticks-1.0.19 (c (n "longport-candlesticks") (v "1.0.19") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0gjfcdi89szq8kjskxij2fx6zp6nn3qhq3d764ybwcwnm6pnnmqk")))

(define-public crate-longport-candlesticks-1.0.20 (c (n "longport-candlesticks") (v "1.0.20") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1y54f5cvpv118s2drg90g5pz435j3akraqnyqlgl9rs3q58hvqwb")))

(define-public crate-longport-candlesticks-1.0.21 (c (n "longport-candlesticks") (v "1.0.21") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1yxmddalpmdz30nr5rwwh4pkg5pc6dhas764f3s2nvkckpkdgfjr")))

(define-public crate-longport-candlesticks-1.0.22 (c (n "longport-candlesticks") (v "1.0.22") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1snsndc71paw3g6drh7nz34zy93y6p8am28vrkxisdz78kmb34q8")))

(define-public crate-longport-candlesticks-1.0.23 (c (n "longport-candlesticks") (v "1.0.23") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0nj75z2qljrhk7dzm10cxiqff0vngf07w73g3s4n8chnyimb0ild")))

(define-public crate-longport-candlesticks-1.0.24 (c (n "longport-candlesticks") (v "1.0.24") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "1yj42x235a8db5za8zsqfcv32prrihx068wd0fl5n0w52rxc55f1")))

(define-public crate-longport-candlesticks-1.0.25 (c (n "longport-candlesticks") (v "1.0.25") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "time-tz") (r "^1.0.2") (d #t) (k 0)))) (h "0mddcmgmvadwa8j8khx9nzj8hbhqbv2f1dcmim8h1rl6yil23kqk")))

