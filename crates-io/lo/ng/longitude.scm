(define-module (crates-io lo ng longitude) #:use-module (crates-io))

(define-public crate-longitude-0.1.0 (c (n "longitude") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1pgs82r3hs8km35n2wa04zqzw1h7szcwa3xqn4yksp34wyvs9nm9")))

(define-public crate-longitude-0.2.0 (c (n "longitude") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0zhgabd0k4rfncbby0l7k2zacivb3n6l5h04z4732zs9yv2dl43h") (f (quote (("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-longitude-0.2.1 (c (n "longitude") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "03nc37dp87amx6bydzdrh1lzdnjiwmkqw60j3kpbhmhaf3lp2xr8") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

