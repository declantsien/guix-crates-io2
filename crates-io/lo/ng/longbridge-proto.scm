(define-module (crates-io lo ng longbridge-proto) #:use-module (crates-io))

(define-public crate-longbridge-proto-0.2.0 (c (n "longbridge-proto") (v "0.2.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0wn7myg22phjb5im1ji3dqv00zh7b726gx5f8rz08xwfl979nsif")))

(define-public crate-longbridge-proto-0.2.1 (c (n "longbridge-proto") (v "0.2.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0y2998injnn99938hvh7ikfk1a2jwx62034dsf4d4bwlihhwc2k1")))

(define-public crate-longbridge-proto-0.2.2 (c (n "longbridge-proto") (v "0.2.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1n47lqgmikm0sb57178hxcp9n05djva9g4f44j1sdhd29l2zq2d8")))

(define-public crate-longbridge-proto-0.2.3 (c (n "longbridge-proto") (v "0.2.3") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0kxbbiwnnx0x91vf8gl51ra54ik83nk6bk5jck2ifkbasylfz3p8")))

(define-public crate-longbridge-proto-0.2.4 (c (n "longbridge-proto") (v "0.2.4") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1r0g0g2fqxp683513q54nfh5y1y1ypj3f40d7myk5d0lahlqbajn")))

(define-public crate-longbridge-proto-0.2.5 (c (n "longbridge-proto") (v "0.2.5") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0wslz6yxsfjrr6wsi1k3ngv2da87namb5ds7073w5mlzqdk6y72l")))

(define-public crate-longbridge-proto-0.2.6 (c (n "longbridge-proto") (v "0.2.6") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0mdfxp2dl7jd4admzijqnvn7gqr6a8dxqkx94m6dwlkg3ncb9vgi")))

(define-public crate-longbridge-proto-0.2.7 (c (n "longbridge-proto") (v "0.2.7") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1k93kc4bkgn79mrr66rly2xh9g0d73hgiqw360w237lp9p1bq460")))

(define-public crate-longbridge-proto-0.2.8 (c (n "longbridge-proto") (v "0.2.8") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0hfx8qjas5h1s8nywaly1whiy5g6gzp4hhsj6ndzhj8yj86lf1n3")))

(define-public crate-longbridge-proto-0.2.9 (c (n "longbridge-proto") (v "0.2.9") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1d24dxr8darldk9k8zl950zyml2g549scy4pqb8ydj45042f5mr8")))

(define-public crate-longbridge-proto-0.2.10 (c (n "longbridge-proto") (v "0.2.10") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0ncn5xkxaq2a0hgpnkhiwksslxdpad71ajqc0h8bdpackjyypmg0")))

(define-public crate-longbridge-proto-0.2.11 (c (n "longbridge-proto") (v "0.2.11") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "01d38wz1s9sd5ysj0nnlf9v9niwknyr4f65ayh11xrwxspf2ji5f")))

(define-public crate-longbridge-proto-0.2.12 (c (n "longbridge-proto") (v "0.2.12") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "032b3ss84k4ygl9jq54gw1p6jd9yk368nmqfq2bpl5rc0027jcfg")))

(define-public crate-longbridge-proto-0.2.13 (c (n "longbridge-proto") (v "0.2.13") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0lpgdb5dg19rmih8658xg0a2w9jddixd5aayy1xjg2j36ijy8ci1")))

(define-public crate-longbridge-proto-0.2.14 (c (n "longbridge-proto") (v "0.2.14") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1ylaazgk8zcpc7ysxiqxg2xp48l30r4jkjcsbq4a6f9x5yrbi2q8")))

(define-public crate-longbridge-proto-0.2.15 (c (n "longbridge-proto") (v "0.2.15") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1l44q1pl4jxq4pq7v5p4m4wkdhps5gmgfpwi3prfrwkvf129jnr9")))

(define-public crate-longbridge-proto-0.2.16 (c (n "longbridge-proto") (v "0.2.16") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0aa7x81369asvc48z2bkqgsilpncn8xjbn2cvzrafnly725qpyar")))

(define-public crate-longbridge-proto-0.2.17 (c (n "longbridge-proto") (v "0.2.17") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "158an8lcds667i5hphj1n5pdj7bfbdp2kwbdj2i0nvsvq901aha0")))

(define-public crate-longbridge-proto-0.2.18 (c (n "longbridge-proto") (v "0.2.18") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "15wz42y61mgkaryx3xfypark76lr8yrf2zc0y1llvvmcwl4p9zjz")))

(define-public crate-longbridge-proto-0.2.19 (c (n "longbridge-proto") (v "0.2.19") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1rknga4ad9y83qnw5rfa9jbkjz8fzh83nzcfgpmswrssyq7i7yd0")))

(define-public crate-longbridge-proto-0.2.20 (c (n "longbridge-proto") (v "0.2.20") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1g1r1dgddcfkl0jh9wa8dz83dja9h3s2wksvsmpircpq5yi6i1c7")))

(define-public crate-longbridge-proto-0.2.21 (c (n "longbridge-proto") (v "0.2.21") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "0v9bw5yxh3pymk4p161q50g4d30518fa1fh0xwqfrdkzx3b5kjs2")))

(define-public crate-longbridge-proto-0.2.22 (c (n "longbridge-proto") (v "0.2.22") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1bcmxwj8lh3m4cdb8k0l2an8ydp653hxplvblddinzfh3bv8k8mr")))

(define-public crate-longbridge-proto-0.2.23 (c (n "longbridge-proto") (v "0.2.23") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.3") (d #t) (k 1)))) (h "1kzhfhqns3sj0j420jpy2mzp7i6p7hi9ildzpsha4w9hhvichrrb")))

(define-public crate-longbridge-proto-0.2.24 (c (n "longbridge-proto") (v "0.2.24") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0bc96lcairad2nms3k48cadsg33djkp9gibmk23dfj4wh2c6hrxm")))

(define-public crate-longbridge-proto-0.2.25 (c (n "longbridge-proto") (v "0.2.25") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1x639gpf04ky9x34b5al5kkjzwyigspkc7dp7hn7sqz1gxib45fq")))

(define-public crate-longbridge-proto-0.2.26 (c (n "longbridge-proto") (v "0.2.26") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0mgqch4knnd9wdysgi0xs7ja436j3p05a7kkng7ch8alf12az40l")))

(define-public crate-longbridge-proto-0.2.27 (c (n "longbridge-proto") (v "0.2.27") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1wdzpa6k0ffjclwifqqx39giphs6llkdfpp5a9xkycy2gzvjk6ga")))

(define-public crate-longbridge-proto-0.2.28 (c (n "longbridge-proto") (v "0.2.28") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1gnbjd7z5y7r93jga2mcrkn8d7jrf81zsvss2bk1a05a676gqvdb")))

(define-public crate-longbridge-proto-0.2.29 (c (n "longbridge-proto") (v "0.2.29") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "030sb9sqg1sy8wvgyfrrpsjw5ab7836sd2xwfvsbji40nb6x5l1m")))

(define-public crate-longbridge-proto-0.2.30 (c (n "longbridge-proto") (v "0.2.30") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1xg4v4js9131hqh7ds9jhr7f6yyci6ips3zndphzna012lv92qv8")))

(define-public crate-longbridge-proto-0.2.31 (c (n "longbridge-proto") (v "0.2.31") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0vlijskq2hcg54bamqi7ypsqd8fq3f49vlfrk3fsqd7p2zs0l4q3")))

(define-public crate-longbridge-proto-0.2.32 (c (n "longbridge-proto") (v "0.2.32") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0ys37avxv5sqxrs9kqqds7qp6y1l3pzlc4044zflp8gbib3wkhhk")))

(define-public crate-longbridge-proto-0.2.33 (c (n "longbridge-proto") (v "0.2.33") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "01lp8liwc6klwfsxj8528qhyd32mz5i5x4j1m27bmgm2cjbcxxl4")))

(define-public crate-longbridge-proto-0.2.34 (c (n "longbridge-proto") (v "0.2.34") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1fblzljqa2sz1b243wbkzj6azfl71d7b3ky8169c7h2gpmjw7q5c")))

(define-public crate-longbridge-proto-0.2.35 (c (n "longbridge-proto") (v "0.2.35") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "02sdmqgwfps4zhpn9hpzybcng1v7z2n9l5cxiscabbv7cybghw56")))

(define-public crate-longbridge-proto-0.2.36 (c (n "longbridge-proto") (v "0.2.36") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "00741y2dlir7dh900kndlahv8vyycxw35z9vkvgp2f7jh0q29807")))

(define-public crate-longbridge-proto-0.2.37 (c (n "longbridge-proto") (v "0.2.37") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0w4941rb63nwxkpy3l0n7c15gdsl4p0aafz2m6zr87ijvj1pvpsh")))

(define-public crate-longbridge-proto-0.2.38 (c (n "longbridge-proto") (v "0.2.38") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0ki3mcrg0c7dcl8vmj3jncmq2vij302mmc9bcx4vwvh5v2fsfhrg")))

(define-public crate-longbridge-proto-0.2.39 (c (n "longbridge-proto") (v "0.2.39") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "05frdsdfyqalibqq8dzxj75ca8gbpgjjdx23qzy9m545kzxagiyw")))

(define-public crate-longbridge-proto-0.2.40 (c (n "longbridge-proto") (v "0.2.40") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0jxvag94yjbph6izqgp2w81fb700xr11h870j99f1pqy90jcf9r4")))

(define-public crate-longbridge-proto-0.2.41 (c (n "longbridge-proto") (v "0.2.41") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "15lwsm6rgy9jy6zznffr700zy11ihdxzw61vjrjf4603hp3x8nam")))

(define-public crate-longbridge-proto-0.2.42 (c (n "longbridge-proto") (v "0.2.42") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "06zpxykg0bm7f764jh98lnik6gi8vbxnqwsw6k49an8gx37mg1yc")))

(define-public crate-longbridge-proto-0.2.43 (c (n "longbridge-proto") (v "0.2.43") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1q45s46xzisb1gwndbrj9wvvs617b6zig3w0bfalbjibl5d89z0n")))

(define-public crate-longbridge-proto-0.2.44 (c (n "longbridge-proto") (v "0.2.44") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0dlykzwppvzsn59dc39gaigks40dyjshjgglqi73qjggzjny8q29")))

(define-public crate-longbridge-proto-0.2.45 (c (n "longbridge-proto") (v "0.2.45") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1wxsapnx9y417b0v4af9bz05kgxk3jr05bzzvim1cb163jdsq06r")))

(define-public crate-longbridge-proto-0.2.46 (c (n "longbridge-proto") (v "0.2.46") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0s6wy2dx64hjgq5wwr5d77kgb3dr4i1vsx990w8z3k11k8d133dq")))

(define-public crate-longbridge-proto-0.2.47 (c (n "longbridge-proto") (v "0.2.47") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0mlpxrllabhziszkp3fkdni553yfwxlrgwy8s6wzcwjyrhk28rzs")))

(define-public crate-longbridge-proto-0.2.48 (c (n "longbridge-proto") (v "0.2.48") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0rxwq40axgdc1ffs7gfv8jz57i1p511qni68gf4d8hvsmdcmb7vq")))

(define-public crate-longbridge-proto-0.2.49 (c (n "longbridge-proto") (v "0.2.49") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0k94rcisq4bc2xfxgpwwxkdgaxs477a5snipmvh7688rjy7vm1q5")))

(define-public crate-longbridge-proto-0.2.50 (c (n "longbridge-proto") (v "0.2.50") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1x4pzkw5k34pc0fyacxlwv1j11y051ziq8ymjz6a4b5wxqf86073")))

(define-public crate-longbridge-proto-0.2.51 (c (n "longbridge-proto") (v "0.2.51") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1g0qs0ip6g8bwv0dpgmg06v02r91xcvsvmfy89dcwz89l9qy3w0f")))

(define-public crate-longbridge-proto-0.2.52 (c (n "longbridge-proto") (v "0.2.52") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0n4p258xcj5pimpkgrkkjz6fsxjjn49h7dc04j5c43ccymcjp2b8")))

(define-public crate-longbridge-proto-0.2.53 (c (n "longbridge-proto") (v "0.2.53") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "02pm8jx45vx5217dr1xsbr8a91kydxapms7jahn5zwb35jc15757")))

(define-public crate-longbridge-proto-0.2.54 (c (n "longbridge-proto") (v "0.2.54") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0mgb9jr2pw7s8ivxdbib3rbk88rl22k34lampv13k4bqscc8rcn7")))

(define-public crate-longbridge-proto-0.2.55 (c (n "longbridge-proto") (v "0.2.55") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0k167ibmcn0fm91l9b3f8lgv4rnla0gq95ivq9zqzhm0ddy667l2")))

(define-public crate-longbridge-proto-0.2.56 (c (n "longbridge-proto") (v "0.2.56") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1v4qfsxh2asfmbk67y9ad4pzldyspc8887y5zynz0va2jixb60vf")))

(define-public crate-longbridge-proto-0.2.57 (c (n "longbridge-proto") (v "0.2.57") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "10n3zdvp8phsdz8wzz7f5i0gq55b3z6jrargs3b2451cp271xsvk")))

(define-public crate-longbridge-proto-0.2.58 (c (n "longbridge-proto") (v "0.2.58") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0rj04gj7qq19znnmgcc96bc2l891macnsmx1s86i3fhqfgphj31g")))

(define-public crate-longbridge-proto-0.2.59 (c (n "longbridge-proto") (v "0.2.59") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "035x4kbf28w44ar8hs6m267s3zf2z2xpdv76xs5gkqj5wc10yl2m")))

(define-public crate-longbridge-proto-0.2.60 (c (n "longbridge-proto") (v "0.2.60") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0vbc7d9yrkg492y0gyk7b05ml8ggwr0lr7bmmwsr3n0d9qnqrgyh")))

(define-public crate-longbridge-proto-0.2.61 (c (n "longbridge-proto") (v "0.2.61") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "135rh6g71c97zv5d4zl44qxyzxcibpys5q7kp3dlc9gdwzn9acsi")))

(define-public crate-longbridge-proto-0.2.63 (c (n "longbridge-proto") (v "0.2.63") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "07krk3qggzc3m1lcadzyh1znagi2z7li2x5w9x5fp9hynw7vwxra")))

(define-public crate-longbridge-proto-0.2.64 (c (n "longbridge-proto") (v "0.2.64") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "18imi30p30fcvqxs6317xmhidimfxx0mpglxzdhifdric8pp2d44")))

(define-public crate-longbridge-proto-0.2.65 (c (n "longbridge-proto") (v "0.2.65") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1zj2ic21qh1j3bja2030zb3l5zxbgkz7y6xdd0h8rr6jdda74j49")))

(define-public crate-longbridge-proto-0.2.66 (c (n "longbridge-proto") (v "0.2.66") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0wd40v251wmf1jwf2da0w41kmqfjnbzhpqxjrmky7ybavjm8phkv")))

(define-public crate-longbridge-proto-0.2.67 (c (n "longbridge-proto") (v "0.2.67") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1kkkrhsk36962m20m7sa3pc6hb994ng13x9676f6bjhv3kgi5y67")))

(define-public crate-longbridge-proto-0.2.68 (c (n "longbridge-proto") (v "0.2.68") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0s63pg13irkvr5v1r9dr780fmyzgkczdip3ajbbpp40d2dzhqrdl")))

(define-public crate-longbridge-proto-0.2.69 (c (n "longbridge-proto") (v "0.2.69") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1ya1mbypgcpg4bfq7531pap8az73l83cr5zpqykkwvppss16kxf0")))

(define-public crate-longbridge-proto-0.2.70 (c (n "longbridge-proto") (v "0.2.70") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1bipihl2gq32hh62sx8zwly84vml8r5w6nq6hw2mrylz3zh3csmv")))

(define-public crate-longbridge-proto-0.2.71 (c (n "longbridge-proto") (v "0.2.71") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0334nhwdjy0al6pkm374hyzgfqm777fnkp9jplvg4mhj5m7d0qz3")))

(define-public crate-longbridge-proto-0.2.72 (c (n "longbridge-proto") (v "0.2.72") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0hpv72a0c53akrq84pb7rbni4gdpffmdhhdvdj9hk8h1yrx5vqym")))

(define-public crate-longbridge-proto-0.2.73 (c (n "longbridge-proto") (v "0.2.73") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0x249dg9hxpbzpy5c20i06kyzj89m9yib34gp5zjynjziimg1s2y")))

(define-public crate-longbridge-proto-0.2.74 (c (n "longbridge-proto") (v "0.2.74") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "138fcdk34d8iwflrlpr6gq9fgnw1si3fckczrpry1vlhr1z51hwk")))

(define-public crate-longbridge-proto-0.2.75 (c (n "longbridge-proto") (v "0.2.75") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1mgszvn0k8lwzgh3wxmwmlgxk3f8n9a4nm93wkg4i3db2gd04hag")))

(define-public crate-longbridge-proto-0.2.76 (c (n "longbridge-proto") (v "0.2.76") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1lcrj242ip4wgmw6pw2mwhphzajz1kwyxna4xqfh6jjnl57wvsxc")))

(define-public crate-longbridge-proto-0.2.77 (c (n "longbridge-proto") (v "0.2.77") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1ghmbr29z30rxz2h4gklhl358kgajg6kl6p8ywzj2hq3piv5dhxb")))

