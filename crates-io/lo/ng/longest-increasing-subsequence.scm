(define-module (crates-io lo ng longest-increasing-subsequence) #:use-module (crates-io))

(define-public crate-longest-increasing-subsequence-0.1.0 (c (n "longest-increasing-subsequence") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "11hxv1jya5m9nbs9jj7qh88i646sz9sn47xpzmb10mwhrp90vgdk")))

