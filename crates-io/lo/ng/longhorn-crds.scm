(define-module (crates-io lo ng longhorn-crds) #:use-module (crates-io))

(define-public crate-longhorn-crds-0.1.0 (c (n "longhorn-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0i9lmyfcb6wqlp4b73hfgcp4ji6bnk8i971wkz3m0hpla9zd6f03")))

