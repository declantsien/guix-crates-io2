(define-module (crates-io lo ng longdouble) #:use-module (crates-io))

(define-public crate-longdouble-0.1.0 (c (n "longdouble") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1slhb5h4shazd6dh74y5z86bn6wvfy1l73xfsmwb15d9mds54pfj")))

(define-public crate-longdouble-0.1.1 (c (n "longdouble") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1ngzm2knifyrr0ic7z4sd19fc1im9ji6i6kf5c4qjwkvc8ib89qd")))

(define-public crate-longdouble-0.1.2 (c (n "longdouble") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "04yb3g9dynwwnz76cmhlaim6c1iwnr680gk6vgzqi09j2gr2bvjs")))

