(define-module (crates-io lo ng long-running-task) #:use-module (crates-io))

(define-public crate-long-running-task-0.1.0 (c (n "long-running-task") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "00yzd5r346zc58ns9j9q2zqd8whds2xjhc3fp7kphgwc9r3my7pi") (f (quote (("lifespan") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-long-running-task-0.1.1 (c (n "long-running-task") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "11m5j2bzisnrni3dic31hrf3972z6vxgby72w1p82asry2sx7773") (f (quote (("lifespan") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

