(define-module (crates-io lo ng longport-proto) #:use-module (crates-io))

(define-public crate-longport-proto-1.0.0 (c (n "longport-proto") (v "1.0.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1217mnk66mpr26mvrmgdm70aznj3nnl5hfcs366297qqzrklimjp")))

(define-public crate-longport-proto-1.0.1 (c (n "longport-proto") (v "1.0.1") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1rvgh2m51gk3p29pd1ys1ykkx1i804yl13xlfy38yfsykyx7din8")))

(define-public crate-longport-proto-1.0.2 (c (n "longport-proto") (v "1.0.2") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "02kpv2a7j0i5fxpmjibrssdhspg18i1zxjl6pa5hxx1dr5n8ami6")))

(define-public crate-longport-proto-1.0.3 (c (n "longport-proto") (v "1.0.3") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "13wlizy3rqcidvjy8y8jn4aqpcn4mz16b1ij55fn83ah0ib7rdg4")))

(define-public crate-longport-proto-1.0.4 (c (n "longport-proto") (v "1.0.4") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0vbh5bja9m6a4nw7vgpigcmvdgf4891rfsaziwsxmyvsayhvm9fr")))

(define-public crate-longport-proto-1.0.5 (c (n "longport-proto") (v "1.0.5") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0l2y2lkmiqx9c51w1cwilasrjnsx350lb59j9bd8fd01fb8996mi")))

(define-public crate-longport-proto-1.0.6 (c (n "longport-proto") (v "1.0.6") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "04kl3zf7pa85kmrlbk0wyvpwfx849hcvys9l4f1nxxdbzfk9lmpq")))

(define-public crate-longport-proto-1.0.7 (c (n "longport-proto") (v "1.0.7") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "03cn536x2d73xp8pcdy9qlnzcjfxqdyjk2bz9p9zqlj6lbi421k3")))

(define-public crate-longport-proto-1.0.8 (c (n "longport-proto") (v "1.0.8") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0vl2wvmf8igq4ycymq8cdyxa6sb6lj5i3fwni68kq4cb07xghadq")))

(define-public crate-longport-proto-1.0.9 (c (n "longport-proto") (v "1.0.9") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1p0hk62lgplwhy44nrff5m264gybhvmik771c5bxb1gwgar0ii10")))

(define-public crate-longport-proto-1.0.10 (c (n "longport-proto") (v "1.0.10") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0awnxs2c47abyb1cy2gjl9ypm21xws4v8ykidfyiafmd9hslbd6m")))

(define-public crate-longport-proto-1.0.11 (c (n "longport-proto") (v "1.0.11") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0m5ksj6h17k4wvsqv9l4hsxwl6fngpl97pla533ycc5gbjrgj7lk")))

(define-public crate-longport-proto-1.0.13 (c (n "longport-proto") (v "1.0.13") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "07znaifa4cs4n9zxpgvx1gg9la262p62xayfxifhfyc2z2j6y05z")))

(define-public crate-longport-proto-1.0.14 (c (n "longport-proto") (v "1.0.14") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0qbpa1cfbp51j88nkl7i86290gfpgkqkhzs3a59a7c709ph63hjb")))

(define-public crate-longport-proto-1.0.15 (c (n "longport-proto") (v "1.0.15") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1098m1sn33r680b65l1f48r0d93vhl6s79gj3nac8zmhrajyc9r6")))

(define-public crate-longport-proto-1.0.16 (c (n "longport-proto") (v "1.0.16") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0vlmrpwbbdm8mxn6k21p5sfxqr3b8qpzg0djzybf1b133mrf274z")))

(define-public crate-longport-proto-1.0.18 (c (n "longport-proto") (v "1.0.18") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "18g48l29yc91qn5a2mfcq6pa8h5h8wd0vx7y69mvz1gvlhkm1alz")))

(define-public crate-longport-proto-1.0.19 (c (n "longport-proto") (v "1.0.19") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "13zcr0xjl4l365ifbfs60if04kfkbrnifaxqzd931kskzhp4zzwq")))

(define-public crate-longport-proto-1.0.20 (c (n "longport-proto") (v "1.0.20") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "12p75jr07fbn0ycmv21riy9z18y9zy7rp67x58xnxrp38764msvv")))

(define-public crate-longport-proto-1.0.21 (c (n "longport-proto") (v "1.0.21") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "1qv1sigylr1nglcx14sxgn0s4srsnj6hqjlkmjg3p28gfg3wlbgj")))

(define-public crate-longport-proto-1.0.22 (c (n "longport-proto") (v "1.0.22") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0jfmd62jmysclsdnsmq0ramyj73zhsdayk019hm3ilqn1h0zbrdc")))

(define-public crate-longport-proto-1.0.23 (c (n "longport-proto") (v "1.0.23") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0s96s58sgd6yzw6v2mprm9szy5awlihp1wqjpnqx6r600kcnkhz4")))

(define-public crate-longport-proto-1.0.24 (c (n "longport-proto") (v "1.0.24") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "15qn7iz618frr8jd7v5aqrg55gbx0l49da8ad9dsr0s51amgip23")))

(define-public crate-longport-proto-1.0.25 (c (n "longport-proto") (v "1.0.25") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)))) (h "0b3i4c4qgc1naalpv7nsgcgfz71l2r72zcnba538z30hq7vdnm08")))

