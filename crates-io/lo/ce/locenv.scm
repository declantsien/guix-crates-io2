(define-module (crates-io lo ce locenv) #:use-module (crates-io))

(define-public crate-locenv-0.1.0 (c (n "locenv") (v "0.1.0") (h "029grqicwkjqbdb8412hd0nqzdnwys9aig6k0xv56c300pvm7bzb")))

(define-public crate-locenv-0.2.0 (c (n "locenv") (v "0.2.0") (h "1qy3633m4f9yb1sg81p69i433s1s8ysa12h0vcwdgi0g52akad09")))

(define-public crate-locenv-0.3.0 (c (n "locenv") (v "0.3.0") (h "06xca8n12j8p5c8r4xgsln06mxrmgr4n8s06jy6awh6j2lsw43vd")))

(define-public crate-locenv-0.4.0 (c (n "locenv") (v "0.4.0") (h "0g9gzl2qhz1lbwivm2iv59bznm9a3cwrfzcjxv2vv0vnqipz15kh")))

(define-public crate-locenv-0.5.0 (c (n "locenv") (v "0.5.0") (h "1f7y3imdxsrjxqb14h9ignxrrcyvclf43qqw2arjjf7r1s4a5rcv")))

(define-public crate-locenv-0.6.0 (c (n "locenv") (v "0.6.0") (h "0z75zkf9jvdnfiy29k6k9xraqdss30zb7c00a7p41baykmk8i7mx")))

(define-public crate-locenv-0.6.1 (c (n "locenv") (v "0.6.1") (h "00r2r2b5a624j2w55553hw2896y514n91zm63gf82padlkf2xz5q")))

(define-public crate-locenv-0.7.0 (c (n "locenv") (v "0.7.0") (h "0jqyj9nmnr1wb2zzn3ixx628pdzm89rvih36rfkp2bqw8q2zrkw4")))

