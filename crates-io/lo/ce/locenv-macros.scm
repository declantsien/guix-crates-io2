(define-module (crates-io lo ce locenv-macros) #:use-module (crates-io))

(define-public crate-locenv-macros-0.1.0 (c (n "locenv-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1530kkwn9i4kvk6jx85zbsyg469qm0wsw21kwkcqx73xn37vxn7s")))

(define-public crate-locenv-macros-0.2.0 (c (n "locenv-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rnbwhz0ln2lbiv5v51jkjnxrnis1il4f23ja04gmh3k0702m3xr")))

(define-public crate-locenv-macros-0.3.0 (c (n "locenv-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cmpm28a7gwrsv9qprabm82nkkrq5l27jbi1xh168wk84fhvv1k0")))

(define-public crate-locenv-macros-0.4.0 (c (n "locenv-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnalj9hpkfx3gh4bv3nq8n28q9l6cnsmknsvrsyisriwxiqcpc9")))

(define-public crate-locenv-macros-0.5.0 (c (n "locenv-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfr30c4gpviwv1a04cj14bsnlrblajshsdrax9j9zgw58dqdsbn")))

(define-public crate-locenv-macros-0.6.0 (c (n "locenv-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xm40zfrqjs8r3wxadiwy8hnlz0qbsjvgi9qd15gjxpjil6493m4")))

(define-public crate-locenv-macros-0.6.1 (c (n "locenv-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09qlw5k62y3np2fmjvzh96jvs5mw69xyxp1f80p0wkvbnnglis4q")))

(define-public crate-locenv-macros-0.7.0 (c (n "locenv-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wxnx456krwhdk3mn556ng3678dsx2chsscb5rp6g86n8iy917xz")))

