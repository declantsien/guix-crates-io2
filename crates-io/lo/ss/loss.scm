(define-module (crates-io lo ss loss) #:use-module (crates-io))

(define-public crate-loss-0.1.0 (c (n "loss") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (k 0)))) (h "1mck3r80w1vm6fh2vspw4hz3yrh56c3km37yqd0xdl10vbdjwj8r") (f (quote (("tracing-log" "tracing-subscriber/tracing-log") ("smallvec" "tracing-subscriber/smallvec") ("json" "tracing-subscriber/json") ("env-filter" "tracing-subscriber/env-filter") ("default" "alloc" "attributes" "env-filter" "json" "smallvec" "tracing-log") ("attributes" "tracing/attributes") ("alloc" "tracing-subscriber/alloc"))))))

