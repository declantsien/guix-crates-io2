(define-module (crates-io lo cs locspan) #:use-module (crates-io))

(define-public crate-locspan-0.1.0 (c (n "locspan") (v "0.1.0") (h "04spggm6kagrwxyhlnvqb6f7wlzp9y89xca2bspyzxkjhfpbkfgr")))

(define-public crate-locspan-0.1.1 (c (n "locspan") (v "0.1.1") (h "1qr3dx6n0cy4a85y5sz60dqrsgrcgy9576qn957br5pj5746hx9v")))

(define-public crate-locspan-0.2.0 (c (n "locspan") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "12i9mwddcd79d79spg84kbizn5dwma3c6vf2dx5dngyj5fx17bxr") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2.1 (c (n "locspan") (v "0.2.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1b94p5d5d11czwvnz01j03ppsxw2mkb08dpa13mqwp1zl0k9fy0r") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2.2 (c (n "locspan") (v "0.2.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "18dlvhbyww3iwdaz7b36lg439zdni7ap8jw22b1w4r808gg4lvwz") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2.3 (c (n "locspan") (v "0.2.3") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "02vbz844c87qbwqsd4jv1c9gvayxl1c1wba6gi69jsv57sn5nph8") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2.4 (c (n "locspan") (v "0.2.4") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "10hr51236669api79jkgp1m0z13fl09mjv7gf5vzspxkc42fqrzi") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3.0 (c (n "locspan") (v "0.3.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1y5wwk7ij6945vy7h9vawrl2cvpkp0w084x72v8yv7smwywz2hl3") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3.1 (c (n "locspan") (v "0.3.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0dvr9s4lsf4sm53rvc7hfi4dpmb2jmfg46x0xdihwvhn4hvgacrk") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3.2 (c (n "locspan") (v "0.3.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "10iz2zxs3c9alc9ad4hwrb3y251i0msg5iiway0mgx759l7yjzgp") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.0 (c (n "locspan") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1d2n1s0958x3ch6i3jbi75xa8j4jij256cacyplskc18qazzamn9") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.1 (c (n "locspan") (v "0.4.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0shzlycyx8ax1rr2aqlc8jfkkhi0g41bjg1ix224s2fibg6vs553") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.2 (c (n "locspan") (v "0.4.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0j7wmskc6q7frg9cs0l2wywxi290fpc086942wcygarjj459cvrs") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.3 (c (n "locspan") (v "0.4.3") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0xld854ichvkkzggv6lwwnzn450yr9yw91sgg4gmipvdjpcnb7si") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.4 (c (n "locspan") (v "0.4.4") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1kvsn4q0fy9ysc2xj4p35ns7gijw4kh9fzbxz7lqkm2wfy8nm91c") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4.5 (c (n "locspan") (v "0.4.5") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "09abz4iqci00zynl1hr2bf8dwnl097p2022by84rp7fa9hqhjl6c") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5.0 (c (n "locspan") (v "0.5.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1w1j7awlkgakap75cv1fbhhhlrxm3n6bidf02i06lwn2vn3p9jfd") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5.1 (c (n "locspan") (v "0.5.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1kll6isxlppcs3d7jg5gvmmr9irbw6fdq3m1q1h9rr6pab9slnky") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5.2 (c (n "locspan") (v "0.5.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "18is20qdszm27c4nza86srwyhlmgj99fvpl5cgm0lqms3kcmx6zd") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5.3 (c (n "locspan") (v "0.5.3") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1kwk2j2agypkvfm7yqvviwwgihw2g1aqvlc75d8sqb9j2nvz0jx3") (f (quote (("reporting" "codespan-reporting") ("default")))) (y #t)))

(define-public crate-locspan-0.5.4 (c (n "locspan") (v "0.5.4") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "102yn35mjh0yc36zqpzqcjasqbza827hj06wbprsfdx4r9qmd432") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.6.0 (c (n "locspan") (v "0.6.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1rxgh5irpgimgr5kv53kkzs78x78phnv2d0pc0ryhy0fjccwlmi9") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.0 (c (n "locspan") (v "0.7.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0a7dp846nisq4vsh968bm59y7p8wvic1l17sg7fwly53kb0dnwrj") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.1 (c (n "locspan") (v "0.7.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "086y6v4xadiwblfilfsn950k37jkhdd1hf3ssfqsknbnxb86j5y7") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.2 (c (n "locspan") (v "0.7.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0izfv13lmkghscgjlq03rmvjj823zmx267nkh8sn69warr5zxgrx") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.3 (c (n "locspan") (v "0.7.3") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1js5bd9vc5aj7lk23il373ipa2nz1wfnk449wqj5911kk55ymy1v") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.4 (c (n "locspan") (v "0.7.4") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1g8ap1f9rs22h0bcs2w7ih2ddj7nrhw0k4h4bqfx2ydg4ip7fb7p") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.5 (c (n "locspan") (v "0.7.5") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1qshilv4l2y7kngriv38vzs832q2351gncv1yamkx9mpxkj126sw") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.6 (c (n "locspan") (v "0.7.6") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "05z9s7q4cgslyfpbppns9r3s65d0fmnwqgvl3rl3k5k194dy047i") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.8 (c (n "locspan") (v "0.7.8") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1gabgnv6fljd36r4yvdid24dw9dyjccj7336hjdsdhcmz4h1z8dp") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.9 (c (n "locspan") (v "0.7.9") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0s7bccv936q2kgyyhazrkk1rr3r825vby3ldrwa041s0968f02w3") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.10 (c (n "locspan") (v "0.7.10") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "078q7lnyn1rzh9ga14hl704byl9asyhw3v477sgzlqil1fplvil3") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.11 (c (n "locspan") (v "0.7.11") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0nvjqgn64dnszaq2997p2grb8abry77k3z7b3is8a2rdv3pif1z1") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.12 (c (n "locspan") (v "0.7.12") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1b5rc3x9gzk93qap2x3qq73d3165ssi3fm7s51v9bh2w12xx7cn4") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.13 (c (n "locspan") (v "0.7.13") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0m1fwx0vqk1zid8aqw1j57s40zh979wh0g5g3csnq9p4lfsjcpk8") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.14 (c (n "locspan") (v "0.7.14") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0xm3x3sf5zmwl6biqvyf3a59ib2bhj6pvj11bgc3h6ylj3i11fsh") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.15 (c (n "locspan") (v "0.7.15") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1w997gl6mh0wyv54xjkcnbg4mpmg57rl63n2qmwjbwbv6ksrps51") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7.16 (c (n "locspan") (v "0.7.16") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wqxakalxj9xhrx7qb46s70asq9r1dxzmyn45rw903y5pbx43dbf") (f (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.8.0 (c (n "locspan") (v "0.8.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ifjazfn870r5a5agz5mnl4cd27g9f6dwn0in9vh9fwrgd3d1al3") (f (quote (("reporting" "codespan-reporting") ("default")))) (y #t)))

(define-public crate-locspan-0.8.1 (c (n "locspan") (v "0.8.1") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1klmvm0mvdvgbw2lzjal1mrp340ma1m3rf27i9gni8r6hkabbwmp") (f (quote (("reporting" "codespan-reporting") ("default")))) (y #t)))

(define-public crate-locspan-0.8.2 (c (n "locspan") (v "0.8.2") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "contextual") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "180029rmaz68a5prla2fxdfmxx91yd5r94i06na8xj7szi4h929k") (f (quote (("reporting" "codespan-reporting") ("default"))))))

