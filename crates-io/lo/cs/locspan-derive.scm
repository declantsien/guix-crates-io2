(define-module (crates-io lo cs locspan-derive) #:use-module (crates-io))

(define-public crate-locspan-derive-0.1.0 (c (n "locspan-derive") (v "0.1.0") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l86b78mg08k4wmqafqkwabbg2j81q18paslyiry2q0h9ym4qn9p")))

(define-public crate-locspan-derive-0.2.0 (c (n "locspan-derive") (v "0.2.0") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12fh9lmsw9mw4jl3dgzz0asbmvnk0vkbykj44nfi7hisa60wmvm0")))

(define-public crate-locspan-derive-0.2.1 (c (n "locspan-derive") (v "0.2.1") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1igshv2biji4pd0wr1dal97jbjbyjsw68aqhcqkfna3nhj0y35c4")))

(define-public crate-locspan-derive-0.2.2 (c (n "locspan-derive") (v "0.2.2") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ny5akwnm4kz9jc4j5646gp37h1bs85w8wdb7y8qs795gb2s1nvq")))

(define-public crate-locspan-derive-0.3.0 (c (n "locspan-derive") (v "0.3.0") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14267gvz36jn9hfrxa6hi38pv8lls5cvdbhcns2xi8003wy4f99v")))

(define-public crate-locspan-derive-0.4.0 (c (n "locspan-derive") (v "0.4.0") (d (list (d (n "locspan") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i9hp6m2r3six1m8vvsqa57jky035ipy29dvb56dbr8k1nsszd8q")))

(define-public crate-locspan-derive-0.5.0 (c (n "locspan-derive") (v "0.5.0") (d (list (d (n "locspan") (r "^0.5.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vla517zzp2dd33cbdn9snbxa4wf07js3f0j0mp05irs0sx8q4i0") (y #t)))

(define-public crate-locspan-derive-0.5.1 (c (n "locspan-derive") (v "0.5.1") (d (list (d (n "locspan") (r "^0.5.4") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01ry5fmw2jr1rzb16xxdd5wsr7qdkdh2dbnxdfb1b5an3drncc42")))

(define-public crate-locspan-derive-0.5.2 (c (n "locspan-derive") (v "0.5.2") (d (list (d (n "locspan") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z4rcq9r698c4sgiy1mrn1byw9dy12m8xli54xi44642zhi7y6fy")))

(define-public crate-locspan-derive-0.6.0 (c (n "locspan-derive") (v "0.6.0") (d (list (d (n "locspan") (r "^0.7.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06lnqh8wrpnxb6kghjzfyfh3c4sq75bc0q0zr8lkv6h47ci932g8")))

