(define-module (crates-io lo lo lolog) #:use-module (crates-io))

(define-public crate-lolog-0.1.0 (c (n "lolog") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0l61d6arq018p8a54gcmy6m9hi57zg895j1w6jxknq3aj8wl2f5l")))

(define-public crate-lolog-0.1.1 (c (n "lolog") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01daw12nwmgdpmdidjyhzqni8b11jsj47bajzapkzgx6b1rw432n")))

(define-public crate-lolog-0.1.2 (c (n "lolog") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "17dmc8ylv5yrcc3fpkr16xclq6qyaxd7inajrsmb4pkvd2ss7mal")))

