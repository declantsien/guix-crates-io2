(define-module (crates-io lo lr lolrofl) #:use-module (crates-io))

(define-public crate-lolrofl-0.1.0 (c (n "lolrofl") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "blowfish") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "0pgpydwqqr7ddakr3g1h8xxm7l83jad8y5l94jxr6vmivd6fycn4") (f (quote (("payload" "blowfish" "flate2" "base64") ("default" "payload"))))))

(define-public crate-lolrofl-0.2.0 (c (n "lolrofl") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "blowfish") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "1n6qb4g7d7a4q01j8ccgdx5qa63pbbsc237d5zhrylb7djk2m8gw") (f (quote (("payload" "blowfish" "flate2" "base64") ("default" "payload"))))))

