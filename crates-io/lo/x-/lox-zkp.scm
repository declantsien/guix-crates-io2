(define-module (crates-io lo x- lox-zkp) #:use-module (crates-io))

(define-public crate-lox-zkp-0.8.0 (c (n "lox-zkp") (v "0.8.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^4") (f (quote ("serde" "rand_core" "alloc" "digest" "precomputed-tables"))) (k 0)) (d (n "merlin") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w6krmy70yr5nhhhxbqkz73qyhn4plp40lhsh882q6nlmkqbd94r") (f (quote (("debug-transcript" "merlin/debug-transcript") ("bench"))))))

