(define-module (crates-io lo x- lox-lang) #:use-module (crates-io))

(define-public crate-lox-lang-0.0.1 (c (n "lox-lang") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)))) (h "17fzpmx6w8c28a0ny3y46b87x6lmnk5hj9c6cjrnrj6fl1pljrjy") (f (quote (("trace-scanning") ("trace-gc") ("trace-execution") ("trace-compilation") ("stress-test-gc") ("default"))))))

(define-public crate-lox-lang-0.0.2 (c (n "lox-lang") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)))) (h "0ig1m3wbx6f0w508klfn4ab2ndg59jgs4rimlcmj3pgnyyli6vcz") (f (quote (("trace-scanning") ("trace-gc") ("trace-execution") ("trace-compilation") ("stress-test-gc") ("default"))))))

(define-public crate-lox-lang-0.0.3 (c (n "lox-lang") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)))) (h "1qz2g8gv1xp8z2i7ksqw1kjndxyimjgris05j41p7b18qx1ay5wm") (f (quote (("trace-scanning") ("trace-gc") ("trace-execution") ("trace-compilation") ("stress-test-gc") ("default"))))))

