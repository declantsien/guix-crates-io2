(define-module (crates-io lo x- lox-scanner) #:use-module (crates-io))

(define-public crate-lox-scanner-0.0.1 (c (n "lox-scanner") (v "0.0.1") (h "1yrdn1ni41zmysfdp2hwgsbyimnj6digjic26grygrfzzwcplv8g")))

(define-public crate-lox-scanner-0.0.2 (c (n "lox-scanner") (v "0.0.2") (h "14121z5bszcj01mh0vpa3vnhfambhkg26c3z892qjnd30y5gacg2")))

(define-public crate-lox-scanner-0.1.0 (c (n "lox-scanner") (v "0.1.0") (d (list (d (n "peekmore") (r "~1") (d #t) (k 0)))) (h "11a5pxfy2ngvxpda9xq9z2247g6yi1h7x9xzd36rff9ayzhnjf02")))

