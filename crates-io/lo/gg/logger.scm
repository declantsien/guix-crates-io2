(define-module (crates-io lo gg logger) #:use-module (crates-io))

(define-public crate-logger-0.0.1 (c (n "logger") (v "0.0.1") (d (list (d (n "iron") (r "*") (d #t) (k 0)))) (h "1ikn5v45p438m2zqyfppni32krnsbin6yznpi5df4d4k1vs9w7s8")))

(define-public crate-logger-0.0.2 (c (n "logger") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0a416j3hsz26fssyvi26magdrp4v6crar72xwwiwkhrwmzphg4p7")))

(define-public crate-logger-0.0.3 (c (n "logger") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1jvdpdsdmrkd7xwrnawgbx4cxifnyl1ykqgf7nhhplr84kw1jzqy")))

(define-public crate-logger-0.1.0 (c (n "logger") (v "0.1.0") (d (list (d (n "iron") (r "^0.4") (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0ssnp4r3dl1gjr7vlaw47ci2byfvzd2qsqcihdq341736j0liwiw")))

(define-public crate-logger-0.2.0 (c (n "logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "iron") (r "^0.4") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0mi1hafi3qpq3r9jrbz2irnmzsg1w240z339fb63rz1rhmvnp4fw")))

(define-public crate-logger-0.3.0 (c (n "logger") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "iron") (r ">= 0.4, < 0.6.0") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "19rb32lmfn9wgpkns83ww7756ja2mn4q8szy27vk1zwplzwmkzwj")))

(define-public crate-logger-0.4.0 (c (n "logger") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "iron") (r ">= 0.4, < 0.7.0") (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "14xlxvkspcfnspjil0xi63qj5cybxn1hjmr5gq8m4v1g9k5p54bc")))

