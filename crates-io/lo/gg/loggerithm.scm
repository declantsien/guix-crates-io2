(define-module (crates-io lo gg loggerithm) #:use-module (crates-io))

(define-public crate-loggerithm-1.0.0-rc.1 (c (n "loggerithm") (v "1.0.0-rc.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "05vb0sxi2wfylcmckk7rd13aan51q4rd8y99hwaycy4g151dy6qs")))

(define-public crate-loggerithm-1.0.0-rc.2 (c (n "loggerithm") (v "1.0.0-rc.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0cdnzw41cq2aa0acc812hakfj8qi7wkrrdgd53cgpr28sjcn20pp")))

(define-public crate-loggerithm-1.0.0 (c (n "loggerithm") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0vlplaxd9smgaz0jr35jd14iisg869flcbmma4b8rs00k533ywi7")))

(define-public crate-loggerithm-1.0.1 (c (n "loggerithm") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "07gznpjlryv81azn8zg8hjjnxkkcd71d9z9mscbjmqbwsbf0038w")))

(define-public crate-loggerithm-1.0.2 (c (n "loggerithm") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0winxhpangpgap2cma7lxvzrc0hfkdp5r99zva69xzl8w93yin36")))

(define-public crate-loggerithm-1.1.0 (c (n "loggerithm") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "1nrgxr7y3vjnas29nllql5p2bs72ja58z76fisr71ngkxw17d5br")))

(define-public crate-loggerithm-1.1.1 (c (n "loggerithm") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)))) (h "0pzqg146fcgj41cp8kdhda73sbr6kqrzh7z37d9fhmrw376m8r6l")))

