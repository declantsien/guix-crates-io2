(define-module (crates-io lo gg logging4dummys) #:use-module (crates-io))

(define-public crate-Logging4Dummys-0.1.0 (c (n "Logging4Dummys") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0zf6wclmii9pvfjjbm13xla4dzhq9gx7afxlcl1akjn0hbjp4h4m") (y #t)))

(define-public crate-Logging4Dummys-0.1.1 (c (n "Logging4Dummys") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1pyavkvfbclri74k2zb12ddgygmq0yz3mhvpljfnmlgxdygsmfl8") (y #t)))

(define-public crate-Logging4Dummys-0.1.2 (c (n "Logging4Dummys") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1x68mbxml8ssdmgn935b32x0icp9h3ix17w10dqqhjr6wlwkj18k") (y #t)))

(define-public crate-Logging4Dummys-0.1.3 (c (n "Logging4Dummys") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1c64n7kq23z7vnhg0sdkv9lfswvb50xwrsrf9zh35gk1hnamdynk") (y #t)))

(define-public crate-Logging4Dummys-1.0.0 (c (n "Logging4Dummys") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "046y60494ljyw992kbfdjwfy1nhg1gixy1hhzh6lpr7knsidndnl")))

(define-public crate-Logging4Dummys-1.0.1 (c (n "Logging4Dummys") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1rmg3fs8i7r4akf0y3lzffc6swnpj21jipjqcz9jhzwnq6k0p3jh")))

(define-public crate-Logging4Dummys-1.0.2 (c (n "Logging4Dummys") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "03yg7355wlil10hjdg0j9xab050rcwgymn854pnjc9x5mkn751ij")))

