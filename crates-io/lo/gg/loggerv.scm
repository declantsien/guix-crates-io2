(define-module (crates-io lo gg loggerv) #:use-module (crates-io))

(define-public crate-loggerv-0.1.0 (c (n "loggerv") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1ng582jcjj26j4w1mq61l1n1zq2vz5h4xajyhyfrypzzb91skp79")))

(define-public crate-loggerv-0.2.0 (c (n "loggerv") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0k9vckf49na1vis3s3s7vza87smm1vjwy8xc1s58mbgg00fmf761")))

(define-public crate-loggerv-0.3.0 (c (n "loggerv") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0pvyivzqls36v8r0iv4a84cf4rh66c21m7a7x1ab57sw1ficrgq0")))

(define-public crate-loggerv-0.4.0 (c (n "loggerv") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "atty") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "03xgz6c62r56ibm2bpd9z4pnw1y3aaxmwbjwkl3k1aqg39nbjv1i")))

(define-public crate-loggerv-0.5.0 (c (n "loggerv") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "atty") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "1hlibf02rlqav5cnl11njfnh8skiclsqqbcnir0kpjd0mb7g84pv") (y #t)))

(define-public crate-loggerv-0.5.1 (c (n "loggerv") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "atty") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "0q9nq5bsarianwv6qa78ajipn6a16i0x9isw01kl0acrxphwkdsv")))

(define-public crate-loggerv-0.6.0 (c (n "loggerv") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "atty") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.3.7") (d #t) (k 0)))) (h "1wg488zg8lc71a563zppadmcwiada5piw7lkxasdvdpsaf98fy5i")))

(define-public crate-loggerv-0.7.0 (c (n "loggerv") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "atty") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1hz4bfp2161ax866qgjkyz4gp5wyw5hadkrfqnzx828ydhbnm6y8")))

(define-public crate-loggerv-0.7.1 (c (n "loggerv") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "13bh3p9ij4dhbayw1d42lap1fhgcs4f0r1r309p7q6bdjmj0csxs")))

(define-public crate-loggerv-0.7.2 (c (n "loggerv") (v "0.7.2") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0x6k903wpm557fkcbr5isgh4yqkkbzw4fm7hwyy61rvimqaxxn30")))

