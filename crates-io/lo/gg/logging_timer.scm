(define-module (crates-io lo gg logging_timer) #:use-module (crates-io))

(define-public crate-logging_timer-0.1.0 (c (n "logging_timer") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pfwqb0an1d9pabsd4p0698f7f4qhi47dw7y8wjigb58vn28wl1g")))

(define-public crate-logging_timer-0.2.0 (c (n "logging_timer") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b8hhhzrvmszn5j0s3dgz0g9kwyjz7xjpl0zqzg9vvafv6rbzl2s")))

(define-public crate-logging_timer-0.2.1 (c (n "logging_timer") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f1nwzknsrycbvn7vp5rmb31iqvkv52rndw61l1dsd22h2cccah4")))

(define-public crate-logging_timer-0.2.2 (c (n "logging_timer") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ysvpfkl3m3c9fa2xk43afx319xc99zzpk2f2khx4vwpg5jc8709")))

(define-public crate-logging_timer-0.3.0 (c (n "logging_timer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k362vigfl9gl5p4jyzzf10hr3ym292f01b8rsrymdpzr16k20ni")))

(define-public crate-logging_timer-0.4.0 (c (n "logging_timer") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x8pg5vaqfh7dfyx40zqjfwh2nnmx0p56gx4i9srdkpsgj4h66pi")))

(define-public crate-logging_timer-0.5.0 (c (n "logging_timer") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.5") (d #t) (k 0)))) (h "1mf6yqlm7sbx4js2z4l3xax9a8w73bgqhvncwdkxhicjzahm4567")))

(define-public crate-logging_timer-0.9.0 (c (n "logging_timer") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9") (d #t) (k 0)))) (h "0rkbiyjzizpkyq1vb0yszqrh8ixhdmqx2fbhfzrci77bnrhhwkxy")))

(define-public crate-logging_timer-0.9.1 (c (n "logging_timer") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.1") (d #t) (k 0)))) (h "0a5sg71qzv1pnw0mv5rikgr9kl0hkxah0zb9c11kqwwglk9l8qci")))

(define-public crate-logging_timer-0.9.2 (c (n "logging_timer") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.2") (d #t) (k 0)))) (h "0rr88m0nl8d1rvgav22l9d398348dd9cqlwcbqyrxh1s93zh54d6")))

(define-public crate-logging_timer-0.9.2-beta.0 (c (n "logging_timer") (v "0.9.2-beta.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.2") (d #t) (k 0)))) (h "1vqql985l70l991hlnl3kplkwqyzn57r39cnn6pp9v7ar5wwrzpc") (y #t)))

(define-public crate-logging_timer-0.9.3-beta.1 (c (n "logging_timer") (v "0.9.3-beta.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.2") (d #t) (k 0)))) (h "0h5n9k0jga9r0gn5m6r9rgdzxiv3ps5ix7p088fn9d149nj5l1np") (y #t)))

(define-public crate-logging_timer-0.9.3-beta.2 (c (n "logging_timer") (v "0.9.3-beta.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.3-beta.2") (d #t) (k 0)))) (h "1qhakfnc8s9p56j1rirrq252zw1grblwbbqjd8n6gxhqi320m5c4") (y #t)))

(define-public crate-logging_timer-0.9.9-beta.1 (c (n "logging_timer") (v "0.9.9-beta.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^0.9.9-beta.1") (d #t) (k 0)))) (h "1dgr9q76lrgqdas26vv2yyrqii2smz31g0151xdzmrfbaflkvl5l")))

(define-public crate-logging_timer-1.0.0 (c (n "logging_timer") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^1.0.0") (d #t) (k 0)))) (h "14x7wanqbfmisb8v3xvmwyxrwl6j2qdm0pvbz3wc45swjm4w5l20")))

(define-public crate-logging_timer-1.1.0 (c (n "logging_timer") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^1.1.0") (d #t) (k 0)))) (h "1sag52pv7p7imc7x69sbk76xqh9jh97bfsspma4p0jv83lk6zsb4")))

(define-public crate-logging_timer-1.1.1 (c (n "logging_timer") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "logging_timer_proc_macros") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wc44vrhfqvjlzdbndk7iphkhhjpjm21ar43vvvbb85lpjfw0san")))

