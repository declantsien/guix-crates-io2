(define-module (crates-io lo gg loggr) #:use-module (crates-io))

(define-public crate-loggr-0.1.0 (c (n "loggr") (v "0.1.0") (d (list (d (n "i-o") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.3.1") (d #t) (k 0)))) (h "089gwhbbr3ycgfy36pw0r6vapjx0ggvd8669l6zbw9bcg0sjfsf0")))

(define-public crate-loggr-0.2.0 (c (n "loggr") (v "0.2.0") (d (list (d (n "i-o") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.3.1") (d #t) (k 0)))) (h "038q4dj3xzgij061p255nryff57lr0k57y7nansjviziqk7826vy")))

(define-public crate-loggr-0.3.0 (c (n "loggr") (v "0.3.0") (d (list (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (k 0)) (d (n "unix") (r "^0.4") (d #t) (k 0)))) (h "0gv9ksmnv4vjw2ffcbli5gmy70kdlchdqncv3li5p35bjm19cjnd")))

