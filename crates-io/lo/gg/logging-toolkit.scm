(define-module (crates-io lo gg logging-toolkit) #:use-module (crates-io))

(define-public crate-logging-toolkit-0.1.0 (c (n "logging-toolkit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "05hy4q3mx9bvd4xia74asm6i3kbhzwsvmnnb6dvh45brmik6zdwa")))

(define-public crate-logging-toolkit-0.1.1 (c (n "logging-toolkit") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "1xixgbssjvgh9fywr809haqq6jvws64fkb85dizzm118a8bfdy2w")))

(define-public crate-logging-toolkit-0.2.0 (c (n "logging-toolkit") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "1bzib31pk8f5vcdyzijrw8br6sgxvxm5m1qdvj5hspc60lp7hjnb")))

(define-public crate-logging-toolkit-0.3.0 (c (n "logging-toolkit") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "1by4qaic79162d16b77g735wwp8j0ph31jd995kv2af4v40bkxq5")))

(define-public crate-logging-toolkit-0.4.0 (c (n "logging-toolkit") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "07dx4sy2mkmx9f1afjyzajlfd3z21faygqcg3djdc1c84g87i8sr")))

(define-public crate-logging-toolkit-0.5.0 (c (n "logging-toolkit") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-json") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)))) (h "17xla5vl71m4zh068zr5kh3ha50h27vzbifsvvnw6p1qlrrwhnqb")))

