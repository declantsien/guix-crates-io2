(define-module (crates-io lo gg logging-allocator) #:use-module (crates-io))

(define-public crate-logging-allocator-0.1.0 (c (n "logging-allocator") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 0)))) (h "0315r8nb7996l5fvkawry0kjmbfm2d2di9qlpmkg29kwnzwrjfxl")))

(define-public crate-logging-allocator-0.1.1 (c (n "logging-allocator") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1h6157da1880fffr1iz2p2vi35bwd42yv4vf0rbalmrbpqksj8ca")))

