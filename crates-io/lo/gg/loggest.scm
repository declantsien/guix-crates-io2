(define-module (crates-io lo gg loggest) #:use-module (crates-io))

(define-public crate-loggest-0.1.0 (c (n "loggest") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)))) (h "1r6b5lvr6z6w5ksslppaarkbzfrvgv6kp8xkqqgjhl4l8rrkkf4k")))

(define-public crate-loggest-0.2.0 (c (n "loggest") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q47zxbjggrn0iamk1yiyq3469figbj5shds5qbld1fn4mdcavxw")))

(define-public crate-loggest-0.2.1 (c (n "loggest") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0raap3alyla60a8phq3021zcqz9h132vz53ddig40dpfpbd8rya4")))

(define-public crate-loggest-0.2.2 (c (n "loggest") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "nix") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17v076w2k6bypkmkwrp3ydrfyzqj6v9svqxkcmj4prwfqf1dsgzi")))

