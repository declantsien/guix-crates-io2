(define-module (crates-io lo gg logger_utc) #:use-module (crates-io))

(define-public crate-logger_utc-0.1.0 (c (n "logger_utc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0cfp2ijg0yllahhfjd757y4lb08m80w7ynrzh6fz8s79rg3ca9zh")))

(define-public crate-logger_utc-0.1.1 (c (n "logger_utc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "08771kzgpkm60rx9fq4vb3sl0n2gm67s787wzwkspd0zdm3d7x4z")))

(define-public crate-logger_utc-0.1.2 (c (n "logger_utc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0h2f0yq09r7v2bbh1ahhfad7mxaniixlb2p46krbvjdw46dgzkdj")))

(define-public crate-logger_utc-0.1.3 (c (n "logger_utc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "13sw937yhbylkpxn0j771by68a6w14fs48fcnf1axdyp0lx69a4v")))

(define-public crate-logger_utc-0.1.4 (c (n "logger_utc") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0v8bbsfnh463ad6vqpxfysfnfxhb4shqjxakf35907blyg7awgf2")))

(define-public crate-logger_utc-0.1.5 (c (n "logger_utc") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "123dva039hi09ki1s1zq2xpvrkh1ca4pj17dhm5lyb2531yd6cl0")))

(define-public crate-logger_utc-0.1.6 (c (n "logger_utc") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0jh24yrck6b76lra4ams9gj51qryc0g5lkwqv23sgb5a675jwn19")))

(define-public crate-logger_utc-0.1.7 (c (n "logger_utc") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "1z61lvwwg8q9dhj62dxjrb2hvmhfx69k30szyjdza0r5nhl30dip")))

(define-public crate-logger_utc-0.1.8 (c (n "logger_utc") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0b5359qlix4hrsz13zqm3nkasbxwlgqwic3hnwn716rx4cch7nbs")))

(define-public crate-logger_utc-0.1.9 (c (n "logger_utc") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0vcx0h06152yid0bdyb3w7cw6jixd6nl78hdzfkng3jli86d9wly")))

(define-public crate-logger_utc-0.1.10 (c (n "logger_utc") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1v83bpq4qxrlh7jfp0vzg78wppkm4mh1i8yrl7s1v7apsvl5k7q1")))

(define-public crate-logger_utc-0.1.11 (c (n "logger_utc") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "14xlrid8k0f172i6zj6n5ikwzj0lmyyx0d3axbx3pd1ddsdwf1wy")))

