(define-module (crates-io lo gg logga) #:use-module (crates-io))

(define-public crate-logga-0.1.0 (c (n "logga") (v "0.1.0") (h "1n9v63m7gx7jrwicaicjsnsdsxzp04g2n57g3yqwr8zwk5pk6y4w")))

(define-public crate-logga-0.1.1 (c (n "logga") (v "0.1.1") (h "18n7i46j5vlfm1w8dpzbwnxqq8hxr6wmbnvhm4xrmws91115n22d")))

(define-public crate-logga-0.1.2 (c (n "logga") (v "0.1.2") (h "0yszy8iwhk4akr74n060r1ckcbh80ahgffb6jzs0wdx00z2y867y")))

