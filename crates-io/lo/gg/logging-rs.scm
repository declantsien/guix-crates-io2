(define-module (crates-io lo gg logging-rs) #:use-module (crates-io))

(define-public crate-logging-rs-1.0.0 (c (n "logging-rs") (v "1.0.0") (h "089dsvnjdklcvn7wfy8c0z9ksbx5ljkz9vmyfp70g0bfpy257dpp") (r "1.69")))

(define-public crate-logging-rs-1.1.0 (c (n "logging-rs") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1p9ibmbyanzv1kpwnjn9w74rnil5273shrfis0dgri43zv0jaxfd") (r "1.69")))

