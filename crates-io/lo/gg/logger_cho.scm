(define-module (crates-io lo gg logger_cho) #:use-module (crates-io))

(define-public crate-logger_cho-0.1.0 (c (n "logger_cho") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "1kyqn0lzmfm4gchampjyhhvnwgcrn4qgy64lr19qv9cpq225dp6z")))

(define-public crate-logger_cho-0.1.1 (c (n "logger_cho") (v "0.1.1") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "09ny1jsgk0ji3v9r5g5jh0p0xf3lk51y91fvh4qka9z6s259vhv7")))

(define-public crate-logger_cho-0.1.2 (c (n "logger_cho") (v "0.1.2") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "0y9sycn6axpjj18jv29zf3nnm9a9k8w7isx36jikwhb69ifx516c")))

(define-public crate-logger_cho-0.1.3 (c (n "logger_cho") (v "0.1.3") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "0v1dv8skd3g8wk4swrqs7xw0w5c3580wg9zsk25xihz65mz1s9aq")))

(define-public crate-logger_cho-0.1.4 (c (n "logger_cho") (v "0.1.4") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "0sz10hrk300k0sqxprq1lp41spb6lcbp7090ipn8rxzjky7phih8")))

(define-public crate-logger_cho-0.1.5 (c (n "logger_cho") (v "0.1.5") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "144yhmx9szvslz8vhw907h6qikrnwpn2cha9nsh2qn9w6aylh83y")))

(define-public crate-logger_cho-0.1.6 (c (n "logger_cho") (v "0.1.6") (d (list (d (n "android_logger") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)))) (h "126l9gxn11cc1r8pyksad2v9dqa07mc5n49fsm5yn0rh5wxvscxz")))

