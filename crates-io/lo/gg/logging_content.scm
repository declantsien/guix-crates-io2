(define-module (crates-io lo gg logging_content) #:use-module (crates-io))

(define-public crate-logging_content-0.1.0 (c (n "logging_content") (v "0.1.0") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "simplelog") (r "^0.10") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gjfv6jas7kmpbf91vp1qvhdyjdniapvb9wb3sdhm0gcdcjrlz7m")))

(define-public crate-logging_content-0.1.1 (c (n "logging_content") (v "0.1.1") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.7") (d #t) (k 2)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0n98w85bbidhp5v8d9zbdksa2bksr3yiz552m18fwrzxgz1xz91n")))

