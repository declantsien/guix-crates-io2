(define-module (crates-io lo gg loggly) #:use-module (crates-io))

(define-public crate-loggly-0.1.0 (c (n "loggly") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "16ifbvvpi5h6gfcp5q82v0hxipkh6dykxwp1pssncyiqpnmx4k45")))

