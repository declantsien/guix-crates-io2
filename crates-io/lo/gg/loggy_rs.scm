(define-module (crates-io lo gg loggy_rs) #:use-module (crates-io))

(define-public crate-loggy_rs-0.0.1 (c (n "loggy_rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0lx3gq2r823cswincwws6zmf2fb5hsq074lwqqxdiwr09vgvip7f") (y #t)))

