(define-module (crates-io lo gg loggy-macros) #:use-module (crates-io))

(define-public crate-loggy-macros-0.1.0 (c (n "loggy-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q636vkiifq839n3h8jia0z68ld509b81h1vph390rfxymxh0bqj")))

(define-public crate-loggy-macros-0.2.0 (c (n "loggy-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i6m1nd10aiw7981z1xhq695lcx1hrs2scmjrcacwnh4hkzfakr4")))

