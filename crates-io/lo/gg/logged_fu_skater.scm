(define-module (crates-io lo gg logged_fu_skater) #:use-module (crates-io))

(define-public crate-logged_fu_skater-0.1.0 (c (n "logged_fu_skater") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0mph13nald3rap7aj4a9zrprx1nyrcfkk2d4z53c1nmwv1qrsnnj")))

(define-public crate-logged_fu_skater-0.1.1 (c (n "logged_fu_skater") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1fplviwdk22m7jdl3ygnysr6mqim4x4zvk8d1745439vwgzar0gk")))

(define-public crate-logged_fu_skater-1.0.0 (c (n "logged_fu_skater") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0813zkam96pnzf6ym4y0hkaq0c2g1q7g2xr4q9df7170cnl7rglx")))

(define-public crate-logged_fu_skater-1.0.1 (c (n "logged_fu_skater") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "08dy8gp1sgmc2gipyb4yxs24953jx4g7px7kr4c2agqzdiv7yf5f")))

(define-public crate-logged_fu_skater-1.0.2 (c (n "logged_fu_skater") (v "1.0.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "05l6rdsirly0x5gqm6bihlm0kcf4i91w5gg8bps2191g63h52ggm") (f (quote (("default-implementations"))))))

