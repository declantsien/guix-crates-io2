(define-module (crates-io lo gg logger-rust) #:use-module (crates-io))

(define-public crate-logger-rust-0.1.0 (c (n "logger-rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0wfp1a3cd78wa2ksb5gfzblc2893v841abbhrz4kw8k96m4cirff") (y #t)))

(define-public crate-logger-rust-0.1.1 (c (n "logger-rust") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1c6j16hbgaf678wz64vs0bmgxkadwsak7jsvs18rgzy619b43fi3") (y #t)))

(define-public crate-logger-rust-0.1.2 (c (n "logger-rust") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0d0hzyadr95n1x5yrv9m1433hfy5kh8al1yp6bcyklp71flcznmg")))

(define-public crate-logger-rust-0.1.35 (c (n "logger-rust") (v "0.1.35") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1dygda2pysc236xqbz38q8y0xh1vmqd79pf2x7x1h7k05pfq183b")))

(define-public crate-logger-rust-0.1.37 (c (n "logger-rust") (v "0.1.37") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "169ry8bhajfd69d91g0s1xxisib0085gy1vr15ypygc0yia8yfwi")))

(define-public crate-logger-rust-0.1.39 (c (n "logger-rust") (v "0.1.39") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1w3rml5wrngvic29pwqr0wra7sy4h0ppiqlz750a6ifx5xyi3sic")))

(define-public crate-logger-rust-0.1.40 (c (n "logger-rust") (v "0.1.40") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0q97cy112ajl5vzf0jsdhldrp7nk4cly21fy1lb330h6gziszf1q")))

(define-public crate-logger-rust-0.1.41 (c (n "logger-rust") (v "0.1.41") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0nvmnbz9h6q0nj6q4l1zhmr25s1fgg2phsxzv3wp78wywfl0jn39")))

(define-public crate-logger-rust-0.1.42 (c (n "logger-rust") (v "0.1.42") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0x6519vsa14c5w3rbj6aifmn7dzsjcpdvcv0il7k6lh0icggqcl3")))

(define-public crate-logger-rust-0.1.43 (c (n "logger-rust") (v "0.1.43") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1cawkisn8rl2c39r50m8ch9dj8fb1jm14gp66b45rmwqpkzr7mis")))

(define-public crate-logger-rust-0.1.44 (c (n "logger-rust") (v "0.1.44") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "01d58rnrdidqm8h2snfmpqxnimaw6wcr40mnhg5i1grjmszp18y6")))

(define-public crate-logger-rust-0.1.45 (c (n "logger-rust") (v "0.1.45") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0jjv1lyjwcb3s2scyd82c4563yqdzxfh0mr61dis691mrvlbbz16")))

(define-public crate-logger-rust-0.2.0 (c (n "logger-rust") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "00pwfngkbpcd8k3kihxspvrvrzblyhap44dckas0nkji5sip8wn2")))

(define-public crate-logger-rust-0.2.10 (c (n "logger-rust") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1m7272fpw3j5qsr43qfl1kpc5q1qznbfv7w4sac361jnn3jg0srs")))

(define-public crate-logger-rust-0.2.12 (c (n "logger-rust") (v "0.2.12") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "03030d3czxy0yf1cxh9145kpkgqwjb6zp4ljcl97njmgh8va8pmv")))

