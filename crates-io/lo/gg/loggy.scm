(define-module (crates-io lo gg loggy) #:use-module (crates-io))

(define-public crate-loggy-0.1.0 (c (n "loggy") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "0s8ym0iapxwbq981vrqk0px54afkqljdk8zbywcagd6y7vrgq3hr")))

(define-public crate-loggy-0.1.1 (c (n "loggy") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "065f1sg76zkd3qxycz2zsa0r1w7nlm0hsb4s8qinw8f7qdp1cr1y")))

(define-public crate-loggy-0.1.2 (c (n "loggy") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "03xbqgagi9vadsy6d2qjz2dm672mgl0rkrksc92fcglj0qsv9xgg")))

(define-public crate-loggy-0.1.3 (c (n "loggy") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "09i62m4fw86i5fg9fqvb41sv667a076nh1vps8lhsq87z6ah0gz2")))

(define-public crate-loggy-0.1.4 (c (n "loggy") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1vldlyrqm31l5xm3f4lsp7r4c8hvnf6khzjcpl71225vc866cqvn")))

(define-public crate-loggy-0.1.5 (c (n "loggy") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "0vhi31msz3mjqv9d1m4yb2i50888714sa2wazliacbq15qyf7m7b")))

(define-public crate-loggy-0.1.6 (c (n "loggy") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "1lr6gdbvw1x5hshc4xfh9xf9gnsl1h141k4jn4ygwd3zgqrsqza0")))

(define-public crate-loggy-0.1.7 (c (n "loggy") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "1wlq6209cn7ll7ky3fknhgpmqs7mzxcpnnpv5x37h6fvh9xxn4zz")))

(define-public crate-loggy-0.2.0 (c (n "loggy") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "153yjp0dj6qnsz0knz4j9frj8rhpn4ryzc8jb1bksrs4dkwghv6k")))

(define-public crate-loggy-0.2.1 (c (n "loggy") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "12zvcyrv3w01ykbrr3j92l3jvs7yjacaapxa79qhm10ll0fj401n")))

(define-public crate-loggy-0.3.0 (c (n "loggy") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "0pxvs6vx06kp3pyirq6246jpr06j0wix3bd1napkymh8dnyfync7")))

(define-public crate-loggy-0.3.1 (c (n "loggy") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "1qlmdz4hafp1qhqakl109ksgf3r6a0nck0g2y4vb7y70343nqlmz")))

(define-public crate-loggy-0.3.2 (c (n "loggy") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "unindent") (r "^0.1.2") (d #t) (k 0)))) (h "088gwwy5hdmg4z02f9zj6cdi2al0nriavnmfqmq5859vacng0v8y")))

(define-public crate-loggy-0.4.0 (c (n "loggy") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "loggy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "time") (r "^0.3.1") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.7") (d #t) (k 0)))) (h "141xdig58bp2807mbms30ld1dadymj3praxdhdvnh8vj5mrf21q9")))

(define-public crate-loggy-0.5.1 (c (n "loggy") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "loggy-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "time") (r "^0.3.1") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.7") (d #t) (k 0)))) (h "1xkkmxp3q4h8irad2c0mq3bxx21rg66gqy08m5400ynxbzwmm3r2")))

(define-public crate-loggy-0.5.2 (c (n "loggy") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "loggy-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("local-offset" "formatting"))) (d #t) (k 0)) (d (n "unindent") (r "^0.1.9") (d #t) (k 0)))) (h "0q1ay43swifs03mxlnnmv4jyysc3rp5ckzz8jwj342p987gsl2yh")))

(define-public crate-loggy-0.5.3 (c (n "loggy") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "loggy-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.9") (d #t) (k 0)))) (h "0vihsc68qmbfgvlqssk6xaddwy88hk6vrca305hh197wiriq6pza")))

