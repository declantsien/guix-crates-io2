(define-module (crates-io lo gg logger-rs) #:use-module (crates-io))

(define-public crate-logger-rs-0.1.0 (c (n "logger-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "01vldbhlrkibnp7brnsh3nya0wz7bzkvgdhg3chjnxlcmgawd29p")))

