(define-module (crates-io lo ad load_lpp) #:use-module (crates-io))

(define-public crate-load_lpp-11.0.3 (c (n "load_lpp") (v "11.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "4.0.*") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "0vi7n87qrp75p17w7jvindnvjwx621rvbgzw1h8l4nsqaq9wllzh")))

