(define-module (crates-io lo ad loading) #:use-module (crates-io))

(define-public crate-loading-0.0.1 (c (n "loading") (v "0.0.1") (h "0wrfh3mmlb0r47l1622dcwl2r09vgflxvlbd2n9llshcgh2g8wy9") (y #t)))

(define-public crate-loading-0.1.0 (c (n "loading") (v "0.1.0") (h "1d2m8y7a33jnqh7gg8cshdn8psyrvb5x5axypm1jplc30l0z7h3s") (y #t)))

(define-public crate-loading-0.1.1 (c (n "loading") (v "0.1.1") (h "02pz4nrvbp8j613niyg0ndpwbpbxki42ah4v08dlnc7lszjdrl19") (y #t)))

(define-public crate-loading-0.1.2 (c (n "loading") (v "0.1.2") (h "1d16cc30cmk1h3i6h4zzawxbx7nwbzcndhcchd7drz7n3fkymynh") (y #t)))

(define-public crate-loading-0.2.0 (c (n "loading") (v "0.2.0") (h "0cxr3h9g437wxblm1qk8ar8kky2p1madj11lvl0l9k5j3i9zn4gm") (y #t)))

(define-public crate-loading-0.2.1 (c (n "loading") (v "0.2.1") (h "0nsdg0p30sywvqkmhqfz3y9729hs3hif2c4cdnap2406y256m69g") (y #t)))

(define-public crate-loading-0.3.0 (c (n "loading") (v "0.3.0") (h "0zrm044wnha875mdkxll4r197s0r2gkfwj41awin85vj6aq20r37")))

