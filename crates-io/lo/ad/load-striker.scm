(define-module (crates-io lo ad load-striker) #:use-module (crates-io))

(define-public crate-load-striker-0.1.0 (c (n "load-striker") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n4f2rk90qrghxx6sic4xyabrngpcsvkdw49sn4s96l903n11jhc")))

(define-public crate-load-striker-0.1.1 (c (n "load-striker") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03wa65ard63l06b00m0zfn922780qjqnyg0dx99qafah5jrnqy17")))

(define-public crate-load-striker-0.1.2 (c (n "load-striker") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19xa59v0zaa0bhbplafy8gp31w3lymmxsk8f88dpzprygxx9jn6p")))

