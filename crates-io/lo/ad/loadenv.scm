(define-module (crates-io lo ad loadenv) #:use-module (crates-io))

(define-public crate-loadenv-0.1.0 (c (n "loadenv") (v "0.1.0") (h "1rbwlm085vpmimsdqd3hyawhiyvh4wdy0n3fqkk6464zcasd68lc")))

(define-public crate-loadenv-0.1.1 (c (n "loadenv") (v "0.1.1") (h "11q3b7w8gfvakjgzp3ma5bdmn4qipa5hp0vjgz3y49rbkncdcf9p")))

(define-public crate-loadenv-0.1.2 (c (n "loadenv") (v "0.1.2") (h "01brbqkz6jvqw4wa3xjq4mivjgkgq3w5z0nwzmg4inv6nala5azz")))

(define-public crate-loadenv-0.1.3 (c (n "loadenv") (v "0.1.3") (h "0as1yzd1lhv2fyhw52c5jm38whvmm5k5016vfxh4mc160sw2s5f3")))

(define-public crate-loadenv-0.1.4 (c (n "loadenv") (v "0.1.4") (h "0c1crbb08xzpfd5iir2ici6wgsrhc76006fc757i9cbjk0chv8r5")))

