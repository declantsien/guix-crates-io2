(define-module (crates-io lo ad load_feature_bc) #:use-module (crates-io))

(define-public crate-load_feature_bc-0.1.1 (c (n "load_feature_bc") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "io_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0zpwnm4sivknxdnh2g4aj42fqak5xiyzhjlqzl20xfdn8506rk8c")))

(define-public crate-load_feature_bc-0.1.2 (c (n "load_feature_bc") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "19q6nzxff17941ahmjkg47xycx2mfad7mkbys531ggw1d1a5p0fi")))

(define-public crate-load_feature_bc-0.1.3 (c (n "load_feature_bc") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0p1npsicic2zl6gf87gmhn4bl4p8aiajh3p2q0wbqsmgmwsh97cv")))

(define-public crate-load_feature_bc-0.1.4 (c (n "load_feature_bc") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "045f9gr8i0qb7p53hgz7k5wba43836ag99r38ff26k8ibxzmpnvh")))

(define-public crate-load_feature_bc-0.1.5 (c (n "load_feature_bc") (v "0.1.5") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.2") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "019j26f4nq7j77aghykvhr8r7xrpz68x5b31ansrri3dys08s9jh")))

(define-public crate-load_feature_bc-0.1.6 (c (n "load_feature_bc") (v "0.1.6") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0qdvmwz0lb4fybjhrl91b1hgfigpy1q92lx5706d0gbnhyk11c3f")))

(define-public crate-load_feature_bc-0.1.7 (c (n "load_feature_bc") (v "0.1.7") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "io_utils") (r "^0.3") (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0pmvc6chdwnyv5c9d42qp9g6ss4jgdc24lqc1nd5rzkbh5n8fy5z")))

