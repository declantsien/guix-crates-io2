(define-module (crates-io lo ad loadless) #:use-module (crates-io))

(define-public crate-loadless-0.1.0 (c (n "loadless") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1bch7sx2cz50m9q1927164vmp7z6scw59pczynr3bmrsc0fgzrj7") (y #t)))

(define-public crate-loadless-0.1.1 (c (n "loadless") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1h9ssrv2yaw38y4wzkvs7bia5jg4ggxzy7n0xrh4vy6s79sqhdd0") (y #t)))

(define-public crate-loadless-0.1.2 (c (n "loadless") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0ja2g7g80h6ddmivqy9smmbckkvqhpakk2bjjkkvdl7p001pk87d") (y #t)))

(define-public crate-loadless-0.1.4 (c (n "loadless") (v "0.1.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0qsgngsxjasbmbqbfxzyznv6mp1ik126z4cifzy25cm3xi6skmpp")))

