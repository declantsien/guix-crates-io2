(define-module (crates-io lo ad load_sql_proc) #:use-module (crates-io))

(define-public crate-load_sql_proc-0.1.3 (c (n "load_sql_proc") (v "0.1.3") (d (list (d (n "minify_sql") (r "^0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "0q9zk465y3r5i2w0ihx93775rxzl5q9aki5wv9h3lxxqlqj48kdj")))

(define-public crate-load_sql_proc-0.1.4 (c (n "load_sql_proc") (v "0.1.4") (d (list (d (n "minify_sql") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "0xx9gpjyg7gxmbk8cbp5w27gmjszi0m4p42fy4zng6595yhb6vam")))

