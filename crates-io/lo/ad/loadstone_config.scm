(define-module (crates-io lo ad loadstone_config) #:use-module (crates-io))

(define-public crate-loadstone_config-1.0.0 (c (n "loadstone_config") (v "1.0.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "ecdsa") (r "^0.11") (f (quote ("pem"))) (k 0)) (d (n "enum-iterator") (r "0.6.*") (d #t) (k 0)) (d (n "itertools") (r "0.10.*") (d #t) (k 0)) (d (n "p256") (r "^0.8.1") (f (quote ("ecdsa" "sha256" "pem"))) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "tightness") (r "1.0.*") (d #t) (k 0)))) (h "0cssc1zq53myjq3phwd4wq3ahwmkpglccc6mpvzxmq0ygyvrkzfp")))

