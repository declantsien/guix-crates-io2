(define-module (crates-io lo ad loading_bar) #:use-module (crates-io))

(define-public crate-loading_bar-1.0.0 (c (n "loading_bar") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0bphdn6nj4pnm0mfaszlxpkmdwcr8yba47y18ai94wvrmz4xwgk0")))

(define-public crate-loading_bar-1.0.1 (c (n "loading_bar") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0pdani8y94vv3rk7wk9lyvjpyxxqg4p0pdandvkrh7zz2v2g06zi") (y #t)))

(define-public crate-loading_bar-1.0.1-beta.0 (c (n "loading_bar") (v "1.0.1-beta.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0rmz7xvgrszcsqw8dfg20w3crpj9adp315a8gr2di2027pifv1rm")))

(define-public crate-loading_bar-1.1.0 (c (n "loading_bar") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1b1z9h3pfzckalg6jmn0m3bd1j69qj3w71vid0wmv7h47fassj16")))

