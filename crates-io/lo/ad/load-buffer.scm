(define-module (crates-io lo ad load-buffer) #:use-module (crates-io))

(define-public crate-load-buffer-0.1.0 (c (n "load-buffer") (v "0.1.0") (h "0lw157xpx9xqs3jd09i36n4h1lwkc4ffdws05d59j2hqki658pfa") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-load-buffer-0.1.2 (c (n "load-buffer") (v "0.1.2") (h "1sdfsd502qf7664zbsc5v2798nvra63ah1hv58ip2dpdnpr7pf81") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-load-buffer-0.1.3 (c (n "load-buffer") (v "0.1.3") (h "0fm70xjmhy3yx75zbql5njf5my3c1s1hgaj3wwci31db310dpwjw") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-load-buffer-1.0.0 (c (n "load-buffer") (v "1.0.0") (h "11wlkwfr8jnsams07ya0103w95rlb2lmz7zgh74ir2jsl3cfxs32") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

