(define-module (crates-io lo ad loadingbar) #:use-module (crates-io))

(define-public crate-loadingbar-1.0.0 (c (n "loadingbar") (v "1.0.0") (h "00y6bi7m1v2dmhsbs25qk1sf7kxy0smcnq9pas3n2q2ps374hhqr")))

(define-public crate-loadingbar-1.0.1 (c (n "loadingbar") (v "1.0.1") (h "0qx0m2bvjjr03zhfir4n340rwbl323kndbq9yig2j7dz8cfpc0fa")))

