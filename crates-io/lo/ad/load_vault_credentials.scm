(define-module (crates-io lo ad load_vault_credentials) #:use-module (crates-io))

(define-public crate-load_vault_credentials-0.1.0 (c (n "load_vault_credentials") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0diggd9bz6c3macqlwyg7dlr5ylvyrnvc3bpa6v8skf5w278kl4q")))

(define-public crate-load_vault_credentials-0.1.1 (c (n "load_vault_credentials") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "01b2hzb9pjk6qc6dr8awak684blj2z1m111scizbw1kj9pm1x05v")))

