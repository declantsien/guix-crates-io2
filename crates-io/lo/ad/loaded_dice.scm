(define-module (crates-io lo ad loaded_dice) #:use-module (crates-io))

(define-public crate-loaded_dice-0.1.0 (c (n "loaded_dice") (v "0.1.0") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1avxqwza4l6zxcfphbc2wskr1sfm60g219xp4vnm1cl3lrk16xib")))

(define-public crate-loaded_dice-0.1.1 (c (n "loaded_dice") (v "0.1.1") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0n7pi1clk0isjdgpplsg0p2i8lkp7178qibv4rsh8xzhd7jxmc61")))

(define-public crate-loaded_dice-0.2.1 (c (n "loaded_dice") (v "0.2.1") (d (list (d (n "rand") (r "0.7.*") (d #t) (k 0)))) (h "1w028ydqfzbrd3mc96fyfyfyi2srw5jy93vh8ns3ixv430nw90dn")))

(define-public crate-loaded_dice-0.2.2 (c (n "loaded_dice") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (k 0)))) (h "0hwb9rq599lgxfvvk7ag4b6bh7j46jxi8bd990amxf8y9547jkzn") (f (quote (("std" "rand/std" "rand/std_rng"))))))

