(define-module (crates-io lo ad load_file) #:use-module (crates-io))

(define-public crate-load_file-0.1.0 (c (n "load_file") (v "0.1.0") (h "06i8ympnww42swacx7c8pcn03ybzjyqcgllpp7bcifzrw6hxrg03")))

(define-public crate-load_file-0.1.1 (c (n "load_file") (v "0.1.1") (h "0n31h7bamafxma7m35ar5xadx9c4sccnm05fqlcdnijn4qgwc0dh")))

(define-public crate-load_file-0.1.2 (c (n "load_file") (v "0.1.2") (h "042w0fwsmkq6qmdprvbdf4br2nj5rl30mjaqsps2335gbqa81mcg")))

(define-public crate-load_file-0.1.3 (c (n "load_file") (v "0.1.3") (h "169igf484ca8jnqkmww63vbdb95fkc5mmrkvifpjrb3rbcf4ipr5")))

(define-public crate-load_file-1.0.0 (c (n "load_file") (v "1.0.0") (h "1k202qahil4k8vklk595xzja082ppf8zkg4n6584bg9z3djfqzxr")))

(define-public crate-load_file-1.0.1 (c (n "load_file") (v "1.0.1") (h "1kbr7vg432r48kmql3q80hdh6gqj9c6lv91n001l32a2cb74pnii")))

