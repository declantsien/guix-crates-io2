(define-module (crates-io lo ad loadconf) #:use-module (crates-io))

(define-public crate-loadconf-0.1.2 (c (n "loadconf") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "033p7kx8chhcs0758h9171xklay5yfnb7hzxvcwyqs4a1dhv7fp2")))

(define-public crate-loadconf-0.2.0 (c (n "loadconf") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "15qjvsqc7ajc2cgvr241nkcl8afm9sf14fc7aqc16vlidiw55v7k")))

