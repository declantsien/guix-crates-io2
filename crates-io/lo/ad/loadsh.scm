(define-module (crates-io lo ad loadsh) #:use-module (crates-io))

(define-public crate-loadsh-0.1.0 (c (n "loadsh") (v "0.1.0") (h "0zv207glk69fvhmybcpvzhhw358zf8wb01amf18cfkfip0dph23b")))

(define-public crate-loadsh-0.1.1 (c (n "loadsh") (v "0.1.1") (h "0zn2gh79w8m6v2dm60kgnk49w09nyw7b1z7hqqfzqfljhjfbilxr")))

(define-public crate-loadsh-0.1.2 (c (n "loadsh") (v "0.1.2") (h "0q1wp0kvz4zi3mjkgqprls0rvw5w7b8w2nraqg8zvn07zsjg3mwa")))

