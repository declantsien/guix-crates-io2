(define-module (crates-io lo ad load-dotenv) #:use-module (crates-io))

(define-public crate-load-dotenv-0.1.0 (c (n "load-dotenv") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1r2ij8csvn0w79pbxsmfvs2gi1f5qlazxdwm8bg6w731pfg0w6ng")))

(define-public crate-load-dotenv-0.1.1 (c (n "load-dotenv") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "14n6wrc732gnj0xkl0nbjvzb7vmasz3w8h5zl623s0bpcmhxblfd")))

(define-public crate-load-dotenv-0.1.2 (c (n "load-dotenv") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)))) (h "0m2bhr1azs8jxsz5x73sd2gbx2s3laycsqaaamiw39m2imnwzavi")))

