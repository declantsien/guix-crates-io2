(define-module (crates-io lo ad load-env) #:use-module (crates-io))

(define-public crate-load-env-0.1.0 (c (n "load-env") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1ibhwn035x8n7ygvg98fh18mp6la143pl2babvhx7v76xl1255j2") (y #t)))

(define-public crate-load-env-0.2.0 (c (n "load-env") (v "0.2.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "0j5i5ih522bxf0a3yxf04kyblm8pb76lygx36a14nk8r07m3bqvq")))

(define-public crate-load-env-0.3.0 (c (n "load-env") (v "0.3.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)))) (h "1vgqz43lz2sbx59ncplcq86kwsilmyimm5kz2vcqiv807mlwpfb3")))

