(define-module (crates-io lo bs lobster) #:use-module (crates-io))

(define-public crate-lobster-0.1.0 (c (n "lobster") (v "0.1.0") (h "0n9as9fbghhqpm6jhavqpswf22symhlqdgbw651ilra9bks8sf4z")))

(define-public crate-lobster-0.2.0 (c (n "lobster") (v "0.2.0") (h "173b1h5kp1zs3jv9rmlzsmx99y3ilipbp839cgmg1sxbki0lr6dh")))

(define-public crate-lobster-0.3.0 (c (n "lobster") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1850xx5gczqk64zzprv0miimry9jg6l35rimcwr5sxp5iy6yd8hi")))

(define-public crate-lobster-0.3.1 (c (n "lobster") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1zs5cslzifgai1hscwkn4qppsn9cwx1a1g7b3h3mvgzliv838h9y")))

(define-public crate-lobster-0.4.0 (c (n "lobster") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0bmnw1xxjb8bv40lzkqcawxjfdkav5lzjsmz7n83llv7mab7mz1g")))

(define-public crate-lobster-0.5.0 (c (n "lobster") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "103n2sirwkrm9vykif82nzb8lgbggyi1xg98db1zvp927b7jg7y9")))

(define-public crate-lobster-0.6.0 (c (n "lobster") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0qlj0iyrrgmcw5b5qlcjcv0ph8ipj19j609j39ifkpc45r6by9md")))

(define-public crate-lobster-0.6.1 (c (n "lobster") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0scn8z474mzwp54h5zi39hq5y4rvpn4qargc441i10jnasnf8s30")))

(define-public crate-lobster-0.7.0 (c (n "lobster") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vxilw0apsnlgciahlddf6hnqgnza94x8iq26fkd5wkbhn72knkd")))

