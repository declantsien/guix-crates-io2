(define-module (crates-io lo ag loago) #:use-module (crates-io))

(define-public crate-loago-0.1.0 (c (n "loago") (v "0.1.0") (h "1ngqq9p5fm7zsfa4lsk3j8z9wy7kxkdpgiy8sl2769k7pksa456r")))

(define-public crate-loago-0.2.0 (c (n "loago") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1rs09d8ab0fifnwvrfmrmzdslpkknw7c5hvgrjl6aaa1v8izb758") (y #t)))

(define-public crate-loago-0.2.1 (c (n "loago") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0mnn6vf3n85gysldb0pq3naln0bzcryivj67w9zl675zny5qcb76")))

(define-public crate-loago-0.3.0 (c (n "loago") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("wrap_help" "derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "05myn5s2917gwgbxgx6lshfj4i5scilr6qy08al5jig9i4li65kn")))

