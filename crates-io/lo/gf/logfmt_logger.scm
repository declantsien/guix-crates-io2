(define-module (crates-io lo gf logfmt_logger) #:use-module (crates-io))

(define-public crate-logfmt_logger-0.1.0 (c (n "logfmt_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "0ai5kw68kgzdx8c3bxcbnyhgb9rsgln1gpa7fgi0r3h610k3z13i") (f (quote (("env" "env_logger") ("default" "env" "color") ("color" "termcolor"))))))

(define-public crate-logfmt_logger-0.1.1 (c (n "logfmt_logger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termcolor") (r "^1") (o #t) (d #t) (k 0)))) (h "031gdr14w2dggz65lij88w73hx8s72p8yd1k7315b3xg6l48mnk0") (f (quote (("env" "env_logger") ("default" "env" "color") ("color" "termcolor"))))))

