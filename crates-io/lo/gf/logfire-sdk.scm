(define-module (crates-io lo gf logfire-sdk) #:use-module (crates-io))

(define-public crate-logfire-sdk-0.0.0 (c (n "logfire-sdk") (v "0.0.0") (h "0hgyb93avh15y7qg1x05g58572rpzzw41dbh00g7ybvp02b8cw36")))

(define-public crate-logfire-sdk-0.0.1 (c (n "logfire-sdk") (v "0.0.1") (h "1fdmaamqnxhdbl4wqlz6srfmx9mh4fdb3fxkm1gyifdvw6iymcg0")))

