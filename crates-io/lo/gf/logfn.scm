(define-module (crates-io lo gf logfn) #:use-module (crates-io))

(define-public crate-logfn-0.1.0 (c (n "logfn") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x29j25lxsl592bzk7pbn5lwqiri9n43nc82mrnanjlvb0yfv145")))

