(define-module (crates-io lo gf logfather) #:use-module (crates-io))

(define-public crate-logfather-0.1.0 (c (n "logfather") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fs4") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0giyrsdfsd08l41hpgklmgrkz6g86rvlpb0yk7vzh9nbn1a9lvvi")))

(define-public crate-logfather-0.1.1 (c (n "logfather") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fs4") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03pv9knrrljb28pbnx7kcym89viq3ayj64jkcmcbhqs0hisaf830")))

(define-public crate-logfather-0.2.0 (c (n "logfather") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "fs4") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1rmvbkj0gv4c8qwzzp1jicqrirsiwg7qbvmcnq83pqkx8z65kiwp")))

(define-public crate-logfather-0.2.1 (c (n "logfather") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fs4") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xjm89z6c2qhmnd209k58ifm4d76sg0zida3w9r5qcjq4jj3jadq")))

(define-public crate-logfather-0.2.2 (c (n "logfather") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fs4") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xxllkcm52dh8xdc2xh8k3h762mrlaq2cl3670qixs9csc03r49j")))

(define-public crate-logfather-0.2.3 (c (n "logfather") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "simplicio") (r "^0.1.1") (d #t) (k 0)))) (h "0yccrgp67kzar5dmqz7q5ybd0f909hwjvisbjpj0hj6xzy69jzka")))

(define-public crate-logfather-0.2.4 (c (n "logfather") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dekor") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "simplicio") (r "^0.1.1") (d #t) (k 0)))) (h "0df9zrgcpj2jxp7dfq0g9y2xhpr7mklivkfyqib1zdybdz3x01ki")))

(define-public crate-logfather-0.2.5 (c (n "logfather") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dekor") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "simplicio") (r "^0.1.1") (d #t) (k 0)))) (h "0d6gayn64lm1vxym9aww3a83fxd1wwaminy1kk7wdwlbjznk0fvb")))

(define-public crate-logfather-0.2.6 (c (n "logfather") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dekor") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "simplicio") (r "^0.1.1") (d #t) (k 0)))) (h "10g6fqb8v87wssas2d46havkldghals73zf9dpwrqdmrab3q2jbq")))

