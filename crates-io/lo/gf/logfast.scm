(define-module (crates-io lo gf logfast) #:use-module (crates-io))

(define-public crate-logfast-0.1.0 (c (n "logfast") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "0qv16kcc7z3jqxlilgpy9krg36fypx2s6vkxixpnb382zfikmg9f")))

(define-public crate-logfast-0.1.1 (c (n "logfast") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "1c3k80d1kiyxpfyp6r3spadmw1hva49aphdlv1493gylj5m77ggw")))

(define-public crate-logfast-0.1.2 (c (n "logfast") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "15cjyznp6zmisqnhyccgr150r9gymayw69j335nlwnzxk02cs8w1")))

(define-public crate-logfast-1.1.4 (c (n "logfast") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "07632hnksliwx68p03sswafn5n3gj6fs4dimc59zd6adfy0x61f5")))

(define-public crate-logfast-1.1.5 (c (n "logfast") (v "1.1.5") (d (list (d (n "chrono") (r "=0.4.19") (d #t) (k 0)) (d (n "die") (r "=0.2.0") (d #t) (k 0)))) (h "1k69jxyrcxj10c9hs022riff8x0r4gqlk1583rb0ra3hldkb0xdr")))

(define-public crate-logfast-1.1.8 (c (n "logfast") (v "1.1.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)))) (h "02gvl84aj9n6m0dym1ws3kyx28qf7479gnycqq4rl1g73m2x8h3m")))

