(define-module (crates-io lo gf logflume) #:use-module (crates-io))

(define-public crate-logflume-0.0.1 (c (n "logflume") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "1d6d9q53f997c2hb39nnxfs0lsndlq2gj347h9fnzrvm4740dicw")))

(define-public crate-logflume-0.0.2 (c (n "logflume") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "020zb73dr9yf53wbn4ic82rzziwgjbqs2pz5wjrps4d5dd051i6f")))

(define-public crate-logflume-0.0.3 (c (n "logflume") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "1a4pagz0n58d4pbch1zywbnpvny82akjlmczbkg1b26nfcz2dqw4")))

(define-public crate-logflume-0.0.4 (c (n "logflume") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "16fdpiwhnk70laj261d5200s8yh83yr78hzqzmll2wfrym7xfdwk")))

(define-public crate-logflume-0.0.5 (c (n "logflume") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "17im3v4slk5qi6xb8x7k5z8ld3gljzgdffhbnjkp55z4rpq8sxvz")))

(define-public crate-logflume-0.0.6 (c (n "logflume") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 2)))) (h "073fknm0kw7pvllk8vfyj3ysplgidaz8wqchv2xyvnhy42pry24k")))

