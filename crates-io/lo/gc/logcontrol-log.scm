(define-module (crates-io lo gc logcontrol-log) #:use-module (crates-io))

(define-public crate-logcontrol-log-0.1.0 (c (n "logcontrol-log") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log-reload") (r "^0.1.0") (d #t) (k 0)) (d (n "logcontrol") (r "^1.0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "systemd-journal-logger") (r "^2.0.0") (d #t) (k 0)) (d (n "zbus") (r "^3.14.1") (d #t) (k 2)))) (h "0ykf6bkfn810h163g7b38mgwwv235lygc5vzww4434xj3lc2jx4c") (r "1.71")))

