(define-module (crates-io lo gc logchop) #:use-module (crates-io))

(define-public crate-logchop-0.1.0 (c (n "logchop") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0ybkzx90xnwm4ivpyns3vwjdl7x3gw5apr5b9lranhahicam2v38")))

(define-public crate-logchop-0.1.1 (c (n "logchop") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0wx2ag8w28lvhbqpmql70brfiy9radgisf7kj9vasxdqj31mc1zw")))

(define-public crate-logchop-0.1.2 (c (n "logchop") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0lh4vj73ya620a7w0maqy9aabibn8ch2jmmjk7xg5yrg7q98i6f1")))

