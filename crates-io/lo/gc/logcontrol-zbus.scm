(define-module (crates-io lo gc logcontrol-zbus) #:use-module (crates-io))

(define-public crate-logcontrol-zbus-1.0.0 (c (n "logcontrol-zbus") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "logcontrol") (r "^1.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "06laxb4pqg5jqbgavy2mkpqg2m4ndnnd1fccb0q87xsz6ijw4bgw") (r "1.71")))

(define-public crate-logcontrol-zbus-1.1.0 (c (n "logcontrol-zbus") (v "1.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "logcontrol") (r "^1.0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 2)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "03xrd0vhx5w3z52byc7qi4hbf16bczxr2qcg9lzvpmzk0yizri5d") (r "1.71")))

(define-public crate-logcontrol-zbus-2.0.0 (c (n "logcontrol-zbus") (v "2.0.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "logcontrol") (r "^1.0.1") (d #t) (k 0)) (d (n "zbus") (r "^4.0.0") (d #t) (k 0)))) (h "1f7fj3gyr0z8rpy6gqpgkk39znm5x96nbmwlbcpya43dbk4rcvsl") (r "1.71")))

