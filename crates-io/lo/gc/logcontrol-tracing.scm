(define-module (crates-io lo gc logcontrol-tracing) #:use-module (crates-io))

(define-public crate-logcontrol-tracing-0.1.0 (c (n "logcontrol-tracing") (v "0.1.0") (d (list (d (n "logcontrol") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)) (d (n "tracing-journald") (r "^0.3.0") (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("ansi" "json"))) (k 2)))) (h "0bv7wyp13kxkkzxgic08x70876qfs1wsvyic9a8rp9jhw147wpqr") (r "1.71")))

(define-public crate-logcontrol-tracing-0.2.0 (c (n "logcontrol-tracing") (v "0.2.0") (d (list (d (n "logcontrol") (r "^1.0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)) (d (n "tracing-journald") (r "^0.3.0") (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("ansi" "json"))) (k 2)))) (h "0p3ic9p2qjraj4yr54m2awwvnrzcfnqjj3jr9dzj2qnjy3vn2gav") (r "1.71")))

