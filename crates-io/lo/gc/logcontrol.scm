(define-module (crates-io lo gc logcontrol) #:use-module (crates-io))

(define-public crate-logcontrol-1.0.0 (c (n "logcontrol") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1bvizb85yw3y1lzalkck0l8l0qyvz4kz04pxm9d7hb6hq89834dv") (r "1.71")))

(define-public crate-logcontrol-1.0.1 (c (n "logcontrol") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xncb5djw9i3x1x5g5v8kv7mr6hasvcxyv03kn2vnsrkg752lsvv") (r "1.71")))

