(define-module (crates-io lo pb lopbox) #:use-module (crates-io))

(define-public crate-lopbox-0.1.1 (c (n "lopbox") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0ki7gs0pz35cw4301qrs73p6fgpr6lylq1kl3xxjmr2rsp61iza8")))

