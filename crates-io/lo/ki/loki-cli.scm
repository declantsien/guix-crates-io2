(define-module (crates-io lo ki loki-cli) #:use-module (crates-io))

(define-public crate-loki-cli-0.1.0 (c (n "loki-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s7lpkwi8zvwc4lqgwq2110jcxrjpj5p5r75s8g7kcabadffz79s")))

(define-public crate-loki-cli-0.1.1 (c (n "loki-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v63jllwl1rvychg8kddw0vcbdn0idipzv6vzafk60brdag4ar49")))

(define-public crate-loki-cli-0.1.2 (c (n "loki-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nks1wijsg6pabw561j37f7vmnf1361km4n41x1a10f8hazwccvy")))

(define-public crate-loki-cli-0.2.0 (c (n "loki-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dc47589sxxsdp37l13rwnlwvmmnh6ln1d2zb3hqfclyjyv7clgx")))

(define-public crate-loki-cli-0.3.0 (c (n "loki-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)))) (h "19cla5xq1vwjz8hwh7q5vf4n6gn3wisqpym4iv4iw051mj9piid5")))

(define-public crate-loki-cli-0.4.0 (c (n "loki-cli") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)))) (h "1s28fdqpb74c6wr1f1id70dl5dn59alg3w6bxkc6vyjxki3y8zpy")))

(define-public crate-loki-cli-0.5.0 (c (n "loki-cli") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)))) (h "0fm97zkfcbvj540mnif8rpybq17vh192j1fqc99q5c2a9bkpac9g")))

(define-public crate-loki-cli-0.6.0 (c (n "loki-cli") (v "0.6.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "time") (r "^0.3.17") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1gg69vvh04pm49px7b562db959dbyp3nhsm875p8194axk2k740r")))

(define-public crate-loki-cli-0.6.1 (c (n "loki-cli") (v "0.6.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)))) (h "1ffgs7kqr3602pr5s604pn89ic9yj4wja4z27wvgkwxly1ripz9w")))

(define-public crate-loki-cli-0.7.0 (c (n "loki-cli") (v "0.7.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "unicode" "env"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0qicq6024z342zlwaywq8g8c4yw75scp41ml5fn8scllhwjifpwl")))

(define-public crate-loki-cli-0.7.1 (c (n "loki-cli") (v "0.7.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "unicode" "env"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "1jmpkkd9s7qya1iz6awb4n5w0a8p6f4pgdm12j7lc2p8qan7g788")))

(define-public crate-loki-cli-0.7.2 (c (n "loki-cli") (v "0.7.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "unicode" "env"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "07jb0nhngv0zm161p00sgcfrjzmlhayy76hfhrfj2rlfy1zpkw3f")))

(define-public crate-loki-cli-0.7.3 (c (n "loki-cli") (v "0.7.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "unicode" "env"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "08qgldb9s98vgid1ilkj7840rvfx0api5w5g1ls26nhp5da65d98")))

(define-public crate-loki-cli-0.7.4 (c (n "loki-cli") (v "0.7.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "unicode" "env"))) (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0i0sdhrvpy2x24zf0365k2wriwjmx44m9l08xddcllcwzy6bgqg1")))

