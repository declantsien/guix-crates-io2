(define-module (crates-io lo ki loki-rs) #:use-module (crates-io))

(define-public crate-loki-rs-0.1.0 (c (n "loki-rs") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)))) (h "12jqcl45knk8dyv4ssqy949s7b0n3zkc2kx0r1c1i2c2xk2f439z")))

