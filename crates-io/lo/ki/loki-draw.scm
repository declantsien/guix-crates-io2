(define-module (crates-io lo ki loki-draw) #:use-module (crates-io))

(define-public crate-loki-draw-0.1.0 (c (n "loki-draw") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glutin") (r "^0.31.2") (d #t) (k 2)) (d (n "glutin-winit") (r "^0.4.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.3") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (f (quote ("rwh_05"))) (d #t) (k 2)))) (h "0d8x0z8ajf0ddkq1mlkydakprm0hxsw7plb280wz75wax5sjfm18")))

