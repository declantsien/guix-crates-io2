(define-module (crates-io lo ki loki-api) #:use-module (crates-io))

(define-public crate-loki-api-0.0.0 (c (n "loki-api") (v "0.0.0") (h "1ffb0mm5jqxkpz20ldpdcwl0pb7bmd38kp0z0pdmd74vkvpicq2g")))

(define-public crate-loki-api-0.1.0 (c (n "loki-api") (v "0.1.0") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)))) (h "18ic2zlnr36692p15pk95fj9bj01d6kc0vir8z5mmqknpjk1zcni")))

(define-public crate-loki-api-0.1.1 (c (n "loki-api") (v "0.1.1") (d (list (d (n "prost") (r ">=0.9.0, <0.12.0") (d #t) (k 0)) (d (n "prost-types") (r ">=0.9.0, <0.12.0") (d #t) (k 0)))) (h "1v8wi8dm6mxlarwh9rv56vcxbxzyjzjvlb5ncbsafss8fgskcvgm")))

