(define-module (crates-io lo os loose-liquid-lib) #:use-module (crates-io))

(define-public crate-loose-liquid-lib-0.27.0 (c (n "loose-liquid-lib") (v "0.27.0") (d (list (d (n "deunicode") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.27.0") (f (quote ("derive"))) (d #t) (k 0) (p "loose-liquid-core")) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "12z29dhfr8d9fjxl76ldk2ilnfv5932z6hi6l1ghb62fdj881icv") (f (quote (("stdlib") ("shopify") ("jekyll" "deunicode") ("extra") ("default" "stdlib") ("all" "stdlib" "jekyll" "shopify" "extra")))) (r "1.70.0")))

