(define-module (crates-io lo os loose-liquid-derive) #:use-module (crates-io))

(define-public crate-loose-liquid-derive-0.27.0 (c (n "loose-liquid-derive") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xkaab61a06m63lh61v28b7yywh4srkjhciqy3sl3vsd4j0z0br9") (r "1.70.0")))

