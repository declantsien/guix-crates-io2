(define-module (crates-io lo os loosen_map) #:use-module (crates-io))

(define-public crate-loosen_map-0.1.0 (c (n "loosen_map") (v "0.1.0") (d (list (d (n "loosen") (r "^0.1.0") (d #t) (k 2)))) (h "0i2m9kj6gjsc4zyshvnxv2wy88snbnyq3naa7iz8habnzgjdipkh")))

(define-public crate-loosen_map-0.1.1 (c (n "loosen_map") (v "0.1.1") (d (list (d (n "loosen") (r "^0.1.0") (d #t) (k 2)))) (h "0c9im0m4nc6wsfnlgmsv7mjmic03qq5znx7390gkdlglnh3vnn82")))

