(define-module (crates-io lo os loosen) #:use-module (crates-io))

(define-public crate-loosen-0.1.0 (c (n "loosen") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0wq20vlaagwvz6xkmrmkvc3dgyqafi3x29r3sijlgp5ccdnfza99")))

(define-public crate-loosen-0.1.1 (c (n "loosen") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1hxbwd5xvxc02b1zisqcznp3n40j89aizf0js7rphlkfm983iprp")))

(define-public crate-loosen-0.1.2 (c (n "loosen") (v "0.1.2") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1mb3dwjx2n2s0lxg0csh7mwljl92ry81607p4vhwbkss98m10z7z")))

