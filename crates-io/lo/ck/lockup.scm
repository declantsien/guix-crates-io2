(define-module (crates-io lo ck lockup) #:use-module (crates-io))

(define-public crate-lockup-1.0.5 (c (n "lockup") (v "1.0.5") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1zmf65ycp6wkf630nchj4mvs0scck5rh9b1fyhlvhw11m0c32yxf") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.0.6 (c (n "lockup") (v "1.0.6") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "19mccxxffxfv2bcr5bixxr1jkindxvjrm7b918ghnk8f68gzbf1p") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.0.7 (c (n "lockup") (v "1.0.7") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "07iyw9sn8xzlakv11qqv8ikrdywqcxnw0bw33v5davzq57px1350") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.0.10 (c (n "lockup") (v "1.0.10") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1bv5b08ldmdnqv4vi2d50h4n4s6b5vr517d73zs13778dwq3d2ra") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.0.11 (c (n "lockup") (v "1.0.11") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "15mdi4jxyb639ifp942sxld1jzspzxblwdj9q8gnnm5b8rww3lpc") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.1.0 (c (n "lockup") (v "1.1.0") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "0za8pi5g5yjblv8jl8si0rcj9ahig2zlbiv84fvikkjl3pr30k8w") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.1.1 (c (n "lockup") (v "1.1.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "0prql4ibp9z7rg8rj9ps6mflpq7a8c8h0j1iks4r3x4r95fmqcgq") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-lockup-1.1.2 (c (n "lockup") (v "1.1.2") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "mint-proxy") (r "^1.0") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^2.0.1") (d #t) (k 0)))) (h "1l3rrim5ybr9m4qnkh3r87bkxw7f83jlvfaj49dnh42i7pn4x3xq") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

