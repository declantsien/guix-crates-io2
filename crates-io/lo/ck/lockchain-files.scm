(define-module (crates-io lo ck lockchain-files) #:use-module (crates-io))

(define-public crate-lockchain-files-0.0.0 (c (n "lockchain-files") (v "0.0.0") (d (list (d (n "lockchain-core") (r ">= 0.3.2") (d #t) (k 0)))) (h "0cs1zzs8bcpvzixwc14sb7hc1kv3nip6rs41xa4qv08ml2z08r3w")))

(define-public crate-lockchain-files-0.1.0 (c (n "lockchain-files") (v "0.1.0") (d (list (d (n "lockchain-core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bfgvyf1fbh6rwvz0fdw62i2c3407j4zlgp9l3a9id6ajmjj6qw3")))

(define-public crate-lockchain-files-0.7.0 (c (n "lockchain-files") (v "0.7.0") (d (list (d (n "lockchain-core") (r "^0.7.0") (d #t) (k 0)))) (h "178ybpsi903rrinq64ra6rc6cig9cpsg69hcj6k840ka3mqfds9q")))

(define-public crate-lockchain-files-0.7.1 (c (n "lockchain-files") (v "0.7.1") (d (list (d (n "lockchain-core") (r "^0.7.2") (d #t) (k 0)))) (h "16qsnmngj76s2hg0kz3rwibbr3g6vdfp7w8s2gv0q9a00adcf04x")))

(define-public crate-lockchain-files-0.7.2 (c (n "lockchain-files") (v "0.7.2") (d (list (d (n "lockchain-core") (r "^0.8.2") (d #t) (k 0)))) (h "0jf7fi4galzh2dbzxj7mablqvwwsvj91lw49c4wwlkhjfb7p986z")))

(define-public crate-lockchain-files-0.9.0 (c (n "lockchain-files") (v "0.9.0") (d (list (d (n "lockchain-core") (r "^0.9.0") (d #t) (k 0)))) (h "0z76jppighv27n2skzpsq81vqsrvkkxdbway0bf0ixsy1p78lpp6")))

