(define-module (crates-io lo ck lock_ext) #:use-module (crates-io))

(define-public crate-lock_ext-0.1.0 (c (n "lock_ext") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "1vsmq73wabdgvw543hhlzin4hhwf2qha89rns37cfl3vslp6wf6z")))

(define-public crate-lock_ext-0.1.1 (c (n "lock_ext") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0wxrx9mvli7fi3n2wyc08w28n59a3fsk02qkn2b352nz8l0kjcdr")))

(define-public crate-lock_ext-0.1.2 (c (n "lock_ext") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0my45imcjd07f1n6ccxg7w4rd9y91xh9zimfx68h1bg50713r8yg")))

(define-public crate-lock_ext-0.1.3 (c (n "lock_ext") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "1rrz9gy74dhr3z3934pp3h9vmiim6z09mgyjhd46fr89d0qjsm04")))

