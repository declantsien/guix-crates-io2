(define-module (crates-io lo ck lockless) #:use-module (crates-io))

(define-public crate-lockless-0.1.0 (c (n "lockless") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)))) (h "04wphyz37dykf946m11mkzyb85407b9j5ac3h6q6mdzdb7asxx9c")))

(define-public crate-lockless-0.1.1 (c (n "lockless") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)))) (h "1p96mjb9dv2h0a3n86h9z5rd072svjp4vnsfnidx7ck6phzj0sbi")))

(define-public crate-lockless-0.1.2 (c (n "lockless") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)))) (h "13f34as15pm9kk4v54a2k3w1pd1xdpqflm1f0phbazbi6bdn6dqi")))

(define-public crate-lockless-0.2.0 (c (n "lockless") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)))) (h "1a99cywy2zjij63w1xqm5s9cz1ivdir5im0lzivrdrwampirvn2z")))

