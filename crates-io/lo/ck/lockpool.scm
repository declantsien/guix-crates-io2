(define-module (crates-io lo ck lockpool) #:use-module (crates-io))

(define-public crate-lockpool-0.1.0 (c (n "lockpool") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)))) (h "0rjfyidvfn9ax6hn0h8j0n99jr64n13bbgyjz138jdblyrm3bnmv")))

(define-public crate-lockpool-0.2.0 (c (n "lockpool") (v "0.2.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)))) (h "133jk0mxy8qjxm7lajbcv6p10c9hjsmd3320v23q3cma09c2afr8")))

(define-public crate-lockpool-0.3.0 (c (n "lockpool") (v "0.3.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "00xqpv35j2wbihl9j1why4q7nhq4kpw104kr8ac7mvg3smsvjajb")))

(define-public crate-lockpool-1.0.0 (c (n "lockpool") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1zsxj06c8bayha58z2my5ydi2qmcanbxbjvc5k3x7igwr9xkr0wz")))

(define-public crate-lockpool-2.0.0 (c (n "lockpool") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0ghaq11yrqqb11jgcrsskqas3ax697zz8i2rywj1wnmyavd3w6c4")))

(define-public crate-lockpool-2.0.1 (c (n "lockpool") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0vqx6j8ih20a1qbdpipg28y9dclachh6imi4hwb6hz7aczada448")))

(define-public crate-lockpool-3.0.0 (c (n "lockpool") (v "3.0.0") (d (list (d (n "async-trait") (r "^0.1.58") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.14") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "owning_ref_lockable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0vm9hafhhpf8val2bzfk6vspps8jynmhvl8f6v4xh27wkvp1x07q") (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait")))) (r "1.65.0")))

(define-public crate-lockpool-3.0.1 (c (n "lockpool") (v "3.0.1") (d (list (d (n "async-trait") (r "^0.1.58") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.14") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "owning_ref_lockable") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0j23x8lr754pjq84z8v8k95b0m3rm545kywbq8yzjv9n2qxqrw1b") (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait")))) (r "1.65.0")))

