(define-module (crates-io lo ck lock_keys) #:use-module (crates-io))

(define-public crate-lock_keys-1.0.0 (c (n "lock_keys") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "186sm1cxkjgr86g35cfqvslfhydvppxnl0cf7n9jkgbs8jzhva5n")))

(define-public crate-lock_keys-1.1.0 (c (n "lock_keys") (v "1.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "io-kit-sys") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "mach") (r "^0.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xz0l0p41f791d28hyx2r76xw7vw5r8ckwsmdh8j7mq88b6rz6jz")))

