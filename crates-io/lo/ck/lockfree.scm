(define-module (crates-io lo ck lockfree) #:use-module (crates-io))

(define-public crate-lockfree-0.1.0 (c (n "lockfree") (v "0.1.0") (h "1s3m9znqr1g2vpfm2zbl7ky0wxi5xmfr2jf899wsjy81sndv01hp")))

(define-public crate-lockfree-0.1.1 (c (n "lockfree") (v "0.1.1") (h "087wbi20ancz7hxdg4q2ing3m4y3m9c387prmzn4v5xsghmbhxzv")))

(define-public crate-lockfree-0.1.2 (c (n "lockfree") (v "0.1.2") (h "1ipbf4lmg28yh6mal58dv3gczpdvp57yhli470fr81pj1mi042pn")))

(define-public crate-lockfree-0.2.0 (c (n "lockfree") (v "0.2.0") (h "0wg5qg6vcswzzn0b8l2hplmvvm2a4hgk4d0yxxi3xwvwbpn5kbh1")))

(define-public crate-lockfree-0.3.0 (c (n "lockfree") (v "0.3.0") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "12zmribkqzfjqfmggbbmd6r8jnsaf2a1fwgm3x970vddm4fkik1h")))

(define-public crate-lockfree-0.3.1 (c (n "lockfree") (v "0.3.1") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "0z1x812mv96jgj0zmygyqmyvw75c5v5wyrfic6mg0jd9smp4nkp0")))

(define-public crate-lockfree-0.3.2 (c (n "lockfree") (v "0.3.2") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "1wbs4rbn6c74ii4adibmlfpjp9dbcn422fklmbj9fb9pd7cmfv34")))

(define-public crate-lockfree-0.4.0 (c (n "lockfree") (v "0.4.0") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "0yg4d138qvip4cnjpwnrlm04s78fk0mwk7163d1p4l54wd7xx6ja")))

(define-public crate-lockfree-0.4.1 (c (n "lockfree") (v "0.4.1") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "03nlgzk03qvpxjx34l4hd0ki35nw60mrhs3wlbpblgmd1w6arj99")))

(define-public crate-lockfree-0.5.0 (c (n "lockfree") (v "0.5.0") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "19h6fb22rml4r0sfj3vkrijv36w1gazay3yvcfj1sxylyrljp4r1")))

(define-public crate-lockfree-0.5.1 (c (n "lockfree") (v "0.5.1") (d (list (d (n "owned-alloc") (r "^0.2") (d #t) (k 0)))) (h "08yx0aq2qg37s60ki680w57ywlh97mw0y12sijwpqg0imnsr9vkl")))

