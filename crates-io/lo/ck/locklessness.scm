(define-module (crates-io lo ck locklessness) #:use-module (crates-io))

(define-public crate-locklessness-0.2.1 (c (n "locklessness") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0p02kwb7cpng5v7q3kck1cwn7gqy3mlfgbjl75wmgzh3nxjpz0rs")))

