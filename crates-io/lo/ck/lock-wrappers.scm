(define-module (crates-io lo ck lock-wrappers) #:use-module (crates-io))

(define-public crate-lock-wrappers-0.1.0 (c (n "lock-wrappers") (v "0.1.0") (h "14zqcp4hx298gbyh2yc3nhzz1jv78m8mp1s1ix8xyzl32bdn9mp8")))

(define-public crate-lock-wrappers-0.1.1 (c (n "lock-wrappers") (v "0.1.1") (h "1yw11axg1cqyg99i35xgqpzcxl032alc54abwqv3667s22g6kkaw")))

(define-public crate-lock-wrappers-0.1.2 (c (n "lock-wrappers") (v "0.1.2") (h "0ym0n3n9b5m0cfzf44dj6sq6z7bda3s97kzx7a3nq9hfm524yrcd")))

