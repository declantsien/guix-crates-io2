(define-module (crates-io lo ck locktree-derive) #:use-module (crates-io))

(define-public crate-locktree-derive-0.1.0 (c (n "locktree-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.32") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0acp31rzry5ayj3bgh3kiwlmg356dw246585xvd865j05gqj4mzx")))

(define-public crate-locktree-derive-0.2.0 (c (n "locktree-derive") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "084zhqsqzynh0gdjcvmna9y70v5h5a93v9hxkb02v6i46ynx1335")))

(define-public crate-locktree-derive-0.3.0 (c (n "locktree-derive") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nx3g41wfijdvk6zwanj7ydrm7vaks1z5cyrr1w1wjs8fg9rz1c4")))

