(define-module (crates-io lo ck lock_api) #:use-module (crates-io))

(define-public crate-lock_api-0.1.0 (c (n "lock_api") (v "0.1.0") (d (list (d (n "owning_ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "10ayp5cm91c7ya75vja9c5qg84lfhvyyr4hxiqw2qhlpw9kyw159") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.1.1 (c (n "lock_api") (v "0.1.1") (d (list (d (n "owning_ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "04vz4ca2yx15n6wklmddjl8kbm6ma3hijdamd44cn21c8my77105") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.1.2 (c (n "lock_api") (v "0.1.2") (d (list (d (n "owning_ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "0axslmbmnhxdsj1kqh4yy0kqsrkvlxirqcs1pm1yngx7hjwx089j") (f (quote (("nightly")))) (y #t)))

(define-public crate-lock_api-0.1.3 (c (n "lock_api") (v "0.1.3") (d (list (d (n "owning_ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "0m3c0dh22k7wgg89vss6ksvnh5yjg0knjmrxghx1p37irjjjd64l") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.1.4 (c (n "lock_api") (v "0.1.4") (d (list (d (n "owning_ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "0ypvgk90szsp0q2llg92wj8kghbabldhml4dngwlvplvwsim2mvp") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.1.5 (c (n "lock_api") (v "0.1.5") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3") (k 0)))) (h "0b24q9mh258xa52ap636q1sxz0j5vrnp0hwbbh7ddjka3wwz3sv2") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.2.0 (c (n "lock_api") (v "0.2.0") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "1zx7pksmgyggpczgw4qrr4vj2nkdk5lipgiysvr20slm552nv57d") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.3.0 (c (n "lock_api") (v "0.3.0") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "0bppa6rdkqly08rsf6qr7d5yfjs4bzymyfipvlvndb70g8f00kzj") (f (quote (("nightly")))) (y #t)))

(define-public crate-lock_api-0.3.1 (c (n "lock_api") (v "0.3.1") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "1p04271jikw69ja0ap0plrfwm9incf1iny48g0b3ma9k4mw2x4gq") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.3.2 (c (n "lock_api") (v "0.3.2") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "11nmqxlzjqr1h6zddsqwvf1w6z4mwb1gd5qjxkdn0asxfabkjyz5") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.3.3 (c (n "lock_api") (v "0.3.3") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "0yzlz7f5xl5sm129dq8jqsrcrkyv7jjnqwd4zr4ijsdlxjaxxckr") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.3.4 (c (n "lock_api") (v "0.3.4") (d (list (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "0xgc5dzmajh0akbh5d6d7rj9mh5rzpk74pyrc946v2ixgakj9nn4") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.0 (c (n "lock_api") (v "0.4.0") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (k 0)))) (h "0knsma0ndwdbi26z62wwp23aj1pv3kig5flgfc9xp0klzvhjqc6y") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.1 (c (n "lock_api") (v "0.4.1") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (k 0)))) (h "0716z2rs0kydmd1818kqp4641dfkqzr0rpbnrpxhabxylp2pq918") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.2 (c (n "lock_api") (v "0.4.2") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (k 0)))) (h "04hkhfq308agxg9wwmzh7ncfiyyyhn0d49n07abppzdj6p8zz5nx") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.3 (c (n "lock_api") (v "0.4.3") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (k 0)))) (h "0xii6ch47akyky2spb4h53wxm1xvigcpm6di9dwpfrxf9v192g2s") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.4 (c (n "lock_api") (v "0.4.4") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.114") (o #t) (k 0)))) (h "1sq7pw7h7jbfvnv5nq3vm912gdwhhv8idi3njifd3xnz0q38i0h3") (f (quote (("nightly"))))))

(define-public crate-lock_api-0.4.5 (c (n "lock_api") (v "0.4.5") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "028izfyraynijd9h9x5miv1vmg6sjnw1v95wgm7f4xlr7h4lsaki") (f (quote (("nightly") ("arc_lock"))))))

(define-public crate-lock_api-0.4.6 (c (n "lock_api") (v "0.4.6") (d (list (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "0frbbqqiwngg33xrc69xagi4rqqk62msllr7z95mlbjaxzbkv548") (f (quote (("nightly") ("arc_lock"))))))

(define-public crate-lock_api-0.4.7 (c (n "lock_api") (v "0.4.7") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "0lwckl9l51y69bwf854kmdmmr1543spbxaa9xjclc3lllsvaazrj") (f (quote (("nightly") ("arc_lock"))))))

(define-public crate-lock_api-0.4.8 (c (n "lock_api") (v "0.4.8") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "1463iqx8adn26vz5lkn5qh8kpgzj32vwzl8hhbycn9dgmidbz04z") (f (quote (("nightly") ("arc_lock"))))))

(define-public crate-lock_api-0.4.9 (c (n "lock_api") (v "0.4.9") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "1py41vk243hwk345nhkn5nw0bd4m03gzjmprdjqq6rg5dwv12l23") (f (quote (("nightly") ("arc_lock"))))))

(define-public crate-lock_api-0.4.10 (c (n "lock_api") (v "0.4.10") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "05nd9nzxqidg24d1k8y5vlc8lz9gscpskrikycib46qbl8brgk61") (f (quote (("nightly") ("default" "atomic_usize") ("atomic_usize") ("arc_lock"))))))

(define-public crate-lock_api-0.4.11 (c (n "lock_api") (v "0.4.11") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "0iggx0h4jx63xm35861106af3jkxq06fpqhpkhgw0axi2n38y5iw") (f (quote (("nightly") ("default" "atomic_usize") ("atomic_usize") ("arc_lock")))) (r "1.49.0")))

(define-public crate-lock_api-0.4.12 (c (n "lock_api") (v "0.4.12") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "owning_ref") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)))) (h "05qvxa6g27yyva25a5ghsg85apdxkvr77yhkyhapj6r8vnf8pbq7") (f (quote (("nightly") ("default" "atomic_usize") ("atomic_usize") ("arc_lock")))) (r "1.56.0")))

