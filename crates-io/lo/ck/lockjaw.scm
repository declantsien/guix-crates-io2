(define-module (crates-io lo ck lockjaw) #:use-module (crates-io))

(define-public crate-lockjaw-0.0.1 (c (n "lockjaw") (v "0.0.1") (d (list (d (n "lockjaw_processor") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0khig6ak7d8jksyr56k7ghr1qxwdajf8jdd0ad7867pxlhk1yddh")))

(define-public crate-lockjaw-0.0.3 (c (n "lockjaw") (v "0.0.3") (d (list (d (n "lockjaw_processor") (r "^0.0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k3plhhsay97iv8a67fi6l6bh8rd7436n5v34l0kxpxwmxrwwnwc")))

(define-public crate-lockjaw-0.0.4 (c (n "lockjaw") (v "0.0.4") (d (list (d (n "lockjaw_processor") (r "^0.0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11g93k2hl7vxxpgsyap0flz4sy7rr1h4yn6l5744n2y6gh8wbrw3")))

(define-public crate-lockjaw-0.1.0 (c (n "lockjaw") (v "0.1.0") (d (list (d (n "lockjaw_processor") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "1xc6h4p09nxbmqa3svkvr3v2d8wljqd11cqqb1ag3lgvhnq3v8fv")))

(define-public crate-lockjaw-0.2.0 (c (n "lockjaw") (v "0.2.0") (d (list (d (n "lockjaw_processor") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "0w7ncfmznrsn3m4ygnbq589fvxzljd1am2ymbbgk7aqbhpvn7rbz")))

(define-public crate-lockjaw-0.2.1 (c (n "lockjaw") (v "0.2.1") (d (list (d (n "lockjaw_processor") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "06lh8gr8s8ipfsds7480h88agjijlggs395m0091w02v46iwlg2h")))

(define-public crate-lockjaw-0.2.2 (c (n "lockjaw") (v "0.2.2") (d (list (d (n "lockjaw_processor") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "1s9nip5zdi1c5rgvbkzmcwpkzm33q0yg3b1ym3svwfinm8p25dfv")))

