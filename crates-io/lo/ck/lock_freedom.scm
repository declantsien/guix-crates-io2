(define-module (crates-io lo ck lock_freedom) #:use-module (crates-io))

(define-public crate-lock_freedom-0.1.0 (c (n "lock_freedom") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "owned-alloc") (r "^0.2.0") (d #t) (k 0) (p "tux-owned-alloc")))) (h "0lixaq3sya04japvd7q98wkabxdbbqkrklck86n8hpzb3yfn411b") (f (quote (("std") ("default" "std")))) (r "1.60.0")))

