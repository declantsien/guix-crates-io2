(define-module (crates-io lo ck lockchain-http) #:use-module (crates-io))

(define-public crate-lockchain-http-0.0.0 (c (n "lockchain-http") (v "0.0.0") (d (list (d (n "gotham") (r "^0.2") (d #t) (k 0)) (d (n "gotham_derive") (r "^0.2") (d #t) (k 0)) (d (n "gotham_serde_json_body_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "lockchain-core") (r "<= 0.3.2") (d #t) (k 0)))) (h "00pkinq6amik7rxg99w5pldzzyqli4h4v1claaglxaawb33rzvyg")))

(define-public crate-lockchain-http-0.1.0 (c (n "lockchain-http") (v "0.1.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hwcca9j7x53azkyl9an2mzcnqic9r9aybyv1sa7khf8mka7pkbh")))

(define-public crate-lockchain-http-0.1.1 (c (n "lockchain-http") (v "0.1.1") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16zzhlm515d3gp22yn9bjrb0n44sb585a517vnpzc6a03pzmw02p")))

(define-public crate-lockchain-http-0.2.0 (c (n "lockchain-http") (v "0.2.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sibdb3awah42li7y3ybp1izzxf8q4g8ryz1pwnxfhs6idk1mjlb")))

(define-public crate-lockchain-http-0.2.1 (c (n "lockchain-http") (v "0.2.1") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1m9h4ky08hgvm49krc0hlgwsizz4bslk2r3xxw87ywpapz7b5naf")))

(define-public crate-lockchain-http-0.3.0 (c (n "lockchain-http") (v "0.3.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p8j2sz4w1i2hpkqymypw01m54a61jfyc7g9xmach5pa0i9i2lcx")))

(define-public crate-lockchain-http-0.4.0 (c (n "lockchain-http") (v "0.4.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lockchain-core") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gz8798fn5ggxdlgyzk9a06d81sj0pclrzrh5y4v0n662anrlnzr")))

