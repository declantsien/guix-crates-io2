(define-module (crates-io lo ck lock-free-static) #:use-module (crates-io))

(define-public crate-lock-free-static-0.1.0-rc.0 (c (n "lock-free-static") (v "0.1.0-rc.0") (h "0jsmf3pjfaq0ki2a1jb7b3dvy53r2qmjh1rm3fvnbpm8n24svd00")))

(define-public crate-lock-free-static-0.1.0-rc.1 (c (n "lock-free-static") (v "0.1.0-rc.1") (h "1s1ijppcjlz0g3sha0kx65wgycgmm0axyg83myjz8mn10xp0mrbz")))

(define-public crate-lock-free-static-0.1.0 (c (n "lock-free-static") (v "0.1.0") (h "0nakkrxcdm6ifrliazlvas4a31dm8xv9yi8y3w3na24q1kj7khbn")))

(define-public crate-lock-free-static-0.2.0 (c (n "lock-free-static") (v "0.2.0") (h "1ys3xnkci0msd9ql7457kc03l88qm25afna7qizayli6ph10d2z0")))

