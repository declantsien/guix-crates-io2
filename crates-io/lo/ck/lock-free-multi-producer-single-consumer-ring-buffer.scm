(define-module (crates-io lo ck lock-free-multi-producer-single-consumer-ring-buffer) #:use-module (crates-io))

(define-public crate-lock-free-multi-producer-single-consumer-ring-buffer-0.1.0 (c (n "lock-free-multi-producer-single-consumer-ring-buffer") (v "0.1.0") (h "0w4rajh8g691cp86v0hggqawyan1wc44162s5sklzznpgqj5029k")))

(define-public crate-lock-free-multi-producer-single-consumer-ring-buffer-0.2.0 (c (n "lock-free-multi-producer-single-consumer-ring-buffer") (v "0.2.0") (h "0l5nla7xsf44mbl18c145g991qq8f9qp4s5n7n9xc3k6g2zl686x")))

(define-public crate-lock-free-multi-producer-single-consumer-ring-buffer-0.3.0 (c (n "lock-free-multi-producer-single-consumer-ring-buffer") (v "0.3.0") (d (list (d (n "likely") (r "^0.1.0") (d #t) (k 0)))) (h "0aqgdraa0b3h910b15cwjc7fiix3hd7vgw4vx5kfjci6aprlf7an")))

(define-public crate-lock-free-multi-producer-single-consumer-ring-buffer-0.4.0 (c (n "lock-free-multi-producer-single-consumer-ring-buffer") (v "0.4.0") (d (list (d (n "likely") (r "^0.1.0") (d #t) (k 0)))) (h "0fprrlk311a1p51nd8vdifg3hsly01rhqp59zdc2znb053ckyhhq")))

