(define-module (crates-io lo ck lockfreehashmap) #:use-module (crates-io))

(define-public crate-lockfreehashmap-0.1.0 (c (n "lockfreehashmap") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0sk7kh2xmkg652k2gqjml6i5lnv4wmfahy2b9v6hqqfhmahip78r")))

(define-public crate-lockfreehashmap-0.1.1 (c (n "lockfreehashmap") (v "0.1.1") (d (list (d (n "crossbeam-epoch") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0hfyqhbw6y8s0435fk60j2wx27v526raw9wa528a80268v3d5083")))

(define-public crate-lockfreehashmap-0.1.2 (c (n "lockfreehashmap") (v "0.1.2") (d (list (d (n "crossbeam-epoch") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "18wng4g0d71ikxs8qak81cks3mbqw9m56w4vkxi1hdmyvq3r3n2s")))

