(define-module (crates-io lo ck locker) #:use-module (crates-io))

(define-public crate-locker-0.1.0 (c (n "locker") (v "0.1.0") (h "0yhynqh2ljflcb8mn0widib94ynl733kvi5mfw1c41yrz3vy3xid")))

(define-public crate-locker-0.2.0 (c (n "locker") (v "0.2.0") (h "0rbkbzdp2r63kfhf9qqya31xjjcszwbn21i2wxg0lr6pi5pyigs7")))

(define-public crate-locker-0.3.0 (c (n "locker") (v "0.3.0") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "00xfzr3jm0id8swaxpzkwy0b4jcfzmq72xw7kzbm0615hh5j5db4")))

(define-public crate-locker-0.3.1 (c (n "locker") (v "0.3.1") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1jfsdnby1ag17mnxyh75a3q3781nvm8lln9frk587b8z7n5jfnpq")))

(define-public crate-locker-0.4.0 (c (n "locker") (v "0.4.0") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1nn0n41kq8w41a06113qq204la6l475xgpxnynrbcidn2yq5w5nx") (f (quote (("sync") ("default" "async") ("async" "tokio"))))))

(define-public crate-locker-0.4.1 (c (n "locker") (v "0.4.1") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "179chnp9gg20g6325j1g0cpmhlqr5bx3mf2pj8sc18avagglkd68") (f (quote (("sync") ("default" "async") ("async" "tokio"))))))

(define-public crate-locker-0.4.2 (c (n "locker") (v "0.4.2") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "11na58gwqkf34i4j2602vq2mv3q7viz69krvj685gscwr9kbpls2") (f (quote (("sync") ("default" "async") ("async" "tokio"))))))

(define-public crate-locker-0.4.3 (c (n "locker") (v "0.4.3") (d (list (d (n "tokio") (r "^0.2.13") (f (quote ("sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1j4dr5hb57q1xx97v621amjihvzsy31r8bvl22ijf4nyr9b1mw9z") (f (quote (("sync") ("default" "async") ("async" "tokio"))))))

