(define-module (crates-io lo ck lockspot) #:use-module (crates-io))

(define-public crate-lockspot-0.0.1 (c (n "lockspot") (v "0.0.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "059lwfqbrzvvssgxhx85xi12rq2wib4yx7m6yyc91v9lsjahvycy")))

(define-public crate-lockspot-0.0.2 (c (n "lockspot") (v "0.0.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bhx54ngckyv68p2p5r4qdksr4d73zjyxg4b4z17ygkqxzywh3dr")))

(define-public crate-lockspot-0.0.3 (c (n "lockspot") (v "0.0.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nwyvx42xlr140zz02sjprf51r706bcj9k4jqz5kw40b5m654xpy")))

(define-public crate-lockspot-0.0.4 (c (n "lockspot") (v "0.0.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jxmc6jzlhzs9rm3rg7ry0qdmah4390fxlwl69panrdfbnh202x7")))

(define-public crate-lockspot-0.0.5 (c (n "lockspot") (v "0.0.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01r4qr4jci1cg6qr92lw7njyc1kpg8mn3g41n1b3bkvrg4smb1fr")))

