(define-module (crates-io lo ck lockfile) #:use-module (crates-io))

(define-public crate-lockfile-0.1.0 (c (n "lockfile") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19xzaf4q8xrhyyrifq7mkzwhngflfaqrz8iyn8rv789r1ihksbdv")))

(define-public crate-lockfile-0.2.0 (c (n "lockfile") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0rm8645gcsniqaw0vj0jrlcgb37apxwiwijvxxhqk486cgw79kd4")))

(define-public crate-lockfile-0.2.1 (c (n "lockfile") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0l92ap698yrss1qva9j1phcpmr3wdv5fg78j1k853k7a19hh8bpm")))

(define-public crate-lockfile-0.2.2 (c (n "lockfile") (v "0.2.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03z4m8n4py880d845dsy1fpz08j7g741lrlhqg0zwxjwfg2036wy")))

(define-public crate-lockfile-0.3.0 (c (n "lockfile") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1p2mgcqrls0a5i07hss8cbsbd0gbs2fvrifil10c2sjvgx1czpha")))

(define-public crate-lockfile-0.4.0 (c (n "lockfile") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0c3nqd211n6y73zn0zmd8bz46bmf4r3648srwjilpiqr0ccwzqav")))

