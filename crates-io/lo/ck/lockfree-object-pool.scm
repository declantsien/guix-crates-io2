(define-module (crates-io lo ck lockfree-object-pool) #:use-module (crates-io))

(define-public crate-lockfree-object-pool-0.1.0 (c (n "lockfree-object-pool") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)))) (h "0gri6n0ikgmbjrwz071si6if4kwlr4ygkywsrldhn363q9gpvgxn")))

(define-public crate-lockfree-object-pool-0.1.1 (c (n "lockfree-object-pool") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)))) (h "0r9jx689ml8604x5f6gjc8af8bz0cyajx70czc1r4585i4ji8y25")))

(define-public crate-lockfree-object-pool-0.1.2 (c (n "lockfree-object-pool") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "object-pool") (r "^0.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "1fqfasz6dif5pkkxp274is1ydaacr6ysm4ri8a3j5rka7qgiq5j6")))

(define-public crate-lockfree-object-pool-0.1.3 (c (n "lockfree-object-pool") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "object-pool") (r "^0.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "12gvlisi2c0wq1zrfqc1lqa2vhnljznjad6gi7mcn6hlxzfznrl1")))

(define-public crate-lockfree-object-pool-0.1.4 (c (n "lockfree-object-pool") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.5") (d #t) (k 2)) (d (n "object-pool") (r "^0.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "0mw5p0y3d4xwyl22jymsr3ddy9k2v96xxz7wds53f1mi4zxxwczf")))

(define-public crate-lockfree-object-pool-0.1.5 (c (n "lockfree-object-pool") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.5") (d #t) (k 2)) (d (n "object-pool") (r "^0.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "1g7ydn1a5dlab2vgk52slwp3fcyx3aj7vplmayslqhn23x4c0s9s")))

(define-public crate-lockfree-object-pool-0.1.6 (c (n "lockfree-object-pool") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.5") (d #t) (k 2)) (d (n "object-pool") (r "^0.5") (d #t) (k 2)) (d (n "sharded-slab") (r "^0.1") (d #t) (k 2)))) (h "0bjm2g1g1avab86r02jb65iyd7hdi35khn1y81z4nba0511fyx4k")))

