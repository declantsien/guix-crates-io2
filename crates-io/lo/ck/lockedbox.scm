(define-module (crates-io lo ck lockedbox) #:use-module (crates-io))

(define-public crate-lockedbox-0.1.0 (c (n "lockedbox") (v "0.1.0") (d (list (d (n "memsec") (r "^0.6.3") (d #t) (k 0)))) (h "1mmpdj5501z0wrj54rx8v07l5gr1qzy184a34421mng82i2n73qq") (r "1.56.1")))

(define-public crate-lockedbox-0.1.1 (c (n "lockedbox") (v "0.1.1") (d (list (d (n "memsec") (r "^0.6.3") (d #t) (k 0)))) (h "1b3i0prfxlrbx4599c9mg02p3cbyfrq7j4s6fmls06qqapd9rixx") (r "1.56.1")))

