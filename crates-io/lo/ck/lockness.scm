(define-module (crates-io lo ck lockness) #:use-module (crates-io))

(define-public crate-lockness-0.0.0 (c (n "lockness") (v "0.0.0") (h "0zivaqpjhza82cpbzidx3nmvx8z89iklaznx9glwyibx9dm5cslj")))

(define-public crate-lockness-0.0.1 (c (n "lockness") (v "0.0.1") (h "0ap3hc8aci2jsvq3c8f0i9rciaxk5jqskcg0fwqdy2awplfdi8hi") (r "1.66")))

(define-public crate-lockness-0.0.2 (c (n "lockness") (v "0.0.2") (h "026jrp3702a5ihwjwp8a3jwwk3pm3ywglrd24pwlww3nrqpp3hgs") (r "1.66")))

