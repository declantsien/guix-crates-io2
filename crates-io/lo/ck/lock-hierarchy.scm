(define-module (crates-io lo ck lock-hierarchy) #:use-module (crates-io))

(define-public crate-lock-hierarchy-0.1.1 (c (n "lock-hierarchy") (v "0.1.1") (h "0x7x57qfq792vxjq3bz85hf0fvbfnrvgbni1myhpccnksy0zgd0h")))

(define-public crate-lock-hierarchy-0.1.2 (c (n "lock-hierarchy") (v "0.1.2") (h "01acmgb9c6pnfzxhk4lv2sdfkaxaq8lxa3z05xif681pmx4zxz7k")))

