(define-module (crates-io lo ck lock_many) #:use-module (crates-io))

(define-public crate-lock_many-0.1.0 (c (n "lock_many") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06d3g1dfqldic69i8brxkh3354zjmi96aa67wpyywvjq9q82rv08") (f (quote (("default"))))))

(define-public crate-lock_many-0.1.1 (c (n "lock_many") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11r17b29gvh5c88i08rr72rijprhjpgm49dr30j6w43hwmmhdrxj") (f (quote (("default"))))))

