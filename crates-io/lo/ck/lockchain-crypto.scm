(define-module (crates-io lo ck lockchain-crypto) #:use-module (crates-io))

(define-public crate-lockchain-crypto-0.1.0 (c (n "lockchain-crypto") (v "0.1.0") (d (list (d (n "base64") (r "^0.8") (d #t) (k 0)) (d (n "blake2") (r "^0.7") (d #t) (k 0)) (d (n "lockchain-core") (r ">= 0.3.2") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08bh7k1wwya4kpnzc4qdcbibwyd2mm9x12xqmsp00qlzqkglw0w0")))

(define-public crate-lockchain-crypto-0.2.0 (c (n "lockchain-crypto") (v "0.2.0") (d (list (d (n "lockchain-core") (r ">= 0.5.0-alpha") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1masg1ffpqpyk4vg1p78g9ds3yq1178qw7vc24f4mfl50pv61w41")))

(define-public crate-lockchain-crypto-0.3.0 (c (n "lockchain-crypto") (v "0.3.0") (d (list (d (n "lockchain-core") (r "^0.5.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ga722w83rn1g3n9hwafh6wb4pxc16l348pqk82kbna9z4dffrib")))

(define-public crate-lockchain-crypto-0.7.0 (c (n "lockchain-crypto") (v "0.7.0") (d (list (d (n "lockchain-core") (r "^0.7.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qdx1mqm83l261bn36i62mfac8x3z5a4jxy0d642ljmv488khqdb")))

(define-public crate-lockchain-crypto-0.7.1 (c (n "lockchain-crypto") (v "0.7.1") (d (list (d (n "lockchain-core") (r "^0.8.2") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "185r2jmqcvwa938rk56pq4j2qjxbrlb5z89lhbbgbl03dg3rzw82")))

(define-public crate-lockchain-crypto-0.8.0 (c (n "lockchain-crypto") (v "0.8.0") (d (list (d (n "lockchain-core") (r "^0.9.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hyfgvjxgj6gm16kxsl762bkz8zrjhq0rmmhwq37dgnqn7jj5s8i")))

