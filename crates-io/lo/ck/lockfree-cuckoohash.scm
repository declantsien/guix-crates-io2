(define-module (crates-io lo ck lockfree-cuckoohash) #:use-module (crates-io))

(define-public crate-lockfree-cuckoohash-0.1.0 (c (n "lockfree-cuckoohash") (v "0.1.0") (d (list (d (n "clippy-utilities") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1lclyh0fwj7w8ndshr99lq9h4g9kg5256djyz6fbfb80mvf4w54d")))

