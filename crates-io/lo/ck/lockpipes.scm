(define-module (crates-io lo ck lockpipes) #:use-module (crates-io))

(define-public crate-lockpipes-1.0.0 (c (n "lockpipes") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1acghs30cl8dzhwy8y8q7c7xq09ncvf0k5fhhbwr57m26kjns04j")))

