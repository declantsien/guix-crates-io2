(define-module (crates-io lo ck locked-resource) #:use-module (crates-io))

(define-public crate-locked-resource-0.1.0 (c (n "locked-resource") (v "0.1.0") (h "0fk8awazczr7a3i25li3rmqg390d6kw67zb945vxcqcw3s89kacr")))

(define-public crate-locked-resource-0.1.1 (c (n "locked-resource") (v "0.1.1") (h "09w6ixj4vsj18nyg09bcjkikiqh9h0aj33k8vpv352zl9l6gyqfx")))

