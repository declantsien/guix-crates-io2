(define-module (crates-io lo ck lock_pool) #:use-module (crates-io))

(define-public crate-lock_pool-0.1.0 (c (n "lock_pool") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("futures" "checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 2)) (d (n "ref_count") (r "^0.1.2") (d #t) (k 0)))) (h "1k4jbqvz23zzw0vmsz1zc82cx7n31c05y5237ny6p4qndmxprhi9") (f (quote (("std" "ref_count/std") ("alloc"))))))

(define-public crate-lock_pool-0.2.0 (c (n "lock_pool") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "loom") (r "^0.7.1") (f (quote ("futures" "checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 2)) (d (n "ref_count") (r "^0.1.2") (d #t) (k 0)))) (h "0g9cmqqdic9bchxgiw4ff4s6vhcs8v24hnzs46ad49hgbxhana5g") (f (quote (("std" "ref_count/std") ("alloc"))))))

