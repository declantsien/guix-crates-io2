(define-module (crates-io lo ck lockfree-ringbuffer) #:use-module (crates-io))

(define-public crate-lockfree-ringbuffer-0.1.1 (c (n "lockfree-ringbuffer") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0jsbdj8gfy1v8diw0xddxahb81s2nxxxgxnsdj5m369lhn9shv6g")))

(define-public crate-lockfree-ringbuffer-0.1.2 (c (n "lockfree-ringbuffer") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10n5622c8dfamfmlzziccyf0lp14ylgcsc0mwd12psxpjdr2k1fd")))

(define-public crate-lockfree-ringbuffer-0.1.3 (c (n "lockfree-ringbuffer") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "01r15wi0yhy7522x0hkz946nggjh67wfdiv68ynl5rl60y5wa51l")))

