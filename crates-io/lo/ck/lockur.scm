(define-module (crates-io lo ck lockur) #:use-module (crates-io))

(define-public crate-lockur-0.1.0 (c (n "lockur") (v "0.1.0") (d (list (d (n "aead") (r "^0.5.2") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0x4hsfva8ilgnszcb9ca2wqg406l5wmb9hpc8dalkfp96av5hzb0")))

