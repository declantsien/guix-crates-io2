(define-module (crates-io lo ck lock_cell) #:use-module (crates-io))

(define-public crate-lock_cell-0.1.0 (c (n "lock_cell") (v "0.1.0") (h "156nzjrmqv2x8siv4d205kx7q1c33v4knmlckivvp2vpssaqfldv") (f (quote (("logging") ("enable_std") ("default" "enable_std"))))))

(define-public crate-lock_cell-0.2.0 (c (n "lock_cell") (v "0.2.0") (h "0gsqw305p6jlfzwzbc9l1q7kbgcwvjg7agx7aqc8yxkpvi2fjpgg") (f (quote (("logging") ("enable_std") ("default" "enable_std"))))))

(define-public crate-lock_cell-0.3.0 (c (n "lock_cell") (v "0.3.0") (h "15d7pkrmyz3iw5yvd4y96bxz6f3myh253j0xh34k3c23cyy6aiyp") (f (quote (("logging") ("enable_std") ("default" "enable_std"))))))

(define-public crate-lock_cell-0.4.0 (c (n "lock_cell") (v "0.4.0") (h "15hlv422maxrpms9n9ilqg2r4n7mvqy4d4bnqy60kqp7ajp2psgd") (f (quote (("logging") ("enable_std") ("default" "enable_std"))))))

(define-public crate-lock_cell-0.5.0 (c (n "lock_cell") (v "0.5.0") (h "1my6l1qkaajazn17ib9mz56h0cv3fr9zpb6dn0rs2gyqr13581vj") (f (quote (("logging") ("enable_std") ("default" "enable_std") ("debug_lockcell"))))))

(define-public crate-lock_cell-0.5.1 (c (n "lock_cell") (v "0.5.1") (h "0r00x5nq0dizibbjck4hh3wrj0wgvldl7yjcsaacbw8iz18rihiy") (f (quote (("logging") ("enable_std") ("default" "enable_std") ("debug_lockcell"))))))

(define-public crate-lock_cell-0.6.0 (c (n "lock_cell") (v "0.6.0") (h "0a45jj364vp98az4dlnz7557y14vmjxfx03rci5vbhcj1b901s10") (f (quote (("logging") ("enable_std") ("default" "enable_std") ("debug_lockcell"))))))

