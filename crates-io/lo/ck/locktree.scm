(define-module (crates-io lo ck locktree) #:use-module (crates-io))

(define-public crate-locktree-0.1.0 (c (n "locktree") (v "0.1.0") (d (list (d (n "locktree-derive") (r "^0.1.0") (d #t) (k 0)))) (h "13v0gvaiwg29z00ynk3nz6n2yimn0wa956cx9mwhiwlbv4h3ir9j")))

(define-public crate-locktree-0.2.0 (c (n "locktree") (v "0.2.0") (d (list (d (n "locktree-derive") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "sync"))) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0s0pq0wic0slksaijna78pgkbw0fachrsb06lpiwlmq45pbh1qrc") (f (quote (("tokio_0_2" "tokio") ("default" "async" "tokio_0_2") ("async"))))))

(define-public crate-locktree-0.3.0 (c (n "locktree") (v "0.3.0") (d (list (d (n "locktree-derive") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "sync"))) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16mxzjfcmy173py3nwsmrpm1xy491p9d5lkang5dc80jp0zp01jb") (f (quote (("tokio_0_2" "tokio") ("default" "async") ("async"))))))

