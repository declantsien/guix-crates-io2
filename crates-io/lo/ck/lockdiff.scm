(define-module (crates-io lo ck lockdiff) #:use-module (crates-io))

(define-public crate-lockdiff-1.1.1 (c (n "lockdiff") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1sa2zl6ypmhvfszfk04mcm7ljfkifwi3wxb9878jfv4iv5c11nn5")))

(define-public crate-lockdiff-1.1.2 (c (n "lockdiff") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1wyz4728qmf5i0b0nd1jy3m17mx8nf0jf82qgm00fq7kvhjm7yn1")))

(define-public crate-lockdiff-1.1.3 (c (n "lockdiff") (v "1.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "0wk9mcvqm6n0v2291ysxvkq355m1d6rwfgc3as8rvqq3nn2d9dr1")))

(define-public crate-lockdiff-1.1.4 (c (n "lockdiff") (v "1.1.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "0yd10ah6xykw9j53w6jbnsjw5whai2v5i7qhxpjiaahi6hsqva31")))

(define-public crate-lockdiff-1.1.5 (c (n "lockdiff") (v "1.1.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "08wkiixj0gxjfi88gq290hwax720fmgp33m4f1z16y5ll9sg1gq6")))

(define-public crate-lockdiff-1.1.6 (c (n "lockdiff") (v "1.1.6") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.6.0") (d #t) (k 0)))) (h "0c78gvgps9x5wg4nyr9ajzlynsc3sa1rzjd6ad1sly2r1h2i5gfl")))

