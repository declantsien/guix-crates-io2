(define-module (crates-io lo ck lock-free-freelist) #:use-module (crates-io))

(define-public crate-lock-free-freelist-0.1.0 (c (n "lock-free-freelist") (v "0.1.0") (d (list (d (n "bit_fiddler") (r "^2.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "reusable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lbwjhyq9q6zwpz2q1a9fh38s3c20vz9w36a7v8bpm6xlbg8anca")))

