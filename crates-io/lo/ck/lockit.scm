(define-module (crates-io lo ck lockit) #:use-module (crates-io))

(define-public crate-lockit-0.1.0 (c (n "lockit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "1mkbq15z4vwf3hcdm5m9qnjqjs0iayf0hygpx3mp3iiv6wkccc2z")))

