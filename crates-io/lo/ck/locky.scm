(define-module (crates-io lo ck locky) #:use-module (crates-io))

(define-public crate-locky-0.0.1 (c (n "locky") (v "0.0.1") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 2)) (d (n "aes-kw") (r "^0.2.1") (d #t) (k 0)) (d (n "ml-kem-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "tonic") (r "^0.11.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "0mnwnzpsi86p0vkjjirmirc88j2gr14g1bimcdn3x8wl6y41fcdq") (f (quote (("tls-webpki-roots" "tonic/tls-webpki-roots") ("default" "tonic/tls-roots") ("build-protos" "tonic-build"))))))

