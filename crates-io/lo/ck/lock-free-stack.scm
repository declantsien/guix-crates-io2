(define-module (crates-io lo ck lock-free-stack) #:use-module (crates-io))

(define-public crate-lock-free-stack-0.1.0 (c (n "lock-free-stack") (v "0.1.0") (h "0czhcn8ba9fbkagmdxgyvga0jl45vl5ld44a0fc0d9qwmc3mz9j8")))

(define-public crate-lock-free-stack-0.1.1 (c (n "lock-free-stack") (v "0.1.1") (h "0788zv8dlg577jl2llwszkkihv91xnqn4b3av4rc3dysqy28l74r")))

