(define-module (crates-io lo ck locked) #:use-module (crates-io))

(define-public crate-locked-0.1.0 (c (n "locked") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02rmyrafklklczqg00fawmvnvn3w5x6rgszmy426wahx7silax5v")))

