(define-module (crates-io lo an loaned) #:use-module (crates-io))

(define-public crate-loaned-0.1.0 (c (n "loaned") (v "0.1.0") (h "07bail7hqv2p2zfqfz60far24xs35s653nhqhixi5xzvgd8n8bys") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-loaned-0.1.1 (c (n "loaned") (v "0.1.1") (h "1bqmpb9cx3xnbcjxx42n543pim01ybfijx6cdw1gzf17c41bwvzi") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

