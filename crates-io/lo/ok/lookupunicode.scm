(define-module (crates-io lo ok lookupunicode) #:use-module (crates-io))

(define-public crate-lookupunicode-0.1.0 (c (n "lookupunicode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 1)) (d (n "indoc") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0jvpm958zgxlw0cih8hpm03zl97p6ww1s0ia905v9hjmpjhcskqq")))

