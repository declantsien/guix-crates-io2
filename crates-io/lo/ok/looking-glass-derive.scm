(define-module (crates-io lo ok looking-glass-derive) #:use-module (crates-io))

(define-public crate-looking-glass-derive-0.1.0 (c (n "looking-glass-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "looking-glass") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0v9yzx0jq1mk9kcr9bf5h1gjqldd1h7sby0y85dmc89sfibgdbys")))

(define-public crate-looking-glass-derive-0.1.1 (c (n "looking-glass-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "looking-glass") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0flm41vhgj0x6vvsg41yqdsr37hf8ajb2l5bzbry00mmi188kdkj")))

