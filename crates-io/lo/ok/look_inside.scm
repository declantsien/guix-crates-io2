(define-module (crates-io lo ok look_inside) #:use-module (crates-io))

(define-public crate-look_inside-0.1.0 (c (n "look_inside") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.43") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0i0f4fq806wahpazawd6a3z3yn5vj7djamdrrnf13lp7r31125k7")))

(define-public crate-look_inside-0.2.1 (c (n "look_inside") (v "0.2.1") (d (list (d (n "look_inside_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0ki20arsqjrza0kpcm292210n5kkib5llqlswhvhlhzkr6qz8jj6")))

