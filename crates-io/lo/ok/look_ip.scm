(define-module (crates-io lo ok look_ip) #:use-module (crates-io))

(define-public crate-look_ip-0.1.0 (c (n "look_ip") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "1b44fmnavjf2ncgfdw2xd7qg25c9faz1fsbql1gq1il2dy0f9hgi")))

(define-public crate-look_ip-0.1.1 (c (n "look_ip") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0yhqldypqwa9wi06q91hm3smi969rd802lcwkjp6lq81881ngpnr")))

(define-public crate-look_ip-0.1.2 (c (n "look_ip") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0k84vl8lp7jck42g9qxzgnm9ymwi4gv00dmx7cmb6kk3kxm9kcsx")))

(define-public crate-look_ip-0.1.3 (c (n "look_ip") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "1p3xmgfb8qa9ql1cvkzi8lxs9y87a2ql52h087j4w1c8f9hbca3q")))

(define-public crate-look_ip-0.1.4 (c (n "look_ip") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0lgjbs418ghd7a7p9aixlsvvkj3wm1impmghgl0km5w5gnp1j76v")))

(define-public crate-look_ip-0.1.5 (c (n "look_ip") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0dipkp1aqgrg7dhxj7i87sk5dxqvayxjan8f9da1ff8k877y9dj8")))

(define-public crate-look_ip-0.1.6 (c (n "look_ip") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "1jv7cq88yi6ljdgms4qgadx5qcbwz3553v9235w41fqhl4hymckj")))

(define-public crate-look_ip-0.1.7 (c (n "look_ip") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "1jgvwvzkv40myrqc6jjajxpazgh7why3dmn15iknx851yrbw14ba")))

(define-public crate-look_ip-0.1.8 (c (n "look_ip") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0ziwa0iypmw0ygclp8cmpn83ky6ksg1xacz00a9jrwhhvadgh4ba")))

(define-public crate-look_ip-0.1.9 (c (n "look_ip") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0fpf4sa55klja55fxy9fh0r89068ay0y2x3rd38dd4cksv0j6csg")))

(define-public crate-look_ip-1.0.0 (c (n "look_ip") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21") (f (quote ("dns-over-rustls" "tokio"))) (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.18") (d #t) (k 0)))) (h "0fpk0w3cydj112vjik5qk5fqnz720kcjaaxcvdhbj4c55c0k2vp3")))

