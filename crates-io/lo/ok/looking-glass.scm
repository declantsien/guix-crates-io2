(define-module (crates-io lo ok looking-glass) #:use-module (crates-io))

(define-public crate-looking-glass-0.1.0 (c (n "looking-glass") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i8lfh62h182qx8p3kjirmgi9r2i7810wjsv3a4lnnnh01kaxh42")))

(define-public crate-looking-glass-0.1.1 (c (n "looking-glass") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "looking-glass-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zk1mavrzwsp4w0cmii8861mviw93p23zmch2hi15nkwvb4iwcaw")))

