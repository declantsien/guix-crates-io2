(define-module (crates-io lo ok lookups) #:use-module (crates-io))

(define-public crate-lookups-0.0.1 (c (n "lookups") (v "0.0.1") (h "08h0gzg53alf30x7z3mvykmvv2krawwy5zgbirmrwixk8ijna04m")))

(define-public crate-lookups-0.1.1 (c (n "lookups") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1j82q415rn3q1404acw6iq12bzqg9vy0fsbxbqaxip1kdv83xykb") (s 2) (e (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-lookups-0.1.2 (c (n "lookups") (v "0.1.2") (d (list (d (n "divan") (r "^0.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1m4yirafkq77w4qy5dwgavlmp4xxhc05j4603adrvx7zs96yxaif") (s 2) (e (quote (("hashbrown" "dep:hashbrown")))) (r "1.75")))

(define-public crate-lookups-0.2.0 (c (n "lookups") (v "0.2.0") (d (list (d (n "divan") (r "^0.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1fpbkfvnxdk4avdmibph9p5lapawmk0bk1gxmwids4szwm9g9889") (s 2) (e (quote (("hashbrown" "dep:hashbrown")))) (r "1.75")))

