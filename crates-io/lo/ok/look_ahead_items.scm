(define-module (crates-io lo ok look_ahead_items) #:use-module (crates-io))

(define-public crate-look_ahead_items-0.1.0 (c (n "look_ahead_items") (v "0.1.0") (h "0v2pxbm1pn9clz6shl3l29mgg0bx9ipj2ahawsb1zb0fzw483dyj")))

(define-public crate-look_ahead_items-0.1.1 (c (n "look_ahead_items") (v "0.1.1") (h "1jcc5d911j905yrk07rc2ikbfh5fapipbaszb41fv541hq3m4yw6")))

