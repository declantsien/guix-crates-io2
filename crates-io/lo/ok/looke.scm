(define-module (crates-io lo ok looke) #:use-module (crates-io))

(define-public crate-looke-0.1.0 (c (n "looke") (v "0.1.0") (d (list (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)))) (h "000yizyii94fz6ibwj5jnavahgp2c2pwggvr2s6qd59bs0pw7dk3")))

(define-public crate-looke-0.2.0 (c (n "looke") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)))) (h "0cp80pv06nidnkkfb9xcky0lhjhcmb0i1s1p1innnizq74fi7wvg")))

(define-public crate-looke-0.2.1 (c (n "looke") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)))) (h "1lcalsf0q0x2sygdn0lw2mshk4afb77ja03xlx961z3yvln5vzmb")))

