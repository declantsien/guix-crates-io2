(define-module (crates-io lo ok looking-glass-protobuf) #:use-module (crates-io))

(define-public crate-looking-glass-protobuf-0.1.0 (c (n "looking-glass-protobuf") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "looking-glass") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0d3j02zlyv3cscf0c5i4nar589h34bhqbjxgk0di5aiq9lrpmp0k")))

(define-public crate-looking-glass-protobuf-0.1.1 (c (n "looking-glass-protobuf") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "looking-glass") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1zgil03sq61zpj1p7gj1s62ipx88dgq244h68kynhsyjlp28b1dl")))

