(define-module (crates-io lo ok lookit) #:use-module (crates-io))

(define-public crate-lookit-0.1.0 (c (n "lookit") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (f (quote ("async"))) (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1c45bgj3wjy3id5288nxjmg5zx4zz113vhnwijb5lgy7c5zag94q")))

(define-public crate-lookit-0.1.1 (c (n "lookit") (v "0.1.1") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.6") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0jhz7b2nzzxhchi1qcc2x8ccjwn15l0jhmymfmwpnanwksk9kh7l")))

(define-public crate-lookit-0.2.0 (c (n "lookit") (v "0.2.0") (d (list (d (n "async_main") (r "^0.2") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.13") (d #t) (k 0)) (d (n "smelling_salts") (r "^0.11") (d #t) (k 0)))) (h "1r9ai4viadp876xccywz7nbbpyrwwsa5wa9c19mwf5qr4plqb3aq") (r "1.66")))

(define-public crate-lookit-0.3.0 (c (n "lookit") (v "0.3.0") (d (list (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 0)) (d (n "smelling_salts") (r "^0.12") (d #t) (k 0)))) (h "1hy3xymvakrkkfzxay4jyjar22m6q601484y8f1hb6ahgnzzyk5b") (r "1.66")))

(define-public crate-lookit-0.3.1 (c (n "lookit") (v "0.3.1") (d (list (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 0)) (d (n "smelling_salts") (r "^0.12") (d #t) (k 0)))) (h "11s9ws1p77bk8mhkmv42zp3pnwysjkswsj5pa7aw4v0dd36hhrvm") (r "1.66")))

(define-public crate-lookit-0.3.2 (c (n "lookit") (v "0.3.2") (d (list (d (n "async_main") (r "^0.3") (f (quote ("pasts"))) (d #t) (k 2)) (d (n "pasts") (r "^0.14") (d #t) (k 0)) (d (n "smelling_salts") (r "^0.12") (d #t) (k 0)))) (h "10jg9y51v6ad30lrzsscyzgfzgm9iv02is72wy9q82lmwnf9axci") (r "1.66")))

