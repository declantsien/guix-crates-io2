(define-module (crates-io lo ok lookbusy) #:use-module (crates-io))

(define-public crate-lookbusy-0.1.0 (c (n "lookbusy") (v "0.1.0") (h "048csjrv2xd48za0d6c5zc3dawq24anymdypavmmmbah1phnswkv") (y #t)))

(define-public crate-lookbusy-0.1.1 (c (n "lookbusy") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07pzvh6xmwncz31cvy7p25f3csjl7r0by019k31cgqifj6ygfww3")))

(define-public crate-lookbusy-0.2.0 (c (n "lookbusy") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16223ybgm5nmnm9vqh3m9d28p53aqm17in3bzmpb8c3dl16lb26c")))

(define-public crate-lookbusy-0.2.2 (c (n "lookbusy") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)))) (h "1j0lgdlkqb9j3y239va6cz713cixggkw3a3ahb1xhx62sn1lgc9x")))

(define-public crate-lookbusy-0.2.3 (c (n "lookbusy") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)))) (h "1fkgr6g3dbjrinprdhql5jpfidny3wzh023jfjiyrp542dr0z13d")))

(define-public crate-lookbusy-0.2.4 (c (n "lookbusy") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)))) (h "0by70snqckn8vs9a87cy4gh7rcdmyxz4pd0syqvari4j55imyf9d") (y #t)))

(define-public crate-lookbusy-0.2.5 (c (n "lookbusy") (v "0.2.5") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)))) (h "182cibp127m6bkq78h7rz8zzrljr56a34xpqqn553b3a87nlya06")))

(define-public crate-lookbusy-0.2.6 (c (n "lookbusy") (v "0.2.6") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)))) (h "0p4qr56198yq6jsrn6pafzy3bfngzxs395k22mhg5cp073wv43ga")))

