(define-module (crates-io lo ok lookrd) #:use-module (crates-io))

(define-public crate-lookrd-0.1.0 (c (n "lookrd") (v "0.1.0") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "0h80hfq4fk8y75s9qfvf38wakfnnagh60xvhmpa61n32hfv5vphs")))

(define-public crate-lookrd-0.1.1 (c (n "lookrd") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0q9645vc6diw84f9j4cxf7lkn4fvnlmr2kqmg3dcpkg1kzafyfh0")))

