(define-module (crates-io lo ok lookaround) #:use-module (crates-io))

(define-public crate-lookaround-0.1.0 (c (n "lookaround") (v "0.1.0") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ksx3wv0lh5vi10z8m72syflb6gk1d5f1g4icpk529cm5fmqalb5")))

(define-public crate-lookaround-0.1.1 (c (n "lookaround") (v "0.1.1") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nd8ysj89ff5iflgqjgxkw299gz1x4zlbb5irqk6dr5smma23qbp")))

(define-public crate-lookaround-0.1.2 (c (n "lookaround") (v "0.1.2") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0dx7jifrs6sw564cq5c109lpyfb1xd687fwvhfa20k3swkvjdw07")))

(define-public crate-lookaround-0.1.3 (c (n "lookaround") (v "0.1.3") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07vlaj5asf0nhmrdd4hsn30lkaxvnzdafd50v7kcwqrzhbb91bkf")))

(define-public crate-lookaround-0.1.4 (c (n "lookaround") (v "0.1.4") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10b9kf71iwpk9xpqa16p4shlgvxi7vl99djlx5l0fli7jijb6723")))

(define-public crate-lookaround-0.1.5 (c (n "lookaround") (v "0.1.5") (d (list (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("fs" "net" "rt" "time"))) (d #t) (k 0)))) (h "0qv7bg9vgiskad2dk05jgqfg9gjx57ymln30icamsaf4kx53krjh")))

(define-public crate-lookaround-0.1.6 (c (n "lookaround") (v "0.1.6") (d (list (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("fs" "net" "rt" "time"))) (d #t) (k 0)))) (h "0fc07773hi0hlg6c7h3i1ny411xb8vpp9gnf6cv5axyyry8xx3rm")))

