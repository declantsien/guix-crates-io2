(define-module (crates-io lo ok lookr) #:use-module (crates-io))

(define-public crate-lookr-0.1.0 (c (n "lookr") (v "0.1.0") (h "1l7m3rvwrxc0xa7dz10ld319g3qbx9vmq5pgd30npp0x3bbrk7hs")))

(define-public crate-lookr-0.1.1 (c (n "lookr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lookrd") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2") (d #t) (k 0)))) (h "08m0fsfnjxhy726bycnfgrkgajfgxxphb95r01awf2ibqsnq31vb")))

