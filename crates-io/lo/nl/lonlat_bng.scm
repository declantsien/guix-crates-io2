(define-module (crates-io lo nl lonlat_bng) #:use-module (crates-io))

(define-public crate-lonlat_bng-0.1.1 (c (n "lonlat_bng") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0zycxsydgl7kq4brxsd5b3x3dvrq2l551f5dcz8ixrk3jnlzy8x8")))

(define-public crate-lonlat_bng-0.1.2 (c (n "lonlat_bng") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pbmqmnl4sn4xpdxyx3issldqaa9xh9x8v1h8nclhwfr8vfapav0")))

(define-public crate-lonlat_bng-0.1.3 (c (n "lonlat_bng") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p86hq97qh45igh90i6764jsx82j29ga7052qfp3rda96paswlyn")))

(define-public crate-lonlat_bng-0.1.5 (c (n "lonlat_bng") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "0cgmss06c0nlxyxrbr6bi5ilpaanis7gcihjvh72fl6r0mq6a8na")))

(define-public crate-lonlat_bng-0.1.6 (c (n "lonlat_bng") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "154f2q78lpx78rwy33789hjmq2j4f0qgam0kcjp39gyhixmqamql")))

(define-public crate-lonlat_bng-0.1.7 (c (n "lonlat_bng") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "17w4j996nkyh9v7nfd7bydv2y5mw4wbbnb579ankbzx4a6nax5c3")))

(define-public crate-lonlat_bng-0.1.8 (c (n "lonlat_bng") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0fji5hzklx9frpbkiy2wzwh7pxm14cdgl73dv863app6mxcan05s")))

(define-public crate-lonlat_bng-0.1.9 (c (n "lonlat_bng") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "1cg690mnibg9ax874yhbr4sb206wrnsfwsqfzj9yksdbyi1awahn")))

(define-public crate-lonlat_bng-0.1.10 (c (n "lonlat_bng") (v "0.1.10") (d (list (d (n "crossbeam") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "06d1pa1pv2w766ncggfnanjqw5m2vbcby164m4wz35g61hvwacpq")))

(define-public crate-lonlat_bng-0.1.11 (c (n "lonlat_bng") (v "0.1.11") (d (list (d (n "crossbeam") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "17d4sizplj2kw7zgw5g7x9g1hx8c2l718kzw9rkxkrlbpqrdawj7")))

(define-public crate-lonlat_bng-0.2.2 (c (n "lonlat_bng") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0lmg7gqgac34clj2qb9jdndy30f7njaxqb1yrs1gp4kwcrpnk0x3")))

(define-public crate-lonlat_bng-0.2.3 (c (n "lonlat_bng") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "08piq8yii1x4cxpgzg1zjdj16gr5c86ms04sb4ag0rm755a0dqgk")))

(define-public crate-lonlat_bng-0.2.4 (c (n "lonlat_bng") (v "0.2.4") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.10") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0md1h8sl4xqj8kh8gs2xkf011qsxwqh25cx80pp9202lyw9fjxxl")))

(define-public crate-lonlat_bng-0.2.6 (c (n "lonlat_bng") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1y0w40x8g5c8lb4a4023cq9dbqjddrnxl23bzqa4y1d334c30zgv")))

(define-public crate-lonlat_bng-0.2.8 (c (n "lonlat_bng") (v "0.2.8") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.11") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1d1hpnf1yi7m8xdhh937xqc84hq1j7in2yxbsd665yq8rr8si35c")))

(define-public crate-lonlat_bng-0.2.9 (c (n "lonlat_bng") (v "0.2.9") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.11") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1md00jsbkwfkfjrkqq4i8y3x2b8a7fjcli9r6q2rvhsyanf1543c")))

(define-public crate-lonlat_bng-0.3.0 (c (n "lonlat_bng") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.11") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "045bxmsbhz97mjzyrn4wclb95rfhsdml4mwcrifnlgz3wars0ff2")))

(define-public crate-lonlat_bng-0.3.1 (c (n "lonlat_bng") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.11") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1cn0iknfb4bkvsmzhv0al706q9hsj9a3v3r1xqm4yxr7w2z8wzwf")))

(define-public crate-lonlat_bng-0.3.7 (c (n "lonlat_bng") (v "0.3.7") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.12") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1j5km2hdf4yg5ffyddnfjswal6banyaqpqg0bifdf5n8fm5wldvz")))

(define-public crate-lonlat_bng-0.3.8 (c (n "lonlat_bng") (v "0.3.8") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "ostn02_phf") (r "^0.1.13") (d #t) (k 0)) (d (n "phf") (r "^0.7.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.14") (d #t) (k 1)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0bpdgkx717i003mhyffal7y7l5nwpfic2v9sllp3chxl1cyizzn0")))

