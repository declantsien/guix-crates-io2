(define-module (crates-io lo gz logz) #:use-module (crates-io))

(define-public crate-logz-0.1.0 (c (n "logz") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "08wfaqcyac61v7krm27hy3fzxgi48km1mlcgm7ac610dp77rmaxz")))

(define-public crate-logz-0.1.1 (c (n "logz") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15r52914y6vs1nyn52qkslpl8cmh49h7bwqcags9sl2czcmij1kp")))

(define-public crate-logz-0.1.2 (c (n "logz") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07yp9cl0mxndqw4ijlz3pmh912nwy144n07fjbn76l0kbfkfjppz")))

