(define-module (crates-io lo gm logma) #:use-module (crates-io))

(define-public crate-logma-0.1.0 (c (n "logma") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1fx3s2qaxwpzb6wc47vgh24957g87ykk41gmmyi69ibsjz0gqq37") (y #t)))

(define-public crate-logma-0.1.1 (c (n "logma") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0ws0b7lp9wx7y1qy814lqc1xr8dl173aqjr52xc7s4arasfimr71") (y #t)))

(define-public crate-logma-0.1.2 (c (n "logma") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0klvv4bld5a115w0rcx6f8vmpy5bjxqsbdn6zwmh9f52jhdbsxy4") (y #t)))

(define-public crate-logma-0.1.3 (c (n "logma") (v "0.1.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0zrnmy778jnrg41sg93h2g7qagrk9rg49fhdxn824i1by7djgv6k") (y #t)))

(define-public crate-logma-0.1.4 (c (n "logma") (v "0.1.4") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0mw1gg13iiq21a6d2446h950cc2n20ksjlskscsvkw4rad951kz0") (y #t)))

(define-public crate-logma-0.1.5 (c (n "logma") (v "0.1.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0k6mgzzi1h6j947av7h6ankf919bssw824z49gjy1dkx25yaqj5q") (y #t)))

(define-public crate-logma-0.1.6 (c (n "logma") (v "0.1.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0k0w45y10p15aw0xw580nni82ahp091k2kak3mzd0ny246rr97ks") (y #t)))

(define-public crate-logma-0.1.7 (c (n "logma") (v "0.1.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "06n93gygzmwvhd7yj0iq6dqm895k2ab2x0kj56jqnp3ip8xkiyam") (y #t)))

(define-public crate-logma-0.1.8 (c (n "logma") (v "0.1.8") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1v15iqvbfvbimafygw2h384qclkl4nzyk4aina6rbx0j31z30y9c") (y #t)))

(define-public crate-logma-0.1.9 (c (n "logma") (v "0.1.9") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1hpi4zlb9kqlyw0kc2bcjjr9fb0zh91z7l7rz4ii17rmdlivrcha")))

