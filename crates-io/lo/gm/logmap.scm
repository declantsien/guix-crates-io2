(define-module (crates-io lo gm logmap) #:use-module (crates-io))

(define-public crate-logmap-0.1.0 (c (n "logmap") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.3.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "15f52ywi4763srfixfqqq91xvlh57z79fxnvc8v9brj5fh0zsr2j")))

(define-public crate-logmap-0.2.0 (c (n "logmap") (v "0.2.0") (d (list (d (n "abox") (r "^0.4.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.9") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.3.2") (d #t) (k 2)) (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "0wavkck2626gn68i6r973q3xbxv8562z26877ixmzh7vv32cbbmm")))

