(define-module (crates-io lo on loongarch64) #:use-module (crates-io))

(define-public crate-loongArch64-0.1.0 (c (n "loongArch64") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "00kd3q8grnl61hrz7byhxy8j6cwyzsn3cccmvd3dr9qahcgk57r0") (y #t)))

(define-public crate-loongArch64-0.2.0 (c (n "loongArch64") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0vjl8amnb0pbzakbakqhvh9pnpkzs982jxsmwpqff8b48jxp9wsx")))

(define-public crate-loongArch64-0.2.1 (c (n "loongArch64") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "148jdgarqsp2ds9bjnjggbp1c66ph7gbj98k9z1q7kaqp7xfh0xg")))

(define-public crate-loongArch64-0.2.2 (c (n "loongArch64") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0i14fgwn6850lgz6prrb8a9z8l7lfl6x49gpxh1cs1jzb208sq38")))

(define-public crate-loongArch64-0.2.4 (c (n "loongArch64") (v "0.2.4") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0lmm2xafi6n4lb9xmaxbn5h7si5mfx5j16w99r76crjl8q6j0j6x")))

