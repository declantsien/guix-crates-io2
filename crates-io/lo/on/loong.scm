(define-module (crates-io lo on loong) #:use-module (crates-io))

(define-public crate-loong-0.1.0 (c (n "loong") (v "0.1.0") (h "11q7mcyh89mz7k2964k9r7rqa9949mclpjjvikcamyj4lphfbh08")))

(define-public crate-loong-0.1.1 (c (n "loong") (v "0.1.1") (h "0f6mnllfasxk3rdpm02n1w5q4jjzbm4mv72mx0ir56q31pmj4vn4")))

