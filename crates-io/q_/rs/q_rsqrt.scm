(define-module (crates-io q_ rs q_rsqrt) #:use-module (crates-io))

(define-public crate-q_rsqrt-0.1.0 (c (n "q_rsqrt") (v "0.1.0") (h "0rcnblifkl4701xdshb5f7hm41rpr8cvcf1qrvv1ggnv2riir5sd")))

(define-public crate-q_rsqrt-0.1.1 (c (n "q_rsqrt") (v "0.1.1") (h "0zqa29369gm0jgv95mrmbicjhgls5jrxy9mmp450fkicraqwgr2k")))

