(define-module (crates-io q_ co q_compress) #:use-module (crates-io))

(define-public crate-q_compress-0.0.0 (c (n "q_compress") (v "0.0.0") (h "1bszf73pzq7ql8bp9a6g0zzwl082m48g3radnncv83r3y3h8p0ns")))

(define-public crate-q_compress-0.1.0 (c (n "q_compress") (v "0.1.0") (h "1rra35ra6q6dprvdn0wn43iq9nnq46zi2pjizz2668lgkr2ydvvj")))

(define-public crate-q_compress-0.2.0 (c (n "q_compress") (v "0.2.0") (h "1kliig46g95fr46hc6g76fngkvx3c0z4jp000k8c1h59vq38pr5a")))

(define-public crate-q_compress-0.2.1 (c (n "q_compress") (v "0.2.1") (h "1axwwfhb0v6zqzrmg2jlqq8x1kgrf6m6k9g0n2hhvmfk2x1pjkr4")))

(define-public crate-q_compress-0.2.2 (c (n "q_compress") (v "0.2.2") (h "1i1ncyr9cjm5h22g9wvb72jbhsx4097sbj8nmwz9cb2bwsh5qi7s")))

(define-public crate-q_compress-0.2.3 (c (n "q_compress") (v "0.2.3") (h "05gi7q745jkvpcl8pwmazj2255lb9mcxmn7hkwdyw9si8fvhkycj")))

(define-public crate-q_compress-0.3.0 (c (n "q_compress") (v "0.3.0") (h "0gsdx76hazilj2hajl2im9icha9jxh2hc8p1zxk6n16hqj7yn1y6")))

(define-public crate-q_compress-0.3.1 (c (n "q_compress") (v "0.3.1") (h "19dq729i4ma92sgim9m0j2kl80rik7nfzr26p1grk0mdhh44433b")))

(define-public crate-q_compress-0.4.0 (c (n "q_compress") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0x8ly4w3lx40993kayf0x35ayb5lsxnvvpscib9jlk5p70n03jwi")))

(define-public crate-q_compress-0.4.1 (c (n "q_compress") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1aicz0ryqxqh0y33lxginr699dyanxsda4ihyjqsw78g82gldnyx")))

(define-public crate-q_compress-0.5.0 (c (n "q_compress") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0kfl9m8x9bgzm8sgbf62dyby0244948rb0npzm7zas6byjk28ng1") (y #t)))

(define-public crate-q_compress-0.6.0 (c (n "q_compress") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0n5pq2fggczgxli0pgwijha4b46aih036dlhbg24sf79dxic1092")))

(define-public crate-q_compress-0.6.1 (c (n "q_compress") (v "0.6.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0ayx66h22jicyisby8kingz2ndw7w9w3cakqj6ydsc72phd44ai4")))

(define-public crate-q_compress-0.7.0 (c (n "q_compress") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "05mww7x8yw84460zilhvjgqr358psxwfjnvdrspcxawpx8fc82xh")))

(define-public crate-q_compress-0.8.0 (c (n "q_compress") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "117cjyhrmsw2w5ryymrnhzhljg0cdgmjffilf2qgmqgmaax510g8")))

(define-public crate-q_compress-0.9.0 (c (n "q_compress") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "0sr5pr5y60a5y1f3bl0fagzcc064icb044zg5d6c89qv60pvvs4m")))

(define-public crate-q_compress-0.9.1 (c (n "q_compress") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1al7im2cvalyrv7faiv36jwj5bjkp9jzfysv1h1ids1i7q52vn9x")))

(define-public crate-q_compress-0.9.2 (c (n "q_compress") (v "0.9.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "0xdirh4n0hafgjf2abg2j64bmiqln2llrdba1xp2qchfq3s862lk")))

(define-public crate-q_compress-0.9.3 (c (n "q_compress") (v "0.9.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "012k1snfa2yjq0ym4j5i9ng7igq0ygy3ihnyh1wdmijmgxs7dz21")))

(define-public crate-q_compress-0.10.0 (c (n "q_compress") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1k4r7as8vg3kiakinpwffd7789a3raqf7q4q7qy72nc2rfk6sab2")))

(define-public crate-q_compress-0.10.1 (c (n "q_compress") (v "0.10.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "13nlrfw4vf87xq53d1a2kk6dpqmb5i17wg7mpgamfqaxvv0w18xx")))

(define-public crate-q_compress-0.10.2 (c (n "q_compress") (v "0.10.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1ackxn49ga1f5zxd1gqxgwsqpcva1wyp49y7maar3c3ihkmawjg8")))

(define-public crate-q_compress-0.11.0 (c (n "q_compress") (v "0.11.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1xfiqxm4qc27fv4w0ys2mx8skp446qpdniy0vplh0f829f9f0m93") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.1 (c (n "q_compress") (v "0.11.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1ag47g96bm2i1wr5c3jgk97iccxg95wwqgk4vpghykzp357sb43r") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.2 (c (n "q_compress") (v "0.11.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "126i2nqkg4hw2q50lxr0s3bs6zp1d7aviqy37jb4mpw1mhm9hd5q") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.3 (c (n "q_compress") (v "0.11.3") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1n1h3znrwyymdxfdc282hlkwl417fylrprcz9f0ipzm7078n4zkl") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.4 (c (n "q_compress") (v "0.11.4") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "0fpv0x1i8gffl399z6ddwkbpfqpcabf9p4m9bv3pxfx5n31kgr8x") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.5 (c (n "q_compress") (v "0.11.5") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1gr5xf5cgpzf7cxbar3xwl8m785y8ny7jz124nmi6snpgwivifr3") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.6 (c (n "q_compress") (v "0.11.6") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1yqcjyqigbr47q4gw7ks6j5bn1w3hczzv7iw8dvp8kywp9p8wdwi") (f (quote (("timestamps_96"))))))

(define-public crate-q_compress-0.11.7 (c (n "q_compress") (v "0.11.7") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "zstd") (r "^0.10") (d #t) (k 2)))) (h "1m77ga7v78ni1p95dblqxjrdlcr3v35h90rlq26n3klws9fypmbd") (f (quote (("timestamps_96"))))))

