(define-module (crates-io w7 #{50}# w7500x) #:use-module (crates-io))

(define-public crate-w7500x-0.1.0 (c (n "w7500x") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pzgkqvfl5nqfklpcqiy44kffk19ndzgahdzgxpn53a9xp7qgrpp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

