(define-module (crates-io w7 #{50}# w7500x-pac) #:use-module (crates-io))

(define-public crate-w7500x-pac-0.2.0 (c (n "w7500x-pac") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0jgglii2gyg7qbmfz7zfikvykxi8lyc9zs797h25ral53lxwaxix") (f (quote (("rt" "cortex-m-rt/device"))))))

