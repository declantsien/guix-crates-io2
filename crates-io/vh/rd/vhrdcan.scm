(define-module (crates-io vh rd vhrdcan) #:use-module (crates-io))

(define-public crate-vhrdcan-0.1.0 (c (n "vhrdcan") (v "0.1.0") (d (list (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fjk6cwzwr7q5wgnqswgvaic6k9pq2yij1y8yzk6hb58x7aj1hpk") (f (quote (("serialization" "serde"))))))

