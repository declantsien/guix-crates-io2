(define-module (crates-io vh dl vhdl_lang_macros) #:use-module (crates-io))

(define-public crate-vhdl_lang_macros-0.74.0 (c (n "vhdl_lang_macros") (v "0.74.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1k3cfc3hwckfd9g8x2gf0pf14rhl2bf7148if194xqjnn8s1g551")))

(define-public crate-vhdl_lang_macros-0.75.0 (c (n "vhdl_lang_macros") (v "0.75.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12vqgwc3pj93326gn8rys8x0y6yzzmwiwk81f1zyz0x73ndy0nmq")))

(define-public crate-vhdl_lang_macros-0.76.0 (c (n "vhdl_lang_macros") (v "0.76.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jrs5ln5p5alhy3834n956vx6hvca8clzm2wpq3sapf8hyidy1fl")))

(define-public crate-vhdl_lang_macros-0.77.0 (c (n "vhdl_lang_macros") (v "0.77.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0369vg9mwf08cxbn1bv5i3yp8rbx9y6g89rgmvz6h0jrv7c85mip")))

(define-public crate-vhdl_lang_macros-0.78.0 (c (n "vhdl_lang_macros") (v "0.78.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbldr8fkv8p290fcbry9n21jqmy2ggg615k3y71yaxgcsbzqp38")))

(define-public crate-vhdl_lang_macros-0.78.1 (c (n "vhdl_lang_macros") (v "0.78.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0snz6zh05n0s6zk0ffajr5k1b0m0il0kbp92n1knpwlsn59zc1in")))

(define-public crate-vhdl_lang_macros-0.78.2 (c (n "vhdl_lang_macros") (v "0.78.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "050bccmb3w392bbg0li2d0vrn712wisg48biqfq341xdq344r199")))

(define-public crate-vhdl_lang_macros-0.79.0 (c (n "vhdl_lang_macros") (v "0.79.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w1a6v8sz8dnx1851aas73bb09rg4g1jhnnwpc4rhqzhzbgrcfli")))

(define-public crate-vhdl_lang_macros-0.80.0 (c (n "vhdl_lang_macros") (v "0.80.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "08ww3c8vygqhjsksybj7acqx1vzsyhv2pqla2p5clh9n1zw85h5h")))

(define-public crate-vhdl_lang_macros-0.81.0 (c (n "vhdl_lang_macros") (v "0.81.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18b0n3fqd51blgja9xgllfpghdakyx0jr1fdgprzm39avmrz30rd")))

