(define-module (crates-io vh r_ vhr_datatypes) #:use-module (crates-io))

(define-public crate-vhr_datatypes-0.1.0 (c (n "vhr_datatypes") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)) (d (n "vhr_serde") (r "^0.1.0") (d #t) (k 0)))) (h "1hxy0zd50wbpvp1mq2ajmfx0mm8mh9wb3xnrkskcw4px058vs9nx")))

