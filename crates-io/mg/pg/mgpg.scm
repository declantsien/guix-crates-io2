(define-module (crates-io mg pg mgpg) #:use-module (crates-io))

(define-public crate-mgpg-0.1.0 (c (n "mgpg") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "gpgme") (r "^0.9") (d #t) (k 0)) (d (n "keyring") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "141y6kx4r9yaaj71yw9afazszqfjagbdf2mvx2db4dx8pvrzgpji")))

