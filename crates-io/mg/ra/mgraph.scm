(define-module (crates-io mg ra mgraph) #:use-module (crates-io))

(define-public crate-mgraph-0.1.0 (c (n "mgraph") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "182034b5hvvqsgb6dbd9p2g7w67glp62hgkg45mc776fzlx7zg6j")))

(define-public crate-mgraph-0.1.1 (c (n "mgraph") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "106ki3y8w0y3vckw12bkz3kjhj1pc2yx9bnrix4v7cpidh96x3f7")))

(define-public crate-mgraph-0.1.2 (c (n "mgraph") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vqa3zwmafrxn498appj9h5mw12km6lsd7jr9v4kwhhpxw7pljji")))

(define-public crate-mgraph-0.1.3 (c (n "mgraph") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gpkz802wmf1gi5257xzjz4zbmqrdc3431vb4n9f3w4xs7pxp36b")))

(define-public crate-mgraph-0.1.4 (c (n "mgraph") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ibixw8w5zhb94x8v1cc2hhcfrz2068pvvr7mfbdj13s58ghbc9w")))

(define-public crate-mgraph-0.1.5 (c (n "mgraph") (v "0.1.5") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0njprj745gwr8g0yzyaxnf91znaqqbpmazfzinc1pn0647lr981h")))

