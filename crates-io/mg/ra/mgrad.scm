(define-module (crates-io mg ra mgrad) #:use-module (crates-io))

(define-public crate-mgrad-0.1.0 (c (n "mgrad") (v "0.1.0") (d (list (d (n "color_space") (r "^0.5") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)))) (h "1iqrsq48yh98f9gj3q28z7wv37lmzqikbgbw6zldlx5dkzggqfc7")))

