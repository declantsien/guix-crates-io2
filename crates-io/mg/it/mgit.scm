(define-module (crates-io mg it mgit) #:use-module (crates-io))

(define-public crate-mgit-0.1.0 (c (n "mgit") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hrm43mln6cjalcs6yf0ix2pc1mqf3yw9r2dxn5c2v8yhrjd0zjg")))

(define-public crate-mgit-0.1.1 (c (n "mgit") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18as4xvf2bmz2faxi295xmqy7dalnj2nfl2iz6m4mhwgzdk12v9b")))

(define-public crate-mgit-0.1.2 (c (n "mgit") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08jrlblb2sqk08wzjr854vkxcz9saf5s3l60b5v5f6y9g15xda20")))

(define-public crate-mgit-0.1.3 (c (n "mgit") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1sj2bzw7jcnzrshhlpighsb61k88822pax720nwddjawznwrbylb")))

(define-public crate-mgit-0.1.4 (c (n "mgit") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1fvgvikf7qz7q08zg4l7gdn775y8qhd94wn21c0hn0j9k5nl0xbk")))

(define-public crate-mgit-0.1.5 (c (n "mgit") (v "0.1.5") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zirzjzbn80l3x1c3xnarp06vfh6di51fx4a61gij73vdpp5ij4w")))

(define-public crate-mgit-0.1.6 (c (n "mgit") (v "0.1.6") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1xbh17ax8hhjlrbhx5ayhfkc97xj3hkj79qflwnxlpgk06lmw0bx")))

(define-public crate-mgit-0.1.7 (c (n "mgit") (v "0.1.7") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1r5z8z7r7gr6szcqf6zf5hc516fha1yy6my9cicr9gpszixkp1wg")))

(define-public crate-mgit-0.1.8 (c (n "mgit") (v "0.1.8") (d (list (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "05fp8fhmijcsvqibbcp7czavbpiira97vvn91qapn3p9x8v9nkpn")))

(define-public crate-mgit-0.1.9 (c (n "mgit") (v "0.1.9") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1ycjjviq18rql8bilpx6x3n16525k0a47giav98qyxvd1lpi4iw4")))

(define-public crate-mgit-0.1.10 (c (n "mgit") (v "0.1.10") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zg45rxlkc1mlk6mq86sk4wmm13ylavmkmqs5iwkgfzm3ic8lj57")))

