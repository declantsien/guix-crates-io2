(define-module (crates-io mg -s mg-settings) #:use-module (crates-io))

(define-public crate-mg-settings-0.0.1 (c (n "mg-settings") (v "0.0.1") (h "199ykcxi74w3j7cgy2nk0xh8xg0ypc77235x86myrka85hdnvs2w") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.2 (c (n "mg-settings") (v "0.0.2") (h "1l6g4g86kc5glas822j3vmjmcyhsy4cln92hz9b4jriyicd6ljgb") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.3 (c (n "mg-settings") (v "0.0.3") (h "0amgyli9xcbfihmbj5lgjqwfzw43w9f1gpziy9zad9y0pn34zifw") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.4 (c (n "mg-settings") (v "0.0.4") (h "1fq08qjnf19w8hxx8a0z6rwjkbfwpm467r7r13n8w0rxiwkzkgn8") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.5 (c (n "mg-settings") (v "0.0.5") (d (list (d (n "mg-settings-macros") (r "^0.0.1") (d #t) (k 0)))) (h "01gjcl8zllpxqlmawgzi1xkpirgcsgmj470maa8a9frxpqbj0x51") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.0 (c (n "mg-settings") (v "0.1.0") (d (list (d (n "mg-settings-macros") (r "^0.0.2") (d #t) (k 0)))) (h "0f3pfdf0qsfmzai3jqadlp1092i99gkl00n9riy9c9mwazja1j36") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.1 (c (n "mg-settings") (v "0.1.1") (d (list (d (n "mg-settings-macros") (r "^0.0.2") (d #t) (k 0)))) (h "0qcrkr0da4c4v0n6iizwhg7f3axlaiwmagzbj5ra9i6mlzpr8l49") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.2 (c (n "mg-settings") (v "0.1.2") (d (list (d (n "mg-settings-macros") (r "^0.1.0") (d #t) (k 0)))) (h "13r72n8rmgn34svj4v6j44lqqzn7rxiyr8ji5bpkyclrm2l8gd9k") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.3 (c (n "mg-settings") (v "0.1.3") (d (list (d (n "mg-settings-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0ps7iqn0g1mmil483rsly6kf0xdiyacc7xz71ssknhpslnj416yw") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.4 (c (n "mg-settings") (v "0.1.4") (d (list (d (n "mg-settings-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0143pjh3l6g49g4m9pfb12wrv328wjzk8wk9fb1hddkbciv83vwb") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.5 (c (n "mg-settings") (v "0.1.5") (d (list (d (n "mg-settings-macros") (r "^0.1.0") (d #t) (k 0)))) (h "17bksnarqpfci6r6v0mdq5asa15a5xb1xhjw1rs2ymq8y1802zh4") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.1.6 (c (n "mg-settings") (v "0.1.6") (d (list (d (n "mg-settings-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0p46gh2v1vsiz1pay59b7x3vqybmdzbzsqc38n2mzlxhbz2b277a") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.2.0 (c (n "mg-settings") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "mg-settings-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1c27z8l4fqkbclpibp416nlcbywnrfg8hcmch0z19mmr9as4h285") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.2.1 (c (n "mg-settings") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "mg-settings-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0riyzs2nfbkms3iagl5ras1cgchfz42qz7ls3si7xgvqw6p9q8m5") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.2.2 (c (n "mg-settings") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "mg-settings-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1ndp42d02cav7acjc2lv75kif0905vzdlh8ghc573w84a4blvwzy") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.3.0 (c (n "mg-settings") (v "0.3.0") (d (list (d (n "mg-settings-macros") (r "^0.2.0") (d #t) (k 0)))) (h "06a3qn9889lgl2ddlcx1scq8ksssscy2ph55bx03hy805krwccqq") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.3.1 (c (n "mg-settings") (v "0.3.1") (d (list (d (n "mg-settings-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0qlqm902x41yrskcjgnbpbqisyri1qp2mg7g296rjs8al3rx90g3") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.3.2 (c (n "mg-settings") (v "0.3.2") (d (list (d (n "mg-settings-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1n3f0rkhzvnm6irk4ckfasgl1mp1x5zs4s1ii68wakwgnxlhkzh5") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.4.0 (c (n "mg-settings") (v "0.4.0") (d (list (d (n "mg-settings-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0845z2j53gzbr6sfk788iyh6955lym8431yx8py9hpxb40dadmyb") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.4.1 (c (n "mg-settings") (v "0.4.1") (d (list (d (n "mg-settings-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0v5a9nwhnwv6cj4fyrbf5p7q4brxwzsn1yfxqccjgjwid6il94xg") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.4.2 (c (n "mg-settings") (v "0.4.2") (d (list (d (n "mg-settings-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1nsjgky4c9a74c51plb1b9z2l5w34cbiminq2d9zx82ki54k4pph") (f (quote (("nightly"))))))

(define-public crate-mg-settings-0.4.3 (c (n "mg-settings") (v "0.4.3") (d (list (d (n "mg-settings-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0779hq3wlx8a1lpr1fwlkk23yg3l5nbk49az9259r8gpfm8qyhy5") (f (quote (("nightly"))))))

