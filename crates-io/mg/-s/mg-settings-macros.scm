(define-module (crates-io mg -s mg-settings-macros) #:use-module (crates-io))

(define-public crate-mg-settings-macros-0.0.1 (c (n "mg-settings-macros") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1p140ksyqwpah9lgc9ndy9wg05wl73x0zxk9qgq0dh49ind6sxpv")))

(define-public crate-mg-settings-macros-0.0.2 (c (n "mg-settings-macros") (v "0.0.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1xqm4b14c6xpgjlcllf3izbjxp6fznmbk6czfnfhmv823abm8igd")))

(define-public crate-mg-settings-macros-0.0.3 (c (n "mg-settings-macros") (v "0.0.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "043lgm5cqn4v54i8b2pdpzzfk4bir7f07izv6iwxsawwpn5g7png")))

(define-public crate-mg-settings-macros-0.1.0 (c (n "mg-settings-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0zbqs8ci7z7mp0g59yvx3dik2jmf2icmvm0xm299xs59qfbhqp2m")))

(define-public crate-mg-settings-macros-0.1.1 (c (n "mg-settings-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1jxaf6ikm172kkwkd4lvsimw9p87cwacvax0lwviayfpg4ipz5g8")))

(define-public crate-mg-settings-macros-0.1.2 (c (n "mg-settings-macros") (v "0.1.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "15gaph1iq0xsqa9sqi7dv7i8r1q81yx3q8wnx0zinsp50wj4wfvp")))

(define-public crate-mg-settings-macros-0.1.3 (c (n "mg-settings-macros") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "10fwbxq9r1i041hqj6236bxjj5f1nm9hsffags8a3i5zbic360fb")))

(define-public crate-mg-settings-macros-0.1.4 (c (n "mg-settings-macros") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1kwainwjbsxm2gmccl2b6fpx8g89ay191ybngvsj6v8mdaxd6fny")))

(define-public crate-mg-settings-macros-0.2.0 (c (n "mg-settings-macros") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "16sar1ilz292vapns8zfzq6a94fgav2dyrj4wanbpnjx3inmcb93")))

(define-public crate-mg-settings-macros-0.2.1 (c (n "mg-settings-macros") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "mg-settings") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "083dvj6pr8ydxqm250ym0mngpl9w4q21fd71k3hh8agmsy76s3b4")))

(define-public crate-mg-settings-macros-0.2.2 (c (n "mg-settings-macros") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "mg-settings") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "163iib8razn5c0g2snyx48wnc5mq1mc68gzkg8j8k2w9mr1snair")))

(define-public crate-mg-settings-macros-0.3.0 (c (n "mg-settings-macros") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "mg-settings") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0qdc1ssaxjyygl66g4a7naar0gjl3hks2vfynqa50780ah2kl2jz")))

(define-public crate-mg-settings-macros-0.4.0 (c (n "mg-settings-macros") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0y5k1z0vjw96smv113m6ggvzkfpq4l7l4l5mswzshp7sfgsibvax")))

(define-public crate-mg-settings-macros-0.4.1 (c (n "mg-settings-macros") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mg-settings") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hn4i0kslfap72r6xiavmzhvgg44sq7a5p61q2380hf5l4zc94g7") (y #t)))

(define-public crate-mg-settings-macros-0.4.2 (c (n "mg-settings-macros") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mg-settings") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01lp5xllidhpgy84v7dc8a332fm2pq4fnglp3slwmk29rhyghnqn")))

