(define-module (crates-io mg rr mgrrs) #:use-module (crates-io))

(define-public crate-mgrrs-0.1.0 (c (n "mgrrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15427j7zwdsgr1gbw32xslba9yk3wjjvnbmwkjzg9kp9cq80y1dy")))

