(define-module (crates-io pn g2 png2t) #:use-module (crates-io))

(define-public crate-png2t-0.2.0 (c (n "png2t") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "human-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13vv13iqwb7lchkcd7gj09s208ar5iq8lyz4l06rfppky1g7yi2y")))

