(define-module (crates-io pn g2 png2wasm4src) #:use-module (crates-io))

(define-public crate-png2wasm4src-0.1.0 (c (n "png2wasm4src") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ybiwy93dgmbr4j61365g1y5c38aly0zfm0xhh7nf144flf8g0kf")))

