(define-module (crates-io pn gc pngchat) #:use-module (crates-io))

(define-public crate-pngchat-1.0.0 (c (n "pngchat") (v "1.0.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)))) (h "1sa1vcp7lfzs2f5syb8w412lxf3wacwpb9sh3snq23vkdz950pvj")))

(define-public crate-pngchat-1.0.1 (c (n "pngchat") (v "1.0.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)))) (h "0435zzprqv0pi27hls1ih1qbsvjsnd841219jyqpxicmdky3ccy5")))

