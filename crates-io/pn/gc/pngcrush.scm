(define-module (crates-io pn gc pngcrush) #:use-module (crates-io))

(define-public crate-pngcrush-0.1.0 (c (n "pngcrush") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "0fgw64pmak17640zan6i35dfmi2kvbqjpr373x0rpigmk0w6020k")))

(define-public crate-pngcrush-0.1.1 (c (n "pngcrush") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)))) (h "1bjn75l2ijfz0d2h91x7nfih0s2vfqbilq7rp79l6pr46408bdma")))

