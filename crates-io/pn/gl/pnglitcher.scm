(define-module (crates-io pn gl pnglitcher) #:use-module (crates-io))

(define-public crate-pnglitcher-0.1.0 (c (n "pnglitcher") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "13dqa4c9r6l4lz6qcscsizh2pqdrhhd0jfx8d6bhqpwgdn8d3nr5")))

(define-public crate-pnglitcher-0.1.1 (c (n "pnglitcher") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "0zr5f6ggvax414c8pgiybzyyrrbmxfq9iw37hgivx15fb6c5a6h2")))

(define-public crate-pnglitcher-0.1.2 (c (n "pnglitcher") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.1") (d #t) (k 0)))) (h "03f3pplrh5101ca0vjyqqj6qa6b700fpxz2m5r7z3wadpnhsjgnc")))

(define-public crate-pnglitcher-0.1.3 (c (n "pnglitcher") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.1") (d #t) (k 0)))) (h "1n9qyqhqi9yglx3hyssj5k7c8d2mhgqkzby00d9rh6x9biydnjvr")))

(define-public crate-pnglitcher-0.2.0 (c (n "pnglitcher") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "12j186vp7q70d1b7lwr4s33ri5lxwg6rymipbdm9vl96dfkg7pzz")))

(define-public crate-pnglitcher-0.3.0 (c (n "pnglitcher") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dycdzivr2pv69006lljwh0599p09w42vg48383ir09ki6pj3dxx")))

(define-public crate-pnglitcher-0.3.1 (c (n "pnglitcher") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17wynmshb0zzpazawvkv6jgjljka239z3rbdh16j7gzbypn3xhqc")))

