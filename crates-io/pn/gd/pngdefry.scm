(define-module (crates-io pn gd pngdefry) #:use-module (crates-io))

(define-public crate-pngdefry-0.0.1 (c (n "pngdefry") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1j4v1wwnwynkcwlfw83g4nj3njijkxzkyiiqcdvf3wad9vb9j2v9")))

(define-public crate-pngdefry-0.0.2 (c (n "pngdefry") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "0wqzhk0p04rfd9qqq0wwd5msf5g0qhsxq89y33zwhb7sbzzddimb")))

(define-public crate-pngdefry-0.0.3 (c (n "pngdefry") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "00rcf5s4jmfgysbzrhc908qy77a9ka4n2qmrqrvhg58yhh74daiv")))

(define-public crate-pngdefry-0.0.4 (c (n "pngdefry") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1b67x5bdisqjgfdwnn4qf3gxfkb2cj07p5m1jwg06gdy2y3b01b3")))

(define-public crate-pngdefry-0.0.5 (c (n "pngdefry") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1pzzk58b19sl4d91fhrzhagvxhyvhs9zszrd8ck5zjlwvl9nzgbb")))

(define-public crate-pngdefry-0.0.6 (c (n "pngdefry") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "14vzc391lzklbj79vvnmx9q44qi0cfww8qy5cfazw6hpwmkgdp6z")))

