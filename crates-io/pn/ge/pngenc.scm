(define-module (crates-io pn ge pngenc) #:use-module (crates-io))

(define-public crate-pngenc-0.1.0 (c (n "pngenc") (v "0.1.0") (d (list (d (n "atools") (r "^0.1.1") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.5") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.7") (d #t) (k 0)))) (h "08ps4a9q3ghx7rnpdv3vb982c5450jjd95xz1l5f31mqscxnpxx6")))

