(define-module (crates-io pn ge pngeq) #:use-module (crates-io))

(define-public crate-pngeq-0.1.0 (c (n "pngeq") (v "0.1.0") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "lodepng") (r "^0.8.1") (d #t) (k 0)))) (h "1mivv567xywjvpwx5k68qf0fvpfvlpx8lzil4i0wyh4l4han80rq")))

