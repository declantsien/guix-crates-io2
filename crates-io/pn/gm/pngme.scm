(define-module (crates-io pn gm pngme) #:use-module (crates-io))

(define-public crate-pngme-0.5.0 (c (n "pngme") (v "0.5.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "05ys6qahirmsmwkwy1a88riwhn41ydahrmnrx4632gmdzcjqgwbb")))

(define-public crate-pngme-0.5.1 (c (n "pngme") (v "0.5.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "12y0bnmll22myj0al60lqs7hbdsfr6y9ns91ak4019y1fi2pn45s")))

(define-public crate-pngme-0.5.2 (c (n "pngme") (v "0.5.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0s1zza7wapbqbf10rfrihxganqz12bibg5dx8ssb4qnrzm8365b6")))

