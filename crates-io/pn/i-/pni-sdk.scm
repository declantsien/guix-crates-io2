(define-module (crates-io pn i- pni-sdk) #:use-module (crates-io))

(define-public crate-pni-sdk-0.1.0 (c (n "pni-sdk") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0521szycafni9ys1nwx8lwpm8yw6vgpf16wv4m0in1z4574d9zgi") (f (quote (("reserved"))))))

