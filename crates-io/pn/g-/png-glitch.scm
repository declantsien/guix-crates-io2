(define-module (crates-io pn g- png-glitch) #:use-module (crates-io))

(define-public crate-png-glitch-0.1.0 (c (n "png-glitch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (f (quote ("zlib"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "025g1hgxzba9x5pb7vzz6xrhpmznmhs7d8m0xml355zm00rha2mb")))

(define-public crate-png-glitch-0.1.1 (c (n "png-glitch") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (f (quote ("zlib"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "031xvnh435s5y55v3pcn0gcyljxmip8wwfjbv2kb141apb8y82ya")))

(define-public crate-png-glitch-0.1.2 (c (n "png-glitch") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (f (quote ("zlib"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0cr6pmgwa15axdj8l4k899l8x22bldfr9rz1f5zm99g7h4sfcqfl")))

(define-public crate-png-glitch-0.1.3 (c (n "png-glitch") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fdeflate") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mdb3xvqc7v0j014q4swla5j0f5l2jvakfzpgm0k6r6y7mgxv9cf")))

(define-public crate-png-glitch-0.1.5 (c (n "png-glitch") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fdeflate") (r "^0.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "04609bwfl5ymickcyx8k70f8v5h8shw508q948q3phbz09s8dh61")))

