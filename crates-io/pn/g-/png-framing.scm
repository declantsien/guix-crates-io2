(define-module (crates-io pn g- png-framing) #:use-module (crates-io))

(define-public crate-png-framing-0.1.0 (c (n "png-framing") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^0.11") (d #t) (k 0)))) (h "0qzhly6z1qzkjgkvy2zby5730y0mldxlna0zamscbs5hqlzykf8v")))

(define-public crate-png-framing-0.2.0 (c (n "png-framing") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^0.11") (d #t) (k 0)))) (h "156d84ac1bymss411nlp26s1827svyr0frlyh8rvkmcsfyb9p6yl")))

(define-public crate-png-framing-0.2.1 (c (n "png-framing") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "lodepng") (r "^1.1") (d #t) (k 0)))) (h "1q1b9x0w0gvykdk7672zzpwz1b1pv0n9wdmg3m0g06g814irz2zy")))

(define-public crate-png-framing-0.3.0 (c (n "png-framing") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "lodepng") (r "^1.1") (d #t) (k 0)))) (h "0k6j95hs61pyvhvmqv560n1jprk6c46i1kznh6xkcdd0nspinavk")))

(define-public crate-png-framing-0.3.1 (c (n "png-framing") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "lodepng") (r "^1.1") (d #t) (k 0)))) (h "1gigha8jm9lcyzg4a1drj9kjjny2lisyhmscp8p9lnqzv20xvni2")))

(define-public crate-png-framing-0.4.0 (c (n "png-framing") (v "0.4.0") (d (list (d (n "framing") (r "^0.6") (d #t) (k 0)) (d (n "lodepng") (r "^1.1") (d #t) (k 0)))) (h "10nqy3ml9lk3a38a7acfywrcgdqg0503plw10i5ljkrfgwjhkvgy")))

(define-public crate-png-framing-0.5.0 (c (n "png-framing") (v "0.5.0") (d (list (d (n "framing") (r "^0.6") (d #t) (k 0)) (d (n "lodepng") (r "^1.1") (d #t) (k 0)))) (h "1lqz6y1p8s0czsw6x6ijfsrgdb7pl5p7jcpc18hckhkxl7b415c6")))

