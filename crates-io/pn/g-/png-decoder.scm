(define-module (crates-io pn g- png-decoder) #:use-module (crates-io))

(define-public crate-png-decoder-0.1.0 (c (n "png-decoder") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "00p0aiqfv5b514iqfq1w1zrgc1jqha3hbxz6z5pv63699k0j0kc2")))

(define-public crate-png-decoder-0.1.1 (c (n "png-decoder") (v "0.1.1") (d (list (d (n "crc32fast") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png"))) (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "0i2rc4bszzk0xazq9avjp2dyhjawwb9spbybvkr0j9jai3a96giy")))

