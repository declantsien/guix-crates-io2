(define-module (crates-io pn et pnet_sys_bandwhich_fork) #:use-module (crates-io))

(define-public crate-pnet_sys_bandwhich_fork-0.23.0 (c (n "pnet_sys_bandwhich_fork") (v "0.23.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1gys9knw6y9cp561ll6f9vpsh8f3dkgmq50djnp27zhs8gf5r701")))

