(define-module (crates-io pn et pnet_base) #:use-module (crates-io))

(define-public crate-pnet_base-0.20.0 (c (n "pnet_base") (v "0.20.0") (h "1dd70mgxahicqcrykplybjc2pdjgw7vclk3b79k35iv8nn81dy8y")))

(define-public crate-pnet_base-0.21.0 (c (n "pnet_base") (v "0.21.0") (h "1p3n98gav4m4mcw1d1z8n1x24gg5rbk06xs93pd8m226dz9vv3cl")))

(define-public crate-pnet_base-0.22.0 (c (n "pnet_base") (v "0.22.0") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "07w0z6wmhl0klkhzxkiabz5gj5xn5c69l6mrs9nl6xyc5z7qmwjd")))

(define-public crate-pnet_base-0.23.0 (c (n "pnet_base") (v "0.23.0") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "0lg0al8nq3w8clbbz561xpy1vbppjhm6r3x1ywicv5q89nwiin7p") (y #t)))

(define-public crate-pnet_base-0.24.0 (c (n "pnet_base") (v "0.24.0") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "0l9caq7z4vx4d9xc78i9ri9svvb6ywxxjfgnkl1pfy0m2ycd7bym") (y #t)))

(define-public crate-pnet_base-0.25.0 (c (n "pnet_base") (v "0.25.0") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "0ryrf4vx8kanxywc45a83j2179g2ly86k65778d90g6s3xmfr4xa")))

(define-public crate-pnet_base-0.23.1 (c (n "pnet_base") (v "0.23.1") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "0nx6nb6rgj6lidrdfrivklwl9c4fjli9pk7yp2ajdd5l59svrl3c")))

(define-public crate-pnet_base-0.26.0 (c (n "pnet_base") (v "0.26.0") (d (list (d (n "serde") (r "~1") (o #t) (k 0)) (d (n "serde_test") (r "~1") (d #t) (k 2)))) (h "1d8i670dy8hb5isjj75avhwry20hx90ji9m9n1kgl2i22mz5zkdp")))

(define-public crate-pnet_base-0.27.0 (c (n "pnet_base") (v "0.27.0") (d (list (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (k 0)) (d (n "serde_test") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1c4iwg2b8b2w2iz04njfhbvf78p64wf4wlnzqljcznvc6d7153rf")))

(define-public crate-pnet_base-0.27.1 (c (n "pnet_base") (v "0.27.1") (d (list (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (k 0)) (d (n "serde_test") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0r83bkr3qwzawrn3dla4x6bp9z11dpga7f1zbqrmj35v9myy9cws")))

(define-public crate-pnet_base-0.27.2 (c (n "pnet_base") (v "0.27.2") (d (list (d (n "serde") (r ">=1.0.0, <2.0.0") (o #t) (k 0)) (d (n "serde_test") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1v8bcbv9jh581ill77qjzzw2ay76pl781982yclj3xky96m8hijf")))

(define-public crate-pnet_base-0.28.0 (c (n "pnet_base") (v "0.28.0") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.126") (d #t) (k 2)))) (h "1f3g2p8zgmgsgyzjai4mv0nl22k1lxlrzz7zlsmdqlx7a7aqqj15")))

(define-public crate-pnet_base-0.29.0 (c (n "pnet_base") (v "0.29.0") (d (list (d (n "serde") (r "^1.0.126") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.126") (d #t) (k 2)))) (h "0f0mgl8gwbqsccp89604chryfqnxxngw2m9ikgvklfnl9c4gw1c2")))

(define-public crate-pnet_base-0.30.0 (c (n "pnet_base") (v "0.30.0") (d (list (d (n "no-std-net") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.126") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.126") (d #t) (k 2)))) (h "1i0h4p6ihvqyjxb3gsn727x69ymn4qg7nzr8qyyqjbw4dhf3921f") (f (quote (("std" "no-std-net/std") ("default" "std"))))))

(define-public crate-pnet_base-0.31.0 (c (n "pnet_base") (v "0.31.0") (d (list (d (n "no-std-net") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.137") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "1dh23hb5frx34krasrbcv1r1zjnx9afrkmjlv3adapwysj9sklzr") (f (quote (("std" "no-std-net/std") ("default" "std"))))))

(define-public crate-pnet_base-0.32.0 (c (n "pnet_base") (v "0.32.0") (d (list (d (n "no-std-net") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.137") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "0y4b6p3k4p7rzwi7m0kkgv47b7l1yq22nqvmk106f6z2li4f773f") (f (quote (("std" "no-std-net/std") ("default" "std"))))))

(define-public crate-pnet_base-0.33.0 (c (n "pnet_base") (v "0.33.0") (d (list (d (n "no-std-net") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.152") (d #t) (k 2)))) (h "0bpl72la4xv9pl9d1mwc31prrkgsvyqn9anc359g7ss4c4s4cbl7") (f (quote (("std" "no-std-net/std") ("default" "std"))))))

(define-public crate-pnet_base-0.34.0 (c (n "pnet_base") (v "0.34.0") (d (list (d (n "no-std-net") (r "^0.6.0") (k 0)) (d (n "serde") (r "^1.0.171") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.171") (d #t) (k 2)))) (h "0k2z3jh6vw99bwl0ckpsd142n4yiscza1bmj3b86i2xk7bxzck7y") (f (quote (("std" "no-std-net/std") ("default" "std"))))))

