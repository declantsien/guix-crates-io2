(define-module (crates-io pn et pnets) #:use-module (crates-io))

(define-public crate-pnets-0.1.0 (c (n "pnets") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "indexed_vec") (r "^1.2") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)))) (h "0vpj1420ff8zqc2dxhih2k7dcxdk4cpwz14bin7ay5gldms0l18l") (y #t)))

(define-public crate-pnets-0.1.1 (c (n "pnets") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "indexed_vec") (r "^1.2") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)))) (h "13a583s7xc6i15a7khp8f7rr1g0p3mrzq5mh0j76h2fgmsg3k4cq")))

(define-public crate-pnets-0.1.2 (c (n "pnets") (v "0.1.2") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "indexed_vec") (r "^1.2") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)))) (h "12pc0593yy87hh7r136b2bjlpvqvwvyxi97i9pqfyxw1la00n3g3")))

(define-public crate-pnets-0.1.3 (c (n "pnets") (v "0.1.3") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "indexed_vec") (r "^1.2") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)))) (h "0892a7xwjird37a59li63cmrdqkak37avzh0rlrk0czqw86jhrcn")))

