(define-module (crates-io pn et pnet_transport) #:use-module (crates-io))

(define-public crate-pnet_transport-0.20.0 (c (n "pnet_transport") (v "0.20.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pnet_base") (r "^0.20.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.20.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.20.0") (d #t) (k 0)))) (h "0npdsdylbv43npyssmilr44zpza8b03dvigkywq2461qc42yqzi5")))

(define-public crate-pnet_transport-0.21.0 (c (n "pnet_transport") (v "0.21.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.21.0") (d #t) (k 0)))) (h "1q6a32qdwkjz6ablvjn58fqafc4p5f0srdmrx44bnkppwa7i32jn")))

(define-public crate-pnet_transport-0.22.0 (c (n "pnet_transport") (v "0.22.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.22.0") (d #t) (k 0)))) (h "0kqh797bpb6rcs454h16z8sfl5wgxbgqizywk9lplj15yzf5bajz")))

(define-public crate-pnet_transport-0.23.0 (c (n "pnet_transport") (v "0.23.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.23.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.23.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.23.0") (d #t) (k 0)))) (h "1h2g671b7k6fgrjm54ws31590mln662lhpn8w4hg8k9n4gsiv1a0") (y #t)))

(define-public crate-pnet_transport-0.25.0 (c (n "pnet_transport") (v "0.25.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.25.0") (d #t) (k 0)))) (h "1dk7qqg65mk0ji0cabcmx7fjxhrwnzpbqivxkagvmnmmwypcqx8v")))

(define-public crate-pnet_transport-0.23.1 (c (n "pnet_transport") (v "0.23.1") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.23.1") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.23.1") (d #t) (k 0)))) (h "16ry354nz7ai3l80a1fiphyfr1mgl3p4qdpkg6argbch5d9jdb4i")))

(define-public crate-pnet_transport-0.26.0 (c (n "pnet_transport") (v "0.26.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "pnet_base") (r "^0.26.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.26.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.26.0") (d #t) (k 0)))) (h "044bqj2f709npl2pqyrka6sf8qb29b352g4yhi6ljh23advan4k7")))

(define-public crate-pnet_transport-0.27.1 (c (n "pnet_transport") (v "0.27.1") (d (list (d (n "libc") (r ">=0.2.39, <0.3.0") (d #t) (k 0)) (d (n "pnet_base") (r ">=0.27.1, <0.28.0") (d #t) (k 0)) (d (n "pnet_packet") (r ">=0.27.1, <0.28.0") (d #t) (k 0)) (d (n "pnet_sys") (r ">=0.27.1, <0.28.0") (d #t) (k 0)))) (h "00v0r5waq8dbxvi6kczl566gipzl9bddyflszkclkpm85pvy7cwl")))

(define-public crate-pnet_transport-0.27.2 (c (n "pnet_transport") (v "0.27.2") (d (list (d (n "libc") (r ">=0.2.39, <0.3.0") (d #t) (k 0)) (d (n "pnet_base") (r ">=0.27.2, <0.28.0") (d #t) (k 0)) (d (n "pnet_packet") (r ">=0.27.2, <0.28.0") (d #t) (k 0)) (d (n "pnet_sys") (r ">=0.27.2, <0.28.0") (d #t) (k 0)))) (h "1x8ad40xp16ckyfb38shq3571qbdd1cpn22qpm1yks3h5vyvssij")))

(define-public crate-pnet_transport-0.28.0 (c (n "pnet_transport") (v "0.28.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "pnet_base") (r "^0.28.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.28.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.28.0") (d #t) (k 0)))) (h "1kibgv0p8wcghq8saxfflqqzv9p08a8wjga433xcbg4ksqb2jawk")))

(define-public crate-pnet_transport-0.29.0 (c (n "pnet_transport") (v "0.29.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "pnet_base") (r "^0.29.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.29.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.29.0") (d #t) (k 0)))) (h "0yhvnh13sqv2jr4p75hlzs2k20am15p0mzw4kvhj57g1c5wjl3wm")))

(define-public crate-pnet_transport-0.30.0 (c (n "pnet_transport") (v "0.30.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "pnet_base") (r "^0.30.0") (k 0)) (d (n "pnet_packet") (r "^0.30.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.30.0") (d #t) (k 0)))) (h "0gcis8gdcv9w3rwhrspcj7lxmm78n9509xw305czfqvqyc3gzf3m") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

(define-public crate-pnet_transport-0.31.0 (c (n "pnet_transport") (v "0.31.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "pnet_base") (r "^0.31.0") (k 0)) (d (n "pnet_packet") (r "^0.31.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.31.0") (d #t) (k 0)))) (h "097iwz5rk4mxd5v3swkyq4bkxix1jaldpr1264dngxgiws2p2ngz") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

(define-public crate-pnet_transport-0.32.0 (c (n "pnet_transport") (v "0.32.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "pnet_base") (r "^0.32.0") (k 0)) (d (n "pnet_packet") (r "^0.32.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.32.0") (d #t) (k 0)))) (h "0siklfwm7f88m3i3s0cz32mhn6s4jf9c0hsdcdgyyl7szg041lma") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

(define-public crate-pnet_transport-0.33.0 (c (n "pnet_transport") (v "0.33.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "pnet_base") (r "^0.33.0") (k 0)) (d (n "pnet_packet") (r "^0.33.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.33.0") (d #t) (k 0)))) (h "0jcjmcy4dbxsnnqlawg7mfsvyxrb29gpbqbg5zi7xgpg9l71qgc1") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

(define-public crate-pnet_transport-0.34.0 (c (n "pnet_transport") (v "0.34.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pnet_base") (r "^0.34.0") (k 0)) (d (n "pnet_packet") (r "^0.34.0") (d #t) (k 0)) (d (n "pnet_sys") (r "^0.34.0") (d #t) (k 0)))) (h "1hrccv8q29772n3lhcgbwra3wkvhr35zqfirfhpywx79gm6y2dr6") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

