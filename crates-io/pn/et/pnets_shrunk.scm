(define-module (crates-io pn et pnets_shrunk) #:use-module (crates-io))

(define-public crate-pnets_shrunk-0.1.0 (c (n "pnets_shrunk") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indexed_vec") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnets") (r "^0.1") (d #t) (k 0)) (d (n "pnets_tina") (r "^0.1") (d #t) (k 0)))) (h "0qy57nd8rimsgrn8nn20p7yv9r175qjccmfvikxvdr9yr74dhvsl")))

