(define-module (crates-io pn et pnet_sys) #:use-module (crates-io))

(define-public crate-pnet_sys-0.20.0 (c (n "pnet_sys") (v "0.20.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1wwdfg9n2im2ajkqmql0sxlqlkd39ahpy381cr66mkbag6n4z0rv")))

(define-public crate-pnet_sys-0.21.0 (c (n "pnet_sys") (v "0.21.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1c5cp71cmfbi70bl4yclq91kgl10zwdsdzf3mdqc6fjwl04r2fwn")))

(define-public crate-pnet_sys-0.22.0 (c (n "pnet_sys") (v "0.22.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0xpicf1wmkraw9xfiq4zv3zb0g3dxdw3z09nhg70ni6chk52wav8")))

(define-public crate-pnet_sys-0.23.0 (c (n "pnet_sys") (v "0.23.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1syh1gmvjg52jwsw511i6fbisikpixzag4l1zh9kaxrqyw8q6bxb") (y #t)))

(define-public crate-pnet_sys-0.24.0 (c (n "pnet_sys") (v "0.24.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1akddi6krrk1rknmkb3x6rky5n7k850axjz9mqh3gdvabb4w4qxr") (y #t)))

(define-public crate-pnet_sys-0.25.0 (c (n "pnet_sys") (v "0.25.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0a9wybvwi1khmkmwcl7wqqabnwqp5ml48qfv85aqrjassyk83y42")))

(define-public crate-pnet_sys-0.23.1 (c (n "pnet_sys") (v "0.23.1") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0fcz7jzsxxq36zn2inbpyvi9vwmwqfkx9w4mn5g436a3j2kam69g")))

(define-public crate-pnet_sys-0.26.0 (c (n "pnet_sys") (v "0.26.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0b476fl39mh0f8wn3ryd22zipaqnj16y576p4mxib7v0a86dxw3k")))

(define-public crate-pnet_sys-0.27.0 (c (n "pnet_sys") (v "0.27.0") (d (list (d (n "libc") (r ">=0.2.39, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.8, <0.4.0") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fckgxlc53lp5y8snzg7pknb2rcpwfms2irv7kv4alg71qdhx6rs")))

(define-public crate-pnet_sys-0.27.1 (c (n "pnet_sys") (v "0.27.1") (d (list (d (n "libc") (r ">=0.2.39, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.8, <0.4.0") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ljxzfw2agba3pd7bgd8z84sm8i465rqm3ywwkqlfk9qfcc4b4bk")))

(define-public crate-pnet_sys-0.27.2 (c (n "pnet_sys") (v "0.27.2") (d (list (d (n "libc") (r ">=0.2.39, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.8, <0.4.0") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05gz5xixzvs5iw7a3l0r7sic7s44x8xmv9pqvzzs6wpdwz2f92bm")))

(define-public crate-pnet_sys-0.28.0 (c (n "pnet_sys") (v "0.28.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zzhaplmbx2f2rhzqam9im46mdgn2r3d1vhrznz1amgl1lmz78yr")))

(define-public crate-pnet_sys-0.29.0 (c (n "pnet_sys") (v "0.29.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01pdwxz719k9paa18bkijdcxsspi5rgh58lry1yw13x4wfjqg302")))

(define-public crate-pnet_sys-0.30.0 (c (n "pnet_sys") (v "0.30.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vqz8cypx7977220dydg8s9x1i7zqkxpb90f9ik2zhjmpkphaalf")))

(define-public crate-pnet_sys-0.31.0 (c (n "pnet_sys") (v "0.31.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0188m73c897gir17yqrhi6fb6csb1dwz66s2h9yj8vfx18gj73ij")))

(define-public crate-pnet_sys-0.32.0 (c (n "pnet_sys") (v "0.32.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16xy9661787gnpv1cx7xgbzh9miwkylg39139s3wgdb5m3mvwd2j")))

(define-public crate-pnet_sys-0.33.0 (c (n "pnet_sys") (v "0.33.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05izqzczq3004pignslk9ghzr24gzshpi4myfjiiin03525sbxzs")))

(define-public crate-pnet_sys-0.34.0 (c (n "pnet_sys") (v "0.34.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winsock2" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "004d28vbaxv3m55cd741mpjm34031c3p2dpp8kazcwxms7n0nz21")))

