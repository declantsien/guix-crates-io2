(define-module (crates-io pn et pnet_macros_bandwhich_fork) #:use-module (crates-io))

(define-public crate-pnet_macros_bandwhich_fork-0.23.0 (c (n "pnet_macros_bandwhich_fork") (v "0.23.0") (d (list (d (n "pnet_base_bandwhich_fork") (r "^0.23.0") (d #t) (k 2)) (d (n "pnet_macros_support_bandwhich_fork") (r "^0.23.0") (d #t) (k 2)) (d (n "regex") (r "1.0.*") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42") (d #t) (k 0)))) (h "0sg5ycksngbabifcqlm49azlsqsrjl4jvrzswgjb7bc26j0hma1h") (f (quote (("travis") ("default"))))))

