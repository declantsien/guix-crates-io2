(define-module (crates-io pn et pnets_tina) #:use-module (crates-io))

(define-public crate-pnets_tina-0.1.0 (c (n "pnets_tina") (v "0.1.0") (d (list (d (n "pnets") (r "^0.1") (d #t) (k 0)))) (h "1hacy7mdq966nnyyp4r5w1viqqf5nf2r9l7db7adqibmxq6kfnj5")))

(define-public crate-pnets_tina-0.1.1 (c (n "pnets_tina") (v "0.1.1") (d (list (d (n "pnets") (r "^0.1") (d #t) (k 0)))) (h "079c1d8ny8vdy2lannp76f46s8nrsmjd7z1w2mvvs8cnrlmifi41")))

