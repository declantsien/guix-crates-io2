(define-module (crates-io pn et pnet_macros_support) #:use-module (crates-io))

(define-public crate-pnet_macros_support-0.1.0 (c (n "pnet_macros_support") (v "0.1.0") (h "0m0nkiys9a1i7gjq7d4s5nyf2xsdwj245lgv1zkg8y87r3xarmgf")))

(define-public crate-pnet_macros_support-0.1.1 (c (n "pnet_macros_support") (v "0.1.1") (h "1lcql2rxlm7z1xaq366mrm10ras6agnfw412y328bx1bh79zz0fp") (f (quote (("travis"))))))

(define-public crate-pnet_macros_support-0.2.0 (c (n "pnet_macros_support") (v "0.2.0") (h "1wgsrkvl2fvpqxf9h5saimx9l03lrx00phgjplay9k3x2dag4ycc") (f (quote (("travis"))))))

(define-public crate-pnet_macros_support-0.20.0 (c (n "pnet_macros_support") (v "0.20.0") (d (list (d (n "pnet_base") (r ">= 0.20.0") (d #t) (k 0)))) (h "1z2kbsrpqarcwiwm7x01w32kiwn8n1pa37cgr0dr3m771rga0kyw")))

(define-public crate-pnet_macros_support-0.21.0 (c (n "pnet_macros_support") (v "0.21.0") (d (list (d (n "pnet_base") (r ">= 0.21.0") (d #t) (k 0)))) (h "1i3agydcsgzhr32kqqwi2wdv7v9ilfjs3xy3p3gl1wcd650p2azr")))

(define-public crate-pnet_macros_support-0.22.0 (c (n "pnet_macros_support") (v "0.22.0") (d (list (d (n "pnet_base") (r ">= 0.22.0") (d #t) (k 0)))) (h "1bl04xfyg3kxa1354jvkharc6zs5qgfa8b7yy837mhzgvln4ys44")))

(define-public crate-pnet_macros_support-0.23.0 (c (n "pnet_macros_support") (v "0.23.0") (d (list (d (n "pnet_base") (r ">= 0.23.0") (d #t) (k 0)))) (h "1slq90x703yskkvslyy2jvfa092ji6wzhpvga80wxf976sy8n5fn") (y #t)))

(define-public crate-pnet_macros_support-0.24.0 (c (n "pnet_macros_support") (v "0.24.0") (d (list (d (n "pnet_base") (r ">= 0.23.0") (d #t) (k 0)))) (h "0xcyajydwynl4a7rfa8yxgq293xcm1x8l4fqkb0k651cx60lj3af") (y #t)))

(define-public crate-pnet_macros_support-0.25.0 (c (n "pnet_macros_support") (v "0.25.0") (d (list (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)))) (h "0frq9biybfy609mqmk313xzv8rj5c931lvj89kviag3hp9a6hn2y")))

(define-public crate-pnet_macros_support-0.23.1 (c (n "pnet_macros_support") (v "0.23.1") (d (list (d (n "pnet_base") (r "^0.22.0") (d #t) (k 0)))) (h "1gfr44lvnrjiw4817kh4s6ghwlpj3b43zqd6qlskix1x0rfg29jk")))

(define-public crate-pnet_macros_support-0.26.0 (c (n "pnet_macros_support") (v "0.26.0") (d (list (d (n "pnet_base") (r "^0.26.0") (d #t) (k 0)))) (h "0nrw8qlgl2c1czyn753v6vnbhw87f306iclsvajd0rk7qg0cbyfs")))

(define-public crate-pnet_macros_support-0.27.0 (c (n "pnet_macros_support") (v "0.27.0") (d (list (d (n "pnet_base") (r ">=0.27.0, <0.28.0") (d #t) (k 0)))) (h "0ylfrplzvplvl6mz4ib64f28dvwq57n7nyw7fnlg0ai6rbw158ka")))

(define-public crate-pnet_macros_support-0.27.1 (c (n "pnet_macros_support") (v "0.27.1") (d (list (d (n "pnet_base") (r ">=0.27.1, <0.28.0") (d #t) (k 0)))) (h "0jwbnr6r2mnnpswzk55h2zx6hv19igfx5my9kb85sxiry2b7yaj0")))

(define-public crate-pnet_macros_support-0.27.2 (c (n "pnet_macros_support") (v "0.27.2") (d (list (d (n "pnet_base") (r ">=0.27.2, <0.28.0") (d #t) (k 0)))) (h "0v0z934sld79in6vd21fr6mxjhv5kws588hkm43bqw458nd2d6ab")))

(define-public crate-pnet_macros_support-0.28.0 (c (n "pnet_macros_support") (v "0.28.0") (d (list (d (n "pnet_base") (r "^0.28.0") (d #t) (k 0)))) (h "144gxxamd1904azjlrlky324mp9h5n7h9kmd0lq05aqcyc84wwfl")))

(define-public crate-pnet_macros_support-0.29.0 (c (n "pnet_macros_support") (v "0.29.0") (d (list (d (n "pnet_base") (r "^0.29.0") (d #t) (k 0)))) (h "17mjj5q1jg2mc0aypfsxy0nr4j7zsgqcmml4qn723fvam65sbazy")))

(define-public crate-pnet_macros_support-0.30.0 (c (n "pnet_macros_support") (v "0.30.0") (d (list (d (n "pnet_base") (r "^0.30.0") (d #t) (k 0)))) (h "1kd56c016d0dhm9jr5vyn2rm2632zdqjcqcyv0lw085klksk59dv") (f (quote (("std" "pnet_base/std") ("default" "std"))))))

(define-public crate-pnet_macros_support-0.31.0 (c (n "pnet_macros_support") (v "0.31.0") (d (list (d (n "pnet_base") (r "^0.31.0") (k 0)))) (h "0l07fkk9vq65fzfdm5s8vvm3rrqidbpx3bhkk5alk4vkqxfhkpl9")))

(define-public crate-pnet_macros_support-0.32.0 (c (n "pnet_macros_support") (v "0.32.0") (d (list (d (n "pnet_base") (r "^0.32.0") (k 0)))) (h "0riyf9af6jn7897917i63bk6shndkmwmgs5snx99125dzyxw7w34")))

(define-public crate-pnet_macros_support-0.33.0 (c (n "pnet_macros_support") (v "0.33.0") (d (list (d (n "pnet_base") (r "^0.33.0") (k 0)))) (h "1g42hl3yyw132iyvrla7cc48gb4d8516vcdq9s1xgvrj9w9k5ng6")))

(define-public crate-pnet_macros_support-0.34.0 (c (n "pnet_macros_support") (v "0.34.0") (d (list (d (n "pnet_base") (r "^0.34.0") (k 0)))) (h "0mhg0dzdxg6lkpx3z60c6nc39dkq9jz1n8hgmf77zlsb5yvjbagf")))

