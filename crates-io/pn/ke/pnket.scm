(define-module (crates-io pn ke pnket) #:use-module (crates-io))

(define-public crate-pnket-0.3.1 (c (n "pnket") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zstd") (r "^0.4") (d #t) (k 0)))) (h "1s9g5nlxq61ykyald76vfx2ilb1s876bl6c2a3v8rx7mj7qlcq04")))

