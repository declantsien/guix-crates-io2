(define-module (crates-io pn -d pn-dcp-macro) #:use-module (crates-io))

(define-public crate-pn-dcp-macro-0.1.0 (c (n "pn-dcp-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "003rshk075dg9s8pa62rhbmskxhg1r4270lgn396kffxmbq33nsq")))

(define-public crate-pn-dcp-macro-0.3.0 (c (n "pn-dcp-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1blqc3iscs2d9j0nal84n7ifbi3y24klfzmmpndymnf0yh7wzaq9")))

