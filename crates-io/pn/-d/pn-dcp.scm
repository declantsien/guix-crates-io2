(define-module (crates-io pn -d pn-dcp) #:use-module (crates-io))

(define-public crate-pn-dcp-0.1.0 (c (n "pn-dcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "pn-dcp-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (d #t) (k 2)) (d (n "pnet_macros") (r "^0.29.0") (d #t) (k 0)) (d (n "pnet_macros") (r "^0.29.0") (d #t) (k 2)) (d (n "pnet_macros_support") (r "^0.29.0") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.29.0") (d #t) (k 2)))) (h "0v50qcmnlckgq9qsm6b0vmd8igkg8187zjbv4yvmk6wv445vz2k0")))

(define-public crate-pn-dcp-0.2.0 (c (n "pn-dcp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "pn-dcp-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 2)))) (h "0p0cz0p54nxsrl0z0sczz1iajrkrd2gq5rnycgjank5awp1n22fq")))

(define-public crate-pn-dcp-0.3.0 (c (n "pn-dcp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "pn-dcp-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (f (quote ("std"))) (d #t) (k 0)))) (h "138gyfxalfdys777n2nx3d66qs0s47k75dhj7w19zg1aqgb0x79i")))

