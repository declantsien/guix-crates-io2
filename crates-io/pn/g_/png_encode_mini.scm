(define-module (crates-io pn g_ png_encode_mini) #:use-module (crates-io))

(define-public crate-png_encode_mini-0.1.0 (c (n "png_encode_mini") (v "0.1.0") (h "13xrc3rfzqsbyncz85rnb75gkzk93cqpi5wzwh250ppb8az1i7s0")))

(define-public crate-png_encode_mini-0.1.1 (c (n "png_encode_mini") (v "0.1.1") (h "0nwxvxywibflpl1pmp4khbm533kc24yn47dbqp2yskicdygiz85v")))

(define-public crate-png_encode_mini-0.1.2 (c (n "png_encode_mini") (v "0.1.2") (h "0rpn91i8rkkgs607aq1fnkvali8b8zf56b2wj6cryrqsx9ifjshi")))

