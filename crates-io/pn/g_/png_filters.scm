(define-module (crates-io pn g_ png_filters) #:use-module (crates-io))

(define-public crate-png_filters-0.1.0 (c (n "png_filters") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "0bm6n1f2c3gp5cjan8r85ycj7h2whcg7bbhryq59xwc1dig5rgkx")))

(define-public crate-png_filters-0.1.1 (c (n "png_filters") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "19gsw6yha79b4dsm88lm1w7bddcgcz5lwqc62ck3cnjp2y15arij")))

(define-public crate-png_filters-0.1.2 (c (n "png_filters") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "1l6xqlj1csmhklicm928izq26am335jn5izxsmllx8mdkfw8dbdx")))

