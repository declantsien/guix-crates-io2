(define-module (crates-io pn g_ png_ect) #:use-module (crates-io))

(define-public crate-png_ect-1.0.0 (c (n "png_ect") (v "1.0.0") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "016g1h1hl3g0ikdf47arl1rzlb2ih33yc67gl5ca4bsah79mgx3z")))

