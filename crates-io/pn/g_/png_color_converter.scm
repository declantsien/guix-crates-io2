(define-module (crates-io pn g_ png_color_converter) #:use-module (crates-io))

(define-public crate-png_color_converter-0.1.0 (c (n "png_color_converter") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "png") (r "^0.17.11") (d #t) (k 0)))) (h "0rlag4cfha0l152fhn27nhsxfwag1fl7y235gn6vcf3jdfsj54ha")))

