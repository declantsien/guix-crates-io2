(define-module (crates-io pn g_ png_pong) #:use-module (crates-io))

(define-public crate-png_pong-0.0.1 (c (n "png_pong") (v "0.0.1") (d (list (d (n "deflate") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "inflate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.6") (d #t) (k 0)))) (h "163d258fb4cisxx1gp5k4fdc57cv6npm27n4wkxwfsib34f5s28f") (f (quote (("flate" "deflate" "inflate") ("default" "flate"))))))

(define-public crate-png_pong-0.0.2 (c (n "png_pong") (v "0.0.2") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.6") (d #t) (k 0)))) (h "13jrrilx8yr5pxqswah4wdsx459wkr78f6q3a43kqvjqbkvy209k") (f (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.1.0 (c (n "png_pong") (v "0.1.0") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.7") (d #t) (k 0)))) (h "0hyy2mnn6mbpkqx8kabxwskjhiah7gqnpyj3gkr07acziyqpl0ll") (f (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.2.0 (c (n "png_pong") (v "0.2.0") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.10") (d #t) (k 0)))) (h "1fxnidpnr452cfl0nvs7dnhy7dxs96z90d1bazrq2cmbj56dswfn") (f (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.2.1 (c (n "png_pong") (v "0.2.1") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.10") (d #t) (k 0)))) (h "01nn6fysg0z71xhspp2bwndxip3n4xja75nkjwiaddzh3bhx7fdl") (f (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.2.2 (c (n "png_pong") (v "0.2.2") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.10") (d #t) (k 0)))) (h "028vngx7sapdri2vyha5a6hj9izvagmv8yaasxn0krn41xnsm7kl") (f (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.3.0 (c (n "png_pong") (v "0.3.0") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.11") (d #t) (k 0)))) (h "1w1kd9rx9rvk3p1jhc4i887v536inv24g7fsvsa4l1ab4x50n558") (f (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.4.0 (c (n "png_pong") (v "0.4.0") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.12") (d #t) (k 0)))) (h "1plp9z2b03rmzjyigklnyb902jy9cpxd1sbbkba29w69rx0qn022") (f (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.5.0 (c (n "png_pong") (v "0.5.0") (d (list (d (n "miniz_oxide") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "11b586jf43fhmqml8zakzzzrjw95jdxl03x9ildn3bgzp5kaji55") (f (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.6.0 (c (n "png_pong") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "07b7s76lg5r3pd0r22hh9w5l32y6n991yfrp8s4wji7315swrkj8") (f (quote (("default"))))))

(define-public crate-png_pong-0.7.0 (c (n "png_pong") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "1qymhfm1bvq54nfykhhizqc2li70vmdxbspv68z6qhdgajj6pxyl") (f (quote (("default"))))))

(define-public crate-png_pong-0.8.0 (c (n "png_pong") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "1zvh5cwwwwfajnq05r86jyqvqmsmw80gmqh3kq3p7m0v118131bm") (f (quote (("default"))))))

(define-public crate-png_pong-0.8.1 (c (n "png_pong") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "14nk2sfs6gyqgpz4hsq274sjx2rsww0b2gbihnglh7f9d9c9vajs") (f (quote (("default"))))))

(define-public crate-png_pong-0.8.2 (c (n "png_pong") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)))) (h "13vpcxnr2dcijjl08yv469sl8xd72n82p5w1iv8zw1lll4fnrdrj") (f (quote (("default"))))))

(define-public crate-png_pong-0.9.0 (c (n "png_pong") (v "0.9.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.7") (f (quote ("simd"))) (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "simd-adler32") (r "^0.3") (d #t) (k 0)))) (h "01ksn6graaas3p2q546fdhdh41x8fg5ha48wify2f2mw1k4k35na") (f (quote (("default")))) (r "1.77")))

(define-public crate-png_pong-0.9.1 (c (n "png_pong") (v "0.9.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.7") (f (quote ("simd"))) (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "simd-adler32") (r "^0.3") (d #t) (k 0)))) (h "1zgzkbz11plxk8gqyix5q4lll8bqbak8b6i3syj36q6k61rmmaix") (f (quote (("default")))) (r "1.70")))

