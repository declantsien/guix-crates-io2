(define-module (crates-io nd le ndless-macros) #:use-module (crates-io))

(define-public crate-ndless-macros-0.1.0 (c (n "ndless-macros") (v "0.1.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-static-vars") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0sbd9niyjbskxzjd5i37mysl5vcxjhwyphz7011jip80qrrv50rr")))

(define-public crate-ndless-macros-0.1.1 (c (n "ndless-macros") (v "0.1.1") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-static-vars") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "075mshf9qg6pm625l85j7abi15934fawnpfzagxwrxydkvcw8crm")))

(define-public crate-ndless-macros-0.2.1 (c (n "ndless-macros") (v "0.2.1") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-static-vars") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vzcli3l6gc5jfr1sc81knqk8pzwq0dcj0vc7y3d646wz6jybs1x")))

(define-public crate-ndless-macros-0.3.0 (c (n "ndless-macros") (v "0.3.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-static-vars") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1c1xwmk5bzqa7maq4l4lfkrhkw5vk9gqngax3r49h2mzzfyjkj9k")))

(define-public crate-ndless-macros-0.4.0 (c (n "ndless-macros") (v "0.4.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-static-vars") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0p17xdigxwlllfjms8m89djpgyqhxwfky31b81z9c6mdah56k1ay")))

