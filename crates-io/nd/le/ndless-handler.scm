(define-module (crates-io nd le ndless-handler) #:use-module (crates-io))

(define-public crate-ndless-handler-0.1.0 (c (n "ndless-handler") (v "0.1.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "nspire") (r "^0.3.0") (d #t) (k 0)))) (h "0gz3hv8n9rkrvy2zyh9mwx35vbsahih7lmv3wkmxvk3wlrfdffmh") (f (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-eh-personality") ("disable-allocator"))))))

(define-public crate-ndless-handler-0.1.1 (c (n "ndless-handler") (v "0.1.1") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "nspire") (r "^0.3.0") (d #t) (k 0)))) (h "1pfsa9a1n6dccan5x8pyp340h275jwvaym60kzla89pv2ac2bywk") (f (quote (("disable_ctype_ptr") ("disable-panic-handler") ("disable-oom-handler") ("disable-eh-personality") ("disable-allocator"))))))

(define-public crate-ndless-handler-0.1.2 (c (n "ndless-handler") (v "0.1.2") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "nspire") (r "^0.3.0") (d #t) (k 0)))) (h "11mkyz1qaz5gpi320nmwj5czj97mlfhafc69pp6msk3czv07z9vq") (f (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-fake-atomics") ("disable-eh-personality") ("disable-ctype-ptr") ("disable-allocator"))))))

(define-public crate-ndless-handler-0.2.0 (c (n "ndless-handler") (v "0.2.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.0") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.2.0") (d #t) (k 0)))) (h "178r30vv95iail3ry8ifs03fjjpgiglk4bq6b77gv8019mqdgvgy") (f (quote (("panic-handler") ("oom-handler") ("eh-personality") ("default" "allocator" "oom-handler" "panic-handler" "eh-personality" "ctype-ptr") ("ctype-ptr") ("allocator"))))))

(define-public crate-ndless-handler-0.2.1 (c (n "ndless-handler") (v "0.2.1") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.2") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0wr17dhr09ck1l59h3d9l0z6w516wj9wiyq699fsxj85gczhxdx0") (f (quote (("panic-handler") ("oom-handler") ("eh-personality") ("default" "allocator" "oom-handler" "panic-handler" "eh-personality") ("ctype-ptr") ("allocator"))))))

(define-public crate-ndless-handler-0.3.0 (c (n "ndless-handler") (v "0.3.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.6") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0s34kl69nmqpdhkx6pch8cr587p8qizd0mdbrmgjvirwfy7h1gxv") (f (quote (("panic-handler") ("oom-handler") ("lang-start") ("eh-personality") ("default" "allocator" "oom-handler" "panic-handler" "eh-personality" "lang-start") ("ctype-ptr") ("allocator"))))))

