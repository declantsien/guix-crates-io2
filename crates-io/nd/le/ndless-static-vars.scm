(define-module (crates-io nd le ndless-static-vars) #:use-module (crates-io))

(define-public crate-ndless-static-vars-1.0.0 (c (n "ndless-static-vars") (v "1.0.0") (h "03qxbk9i3yiwsydqv3nfmxyck04ysjafpihgb69cfdm07hpsdmi3")))

(define-public crate-ndless-static-vars-2.0.0 (c (n "ndless-static-vars") (v "2.0.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "1b7kjyisd12fpxd8z1gf13zpnnsfzavd43x80dap6kdszcl8i91v")))

(define-public crate-ndless-static-vars-2.0.1 (c (n "ndless-static-vars") (v "2.0.1") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "193l3w82c8x96ijpx9jk932c5wwxwcpkxfyv01k1rvsjmpvm1lrm")))

(define-public crate-ndless-static-vars-2.1.0 (c (n "ndless-static-vars") (v "2.1.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "0prdyp6p657cyghdj2g7vrzzpzjcjlli02xk0fk1daka666fnyca")))

