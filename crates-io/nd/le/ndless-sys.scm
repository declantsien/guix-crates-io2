(define-module (crates-io nd le ndless-sys) #:use-module (crates-io))

(define-public crate-ndless-sys-0.1.0 (c (n "ndless-sys") (v "0.1.0") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "08lhkfpy8x98s77pd2pj73i79mxz3g08xrv3bdsfvr3irvryldy4") (y #t)))

(define-public crate-ndless-sys-0.1.1 (c (n "ndless-sys") (v "0.1.1") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "03jb8jaapd0f7hg8vwarmxbczhfcfm4y0iq2zn0iz1f6xmxhv3ab")))

(define-public crate-ndless-sys-0.1.2 (c (n "ndless-sys") (v "0.1.2") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "0z775igkkfpzvix6zg8qnhggmr3g4l405kx3clg3mwzfpkdq4mqc")))

(define-public crate-ndless-sys-0.1.3 (c (n "ndless-sys") (v "0.1.3") (d (list (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "1ygnyr5fyi3ixbzakk6iw4bi7dv3p0l8rvc32samly3q2m5yhvm9")))

(define-public crate-ndless-sys-0.1.4 (c (n "ndless-sys") (v "0.1.4") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "1qnxf7isvg61lhi2g9dibm0j72z34x4pz21fx3zbfdb9v572xqv5") (y #t)))

(define-public crate-ndless-sys-0.2.0 (c (n "ndless-sys") (v "0.2.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)))) (h "1isnl18s47jizk994jr2mp2rp14smr87cdi1ifsffsfjil5gn804")))

