(define-module (crates-io nd le ndless-freetype) #:use-module (crates-io))

(define-public crate-ndless-freetype-0.1.0 (c (n "ndless-freetype") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1.2") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "embedded-freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (k 0)) (d (n "ndless-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "nspire") (r "^0.5.1") (d #t) (k 0)))) (h "1fr1lrzrdf8x5sjkh1vpjmfxlw8nk858287mmisanh2qzr0yb7rb")))

(define-public crate-ndless-freetype-0.1.1 (c (n "ndless-freetype") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1.2") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "embedded-freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (k 0)) (d (n "ndless") (r "^0.5.2") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1jb90szsp0gzzdzxymj90r6jc2qbykxaq39vs12rjdnyx62ck80z")))

(define-public crate-ndless-freetype-0.1.2 (c (n "ndless-freetype") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "embedded-freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (k 0)) (d (n "ndless-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0iwknj1kyvl8y18h1nm6crsrzp45ydpi1nb89b7rppy4yfcvb8qh")))

(define-public crate-ndless-freetype-0.1.3 (c (n "ndless-freetype") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-freetype-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (k 0)) (d (n "ndless-sys") (r "^0.2.0") (d #t) (k 0)))) (h "104x8109ishkkza6hxjsq40ky867q7b6j1pibim49x1ljf28s80d")))

