(define-module (crates-io nd le ndless-sdl) #:use-module (crates-io))

(define-public crate-ndless-sdl-0.1.0 (c (n "ndless-sdl") (v "0.1.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "nspire") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "rand") (r "^0.6.5") (k 0)))) (h "0nczhvi8r2rja6sfb6jk7yprhzws3g2h0d0anssdsmdxvw7bawv9")))

(define-public crate-ndless-sdl-0.1.1 (c (n "ndless-sdl") (v "0.1.1") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "nspire") (r "^0.5.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "rand") (r "^0.6.5") (k 0)))) (h "13d934ch2ddmnn6gl96x6cyc8504v3fp73jjdn8zw1d0vappagqx")))

(define-public crate-ndless-sdl-0.1.2 (c (n "ndless-sdl") (v "0.1.2") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1h0y2msyfb3w2icdwwpnmh40xdi6vhyvdvh773sv00sskkqwx45z")))

(define-public crate-ndless-sdl-0.1.3 (c (n "ndless-sdl") (v "0.1.3") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "10yk94qfhv8p32cjzhsmgy1z52xfa1124926s87pm3ji8z59gib3")))

(define-public crate-ndless-sdl-0.1.4 (c (n "ndless-sdl") (v "0.1.4") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "01dz3i7b25liqjbqwpxd8rwwxg3gh8m2m3yhl6g4flsh4i5m5l0n")))

(define-public crate-ndless-sdl-0.1.5 (c (n "ndless-sdl") (v "0.1.5") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0bhb09jjdxla1xng93bivivc2gyx80awcigrwzpc0cfv7mxjb101")))

(define-public crate-ndless-sdl-0.1.6 (c (n "ndless-sdl") (v "0.1.6") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "130dfk8w5iniilkg4712l0m3l1lc02ramj2chjiyjihapp3q3p61") (f (quote (("disable_ctype_ptr"))))))

(define-public crate-ndless-sdl-0.1.7 (c (n "ndless-sdl") (v "0.1.7") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.0") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "191nx7qaryxfd7jhy2qj8k22i7nm6cib630vq5wk805zz408ldqb")))

(define-public crate-ndless-sdl-0.1.8 (c (n "ndless-sdl") (v "0.1.8") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless") (r "^0.6.1") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "1j7dzvfv3qh0cgjlgm2cdqpip9sljgac7hbfxcw8613zi2h5nk01")))

(define-public crate-ndless-sdl-0.1.9 (c (n "ndless-sdl") (v "0.1.9") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "ndless") (r "^0.8.2") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "14fg93lnda66n284g1lg71wqj76hi752pb7ddrbi2hcblv50vsdx")))

(define-public crate-ndless-sdl-0.2.0 (c (n "ndless-sdl") (v "0.2.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "ndless") (r "^0.8.2") (d #t) (k 0)) (d (n "ndless-freetype") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1s54sbzdfl7krpb5hji7346vbs4pamggc6cjm76n88d7wjbs65nz")))

