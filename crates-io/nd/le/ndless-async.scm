(define-module (crates-io nd le ndless-async) #:use-module (crates-io))

(define-public crate-ndless-async-0.1.0 (c (n "ndless-async") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.5") (f (quote ("alloc" "async-await-macro"))) (k 0)) (d (n "ignore-result") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.6") (d #t) (k 0)))) (h "0l7m85a18ws9052hzn83qb36ddfkcrx8g9l842nan1k7m5jp43vz")))

(define-public crate-ndless-async-0.1.1 (c (n "ndless-async") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.5") (f (quote ("alloc" "async-await-macro"))) (k 0)) (d (n "ignore-result") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.6") (d #t) (k 0)))) (h "1zy19v5wi6cfw3s2sl2bikj3cvv7bdqyhs3glfb1q8l4bzh2hipw")))

(define-public crate-ndless-async-0.1.2 (c (n "ndless-async") (v "0.1.2") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (f (quote ("alloc"))) (k 0)) (d (n "futures-util") (r "^0.3.5") (f (quote ("alloc" "async-await-macro"))) (k 0)) (d (n "ignore-result") (r "^0.2.0") (d #t) (k 0)) (d (n "ndless") (r "^0.8.6") (d #t) (k 0)))) (h "1vyclfxxbvchl6c7z60b8ld143vb2196985a47p0iy9n47944iwj")))

