(define-module (crates-io nd ma ndmath) #:use-module (crates-io))

(define-public crate-ndmath-0.1.0 (c (n "ndmath") (v "0.1.0") (h "13vrdc2j8yracxxdq3fdbnds9xlmq2inn1anig01cy0m805wqi4z")))

(define-public crate-ndmath-0.1.1 (c (n "ndmath") (v "0.1.1") (h "0d6sxvifjhg5gpfj8r213y5ir856k8l81lgzhw3xjqj2kjpi74fv")))

(define-public crate-ndmath-0.1.2 (c (n "ndmath") (v "0.1.2") (h "0fvd1ldqmwr0gg3jrpnz7l4dl37hx33zhkchz0iw0bs2493xc0si")))

