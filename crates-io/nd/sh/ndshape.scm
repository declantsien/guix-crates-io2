(define-module (crates-io nd sh ndshape) #:use-module (crates-io))

(define-public crate-ndshape-0.1.0 (c (n "ndshape") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0ygrmjvsrcq2sl6k2c3wrjyvnr536izia6iady2m318fyblppfyh")))

(define-public crate-ndshape-0.2.0 (c (n "ndshape") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "16dh2i0xxqfcxla9m76gsskk4d4bmihrhlk7wh277rkysj3xjgld")))

(define-public crate-ndshape-0.3.0 (c (n "ndshape") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "033hibaajwvq9gbbz5c6b1m6hl3hq03yjvn2dmzy4dynd9ccwnwp")))

