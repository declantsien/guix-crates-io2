(define-module (crates-io nd k- ndk-logger) #:use-module (crates-io))

(define-public crate-ndk-logger-0.0.1 (c (n "ndk-logger") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1d7lg0zcl7py721ffdqgy4qci48cdwbsv5xxr6v9nr647b3idf7l") (y #t)))

(define-public crate-ndk-logger-0.0.2 (c (n "ndk-logger") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1p1ni2p8kgxbawly14945pg62pcgxfcls87f81vcp5fn18hp0qa9")))

