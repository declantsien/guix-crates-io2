(define-module (crates-io nd k- ndk-tool) #:use-module (crates-io))

(define-public crate-ndk-tool-0.1.0 (c (n "ndk-tool") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "ndk-build") (r "^0.4.3") (d #t) (k 0)))) (h "1bx687mgq9kza22zgzki1cfqpivlayvzym8gcyiafshkc517swjy")))

