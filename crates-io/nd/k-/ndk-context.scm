(define-module (crates-io nd k- ndk-context) #:use-module (crates-io))

(define-public crate-ndk-context-0.1.0 (c (n "ndk-context") (v "0.1.0") (h "112q689zc4338xmj55a8nxdlkjmrw34s3xkpy3l1zqiphv35qg2f")))

(define-public crate-ndk-context-0.1.1 (c (n "ndk-context") (v "0.1.1") (h "12sai3dqsblsvfd1l1zab0z6xsnlha3xsfl7kagdnmj3an3jvc17")))

