(define-module (crates-io nd k- ndk-macro) #:use-module (crates-io))

(define-public crate-ndk-macro-0.1.0 (c (n "ndk-macro") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2fg6cy43kh1zxvj97prl3ximvj38clpfjch7fymjrna3vqib11") (f (quote (("logger") ("default"))))))

(define-public crate-ndk-macro-0.2.0 (c (n "ndk-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (f (quote ("full"))) (d #t) (k 0)))) (h "07a8vjr4fpksssgp453bf82n73i4i17yj1lvbgvd0964glqcdl85") (f (quote (("logger") ("default"))))))

(define-public crate-ndk-macro-0.3.0 (c (n "ndk-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0v3sxc11kq3d5vdwfml62l7y5dr0flsf6kp5xid9sbv7qh0arxqd") (f (quote (("logger") ("default"))))))

