(define-module (crates-io nd k- ndk-sys) #:use-module (crates-io))

(define-public crate-ndk-sys-0.1.0 (c (n "ndk-sys") (v "0.1.0") (h "0zc85pqm3q6lbq1nrly5mk74hh54nrrcr6n7j7nsbarlm6n20a1b") (f (quote (("test") ("rustdoc"))))))

(define-public crate-ndk-sys-0.1.1 (c (n "ndk-sys") (v "0.1.1") (h "02xafh4z7kzs1bz0bivzsvavyy90gygxsax42983a4ljgmj4ya1x") (f (quote (("test") ("rustdoc") ("media") ("bitmap") ("aaudio")))) (y #t)))

(define-public crate-ndk-sys-0.2.0 (c (n "ndk-sys") (v "0.2.0") (h "0zigwnnagwnm276q49yli10c8dnkg5vvrjb6pcr6f26aixf560fy") (f (quote (("test") ("rustdoc") ("media") ("bitmap") ("aaudio"))))))

(define-public crate-ndk-sys-0.2.1 (c (n "ndk-sys") (v "0.2.1") (h "13c68a217ag3k18vlffpcj2qjfinchxxchzlwnsp075v7p5j4jf4") (f (quote (("test") ("media") ("bitmap") ("aaudio"))))))

(define-public crate-ndk-sys-0.2.2 (c (n "ndk-sys") (v "0.2.2") (h "08915adplysmvx0ha12if1v7zxzx82xgj3nnmiddkm8aq9sdvg71") (f (quote (("test") ("media") ("bitmap") ("aaudio"))))))

(define-public crate-ndk-sys-0.3.0 (c (n "ndk-sys") (v "0.3.0") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "15zsq4p6k5asf4mc0rknd8cz9wxrwvi50qdspgf87qcfgkknlnkf") (f (quote (("test") ("media") ("bitmap") ("aaudio"))))))

(define-public crate-ndk-sys-0.4.0 (c (n "ndk-sys") (v "0.4.0") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0ilhw848rvsqkx32bdl746bmjrmdpl4fba000aavzi9yqv4kxn11") (f (quote (("test") ("media") ("bitmap") ("audio"))))))

(define-public crate-ndk-sys-0.4.0+25.0.8775105 (c (n "ndk-sys") (v "0.4.0+25.0.8775105") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0niqaq7kwq7z9caq6p5452hg2ncpmir39x7sk79ki3fsnxadskgp") (f (quote (("test") ("media") ("bitmap") ("audio")))) (y #t) (r "1.60")))

(define-public crate-ndk-sys-0.4.1+23.1.7779620 (c (n "ndk-sys") (v "0.4.1+23.1.7779620") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "18z5xsnrnpq65aspavb8cg925m3scs8hb1b9a2n2q8xxb3lsmwiw") (f (quote (("test") ("media") ("bitmap") ("audio"))))))

(define-public crate-ndk-sys-0.5.0-beta.0+25.2.9519653 (c (n "ndk-sys") (v "0.5.0-beta.0+25.2.9519653") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0hj0cbxi3zl7q4qvd91n0qm6cwyzcyqv3yj1247hv8fbflvn0f7z") (f (quote (("test") ("media") ("bitmap") ("audio")))) (r "1.60")))

(define-public crate-ndk-sys-0.5.0+25.2.9519653 (c (n "ndk-sys") (v "0.5.0+25.2.9519653") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "14bnxww0f17xl8pyn6j5kpkl98snjl9lin8i7qv4zzb0vmlnf6cc") (f (quote (("test") ("sync") ("media") ("bitmap") ("audio")))) (r "1.60")))

(define-public crate-ndk-sys-0.6.0+11769913 (c (n "ndk-sys") (v "0.6.0+11769913") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0wx8r6pji20if4xs04g73gxl98nmjrfc73z0v6w1ypv6a4qdlv7f") (f (quote (("test") ("sync") ("nativewindow") ("media") ("bitmap") ("audio")))) (r "1.60")))

