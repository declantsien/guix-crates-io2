(define-module (crates-io nd _m nd_matrix) #:use-module (crates-io))

(define-public crate-nd_matrix-0.1.0 (c (n "nd_matrix") (v "0.1.0") (h "1v994hms1gn11v1d4ci2iwjm6i82vfn35agyxarfammylmzf82k8")))

(define-public crate-nd_matrix-0.2.0 (c (n "nd_matrix") (v "0.2.0") (h "0xywygj2s6izylr60amgv7g98x3lqj53959h3cbsy53cjhhmfpgb")))

(define-public crate-nd_matrix-0.3.0 (c (n "nd_matrix") (v "0.3.0") (h "108h5xqmbgrzjdj0kd0h1xmlz912kizd3sij672vx315i96z6lsf")))

