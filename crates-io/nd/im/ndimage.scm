(define-module (crates-io nd im ndimage) #:use-module (crates-io))

(define-public crate-ndimage-0.1.0 (c (n "ndimage") (v "0.1.0") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "16zd7j1k0qj1w9ac54l1k2gjkvrg26f75ak03rq29fnqwz610370")))

(define-public crate-ndimage-0.1.1 (c (n "ndimage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 2)))) (h "0lhgb8yz2kbcixvm29xfjbfy98nivfqcmpsbb9id8ycifdzq2zd4")))

