(define-module (crates-io nd co ndcopy) #:use-module (crates-io))

(define-public crate-ndcopy-0.1.0 (c (n "ndcopy") (v "0.1.0") (d (list (d (n "ndshape") (r "^0.1") (d #t) (k 0)))) (h "0w81rwj8zn7qp6gnc18rggviknnfiyafyznwzgs8dingx86fhf30")))

(define-public crate-ndcopy-0.2.0 (c (n "ndcopy") (v "0.2.0") (d (list (d (n "ndshape") (r "^0.2") (d #t) (k 0)))) (h "0s5qwkbk50sc8015m7k1hwxsivjrj1qjb3hgd89p7cv44dzrgz3a")))

(define-public crate-ndcopy-0.3.0 (c (n "ndcopy") (v "0.3.0") (d (list (d (n "ndshape") (r "^0.3") (d #t) (k 0)))) (h "0naw17z0wsy36phmymdg8gyalvwa6sxy2nvm8avs9xz3h4dy9x14")))

