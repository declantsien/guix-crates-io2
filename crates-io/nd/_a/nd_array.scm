(define-module (crates-io nd _a nd_array) #:use-module (crates-io))

(define-public crate-nd_array-0.1.0 (c (n "nd_array") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0yjx5yhvlgw1piv5q359njv9hn3l3mb27rp2jr0jnwrkpj0w7n3x")))

