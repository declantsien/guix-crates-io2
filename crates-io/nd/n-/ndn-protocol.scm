(define-module (crates-io nd n- ndn-protocol) #:use-module (crates-io))

(define-public crate-ndn-protocol-0.3.0 (c (n "ndn-protocol") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ndn-tlv") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (f (quote ("sha2" "pkcs5"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "07707w9jz04zyw6vfbl0qgl8hkfjmak8548m6d4mky5cbr2xhfz3")))

