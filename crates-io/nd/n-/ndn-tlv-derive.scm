(define-module (crates-io nd n- ndn-tlv-derive) #:use-module (crates-io))

(define-public crate-ndn-tlv-derive-0.1.0 (c (n "ndn-tlv-derive") (v "0.1.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1gfqwkv51gnbjbc0333nzcf6r508bfynz306bik45gs780r6hmr0")))

(define-public crate-ndn-tlv-derive-0.2.0 (c (n "ndn-tlv-derive") (v "0.2.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0al7g2h3xk7gvifs7abi2nys9rfnwf6744gzpk2x2lywji6ryl5g")))

(define-public crate-ndn-tlv-derive-0.3.0 (c (n "ndn-tlv-derive") (v "0.3.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0pp8c27h810cnayywq48ih11w9cpcr3wr7fr7r2vlpxd8jj3qc5f")))

