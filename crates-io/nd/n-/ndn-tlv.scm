(define-module (crates-io nd n- ndn-tlv) #:use-module (crates-io))

(define-public crate-ndn-tlv-0.1.0 (c (n "ndn-tlv") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "ndn-tlv-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1psjlb9m5d3mfxs96ymihjdjd09d12sx4wjy6v8bplg9pnqsa6m5")))

(define-public crate-ndn-tlv-0.2.0 (c (n "ndn-tlv") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "ndn-tlv-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0wmlyxkm74b1cmq7hdnk281sdzn84gll4cm2j7xlw1p2v5qgkgfs") (y #t)))

(define-public crate-ndn-tlv-0.3.0 (c (n "ndn-tlv") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "ndn-tlv-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0an4wc34aivh0l67fw48vlh9aq8qx80sbknahjfagvhhdxyxjzmx")))

