(define-module (crates-io nd _i nd_iter) #:use-module (crates-io))

(define-public crate-nd_iter-0.0.1 (c (n "nd_iter") (v "0.0.1") (h "1bkxpkbs5h07974xwdjc0ip2i8rq380j7qzb1hyhkx3py3dl8ywx")))

(define-public crate-nd_iter-0.0.2 (c (n "nd_iter") (v "0.0.2") (h "0ggzzh8p0n0ci2qg6z4f3nwdjvkkknql6gai7bpssvsdx3pxlm66")))

(define-public crate-nd_iter-0.0.3 (c (n "nd_iter") (v "0.0.3") (h "0j5xvbkrhwnsxqsjrkm4gr964rsvjsm2ia7ahpww9c34l7a3ry01")))

(define-public crate-nd_iter-0.0.4 (c (n "nd_iter") (v "0.0.4") (h "12g8dm410dsc78dihk98jsgca07wsjv8apw63zqmxgmrq18nqqlp")))

