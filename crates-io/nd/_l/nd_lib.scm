(define-module (crates-io nd _l nd_lib) #:use-module (crates-io))

(define-public crate-nd_lib-0.1.0 (c (n "nd_lib") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 0)))) (h "0xixd1l18z1m1vhf5hv4b1vz1wjndh0z0fyb0r8djqngl09mkp9v")))

