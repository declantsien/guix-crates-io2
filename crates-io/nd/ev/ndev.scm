(define-module (crates-io nd ev ndev) #:use-module (crates-io))

(define-public crate-ndev-0.1.0 (c (n "ndev") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ymj0ia0bmbqla1155rh9fphc9ijy94vgsiv7ya9xsxlg15gzi0a")))

