(define-module (crates-io nd js ndjsonloggercore) #:use-module (crates-io))

(define-public crate-ndjsonloggercore-0.1.0 (c (n "ndjsonloggercore") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "random-fast-rng") (r "^0.1.1") (d #t) (k 2)) (d (n "ryu") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)))) (h "19pli2swrprjfp4yy3mnyba2h7lnisrn2y90sjxzzy8s7kgpjib7") (f (quote (("std") ("default")))) (s 2) (e (quote (("isotimestamp" "std" "dep:chrono"))))))

