(define-module (crates-io nd js ndjson_zst) #:use-module (crates-io))

(define-public crate-ndjson_zst-0.1.0 (c (n "ndjson_zst") (v "0.1.0") (d (list (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "1wbvj9l0h6fsyql1i9ahli3a1h4j7frr0arfsbfk9wgi82d651ra")))

(define-public crate-ndjson_zst-0.2.0 (c (n "ndjson_zst") (v "0.2.0") (d (list (d (n "zstd") (r "^0.13.1") (d #t) (k 0)))) (h "1n5b6k6w3398zjjazslbqhk0w06s54zbyk82ackkm946lm24qw43")))

