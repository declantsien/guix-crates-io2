(define-module (crates-io nd js ndjson-stream) #:use-module (crates-io))

(define-public crate-ndjson-stream-0.1.0 (c (n "ndjson-stream") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "kernal") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1r884bvc2rhx5jgr7d63xaz5fv16fq954l9dn9nbbykv77h1pnzl") (f (quote (("iter") ("default" "iter")))) (s 2) (e (quote (("stream" "dep:futures" "dep:pin-project-lite") ("bytes" "dep:bytes"))))))

