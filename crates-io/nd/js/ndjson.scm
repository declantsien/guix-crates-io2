(define-module (crates-io nd js ndjson) #:use-module (crates-io))

(define-public crate-ndjson-0.1.0 (c (n "ndjson") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1s7ijix483pycb4c247aan8mfjd29js067rcgmmwcbsyh0xqmwrq")))

(define-public crate-ndjson-0.2.0 (c (n "ndjson") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1fq2c83dwj0s5q2jj9zpjhr2vnp1srrgcdxf2hbv38w2l32kv2k6")))

