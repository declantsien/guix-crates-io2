(define-module (crates-io nd u_ ndu_add_one) #:use-module (crates-io))

(define-public crate-ndu_add_one-0.1.0 (c (n "ndu_add_one") (v "0.1.0") (h "164sarrjqxgaxa42gf4bbws2jrl7r9xlipnfpaqjvrm9d9239jwj")))

(define-public crate-ndu_add_one-0.1.1 (c (n "ndu_add_one") (v "0.1.1") (h "0wj8i4l9yh00m6qlrcpk817yhkn268bfc89532xqp3ij0djpjrhz")))

