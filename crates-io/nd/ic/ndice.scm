(define-module (crates-io nd ic ndice) #:use-module (crates-io))

(define-public crate-ndice-0.1.0 (c (n "ndice") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14p3kg4wgrfmv72qc49pvlaxch1qsf3qayrlvwzziy2km64lnmwx")))

(define-public crate-ndice-1.0.0 (c (n "ndice") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qlg8iv64sdxsybp4p2vs0pxyypa3wz0kw0hn9jfdyl9lr5ffdi4") (s 2) (e (quote (("json" "dep:serde"))))))

