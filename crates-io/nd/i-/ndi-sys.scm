(define-module (crates-io nd i- ndi-sys) #:use-module (crates-io))

(define-public crate-ndi-sys-0.1.0 (c (n "ndi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)))) (h "04md3r203kag3ihv11rb1fv6xk8rcql7xp96ij305mbdk4ph2l1n") (f (quote (("dynamic_link") ("default" "dynamic_link") ("bindings" "bindgen")))) (l "ndi")))

(define-public crate-ndi-sys-0.1.1 (c (n "ndi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)))) (h "045sdra6zsi8ikqipln7lari8s9vxp7pns0nngfs3xc8q7xwjh95") (f (quote (("dynamic_link") ("default" "dynamic_link") ("bindings" "bindgen")))) (l "ndi")))

