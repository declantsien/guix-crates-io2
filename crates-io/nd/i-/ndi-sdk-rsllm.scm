(define-module (crates-io nd i- ndi-sdk-rsllm) #:use-module (crates-io))

(define-public crate-ndi-sdk-rsllm-0.1.1 (c (n "ndi-sdk-rsllm") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 2)) (d (n "ptrplus") (r "^1.0") (d #t) (k 0)))) (h "0s9ykj6wgw0nknznrcljgm348mvh8kjaid29vyx239mhv3jbvazh") (f (quote (("dynamic-link"))))))

(define-public crate-ndi-sdk-rsllm-0.1.2 (c (n "ndi-sdk-rsllm") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "png") (r "^0.14.0") (d #t) (k 2)) (d (n "ptrplus") (r "^1.0") (d #t) (k 0)))) (h "16w9kbykgdv0ngcnppx1vq2kznl5m18y5r5v0wg9zm1ha1rf6a8d") (f (quote (("dynamic-link"))))))

