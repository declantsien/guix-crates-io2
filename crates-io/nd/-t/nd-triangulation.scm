(define-module (crates-io nd -t nd-triangulation) #:use-module (crates-io))

(define-public crate-nd-triangulation-0.1.0 (c (n "nd-triangulation") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1carlks8kgx62l5lclg5js2lfyw507y4h8i1rbvr41b63xjpj8zk")))

(define-public crate-nd-triangulation-0.1.1 (c (n "nd-triangulation") (v "0.1.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "14qlp34rb5qmsgy6cp7p133gnkpzq3hi75a46m86f80paal1x22y")))

(define-public crate-nd-triangulation-0.2.0 (c (n "nd-triangulation") (v "0.2.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0nfg1x95d1g1cg1ynnhdr1d4ggxyi73fa0d25ff2n4hyb04mb3vp")))

(define-public crate-nd-triangulation-0.3.0 (c (n "nd-triangulation") (v "0.3.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1cinkq8p08dn37v0aw9f2ycyk3rnjzlia13h7ij3hw2mlq5zhp91") (f (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3.1 (c (n "nd-triangulation") (v "0.3.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0w57b8xvlg1m3ivk1swx8jp8zdfsjvnfs7s3i7s0r32dgxqncilw") (f (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3.2 (c (n "nd-triangulation") (v "0.3.2") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "04p77maf5n7zjrarh7q6gxsqs9qdvhrcqi0am3x7c5b08fsfkbyv") (f (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3.3 (c (n "nd-triangulation") (v "0.3.3") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1rd87k3vx1hcyzs10rzqxjxwqzfciflca6wfxyfa9argy6qbh7ch") (f (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3.4 (c (n "nd-triangulation") (v "0.3.4") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1svg1iplmw0kk2y9rpdbmg8n74z22m7cqi18504nv773356869is") (f (quote (("docs-rs"))))))

