(define-module (crates-io nd ve ndvec) #:use-module (crates-io))

(define-public crate-ndvec-0.1.0 (c (n "ndvec") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1m2w2ai9vxhn4w0k36xpxfay9z2sgdb3kmw1m4g38gpq44d35354") (f (quote (("default")))) (s 2) (e (quote (("serde_arrays" "serde" "dep:serde_arrays") ("serde" "dep:serde"))))))

