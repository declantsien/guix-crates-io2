(define-module (crates-io nd ar ndarray-utils) #:use-module (crates-io))

(define-public crate-ndarray-utils-0.1.0 (c (n "ndarray-utils") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1hssq6f621ali0didkz49rz4iibdi2rzmj2lpkw9qfk7f0kyywxp")))

(define-public crate-ndarray-utils-0.1.1 (c (n "ndarray-utils") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1xhjkvafplisn9nay1rb6aicj2wx88z7fp3h64i0ikqjrciciy7h")))

(define-public crate-ndarray-utils-0.1.2 (c (n "ndarray-utils") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "14qwjyf4yayc5mjz03s4q2p6d8f1dirnlvwr58cxrxbf0y8r69i2")))

(define-public crate-ndarray-utils-0.1.3 (c (n "ndarray-utils") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l11n6fb4frxp11h1gnd203cw4b0mjpjivcbc5r4vvsz0g2jbd2a")))

(define-public crate-ndarray-utils-0.1.4 (c (n "ndarray-utils") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0z7gg534r7c8yaw3980dzw798v3mq0v930w9svrsms0py949gwvy")))

