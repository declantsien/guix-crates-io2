(define-module (crates-io nd ar ndarray-rblas) #:use-module (crates-io))

(define-public crate-ndarray-rblas-0.1.0 (c (n "ndarray-rblas") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 2)) (d (n "rblas") (r "^0.0.13") (d #t) (k 0)))) (h "16kb47004nmx500ym6qa0a4z5492zawpz3f4jxg97wcy1ckrvr54")))

(define-public crate-ndarray-rblas-0.1.1 (c (n "ndarray-rblas") (v "0.1.1") (d (list (d (n "ndarray") (r ">= 0.4.9, < 0.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 2)) (d (n "rblas") (r "^0.0.13") (d #t) (k 0)))) (h "1ph24c9ivks5cgagh535qgfs5jphg42pfpjqbszlkqav964hb6zh")))

