(define-module (crates-io nd ar ndarray-csv) #:use-module (crates-io))

(define-public crate-ndarray-csv-0.1.0 (c (n "ndarray-csv") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0s0nhlchcjprg5skqjr6ffv29ajvniijfaavamdahggby2kfh5rs")))

(define-public crate-ndarray-csv-0.2.0 (c (n "ndarray-csv") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ksbpsydjd3fii1kagbxbrznrm09dw39ca4z7s8shaswli3yykjm")))

(define-public crate-ndarray-csv-0.2.1 (c (n "ndarray-csv") (v "0.2.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zlsx83xkh7k4br5x802vhdwm8hnj5njwcrr7wrchccffs9f8h1k")))

(define-public crate-ndarray-csv-0.2.2 (c (n "ndarray-csv") (v "0.2.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "12zlmggsvpashpfd9kmhlm0vc0b0f4zma3pj033jx7sac7m62y85")))

(define-public crate-ndarray-csv-0.3.0 (c (n "ndarray-csv") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "05f02qrp9vfqpi7kr4m3lkyk9fgcx5a8bvqnw9qsia4m0afpv7sc")))

(define-public crate-ndarray-csv-0.3.1 (c (n "ndarray-csv") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "00pbwivzhjdfvk5irj6lwjvknyrif7pampplbmvs6h4qm8grcr3x")))

(define-public crate-ndarray-csv-0.4.0 (c (n "ndarray-csv") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0a06kwl9pmbc84a16bl6x4lhm4mw4v4v2hrh2g22nrk77nf0a36f")))

(define-public crate-ndarray-csv-0.4.1 (c (n "ndarray-csv") (v "0.4.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "13mrr4naa12mqhsd25cxnj3x0h3kd9vwmq9461rbzkwa2whx9hai")))

(define-public crate-ndarray-csv-0.5.0 (c (n "ndarray-csv") (v "0.5.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "16jlf8krykz5nqyl0sjl0mn2qpr8y06djbh5vk4am5fljf9n1p1g")))

(define-public crate-ndarray-csv-0.5.1 (c (n "ndarray-csv") (v "0.5.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r ">=0.7, <0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1d02378p7a75x3jw08qdalr9jrh2cnrrvbx8chdm8nmw3sc72z4z")))

(define-public crate-ndarray-csv-0.5.2 (c (n "ndarray-csv") (v "0.5.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "ndarray") (r ">=0.7, <0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1y5q00l96n2gmhl7631rb0x9ya9mrc0qkf7xvnzr6rfl2npxnmi9")))

