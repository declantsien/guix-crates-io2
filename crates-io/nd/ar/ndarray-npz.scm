(define-module (crates-io nd ar ndarray-npz) #:use-module (crates-io))

(define-public crate-ndarray-npz-0.1.0 (c (n "ndarray-npz") (v "0.1.0") (d (list (d (n "aligned-vec") (r "^0.5") (d #t) (k 2)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (k 0)) (d (n "zip") (r "^0.6.4") (k 0)))) (h "0y36gyfpyg44hhkwxlyl5r3w6rj6vz8mfx6gr1djq0v81r05gviw") (f (quote (("num-complex-0_4" "ndarray-npy/num-complex-0_4") ("default" "compressed" "num-complex-0_4") ("compressed" "zip/deflate")))) (r "1.60")))

