(define-module (crates-io nd ar ndarray-tensorflow) #:use-module (crates-io))

(define-public crate-ndarray-tensorflow-0.1.0 (c (n "ndarray-tensorflow") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "tensorflow") (r "^0.13") (d #t) (k 0)))) (h "0faw3ads57mmhvypwxr70w8zazax0y42np3155077krb87l2fyg8")))

(define-public crate-ndarray-tensorflow-0.2.0 (c (n "ndarray-tensorflow") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "tensorflow") (r "^0.13") (d #t) (k 0)))) (h "1b68xpklzr28zn5h0xy0lx057bh8wr4p7a2f0r83i58jzwpc247v")))

(define-public crate-ndarray-tensorflow-0.3.0 (c (n "ndarray-tensorflow") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "tensorflow") (r "^0.13") (d #t) (k 0)))) (h "1a09w2mvmpx52sn4qcwysriwq3warcvz4krvnflnyslbfai3lmwb")))

