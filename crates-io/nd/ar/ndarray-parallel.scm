(define-module (crates-io nd ar ndarray-parallel) #:use-module (crates-io))

(define-public crate-ndarray-parallel-0.1.0 (c (n "ndarray-parallel") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.6") (d #t) (k 0)))) (h "0miyrngdkcr32j63qag5ymjm446d5hiz5h4m5cy8s9ah6ana8fm0")))

(define-public crate-ndarray-parallel-0.1.1 (c (n "ndarray-parallel") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.7.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.6") (d #t) (k 0)))) (h "1701qg88dg85jkkz2x98dxkv75hsflkmdab5d2pl50f6i972saiw")))

(define-public crate-ndarray-parallel-0.2.0 (c (n "ndarray-parallel") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.8.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.6") (d #t) (k 0)))) (h "07l0zy7ihsa852bgnl0nxa4bhl0j5n8hz2h06g8yb06722681cik")))

(define-public crate-ndarray-parallel-0.3.0-alpha.1 (c (n "ndarray-parallel") (v "0.3.0-alpha.1") (d (list (d (n "ndarray") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "1546pcv8w2vpv0dzxz5x9kh3zzzh2s3pajsklc3ky067162pynqd")))

(define-public crate-ndarray-parallel-0.3.0 (c (n "ndarray-parallel") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.9.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "0mc013q316sq92vmrf0mybbw7pcamr67rw07jl7dp3v7pp17m6ym")))

(define-public crate-ndarray-parallel-0.4.0 (c (n "ndarray-parallel") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.9.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1hbqxr36hdvw8ri0b8bb6zps8zpqi8rwyfj15iyw85asir0xv9ca")))

(define-public crate-ndarray-parallel-0.5.0 (c (n "ndarray-parallel") (v "0.5.0") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 2)) (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "0y2avaww4zh9knvb4l5dbc56i5s7ajaqgsz1g6j51ayqmmdlcmjx")))

(define-public crate-ndarray-parallel-0.6.0 (c (n "ndarray-parallel") (v "0.6.0") (d (list (d (n "itertools") (r "^0.7.0") (k 2)) (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "07sk271l3cs3l1wl28rjdgrf5vjp94jqwypccrmvxgv0ah46q7l3")))

(define-public crate-ndarray-parallel-0.7.0 (c (n "ndarray-parallel") (v "0.7.0") (d (list (d (n "itertools") (r "^0.7.0") (k 2)) (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "1viv2f2xbfcha06dzh209w8qsqy6s6507i6p4yphpvkw28zgm5r3")))

(define-public crate-ndarray-parallel-0.8.0 (c (n "ndarray-parallel") (v "0.8.0") (d (list (d (n "itertools") (r "^0.7.0") (k 2)) (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1b5xxcdlp0s4a8zhv7l28gvar27qnf7mim44cwzfkrj4p6l578v4")))

(define-public crate-ndarray-parallel-0.9.0 (c (n "ndarray-parallel") (v "0.9.0") (d (list (d (n "itertools") (r "^0.7.0") (k 2)) (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0p7k2fl4vpi2ic57k8plla9w1khm3r38y5k16dzc1skn6wqsadzq")))

(define-public crate-ndarray-parallel-0.9.1 (c (n "ndarray-parallel") (v "0.9.1") (d (list (d (n "itertools") (r "^0.7.0") (k 2)) (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0s3xmpfa63kflzp7idax50b9xiwv6p94qwzlrs88379gdjg4qi59")))

