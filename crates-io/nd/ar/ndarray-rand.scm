(define-module (crates-io nd ar ndarray-rand) #:use-module (crates-io))

(define-public crate-ndarray-rand-0.1.0 (c (n "ndarray-rand") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "103ln9dnnqfxyz5xxa9svfbl904s5k8fkr790szq7lpzak4zlh92")))

(define-public crate-ndarray-rand-0.1.1 (c (n "ndarray-rand") (v "0.1.1") (d (list (d (n "ndarray") (r ">= 0.4.9, < 0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1x1d8k36mnp0bj7m2snpq2xcksjdaxmc6f53vblv7bfvlbyzm39s")))

(define-public crate-ndarray-rand-0.2.0 (c (n "ndarray-rand") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0kvb0ix0wg1bb60227x803ldp1nqi2lgprdwchbl94yawpvjzlkr")))

(define-public crate-ndarray-rand-0.3.0 (c (n "ndarray-rand") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b0dcg66gkyfsjpk62bhph4b9kvz70r63rw28cajfzzjri660vm9")))

(define-public crate-ndarray-rand-0.3.1 (c (n "ndarray-rand") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0d6s8hbr022z43sb0036jsmxiypfdsc6k1n0zyb5fldrj8m9yfs7")))

(define-public crate-ndarray-rand-0.4.0 (c (n "ndarray-rand") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "17z2wyi9xiz0h42lz4lal6nrsh4myi435ijjw0vh9q155bxw4iiv")))

(define-public crate-ndarray-rand-0.5.0 (c (n "ndarray-rand") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "08v0k0cq3s0wadkadjwnr8p30x5k9zrsi2ylfxjcypflbngq5pmz")))

(define-public crate-ndarray-rand-0.6.0 (c (n "ndarray-rand") (v "0.6.0") (d (list (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0laz8zs6ddmbrkz6d49r7rjqam12jxz8p7iwbb3avm25jc8d8rhm")))

(define-public crate-ndarray-rand-0.6.1 (c (n "ndarray-rand") (v "0.6.1") (d (list (d (n "ndarray") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0jyjq42jsv3pxxw9ff4bkiilm6k0hl67wb7blhs5134aclw5l8mb")))

(define-public crate-ndarray-rand-0.7.0 (c (n "ndarray-rand") (v "0.7.0") (d (list (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "0pwkdkm01rz03xgiyd0zid23735jym553qd9zm2kicsj8yv4y73s")))

(define-public crate-ndarray-rand-0.8.0 (c (n "ndarray-rand") (v "0.8.0") (d (list (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1x9br3109r2m0fb3xhrklyx27xzla24s6a3vigs7513v2l9vjj8w")))

(define-public crate-ndarray-rand-0.9.0 (c (n "ndarray-rand") (v "0.9.0") (d (list (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0l22wl3ixsdhc4j9m4xh48rfmby5mh1nk9bv8jjpxag6x5zbi8mm")))

(define-public crate-ndarray-rand-0.10.0 (c (n "ndarray-rand") (v "0.10.0") (d (list (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.1") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 2)))) (h "0zmp53f8472v7clj49nkliah1lj1fcfx2hygm5xk7aim3zz7wqpg")))

(define-public crate-ndarray-rand-0.11.0 (c (n "ndarray-rand") (v "0.11.0") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.1") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 2)))) (h "1rrwdjnwdzlakydvzrdp2c5x68fskcp5vh61kllsf1rkg7n2zwq4")))

(define-public crate-ndarray-rand-0.12.0 (c (n "ndarray-rand") (v "0.12.0") (d (list (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 2)))) (h "1m92ik533ipz7qx9dgas6zih9ixf93ci4fas618q707va0ri876x")))

(define-public crate-ndarray-rand-0.13.0 (c (n "ndarray-rand") (v "0.13.0") (d (list (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.8.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 2)))) (h "088xan50zhll8rzpmda8ccyivmv1h0r4fwq8yclanjlp7gz080ys")))

(define-public crate-ndarray-rand-0.14.0 (c (n "ndarray-rand") (v "0.14.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.8.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 2)))) (h "1225iiqhc9h0sd4sdf4a4vf6fpdwy3s41ksd2rdmywncga9qyq35")))

