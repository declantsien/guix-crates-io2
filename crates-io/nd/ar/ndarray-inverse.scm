(define-module (crates-io nd ar ndarray-inverse) #:use-module (crates-io))

(define-public crate-ndarray-inverse-0.1.0 (c (n "ndarray-inverse") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1h1qsmcngaqzniwqnw5nxvcqsf2dq9a3swsxfaqw3gyip155k68c") (y #t)))

(define-public crate-ndarray-inverse-0.1.1 (c (n "ndarray-inverse") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qi6s3vz1sca9a1d14i0qywp4ndsmrkjn8a09xnj25sc3akdx3zk") (y #t)))

(define-public crate-ndarray-inverse-0.1.2 (c (n "ndarray-inverse") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13hn3g00yghwaa63iirhngaz76q04w91f4gk06nzygaxv7285fxf") (y #t)))

(define-public crate-ndarray-inverse-0.1.3 (c (n "ndarray-inverse") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vh1hx60xx0srsx2ajyp7qjwgdjzzsgxh9w7rn85yzyyrwxsdaw6") (y #t)))

(define-public crate-ndarray-inverse-0.1.4 (c (n "ndarray-inverse") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gkbymjcd2mm4a0nqwmm4hh07p829khpa8cn3h8b36ly5s5a9hp4") (y #t)))

(define-public crate-ndarray-inverse-0.1.5 (c (n "ndarray-inverse") (v "0.1.5") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1khcz21j5975ac1a4acvcf2rh5s3a3x81x4la9cpaf8giqz9gklm") (y #t)))

(define-public crate-ndarray-inverse-0.1.6 (c (n "ndarray-inverse") (v "0.1.6") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11vc7md55b0yb1lnjcaikpvz1xmxrbvzisnif5jp72qwf56y86a2")))

(define-public crate-ndarray-inverse-0.1.7 (c (n "ndarray-inverse") (v "0.1.7") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10932pfjfwpaxxxn1y2f1m9ijpx2dl9klrnqdxb0b65v22ahwpas")))

(define-public crate-ndarray-inverse-0.1.8 (c (n "ndarray-inverse") (v "0.1.8") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0021grfdcpfrxszfkbaigffgz5s1r9wzswji87krdx17hl6737z7")))

(define-public crate-ndarray-inverse-0.1.9 (c (n "ndarray-inverse") (v "0.1.9") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mpfm224q04nilg8iwb2g3himbidy9a4m92nc4x68d3h4pf5ijq3")))

