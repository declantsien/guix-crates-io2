(define-module (crates-io nd ar ndarray-unit) #:use-module (crates-io))

(define-public crate-ndarray-unit-0.1.0 (c (n "ndarray-unit") (v "0.1.0") (h "1nr4vzcbpnk2fm86ryh9cfbzc4ddrrk90zsark0n3b8nw6slwr6v")))

(define-public crate-ndarray-unit-0.1.1 (c (n "ndarray-unit") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "1r8m8z21r7wn7xm1v8hln1ixpry8ls355r68c3gb5sk6rknalh8s")))

(define-public crate-ndarray-unit-0.2.0 (c (n "ndarray-unit") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "0109djy7a1zcfr3q3jlqrfscjgq9w1li3aqslkavbkhirnbnyd4j")))

(define-public crate-ndarray-unit-0.2.1 (c (n "ndarray-unit") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "1xgxviw3159w70w4pm1j6vhbfkchrp9x12pg3gkql4gfn9hbkq7k")))

(define-public crate-ndarray-unit-0.2.2 (c (n "ndarray-unit") (v "0.2.2") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "0w8wvglyr68jjvj0af470xh4pz98and10m11j26hyn25sf6amphk")))

