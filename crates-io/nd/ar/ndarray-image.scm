(define-module (crates-io nd ar ndarray-image) #:use-module (crates-io))

(define-public crate-ndarray-image-0.1.0 (c (n "ndarray-image") (v "0.1.0") (d (list (d (n "image") (r "^0.22.1") (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 2)))) (h "0q2rqfdd57bg65ayckvi02r5f0bn3lzb14f2ycwlk72zhwdfbxga")))

(define-public crate-ndarray-image-0.1.1 (c (n "ndarray-image") (v "0.1.1") (d (list (d (n "image") (r "^0.22.1") (k 0)) (d (n "image") (r "^0.22.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 2)))) (h "0hfmfgln4mgbhr2xxgz8mf9ki0qjyx83brrlkh2hm21bipby2pjh")))

(define-public crate-ndarray-image-0.2.0 (c (n "ndarray-image") (v "0.2.0") (d (list (d (n "image") (r "^0.23.2") (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.0") (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 2)))) (h "13g5qas1fwwab1h1fkk8xl6hcvjf79ldljjz0b8h0azqjl9x3ygc")))

(define-public crate-ndarray-image-0.2.1 (c (n "ndarray-image") (v "0.2.1") (d (list (d (n "image") (r "^0.23.2") (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.0") (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 2)))) (h "13nq34qb05hqrqzxrrjn5xpnnwr4fjgrjf01w3h4pqhmccmaj5s0")))

(define-public crate-ndarray-image-0.3.0 (c (n "ndarray-image") (v "0.3.0") (d (list (d (n "image") (r "^0.23.12") (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "ndarray") (r "^0.14.0") (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)))) (h "0dgki2g6af5g9ar63avkcldkmvxywzrhmwv8n5vr6r219lb7lhz3")))

