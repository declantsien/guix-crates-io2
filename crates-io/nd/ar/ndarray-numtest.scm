(define-module (crates-io nd ar ndarray-numtest) #:use-module (crates-io))

(define-public crate-ndarray-numtest-0.1.0 (c (n "ndarray-numtest") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13grbvqxssxbsisdhxzvbfcpa97cd9sm69fgwqg4gszrkprqgxlf")))

(define-public crate-ndarray-numtest-0.1.1 (c (n "ndarray-numtest") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17n9jwfv2cvk122bkaprv6jbc3g1fk8hl8hqb8sf6qsywfkw26jm")))

(define-public crate-ndarray-numtest-0.1.2 (c (n "ndarray-numtest") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05f4ndbrz35w5nfk62x7nsmwvynim9k2faxrdlfvcr7n7vg7a9yn")))

(define-public crate-ndarray-numtest-0.1.3 (c (n "ndarray-numtest") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vzfk68b3igxpsjmv8g57fwv0h1p1p9wycbg3hi33gi55jv3vkkq")))

(define-public crate-ndarray-numtest-0.1.4 (c (n "ndarray-numtest") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mvlgmm8hvakxw27jj9lziqw9bpjsxv06yp9i3w54vlc2lhzf79z")))

(define-public crate-ndarray-numtest-0.2.0 (c (n "ndarray-numtest") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.8") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.4") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1m9f3qyaclkkwyjqmggmpyx5p7mpfbwg56z8adz75c7hqd26bcam")))

