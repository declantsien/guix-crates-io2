(define-module (crates-io nd ar ndarray-zfp-rs) #:use-module (crates-io))

(define-public crate-ndarray-zfp-rs-0.1.0 (c (n "ndarray-zfp-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.17") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "zfp-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1nbqm77716270mw801dy8fr7phh0p9v7y4k4rmxn2pc3dk67njzp")))

