(define-module (crates-io nd ar ndarray-ndimage) #:use-module (crates-io))

(define-public crate-ndarray-ndimage-0.1.0 (c (n "ndarray-ndimage") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1s20hmh2p8v35d91564r77sfp94v8mlfx7qhxx4ph79fa1myhhc5")))

(define-public crate-ndarray-ndimage-0.1.1 (c (n "ndarray-ndimage") (v "0.1.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0y44nijcpn7j3a2spcqv0zcrlkb9487zkmp3ldy8vqbrba2c80d9")))

(define-public crate-ndarray-ndimage-0.1.2 (c (n "ndarray-ndimage") (v "0.1.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1g2vhyrqrsbxy6l49ss3srsn60k84nrcrky3ypc58c4ccz2andz1")))

(define-public crate-ndarray-ndimage-0.1.3 (c (n "ndarray-ndimage") (v "0.1.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1pb0ddkmwl9xmxlpgca4fqbqwvky8zdky056sqrgrmd1pwc99yj1")))

(define-public crate-ndarray-ndimage-0.2.0 (c (n "ndarray-ndimage") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0p7s3s749dlhbaw0i0bkbpbwj80z5z5sjg0nzsjgyw0rv1zgaa3j")))

(define-public crate-ndarray-ndimage-0.2.1 (c (n "ndarray-ndimage") (v "0.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1h4d1bmfydlwdnny24vhaady8nw03l4gd6vawl1b3p1n1649pvrs")))

(define-public crate-ndarray-ndimage-0.2.2 (c (n "ndarray-ndimage") (v "0.2.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1qj28yk37wwvn7mdcfcv069p9vv8wkdz9r88b2gpf44zypqxn7bc")))

(define-public crate-ndarray-ndimage-0.3.0 (c (n "ndarray-ndimage") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0fl33bl26rjnlaxz6xkzbg6qpwqx4vxjfskhcn61vbwypafvkzkd")))

(define-public crate-ndarray-ndimage-0.4.0 (c (n "ndarray-ndimage") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1s1gzpwvymr6zfq84msdk114kpqfgyw9y70pr38iwibgyx1z298f")))

