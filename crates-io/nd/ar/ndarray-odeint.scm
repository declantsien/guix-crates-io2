(define-module (crates-io nd ar ndarray-odeint) #:use-module (crates-io))

(define-public crate-ndarray-odeint-0.1.0 (c (n "ndarray-odeint") (v "0.1.0") (d (list (d (n "itertools") (r "^0.5.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (d #t) (k 2)))) (h "0770ack8rnyly9vxv6aqn4dqfj94m2ablz2crn7x8hlfl1ws6l6a")))

(define-public crate-ndarray-odeint-0.2.0 (c (n "ndarray-odeint") (v "0.2.0") (d (list (d (n "itertools") (r "^0.5.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (d #t) (k 2)))) (h "1wnqiiynsw70sdfn40xynszv7mqj2hv8sckl325mb2x6bcrgz1xj")))

(define-public crate-ndarray-odeint-0.2.1 (c (n "ndarray-odeint") (v "0.2.1") (d (list (d (n "itertools") (r "^0.5.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (d #t) (k 2)))) (h "0mc5x2rdf76iz4pr0bnxnfx2yksky4ihdvgzfcb83xvjl98b4nnf")))

(define-public crate-ndarray-odeint-0.2.2 (c (n "ndarray-odeint") (v "0.2.2") (d (list (d (n "itertools") (r "^0.5.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (d #t) (k 2)))) (h "1dxdhvpj289m1y0ivv78gy6ij67c8vggmk34rcchg4zzpivgyj18")))

(define-public crate-ndarray-odeint-0.2.3 (c (n "ndarray-odeint") (v "0.2.3") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.7.3") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray-numtest") (r "^0.1.4") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.35") (d #t) (k 2)))) (h "0invci01653mnnf44rd0ninkpi0b7pjrnjrj9b7lkkyzv2pnn7hb")))

(define-public crate-ndarray-odeint-0.3.0 (c (n "ndarray-odeint") (v "0.3.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.8") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.3") (d #t) (k 0)) (d (n "ndarray-numtest") (r "^0.2") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0sniyqiff9jcghpkzllfvsgwsvbrjlaw7cv98pqbv194ld4fhdnk")))

(define-public crate-ndarray-odeint-0.4.0 (c (n "ndarray-odeint") (v "0.4.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.8") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.3") (d #t) (k 0)) (d (n "ndarray-numtest") (r "^0.2") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1mzwf1mrwclzmfpr0ls5qy6hx4b7yp0f4dfvbj4jgf49ghzxcvn7")))

(define-public crate-ndarray-odeint-0.5.0 (c (n "ndarray-odeint") (v "0.5.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand-extra") (r "^0.1") (d #t) (k 2)))) (h "16ldd2hsylnfldhc7nb5mac5pfnplyfdprwij7n5im76bkjiharr")))

(define-public crate-ndarray-odeint-0.5.1 (c (n "ndarray-odeint") (v "0.5.1") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand-extra") (r "^0.1") (d #t) (k 2)))) (h "1l7j6d4pmzi24f0mqaxqpmk0fwyi6bks21rawi799120cqa001hr")))

(define-public crate-ndarray-odeint-0.5.2 (c (n "ndarray-odeint") (v "0.5.2") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.5.4") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.5") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand-extra") (r "^0.1") (d #t) (k 2)))) (h "1csssl24z6rlfd1pc0ixzdw92j8afawdwd6v0ii9i7qjld5dg8gz")))

(define-public crate-ndarray-odeint-0.6.0 (c (n "ndarray-odeint") (v "0.6.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.5.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0as49gqxilahq75q5qniqw1kb7sbsjraxijlx70j6ajagn7lqpcb") (y #t)))

(define-public crate-ndarray-odeint-0.6.1 (c (n "ndarray-odeint") (v "0.6.1") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.5.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "064pq55jgd88a4lwplzampdvygj01vkr2m1cb45bjlbd4wiacr0c")))

(define-public crate-ndarray-odeint-0.7.0 (c (n "ndarray-odeint") (v "0.7.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0g85qyzbiqayvqif5d4xbhljl6w97zkzxyjmsmbannvag1c57wzi")))

