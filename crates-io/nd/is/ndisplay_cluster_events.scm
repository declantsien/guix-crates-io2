(define-module (crates-io nd is ndisplay_cluster_events) #:use-module (crates-io))

(define-public crate-ndisplay_cluster_events-0.1.0 (c (n "ndisplay_cluster_events") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0wwjh18vvk8z5fmvdxnh9nxy6lvpl38zxsqk792pkin5dd9mminw")))

(define-public crate-ndisplay_cluster_events-0.2.0 (c (n "ndisplay_cluster_events") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "13kfjpv48gwgr4x7kmlfqk1fmvak8m6i4gn8y6v52ih8sm7fiijh")))

(define-public crate-ndisplay_cluster_events-0.3.0 (c (n "ndisplay_cluster_events") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0gg52kmh75hgdf0cjqggmyy6ng1cqw87mv6dxq8z6ibzzyzv6l5n")))

(define-public crate-ndisplay_cluster_events-0.3.1 (c (n "ndisplay_cluster_events") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1flcd8359s909llagv88gzjy1idr9rnr6nsk3gd9pgp1z3p7nc4m")))

(define-public crate-ndisplay_cluster_events-0.4.0 (c (n "ndisplay_cluster_events") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "17k3vzv0l1nrpvqqczpydanyavicgpv8528jrz229k21nw1ja2m6")))

(define-public crate-ndisplay_cluster_events-0.5.0 (c (n "ndisplay_cluster_events") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1zxjdfchpjsmnmbrgi9bfp4k979m4xffkc2f7da4h0xdfwvfkaqx")))

