(define-module (crates-io nd at ndata) #:use-module (crates-io))

(define-public crate-ndata-0.1.0 (c (n "ndata") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.2") (d #t) (k 0)))) (h "19szbq2if09a9x9gy7qnvxgnbfqwipjm6y0xwmrca9w1sq5fydr6")))

(define-public crate-ndata-0.1.1 (c (n "ndata") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.2") (d #t) (k 0)))) (h "0hmfnpdwwn36zf2kimm4wdppk2xqvsq9mklmdpvg45k42sd3vl8j")))

(define-public crate-ndata-0.1.2 (c (n "ndata") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.2") (d #t) (k 0)))) (h "11j6vgw303wawg16mjgyb0cvrjxr7bymlazrj2a3l7dx6h2mqy8x")))

(define-public crate-ndata-0.1.3 (c (n "ndata") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "02f7kmlphsprymjgm4yrfvawvbf5jn60j9v8dndi13nwq01pxsmi")))

(define-public crate-ndata-0.1.4 (c (n "ndata") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "0yfznzzshav4yv8kh89m0cg2c0nf2b8wp5iiwbl5q7rzw7c9mhga")))

(define-public crate-ndata-0.1.5 (c (n "ndata") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "035prlkjwkk56xfa4dsfs0hnhs1f8vnkqnl6fk7z1xaar8w4vwhs")))

(define-public crate-ndata-0.2.0 (c (n "ndata") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "1ikba2i0lplagn5pdvinkfcax0ddidj9p12dqnvbnx2v1mzs4biq") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.1 (c (n "ndata") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "1mia34k5mf01rgasdzfvzrjasphk2w5lmks7qw1bn1lsqppf3wyv") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.2 (c (n "ndata") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)))) (h "1idqlnjvpdz6z8qhfa5qzvg21d4gpfmhl24wfcriqgvhdskbpyvl") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.3 (c (n "ndata") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1cxbhk8y9k6pccpaa9c7k58igi2d6f1025mrn4ckwwsmw5n3xc65") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.4 (c (n "ndata") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "077i0asz45b7pgbv166bfgc9zw7941300r8dx2gh3kxh5lm1dcfs") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.5 (c (n "ndata") (v "0.2.5") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0vsgjmjdg5wx0fs1j7wd2rwnrsl5030q8js35d21mi6hzjjdd4r2") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.6 (c (n "ndata") (v "0.2.6") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1cwrk2g24p0w4hfivlbzqsx9qr3jqdjg2450bd10xhqw66vh2qpl") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.7 (c (n "ndata") (v "0.2.7") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1dpypc49wby8m6yp9svd9r8j1b9nb73cybimbydqkp39n2bpmmr3") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.8 (c (n "ndata") (v "0.2.8") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0xg6bpay6r3r0s7yi79w8f58485074qhpdsab26j8wvscf02vznx") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.9 (c (n "ndata") (v "0.2.9") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0av6c6n1xlbran6fl7vpalnjqdlhss8a56jwn9h53yb1isxw9ali") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.10 (c (n "ndata") (v "0.2.10") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0q6i4s0qfska2478kzbj8bzfhfk5032yzhaxcicmblpfklhdbvfj") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.11 (c (n "ndata") (v "0.2.11") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0flcdhbyr0q3mbd17jck9718hkwinlpf1kvbifr8a71i0g945z6r") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.12 (c (n "ndata") (v "0.2.12") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1y6zcp5mvvwmndk5ca9i6bsmv9jhi71i821ynrq7qx9kgj098ak0") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.13 (c (n "ndata") (v "0.2.13") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "02padcanlqaa55ygy8yskil432q6vxhvgkchm9q6jyhkbrlpjqs2") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.14 (c (n "ndata") (v "0.2.14") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "037g5bzsw7ywvm8pxqr74v4zf5b96hngsbjd7r1cx4k6yjkzrkww") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.2.15 (c (n "ndata") (v "0.2.15") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "12mcyg73xn587dlr1i6v3pbm682jm7yvmxwvmdw3i288wsg8jf77") (f (quote (("serde_support" "serde_json") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.16 (c (n "ndata") (v "0.2.16") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0via1zjn3c3p1ckw9pdrc6kl25jc60x4qd719qdsxdmv4kmbq28w") (f (quote (("serde_support" "serde_json") ("reload") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.17 (c (n "ndata") (v "0.2.17") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1058vgjbhnlq2byjs1lay5yb5pzlv1sx3x2kjywd0cqc7f7is8f9") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.18 (c (n "ndata") (v "0.2.18") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0a18k6s796f8pd9jhkwdisd024dp33ghlsnms9386hc5akpyxp1v") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.19 (c (n "ndata") (v "0.2.19") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1f2lcwibim91hv5qpdbycw5br0322360l38rfxwdchv6m7igl2q6") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.20 (c (n "ndata") (v "0.2.20") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0qjbv44ns1bq0x2akpiinsr1dvln9dx2hd6mdyrgrbiaizbypa3p") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.21 (c (n "ndata") (v "0.2.21") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "04paiv312cd4gmm3ghnpavp2c4ms62791s5g4x5gs53r97bi3rsz") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.22 (c (n "ndata") (v "0.2.22") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0s700kl2zw9py530w839981aqj6kcyk01dgn49snfs7skgnbi52q") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.23 (c (n "ndata") (v "0.2.23") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "05ka98yxps04x7x12nkcdwj7knll6qjizqk5pkncp0q5wdliyjg6") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.24 (c (n "ndata") (v "0.2.24") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0f4lmdsayrgskl2q1l3npllilkyb7gjsbjwx9mkfniw2xacvli3b") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.2.25 (c (n "ndata") (v "0.2.25") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "13lybygb4wadp2vgx1n6cnz63rdyjlxjwxyhn4sj282ycb8cm4vw") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.0 (c (n "ndata") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "129ncrbmd5as4f7h5wccyyarzva9nqn20wjlwm1pf8lkfc239ysh") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.1 (c (n "ndata") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "05kb66b7b05m8111x95v8s3mqhya2xvc10bpsbabyh36mp5216lq") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.2 (c (n "ndata") (v "0.3.2") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "0i04h5nj14pwjrgdgh4l293mbn56lafvbghafv1kva798bmslfyz") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.3 (c (n "ndata") (v "0.3.3") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "04l93c9ll7a0pplhyvas6gdjljs380jsj93r7r98xbdlxs4m6bag") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.4 (c (n "ndata") (v "0.3.4") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1nfkf4kkg04vsl43frgsk9z1ymbvpgfxk5d8vi6mwf3n9mam00cb") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.5 (c (n "ndata") (v "0.3.5") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "015czd5zw4mp0m4x39q1hy6q0y4ggdjs894g0820ywbc0z1r6bf7") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.6 (c (n "ndata") (v "0.3.6") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1y128vh6nbc7kpfjlqw13817c7ax9jgwr6mw93y1kj24k33qz26m") (f (quote (("serde_support" "serde_json") ("no_std_support" "hashbrown") ("mirror") ("debug_mutex" "backtrace"))))))

(define-public crate-ndata-0.3.7 (c (n "ndata") (v "0.3.7") (d (list (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1l0wndasb89qchp68rbd1hk07vfmxd36ca8km8jj9rr2p7p7h3b2") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.3.8 (c (n "ndata") (v "0.3.8") (d (list (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02pzas4vqy68ds4v0hjbs8mzkq0jjyph9fngkh7w12vrx221p4dm") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.3.9 (c (n "ndata") (v "0.3.9") (d (list (d (n "serde_json") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "1f73kg6hq64nc0mfrfvvx8nixa8icd8q0xphnl94lpmdzvhc2ln6") (f (quote (("serde_support" "serde_json"))))))

(define-public crate-ndata-0.3.10 (c (n "ndata") (v "0.3.10") (d (list (d (n "serde_json") (r "^1.0.111") (o #t) (d #t) (k 0)))) (h "07x26z021d8g57xrckcng0xgi6zfl9jmvm5ghka64rqcgsf2q9fk") (f (quote (("serde_support" "serde_json") ("mirror"))))))

