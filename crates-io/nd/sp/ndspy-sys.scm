(define-module (crates-io nd sp ndspy-sys) #:use-module (crates-io))

(define-public crate-ndspy-sys-0.1.1 (c (n "ndspy-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "08k4a555py6siaz7v94zhzjpwjy34mi89a6i50kbz6ya2dkc20ma") (f (quote (("download_3delight_lib" "reqwest"))))))

(define-public crate-ndspy-sys-0.1.2 (c (n "ndspy-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "1ygh84sgi7qxkf16vyrxflckqc6qc427fqbs1nfnhcx7jf10maip") (f (quote (("download_3delight_lib" "reqwest"))))))

(define-public crate-ndspy-sys-0.1.3 (c (n "ndspy-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "0giv0vlc0skrdz68p5ffa0dwyjprkgfc7xm2izr66wb2i2hc9br3")))

(define-public crate-ndspy-sys-0.1.4 (c (n "ndspy-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)))) (h "0hrhhb4yrrmzhswqy7f0wgapgxh9ldc3xygi0vain4p9x3g9837b")))

(define-public crate-ndspy-sys-0.1.5 (c (n "ndspy-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "13l46pirrfccfas8m5pkk8w5z79s24k7mcwhbspbzppmkppdh3d6")))

(define-public crate-ndspy-sys-0.1.6 (c (n "ndspy-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1n1nf7i8hfvrnss9dazcj9da6g60lfqcl99vwcz9zp5whgfz8ihr")))

(define-public crate-ndspy-sys-0.1.7 (c (n "ndspy-sys") (v "0.1.7") (d (list (d (n "bindgen") (r ">=0.55.1, <0.56.0") (d #t) (k 1)))) (h "1dkd4wjf1y9nh0hjwrk238ydzf1ji0a61g1lxxxh79rz4mrwx9vy") (f (quote (("link_lib3delight") ("default"))))))

(define-public crate-ndspy-sys-0.2.0 (c (n "ndspy-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "0qikj72zdrpgd9msh9sfpk0ysq59m2m2qgiydbrc38azck8lhn4b") (f (quote (("link_lib3delight") ("default"))))))

(define-public crate-ndspy-sys-0.2.1 (c (n "ndspy-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "13ngwsx8bh3xyn0c8xamxj570lsridwb2s78r64bmfdkyghs1y1r") (f (quote (("link_lib3delight") ("default"))))))

(define-public crate-ndspy-sys-0.2.2 (c (n "ndspy-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0b9wvpjp6396bksgv61rr5z1vjrzazf7vd2p8xyvshg3xyj7n0gm")))

