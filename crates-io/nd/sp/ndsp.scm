(define-module (crates-io nd sp ndsp) #:use-module (crates-io))

(define-public crate-ndsp-0.1.0 (c (n "ndsp") (v "0.1.0") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "082ai013mslp3rjhvx1af7qkydn6hmnxgbik0lgfy268plb31ps8") (f (quote (("std" "plotters") ("default"))))))

(define-public crate-ndsp-0.1.1 (c (n "ndsp") (v "0.1.1") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1biijb14vzr6wj44al766phq0sp1ilzadqb5mikc0d9sq001vy1i") (f (quote (("std" "plotters") ("default"))))))

(define-public crate-ndsp-0.1.2 (c (n "ndsp") (v "0.1.2") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0lkrhb4jm0a7qf985i3c8n7i3bca5jrchj1xinls36sc64hr2yfb") (f (quote (("std" "plotters") ("default"))))))

(define-public crate-ndsp-0.2.0 (c (n "ndsp") (v "0.2.0") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0y9qx68nfhlibi3ldk2zpwz0ywsx5icwwihmbcygxy64y11j7kif") (f (quote (("std" "plotters") ("default"))))))

(define-public crate-ndsp-0.3.0 (c (n "ndsp") (v "0.3.0") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "14cd3x723476kvc5x8c76srw8z2rwmy3h160jaj0qr0yrx25z4n8") (f (quote (("std" "plotters") ("default"))))))

(define-public crate-ndsp-0.3.1 (c (n "ndsp") (v "0.3.1") (d (list (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0xv5578si1wqzgrx7nwy52d3k8qllx1zbnkqai8sbrhpfpmrg250") (f (quote (("std" "plotters") ("default"))))))

