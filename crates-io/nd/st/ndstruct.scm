(define-module (crates-io nd st ndstruct) #:use-module (crates-io))

(define-public crate-ndstruct-1.0.0 (c (n "ndstruct") (v "1.0.0") (d (list (d (n "cl-aux") (r "^1.0") (f (quote ("serde"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1ivjgv1gs7v0r6hp72mzyp2ll00sany51dkpizyb6ma1qyn9sx49") (f (quote (("std" "alloc") ("default") ("alloc" "cl-aux/alloc")))) (s 2) (e (quote (("serde" "dep:serde") ("rayon" "dep:rayon") ("rand" "dep:rand"))))))

(define-public crate-ndstruct-2.0.0 (c (n "ndstruct") (v "2.0.0") (d (list (d (n "cl-aux") (r "^2.0") (f (quote ("serde"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1ghcs6bngaxww7rwz3n4gnq6flghvjz3a8f6z0d6a93an55jyzz6") (f (quote (("std" "alloc") ("default") ("alloc" "cl-aux/alloc"))))))

(define-public crate-ndstruct-2.1.0 (c (n "ndstruct") (v "2.1.0") (d (list (d (n "cl-aux") (r "^3.0") (f (quote ("serde"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "04yrkdxba49n9zngy68sly6q55swn90p8b5rqscb8143a4d0gx2r") (f (quote (("std" "alloc") ("default") ("alloc" "cl-aux/alloc"))))))

(define-public crate-ndstruct-2.2.0 (c (n "ndstruct") (v "2.2.0") (d (list (d (n "cl-aux") (r "^4.0") (f (quote ("serde"))) (k 0)) (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "01wvc6hwvacp3hrm6h1i7wgw0p2as81450ri0x962hh5mp4rzmqx") (f (quote (("std" "alloc") ("default") ("alloc" "cl-aux/alloc"))))))

