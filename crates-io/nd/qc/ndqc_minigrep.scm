(define-module (crates-io nd qc ndqc_minigrep) #:use-module (crates-io))

(define-public crate-ndqc_minigrep-0.1.0 (c (n "ndqc_minigrep") (v "0.1.0") (h "1nsrjsi04paa6517l81x2qw8zcq1rssqx5dm8fyxniyl5n8sw67m") (y #t)))

(define-public crate-ndqc_minigrep-0.1.1 (c (n "ndqc_minigrep") (v "0.1.1") (h "0yh4d93ywy5n7p30vfpimh4gnbihm1sf9vy822sp94wv7cs0vr0h")))

