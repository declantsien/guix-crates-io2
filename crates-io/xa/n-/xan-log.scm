(define-module (crates-io xa n- xan-log) #:use-module (crates-io))

(define-public crate-xan-log-0.1.0 (c (n "xan-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0ykaph7jb4mxc2k97m203j9b080bg4ikkr829sffsk966nhv9j0n")))

(define-public crate-xan-log-0.1.1 (c (n "xan-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0m3rprbsgyhv4dgrr8n8hklawsj0avc3pqwfy6vvzjiwsgbxm2a6")))

