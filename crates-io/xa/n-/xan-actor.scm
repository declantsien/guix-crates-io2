(define-module (crates-io xa n- xan-actor) #:use-module (crates-io))

(define-public crate-xan-actor-1.1.0 (c (n "xan-actor") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0xv7z3cyvmsgi2y19f5kyfd7lbw28mxm9png7b4wvkrylj3ins1n") (f (quote (("sync")))) (y #t) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.1.1 (c (n "xan-actor") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1kkl50adjnnvfsd378c39jgz52dr0dy710irl6sk6210ffyqhqlz") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.1.2 (c (n "xan-actor") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1spvy3p19vn6qca84xxq5ij0qiv9hbx4ry9yyrnzrv1idxnd2hhl") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.1.3 (c (n "xan-actor") (v "1.1.3") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0569jj9f435p7g3qlxp6i1wzc7rchfp8sl37i6wlmjrknp7r949k") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.1.4 (c (n "xan-actor") (v "1.1.4") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "00789rrmwp1wlzkff96cgkp8avvn7s8fd572i17jsdvzq4d841dd") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.0 (c (n "xan-actor") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0s9mwi0fnd13q01b04ld1w1rd37f2ra5mx21mrcszsfcklqsdzvj") (f (quote (("sync")))) (y #t) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.1 (c (n "xan-actor") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0xyj7yqrp54a76rlr1vdwv02rn7r9ny9b9kscsbmacmilic723my") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.2 (c (n "xan-actor") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0yfhk759881jj3s5n21ad7a8vqy5hdj0vldzwb5k52qddb0vzm3p") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.3 (c (n "xan-actor") (v "1.2.3") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0x38ndqwc2pbxpivd3jp7hfbdf1qay00ia7mf1dz2hjzya8pa3n4") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.4 (c (n "xan-actor") (v "1.2.4") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0c5jk2dk2qjp5m73sp8ajky1i1n200q7a93fhg08yyhskrgpp68k") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.5 (c (n "xan-actor") (v "1.2.5") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0flz2wai6k2i5bjdjpcqgpf66x55cvyvmr7l568vgmdsgk6qai0w") (f (quote (("sync")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:async-trait"))))))

(define-public crate-xan-actor-1.2.6 (c (n "xan-actor") (v "1.2.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ggavdjclllal27n0h832670c1iy0j2rikpczr8icvjkphxddkac")))

(define-public crate-xan-actor-1.2.7 (c (n "xan-actor") (v "1.2.7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ihm16yw4vgddaik2bxdi807mi4xddp38qk2pl3ffz6635g4w278")))

