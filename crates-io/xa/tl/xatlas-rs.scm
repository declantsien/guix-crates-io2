(define-module (crates-io xa tl xatlas-rs) #:use-module (crates-io))

(define-public crate-xatlas-rs-0.1.0 (c (n "xatlas-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "194y87ay2jysdymj1b3mxnm7jv480dh29vbyjgfc81sd59ga6s1w") (f (quote (("generate_bindings"))))))

(define-public crate-xatlas-rs-0.1.1 (c (n "xatlas-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0npsax5bj1xcn94qcvaxh6w5n9rbhcifqwwzqd2icfzvimx3g3kx") (f (quote (("generate_bindings"))))))

(define-public crate-xatlas-rs-0.1.3 (c (n "xatlas-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "0v6anwa40g9nlm71x723jvsvhhb68x9dgz7k8s684dk3al3m2fsm") (f (quote (("generate_bindings"))))))

