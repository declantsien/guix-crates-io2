(define-module (crates-io xa nd xandikos) #:use-module (crates-io))

(define-public crate-xandikos-0.2.8 (c (n "xandikos") (v "0.2.8") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0vqc33gkq87v945dwhmnmln9hrl5dkbp934c7s8bvzqfwyvfgg66")))

(define-public crate-xandikos-0.2.10 (c (n "xandikos") (v "0.2.10") (d (list (d (n "clap") (r "<=4.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05yn993g41hc8lwjs7xgs1mxn86pgn0k2da8xqy23p3sfasvss0s")))

