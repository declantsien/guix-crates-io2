(define-module (crates-io xa nd xand-address) #:use-module (crates-io))

(define-public crate-xand-address-0.4.2 (c (n "xand-address") (v "0.4.2") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "xand-utils") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "sp-core") (r "^10.0.0") (d #t) (k 2)))) (h "1fv3hc0n872rcgfrf8nqd2b9420bx1c3777kayz2s2mk7pmq8gfn")))

