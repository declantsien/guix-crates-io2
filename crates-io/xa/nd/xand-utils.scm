(define-module (crates-io xa nd xand-utils) #:use-module (crates-io))

(define-public crate-xand-utils-1.0.0-beta.28 (c (n "xand-utils") (v "1.0.0-beta.28") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.1") (d #t) (k 0)))) (h "0sx40bz6mx5b83qcjsd9wjilkq4x60zlcgxpzg7s3fyiijxx3r58")))

(define-public crate-xand-utils-1.0.0 (c (n "xand-utils") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.1") (d #t) (k 0)))) (h "0j3svafs44hsxkr205yva2hdd98w2g0351lpcaw0x52yndpw2dnl")))

