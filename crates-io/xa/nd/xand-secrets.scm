(define-module (crates-io xa nd xand-secrets) #:use-module (crates-io))

(define-public crate-xand-secrets-0.4.2-beta.38 (c (n "xand-secrets") (v "0.4.2-beta.38") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gyphgdwxkkipczjnkmfzl6dp77ikbvqcs6yz6pa0411gyax5cqh")))

(define-public crate-xand-secrets-0.4.2 (c (n "xand-secrets") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lv0szi8bw2103i3c314f34cjg13q6k9nwl2skvhgbh5cz1plwmi")))

