(define-module (crates-io xa nd xand_money) #:use-module (crates-io))

(define-public crate-xand_money-2.0.4 (c (n "xand_money") (v "2.0.4") (d (list (d (n "rust_decimal") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g1zni8wy2xw29z2zpy3vdn0vmy4hrp9dbjavc06i70812vs2aw2")))

