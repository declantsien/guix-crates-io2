(define-module (crates-io xa tt xattr) #:use-module (crates-io))

(define-public crate-xattr-0.1.0 (c (n "xattr") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "000lav816acknjg0wchhch8f8g8kpars4zg45hqjhm2ndbvxqjx9")))

(define-public crate-xattr-0.1.1 (c (n "xattr") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1rqi30nbahrw0qib6agpk2ixfmj0dznsv3p8jp1qqar47glyf2ij")))

(define-public crate-xattr-0.1.2 (c (n "xattr") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "0g0v0wxirrqljpp11bw64s0jw7bcl085dk4cb0c4dmd8hnlqy3dc")))

(define-public crate-xattr-0.1.3 (c (n "xattr") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "103kvvdhyjp6azmljba32y4812msm6gm6mwi4qpcd1hxn4ahwv4v")))

(define-public crate-xattr-0.1.4 (c (n "xattr") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "10ykjd0n1kg0dxicg1m1kahl5zham2z79fykk7yx4jymmlrbq337")))

(define-public crate-xattr-0.1.5 (c (n "xattr") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1nn1vjh4qis8sp8h960fnymjbkh0wy29lr87sk2wvapaq68lnih4")))

(define-public crate-xattr-0.1.6 (c (n "xattr") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1p9abji81s3lahk23qf4lgyk1k8vzpdxyvd5k29klxvrg0y215r3")))

(define-public crate-xattr-0.1.7 (c (n "xattr") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "1z7zjzdbwcvphy8w487xfwsqvgn7f8zbicvwwj2vx2gl8v73hf7s")))

(define-public crate-xattr-0.1.8 (c (n "xattr") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "0bgp111b35mnr69cmkv4yz2my4wr2sqli4zd2nfdjlm9k2bsfcwz")))

(define-public crate-xattr-0.1.9 (c (n "xattr") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "1l15drskhvi1xcq2vas1d44j9isg0j0gs5pqg1n5p65qdz1g1i21")))

(define-public crate-xattr-0.1.10 (c (n "xattr") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "1mrn2643kmq55ji62wwhpdfkmkw7im41baxg4g3g6dk5zplg1mh6") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-0.1.11 (c (n "xattr") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "1z7knqssm1kf8ml7bc9789ayqdfifdmm5n4vkqprlj262f5dw12z") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-0.2.0 (c (n "xattr") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "07g18sjbsxrd8g4mcihpzajfara8rv16gwdqn0mfxp7i78nxj3pj") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-0.2.1 (c (n "xattr") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)))) (h "0hvlfywy1nw9pq0s9i511q987yxl56ch1v3bsq0l72p35nwp7cxb") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-0.2.2 (c (n "xattr") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0k556fb6f5jc907975j9c8iynl2fqz3rf0w6fiig83i4yi0kfk14") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-0.2.3 (c (n "xattr") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1g7s46l5c0g07ywfjp85hwqy59fzpj3xkx45d2q5xsxawnxjc5bd") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.0.0 (c (n "xattr") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jn35gv7cyxjsh4ljy4vxpahlballayazpd48ql55h83r8vk89pa") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.0.1 (c (n "xattr") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "11b93igkwsq88b6m14x63c13zns418njh6ngvg2fbwqzyw4n0s7l") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.1.0 (c (n "xattr") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.25") (f (quote ("fs" "std"))) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1yhfqb1wrg54733si2d5h6pa2x7b23hvkbdm0pgqjimzbxni3c6n") (f (quote (("unsupported") ("default" "unsupported")))) (y #t)))

(define-public crate-xattr-1.1.1 (c (n "xattr") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1bkzik6ilf9wgnwwnfcpbk768zcyrga2pkfv3j81lzchq5papipv") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.1.2 (c (n "xattr") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("fs" "std"))) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("net"))) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0h9c7idi2283k3i8k73gskvxxmmg492fk9mfiqywxpvbwxm44ryk") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.1.3 (c (n "xattr") (v "1.1.3") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("fs" "std"))) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("net"))) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1589flpsx126blns82z423j9cl8pmj4r2199in7xpy715w3ybnm7") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.2.0 (c (n "xattr") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("fs" "std"))) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("net"))) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wd0yb1hynn0shb0im7v84shza1vaq7b6krrricsjzry87k6cici") (f (quote (("unsupported") ("default" "unsupported"))))))

(define-public crate-xattr-1.3.0 (c (n "xattr") (v "1.3.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.29") (f (quote ("fs" "std"))) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("net"))) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1pw8sk5wp8rg50canxh5jlnpz1qyfn5jap9wgy82rb5d3scgckzd") (f (quote (("unsupported") ("default" "unsupported")))) (y #t)))

(define-public crate-xattr-1.3.1 (c (n "xattr") (v "1.3.1") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(any(target_os = \"freebsd\", target_os = \"netbsd\"))") (k 0)) (d (n "linux-raw-sys") (r "^0.4.11") (f (quote ("std"))) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.29") (f (quote ("fs" "std"))) (k 0)) (d (n "rustix") (r "^0.38.28") (f (quote ("net"))) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0kqxm36w89vc6qcpn6pizlhgjgzq138sx4hdhbv2g6wk4ld4za4d") (f (quote (("unsupported") ("default" "unsupported"))))))

