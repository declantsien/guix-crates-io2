(define-module (crates-io xa na xanadu) #:use-module (crates-io))

(define-public crate-xanadu-0.0.0 (c (n "xanadu") (v "0.0.0") (h "0c70ccmycwdpvdhwdsb4xd3sra0bq6cn61nsc48rsvrpmcmiai0i")))

(define-public crate-xanadu-0.0.1 (c (n "xanadu") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0zxpqb55w5h1fd7zyjh7smcrfrd2qawr31l1k57a0l51qs0b30qp")))

(define-public crate-xanadu-0.0.2 (c (n "xanadu") (v "0.0.2") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1s9qbk424rnwg4jy9d7vd2781zm3v5cc1saa532xsw2spw9lsd0s") (f (quote (("test_in_browser"))))))

(define-public crate-xanadu-0.0.3 (c (n "xanadu") (v "0.0.3") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "12kvfq6q2lkmyi3nr5z70l4j6idm88icrpvhqqa5xxz3qb6glvly") (f (quote (("test_in_browser"))))))

(define-public crate-xanadu-0.0.4 (c (n "xanadu") (v "0.0.4") (d (list (d (n "bevy_ecs") (r "^0.13.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "026zwdjl5bcxh5nyrwl1xmcrn961961f6im00lfr2c1qzd4i7n40") (f (quote (("test_in_browser"))))))

(define-public crate-xanadu-0.0.5 (c (n "xanadu") (v "0.0.5") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0ya9p5xn0gbsbhjq24aiq1s9jrqxd4xm3b39zq31pdf2k2phd72y") (f (quote (("test_in_browser"))))))

