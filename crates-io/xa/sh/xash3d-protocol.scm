(define-module (crates-io xa sh xash3d-protocol) #:use-module (crates-io))

(define-public crate-xash3d-protocol-0.1.0 (c (n "xash3d-protocol") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "log") (r "<0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0pkpwcy69hjqr6injqc4757xgn49si1p9s8cd3r2mki3ircsg1sm") (r "1.56")))

