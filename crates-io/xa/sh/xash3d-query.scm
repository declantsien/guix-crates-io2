(define-module (crates-io xa sh xash3d-query) #:use-module (crates-io))

(define-public crate-xash3d-query-0.1.0 (c (n "xash3d-query") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "termion") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "xash3d-protocol") (r "^0.1.0") (d #t) (k 0)))) (h "0ij52sfcfbp1y46xjkzbmiqiifnqjcc13j3mrw1fh6r12w2jqlm9") (f (quote (("default" "color") ("color" "termion")))) (r "1.56")))

