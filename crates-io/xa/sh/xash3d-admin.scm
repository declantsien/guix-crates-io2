(define-module (crates-io xa sh xash3d-admin) #:use-module (crates-io))

(define-public crate-xash3d-admin-0.1.0 (c (n "xash3d-admin") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "<0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "xash3d-protocol") (r "^0.1.0") (d #t) (k 0)))) (h "02irgr6v3z1q4gp57h2m66qj2g4ag3p6j452jpj28i4hqnd85dgn") (r "1.56")))

