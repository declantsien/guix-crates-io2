(define-module (crates-io xa tc xatcoder) #:use-module (crates-io))

(define-public crate-xatcoder-0.1.0 (c (n "xatcoder") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13") (d #t) (k 0)))) (h "06mifa9z4aad6jrla5h8mm7i2b3wld6l697irhciqkvcsmvm94rw")))

