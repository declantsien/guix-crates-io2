(define-module (crates-io xa pp xapphire) #:use-module (crates-io))

(define-public crate-xapphire-0.1.0 (c (n "xapphire") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0w3q28jxpfdskz7i6nq8wp1rvnd4kdqar5jwh9yzm9sisl4y5jv0")))

