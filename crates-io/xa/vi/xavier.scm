(define-module (crates-io xa vi xavier) #:use-module (crates-io))

(define-public crate-xavier-0.1.1 (c (n "xavier") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "xavier-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "xavier-internal") (r "^0.1.1") (d #t) (k 0)))) (h "1zds1swbgnpdi706lx8pp64hzs8y3nnvsqwqrkqk0g8c2mmsm48r") (y #t)))

(define-public crate-xavier-0.1.2 (c (n "xavier") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "xavier-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "xavier-internal") (r "^0.1.2") (d #t) (k 0)))) (h "1xnikr71ldmbfpnnzg3yd55gfficq92jlxsa14v70qxdd37q9pb6")))

(define-public crate-xavier-0.1.3 (c (n "xavier") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "xavier-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "xavier-internal") (r "^0.1.2") (d #t) (k 0)))) (h "03m4a4v48aiy8v3fvqn8dqpmflc59iv7kzfgi00w7ngsnnyx2gf8")))

