(define-module (crates-io xa vi xavier-derive) #:use-module (crates-io))

(define-public crate-xavier-derive-0.1.1 (c (n "xavier-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1q1v1lc1ylrj69mnhymmp736wif3r2cd04y2xi2rxacbaafl88np") (y #t)))

(define-public crate-xavier-derive-0.1.2 (c (n "xavier-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0xnjl8fq92jh7dwgzxlz2kbi87ja9mfjn8hfgfglgvs14l0jicp6")))

(define-public crate-xavier-derive-0.1.3 (c (n "xavier-derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "1iljhkfkgfshidxb5fcrg12rbpm5h6mb5y2d570lyq62qjc8rws5")))

