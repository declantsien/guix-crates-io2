(define-module (crates-io xa vi xavier-internal) #:use-module (crates-io))

(define-public crate-xavier-internal-0.1.1 (c (n "xavier-internal") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)))) (h "1byzgbgaaji4q4s0v40lp8plc5wbrpiwcfd50jxd1zcbar53x7yz") (y #t)))

(define-public crate-xavier-internal-0.1.2 (c (n "xavier-internal") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)))) (h "1capz9fvbvk654lqas7yhlbwjgnljpii8ywxvlp0cqr5n18gj93n")))

