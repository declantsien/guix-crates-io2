(define-module (crates-io xa rc xarc) #:use-module (crates-io))

(define-public crate-xarc-0.1.0 (c (n "xarc") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 2)))) (h "0c35clf3dc65p384ra6lysmwfw4ni0saf7l0740mr7v06azmmgi0") (y #t)))

(define-public crate-xarc-0.2.0 (c (n "xarc") (v "0.2.0") (d (list (d (n "crossbeam-epoch") (r ">=0.6.0, <0.9.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r ">=0.1.0, <0.3.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "jemallocator") (r ">=0.1.8, <0.4.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 2)) (d (n "rayon") (r ">=0.7.0, <1.5.0") (d #t) (k 2)))) (h "1zk6zz4nfvhr8qn9hwif0f1bgd3p67f448iq6hl322kfq8qf4v89")))

(define-public crate-xarc-0.3.0 (c (n "xarc") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r ">=0.6.0, <0.10.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r ">=0.1.0, <0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r ">=0.7.0, <0.9.0") (d #t) (k 0)) (d (n "jemallocator") (r ">=0.1.8, <0.4.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 2)) (d (n "rayon") (r ">=0.7.0, <1.6.0") (d #t) (k 2)))) (h "0wch28i6rmjygwj5znf6vf7fdz83fksc1xkf9cyyr1j467pnh70v")))

