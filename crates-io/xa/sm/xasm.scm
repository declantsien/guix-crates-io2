(define-module (crates-io xa sm xasm) #:use-module (crates-io))

(define-public crate-xasm-0.1.0 (c (n "xasm") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.1.0") (d #t) (k 0)))) (h "17mk96wh6b7v6310qfxb1ipixk006yfkcyafdk1gshw9fclfvs4z")))

(define-public crate-xasm-0.1.1 (c (n "xasm") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.1.0") (d #t) (k 0)))) (h "0b1hn55wb70j5nx4hsr65zslv74y97rdjbaid6aymc5qv9907l4n")))

(define-public crate-xasm-0.1.2 (c (n "xasm") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.1.0") (d #t) (k 0)))) (h "13adabnwqwwls0vr4651ip8rlg9z5888qb9zcqb883hwi2hdf8vx")))

(define-public crate-xasm-0.1.3 (c (n "xasm") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.1.0") (d #t) (k 0)))) (h "18k46v0rxpfcrgwy7qfmdifvfh21q4sy210q5xfm1fqmbjca8dbz")))

(define-public crate-xasm-0.2.0 (c (n "xasm") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2") (d #t) (k 0)))) (h "17rrpjfn4kw4rl6gdc9wk45mp1g99rcma36fv6rv3v3k036y0rwq")))

(define-public crate-xasm-0.2.1 (c (n "xasm") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2.1") (d #t) (k 0)))) (h "0hsgh87ly1z1vj8ap2439mdvrmc2jgs8if5cyq85ba6cmwyxbv5r")))

(define-public crate-xasm-0.2.2 (c (n "xasm") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2.1") (d #t) (k 0)))) (h "03d8f1v75w8a9ajm2dsh2lxjnj7iawycx5y252f5ibqd3zlhqls9")))

(define-public crate-xasm-0.2.3 (c (n "xasm") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2.5") (d #t) (k 0)))) (h "1mhjgvqwpvcxq9zvvf97y401yzb0ya98zw8vsm7f81hyia2969as")))

(define-public crate-xasm-0.2.4 (c (n "xasm") (v "0.2.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2.5") (d #t) (k 0)))) (h "061w705q6nvln5vq3ln3mlp3jjbfqspgxaf9wvip9bda1q49bb6h")))

(define-public crate-xasm-0.3.0 (c (n "xasm") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.2.5") (d #t) (k 0)))) (h "1n8rs2qbwx1y22wsnbfhffshwxwx26ggx5qh1maj7bhbqkygajvi")))

(define-public crate-xasm-0.3.1 (c (n "xasm") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.3.0") (d #t) (k 0)))) (h "01m25pb40gmlxahm7gymz24w47x67fpab24llknvw6jx513km6mz")))

(define-public crate-xasm-0.3.2 (c (n "xasm") (v "0.3.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.3.0") (d #t) (k 0)))) (h "06cripp0azn6j706jpccc2vqbvlpwcaj15rvc1zp7przl4mv18pc")))

(define-public crate-xasm-0.4.1 (c (n "xasm") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.4.1") (d #t) (k 0)))) (h "06divxswsy0cv52li562r2gr5bpm0grwcakyj1ik4ijs5zsyjkil")))

(define-public crate-xasm-0.4.2 (c (n "xasm") (v "0.4.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "xassembler") (r "^0.4.1") (d #t) (k 0)))) (h "1nnli65lgfjwbfrpnyrfw7xialryi7xzy3kiwjn0k5y59d0sd4pa")))

(define-public crate-xasm-0.5.0 (c (n "xasm") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.0") (d #t) (k 0)))) (h "0kzfwmzzp7zdcacm3b5vrbkziarp7lknaczmsjibbicg96wf70dm")))

(define-public crate-xasm-0.5.1 (c (n "xasm") (v "0.5.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0zkyipr43gndsgia7vx068vcgcyk5anmp12733fpx18rbvrg3chd")))

(define-public crate-xasm-0.5.2 (c (n "xasm") (v "0.5.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "17dlmaz3faa3rp50xbhr56sds21xq6pgcrdck0dishmhhd2izlp6")))

(define-public crate-xasm-0.5.3 (c (n "xasm") (v "0.5.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0j8vbabsmgc9cm9an9473nbdj5m9szga60a444dfyfdr87i10s6m")))

(define-public crate-xasm-0.5.4 (c (n "xasm") (v "0.5.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "1saabwsqkr1x2vq1hcagwbqcycs89pbcva1n0yiyx54p37p2abzx")))

(define-public crate-xasm-0.5.5 (c (n "xasm") (v "0.5.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0fvz9cssj842ida0ybpz59arkgw631bsfj71fgygpsfc8mk00cg7")))

(define-public crate-xasm-0.5.6 (c (n "xasm") (v "0.5.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0rb1agkviczjdjyq69z1g2985xccl8vwz3b2jfxp1islxcvi7zac")))

(define-public crate-xasm-0.5.7 (c (n "xasm") (v "0.5.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "1fj609lx7hgwzi6mbgfmgf6cd8xm85asbjyj175j87k0fkjr6j4a")))

(define-public crate-xasm-0.5.8 (c (n "xasm") (v "0.5.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0cn6fk34qzn3n6ksbdf41xi63j01k5aw2fhkdmgx2dxxcija7gbx")))

(define-public crate-xasm-0.5.9 (c (n "xasm") (v "0.5.9") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "0dmc9zhlqii5rxr4lcnzgnmrp9p2x33p0jb18gaxlzpq9z1cw5ch")))

(define-public crate-xasm-0.5.10 (c (n "xasm") (v "0.5.10") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1") (d #t) (k 0)) (d (n "xassembler") (r "^0.5.1") (d #t) (k 0)))) (h "1wbk0h82wxc2qy2ghar6lbwxdi54rsfypml7bn2irgysfnm16c3f")))

