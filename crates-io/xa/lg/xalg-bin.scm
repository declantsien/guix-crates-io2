(define-module (crates-io xa lg xalg-bin) #:use-module (crates-io))

(define-public crate-xalg-bin-0.1.0 (c (n "xalg-bin") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lib_xalg") (r "^0.1") (d #t) (k 0)))) (h "1w0pri8vakvxlf9qky3rd75gxsp7rk05w3wl90wrn5mxr58baxl3")))

(define-public crate-xalg-bin-0.1.1 (c (n "xalg-bin") (v "0.1.1") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lib_xalg") (r "^0.1") (d #t) (k 0)))) (h "1xbdkfa603qicb1z46vkjfg6iqr1qxh19yb12x7pmicz1la6mvlx")))

(define-public crate-xalg-bin-0.1.2 (c (n "xalg-bin") (v "0.1.2") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lib_xalg") (r "^0.1") (d #t) (k 0)))) (h "0v3kdar4ddm5aga07gg7mcd5bfah62hw8xaq8zcy7cs4bb0agvpl")))

(define-public crate-xalg-bin-0.2.0 (c (n "xalg-bin") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lib_xalg") (r "^0.2") (d #t) (k 0)))) (h "18g6apzkp8yzh34czlk3gq5zzmqadw6j6vm7j5dfj8k9whx4z4lk")))

(define-public crate-xalg-bin-0.2.1 (c (n "xalg-bin") (v "0.2.1") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lib_xalg") (r "^0.3") (d #t) (k 0)))) (h "14j31hr5mppskl8a1ii2lamyvmp8pss1000da3s8ymz4vq0i9979")))

