(define-module (crates-io xa ll xalloc) #:use-module (crates-io))

(define-public crate-xalloc-0.1.0 (c (n "xalloc") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0zm5sgxqws459fz1595qpy4jgxh35f2zpkykpkbq5g144n71hl76") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.0 (c (n "xalloc") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0kqc7qzpbcvqhfs3kpc639ilvvy6xza8792syv4ldwgid6axbmc1") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.1 (c (n "xalloc") (v "0.2.1") (d (list (d (n "num-integer") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1z2hcmymdq0c6j74xijxkailk9257zzidb0ixml6gbmv70qz0yhg") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.2 (c (n "xalloc") (v "0.2.2") (d (list (d (n "num-integer") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0zn00impg1bvnylznvg46xlvarsqbf48yia7bzyv66v33bq96gi9") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.3 (c (n "xalloc") (v "0.2.3") (d (list (d (n "num-integer") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "07vcnwg48l0bkx6hsys88m8dxj0ydnbr4fli2rw6jwwrh17c9702") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.4 (c (n "xalloc") (v "0.2.4") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0ar5fq5mjamg6r2ynpx6svrkl3ss26xiw54rcrni867mn7lszakw") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.5 (c (n "xalloc") (v "0.2.5") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1zy2b0391xl7na28fbr957wiml6f173m33y7cb63zw5pvrhav3lx") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.6 (c (n "xalloc") (v "0.2.6") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1bl6n56w8bhk5aizrpx1liys75xscas70zcqk63iy5ryi5qgdl9w") (f (quote (("nightly"))))))

(define-public crate-xalloc-0.2.7 (c (n "xalloc") (v "0.2.7") (d (list (d (n "num") (r ">=0.2.0, <0.4.0") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "16cw658j8mjskqdd1w3pp8mb4a6anwrwwjicilfzyjcaszs6ac2p") (f (quote (("nightly"))))))

