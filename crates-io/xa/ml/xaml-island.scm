(define-module (crates-io xa ml xaml-island) #:use-module (crates-io))

(define-public crate-xaml-island-0.1.0 (c (n "xaml-island") (v "0.1.0") (d (list (d (n "windows") (r ">=0.30, <=0.35") (f (quote ("alloc" "Win32_Foundation" "Win32_UI_WindowsAndMessaging" "Win32_System_WinRT_Xaml" "UI_Xaml_Hosting"))) (d #t) (k 0)))) (h "0wjng3908jdg4vvqvdjgjv068hii78l4xqvp358f1hji5q2k110z")))

