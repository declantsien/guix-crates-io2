(define-module (crates-io xa gr xagrs) #:use-module (crates-io))

(define-public crate-xagrs-0.1.0 (c (n "xagrs") (v "0.1.0") (h "1z92h49pcrflj9vdplf6nvk4l5gx9ljim7my24pipdr8nlngd5jp")))

(define-public crate-xagrs-0.2.0 (c (n "xagrs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0padfspg5kxp4y183y9qqi1bdkrxyq06p04bk1kqkdrmjxwgqa84")))

