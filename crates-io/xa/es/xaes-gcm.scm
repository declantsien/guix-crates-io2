(define-module (crates-io xa es xaes-gcm) #:use-module (crates-io))

(define-public crate-xaes-gcm-0.1.0 (c (n "xaes-gcm") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "nonce-extension") (r "^0.1.2") (d #t) (k 0)))) (h "0by1i4z0bbf5ahnjgamn4jqhjbsb1ib5nxvc9ankgdd50mj95zh0")))

(define-public crate-xaes-gcm-0.2.0 (c (n "xaes-gcm") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (d #t) (k 0)) (d (n "nonce-extension") (r "^0.2.0") (d #t) (k 0)))) (h "0r95d3ah80wy0prdvb3qmwjvd9c53fvr77akmhcg1b24ayxnkssp")))

