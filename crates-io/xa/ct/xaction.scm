(define-module (crates-io xa ct xaction) #:use-module (crates-io))

(define-public crate-xaction-0.1.0 (c (n "xaction") (v "0.1.0") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "13n39kpqmil2748wlxbb72y7l1vq7548i9x4kk00il913lmxbpl5")))

(define-public crate-xaction-0.2.0 (c (n "xaction") (v "0.2.0") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "1n9nlhfpfll8mbnyy8nz70w4mi22pxwrgzg6wz1fspacbp9irjpd")))

(define-public crate-xaction-0.2.1 (c (n "xaction") (v "0.2.1") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "12l31x8qi11hvz3n01l49k7v6radm3876akijrgkqd8s1i221p1s")))

(define-public crate-xaction-0.2.2 (c (n "xaction") (v "0.2.2") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "0bd8ixp7m8nkn2azz8ax61x1ychpbj1d0mx6idq0w1v9fl19jaxh")))

(define-public crate-xaction-0.2.3 (c (n "xaction") (v "0.2.3") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "1g15jv5xl67hhvin88a47cj7d541zrjmb2w8yfqmcj8ba1p9wf3a")))

(define-public crate-xaction-0.2.4 (c (n "xaction") (v "0.2.4") (d (list (d (n "xshell") (r "^0.1.6") (d #t) (k 0)))) (h "0smbz5cjy6r1xckc3nfhs1yrm5qsxj0hxr56d7za6bjdjqicqman")))

