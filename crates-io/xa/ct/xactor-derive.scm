(define-module (crates-io xa ct xactor-derive) #:use-module (crates-io))

(define-public crate-xactor-derive-0.1.0 (c (n "xactor-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0pmzkql639i2x60gcmnni1w0dx8yqk6yl9l1nwwq3d6bddb5pbqx")))

(define-public crate-xactor-derive-0.2.0 (c (n "xactor-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1bl5s0r87xk6snril7zmjxk48cb4yyf1jm2jvv2myzlrha9i7y9a")))

(define-public crate-xactor-derive-0.6.6 (c (n "xactor-derive") (v "0.6.6") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1avvp89phi5jm9s5dwq3yisbzrxasm19j8kxxfypf04mjbshn46i")))

(define-public crate-xactor-derive-0.7.0 (c (n "xactor-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "03lq8a3dxbipin001xfv08fafiman29h9c3iv6nw2hpnfscw0qcn")))

(define-public crate-xactor-derive-0.7.1 (c (n "xactor-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1vfbmrf5zh1r4c33bxbib5hasvr2fm0ibcz7p0v6zw7dizkscrff")))

