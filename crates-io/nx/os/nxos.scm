(define-module (crates-io nx os nxos) #:use-module (crates-io))

(define-public crate-nxos-0.0.1 (c (n "nxos") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03vp44lf425sfq1znl2h9sfishzn65c59smpj7jw7vhkh14x1xll")))

