(define-module (crates-io nx -e nx-emulator) #:use-module (crates-io))

(define-public crate-nx-emulator-0.1.0 (c (n "nx-emulator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pixels") (r "^0.7.0") (d #t) (k 0)) (d (n "pixels-u32") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "07abkr6wls1z39bk5f1s5g58bs9fz6nsxxphds4xy2xkiizgv8wn")))

