(define-module (crates-io nx tn nxtnote-configfile-format) #:use-module (crates-io))

(define-public crate-nxtnote-configfile-format-0.1.0-alpha (c (n "nxtnote-configfile-format") (v "0.1.0-alpha") (d (list (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "libsqlite3-sys") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0vidkm5sjv6ak4hp5dxrikz4iq1djimzl2q4ycc7pn6mz4dysqnk")))

(define-public crate-nxtnote-configfile-format-0.1.1-alpha (c (n "nxtnote-configfile-format") (v "0.1.1-alpha") (d (list (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "libsqlite3-sys") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0yvg0h1dzw0275kizi3qqi1h16qsynyk52y7b7rrnk3svs177v1b")))

