(define-module (crates-io nx mp nxmpki) #:use-module (crates-io))

(define-public crate-nxmpki-0.1.0 (c (n "nxmpki") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0hib9cgp4qvz1injb8rrkyzyaql59bvd4gsvma84ibqz4clwk1rl")))

(define-public crate-nxmpki-0.2.1 (c (n "nxmpki") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "12nd7yf1wyr4pvq2lsk1lprpwnx6pgw7pd4z9i70g00fgh1dvps5")))

(define-public crate-nxmpki-0.2.2 (c (n "nxmpki") (v "0.2.2") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0b3l1k39qgk0qln9s2pzvy58r9w49gyrz3nv4iac1b1ify5b9ail")))

(define-public crate-nxmpki-0.2.3 (c (n "nxmpki") (v "0.2.3") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1lapyd4dldg43fm09s77c99bchfijjfnm0qpp2wf3ak1n6f3vd2g")))

(define-public crate-nxmpki-0.2.5 (c (n "nxmpki") (v "0.2.5") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1z5fgzinpzvgffv5iys6fp4n34b08nxkq8d5s6h9x252x1chhgdv")))

(define-public crate-nxmpki-0.2.6 (c (n "nxmpki") (v "0.2.6") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "15cf8miihb3p4na9kn0v193pxgqzwmr5wa4hvaamcdsv9h74p564")))

(define-public crate-nxmpki-0.2.7 (c (n "nxmpki") (v "0.2.7") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0z032d6m5y99d8psd5a059y3x543rgbvq503sm856lzxvb7clpik")))

(define-public crate-nxmpki-0.2.8 (c (n "nxmpki") (v "0.2.8") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0kg0rwplvflmy0c8zg1sjbsk7mi73jlfl9ywnbj5par65qwazmj7")))

(define-public crate-nxmpki-0.2.9 (c (n "nxmpki") (v "0.2.9") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0x647a8xdwvlamxsnlli3g8rc78bsmps13blphgjqpqf3cqx67pm")))

(define-public crate-nxmpki-0.2.10 (c (n "nxmpki") (v "0.2.10") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1z45c7v4mgp69wh7l9dw5pzg0bvm8riahxaaz5s7xij0qx3b4zs7")))

(define-public crate-nxmpki-0.2.11 (c (n "nxmpki") (v "0.2.11") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "15al58g89184h30z3lwkp1cmh0smaag1qnsji7ivvhlp42qkjywb")))

