(define-module (crates-io nx yz nxyz) #:use-module (crates-io))

(define-public crate-nxyz-0.1.0 (c (n "nxyz") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1k9hjb7kizjr3lkgyaqmpm0nyv28r27l07wfvjkll3wmi8ds4cxc")))

(define-public crate-nxyz-1.0.0 (c (n "nxyz") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1k1al4cw93zcqfi3bdbkgyp376wmsm397278gfzs3ikxwg7k2896")))

