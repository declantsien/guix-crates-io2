(define-module (crates-io vi ca vicardi) #:use-module (crates-io))

(define-public crate-vicardi-0.1.0 (c (n "vicardi") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04z9n8a0kchgz5ni0k09pqh1901jf8zkdvk2qla1r8a0a1qd0s2s")))

(define-public crate-vicardi-0.1.1 (c (n "vicardi") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19j040jsi98p66hd2g047q1r7wsmkngmw59q5psfn63w7vb3q5cg")))

(define-public crate-vicardi-0.1.2 (c (n "vicardi") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15v6sffhdqb9gqnci94h8nw8p9d1dglm4n6hacrqia0dmh01zvpc")))

(define-public crate-vicardi-0.1.3 (c (n "vicardi") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yzg7sga2nqqxkfqbn1rc776yd7d6k7xsiyw8x98w75p0zcjbd5b")))

(define-public crate-vicardi-0.1.6 (c (n "vicardi") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13yk59mrdpdjn8cvf3qaim8kcj49hs3qwxc7fcfdx3ksyn6bxbqr")))

(define-public crate-vicardi-0.1.7 (c (n "vicardi") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10vfzyfbk8n1bgjr5385v4854gb3mz33qah07pcia44hjv4lljnm")))

