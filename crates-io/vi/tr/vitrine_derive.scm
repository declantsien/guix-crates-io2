(define-module (crates-io vi tr vitrine_derive) #:use-module (crates-io))

(define-public crate-vitrine_derive-0.1.0 (c (n "vitrine_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1w5v441m664n6pmgpy5d5r7mbra1rk0ymrdzml4ayiq54a62jbi7")))

(define-public crate-vitrine_derive-0.1.1 (c (n "vitrine_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.45") (d #t) (k 0)))) (h "01f5j9hd9s2ajqwysy31r9cdni400x3nwmk5rwz4m1ym4cv67yzx")))

(define-public crate-vitrine_derive-0.1.2 (c (n "vitrine_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1g0qad846dbbbi94axq3yi91jsvbcfflyx4kc4cbnc09a0sk2hvp")))

(define-public crate-vitrine_derive-0.1.3 (c (n "vitrine_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1njdcw5z888yi329jby9bidpmqci5igvvd85yrkdzi0ddf6nf33l")))

(define-public crate-vitrine_derive-0.1.4 (c (n "vitrine_derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "0g6xx574lqqfm39skh2kq1vqr7ricnz5mhq27061ladlk3n53xss")))

