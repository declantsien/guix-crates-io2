(define-module (crates-io vi tr vitree) #:use-module (crates-io))

(define-public crate-vitree-0.1.0 (c (n "vitree") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "skima") (r "^0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MouseEvent"))) (d #t) (k 0)))) (h "18537s72zwzkh55bh2rjp17ibrws6j2580k5admvjw655454r811")))

