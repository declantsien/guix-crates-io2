(define-module (crates-io vi no vino-macros) #:use-module (crates-io))

(define-public crate-vino-macros-0.6.2 (c (n "vino-macros") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vino-transport") (r "=0.6.2") (d #t) (k 0)))) (h "11k7r443b8v6b4m139brnzl6f7ca99bpq1ghzcbpy7rmc37ixa6i")))

(define-public crate-vino-macros-0.7.0 (c (n "vino-macros") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vino-transport") (r "^0.7.0") (d #t) (k 0)))) (h "02cdq2fggb2rskxsljpd2xvsg2qp4afy06dr3w069cmbl6fx2f0x")))

(define-public crate-vino-macros-0.8.0 (c (n "vino-macros") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wakcbbk1vy5nbfp6g0ch8rpxdjff1xmdq0jhgqn5314qb40xz9y")))

(define-public crate-vino-macros-0.9.0 (c (n "vino-macros") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fv9igmvdgh1qjd236w6lw9y4v9i1ama6zaryg0m4c9cy5m262sx")))

