(define-module (crates-io vi no vino-wapc) #:use-module (crates-io))

(define-public crate-vino-wapc-0.8.0 (c (n "vino-wapc") (v "0.8.0") (h "05vi52pnxbrmr290s9z7szavw5sa1glqc85jf3i10q4ns37yr5a3") (f (quote (("guest") ("default"))))))

(define-public crate-vino-wapc-0.9.0 (c (n "vino-wapc") (v "0.9.0") (h "1yv7l0988mli16vqqvanjapqqfd15994hwih8krlk30z3hgxqg2c") (f (quote (("guest") ("default"))))))

