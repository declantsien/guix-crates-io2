(define-module (crates-io vi no vino-entity) #:use-module (crates-io))

(define-public crate-vino-entity-0.6.2 (c (n "vino-entity") (v "0.6.2") (d (list (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "vino-macros") (r "=0.6.2") (d #t) (k 0)))) (h "0pl2lkdcr4wp2l2k2c3cgmfxn1bwi6ps9260ff08h76m5m5w6zgm")))

(define-public crate-vino-entity-0.7.0 (c (n "vino-entity") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "vino-macros") (r "^0.7.0") (d #t) (k 0)))) (h "1ib81xs3grfbl79g1n58lpghl4dhq984dqrff3619bdzbl26r3cd")))

(define-public crate-vino-entity-0.8.0 (c (n "vino-entity") (v "0.8.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "vino-macros") (r "^0.8.0") (d #t) (k 0)))) (h "1k548p0p85c4dhpk7mg33jp2b6i0axlfpgg5hz1ja3mnjm8sam8k")))

(define-public crate-vino-entity-0.9.0 (c (n "vino-entity") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "vino-macros") (r "^0.9.0") (d #t) (k 0)))) (h "18mpdxixy01jag8cjcj16x6z1mm6wr0rmj9ahwr49xd1zm5vh57i")))

