(define-module (crates-io vi no vino-types) #:use-module (crates-io))

(define-public crate-vino-types-0.6.2 (c (n "vino-types") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r33pcgcydcwm5x3v8m839s5i1j6prf962yh04ppqhhq0n4xawqq")))

(define-public crate-vino-types-0.7.0 (c (n "vino-types") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bwg5w7pg7n26lih75by9h92z6nki90lnksyxnwpnf8alqa6gs1d")))

(define-public crate-vino-types-0.8.0 (c (n "vino-types") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-env-log") (r "^0.2") (d #t) (k 2)))) (h "0zpqzgkw5ravbkb47n8qlpknd4il324144piy4c1ribfv7mnrw7x")))

(define-public crate-vino-types-0.9.0 (c (n "vino-types") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-env-log") (r "^0.2") (d #t) (k 2)))) (h "1gj7f9pi763zhj6gh17frk0lsv2fy28iy3dilxsf356kxh5v228x")))

