(define-module (crates-io vi m_ vim_edit) #:use-module (crates-io))

(define-public crate-vim_edit-0.1.0 (c (n "vim_edit") (v "0.1.0") (d (list (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0slm4g4iviaggig9rzn2510rqaxahfayhww0ilj427bykc91g7rj")))

(define-public crate-vim_edit-0.1.1 (c (n "vim_edit") (v "0.1.1") (d (list (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "144fg7fa3xp2wgd3cy3vwvq10ifjm7cnnf8rphs1ip54kf4zlhvh")))

