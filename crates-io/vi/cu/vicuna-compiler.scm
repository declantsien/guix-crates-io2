(define-module (crates-io vi cu vicuna-compiler) #:use-module (crates-io))

(define-public crate-vicuna-compiler-0.1.1 (c (n "vicuna-compiler") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "chumsky") (r "^0.9.2") (f (quote ("std"))) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "miette") (r "^5.8.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "18ggw1fzarcbqzcp170g4s4s7bj5mrxg5gkyca4x0dk836gndyik")))

