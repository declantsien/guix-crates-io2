(define-module (crates-io vi cu vicuna-runtime) #:use-module (crates-io))

(define-public crate-vicuna-runtime-0.1.1 (c (n "vicuna-runtime") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0y5slwhln0d1gfn7nbq2w941k473d3jwbmrjbnpp7vqwlc2pqgp2")))

