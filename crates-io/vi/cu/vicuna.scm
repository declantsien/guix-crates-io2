(define-module (crates-io vi cu vicuna) #:use-module (crates-io))

(define-public crate-vicuna-0.1.0 (c (n "vicuna") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "1v1l30wx4vjgq7k5w9rca4g8640375a7lnq4bj2qmj73vjj8l2rl")))

(define-public crate-vicuna-0.1.1 (c (n "vicuna") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "125q23i815l31kggjq5vxws906j633ylcsrj8j6qwsmjzlij7xv6")))

(define-public crate-vicuna-0.1.2 (c (n "vicuna") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "02zd3rlc30lw96mpq7ilrl4r4vj7vhpy573xjymwai8vgk6zgm97")))

(define-public crate-vicuna-0.1.3 (c (n "vicuna") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "1gzs6d942wcg3sq9f732nipjzc7vgmy0z8fqmywa5idpxhh4611m")))

(define-public crate-vicuna-0.2.0 (c (n "vicuna") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "1nxckd3yd7zf6k2bsp1d5w37fiam9gzhmr97v9lv6wpbjp3zwpaa")))

(define-public crate-vicuna-0.3.0 (c (n "vicuna") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "1ah1384sbfjii8yqfkxq9pv2wy67vhw5d9dzl3a6lli3fpi34dzk")))

(define-public crate-vicuna-0.4.0 (c (n "vicuna") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "1fspndd0z3g93lgljlvmzwwnw4d64gh448d3wnwr9fh0whs0cbm2")))

(define-public crate-vicuna-0.4.1 (c (n "vicuna") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lambda_http") (r "^0.1.1") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)))) (h "04khi4jb4vg2rjcik1v8bnkl9vgnsdc5i002ynyygkmzj8pqfq1a")))

