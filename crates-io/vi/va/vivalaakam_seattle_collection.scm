(define-module (crates-io vi va vivalaakam_seattle_collection) #:use-module (crates-io))

(define-public crate-vivalaakam_seattle_collection-0.1.0 (c (n "vivalaakam_seattle_collection") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wbicv5k841gdaxd0914p8vwszr02w8paydl951llzqiv1ghafx4")))

