(define-module (crates-io vi am viam-mdns) #:use-module (crates-io))

(define-public crate-viam-mdns-3.0.1 (c (n "viam-mdns") (v "3.0.1") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1xl22b03vbkbm4k2pvblf45gi5rvcsn9fz646qd96m8472b80qpd")))

