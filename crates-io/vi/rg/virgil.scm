(define-module (crates-io vi rg virgil) #:use-module (crates-io))

(define-public crate-virgil-0.1.0 (c (n "virgil") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "mustache") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "1m1qzp2s6ddxx5yd88jgk6yc32qawsfiiqzizwaqr71mcz1bqgdq")))

(define-public crate-virgil-0.2.1 (c (n "virgil") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "frontmatter") (r "^0.3") (d #t) (k 0)) (d (n "mustache") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "012zg22yxqq35lxypcfggmys9mq1a5j5nj46rr1fidy1pd52ypfz")))

(define-public crate-virgil-0.2.2 (c (n "virgil") (v "0.2.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "frontmatter") (r "^0.3") (d #t) (k 0)) (d (n "mustache") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0n38564zc82ymwb42fizq6wrxl452m6rkdqh91hkpvwjmvg477q0")))

(define-public crate-virgil-0.2.3 (c (n "virgil") (v "0.2.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "frontmatter") (r "^0.3") (d #t) (k 0)) (d (n "mustache") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0k98a7njkyb5bbzraa3a69gw5iqahsia102irqs8hgl52xgf7wbm")))

