(define-module (crates-io vi rv virvadb) #:use-module (crates-io))

(define-public crate-virvadb-0.1.0 (c (n "virvadb") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)))) (h "1q0bv49zs4a9rbxhkci0axs5ki4437lqranhqymyyhizly38yfzv")))

(define-public crate-virvadb-0.1.1 (c (n "virvadb") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)))) (h "0acijjl9abf8v2kdmchalpmjj2112rih6hindmp0pfc6w06067y3")))

