(define-module (crates-io vi da vidar) #:use-module (crates-io))

(define-public crate-vidar-0.1.0 (c (n "vidar") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)))) (h "1lw4gq8vidnk96z02hysg1h4qkrj2lsqby746xy16k0kpq2wx165")))

