(define-module (crates-io vi fi vifi-prompt) #:use-module (crates-io))

(define-public crate-vifi-prompt-0.1.0 (c (n "vifi-prompt") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1vi0qzk4nqml16jc9axkn65lvc8m289r8lx1wivb4h8iaw6mxkpb")))

(define-public crate-vifi-prompt-0.1.1 (c (n "vifi-prompt") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "0xsi8ycmfha9kdxfmvkjficr8awz7sk3nxyx9089044s2c19gqf3")))

(define-public crate-vifi-prompt-0.2.0 (c (n "vifi-prompt") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "0jibv2y0r64yy6ndsb7lpm4s2hha10yyh13af6jzqjakl2yspf11")))

(define-public crate-vifi-prompt-0.2.1 (c (n "vifi-prompt") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1iwzxlhfc4qidkr2v5ly0jf3gkhg303i6v8bayw1pf8imxsalf00")))

(define-public crate-vifi-prompt-0.2.2 (c (n "vifi-prompt") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1zy7gwgqx9a171nrh33f62yqi7637mkyw1r0fvr39qcd042zhdnb")))

(define-public crate-vifi-prompt-0.2.3 (c (n "vifi-prompt") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "045ysacifa5figfr3k9zcbazfg9v7blym1qwav1fp2i4iz32sc8h")))

(define-public crate-vifi-prompt-0.2.4 (c (n "vifi-prompt") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1z696kmr6jspw5bimvnp5gl8irdgmpz5d16q8yybvmlh8cmbh3xi")))

(define-public crate-vifi-prompt-0.2.5 (c (n "vifi-prompt") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "0xvfhl8qf53y1ccfzr3fw9h521jz3aym8qazcrs2ayq7r6g1i4i3")))

(define-public crate-vifi-prompt-0.2.6 (c (n "vifi-prompt") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1njlm5bw5k9wkbdn4369c7zkmi7ibbvnd21z896k8n3s53lbzrg1")))

(define-public crate-vifi-prompt-0.2.7 (c (n "vifi-prompt") (v "0.2.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "0sr7vajkl8q6rr8cws4qrrm8avx3s45hc78mhk9x0ggilhk86g33")))

(define-public crate-vifi-prompt-0.2.8 (c (n "vifi-prompt") (v "0.2.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1y8mxlxknalg3w3ic4l3x3lkdyc5cp9dsl90z0aq3ix51kjbxig7")))

