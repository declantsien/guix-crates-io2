(define-module (crates-io vi ft vifterpreter) #:use-module (crates-io))

(define-public crate-vifterpreter-0.1.0 (c (n "vifterpreter") (v "0.1.0") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)))) (h "0qjx2lxzh9c4fki2l3d2l9vkbjiaspps5v38ysgc47liaqvh5a4j")))

