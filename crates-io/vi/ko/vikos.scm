(define-module (crates-io vi ko vikos) #:use-module (crates-io))

(define-public crate-vikos-0.1.1 (c (n "vikos") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0l6460vdgb4mw777zcc6j7b3yvp8hl6kh13x4dd9issmfsk18jly")))

(define-public crate-vikos-0.1.2 (c (n "vikos") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1g5fnrzlnfhk0cjd0p87aplsivx6zsl08qrr7afwr72zvk0w0l5b")))

(define-public crate-vikos-0.1.3 (c (n "vikos") (v "0.1.3") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)))) (h "1aqf1qmndqndcfxicm9vizhy18wf1vfg2h19f1aq21y5yq9mbnnq")))

(define-public crate-vikos-0.1.4 (c (n "vikos") (v "0.1.4") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)))) (h "0svpk9yhg5nr12xj72i8h3mxb7zlj5j2azzqnskw3cbis7w6c5c2")))

(define-public crate-vikos-0.1.5 (c (n "vikos") (v "0.1.5") (d (list (d (n "num") (r "^0.1.35") (d #t) (k 0)))) (h "18gwp5lpcxzh6rm9vpjjq70q1wp338gq3q0cq2fvq45gj7x5lfip")))

(define-public crate-vikos-0.1.6 (c (n "vikos") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.35") (d #t) (k 0)))) (h "0y08jhwvfh5yd50fsf8s8j3gfrmzpzmqh2lp7z0q6g442qibkqad") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.1.7 (c (n "vikos") (v "0.1.7") (d (list (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 2)) (d (n "num") (r "^0.1.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "10q6hm1rcdf96mzz743cm1hd3i8znzh45bb32cqzd5ri00x8f1hy") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.1.8 (c (n "vikos") (v "0.1.8") (d (list (d (n "clippy") (r "^0.0.98") (o #t) (d #t) (k 0)) (d (n "csv") (r "^0.14.7") (d #t) (k 2)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "03vfs840jk89k3h6m90a0p89vpqnmai4sck82kpm50883ha2rsk3") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.2.0 (c (n "vikos") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1gih4am94jkyzc0abp566x693h99fmry491rkd2x2nmjqmnladkb")))

(define-public crate-vikos-0.2.1 (c (n "vikos") (v "0.2.1") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1k3kjhgpp20l360sasz574pdp3f9b7b96fhbq4q2x4cnpwrnl0i7")))

(define-public crate-vikos-0.3.0 (c (n "vikos") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1fbmzfkydaa2h6nm51amv4rdc1blcby5y7yr11xcczflj350pyva")))

(define-public crate-vikos-0.3.1 (c (n "vikos") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0qwp62g48bfvhk1wirklwm8jbg05901ah65z0h6cfccngkg2zwsx")))

