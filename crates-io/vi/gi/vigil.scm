(define-module (crates-io vi gi vigil) #:use-module (crates-io))

(define-public crate-vigil-0.1.0 (c (n "vigil") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0xk4a9pmb78rzwh3ii9lg9gws56yp4bkxqv53zjcyhys598nznai")))

(define-public crate-vigil-0.2.0 (c (n "vigil") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0l4ql7kyqd1nkrnh8svk9h3x1ycw18dl6ax71c9fhxmgpz0njh1x")))

(define-public crate-vigil-0.2.1 (c (n "vigil") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1z0dgqq7h3hsqsz0dkh6agh1pnbj37jgn429w8rc1kh2j3rrpf7v")))

