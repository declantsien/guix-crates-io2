(define-module (crates-io vi gi vigicrues) #:use-module (crates-io))

(define-public crate-vigicrues-0.1.0 (c (n "vigicrues") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ns8jwxklqg8gxkc8m074i45bmz9ns5ppyxkg2bsls3qyirwr917")))

