(define-module (crates-io vi gi vigil-reporter) #:use-module (crates-io))

(define-public crate-vigil-reporter-0.0.1 (c (n "vigil-reporter") (v "0.0.1") (h "0vxq3z7r27n1l9qjlvgz95zfvn53afvmz977gr1nncv7lm0a0lg7") (y #t)))

(define-public crate-vigil-reporter-1.0.0 (c (n "vigil-reporter") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "1smg7hvg97j3qivx6jh2df5yq0a0z51zhbw584dc4xhqwqs9sgcn")))

(define-public crate-vigil-reporter-1.0.1 (c (n "vigil-reporter") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "1n8b83br2yanvgw9dlygldj3bs272ccgisw1khzhfv85r77x9j6l")))

(define-public crate-vigil-reporter-1.0.2 (c (n "vigil-reporter") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "05lqbca89wxc0z2nz0bynlvyq8nid3dzdk1647d42vcs6wblks89")))

(define-public crate-vigil-reporter-1.0.3 (c (n "vigil-reporter") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "18d5ivlgllls1vn0cnf8b1w77fb1r9dwc7gj68bj6gsz82r47q39")))

(define-public crate-vigil-reporter-1.1.0 (c (n "vigil-reporter") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "17pq514nfb7d3kg4531idrk62xxbc45klbfmx8nj56nq42l06k79")))

(define-public crate-vigil-reporter-1.1.1 (c (n "vigil-reporter") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5") (d #t) (k 0)))) (h "12k4d7qr3i8z6m8fvsbf6yb58ccai0hqgdpcipqgfv08igwpjfj2")))

(define-public crate-vigil-reporter-1.2.0 (c (n "vigil-reporter") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls-vendored" "gzip" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (d #t) (k 0)))) (h "0n3gi6snkdy0020mpzdq1rz1wwcbq6ahxfxxy3q8bqz8n8y7fav7")))

(define-public crate-vigil-reporter-1.2.1 (c (n "vigil-reporter") (v "1.2.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls-vendored" "gzip" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (d #t) (k 0)))) (h "1vv0y33n5fq94pjdq2ii1803pq61zd7632cw07m1lmnsnypmrvcs")))

(define-public crate-vigil-reporter-1.3.0 (c (n "vigil-reporter") (v "1.3.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "http_req") (r "^0.9") (f (quote ("rust-tls"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)))) (h "0ff0ylipnxylv0k7gg4pbd1z9pbalcc0v085x6v85dpswdnx3i05")))

