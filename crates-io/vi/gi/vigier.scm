(define-module (crates-io vi gi vigier) #:use-module (crates-io))

(define-public crate-vigier-0.1.0 (c (n "vigier") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0r56sg7h1iz9mpjn5grmifmdyvpr20m0bcbdd527ii3g7083plcp")))

(define-public crate-vigier-0.1.1 (c (n "vigier") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lqwk8mh1154xjb01vnfps2clnmqn24073z7dbjhr8phklchrljg")))

