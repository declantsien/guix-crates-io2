(define-module (crates-io vi xa vixargs) #:use-module (crates-io))

(define-public crate-vixargs-0.1.0 (c (n "vixargs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.6") (f (quote ("derive" "color"))) (d #t) (k 0)))) (h "08x2wjjiklcb7lgj1h12c96q8sc41qww0clzj2pgdi89vckqiy9v")))

