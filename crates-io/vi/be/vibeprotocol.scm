(define-module (crates-io vi be vibeprotocol) #:use-module (crates-io))

(define-public crate-VibeProtocol-0.1.0 (c (n "VibeProtocol") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.7") (d #t) (k 0)))) (h "1z90dqwnza3jd39l45jlwracnp8gq1469y1l59b7pqzvy8f28vn9")))

