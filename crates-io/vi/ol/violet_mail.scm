(define-module (crates-io vi ol violet_mail) #:use-module (crates-io))

(define-public crate-violet_mail-0.1.0 (c (n "violet_mail") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "isahc") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1sx5jv32zrjq1fgyvgpphd94grska3y182dh7zvvv442frj4wcc1")))

