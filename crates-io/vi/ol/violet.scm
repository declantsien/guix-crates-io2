(define-module (crates-io vi ol violet) #:use-module (crates-io))

(define-public crate-violet-0.0.1 (c (n "violet") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "flax") (r "^0.4.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0n7da6ff4kwhbjd6rrbprg0mcaydmkwcnl78zn0djlv6zpm0z8pf")))

