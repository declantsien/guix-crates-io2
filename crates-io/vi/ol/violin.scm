(define-module (crates-io vi ol violin) #:use-module (crates-io))

(define-public crate-violin-0.0.1 (c (n "violin") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0s5b5vxla78g15syyw3hbp5d9hywsycm5ly15h47hwjxh0if4mw9") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default") ("alloc" "rand/alloc"))))))

(define-public crate-violin-0.0.2 (c (n "violin") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1m7rggbqxi13myvydz2hzc9d92ffyn70r3kw7zc2877rzbh6cggi") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default" "std") ("builder") ("alloc" "rand/alloc"))))))

(define-public crate-violin-0.1.0 (c (n "violin") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0h9dc59r4gs3w3qab50d52cq07j5na15ciw9visys53hfsgfwypy") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default" "std" "alloc") ("alloc" "rand/alloc"))))))

(define-public crate-violin-0.2.0 (c (n "violin") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1b8qz6kbqkb1a7dgbhmbm9gp7q1ma9c5gwh76sykqyyg9iflvs8c") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default" "std" "alloc") ("alloc" "rand/alloc")))) (r "1.59.0")))

(define-public crate-violin-0.2.1 (c (n "violin") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0b79clbh68pd8sylq6r05xdlhc4i0i6ibjvgqf0nmaxlj67s2253") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default" "std" "alloc") ("alloc" "rand/alloc")))) (r "1.59.0")))

(define-public crate-violin-0.3.0 (c (n "violin") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0n1hazizfzgq6whk1vp3jlgz06lnp0k1slq52d1afw46gmvvpg4w") (f (quote (("std" "rand/std") ("nightly") ("doc") ("default" "std" "alloc") ("alloc" "rand/alloc")))) (r "1.59.0")))

