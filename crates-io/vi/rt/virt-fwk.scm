(define-module (crates-io vi rt virt-fwk) #:use-module (crates-io))

(define-public crate-virt-fwk-0.1.0 (c (n "virt-fwk") (v "0.1.0") (d (list (d (n "block2") (r "^0.2.0-alpha.8") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "objc2") (r "^0.3.0-beta.5") (d #t) (k 0)))) (h "05qry53dq87xj6pxrzgabnfnf20k9nni5fhjf5g1fvv1k70qwxwf")))

