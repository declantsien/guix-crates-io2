(define-module (crates-io vi rt virtio-bindings) #:use-module (crates-io))

(define-public crate-virtio-bindings-0.1.0 (c (n "virtio-bindings") (v "0.1.0") (h "0sxxhhmz1r4s4q5pd2lykswcv9qk05fmpwc5xlb8aj45h8bi5x9z") (f (quote (("virtio-v5_0_0") ("virtio-v4_14_0"))))))

(define-public crate-virtio-bindings-0.2.0 (c (n "virtio-bindings") (v "0.2.0") (h "1zqxhf5kmhfv56gz3rk6jnfq7wid8gqwijp2d9ksk6hvz7x8940b") (f (quote (("virtio-v5_0_0") ("virtio-v4_14_0"))))))

(define-public crate-virtio-bindings-0.2.1 (c (n "virtio-bindings") (v "0.2.1") (h "162vb9rlf3fyaj23h89h6z1snxzqpfn5nnr6x9q6954a15s7p3f1") (f (quote (("virtio-v5_0_0") ("virtio-v4_14_0"))))))

(define-public crate-virtio-bindings-0.2.2 (c (n "virtio-bindings") (v "0.2.2") (h "11mfm9brqgwpfg0izsgc4n7xkqwxk5ad03ivslq0r88j50dwp2w7") (f (quote (("virtio-v5_0_0") ("virtio-v4_14_0"))))))

