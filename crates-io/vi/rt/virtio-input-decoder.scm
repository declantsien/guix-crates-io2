(define-module (crates-io vi rt virtio-input-decoder) #:use-module (crates-io))

(define-public crate-virtio-input-decoder-0.1.0 (c (n "virtio-input-decoder") (v "0.1.0") (h "07wmrf5lqvqf1ds521myxlkhjdsy6i91ljr7a9j9y8kjvdvjislx")))

(define-public crate-virtio-input-decoder-0.1.1 (c (n "virtio-input-decoder") (v "0.1.1") (h "1fczgdkki38xgpgyrcpn4svjnkj517kkdlab1lazwn063w31y5n2")))

(define-public crate-virtio-input-decoder-0.1.2 (c (n "virtio-input-decoder") (v "0.1.2") (h "1qlhawcs1vdnlwb2hp6lzwk565jl507xc27li66nq3adg8b683ky")))

(define-public crate-virtio-input-decoder-0.1.3 (c (n "virtio-input-decoder") (v "0.1.3") (h "1jsmifcn5sii7wqz7qc9x8v72s2xw6q1hf18k0yhlg2s473aqhcq")))

(define-public crate-virtio-input-decoder-0.1.4 (c (n "virtio-input-decoder") (v "0.1.4") (h "1xf6ym6d72bsp5z238i7r97mkkfraz2yddp9sbgc1bz19mzzr4kj")))

