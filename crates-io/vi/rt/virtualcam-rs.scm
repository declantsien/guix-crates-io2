(define-module (crates-io vi rt virtualcam-rs) #:use-module (crates-io))

(define-public crate-virtualcam-rs-0.1.0 (c (n "virtualcam-rs") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "synchapi" "winbase" "handleapi" "memoryapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.50.0") (d #t) (k 0)))) (h "13829j73n1dqiik5qxbsncjhxal8qjcldpvrkkpldfs8fx5wn3a8")))

