(define-module (crates-io vi rt virtio-media) #:use-module (crates-io))

(define-public crate-virtio-media-0.0.1 (c (n "virtio-media") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "enumn") (r "^0.1.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "memfd") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "v4l2r") (r "^0.0.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.31") (f (quote ("derive"))) (d #t) (k 0)))) (h "1821wy0nlfdqdsaql3ld6lgil2q1h5y3bgdj5rblrisz17a7nqf4") (f (quote (("simple-device" "memfd") ("default" "simple-device"))))))

