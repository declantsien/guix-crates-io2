(define-module (crates-io vi rt virtual_view) #:use-module (crates-io))

(define-public crate-virtual_view-0.1.0 (c (n "virtual_view") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0139pba81xk3rpz4r3zklzm5hqd1fa891dmvjg6a7qh77csrkx0g")))

(define-public crate-virtual_view-0.1.1 (c (n "virtual_view") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rxm6ndyhmy1sp7bjj0m4apfq214k3kp8f6lbf9g3mlk3k581l4h")))

(define-public crate-virtual_view-0.1.2 (c (n "virtual_view") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vvnq929k90jmnjp5xl161yjqw26y66zamc0qk5jf9s1d44sirdk")))

(define-public crate-virtual_view-0.1.3 (c (n "virtual_view") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "main_loop") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z721fq2w0k8mf42x6362f89w3zriyg230n7kpdpg0gpckl5c5ni")))

(define-public crate-virtual_view-0.1.4 (c (n "virtual_view") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "main_loop") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n7lcmsyrldy4ajv539dparvdwrx4c8ix37bimzn2gacpnfkv343")))

(define-public crate-virtual_view-0.1.5 (c (n "virtual_view") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "main_loop") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fj8v0gz4ikbhih8zhdf3qgiyharz54p3lpgcx22g7miqm4wg126")))

(define-public crate-virtual_view-0.1.6 (c (n "virtual_view") (v "0.1.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r3ycgmxzn3scakqfyxb0n7b2g9q649rgy4sp69hcy9hmf7xvx1z")))

(define-public crate-virtual_view-0.2.0 (c (n "virtual_view") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k41vmlgmqnma2msyl69scsd60m8vbiivb682mnqrqrydcr0lcfp")))

(define-public crate-virtual_view-0.2.1 (c (n "virtual_view") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pbznqy56qhcsv1x206vq9a2x7380m3mcs0cwvffbqc4kn6sf9wm")))

(define-public crate-virtual_view-0.2.2 (c (n "virtual_view") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "messenger") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0sf3iznzn4zqvzdlacq7i1azxyfwn1i35kls83k5jcp3f5k6080g")))

