(define-module (crates-io vi rt virtue) #:use-module (crates-io))

(define-public crate-virtue-0.0.1 (c (n "virtue") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 2)))) (h "07l0vk8bdcwz19lqwba3grd89sfxs3zs5zdf3xzsqlfsr37khfp1")))

(define-public crate-virtue-0.0.2 (c (n "virtue") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1kbnladlhlrzaah5lz6gvk3nd3gv9g4pfgnlri7dqc8ld7dihzkr")))

(define-public crate-virtue-0.0.3 (c (n "virtue") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1hgaf3qwmfyzhqpfl3d5cvgazbs0c3h9li9wa06s39yci7jv2pzs")))

(define-public crate-virtue-0.0.4 (c (n "virtue") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1f2nkfkjrcjkyd3cla85j3lhmwyc7kl3rkhw6gmc1g3a0v8mxs7h")))

(define-public crate-virtue-0.0.5 (c (n "virtue") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0wypmv05s0ka4sa530vlmb4d47bc2c4mv8yhx98lmsjwkwsdn3nz")))

(define-public crate-virtue-0.0.6 (c (n "virtue") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "13rip0p0l7r2sfcq28bm24vhnffb19dlgw9sf5794ilfbjb59cny")))

(define-public crate-virtue-0.0.7 (c (n "virtue") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1pbly878nffinzi137fg8wdlaz84cga3drcpzqi6zvhp1pzgnz3m")))

(define-public crate-virtue-0.0.8 (c (n "virtue") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "0qqa48hqrxpbzkb4rx08zjknswj3k069f9nlkfzmmm2dlvbdqq3v")))

(define-public crate-virtue-0.0.9 (c (n "virtue") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1wakpah9n6vj99cj5hlqcyghl6g6slvy092s5mjn3084r84zj6pg")))

(define-public crate-virtue-0.0.10 (c (n "virtue") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1ycj4gjvxz0ibab3yysalmd0zdp3sw5pcfq6g4hr30k0zgchkb6b")))

(define-public crate-virtue-0.0.11 (c (n "virtue") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1342pl1qji0xl22am4yysyfs495nrcns9l9wgpa6k62xc32kjw8l")))

(define-public crate-virtue-0.0.12 (c (n "virtue") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "151k6wjq8s3yzm7yfaw1ph77957y46dsfsmvyddp1b7izpmv1x7g")))

(define-public crate-virtue-0.0.13 (c (n "virtue") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "051k8yr55j0iq28xcmr9jsj7vlri28ah9w8f5b479xsdcb061k4x")))

(define-public crate-virtue-0.0.14 (c (n "virtue") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1zw98a2xkfw999543j3zx3qshqg412d8j1wrgk2pslykx8azf8mm")))

(define-public crate-virtue-0.0.15 (c (n "virtue") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1xnibhw221b7c5fbmm7b97qcm7973ll781cmzh2x2vr1g31hkl1w")))

(define-public crate-virtue-0.0.16 (c (n "virtue") (v "0.0.16") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1ixv35xj9pph1aq296i6cryq4vzva5apnl1p16xz3357hskjcs1v")))

