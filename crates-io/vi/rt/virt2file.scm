(define-module (crates-io vi rt virt2file) #:use-module (crates-io))

(define-public crate-virt2file-0.1.0 (c (n "virt2file") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (d #t) (k 0)))) (h "193h7maa1hgagl167rx4x47q4fp7fvc6fh03fbdb2fxnzyfk0syb")))

(define-public crate-virt2file-0.2.0 (c (n "virt2file") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (d #t) (k 0)))) (h "04khfxa3q24wj0pcljprwni0qix9hv4qb5y99a7ggxx1laxzl0ii")))

