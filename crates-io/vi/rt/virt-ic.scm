(define-module (crates-io vi rt virt-ic) #:use-module (crates-io))

(define-public crate-virt-ic-0.1.0 (c (n "virt-ic") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pk6b2i2ydlyr2d8xwg8v5wvy4jx0d2317lx43c0kc6w49iszal5")))

(define-public crate-virt-ic-0.1.1 (c (n "virt-ic") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1k4p0cxzxaavdspa557bjd7fmlpmhcmdjb4hl6n8svzlih3v0vys")))

(define-public crate-virt-ic-0.1.2 (c (n "virt-ic") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0mp83zzpaicsrypg35snxq57cv6zg78chxs6bph4z16zd56ilprf")))

(define-public crate-virt-ic-0.1.3 (c (n "virt-ic") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1jnlgwyf0dpapg98bryw1v5ryc8sw87b1yagzgi90r0kplaj91x1")))

(define-public crate-virt-ic-0.2.0 (c (n "virt-ic") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mnv1himbkxqkag3kqjir7a8w01l3020y5zzs6q3dzhazxkm8ipv")))

(define-public crate-virt-ic-0.2.1 (c (n "virt-ic") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ih4s1dl62zpgm4fnah7lswwyi1q2xwksyl5q71rrbjv464bppwc")))

(define-public crate-virt-ic-0.3.0 (c (n "virt-ic") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1x96h05gxf3w568njgxp5plwqvb9b6zdpxm6ik2fg3580krx63g0")))

(define-public crate-virt-ic-0.5.0 (c (n "virt-ic") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02xndnjnr3qb3fbwhksw1dlyg4j7pj2zs9fb983kyjsdvkdijf1q") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:ron" "bitflags/serde"))))))

(define-public crate-virt-ic-0.5.1 (c (n "virt-ic") (v "0.5.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13gnkisc4bsgcd3zvdlcxi3ypqygqykk49rbzyh5a3xw6jdkw2fd") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:ron" "bitflags/serde"))))))

(define-public crate-virt-ic-0.5.2 (c (n "virt-ic") (v "0.5.2") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v5rh367qvch21sb1r2m9mnjznf2d22zf8dflz1i6wkf28cr2z5y") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:ron" "bitflags/serde"))))))

