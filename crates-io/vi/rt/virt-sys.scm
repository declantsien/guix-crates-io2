(define-module (crates-io vi rt virt-sys) #:use-module (crates-io))

(define-public crate-virt-sys-0.1.0 (c (n "virt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "0wsq7w8a074j6zkhk74gbisbby8168l02dybkwv82c31vbphg0mq") (f (quote (("qemu")))) (l "virt")))

(define-public crate-virt-sys-0.2.0 (c (n "virt-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1nx2xf8i0i4m7gxndrm20rpihbm7djdmrh7sf71yfgxbs3hgcffd") (f (quote (("qemu") ("bindgen_regenerate" "bindgen")))) (l "virt")))

