(define-module (crates-io vi rt virtual-memory) #:use-module (crates-io))

(define-public crate-virtual-memory-0.1.0 (c (n "virtual-memory") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rb6mp3rg10y1sjndsv443rk5w3yiw4iv6731gc4a5xkb6s3vmwq")))

(define-public crate-virtual-memory-0.1.1 (c (n "virtual-memory") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0543km4gy24yq6wjis0k5fxh62bv0bk1dxp1d6c8hwzyb25vdkv9")))

