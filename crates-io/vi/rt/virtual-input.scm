(define-module (crates-io vi rt virtual-input) #:use-module (crates-io))

(define-public crate-virtual-input-0.1.0 (c (n "virtual-input") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1x54wlwqikbl4rn217wd85hpqkqcz77pm1fl5j8c36dchd9p1d66")))

(define-public crate-virtual-input-0.1.1 (c (n "virtual-input") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1pdmb7sijkj9afkc3znj5rf4k0i7a70dp3ip3hc1v34k1h9i8l9x")))

(define-public crate-virtual-input-0.2.0 (c (n "virtual-input") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "08pspj4id8c3r9d3k2464b51mbvjw4k7ysw0xqcs09yl72jsihrp")))

(define-public crate-virtual-input-0.2.1 (c (n "virtual-input") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jdz9ccqbm7gppf2h28xfshb469nxa02hj1g57fpzcf7h23iljps")))

(define-public crate-virtual-input-0.2.2 (c (n "virtual-input") (v "0.2.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0m6q3lgd300jha2hkd5014l6yb45bi196i5vip3xww2r6k9zpbas")))

(define-public crate-virtual-input-0.2.3 (c (n "virtual-input") (v "0.2.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jw4w5synwiwliiv4dki1181wyrn2pvr29695lbnxha2bdk78jaz")))

(define-public crate-virtual-input-0.2.4 (c (n "virtual-input") (v "0.2.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "02b231sq1yfaims53qv0y525nwsgjd91mqhrg42ccjzgaasw6hxx")))

(define-public crate-virtual-input-0.2.5 (c (n "virtual-input") (v "0.2.5") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "006ggjgmkkskqv6zlzrb63rrcslvxwyipl5if54hq59s97b9apx4")))

(define-public crate-virtual-input-0.2.6 (c (n "virtual-input") (v "0.2.6") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0d8zjba0lc80539ghijqz8df9v010hlszsbc9js7wxbm0xwb8vaa")))

(define-public crate-virtual-input-0.2.7 (c (n "virtual-input") (v "0.2.7") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "19xwqn4sk6dd428bkyd3s7j57a5jz193vdrj0f90yjvxfhjfxkan")))

(define-public crate-virtual-input-0.2.8 (c (n "virtual-input") (v "0.2.8") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.19") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 0)))) (h "12z8bmwwkhxs2mrf25hrj15skbs9zcj4a20a8pc3c3yzydzx204r")))

