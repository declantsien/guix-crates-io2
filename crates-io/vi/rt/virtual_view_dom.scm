(define-module (crates-io vi rt virtual_view_dom) #:use-module (crates-io))

(define-public crate-virtual_view_dom-0.1.0 (c (n "virtual_view_dom") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.2") (d #t) (k 0)) (d (n "virtual_view") (r "^0.1") (d #t) (k 0)))) (h "138yb2q4jl7d24s2naa78fxwf0v2skkzw5yigj64gdkw8bnfn8da")))

(define-public crate-virtual_view_dom-0.1.1 (c (n "virtual_view_dom") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.2") (d #t) (k 0)) (d (n "virtual_view") (r "^0.1") (d #t) (k 0)))) (h "1qldnvnsbpcjrdx9qw7h5ypi0gwbsgbv46k5l7xhggdpx4jpnjw6")))

(define-public crate-virtual_view_dom-0.1.2 (c (n "virtual_view_dom") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.3") (d #t) (k 0)) (d (n "virtual_view") (r "^0.1") (d #t) (k 0)))) (h "0iaqn9mk8y4zxnh90ah9cjx6gs66vpjqqikaj6ybgr498kkqd1xs")))

(define-public crate-virtual_view_dom-0.2.0 (c (n "virtual_view_dom") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.3") (d #t) (k 0)) (d (n "virtual_view") (r "^0.2") (d #t) (k 0)))) (h "0v0f4iwsayqjplwfh2pw22i2j9ljddyadf65847mzrrnz79fg21r")))

(define-public crate-virtual_view_dom-0.2.1 (c (n "virtual_view_dom") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)) (d (n "virtual_view") (r "^0.2") (d #t) (k 0)))) (h "0f4lklfp6pkd5k1by1vw52xgsp0x5smndiw2i89y7mmhs8cyzamx")))

