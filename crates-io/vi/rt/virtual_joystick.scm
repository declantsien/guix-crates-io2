(define-module (crates-io vi rt virtual_joystick) #:use-module (crates-io))

(define-public crate-virtual_joystick-0.1.0 (c (n "virtual_joystick") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0pqjz9irbknnd8r1nqbkx4wv6shk82vvf7ws70hnb50gyahw2nvf") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-0.1.1 (c (n "virtual_joystick") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1f00bx0k14dydb7wmznxg1riw4dkajk3lj60dm2f981fhi1fg5xz") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.0.0 (c (n "virtual_joystick") (v "1.0.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "165967k77m5rlixdp4faskd741is88k22jlnwxjkikad6jwp1lnd") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.0.1 (c (n "virtual_joystick") (v "1.0.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0fwi3rw21k9ly6sq0dqm3fb6nfvpr0d86qslh7rvvhv4pl3x8nmd") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.0.2 (c (n "virtual_joystick") (v "1.0.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0mrwvgg8pv6s46510ks7b73g4hw5fqc3waf33d7y8ch2g7hbdk2x") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.0.3 (c (n "virtual_joystick") (v "1.0.3") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0g1wiahn1cy7d45qnkm28jss0fqgf56yrhni4ykpn65q1zg177pz") (f (quote (("inspect" "bevy-inspector-egui") ("default")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.1.0 (c (n "virtual_joystick") (v "1.1.0") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a0f69ngzmcxgkjsvff1ms7ygymkswgd874xcgknfkzkhxagm4iz") (f (quote (("serialize" "serde") ("inspect" "bevy-inspector-egui") ("default" "serialize")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.1.1 (c (n "virtual_joystick") (v "1.1.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nw553s7nhc1k6rry12dqlfk4fgnfhl67xqrm03z6qb14spsw2a6") (f (quote (("serialize" "serde") ("inspect" "bevy-inspector-egui") ("default" "serialize")))) (r "1.67.0")))

(define-public crate-virtual_joystick-1.1.2 (c (n "virtual_joystick") (v "1.1.2") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wk8716ybq53s07lyfn7chfjadj0nq92xl795kxd1kb5wv0iyjlc") (f (quote (("serialize" "serde") ("inspect" "bevy-inspector-egui") ("default" "serialize")))) (r "1.67.0")))

(define-public crate-virtual_joystick-2.0.1 (c (n "virtual_joystick") (v "2.0.1") (d (list (d (n "bevy") (r "^0.11") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01fvcn6b3fgggz9qqbfv2bchzkagv28nj31f1cgq31a9vlfsvra0") (f (quote (("serialize" "serde") ("inspect" "bevy-inspector-egui") ("default" "serialize")))) (r "1.67.0")))

(define-public crate-virtual_joystick-2.1.0 (c (n "virtual_joystick") (v "2.1.0") (d (list (d (n "bevy") (r "^0.12") (f (quote ("bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pn6h3hx06i39kk2cd1n0gh45d1jhpdsrh2mi2cp1m2mfmk3krvx") (f (quote (("inspect" "bevy-inspector-egui") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.67.0")))

(define-public crate-virtual_joystick-2.2.0 (c (n "virtual_joystick") (v "2.2.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("bevy_render" "bevy_ui"))) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1car3wy2lqgzr1p813yydzhzi5r5anwk6zddj88wghs83nw9xjfl") (f (quote (("inspect" "bevy-inspector-egui") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.67.0")))

