(define-module (crates-io vi rt virtio-drivers) #:use-module (crates-io))

(define-public crate-virtio-drivers-0.1.0 (c (n "virtio-drivers") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13j6gwig0blaabbmv35h2w857zriac3gp0i81xc9b47dw782bykh")))

(define-public crate-virtio-drivers-0.2.0 (c (n "virtio-drivers") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "15pw96iwf6vn7dmv5z0jb1vb8vzaafsrdpxxf24qxlp9ma3690xz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-virtio-drivers-0.3.0 (c (n "virtio-drivers") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0h2cjd5nikl75gj8j1bcm4r8j1qkg2jdc0pvdcxkip264hcz3nmf") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-virtio-drivers-0.4.0 (c (n "virtio-drivers") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0abh8g75cqg93baa37sp08xg61zc2s63kbiyzs3ga6pnqrbrjaa2") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-virtio-drivers-0.5.0 (c (n "virtio-drivers") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "1kxv0bj7y1kjj0cxln3z5lslkj2dc4vbjbxk56pcsxwywnhqs6vc") (f (quote (("default" "alloc") ("alloc" "zerocopy/alloc"))))))

(define-public crate-virtio-drivers-0.6.0 (c (n "virtio-drivers") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (f (quote ("alloc"))) (d #t) (k 2)))) (h "1ggkzy6z9xpppbj754kwzbqkn5d61w7i6cqrvgzbpjgb6rm9q73j") (f (quote (("default" "alloc") ("alloc" "zerocopy/alloc"))))))

(define-public crate-virtio-drivers-0.7.0 (c (n "virtio-drivers") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (f (quote ("alloc"))) (d #t) (k 2)))) (h "0vbpv2qhlfwfk4sjmd9hg4kkjqr0v0g85pigffxjdnpp504hkmzm") (f (quote (("default" "alloc") ("alloc" "zerocopy/alloc"))))))

(define-public crate-virtio-drivers-0.7.1 (c (n "virtio-drivers") (v "0.7.1") (d (list (d (n "bitflags") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.5") (f (quote ("alloc"))) (d #t) (k 2)))) (h "14hk8dlm3hl1i9apfw6ldiym1kc1m5yy2az4cv9gj9xdkp351zgl") (f (quote (("default" "alloc") ("alloc" "zerocopy/alloc"))))))

(define-public crate-virtio-drivers-0.7.2 (c (n "virtio-drivers") (v "0.7.2") (d (list (d (n "bitflags") (r "^2.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.5") (f (quote ("alloc"))) (d #t) (k 2)))) (h "148zm6513w7xw7wk99znhhn8698jjn7vb4bq0axh9h2myygycsgz") (f (quote (("default" "alloc") ("alloc" "zerocopy/alloc"))))))

