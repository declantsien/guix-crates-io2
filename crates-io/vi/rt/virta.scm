(define-module (crates-io vi rt virta) #:use-module (crates-io))

(define-public crate-virta-0.1.0 (c (n "virta") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "async-event") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clone-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "recycle-box") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 0)) (d (n "stdcode") (r "^0.1.14") (d #t) (k 0)))) (h "11dwb2jrbar61wc6wfc29zii6p95hxp48hslbal0hbdwiis5cc7k")))

