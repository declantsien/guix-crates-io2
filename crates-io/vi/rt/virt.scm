(define-module (crates-io vi rt virt) #:use-module (crates-io))

(define-public crate-virt-0.1.0 (c (n "virt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1dnkn70fiyx5vf43cc7ywggh6qlvc9qf3yhqb0xdb0dbmq0zycb0")))

(define-public crate-virt-0.1.1 (c (n "virt") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0c95c8ss5mylrbifar6a5j0hsvhi9bjq15bi6pgfm91ni49id6j3")))

(define-public crate-virt-0.1.2 (c (n "virt") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0k7dq4i3fi4a51kylhz89i7c50fi9hydklqw89iz47hvg37d5mjb")))

(define-public crate-virt-0.1.3 (c (n "virt") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "17941nz6d5fif73a2yhwhjkm8319hs061ss9c9n4xqjfz4jdlyph")))

(define-public crate-virt-0.1.4 (c (n "virt") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "067fb7hfmjjcwf33150hvyx2ki03rhk5nbdfk5xxj4iw4dhb1bz2")))

(define-public crate-virt-0.1.5 (c (n "virt") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1rlk9xs1zhc9k28bm6kdnqlzy5ismmf407l3ar1m4f9s5jaj9yyp")))

(define-public crate-virt-0.1.6 (c (n "virt") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "15g2dxw12bzp8y9v40z2g5a4sippwv2i4zcysddfnhdrmvrdmrhp")))

(define-public crate-virt-0.1.7 (c (n "virt") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1b79haxcb34r0g98kv39ga0lvwmpjr96n66ii2ggssa4hrxbdagx")))

(define-public crate-virt-0.1.8 (c (n "virt") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "08cp402f54xiyfwc225hmdxbgvn928j28cg1727cjjg7gmr596bc")))

(define-public crate-virt-0.2.0 (c (n "virt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "033n16c3gqpd7csxba4k0y8kbly6rvix2jkya6vgsj7y4kia4sfj")))

(define-public crate-virt-0.2.1 (c (n "virt") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1xcidi3kmyb1hg5ji9qx5nccjdhi8vzzwq1agnxj749ac3c1yrkl")))

(define-public crate-virt-0.2.2 (c (n "virt") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1w9yq4a1w2ip6f9spbc3rak2py0sip8ns593z7qlf0nxvhas8y1m")))

(define-public crate-virt-0.2.3 (c (n "virt") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1q4aaridk4kqr0gz8swqq8fdcf4jpq42p54fcj3f83mfh3fh0k5i")))

(define-public crate-virt-0.2.4 (c (n "virt") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0i2wwlc5wm8hww6lla8jc0mm80a95rww0n250dxrnlrhgyhpa974")))

(define-public crate-virt-0.2.5 (c (n "virt") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1jyggfzx9z82bjbkys399yv3yg0nxl1g87i8gs75a6g7rpnwlq40")))

(define-public crate-virt-0.2.6 (c (n "virt") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0zwabk066yvf3v0s6nq1pvwzld3cg1mij0mmmrx2yljnjfz5qby0")))

(define-public crate-virt-0.2.7 (c (n "virt") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0374a7wywldqrhbgklg887pymhbg56kily2vq4cpri0qplpifcbb")))

(define-public crate-virt-0.2.8 (c (n "virt") (v "0.2.8") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0k132knj68svpy6hyscgqbxrip5l3aqga05xaazbnzc46vzzjq26")))

(define-public crate-virt-0.2.9 (c (n "virt") (v "0.2.9") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1m72fcrh9b4n94bf0740k0az84fdm1ipsh0mcv3zbcvm92brkhfj")))

(define-public crate-virt-0.2.10 (c (n "virt") (v "0.2.10") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "141ch3x5b8nnhj9ibiflci9ck1jhr1bwlzmbgcj1bp4s98d781nm")))

(define-public crate-virt-0.2.11 (c (n "virt") (v "0.2.11") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1jk9njjq6zxivwlm62pcp1fxqpp5yfxk3ifxi089smda6vamrkh6")))

(define-public crate-virt-0.2.12 (c (n "virt") (v "0.2.12") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "virt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1gb7y0z6lziscl9g0kf39gw5xw2310ssvw1gwk79h4ync1z6kw3x") (f (quote (("qemu" "virt-sys/qemu"))))))

(define-public crate-virt-0.3.0 (c (n "virt") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "virt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1ksk3s0kwyp0gpslhcsx4idr90a1gq4hgazqgsg21v7if7gs05lk") (f (quote (("qemu" "virt-sys/qemu") ("bindgen_regenerate" "virt-sys/bindgen_regenerate"))))))

(define-public crate-virt-0.3.1 (c (n "virt") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "virt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "10fvfg7m3qd60968wddxpn2amhi6kf1vsdfkwl901mh63ndkgjlb") (f (quote (("qemu" "virt-sys/qemu") ("bindgen_regenerate" "virt-sys/bindgen_regenerate"))))))

