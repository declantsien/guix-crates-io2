(define-module (crates-io vi ci vicis) #:use-module (crates-io))

(define-public crate-vicis-0.1.0 (c (n "vicis") (v "0.1.0") (d (list (d (n "anyhow") (r "=1.0.38") (d #t) (k 0)) (d (n "either") (r "=1.6.1") (d #t) (k 0)) (d (n "funty") (r "=1.1.0") (d #t) (k 0)) (d (n "id-arena") (r "=2.2.1") (d #t) (k 0)) (d (n "indicatif") (r "=0.15.0") (d #t) (k 0)) (d (n "nom") (r "=6.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "=1.1.0") (d #t) (k 0)))) (h "0gpmx2zr15ls848yjkda4ma708jn5cksnsi8l01anhrbspkz7sbp")))

