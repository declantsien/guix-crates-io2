(define-module (crates-io vi ci vicis-core) #:use-module (crates-io))

(define-public crate-vicis-core-0.2.0 (c (n "vicis-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (f (quote ("backtrace"))) (d #t) (t "tarpaulin") (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1pljacsxqgd1i9w3rhr8da8bfz855asnckv6fgg8njlwc206cb8r")))

(define-public crate-vicis-core-0.2.1 (c (n "vicis-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (f (quote ("backtrace"))) (d #t) (t "tarpaulin") (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "07wwkrzypnz3fp9djhwgx14s2yp8vwhj96lwpac3ql2q2p26xxsv")))

