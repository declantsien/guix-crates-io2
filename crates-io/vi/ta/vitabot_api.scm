(define-module (crates-io vi ta vitabot_api) #:use-module (crates-io))

(define-public crate-vitabot_api-1.0.0 (c (n "vitabot_api") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1rq0xjn3h8qdsl0i87r4zx9jfhj8gg094yi3lnhg8n84gyrdjj")))

(define-public crate-vitabot_api-1.0.1 (c (n "vitabot_api") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hj0qm4j1yjr176j4n7vdqz7qf3s89zh6xhx3qry2aw9zsq8kb5g")))

