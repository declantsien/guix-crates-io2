(define-module (crates-io vi ta vita-newlib-shims) #:use-module (crates-io))

(define-public crate-vita-newlib-shims-0.1.0 (c (n "vita-newlib-shims") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "0hxjw974s040h9gianvbv3anh0qy3qfql2wc0xdki9vxbkc04cz8")))

(define-public crate-vita-newlib-shims-0.2.0 (c (n "vita-newlib-shims") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)))) (h "1ahda4xys82d5qig9dacc00df664ggxqblhlhv6hvrnck4np3fwq") (f (quote (("socketpair") ("pipe2") ("default" "socketpair" "pipe2"))))))

(define-public crate-vita-newlib-shims-0.2.1 (c (n "vita-newlib-shims") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.148") (d #t) (k 0)))) (h "0n8x1dz6l676xn9q1vk428kjk4m7qzzdryl7ymmv2b1w708hshc2") (f (quote (("socketpair") ("pipe2") ("default" "socketpair" "pipe2"))))))

(define-public crate-vita-newlib-shims-0.2.2 (c (n "vita-newlib-shims") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0rb5kbr6lqq9zj26g0nlc0zzi5csqmkzdi057ssj6fj04jz6a6vv") (f (quote (("socketpair") ("pipe2"))))))

(define-public crate-vita-newlib-shims-0.3.0 (c (n "vita-newlib-shims") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0w5148f1wdm6c1r0gshwj098jdkih620prq69kc5acvhj6ffqgxx") (f (quote (("socketpair") ("pipe2") ("fcntl"))))))

(define-public crate-vita-newlib-shims-0.3.1 (c (n "vita-newlib-shims") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0cwj2fxl1wjz75bl9v80w96w3lc5wfhjfkm5zdp0hi11rpw65f3c") (f (quote (("socketpair") ("pipe2") ("fcntl"))))))

