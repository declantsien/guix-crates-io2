(define-module (crates-io vi ta vitalium_verb_dsp) #:use-module (crates-io))

(define-public crate-vitalium_verb_dsp-0.1.0 (c (n "vitalium_verb_dsp") (v "0.1.0") (h "0z09h3x0kpk07vig0b8nhfmsl3cnq6sk77apv8lp1l6q3b9nq19x")))

(define-public crate-vitalium_verb_dsp-0.1.1 (c (n "vitalium_verb_dsp") (v "0.1.1") (h "0xggz4149r6p6zzfljjz3w6nj1a9dmnk5jwxiv2m1g28dg0j4cg9")))

