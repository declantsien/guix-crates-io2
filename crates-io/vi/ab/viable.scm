(define-module (crates-io vi ab viable) #:use-module (crates-io))

(define-public crate-viable-0.1.0 (c (n "viable") (v "0.1.0") (h "19h7ragv8ingvbdf1q1l4n4fd6jwgz7yggvh0z308f51rvyzamb5")))

(define-public crate-viable-0.1.1 (c (n "viable") (v "0.1.1") (d (list (d (n "viable-impl") (r "^0.1") (d #t) (k 0)))) (h "0p4wf4km6n1cxzqswp8vh8r88cfk4s2fxf71zq3v044mr9brq2q4")))

(define-public crate-viable-0.1.2 (c (n "viable") (v "0.1.2") (d (list (d (n "viable-impl") (r "^0.1") (d #t) (k 0)))) (h "1ab35ki5kfn1kml3r6qprldnlyzymr6r4afazrw93q6qq2n7j8ji")))

(define-public crate-viable-0.1.3 (c (n "viable") (v "0.1.3") (d (list (d (n "viable-impl") (r "^0.1") (d #t) (k 0)))) (h "1x1lv8w58w0s91brjnjlpgh3zbwkwjvlcbcyc2kk1clpmwbmw36d")))

(define-public crate-viable-0.1.4 (c (n "viable") (v "0.1.4") (d (list (d (n "viable-impl") (r "^0.1") (d #t) (k 0)))) (h "15953p6hh2rph36nphki1yhrd87bq26wnm4m4k4yrnzj3r2wg66y")))

(define-public crate-viable-0.1.5 (c (n "viable") (v "0.1.5") (d (list (d (n "viable-impl") (r "^0.1") (d #t) (k 0)))) (h "1l1zcvzdpwsk1pn3zngmp2n28bk9mcqb35d00jvn21mlkksh52hx")))

(define-public crate-viable-0.2.0 (c (n "viable") (v "0.2.0") (d (list (d (n "viable-impl") (r "^0.2") (d #t) (k 0)))) (h "0q6bk7992f1jvg8zvrgjyn4gc5c59lv4lks1k18pafmhwykc2ikp")))

