(define-module (crates-io vi ab viable-impl) #:use-module (crates-io))

(define-public crate-viable-impl-0.1.0 (c (n "viable-impl") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "02ghdpdcjx5azkhx1hci6bwlyjq99jvwhgym96jc600pgh2fy67h")))

(define-public crate-viable-impl-0.1.1 (c (n "viable-impl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "160ibccfjnc52kcj269pbgshzyg7p3cjp0cy1s4ddl95ldb79qxr")))

(define-public crate-viable-impl-0.1.2 (c (n "viable-impl") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lyxpxxgn62laq4p7bh8j57d7qd3v4rpmrh93arsdpi9w6h6j3ax")))

(define-public crate-viable-impl-0.1.3 (c (n "viable-impl") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "0wk6vvf4pj8g65i3gyhjqcw821klhz4scfanjs7dpz41qxbq3ram")))

(define-public crate-viable-impl-0.1.4 (c (n "viable-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "01vrilm12m2hy5zxhhi69s9iwf77nrwcvdxajdi0inscbhy8j22a")))

(define-public crate-viable-impl-0.1.5 (c (n "viable-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "1iays3ypdm0p8y0dhknrrvlj81ax405kw54ss1jr206qqlj99xf5")))

(define-public crate-viable-impl-0.2.0 (c (n "viable-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "18934n72asggkm79n2xx6hcjgkrdxdaqghy56x8iykn5ysagirf1")))

(define-public crate-viable-impl-0.2.1 (c (n "viable-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vrssnagg1bc71cxcv9f5fkyr3jxw98g5hgdavl909xvsr1wzg2m")))

