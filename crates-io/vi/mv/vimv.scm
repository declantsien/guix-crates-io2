(define-module (crates-io vi mv vimv) #:use-module (crates-io))

(define-public crate-vimv-0.1.0 (c (n "vimv") (v "0.1.0") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)))) (h "09d2szppqcnkmr75r9wdcvj0h28ndqgxms4ikvla3iv6c6n7bzms")))

(define-public crate-vimv-0.2.0 (c (n "vimv") (v "0.2.0") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "0ydvg32f79g8yxmh7a75lwpqjwyafnm5mhzc8dsqkrli7za7j3na")))

(define-public crate-vimv-0.2.1 (c (n "vimv") (v "0.2.1") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "0vsxbdjfkxlvxf39jr9akz51l8xxj0did6yd0cbhq04hn91kgkvv")))

(define-public crate-vimv-0.3.0 (c (n "vimv") (v "0.3.0") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "1df4ggd2xdh780cjwqw1dkj5gbxlp5qr56qa75ksd1myndr3x34p")))

(define-public crate-vimv-0.3.1 (c (n "vimv") (v "0.3.1") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "1fxy2hkdxwzwyqwg7fwplz37aaq4ap7hz9wc983zj7lzfa2dmwcw")))

(define-public crate-vimv-0.3.2 (c (n "vimv") (v "0.3.2") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "0nzsxy408qdqqy0w06ibkj6macwc4spzwnm98nr12x4qq5g4pncj")))

(define-public crate-vimv-0.4.0 (c (n "vimv") (v "0.4.0") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)))) (h "1ckxp93mngznl2wy9srq4lh4knw2shx8161zd25z50xavdim03vk")))

(define-public crate-vimv-0.4.1 (c (n "vimv") (v "0.4.1") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "15vb78nlf0kfai9bd6iwz59fpbwn2f0fxwfsyiy56c3vzpxxsk0q")))

(define-public crate-vimv-0.4.2 (c (n "vimv") (v "0.4.2") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "0hf6kc92mn4kz67c4n8j88k4bkn0l2lybbh5gqvy8y0qzhajxj1i")))

(define-public crate-vimv-0.4.3 (c (n "vimv") (v "0.4.3") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "1lzqzvias25ha28m6ivwhg37viajiyhknfbzkcjz3qq8xqajrmyz")))

(define-public crate-vimv-0.4.4 (c (n "vimv") (v "0.4.4") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "0azz0sdijs8b6chsj0agdf57kj1gfqnj21dg7q9jvhvj17ph420q")))

(define-public crate-vimv-1.0.0 (c (n "vimv") (v "1.0.0") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "1fjbg5rz0xgjfix8r3pijj06myn3in6c1wlc775mxhf4zdcy9rhn")))

(define-public crate-vimv-1.0.1 (c (n "vimv") (v "1.0.1") (d (list (d (n "arguably") (r "^1.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)))) (h "1f6bm4qyz8vj6qkw3rnjij30b12fc7h45951kbbsq17rda5z45m4")))

(define-public crate-vimv-1.1.0 (c (n "vimv") (v "1.1.0") (d (list (d (n "arguably") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "edit") (r ">=0.1.2, <0.2.0") (d #t) (k 0)) (d (n "trash") (r ">=1.2.0, <2.0.0") (d #t) (k 0)))) (h "1xz015rsipw5z657cy0y5pvb0c8a9k2v03k9hl5ax7chknagwnvb")))

(define-public crate-vimv-1.1.1 (c (n "vimv") (v "1.1.1") (d (list (d (n "arguably") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "edit") (r ">=0.1.2, <0.2.0") (d #t) (k 0)) (d (n "trash") (r ">=1.2.0, <2.0.0") (d #t) (k 0)))) (h "0mac4w3b8mngn5lxpgrx90xjr8jqm7kjigafrqfh9bzc0ribjjz9")))

(define-public crate-vimv-1.1.2 (c (n "vimv") (v "1.1.2") (d (list (d (n "arguably") (r "^1.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "trash") (r "^1.2") (d #t) (k 0)))) (h "0q6wq5sd4h93x3z3rh1cdnp3vh6vqxnirzc42cqwaghspm4w75rl")))

(define-public crate-vimv-1.2.0 (c (n "vimv") (v "1.2.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "trash") (r "^1.2") (d #t) (k 0)))) (h "0a1qrynj1mw1jqlryv41v4aj2pna2vpvypwhf300hiv4hqdarx9v")))

(define-public crate-vimv-1.3.0 (c (n "vimv") (v "1.3.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "trash") (r "^1.2") (d #t) (k 0)))) (h "1vfh0fchbj4006wh9azgxj7cbz7q8akl7dc7crkswji0ck5r4ibr")))

(define-public crate-vimv-1.4.0 (c (n "vimv") (v "1.4.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "trash") (r "^1.2") (d #t) (k 0)))) (h "153hpz4zim46jl6dcp20fzibbhfhh2jhddrrfznbsqd73add6gb7")))

(define-public crate-vimv-1.4.1 (c (n "vimv") (v "1.4.1") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "trash") (r "^1.2") (d #t) (k 0)))) (h "0ydbaa7108w529yw35zr21688dx2zz0rmmxc2nphkv4lhp5wik39")))

(define-public crate-vimv-1.5.0 (c (n "vimv") (v "1.5.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "1ds4sq5v4fpfbagsyln0ag120xhcr9d34qd7lmp8nm8jm74kdabi")))

(define-public crate-vimv-1.5.1 (c (n "vimv") (v "1.5.1") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "10fnv0ann3hkk9xy84qyr8lk2r3c349ms5r10ia498ranq47f00y")))

(define-public crate-vimv-1.6.0 (c (n "vimv") (v "1.6.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "1dd7zz192jngga5v69lflk6cjqh55wzrvw433sansv6f4w98mdq6")))

(define-public crate-vimv-1.6.1 (c (n "vimv") (v "1.6.1") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "0mr0s7nq60fqc6zmwbhy7l28c9bxdn8lnav7gcfk0iz65p1pc7lq")))

(define-public crate-vimv-1.6.2 (c (n "vimv") (v "1.6.2") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "0yrgi7svmdzdcpxnc6pygx4kr714rb3nvlizav4k644699vhjl54")))

(define-public crate-vimv-1.6.3 (c (n "vimv") (v "1.6.3") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3") (d #t) (k 0)))) (h "040hl41nzhas6fh1yn2dj15k207imjn4c4nh3xs6w8nfmc1fbicj")))

(define-public crate-vimv-1.7.0 (c (n "vimv") (v "1.7.0") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3.0") (d #t) (k 0)))) (h "1f43wbjpgq50ax0cq3cz4jrn2zga51g6i61n75mr1mjnpyf30zcr")))

(define-public crate-vimv-1.7.1 (c (n "vimv") (v "1.7.1") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3.0") (d #t) (k 0)))) (h "0vrwdyv87l7cl3syb2gggh6bpapq5pkjq33a7jnwx1gina05chki")))

(define-public crate-vimv-1.7.2 (c (n "vimv") (v "1.7.2") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "trash") (r "^1.3.0") (d #t) (k 0)))) (h "0f4gzp0ypb7ixqw0fy4ihq59ikd2pxwqafpvpdhgnf4iyvl46664")))

(define-public crate-vimv-1.7.3 (c (n "vimv") (v "1.7.3") (d (list (d (n "arguably") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "trash") (r "^2.0") (d #t) (k 0)))) (h "1sd3jhqkw283r2v62dhbk1yv039wldrg8ms86jam3h9224piz0zi")))

(define-public crate-vimv-1.7.5 (c (n "vimv") (v "1.7.5") (d (list (d (n "arguably") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "trash") (r "^2.0") (d #t) (k 0)))) (h "0dpbpcrbq0cdyi64aq8sny64qqhr36aiwpjv2ybxvfkn0jqha58z")))

(define-public crate-vimv-1.7.6 (c (n "vimv") (v "1.7.6") (d (list (d (n "arguably") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "trash") (r "^2.0") (d #t) (k 0)))) (h "0lvsllm93ccfdyrgnam74qby5lv8ws7mgzs23rl7iy8lrysgvxx5")))

(define-public crate-vimv-1.7.7 (c (n "vimv") (v "1.7.7") (d (list (d (n "arguably") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "trash") (r "^2.0") (d #t) (k 0)))) (h "1ccnry448yrjvraf9sbzcfkk8340ijqjh7xk0s6rar2smphyapnr")))

(define-public crate-vimv-1.8.0 (c (n "vimv") (v "1.8.0") (d (list (d (n "arguably") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "trash") (r "^2.0") (d #t) (k 0)))) (h "1lp9i8ldbdbb80b70zh8hjb654fqdwamvn0gx9vpy8xlmq7s633q")))

(define-public crate-vimv-2.0.0 (c (n "vimv") (v "2.0.0") (d (list (d (n "arguably") (r "^2.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trash") (r "^3.0.1") (d #t) (k 0)))) (h "19baxx3s0q9fpg1wmkyahjnnbv123bw9kclnckppqfs9f13bj1ps")))

(define-public crate-vimv-3.0.0 (c (n "vimv") (v "3.0.0") (d (list (d (n "arguably") (r "^2.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trash") (r "^3.0.1") (d #t) (k 0)))) (h "07bhnjrjc3s6q5fdjxhqqvkyidph4gbw36k3llnv7qbwg622nq1r")))

(define-public crate-vimv-3.1.0 (c (n "vimv") (v "3.1.0") (d (list (d (n "arguably") (r "^2.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "edit") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trash") (r "^3.0.1") (d #t) (k 0)))) (h "0y9w5qvfqhvndxkmm47x81jw37ysbxw5gpylp92mpm805q3bfncj")))

