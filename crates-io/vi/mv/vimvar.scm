(define-module (crates-io vi mv vimvar) #:use-module (crates-io))

(define-public crate-vimvar-0.1.0 (c (n "vimvar") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1ik0pdxsm7bw2vkjl0j7cj7bnwly2bn7lgvrgymbmjlv14r08xwh")))

(define-public crate-vimvar-0.1.1 (c (n "vimvar") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1iywjx2rjkmi6fwxv0c8qgali05w3chpy98awbg07521cdh2s10a")))

(define-public crate-vimvar-0.2.0 (c (n "vimvar") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0lriggmhybl80g2hdjyv6kd4blzryahx4i7anyrgv89dvs8a8hza")))

(define-public crate-vimvar-0.3.0 (c (n "vimvar") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "06pzx9yp68lfhfhzhcfwk9079h773qv4ikmpqr5737bnfas9ndrz")))

