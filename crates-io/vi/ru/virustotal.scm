(define-module (crates-io vi ru virustotal) #:use-module (crates-io))

(define-public crate-virustotal-0.1.0 (c (n "virustotal") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rhzs8znh37iya8cad1mjdvfj7yi84m9fhrygrb5lvq7jpgvbrfn")))

(define-public crate-virustotal-0.1.1 (c (n "virustotal") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ykg7f8ywrax5fg12pl7gffd4ck0vfi2gf50c8qkckcqa9s0lk6p")))

(define-public crate-virustotal-2.0.0 (c (n "virustotal") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1zykvzl62p2fcwrzhvlljdxpyh39gk5hg4xnx7lq5nm8ncai4nv8")))

