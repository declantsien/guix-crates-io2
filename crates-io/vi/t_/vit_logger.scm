(define-module (crates-io vi t_ vit_logger) #:use-module (crates-io))

(define-public crate-vit_logger-0.1.0 (c (n "vit_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 0)))) (h "1zql5diwhr7js0y227b9vp6fg9klsnjwvhm12dmv4j4ddbyn23aw")))

(define-public crate-vit_logger-0.1.1 (c (n "vit_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 0)))) (h "0njjl9nhjakd8211alpj5j7dxb6hrcqzbih7nb602ifzc3r41kdw")))

