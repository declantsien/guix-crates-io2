(define-module (crates-io vi ka vika_community) #:use-module (crates-io))

(define-public crate-vika_community-0.1.0 (c (n "vika_community") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n3wb2338194dadck4jxjh1lhsv9rq84klaklyp3sk56nl5gqsw6")))

(define-public crate-vika_community-0.1.1 (c (n "vika_community") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a6a1lxwx58f88m4cxf99cwgam8f8l3c3rgycrp16ggng6qml1j3")))

(define-public crate-vika_community-0.1.2 (c (n "vika_community") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10hfvq8258p5lvz3qfmwx2nmv2ks6d137yn9rcvj32hssmkbrn6x")))

