(define-module (crates-io vi ka vika) #:use-module (crates-io))

(define-public crate-vika-0.1.0 (c (n "vika") (v "0.1.0") (h "1nf11brkri0lsaj0iahhyn14v8hd0z5098wcxdrrq42ghr0mym3w")))

(define-public crate-vika-0.1.1 (c (n "vika") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (f (quote ("use_core"))) (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pmcj5c02h3871bnnn3iwi88g1q78gf0lvq4nshdbwizs748k4ps") (y #t)))

(define-public crate-vika-0.1.2 (c (n "vika") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (f (quote ("use_core"))) (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gaf9k38lmrzmlygp6x786pdyasm9bhaygllckz9c10z58sqiliw")))

