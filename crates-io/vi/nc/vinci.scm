(define-module (crates-io vi nc vinci) #:use-module (crates-io))

(define-public crate-vinci-0.2.0 (c (n "vinci") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0m9s2i5sr9cl55ir4vl1bykzwissqxn81ykp9bircxxf6a0nm9j0")))

