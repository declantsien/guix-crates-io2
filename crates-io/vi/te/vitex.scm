(define-module (crates-io vi te vitex) #:use-module (crates-io))

(define-public crate-vitex-0.1.0 (c (n "vitex") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0a5iqh0gkflss0pz4rz70ph7zs2vgkh8si75jyrxz2p4cvy1kywv")))

(define-public crate-vitex-0.2.0 (c (n "vitex") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0hq3d5c5wqgfmxxlk7zg78bm27kksn223qd65paha4h46cj8prqv")))

(define-public crate-vitex-0.2.1 (c (n "vitex") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zhhx86jybf6ja6nwqcv8zsh392d5mqfx9zn1h4kpyaqcdbj5ybr")))

