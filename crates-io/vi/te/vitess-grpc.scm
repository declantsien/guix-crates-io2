(define-module (crates-io vi te vitess-grpc) #:use-module (crates-io))

(define-public crate-vitess-grpc-0.1.0 (c (n "vitess-grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "1cq516xcy61ijddqzydllv6nq3c6gdj5i04f3ryy55nrclhvv1z7")))

(define-public crate-vitess-grpc-0.1.1 (c (n "vitess-grpc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "1fvp5pmlsm3imj0yr3a2nn4xig6glmmmjannb7sip685vrw09c24")))

(define-public crate-vitess-grpc-0.1.2+vitess-16.0.2 (c (n "vitess-grpc") (v "0.1.2+vitess-16.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "0lnjgszy00vj7vl1px3c190jpzzahb7dmr5lcsrnj5w31v2rilgp")))

(define-public crate-vitess-grpc-0.2.0+vitess-17.0.1 (c (n "vitess-grpc") (v "0.2.0+vitess-17.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "0dqp2mbx0i3zpfbdxnzs778yvlvyj4mhzgny1jgvpryah73y1dri")))

(define-public crate-vitess-grpc-0.3.0+vitess-18.0.0 (c (n "vitess-grpc") (v "0.3.0+vitess-18.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "1adcn1ggy4c84ab848dxdc891w6xhq632ysh4q4pyy9ys09ifcjz")))

