(define-module (crates-io vi ll villain) #:use-module (crates-io))

(define-public crate-villain-0.0.1 (c (n "villain") (v "0.0.1") (d (list (d (n "html") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1r58qxsjpxa2m4m6dprqkxlwmjaq6rbvkk6mm3hjj66sv5ml67cp")))

