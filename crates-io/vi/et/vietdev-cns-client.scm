(define-module (crates-io vi et vietdev-cns-client) #:use-module (crates-io))

(define-public crate-vietdev-cns-client-0.1.0 (c (n "vietdev-cns-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.0") (d #t) (k 0)))) (h "01a3xhqr0mk1i9am4qhkg5r9cxm3aa9lzspxx7as1mlllp0npc0x")))

