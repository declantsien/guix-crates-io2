(define-module (crates-io vi sc visctl) #:use-module (crates-io))

(define-public crate-visctl-0.1.0 (c (n "visctl") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "1glnv78saqkj4g6vrm9221blfgaz0aknxkcwydyx8d75495idrx4")))

(define-public crate-visctl-0.1.1 (c (n "visctl") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "websocket") (r "^0.20") (d #t) (k 0)))) (h "0msbc870m7yqn8iyr80v1bmy09hxd22vv0cig3ml758v32bn08rz")))

