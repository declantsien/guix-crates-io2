(define-module (crates-io vi sc viscous) #:use-module (crates-io))

(define-public crate-viscous-0.0.0 (c (n "viscous") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "mini-fs") (r "^0.2.2") (d #t) (k 0)) (d (n "shellwords") (r "^1.0.0") (d #t) (k 0)) (d (n "thrussh") (r "^0.22.1") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.18") (d #t) (k 0)) (d (n "twitter-stream") (r "^0.10.0-alpha.6") (d #t) (k 0)) (d (n "vfs") (r "^0.2.1") (d #t) (k 0)))) (h "12wxscvgbqvn1jm3qgzv6g9iibrs04zxy4sfyp5i58vnpf70sn0f")))

