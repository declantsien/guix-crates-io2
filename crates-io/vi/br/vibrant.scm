(define-module (crates-io vi br vibrant) #:use-module (crates-io))

(define-public crate-vibrant-0.1.0 (c (n "vibrant") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.15") (o #t) (d #t) (k 0)) (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.3.12") (d #t) (k 0)) (d (n "itertools") (r "^0.3") (d #t) (k 0)))) (h "0ds3pivl9mrhc9n5mx9cv341y51bqzc688jsri0x9pcm74lcq5pa") (f (quote (("dev" "clippy") ("default"))))))

