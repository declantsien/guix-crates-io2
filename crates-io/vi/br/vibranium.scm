(define-module (crates-io vi br vibranium) #:use-module (crates-io))

(define-public crate-vibranium-0.1.0 (c (n "vibranium") (v "0.1.0") (d (list (d (n "ethabi") (r "^7.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^11.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)) (d (n "toml-query") (r "^0.8.0") (d #t) (k 0)) (d (n "web3") (r "^0.7.0") (d #t) (k 0)))) (h "1ll9ffwjl1rk11sw2qd5pz8mlb0c8y7shb2wdvj7whniir4kp08j")))

