(define-module (crates-io vi ew viewbuilder-macros) #:use-module (crates-io))

(define-public crate-viewbuilder-macros-0.1.0 (c (n "viewbuilder-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "08dmv23s6r6yxdcwgj2ip9l0h9pv5wg87mdxmq9b5yl437vgy94l")))

(define-public crate-viewbuilder-macros-0.1.1 (c (n "viewbuilder-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "1r89n2mhwqjlg41j5lwgvrgxai02ba2amwbjj2kk17q9yqiqd4nm")))

(define-public crate-viewbuilder-macros-0.2.0 (c (n "viewbuilder-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0rzhfbw4ad5d69zyqw4vqlzgh936091dgz1076kk64msp4x544ma")))

