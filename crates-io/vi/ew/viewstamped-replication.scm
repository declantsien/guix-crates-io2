(define-module (crates-io vi ew viewstamped-replication) #:use-module (crates-io))

(define-public crate-viewstamped-replication-0.1.0 (c (n "viewstamped-replication") (v "0.1.0") (h "11sdb844js0z3wcm5wfcibpdy5mah648564fa04j5pcfh9gb2kb5") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.2.0 (c (n "viewstamped-replication") (v "0.2.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "0b0wkz2dhpvzp07a6qrrbqdyk707dqf1afgajqlflj2ky64bm2wk") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.3.0 (c (n "viewstamped-replication") (v "0.3.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "1rs3vnwhsfsn4x4q2c4f06br8vvljz3q6a1dw8y19al0bhnzv73p") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.4.0 (c (n "viewstamped-replication") (v "0.4.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "1jcgql6xr8ci71ry467zp8cb8rq4kr99iw5r36vzggfj1v85zpvq") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.5.0 (c (n "viewstamped-replication") (v "0.5.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "1zd9kf77rd2zz1crj3l84zi0wi22yiq5caavvcmc7ibzjrz79nv4") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.6.0 (c (n "viewstamped-replication") (v "0.6.0") (d (list (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "03j94mwgzk6z15j1ydk64gms30j1ygvdv6npkyh34j27w30aq7r0") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.7.0 (c (n "viewstamped-replication") (v "0.7.0") (d (list (d (n "postcard") (r "^1.0.8") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (f (quote ("v7"))) (d #t) (k 0)))) (h "0kwqiasrvjpci6xql77k7799a4h3crbnq00yxdyp8si6zgxg7syz") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.8.0 (c (n "viewstamped-replication") (v "0.8.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "05ivv5s50wv0p9dpq2dgbr577ypjmzhpnwg2l849x9d4sxp1hgnl") (f (quote (("default"))))))

(define-public crate-viewstamped-replication-0.9.0 (c (n "viewstamped-replication") (v "0.9.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("color" "derive"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m3b8s0vmmrj0jhkwar16pbqgq1knc2lirvqm2agzznnsy4fk9mq") (f (quote (("default"))))))

