(define-module (crates-io vi ew viewrs) #:use-module (crates-io))

(define-public crate-viewrs-0.1.0 (c (n "viewrs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "polars-sql") (r "^0.30.0") (d #t) (k 0)))) (h "1jyl550na1sq50i5p6dnlf5i684qnw57603hcf8kvn9aqpqavfxw") (y #t)))

(define-public crate-viewrs-0.1.1 (c (n "viewrs") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "polars-sql") (r "^0.30.0") (d #t) (k 0)))) (h "084zld8sivsw300wf410svddnfjsslq6ldilxa6yvq5prlgdpvaj")))

(define-public crate-viewrs-0.1.2 (c (n "viewrs") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "polars-sql") (r "^0.30.0") (d #t) (k 0)))) (h "0kb5wkrqf03qz7js9plf658lvi4qk2vzhqm53pcx8wryi4vp7mab")))

