(define-module (crates-io vi ew viewimg) #:use-module (crates-io))

(define-public crate-viewimg-0.1.0 (c (n "viewimg") (v "0.1.0") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "0jjgzrh1821smpnbrhx5rcfsnapfa1h5wwgdnyrbfbb02l3wp2pn")))

(define-public crate-viewimg-0.1.1 (c (n "viewimg") (v "0.1.1") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "1in77phzk7pj2992z12sfl6lkm7ypdqklnxxa2myz98skhvps3bq")))

(define-public crate-viewimg-0.1.2 (c (n "viewimg") (v "0.1.2") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "0s78cb7xzrw666bzk8mwh5jv9mgrb5k1hvdl3jfk3ligqa16shbx")))

(define-public crate-viewimg-0.2.0 (c (n "viewimg") (v "0.2.0") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "1ffh1ap7pkbnmcdi4dsg88n973q8fly3465ky65xfbb22h25pnjn")))

(define-public crate-viewimg-0.2.1 (c (n "viewimg") (v "0.2.1") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "125nydr054kcc78igv21f4gmz2gwzmqkzxwlivbrjyq2f05ky6za")))

(define-public crate-viewimg-0.2.2 (c (n "viewimg") (v "0.2.2") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "0d611k1261wd80nfv52x9pxm6l0nh4cv1ajbrbba6p12aghf2w55")))

(define-public crate-viewimg-0.2.3 (c (n "viewimg") (v "0.2.3") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "010wbpaw7xsc6zs25ad3lvvi39vhzqqrznpmhxa74bwk0k1v4rg8")))

(define-public crate-viewimg-0.2.4 (c (n "viewimg") (v "0.2.4") (d (list (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "= 0.20.0-alpha5") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.4.0-alpha4") (d #t) (k 0)))) (h "0z7rdkabwp94s2rwqbb7167c5f4x0hr1wrmf1mwbr6y1shqpialw")))

(define-public crate-viewimg-0.3.0 (c (n "viewimg") (v "0.3.0") (d (list (d (n "exr") (r "^0.7") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "1jwrai582wv351n1n7cf8k5i7ifsmlgn35fr7jdji4b8ng9f4z2m")))

(define-public crate-viewimg-0.4.0 (c (n "viewimg") (v "0.4.0") (d (list (d (n "exr") (r "^0.7") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "11p3l1yjk0kpc6n3m46r3crjybydqc4hqk54cmsx6bw2k718l3xb")))

(define-public crate-viewimg-0.4.2 (c (n "viewimg") (v "0.4.2") (d (list (d (n "exr") (r "^0.7") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "1xnglhcq3h11rsc65y36zw7rawzwsr16dgi7p0jcw6v06r07y1l0")))

(define-public crate-viewimg-0.5.0 (c (n "viewimg") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "^0.7") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "0rq5wqfdl4z64304f09y6874b2dv5qvdkf8gbv0l0gnvhf2rafx1")))

(define-public crate-viewimg-0.5.1 (c (n "viewimg") (v "0.5.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "= 0.7.3") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "1srdn8xdb39wn7yxv7vwpxba56gaki4mrng6z6z2mg6j36l1ghlk")))

(define-public crate-viewimg-0.6.0 (c (n "viewimg") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "= 0.7.3") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "1wf41g71v71d4fbdsr2w4zskc21kdn1gz16rvjl72s3b9s76g5j3")))

(define-public crate-viewimg-0.7.0 (c (n "viewimg") (v "0.7.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "= 0.7.3") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "1926jm5b2cmd4li4pv4y55fv0xdvsh8aqk1sbqas2dabp6xy86b5")))

(define-public crate-viewimg-0.8.0 (c (n "viewimg") (v "0.8.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "= 0.7.3") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.5") (d #t) (k 0)))) (h "10c6sa8l6zapf63488qz6mz3a7ylmmlxyjfk2xqsdm6crkah39n9")))

(define-public crate-viewimg-0.8.1 (c (n "viewimg") (v "0.8.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "=0.7.3") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.13") (d #t) (k 0)))) (h "06hg7587lkns04qw95rg23l8ica5d4cd47rbkvcm8m3ikf4wy9pn")))

(define-public crate-viewimg-0.9.0 (c (n "viewimg") (v "0.9.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "exr") (r "^1.72.0") (d #t) (k 0)) (d (n "image2") (r "^0.11.3") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.13") (d #t) (k 0)))) (h "136b10d34wgbdd9v1ck8sxwafv4wf1ydhnggv46wgpy19bc1agwa")))

