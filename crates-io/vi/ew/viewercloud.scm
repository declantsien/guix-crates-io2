(define-module (crates-io vi ew viewercloud) #:use-module (crates-io))

(define-public crate-viewercloud-0.2.0 (c (n "viewercloud") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "kiss3d") (r "^0.27.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)))) (h "0q7p5kzcpdllm30ryl58cx8q6k1gwmjan0yipsb49xrdwh09vka1")))

(define-public crate-viewercloud-0.2.1 (c (n "viewercloud") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "kiss3d") (r "^0.27.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)))) (h "124z8a94g6vmdkpqlqrallkr3s7d0xjhnj2sx3710x0w3p4sldx4")))

