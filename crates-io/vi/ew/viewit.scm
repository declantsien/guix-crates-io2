(define-module (crates-io vi ew viewit) #:use-module (crates-io))

(define-public crate-viewit-0.0.0 (c (n "viewit") (v "0.0.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r09vrk63f7k7f9b8pjgf178j57rsasiracg5cp09m5646p54fyf")))

(define-public crate-viewit-0.0.1 (c (n "viewit") (v "0.0.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06kpn63lmjq8380pzb8ach471s4v9anyqlsnsyr7c8qqsdrm1f44")))

(define-public crate-viewit-0.1.1 (c (n "viewit") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "027db2w7x6b2mnkbxhs72cf4bldgbg15q0f2ag94qv4ka5is9a5p")))

(define-public crate-viewit-0.1.2 (c (n "viewit") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hx50kr1x8yr44l4yjk6lg6qxc40mx51jqp81dnfiqbs9pp9v8fh")))

(define-public crate-viewit-0.1.3 (c (n "viewit") (v "0.1.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s0mv8vyxs08gzw9x2z42cqwkrzbfjfm0y2n80w1frfxqb5k2jhg") (y #t)))

(define-public crate-viewit-0.1.4 (c (n "viewit") (v "0.1.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fa37n20hgjmhf1fpkkshzbiivwywb2pqjl0j91dyj58lfbp9pb8")))

(define-public crate-viewit-0.1.5 (c (n "viewit") (v "0.1.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzasz6scm4dh37smihi1vc1narigk29zhfxdn0g0wp9pp357blx")))

