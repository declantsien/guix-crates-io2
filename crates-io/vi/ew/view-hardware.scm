(define-module (crates-io vi ew view-hardware) #:use-module (crates-io))

(define-public crate-view-hardware-0.1.0 (c (n "view-hardware") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1cq32pkmpvyzf0pg1j70nby8wycd616w730p6m0g74g3frcf8ymk")))

(define-public crate-view-hardware-0.1.1 (c (n "view-hardware") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1h16wg86fikgqpqlficwzz8ls2a03z1lyaiammbx7j9ks7a85brg")))

(define-public crate-view-hardware-0.1.2 (c (n "view-hardware") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "05r2xavzsq9dv278j6r7vww2rb2yg3xq3j7yawg4670knblz2zg0")))

