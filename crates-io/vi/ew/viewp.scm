(define-module (crates-io vi ew viewp) #:use-module (crates-io))

(define-public crate-viewp-0.1.0 (c (n "viewp") (v "0.1.0") (h "1zcjz41k0v6y2vigmqh4y7nj1r9wxjzf9a7ly2cy8pi54h0ljz2l")))

(define-public crate-viewp-0.1.1 (c (n "viewp") (v "0.1.1") (h "1j56v12ri0rxvwlzczzvar73bzyps8gdwiamj1sja31blrq1nl2k")))

(define-public crate-viewp-0.1.2 (c (n "viewp") (v "0.1.2") (h "1pjkh94y251ckcsxsni6jxql3gikiqwk7xzziajr2idypjsm02zw")))

(define-public crate-viewp-0.1.3 (c (n "viewp") (v "0.1.3") (h "1pcdh54pp3vyqs9qpmyrmsjgc3v2xrm27n7narrhmizgkvpbc6q0")))

(define-public crate-viewp-0.1.4 (c (n "viewp") (v "0.1.4") (h "1s2nqqj4jv4pah8ms6f0mmsf16wj3akmprh8gfnk7sxg1c1bm8mf")))

