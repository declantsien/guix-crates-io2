(define-module (crates-io vi ew view_macro) #:use-module (crates-io))

(define-public crate-view_macro-0.0.3 (c (n "view_macro") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1q2cwh4jwp5nd47d9vrv0q60whkvwia1g5gk5zsnzanbxmj687wi")))

(define-public crate-view_macro-0.1.1 (c (n "view_macro") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "06j6pv59zswh0cl4djay41452rir70j91k377b93j0cl7h11jyba")))

(define-public crate-view_macro-0.1.2 (c (n "view_macro") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0yljpz4pmz0fzxp2p0110r3bmaac8mbn8v7dbdbjrrj5wxipqsn3")))

