(define-module (crates-io vi ew viewy-codegen) #:use-module (crates-io))

(define-public crate-viewy-codegen-1.7.2 (c (n "viewy-codegen") (v "1.7.2") (h "0ixz1hcyjdj27l43kqyk9kphll9kf4ah3j85b5id4w9rk9whvmb8")))

(define-public crate-viewy-codegen-1.8.0 (c (n "viewy-codegen") (v "1.8.0") (h "11q8qrmrs38qvvv3rb8yhwv3g9z2h8rnpgnq420rj5r8rp5991lj")))

(define-public crate-viewy-codegen-1.8.1 (c (n "viewy-codegen") (v "1.8.1") (h "1kyrknacxmk9xb44g109n4x4qmhx5cnxgxbyz3sw1vv9yx3rd553")))

(define-public crate-viewy-codegen-1.8.2 (c (n "viewy-codegen") (v "1.8.2") (h "02y99mgpxx4nq9qywvfps5jasyrc33vjrd5m37a4cq1xyp05qcdy")))

(define-public crate-viewy-codegen-1.8.3 (c (n "viewy-codegen") (v "1.8.3") (h "0dlg5pwxa1v04dcb7kwwxsz0pgqkvblphqcindhwyrwgq6pglvm2")))

(define-public crate-viewy-codegen-1.8.4 (c (n "viewy-codegen") (v "1.8.4") (h "0mf8ci636swmlkfhbahla93kpijzam560rib445a9jg9wjy28knj")))

(define-public crate-viewy-codegen-1.8.5 (c (n "viewy-codegen") (v "1.8.5") (h "0g2w538h44gvxbnv7dydhi7i56rsyql1l8g9l8p9bj7ba8wxm390")))

(define-public crate-viewy-codegen-1.8.6 (c (n "viewy-codegen") (v "1.8.6") (h "0b8s1m7aqnikq18rlkllrc5yx9mwj0cnfx22zv1pn7s77ms9xn8k")))

(define-public crate-viewy-codegen-1.8.7 (c (n "viewy-codegen") (v "1.8.7") (h "1vdr7jnkhxx7s1py30r5xi9xjw0v3f11aaf6pbifpl7w8zbb3j6z")))

(define-public crate-viewy-codegen-1.8.8 (c (n "viewy-codegen") (v "1.8.8") (h "1ngdc2c4niskab7x1p1n2bhy49phni99zlbciwwq7bvj5jp38n8z")))

(define-public crate-viewy-codegen-1.8.9 (c (n "viewy-codegen") (v "1.8.9") (h "02633hwj032hz0hf53ia68ac18jlj02ah2m57q0p55b0hb6c2zxr")))

(define-public crate-viewy-codegen-1.8.10 (c (n "viewy-codegen") (v "1.8.10") (h "0c5r13qpzip39qx0swnaf3najdxm1dm644lg2hf88aj4dk9z6ibv")))

(define-public crate-viewy-codegen-1.8.11 (c (n "viewy-codegen") (v "1.8.11") (h "10yc9wjh29qsqfksqzw3v37r4sz9kxpwdmkqy2pxfmrw4r38jq6v")))

(define-public crate-viewy-codegen-1.8.12 (c (n "viewy-codegen") (v "1.8.12") (h "15ysc9kax41inlfgrhrzkcby700wqyzvy8prqmypj5cgl375azjx")))

(define-public crate-viewy-codegen-1.8.13 (c (n "viewy-codegen") (v "1.8.13") (h "1qcwfjvw8zln3kmaqsavvria94010dc2vfrfibym7rc6dic1llvg")))

(define-public crate-viewy-codegen-1.8.14 (c (n "viewy-codegen") (v "1.8.14") (h "0l9x939f2h7wdks6zlwrx0pl7yw7nhhfyjm3c42c91hzl18h106q")))

(define-public crate-viewy-codegen-1.8.15 (c (n "viewy-codegen") (v "1.8.15") (h "113qfannrgms18g2hn9fmvass9g91n1g48ln806i89nbjdh86aw5")))

(define-public crate-viewy-codegen-1.9.0 (c (n "viewy-codegen") (v "1.9.0") (h "1zsry3bpygfqmp8g84rqmqnrjz3w3m0wldldxik8qb3sm5yhx62w")))

(define-public crate-viewy-codegen-1.9.1 (c (n "viewy-codegen") (v "1.9.1") (h "0mm231hvj34ag406gis2q93mwk9h5kmkyhs6q04bsb9b8xkwpxhg")))

(define-public crate-viewy-codegen-1.9.2 (c (n "viewy-codegen") (v "1.9.2") (h "0bfdk5nvw0hp9qpwlals8gyxf5j98pnzr75rzjk23ccjdvqij147")))

(define-public crate-viewy-codegen-1.9.3 (c (n "viewy-codegen") (v "1.9.3") (h "0p1lqb5qg6d5x2pldcgdr37jnzsjfkm7gb8vcckyflpf19pdqzri")))

(define-public crate-viewy-codegen-1.10.0 (c (n "viewy-codegen") (v "1.10.0") (h "0j8wqafpvddq335s517vjs12mj6x3vr94npw5icambjpbzq8xbpg")))

(define-public crate-viewy-codegen-1.10.1 (c (n "viewy-codegen") (v "1.10.1") (h "0wnx6gncgxa2il90qgpabi20xy4580f2qpxcw0kny5kzzvy2bh30")))

(define-public crate-viewy-codegen-1.10.2 (c (n "viewy-codegen") (v "1.10.2") (h "1kxgnw1gz19dily5i2syl9kybd2jpcb511fx3bmh340bmrh7z2dh")))

(define-public crate-viewy-codegen-1.10.3 (c (n "viewy-codegen") (v "1.10.3") (h "1rb6xjwmj35rryjl43rd8lr5f7hixih8b71m64ih10p3qfpd9nac")))

(define-public crate-viewy-codegen-1.10.4 (c (n "viewy-codegen") (v "1.10.4") (h "0mf3mxpajgxrhifxxknjfim44v3ijwlqxcjj96d9z3zjyk59zyzq")))

(define-public crate-viewy-codegen-1.10.5 (c (n "viewy-codegen") (v "1.10.5") (h "08qxsginx5h7ka59hqvzslggl2kpgfdg2vy4mhslj2nnlx6wx4ia")))

(define-public crate-viewy-codegen-1.11.0 (c (n "viewy-codegen") (v "1.11.0") (h "1h356w1hinrnxfidv4balywps1669vd5y03gc7cd4akbx0mdgbf4")))

(define-public crate-viewy-codegen-1.12.0 (c (n "viewy-codegen") (v "1.12.0") (h "1bbjjf840h2qgn9w9njg5bkhw48jjmjxz8czj1v1k5k5gnxcjcy7")))

(define-public crate-viewy-codegen-1.12.1 (c (n "viewy-codegen") (v "1.12.1") (h "0rmxmfq11kijg5cyw0l6yk1nf1ijzwc7zy7bccnxzwi9bh3b1gwc")))

(define-public crate-viewy-codegen-1.12.2 (c (n "viewy-codegen") (v "1.12.2") (h "1wjdh002bmw3j4p2h57bwcgds8nw903zk0nyaw5j60nzkir1nll0")))

(define-public crate-viewy-codegen-1.12.3 (c (n "viewy-codegen") (v "1.12.3") (h "07vzfwccpk359kfrp2yzqzy5gkv763nxyz2cis62gg4xzns1y91f")))

