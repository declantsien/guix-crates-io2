(define-module (crates-io vi ew view) #:use-module (crates-io))

(define-public crate-view-0.0.1 (c (n "view") (v "0.0.1") (h "0xiin9rxybrcjybff4q76xkznk0756qn98fqyjpq6p1wa630h6s7")))

(define-public crate-view-0.0.2 (c (n "view") (v "0.0.2") (h "0d3b0v0rpl7kf07pv4pav0gwlc87pypp87nz4xvkmng04vyh6651")))

(define-public crate-view-0.0.3 (c (n "view") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "view_macro") (r "^0.0.3") (d #t) (k 0)))) (h "1621grq0c6281nr5cv1l6iifg72jj4brk1afpx66wvbi4s8v2k3n")))

(define-public crate-view-0.1.0 (c (n "view") (v "0.1.0") (h "0y27kmk2a64rzn8wd2m0iy104nx30dnlg4glf0sh5n81cwawgxa0")))

(define-public crate-view-0.1.1 (c (n "view") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "view_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1sw44qs3xfbvr1x76pqq3rs38i5nr0085is12a3bsjy64h8c3pfp")))

(define-public crate-view-0.1.2 (c (n "view") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "view_macro") (r "^0.1.2") (d #t) (k 0)))) (h "15dv50s77qyl6rjvc76i2aybj4ir63k7s43869963nas8c4aazhm")))

(define-public crate-view-0.1.3 (c (n "view") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "view_macro") (r "^0.1.2") (d #t) (k 0)))) (h "19vgbcwz12204hnkrll9m2czfi3ggwr7zhqfci0h5d1pc6qfxxbs")))

(define-public crate-view-0.2.0 (c (n "view") (v "0.2.0") (h "1jn6n2nppzjiwcx7fchv1f879yiav1pirak999v7gv6i5zx94953")))

(define-public crate-view-0.3.0 (c (n "view") (v "0.3.0") (h "1cqdij9ykpfkqy5w3284whggbf7pib56bg3rslwn1wfjb949ly8y")))

(define-public crate-view-0.4.0 (c (n "view") (v "0.4.0") (h "1vgzjg97g0b398inxb6m825h2pklly7ip48fi26id0xbgpjr1lkh")))

(define-public crate-view-0.4.1 (c (n "view") (v "0.4.1") (h "0ax9vdwdhfd4hxzi2pdisip9kk0s17g3nc43gmh0yqxd2shmwphv")))

