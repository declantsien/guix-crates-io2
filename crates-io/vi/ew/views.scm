(define-module (crates-io vi ew views) #:use-module (crates-io))

(define-public crate-views-0.1.0 (c (n "views") (v "0.1.0") (h "0fr1dkj4xfzzc6fpphcha9dpnr9qzhfjgqpkssvwi27lnhcif34l") (y #t)))

(define-public crate-views-0.2.0 (c (n "views") (v "0.2.0") (h "00j375plxg1fq9vy97w992qqn0y1zxhqcgq6b68fidny274rz8j5") (f (quote (("coding")))) (y #t)))

(define-public crate-views-0.3.0 (c (n "views") (v "0.3.0") (h "0dxrm6wwws4b0yx2rcrapjxv2f3hqqai93mplzkhkf7d7i87jnyl") (y #t)))

(define-public crate-views-0.4.0 (c (n "views") (v "0.4.0") (h "0z9kwcq6jq4s3k3pvaaqy8m5ywb86g31npgabggpkazlbbkg08km") (y #t)))

