(define-module (crates-io vi zz vizz) #:use-module (crates-io))

(define-public crate-vizz-0.1.0 (c (n "vizz") (v "0.1.0") (d (list (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0qai94rnw8ipsq0y5dvky3n4sywdbxkcs3wc20bmzkj5w3q36aln")))

(define-public crate-vizz-0.2.0 (c (n "vizz") (v "0.2.0") (d (list (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "vizz_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "16am99287wd1h6v5i0690ri8x4ycywwcfrab037waw8854g7dj62") (f (quote (("derive" "vizz_derive") ("default" "derive"))))))

