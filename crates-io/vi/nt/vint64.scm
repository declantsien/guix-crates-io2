(define-module (crates-io vi nt vint64) #:use-module (crates-io))

(define-public crate-vint64-0.1.0 (c (n "vint64") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "1n84a69f8gwywc3hkljwp2x3xj88lpxskwx25h5vd8wspw3xyb2z")))

(define-public crate-vint64-0.1.1 (c (n "vint64") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "0gq7g98g7sipwdbyn9vb564kb2pfq3mphk3w6k61l39q9c8ip3kq")))

(define-public crate-vint64-0.1.2 (c (n "vint64") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "0hqpq7n2kb3bgmbmiizl8319cax9v4vji0dhbxic95m93p3jyfxp")))

(define-public crate-vint64-0.2.0 (c (n "vint64") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "1jx5hmzxngzsv59y9knr09ar9arnsxm0cbwnpqj34h5apl8028xg")))

(define-public crate-vint64-0.2.1 (c (n "vint64") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "1f4gw524pmdq7ijcihnhr6w2x61ccmy47z1xqnq2jic0p20dp04l")))

(define-public crate-vint64-0.3.0 (c (n "vint64") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "0j0kxch21r24v9id6agl537igm0zlqrqzvby17p85xgzkfl2rcz5")))

(define-public crate-vint64-1.0.0 (c (n "vint64") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)))) (h "18rd4ivkj0amljgs547rcjslvnwfh4rqfq49rnwir5lz9vhhkjxv") (f (quote (("std"))))))

(define-public crate-vint64-1.0.1 (c (n "vint64") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0b24v75bjw2qcd4hbiw5ay5cgndl13b5042sizn47pxmhxwxvcz0") (f (quote (("std"))))))

