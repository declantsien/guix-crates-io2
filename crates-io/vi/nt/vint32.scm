(define-module (crates-io vi nt vint32) #:use-module (crates-io))

(define-public crate-vint32-0.1.0 (c (n "vint32") (v "0.1.0") (h "1qhl4adghcn3g96k5n99k1hcypbh2chfg7xysaw3xwwl4vap2dsv")))

(define-public crate-vint32-0.1.1 (c (n "vint32") (v "0.1.1") (h "0cy7yqzy4zaf6yqri18xdlspzwbjacsqnmq77h8wyhi0k3ybprlc")))

(define-public crate-vint32-0.1.2 (c (n "vint32") (v "0.1.2") (h "198xrr70nm4l1d3m0djvkrbiljqk8ygyvqmr7ldzdlp25qxblsp0")))

(define-public crate-vint32-0.2.0 (c (n "vint32") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0kp3q4mdypcdlp2cly46g6b9ml792h4f26bjbkkh3haw4jy9siif") (f (quote (("default") ("common-encoding" "itertools"))))))

(define-public crate-vint32-0.3.0 (c (n "vint32") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0bs2i43s6amry13ziwrxdbxjlikkfyd73gkpn31p2vj7cfiz0d41") (f (quote (("default") ("common-encoding" "itertools"))))))

