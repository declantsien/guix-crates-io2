(define-module (crates-io vi nd vindicator) #:use-module (crates-io))

(define-public crate-vindicator-0.1.0 (c (n "vindicator") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.9") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0dy6y2g98ynq47f7h8qf075d60s8gjda4zjbnl9vh5wvrsmq38sf")))

