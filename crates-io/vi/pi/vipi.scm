(define-module (crates-io vi pi vipi) #:use-module (crates-io))

(define-public crate-vipi-0.1.0 (c (n "vipi") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1zqbp499bzwkrk1xm9nvbh55b0w5nd2n8jvv896zhcwhyq9pwdvp")))

(define-public crate-vipi-0.1.2 (c (n "vipi") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1ygj3sb024zmfdivgzka64igmq9wzmvcrz6a5vyb5zzkrqzlyf2y")))

(define-public crate-vipi-0.1.3 (c (n "vipi") (v "0.1.3") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1y4x3y1kp9l24zx59f7if7159wj07k4p93xmjc7g1x6bdc5vnlc3")))

(define-public crate-vipi-0.1.4 (c (n "vipi") (v "0.1.4") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0wga0q73zlrmgkzvwl0rsnkj82c3j6fx6p7kl5lvkaqmmh9jgnpd")))

(define-public crate-vipi-0.1.5 (c (n "vipi") (v "0.1.5") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1h0k6xxlna7y46rv45f6m878y5dwwqfcc4vj0ygq63zisj48mnxm")))

(define-public crate-vipi-0.1.6 (c (n "vipi") (v "0.1.6") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1lnb1m8iqnq0fr17l8sjr5yiks9inkbczca3xmwfmb7kssn0dvvi")))

