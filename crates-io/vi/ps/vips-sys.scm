(define-module (crates-io vi ps vips-sys) #:use-module (crates-io))

(define-public crate-vips-sys-0.1.0 (c (n "vips-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1290av3sqgqvbcrhh324dsgfc58s3g9cjm09qpvlhjg3wwpha1rp")))

(define-public crate-vips-sys-0.1.1 (c (n "vips-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1ibbxbbdpvr7j43hrsfypz76zdnkknc45vfdw6jqn8npc80ps1rs")))

(define-public crate-vips-sys-0.1.2 (c (n "vips-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0jcwb1zzr3h1d7w5299r87dhxvy5zzcr97n9v3iw8gwa0wq21spb")))

(define-public crate-vips-sys-0.1.3-beta.0 (c (n "vips-sys") (v "0.1.3-beta.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0a62wcn37hi773fhd5zd8q4vlci0dw5rj13pwaizfmb4ww9m987d") (l "vips")))

