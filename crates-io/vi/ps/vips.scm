(define-module (crates-io vi ps vips) #:use-module (crates-io))

(define-public crate-vips-0.1.0-alpha.1 (c (n "vips") (v "0.1.0-alpha.1") (d (list (d (n "vips-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x8slrs7spad9b8z2rggyj89928m54s7b9418cxram1wa7kid8mc")))

(define-public crate-vips-0.1.0-alpha.2 (c (n "vips") (v "0.1.0-alpha.2") (d (list (d (n "compiletest_rs") (r "^0.3.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "vips-sys") (r "^0.1.2") (d #t) (k 0)))) (h "11k1k7lallhllyj584m8zys3w19ym6z5ap29yzyh8qrgy8vg8k27")))

