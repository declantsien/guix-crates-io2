(define-module (crates-io vi bh vibha) #:use-module (crates-io))

(define-public crate-vibha-0.1.0 (c (n "vibha") (v "0.1.0") (d (list (d (n "linefeed") (r "^0.4") (d #t) (k 2)) (d (n "randomize") (r "^0.890.0") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 2)) (d (n "trivial_colours") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "libloaderapi" "winbase"))) (d #t) (k 2)))) (h "0k7d5flmjfbjp87qk88jy7jcj839sik4q4kmfx5b0in1qjkh5lx3")))

(define-public crate-vibha-0.1.1 (c (n "vibha") (v "0.1.1") (d (list (d (n "linefeed") (r "^0.4") (d #t) (k 2)) (d (n "randomize") (r "^0.890.0") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1") (d #t) (k 2)) (d (n "trivial_colours") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "libloaderapi" "winbase"))) (d #t) (k 2)))) (h "0inx8g7ndkjqxpcjl59ps8qvv1pjrh2fv5baspx07wkc6lya18bv")))

