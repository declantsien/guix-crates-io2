(define-module (crates-io vi sa visa-rs-proc) #:use-module (crates-io))

(define-public crate-visa-rs-proc-0.1.0 (c (n "visa-rs-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1606ns3ypfdjyzjg0fyqrylgkl8w15ansmw526p57qp7zd8hsm1m") (y #t)))

(define-public crate-visa-rs-proc-0.2.0 (c (n "visa-rs-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v5192agjqrcbzrz1g2lx8ya8whg11rcfgc7b94z2x98v5qm3qh3")))

(define-public crate-visa-rs-proc-0.2.1 (c (n "visa-rs-proc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1clf078m4y814bf1sjq57dhy44ih93f2z853c8p77ghbvhhdxi22")))

(define-public crate-visa-rs-proc-0.2.2 (c (n "visa-rs-proc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "visa-sys") (r "^0.1") (d #t) (k 0)))) (h "0nfnyyyhwwr4am05y19rv2dcswjyjvygjcshmfw297qvn6hadrmy") (y #t)))

(define-public crate-visa-rs-proc-0.2.3 (c (n "visa-rs-proc") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02xhbg8r81ghdkjnvc5g6bvw6v23dj44av03jhywaiilj8vb62lz")))

(define-public crate-visa-rs-proc-0.3.0 (c (n "visa-rs-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "037i08ni05qdy6878sxnqna3bpa2vg8ab78q2wdin49jbgm1pk4y")))

(define-public crate-visa-rs-proc-0.3.1 (c (n "visa-rs-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08l13x2s5sd386974fn9y13ni1rh7z01djrgq9gfg4gmvrsn06qv")))

(define-public crate-visa-rs-proc-0.3.2 (c (n "visa-rs-proc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0a6zxqhns1sygmis6q1a0k9s2gdm1h6lwxk916lmk9k4mgmkdi9a")))

(define-public crate-visa-rs-proc-0.4.0 (c (n "visa-rs-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g0q44mr8x6vx51pkz188s269lk1rw1ji96vlzp4id046hilsj4l")))

(define-public crate-visa-rs-proc-0.5.0 (c (n "visa-rs-proc") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11phd9v9v24if8kzawnf5fmil689wx27lfxpcasfpqwq092x2c6v")))

(define-public crate-visa-rs-proc-0.6.0 (c (n "visa-rs-proc") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "visa-sys") (r "^0.1.6") (f (quote ("proc"))) (d #t) (k 0)))) (h "051lf6vb1vndaqidjvwa07ifdmrzjdrznc79v4vxy6jm8jma3305")))

(define-public crate-visa-rs-proc-0.6.1 (c (n "visa-rs-proc") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "visa-sys") (r "^0.1.6") (f (quote ("proc"))) (d #t) (k 0)))) (h "1dyphccrfmav48qc26hwlq85iiwwfh4vxi4f4k93qdcc3455w5x0")))

