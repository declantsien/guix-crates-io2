(define-module (crates-io vi sa visa-api) #:use-module (crates-io))

(define-public crate-visa-api-0.1.0 (c (n "visa-api") (v "0.1.0") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "03rdl2hpp4b423hc5xdgv5452bxvy48d217g651xv6ba9c9522ak")))

(define-public crate-visa-api-0.1.1 (c (n "visa-api") (v "0.1.1") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "1bm0ijvlakiycjbpsjpbzdvyssh3kshpmq9ar068v4fclsaccp9k")))

(define-public crate-visa-api-0.1.2 (c (n "visa-api") (v "0.1.2") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "1xcab7q1ryxf99021n8yljsg3y93gyz7jml63cbsap19y5wg8r3f")))

(define-public crate-visa-api-0.2.0 (c (n "visa-api") (v "0.2.0") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "0pkc3wipi6bwlmzwncgsn16zh38gpmriwg6jl50fwqphhisnjc6x")))

(define-public crate-visa-api-0.2.1 (c (n "visa-api") (v "0.2.1") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "1d3sac7sa2j98dh0hnpafzji0yq7jb35xj8ccn4n1bdk0sshbwmz")))

(define-public crate-visa-api-0.2.2 (c (n "visa-api") (v "0.2.2") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "16hky15kwkmgrsg5jn9jm33cmjvl8gg78ww386gim42lcllva9dn")))

(define-public crate-visa-api-0.3.0 (c (n "visa-api") (v "0.3.0") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "0g7rlfks7clyzpr1mg23mn4h2b72swjqcdcxfsv1k6ak07wx8qwm")))

(define-public crate-visa-api-0.3.1 (c (n "visa-api") (v "0.3.1") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "0n6fvfcs3vs4fq0rlm5f1qcf3h7fbpisqbqkylyl76mp7csi1hc9")))

(define-public crate-visa-api-0.3.2 (c (n "visa-api") (v "0.3.2") (d (list (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "visa-rs") (r "^0.5.0") (d #t) (k 0)))) (h "1v8h9j2xbgwziqmi701jmx99qq1aa7xn4wbkypjblp82kjravl8b")))

