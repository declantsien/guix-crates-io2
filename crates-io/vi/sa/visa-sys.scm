(define-module (crates-io vi sa visa-sys) #:use-module (crates-io))

(define-public crate-visa-sys-0.1.0 (c (n "visa-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "13x9130xq1lz1wcxvg4vr7mgnnifbzkni2z71jgyi34cw7y1c666")))

(define-public crate-visa-sys-0.1.1 (c (n "visa-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "1pqy6pqzvcy2i0wjhq22qwwd59z7nr5sl8d23da8mrkrgpahyfma")))

(define-public crate-visa-sys-0.1.2 (c (n "visa-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "1n33fiyfjvjgp10hlq80qsfhpp6ggzv55gndv2fhqp4ivqqi1fj7") (y #t)))

(define-public crate-visa-sys-0.1.3 (c (n "visa-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "0kgcllml4f639vrjjb3q50q7ixsgvjws234qfwsjhyg88jp132gj")))

(define-public crate-visa-sys-0.1.4 (c (n "visa-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "07jvf8r8zkrr29lifczhw912qs167vlqamimjq2ym21zg6ffcl9n")))

(define-public crate-visa-sys-0.1.5 (c (n "visa-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "1wa10g79n3xm3irrnpkapsdim22b24yrqzq8pnkarm6yxm3yy5gy")))

(define-public crate-visa-sys-0.1.6 (c (n "visa-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)))) (h "0vn83ckxrwnacs2v79rz1h37x1kqzrzxvm9w0dcyrdkxy4gd510w") (f (quote (("proc"))))))

