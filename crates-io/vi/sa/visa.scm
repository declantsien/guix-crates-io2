(define-module (crates-io vi sa visa) #:use-module (crates-io))

(define-public crate-visa-0.1.0 (c (n "visa") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1pgkfbi9hwq9v94pkq2qs5v0w4zzqwgs6qmdqcc4lcvv80r8z0y4")))

(define-public crate-visa-0.1.1 (c (n "visa") (v "0.1.1") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)))) (h "0dbjz8xasbg0ichyl2ckpg8p3a1y6fpghyb6b7d8h85p0rx7fx3l")))

