(define-module (crates-io vi de videotoolbox-sys) #:use-module (crates-io))

(define-public crate-videotoolbox-sys-0.0.1 (c (n "videotoolbox-sys") (v "0.0.1") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "coremedia-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "corevideo-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14ygkixglkxqafg76fvln5lh5wf6jnhr51fv9pdb37jk26sq5g4w")))

(define-public crate-videotoolbox-sys-0.0.2 (c (n "videotoolbox-sys") (v "0.0.2") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "coremedia-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "corevideo-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sw77smy6y0rg277z1v043sf6dk4n7g9wls95sbqr72mnsvwv37z")))

