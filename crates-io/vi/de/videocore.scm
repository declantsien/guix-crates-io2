(define-module (crates-io vi de videocore) #:use-module (crates-io))

(define-public crate-videocore-0.1.0 (c (n "videocore") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "13amqfvcxcjz9cbhz31j8csdi529849x8c4nhv9hrfifvd8sq3ax")))

(define-public crate-videocore-0.1.1 (c (n "videocore") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0vj6gc8gqdmdjbj441xgm0nj3ld2f9jys9p3xjgval0fim4nsvhk")))

(define-public crate-videocore-0.1.2 (c (n "videocore") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0mj3cngvn3g01zwdmr1n1gng68dc9pzvls3py4nqwvjrlbznhsmw")))

(define-public crate-videocore-0.1.3 (c (n "videocore") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0r9v3bwmk4csv3yhbaf4gdn5003l7p27lck78mdvqz9vg69dmkn0")))

