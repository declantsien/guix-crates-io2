(define-module (crates-io vi de videostream) #:use-module (crates-io))

(define-public crate-videostream-0.1.0 (c (n "videostream") (v "0.1.0") (d (list (d (n "videostream-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0klksf3wmlja2dgadi0d5zj8d6z0y64gq4s8pnwbw0mnm6bx6rwb")))

(define-public crate-videostream-0.2.0 (c (n "videostream") (v "0.2.0") (d (list (d (n "videostream-sys") (r "^0.2.0") (d #t) (k 0)))) (h "150hbrvibxrmf7id7kh1ik6j54lhp75xxvgmq97gy3fapac1dwzy")))

(define-public crate-videostream-0.3.4 (c (n "videostream") (v "0.3.4") (d (list (d (n "videostream-sys") (r "^0.3.4") (d #t) (k 0)))) (h "1f7hbs9g25a1klv7scx93s4nsf1flg5jl1i7kjip7ljd498zv8qa")))

(define-public crate-videostream-0.4.0 (c (n "videostream") (v "0.4.0") (d (list (d (n "videostream-sys") (r "^0.4.0") (d #t) (k 0)))) (h "08mkz431j6hqnw9d4ggrw6272qglvx4lwb9qr9vrpwfav3aw88wj")))

(define-public crate-videostream-0.4.1 (c (n "videostream") (v "0.4.1") (d (list (d (n "videostream-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0whwsmg8zydhrpikkbrykk86n6vrahx6ldbsrr8kdwpc0djhx387")))

(define-public crate-videostream-0.5.0 (c (n "videostream") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.5.0") (d #t) (k 0)))) (h "13aq9i0dc16m8zjlbdhhwqvdspwb5xa433rymzg65hcxg0phy5j9")))

(define-public crate-videostream-0.5.1 (c (n "videostream") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.5.1") (d #t) (k 0)))) (h "0vkynwg633g95nisnvld3dda57vww3w1ki1h4bw7cswxyrvmd9j6")))

(define-public crate-videostream-0.5.2 (c (n "videostream") (v "0.5.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1rb1n7z50q8wpcbja3jf9bc61040fkm74zvfp4nx2n586kpx3zib")))

(define-public crate-videostream-0.4.2 (c (n "videostream") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.4.2") (d #t) (k 0)))) (h "0xvsgrah2yaw1lagfqc68gqq4phcl19nkkpqjyi679y6xvam85m5")))

(define-public crate-videostream-0.4.3 (c (n "videostream") (v "0.4.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.4.3") (d #t) (k 0)))) (h "1bc0rgd1h5zx513c2k6wcp58ky8wbfdf3z4ix4dgzkcvmq2snz7j")))

(define-public crate-videostream-0.6.0 (c (n "videostream") (v "0.6.0") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1r1dlay8546jmh76x7vqivmnhf8h2sdzjhvxz18spcgp681ryfnd")))

(define-public crate-videostream-0.6.1 (c (n "videostream") (v "0.6.1") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.6.1") (d #t) (k 0)))) (h "13ym47jxvkxvkbnlrf66qxpris7a6n91lkjwczimj007gw40p04p")))

(define-public crate-videostream-0.6.2 (c (n "videostream") (v "0.6.2") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.6.2") (d #t) (k 0)))) (h "1wa2crc2gnll5137124g12vfr4lxssg4rynf50dl6iaba6xz9jlf")))

(define-public crate-videostream-0.6.3 (c (n "videostream") (v "0.6.3") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "videostream-sys") (r "^0.6.3") (d #t) (k 0)))) (h "084zb9jvpybv55s8j30lwafr485x055b6qw78801rrbywsx5ai2m")))

(define-public crate-videostream-0.6.4 (c (n "videostream") (v "0.6.4") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "unix-ts") (r "^0.6.0") (d #t) (k 0)) (d (n "videostream-sys") (r "^0.6.4") (d #t) (k 0)))) (h "1n997kdrjp7xhyrsg0anqbkpq1jzwnp7zhgykzp680bk37682ms9")))

(define-public crate-videostream-0.6.5 (c (n "videostream") (v "0.6.5") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "unix-ts") (r "^0.6.0") (d #t) (k 0)) (d (n "videostream-sys") (r "^0.6.5") (d #t) (k 0)))) (h "01c74iixsay0cwxmpywy921wmyj1paagnjf3gpd0v3lr4znqah9r")))

(define-public crate-videostream-0.7.0 (c (n "videostream") (v "0.7.0") (d (list (d (n "dma-buf") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "unix-ts") (r "^0.6.0") (d #t) (k 0)) (d (n "videostream-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1xcb371f450n4sarbyhnx6adyn0rjsiqs46k88kmp3xh77lkydn2")))

(define-public crate-videostream-0.7.1 (c (n "videostream") (v "0.7.1") (d (list (d (n "dma-buf") (r "^0.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "unix-ts") (r "^0.6.0") (d #t) (k 0)) (d (n "videostream-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0j1rrv36aigxmwajxy3qqcvhwih49rdqhdc0kyvahy5sx41lr1fw")))

(define-public crate-videostream-0.8.0 (c (n "videostream") (v "0.8.0") (d (list (d (n "dma-buf") (r "^0.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "unix-ts") (r "^1.0.0") (d #t) (k 0)) (d (n "videostream-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1vash5mcys1004mznb04w8aqmkw9m5j52dvsfvp04v6b23kdx27b")))

