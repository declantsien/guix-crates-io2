(define-module (crates-io vi de video_ludo) #:use-module (crates-io))

(define-public crate-video_ludo-1.0.0 (c (n "video_ludo") (v "1.0.0") (d (list (d (n "ffmpeg-sys") (r "^3.4.1") (d #t) (k 0)) (d (n "glium") (r "^0.25.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.1") (d #t) (k 0)))) (h "07rbrbbdjyagnibrpnj6sn03bb59dgwjwhp42na52wazlg37w0ab")))

