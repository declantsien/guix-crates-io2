(define-module (crates-io vi de video-rs-adder-dep) #:use-module (crates-io))

(define-public crate-video-rs-adder-dep-0.4.1 (c (n "video-rs-adder-dep") (v "0.4.1") (d (list (d (n "ffmpeg-next") (r "^6.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "06bnb8nwksajzp2n2vjn98sglbhr9i0hisqk1m2vc06nqbvi5xzr")))

