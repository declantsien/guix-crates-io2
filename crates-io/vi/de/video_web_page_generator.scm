(define-module (crates-io vi de video_web_page_generator) #:use-module (crates-io))

(define-public crate-video_web_page_generator-0.1.0 (c (n "video_web_page_generator") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kqx5sckzy62qwpbia7dmcz6j4fbp7y70g6xdw007fy2bgxnm5il")))

(define-public crate-video_web_page_generator-0.1.1 (c (n "video_web_page_generator") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rpmcxgx6k0gd09lqlc7hpiw533k30rf63p1jrjz0zrim6dvawr2")))

(define-public crate-video_web_page_generator-0.1.2 (c (n "video_web_page_generator") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "11zsbp26fdjbqx23lrhsixxfp5bqvrvahhwwj3rmm57vw7zvwld4")))

(define-public crate-video_web_page_generator-0.2.0 (c (n "video_web_page_generator") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "15pw2qrmpixcdgz81gd13hykynklin3xgrfnv0k0fs4l5s0qkhmk")))

(define-public crate-video_web_page_generator-0.2.1 (c (n "video_web_page_generator") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cgpa9jm0ghr2rwpcys2lilpv6a0x4xgyw36ki7sc04krj973v8v")))

