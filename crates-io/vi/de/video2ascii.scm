(define-module (crates-io vi de video2ascii) #:use-module (crates-io))

(define-public crate-video2ascii-0.1.0 (c (n "video2ascii") (v "0.1.0") (d (list (d (n "ansi_rgb") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "^0.63.0") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0kpv9m356azjcwrq0ify7qcckv77d4qmrln1cipwqfiqf1kfg89n")))

(define-public crate-video2ascii-0.1.1 (c (n "video2ascii") (v "0.1.1") (d (list (d (n "ansi_rgb") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "opencv") (r "^0.63.0") (f (quote ("rgb"))) (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1r76zi1f3m2lgvwq18chd5qsphapkrvw6pj9akv56dic3ycjzkg5")))

