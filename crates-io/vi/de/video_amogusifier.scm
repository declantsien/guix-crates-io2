(define-module (crates-io vi de video_amogusifier) #:use-module (crates-io))

(define-public crate-video_amogusifier-1.0.0 (c (n "video_amogusifier") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filehash-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1i088a02g8qln1hfs4b5grklh01j9schdw3jccd823sz906bjfcj")))

(define-public crate-video_amogusifier-1.0.1 (c (n "video_amogusifier") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filehash-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1714km3v0wjmaspgcgry7z9k37346n0lalmbd11ymzdji3a0lhr3")))

(define-public crate-video_amogusifier-1.0.2 (c (n "video_amogusifier") (v "1.0.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filehash-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08y3sl1425qlfjc7ysx4ng9mzqn59c3dv090p8b4hn2x7x2sydvh")))

