(define-module (crates-io vi de video-rs) #:use-module (crates-io))

(define-public crate-video-rs-0.1.0 (c (n "video-rs") (v "0.1.0") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1ww7s5wy9y9lqc993cb3xxa205yrq5filxlyb4ild76mpw95013a")))

(define-public crate-video-rs-0.1.1 (c (n "video-rs") (v "0.1.1") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0a8rnkvsnkf692d70333ppqn7v25c4yl16b9nsgckwd23jp68kwi")))

(define-public crate-video-rs-0.1.2 (c (n "video-rs") (v "0.1.2") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "19z20b4nl4j1z5yhpkrfsdvjzvp37yda59nvk0w0b2my2aylzsfj")))

(define-public crate-video-rs-0.1.3 (c (n "video-rs") (v "0.1.3") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1xkm4ah4dyja9ghv2ssfrcyyq1jpy9k7jc023wxklzxz5djh6zqy")))

(define-public crate-video-rs-0.1.4 (c (n "video-rs") (v "0.1.4") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1f46crxns678m8vcfv7n9g2978xz8vm02xzjbaka5flig43cmy4a")))

(define-public crate-video-rs-0.1.5 (c (n "video-rs") (v "0.1.5") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1a33y8czgg4r84wy5zvjawggg1dqbdpzczzz33hcc0602knqzp3l")))

(define-public crate-video-rs-0.1.6 (c (n "video-rs") (v "0.1.6") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1rj0x8k6f6xrw94qinjj8splqzzljhkv66kbj0y9s0fq2nqcyw7d")))

(define-public crate-video-rs-0.1.7 (c (n "video-rs") (v "0.1.7") (d (list (d (n "ffmpeg-next") (r "^4.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1ry4g1byp8lkn3gkbnnqjsa9n8az2bz1yvgx0cn5z6zc7vrx14w5")))

(define-public crate-video-rs-0.1.8 (c (n "video-rs") (v "0.1.8") (d (list (d (n "ffmpeg-next") (r "^5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "06gbkl3lyv0ns0mydcp4n7hgdc3x5rkng56frgl5cq8i8j0zc0d4")))

(define-public crate-video-rs-0.2.0 (c (n "video-rs") (v "0.2.0") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "14wx7hnqidfsi6kv6bzh8i0bcz85j4m3gi31kqs6wis2jlff2xpk")))

(define-public crate-video-rs-0.2.1 (c (n "video-rs") (v "0.2.1") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0zhqgx5l3ya40c8dziv88f8ib20n121qiw7wbignhn4g6j61iq1a")))

(define-public crate-video-rs-0.2.2 (c (n "video-rs") (v "0.2.2") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "00r3h7jbly9m9an4iawihyc1cigl0fghsv951ha41dph8xxyq9vy")))

(define-public crate-video-rs-0.2.3 (c (n "video-rs") (v "0.2.3") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0vf5k76940pq2v7g6s5rfs6pm8jf0pggm6mlcn731m4snl4cfn84")))

(define-public crate-video-rs-0.2.4 (c (n "video-rs") (v "0.2.4") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1yvx6z9vxra33zd6nhgj5x3qry9g4xv5sn5r867k2wj8lr737skg")))

(define-public crate-video-rs-0.2.5 (c (n "video-rs") (v "0.2.5") (d (list (d (n "ffmpeg-next") (r "^5.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0agbpp9ck9mclyhwpm8yyklmmrjncm9h3c0ljccf4n8fl090x6q1")))

(define-public crate-video-rs-0.3.0 (c (n "video-rs") (v "0.3.0") (d (list (d (n "ffmpeg-next") (r "^6.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0r3j247s4a0pwkphzhzkmcbv2hbfif0awfql0dmkrzi0casiak7p")))

(define-public crate-video-rs-0.4.0 (c (n "video-rs") (v "0.4.0") (d (list (d (n "ffmpeg-next") (r "^6.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1yg9a60rhlgxyqil0pywxwlnz2ghl2b1r606b28ri6dys5c0pagi")))

(define-public crate-video-rs-0.4.1 (c (n "video-rs") (v "0.4.1") (d (list (d (n "ffmpeg-next") (r "^6.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0d8as8a4qr09ys4647rndshmhwbpr8skvbpyq461qcamjwqaamwg")))

(define-public crate-video-rs-0.5.0 (c (n "video-rs") (v "0.5.0") (d (list (d (n "ffmpeg-next") (r "^6.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0p9anp54zc62891si2sxrk8ixgdqagx18shqvg4mgg20maldhkmk")))

(define-public crate-video-rs-0.6.0 (c (n "video-rs") (v "0.6.0") (d (list (d (n "ffmpeg-next") (r "^6.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1w5giym0kfv49n3d0r5w9sdxr3rpwlshj25dfs88v6sldwxxvlyq")))

(define-public crate-video-rs-0.6.1 (c (n "video-rs") (v "0.6.1") (d (list (d (n "ffmpeg-next") (r "^6.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0cv1bifx6vfpgl1zrzb44k56varmn8xbw1y4j0l148mbas0x0gi4")))

(define-public crate-video-rs-0.7.0 (c (n "video-rs") (v "0.7.0") (d (list (d (n "ffmpeg-next") (r "^6.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0b2qb5cszwd93rf4l5ixxwhj0xi05ik4cq57wb93yy543zk4vp8y")))

(define-public crate-video-rs-0.7.1 (c (n "video-rs") (v "0.7.1") (d (list (d (n "ffmpeg-next") (r "^6.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0gb00qx45c8977d84y9f8d6jfl8gkq0sbknhl58amhv2wgm30ibm")))

(define-public crate-video-rs-0.7.2 (c (n "video-rs") (v "0.7.2") (d (list (d (n "ffmpeg-next") (r "^6.1") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1vfd8y3p6rdrr5yylik0xm4ppm6i4dqvvpfkkfyk7wygnwqwymgk")))

(define-public crate-video-rs-0.7.3 (c (n "video-rs") (v "0.7.3") (d (list (d (n "ffmpeg-next") (r "^7.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1jw7wycq4rvwsqa8vbri1kmshfn3girxpljv19asafzxal51absy")))

(define-public crate-video-rs-0.7.4 (c (n "video-rs") (v "0.7.4") (d (list (d (n "ffmpeg-next") (r "^7.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1djiq3hpldpik2f9y1a9sh86gr1m9ybpyv0vaxycfdqywzsq5qqf")))

(define-public crate-video-rs-0.8.0 (c (n "video-rs") (v "0.8.0") (d (list (d (n "ffmpeg-next") (r "^7.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "136nfbqn6zzvn7cyjp9icds602q8zzysczc802pw4w3ccbl8hzy4")))

(define-public crate-video-rs-0.8.1 (c (n "video-rs") (v "0.8.1") (d (list (d (n "ffmpeg-next") (r "^7.0") (f (quote ("format" "codec" "software-resampling" "software-scaling"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "04g4n3ydyi42ccwzzkp9rwmyim0z64c9008dgqhvx3x10r9ibn55")))

