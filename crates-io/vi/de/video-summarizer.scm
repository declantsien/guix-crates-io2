(define-module (crates-io vi de video-summarizer) #:use-module (crates-io))

(define-public crate-video-summarizer-1.2.1 (c (n "video-summarizer") (v "1.2.1") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0rqwknw0vwxi8259vswyh61fvhiw78mixxdyd0vfpgagwj87gbja")))

(define-public crate-video-summarizer-1.2.2 (c (n "video-summarizer") (v "1.2.2") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "196cz78fcwyig6i6bgvqqx4xnmiszdqjdy6xv7r5lm4lsp1rl5cs")))

(define-public crate-video-summarizer-1.2.3 (c (n "video-summarizer") (v "1.2.3") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "minimp3") (r "^0.3.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "03b8yiddvykb7ygi26jh4jjgvyqa8ipsx937rbmyq3zigplssrwg")))

