(define-module (crates-io vi de video) #:use-module (crates-io))

(define-public crate-video-0.0.2 (c (n "video") (v "0.0.2") (h "152v93ja3cfkyairldzj3psidwkwz7dkaj5h2wc7rsngliarv1my")))

(define-public crate-video-0.1.0 (c (n "video") (v "0.1.0") (h "14plmg8c0dvcihm2skishhzcqb7srqgg4iq84a7xvdmcl0vni74l")))

(define-public crate-video-0.1.1 (c (n "video") (v "0.1.1") (h "06j4w6cfcdvlazadgl99dc40spjmix4ldvbrkk74pb73h7x6siyl")))

