(define-module (crates-io vi de video-metadata) #:use-module (crates-io))

(define-public crate-video-metadata-0.1.0 (c (n "video-metadata") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1saq7z510iz16lbpg231hngddndz5j4xp2r8mbrh2y8k7cv1vr6n")))

(define-public crate-video-metadata-0.1.1 (c (n "video-metadata") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ckysnmmlyhghb6a97ghz1slj3p4wi1nnfmib2bpbf6afpljimvh")))

(define-public crate-video-metadata-0.1.2 (c (n "video-metadata") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1csgb7645q1lzq5rwflqxgqzpfczi68pncp5kgn11cc92hcfx4q8")))

