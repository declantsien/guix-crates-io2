(define-module (crates-io vi de videostream-sys) #:use-module (crates-io))

(define-public crate-videostream-sys-0.1.0 (c (n "videostream-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b2ckf1nlkvzpcypgbbkki9qgzp86h7li2ih98n63fi2mkp5y0vh")))

(define-public crate-videostream-sys-0.2.0 (c (n "videostream-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bsi0chpr26jxlv9l683f9ym9ssg8vmv24wmp6dw44cr9sppjhrs")))

(define-public crate-videostream-sys-0.3.4 (c (n "videostream-sys") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00yx4dcphwrj000jd73mn7zsh46i5i4c6pbndp9m91a0wv1fbdi9")))

(define-public crate-videostream-sys-0.4.0 (c (n "videostream-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b7lyr84wn5127dig3h77h0f0c2v1lg6jhz8gj8pmj9976pl2ldd")))

(define-public crate-videostream-sys-0.4.1 (c (n "videostream-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07mjfsqkv4l9k0yn4nbn8mqnzpbnyf5wny21h1igppkvmhbvv40g")))

(define-public crate-videostream-sys-0.5.0 (c (n "videostream-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rin4kh8xpjxma1pjkj54xjbj5cfaz7frwcmm5fg9mdq11p49jri")))

(define-public crate-videostream-sys-0.5.1 (c (n "videostream-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p7fndz8h97iri00jml7pp28c28i1ycfrs89cgm4qvn0gvhxbmw2")))

(define-public crate-videostream-sys-0.5.2 (c (n "videostream-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lnrsj7cn52pfix712spxpx2qalgrjw7jbixrhq6jx4037drw2ib")))

(define-public crate-videostream-sys-0.4.2 (c (n "videostream-sys") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k4cxr4s2wxngycma4rkir1abjpixqnvxk0k5hd239mwnnxknvab")))

(define-public crate-videostream-sys-0.4.3 (c (n "videostream-sys") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08waq3w4pmyamrzz7lbr8mivkh24dzrl84dvdn5h6nmfa9xjazrg")))

(define-public crate-videostream-sys-0.6.0 (c (n "videostream-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yd8is2k4k72vjx4sax0lv7r583fd9vm0wpsbfgk1rx33231lyd6")))

(define-public crate-videostream-sys-0.6.1 (c (n "videostream-sys") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cf8s8d40x5ic0y7lyiwpxyjvvikpw3yyy41fksxdn3y58qjyy5z")))

(define-public crate-videostream-sys-0.6.2 (c (n "videostream-sys") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p6hldj6sj0k2916w2hrzi88pxz749r7zj7f1cb64d3qdb0b2xlx")))

(define-public crate-videostream-sys-0.6.3 (c (n "videostream-sys") (v "0.6.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05n02rsqs4fs271jwcwwy17vsfclncr3hwaqz39y0nqqwcjxlsjc")))

(define-public crate-videostream-sys-0.6.4 (c (n "videostream-sys") (v "0.6.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qcd1sp27i6qdbvskvs8xa570ivw9dhalpv78lj1r4xi2jy4876b")))

(define-public crate-videostream-sys-0.6.5 (c (n "videostream-sys") (v "0.6.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11kd6lqny16x01imcyabd0bskdl0skp37vq7709x69pvjg929qy8")))

(define-public crate-videostream-sys-0.7.0 (c (n "videostream-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r46wp7b06f67m8lai0k585a7jpn4i1ll1ri58jb6b5w6bg2gmxc")))

(define-public crate-videostream-sys-0.7.1 (c (n "videostream-sys") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lny3xpn2msm4k0z9m7ngff0i6450qxj8qkill7vqwa53fb5zahv")))

(define-public crate-videostream-sys-0.8.0 (c (n "videostream-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "169gjrdpsfaym0a4ln0jb3x6mvqq6yclmn32c34h8xni044mxvw7")))

