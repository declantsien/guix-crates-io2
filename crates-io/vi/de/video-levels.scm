(define-module (crates-io vi de video-levels) #:use-module (crates-io))

(define-public crate-video-levels-0.1.0 (c (n "video-levels") (v "0.1.0") (h "15rkmdsjipdifvn7dbw8vswsrnrbf9fc0hjwdcfxm9vmi8255cn9")))

(define-public crate-video-levels-0.1.1 (c (n "video-levels") (v "0.1.1") (h "0k7f7pawd5lg6grfc7y10sy0897wq0k2ld53w44ndbh2x8n3crr3")))

(define-public crate-video-levels-1.0.0 (c (n "video-levels") (v "1.0.0") (d (list (d (n "yuv") (r "^0.1.5") (d #t) (k 0)))) (h "0cn5cj5bg0a50sx792pyy3xj0s17363la43bmw78985a9wz01b6v")))

