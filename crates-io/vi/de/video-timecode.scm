(define-module (crates-io vi de video-timecode) #:use-module (crates-io))

(define-public crate-video-timecode-0.1.0 (c (n "video-timecode") (v "0.1.0") (h "14h7alns2mhhmyzizim6aa8f9i7yqva8523cmvjk82vfshkjn40b")))

(define-public crate-video-timecode-0.2.0 (c (n "video-timecode") (v "0.2.0") (h "04xcrj08lmbj1ib0fzixnrj0h4kxswy6q7wr8xb9sidad3jdv0b8")))

(define-public crate-video-timecode-0.2.1 (c (n "video-timecode") (v "0.2.1") (h "1p0g90gm4clhry42kqppz2hd3ka8s6v0r27qm58rlc37a5g8nrzr")))

(define-public crate-video-timecode-0.2.2 (c (n "video-timecode") (v "0.2.2") (h "1cmg3z5y0mnjdy0bvkd9a80p81lx6nz1njn8cnc9wlyrnlwi2y9v")))

(define-public crate-video-timecode-0.3.0 (c (n "video-timecode") (v "0.3.0") (h "0f1754jb1randlkhmgq34vvkhb1x3qccqcyxjzxzf75h32hb83kz")))

(define-public crate-video-timecode-0.3.1 (c (n "video-timecode") (v "0.3.1") (h "051w5bsmbxsmk95fh7xpnzwzjfng5mx1m8c23k2xhv6nn8xhjakf")))

(define-public crate-video-timecode-0.3.2 (c (n "video-timecode") (v "0.3.2") (h "1xlva0aba82r36cgfg3km69hzidd02bmck9427bbd8q8jkr2a8mz")))

(define-public crate-video-timecode-0.4.0 (c (n "video-timecode") (v "0.4.0") (h "10r0y3dhg5zk46zjsdn94ib2x2y03r49zsj3qylln8w8np3mw3ib")))

(define-public crate-video-timecode-0.5.0 (c (n "video-timecode") (v "0.5.0") (h "12ha2mjg8rvsq1w5lacc8czk3nq6dv7idcrcm65yyfqsscnfj9y1")))

(define-public crate-video-timecode-0.5.1 (c (n "video-timecode") (v "0.5.1") (h "0wf9y6psggxdickvmfj1n8s40rpjbwr5zpcnhadj7mrsvij0ksyf")))

(define-public crate-video-timecode-0.5.2 (c (n "video-timecode") (v "0.5.2") (h "1cm32k8vdksxzl9k5g8n23vc4jmiwf5bksqi4vq90d3pxawinl70")))

(define-public crate-video-timecode-0.5.3 (c (n "video-timecode") (v "0.5.3") (h "03m1sx1nqczap2a5dkr5admahg29lvy1lqlr8p3fbb1l21xcbjq8")))

(define-public crate-video-timecode-0.6.0 (c (n "video-timecode") (v "0.6.0") (h "0qcq9sly8s8jg8hh15pc2kh2f3db3fmgzaghs4bb1ja3nam50l1z")))

(define-public crate-video-timecode-0.6.1 (c (n "video-timecode") (v "0.6.1") (h "0igz36pp1p1k7790i2qlh1ampb664girrw3mbihf0dhxf4yr8lgf")))

