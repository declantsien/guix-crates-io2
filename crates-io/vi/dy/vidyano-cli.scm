(define-module (crates-io vi dy vidyano-cli) #:use-module (crates-io))

(define-public crate-vidyano-cli-0.3.0 (c (n "vidyano-cli") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.42") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.42") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.7") (d #t) (k 0)))) (h "172j5r5109rcd1995hafqqlxm4izwdwzv6jccnbfair9bpldl46y") (y #t)))

