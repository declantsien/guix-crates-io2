(define-module (crates-io vi vo vivox_rs_sys) #:use-module (crates-io))

(define-public crate-vivox_rs_sys-0.0.1 (c (n "vivox_rs_sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "1jhdzpjv922w87armcnsji9nr8qf4c55q4k0yriphm9v3lmd8rlr") (y #t)))

(define-public crate-vivox_rs_sys-0.0.2 (c (n "vivox_rs_sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "0z5sww857v67n8hnq28ydmrpg00kf890yf4z0p52rkqf2dj7bb9f")))

(define-public crate-vivox_rs_sys-5.9.0 (c (n "vivox_rs_sys") (v "5.9.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "0w3jx3sa91i02nyar6jdqa5izy5y2ldqw6vd0rbx8vnx3dngrn2m")))

