(define-module (crates-io vi as viaspf-record) #:use-module (crates-io))

(define-public crate-viaspf-record-0.4.0-alpha.3 (c (n "viaspf-record") (v "0.4.0-alpha.3") (h "1xndq1wydcv0pkh7vg568q5a51qlrn075fclgrgdfpz9w1wbv4qq") (r "1.56.1")))

(define-public crate-viaspf-record-0.4.0-alpha.4 (c (n "viaspf-record") (v "0.4.0-alpha.4") (h "142cbm00gc62l04brm6527d7ix5949w9c4paldbyqhxcgs110vm6") (r "1.56.1")))

(define-public crate-viaspf-record-0.4.0 (c (n "viaspf-record") (v "0.4.0") (h "124k0w3mi82h2h2nxxg2zbpggs7321yc32vsq52vwigj19cp17xs") (r "1.56.1")))

(define-public crate-viaspf-record-0.4.1 (c (n "viaspf-record") (v "0.4.1") (h "0b90x3n26qp5rrxph2div1x5x29flx9rffybg09glkf6awha89ls") (r "1.56.1")))

(define-public crate-viaspf-record-0.5.0-alpha.1 (c (n "viaspf-record") (v "0.5.0-alpha.1") (h "1zq555hfp4md4rp6vm0bq99xvqiiapil3z2445nk91r3fz6r3r6l") (r "1.65.0")))

(define-public crate-viaspf-record-0.5.0 (c (n "viaspf-record") (v "0.5.0") (h "0fagkjsbglny1w22mwq7gplb42dylhxwv6j5fgdzar84qp7yfjb8") (r "1.65.0")))

(define-public crate-viaspf-record-0.5.1 (c (n "viaspf-record") (v "0.5.1") (h "03059y06qbxa4lrqgw69xq36xvcd2djqlkzkahnhhipcqxchangf") (r "1.65.0")))

