(define-module (crates-io vi m- vim-foldtree) #:use-module (crates-io))

(define-public crate-vim-foldtree-0.1.0 (c (n "vim-foldtree") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)))) (h "1ia7isz2jzzzapnzypb0kngafagajsln3j64pawiysaxjrbbd9mz")))

