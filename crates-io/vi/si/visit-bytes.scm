(define-module (crates-io vi si visit-bytes) #:use-module (crates-io))

(define-public crate-visit-bytes-0.1.0 (c (n "visit-bytes") (v "0.1.0") (d (list (d (n "camino_") (r "^1.0") (o #t) (d #t) (k 0) (p "camino")))) (h "16am8mqym6vzcn78lcbd9pzqanb3ijb86ajlgxlmkqarpxfvdd4g") (f (quote (("default" "alloc") ("camino" "alloc" "camino_") ("alloc"))))))

