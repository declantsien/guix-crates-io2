(define-module (crates-io vi si visim) #:use-module (crates-io))

(define-public crate-visim-0.1.0 (c (n "visim") (v "0.1.0") (d (list (d (n "gl_dstruct") (r "^0.2.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "12kqjyf8wfz7zxfxkpzhwy8yqd6xvfwjn3fqzgvrh10cwjdkrqi7") (f (quote (("gl_debug" "gl_dstruct/debug"))))))

