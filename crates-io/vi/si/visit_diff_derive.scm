(define-module (crates-io vi si visit_diff_derive) #:use-module (crates-io))

(define-public crate-visit_diff_derive-0.1.0 (c (n "visit_diff_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0ahbfnh2h4vmq1ba6759jjdk3f6ma62qbdwnqlwcs8lbpgkx3jmk")))

