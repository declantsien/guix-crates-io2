(define-module (crates-io vi si visibility) #:use-module (crates-io))

(define-public crate-visibility-0.0.0 (c (n "visibility") (v "0.0.0") (h "0qzqfpkxmg7sbp5aa2dg622534hlnlzghcp3m0gajl53m8qkl9h3")))

(define-public crate-visibility-0.0.1 (c (n "visibility") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lch0vxlqvqdq8nnql1pz792190pa4gghnnyy6r3skp31b6db0c8") (f (quote (("nightly"))))))

(define-public crate-visibility-0.1.0-rc1 (c (n "visibility") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ilcpk5qnj680iqz72ka3zd7p4qvay48vncqa9krl0m3mj21py1i") (f (quote (("nightly"))))))

(define-public crate-visibility-0.1.0 (c (n "visibility") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y70n5dpvj4vmf7jw5ab7livqhhg28gkxn2rivr7q8mrkncrizdk") (f (quote (("nightly"))))))

