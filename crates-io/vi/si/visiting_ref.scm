(define-module (crates-io vi si visiting_ref) #:use-module (crates-io))

(define-public crate-visiting_ref-0.1.0 (c (n "visiting_ref") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded"))) (d #t) (k 2)))) (h "1zazp9jwp8hh8l6n2dz8lzmbyh060bglr57gr46srxhgm47aic25")))

(define-public crate-visiting_ref-0.2.0 (c (n "visiting_ref") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "04hl3gs9v8andc2mvbl93vnz8zh87hmq9dkrf3yzyiv4h7n37p0z")))

