(define-module (crates-io vi si visita) #:use-module (crates-io))

(define-public crate-visita-0.1.0 (c (n "visita") (v "0.1.0") (d (list (d (n "visita_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0m7q6k694pzzxab9n9h3sw31d3xcgvm0jrzjbqgyd224ql19njzz") (y #t)))

(define-public crate-visita-0.1.1 (c (n "visita") (v "0.1.1") (d (list (d (n "visita_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1v38cvj8b00prsrzahqsk33c1rj8s7jpj09k9cr8236v0myg9cid")))

(define-public crate-visita-0.2.0 (c (n "visita") (v "0.2.0") (d (list (d (n "visita_macros") (r "^0.2.0") (d #t) (k 0)))) (h "08zv3ai8mdfl6h63br9ywykwsqvqhk946b2ldpfb20rz0phf55kn")))

(define-public crate-visita-0.2.1 (c (n "visita") (v "0.2.1") (d (list (d (n "visita_macros") (r "^0.2.0") (d #t) (k 0)))) (h "06g6ssxrbqlflf8kj98znyfcr7769y566394zh5syi7z1x47j6z0")))

