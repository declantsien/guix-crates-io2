(define-module (crates-io vi si visioncortex) #:use-module (crates-io))

(define-public crate-visioncortex-0.1.0 (c (n "visioncortex") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n5aihawg1d8d8j7gg9k5pbzglihl8mz6fgbsx9ss24s6rqmc858")))

(define-public crate-visioncortex-0.2.0 (c (n "visioncortex") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ddvsjsi3f0c4yvk8szgc9zydgalmqh2c7rr8fvs6224gi503qsf")))

(define-public crate-visioncortex-0.3.0 (c (n "visioncortex") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1q02z080491zkjxx4snm496cyajkp50bwvk4cxfdhjglg2svc9ng")))

(define-public crate-visioncortex-0.4.0 (c (n "visioncortex") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ki7l9qhidrnlavzgb5bnzqa2gabya9aw0f0gl43s3768jikm95y")))

(define-public crate-visioncortex-0.5.0 (c (n "visioncortex") (v "0.5.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1iimqkydqv6wvjbim6cpi8kxij7px1ad4wxr65cdnldx9aiysmj3")))

(define-public crate-visioncortex-0.6.0 (c (n "visioncortex") (v "0.6.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "197jiljv98g704f2l1x5dhfk21annqda2wah3z6n8vffk10pgph6")))

(define-public crate-visioncortex-0.6.1 (c (n "visioncortex") (v "0.6.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18xcx7bxclccv2ar73i4w14z0pvda1l92ajmgz17ynz5l1yiq4ik")))

(define-public crate-visioncortex-0.7.0 (c (n "visioncortex") (v "0.7.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "059v5172qrvydbjn43hgxn7sdknxhhz1k4lvc30cc96rbg1zw4qm")))

(define-public crate-visioncortex-0.8.0 (c (n "visioncortex") (v "0.8.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "048j71j706vzpksfz9wqd7g7ammldp2rc02f324x7cn3d45f5f42")))

(define-public crate-visioncortex-0.8.1 (c (n "visioncortex") (v "0.8.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mc7yj1si5644vppzqs1sbskv6nw6gjxrbax7s7wm19g9vq9azyq")))

(define-public crate-visioncortex-0.8.2 (c (n "visioncortex") (v "0.8.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18y6hbkq6c2cizzsqv3w1sam5g5cv132xjsf0r9az3pn6nphqqvj")))

(define-public crate-visioncortex-0.8.3 (c (n "visioncortex") (v "0.8.3") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0bzvlky1q4g2mwgk9mlb4rrgzr2psa2n7nz6x3s3i0irhx2rwb3v")))

(define-public crate-visioncortex-0.8.4 (c (n "visioncortex") (v "0.8.4") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rmkhgvh8gbpk4rs5l1jp1zlk5cvzjapi2pxlzlrs4shaaky6qwm")))

(define-public crate-visioncortex-0.8.5 (c (n "visioncortex") (v "0.8.5") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0w91nfwjl090hzparxj83dgv2221vpnfv9l8vmhpkilcckw1c9gv")))

(define-public crate-visioncortex-0.8.6 (c (n "visioncortex") (v "0.8.6") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0snj3s43988bz9ajzkw4plj014r97y59mjyivgik3fscb64p1rly")))

(define-public crate-visioncortex-0.8.8 (c (n "visioncortex") (v "0.8.8") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zwzvyczvwq720h6rvsqilqw9fdvz7kqvhdd48ilpmw1q97ibgjd")))

