(define-module (crates-io vi si vision) #:use-module (crates-io))

(define-public crate-vision-0.0.1 (c (n "vision") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "hyper") (r "^0.11.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)))) (h "1z3dlm73pp60skd63gh6dffsd2x4i2ypkl21xlfi8ndw9nqbc4rd")))

(define-public crate-vision-0.0.2 (c (n "vision") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "hyper") (r "^0.11.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.12") (d #t) (k 0)))) (h "0krz3n6psfyfkb0p7isnqwcmp7wgswlfhqcyfw2fjfj9dfb3brff")))

