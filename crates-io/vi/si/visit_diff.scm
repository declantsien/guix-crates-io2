(define-module (crates-io vi si visit_diff) #:use-module (crates-io))

(define-public crate-visit_diff-0.1.0 (c (n "visit_diff") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (k 0)) (d (n "visit_diff_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "0aqj7xn5l5m1pjs28r35y375fay1bkas3634axxyg6r0xqwj96pv") (f (quote (("std") ("default" "visit_diff_derive" "std"))))))

(define-public crate-visit_diff-0.1.1 (c (n "visit_diff") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (k 0)) (d (n "visit_diff_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "1idblwsnlxsgznsbc9cxkc58bxi4c0xmpclz6ds85nbzwkzbg0dw") (f (quote (("std") ("default" "visit_diff_derive" "std"))))))

