(define-module (crates-io vi si visionmagic) #:use-module (crates-io))

(define-public crate-visionmagic-0.1.0 (c (n "visionmagic") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "0cwdzc87wc2j5l1yilicwv0w0yv7l7rn9gz86a559k2ryn6pj9c1")))

(define-public crate-visionmagic-0.1.1 (c (n "visionmagic") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "03jp4lqhif9hsb3cl5ax3jw6vxrsjr4kpb4cmpavfdz7z0lcxwi7")))

(define-public crate-visionmagic-0.1.2 (c (n "visionmagic") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "0ig10zjz0hll9nb57p1k1mvycg9j7xsmbl24a3v604wxy96j5s45")))

(define-public crate-visionmagic-0.2.0 (c (n "visionmagic") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "visioncortex") (r "^0.4.0") (d #t) (k 0)))) (h "1r5j96h83jrvg4h605jz2b3h2h3iqy31j5k2x5qq3vkyfbwn4njf")))

