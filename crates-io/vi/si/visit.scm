(define-module (crates-io vi si visit) #:use-module (crates-io))

(define-public crate-visit-0.1.0 (c (n "visit") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "simple") (r "^0.1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xpflaibyak5haq3hhzvsf4n8gzfxjwg0zfrx2q3hnb3qy0ljg6x")))

