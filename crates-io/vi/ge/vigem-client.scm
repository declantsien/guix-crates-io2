(define-module (crates-io vi ge vigem-client) #:use-module (crates-io))

(define-public crate-vigem-client-0.1.0 (c (n "vigem-client") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (d #t) (k 0)))) (h "0k88wnd1psrk28827di415f9q9mddgawypd1j8790bxial1dn96n") (f (quote (("unstable"))))))

(define-public crate-vigem-client-0.1.1 (c (n "vigem-client") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (d #t) (k 0)))) (h "1qvd598bapxqxhvrhh8jd6g4b7svyd99kfbqd303dsy4isf38pln") (f (quote (("unstable"))))))

(define-public crate-vigem-client-0.1.2 (c (n "vigem-client") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (d #t) (k 0)))) (h "1m8jqa08d94lay01014jc62zpf4xhbvkv4vxm413n7cbx096k22x") (f (quote (("unstable"))))))

(define-public crate-vigem-client-0.1.3 (c (n "vigem-client") (v "0.1.3") (d (list (d (n "rusty-xinput") (r "^1.2.0") (d #t) (k 2)) (d (n "urandom") (r "^0.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (d #t) (k 0)))) (h "0nc6dk71g34azi74id3q57wpl7mpx34bh2c7zpn8gak2g0zpmgp6") (f (quote (("unstable_xtarget_notification") ("unstable_ds4"))))))

(define-public crate-vigem-client-0.1.4 (c (n "vigem-client") (v "0.1.4") (d (list (d (n "rusty-xinput") (r "^1.2.0") (d #t) (k 2)) (d (n "urandom") (r "^0.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (d #t) (k 0)))) (h "0ljdcckl8vkwppf8x4nidg2qxv77ibg3bc6zwjqiw7pykvwycmxq") (f (quote (("unstable_xtarget_notification") ("unstable_ds4"))))))

