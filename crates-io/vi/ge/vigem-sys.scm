(define-module (crates-io vi ge vigem-sys) #:use-module (crates-io))

(define-public crate-vigem-sys-0.1.0 (c (n "vigem-sys") (v "0.1.0") (h "1w0fy1a53yvll5hypmrn01f4hp39ba5973swjc1qdnfgbqbrdha8")))

(define-public crate-vigem-sys-0.2.0 (c (n "vigem-sys") (v "0.2.0") (h "1xhgfl93ywcrhqpqrnxl07pqml7c134nl7frvwhp64adgi0hp0jn")))

(define-public crate-vigem-sys-1.0.0 (c (n "vigem-sys") (v "1.0.0") (h "0x15kf9dnrxrj0rrl440fa6yh78iij6djc8mlv3qy9xm7gw3291i")))

(define-public crate-vigem-sys-1.1.0 (c (n "vigem-sys") (v "1.1.0") (h "1iva5fmps8szrsvavdkz5bl7wl0cvcs8wpryqfcxi2vx0grmgkkn")))

