(define-module (crates-io vi ge vigem) #:use-module (crates-io))

(define-public crate-vigem-0.1.0 (c (n "vigem") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0x7ikzv0l7fkjzibwvxdn0jpam78njlb73634vc1km4qx4vdsy7y")))

(define-public crate-vigem-0.2.0 (c (n "vigem") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0r0jj7lg3rwbcb5j8gf4vl4ln0imzklr1x07f1gbfg4c6q4cw400")))

(define-public crate-vigem-0.3.0 (c (n "vigem") (v "0.3.0") (h "0pv8pibd0czbn26r1x2vcsrljyglfk3a35frncjxxys1hjz6qhzp")))

(define-public crate-vigem-0.4.0 (c (n "vigem") (v "0.4.0") (h "11zb7yzn3gnxpsy82q58lqlfcc7q8ma6r3dkcbc0c14qlx80icdj")))

(define-public crate-vigem-0.5.0 (c (n "vigem") (v "0.5.0") (h "08hs972z1xpsvgyirq4y0js5ng6d5vvrx3zcxx2s0095br3fvzz3")))

(define-public crate-vigem-0.6.0 (c (n "vigem") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "vigem-sys") (r "^0.2") (d #t) (k 0)))) (h "08rdblrjyplcqg8fkqxkfzynz1fpprvhmb4fa67bfrm065hg5jbf")))

(define-public crate-vigem-0.7.0 (c (n "vigem") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "vigem-sys") (r "^1") (d #t) (k 0)))) (h "0fg1yvybh4hczgx403mwsdy76arbkm29lvanjkml3vxmn5hsayw8")))

(define-public crate-vigem-0.8.0 (c (n "vigem") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "vigem-sys") (r "^1") (d #t) (k 0)))) (h "1f0hzqxhn5xmaxanv8rsv6zl77za5g7lh5xhx8bg9ccr54qwpqbz")))

(define-public crate-vigem-0.9.0 (c (n "vigem") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "vigem-sys") (r "^1") (d #t) (k 0)))) (h "1n3x0bg59zjkq2yklz0gs5nz1zv7px9jwry3gcrjr63ixpznb4fc")))

(define-public crate-vigem-0.9.1 (c (n "vigem") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "vigem-sys") (r "^1.1") (d #t) (k 0)))) (h "0lpb6npyibfi3zzqn8gw2z6lrif51cxl62fp24ck8hap5jgxfy7j")))

