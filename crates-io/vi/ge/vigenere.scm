(define-module (crates-io vi ge vigenere) #:use-module (crates-io))

(define-public crate-vigenere-0.0.1 (c (n "vigenere") (v "0.0.1") (h "0yhgf32jyyi5y0ynxmgzwshx5xmsyd9c4jql8p3pf2v5csc9lvzm")))

(define-public crate-vigenere-0.0.2 (c (n "vigenere") (v "0.0.2") (h "1x6mzrwrxhlcwcg3iiivfs33bdfkdqbygmgvyrw9rf42q81jxf5k")))

(define-public crate-vigenere-0.0.3 (c (n "vigenere") (v "0.0.3") (h "0wnv5xcpmicwjwhiag4vzihbri3mgld777g358vrs8ipx5h99xw2")))

