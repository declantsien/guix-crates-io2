(define-module (crates-io vi zi vizio-openrtb) #:use-module (crates-io))

(define-public crate-vizio-openrtb-0.2.1 (c (n "vizio-openrtb") (v "0.2.1") (d (list (d (n "phf") (r "~0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_macros") (r "~0.8.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.10.6") (f (quote ("json" "blocking"))) (d #t) (k 2)))) (h "1iwca6w1khm7ajjsgpgbhyix8a3514jx82br8yir2y7l6hbnsq9i")))

(define-public crate-vizio-openrtb-0.2.2 (c (n "vizio-openrtb") (v "0.2.2") (d (list (d (n "phf") (r "~0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_macros") (r "~0.8.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.10.6") (f (quote ("json" "blocking"))) (d #t) (k 2)))) (h "1ndn542ssisq5v56v8rgs4zrrxqqlkib7qvbdsxy17k5fnpxcd8l")))

(define-public crate-vizio-openrtb-0.2.3 (c (n "vizio-openrtb") (v "0.2.3") (d (list (d (n "phf") (r "~0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "phf_macros") (r "~0.8.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.10.6") (f (quote ("json" "blocking"))) (d #t) (k 2)))) (h "12nvicbvkwcn6ysdvnq7has85ln5817vkqmn2m07d6hfammi9xw6")))

