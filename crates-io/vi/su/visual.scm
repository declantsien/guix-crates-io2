(define-module (crates-io vi su visual) #:use-module (crates-io))

(define-public crate-visual-0.1.0 (c (n "visual") (v "0.1.0") (h "1g7vyxgmsc2z6qm9fpplznnhmry6v0nzd6ccqk9jd3g3jd40fx82")))

(define-public crate-visual-0.2.0 (c (n "visual") (v "0.2.0") (h "16q655lpvavr5cq88bfxgs2n2wj0mv5ccbg4gi4ndvrcaq85snga")))

