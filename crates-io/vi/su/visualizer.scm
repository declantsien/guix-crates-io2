(define-module (crates-io vi su visualizer) #:use-module (crates-io))

(define-public crate-visualizer-0.1.0 (c (n "visualizer") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "16g1i555n64r4y3xmlp013qw2kps1vmghigg65gkvzaqpdc6hq56")))

(define-public crate-visualizer-0.1.1 (c (n "visualizer") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1hwzi4h8sa1357rxl6j1rcf210swkbzampw885ci60m43yv8gl0x")))

