(define-module (crates-io vi su visualpanic-rs) #:use-module (crates-io))

(define-public crate-visualpanic-rs-0.1.0 (c (n "visualpanic-rs") (v "0.1.0") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "135sn034nzbs1cik12bx3fg9l1qm4ff9ckxyqn0dw8lkqbk0qq9c")))

(define-public crate-visualpanic-rs-0.1.1 (c (n "visualpanic-rs") (v "0.1.1") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ynpy2iq16q9nlh13ac9q4mjvm49zzdc56qpsqvsc1njqqjgkzi5")))

(define-public crate-visualpanic-rs-0.1.2 (c (n "visualpanic-rs") (v "0.1.2") (d (list (d (n "native-dialog") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "10iad91hpkixlmdzm38q4yrcf9vpk7kb55abvxn2h3ahvfbdwlpg")))

