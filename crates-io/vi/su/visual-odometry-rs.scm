(define-module (crates-io vi su visual-odometry-rs) #:use-module (crates-io))

(define-public crate-visual-odometry-rs-0.1.0 (c (n "visual-odometry-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1dfmib3v3xwwarpk8ag0jzk0m9w8c2hlp954v1fl3b3h2bz6xhfj")))

