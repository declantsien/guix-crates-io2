(define-module (crates-io vi su visualize-sqlite) #:use-module (crates-io))

(define-public crate-visualize-sqlite-1.0.0 (c (n "visualize-sqlite") (v "1.0.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)))) (h "06v7gkxkqx9y11hr11kb2ygwrbpjwzydfll1vjnglxzibyindm0v")))

(define-public crate-visualize-sqlite-1.0.1 (c (n "visualize-sqlite") (v "1.0.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)))) (h "03cnaqxy8spxa6170w6vc73yzwkqifrhbghm184s982n1q24zvrl")))

(define-public crate-visualize-sqlite-1.1.0 (c (n "visualize-sqlite") (v "1.1.0") (d (list (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)))) (h "1ndima72kv1az3qw5h2y56ls5k0xvigx3c6giqybg96v7sba3ir7")))

(define-public crate-visualize-sqlite-1.1.1 (c (n "visualize-sqlite") (v "1.1.1") (d (list (d (n "diesel") (r "^1.4.7") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)))) (h "13s6w0167rrpjzlj8gyvxr804l5wcc8jfxdgk2q3l0p3rvm2gsfn")))

(define-public crate-visualize-sqlite-2.0.0-rc.0 (c (n "visualize-sqlite") (v "2.0.0-rc.0") (d (list (d (n "diesel") (r "^2.0.0-rc.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)))) (h "09xnra5wi2jv804whn3k94p1v6zy4hdb1kq663cnrq51a6baj9nb")))

(define-public crate-visualize-sqlite-2.0.0 (c (n "visualize-sqlite") (v "2.0.0") (d (list (d (n "diesel") (r "^2.0.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)))) (h "1kzqagz2sy3zivhjiixfgqzh7hxz9ls1js2w97nip1irjjn4cxbd")))

