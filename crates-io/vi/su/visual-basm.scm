(define-module (crates-io vi su visual-basm) #:use-module (crates-io))

(define-public crate-visual-basm-0.2.0 (c (n "visual-basm") (v "0.2.0") (d (list (d (n "cpclib-asm") (r "^0.6.0") (f (quote ("basm"))) (d #t) (k 0)) (d (n "eframe") (r "^0.17.0") (d #t) (k 0)) (d (n "rfd") (r "^0.8.2") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)))) (h "0hwkp31h07irbz5wa3s7lhxyy62vj0yfddwifb0xnlizkrh53amg")))

