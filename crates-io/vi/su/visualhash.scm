(define-module (crates-io vi su visualhash) #:use-module (crates-io))

(define-public crate-visualhash-1.0.0 (c (n "visualhash") (v "1.0.0") (d (list (d (n "image") (r "^0.21.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.4.0") (d #t) (k 0)))) (h "07a31anpmhii6s73qwhkg7xlk2ydqmg6ig3rr21jn4dbb01zsx99")))

