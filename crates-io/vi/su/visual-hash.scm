(define-module (crates-io vi su visual-hash) #:use-module (crates-io))

(define-public crate-visual-hash-3.3.2 (c (n "visual-hash") (v "3.3.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.4") (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 2)) (d (n "rustdct") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kf18qc783clsvgw0pmyy2z0nmv3flcm0f7zpljhca1kihl1wm5n")))

