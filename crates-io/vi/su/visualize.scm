(define-module (crates-io vi su visualize) #:use-module (crates-io))

(define-public crate-visualize-0.0.0 (c (n "visualize") (v "0.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gdndjn0858jx2vv8kc71by3sppdqmrh6046wc252ac8hma9aba8")))

(define-public crate-visualize-0.0.1 (c (n "visualize") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10jh01nxklf52qj2zp70dqg993hdyy4m1wvlmrib3i69pc3ajwj1")))

