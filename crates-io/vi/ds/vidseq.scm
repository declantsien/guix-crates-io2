(define-module (crates-io vi ds vidseq) #:use-module (crates-io))

(define-public crate-vidseq-0.0.1 (c (n "vidseq") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "gstreamer") (r "^0.17.4") (d #t) (k 0)) (d (n "gstreamer-app") (r "^0.17.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0jb6hsb0kcdlwkvsvkxf695hc80gi1xc0lsyp7znk95w1f431y28")))

