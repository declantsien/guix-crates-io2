(define-module (crates-io vi rm virmin) #:use-module (crates-io))

(define-public crate-virmin-0.1.0 (c (n "virmin") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0gsscnqph5q2rk3hcn8flr3dk7p31657zlhnymgami6yvmccv341")))

(define-public crate-virmin-0.2.0 (c (n "virmin") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "170ca6wv5prqv4bicyli88xv1w5i4rxvnznd39wd8p4zil8k5ync")))

(define-public crate-virmin-0.3.0 (c (n "virmin") (v "0.3.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1m3gdxypk3f07sv2qkc1y03sadghiiqcdskgr472k7zndl713p5k")))

