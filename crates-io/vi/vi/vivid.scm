(define-module (crates-io vi vi vivid) #:use-module (crates-io))

(define-public crate-vivid-0.4.0 (c (n "vivid") (v "0.4.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0pb04i4c5pyhs6rk35ca482p1ba2bd58d4jmav6pf86bw4b36fqv")))

(define-public crate-vivid-0.5.0 (c (n "vivid") (v "0.5.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1afqlnn74afzj29i1b1jqvs6ws1wfvqvs69f9hk14yc489jd0kdd")))

(define-public crate-vivid-0.6.0 (c (n "vivid") (v "0.6.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.6.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "125gb14fr7w0s7d337lwldhixa31k7kj2cdr1i1d1piq8362hpfk")))

(define-public crate-vivid-0.7.0 (c (n "vivid") (v "0.7.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^3.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.6.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "01fds6dm19bqgqydaa6n051v9l4wh9rb5d6sr9akwp2cc0fs43b7")))

(define-public crate-vivid-0.8.0 (c (n "vivid") (v "0.8.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1g42ppy6hlgqrqki92nyhsi3jqlabmjvqcqxa3f4w54ls98af0zf")))

(define-public crate-vivid-0.9.0 (c (n "vivid") (v "0.9.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("suggestions" "color" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "08nzz6r6n188zadpq6gq2z2ai8xf1csl6fmz7lw96hq1rb3dny9r")))

