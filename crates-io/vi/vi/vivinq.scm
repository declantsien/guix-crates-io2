(define-module (crates-io vi vi vivinq) #:use-module (crates-io))

(define-public crate-vivinq-1.0.0 (c (n "vivinq") (v "1.0.0") (d (list (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset" "brotli"))) (d #t) (k 0)))) (h "16ybn9s7fyax5bkbhzvypmcbjnxjp0b12lalqmp9g7hmvh4dcixj")))

(define-public crate-vivinq-1.1.0 (c (n "vivinq") (v "1.1.0") (d (list (d (n "ureq") (r "^2.6") (f (quote ("json" "charset" "brotli"))) (d #t) (k 2)))) (h "1q2f2fxvh6sbkf30klfanzrlba2yw5lfjbnc3krcb8518yx0nvv6")))

