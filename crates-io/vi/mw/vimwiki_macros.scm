(define-module (crates-io vi mw vimwiki_macros) #:use-module (crates-io))

(define-public crate-vimwiki_macros-0.1.0-alpha.1 (c (n "vimwiki_macros") (v "0.1.0-alpha.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "vimwiki") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "190wni19r257pxf0vd3b7g4gmbws4qrjv5gg423cflkbfcih4gx2")))

(define-public crate-vimwiki_macros-0.1.0-alpha.2 (c (n "vimwiki_macros") (v "0.1.0-alpha.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "vimwiki") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "1h1yhad8byycmkc9g81qzyzavay4n3x4nkld398clh5q0mk0cnz2")))

(define-public crate-vimwiki_macros-0.1.0-alpha.3 (c (n "vimwiki_macros") (v "0.1.0-alpha.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "vimwiki") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "1d4fx3x8x4fh38iwsn17ngwj07p72ab36hxcsjciw1vsfzblgsby")))

(define-public crate-vimwiki_macros-0.1.0-alpha.4 (c (n "vimwiki_macros") (v "0.1.0-alpha.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "vimwiki") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1qdik5afs3aj6ml8l2kf339kbqi6s06h7hnm934wm8cn7hm34n36")))

(define-public crate-vimwiki_macros-0.1.0-alpha.5 (c (n "vimwiki_macros") (v "0.1.0-alpha.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "vimwiki") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0fnbm3x1amsmmdy7v8fga0qfv3f0js4lrjhrxz8cipwlq68zxkfv")))

(define-public crate-vimwiki_macros-0.1.0 (c (n "vimwiki_macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)) (d (n "vimwiki-core") (r "=0.1.0") (d #t) (k 0)))) (h "1rwff9w54sza0lgp51zcafpvy43wd1dak2019a79l7p7wm0idk7q")))

