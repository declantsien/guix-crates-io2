(define-module (crates-io vi mw vimwiki-wasm) #:use-module (crates-io))

(define-public crate-vimwiki-wasm-0.1.0 (c (n "vimwiki-wasm") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.51") (d #t) (k 0)) (d (n "vimwiki") (r "=0.1.0") (f (quote ("html"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0nyl6w8dsxy8asrbff9g884prgaj3r217311lvq6ndkqkhc5sd6v")))

