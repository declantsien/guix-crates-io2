(define-module (crates-io vi mb vimba-sys) #:use-module (crates-io))

(define-public crate-vimba-sys-0.1.0 (c (n "vimba-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0bxr550fa2rg9d7i3xi45vd6wm1pzmah26fx0g4lk72v26x9l6k7") (r "1.56")))

(define-public crate-vimba-sys-0.1.1 (c (n "vimba-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0h7hqv8vj6ncnm7xxfa8ddfpffspd04ri7x1bcddkjhy1grbka0x") (r "1.56")))

(define-public crate-vimba-sys-0.1.2 (c (n "vimba-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "177bfnnirz4yd4ibz2z3adgvvr91as2k0zdxxavhw8lcm6frd05k") (r "1.56")))

(define-public crate-vimba-sys-0.2.0 (c (n "vimba-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "1n8x88503gspmz6qs4l6ylcjirsa8l47lkf4zvvy0ymddqkdj3zd") (r "1.56")))

