(define-module (crates-io vi n- vin-info) #:use-module (crates-io))

(define-public crate-vin-info-0.1.0 (c (n "vin-info") (v "0.1.0") (h "0x7m95bm91a0zrkh8sf80lkj3cb4z7yd5d7l95cqfalnl6f785yn")))

(define-public crate-vin-info-0.1.1 (c (n "vin-info") (v "0.1.1") (h "1z9qcgwdsz2gznb0zq8wn688nnvblg6n6sy3x8nz3lk06740j854")))

(define-public crate-vin-info-0.1.2 (c (n "vin-info") (v "0.1.2") (h "08ah9d8lxp3k2qalf5vr1rk8878l78mcwway9izb86rm5zppvj71")))

