(define-module (crates-io vi n- vin-macros) #:use-module (crates-io))

(define-public crate-vin-macros-1.0.1 (c (n "vin-macros") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dx6xd9bcq3w2zz1ij16sp5nf4jy9815768hs35g6c0k8ahiky82")))

(define-public crate-vin-macros-1.1.1 (c (n "vin-macros") (v "1.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wxvijls7dwnxjyjcndgifvad8qfkrl56mja3l2mbf89678j3l0y")))

(define-public crate-vin-macros-1.1.2 (c (n "vin-macros") (v "1.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xsrc39bzjppkw6jzyav9p53ch03kqm1l4ldj38ckf4r3gxvsn3z")))

(define-public crate-vin-macros-1.2.2 (c (n "vin-macros") (v "1.2.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wzfdr7bbbx9b1ypcc3wm8by01ymy9vci3g3z5fknliw4b3dx28q")))

(define-public crate-vin-macros-2.0.0 (c (n "vin-macros") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a25lvf242gqhwb3yc0yxvr0mm0hdzzx3b8yni9dp35k62ahlw7w")))

(define-public crate-vin-macros-2.1.0 (c (n "vin-macros") (v "2.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n7x0hvxwi8x5rzair4ihxi0yv2fwbvfwp05vyal6digc88jr1ms")))

(define-public crate-vin-macros-2.1.1 (c (n "vin-macros") (v "2.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19ipxics64rjgka6ipp8n29ywm029cc4l16ggn4j6d4466z92gba")))

(define-public crate-vin-macros-3.0.0 (c (n "vin-macros") (v "3.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r6d56v3hxv84lnmgs7mxxlx44slimm04x7r2zdf46vhngvh9lch")))

(define-public crate-vin-macros-4.0.0 (c (n "vin-macros") (v "4.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "169x49575fjig75d2x5flmk42jg4pkaryi8mibgzc62n0kxz43br")))

(define-public crate-vin-macros-4.0.1 (c (n "vin-macros") (v "4.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rww8w49llagg8vsn2k25yi9gy9jg19zcs8lhvalwnqwjbf3a3w4")))

(define-public crate-vin-macros-4.0.2 (c (n "vin-macros") (v "4.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06fca5kz4yggyzyq54s9ilp52qqaddwr4dms98zw29fi61dn848b")))

(define-public crate-vin-macros-4.0.3 (c (n "vin-macros") (v "4.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a09x4jjx0f9s2jkhhrap3qdlchghi8mbd4d2i15ha32lqxdaac6")))

(define-public crate-vin-macros-4.0.4 (c (n "vin-macros") (v "4.0.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xrp2b89vfq0wlbhk14kq38640axdgkk2m1dnkr4pf7wdp3yv05n")))

(define-public crate-vin-macros-4.1.0 (c (n "vin-macros") (v "4.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qq1v35f5igad1cha0va8aabfahsvw6j5a35pblz7xc8jx2xmlrl")))

(define-public crate-vin-macros-4.1.1 (c (n "vin-macros") (v "4.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s79x4i1j1996hrd00cxdbpjdpib8xy1dd7cgnanrd846xvg5sw7")))

(define-public crate-vin-macros-5.0.0 (c (n "vin-macros") (v "5.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k4km8swc1nyyc1lzc802pqm7pizdqxyzzk319clz3s7lx9hy772")))

(define-public crate-vin-macros-5.0.1 (c (n "vin-macros") (v "5.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nqgy5h4kb97wkv12766gllbb6nrh43i1wlh3b1j05pyvqaapzz3")))

(define-public crate-vin-macros-5.0.2 (c (n "vin-macros") (v "5.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17i8dl8r55gfkn1r0vxampg1f00245c7z5jfhmg1yjbqb23ygw2j")))

(define-public crate-vin-macros-5.0.3 (c (n "vin-macros") (v "5.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z33caw253qcymnrgnhw66601q9p2f5zg1i07qw69i459bbmzysy")))

(define-public crate-vin-macros-5.0.4 (c (n "vin-macros") (v "5.0.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04imm65250acgjfcrbnpjka1bm4kpgg6gnqjwv42yl3761hihkbd")))

(define-public crate-vin-macros-6.0.0 (c (n "vin-macros") (v "6.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wy3nfmkpk9d4kqqyjszvx42nhlyxyv5wxc4qxd12ix6pcbf7mf9")))

(define-public crate-vin-macros-6.0.1 (c (n "vin-macros") (v "6.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fnkkkaag0l07farm84frvfkqa9gml439i2ixr43zi6z9v3bgzwj")))

(define-public crate-vin-macros-7.0.0 (c (n "vin-macros") (v "7.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vwj2i7a0bwxg4g509aj8gnzpk0wrmzq25lh3yxy18swhg88ksdy")))

(define-public crate-vin-macros-7.0.1 (c (n "vin-macros") (v "7.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1abnzfg99d9xrar1ww4rmh8vaz9kx89kz4dfw0bkx49gdgxf2ll8")))

(define-public crate-vin-macros-8.0.0 (c (n "vin-macros") (v "8.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n5ffplxbg6ps6d2dl8q1f9r613sr4abxmikizvv5nh8vx0ncjcr")))

(define-public crate-vin-macros-8.0.1 (c (n "vin-macros") (v "8.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pf553a9zlk4b30yrva6s55f5pkvkhy1zwy50ymqffy7a51a0kkw")))

(define-public crate-vin-macros-8.0.2 (c (n "vin-macros") (v "8.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rassert-rs") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m9g716d2qvjyijnjg3is2kgkn3aksz76w8chnf6cgp5qzzafxn2")))

