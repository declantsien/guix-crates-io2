(define-module (crates-io vi ct victor-tree-internal-proc-macros) #:use-module (crates-io))

(define-public crate-victor-tree-internal-proc-macros-0.0.1 (c (n "victor-tree-internal-proc-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0dxvyabphlqjhwd00i2x9xzgwjcrbslkzf9dhd93fxjj2ims5f8z")))

