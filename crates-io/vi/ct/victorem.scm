(define-module (crates-io vi ct victorem) #:use-module (crates-io))

(define-public crate-victorem-0.1.0 (c (n "victorem") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "1q9jshyyd14wcqsf48941nxyy5nawk4ax0bffxdbm4f3zz934xjv")))

(define-public crate-victorem-0.2.0 (c (n "victorem") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "156spc49c08g77ds8zwj6cnylzir2a6h20vfh1sij7lj630113ma")))

(define-public crate-victorem-0.3.0 (c (n "victorem") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0jdbym9vvfjb0gkajwbnfsnvjznprz7nxi43a6y53kkhdpl0bid5")))

(define-public crate-victorem-0.4.0 (c (n "victorem") (v "0.4.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0ilkxqqx4jxns2vv06dgw9dddh77spcn9qpisrqchaq67ndbanqb")))

(define-public crate-victorem-0.5.0 (c (n "victorem") (v "0.5.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "01s06dr0li66yvc6zarnvddh2294j8y87ahsa0hk1rf69vg0m7ss")))

(define-public crate-victorem-0.6.0 (c (n "victorem") (v "0.6.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0i54psqhimwgv6kr0xhp3h3riq5hfjvg4iwg23xfw08j4bkz4jz2")))

(define-public crate-victorem-0.7.0 (c (n "victorem") (v "0.7.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "10li66cj1wp0cdwr10aq9fvlznrlbi3xmrpxzbn5n8a0z95sr457")))

(define-public crate-victorem-0.7.1 (c (n "victorem") (v "0.7.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0i2mnzl3f91hx0xrzic5pwdkgjjvx1z9rmvwhb0z2xkq40hvyvvd")))

(define-public crate-victorem-0.8.0 (c (n "victorem") (v "0.8.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0fw78aza2p9l3ypigzz92k97b1aaib297cigq5rb8nff00iyxlp4")))

(define-public crate-victorem-0.8.1 (c (n "victorem") (v "0.8.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "1jzh128xdgap5s1fi63sacgxd3k5w3a4xv4p9d13l4gxmkhxdbs7")))

(define-public crate-victorem-0.8.2 (c (n "victorem") (v "0.8.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "0pik08ida7dgdsgz0anq3sa0f50r0w2dn1byl08qnc3bzcbdzqcn")))

