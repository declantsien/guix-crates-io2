(define-module (crates-io vi ct victor) #:use-module (crates-io))

(define-public crate-victor-0.0.1 (c (n "victor") (v "0.0.1") (d (list (d (n "cssparser") (r "^0.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "selectors") (r "^0.5") (d #t) (k 0)) (d (n "string_cache") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.2") (d #t) (k 0)))) (h "0gfcqa29xhhpzixnxslzpfdkv0dxj4a9i7bkv787s84pqc9rsml4")))

