(define-module (crates-io vi ct victoria-dom) #:use-module (crates-io))

(define-public crate-victoria-dom-0.1.0 (c (n "victoria-dom") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1b1m3n29r7g7z75qvmhmvfdpg9gppsqg7zfz3nbhqrg1y0gzs8g6")))

(define-public crate-victoria-dom-0.1.1 (c (n "victoria-dom") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cg9lhwdaali8a5qxap8v3v86hjnl2mmx64zyd3ssx2cajd3vdlh")))

(define-public crate-victoria-dom-0.1.2 (c (n "victoria-dom") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "00is508vxg3zdcfrb55nxm04m056zc22svriq5qf1vq37qsjxvr7")))

