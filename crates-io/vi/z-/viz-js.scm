(define-module (crates-io vi z- viz-js) #:use-module (crates-io))

(define-public crate-viz-js-3.1.0 (c (n "viz-js") (v "3.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.37") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("SvgsvgElement"))) (d #t) (k 0)))) (h "1569spapisfqvfs7ypm7ai2mayg2la88zll04ylj6mbvkzip13xf")))

