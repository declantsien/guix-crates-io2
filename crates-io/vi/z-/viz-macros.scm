(define-module (crates-io vi z- viz-macros) #:use-module (crates-io))

(define-public crate-viz-macros-0.1.0-alpha (c (n "viz-macros") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "117lj8akcs0m1dqri4l1z5s127jp9gbjricpqirpl03d3xfhlksn")))

(define-public crate-viz-macros-0.1.0 (c (n "viz-macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0vclhyxdcscq69kqm3mapbvadn2yv49s8r6l46ixiyy6s2iaxlk2")))

(define-public crate-viz-macros-0.2.0 (c (n "viz-macros") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("net" "rt" "macros"))) (d #t) (k 2)) (d (n "viz-core") (r "^0.6.0") (d #t) (k 2)))) (h "19nwp2ykcazbsa5sj40k37ggj5f6fdkj7yx069lh0g22d7mqw6yi") (r "1.63")))

