(define-module (crates-io vi ny vinyl-core) #:use-module (crates-io))

(define-public crate-vinyl-core-0.0.1 (c (n "vinyl-core") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0dw08n6kbvsdpfiafhz213ciqvc9r9njh93i677vfhk1amxp0hy9") (f (quote (("build_protos" "protoc-rust"))))))

(define-public crate-vinyl-core-0.0.2 (c (n "vinyl-core") (v "0.0.2") (d (list (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0fvn43c4qkm6zwkxp3qc7yiv7vjazs49gawgv3v30bczwil8r3ab") (f (quote (("build_protos" "protoc-rust"))))))

