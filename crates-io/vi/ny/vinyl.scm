(define-module (crates-io vi ny vinyl) #:use-module (crates-io))

(define-public crate-vinyl-0.0.1 (c (n "vinyl") (v "0.0.1") (h "085ai6fq1901yh1d02z0msdm8qwzg6zik9hap6m1dgi2vvvw26ai")))

(define-public crate-vinyl-0.0.2 (c (n "vinyl") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "~0.1") (d #t) (k 0)) (d (n "grpc") (r "~0.6") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1czqwpc6yldf8z43rw97pjvrabd9bgb51b464z5d2sslizrh7q57") (f (quote (("build_protos" "protoc-rust" "protoc-rust-grpc"))))))

(define-public crate-vinyl-0.0.3 (c (n "vinyl") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "~0.1") (d #t) (k 0)) (d (n "grpc") (r "~0.6") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "vinyl-core") (r "^0.0.1") (d #t) (k 0)))) (h "0dcs2933i57s0fm4fn41y2yymbf3p1pixbra6mhlp5gkahncqnbg") (f (quote (("build_protos" "protoc-rust-grpc"))))))

(define-public crate-vinyl-0.0.4 (c (n "vinyl") (v "0.0.4") (d (list (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "~0.1") (d #t) (k 0)) (d (n "grpc") (r "~0.6") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "vinyl-core") (r "^0.0.2") (d #t) (k 0)))) (h "0s390c86gxdycx1fcdlam4vk03ls6x965dsnvq65cif1cpjqv9i5") (f (quote (("build_protos" "protoc-rust-grpc"))))))

