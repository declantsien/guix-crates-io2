(define-module (crates-io vi ny vinyl-embly) #:use-module (crates-io))

(define-public crate-vinyl-embly-0.0.1 (c (n "vinyl-embly") (v "0.0.1") (d (list (d (n "embly") (r "^0.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "vinyl-core") (r "^0.0.1") (d #t) (k 0)))) (h "1zx31wff32aalfa73ck9ga7hv0fd1i9harbbxl9gf118z8s6hj1m")))

(define-public crate-vinyl-embly-0.0.2 (c (n "vinyl-embly") (v "0.0.2") (d (list (d (n "embly") (r "^0.0.4") (d #t) (k 0)) (d (n "failure") (r "~0.1") (f (quote ("std"))) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "vinyl-core") (r "^0.0.2") (d #t) (k 0)))) (h "17f9kl1jwq7mawdraghicyyz199fznrzprqsqq1y42bmp18pglfv")))

