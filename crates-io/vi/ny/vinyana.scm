(define-module (crates-io vi ny vinyana) #:use-module (crates-io))

(define-public crate-vinyana-0.1.0 (c (n "vinyana") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xzzk46wsdfrnjzc587zlc0s66sg2bsfikqx19i8qqmnq1a6lnl3")))

(define-public crate-vinyana-0.2.0 (c (n "vinyana") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mnmrj10rv4a2wsdb3ya0ql1il7z4n8vczaysjih2dyi5aj0zw3s")))

(define-public crate-vinyana-0.3.0 (c (n "vinyana") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("serde" "matrixmultiply-threading"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10k132rqh7d9hyw21nv0k5ccdfj51842awd8siwzw8f4x7j4d0nm")))

(define-public crate-vinyana-0.3.1 (c (n "vinyana") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("serde" "matrixmultiply-threading"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xjiaqdgwmqncd8ydlx8rjxkdy2qmblbihxps0s6qv7bb6mm66kx")))

