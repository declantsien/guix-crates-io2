(define-module (crates-io vi ki vikid) #:use-module (crates-io))

(define-public crate-vikid-0.1.0 (c (n "vikid") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0al7g1p94zi24lya48b5yivn4fgm22mjxz50kxc0hc66gy6i11my")))

(define-public crate-vikid-0.1.1 (c (n "vikid") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cfvc879akwvg3drr4ji0mixl0457wl3ffka57mmjrrpk2zjwcm1")))

(define-public crate-vikid-0.1.2 (c (n "vikid") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01m8ry90ggmqpmz5v51n83kf71hr0khmcv9pqlyy829yx8j7zl6y")))

(define-public crate-vikid-0.1.3 (c (n "vikid") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ccqdw9mng9f0c5vps4dpw8n2dyy3vkvqq7ncy576d264vrrfkja")))

(define-public crate-vikid-0.1.4 (c (n "vikid") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h507pbbkpc5sdd30a2arkn8xmchyfla5j0c8f2vsxf6h61wb539")))

(define-public crate-vikid-0.1.6 (c (n "vikid") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0-rc.1") (d #t) (k 2)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nwn5r490dc9lmb0km5hl1c0f56qx6gbswbkqwxaq3w0hf1fj5xq")))

