(define-module (crates-io vi pe viper-rs) #:use-module (crates-io))

(define-public crate-viper-rs-0.2.1 (c (n "viper-rs") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "requests") (r "^0.0.30") (d #t) (k 0)))) (h "0ccxnindr69bmb4j91kzk7llnrc26aj4cdvm04i3d61avgczlk5c") (y #t)))

