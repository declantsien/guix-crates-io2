(define-module (crates-io vi pe vipera) #:use-module (crates-io))

(define-public crate-vipera-0.1.2 (c (n "vipera") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "06vfqqimwkmryakyl8gbiqgay7lhd2pl67z0s1i1rly7f7qvjl69")))

