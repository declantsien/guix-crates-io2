(define-module (crates-io vi pe vipers) #:use-module (crates-io))

(define-public crate-vipers-0.1.0 (c (n "vipers") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.13.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.13.2") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0xj17ndzkr59mwb6q0wph4ks1a9b0zikiwi4z7c7kq7pd1hl5g25")))

(define-public crate-vipers-0.1.1 (c (n "vipers") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.13.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.13.2") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0g1h91gc3pri0gr627qdpb1kwd0ma7kd00l059mlf6w59lbd01pk")))

(define-public crate-vipers-1.0.0 (c (n "vipers") (v "1.0.0") (d (list (d (n "anchor-lang") (r "^0.15.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.15.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.8") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "080n0c01c2g6j26bdknjn5zdzbksaz3brdagq6cc5zbhxsamlimq")))

(define-public crate-vipers-1.1.0 (c (n "vipers") (v "1.1.0") (d (list (d (n "anchor-lang") (r "^0.16.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.16.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0z5w58mwx4npg4pgf5h3fs2mrpj1fadq4r1y9sssdy5cdg02k0bk")))

(define-public crate-vipers-1.1.1 (c (n "vipers") (v "1.1.1") (d (list (d (n "anchor-lang") (r "^0.16.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.16.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "079gc8dbmw2dckahna9dyddw19nnk8m3h0z23wxslg5bv9v7b3jq")))

(define-public crate-vipers-1.2.0 (c (n "vipers") (v "1.2.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1j7wj441iwv4yr62rr232lpl6x86aqbwa1wvjpvx83fb3wvxamn6")))

(define-public crate-vipers-1.2.1 (c (n "vipers") (v "1.2.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1mz4zbbmn4xzqsrma4h4z8vb4gc0hv1mf3hdcblrpvinl59v0q49")))

(define-public crate-vipers-1.3.0 (c (n "vipers") (v "1.3.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1gqxkjzh9nz4i5af401p4f173y1mnm79sl3zbjqng6p2hk15r9xy")))

(define-public crate-vipers-1.4.0 (c (n "vipers") (v "1.4.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0kgsh7y3jrqwha6s46bcyw2v2a6g8pzgb7m9rycnkp5f04zhkl5c")))

(define-public crate-vipers-1.5.0 (c (n "vipers") (v "1.5.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "03g5l97gq5v8rys3aa0d65w2hcvzgqnar0lnc0kksg0c6pgcn3yx")))

(define-public crate-vipers-1.5.1 (c (n "vipers") (v "1.5.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0xg33w8wkgbhqf4kayh9dx2fpykcgqdy0rcvn5gyli8praaq8vdz")))

(define-public crate-vipers-1.5.2 (c (n "vipers") (v "1.5.2") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1nbk4gr1ymfmg7m9n5s9v9m1da5ykmlrcbkvilfsbqg5qwvr76ms")))

(define-public crate-vipers-1.5.3 (c (n "vipers") (v "1.5.3") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1h7za0h32vs8glpykv59y3yd0pck3axnz08rgmrp10irfm23i410")))

(define-public crate-vipers-1.5.4 (c (n "vipers") (v "1.5.4") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "02sv5lp92h5bank0vvgj1j07sr9nlpmbq1bdkp9js4szmk48ripg")))

(define-public crate-vipers-1.5.5 (c (n "vipers") (v "1.5.5") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1328qk948nfhzjp33h2kskqxw29ani41wm022h55gxvslfh1cirl")))

(define-public crate-vipers-1.5.7 (c (n "vipers") (v "1.5.7") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1mzlxs307zl3187xgq2nqs3qxndqpv5jy9gcvjzs9bcfzzng0d8r")))

(define-public crate-vipers-1.5.8 (c (n "vipers") (v "1.5.8") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 2)))) (h "02v7bkig7s2hllzq222cbkm4wzvr7cc4rd4cx2w1pnpvxz2dcf4j")))

(define-public crate-vipers-1.5.9 (c (n "vipers") (v "1.5.9") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "02fz63nrkncwbhs09d216as3ac5ax8zan1cxj12pnxqm5ag8735a")))

(define-public crate-vipers-1.6.0 (c (n "vipers") (v "1.6.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "0gk5zs14gdwbc3jmgi83ssr6j77y9v8i7biwis7xaw1gjkqn2r32")))

(define-public crate-vipers-1.6.1 (c (n "vipers") (v "1.6.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "0l046kd0a9k2m4vg65fky6hhjhijb7j84y67dcb8rpxvj94dra59")))

(define-public crate-vipers-2.0.0 (c (n "vipers") (v "2.0.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "0riij67jb0qrk5j6x6np6kj0v98i0hhq6nal3fgylpazv2617yp0")))

(define-public crate-vipers-2.0.1 (c (n "vipers") (v "2.0.1") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "1p5k94ikryxj4bf6gv1jdy905zbfz6xb7d02rall8f48ywn57yn7")))

(define-public crate-vipers-2.0.2 (c (n "vipers") (v "2.0.2") (d (list (d (n "anchor-lang") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "1dz4ij3m9s1lvqpmhprcdqiia7lcidq6cd0vbh18lq6q8qwjsnqc")))

(define-public crate-vipers-2.0.3 (c (n "vipers") (v "2.0.3") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "00rxv1yxgwpxf1q87pxyqlii19na4wj2bvz4hyxq2lyb0cjkx7c8")))

(define-public crate-vipers-2.0.4 (c (n "vipers") (v "2.0.4") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "0a35wzxhxyjcrkfknm54hpql0sicky6q3kj9msmsa41lhaqp55q1") (f (quote (("default") ("ata" "spl-associated-token-account"))))))

(define-public crate-vipers-2.0.6 (c (n "vipers") (v "2.0.6") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.25") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.25") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (o #t) (d #t) (k 0)) (d (n "spl-token") (r "^3") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 2)))) (h "1gq5gizjjj56pwjwi6yzsc0h16n7f5k057gnkwyn5v3cv6armbjr") (f (quote (("default") ("ata" "spl-associated-token-account"))))))

