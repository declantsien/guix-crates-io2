(define-module (crates-io vi d2 vid2img) #:use-module (crates-io))

(define-public crate-vid2img-0.1.0 (c (n "vid2img") (v "0.1.0") (d (list (d (n "glib") (r "^0.10.1") (d #t) (k 0)) (d (n "gstreamer") (r "^0.16.2") (d #t) (k 0)) (d (n "gstreamer-app") (r "^0.16.0") (d #t) (k 0)) (d (n "gstreamer-video") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zb41i5gghmip3rzsv9a6ggwhv49wm5km69ji43ajkzcbji7gc1z")))

(define-public crate-vid2img-0.1.1 (c (n "vid2img") (v "0.1.1") (d (list (d (n "glib") (r "^0.10.1") (d #t) (k 0)) (d (n "gstreamer") (r "^0.16.2") (d #t) (k 0)) (d (n "gstreamer-app") (r "^0.16.0") (d #t) (k 0)) (d (n "gstreamer-video") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19klfqbn8jslxrqcndn2vxyq8w9zcv78i4a2bkngq02xfbgk8z41")))

