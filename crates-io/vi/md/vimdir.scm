(define-module (crates-io vi md vimdir) #:use-module (crates-io))

(define-public crate-vimdir-0.1.0 (c (n "vimdir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "15ycd0555rx1hdj9317c1mk5z8fkcfinbr820l979rdsws536k6i") (y #t)))

(define-public crate-vimdir-0.2.0 (c (n "vimdir") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1w0z6sjdywzivsb04abh6j14018m9ly43vc80hpyqcz96ysmcnyl")))

(define-public crate-vimdir-0.3.0 (c (n "vimdir") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "060rm0w60gbzva3ih6i67dxipaw4am1gbsyl5hlyb16hzjpgwgb3")))

