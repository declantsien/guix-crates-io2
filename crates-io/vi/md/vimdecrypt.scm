(define-module (crates-io vi md vimdecrypt) #:use-module (crates-io))

(define-public crate-vimdecrypt-0.1.0 (c (n "vimdecrypt") (v "0.1.0") (d (list (d (n "blowfish") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "0jwzxkpcx2khw32pmw4dp23ilwjq2l43xdjn05j4p527qq4cwm3r")))

(define-public crate-vimdecrypt-0.1.1 (c (n "vimdecrypt") (v "0.1.1") (d (list (d (n "blowfish") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1yijva1nqlc9vwvmwda565ph4hyygp6jqiwyd7sbvm2zdg0spazm")))

(define-public crate-vimdecrypt-0.1.2 (c (n "vimdecrypt") (v "0.1.2") (d (list (d (n "blowfish") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "1mi4cs3r1b1yl41qzl43d0saws3gybgq1zm17rbndi80m9q18fai")))

(define-public crate-vimdecrypt-0.1.3 (c (n "vimdecrypt") (v "0.1.3") (d (list (d (n "blowfish") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0xmqgj9a67g8sqipfk7bxc4zg4wr8x1da23vpxll956s2kxjv0pw")))

