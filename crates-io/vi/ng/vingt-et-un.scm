(define-module (crates-io vi ng vingt-et-un) #:use-module (crates-io))

(define-public crate-vingt-et-un-0.1.0 (c (n "vingt-et-un") (v "0.1.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vg7k1713v6xl7l1w5hldqy18wvgs39i4k47i5y9v3ld6lssm7g3")))

(define-public crate-vingt-et-un-0.1.1 (c (n "vingt-et-un") (v "0.1.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qiw54ip64z9zhnimdy4rff2w1f1li3hhirarbpha0nhmyq3r6xl")))

