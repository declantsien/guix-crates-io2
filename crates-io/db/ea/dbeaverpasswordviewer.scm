(define-module (crates-io db ea dbeaverpasswordviewer) #:use-module (crates-io))

(define-public crate-DBeaverPasswordViewer-0.1.0 (c (n "DBeaverPasswordViewer") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xnngjv9fj18zpaqw5m4ciihd8anazs18fbhv49vhj3iz1b68pwk")))

