(define-module (crates-io db -u db-up-rbatis) #:use-module (crates-io))

(define-public crate-db-up-rbatis-0.1.0 (c (n "db-up-rbatis") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "db-up") (r "^0.1.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)))) (h "07xifp68yacw0pbamwxl2j7px826k9rgzpq9mla3fbwn4b9y88p5")))

(define-public crate-db-up-rbatis-0.2.0 (c (n "db-up-rbatis") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "db-up") (r "^0.2.0") (d #t) (k 0)) (d (n "rbatis") (r "^4.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)))) (h "1qm01ncl8n2midzbmxv99glaw01kdwvwni4w5wmyxfi6mygw41ma")))

