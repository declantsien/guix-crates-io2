(define-module (crates-io db -u db-up-sql-changelog) #:use-module (crates-io))

(define-public crate-db-up-sql-changelog-0.1.0 (c (n "db-up-sql-changelog") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0pzwm1j44zxz7p3q35lxpxhw15dsml50vi2csxwdllkz7vqifmav")))

(define-public crate-db-up-sql-changelog-0.2.0 (c (n "db-up-sql-changelog") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0mpssib3h5l3yhz4835i0jam7bj7f0xdqbkjdl46jifmi7bybxsw")))

