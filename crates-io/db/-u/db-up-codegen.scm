(define-module (crates-io db -u db-up-codegen) #:use-module (crates-io))

(define-public crate-db-up-codegen-0.1.0 (c (n "db-up-codegen") (v "0.1.0") (d (list (d (n "db-up-sql-changelog") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0rs2gpf6gg7b1r22v86nqph2im9cc3bkpv3p98cwsmi3c0aadwjb")))

(define-public crate-db-up-codegen-0.2.0 (c (n "db-up-codegen") (v "0.2.0") (d (list (d (n "db-up-sql-changelog") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3lhf2jp3257bc1fs9g24xk2s9j1bcbsasmypz5r85r9kfl5jyg")))

