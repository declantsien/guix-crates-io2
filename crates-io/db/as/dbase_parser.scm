(define-module (crates-io db as dbase_parser) #:use-module (crates-io))

(define-public crate-dbase_parser-0.1.0 (c (n "dbase_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.8") (d #t) (k 0)))) (h "1xzsd5p0hcbqwfaf1c3g38f2ac8axrfkdf4bh3d5ggidz39fq5ya")))

