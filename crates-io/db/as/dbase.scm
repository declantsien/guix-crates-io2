(define-module (crates-io db as dbase) #:use-module (crates-io))

(define-public crate-dbase-0.0.1 (c (n "dbase") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "0wc4rv471qxrnkz9bdfwm1f5sdr31xmg35sfm0nhgbgfryx4g2gz")))

(define-public crate-dbase-0.0.2 (c (n "dbase") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "1p2ng72mk5i7zss2iqiiwjd9xr7mf6iqnp0w9bb9hvpjhkavsdnw")))

(define-public crate-dbase-0.0.3 (c (n "dbase") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "0brhb1w92832bjvc969dkbsy0qill5lg814s00wmw6khwsjzjxr5")))

(define-public crate-dbase-0.0.4 (c (n "dbase") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "06mwfa46dr3nixipls3lnawn82p8mkfgc3by3nqv88fjk7bk45gi")))

(define-public crate-dbase-0.1.0 (c (n "dbase") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "1blp2dj4y1xynh76i77ycnnahq4gvvvlhgc2dzv2d8qfaahv43wl")))

(define-public crate-dbase-0.1.1 (c (n "dbase") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0cbqm0jrvqggmy726nr4pd5hcmpwmn6yns1is0kr0b8ggqa8qkkm")))

(define-public crate-dbase-0.1.2 (c (n "dbase") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "19y36pchkczsvvhqf5s0a8wiqzcqkls1d6p3azdwdryp0jwyasi8")))

(define-public crate-dbase-0.2.0 (c (n "dbase") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0djalq3qkd7fdw4chqk8a3c1b7mp39lsm91zf1rj5jd0lcqldrar")))

(define-public crate-dbase-0.2.1 (c (n "dbase") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "0as50w7spw94jdj7pi8jihkd0pa29kinlxmrymm4l3sy2m6f5260")))

(define-public crate-dbase-0.2.2 (c (n "dbase") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "1y43bsqpyjgsdqp14mpbwhp99fqz5vy6zby04x405xcskqklp6q5")))

(define-public crate-dbase-0.2.3 (c (n "dbase") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)))) (h "1yw0s5k3s85px8kmrarf6darc6vvdmg5whql14pxpq3b8745rcvh")))

(define-public crate-dbase-0.3.0 (c (n "dbase") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "yore") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "1rvzfaizhxd4il6cz13hr6653wbw5kn58nzjk7sq5293x6ls5hg3")))

(define-public crate-dbase-0.4.0 (c (n "dbase") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "datafusion") (r "^20") (o #t) (d #t) (k 0)) (d (n "datafusion-expr") (r "^20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26") (d #t) (k 2)) (d (n "yore") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1gr84h3089l8agnarypf2nxm8739izz3nnq9m2wr5jls5nkrgg46") (s 2) (e (quote (("datafusion" "dep:datafusion" "dep:datafusion-expr" "dep:async-trait"))))))

(define-public crate-dbase-0.5.0 (c (n "dbase") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "datafusion") (r "^31") (o #t) (d #t) (k 0)) (d (n "datafusion-expr") (r "^31") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26") (d #t) (k 2)) (d (n "yore") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "092df4zahx32vncp4b16l4z4nc00xdhcpcsdqvwq0g9s9xfhnz44") (s 2) (e (quote (("datafusion" "dep:datafusion" "dep:datafusion-expr" "dep:async-trait"))))))

