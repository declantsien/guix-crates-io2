(define-module (crates-io db ca dbcat) #:use-module (crates-io))

(define-public crate-dbcat-0.1.0 (c (n "dbcat") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "0p2mprwcrvqf8b2xk4vldn1imsl9gi1axg5zd7858fw72m1yz3bx")))

(define-public crate-dbcat-0.1.1 (c (n "dbcat") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "08bd9r0addiqwkjw2nmjsrp4dv2dpw4i48clsnw3dkgrfb49p125")))

(define-public crate-dbcat-0.1.2 (c (n "dbcat") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "1kxfv8q9p5y2ac15bwxvmrirjam90ri2ah0yj7wka4dja8yqbc80")))

(define-public crate-dbcat-0.1.3 (c (n "dbcat") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "0n12bfhk9v2pkj66qr4gj2aqf1gi70vgn5zm8a7s6wq6pbmscvpl")))

(define-public crate-dbcat-0.1.4 (c (n "dbcat") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "1qr1ar0ikhh5vrnjmw10cwdwp7mrb5kk92g1a06jr7vby64rjqd8")))

(define-public crate-dbcat-0.1.5 (c (n "dbcat") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "0landv4pl24ip8m8gd0bbsz4jkxz1hxq8hq8ffxqhmpb3rbzpvfl")))

