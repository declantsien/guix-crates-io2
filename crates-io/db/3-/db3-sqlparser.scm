(define-module (crates-io db #{3-}# db3-sqlparser) #:use-module (crates-io))

(define-public crate-db3-sqlparser-0.0.1 (c (n "db3-sqlparser") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "~0.20") (d #t) (k 0)))) (h "1g92g5fgsfn9jqzxbrj1bmrlalm8r9bygncw54k7a9vm2mxvfbhk")))

