(define-module (crates-io db #{3-}# db3-proto) #:use-module (crates-io))

(define-public crate-db3-proto-0.4.2 (c (n "db3-proto") (v "0.4.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)))) (h "1pqmb7vm464m68nl3m0q4mpr98b7fwwm5y0iy67lz4nd0wpxcapx")))

