(define-module (crates-io db _m db_meta_derive) #:use-module (crates-io))

(define-public crate-db_meta_derive-0.1.0 (c (n "db_meta_derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jbfngjirg7w3m3wwb4xbncf57ms64dqnfyn9zgj3cj1sjhac2dn")))

