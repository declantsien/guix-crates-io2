(define-module (crates-io db _u db_utils) #:use-module (crates-io))

(define-public crate-db_utils-0.1.0 (c (n "db_utils") (v "0.1.0") (h "0aw9fp71dr9w7jv9c9gbxvb6lcfk0006g8k26v85q9b3428qyn8m") (y #t)))

(define-public crate-db_utils-0.1.1 (c (n "db_utils") (v "0.1.1") (d (list (d (n "db_utils_config") (r "^0.1.1") (d #t) (k 0)) (d (n "db_utils_dao") (r "^0.1.1") (d #t) (k 0)) (d (n "macro_builder") (r "^0.1.1") (d #t) (k 0)))) (h "0jih2jakw7c359snc4mrxk32alsgiqgjy50akrh8bnwx71h6gxvk") (y #t)))

(define-public crate-db_utils-0.1.2 (c (n "db_utils") (v "0.1.2") (d (list (d (n "db_utils_config") (r "^0.1.1") (d #t) (k 0)) (d (n "db_utils_dao") (r "^0.1.1") (d #t) (k 0)) (d (n "macro_builder") (r "^0.1.2") (d #t) (k 0)))) (h "1w2wp8qbjyp42in524n97hqawlmicrh4acyws7bgjdlrq5knnwxd") (y #t)))

(define-public crate-db_utils-0.1.3 (c (n "db_utils") (v "0.1.3") (d (list (d (n "db_utils_config") (r "^0.1.1") (d #t) (k 0)) (d (n "db_utils_dao") (r "^0.1.1") (d #t) (k 0)) (d (n "macro_builder") (r "^0.1.3") (d #t) (k 0)))) (h "1rvxyxly7wzd14h8d2ghq3cbygvys7xgrjzbbivvl4ync31pwgyf") (y #t)))

(define-public crate-db_utils-0.1.4 (c (n "db_utils") (v "0.1.4") (d (list (d (n "db_utils_config") (r "^0.1.1") (d #t) (k 0)) (d (n "db_utils_dao") (r "^0.1.2") (d #t) (k 0)) (d (n "macro_builder") (r "^0.1.3") (d #t) (k 0)))) (h "055sf07qj5nmq5c1ap06jsxi0mw26gyv8zri5f1rjl64kpixpybm") (y #t)))

(define-public crate-db_utils-0.1.5 (c (n "db_utils") (v "0.1.5") (d (list (d (n "db_utils_config") (r "^0.1.1") (d #t) (k 0)) (d (n "db_utils_dao") (r "^0.1.2") (d #t) (k 0)) (d (n "macro_builder") (r "^0.1.3") (d #t) (k 0)))) (h "03m43sxwi8dj01qi6fnx9gnpih2dz802ipk66zpqmlprqkig2d5i") (y #t)))

