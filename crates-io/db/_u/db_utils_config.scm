(define-module (crates-io db _u db_utils_config) #:use-module (crates-io))

(define-public crate-db_utils_config-0.1.1 (c (n "db_utils_config") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "sync" "rt" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1rxa4yg95pk7lk405pv9b50xks8nbc7rw1pmm1swawq0bwxpyd38") (y #t)))

