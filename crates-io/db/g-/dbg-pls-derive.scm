(define-module (crates-io db g- dbg-pls-derive) #:use-module (crates-io))

(define-public crate-dbg-pls-derive-0.1.0 (c (n "dbg-pls-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jmvd1hi4ym8140kzjxwss7k3p8g77hj161q8687qzkiyk99kk7v")))

(define-public crate-dbg-pls-derive-0.1.1 (c (n "dbg-pls-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wpl526zn86vzmgkmrmqn8w44sdlajbal1fhc3paj270sbprrcvz")))

(define-public crate-dbg-pls-derive-0.1.2 (c (n "dbg-pls-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12dljp6agxqcqgpj8kqzba63kx69jahja31nsvfgyzk6zldg1l8v")))

(define-public crate-dbg-pls-derive-0.1.3 (c (n "dbg-pls-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lh1l6b86nvvz18xnmchywn9jgm0fhbgmh6djdcxik7zfjxp9bc5")))

(define-public crate-dbg-pls-derive-0.2.0 (c (n "dbg-pls-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wa8k9dclgkjm7f1h510g5n5y8psz49cskyq7i7wgzz8rnmad8xq")))

(define-public crate-dbg-pls-derive-0.2.1 (c (n "dbg-pls-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cfyxv9vhsja3wikqk9ayy2f4qmp9d2v1ywkl5ar0xq3fsrihqxl")))

(define-public crate-dbg-pls-derive-0.2.2 (c (n "dbg-pls-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "009mzxs6vbrjsms7cdhcddacarrs0rb4awxsp067vric0h3ka7rw")))

(define-public crate-dbg-pls-derive-0.3.0 (c (n "dbg-pls-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07r7z1bd02f9208hxbmn2vxqllan34prrk34yw5kn24yw0r0vb8q")))

(define-public crate-dbg-pls-derive-0.3.1 (c (n "dbg-pls-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zy9kwmz05vf9pxr8x9lm058v7d4n8kh3x69mqjbay6yh78zrhnf")))

(define-public crate-dbg-pls-derive-0.3.2 (c (n "dbg-pls-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04zgm3vfhrx6lixxr0j7ii5fablgd9bpqxlawg1v88kgyy76r4y7")))

(define-public crate-dbg-pls-derive-0.4.0 (c (n "dbg-pls-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gxl0fdvjsgc3k2l1106dbm9hjjplszjcf1kg409n0vx0skcryxd")))

