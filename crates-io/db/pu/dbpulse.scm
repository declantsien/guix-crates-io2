(define-module (crates-io db pu dbpulse) #:use-module (crates-io))

(define-public crate-dbpulse-0.1.0 (c (n "dbpulse") (v "0.1.0") (h "0ygvn3r2cn38dmg3ldi1ijpqr23kc41vk5f8qvqa6y0a7lw8fn1z")))

(define-public crate-dbpulse-0.1.1 (c (n "dbpulse") (v "0.1.1") (d (list (d (n "mysql") (r "^16.0.2") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)))) (h "1vc174k1ngw7zjfnari7b69bwxwkf7ifss4g1rz7xxdygv04g381")))

(define-public crate-dbpulse-0.1.2 (c (n "dbpulse") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)))) (h "0dr2kp3ls629dlq83n44sjpbwiwa18js30cilsz46n5vpbp0idhq")))

(define-public crate-dbpulse-0.1.3 (c (n "dbpulse") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)))) (h "0p7hfnzmd4r3cqfp0zdw8ix345iydgih7wx30drr17y9d4147xa3")))

(define-public crate-dbpulse-0.1.6 (c (n "dbpulse") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)))) (h "01yvv717h2lm7yqjcldpdkhmpmhrwrnkl6jv66dah25dj5g0jsjn")))

(define-public crate-dbpulse-0.2.0 (c (n "dbpulse") (v "0.2.0") (d (list (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jqky6mjhj50lsrd4lhifvsqami27gxsjbibia9vh63cqc4bhpk6")))

(define-public crate-dbpulse-0.2.1 (c (n "dbpulse") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z5wy0ajksh2d7zr4yxisg39nf22lwkfzc49x96v7dnzv43ykc8b")))

(define-public crate-dbpulse-0.2.2 (c (n "dbpulse") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a1bisd18iyb9929y35ykqp3nrqz0ydp1sz2pls73jzmhci5xx19")))

(define-public crate-dbpulse-0.2.3 (c (n "dbpulse") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nq6dxx943j6vvkyr5swpgk268c87xzwy705d16am3297s9d0s9y")))

(define-public crate-dbpulse-0.2.4 (c (n "dbpulse") (v "0.2.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slack-hook") (r "^0.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0b56pv865151v49bl7pbv1ysjgx735rbgij02wd9vgfpry0j6brn")))

(define-public crate-dbpulse-0.3.0 (c (n "dbpulse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("env"))) (d #t) (k 0)) (d (n "dsn") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mysql_async") (r "^0.29.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (f (quote ("process"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "00fbi5d20ndsw8cdp37s3n6sm01xp85bdbh9bci9d180fz2mrkn0")))

