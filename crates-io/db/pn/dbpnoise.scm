(define-module (crates-io db pn dbpnoise) #:use-module (crates-io))

(define-public crate-dbpnoise-0.1.0 (c (n "dbpnoise") (v "0.1.0") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "1v31yjiil320m7qp9lqlp0acgz5mr3582wl869b156l4fwghs5wj")))

(define-public crate-dbpnoise-0.1.1 (c (n "dbpnoise") (v "0.1.1") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "1pqy0gdns5fcnscl1r1h4fhml90qs1byy1giic5lkbcym7jl26is")))

(define-public crate-dbpnoise-0.1.2 (c (n "dbpnoise") (v "0.1.2") (d (list (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "11l5dglp5alrccdwz6wvlz4dzfr27m1ih407ahs53rb06qnjxadj")))

