(define-module (crates-io db en dbent-derive) #:use-module (crates-io))

(define-public crate-dbent-derive-0.1.0 (c (n "dbent-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "01c12q5gkhyf3cbaiy6s88f7yp7i3nmyslz1h5p15wws1mi9yzm1")))

