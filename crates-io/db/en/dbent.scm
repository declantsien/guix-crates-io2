(define-module (crates-io db en dbent) #:use-module (crates-io))

(define-public crate-dbent-0.1.0 (c (n "dbent") (v "0.1.0") (d (list (d (n "dbent-derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dcwbarmg99m5wvdh83p8hv3jb4zzizbgfp7d0bik7a8sb50fk3l") (f (quote (("derive" "dbent-derive") ("default" "serde" "derive"))))))

(define-public crate-dbent-0.1.1 (c (n "dbent") (v "0.1.1") (d (list (d (n "dbent-derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cjiqi9pdyx4rb4ns45v46biydp9nby1h1kwvc9f0302lbdv98x6") (f (quote (("derive" "dbent-derive") ("default" "serde" "derive"))))))

