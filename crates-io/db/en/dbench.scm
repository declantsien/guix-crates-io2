(define-module (crates-io db en dbench) #:use-module (crates-io))

(define-public crate-dbench-0.1.0 (c (n "dbench") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "mysql") (r "^8.0.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0s9hh19nhivj5v8c0w0hy2sjq8x0h0is0k6a2xsflw68slbp1dbm")))

