(define-module (crates-io db sc dbscan) #:use-module (crates-io))

(define-public crate-dbscan-0.1.0 (c (n "dbscan") (v "0.1.0") (h "102vis5v6p341b374pr0dy8r8nsza6s9386fqmyj1r34mfq4s1xh")))

(define-public crate-dbscan-0.1.1 (c (n "dbscan") (v "0.1.1") (h "0ap3bcz86mnbcljkxk9wg693fvc1vjxrw7cv04r8i6dqyqpl9kcg")))

(define-public crate-dbscan-0.2.0 (c (n "dbscan") (v "0.2.0") (h "1hkwngg9rygl7fry06x127pzbws2h75754abc46cjfla2lx9n7fk")))

(define-public crate-dbscan-0.3.0 (c (n "dbscan") (v "0.3.0") (h "1ng5dc84g33pbl0xajmp8ad10lcqdpfksvkdavkc6m0lw106pxzc")))

(define-public crate-dbscan-0.3.1 (c (n "dbscan") (v "0.3.1") (h "11cakn8mhb0jpqk42rg8m5j16vkcvqnyfhylp5qjmih7iaf10m4n")))

