(define-module (crates-io db qu dbquota) #:use-module (crates-io))

(define-public crate-dbquota-0.1.0 (c (n "dbquota") (v "0.1.0") (h "0qzs08pmz1kdqcm4sqw05fcyglkbx8c139z5cjbrl47ljsyc55kc")))

(define-public crate-dbquota-0.2.0 (c (n "dbquota") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)))) (h "08kgciz61p4i091w7nzxs66jzwq71f0sw06ys2zr24ggx0mqivra")))

(define-public crate-dbquota-0.3.0 (c (n "dbquota") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)))) (h "0ryznwg4zg8r41cfkyvj3rk39w6kj9z77asm3rv1hvx0xhl5kkib")))

(define-public crate-dbquota-0.3.1 (c (n "dbquota") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "mysql") (r "^16.0") (d #t) (k 0)))) (h "1771ywx2wwpnvbpgf89w01vkrv31fi9ijrw2abgf540myn8xfxvs")))

