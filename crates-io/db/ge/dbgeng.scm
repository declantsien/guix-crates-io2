(define-module (crates-io db ge dbgeng) #:use-module (crates-io))

(define-public crate-dbgeng-0.1.0 (c (n "dbgeng") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_Diagnostics" "Win32_System_Diagnostics_Debug" "Win32_System_Diagnostics_Debug_Extensions" "Win32_System_SystemInformation"))) (d #t) (k 0)))) (h "0bzwnjxvwpvyr7r6m51jkdwl349jz466izavx0sb7g5hpmgq9xqn") (r "1.70")))

