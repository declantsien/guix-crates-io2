(define-module (crates-io db -r db-rs) #:use-module (crates-io))

(define-public crate-db-rs-0.1.0 (c (n "db-rs") (v "0.1.0") (h "07qw5wa551prfb3f53n4il2kp102aixwim1b8rdizxh3rydnlx1k")))

(define-public crate-db-rs-0.1.1 (c (n "db-rs") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hx3m74l550ykr8lca6yapmks8i8iy8wxm9qiw27rlcslaj7lgmj")))

(define-public crate-db-rs-0.1.2 (c (n "db-rs") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v305m5r4pwna7wpqsgs2j6zcrmjbqvbrjqscdx3dk14s5g099dl")))

(define-public crate-db-rs-0.1.3 (c (n "db-rs") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "141xjcb5qw93qn9x3s61z18n95cb1gkg626079y42qxv6456p2l7")))

(define-public crate-db-rs-0.1.4 (c (n "db-rs") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "047vpla5plvx5mkrv1cg145x6sbrb24fnsbdxkg6bvmakxjdj4h9")))

(define-public crate-db-rs-0.1.5 (c (n "db-rs") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y12zqs37hf47j8qpaycphbsikhxb686ym6j2yaj4l3ndfrbi6l1")))

(define-public crate-db-rs-0.1.7 (c (n "db-rs") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j6qyb478vahji5jka5ykrik4kdkfkk8w1am8dihr5z7s4b88f8v")))

(define-public crate-db-rs-0.1.8 (c (n "db-rs") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iz2x38a25161aji3y38d2ly091z14npgyq5pa8cw00xcw7gpbck")))

(define-public crate-db-rs-0.1.9 (c (n "db-rs") (v "0.1.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jgzrlqd8kgk90c4gf4kra4zmklzaaqvqpfdsdq5sdvxwcy5i0qf")))

(define-public crate-db-rs-0.1.10 (c (n "db-rs") (v "0.1.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00y320m7vl3h371wgi9zag150nqgqs7zj6fwlnm552q3zk7jg57d")))

(define-public crate-db-rs-0.1.11 (c (n "db-rs") (v "0.1.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "186yjz0s7jq9xypg88dmfixni7097klw1xj8vkaa4vrd4ac6vwik")))

(define-public crate-db-rs-0.1.12 (c (n "db-rs") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j0vx2vw9mr6kgkgzjb5ymfnf1mkj0bw7bkp0dz4ikgl48pwffaj")))

(define-public crate-db-rs-0.1.13 (c (n "db-rs") (v "0.1.13") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ag7f0l07yjsnyckb4dzibh2gpirafyby94l7fz3lhipvrkbj51p")))

(define-public crate-db-rs-0.1.14 (c (n "db-rs") (v "0.1.14") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "030h9xqyd9bb11krnn9mlm8l8mrak61sd5wrninz9ldb8zwgfal5")))

(define-public crate-db-rs-0.1.15 (c (n "db-rs") (v "0.1.15") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00zkhgrfnk8apjkd9nibncg5nn3ichz2qzxh1i57gl253iid5129")))

(define-public crate-db-rs-0.1.16 (c (n "db-rs") (v "0.1.16") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h3nlagzfhy5rf6mbvnywpnj97w0wpyn8dmz2s8rvn01s1s7czsy")))

(define-public crate-db-rs-0.1.17 (c (n "db-rs") (v "0.1.17") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w448da3nmkhqf1iz8h94ykkirlvm64w11p6j4fzvmgwqcwk42vd")))

(define-public crate-db-rs-0.1.18 (c (n "db-rs") (v "0.1.18") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k4mm7ms05fhhwavk14v9mffzv0mpin9valzx9f6pf7d8aqcv3z3")))

(define-public crate-db-rs-0.2.0 (c (n "db-rs") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hjp2j6yrby4xd5ngzgjbylsxas90ylr5kmn3hm5v6lajjbbj1n8")))

(define-public crate-db-rs-0.2.1 (c (n "db-rs") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "046s5w5fw5ly3n52zbj2bx2x3qgcg0pa9433j81qq0dqwg43gdrn") (f (quote (("clone"))))))

(define-public crate-db-rs-0.3.0 (c (n "db-rs") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q0lf2wbdb220mijm3wc23i0840lf187k2b22jk8kb2f3hbvsxkf") (f (quote (("clone"))))))

(define-public crate-db-rs-0.3.1 (c (n "db-rs") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fhf4zk62mndkh2nbkz3qz8qwlzmzpg7h1wnpbgg7n6bzxbp101") (f (quote (("clone"))))))

