(define-module (crates-io db -r db-rpc-client-rs) #:use-module (crates-io))

(define-public crate-db-rpc-client-rs-0.1.0 (c (n "db-rpc-client-rs") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)))) (h "0hxgna9v2rhwc3ja2xmvww6kd690ljh80vd0yf9dmbqj264x7wkc")))

(define-public crate-db-rpc-client-rs-0.1.1 (c (n "db-rpc-client-rs") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.1") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.14") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)))) (h "1g0zih2gjf43h0cynxnkaa2iaszx2m4nip6jzidd303b5ncl3nx1")))

