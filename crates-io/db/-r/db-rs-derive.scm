(define-module (crates-io db -r db-rs-derive) #:use-module (crates-io))

(define-public crate-db-rs-derive-0.1.3 (c (n "db-rs-derive") (v "0.1.3") (d (list (d (n "db-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0qz58lrb8v9hp90lyiv8lzvgw7hnvs0iq5vcc2pxrnma5nnsh19s")))

(define-public crate-db-rs-derive-0.1.4 (c (n "db-rs-derive") (v "0.1.4") (d (list (d (n "db-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0xny38p4fdymw60wlrsdmmzr02mbw5rpgisd0miiz5ac3jd8fbfy")))

(define-public crate-db-rs-derive-0.1.5 (c (n "db-rs-derive") (v "0.1.5") (d (list (d (n "db-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "02w6chyw1yxx1alig0xw2y7fzw2iy0radpsmgj12ixcc9phmq44p")))

(define-public crate-db-rs-derive-0.1.6 (c (n "db-rs-derive") (v "0.1.6") (d (list (d (n "db-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0d22kn0d2h95yhf18y8i0vzc5myf615h7p6k12wmlbpkm2qicfq2")))

(define-public crate-db-rs-derive-0.1.7 (c (n "db-rs-derive") (v "0.1.7") (d (list (d (n "db-rs") (r "^0.1.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1gscqmbi44isrznis2z9v4qqh8p8kazsh93cdkh058fjq4dan3fz")))

(define-public crate-db-rs-derive-0.1.8 (c (n "db-rs-derive") (v "0.1.8") (d (list (d (n "db-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1wnrrjy68dv1aipd480c19mqlnzfrbc8snqirxkkgdf241xdkvji")))

(define-public crate-db-rs-derive-0.1.9 (c (n "db-rs-derive") (v "0.1.9") (d (list (d (n "db-rs") (r "^0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1iq6skjzqwlx1mmf7njm7gb3kmvr9py0anr5z4ilpgzpxsbsm845")))

(define-public crate-db-rs-derive-0.1.10 (c (n "db-rs-derive") (v "0.1.10") (d (list (d (n "db-rs") (r "^0.1.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1gmavm7xrxbjws132hax47vvq7sxspanpphkcw3b96w4q9jx7rjc")))

(define-public crate-db-rs-derive-0.1.11 (c (n "db-rs-derive") (v "0.1.11") (d (list (d (n "db-rs") (r "^0.1.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0913377lbd1rjp2cswvbqw4m81zh91rw57jiir1mc45xf80ksw7l")))

(define-public crate-db-rs-derive-0.1.12 (c (n "db-rs-derive") (v "0.1.12") (d (list (d (n "db-rs") (r "^0.1.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0vsx128lwvw21bzkz3grvsym3bc7iv247mwh76hrvydfq1d9nr5n")))

(define-public crate-db-rs-derive-0.1.13 (c (n "db-rs-derive") (v "0.1.13") (d (list (d (n "db-rs") (r "^0.1.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "18ly86i1rs4lq7q7wdick701qps5l8lf71iz6makzmn2lgzw3fd6")))

(define-public crate-db-rs-derive-0.1.14 (c (n "db-rs-derive") (v "0.1.14") (d (list (d (n "db-rs") (r "^0.1.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0iblkrvv2jm8afq68f488f47a7w2zia749k5zbvcwk5n662qi5bd")))

(define-public crate-db-rs-derive-0.1.15 (c (n "db-rs-derive") (v "0.1.15") (d (list (d (n "db-rs") (r "^0.1.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "16pnk6vcwdcfv3drsv6hazslx7fgkiz2vqadcaknjvn4z0civ2qn")))

(define-public crate-db-rs-derive-0.1.16 (c (n "db-rs-derive") (v "0.1.16") (d (list (d (n "db-rs") (r "^0.1.16") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0x6z2bwk7dgyf2msxrpisy3rcn5cz63qvviqcw1sdd0iq01hi4nj")))

(define-public crate-db-rs-derive-0.1.17 (c (n "db-rs-derive") (v "0.1.17") (d (list (d (n "db-rs") (r "^0.1.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0hz32b6agwwas3k6x9way5377ghn78d8hbssrh9m4sjsb406n2al")))

(define-public crate-db-rs-derive-0.1.18 (c (n "db-rs-derive") (v "0.1.18") (d (list (d (n "db-rs") (r "^0.1.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1fx0n8hv34q7aw36v8j5q4ngkiw512ms30am7kqlw8fmxj4pqc5c")))

(define-public crate-db-rs-derive-0.2.0 (c (n "db-rs-derive") (v "0.2.0") (d (list (d (n "db-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1b7im16alb7zx8wm3ki09ghmcx19sjg7p0rqqlnf5nl0riqm0lnc")))

(define-public crate-db-rs-derive-0.2.1 (c (n "db-rs-derive") (v "0.2.1") (d (list (d (n "db-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "04xdskf8hck8bn4mdf65nyzaf4m5mdm3iqwkr6nhja6wkndw4q89")))

(define-public crate-db-rs-derive-0.3.0 (c (n "db-rs-derive") (v "0.3.0") (d (list (d (n "db-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "15ajxbps64q3na07zzvnvcl95wn7qwp37wdq95ym3abv9ffnc1n4")))

(define-public crate-db-rs-derive-0.3.1 (c (n "db-rs-derive") (v "0.3.1") (d (list (d (n "db-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0am371kyshm8w773bd2wg544rdc28q1gzjcd9l5p0hx87d592p33")))

