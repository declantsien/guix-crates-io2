(define-module (crates-io db g6 dbg64_plugins_sdk_sys) #:use-module (crates-io))

(define-public crate-dbg64_plugins_sdk_sys-0.1.0 (c (n "dbg64_plugins_sdk_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0z0rx9xynl5i7mn81g076npqhla5k0943354xfh7pb4ql3f1f1lk")))

(define-public crate-dbg64_plugins_sdk_sys-0.1.1 (c (n "dbg64_plugins_sdk_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1bk481q732zssggqvgr40320hbkf6w3rpi5l5bf6n0b502z0pqn2")))

(define-public crate-dbg64_plugins_sdk_sys-0.1.2 (c (n "dbg64_plugins_sdk_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1wwqcgzccy2r6053a5gm5k3nl1mf0rwfa7yp1g3xsgx10x5m2mz2") (y #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1.3 (c (n "dbg64_plugins_sdk_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1dib2h778kc2d7n04l7hqg1rmd5f805d29jp9dbv4c8vd0yqrhaq") (y #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1.4 (c (n "dbg64_plugins_sdk_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0jjv680mh83vaahlrcka7zc40x62dvjw937yxwzb3gbmkpnvrxsp") (y #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1.5 (c (n "dbg64_plugins_sdk_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)))) (h "0xr1d5hlsgmpc76afqbgqwlyz4qg1r3xk9781r4xjpqaqc2s37qp") (f (quote (("x86") ("x64") ("default" "x64")))) (y #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1.6 (c (n "dbg64_plugins_sdk_sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)))) (h "17va8d369zlwk7s547s4k2ciikcc9w52xnmliardhs3wqx36ibdw") (f (quote (("x86") ("x64") ("default" "x64")))) (y #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1.7 (c (n "dbg64_plugins_sdk_sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 1)))) (h "1pngf3qs8h69l76wz53wqjbycmg99g5h2xff7yxb2r97a5ncc0lw") (f (quote (("x86") ("x64") ("default" "x64"))))))

