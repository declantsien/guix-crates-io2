(define-module (crates-io db _c db_code) #:use-module (crates-io))

(define-public crate-db_code-0.1.2 (c (n "db_code") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "db_code_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "rust_kits") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("runtime-tokio-rustls" "sqlite"))) (d #t) (k 0)) (d (n "xid") (r "^1.0.3") (d #t) (k 0)))) (h "1a8pg7ybq73cwbci92a5dnwhk3k2vpli3hwipd2ahmc6xlgiai0c")))

(define-public crate-db_code-0.1.3 (c (n "db_code") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "rust_kits") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("runtime-tokio-rustls" "sqlite"))) (d #t) (k 0)) (d (n "xid") (r "^1.1.1") (d #t) (k 0)))) (h "0ylj3gf7pnbac0ldhrfg08h3kq40g0sn8ckjw6b8ymcmpbvnxjgz")))

