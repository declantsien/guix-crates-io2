(define-module (crates-io db gt dbgtools) #:use-module (crates-io))

(define-public crate-dbgtools-0.1.0 (c (n "dbgtools") (v "0.1.0") (d (list (d (n "dbgtools-win") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0qdg9d7fidl0145n789cirzmdjxajbdqnv8f0rj6rf1grr5nrfcj")))

(define-public crate-dbgtools-0.2.0 (c (n "dbgtools") (v "0.2.0") (d (list (d (n "dbgtools-win") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1dip4r0yz103jdh4ydrdjqsnwi0ijd6riig53daw1hx6z1xspw02")))

(define-public crate-dbgtools-0.2.1 (c (n "dbgtools") (v "0.2.1") (d (list (d (n "dbgtools-hexdump") (r "^0.1") (d #t) (k 0)) (d (n "dbgtools-win") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1n85i2nvbdgic3dr00a5nab7ksq3ps3d625zcxw4k1fry8yj24nf")))

