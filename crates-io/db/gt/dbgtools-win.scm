(define-module (crates-io db gt dbgtools-win) #:use-module (crates-io))

(define-public crate-dbgtools-win-0.1.0 (c (n "dbgtools-win") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.3") (d #t) (k 0)) (d (n "windows") (r "^0.3") (d #t) (k 1)))) (h "0s7z3cwnrk1s2lapnpwrds7zf3qjngl7mq84f13f7d5isfv93bfm")))

(define-public crate-dbgtools-win-0.2.0 (c (n "dbgtools-win") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.9") (d #t) (k 0)) (d (n "windows") (r "^0.9") (d #t) (k 1)))) (h "1ijpsd599qrsr6qnd22vv5dy1rcmxjjp2izczfvyryisz1771nzs")))

(define-public crate-dbgtools-win-0.2.1 (c (n "dbgtools-win") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.9") (d #t) (k 0)) (d (n "windows") (r "^0.9") (d #t) (k 1)))) (h "09cl7q9mwxm57zfqsrxj97xzka0p9d1impyy18d5fhgl02wr63f1")))

