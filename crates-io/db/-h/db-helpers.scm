(define-module (crates-io db -h db-helpers) #:use-module (crates-io))

(define-public crate-db-helpers-0.1.0 (c (n "db-helpers") (v "0.1.0") (h "0l8cfk10xci94iclds2r9fgzn642sv094dwpkp69d0sjf6h34g36")))

(define-public crate-db-helpers-0.2.0 (c (n "db-helpers") (v "0.2.0") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1pmmxyix6x1rdbi50i2b9h1cpdw74wjhijyxnc7l7g3w70hnlap0") (f (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.3.0 (c (n "db-helpers") (v "0.3.0") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1p71kfgjf49nlqv436bvrgwmyhfg8dav4r64r4yd92p58a78ayg7") (f (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.4.0 (c (n "db-helpers") (v "0.4.0") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1g5cjhkxhdbay94biy6l8paz48g4ykyh4y7wkahmgsfigpcfd1wc") (f (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.5.0 (c (n "db-helpers") (v "0.5.0") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0abvxqvas3qcgfx59p54hi19q2f5vvng1b9b8kb15ijlp766c1ni") (f (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.5.1 (c (n "db-helpers") (v "0.5.1") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1gx9zr8i586dbnldv74mg2vxvwmfzxwxffs1qk4fhsr5pwsf4y6b") (f (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.6.0 (c (n "db-helpers") (v "0.6.0") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0gynacq9rcbnb64d8fvjgrsy6f9yw8c4f72h6cmgcj6s9d4mh85c")))

(define-public crate-db-helpers-0.7.0 (c (n "db-helpers") (v "0.7.0") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "17l4sr2p3ba5w3nv3r8fsdnv61cvdxq3kb3rryak5x8sx5d7rzzp")))

(define-public crate-db-helpers-0.7.1 (c (n "db-helpers") (v "0.7.1") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "13xsl81asilajcz869k8nqshf3z3504mshw5qxyvrxcpqw5l6j2g")))

(define-public crate-db-helpers-0.8.0 (c (n "db-helpers") (v "0.8.0") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0l4kg0f7136g3kjdfdbnihw75nvkrhjzaq3f1qg400i96xkn1mch")))

(define-public crate-db-helpers-0.9.0 (c (n "db-helpers") (v "0.9.0") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0pygpm8pw38k0v3xps3k3gbk1n7a3mkqy5nwhhlc48a9s65qzmns")))

(define-public crate-db-helpers-0.9.1 (c (n "db-helpers") (v "0.9.1") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "1chyhian6m90f8m468gzd646cmyp2kbw923js8r4gfq99ana7h1z")))

(define-public crate-db-helpers-0.10.0 (c (n "db-helpers") (v "0.10.0") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0qargwj6lzvdw00w5w7z0gj4mqf47mdlqc8z3zga2zrhcyrv9wla")))

(define-public crate-db-helpers-0.10.1 (c (n "db-helpers") (v "0.10.1") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "12qvp2nkkz4awi2y1vivnsjxkzj6fk4zl4i7b02c97bqwpr8hmvv")))

(define-public crate-db-helpers-0.11.0 (c (n "db-helpers") (v "0.11.0") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0iq4djwhcqp92zpmmncr7y96q0c7hn3gpp68s4amjpp0cz5lklck") (f (quote (("pg" "tokio-postgres") ("default" "pg"))))))

(define-public crate-db-helpers-0.11.1 (c (n "db-helpers") (v "0.11.1") (d (list (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0czhil5q9i9y6l9raclmsjj3j38zsaflfxlhvbwx0dn7b0zjx5a6") (f (quote (("pg" "tokio-postgres") ("default" "pg"))))))

(define-public crate-db-helpers-1.0.0 (c (n "db-helpers") (v "1.0.0") (d (list (d (n "db-helpers-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "05246s8hs6xn32rm38fivpfjvqsih9rn9ncgy657nmnm5v8l4v7w") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.0.1 (c (n "db-helpers") (v "1.0.1") (d (list (d (n "db-helpers-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "13fwipcbjpr01ij4vh46z3vgnmf98a8bzv09mnsr91g3m9vdx4pw") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.1.0 (c (n "db-helpers") (v "1.1.0") (d (list (d (n "db-helpers-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0r59ymj9jm07kgbgmdrz068rw04km6f3s3cqp3jkpyjy9hgqqmm3") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.3.0 (c (n "db-helpers") (v "1.3.0") (d (list (d (n "db-helpers-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "01wa65z73qmhq9vp5qqahpdph2b3nzh4z29rx1kiw10hq1gg3zkh") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.3.1 (c (n "db-helpers") (v "1.3.1") (d (list (d (n "db-helpers-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1w6s68zwyf7sf6v45npys3mas230pfzjsccz77fxjwqcxkw8drs9") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.4.0 (c (n "db-helpers") (v "1.4.0") (d (list (d (n "db-helpers-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "077npf9bs8b9927vgcmkvwf93z885ah0fvc1z1nn6g3dml0cf57n") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1.5.0 (c (n "db-helpers") (v "1.5.0") (d (list (d (n "db-helpers-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (o #t) (d #t) (k 0)))) (h "096q50aqqmlp7svrbf8694kfy83wdsxr01242m266xzplw6rjwrm") (f (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

