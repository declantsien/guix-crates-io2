(define-module (crates-io db #{-i}# db-introspector-gadget) #:use-module (crates-io))

(define-public crate-db-introspector-gadget-0.1.0 (c (n "db-introspector-gadget") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("mysql" "postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16rf8k3cwkm1k87ls56w7aa6ba6fkr1gc384qx9gfw92xnbr92lr")))

(define-public crate-db-introspector-gadget-0.2.0 (c (n "db-introspector-gadget") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("mysql" "postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x555i03xbv27dlyz0hnavb63ynm9y081j6jaqcz2qd1pfdm9zc1")))

