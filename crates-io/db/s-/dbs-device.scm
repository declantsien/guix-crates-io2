(define-module (crates-io db s- dbs-device) #:use-module (crates-io))

(define-public crate-dbs-device-0.1.0 (c (n "dbs-device") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sg4azy4f2mnk8ib3ragf1y2wvd11pdzcxb13xv9ribzcldvc7j1")))

(define-public crate-dbs-device-0.2.0 (c (n "dbs-device") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jk519liwlflaimy82p4f3f8c4kqxa5qclfb5h61r1mwni2fmv0l")))

