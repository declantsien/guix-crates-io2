(define-module (crates-io db s- dbs-uhttp) #:use-module (crates-io))

(define-public crate-dbs-uhttp-0.1.0 (c (n "dbs-uhttp") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext" "os-poll"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.1") (d #t) (k 2)))) (h "04svfwhbph6alw9insm1fqpnswvkdgx07gvzd5rqkxl3fh48girn")))

(define-public crate-dbs-uhttp-0.2.0 (c (n "dbs-uhttp") (v "0.2.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext" "os-poll"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.1") (d #t) (k 2)))) (h "1p0c5k4q2vp06dx8c4jqjc3kn4vbhg0cg438fklki108p7qzfwxp")))

(define-public crate-dbs-uhttp-0.3.0 (c (n "dbs-uhttp") (v "0.3.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "mio") (r "^0.8.0") (f (quote ("os-ext" "os-poll"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.1") (d #t) (k 2)))) (h "1dmznk9g33qhcvg0dl3vqpidhwi67v28zc40m8a0h3pcxcgc28qb")))

(define-public crate-dbs-uhttp-0.3.1 (c (n "dbs-uhttp") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext" "os-poll"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0") (d #t) (k 2)))) (h "1k61jsq0q6zy8sq8lbhy0ldshyfj6qw88048xf6zm0dswx7m9l3g")))

(define-public crate-dbs-uhttp-0.3.2 (c (n "dbs-uhttp") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext" "os-poll"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0") (d #t) (k 2)))) (h "0j3j9fmb6hp4avaws6kmf9pd1nhmxcvpmn4aqf2cgjprgd2rpaxw")))

