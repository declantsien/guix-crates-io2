(define-module (crates-io db s- dbs-snapshot) #:use-module (crates-io))

(define-public crate-dbs-snapshot-1.5.0 (c (n "dbs-snapshot") (v "1.5.0") (d (list (d (n "criterion") (r "^0.5.0") (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "versionize") (r "^0.1.10") (d #t) (k 0)) (d (n "versionize_derive") (r "^0.1.5") (d #t) (k 0)))) (h "0vjzr0h2rkgnzk3y3iam11m6z0qsagaq9yzrcp3r6kj184nciw5s")))

(define-public crate-dbs-snapshot-1.5.1 (c (n "dbs-snapshot") (v "1.5.1") (d (list (d (n "criterion") (r "^0.5.0") (k 2)) (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "versionize") (r "^0.1.10") (d #t) (k 0)) (d (n "versionize_derive") (r "^0.1.5") (d #t) (k 0)))) (h "0ybf6bxvfmsk147f2mxl3fz0jgk682r8awicz0smckl03xvr531w")))

