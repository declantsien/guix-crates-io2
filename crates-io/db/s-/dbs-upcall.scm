(define-module (crates-io db s- dbs-upcall) #:use-module (crates-io))

(define-public crate-dbs-upcall-0.1.0 (c (n "dbs-upcall") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dbs-utils") (r "^0.2") (d #t) (k 0)) (d (n "dbs-virtio-devices") (r "^0.1") (f (quote ("virtio-vsock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "timerfd") (r "^1.2.0") (d #t) (k 0)))) (h "0dcdb8p7f5w497l7p819wqdjmdl9m5gipsnfx9wigmvwcmkqpymj")))

(define-public crate-dbs-upcall-0.2.0 (c (n "dbs-upcall") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dbs-utils") (r "^0.2") (d #t) (k 0)) (d (n "dbs-virtio-devices") (r "^0.2") (f (quote ("virtio-vsock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "timerfd") (r "^1.2.0") (d #t) (k 0)))) (h "0gzw5virh66866biggnzmi8a0nvq6yy93zblsh04pbj4ljpn57k9")))

(define-public crate-dbs-upcall-0.3.0 (c (n "dbs-upcall") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dbs-utils") (r "^0.2") (d #t) (k 0)) (d (n "dbs-virtio-devices") (r "^0.3") (f (quote ("virtio-vsock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "timerfd") (r "^1.2.0") (d #t) (k 0)))) (h "1yz4bkj2fv3afdbq3qdi01fxqy5kcb17axi5225qpgnhiw97hfpa")))

