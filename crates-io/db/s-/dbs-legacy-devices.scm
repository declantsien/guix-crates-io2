(define-module (crates-io db s- dbs-legacy-devices) #:use-module (crates-io))

(define-public crate-dbs-legacy-devices-0.1.0 (c (n "dbs-legacy-devices") (v "0.1.0") (d (list (d (n "dbs-device") (r "^0.2.0") (d #t) (k 0)) (d (n "dbs-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "vm-superio") (r "^0.5.0") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.10.0") (d #t) (k 0)))) (h "1pxw43g5fks8czbsqy10fl2g5k1pg1x4yjbi688qnhff2gi2xhg8")))

(define-public crate-dbs-legacy-devices-0.1.1 (c (n "dbs-legacy-devices") (v "0.1.1") (d (list (d (n "dbs-device") (r "^0.2.0") (d #t) (k 0)) (d (n "dbs-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "vm-superio") (r "^0.5.0") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "07s3gwx8lca0n32if06impvr75rf8q4xwndy6f0nq62d3jn8kl64")))

