(define-module (crates-io db s- dbs-allocator) #:use-module (crates-io))

(define-public crate-dbs-allocator-0.1.0 (c (n "dbs-allocator") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dlrr5q51n7kj7c9pdlkfzd09mbcvj6fcrksdi42rhnrkfn898wj")))

(define-public crate-dbs-allocator-0.1.1 (c (n "dbs-allocator") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "010hf2iw51y8z3fghfdbcxda95if8mbghidv5ryl7hab9fwi2dsl")))

