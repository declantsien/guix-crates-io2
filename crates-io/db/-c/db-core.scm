(define-module (crates-io db -c db-core) #:use-module (crates-io))

(define-public crate-db-core-0.1.0 (c (n "db-core") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0ppyay1v1v77zilccyc8pm7frlzklw7ll5gvjcn48f7k3nj43pvg")))

(define-public crate-db-core-0.1.1 (c (n "db-core") (v "0.1.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "16wfcqd10xn4595ixjyqg7pnsypqa3khxgwhmqshdxj800nf9c33")))

(define-public crate-db-core-0.2.0 (c (n "db-core") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1ddv1sx4ff7y12v8qq18gizgdgiy9mylk1f5rz7ypk64932qbd1j")))

(define-public crate-db-core-0.2.1 (c (n "db-core") (v "0.2.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0pp2anzadnwymd5gmh6z69iblqnpacbaddqfnca79l9p1nax1fn3")))

