(define-module (crates-io db f_ dbf_dextractor) #:use-module (crates-io))

(define-public crate-dbf_dextractor-0.1.0 (c (n "dbf_dextractor") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0i5kp16wiy3rnnhkcyhq767iplm99fmvr85jfpl76nzvnayfnd8c")))

(define-public crate-dbf_dextractor-0.1.1 (c (n "dbf_dextractor") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "11l7r9vvrip06lf9fa4250r1yc0flhanvlxb8crws35ik2ds27mn")))

(define-public crate-dbf_dextractor-0.1.2 (c (n "dbf_dextractor") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)))) (h "103ssv02w51hwv1r3j9n93q8vj1hjs5v5f0d4hz50xjvzhpzncbw")))

