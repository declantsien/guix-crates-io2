(define-module (crates-io db gf dbgf) #:use-module (crates-io))

(define-public crate-dbgf-0.1.0 (c (n "dbgf") (v "0.1.0") (h "093d1z1iph4rpn04b2a7i0cjhx7qyncaqj0rvmc5b4sbnakyp4a4")))

(define-public crate-dbgf-0.1.1 (c (n "dbgf") (v "0.1.1") (h "1xxjjkl9zfbrs7rf4xhrjxqwampxai48jyh6fqrb312alr4wms13")))

(define-public crate-dbgf-0.1.2 (c (n "dbgf") (v "0.1.2") (h "0scism6lr9ddaw9gpnz8pz1v7z09s8dijbs6w12q02x7bjs9djp6")))

