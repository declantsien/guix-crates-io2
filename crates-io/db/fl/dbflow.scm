(define-module (crates-io db fl dbflow) #:use-module (crates-io))

(define-public crate-dbflow-0.0.1 (c (n "dbflow") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10iqpqr0622ajmf86xq92b51968l0p9g75197p511qylffid57qh")))

