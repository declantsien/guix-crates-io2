(define-module (crates-io db in dbin) #:use-module (crates-io))

(define-public crate-dbin-0.1.0 (c (n "dbin") (v "0.1.0") (h "0650h29xi8lnkvwl8az1ch7k16q2225jrj3gdcfr3mncx5y5m9gl")))

(define-public crate-dbin-0.1.1 (c (n "dbin") (v "0.1.1") (h "1z93pwyr08za1kq9xmfgjjlwh9xnwkp6g53l5gfwbg0n2z33p2i5")))

(define-public crate-dbin-0.1.2 (c (n "dbin") (v "0.1.2") (h "0kwadqx0ajkrrbq1lxg42fnzk7wv9670apc9npy5qpqf5pq5sisy")))

(define-public crate-dbin-0.1.3 (c (n "dbin") (v "0.1.3") (h "1qgny8zj8rci53ykdsk1980cxjghvl6qizlgg2wyw3msbbyx6yw0")))

(define-public crate-dbin-0.1.4 (c (n "dbin") (v "0.1.4") (h "1b0j05qa10vls28l37h7pz5qzvfq6xl8j9wmslcxpzvmbiww85fg")))

(define-public crate-dbin-0.1.5 (c (n "dbin") (v "0.1.5") (h "0vmsc49r6l2calr1gjxj1ll24b0y6ghzw6qd42rl5d9rzhhm64yj")))

(define-public crate-dbin-0.1.6 (c (n "dbin") (v "0.1.6") (h "107abvfj46wm554d8xggbqarp9hhjsij6z8xz20v6c6zcdba4avz")))

(define-public crate-dbin-0.1.7 (c (n "dbin") (v "0.1.7") (h "07rm0zclwbq2d4sa5x6mgwqykqkxwbh4ffpv1vbyl29l7qsp1pw3")))

