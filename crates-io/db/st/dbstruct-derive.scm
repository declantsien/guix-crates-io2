(define-module (crates-io db st dbstruct-derive) #:use-module (crates-io))

(define-public crate-dbstruct-derive-0.1.0-alpha (c (n "dbstruct-derive") (v "0.1.0-alpha") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j0059cxpm1797krrjni9qg1013s50va48z6hnngrzvdqjm2wga0")))

(define-public crate-dbstruct-derive-0.1.0-alpha.1 (c (n "dbstruct-derive") (v "0.1.0-alpha.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0620magn8xpw06d8cd2zkn441bj2qj4c08p07xrv22c2nmxnv4c9")))

(define-public crate-dbstruct-derive-0.1.0-beta (c (n "dbstruct-derive") (v "0.1.0-beta") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "177p9q91chxz2f5cnbhi3g3fl4z2pgp8pc8lz3jbwsnpsyinm65i")))

(define-public crate-dbstruct-derive-0.1.0 (c (n "dbstruct-derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zv50jqkyib1xsc65l873ny2321f8pmcmb4p5lpxqj4z15illkzz")))

(define-public crate-dbstruct-derive-0.1.1 (c (n "dbstruct-derive") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pgir09pi4hz30mizcv2qqk3n9j7zrq37l6njk0f21xgr9wdynyk")))

(define-public crate-dbstruct-derive-0.2.0 (c (n "dbstruct-derive") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kcagdb0b2rwyjdqy2ym2p7bkqbsa31ql55pka3cy5k55scl4c1r")))

