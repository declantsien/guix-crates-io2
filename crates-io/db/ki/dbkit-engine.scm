(define-module (crates-io db ki dbkit-engine) #:use-module (crates-io))

(define-public crate-dbkit-engine-0.0.1 (c (n "dbkit-engine") (v "0.0.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1ihvrswszl739bgz41z48ddqb935hc2v3zsh5g5ykbds0i3vllbb")))

(define-public crate-dbkit-engine-0.0.2 (c (n "dbkit-engine") (v "0.0.2") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1bg5v0j5y856b5ijs3wkk2ql432ygg8fhh7lm1j0sqb98w62jsxk")))

(define-public crate-dbkit-engine-0.0.3 (c (n "dbkit-engine") (v "0.0.3") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1gc0fc66rdws71zpqdnkq118fkdjwjwvdida0x4a8rxk21axknqh")))

(define-public crate-dbkit-engine-0.0.4 (c (n "dbkit-engine") (v "0.0.4") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "09y1hhvmm2ijaafszs7nl5qp0i2g12q9liqhbvg5ig7abdgbgwhw")))

(define-public crate-dbkit-engine-0.0.5 (c (n "dbkit-engine") (v "0.0.5") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "17x07j8lkzmldjr2im4qwiwvvii310jhm3044zmcd3d40ly3id3a")))

(define-public crate-dbkit-engine-0.0.6 (c (n "dbkit-engine") (v "0.0.6") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0ixix5j4prjw0l1rdwz11qkxykc3x1lwvqwkqr2w9nsizlbvsphg")))

(define-public crate-dbkit-engine-0.0.7 (c (n "dbkit-engine") (v "0.0.7") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0jvzkry6n6nav88cxhy4xi4bx2myipdrzmbviw94rs582rf1ymb1")))

(define-public crate-dbkit-engine-0.0.8 (c (n "dbkit-engine") (v "0.0.8") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1rff2kh7ijry8nd70x9cqaldszrzns6j42sc85chm9v3kg1xkj8i")))

(define-public crate-dbkit-engine-0.0.9 (c (n "dbkit-engine") (v "0.0.9") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "197whg2zn48336602lawdwbhkvnb2br1fiqk0z6z5xv5hg75rpx9")))

