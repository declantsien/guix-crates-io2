(define-module (crates-io db -m db-map) #:use-module (crates-io))

(define-public crate-db-map-0.1.0 (c (n "db-map") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled"))) (d #t) (k 0)))) (h "124n62smsxnr29qmpl20cclkwfq1fdiaggwddkcfvw3bria65js2")))

