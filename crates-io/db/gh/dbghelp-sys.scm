(define-module (crates-io db gh dbghelp-sys) #:use-module (crates-io))

(define-public crate-dbghelp-sys-0.0.1 (c (n "dbghelp-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1igr0c46ml0lp6zj18f5j4ix1vdhgir8n5vjgdvi7ifhjc702f7g")))

(define-public crate-dbghelp-sys-0.1.0 (c (n "dbghelp-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "1w1khnjf72j0k4691d09wr8immw4w34schw3zjvj1rrhd4nwaacc")))

(define-public crate-dbghelp-sys-0.2.0 (c (n "dbghelp-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0l5qqm7pqr4i4lq67ymkix5gvl94m51sj70ng61c52nb7fjhnncp")))

