(define-module (crates-io db -a db-accelerate) #:use-module (crates-io))

(define-public crate-db-accelerate-2.0.0 (c (n "db-accelerate") (v "2.0.0") (d (list (d (n "clap") (r "^2.5") (f (quote ("color" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^1.2") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "06lvjzipcizx7hqn18qavsswcwmmj7c2jz4ga5lnz12i78xw0fdw") (f (quote (("driver-test") ("driver-postgres" "postgres") ("default" "driver-test" "driver-postgres"))))))

