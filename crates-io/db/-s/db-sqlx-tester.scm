(define-module (crates-io db -s db-sqlx-tester) #:use-module (crates-io))

(define-public crate-db-sqlx-tester-0.1.0 (c (n "db-sqlx-tester") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vz0l8xvbl7bz47lwdy6j7j9iajhx96fgxyg7x9dv6bhx0gbshga")))

(define-public crate-db-sqlx-tester-0.1.1 (c (n "db-sqlx-tester") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nq2k0xqjrgzn6q56ms1cym21h5dg1fvk344sn4hp6y9c66pcl5z")))

