(define-module (crates-io db ui dbui-assets) #:use-module (crates-io))

(define-public crate-dbui-assets-0.0.25 (c (n "dbui-assets") (v "0.0.25") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1s8scqz6ym2im0i5mg0r12dxnwaxlr3gfi1cg6jn9fmwsjcc3lk1")))

(define-public crate-dbui-assets-0.0.26 (c (n "dbui-assets") (v "0.0.26") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "018axrd4rfhwkb6fr2d8890pzb8dl4n0klz2saahcr06905p9sdv")))

(define-public crate-dbui-assets-0.0.27 (c (n "dbui-assets") (v "0.0.27") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0vlagn0ss7l4mfwnb6kzjy8cqp2g6jsqhx605qhn2xdjb0bx9irb")))

(define-public crate-dbui-assets-0.0.28 (c (n "dbui-assets") (v "0.0.28") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "04c4vnagzwq9isp7rak3mr8m4s54nszzw492lgnqk8xghgqa1821")))

(define-public crate-dbui-assets-0.0.29 (c (n "dbui-assets") (v "0.0.29") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1lwp397c716lzrj41nhl16i89m3hw3i4gnx9kdn5av2l41lwaf6x")))

(define-public crate-dbui-assets-0.0.32 (c (n "dbui-assets") (v "0.0.32") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0gcm4fbvyklld8gskjc5f02s4fljk6lhrh3lvcbnkwhk82wzrhy3")))

(define-public crate-dbui-assets-0.0.33 (c (n "dbui-assets") (v "0.0.33") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1qwk92anzri3pml8pw4pyk9phkrvpn6g30qhrmzig7xbakx4j460")))

(define-public crate-dbui-assets-0.0.35 (c (n "dbui-assets") (v "0.0.35") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0gk3ki4c0mpk4b3hrrkglfl36qq5hgq8zphq9gilf31mh4dir65j")))

(define-public crate-dbui-assets-0.0.36 (c (n "dbui-assets") (v "0.0.36") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1bmnkznrwklkqvvw1aqcrsqv75k65drg0q7ya0iz6ff8am051fg6")))

(define-public crate-dbui-assets-0.0.37 (c (n "dbui-assets") (v "0.0.37") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0j52wxk68g56k8nqm5519fv0c2c84h370s3gkayzrn16lvi2my79")))

(define-public crate-dbui-assets-0.0.38 (c (n "dbui-assets") (v "0.0.38") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "14b2dds9in7dblnp0hldgckcfryqi9v40sp3ary0a8dpwrd1lbz0")))

(define-public crate-dbui-assets-0.0.39 (c (n "dbui-assets") (v "0.0.39") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1vi04gf9bzqhy2805v52879q2flp6xg228r6ya8amjqyzylabby5")))

(define-public crate-dbui-assets-0.0.40 (c (n "dbui-assets") (v "0.0.40") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1cyi5fa9kfbyl2h4fyg4wx1p86yrizy2rfi206q6xg500zh3j55s")))

(define-public crate-dbui-assets-0.0.41 (c (n "dbui-assets") (v "0.0.41") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1q4kp5bmprpbvxy19b46kanssqy6gj39h18x9acbhw448zg7rfvz")))

(define-public crate-dbui-assets-0.0.42 (c (n "dbui-assets") (v "0.0.42") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1r5z74bq0igzzx0fbffbbrhdj7mlh0yxprhd5limcmcgis18rh4g")))

(define-public crate-dbui-assets-0.0.43 (c (n "dbui-assets") (v "0.0.43") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0rkc8yg4lvl7r2qh0qd9ygj9zm06ifpycypv6sm771cy71733min")))

(define-public crate-dbui-assets-0.0.44 (c (n "dbui-assets") (v "0.0.44") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "09fp6fsbcyy9r6wvvqnz6l6gq2sigq9yy0q6a2dx9zpwgjhi66w2")))

(define-public crate-dbui-assets-0.0.45 (c (n "dbui-assets") (v "0.0.45") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0g02hp5gmj85x99if5ni9xxsk2yabsyspfkqqvr4xincca0rz50v")))

(define-public crate-dbui-assets-0.0.46 (c (n "dbui-assets") (v "0.0.46") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1kiv9pw707n1zzi39ipa3rj5xywc9k9isrvf53y3vzgfnpfcaiiy")))

(define-public crate-dbui-assets-0.0.47 (c (n "dbui-assets") (v "0.0.47") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1f975spz4b6cwy36yz5q2l7ljibd57a25ymg3kjmngdzjgqv8ajq")))

(define-public crate-dbui-assets-0.0.48 (c (n "dbui-assets") (v "0.0.48") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1sgnxlsjflp630006ly4cs6d6ihh48m1ylszfmx3g8smgjj305am")))

(define-public crate-dbui-assets-0.0.49 (c (n "dbui-assets") (v "0.0.49") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1walc1jd3lg1p6zh02d93sm60dzhbbnnw2pacjbm32x2xdkr30x0")))

(define-public crate-dbui-assets-0.0.50 (c (n "dbui-assets") (v "0.0.50") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0hfvmrd346g5jynjl1hvnn3cygbgpc6mbv54f9b3l1bv10nmaa4p")))

(define-public crate-dbui-assets-0.0.51 (c (n "dbui-assets") (v "0.0.51") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1lhjm42x5akdsg212441zby9pxf06y7ajhjfncz83g7x6q11nqmf")))

(define-public crate-dbui-assets-0.0.52 (c (n "dbui-assets") (v "0.0.52") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0habv36v501rsg0qm1xvvdql0zgd6aiy96b6wppknym9lim5c99m")))

(define-public crate-dbui-assets-0.0.53 (c (n "dbui-assets") (v "0.0.53") (d (list (d (n "rust-embed") (r "^5.1") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1b23hdbvfpdy44mhlphhqxgr75zyvqbaa5s0fr74lrjlfsrjb3wq")))

(define-public crate-dbui-assets-0.0.54 (c (n "dbui-assets") (v "0.0.54") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0nmxp4r6ahqli8h9bhci3fpkrlrsvg8x3yp11msjdj115rp4vc1r")))

(define-public crate-dbui-assets-0.0.55 (c (n "dbui-assets") (v "0.0.55") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "050hgdfgqiw4c51agqvfnmi7nh5pb9g9zr4lwp2iq3hlylqmn7yb")))

(define-public crate-dbui-assets-0.0.56 (c (n "dbui-assets") (v "0.0.56") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1z1gpb72fbrl7hbcxpknrzv796l77q7m712ha8j1raqkm56h62b1")))

(define-public crate-dbui-assets-0.0.57 (c (n "dbui-assets") (v "0.0.57") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0k9vrgzf4i49b3pd4jg5wpkvpbn2j9kj9iakns5qvyihz896yagf")))

(define-public crate-dbui-assets-0.0.58 (c (n "dbui-assets") (v "0.0.58") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0qp6nw3a79fam2m5xhb6jjc9kfgfi3h62hvfy2vya21dq0zg26qb")))

(define-public crate-dbui-assets-0.0.59 (c (n "dbui-assets") (v "0.0.59") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "11dbsf990zc2j77qqcyb4imykkhm2cpfxrg26fdi7bm1fp86hdbn")))

(define-public crate-dbui-assets-0.0.60 (c (n "dbui-assets") (v "0.0.60") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1x19q43kxx6vc6nzwic3dba5g1r0fs1mx230s35jy5794fdmg64g")))

(define-public crate-dbui-assets-0.0.61 (c (n "dbui-assets") (v "0.0.61") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0w6i9qyrzrkib0dczrd2nk6a22zpxxrimrhnj89y3bn1444w0nvy")))

(define-public crate-dbui-assets-0.0.62 (c (n "dbui-assets") (v "0.0.62") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "13aa70kslryqyk1n95pxxanx0bg1s9h7fk6slgcc734kraxr5d8y")))

(define-public crate-dbui-assets-0.0.63 (c (n "dbui-assets") (v "0.0.63") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1dl7d2b8ph5r1rn5z7fpanc1gk66sp8j3xgj0alvzn6p6mvgym9j")))

(define-public crate-dbui-assets-0.0.64 (c (n "dbui-assets") (v "0.0.64") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "0b9aq2mcm70bjmzqvyg50mh959baibbfpj5q1c7x47hxdpr0969z")))

