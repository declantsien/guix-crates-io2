(define-module (crates-io db gc dbgcmd) #:use-module (crates-io))

(define-public crate-dbgcmd-0.1.0 (c (n "dbgcmd") (v "0.1.0") (d (list (d (n "itertools") (r "~0.8.0") (d #t) (k 0)))) (h "0w6650h0k2z7pahvyhm88j6n8ljyx88ih75vbpv0lk0vpif4yixi") (f (quote (("force-enabled"))))))

