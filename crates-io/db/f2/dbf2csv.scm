(define-module (crates-io db f2 dbf2csv) #:use-module (crates-io))

(define-public crate-dbf2csv-0.1.0 (c (n "dbf2csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "dbf") (r "^0.1.1") (d #t) (k 0)))) (h "168yc9na5pv3sqh1xi1fag67akdsdvgxgypjmmzdyc3hx3ys1d2g")))

(define-public crate-dbf2csv-0.1.1 (c (n "dbf2csv") (v "0.1.1") (d (list (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "dbf") (r "^0.1.1") (d #t) (k 0)))) (h "1rq12il89fjkf83b0hxbxrh4gf2s7zwcia97hvszp0306pk11yhh")))

