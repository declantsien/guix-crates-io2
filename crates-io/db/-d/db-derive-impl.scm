(define-module (crates-io db -d db-derive-impl) #:use-module (crates-io))

(define-public crate-db-derive-impl-0.1.0 (c (n "db-derive-impl") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09nndxpx96zi0klp448h48lbk2g2z8w9pvv4qzp1fzzrhngyz37n")))

(define-public crate-db-derive-impl-0.1.1 (c (n "db-derive-impl") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zhfq3j08w8ci3zvxqsj5hywvxj8p2ndswqclxyzyh93y5ih4a8c")))

(define-public crate-db-derive-impl-0.1.2 (c (n "db-derive-impl") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17bhwnv2cihymfcfma7lyxp4p01nivz56fadshps6v03pymh2ff6")))

(define-public crate-db-derive-impl-0.1.3 (c (n "db-derive-impl") (v "0.1.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "109w9nqrgd93zskwr1sq2phrjmk7yznvpqrjqxb0cym2dpscdpz2")))

(define-public crate-db-derive-impl-0.1.4 (c (n "db-derive-impl") (v "0.1.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "126n8vfdlgcrddrbv6xy6mi6qrszhrnn8008j9v28jbzdvhfhk11")))

(define-public crate-db-derive-impl-0.1.5 (c (n "db-derive-impl") (v "0.1.5") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1czpjx5wgk7kb78y2spl38c6ppf6scfhwd102bx4fyvq8ayg24bf")))

(define-public crate-db-derive-impl-0.1.6 (c (n "db-derive-impl") (v "0.1.6") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zd5hc92p9w169q6mlbwqg73hpd5ck46lqkzps6xflh0r654p8h5") (f (quote (("sqlite") ("postgresql") ("default" "postgresql" "sqlite"))))))

(define-public crate-db-derive-impl-0.1.7 (c (n "db-derive-impl") (v "0.1.7") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rf25614yp4j6r8zzm2z1s65k834573z38xq1h2xp3rx36zc9q6f") (f (quote (("sqlite") ("postgresql") ("default" "postgresql" "sqlite"))))))

(define-public crate-db-derive-impl-0.1.8 (c (n "db-derive-impl") (v "0.1.8") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pbpacq7xa6vgz4zg3yglzamp9nc2h08ddiq3bvwz8f7sfg0i0f3") (f (quote (("sqlite") ("postgresql") ("default" "postgresql" "sqlite"))))))

