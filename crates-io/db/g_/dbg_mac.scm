(define-module (crates-io db g_ dbg_mac) #:use-module (crates-io))

(define-public crate-dbg_mac-0.1.0 (c (n "dbg_mac") (v "0.1.0") (h "1wi3vvzffjqj2bs0m2yj7lhc43wz25fsr1284wfwzjv6m37lgzkm")))

(define-public crate-dbg_mac-0.1.1 (c (n "dbg_mac") (v "0.1.1") (h "133ncpa5wp6f6fd0lbxd8w5igkjfr6jz0ckl3ywpx29p7bap1nlv")))

(define-public crate-dbg_mac-0.1.2 (c (n "dbg_mac") (v "0.1.2") (h "0vdi7hc8s50h91z85vp0vi5myva8cx31jk8cwa7fis7i7lvhr1bv")))

