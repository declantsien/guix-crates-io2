(define-module (crates-io db g_ dbg_as_curl) #:use-module (crates-io))

(define-public crate-dbg_as_curl-1.0.0 (c (n "dbg_as_curl") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (k 0)))) (h "075xhhr48b9cfad9k5w0bqj9iml67wwa194fawxby1d05z6j2mj9")))

