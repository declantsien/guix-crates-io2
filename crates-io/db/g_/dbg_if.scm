(define-module (crates-io db g_ dbg_if) #:use-module (crates-io))

(define-public crate-dbg_if-0.1.0 (c (n "dbg_if") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "atomic_float") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1zafph53m93l6cdrj570yv3rxjl12yg3dzglvclpf5miv4z0fha0") (s 2) (e (quote (("float" "dep:approx" "dep:atomic_float"))))))

