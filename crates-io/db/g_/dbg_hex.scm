(define-module (crates-io db g_ dbg_hex) #:use-module (crates-io))

(define-public crate-dbg_hex-0.1.0 (c (n "dbg_hex") (v "0.1.0") (h "0b4b1bs02awl2dhz0zna0nixm1paj567il9cpbx74gay9869hgr1")))

(define-public crate-dbg_hex-0.1.1 (c (n "dbg_hex") (v "0.1.1") (h "0q1xvbdczjp2rqa8q6h1hinnv73cqrlmhmlbak8c9nz48knsnx1g")))

(define-public crate-dbg_hex-0.2.0 (c (n "dbg_hex") (v "0.2.0") (h "1vl4i1zblc0zly934aqn3r5q6adn5b8kfpkzxrjmsaw7sjd0dprm") (r "1.39.0")))

