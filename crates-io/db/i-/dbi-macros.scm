(define-module (crates-io db i- dbi-macros) #:use-module (crates-io))

(define-public crate-dbi-macros-0.1.0 (c (n "dbi-macros") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dzrl4ivz7sg5dksijmk4805c79k6pb8vfa2cfv0lwbx7v6bahk2")))

(define-public crate-dbi-macros-0.2.0 (c (n "dbi-macros") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h9g3z23db9kpabs3081ksm6ajkbbj7gw5qc8sq3m467izm7gva8") (f (quote (("mysql"))))))

(define-public crate-dbi-macros-0.2.1 (c (n "dbi-macros") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hq5ijz1jxr7saskagp2fvc5pcjl2qr60xx0xlz382wlhyyfm1rc") (f (quote (("mysql"))))))

(define-public crate-dbi-macros-0.2.2 (c (n "dbi-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13k3aqsbdfkby2dgf4mdzimdvpm9081r5ckb1kp2knmy8w76y0cq") (f (quote (("mysql"))))))

(define-public crate-dbi-macros-0.2.3 (c (n "dbi-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m445cban0j74n1lyk7lmhhdd95vffx9rhyh4mcbmbsgvml1pda2") (f (quote (("mysql"))))))

(define-public crate-dbi-macros-0.2.4 (c (n "dbi-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11flbl4hy8g7sjbf2wp4y1avqiaayn3anmn07ax4hw3s2bj688cs") (f (quote (("mysql"))))))

(define-public crate-dbi-macros-0.3.0 (c (n "dbi-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bsh6k2336vsi1kssnmc64rvv5hcf8jvbvci42ghhvp471lmqbja") (f (quote (("mysql"))))))

