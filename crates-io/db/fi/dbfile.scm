(define-module (crates-io db fi dbfile) #:use-module (crates-io))

(define-public crate-dbfile-0.1.0 (c (n "dbfile") (v "0.1.0") (h "1r7mkbm3kxh0qvjl9aq868vs2if653610xx0bkfayqc1cp9yp5xz") (y #t)))

(define-public crate-dbfile-0.1.1 (c (n "dbfile") (v "0.1.1") (h "01d5q5azcdsn9vsn39d6vqr5dranqn6s3vb9rcrzkxfir85a7vdb")))

(define-public crate-dbfile-0.1.3 (c (n "dbfile") (v "0.1.3") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)))) (h "0r2zs4ydr25k32yhs704dcpfjav91iqz8d5a2sfjbcq16jnc1y26")))

(define-public crate-dbfile-0.1.4 (c (n "dbfile") (v "0.1.4") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)))) (h "18bl8srl99l0vzf4yf6wf5rk4ysh2d1xyp1m04q8brhlwjm6k1r9")))

(define-public crate-dbfile-0.1.5 (c (n "dbfile") (v "0.1.5") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)))) (h "1l6c5hhpf451n7b8qlizpfw0vs058f35y6nkgx8m2z7wf3xrf8rm")))

(define-public crate-dbfile-0.1.50 (c (n "dbfile") (v "0.1.50") (h "1yvvd9s3l18jsgmaswr145jmqzmh03f365vbp7i02chnzf6s1hld")))

(define-public crate-dbfile-0.1.51 (c (n "dbfile") (v "0.1.51") (h "0r80prcrqvg9bvrw3mx80ci3qcn0bad23x6rchc844xzpv892pg2")))

