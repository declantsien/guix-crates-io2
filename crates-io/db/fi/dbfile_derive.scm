(define-module (crates-io db fi dbfile_derive) #:use-module (crates-io))

(define-public crate-dbfile_derive-0.1.1 (c (n "dbfile_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nlzy0qdav1brbgy7frrl505l1wzb99mb7ysc607iq8px5350qcm")))

(define-public crate-dbfile_derive-0.1.3 (c (n "dbfile_derive") (v "0.1.3") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bxaxfffzlblpf49fzhgmy6cf8sm7csgx3cal6kx89w2swq35cnn")))

(define-public crate-dbfile_derive-0.1.4 (c (n "dbfile_derive") (v "0.1.4") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14vl15iqpyp1nq9nb6fhmczns6pnp0yiq8k5zgaiknirij01anq9")))

(define-public crate-dbfile_derive-0.1.5 (c (n "dbfile_derive") (v "0.1.5") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdgymyg2xms5k5y4dkw62q8fbnkscfrv2yvrajkd49axc7dqp7a")))

(define-public crate-dbfile_derive-0.1.50 (c (n "dbfile_derive") (v "0.1.50") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0agqlir7vxz042wjgnv9php4v7hhspnf42m70f2awzvrcfif4fc6")))

(define-public crate-dbfile_derive-0.1.51 (c (n "dbfile_derive") (v "0.1.51") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10xdnf2ad1d3626n2vrrbk5q3mwaandz0r26adfnlv4gh61j0pmi")))

