(define-module (crates-io db _i db_ip_macros) #:use-module (crates-io))

(define-public crate-db_ip_macros-0.1.0 (c (n "db_ip_macros") (v "0.1.0") (d (list (d (n "locale-codes") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pg2cpsp64wccw8j40d11qrmnzc1fvhmaybdng06g80cyb728xva") (f (quote (("region"))))))

(define-public crate-db_ip_macros-0.2.0 (c (n "db_ip_macros") (v "0.2.0") (d (list (d (n "locale-codes") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cjy9hjblm7540m5rgh38nr9r72j3fiixa8awz2ry1m3pwd1izfq") (f (quote (("region"))))))

(define-public crate-db_ip_macros-0.3.0 (c (n "db_ip_macros") (v "0.3.0") (d (list (d (n "locale-codes") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03v3zmdadz0q4pmj62n7w1crp84hylfl2sr9ni2dlkzjfaz70dm9") (f (quote (("region"))))))

