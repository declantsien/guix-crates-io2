(define-module (crates-io db ut dbutils) #:use-module (crates-io))

(define-public crate-dbutils-0.2.0 (c (n "dbutils") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4") (k 0)))) (h "1qdq1chlgshd15jixm400p5cy3fy595mkm1jqj7yyq7agpfdkv37") (f (quote (("std") ("default" "std"))))))

(define-public crate-dbutils-0.2.1 (c (n "dbutils") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4") (k 0)))) (h "105ghk7rssiy3p0d6sn1441dzaj6i0kcnydq3rcv54c557fg7s4g") (f (quote (("std") ("default" "std"))))))

