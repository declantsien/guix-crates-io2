(define-module (crates-io db sd dbsdk-rs) #:use-module (crates-io))

(define-public crate-dbsdk-rs-0.1.0 (c (n "dbsdk-rs") (v "0.1.0") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "0yqldaydc225g544g68xlyrnylv1vgrjv1adjvszirknqd1pajs9")))

(define-public crate-dbsdk-rs-0.1.1 (c (n "dbsdk-rs") (v "0.1.1") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "1zqvfhw5msn71z7h734vsgscxj95szwwxkd99qls9dvblrj18x8x")))

(define-public crate-dbsdk-rs-0.1.2 (c (n "dbsdk-rs") (v "0.1.2") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "1937qnd6527fb8ys6lhcmss0lh6ykr79p0kfca4mrbc1gg6b89vk")))

(define-public crate-dbsdk-rs-0.1.3 (c (n "dbsdk-rs") (v "0.1.3") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "01qrkd8r9cvp3jkija68lcg7m6j5vq4k86hazrld9wb9blbra807")))

(define-public crate-dbsdk-rs-0.1.4 (c (n "dbsdk-rs") (v "0.1.4") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "19qn75m15xl15ngj14xq4jhfyrjj383bc826i4a3cqpvjl1hm9wq")))

(define-public crate-dbsdk-rs-0.1.6 (c (n "dbsdk-rs") (v "0.1.6") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "0vnfm9fxd4w3y8rmcgzlv9ahqkv45fbyk08axv0ln1djaip2yam8")))

(define-public crate-dbsdk-rs-0.1.7 (c (n "dbsdk-rs") (v "0.1.7") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "17syp14j9iarxkpzdpiww6hxblrga43vjw7x50p8z335pyacbk2x")))

(define-public crate-dbsdk-rs-0.1.8 (c (n "dbsdk-rs") (v "0.1.8") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "0lrgszpkclhlvzyrxp3qw7q0h0jk3i2k3miadq0rzq5f5ghvbjk3")))

(define-public crate-dbsdk-rs-0.1.9 (c (n "dbsdk-rs") (v "0.1.9") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "1prxpalvxazzlnq7lb9n39xkf1lk50vsjg6gjqjbc4xs0qkyz09h")))

(define-public crate-dbsdk-rs-0.1.10 (c (n "dbsdk-rs") (v "0.1.10") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "1ilz5bpsqpnv50nl3kj29x6hd7wz07fspghlyb3n4b2frvd7wx3s")))

(define-public crate-dbsdk-rs-0.1.11 (c (n "dbsdk-rs") (v "0.1.11") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "0caf6bg66g1fvgy5jm7c8y7swgykr3vmamza4jzr8qidiz0k5cfm")))

(define-public crate-dbsdk-rs-0.1.12 (c (n "dbsdk-rs") (v "0.1.12") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "0l9pkcahxlimrz1addl48zvrvrp9x66blz7a343zhvi2hbwh36jl")))

(define-public crate-dbsdk-rs-0.1.13 (c (n "dbsdk-rs") (v "0.1.13") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)))) (h "1bch6whdcqyyzibr1932hcrjms1sf1i0m8lg1f0v2dqc49d9lmj0")))

