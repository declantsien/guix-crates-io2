(define-module (crates-io db sd dbsdk-cli) #:use-module (crates-io))

(define-public crate-dbsdk-cli-0.1.0 (c (n "dbsdk-cli") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.11.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0sdz8gns3fjcdnh6kgwfj817as1v10nd5wmhby38yxyf595s8v8w")))

(define-public crate-dbsdk-cli-0.1.1 (c (n "dbsdk-cli") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.11.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0sfd9z0wqfqpk9ssiayq6lgc5kl1ln4pz9ikl896dna4d6lf8xaq")))

(define-public crate-dbsdk-cli-0.1.2 (c (n "dbsdk-cli") (v "0.1.2") (d (list (d (n "cargo_toml") (r "^0.11.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "14jcdiyqw2f3yc77kg69n72ycyyjs0np7a7h5a7wykwlaghfkqxr")))

