(define-module (crates-io db c- dbc-codegen-cli) #:use-module (crates-io))

(define-public crate-dbc-codegen-cli-0.1.0 (c (n "dbc-codegen-cli") (v "0.1.0") (d (list (d (n "dbc-codegen") (r "^0.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1w4zk0v2p4p15ybnvnkpcmcc4g5j9z8ylad5gp0my5z0qnm8d8xv")))

(define-public crate-dbc-codegen-cli-0.2.0 (c (n "dbc-codegen-cli") (v "0.2.0") (d (list (d (n "dbc-codegen") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0d2ly5afijp16vj3wzjw7hbhxw4m2qjb5jny29qkafwk5hcnnb1l")))

(define-public crate-dbc-codegen-cli-0.3.0 (c (n "dbc-codegen-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbc-codegen") (r "^0.3") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)))) (h "13nzn8s18vd2lvffn82ad2wvha2a5r4m01vd0l9wj7bgaay8kmba")))

