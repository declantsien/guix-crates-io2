(define-module (crates-io db c- dbc-codegen) #:use-module (crates-io))

(define-public crate-dbc-codegen-0.1.0 (c (n "dbc-codegen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "can-dbc") (r "^3.0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)))) (h "16pyjdbykcrdjcs2cx5iz1a0wcq3nrisj3v61bbk3qb9ma19z6i0")))

(define-public crate-dbc-codegen-0.2.0 (c (n "dbc-codegen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "can-dbc") (r "^3.0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)))) (h "0mppiqmq9vb6wgkwbs9a7acl8h00ijh7xi8xqy8x71rp39qfr63g") (f (quote (("std"))))))

(define-public crate-dbc-codegen-0.3.0 (c (n "dbc-codegen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "can-dbc") (r "^5.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)))) (h "1z3b13la8zc453c262rblnr3cr8qdaxgf156aqmk2pw9b76fy1nb") (f (quote (("std")))) (r "1.73.0")))

