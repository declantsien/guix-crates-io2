(define-module (crates-io db z- dbz-cli) #:use-module (crates-io))

(define-public crate-dbz-cli-0.2.1 (c (n "dbz-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbz-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0bbqx7bzsy5y3vcp60ikcmbwxs8klwd2bqfs850wfwacij7qzhjk")))

(define-public crate-dbz-cli-0.2.2 (c (n "dbz-cli") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbz-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1x15vwjm3ivhdsdixijy333xyvxyrn5z1fidj3vcyg6ak3j6w3kd")))

