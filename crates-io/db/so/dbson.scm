(define-module (crates-io db so dbson) #:use-module (crates-io))

(define-public crate-dbson-0.0.1 (c (n "dbson") (v "0.0.1") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (o #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "sqlx") (r "^0.7.2") (o #t) (k 0)))) (h "1pyq2rmh4vlr721hm04wi8bhblhhvwwmq57z6jgqngb9xkzylwg0") (s 2) (e (quote (("sqlx" "dep:sqlx") ("rusqlite" "dep:rusqlite"))))))

