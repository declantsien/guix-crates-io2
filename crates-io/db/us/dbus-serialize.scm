(define-module (crates-io db us dbus-serialize) #:use-module (crates-io))

(define-public crate-dbus-serialize-0.1.0 (c (n "dbus-serialize") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mdl5n4wcagyaxk9b6lqh2qcl72gg83sn15ikws93xcwlnpyz37g")))

(define-public crate-dbus-serialize-0.1.1 (c (n "dbus-serialize") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gp74kz4v2khavg8y6h1zws55gd880qjjjcrj9xvs812si1103ci")))

(define-public crate-dbus-serialize-0.1.2 (c (n "dbus-serialize") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "058dkk4ji5jd07x143m3p485y4mkq8jxsjglva6nbwqj3rkxh771")))

