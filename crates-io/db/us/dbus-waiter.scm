(define-module (crates-io db us dbus-waiter) #:use-module (crates-io))

(define-public crate-dbus-waiter-0.2.0 (c (n "dbus-waiter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "std" "help" "usage" "error-context"))) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros" "time"))) (k 0)) (d (n "zbus") (r "^3.14.1") (f (quote ("tokio"))) (k 0)))) (h "1s8j23kp2hln4msbnyr9g1kbkczyxqw0l6aicq1hbkf9k2sj3509")))

