(define-module (crates-io db us dbusmenu-glib-sys) #:use-module (crates-io))

(define-public crate-dbusmenu-glib-sys-0.1.0 (c (n "dbusmenu-glib-sys") (v "0.1.0") (d (list (d (n "glib") (r ">=0.15") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r ">=0.15") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1synl6ix79a5bgihywd70zdl1n0rmjbwjlxr891wj6076d0fvybz") (f (quote (("dox" "glib/dox" "gobject/dox")))) (l "dbusmenu-glib")))

