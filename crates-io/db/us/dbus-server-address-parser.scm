(define-module (crates-io db us dbus-server-address-parser) #:use-module (crates-io))

(define-public crate-dbus-server-address-parser-1.0.0 (c (n "dbus-server-address-parser") (v "1.0.0") (d (list (d (n "thiserror") (r "~1.0.26") (d #t) (k 0)))) (h "120rrap852w57nfqbv2qr7l1zw1j7ldbqvsbwp27igyqnn1wajg1")))

(define-public crate-dbus-server-address-parser-1.0.1 (c (n "dbus-server-address-parser") (v "1.0.1") (d (list (d (n "thiserror") (r "~1.0.26") (d #t) (k 0)))) (h "1i59zwzgn352f5gksna2jiz8w7yl0vlyxz29kff7a4drvzz19fl7")))

