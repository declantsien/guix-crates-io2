(define-module (crates-io db us dbus) #:use-module (crates-io))

(define-public crate-dbus-0.0.5 (c (n "dbus") (v "0.0.5") (h "10i52x41hhvqj3fys05r8ysvw8j0rpbz8c9ddb6z53vb6rawrabl")))

(define-public crate-dbus-0.0.6 (c (n "dbus") (v "0.0.6") (h "1i83nr39srdb7jfx3aq2dhay2g8g0nnd1vm0j3pimv30f0wbrbr0")))

(define-public crate-dbus-0.0.7 (c (n "dbus") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "11vk768n9jr5wr6ddr26zkvzjp6wyrip4sxw4643wfxwwia48489")))

(define-public crate-dbus-0.0.8 (c (n "dbus") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0g4480z3swm0a94iybvwb3s563xk2mp68ysbpfbsraabhmmaalpl")))

(define-public crate-dbus-0.0.9 (c (n "dbus") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "01fiivljnxk5yaiqcxq8ngawq0wpwyxf8m9c6g4yd81ckxli64iq")))

(define-public crate-dbus-0.1.0 (c (n "dbus") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "156lkdz694v7i7xild0m5q0z3vlkbrra30bkj49i7bfspd82mbhl")))

(define-public crate-dbus-0.1.1 (c (n "dbus") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "12d0yq58hh7pj09p3dzfi4vlfv56mwsyrhjs1s2kgyy9m35qbzn9")))

(define-public crate-dbus-0.1.2 (c (n "dbus") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "03svy2csz008a0zddhgr7bd17dydy3khzmq9d596zkdhr28zpqdk")))

(define-public crate-dbus-0.2.0 (c (n "dbus") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "0b9g3ahx6b0n3ns6a91qcf2i81sdzvgpvyg0i2y1718jh6rr32kg")))

(define-public crate-dbus-0.2.1 (c (n "dbus") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1gricdi3yri6j3yid819rfanj387vwps28x521lqv4vzfg7wc86h")))

(define-public crate-dbus-0.2.2 (c (n "dbus") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "05ji0m6hwrpx0l92bhvn1lj4w06xn9cl8mj0afrkqs5ydmhdj0zv")))

(define-public crate-dbus-0.2.3 (c (n "dbus") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0b85dl7y396g8xh1xh89wxnb1fvvf840dar9axavfhhhlq7c385l")))

(define-public crate-dbus-0.3.0 (c (n "dbus") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0g5ychrln7z2y3ydhkgfajkxrkx2a8ac2a98myd4j40gai8ln1qj")))

(define-public crate-dbus-0.3.1 (c (n "dbus") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "159ydqbnb7v77wkff2g2xkyx9v6ljz6s1j41cc8xv5p5gflifjqg")))

(define-public crate-dbus-0.3.2 (c (n "dbus") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vif0wpra988ydypmm5j7pry6kxhqdbrkkcry8m6s8s715p1rdxb")))

(define-public crate-dbus-0.3.3 (c (n "dbus") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1kzfg2lw65i8lnpyigw5i9hwhl3gjk54j9c2xdc37i8v0gcwdzmf")))

(define-public crate-dbus-0.3.4 (c (n "dbus") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1brycwic9k890llj8j5mdamkasb8lclw90r0s188pxmafal6dlll")))

(define-public crate-dbus-0.4.0 (c (n "dbus") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1k4fcncsy62mbdi8lm62z9ch9n1kl7prma07qbvscs34a72gxpq7")))

(define-public crate-dbus-0.4.1 (c (n "dbus") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0jqbs7vnaq9s7bh4wjczi575mz1mp6wzv76dy5mg6ybgmi67pv2q")))

(define-public crate-dbus-0.5.0 (c (n "dbus") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qld8n9f18q78j68mx6hrjxci1rbw8214nz27x9bsg4kpfgnvy0n")))

(define-public crate-dbus-0.5.1 (c (n "dbus") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1346595l63s0542hakcpspj3wcjz8d99wajkl5qwkn7ighv013cw") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.5.2 (c (n "dbus") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0l74df7av35ylxpq0ccn807nxijasm0zdy8zphkiqh1432bq3y27") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.5.3 (c (n "dbus") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0m6238cs22a9ljna573k22l0hwxz9rkacbk4rbkyb8xdfvxh3vja") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.5.4 (c (n "dbus") (v "0.5.4") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "03mba7r98y4ywp6xb9m0cczyg01py50c1czwg3hz0pxdbza29n5c") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.0 (c (n "dbus") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1x8qa5aj6cizyjrhz3h1zg3d3ls4faqn250wz1xjkp2k8dq5arvr") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.1 (c (n "dbus") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "04475l0l1j9rhrl5pdhym6bb801g2av3rq76f5w67mhdnam5hb2v") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.2 (c (n "dbus") (v "0.6.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "022hifv2hrqns6yqjkiadsacz3lgrl1i8c5d8qfqixdkvwwc4d1y") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.3 (c (n "dbus") (v "0.6.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0fbnfldjj276qpvvdcpa07rihfgj3fdbh46ldhdfvp52b8bmm5rx") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.4 (c (n "dbus") (v "0.6.4") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ms7rvw7rajchy88c2kljfyng4cy3sghykrcaahx98va7ygv7qdr") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.6.5 (c (n "dbus") (v "0.6.5") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "068qyxvaam34sjmhjgxz6iikklvylxly7gp6n00yksqydzrz1da8") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.7.0 (c (n "dbus") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zwzinmqhic4pjhb0dir1ssc7ccclq4bmzkp9bc68k5qvafxyfpq") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.7.1 (c (n "dbus") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03lmxzab4kyszzhh45y36wmxac72scbf57zb4110hjnksqvdvn5d") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.8.0 (c (n "dbus") (v "0.8.0") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "17za29psqlwy8c772saw183wnag3lgdz1kwlvf64xv4m27h8dq40") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.8.1 (c (n "dbus") (v "0.8.1") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nchchw80nkhiw2x8y4yf453lkgar0s617l61za7sxnv382l7l1g") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.8.2 (c (n "dbus") (v "0.8.2") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05n66xj6lcjh569x54l5n1rddjrxmwc9xc1dq7g21g5gnxdqgy1q") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.8.3 (c (n "dbus") (v "0.8.3") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1f53bradsb7c7k0kg8b60cs3wfj95706y04vi0anbwmgzw97mc96") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.8.4 (c (n "dbus") (v "0.8.4") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0cngaaicqmg27vj7h1qhfgyr2l7x28sb0zc91y3a2ih1466fgnaw") (f (quote (("no-string-validation"))))))

(define-public crate-dbus-0.9.0 (c (n "dbus") (v "0.9.0") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "156hfn9lryp0wjbj67qlwqlmbjsdhpx8iyca9p0l1jbhxggqmh12") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.1 (c (n "dbus") (v "0.9.1") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18zlahncyiwlgw5wvic6r54v0590ss4b30dc779crpqx2v0384rv") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.2 (c (n "dbus") (v "0.9.2") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ny01n0gzfdmcy5ydn4q78pamidj4c5q9ixz7gr97dbrza6y15zm") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.3 (c (n "dbus") (v "0.9.3") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.0") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f9hsh7cma2w6dz0h4f466lql0xkfdf8fb7xbgds5cm31asjp1n8") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.4 (c (n "dbus") (v "0.9.4") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.0") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11r4gdj6353c320vy2jqlx3vb1lr7crvqf3ca99q4wfkipw2bck0") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.5 (c (n "dbus") (v "0.9.5") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.0") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kn2v0w0q2q608xwl42c1yb3m2f7zvsm0f9ap1balb5k4mf782ny") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.6 (c (n "dbus") (v "0.9.6") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.0") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dlf2jzf7sjqz437aj9ksj885nzm5685m72jdb94wp1fdpawv2vg") (f (quote (("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

(define-public crate-dbus-0.9.7 (c (n "dbus") (v "0.9.7") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libdbus-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.0") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06vdv4aarjs4w6byg9nqajr67c8qvlhk3153ic2i65pvp63ikchv") (f (quote (("vendored" "libdbus-sys/vendored") ("stdfd") ("no-string-validation") ("futures" "futures-util" "futures-channel"))))))

