(define-module (crates-io db us dbus-tree) #:use-module (crates-io))

(define-public crate-dbus-tree-0.9.0 (c (n "dbus-tree") (v "0.9.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "0qrsx403nxmba7ssn5p9r16r12mf9kx1fkhj63cp3mclnwww3896")))

(define-public crate-dbus-tree-0.9.1 (c (n "dbus-tree") (v "0.9.1") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "1l2gi8073cwvv5vxlg5lafw6sppyhgm88hhpq7hak424x4kifpmi")))

(define-public crate-dbus-tree-0.9.2 (c (n "dbus-tree") (v "0.9.2") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "027mnjc99ss8nab588aga9l8aag2pjvzkcfx35g5fm4fmscfcmpl")))

