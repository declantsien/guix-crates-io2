(define-module (crates-io db us dbusmenu-glib) #:use-module (crates-io))

(define-public crate-dbusmenu-glib-0.1.0 (c (n "dbusmenu-glib") (v "0.1.0") (d (list (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "dbusmenu-glib-sys")) (d (n "glib") (r ">=0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "155q311s8zn830x06d7w7sk950xqs5w7xw5rirkf0xaprkf2j5px") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

