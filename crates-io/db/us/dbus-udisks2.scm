(define-module (crates-io db us dbus-udisks2) #:use-module (crates-io))

(define-public crate-dbus-udisks2-0.1.0 (c (n "dbus-udisks2") (v "0.1.0") (d (list (d (n "dbus") (r "^0.6.2") (d #t) (k 0)))) (h "1mxdz3h45hb9cdfx2ahqkn4j40jh6qc1qwgvxndri7q7fnzz52bp")))

(define-public crate-dbus-udisks2-0.2.0 (c (n "dbus-udisks2") (v "0.2.0") (d (list (d (n "dbus") (r "= 0.6.2") (d #t) (k 0)))) (h "00k7y9ncxrjlgjlpa3cxs6q4zqfr931b5pm5690hvxccn7qbqbkd")))

(define-public crate-dbus-udisks2-0.3.0 (c (n "dbus-udisks2") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.7.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("async-await"))) (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("async-await"))) (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1qkpgl2rkz3h4c81zydmaha9hrnp19casqwfp57ii2phx0dnybcn") (f (quote (("futures" "dbus/futures" "futures-util"))))))

