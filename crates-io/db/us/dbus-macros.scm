(define-module (crates-io db us dbus-macros) #:use-module (crates-io))

(define-public crate-dbus-macros-0.0.1 (c (n "dbus-macros") (v "0.0.1") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "0q70c0kjwfj5gwm2wir82z8k8whgxxkx5r45rbhfn1n19ndq4nbc") (y #t)))

(define-public crate-dbus-macros-0.0.2 (c (n "dbus-macros") (v "0.0.2") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "097ai67m5p3jgm9lz3a0g23vy4f3ya6djjkl4f60g68kfhmc81qw") (y #t)))

(define-public crate-dbus-macros-0.0.3 (c (n "dbus-macros") (v "0.0.3") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "0zvyf5d493v14lp5ziygcsm9v920589ay7dwkq16m7j0k94ms3r4") (y #t)))

(define-public crate-dbus-macros-0.0.4 (c (n "dbus-macros") (v "0.0.4") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "1w2al46nlvklj2m1jg9yn392lm37g92g6805lffkvqi205nyw6xm") (y #t)))

(define-public crate-dbus-macros-0.0.5 (c (n "dbus-macros") (v "0.0.5") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "1d6nx8a5nypk8kn5qphny8i8c34ykb78gzd60sffij2b7vvki6nh") (y #t)))

(define-public crate-dbus-macros-0.0.6 (c (n "dbus-macros") (v "0.0.6") (d (list (d (n "dbus") (r "^0.4") (d #t) (k 0)))) (h "1srbnjdpbabqvw1rpvgy0d7pf35h2i8nkwqnq3jp4x658nwi7q71") (y #t)))

(define-public crate-dbus-macros-0.1.0 (c (n "dbus-macros") (v "0.1.0") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "18hq1750xg29x795hj3ph6kn9xyg4bm2zabg51zkkz4dnc0y40g6") (y #t)))

(define-public crate-dbus-macros-0.2.0 (c (n "dbus-macros") (v "0.2.0") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "0rygm49cjcz2wzgnj1alkjcr27n6ajr6wrdarxixj6rigcr6gys3") (y #t)))

(define-public crate-dbus-macros-0.2.3 (c (n "dbus-macros") (v "0.2.3") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "0c27b99qfz98k9c7x0yxw3sld6bawg95ksj5r0lwg0kka5zhgizg") (y #t)))

(define-public crate-dbus-macros-0.2.4 (c (n "dbus-macros") (v "0.2.4") (d (list (d (n "dbus") (r "^0.5") (d #t) (k 0)))) (h "0nn2p8hdigas2d5pyxhjxifdfcqbkcv267indlkqpk1bwpjylrki") (y #t)))

