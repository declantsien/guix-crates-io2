(define-module (crates-io db us dbus-addr) #:use-module (crates-io))

(define-public crate-dbus-addr-0.1.0 (c (n "dbus-addr") (v "0.1.0") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("user"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hsilynwjchawnjmkb3g6xr0047dqkw50bgia7gqzzxh3kn4lyx6") (f (quote (("vsock"))))))

