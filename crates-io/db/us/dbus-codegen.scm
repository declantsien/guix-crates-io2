(define-module (crates-io db us dbus-codegen) #:use-module (crates-io))

(define-public crate-dbus-codegen-0.2.0 (c (n "dbus-codegen") (v "0.2.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1p2869al56k92v41k8b8akms6k35xnwlrq5pjny2gzca1yw17fjc")))

(define-public crate-dbus-codegen-0.2.1 (c (n "dbus-codegen") (v "0.2.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0hyflsan67nkr9z9sl5nwfixbawsh9vc0gq6sgniccyxryhx8crb")))

(define-public crate-dbus-codegen-0.2.2 (c (n "dbus-codegen") (v "0.2.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "05yvgksqyh4zz3gabv3av57dcsi63hjh4kr6ikhpn52x90p54j22")))

(define-public crate-dbus-codegen-0.3.0 (c (n "dbus-codegen") (v "0.3.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1lpd66206rshyfj85xy3gvwpks58ya27baqiwvrq3zgk976m0c8g")))

(define-public crate-dbus-codegen-0.4.0 (c (n "dbus-codegen") (v "0.4.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "09mp44mydkp0cn4cl1pr78bp5acm1q99y0rv766qsc1scd1zbq26")))

(define-public crate-dbus-codegen-0.4.1 (c (n "dbus-codegen") (v "0.4.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1snsvf7mz79lsds5avrjc9b472xq61mmicrds17y19636wrglc5f")))

(define-public crate-dbus-codegen-0.5.0 (c (n "dbus-codegen") (v "0.5.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "08dpd1iz13wsvpcy6mzmqn2ighrmmmfbrhkbxrn7mcq5iqlifmg5")))

(define-public crate-dbus-codegen-0.9.0 (c (n "dbus-codegen") (v "0.9.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "15gjfszg052qn31hmb5jp734n22lqgacrqa7q9kafrkb5nyabb3m") (f (quote (("default" "dbus"))))))

(define-public crate-dbus-codegen-0.9.1 (c (n "dbus-codegen") (v "0.9.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0z3150dfkkg18izvi68j56inqpps5v25sq2n3n2d8wp8zgysk7d4") (f (quote (("default" "dbus"))))))

(define-public crate-dbus-codegen-0.10.0 (c (n "dbus-codegen") (v "0.10.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1c2ghwz6vcq1gb4pj8fw0kappcjpqxkbxymli6h9ykiyx1fc6vd7") (f (quote (("default" "dbus"))))))

(define-public crate-dbus-codegen-0.11.0 (c (n "dbus-codegen") (v "0.11.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "04n8vpk9z0dc06cnh1i7707v7h5sg20c19vaa9ykrj0zv5signdw") (f (quote (("default" "dbus"))))))

(define-public crate-dbus-codegen-0.12.0 (c (n "dbus-codegen") (v "0.12.0") (d (list (d (n "clap") (r "^4.5") (o #t) (d #t) (k 0)) (d (n "dbus") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "dbus-tree") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1nx1fbfrdy7sl0iyl4ybsdk7siz5qn36m65dnw5ypli0w1w8qyyg") (f (quote (("default" "dbus" "clap"))))))

