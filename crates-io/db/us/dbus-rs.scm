(define-module (crates-io db us dbus-rs) #:use-module (crates-io))

(define-public crate-dbus-rs-0.0.1 (c (n "dbus-rs") (v "0.0.1") (h "1xv8a3s4ahbb956s4l3bn0x6vckyi33f59ymsjkjmslgp4wbard3") (y #t)))

(define-public crate-dbus-rs-0.0.2 (c (n "dbus-rs") (v "0.0.2") (h "1h5nc0gihkmbip54y0n7z50mc40h74x158s3dwsgwxm657j70yi1") (y #t)))

(define-public crate-dbus-rs-0.0.3 (c (n "dbus-rs") (v "0.0.3") (h "1flcf8dzg5s97ianklcxwldxc3jwl10x4l4ak4kx3ly8nmm4jql0") (y #t)))

(define-public crate-dbus-rs-0.0.4 (c (n "dbus-rs") (v "0.0.4") (h "0wx82mif8vvixmfzx1xknzl6sixzkhvmchm8rqf56j8408fk0wiy") (y #t)))

(define-public crate-dbus-rs-0.0.5 (c (n "dbus-rs") (v "0.0.5") (h "0smsz5kzxlnygzngqx33xp1mhb1ckp3azm1fvzm7mj4i44c5rpys") (y #t)))

