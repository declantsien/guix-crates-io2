(define-module (crates-io db us dbusmenu-gtk3) #:use-module (crates-io))

(define-public crate-dbusmenu-gtk3-0.1.0 (c (n "dbusmenu-gtk3") (v "0.1.0") (d (list (d (n "atk") (r ">=0.15") (d #t) (k 0)) (d (n "dbusmenu-glib") (r "^0.1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "dbusmenu-gtk3-sys")) (d (n "glib") (r ">=0.15") (d #t) (k 0)) (d (n "gtk") (r ">=0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vj3k0pn2m6j0wj28qcy5jwgvwlva91ic4fnwk791mzfafzh4pmi") (f (quote (("dox" "ffi/dox" "dbusmenu-glib/dox" "glib/dox" "gtk/dox" "atk/dox"))))))

