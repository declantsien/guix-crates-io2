(define-module (crates-io db us dbusify_hyper) #:use-module (crates-io))

(define-public crate-dbusify_hyper-0.1.0 (c (n "dbusify_hyper") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "rspotify") (r "^0.2.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.2.2") (d #t) (k 0)))) (h "0zz21aiznsy2mfwpb15vydx8zvcklja9alh32zf85cya5kagy0rh")))

