(define-module (crates-io db us dbus-crossroads) #:use-module (crates-io))

(define-public crate-dbus-crossroads-0.1.0 (c (n "dbus-crossroads") (v "0.1.0") (d (list (d (n "dbus") (r "^0.8.4") (d #t) (k 0)))) (h "1q0vh65kpa470jikf5mac80m9ih5w21f3wrd7c4alkkvvidsgsji")))

(define-public crate-dbus-crossroads-0.2.0 (c (n "dbus-crossroads") (v "0.2.0") (d (list (d (n "dbus") (r "^0.8.4") (d #t) (k 0)))) (h "1gkx9bpn2hvmd7972syz0w119fnv141dm27gjc4h3vy70ihg7fvb")))

(define-public crate-dbus-crossroads-0.2.1 (c (n "dbus-crossroads") (v "0.2.1") (d (list (d (n "dbus") (r "^0.8.4") (d #t) (k 0)))) (h "02faxr8s8fskpyrsj5w8kik1qnf8khdgfp6dz2gxr9airx355rsw")))

(define-public crate-dbus-crossroads-0.2.2 (c (n "dbus-crossroads") (v "0.2.2") (d (list (d (n "dbus") (r "^0.8.4") (d #t) (k 0)))) (h "1291ahcy0saa9nyn0xis175dnb5i2gf4485vf6g33zj1sjagcq7m")))

(define-public crate-dbus-crossroads-0.3.0 (c (n "dbus-crossroads") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "0nlrffpi4acgry0czv70h0zv3rbf6iw2fvzsrhdpnb1qwf56x08a")))

(define-public crate-dbus-crossroads-0.4.0 (c (n "dbus-crossroads") (v "0.4.0") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)))) (h "18wcwqm1qp8yisxj62vn81fdbh07kq4vxba9823fcd4zv6zinn64")))

(define-public crate-dbus-crossroads-0.5.0 (c (n "dbus-crossroads") (v "0.5.0") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)))) (h "0hc1j4vgy5k3lf8nqh5s0yk5hs50cmk2i7c5qkgd1izpg15krn55")))

(define-public crate-dbus-crossroads-0.5.1 (c (n "dbus-crossroads") (v "0.5.1") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt" "test-util" "macros" "sync"))) (d #t) (k 2)))) (h "0c3f80dychnx1mi8p2rlr7276pywnqyp6ainmzyk6aq1dlli8ham")))

(define-public crate-dbus-crossroads-0.5.2 (c (n "dbus-crossroads") (v "0.5.2") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt" "test-util" "macros" "sync"))) (d #t) (k 2)))) (h "1q3dyywazr3hppm052fa8q2366q66ml789r42jjlnm47f51q6k1s")))

