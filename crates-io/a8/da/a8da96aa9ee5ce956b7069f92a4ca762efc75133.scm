(define-module (crates-io a8 da a8da96aa9ee5ce956b7069f92a4ca762efc75133) #:use-module (crates-io))

(define-public crate-a8da96aa9ee5ce956b7069f92a4ca762efc75133-0.5.1 (c (n "a8da96aa9ee5ce956b7069f92a4ca762efc75133") (v "0.5.1") (d (list (d (n "base64") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hmac") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12") (o #t) (k 0)) (d (n "sha-1") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "16mw15wfsxvqcxf1bfzmgscji9jriqjnjdqn9cyl56mpy3m0flvc") (f (quote (("scram" "base64" "getrandom" "sha-1" "sha2" "hmac" "pbkdf2") ("default" "scram" "anonymous") ("anonymous" "getrandom"))))))

