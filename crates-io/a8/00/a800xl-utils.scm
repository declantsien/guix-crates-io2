(define-module (crates-io a8 #{00}# a800xl-utils) #:use-module (crates-io))

(define-public crate-a800xl-utils-0.1.0 (c (n "a800xl-utils") (v "0.1.0") (d (list (d (n "ufmt") (r "^0") (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)) (d (n "ufmt-write") (r "^0") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "0l0r4rv898548x7psrk2xkwqd36w8qzl334drdc6szsxgg0g5qqz")))

(define-public crate-a800xl-utils-0.1.1 (c (n "a800xl-utils") (v "0.1.1") (d (list (d (n "ufmt") (r "^0") (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 2)) (d (n "ufmt-write") (r "^0") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "0qbq4q309719l6x8xv2nx3ir4zzks7dapq48kqx4g9lv5ns5pw4x")))

(define-public crate-a800xl-utils-0.1.3 (c (n "a800xl-utils") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "ufmt") (r "^0") (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "04zj45xd2x084jp06slazp4km1vp7axiic6r4aw150nkrqd6ipwc")))

(define-public crate-a800xl-utils-0.1.4 (c (n "a800xl-utils") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "ufmt") (r "^0") (d #t) (k 0)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "1x8j29r30bc4fgf4irmx4h8hp4bma39r5p3ji1xmbd5k5dvxj673") (f (quote (("docs-rs"))))))

(define-public crate-a800xl-utils-0.2.0 (c (n "a800xl-utils") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "ufmt-stdio") (r "^0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "1b9hjhkkmj77b70yrmizamb9vhzvwjs6l73m0m5n0y9gcwdnd3nx") (f (quote (("docs-rs"))))))

(define-public crate-a800xl-utils-0.2.1 (c (n "a800xl-utils") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "ufmt-stdio") (r "^0.3") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0") (d #t) (k 0)))) (h "107igysz3p2szlnjsq8h8pj2pvdb2pp52ly7yflf9k70324k4v7b") (f (quote (("docs-rs"))))))

