(define-module (crates-io cg o_ cgo_oligami) #:use-module (crates-io))

(define-public crate-cgo_oligami-0.3.1 (c (n "cgo_oligami") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "16pignaz4qvq4crx3751i7mi9ac3pzb4kvwjxm1dhgnzpbrhhn2g")))

(define-public crate-cgo_oligami-0.3.2 (c (n "cgo_oligami") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1vgrcr13p2zk5givfapa547wdxgl0cqvflwb59q8cnx3y3i0lsyf")))

(define-public crate-cgo_oligami-0.3.3 (c (n "cgo_oligami") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1r9r26fqzwhabdj6437q08s3n4wfi95b6rph8vyacas73niw9hyz")))

(define-public crate-cgo_oligami-0.3.4 (c (n "cgo_oligami") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1vlqabnr3b957lp1rfh2ag6h6655brk43jix1fl3qazwmqscbh6p")))

(define-public crate-cgo_oligami-0.3.5 (c (n "cgo_oligami") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0pvikds299vrddrfx14sx1ka554mymd95mmd3490brpbm5ws7414")))

(define-public crate-cgo_oligami-0.3.6 (c (n "cgo_oligami") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1ns9f3pr6bv618qywjkgawrdxa6q3nawzn1m51ynxk11hwd7w1cs")))

