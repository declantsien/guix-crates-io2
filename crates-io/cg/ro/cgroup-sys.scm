(define-module (crates-io cg ro cgroup-sys) #:use-module (crates-io))

(define-public crate-cgroup-sys-0.1.0 (c (n "cgroup-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05k6hl9hdxr2kxd0fhgl91i5b45352w29jlr4lm4pii789i084fd")))

(define-public crate-cgroup-sys-0.1.1 (c (n "cgroup-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a63fxk9mn3fpw1yl1gplkkaw7iyiiy2swhlsl0jvlmpb23v1019")))

