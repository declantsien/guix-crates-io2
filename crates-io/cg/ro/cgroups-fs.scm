(define-module (crates-io cg ro cgroups-fs) #:use-module (crates-io))

(define-public crate-cgroups-fs-1.0.0 (c (n "cgroups-fs") (v "1.0.0") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "0gzca6ah8lbkaag7i50p6n79p37bhq4hwiydjg67b8jlxr1bj9ld")))

(define-public crate-cgroups-fs-1.0.1 (c (n "cgroups-fs") (v "1.0.1") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "0cdrfip9hd75a8ilwq1jscbcj16lfsw8k4nz1i59b4lmqjw5lxgq")))

(define-public crate-cgroups-fs-1.0.2 (c (n "cgroups-fs") (v "1.0.2") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "0h6f589g4rnwlwss7dwq4xpps5w2wfrmhh83jqx2044czl8hgfn0")))

(define-public crate-cgroups-fs-1.0.3 (c (n "cgroups-fs") (v "1.0.3") (d (list (d (n "nix") (r "> 0.12.0, < 0.14") (d #t) (k 0)))) (h "11xjc3ljd5hd7zrn0qciq9kyjidn8kyrzpqb8is42hspvz7295ry")))

(define-public crate-cgroups-fs-1.1.0 (c (n "cgroups-fs") (v "1.1.0") (d (list (d (n "nix") (r "> 0.12.0, < 0.15") (d #t) (k 0)))) (h "1vhqr78nbqsflwcgm600h6iqi41fj30g9wiyx189ajf2ips7l20k")))

(define-public crate-cgroups-fs-1.1.1 (c (n "cgroups-fs") (v "1.1.1") (d (list (d (n "nix") (r "> 0.12.0, < 0.16") (d #t) (k 0)))) (h "0ss21yc4989yznrixv0wrx23i4l03jrqi2vd326isaywwg3siymd")))

(define-public crate-cgroups-fs-1.1.2 (c (n "cgroups-fs") (v "1.1.2") (d (list (d (n "nix") (r ">0.12.0, <0.18") (d #t) (k 0)))) (h "18pfch72iigxcay8442mmpwcs1j2jsqljl8l7l33zg6g76wydg28")))

(define-public crate-cgroups-fs-1.1.3 (c (n "cgroups-fs") (v "1.1.3") (d (list (d (n "nix") (r ">0.12.0, <0.25") (d #t) (k 0)))) (h "0w93qwyg38cr5v7viviikpyj2sld8rczykxgbypc5cgsd9p1nf6b")))

