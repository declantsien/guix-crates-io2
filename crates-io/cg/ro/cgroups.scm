(define-module (crates-io cg ro cgroups) #:use-module (crates-io))

(define-public crate-cgroups-0.0.1 (c (n "cgroups") (v "0.0.1") (h "0pa4wf80w8fvivg3rykdfnvxjbahynsbgz5mv68nibmilj09qw99")))

(define-public crate-cgroups-0.0.2 (c (n "cgroups") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "nix") (r "^0.11.0") (d #t) (k 2)))) (h "1hzhkhlaknfjzsavj3a029gkqy5312r9skmm3pnp20zbbr68cgvl")))

(define-public crate-cgroups-0.1.0 (c (n "cgroups") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 2)))) (h "0w3p06jb2fhafgn4ak8h0kcc1jla7dhndf4l93qn987w690qqhrv")))

