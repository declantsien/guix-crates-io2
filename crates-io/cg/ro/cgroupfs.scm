(define-module (crates-io cg ro cgroupfs) #:use-module (crates-io))

(define-public crate-cgroupfs-0.1.0 (c (n "cgroupfs") (v "0.1.0") (d (list (d (n "cgroupfs-thrift") (r "^0.1") (d #t) (k 0)) (d (n "openat") (r "^0.1.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xy44qfhgsq40rs0a0w3cym4701v3pqlnxgk8y4jaalizk6yd3p6")))

(define-public crate-cgroupfs-0.2.0 (c (n "cgroupfs") (v "0.2.0") (d (list (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "openat") (r "^0.1.20") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pbp0rwdkc48qai9pir52sb3dklgxwdzfcr2v936h4pfr3469zk0")))

(define-public crate-cgroupfs-0.3.0 (c (n "cgroupfs") (v "0.3.0") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "openat") (r "^0.1.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h0y9ydc3gz2sxf2iyfanzggcbw6g44dvxcdmqhwmcg83w7cy4r8")))

(define-public crate-cgroupfs-0.4.0 (c (n "cgroupfs") (v "0.4.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1gg8rr092awqvjz182mg16r2vdrdqp5bdd0lmhaxpfzyy94kmklk")))

(define-public crate-cgroupfs-0.4.1 (c (n "cgroupfs") (v "0.4.1") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0ydbv4njzzidpb1iwl03ma98yvpm57xs2jqmg53zjxb32wq891fr")))

(define-public crate-cgroupfs-0.5.0 (c (n "cgroupfs") (v "0.5.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1rylnzf20r1db99wc3xlqz1czckm8qjm3pfyg6hs2nr55vj8k0aw")))

(define-public crate-cgroupfs-0.6.0 (c (n "cgroupfs") (v "0.6.0") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rfdq0ad8zi19rn1lfc245ijs968lkj970ligygj6gbw8hc74gj4")))

(define-public crate-cgroupfs-0.6.1 (c (n "cgroupfs") (v "0.6.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0rnazcicxv0gfg5y59711fwxw53s5a91dn23n11mbmf81m6rs8z4")))

(define-public crate-cgroupfs-0.6.2 (c (n "cgroupfs") (v "0.6.2") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0nmfb4pa2jd1116mv0cxf8851ai7dxjkjw765b16qla4mmn8kcx8")))

(define-public crate-cgroupfs-0.6.3 (c (n "cgroupfs") (v "0.6.3") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0jyzj3jm2v7axxrz27hpzwd5f65snj14aclyzs57fhq97d737q52")))

(define-public crate-cgroupfs-0.7.0 (c (n "cgroupfs") (v "0.7.0") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0fjrzja1h12indwfd0ll0rxs118z2y1mb42f70zyyqv2psh4ysmv")))

(define-public crate-cgroupfs-0.7.1 (c (n "cgroupfs") (v "0.7.1") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0.176") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0ih878i5rnv4dvpl2xa3igy10h39xzgw3bjsfirm9hwc2hi8hd36")))

(define-public crate-cgroupfs-0.8.0 (c (n "cgroupfs") (v "0.8.0") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0lnkxsjb6fb1p0dcrmx7l6f250nabyngisjqk2xicsgkxhx876kf")))

(define-public crate-cgroupfs-0.8.1 (c (n "cgroupfs") (v "0.8.1") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "openat") (r "^0.1.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0.185") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1swsznvvg9dg1d356glqipwnyi9hh42cd9a3zhim383p51xrccs5")))

