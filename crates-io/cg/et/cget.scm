(define-module (crates-io cg et cget) #:use-module (crates-io))

(define-public crate-cget-0.1.1 (c (n "cget") (v "0.1.1") (h "1qxsdag4pqyj4swc1zily2ydlr9ib7p7vpyjckaf9ri07gavdh1a")))

(define-public crate-cget-0.1.2 (c (n "cget") (v "0.1.2") (h "0n9x6i84bj6lhy4kf71cb0l3lrv6ywac6im2dlw98nyb159vxhyw")))

