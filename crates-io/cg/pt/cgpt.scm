(define-module (crates-io cg pt cgpt) #:use-module (crates-io))

(define-public crate-cgpt-0.1.0 (c (n "cgpt") (v "0.1.0") (h "19jbn6qb242fcjz5gcyvjg1sp123qf6gn8g2h9pvakxzafd0886j")))

(define-public crate-cgpt-0.1.1 (c (n "cgpt") (v "0.1.1") (h "0x282zlihcw25iina3fpasm88lbrbccwp91qwiryx7yy8qni4r5a")))

(define-public crate-cgpt-0.1.2 (c (n "cgpt") (v "0.1.2") (h "0f8r77li3y8cjvkpwpkzqjxy2afp7zd203fmcpk1hclif1x5znkf")))

(define-public crate-cgpt-0.1.3 (c (n "cgpt") (v "0.1.3") (h "0gd4bgcf2vqqb87xc7ia0ag77y80mg9zjqdx59xzq1c7bch5zw65")))

(define-public crate-cgpt-0.1.4 (c (n "cgpt") (v "0.1.4") (h "0r3v4ay3pfwpr76mmmmg3jwvgzg7jxhl2im2pw29kv6s070m5ph9")))

