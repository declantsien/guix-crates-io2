(define-module (crates-io cg pt cgpt-cmd) #:use-module (crates-io))

(define-public crate-cgpt-cmd-0.0.1 (c (n "cgpt-cmd") (v "0.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0k4g3n9ywm77hxl70g0ixllph89yaipqcwmrcaiwl7c2hpxl6hwk")))

(define-public crate-cgpt-cmd-0.0.2 (c (n "cgpt-cmd") (v "0.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "0agjxb10nz2hcmmj4fx5rnsjv0v9i1l7bwfd6zf95dp5ck45bdjw")))

