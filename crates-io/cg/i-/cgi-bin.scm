(define-module (crates-io cg i- cgi-bin) #:use-module (crates-io))

(define-public crate-cgi-bin-0.0.1 (c (n "cgi-bin") (v "0.0.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("io"))) (d #t) (k 0)))) (h "0nqcsrhdxbd3d70z2rlg65yy5wf9a3x9xncx77w8w3lwqwy3dy5z")))

