(define-module (crates-io cg os cgos) #:use-module (crates-io))

(define-public crate-cgos-0.1.0 (c (n "cgos") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0mmhb890qhbhc2dyv47msa700wzn2pbacmgwhfvrxz4lzawar83m")))

(define-public crate-cgos-0.2.0 (c (n "cgos") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "013lsrc8v1l1ambhazbyw7n1dgsvgg4dhzs1q90g9nvqzpdzjsma")))

