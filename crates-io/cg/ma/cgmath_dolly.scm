(define-module (crates-io cg ma cgmath_dolly) #:use-module (crates-io))

(define-public crate-cgmath_dolly-0.1.0 (c (n "cgmath_dolly") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "glam") (r ">=0.15, <=0.20") (d #t) (k 2)) (d (n "macroquad") (r "^0.3") (d #t) (k 2)))) (h "182403snip540mbgh794fqwxj54za5mb1rrmp4n9njswg3j2nkdr")))

