(define-module (crates-io cg ma cgmath-std140) #:use-module (crates-io))

(define-public crate-cgmath-std140-0.1.0 (c (n "cgmath-std140") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "std140") (r "^0.1.2") (d #t) (k 0)))) (h "1nyjknana8g259ymxc1jjhqk61j7bcbjin2kp856hc4dzflz05d5")))

(define-public crate-cgmath-std140-0.2.0 (c (n "cgmath-std140") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "std140") (r "^0.1.2") (d #t) (k 0)))) (h "1bwzy2bz8jawq08a411sxbp9ycds06aihr08ndbz5w9bpf746sps")))

(define-public crate-cgmath-std140-0.2.1 (c (n "cgmath-std140") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "std140") (r "^0.2.0") (d #t) (k 0)))) (h "1z96zx1hrhjqm59zndn6ff62i0rksvfaznblawb15x645ickaiyy")))

(define-public crate-cgmath-std140-0.2.2 (c (n "cgmath-std140") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "std140") (r "^0.2.4") (d #t) (k 0)))) (h "0ljjr7qh2x3yfha5n22rv9sxzix08kgc60wl76nl0mnvhv3jy96w")))

