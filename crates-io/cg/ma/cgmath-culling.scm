(define-module (crates-io cg ma cgmath-culling) #:use-module (crates-io))

(define-public crate-cgmath-culling-0.1.0 (c (n "cgmath-culling") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "0g76kxnnrp0xnsq8qyz3k7nhjp7amy5w5h21fq0w464libvkb8qq")))

(define-public crate-cgmath-culling-0.1.1 (c (n "cgmath-culling") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "0np2wzj16spa9n81xap73nhcpfinwjy98xs621rmk90gb5p0f5s7")))

(define-public crate-cgmath-culling-0.1.2 (c (n "cgmath-culling") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "107d5b86kpalgjpqx8w7jfccw8hjy9jmbnrcjkk6yy7f5n2d3djx")))

(define-public crate-cgmath-culling-0.2.0 (c (n "cgmath-culling") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "1dr9jxb1qmk2mi5hqi91l8zbn48al6c403475dks06xaz0bzq9km")))

