(define-module (crates-io cg ma cgmath) #:use-module (crates-io))

(define-public crate-cgmath-0.0.2 (c (n "cgmath") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "19z5lii0b0jl3fkscswwn3qx1bafrqg4l7j8v975chd3ly68vp3l")))

(define-public crate-cgmath-0.0.3 (c (n "cgmath") (v "0.0.3") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1dvcjz3gbmym07c4q72r8iaw80bgfh86nl82xlq2gv5j8pmhilxy")))

(define-public crate-cgmath-0.0.4 (c (n "cgmath") (v "0.0.4") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "17rizxvm2pfb8f6s8ckbll52ffv0hf3dykllc441l4rkgmjmp181")))

(define-public crate-cgmath-0.0.5 (c (n "cgmath") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "155pgkc1i31ynqr04n53idhzlq946c14yjx6cgiksi6kj49m6kir")))

(define-public crate-cgmath-0.0.6 (c (n "cgmath") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "00fmcqr99qvzqkzkl0gmxw6njbbl4bs8wkz50fa70kp1c4cly8q4")))

(define-public crate-cgmath-0.0.7 (c (n "cgmath") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1rmwmidy35d95c6j2pi7iphfdyaca46f5cl84jhwxq4f02jxyldx")))

(define-public crate-cgmath-0.0.8 (c (n "cgmath") (v "0.0.8") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rand_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1vrn3v1y4dfw5xlcghwvr3jglk1ming4d6rbw39vnd1flz7b8gvi")))

(define-public crate-cgmath-0.1.0 (c (n "cgmath") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0y3zia74yd8cawa0l5fm1zl2jx7k38k2rs89zb25hrd90hxzp1kw")))

(define-public crate-cgmath-0.1.1 (c (n "cgmath") (v "0.1.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1cqs2jkfx3p1b1h7x4s1fl4pb68nz8yb6p3fzdh22hzi8jigzqni")))

(define-public crate-cgmath-0.1.2 (c (n "cgmath") (v "0.1.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1s8y7w1f7l336z9k2xsismhd97xmpaw7z31b12hq8j7slx83a5ia")))

(define-public crate-cgmath-0.1.3 (c (n "cgmath") (v "0.1.3") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0vxxlryhv886lpgqd44znd53bjzhmmmj892vb64fvh9nxnq9j59n")))

(define-public crate-cgmath-0.1.4 (c (n "cgmath") (v "0.1.4") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "14z1piw5lh2qj4ss90l3vgr0xh6n0s47cgbql38pbdhfcilx1dc9") (y #t)))

(define-public crate-cgmath-0.1.5 (c (n "cgmath") (v "0.1.5") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "17y1f76xjz365hpwdzv1qcb59r66xsm0kfdf926cbzymxvj1l2x1")))

(define-public crate-cgmath-0.1.6 (c (n "cgmath") (v "0.1.6") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1r4d17w27c3jpwfs6p0k15rgpsd9nvf9vc0n140d2bm482q6xq31") (y #t)))

(define-public crate-cgmath-0.2.0 (c (n "cgmath") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0lwdy396fp80kjsxp3qb0bk9i89ri92wrivyw9fqjd5blxkj0m1a")))

(define-public crate-cgmath-0.3.0 (c (n "cgmath") (v "0.3.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "05v9ffiki1kxd32khv7xisnczimwxvc7ny69vgslgjqjsip9d5gb")))

(define-public crate-cgmath-0.3.1 (c (n "cgmath") (v "0.3.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0pyj122186gv45nlfh62c6wrsnhsa4ax9vxp7sqzyxjkhkwybarx")))

(define-public crate-cgmath-0.4.0 (c (n "cgmath") (v "0.4.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0qilji9afczvmaxaby60znga1w1z70srqk1w8y2w033962fhha11")))

(define-public crate-cgmath-0.5.0 (c (n "cgmath") (v "0.5.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0882hbppn3rqgn93067v8c5rmd0kbfkl2zgpvz37yjxa0dpbmmz8")))

(define-public crate-cgmath-0.6.0 (c (n "cgmath") (v "0.6.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "11y1rbzqic7ksh3q1w5pzmj0kwvpz9rzb0nw2zx925x7lx89yrf4")))

(define-public crate-cgmath-0.7.0 (c (n "cgmath") (v "0.7.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1xmrjrxi584y4hzq8714c2ckkh96c16gb7x68swzyx3fnqz3psbm") (f (quote (("unstable"))))))

(define-public crate-cgmath-0.8.0 (c (n "cgmath") (v "0.8.0") (d (list (d (n "glium") (r "^0.13.5") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1grv07lxmxpqad45sz2p03silv1x60ik9df7pv2hnhn4qsj62kdl") (f (quote (("unstable"))))))

(define-public crate-cgmath-0.9.0 (c (n "cgmath") (v "0.9.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "19480lm2j8aj1lf45a8ga2c3jnkdb4s7nca8l39w261hkc65x0b7") (f (quote (("unstable"))))))

(define-public crate-cgmath-0.9.1 (c (n "cgmath") (v "0.9.1") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1l33l5wzqx46v0h2hp2rddr6fk8sxfrz0d22gf8llxa36ypgpkji") (f (quote (("unstable"))))))

(define-public crate-cgmath-0.10.0 (c (n "cgmath") (v "0.10.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0sgn03k4k6j2kfx4bavz2l5vwwb5msfh6avqkwvnlrcfg2qasiv7") (f (quote (("unstable") ("eders" "serde" "serde_macros") ("default" "rustc-serialize"))))))

(define-public crate-cgmath-0.11.0 (c (n "cgmath") (v "0.11.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)))) (h "10f0a97q688lqywnnmx41ia7s2flmibfd8zfqsp9h8h75v0mvhxf") (f (quote (("unstable") ("eders" "serde" "serde_macros") ("default" "rustc-serialize"))))))

(define-public crate-cgmath-0.12.0 (c (n "cgmath") (v "0.12.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 2)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0az0mgipzcr7f9mq6mk8haa7hj2x64rxii6z9qw1rmms9mh74913") (f (quote (("unstable") ("eders" "serde" "serde_macros") ("default" "rustc-serialize"))))))

(define-public crate-cgmath-0.13.0 (c (n "cgmath") (v "0.13.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15w77ycn8l66mdgvcc6pw1808kjql0nc43l07ng6m0q3z8iwa48f") (f (quote (("use_simd" "simd") ("unstable") ("eders" "serde" "serde_derive") ("default" "rustc-serialize"))))))

(define-public crate-cgmath-0.13.1 (c (n "cgmath") (v "0.13.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11rmspb4ra9mrisvg4hrw96rdwhlmk31cligl0b8nwaxcwvippv4") (f (quote (("use_simd" "simd") ("unstable") ("eders" "serde" "serde_derive"))))))

(define-public crate-cgmath-0.14.0 (c (n "cgmath") (v "0.14.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0lxcdryplaldk0v51d9476s6vidfh2gmdwvr2c0llpbsk9vjcgry") (f (quote (("use_simd" "simd") ("unstable") ("eders" "serde" "serde_derive"))))))

(define-public crate-cgmath-0.14.1 (c (n "cgmath") (v "0.14.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1020iimy1zynyjvixkjy27b9qpvdjw1pjy2w054hvwykgahjbw47") (f (quote (("use_simd" "simd") ("unstable") ("eders" "serde" "serde_derive"))))))

(define-public crate-cgmath-0.15.0 (c (n "cgmath") (v "0.15.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "mint") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1rd2kyqzr604dbydwlgxphb5jyva81lckv22xiqziayglw12qdyj") (f (quote (("unstable"))))))

(define-public crate-cgmath-0.16.0 (c (n "cgmath") (v "0.16.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.19") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1p4dhckhksw5dv8zqxmj4cx531l6vmcb1alzdrh54xk4y8klwgvg") (f (quote (("unstable") ("swizzle"))))))

(define-public crate-cgmath-0.16.1 (c (n "cgmath") (v "0.16.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.19") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07754c03v3srzf64ghsl3fggrdi4kjy6l3vyq2d2wfjfixybb934") (f (quote (("unstable") ("swizzle"))))))

(define-public crate-cgmath-0.17.0 (c (n "cgmath") (v "0.17.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "glium") (r "^0.23") (d #t) (k 2)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "simd") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1rvgila6ivr0dh1bxza450a4yfwdi2pwj3h1vnwg0jy4xk6l8f98") (f (quote (("unstable") ("swizzle"))))))

(define-public crate-cgmath-0.18.0 (c (n "cgmath") (v "0.18.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05sk7c1c1jg5ygqvc3y77kxddp177gwazfibhd864ag3800x760s") (f (quote (("unstable") ("swizzle"))))))

