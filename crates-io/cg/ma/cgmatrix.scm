(define-module (crates-io cg ma cgmatrix) #:use-module (crates-io))

(define-public crate-cgmatrix-0.1.0 (c (n "cgmatrix") (v "0.1.0") (h "1sm1317msjzsig97bl4r2vx3gw0bxbpzbsvg6lg1xbj11h8kl7cw")))

(define-public crate-cgmatrix-0.2.0 (c (n "cgmatrix") (v "0.2.0") (h "0mrpdb7fzzsnyfix7imfgy8yw0i2c5vzbz3y1fldvfdqcbfrbpzv")))

(define-public crate-cgmatrix-0.2.1 (c (n "cgmatrix") (v "0.2.1") (h "01kr78913xp8paw0vfwl8illflw2rr656vxg12fphzayxmvnsixl")))

