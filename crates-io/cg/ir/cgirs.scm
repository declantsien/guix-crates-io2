(define-module (crates-io cg ir cgirs) #:use-module (crates-io))

(define-public crate-cgirs-0.0.1 (c (n "cgirs") (v "0.0.1") (h "1a2jzsmaad6ds5gh236s0nnjxa5hf6pl0qlj38sdh32brrd4khbn")))

(define-public crate-cgirs-0.0.2 (c (n "cgirs") (v "0.0.2") (h "0zmp3phk720nbb4nbqnbijfhkcq2pi3p03hz88jdaxrr0x4h1dq0")))

(define-public crate-cgirs-0.0.3 (c (n "cgirs") (v "0.0.3") (h "0s9pcz8v6dia5nzwl06299dscsw2vf7zzlzqhnflczm3rqp5vdmb")))

(define-public crate-cgirs-0.1.0 (c (n "cgirs") (v "0.1.0") (h "0bv8bi7g3wx2jpq3z8mlghd9624fy8ci6nvcsz12p8snpzrgyjps")))

(define-public crate-cgirs-1.0.0 (c (n "cgirs") (v "1.0.0") (h "0dn6z5swd3bvjczj14gaz38a7ki32flvdcwxiqw15h6fkyx9pcaf")))

(define-public crate-cgirs-1.1.0 (c (n "cgirs") (v "1.1.0") (h "13spf5vm9lg2b9dw7lxllvrwjl5271jzd278fb48pfixc12vc97v")))

