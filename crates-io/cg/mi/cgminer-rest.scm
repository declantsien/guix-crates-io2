(define-module (crates-io cg mi cgminer-rest) #:use-module (crates-io))

(define-public crate-cgminer-rest-0.1.2 (c (n "cgminer-rest") (v "0.1.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1prcvkv5rsjdwgqb9dzd2k26h4j5y9a976aid531s7vxw6n0qzwx")))

