(define-module (crates-io cg ol cgol-tui) #:use-module (crates-io))

(define-public crate-cgol-tui-0.2.0 (c (n "cgol-tui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "145ip2yrwhrhmmmzvh3rfkrp33a3amh3p6i18lpx813xahc5kdc3")))

(define-public crate-cgol-tui-0.2.1 (c (n "cgol-tui") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "19n7rl4b5i8j4i6szn5asvr3bxi92j4kmp1rh55dad98kala6h5d")))

(define-public crate-cgol-tui-0.3.0 (c (n "cgol-tui") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "11wzwzmsa688cq7mp36pm5hz3p42a0d86ak5fbs8gas95dq3ziwx")))

