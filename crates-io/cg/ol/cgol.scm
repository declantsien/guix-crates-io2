(define-module (crates-io cg ol cgol) #:use-module (crates-io))

(define-public crate-cgol-0.1.0 (c (n "cgol") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)))) (h "0r9wssdwlna1zs4jfypn3fcjvg7r5863bb6bncl731hsv226jjzs")))

(define-public crate-cgol-0.1.1 (c (n "cgol") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)))) (h "0y1x2fil41m1h2zgcsn76jpbpr9j4ihwlrmd91qni1mxrq1dq2zf")))

