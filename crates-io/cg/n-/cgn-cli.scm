(define-module (crates-io cg n- cgn-cli) #:use-module (crates-io))

(define-public crate-cgn-cli-0.1.0 (c (n "cgn-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "cgn") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "013273s9gw194lp6sx9vh8ix3v1iip7m0nqva1ap5jhlq4bbzz6s")))

(define-public crate-cgn-cli-0.1.1 (c (n "cgn-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "cgn") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0r1b27xpy85k3d1gppl3fqg7afla0f8iyb8ag9y3chyiwqn07wiq")))

(define-public crate-cgn-cli-0.1.2 (c (n "cgn-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "cgn") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1ws4g47v6hgdfh9q0vav509lzfjclwyv8zmzw6yjv7mgv16bgbdk")))

