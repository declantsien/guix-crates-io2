(define-module (crates-io cg -m cg-math) #:use-module (crates-io))

(define-public crate-cg-math-0.1.0 (c (n "cg-math") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "parry2d-f64") (r "^0.13.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "timeit") (r "^0.1.2") (d #t) (k 0)))) (h "0qwrxp90k5ai6fk524n4l2c0ds9sbqgzdyz7p6lmc52swjna0ssx") (r "1.71.0")))

(define-public crate-cg-math-0.1.1 (c (n "cg-math") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "parry2d-f64") (r "^0.13.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "timeit") (r "^0.1.2") (d #t) (k 0)))) (h "0cld5vkmiz7qci5z51rgf05agbalb72n7h2kdlddfpbf39c14psy") (r "1.71.0")))

(define-public crate-cg-math-0.1.2 (c (n "cg-math") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.7.0") (d #t) (k 0)) (d (n "parry2d-f64") (r "^0.13.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "timeit") (r "^0.1.2") (d #t) (k 0)))) (h "0vh6g4ifsnbis0pk3d08hvkpg8aijsxc85vz92sdaia9b6fnhxyy") (r "1.71.0")))

