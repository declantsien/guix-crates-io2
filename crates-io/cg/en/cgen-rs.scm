(define-module (crates-io cg en cgen-rs) #:use-module (crates-io))

(define-public crate-cgen-rs-0.1.0 (c (n "cgen-rs") (v "0.1.0") (h "1wlq498ahhn98j97r7bwhf9f31pz6wxfz9pii9p4x6qlarbg6bbc") (y #t)))

(define-public crate-cgen-rs-0.1.1 (c (n "cgen-rs") (v "0.1.1") (h "1ky1lmgjc591cs8mbydyisg5ck028kx1kib75ryl4riq7m2iirbz") (y #t)))

(define-public crate-cgen-rs-0.1.2 (c (n "cgen-rs") (v "0.1.2") (h "1in6hjz3n18mdrbfx9wab2drq6zjsmkj1k350169zwy8s5dmw0sx") (y #t)))

