(define-module (crates-io cg -l cg-local) #:use-module (crates-io))

(define-public crate-cg-local-0.1.0 (c (n "cg-local") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "1sljr5ihpnb2k5zib83zai1998nfywxym2r9h441f22nh7z49agj")))

(define-public crate-cg-local-0.1.1 (c (n "cg-local") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "0r772ixvkc9hgycblyixqln7rd24xggc39ywbylkv4f91ba4z3q3")))

