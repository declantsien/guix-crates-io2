(define-module (crates-io cg at cgats) #:use-module (crates-io))

(define-public crate-cgats-0.1.3 (c (n "cgats") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "deltae") (r "^0.1.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 2)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "0li5grl3npf699k2fcr7rh7c9fs7j405azvrjl2dr0gyr0dc621b")))

(define-public crate-cgats-0.1.4 (c (n "cgats") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "deltae") (r "0.1.*") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 2)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "1f491mh6khxqlf1c6bcy1qml48z6npa2n35bz2qppzqr756lyzaz")))

(define-public crate-cgats-0.1.5 (c (n "cgats") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "deltae") (r "0.1.*") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 2)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "0gy6djyxf1ckjfawvajc7dv2g4rzmdpmx05nbkgslka1w4b65izv")))

(define-public crate-cgats-0.2.0 (c (n "cgats") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 2)) (d (n "deltae") (r "^0.3.0") (d #t) (k 0)))) (h "1w1qzkyq8vpv5wpi44ibzj5dkcjamzpcral82i79yx9a3pqxlimr")))

