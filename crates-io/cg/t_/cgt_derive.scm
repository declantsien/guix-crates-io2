(define-module (crates-io cg t_ cgt_derive) #:use-module (crates-io))

(define-public crate-cgt_derive-0.4.0 (c (n "cgt_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0nx51kg4jd0z6vcjamqk6svfzc2n4m226ab76ankg6bm1bg9in2h")))

(define-public crate-cgt_derive-0.4.1 (c (n "cgt_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "07z925c6kb2zbdpzaqxq6d6762jplva7nz5dlq9zd600mlp3mm36")))

