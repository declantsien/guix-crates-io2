(define-module (crates-io cg -g cg-game-server) #:use-module (crates-io))

(define-public crate-cg-game-server-1.0.1 (c (n "cg-game-server") (v "1.0.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-web-actors") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "1v6qmqgip38n2bmr8r12n4vb1b96h1pg2aw1fffhirpbdc2yf2ld") (y #t)))

(define-public crate-cg-game-server-1.0.2 (c (n "cg-game-server") (v "1.0.2") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-web-actors") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "17gms01ak0648lqyj98b2r6wz2w6mzyjwnd23n7y1ky9nb592fad")))

