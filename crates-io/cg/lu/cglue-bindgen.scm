(define-module (crates-io cg lu cglue-bindgen) #:use-module (crates-io))

(define-public crate-cglue-bindgen-0.1.0 (c (n "cglue-bindgen") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17qg7n05rm6vkj10lxcjfs2gl4ymlb4crcid0d9zz7xlpi8svqpn")))

(define-public crate-cglue-bindgen-0.2.0 (c (n "cglue-bindgen") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06j218mxvxkdcpbvlsn31hf3qm50j82lfm5hd0g7qym64g5bf79y")))

(define-public crate-cglue-bindgen-0.2.1 (c (n "cglue-bindgen") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1fmbgnsg3rfmw2g8zd2rniqvbbb58p5942w6ah99qqya5hl7q2p0")))

(define-public crate-cglue-bindgen-0.2.2 (c (n "cglue-bindgen") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1zifnxr5qqn0ibwjwir07fx42j0q8l88240g7w85m6f8cd7i0512")))

(define-public crate-cglue-bindgen-0.2.3 (c (n "cglue-bindgen") (v "0.2.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13qaqcgc6qaz0ag92mx77hsbjgml3msnf8xcp0p6pr0hwiqwai33")))

