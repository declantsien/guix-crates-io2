(define-module (crates-io cg lu cglue-macro) #:use-module (crates-io))

(define-public crate-cglue-macro-0.0.0 (c (n "cglue-macro") (v "0.0.0") (h "18l7ag8qg2b21ncnzifr59x9rjllfbanvzdraw8sv4acsf8707ff") (y #t)))

(define-public crate-cglue-macro-0.1.0 (c (n "cglue-macro") (v "0.1.0") (d (list (d (n "cglue-gen") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c3az3kwflr1b18nks51plrx26wsms30ds8nsjsnb8004j2lnycd") (f (quote (("no_empty_retwrap" "cglue-gen/no_empty_retwrap") ("default"))))))

(define-public crate-cglue-macro-0.2.0 (c (n "cglue-macro") (v "0.2.0") (d (list (d (n "cglue-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18bvlgf9v2gi8lcbywsknw4z68cix7jsq2pf2m6azxwszmc882gi") (f (quote (("unstable" "cglue-gen/unstable") ("rust_void" "cglue-gen/rust_void") ("layout_checks" "cglue-gen/layout_checks") ("default"))))))

(define-public crate-cglue-macro-0.2.1 (c (n "cglue-macro") (v "0.2.1") (d (list (d (n "cglue-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a7m9v267d14hg86mypslnq77zlzzl81hx2q4h3mc5xkjnws6aas") (f (quote (("unstable" "cglue-gen/unstable") ("rust_void" "cglue-gen/rust_void") ("layout_checks" "cglue-gen/layout_checks") ("default"))))))

(define-public crate-cglue-macro-0.2.2 (c (n "cglue-macro") (v "0.2.2") (d (list (d (n "cglue-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "050nr6ay1xq1imvl1wijdi28rbshw0l3fybr8w42yvbqkzqaz1c2") (f (quote (("unstable" "cglue-gen/unstable") ("rust_void" "cglue-gen/rust_void") ("layout_checks" "cglue-gen/layout_checks") ("default"))))))

(define-public crate-cglue-macro-0.2.3 (c (n "cglue-macro") (v "0.2.3") (d (list (d (n "cglue-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ky1vq25bmg70g83fpkjyqgvq17l68psvprfjz4p62859b3vha6g") (f (quote (("unstable" "cglue-gen/unstable") ("rust_void" "cglue-gen/rust_void") ("layout_checks" "cglue-gen/layout_checks") ("default"))))))

