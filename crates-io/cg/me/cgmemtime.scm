(define-module (crates-io cg me cgmemtime) #:use-module (crates-io))

(define-public crate-cgmemtime-0.1.0 (c (n "cgmemtime") (v "0.1.0") (d (list (d (n "lexopt") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "syscall-numbers") (r "^3.1.0") (d #t) (k 0)))) (h "0v874ip2gar9bxqf0h0i54135smyx1c6rmrqnanhlnl2yw9kbxqz")))

