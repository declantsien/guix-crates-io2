(define-module (crates-io cg me cgmemtime-rs) #:use-module (crates-io))

(define-public crate-cgmemtime-rs-0.1.0 (c (n "cgmemtime-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("signal" "resource"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hbrxwjryk571rf6855p9rpwzms9af2465zj0xadfybi8azhil1f")))

