(define-module (crates-io fs _p fs_pro) #:use-module (crates-io))

(define-public crate-fs_pro-1.0.0 (c (n "fs_pro") (v "1.0.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "11l8nfjyawzqsf8qp3sn2sdw3mslhfyi55w8ri3ciqhvwv2fgymd")))

(define-public crate-fs_pro-1.1.0 (c (n "fs_pro") (v "1.1.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "10npvi2v04sflh9xbdbx5rbm6wb8r8k86rzrksp298kczhi0f4z2")))

(define-public crate-fs_pro-1.2.0 (c (n "fs_pro") (v "1.2.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x9wb0867k2fszx5067x3nzljcg3cf82wsc112j43nkhdffhcrx3") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-fs_pro-1.3.0 (c (n "fs_pro") (v "1.3.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "fs_pro_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11zld2adm6lg69hafczc8lm1r9hfv278qpxg7y8j694nabb3g3jv") (f (quote (("json" "serde" "serde_json"))))))

