(define-module (crates-io fs _p fs_pro_macros) #:use-module (crates-io))

(define-public crate-fs_pro_macros-1.0.0 (c (n "fs_pro_macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j4rsx7ydqa9d993kx24zi9lnnvab91birxn6nl1xbgnd4xxhmf7")))

