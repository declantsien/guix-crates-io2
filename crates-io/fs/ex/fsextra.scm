(define-module (crates-io fs ex fsextra) #:use-module (crates-io))

(define-public crate-fsextra-0.0.0 (c (n "fsextra") (v "0.0.0") (h "0cr1z8vfbi1msxn54x3268zxsa6wprv60lral970lyq132kv2qdg") (y #t)))

(define-public crate-fsextra-0.0.1 (c (n "fsextra") (v "0.0.1") (h "03r8cz6q5snp75v5w1sgdpy6y0p0pc5lwiahmqcxx59dj310rcbx") (y #t)))

(define-public crate-fsextra-0.1.0 (c (n "fsextra") (v "0.1.0") (h "03ygm6mplmydmj931pprff0yzzbfyvdcc70238ipvnssglg593xf")))

(define-public crate-fsextra-0.2.0 (c (n "fsextra") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)))) (h "1bhfmfphbc89c59gzzjmdz41yi1a05ixl5a7gf6d0m4hxavwql8h") (f (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.2.1 (c (n "fsextra") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)))) (h "1a7rzj6dh89vvwl6z6qg20x14arjvha37rfpvym9zawm2x2p3la8") (f (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.3.0-alpha.0 (c (n "fsextra") (v "0.3.0-alpha.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ll5k2xaqpvngl7z3q4m2xhjiclp7m1qhnb28wwqk6a0h0p3kirn") (f (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.3.0-alpha.1 (c (n "fsextra") (v "0.3.0-alpha.1") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wlz6qncpvmc3v52hdf2k0bvhrv0lswnfhpl1m55rhlby00mbcym") (f (quote (("crypto" "ring"))))))

