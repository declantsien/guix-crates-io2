(define-module (crates-io fs li fsling) #:use-module (crates-io))

(define-public crate-fsling-0.0.1 (c (n "fsling") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0hb4yrd4rd9py3nyijvvh8jm5sq8jgccdzn91yda06dhzf2idxl9")))

