(define-module (crates-io fs ta fstab) #:use-module (crates-io))

(define-public crate-fstab-0.1.0 (c (n "fstab") (v "0.1.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1rmyw9ps97cqh1sim72akkf4blhmp4r79xh3rh51xh512zpks95m")))

(define-public crate-fstab-0.2.0 (c (n "fstab") (v "0.2.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "11h64qq71c1ahf7s602f7h6pimqn64vdc7aww5gpafi8sh8sz9pi")))

(define-public crate-fstab-0.3.0 (c (n "fstab") (v "0.3.0") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0n4rs427x7dz9wx9md26xrpvlxq5yx73ln0r7ldn9pq26mqyy7hb")))

(define-public crate-fstab-0.3.1 (c (n "fstab") (v "0.3.1") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0lynlc2x7dnmmrxni0kyqb5lfmg5vqvka3vqax2sn38niaxk7q7k")))

(define-public crate-fstab-0.4.0 (c (n "fstab") (v "0.4.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "083qi649064jxphskggg8dnc2xg1qirqcqgl8nmi5v0nhwij8c62")))

