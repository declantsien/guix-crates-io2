(define-module (crates-io fs ta fstapi) #:use-module (crates-io))

(define-public crate-fstapi-0.0.1 (c (n "fstapi") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0kxnh78w8qx94i0ga14myfyy2p7wm80xjd65xaqqn65c25nfibby")))

(define-public crate-fstapi-0.0.2 (c (n "fstapi") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)))) (h "102szzr4np9drpgw5kp7kvhgy0fjj7j99qz75qkqfymbjbmspcvm")))

