(define-module (crates-io fs ta fstab-generate) #:use-module (crates-io))

(define-public crate-fstab-generate-0.1.0 (c (n "fstab-generate") (v "0.1.0") (d (list (d (n "disk-types") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.1.2") (d #t) (k 0)))) (h "1mvrf2v5nlwyw8s6wx4hphwj8lrjq86x9z0mx47pmb60sm95637k")))

(define-public crate-fstab-generate-0.1.1 (c (n "fstab-generate") (v "0.1.1") (d (list (d (n "disk-types") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.1.6") (d #t) (k 0)))) (h "0znhydy4cc33kjnx66mlwakdp7k85r3f1wk0l13pp7mfp6v8d2ny")))

(define-public crate-fstab-generate-0.1.2 (c (n "fstab-generate") (v "0.1.2") (d (list (d (n "disk-types") (r "^0.1.0") (d #t) (k 0)) (d (n "partition-identity") (r "^0.2.0") (d #t) (k 0)))) (h "0nsldyg3a3n4l315symv42bi29gm5l9vwy3dg1ijjn4gx622qwp6")))

