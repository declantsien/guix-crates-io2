(define-module (crates-io fs tr fstrings-rust-proc-macro) #:use-module (crates-io))

(define-public crate-fstrings-rust-proc-macro-1.0.0 (c (n "fstrings-rust-proc-macro") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1j18dlam7bcf59f6672pp5vlbkjf6jrdx43b58kgja8n15qssaqi") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

