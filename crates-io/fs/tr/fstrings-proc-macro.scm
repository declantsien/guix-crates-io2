(define-module (crates-io fs tr fstrings-proc-macro) #:use-module (crates-io))

(define-public crate-fstrings-proc-macro-0.0.1 (c (n "fstrings-proc-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "1mf24lz5295nxm1x0hiw3swvmxdbbglfzkyzmf4zfyid241qn7hr") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1.0 (c (n "fstrings-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "0h4cqlr917v9p3jzj3m7ffwzavg8r47g2qyyzwy4w6lpp1w56113") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1.1 (c (n "fstrings-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "037xh4kcmaf04dapqnszmzk3irjg5sp4gki0f1i0dqp05afk0qz6") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1.2 (c (n "fstrings-proc-macro") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "1m0fcwkc7xajb69lp9lzw9zly7m28x6bcbzn263i1ahxjfiqx2d0") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1.3 (c (n "fstrings-proc-macro") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (d #t) (k 0)))) (h "0194a40yx16mxgy7dkjbl3k6igkmm17lm2hfz0ncnd9n4xqcxggc") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1.4 (c (n "fstrings-proc-macro") (v "0.1.4") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "17nkvqah5709l8fqzf4q5wdvykl62s7fpmwak0l1bxa66rwcldgx") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.0 (c (n "fstrings-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1rz8jc9s6zr6kk4pim39x98lv5nqa7h5zp8m9vyd7cw7gdsh9n8s") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.1 (c (n "fstrings-proc-macro") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0pp7w4k4fknwqdmdyjmmrrkqvf76xmbcmbif6z37zrvp88sf1y2z") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.2 (c (n "fstrings-proc-macro") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzi2929njw7xabs1apf2nvq7pb03xs0awddwnndsb0qy74vdsb1") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.3 (c (n "fstrings-proc-macro") (v "0.2.3") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "179dr49s3c3zlhl6gqlb0a63pbp5rf192a9ji93k7p41fl78rdb3") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.4-rc1 (c (n "fstrings-proc-macro") (v "0.2.4-rc1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "19mwj8hv1p7i08spajv5kx0sn8lgk0ih5lr7wwypv11b3parscri") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2.4-rc2 (c (n "fstrings-proc-macro") (v "0.2.4-rc2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "130ch104d8brxim4bia1f2b4l66bcam9x8fi13vknzv3ad69fb96") (f (quote (("verbose-expansions" "syn/extra-traits"))))))

