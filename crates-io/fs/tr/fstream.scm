(define-module (crates-io fs tr fstream) #:use-module (crates-io))

(define-public crate-fstream-0.1.0 (c (n "fstream") (v "0.1.0") (h "1vg73843g6spd17y5c0y1kl8ksh8f5n566daci0c490xw0v9y99k")))

(define-public crate-fstream-0.1.1 (c (n "fstream") (v "0.1.1") (h "1fwq0dq04a5imly67mcbbxi9rp9zf16vb4m37ww9af0xam6j9kn4")))

(define-public crate-fstream-0.1.2 (c (n "fstream") (v "0.1.2") (h "15p57mhy81ifsl4frqd1s9q0bmw48am0zcyan1xx2gcc6kdzhfzx")))

