(define-module (crates-io fs tr fstrings) #:use-module (crates-io))

(define-public crate-fstrings-0.0.1 (c (n "fstrings") (v "0.0.1") (d (list (d (n "proc-macro") (r "^0.0.1") (d #t) (k 0) (p "fstrings-proc-macro")))) (h "0vs96p78mns48psla7r3237h7jsi889iw80pzsk8x7azhyldwn9j") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions"))))))

(define-public crate-fstrings-0.1.0 (c (n "fstrings") (v "0.1.0") (d (list (d (n "proc-macro") (r "^0.1.0") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)))) (h "13266pbzgc3v5rklcfzd2ii4k2nn6khb2i9axjdgppy02lxki6kd") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1.1 (c (n "fstrings") (v "0.1.1") (d (list (d (n "proc-macro") (r "^0.1.1") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)))) (h "1q7w5fhj9fvba79685lcav523ycanypq2jnfh17v93hgrjkzkns6") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1.2 (c (n "fstrings") (v "0.1.2") (d (list (d (n "proc-macro") (r "^0.1.2") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)))) (h "09318lslwi9pkm37vzkq6pqam06ladmq8dgka6nrs7bqnmanhi61") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1.3 (c (n "fstrings") (v "0.1.3") (d (list (d (n "proc-macro") (r "^0.1.3") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)))) (h "01jnccslh96x2gkba9rafzv8kplgwp60sa206nbv024pq5gw1cjn") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1.4 (c (n "fstrings") (v "0.1.4") (d (list (d (n "proc-macro") (r "^0.1.4") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)))) (h "03vvc84w98s1b9v803xcybxd5rdh6jn4l2hjqigza9gllwvhg9f1") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.0 (c (n "fstrings") (v "0.2.0") (d (list (d (n "proc-macro") (r "^0.2.0") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1w35cjcqx6arlnrn3lry9xvlxvjwcrdfd19j19w5fbx8zxchb3m1") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.1 (c (n "fstrings") (v "0.2.1") (d (list (d (n "proc-macro") (r "^0.2.1") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0b0gr4gvsrlgzlz2l5fpks7la0svw1i9zkyrd3ayjm3a1jly1ksp") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.2 (c (n "fstrings") (v "0.2.2") (d (list (d (n "proc-macro") (r "^0.2.2") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "098bc6jj7a9c3lxxm418hk6ixlxqr9jcyxc0phpka8rddgz8945z") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.3 (c (n "fstrings") (v "0.2.3") (d (list (d (n "proc-macro") (r "^0.2.3") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1riaazfvqnr6193wa6afsfwghmxb5mhqc15dp8vaq1d5bpqs0ibq") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.4-rc1 (c (n "fstrings") (v "0.2.4-rc1") (d (list (d (n "proc-macro") (r "^0.2.4-rc1") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "00rsl9j7vd6c3kqkb3yx1xqm9ylh89rlb68frw3hq9xa4y07kl1d") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2.4-rc2 (c (n "fstrings") (v "0.2.4-rc2") (d (list (d (n "proc-macro") (r "^0.2.4-rc2") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "19vc95imdi5fwxf585d7cg51y645gk5sfn2rz63w8cxmklmkfg3k") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

