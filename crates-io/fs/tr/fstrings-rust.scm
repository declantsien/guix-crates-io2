(define-module (crates-io fs tr fstrings-rust) #:use-module (crates-io))

(define-public crate-fstrings-rust-0.2.2 (c (n "fstrings-rust") (v "0.2.2") (d (list (d (n "proc-macro") (r "^0.2.2") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0crnbk3d2cy2gn22aibzhk8sbg2w79959ciq124p072mvq6a8dzr") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-rust-1.0.0 (c (n "fstrings-rust") (v "1.0.0") (d (list (d (n "proc-macro") (r "^0.2.2") (d #t) (k 0) (p "fstrings-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1kg0h10x3jlhs2lhmri9sd3k5vrhjxr8payqgnz12xsf5m67ih4q") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-rust-1.0.1 (c (n "fstrings-rust") (v "1.0.1") (d (list (d (n "proc-macro") (r "^1.0.0") (d #t) (k 0) (p "fstrings-rust-proc-macro")) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0ahlczmh3s8xsy076kcqxnzi6wsvbgy9gha20ns6jgddvnkd9sl2") (f (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

