(define-module (crates-io fs t- fst-levenshtein) #:use-module (crates-io))

(define-public crate-fst-levenshtein-0.1.0 (c (n "fst-levenshtein") (v "0.1.0") (d (list (d (n "fst") (r "^0.2") (d #t) (k 0)) (d (n "utf8-ranges") (r "^0.1") (d #t) (k 0)))) (h "0y2c425wy9qdllxzb2nq2dd1llmkg7md3jn3k90aljkijpjcv008")))

(define-public crate-fst-levenshtein-0.2.0 (c (n "fst-levenshtein") (v "0.2.0") (d (list (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^0.1") (d #t) (k 0)))) (h "1j4lz65pn3ny9dimpiwxiv8p40yfpm5h6xp4xzxqmmwxavqjmwb4")))

(define-public crate-fst-levenshtein-0.2.1 (c (n "fst-levenshtein") (v "0.2.1") (d (list (d (n "fst") (r "^0.3.1") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "1s5ml10442bbnpmilmwjh4pfixsj6837rg68vjzg63i3djd4524y")))

(define-public crate-fst-levenshtein-0.3.0 (c (n "fst-levenshtein") (v "0.3.0") (d (list (d (n "fst") (r "^0.3.1") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "02ry64a0iz9nnxizydi03f0pixk5mv23imvcs2vcnv0hjqpy4gzv")))

