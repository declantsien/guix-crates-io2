(define-module (crates-io fs t- fst-subseq-ascii-caseless) #:use-module (crates-io))

(define-public crate-fst-subseq-ascii-caseless-0.1.0 (c (n "fst-subseq-ascii-caseless") (v "0.1.0") (d (list (d (n "fst") (r "^0.3") (d #t) (k 0)))) (h "0ixz0wnx9pv6alg7jpdyziiqydmxl5nf10z0p7da3306xv09xkvl")))

(define-public crate-fst-subseq-ascii-caseless-0.1.1 (c (n "fst-subseq-ascii-caseless") (v "0.1.1") (d (list (d (n "fst") (r ">= 0.3.0, < 0.5.0") (d #t) (k 0)))) (h "1b82y03bwvyi7iqjdkc1mnvd08iabfbiv3qbfms5vl85d1cpq1yk")))

