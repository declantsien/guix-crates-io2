(define-module (crates-io fs t- fst-no-std) #:use-module (crates-io))

(define-public crate-fst-no-std-0.4.7 (c (n "fst-no-std") (v "0.4.7") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "utf8-ranges") (r "^1.0.4") (o #t) (d #t) (k 0)))) (h "0fxacm1imha21zv9wm5865ci5csqxzg3xjw16z4mrbw57gcd2ka5") (f (quote (("std" "alloc") ("levenshtein" "utf8-ranges" "std") ("default" "std") ("alloc"))))))

(define-public crate-fst-no-std-0.4.8 (c (n "fst-no-std") (v "0.4.8") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "utf8-ranges") (r "^1.0.4") (o #t) (d #t) (k 0)))) (h "0qxpjf96qigm3792f62aylvr6aw72wr35m4c53fpfrzzzsgc8kf8") (f (quote (("std" "alloc") ("levenshtein" "utf8-ranges" "std") ("default" "std") ("alloc"))))))

