(define-module (crates-io fs t- fst-regex) #:use-module (crates-io))

(define-public crate-fst-regex-0.1.0 (c (n "fst-regex") (v "0.1.0") (d (list (d (n "fst") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^0.1") (d #t) (k 0)))) (h "1jd6sjfhcjdkjgh6fy5slvbczl1fnxnzwncg15ccr0wm1rnq76ph")))

(define-public crate-fst-regex-0.2.0 (c (n "fst-regex") (v "0.2.0") (d (list (d (n "fst") (r "^0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^0.1") (d #t) (k 0)))) (h "1v2zwb4qs0ir1k0i6w8hn4i8wvz1mqqd1dzn0f2ws6shkhg3q9mw")))

(define-public crate-fst-regex-0.2.1 (c (n "fst-regex") (v "0.2.1") (d (list (d (n "fst") (r "^0.3.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "0g5n6bxyfb8glm5ns2m8nzjksfx17ify6pwiwgjm8nv6jrsg0gcv")))

(define-public crate-fst-regex-0.2.2 (c (n "fst-regex") (v "0.2.2") (d (list (d (n "fst") (r "^0.3.1") (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "126xrv3s8mrq8nqsahmpy0nlks6l3wlivqyf6a0i4g7d3vcs3b47") (f (quote (("mmap" "fst/mmap") ("default" "mmap"))))))

(define-public crate-fst-regex-0.3.0 (c (n "fst-regex") (v "0.3.0") (d (list (d (n "fst") (r "^0.3.1") (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "1ca4f4va3rhyi50aivlrk2yc7jv71pz9dp0qq0xfkg4dm0aijrha")))

