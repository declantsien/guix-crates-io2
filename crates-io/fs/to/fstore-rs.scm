(define-module (crates-io fs to fstore-rs) #:use-module (crates-io))

(define-public crate-fstore-rs-0.1.0 (c (n "fstore-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "edit") (r "^0.1") (d #t) (k 0)) (d (n "glob-match") (r "^0.2") (d #t) (k 0)) (d (n "opener") (r "^0.6") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0zl0707lqxw8ba6v12pbz03xfx99hd4zkglmn80p4hwbwiq1dlkq") (y #t)))

