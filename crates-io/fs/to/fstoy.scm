(define-module (crates-io fs to fstoy) #:use-module (crates-io))

(define-public crate-fstoy-0.1.0 (c (n "fstoy") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "023542lzbd27w9lcmg3wck0qwnv33s9fh55lyrn84r4zdrcmp7vs")))

