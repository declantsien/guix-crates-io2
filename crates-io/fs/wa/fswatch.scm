(define-module (crates-io fs wa fswatch) #:use-module (crates-io))

(define-public crate-fswatch-0.1.10 (c (n "fswatch") (v "0.1.10") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "fswatch-sys") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0m98qa4w6wnmlrbiwmhxph5q2p48qbg4ii8cik6vq98myi2zvxf7") (f (quote (("use_time" "time") ("fswatch_1_10_0" "fswatch-sys/fswatch_1_10_0") ("default"))))))

