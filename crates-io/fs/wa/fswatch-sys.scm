(define-module (crates-io fs wa fswatch-sys) #:use-module (crates-io))

(define-public crate-fswatch-sys-0.1.0 (c (n "fswatch-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "046znhb1wsvnm3f0fa8fmicx6d2py47ayb85fz96drw8bz82jkag") (y #t)))

(define-public crate-fswatch-sys-0.1.1 (c (n "fswatch-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f66b72j0w89c4lyi461y9mpmmnwgp65m2bd99bkqnilj5xzqqqa") (y #t)))

(define-public crate-fswatch-sys-0.1.2 (c (n "fswatch-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03v0wrhwq4cxf566ak7m86vhqbcdiyzlz5kag2mjvkxx81gvlqzn")))

(define-public crate-fswatch-sys-0.1.3 (c (n "fswatch-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x4jdixkk9kznfmkzcifyw8w5vxgah5y3p3nws90i1xv24q6hq3w")))

(define-public crate-fswatch-sys-0.1.4 (c (n "fswatch-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14j79am3ncfijk5w3g4faxfdhyszin2s4sic4w47bwljfmcy6ffm")))

(define-public crate-fswatch-sys-0.1.5 (c (n "fswatch-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hnccmxklxp4gd485s0h0zf8q4n7q0k86fav3pgmnhf1ngxsqna9")))

(define-public crate-fswatch-sys-0.1.6 (c (n "fswatch-sys") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1f3b41pqwjx05jy1dpr6cnpfw8fap0h3adgq694bsrlhmqh3mr6g") (f (quote (("use_time" "time") ("default"))))))

(define-public crate-fswatch-sys-0.1.8 (c (n "fswatch-sys") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s44mqg7cfxz56sgqa5q0nw41y2fl7laa9bqdkl29zgz8qxdkg1d")))

(define-public crate-fswatch-sys-0.1.9 (c (n "fswatch-sys") (v "0.1.9") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mcsc5s86mb2dl3br8k3rhxgxnqv5gkmy3hb8fzaz51rfaclfycm") (f (quote (("fswatch_1_10_0"))))))

(define-public crate-fswatch-sys-0.1.10 (c (n "fswatch-sys") (v "0.1.10") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rphdp6z1rbcp5rl4wr83qh27yq1akm0y64qrcj6yyw6mwq28kvj") (f (quote (("fswatch_1_10_0"))))))

