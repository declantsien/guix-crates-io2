(define-module (crates-io fs ev fsevent) #:use-module (crates-io))

(define-public crate-fsevent-0.0.1 (c (n "fsevent") (v "0.0.1") (d (list (d (n "fsevent-sys") (r "~0.0.0") (d #t) (k 0)))) (h "1axd0hn9nhh1468vx187h1akps7z6jxzm5fg0sg9mpxls6w5dbf0")))

(define-public crate-fsevent-0.1.0 (c (n "fsevent") (v "0.1.0") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "137dl3ff3pffl3wifhcnp2m8s9hz95lk0dyflw1h8993b2ds85gh")))

(define-public crate-fsevent-0.2.1 (c (n "fsevent") (v "0.2.1") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "1hwmlkbgjl5lsmsbawdj6dgf5khskffb5qhqacp2ak0j5nd8bya0")))

(define-public crate-fsevent-0.2.2 (c (n "fsevent") (v "0.2.2") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "18ghjdkkjf7129yxpjwlszn7m2q76awmncqf8w0kkyl1n8b5wiqb")))

(define-public crate-fsevent-0.2.3 (c (n "fsevent") (v "0.2.3") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "16b8jsd0fz1x3gn834drnxmjmclwc22cvqs9k0qymxyj3yp4nnrx")))

(define-public crate-fsevent-0.2.4 (c (n "fsevent") (v "0.2.4") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "0ayyyjjj1p401z1l3qqfpdpl2pqb4a333sssj86l3n33xya01sai")))

(define-public crate-fsevent-0.2.5 (c (n "fsevent") (v "0.2.5") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "0riqzcn86p331wljqgydh61icm55r6iyjc253ab85igpllp0i6k0")))

(define-public crate-fsevent-0.2.6 (c (n "fsevent") (v "0.2.6") (d (list (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)))) (h "1ns9ll1d1hlma1y3n946g56d2h2arhqh9w81gxr20qc35wglflml")))

(define-public crate-fsevent-0.2.7 (c (n "fsevent") (v "0.2.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0zyyz822cv8ajy6wghr3amapxcja51nqjr3zp4ilb1k2pm7rlrsl")))

(define-public crate-fsevent-0.2.8 (c (n "fsevent") (v "0.2.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1akjjw8yygg5ryjvlxj6h9sj8yqldz264jny6id4df82y1c6nlm6")))

(define-public crate-fsevent-0.2.9 (c (n "fsevent") (v "0.2.9") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "17bqyh21n7i2ryavhwvq5bbq04ssxh5ixx9qhyx5qp1558hvrn3a")))

(define-public crate-fsevent-0.2.10 (c (n "fsevent") (v "0.2.10") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1r1ps23rwv9f4k2b7mz5vy60ij53b9x60az319hmxrjvf17w8276")))

(define-public crate-fsevent-0.2.11 (c (n "fsevent") (v "0.2.11") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ci6av3ndna1r42kv84bbv40gfbqg4wzsw8qxc0gdza7b653lahg")))

(define-public crate-fsevent-0.2.12 (c (n "fsevent") (v "0.2.12") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1bricqmmk7vkyjbjk564xlcax8adhb078awgk1f27425g8bzj1l5")))

(define-public crate-fsevent-0.2.13 (c (n "fsevent") (v "0.2.13") (d (list (d (n "bitflags") (r "~0.4.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.7") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "16c1ma39pf1jkqlisrxd6y4zzq6phgrn6hlw9hv0rj40d0rf6fry")))

(define-public crate-fsevent-0.2.14 (c (n "fsevent") (v "0.2.14") (d (list (d (n "bitflags") (r "~0.4.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.7") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "08yh7wj4s11nzfafgqw3sgnwr9ycbq41qm7j4236qb9vibwd99ii")))

(define-public crate-fsevent-0.2.15 (c (n "fsevent") (v "0.2.15") (d (list (d (n "bitflags") (r "~0.4.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.7") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "09d1f6gvx25f7fg6a9l381nsl81kvrahb5nrs1yxi0ckb35542kl")))

(define-public crate-fsevent-0.2.16 (c (n "fsevent") (v "0.2.16") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "0f1ccw2ccm3wkm4ipl26da7dl40bi6cnjhi5ic9q8s67rzmr7rfz")))

(define-public crate-fsevent-0.2.17 (c (n "fsevent") (v "0.2.17") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "01gcnn1ady8yskxlbfzx67mmxn732jn6amhb21vd1vjab1qvzfy4")))

(define-public crate-fsevent-0.2.18 (c (n "fsevent") (v "0.2.18") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "0gx7hzrkhq9pw12276x5njpxz22x3x16y4hc4h2bmd5rv93g7pkl") (y #t)))

(define-public crate-fsevent-0.3.0 (c (n "fsevent") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "18z822im7a1f3fdc5dbmbwvfg6r8jn5jdf4d5q781nhf7j5h716m")))

(define-public crate-fsevent-0.4.0 (c (n "fsevent") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "1djxnc2fmv265xqf1iyfz56smh13v9r1p0w9125wjg6k3fyx3dss")))

(define-public crate-fsevent-1.0.1 (c (n "fsevent") (v "1.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^2.0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "1f5zd75g7mzn2596sv9amkvwjgylwh65pjvq3ihkw89wbqmgdqvq")))

(define-public crate-fsevent-1.0.2 (c (n "fsevent") (v "1.0.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^2.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "10xbgwdbkx4hdq29w0s6qdn56l1cz2i33knyzhnx89gjm6djdgyf") (y #t)))

(define-public crate-fsevent-2.0.0 (c (n "fsevent") (v "2.0.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "1rvrr6765akbi25ccsj2v7am59jdj88hbz14wvkzhq8vj3cc4hrd")))

(define-public crate-fsevent-2.0.1 (c (n "fsevent") (v "2.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 2)))) (h "0jr3z49gm85qyqsxmy08pprdh8im19p33ky340bk37gl364yc5hn")))

(define-public crate-fsevent-2.0.2 (c (n "fsevent") (v "2.0.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^3.0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "0qsylfbhgha319q6a8yvkznbm3hf47gy3y8nq82qijcm5hh4gwwp")))

(define-public crate-fsevent-2.1.0 (c (n "fsevent") (v "2.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^3.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "1n6xr8yp66c9djwyy403c7zr5nx1pka8im3clc9qn0490k1j487w") (y #t)))

(define-public crate-fsevent-2.1.1 (c (n "fsevent") (v "2.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "113kxp97g15549d3z9dlba1wkpnivjxwk5327h3wlq2aarla0wkj")))

(define-public crate-fsevent-2.1.2 (c (n "fsevent") (v "2.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "fsevent-sys") (r "^4.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (d #t) (k 2)))) (h "0pvpz0n4yl64yvx3acchxnfd28vhx88x4pvsa6zrb8d08zqx2dl8")))

