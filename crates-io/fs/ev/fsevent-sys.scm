(define-module (crates-io fs ev fsevent-sys) #:use-module (crates-io))

(define-public crate-fsevent-sys-0.0.1 (c (n "fsevent-sys") (v "0.0.1") (h "19c80rdr6mmbjdqri78qb9cjwxwl8c1psdcqz48hzdjfaxrypmcp")))

(define-public crate-fsevent-sys-0.1.0 (c (n "fsevent-sys") (v "0.1.0") (h "04lxfixwlm69r13m96l3plgv5z85xp3i2833hw6kgsminhn5i7sz")))

(define-public crate-fsevent-sys-0.1.1 (c (n "fsevent-sys") (v "0.1.1") (h "07in73xcp4h8nwj5vvrl3zlkizvmml3b63z1klc7r4ri91hcmvjh")))

(define-public crate-fsevent-sys-0.1.2 (c (n "fsevent-sys") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "135sl9c7rl3aqy71ji4x88gwm7129nrz14pfdcgqzv6agmjlg1av")))

(define-public crate-fsevent-sys-0.1.3 (c (n "fsevent-sys") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1sbbmisy9w42sz5wim4bwd47b65xhsp0wczvi77gmvpdfi04hqss")))

(define-public crate-fsevent-sys-0.1.5 (c (n "fsevent-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.9") (d #t) (k 0)))) (h "1rpdhkjaz4w9mrni0pkrhx1cl9j3hqjs7hwm2nb2si06cf93mqvj")))

(define-public crate-fsevent-sys-0.1.6 (c (n "fsevent-sys") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0x180g33gq2qwv7fg36by0xh99zr2mps6xak5l1pml1qqcv2sxqs")))

(define-public crate-fsevent-sys-0.1.7 (c (n "fsevent-sys") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0hyf5ipy6a8ab1nvm4dj2cdbkqhgf65ns0jqqgay2z9sdihc4h2n") (y #t)))

(define-public crate-fsevent-sys-2.0.0 (c (n "fsevent-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1jw2gsz0smvw7x2sn4i1dfk0bl07jgzv2wzanwwp6ym85m7b0j9h")))

(define-public crate-fsevent-sys-2.0.1 (c (n "fsevent-sys") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "18246vxk7rqn52m0sfrhivxq802i34p2wqqx5zsa0pamjj5086zl")))

(define-public crate-fsevent-sys-2.1.0 (c (n "fsevent-sys") (v "2.1.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1accfi0w0fzqgfy60pxvlsbfbx9r9h3f5pk77x51rarrza5qh2kh") (y #t)))

(define-public crate-fsevent-sys-3.0.0 (c (n "fsevent-sys") (v "3.0.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1ys90v4w7rf4y03am0w7l6d5hb68s0vjy6cx89i2z1mzx4i1f7x4")))

(define-public crate-fsevent-sys-3.0.1 (c (n "fsevent-sys") (v "3.0.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "1vkxzrp65fvpf743bp820v5241v33iag3bj6swfxrczccvmgx60y")))

(define-public crate-fsevent-sys-3.0.2 (c (n "fsevent-sys") (v "3.0.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0821k4yac112iwwzqd4n5ml39dxbzi6d59d9fcz4qffay5vrr8kp")))

(define-public crate-fsevent-sys-3.1.0 (c (n "fsevent-sys") (v "3.1.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "1mav57d1zcp4x17h0wprcr188d8yvxfz1c0f1z0p31q52xl5wvya")))

(define-public crate-fsevent-sys-3.2.0 (c (n "fsevent-sys") (v "3.2.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "1gzsrr7mkanwzhajs46c9v9ap9nd9rm6bmjn0gr4aqxf5lxwzpm5") (y #t)))

(define-public crate-fsevent-sys-4.0.0 (c (n "fsevent-sys") (v "4.0.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "0jkkmvlbjrz2yimkw2l40q950dz2g1qvpwgg7c2kr66s4i6mc3jw")))

(define-public crate-fsevent-sys-4.1.0 (c (n "fsevent-sys") (v "4.1.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "1liz67v8b0gcs8r31vxkvm2jzgl9p14i78yfqx81c8sdv817mvkn")))

