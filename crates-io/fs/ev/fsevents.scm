(define-module (crates-io fs ev fsevents) #:use-module (crates-io))

(define-public crate-fsevents-0.1.0 (c (n "fsevents") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (d #t) (k 0)) (d (n "dispatch") (r "^0.2.0") (d #t) (k 0)) (d (n "fsevent-sys") (r "^4.1.0") (d #t) (k 0)))) (h "1ryskb31a46cqq03q5w0wq92qlcwg9ainb1nnlmsmgs3ps56alzm")))

