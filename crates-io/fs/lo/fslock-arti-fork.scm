(define-module (crates-io fs lo fslock-arti-fork) #:use-module (crates-io))

(define-public crate-fslock-arti-fork-0.2.0 (c (n "fslock-arti-fork") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "minwinbase" "winbase" "errhandlingapi" "winerror" "winnt" "synchapi" "handleapi" "fileapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1497acp6nxl30h0qpyyrr2h192990sg6vvqbn82bkdxad9ibs8cb") (f (quote (("std") ("default" "std"))))))

