(define-module (crates-io fs lo fslock-guard) #:use-module (crates-io))

(define-public crate-fslock-guard-0.1.0 (c (n "fslock-guard") (v "0.1.0") (d (list (d (n "fslock") (r "^0.2.0") (d #t) (k 0) (p "fslock-arti-fork")) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1s03f5zl648sfcp46m0bsbbhv2sqfh5dyvxfbd2jina387s11p3c") (f (quote (("full") ("default")))) (r "1.70")))

(define-public crate-fslock-guard-0.1.1 (c (n "fslock-guard") (v "0.1.1") (d (list (d (n "fslock") (r "^0.2.0") (d #t) (k 0) (p "fslock-arti-fork")) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "12blm23sqhxyay5iw9sx3lqhgh9inmbli4x2m6y408vg8mmcpa5j") (f (quote (("full") ("default")))) (r "1.70")))

(define-public crate-fslock-guard-0.1.2 (c (n "fslock-guard") (v "0.1.2") (d (list (d (n "fslock") (r "^0.2.0") (d #t) (k 0) (p "fslock-arti-fork")) (d (n "test-temp-dir") (r "^0.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1irjphqdvbahwn90xxlhishscyqvs9hicd7qlfq7g26x4crd0rwj") (f (quote (("full") ("default")))) (r "1.70")))

