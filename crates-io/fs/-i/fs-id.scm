(define-module (crates-io fs #{-i}# fs-id) #:use-module (crates-io))

(define-public crate-fs-id-0.1.0 (c (n "fs-id") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mlf5dx83hd71n3azr8i3ia4ya26pd5c97pq0mqs7q5szkwhdwvw") (r "1.66")))

(define-public crate-fs-id-0.2.0 (c (n "fs-id") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "wasi") (r "^0.11.0") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f7r7adnm5z7z0h4v1k3a60xgnyrph7yvhdmc4nh93g9i4dqqr7i") (r "1.66")))

