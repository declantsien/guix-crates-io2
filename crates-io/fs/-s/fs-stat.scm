(define-module (crates-io fs -s fs-stat) #:use-module (crates-io))

(define-public crate-fs-stat-0.1.3 (c (n "fs-stat") (v "0.1.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "12snbs7h9japwc69vs6ipwqcd6cm2v359pj8f41zrnj1xrnjb3i8") (y #t)))

(define-public crate-fs-stat-0.1.7 (c (n "fs-stat") (v "0.1.7") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1clyadyg3hk91swgchpv2m5ms82ih803kmng9zz546asd7cfy6z9") (y #t)))

