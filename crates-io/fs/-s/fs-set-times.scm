(define-module (crates-io fs -s fs-set-times) #:use-module (crates-io))

(define-public crate-fs-set-times-0.0.0 (c (n "fs-set-times") (v "0.0.0") (d (list (d (n "posish") (r "^0.0.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kx96jsz19kb7lzr0ciw7ixp1yhwv0xs7addhsd1ywc3svnpdk32")))

(define-public crate-fs-set-times-0.1.0 (c (n "fs-set-times") (v "0.1.0") (d (list (d (n "posish") (r "^0.2.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z4wffnq74rllyqgp6ssh6bkvsz326ab07scx8mlgy680zpkhscq")))

(define-public crate-fs-set-times-0.2.0 (c (n "fs-set-times") (v "0.2.0") (d (list (d (n "posish") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15sjks9iniwk26b0rqxvba9p21cd8b1xdhcby2d4cl190rl56brk")))

(define-public crate-fs-set-times-0.2.1 (c (n "fs-set-times") (v "0.2.1") (d (list (d (n "posish") (r "^0.4.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b9rnk97v6ahdnsc5f1gcxpbam576kcw9jan5bfr6c4knwgc2dsa")))

(define-public crate-fs-set-times-0.2.2 (c (n "fs-set-times") (v "0.2.2") (d (list (d (n "posish") (r "^0.5.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w8xbn90vvdqh1ddgywgygbz62z7m7hjxghjywq14qkkdxigddh8")))

(define-public crate-fs-set-times-0.2.3 (c (n "fs-set-times") (v "0.2.3") (d (list (d (n "posish") (r "^0.5.4") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0367gfb053xx8knqllj2r495npwajznlhwc0q1n5bd14bmp0yw70")))

(define-public crate-fs-set-times-0.2.4 (c (n "fs-set-times") (v "0.2.4") (d (list (d (n "posish") (r "^0.5.6") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "unsafe-io") (r "^0.5.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mqca6n5idxfi72rs22s3xlj5qzszfv91jcfjx5w0mrp9czgs350")))

(define-public crate-fs-set-times-0.3.0 (c (n "fs-set-times") (v "0.3.0") (d (list (d (n "posish") (r "^0.6.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "unsafe-io") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n36z77wn6ycb2rq37949vappawkxl1j0qn367z2lqimhy48b09y")))

(define-public crate-fs-set-times-0.3.1 (c (n "fs-set-times") (v "0.3.1") (d (list (d (n "posish") (r "^0.6.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "unsafe-io") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mby11k8vfcdb7rch542w1ibk46jcv2ccz861ivsbfqpyl0wmw98")))

(define-public crate-fs-set-times-0.4.0 (c (n "fs-set-times") (v "0.4.0") (d (list (d (n "io-lifetimes") (r "^0.1.1") (d #t) (k 0)) (d (n "posish") (r "^0.13.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y1sxwj4v86s58i138zq4kklz52b13lkldjyy14h2g48vz9innjz")))

(define-public crate-fs-set-times-0.5.0 (c (n "fs-set-times") (v "0.5.0") (d (list (d (n "io-lifetimes") (r "^0.2.0") (d #t) (k 0)) (d (n "posish") (r "^0.14.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b35q5d5g82dlz88xg0rdcmbppkbn4asgw3mc5rdm2xiqizwf05v")))

(define-public crate-fs-set-times-0.5.1 (c (n "fs-set-times") (v "0.5.1") (d (list (d (n "io-lifetimes") (r "^0.2.0") (d #t) (k 0)) (d (n "posish") (r "^0.15.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vxh8r51kzqgwkhv0917hmswcszn8f3z0f5ldhcqqmr89hqg0i0d")))

(define-public crate-fs-set-times-0.6.0 (c (n "fs-set-times") (v "0.6.0") (d (list (d (n "io-lifetimes") (r "^0.2.0") (d #t) (k 0)) (d (n "posish") (r "^0.16.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10prmnvx8hrsv6n9yazlqrysw22zxmf79bafaxjax7szw3d21bsn")))

(define-public crate-fs-set-times-0.7.0 (c (n "fs-set-times") (v "0.7.0") (d (list (d (n "io-lifetimes") (r "^0.2.0") (d #t) (k 0)) (d (n "rsix") (r "^0.18.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06wli38lzmhrx0k2bv9bzah8ljj953v825kdcyw985gkim5hxz3s")))

(define-public crate-fs-set-times-0.7.1 (c (n "fs-set-times") (v "0.7.1") (d (list (d (n "io-lifetimes") (r "^0.2.2") (k 0)) (d (n "rsix") (r "^0.18.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rrma3zc5aid70kc722vhlan1ifzfy5xjziyvk4yj8g3xz78vy3y")))

(define-public crate-fs-set-times-0.8.0 (c (n "fs-set-times") (v "0.8.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.19.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h2nz8avlry1as89w3ifvw6s86hlk5q5bpx8i69xz799x1avri71")))

(define-public crate-fs-set-times-0.9.0 (c (n "fs-set-times") (v "0.9.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.20.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gysabpw7968jh92qrj02nsi21m6wyk0kyvik2zcqd78fpb7812p")))

(define-public crate-fs-set-times-0.10.0 (c (n "fs-set-times") (v "0.10.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.21.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ypcra0fv481lljnnw9bks6a35fi8vwwfbpjs9z0326n48w8rrry")))

(define-public crate-fs-set-times-0.11.0 (c (n "fs-set-times") (v "0.11.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.22.4") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zsc58ljg5w1zrp8i4k5gjzs11rglxx2208prprxkxzgmk29lpxh")))

(define-public crate-fs-set-times-0.12.0 (c (n "fs-set-times") (v "0.12.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.23.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j7bw1mk31js905xq9ppz5xxj1p087h0lmmxpscf9yq4vvq3wzl0")))

(define-public crate-fs-set-times-0.12.1 (c (n "fs-set-times") (v "0.12.1") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.24.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1q8ra6f84wlg1maivbj4wm2lmkv0lndbs7xkyrx00vgrby7ymfd7")))

(define-public crate-fs-set-times-0.12.2 (c (n "fs-set-times") (v "0.12.2") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rsix") (r "^0.25.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zqavcdcbi4szyhbg1q964083f9cjzr9nlmr3kf4izzyi4nr19nr")))

(define-public crate-fs-set-times-0.12.3 (c (n "fs-set-times") (v "0.12.3") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rustix") (r "^0.26.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13iy8hvlqz1k3ys9wx1j683pqaif7p4zrzq7cvxlckvb226q50yi") (y #t)))

(define-public crate-fs-set-times-0.13.0 (c (n "fs-set-times") (v "0.13.0") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rustix") (r "^0.26.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j0lghpqf6hlmlq68gmqsxcqr86cn2y25czn8zxkf3gpmkf748by")))

(define-public crate-fs-set-times-0.13.1 (c (n "fs-set-times") (v "0.13.1") (d (list (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "rustix") (r "^0.26.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vw1pmr3l9hdb8qprwkxz5pr3nc87qq5r53ax5y5csp3x188k0xa")))

(define-public crate-fs-set-times-0.14.0 (c (n "fs-set-times") (v "0.14.0") (d (list (d (n "io-lifetimes") (r "^0.4.0") (k 0)) (d (n "rustix") (r "^0.27.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nd53dwzpjx7aaxmba6j9kdbarw3f1sgn0d860km20a4wjvbqlp7")))

(define-public crate-fs-set-times-0.14.1 (c (n "fs-set-times") (v "0.14.1") (d (list (d (n "io-lifetimes") (r "^0.4.0") (k 0)) (d (n "rustix") (r "^0.31.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w887n998ksvmmmlxz8rp67kirzy06za179wvasbcw60k59ggsrl")))

(define-public crate-fs-set-times-0.14.2 (c (n "fs-set-times") (v "0.14.2") (d (list (d (n "io-lifetimes") (r "^0.4.0") (k 0)) (d (n "rustix") (r "^0.32.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13wmiqpljscrv3x8yjc99qb14yfa93mv84n9fb3cp2jq1vgrdi20")))

(define-public crate-fs-set-times-0.15.0 (c (n "fs-set-times") (v "0.15.0") (d (list (d (n "io-lifetimes") (r "^0.5.1") (k 0)) (d (n "rustix") (r "^0.33.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1aljif8wqgg9w5dsv2xr6r3ddv031lzmlysnipm35mg2dvk2xxkx")))

(define-public crate-fs-set-times-0.16.0 (c (n "fs-set-times") (v "0.16.0") (d (list (d (n "io-lifetimes") (r "^0.6.0") (k 0)) (d (n "rustix") (r "^0.34.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ka9cr18ls0z245mi7lswqjlpi2fy7as97pya29ax9xsvajqii5g")))

(define-public crate-fs-set-times-0.17.0 (c (n "fs-set-times") (v "0.17.0") (d (list (d (n "io-lifetimes") (r "^0.7.0") (k 0)) (d (n "rustix") (r "^0.35.6") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xk5f23s73ypl4lc5wqif9yxz1i44bvz4pws6w8ysqchf4jrsjil")))

(define-public crate-fs-set-times-0.18.0-beta.0 (c (n "fs-set-times") (v "0.18.0-beta.0") (d (list (d (n "io-lifetimes") (r "^0.7.0") (k 0)) (d (n "rustix") (r "^0.35.6") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q8yymf09i2yvsrfbdvy4pnfhk3f8ps4zbf08srgff0lpik7wk0i") (y #t)))

(define-public crate-fs-set-times-0.17.1 (c (n "fs-set-times") (v "0.17.1") (d (list (d (n "io-lifetimes") (r "^0.7.0") (k 0)) (d (n "rustix") (r "^0.35.6") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v0svvfz6n9s5m6xp18f2q5m72sx243zwlqdc4cf04j962lvcrx2")))

(define-public crate-fs-set-times-0.18.0-rc0 (c (n "fs-set-times") (v "0.18.0-rc0") (d (list (d (n "io-lifetimes") (r "^1.0.0-rc0") (k 0)) (d (n "rustix") (r "^0.36.0-rc0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0npvhjjw5jy91z8j1bddn4bygswmfq019s48fryi1aspplx6rnma")))

(define-public crate-fs-set-times-0.18.0-rc1 (c (n "fs-set-times") (v "0.18.0-rc1") (d (list (d (n "io-lifetimes") (r "^1.0.0-rc1") (k 0)) (d (n "rustix") (r "^0.36.0-rc1") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14qv3pzm67zy5sx02iyxdy2djjni7d0cs9d7w0r7k9g57q1ciabj")))

(define-public crate-fs-set-times-0.18.0 (c (n "fs-set-times") (v "0.18.0") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x0spbglakmbdgwd3k0p1mkkn5g661rm4089rrwlc58101ms4p72")))

(define-public crate-fs-set-times-0.18.1 (c (n "fs-set-times") (v "0.18.1") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1m2ggajdrb5crqx12xlq52xkblvkjl0m8aabv0v2lv5jvizg4z45")))

(define-public crate-fs-set-times-0.18.2 (c (n "fs-set-times") (v "0.18.2") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zvkqp6i4yhddgpd6wxwdnm91siw95rgh61sm6m4shl43wpwavlm") (y #t)))

(define-public crate-fs-set-times-0.19.0 (c (n "fs-set-times") (v "0.19.0") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0518xh43jwrkqvdavfh6n2dgwb1nqzacai9rbdd9k5pm1l3379xv")))

(define-public crate-fs-set-times-0.19.1 (c (n "fs-set-times") (v "0.19.1") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qc822hifnwvrfd706wnx5xhajqfsc4kn2lmalfda4x02pqx0cvq")))

(define-public crate-fs-set-times-0.19.2 (c (n "fs-set-times") (v "0.19.2") (d (list (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07w0y35syfdari7wmw5s7ial4qnrrx2wc2mmlvyshsw7d9j7n5kd")))

(define-public crate-fs-set-times-0.20.0 (c (n "fs-set-times") (v "0.20.0") (d (list (d (n "io-lifetimes") (r "^2.0.0") (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ffgrlwhf4bmcnqmc2ha7xvsbd2rcgbxxshlhig08522i628nwyx")))

(define-public crate-fs-set-times-0.20.1 (c (n "fs-set-times") (v "0.20.1") (d (list (d (n "io-lifetimes") (r "^2.0.0") (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs" "time"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yxqkl8khk1593ribn4s9v60vf375gi9sgw9hq6nk5svf9yk6fq3")))

