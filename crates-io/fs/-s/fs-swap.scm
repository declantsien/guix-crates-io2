(define-module (crates-io fs -s fs-swap) #:use-module (crates-io))

(define-public crate-fs-swap-0.1.0 (c (n "fs-swap") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12381i93nfg6v2zj9bzd4hh6jp486467h6m6nmcm1lri1fzfv8ca")))

(define-public crate-fs-swap-0.2.0 (c (n "fs-swap") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bd07qsdrw6xqm2gaai1zm7x6fd2mg7bk108b409y4n4pqbciij1")))

(define-public crate-fs-swap-0.2.1 (c (n "fs-swap") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zbka1m14lhm003yiaqcycv0jnl880wi8wkdjvv8qzsa28p2riqq")))

(define-public crate-fs-swap-0.2.2 (c (n "fs-swap") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xkhhfxwqxdmmmdpw4ar7i97h2801d526rz7kp2dsfp50ya4xa9i")))

(define-public crate-fs-swap-0.2.3 (c (n "fs-swap") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libloading") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qkyp9vz9056dd4f9r64cqr31mcsild3scm4cj3n59pqlnr1dy37")))

(define-public crate-fs-swap-0.2.4 (c (n "fs-swap") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libloading") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fvigbsp3gc1bl71ppna0rlca0kfbgp63373dn11mdmki4n367cj")))

(define-public crate-fs-swap-0.2.5 (c (n "fs-swap") (v "0.2.5") (d (list (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r ">=0.2.4, <0.3.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "libloading") (r ">=0.5.0, <0.6.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tempdir") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "winapi") (r ">=0.3.4, <0.4.0") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b7wzjxc659z1vw2js7hwhwiz3v5rbaiviw7kj8s8k728yigsfaq")))

(define-public crate-fs-swap-0.2.6 (c (n "fs-swap") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.90") (d #t) (t "cfg(unix)") (k 0)) (d (n "libloading") (r "^0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("ktmw32" "handleapi" "fileapi" "errhandlingapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "198hbxamj80ffwql65nvbvm6aw82sp9wm1l993cfxv456snpvm03")))

