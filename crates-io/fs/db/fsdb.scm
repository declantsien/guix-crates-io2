(define-module (crates-io fs db fsdb) #:use-module (crates-io))

(define-public crate-fsdb-0.1.0 (c (n "fsdb") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z3bnc08sz3ic6rym32nkbzrp5qajh8fldk2zlinsqr7xdad7iji")))

(define-public crate-fsdb-0.1.1 (c (n "fsdb") (v "0.1.1") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "111g0g93w8ihjsccbk7plbc9v54psby70dl842bdp09wpv9va2vw")))

(define-public crate-fsdb-0.1.2 (c (n "fsdb") (v "0.1.2") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qy3d3nifxpdn5sriy0l93h0aqjp5fzs8zspjlijkvq5cgszhcwx")))

(define-public crate-fsdb-0.1.3 (c (n "fsdb") (v "0.1.3") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lpgllymazah4alhc9pq81p3sx4gbyw101mg606z9rwnxyfjrq2q")))

(define-public crate-fsdb-0.1.4 (c (n "fsdb") (v "0.1.4") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0cvwg3lh6xbzfpbfazdvglbskvval17vflzlsgf5l35ly938kncb")))

(define-public crate-fsdb-0.1.5 (c (n "fsdb") (v "0.1.5") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "010aphaizfwdbzha9skzy7cl58zzghgci5prx2cf4caap6byv8ff")))

(define-public crate-fsdb-0.1.6 (c (n "fsdb") (v "0.1.6") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0z1ks9wlrjgnfvri3z4g5z6wq6s5lcflbsym852ln34j0yskl50d")))

(define-public crate-fsdb-0.1.7 (c (n "fsdb") (v "0.1.7") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "04hlw4yicmpwix95p6yzmgcmvlsippxkjy3xkc4bm2lzmhrcb7yj")))

(define-public crate-fsdb-0.1.8 (c (n "fsdb") (v "0.1.8") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d8ad9xs0zp7zn7chncv63n27lrjfhmpsphihnbz4r80hs34y414")))

(define-public crate-fsdb-0.1.9 (c (n "fsdb") (v "0.1.9") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "119swypqk8238v18kz1hcxywmac6gqx5d2m95b3wx02bn9pi8cjs")))

(define-public crate-fsdb-0.1.10 (c (n "fsdb") (v "0.1.10") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ww6mmj8wd4mbq9ccy0c7sibrp69kcdfiaf0d2l4xf3yyzp5ksj1")))

(define-public crate-fsdb-0.1.11 (c (n "fsdb") (v "0.1.11") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0s676vw63ndhyjrlsvlm21mhjd8jqirdmxfgbvpzg1qb90izaiam")))

(define-public crate-fsdb-0.1.12 (c (n "fsdb") (v "0.1.12") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1681r0p1xkj37s4199b87rl4kqgdxc86q1zmi32a0nbxlv1fhd6y")))

(define-public crate-fsdb-0.1.13 (c (n "fsdb") (v "0.1.13") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0is1yrqw7bjxd4zfca2a7gzm9mrn2xkz70pfz71igf1c749aaky4")))

(define-public crate-fsdb-0.1.14 (c (n "fsdb") (v "0.1.14") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l4gjz4dbw0mijc0m3325pz0sshpiyzv0s3i8pyva4ljv55vkby5")))

(define-public crate-fsdb-0.1.15 (c (n "fsdb") (v "0.1.15") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0b9wpvzp068crvbszcfi7n06vb97n4nx2cxv7asl0wvwn3r5djwz")))

(define-public crate-fsdb-0.1.16 (c (n "fsdb") (v "0.1.16") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01sj4wl5hn4zk6x7j13ixvydd822n4sh3y4bqcasi9cycpd82rxv")))

(define-public crate-fsdb-0.1.17 (c (n "fsdb") (v "0.1.17") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0385vq0z5sbxr334g9bll5hzq3kqzqdsylrw6y97764c5kjxd322")))

(define-public crate-fsdb-0.1.18 (c (n "fsdb") (v "0.1.18") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0w7s7bci1a2la4ny782fxxl5a2rfifw4w3z91m6z9r6b1jrq54xv")))

