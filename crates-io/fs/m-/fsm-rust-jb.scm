(define-module (crates-io fs m- fsm-rust-jb) #:use-module (crates-io))

(define-public crate-fsm-rust-jb-0.1.1 (c (n "fsm-rust-jb") (v "0.1.1") (h "1irik393ln8c32qciyrlcvm1rqwf0yc35wb7d78g7cxavw1l6lwx")))

(define-public crate-fsm-rust-jb-0.1.2 (c (n "fsm-rust-jb") (v "0.1.2") (h "17min9r0xkbvbf70dhix9ygv9v6v7wwyz73ygvgh3z5dqb6vsjg5")))

(define-public crate-fsm-rust-jb-0.1.3 (c (n "fsm-rust-jb") (v "0.1.3") (h "0z7ms2rhlrgjjp4vskqvqnyghh68fyf1sizv1m95f1l8nnkisdpg")))

