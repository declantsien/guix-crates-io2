(define-module (crates-io fs m- fsm-macro) #:use-module (crates-io))

(define-public crate-fsm-macro-0.1.0 (c (n "fsm-macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 0)))) (h "13kr7fl0jkn7mgk6qz3nayaik0p2qnbfg604yxhx3843fx77xc96")))

