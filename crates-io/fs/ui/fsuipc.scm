(define-module (crates-io fs ui fsuipc) #:use-module (crates-io))

(define-public crate-fsuipc-0.1.0 (c (n "fsuipc") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "05djiaq7f6m15xp73zzls048fv6i61841s9d8ags8yh1jgkd0iz8")))

(define-public crate-fsuipc-0.2.0 (c (n "fsuipc") (v "0.2.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "095kxdan40l9dnfmz5hwxxk5smgkm1jqr8y8kj8dbggfpa5d04b1")))

(define-public crate-fsuipc-0.3.0 (c (n "fsuipc") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "08lgcnx0mhgivxvbvbigr97p8ncvbbf28af8pf3nla81aqm96h1k")))

(define-public crate-fsuipc-0.4.0 (c (n "fsuipc") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1cv60jhi1kxcvy2y8h5vil987qf8ycnzsqxj31c0qx3ii4lnhnrz")))

(define-public crate-fsuipc-0.5.0 (c (n "fsuipc") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "14nr4gg5zka8ylsbxq73fx0wrxp67k8g98pjvd037yr1d4ag8ds6")))

