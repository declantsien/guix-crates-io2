(define-module (crates-io fs -l fs-lock) #:use-module (crates-io))

(define-public crate-fs-lock-0.1.0 (c (n "fs-lock") (v "0.1.0") (d (list (d (n "fs4") (r "^0.6.2") (d #t) (k 0)))) (h "0958364wgmfl3ypbrdg585y62j8s5gq8pch6c38hq25cqbvdyxy7") (r "1.61.0")))

(define-public crate-fs-lock-0.1.1 (c (n "fs-lock") (v "0.1.1") (d (list (d (n "fs4") (r "^0.7.0") (d #t) (k 0)))) (h "0k5hz2082gl5zya4n3ln8sk9wd8gwi95lcxmylg7lizk5z9w3vx0") (r "1.61.0")))

(define-public crate-fs-lock-0.1.2 (c (n "fs-lock") (v "0.1.2") (d (list (d (n "fs4") (r "^0.7.0") (d #t) (k 0)))) (h "08sdh92vpp62n6llksyyxql7sbrihj6pi5hwv900l30fsi52nrqa") (r "1.61.0")))

(define-public crate-fs-lock-0.1.3 (c (n "fs-lock") (v "0.1.3") (d (list (d (n "fs4") (r "^0.8.1") (d #t) (k 0)))) (h "00ckps3mk7i5x9dncdqb6ds9vqjhd2npvp8qyy4jqfzag20f3wp4") (r "1.61.0")))

