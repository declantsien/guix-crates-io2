(define-module (crates-io fs t_ fst_stringstring) #:use-module (crates-io))

(define-public crate-fst_stringstring-0.0.1 (c (n "fst_stringstring") (v "0.0.1") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)))) (h "15izyk2nrn53z58zcm48bs2xgx3sd2iqclkn5l1yzlr1i6fjhy9z")))

(define-public crate-fst_stringstring-0.0.2 (c (n "fst_stringstring") (v "0.0.2") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)))) (h "1amrz1l1a2n1z7hdyba5ph9p79jjsklnamkpi359kzq7503qvnar")))

(define-public crate-fst_stringstring-0.0.3 (c (n "fst_stringstring") (v "0.0.3") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)))) (h "1b8wsav9m7rhjv4kwnjh18rn16fj4swry3bzda2a294wkdsvpaa4")))

(define-public crate-fst_stringstring-0.0.4 (c (n "fst_stringstring") (v "0.0.4") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1qliwl4jxihjpk1h1f1pi6ianm3afdsspjr2kif8yaj21bzy03ym")))

(define-public crate-fst_stringstring-0.0.5 (c (n "fst_stringstring") (v "0.0.5") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0z1n4p7lsr7hd5cl32i5x43a46zv15c832lwyib9sq4hm8xmy102")))

(define-public crate-fst_stringstring-0.0.6 (c (n "fst_stringstring") (v "0.0.6") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "13d1yds2shlhjglz6yln402zbj2ifll0ilpyns1x58g21v0a3mlm")))

(define-public crate-fst_stringstring-0.0.7 (c (n "fst_stringstring") (v "0.0.7") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0n4l8msgra06lk5xjjpz976g292zjxsdxc4h839mc7vghvxqsxjq")))

(define-public crate-fst_stringstring-0.0.8 (c (n "fst_stringstring") (v "0.0.8") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1dyxsm8403njqpr6c981sb2ijkmi66vw41si8vx0idi1hrxk0dw9")))

(define-public crate-fst_stringstring-0.0.9 (c (n "fst_stringstring") (v "0.0.9") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0w90a0wq3xz4125lvgpszharyi58ycahmhrsrfyapga6qkp4fawc")))

(define-public crate-fst_stringstring-0.0.10 (c (n "fst_stringstring") (v "0.0.10") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0vjf61bc3wh5n4832vgh8ih87da0jfsnxgcz705q8kcibxha78gj")))

