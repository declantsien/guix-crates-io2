(define-module (crates-io fs er fserv) #:use-module (crates-io))

(define-public crate-fserv-0.1.0 (c (n "fserv") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.2") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0zrmhdls25q61vy9cs33whqvdmswz9cqxx4rycwz3c595cbkggs6")))

(define-public crate-fserv-0.1.1 (c (n "fserv") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.2") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1cy77jcg82j989p4nk6474m97pr6ywrhwqbswh5nd558ndjvj7zv")))

