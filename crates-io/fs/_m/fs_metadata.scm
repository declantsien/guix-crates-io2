(define-module (crates-io fs _m fs_metadata) #:use-module (crates-io))

(define-public crate-fs_metadata-0.1.0 (c (n "fs_metadata") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1wsrxxwwf9pzq5f2vh5i1vxbg63kbpgd8jax7kcjp8pmy48np9gg")))

(define-public crate-fs_metadata-0.2.0 (c (n "fs_metadata") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1d2y4fld7lkzh65lraqggjcjiqyicxslmzi2m57qmy0ngvmf0vci")))

(define-public crate-fs_metadata-0.3.0 (c (n "fs_metadata") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)))) (h "0jk48hgmykx1z574vwny0n9y1s0bqp9b1b48cmg51jqb7l2qlhkv")))

(define-public crate-fs_metadata-0.3.1 (c (n "fs_metadata") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "faccess") (r "^0.2.4") (d #t) (k 0)))) (h "04vvlfkmzy11g7wngfkakrh5b5gbkqcc5wd1ja4xdcr8glgvysdc")))

