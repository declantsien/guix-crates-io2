(define-module (crates-io fs #{10}# fs1027-dg-hal) #:use-module (crates-io))

(define-public crate-fs1027-dg-hal-0.1.0 (c (n "fs1027-dg-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1c0sg1dkii3r39hf31c7z7nc103kja95fkc2yp22hna6vvxqa8iy")))

(define-public crate-fs1027-dg-hal-0.1.1 (c (n "fs1027-dg-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0wvsgb3vprds9a53xd804f264p314gac361ps48r31y7s1l360rq")))

(define-public crate-fs1027-dg-hal-0.1.2 (c (n "fs1027-dg-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0clk7bdwfvf19clhjm74wv82ndzz46aylwqdhpk4w3hmp5b0f6b8")))

(define-public crate-fs1027-dg-hal-0.2.0 (c (n "fs1027-dg-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1wmgfmk4mv920lvbz8n2jqkldgbbnjmr49bavdxvd2blmrsl9zi5")))

