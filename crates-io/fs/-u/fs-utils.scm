(define-module (crates-io fs -u fs-utils) #:use-module (crates-io))

(define-public crate-fs-utils-0.1.0 (c (n "fs-utils") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19aa48qiamaznxk9f4dh5szxqfss93qvcslaa0af5hpyn44mgkjw")))

(define-public crate-fs-utils-0.1.1 (c (n "fs-utils") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1bmf9hpnsxsr2w3b7kkjakmm56dk44z016sj5lxkcrbq174dng5i")))

(define-public crate-fs-utils-1.0.0 (c (n "fs-utils") (v "1.0.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xmzrg22i835hcj3w7gdz58xgw8c4276khh3n2l9vasnqwyxclxy")))

(define-public crate-fs-utils-1.1.0 (c (n "fs-utils") (v "1.1.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0yipx40h4mz0mivfifgaqxy7w8yimxdd1i5lwizf2y3ysx40dcgq")))

(define-public crate-fs-utils-1.1.1 (c (n "fs-utils") (v "1.1.1") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0q6fxabdyhqdb48p5d1ks5mp9hph06gkr02h7afgrvhvxbq6j3iv")))

(define-public crate-fs-utils-1.1.3 (c (n "fs-utils") (v "1.1.3") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0jiwczipi8hp30mc08y0aymr6r9li8firvxc1icc6bv1i6wq9ib2")))

(define-public crate-fs-utils-1.1.4 (c (n "fs-utils") (v "1.1.4") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "14r5wl14mz227v0lpy89lvjzfnxgdxigvrrmm6c4r52w03fakivg")))

