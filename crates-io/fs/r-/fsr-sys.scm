(define-module (crates-io fs r- fsr-sys) #:use-module (crates-io))

(define-public crate-fsr-sys-0.1.0 (c (n "fsr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0r7aig50jb3vvx6mwpj3cwi9fy2palc95mx18j5yhr68m14l6x6f") (f (quote (("vulkan") ("d3d12")))) (y #t)))

(define-public crate-fsr-sys-0.1.1 (c (n "fsr-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1ba0skybih0a0xr4q4r2kasl4ap0v5g21fli7x54yqh661bxbjs0") (f (quote (("vulkan") ("d3d12"))))))

(define-public crate-fsr-sys-0.1.2 (c (n "fsr-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0nlnrnggwqlb6n1g94vnfjkhvscqjjvng3g9dgqld3n1mn81q31x") (f (quote (("vulkan") ("d3d12"))))))

(define-public crate-fsr-sys-0.1.3 (c (n "fsr-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1bjrz460n1pb1nl2ibyx48jy832gz4sq19ljbw38s2n9x68wzsbj") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.4 (c (n "fsr-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "0qhmbj65s2cvmm4lnni7h365lg2yn2fhclxa2qfqw8yb9ps9gjpy") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.5 (c (n "fsr-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1hzrjkfl3b65bjlgk740rf0l003grmqr3mphwv2if5skaw799hvl") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.6 (c (n "fsr-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1369skcscp42qig924dnimvjkakmkh126wm82i25ksnhag2ggwzg") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.7 (c (n "fsr-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1kczjhbw8zhzwlsbnb2d8jbh3q8bqblvx28wvcbmzlgz2d5zkyf4") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.8 (c (n "fsr-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "0yd1248m7mnri9bxj5kl21hi2a90jldvvabihha6wgi5kzvj55gg") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.9 (c (n "fsr-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1s61s1gv4lslc5rrnmsjyv67vn9yiga58qy3bvyrwawbm34x01hy") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.10 (c (n "fsr-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1a7s0zs5glmxhcvci433mc8x1p9135jvbd7xk3103d1l6bqvfsg4") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

(define-public crate-fsr-sys-0.1.11 (c (n "fsr-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "12fw2q9q6diwimqzc1lrggcqccjjafvf23jk8494a284dcb1wccd") (f (quote (("vulkan") ("d3d12")))) (s 2) (e (quote (("generate-bindings" "dep:bindgen"))))))

