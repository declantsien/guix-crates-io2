(define-module (crates-io fs -t fs-tracing) #:use-module (crates-io))

(define-public crate-fs-tracing-0.1.0 (c (n "fs-tracing") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5.10") (d #t) (k 2)) (d (n "tracing") (r "^0.1.23") (d #t) (k 0)) (d (n "tracing-error") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.15") (d #t) (k 2)))) (h "1d26758vd2882f3iw2j3673w7p083g4z6cbpa4acwj26ymrc36yw")))

