(define-module (crates-io fs -t fs-tree) #:use-module (crates-io))

(define-public crate-fs-tree-0.1.0 (c (n "fs-tree") (v "0.1.0") (h "0b07h6wgz6r6pbfi1fv6k4yjq4k2683kmwsd0caaffj73m7ndb0x")))

(define-public crate-fs-tree-0.1.1 (c (n "fs-tree") (v "0.1.1") (h "0zm5729dl39lqfnz1s009qjdbm4cyi0ip0ld1lah3hvww5akm5cd")))

(define-public crate-fs-tree-0.1.2 (c (n "fs-tree") (v "0.1.2") (h "08fryaggp7fxhp62r9yzzikzp0mz0dkpy53lyqfrxshaiwflkqcp")))

(define-public crate-fs-tree-0.1.3 (c (n "fs-tree") (v "0.1.3") (h "1cwww67cafcs5fv174p2psiygpcdw6adlmhi5c2prjrkzww7c5ry")))

(define-public crate-fs-tree-0.2.0 (c (n "fs-tree") (v "0.2.0") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)))) (h "0ifsv75nzwpim17ccaghvivyfh01fyjn22sk5yixas13fri94jn1") (y #t)))

(define-public crate-fs-tree-0.2.1 (c (n "fs-tree") (v "0.2.1") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)))) (h "1gfh0rxmx5ycsih6zwyk1a3c3kk660dazrf7ixdl0vnxmn1mai5j") (y #t)))

(define-public crate-fs-tree-0.2.2 (c (n "fs-tree") (v "0.2.2") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)))) (h "1wzj1xhawmjks8gsnw2j8w539vs4184msf7nyzpkp4kwri8rz0bs")))

(define-public crate-fs-tree-0.3.0 (c (n "fs-tree") (v "0.3.0") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)))) (h "0wivrbxp2bc8bgmp92gi6iay73glkvbkvi9cqbm9jahx5xv1fhnq") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.4.0 (c (n "fs-tree") (v "0.4.0") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "14pn7ykap5injm5sa1m5250dw5ig90yarfwp8mn2cq8g6q88758i") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.0 (c (n "fs-tree") (v "0.5.0") (d (list (d (n "file_type_enum") (r "^0.11") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1nfqfn1madh5n7c591sh1c3hg8lgjm35mr94z4kpqi1rr9s5491a") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.1 (c (n "fs-tree") (v "0.5.1") (d (list (d (n "file_type_enum") (r "^2.0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0mam5hqj2vapzk2qgjrxl092kh0a56vy90ic8qqv2z5l4x9y0z0s") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.2 (c (n "fs-tree") (v "0.5.2") (d (list (d (n "file_type_enum") (r "^2.0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0kwd5ykxnwh1qrndhdzf9h5fsf9l60215bsbc8k7fia2vv54gbcy") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.3 (c (n "fs-tree") (v "0.5.3") (d (list (d (n "file_type_enum") (r "^2.0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "17q69cb05370fv08vfmbw4jpiqb1z6h8kvpxhd0gh291nqwgmb6z") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.4 (c (n "fs-tree") (v "0.5.4") (d (list (d (n "file_type_enum") (r "^2.0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "1qijh6v25k9d3p053yzy8b4c5yc4r2d06ry8iiv0a9wp6dyw3bmj") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5.5 (c (n "fs-tree") (v "0.5.5") (d (list (d (n "file_type_enum") (r "^2.0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "17lmm6vx04kqw2vbnwkrpxnh4ikkds11rbk08l6gf8xj6mw9l5xh") (f (quote (("default" "fs-err")))) (s 2) (e (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

