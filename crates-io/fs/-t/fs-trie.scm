(define-module (crates-io fs -t fs-trie) #:use-module (crates-io))

(define-public crate-fs-trie-0.1.0 (c (n "fs-trie") (v "0.1.0") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 2)))) (h "0iz0s0alx0wsn4hd523pqbn0fy3vs7r0xh4sy7rfj8am9nv14k29")))

(define-public crate-fs-trie-0.1.3 (c (n "fs-trie") (v "0.1.3") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 2)))) (h "0fm66jskjd980xmi460yaw8wppg999p9rj0wf77bmbf42cfpl92f")))

(define-public crate-fs-trie-0.1.4 (c (n "fs-trie") (v "0.1.4") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 2)))) (h "1nx9rblkv29qvjccw72fa8i26091z58vvxpjbm2sq7ivzz4c1xac")))

