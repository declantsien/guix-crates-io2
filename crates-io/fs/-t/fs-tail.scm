(define-module (crates-io fs -t fs-tail) #:use-module (crates-io))

(define-public crate-fs-tail-0.1.0 (c (n "fs-tail") (v "0.1.0") (h "144hk86yvrq179jzsvk45ld5if9r0jxk6m7y2wm2b3kl2bic5w8d")))

(define-public crate-fs-tail-0.1.1 (c (n "fs-tail") (v "0.1.1") (d (list (d (n "memchr") (r "^2.3.4") (d #t) (k 0)))) (h "1q3q53wz89079d0wv6qzcin1k1ah45rx41cnnpkpzjr6zmjxc660")))

(define-public crate-fs-tail-0.1.2 (c (n "fs-tail") (v "0.1.2") (d (list (d (n "memchr") (r "^2.3.4") (d #t) (k 0)))) (h "13il7vr8fh7xidlrb5jiz0dmnnrxag6aspdnm3zyljbfb50k9px7")))

(define-public crate-fs-tail-0.1.3 (c (n "fs-tail") (v "0.1.3") (d (list (d (n "memchr") (r "^2.3.4") (k 0)))) (h "0867rm9pzhqd0yqf9qkha4ipl6gc6d16mws9vlis3m3s34kpbdaw")))

(define-public crate-fs-tail-0.1.4 (c (n "fs-tail") (v "0.1.4") (d (list (d (n "memchr") (r "^2.3.4") (k 0)))) (h "1lkadcslflg779p1w14h092w0xm35ibxhslpm9wnvbdjjj3fxj3k")))

