(define-module (crates-io fs -t fs-tool) #:use-module (crates-io))

(define-public crate-fs-tool-0.0.0 (c (n "fs-tool") (v "0.0.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diagnostic-quick") (r "^0.3.0") (f (quote ("globset"))) (d #t) (k 0)) (d (n "fs-flatten") (r "^0.0.0") (d #t) (k 0)))) (h "14msmn1ak2606za285fn094hwf3ny4qk2haia7yd1w8443f4xdky") (f (quote (("default"))))))

