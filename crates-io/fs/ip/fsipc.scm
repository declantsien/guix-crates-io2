(define-module (crates-io fs ip fsipc) #:use-module (crates-io))

(define-public crate-fsipc-0.9.1 (c (n "fsipc") (v "0.9.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zbus") (r "^3.0") (d #t) (k 0)))) (h "09z68w3h7cyzdn6md91l24b54cjid25x4pjyim9j1d75qyhygy3c")))

(define-public crate-fsipc-0.9.2 (c (n "fsipc") (v "0.9.2") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zbus") (r "^3.0") (d #t) (k 0)))) (h "00cay5ljhk49b7xijw1yadvxr7gvkadnq0an0290bb0i586w1vzk")))

(define-public crate-fsipc-0.9.5 (c (n "fsipc") (v "0.9.5") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zbus") (r "^3.0") (d #t) (k 0)))) (h "0rfrhl7qaya08i00vbyvrwxrhs14jjyk7c2n3yiqags3bdxy4sk1")))

