(define-module (crates-io fs -f fs-flatten) #:use-module (crates-io))

(define-public crate-fs-flatten-0.0.0 (c (n "fs-flatten") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.3.0") (f (quote ("walkdir" "globset"))) (d #t) (k 0)))) (h "0k18i9bzy2xkxgf246pxmlz4xdjwdi6mgmzd6byk4arsx3c300cv") (f (quote (("default"))))))

