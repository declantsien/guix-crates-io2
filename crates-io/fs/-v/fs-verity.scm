(define-module (crates-io fs -v fs-verity) #:use-module (crates-io))

(define-public crate-fs-verity-0.1.0 (c (n "fs-verity") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.4.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0plhqw53ajh62jpyq8mygya6hyyrr8f31g2q83jrfbn1bqc9lq66")))

(define-public crate-fs-verity-0.2.0 (c (n "fs-verity") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1b1a786bv4dfm9sdjrwij6fav7c47s9qlydhjabr40k16382a3jh")))

