(define-module (crates-io fs _e fs_extra) #:use-module (crates-io))

(define-public crate-fs_extra-0.1.0 (c (n "fs_extra") (v "0.1.0") (h "1r3gkyr8pvj7drs4ysdhyg70jwrwz97rrqhs2a0wybax3my3dap9")))

(define-public crate-fs_extra-0.2.0 (c (n "fs_extra") (v "0.2.0") (h "14g7a3nk6j7yqz6rd7dgrs50np0y6fcdj6bw8inmgl6n9d4rryqz")))

(define-public crate-fs_extra-0.2.1 (c (n "fs_extra") (v "0.2.1") (h "0h1jhzzlimpwif17hsgdm388p7aaf2isq1m5rji23dnsc756hbgm")))

(define-public crate-fs_extra-0.3.0 (c (n "fs_extra") (v "0.3.0") (h "1xppsdlrgwhdrzjjsvk820549vw2nzvwc99x8zkm0c0kyvigvi5n")))

(define-public crate-fs_extra-0.3.1 (c (n "fs_extra") (v "0.3.1") (h "0w88044samf0387zxm0ivpgym6iq72f67c62119x4g959a6pc0hh")))

(define-public crate-fs_extra-1.0.0 (c (n "fs_extra") (v "1.0.0") (h "0k4xghm4l9smf1c8rnbyc0gp2k57g10grj319snkf2ln3bxbs4nq")))

(define-public crate-fs_extra-1.1.0 (c (n "fs_extra") (v "1.1.0") (h "0x6675wdhsx277k1k1235jwcv38naf20d8kwrk948ds26hh4lajz")))

(define-public crate-fs_extra-1.2.0 (c (n "fs_extra") (v "1.2.0") (h "151k6dr35mhq5d8pc8krhw55ajhkyiv0pm14s7zzlc5bc9fp28i0")))

(define-public crate-fs_extra-1.3.0 (c (n "fs_extra") (v "1.3.0") (h "075i25z70j2mz9r7i9p9r521y8xdj81q7skslyb7zhqnnw33fw22")))

