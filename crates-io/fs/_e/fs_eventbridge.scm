(define-module (crates-io fs _e fs_eventbridge) #:use-module (crates-io))

(define-public crate-fs_eventbridge-0.1.0 (c (n "fs_eventbridge") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "filetime") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0hz0g3x9l27ac4j48b55141x5kzjwh5r86xfhf4278m7l32r5a23")))

