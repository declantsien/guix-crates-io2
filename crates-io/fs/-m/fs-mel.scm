(define-module (crates-io fs -m fs-mel) #:use-module (crates-io))

(define-public crate-fs-mel-0.6.0 (c (n "fs-mel") (v "0.6.0") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "melodium-core") (r "^0.6.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.6.0") (d #t) (k 0)))) (h "0g0hlf1h1dzbrnx450ryx71ch6v3mz4ap6c3q13hbwih4p739gjp") (r "1.60")))

(define-public crate-fs-mel-0.7.0-rc1 (c (n "fs-mel") (v "0.7.0-rc1") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "0cmlwkgqzng9hiknj5fwz87ihmvp64lgkx5pnm25ch5y512ngibf") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.7.0 (c (n "fs-mel") (v "0.7.0") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "0r8z2hkiajsz64n8882jsiii6564y84x6k5b48zy0n4400x7r8n1") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.7.1 (c (n "fs-mel") (v "0.7.1") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "018z1qr1n08zk8cswm022fvnk0pl7i9c2advdn1qc65k22gbbzx4") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.8.0-rc1 (c (n "fs-mel") (v "0.8.0-rc1") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc1") (d #t) (k 0)))) (h "00fzszmliq4s5crcz6yy73wghy9jdi9gvrxjg4ffqmnfzsjm1wn5") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.8.0-rc2 (c (n "fs-mel") (v "0.8.0-rc2") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc2") (d #t) (k 0)))) (h "0gp58fpfvabmyfpiwvgh49z39g34rkfqapgvxl1whr1nj0rji4sd") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.8.0-rc3 (c (n "fs-mel") (v "0.8.0-rc3") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "std-mel") (r "=0.8.0-rc3") (d #t) (k 0)))) (h "1l1sf8162pr24mi2rrh6ldxi3abhd2pj9p7kqv622i1gr9imx7wf") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-fs-mel-0.8.0 (c (n "fs-mel") (v "0.8.0") (d (list (d (n "async-std") (r "~1.12") (d #t) (k 0)) (d (n "async-walkdir") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.8.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "std-mel") (r "^0.8.0") (d #t) (k 0)))) (h "07zjk2k3dxs6ri038r9nyq74xqi41y4i8ph7cn27ivrwm8r8z748") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

