(define-module (crates-io fs g_ fsg_guessing_game) #:use-module (crates-io))

(define-public crate-fsg_guessing_game-0.1.0 (c (n "fsg_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "133grv3yrbpn5aavkzrc3vljr7vkd0kvwg0ygh9kpgln4fdfby55") (y #t)))

(define-public crate-fsg_guessing_game-0.1.1 (c (n "fsg_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08jgx829csiiw6hb3c9j7hlvwa130za5diwqim0npww9xsrfklw5")))

