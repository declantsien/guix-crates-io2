(define-module (crates-io fs io fsio) #:use-module (crates-io))

(define-public crate-fsio-0.1.0 (c (n "fsio") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "users") (r "^0.9") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0w0nk39w3iix1pw74b2yxg4mlzga0c22g59v2blyl70l3sk032ar") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.1.1 (c (n "fsio") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "users") (r "^0.9") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1xbwyb5q4rnbhjnmz8iy5p72zbqqad9y5657k17wb6gx0sg9f5qj") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.1.2 (c (n "fsio") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "users") (r "^0.9") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0m7w85bndqhpwb345l0rm444nmn5mx3brw52zd6k6rvg141wnc91") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.1.3 (c (n "fsio") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "users") (r "^0.10") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1qs9i8blmbaxjl3csibn5sf0w27qf8dz2sgamvqz8fgpamr0izf1") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.2.0 (c (n "fsio") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "users") (r "^0.11") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "0ryiygpphrw1ziyx0w4crfz2dwspiyg46wjxpjph3biii6m4a055") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.3.0 (c (n "fsio") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "dunce") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "users") (r "^0.11") (o #t) (d #t) (t "cfg(not(windows))") (k 0)))) (h "1svpcdpf5vfdjwvhqlqvw8pp2c39w1yzabsn9fjcg55gxwkpis09") (f (quote (("temp-path" "rand" "users") ("default"))))))

(define-public crate-fsio-0.3.1 (c (n "fsio") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "dunce") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1gp1l105v40rqj1bli5makhfh7favvyzzrs5ywvliih1r63wwvyy") (f (quote (("temp-path" "rand") ("default"))))))

(define-public crate-fsio-0.4.0 (c (n "fsio") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "dunce") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1l1nkb06s8xx84yrba5nj86wm80knv40bmy54nrl3i0cpqqcxl6s") (f (quote (("temp-path" "rand") ("default"))))))

