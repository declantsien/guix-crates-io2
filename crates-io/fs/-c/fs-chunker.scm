(define-module (crates-io fs -c fs-chunker) #:use-module (crates-io))

(define-public crate-fs-chunker-0.1.0 (c (n "fs-chunker") (v "0.1.0") (h "0jsy5gvq3g5qfpvyykj3p5j6wm020hnkam9f3kldyxhl4kp8p02g")))

(define-public crate-fs-chunker-0.2.0 (c (n "fs-chunker") (v "0.2.0") (d (list (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "02f6dk0x9bvmvr6b7a25nr0d9mzlb68grw4qr22wf0qr77xx0319")))

(define-public crate-fs-chunker-0.2.1 (c (n "fs-chunker") (v "0.2.1") (d (list (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "1rr7128zh88kw2jgsgl9pamjkx4l5nv65hd4067fxla261hhiha4")))

(define-public crate-fs-chunker-0.3.0 (c (n "fs-chunker") (v "0.3.0") (d (list (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "0x9gbjcqagkypjivma4zslj3y7d6hawzqgdydbx675dvmhb680qi")))

(define-public crate-fs-chunker-0.4.0 (c (n "fs-chunker") (v "0.4.0") (d (list (d (n "blake3") (r "^1.5.1") (d #t) (k 0)) (d (n "sha256") (r "^1.5.0") (d #t) (k 0)))) (h "1x66dx1kkpkbzmdzf9jh9jqlap9nbijfbzl76cn9grm230k2ggaf")))

