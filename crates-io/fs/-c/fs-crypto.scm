(define-module (crates-io fs -c fs-crypto) #:use-module (crates-io))

(define-public crate-fs-crypto-0.1.0 (c (n "fs-crypto") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rpassword") (r "^2.1.0") (d #t) (k 0)))) (h "0c5lm4fdj0xkr8wy574kq5q5cbgfzq8xwb1yyswfqb63p3abkxv2")))

