(define-module (crates-io fs #{30}# fs3000) #:use-module (crates-io))

(define-public crate-fs3000-0.1.0 (c (n "fs3000") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "interp") (r "^1") (d #t) (k 0)))) (h "1bmm69ryg1vxbi98gvj5alv731kvsnvzfwgwvv8vy59ibp1xdsdj")))

