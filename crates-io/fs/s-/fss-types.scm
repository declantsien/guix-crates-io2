(define-module (crates-io fs s- fss-types) #:use-module (crates-io))

(define-public crate-fss-types-0.1.0 (c (n "fss-types") (v "0.1.0") (d (list (d (n "group-math") (r "^0.2") (k 0)))) (h "1i7baz0xv01cc839z3m6q7mv62qb9k8ps8ffqfhjmziv59hrfxww")))

(define-public crate-fss-types-0.1.1 (c (n "fss-types") (v "0.1.1") (d (list (d (n "group-math") (r "^0.2") (k 0)))) (h "0yixb477yk293lb82p370655s0i50ry9nikmm164z1pfp3llyy2m")))

(define-public crate-fss-types-0.1.2 (c (n "fss-types") (v "0.1.2") (d (list (d (n "group-math") (r "^0.2") (k 0)))) (h "0063cdq0njbhq6pa28m8crykxiy0w9kcjg22cyps2lcy7cfy1596")))

