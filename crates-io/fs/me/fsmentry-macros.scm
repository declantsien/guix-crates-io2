(define-module (crates-io fs me fsmentry-macros) #:use-module (crates-io))

(define-public crate-fsmentry-macros-0.1.0 (c (n "fsmentry-macros") (v "0.1.0") (d (list (d (n "fsmentry-core") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("proc-macro"))) (k 0)))) (h "1njk9mm1rraiqdknaynfym7xlgfzyc17d34v19sj016sgwxc809l") (f (quote (("svg"))))))

(define-public crate-fsmentry-macros-0.1.1 (c (n "fsmentry-macros") (v "0.1.1") (d (list (d (n "fsmentry-core") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("proc-macro"))) (k 0)))) (h "0dlmcrykbci144rnzi73xyg5anhvhsw8hgn5hrlgk2rwh9ic583c") (f (quote (("svg"))))))

(define-public crate-fsmentry-macros-0.1.2 (c (n "fsmentry-macros") (v "0.1.2") (d (list (d (n "fsmentry-core") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("proc-macro"))) (k 0)))) (h "19ndbbd8hmn595jng5hyvng18vxgxq4h81vv0ig9sz5gm65lwmz6") (f (quote (("svg"))))))

