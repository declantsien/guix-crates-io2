(define-module (crates-io fs d_ fsd_interface) #:use-module (crates-io))

(define-public crate-fsd_interface-0.1.17 (c (n "fsd_interface") (v "0.1.17") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1n60wgjxf71v1389hbkyyga5l85vbzzq38jwgx475ap6z2zw6hn1")))

(define-public crate-fsd_interface-0.1.18 (c (n "fsd_interface") (v "0.1.18") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p5fi8haj6sfbprc5d24mg87s0laq3d1pxwkqzan3n8g3apnhccv")))

(define-public crate-fsd_interface-0.1.20 (c (n "fsd_interface") (v "0.1.20") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zx9sm73ypqw3ginj03bck1gnhj0pkvnbma8b0jw8xb83ysys63b")))

(define-public crate-fsd_interface-0.1.21 (c (n "fsd_interface") (v "0.1.21") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13m71xlyjnbpmkcwgq1n08aah1rmsg3jb7liyjan8h1z2v8iy3vc")))

