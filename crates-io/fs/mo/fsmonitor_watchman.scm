(define-module (crates-io fs mo fsmonitor_watchman) #:use-module (crates-io))

(define-public crate-fsmonitor_watchman-0.1.0 (c (n "fsmonitor_watchman") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)) (d (n "watchman_client") (r "^0.8.0") (d #t) (k 0)))) (h "094gs14p55w37398c1j311x1pj2k7fkwc3x88rslbxdm9d0sk6pw") (y #t)))

