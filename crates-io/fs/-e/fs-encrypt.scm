(define-module (crates-io fs -e fs-encrypt) #:use-module (crates-io))

(define-public crate-fs-encrypt-0.1.0 (c (n "fs-encrypt") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "hex") (r "0.4.*") (d #t) (k 0)) (d (n "hkdf") (r "0.12.*") (d #t) (k 0)) (d (n "pbkdf2") (r "0.12.*") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha3") (r "0.10.*") (d #t) (k 0)))) (h "1ch7w5xqiwq8wvinmgk4xk8g0f5fy96vmr2k25p1p8lvzdx3jdna")))

(define-public crate-fs-encrypt-0.1.1 (c (n "fs-encrypt") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "hex") (r "0.4.*") (d #t) (k 0)) (d (n "hkdf") (r "0.12.*") (d #t) (k 0)) (d (n "pbkdf2") (r "0.12.*") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha3") (r "0.10.*") (d #t) (k 0)))) (h "0v2r5yn94q4admz19mwx8icsisrvas3wmbk2akn4kv19834d03iv")))

(define-public crate-fs-encrypt-0.1.2 (c (n "fs-encrypt") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "hex") (r "0.4.*") (d #t) (k 0)) (d (n "hkdf") (r "0.12.*") (d #t) (k 0)) (d (n "pbkdf2") (r "0.12.*") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha3") (r "0.10.*") (d #t) (k 0)))) (h "0zj5y5fz7950lkfy608chgmwlgkp4m0c7299rhmg7gywij9gjp15")))

(define-public crate-fs-encrypt-0.1.3 (c (n "fs-encrypt") (v "0.1.3") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "hex") (r "0.4.*") (d #t) (k 0)) (d (n "hkdf") (r "0.12.*") (d #t) (k 0)) (d (n "pbkdf2") (r "0.12.*") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha3") (r "0.10.*") (d #t) (k 0)))) (h "0f2i3b33gvmvbnyi5q8bmw48xb0q1zv2razk93jakfm0k9yvpgjj")))

