(define-module (crates-io fs co fscommon) #:use-module (crates-io))

(define-public crate-fscommon-0.1.0 (c (n "fscommon") (v "0.1.0") (d (list (d (n "core_io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13rn2a302igiq9wh3c5xqbk35mdc8qs97mahlrkmfw2h5n9lqjx8") (f (quote (("std") ("default" "std"))))))

(define-public crate-fscommon-0.1.1 (c (n "fscommon") (v "0.1.1") (d (list (d (n "core_io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c9m21083hi8yvqbcj988rj00paagps6whvy7rdcrpd5mj2ycp1i") (f (quote (("std") ("default" "std"))))))

