(define-module (crates-io fs ya fsyaml) #:use-module (crates-io))

(define-public crate-fsyaml-1.0.0 (c (n "fsyaml") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0w2zbjddiznnx4lxqd46pjqq0ifkc4yk5mwa065j8yl0rmsy6yxi")))

(define-public crate-fsyaml-1.0.1 (c (n "fsyaml") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0x3nkr5zig7m9ghlxiyy9hbrj993vffw0b2hx1pxwmxyymszl6nk")))

(define-public crate-fsyaml-1.0.2 (c (n "fsyaml") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16xlmmkh0aja7qrh7jlqnaz1bhaszzjz6fjqi1pp60s4x3s67a89")))

(define-public crate-fsyaml-1.0.3 (c (n "fsyaml") (v "1.0.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vh8amdmbahlvk872wxqk7zlhkfxqrwp4qcmrkgljshqz6fjqlvw")))

(define-public crate-fsyaml-1.0.4 (c (n "fsyaml") (v "1.0.4") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1a1lfbrqq3l9w0k64mqa25fn0pp3143nsvby8h04ka089k1ccqza")))

(define-public crate-fsyaml-1.0.5 (c (n "fsyaml") (v "1.0.5") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "suggestions"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rzapqrfbmjxz77qr1a39fjjwxqgvdgvsq16pd3q5vpbv5dq4kd2")))

