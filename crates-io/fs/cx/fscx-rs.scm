(define-module (crates-io fs cx fscx-rs) #:use-module (crates-io))

(define-public crate-fscx-rs-0.1.1 (c (n "fscx-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1b26zn8psx68virx2w9z9gjzjpmzr6digwyfbwqy800hndvhnwnh")))

(define-public crate-fscx-rs-0.1.2 (c (n "fscx-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0ymr82c6cs5gqr9w1cbnccpf80f7a366d2w89i0xjh47z0y7rb8f")))

(define-public crate-fscx-rs-0.1.3 (c (n "fscx-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "122f707kx79bmjvir8mlmgn7xfbm7v5kvsmgapqnjfs8wq74hzrl")))

(define-public crate-fscx-rs-0.1.4 (c (n "fscx-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "146a57i69h3zy87gvba2gcz7kbdlaj7f5jrq5wzrgkr9znl4gcpc")))

