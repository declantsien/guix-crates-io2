(define-module (crates-io fs _u fs_util) #:use-module (crates-io))

(define-public crate-fs_util-0.1.0 (c (n "fs_util") (v "0.1.0") (d (list (d (n "generic_error") (r "^0.1.0") (d #t) (k 0)))) (h "1bxz0hbpsi7nqh19c7xxdzdd35fg3azplfxvmik3lpig7qrx24i0")))

(define-public crate-fs_util-0.1.1 (c (n "fs_util") (v "0.1.1") (d (list (d (n "generic_error") (r "^0.2.0") (d #t) (k 0)))) (h "15jrg7xgjmhgx44x4y7plkddj0rxvmjp5ghfmdsxym3gbnawwy20")))

