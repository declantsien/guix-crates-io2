(define-module (crates-io fs r2 fsr2-sys) #:use-module (crates-io))

(define-public crate-fsr2-sys-0.1.0 (c (n "fsr2-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1mxl8r1c8nkivg4h3a48fq3fnwapcy1m5ywlhzfbkr40p13pzn1r") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.1 (c (n "fsr2-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0yzazqn7d1fh0fhh4cbbi95rnsr5xbwk7z7kh9knaji1c8srkads") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.2 (c (n "fsr2-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "04kijh67f1fjszy64xy7rlf4y6mqcw1x6mnz2r5pdf4v7fha088g") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.3 (c (n "fsr2-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0xy6465qxp51a43fh3830a5ivr1rxa08lidgmw11j75hjhvwziql") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.4 (c (n "fsr2-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1rsfr5sbkp63ss5s6931q3pjy9bvx8p9chsrf1c214gck88xcwqz") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.5 (c (n "fsr2-sys") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "09nqnl4aw9d2m5y7qq5yqw5m62ka3vqlnbys7g1rh3i4clrw6wgi") (f (quote (("vk") ("dx12") ("default" "vk"))))))

(define-public crate-fsr2-sys-0.1.6 (c (n "fsr2-sys") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1ljjl30bddcdzkiqy0gp1czabca2lbdffdk8mh10r2aim80lf612") (f (quote (("vk") ("dx12") ("default" "vk"))))))

