(define-module (crates-io fs iz fsize) #:use-module (crates-io))

(define-public crate-fsize-0.1.0 (c (n "fsize") (v "0.1.0") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 1)))) (h "1i2yjdhjhdwhgbl67j429liw4yvlfy321a02ci3cm3ymzwn34ndk")))

(define-public crate-fsize-1.0.0 (c (n "fsize") (v "1.0.0") (h "0hnankyw8lsid0i4mvd4izh03agydhzn51vi6rydwa1y948x869v")))

(define-public crate-fsize-0.1.1 (c (n "fsize") (v "0.1.1") (h "05fb2fwp1zp4h6a1a760k6fpnva0nfmakw7n1a4fi3ai1jfc3njm")))

