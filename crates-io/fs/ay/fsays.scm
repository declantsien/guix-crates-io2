(define-module (crates-io fs ay fsays) #:use-module (crates-io))

(define-public crate-fsays-0.1.0 (c (n "fsays") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "04gy6dbk3n1nxyxfcp6vd6drsz6gqlc7wr57fasbjazv99sc7vf7")))

(define-public crate-fsays-0.3.0 (c (n "fsays") (v "0.3.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "ferris-says") (r "^0.3") (d #t) (k 0)))) (h "1j7dz2m296fig6crsp8b6sb3ggvg10k1yypvcpg1a2910xfj3lgx")))

