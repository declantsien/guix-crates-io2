(define-module (crates-io fs ea fsearch-plugin-app-lancher) #:use-module (crates-io))

(define-public crate-fsearch-plugin-app-lancher-0.0.1 (c (n "fsearch-plugin-app-lancher") (v "0.0.1") (d (list (d (n "fsearch-core") (r "^0.0.7") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "0lcjnwag638lqqjwn2cjzzvd1savy23y2finx3cp74mxa7kc02yb") (y #t)))

(define-public crate-fsearch-plugin-app-lancher-0.0.2 (c (n "fsearch-plugin-app-lancher") (v "0.0.2") (d (list (d (n "fsearch-core") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "1g7fh04yc9gic36qn98h7mh83b6kffs8wzsx5iysfwiq4lb3fw6f") (y #t)))

(define-public crate-fsearch-plugin-app-lancher-0.0.3 (c (n "fsearch-plugin-app-lancher") (v "0.0.3") (d (list (d (n "fsearch-core") (r "^0.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "18vd43yzkl9qc60svsxsicahczwnazwqj54r037qd0xr7kkvpw05") (y #t)))

(define-public crate-fsearch-plugin-app-lancher-0.0.4 (c (n "fsearch-plugin-app-lancher") (v "0.0.4") (d (list (d (n "fsearch-core") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "0gi7y9isljp6wyp27980snd8vgb7wnshqyb966aynridggrqgv3n") (y #t)))

(define-public crate-fsearch-plugin-app-lancher-0.0.5 (c (n "fsearch-plugin-app-lancher") (v "0.0.5") (d (list (d (n "fsearch-core") (r "^0.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "0a7gl2hpry2gaqcx3a7rfmkv0spwjn9zi707pj9vyj3pvl3akinp") (y #t)))

(define-public crate-fsearch-plugin-app-lancher-0.0.6 (c (n "fsearch-plugin-app-lancher") (v "0.0.6") (d (list (d (n "fsearch-core") (r "^0.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "xdgkit") (r "^3.2.5") (d #t) (k 0)))) (h "0bjwn8hx6wfpx41klhy93n0bbj8x6npq1jshv9ifbwl64ffm21q8") (y #t)))

