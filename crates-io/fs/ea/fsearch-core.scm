(define-module (crates-io fs ea fsearch-core) #:use-module (crates-io))

(define-public crate-fsearch-core-0.0.1 (c (n "fsearch-core") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "1zygdgf1i2z61fjz67pgirl4bhjsmx9y3vq6r6s4dxhvyk3i7ix6") (y #t)))

(define-public crate-fsearch-core-0.0.2 (c (n "fsearch-core") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "1hsc04lcyqzkp58zh17xpif7lyva8hbb6ay93ad5wf6ddhk580rk") (y #t)))

(define-public crate-fsearch-core-0.0.3 (c (n "fsearch-core") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "13p1j4c8h46cdjrv239f7czb7qbi2qf88hbscjp65h6w2zaiwy1r") (y #t)))

(define-public crate-fsearch-core-0.0.4 (c (n "fsearch-core") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "0142slmc11r4rd1n8m2qs96y447rbwm3p1g87xxrvcf8ibpwbr0q") (y #t)))

(define-public crate-fsearch-core-0.0.5 (c (n "fsearch-core") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "0nwq3zh5mqz6i0ihw0kh5vmaq6f954x1i1gnzkfn171mg00s8wcd") (y #t)))

(define-public crate-fsearch-core-0.0.6 (c (n "fsearch-core") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "0gd8qjd0krwmkmf56v6sxyz4yxzw8wrqwf26b8fq73dq82f6x901") (y #t)))

(define-public crate-fsearch-core-0.0.7 (c (n "fsearch-core") (v "0.0.7") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "0sm5b15lngqv6n5na6cjw4r3j47k4gyw7js090rxl0m8b9ld1q1j") (y #t)))

(define-public crate-fsearch-core-0.0.8 (c (n "fsearch-core") (v "0.0.8") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "0dnvfq2d8vyg3da1h762w1qs204qcv33bfcp8yj6l50rf3l4q51n") (y #t)))

(define-public crate-fsearch-core-0.0.9 (c (n "fsearch-core") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "000ir3ppqg8c8kwry90wybam3z69a7km7widgvy3qn6p3rzvfw2n")))

(define-public crate-fsearch-core-0.0.10 (c (n "fsearch-core") (v "0.0.10") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.4") (d #t) (k 0)))) (h "1bwk09pz70nankp99f9qhqganlw4zxpzcwi3a76wjdyqairh6xgw")))

(define-public crate-fsearch-core-0.0.11 (c (n "fsearch-core") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "05rncxrysbayh6kvyjlwnapv3n1ha2psi8bjq1r2x23kwbiw86ii")))

