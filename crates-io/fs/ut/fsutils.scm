(define-module (crates-io fs ut fsutils) #:use-module (crates-io))

(define-public crate-fsutils-0.1.0 (c (n "fsutils") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0ncd17ca2g3wjnn178i6p42c05brrwwz76yy96dqrgm5xxqgkh91")))

(define-public crate-fsutils-0.1.1 (c (n "fsutils") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0yc5abk56b3mby38b4ab2kbv9jbr8qxcdfq6ivkfdr844zz1wssv")))

(define-public crate-fsutils-0.1.2 (c (n "fsutils") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1bca2parq8802bhxmlc1ydb50rf6cbb6z2s5ss15yklr381r58ld")))

(define-public crate-fsutils-0.1.3 (c (n "fsutils") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1b1dlk354d3255vn5ax2iscdz4izzw161jkclsjjpw4gawwk6892")))

(define-public crate-fsutils-0.1.4 (c (n "fsutils") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "01f18gwvsbjr5z1yzxlx897jmabcchzms4nhjvqyqirdjfy1nkbb")))

(define-public crate-fsutils-0.1.5 (c (n "fsutils") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "18ljlsdiy3lbvhrln1laxvv59s8gv2l8m3h4434r93nmhnmwm59i")))

(define-public crate-fsutils-0.1.6 (c (n "fsutils") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1iv96idzzplxpiaajf5dmygx899adsxw7z2cp6n8rk7kk8b57hv0")))

(define-public crate-fsutils-0.1.7 (c (n "fsutils") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0j48ljin6vrid30fzsd2dak9jr7wa8x0vy94c9sqyd6pkpzca9gh")))

