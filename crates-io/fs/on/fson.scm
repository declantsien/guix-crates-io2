(define-module (crates-io fs on fson) #:use-module (crates-io))

(define-public crate-fson-0.1.0 (c (n "fson") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "15gk7mqzs3dy0k4lzs7gfkg0cfzd9cadl5fh890rp8x655xsazv0")))

