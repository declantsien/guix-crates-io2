(define-module (crates-io fs td fstd) #:use-module (crates-io))

(define-public crate-fstd-0.1.0 (c (n "fstd") (v "0.1.0") (h "0n4ygs0mv5zbp38ldd89gf5zblr75sdy5q28ppgi5qs90v5zdgmj")))

(define-public crate-fstd-0.1.1 (c (n "fstd") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0i4y5cd8n8zbpqvbzg0965w1w4cljrnkfbnpbmi6ais12n5i4hg9") (f (quote (("rand_nobias"))))))

(define-public crate-fstd-0.1.2 (c (n "fstd") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "08nkbbmi2yyp5kw38fzs4h7hj1fbgsyqmdwh3ymdidi6wdc7sia4") (f (quote (("rand_nobias"))))))

