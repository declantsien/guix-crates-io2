(define-module (crates-io fs ha fshamer) #:use-module (crates-io))

(define-public crate-fshamer-0.1.0 (c (n "fshamer") (v "0.1.0") (d (list (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03bvfkfg1id5ljjyrsd7x56ds1g1ycdzwbrv4548kxzbihh3rzcm")))

(define-public crate-fshamer-0.1.1 (c (n "fshamer") (v "0.1.1") (d (list (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1757j8dhfbmfyl3cl0bb1hyghnbdvm20qxh6sx3hmnpw45cm86f2")))

(define-public crate-fshamer-0.1.2 (c (n "fshamer") (v "0.1.2") (d (list (d (n "number_prefix") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1948si51sf3v066laq6ab5zdra67fj5gsn0m24iwc3n0di0gp3w5")))

