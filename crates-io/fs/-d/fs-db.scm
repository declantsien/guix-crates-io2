(define-module (crates-io fs -d fs-db) #:use-module (crates-io))

(define-public crate-fs-db-0.1.0 (c (n "fs-db") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0b5z2hi88vmr9qbyy342fzxsazzf2q6p2vaikc3lgwc24hcz2x1s")))

