(define-module (crates-io fs be fsbex) #:use-module (crates-io))

(define-public crate-fsbex-0.0.0 (c (n "fsbex") (v "0.0.0") (h "0hfbijj03cbic0x31j3c76a995gc89m2s1751a1pqvcrs12rzqj3") (y #t)))

(define-public crate-fsbex-0.1.0 (c (n "fsbex") (v "0.1.0") (d (list (d (n "bilge") (r "^0.1.5") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "vorbis_rs") (r "^0.3.0") (d #t) (k 0)))) (h "16b72xg7i1xkn6dfmpljjx11sycswdama898f4fhirh0v5ag9z6r")))

(define-public crate-fsbex-0.2.0 (c (n "fsbex") (v "0.2.0") (d (list (d (n "bilge") (r "^0.1.5") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "vorbis_rs") (r "^0.3.0") (d #t) (k 0)))) (h "0nzwvzhbryan03l03rldia4fv8wa9f5vv8629cjgfdqnz1bws840")))

(define-public crate-fsbex-0.2.1 (c (n "fsbex") (v "0.2.1") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "vorbis_rs") (r "^0.4.0") (f (quote ("stream-serial-rng"))) (d #t) (k 0)))) (h "0ai97i32gd8s47vpjy1bwrzq7w5kla4w5412sj10pcvk63z6zghc")))

(define-public crate-fsbex-0.2.2 (c (n "fsbex") (v "0.2.2") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "vorbis_rs") (r "^0.4.0") (f (quote ("stream-serial-rng"))) (d #t) (k 0)))) (h "01rkn9jlj01xg42g8lxzj2xn9iyrlc9mb6mwz3lvkl7l9y4i1w46")))

(define-public crate-fsbex-0.3.0 (c (n "fsbex") (v "0.3.0") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "vorbis_rs") (r "^0.5.0") (f (quote ("stream-serial-rng"))) (d #t) (k 0)))) (h "1k3xr8ij5bi7bg1p6hylqsfhy4k41g5p52i3djbdk3mr8w75fbin")))

