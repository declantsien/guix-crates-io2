(define-module (crates-io c_ ap c_api_prefix) #:use-module (crates-io))

(define-public crate-c_api_prefix-0.1.0 (c (n "c_api_prefix") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1kybafyww4ay7nw8j4zv0i3spm29p590qdsx9snyldjjc65ma3cs")))

