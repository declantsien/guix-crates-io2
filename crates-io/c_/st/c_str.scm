(define-module (crates-io c_ st c_str) #:use-module (crates-io))

(define-public crate-c_str-1.0.0 (c (n "c_str") (v "1.0.0") (h "06kafbhbph3qw0sqk6iikbk59snf7k308rk8a79jqa34s44yg8w1")))

(define-public crate-c_str-1.0.1 (c (n "c_str") (v "1.0.1") (h "00jz9ij6nvm2vj344gd8srz9rcsjvf78q9cmdbv310qixxnky9gg")))

(define-public crate-c_str-1.0.2 (c (n "c_str") (v "1.0.2") (h "193djj4yk2vkg7am6383zjp9apxrqysnzld7835xgw6j5rrv613s")))

(define-public crate-c_str-1.0.3 (c (n "c_str") (v "1.0.3") (h "0ncjn7gkwc6xi6hgmzk3fhqr2hid4lqz1ri66nnsl6l7n7h42zlb")))

(define-public crate-c_str-1.0.4 (c (n "c_str") (v "1.0.4") (h "1ayw4ddz540lx8a915aipi1mrw1lc9gzljnpgyakb22h3ah2dvqg")))

(define-public crate-c_str-1.0.5 (c (n "c_str") (v "1.0.5") (h "0z09wbfxh36i9mygp3p73n4jp6q8whggxdrhkx5plb5mys6kxl86")))

(define-public crate-c_str-1.0.6 (c (n "c_str") (v "1.0.6") (h "1lkfnik19bg1wcyy28j6cd8hl95rpbizskxz8npsx5i3qdh7gbw0")))

(define-public crate-c_str-1.0.7 (c (n "c_str") (v "1.0.7") (h "0dn3lcm30vpq24rb3n8ghcphcblsqrp1mvbfs36xa46h89ik1y0m")))

(define-public crate-c_str-1.0.8 (c (n "c_str") (v "1.0.8") (h "14gm8wqipk152p5idb55d3nhagw857mkk6w2grp81xnx1fp46di0")))

