(define-module (crates-io c_ st c_str_macro) #:use-module (crates-io))

(define-public crate-c_str_macro-1.0.0 (c (n "c_str_macro") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1k9zb803r7937wsf9iia4vf0cfh7axvniavvzxlxhb0jyhhbmcc9")))

(define-public crate-c_str_macro-1.0.1 (c (n "c_str_macro") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1khgsis8yjmadzs9hbxwfp8d91dq1vwr5ww0g8hh50jxga6j9prk")))

(define-public crate-c_str_macro-1.0.2 (c (n "c_str_macro") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1gdmw8wc9d189sxyvyzabgwd6qspld4w84h3yycbzg3vsdl85q7j")))

(define-public crate-c_str_macro-1.0.3 (c (n "c_str_macro") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1rg51ngv70imiy3qdl2qlf7gnrrg0ncxfrvnbqi9w0b9qi8lkm66")))

