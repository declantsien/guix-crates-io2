(define-module (crates-io c_ st c_string) #:use-module (crates-io))

(define-public crate-c_string-0.1.0 (c (n "c_string") (v "0.1.0") (h "0ky7w4bhpqachaqw5xxyr358g32j04l78aydsg25r125iixqxidc")))

(define-public crate-c_string-0.2.0 (c (n "c_string") (v "0.2.0") (h "0ir5yaw6cdlljrghflvsfkvyqf6bccr7l4dikpmn5ias1gsbyjzi")))

(define-public crate-c_string-0.3.0 (c (n "c_string") (v "0.3.0") (h "1zl6n5vmyhjprm7p8v8ygif4pwfxq108b0a523vww9vpidzs6rf4")))

(define-public crate-c_string-0.3.1 (c (n "c_string") (v "0.3.1") (h "1qywh3nkp7b4pj7h2lsgiab3pmpwk61q29y8ry819xs9jnnj35sv")))

(define-public crate-c_string-0.3.2 (c (n "c_string") (v "0.3.2") (h "1dis4fy8xm99l4w2v0y6g8p4ifnzcsssvbmjwshxranvn3qc9i03")))

(define-public crate-c_string-0.4.0 (c (n "c_string") (v "0.4.0") (h "0cn9kbq8lpzjyv9w97d8dzb00qpz8frwv1j7nsfayjxi1wwfw979")))

(define-public crate-c_string-0.4.1 (c (n "c_string") (v "0.4.1") (h "0mhbwcvkr6nf5dvl67cxx4n074svdils0hd3xa07phgbabdaqgav")))

(define-public crate-c_string-0.5.0 (c (n "c_string") (v "0.5.0") (h "0n48zaic7g6pdahgfilxbwz94p909r4n0jc6lby0nlxpp5h3pjga")))

(define-public crate-c_string-0.5.1 (c (n "c_string") (v "0.5.1") (h "00d914p122ls0kf5hwm0ivxgq1q1zxwp3diwa79w7vwfdypw32pg")))

(define-public crate-c_string-0.6.0 (c (n "c_string") (v "0.6.0") (h "1pyq029fy208xmv7by5psq235110ljads8mx4wcgsp4jbs155f9n")))

(define-public crate-c_string-0.6.1 (c (n "c_string") (v "0.6.1") (h "1s4w4bafpzr9gf5pkl1w9473jsya45nh4x549ndxxhz7vi6wdwdw")))

(define-public crate-c_string-0.6.2 (c (n "c_string") (v "0.6.2") (h "0kzawsdr97d3ascncyr1gsqnkwa7rlabwpbyqs7iv94xwh4bc5ab")))

(define-public crate-c_string-0.7.0 (c (n "c_string") (v "0.7.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0vp6gqd5s0wj8cazv9qbpkwzbla3f7y60slhy2a5p16z1n9mc8h4")))

(define-public crate-c_string-0.7.1-obsolete (c (n "c_string") (v "0.7.1-obsolete") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1617ya51yv118vnr95rwbknpl48zcampdmf73qbr2nlabxf1fviz")))

(define-public crate-c_string-0.7.2 (c (n "c_string") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f0n2szh3jkj0db5l8pgilyqp4vzrn8izdkvc2pc0xpn5r2scl2r")))

