(define-module (crates-io c_ rs c_rs) #:use-module (crates-io))

(define-public crate-c_rs-0.1.9 (c (n "c_rs") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syntex_errors") (r "^0.42.0") (d #t) (k 0)) (d (n "syntex_pos") (r "^0.42.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42") (o #t) (d #t) (k 0)))) (h "01ia059d7rwgyprlqwkcyz3i2gy9204pdww17slxjz5rr33mjw8r") (f (quote (("macro") ("build" "gcc" "syntex_syntax")))) (y #t)))

