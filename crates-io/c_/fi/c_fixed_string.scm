(define-module (crates-io c_ fi c_fixed_string) #:use-module (crates-io))

(define-public crate-c_fixed_string-0.1.0 (c (n "c_fixed_string") (v "0.1.0") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "17kamlqzhlvz9wc1s00lva0f0ql0kyvhrd8hq68jkpamjyqqcznk")))

(define-public crate-c_fixed_string-0.2.0 (c (n "c_fixed_string") (v "0.2.0") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "1ks7gcr0klh5jmbi9g0livblnxwalz18vak7q6jd2cmnw6qk6s7y")))

