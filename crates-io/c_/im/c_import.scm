(define-module (crates-io c_ im c_import) #:use-module (crates-io))

(define-public crate-c_import-0.1.0 (c (n "c_import") (v "0.1.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "161ccsirnykik7mm4jxfz20sjzz07hwkdcaam1nk4axxzjw42zp7")))

(define-public crate-c_import-0.1.1 (c (n "c_import") (v "0.1.1") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rwh3crhmmmsv47fc57v3s2bfvih5q9m0sb2l850wbqs9s0v8l2p")))

(define-public crate-c_import-0.1.2 (c (n "c_import") (v "0.1.2") (h "0sxlj90dsys4qarmiz1jh0smx8ldgbncj1ww85q4081hn266a300")))

(define-public crate-c_import-0.1.3 (c (n "c_import") (v "0.1.3") (h "0dxg5ar3ww3xf5sk618005ryf007f9zl126lvx30001i2i0qi4db")))

(define-public crate-c_import-0.1.4 (c (n "c_import") (v "0.1.4") (h "16cg8mij78294hz1icpy287lixck8kgiiz3p7q0gv19npl920p93")))

(define-public crate-c_import-0.1.5 (c (n "c_import") (v "0.1.5") (h "1jxk37xfgr36x0rq2gv7c200p0a0j17kjyndjbb0riz2avfq4h4l")))

(define-public crate-c_import-0.2.0 (c (n "c_import") (v "0.2.0") (h "15rxk6i03k8rl2c37r159knn6qh5qsy4xkfj7nipwx5wjyk85plj")))

(define-public crate-c_import-0.2.1 (c (n "c_import") (v "0.2.1") (h "1bcg8bz2jr1mn8z46d7znpxhzsaa4jpgszlcaj3yrg706p6rd49d")))

(define-public crate-c_import-0.2.2 (c (n "c_import") (v "0.2.2") (h "0cqndbmfspxxd6v5zggcdm2rl5yvs8jwclgyrb5pysmlc1qyr5pl")))

(define-public crate-c_import-0.2.3 (c (n "c_import") (v "0.2.3") (h "1wsi683b37dnmlbybnr02q4n1rh9dz9wlfa4m33spqjnr68rnrlv")))

(define-public crate-c_import-0.2.4 (c (n "c_import") (v "0.2.4") (h "0az6a0gcbn4xxpkpnfv4yxi8i14h994ypvf164pmzrw05fwv0szn")))

