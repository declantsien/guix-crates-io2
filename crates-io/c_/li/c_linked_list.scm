(define-module (crates-io c_ li c_linked_list) #:use-module (crates-io))

(define-public crate-c_linked_list-0.1.0 (c (n "c_linked_list") (v "0.1.0") (h "1i5pk5hw1j27fw553kll40ag4cns2cx0i1a7vq76hx2qgnvr4mw2")))

(define-public crate-c_linked_list-1.0.0 (c (n "c_linked_list") (v "1.0.0") (h "1ir41p02ad4gwdgjy23jshvlb56hqwzsr73bnn5xhqzmgzxrms4j")))

(define-public crate-c_linked_list-1.1.0 (c (n "c_linked_list") (v "1.1.0") (h "1h4gxdrrr5mn57ygcdknkwqwkzf20pz86v5gfd25bd15m5bljxa8")))

(define-public crate-c_linked_list-1.1.1 (c (n "c_linked_list") (v "1.1.1") (h "0jvjr3233wqgx6li65kciqpg354pvb0cv1i8hc71ka5lsf5m2r29")))

