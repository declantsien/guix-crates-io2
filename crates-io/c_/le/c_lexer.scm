(define-module (crates-io c_ le c_lexer) #:use-module (crates-io))

(define-public crate-c_lexer-0.1.0 (c (n "c_lexer") (v "0.1.0") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rhsrgc1ms7vlw7gigp4mdz2vfj90zl32r25glw58ds26dyz4xw8")))

(define-public crate-c_lexer-0.1.1 (c (n "c_lexer") (v "0.1.1") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "047ica7d1spaaf0ir2mxif71dhgz0l2ishwgnsxx92sbqfsqwjpv")))

