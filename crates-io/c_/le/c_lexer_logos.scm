(define-module (crates-io c_ le c_lexer_logos) #:use-module (crates-io))

(define-public crate-c_lexer_logos-0.1.0 (c (n "c_lexer_logos") (v "0.1.0") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)))) (h "1pnpl9l4mymjsnvbbivq5b9mrxpx3xr85ifvmgaqp3yc6qy8ml56")))

(define-public crate-c_lexer_logos-0.1.1 (c (n "c_lexer_logos") (v "0.1.1") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)))) (h "0rxf9nbzmzq1f9zdb6j6hy5428zmi8r3rp62hv8vzv4i243w0whz")))

