(define-module (crates-io c_ br c_bridge) #:use-module (crates-io))

(define-public crate-c_bridge-0.2.0 (c (n "c_bridge") (v "0.2.0") (h "1kgxlvidxran30zgwljq7hl8mfny952a4l48mqlykv6v2wj58fxy") (y #t)))

(define-public crate-c_bridge-0.3.0 (c (n "c_bridge") (v "0.3.0") (h "19c3f72v78md0lidrn13izwcpbh1s44ki27angs0zzn8fmlwjjrg") (y #t)))

(define-public crate-c_bridge-0.3.1 (c (n "c_bridge") (v "0.3.1") (h "11hsg3a3c3vma2hgjs8i9l46x8acp4zy5631bwn0mhn31sd31fyl") (y #t)))

(define-public crate-c_bridge-0.3.2 (c (n "c_bridge") (v "0.3.2") (h "1qn907kplbw1cg674swqq174r2m93fwf8aix7fvivlycbricayyf") (y #t)))

(define-public crate-c_bridge-0.4.0 (c (n "c_bridge") (v "0.4.0") (h "1qzc9n1314nfdr6ji22y8k9bwx79mz250g0a0hs4i8477i309qs2") (y #t)))

(define-public crate-c_bridge-0.4.1 (c (n "c_bridge") (v "0.4.1") (h "0abn1rwamii03s7v77amgddckf7dx80scrl59skb2vz54jvywmc9") (y #t)))

(define-public crate-c_bridge-0.4.2 (c (n "c_bridge") (v "0.4.2") (h "0pxhicyzhagsqxz4i6iy7dkzjvni9fhp8413k8l460v2n5j47qg7") (y #t)))

(define-public crate-c_bridge-0.5.0 (c (n "c_bridge") (v "0.5.0") (h "102cg07gxjbh5009ga8ay0vazx28g9di1gm0ivq08wmw0l986w9w") (y #t)))

(define-public crate-c_bridge-0.6.0 (c (n "c_bridge") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0laspv2r7g946vn2dmakv9s0vz96iz6k2d3gqhky6qx25a81pzlh") (f (quote (("test_build" "cc") ("default")))) (y #t)))

