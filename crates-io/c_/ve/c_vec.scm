(define-module (crates-io c_ ve c_vec) #:use-module (crates-io))

(define-public crate-c_vec-1.0.0 (c (n "c_vec") (v "1.0.0") (h "0qawpxkx71sy795f4x9zr57nfpyq1741fv7msjhx90d8wbbdr070")))

(define-public crate-c_vec-1.0.1 (c (n "c_vec") (v "1.0.1") (h "0v1abrh6qp36yc83f9740fh9mvxnhllv02v8gs21rmm21rkqdsqi")))

(define-public crate-c_vec-1.0.2 (c (n "c_vec") (v "1.0.2") (h "1y6lpb03y7qv15w7249fdzbq0nn5h50g70ld403m41ady4p53wk9")))

(define-public crate-c_vec-1.0.3 (c (n "c_vec") (v "1.0.3") (h "1f027glrk89306av3zdj692gyrmy7w04n1gigp83c1nwbpxpacg7")))

(define-public crate-c_vec-1.0.4 (c (n "c_vec") (v "1.0.4") (h "15j9warkdf39rz9lrqc312ynbrfv6a74n70ac0ncipinlwdghikm")))

(define-public crate-c_vec-1.0.5 (c (n "c_vec") (v "1.0.5") (h "0n67xa8lqcmx1lpr5x182s0b5adrxbmxq0x81vp7abs5bnz83ywn")))

(define-public crate-c_vec-1.0.6 (c (n "c_vec") (v "1.0.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "06qglwc872zp07dpmbgcbhd02kpxy6l2kvqj714d3k4rbl81h9j7")))

(define-public crate-c_vec-1.0.7 (c (n "c_vec") (v "1.0.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "0qh1687r4p48gp74f577pr734wd623fq6xiz4410bhcg9h1p6j5n")))

(define-public crate-c_vec-1.0.8 (c (n "c_vec") (v "1.0.8") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "1kbivk3zsa4vqs32ln2k7za5kmn6f175q4v6rp1nxg6hh6j78v2z")))

(define-public crate-c_vec-1.0.9 (c (n "c_vec") (v "1.0.9") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "134hf704v24khf96msaf64qncspzh2wv08cs4ma7mmzgqlhzbhbv")))

(define-public crate-c_vec-1.0.10 (c (n "c_vec") (v "1.0.10") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "1sr6nimnrskcw4b4pqa5wfk10dzawqbnmm5ir8xyvhxg632p1cdk")))

(define-public crate-c_vec-1.0.11 (c (n "c_vec") (v "1.0.11") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "16yx7j5ljwrrmwxgjh2ixjdm6ymzqh21kz2lbgi327grgmp2qq4p")))

(define-public crate-c_vec-1.1.0 (c (n "c_vec") (v "1.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "02gkicm59prlhpc2igkvl4gbmclzg6wmancscwdk3z93p04rjqhc")))

(define-public crate-c_vec-1.0.12 (c (n "c_vec") (v "1.0.12") (d (list (d (n "libc") (r "^0.1") (d #t) (k 2)))) (h "16jvxi6kgmclpv4s7izrxiih3qx5sprzpvqrdzrqkqj9gngiv7ma")))

(define-public crate-c_vec-1.2.0 (c (n "c_vec") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "01y5rllqclky30mskrycfm5i9nng5k2xsyksjg9lnapnb2kgan80")))

(define-public crate-c_vec-1.2.1 (c (n "c_vec") (v "1.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "04xx43glcys8n554p48aj7gldrk19jb3fr2cq89w508y9ddaqdv2")))

(define-public crate-c_vec-1.3.0 (c (n "c_vec") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1gfyhjpj9gplh5i2k6i51fdnlj9v05q40rdvs571ma5wmn9npx8i")))

(define-public crate-c_vec-1.3.1 (c (n "c_vec") (v "1.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1ias02m5ck6dy93v5xrj5hzqvcykpl4si2jxjb18ph3ym381k3zs")))

(define-public crate-c_vec-1.3.2 (c (n "c_vec") (v "1.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1sdly4a13avfj1njkabr4qm7gfp6ah444h8sk6pnm0ffjmgb2ckc")))

(define-public crate-c_vec-1.3.3 (c (n "c_vec") (v "1.3.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0c3wgb15h97k6lzfm9qgkwk35ij2ad7w8fb5rbqvalyf3n8ii8zq")))

(define-public crate-c_vec-2.0.0 (c (n "c_vec") (v "2.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1s765fviy10q27b0wmkyk4q728z9v8v5pdlxv5k564y0mlks9mzx")))

