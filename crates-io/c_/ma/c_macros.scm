(define-module (crates-io c_ ma c_macros) #:use-module (crates-io))

(define-public crate-c_macros-0.1.0 (c (n "c_macros") (v "0.1.0") (d (list (d (n "col_macros") (r "^0.1") (f (quote ("owned_slice"))) (d #t) (k 0)))) (h "1vbx4q5h8qmhb8hxpjc1a9s3gr34jw0vvk945c2andy5bn0wc6ab") (y #t)))

(define-public crate-c_macros-0.1.1 (c (n "c_macros") (v "0.1.1") (d (list (d (n "col_macros") (r "^0.1") (f (quote ("owned_slice"))) (d #t) (k 0)))) (h "02d1mawa7mzv6pv4i8166dkwrxx8w75xfhw1dsz4kx116fwxhm8n")))

