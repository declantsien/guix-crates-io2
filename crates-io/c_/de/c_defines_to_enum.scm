(define-module (crates-io c_ de c_defines_to_enum) #:use-module (crates-io))

(define-public crate-c_defines_to_enum-0.1.0 (c (n "c_defines_to_enum") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17p2xpb5zsbwca28wjqsdhpjbv4679xq71fagxvp07b3hqv3g9ac") (y #t)))

(define-public crate-c_defines_to_enum-0.1.1 (c (n "c_defines_to_enum") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z8ppjd6gf3wghbm2nzin44kbyr2bwvzfc6q5mad7x9mj8lb6gac")))

