(define-module (crates-io pg _v pg_vizurl) #:use-module (crates-io))

(define-public crate-pg_vizurl-0.1.0 (c (n "pg_vizurl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i4ciyhrz7bzgk6lgnz382jd6lrnz3waik4wr292cl7h2kpshd8h")))

(define-public crate-pg_vizurl-0.1.1 (c (n "pg_vizurl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive" "wrap_help" "env"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l8i3bk5bmrqc4aiynr8jxq93qyj354ziddm5jwirm55ndh0gk1p")))

