(define-module (crates-io pg du pgdump2sqlite) #:use-module (crates-io))

(define-public crate-pgdump2sqlite-0.1.0 (c (n "pgdump2sqlite") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "11bg919ym8wxnyn8in543p0d9m7iwfv1cwx2j22c6s6v72nl0afg")))

(define-public crate-pgdump2sqlite-0.2.0 (c (n "pgdump2sqlite") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0w3b7z4q628ppgzmk5mya6a7z06s2bz0cqqvqj99ya61fjg13y1y")))

