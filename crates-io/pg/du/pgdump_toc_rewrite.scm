(define-module (crates-io pg du pgdump_toc_rewrite) #:use-module (crates-io))

(define-public crate-pgdump_toc_rewrite-1.0.0 (c (n "pgdump_toc_rewrite") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.40.0") (d #t) (k 0)))) (h "0vlqc6bbsydar8sgih2j7p4dz2zv0qpb1ns27prb58r0kfr6xd2n")))

(define-public crate-pgdump_toc_rewrite-1.0.1 (c (n "pgdump_toc_rewrite") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.40.0") (d #t) (k 0)))) (h "0lpzvydaim20dvmd8x3xrivwz23xqh8d0xa6rsgzfj0mly70yngz")))

(define-public crate-pgdump_toc_rewrite-1.0.2 (c (n "pgdump_toc_rewrite") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.40.0") (d #t) (k 0)))) (h "07pa6dypdprqcr1x2h9qmhq6x7bdhlaw2g8zib093m9dcaipqj7w")))

