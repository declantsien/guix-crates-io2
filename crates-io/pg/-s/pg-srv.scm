(define-module (crates-io pg -s pg-srv) #:use-module (crates-io))

(define-public crate-pg-srv-0.1.0 (c (n "pg-srv") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "=0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02c6r3638b67bps4p14iwic38qpz3gfv75vzxary466dwiypifrb")))

(define-public crate-pg-srv-0.2.0 (c (n "pg-srv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.16") (f (quote ("clock"))) (o #t) (k 0) (p "chrono")) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "=0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ilj72w6lq9lhisp6jyjyhp6hvi8d71ggbd4m9ci1spfv0d00y76") (f (quote (("with-chrono" "chrono") ("default" "with-chrono"))))))

