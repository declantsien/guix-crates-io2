(define-module (crates-io pg -h pg-helper) #:use-module (crates-io))

(define-public crate-pg-helper-0.1.0 (c (n "pg-helper") (v "0.1.0") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "1ks3gdxplaxa4555rbxhqg6rp7q3bljwzcp7wxgh530awh0wlhv8")))

(define-public crate-pg-helper-0.2.0 (c (n "pg-helper") (v "0.2.0") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "02zfjx9wa5b6r55d42p4al0qr706z2kkv07qq0np9ck0wjvcjdak")))

(define-public crate-pg-helper-0.2.3 (c (n "pg-helper") (v "0.2.3") (d (list (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)))) (h "1n4143ggkcq1b9q3n4dpl07n3gm766w2iq5w8dzaa2fj0wx524ca")))

