(define-module (crates-io pg n- pgn-traits) #:use-module (crates-io))

(define-public crate-pgn-traits-0.1.0 (c (n "pgn-traits") (v "0.1.0") (d (list (d (n "board-game-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1pmi36wbnxikgxcbwr3icr3y0wf41bq5478bj5d2klbn58f289r6")))

(define-public crate-pgn-traits-0.1.1 (c (n "pgn-traits") (v "0.1.1") (d (list (d (n "board-game-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0ywnnbc38bmz0zqan1bama5x27l5f3p80c5kk374km373bghlxb7")))

(define-public crate-pgn-traits-0.2.0 (c (n "pgn-traits") (v "0.2.0") (d (list (d (n "board-game-traits") (r "^0.2.0") (d #t) (k 0)))) (h "029d32hgi2np4hxcc8d5w0vzcf5wv0cidmgs7wj7sps9kw3r5csa")))

(define-public crate-pgn-traits-0.2.1 (c (n "pgn-traits") (v "0.2.1") (d (list (d (n "board-game-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1az6yjdry5r1yvc3zda3q9al38dckazs2nz427zsv3205z3bdz28")))

(define-public crate-pgn-traits-0.2.2 (c (n "pgn-traits") (v "0.2.2") (d (list (d (n "board-game-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0n6q8ww9hyyrx9r4nzny0w89hp7j5fp2kcnc25nvas5rma0yq8v3")))

(define-public crate-pgn-traits-0.3.0 (c (n "pgn-traits") (v "0.3.0") (d (list (d (n "board-game-traits") (r "^0.3.0") (d #t) (k 0)))) (h "1l011bbqr01yxfq8ychibw1rmarbbhr1mvl2c261bmx5l2cs3vjm")))

(define-public crate-pgn-traits-0.4.0 (c (n "pgn-traits") (v "0.4.0") (d (list (d (n "board-game-traits") (r "^0.3.0") (d #t) (k 0)))) (h "1n1nj94lpk76vbaqiddsxsk120vm66vcnl5j73rm67a35x3xkw5z")))

(define-public crate-pgn-traits-0.4.1 (c (n "pgn-traits") (v "0.4.1") (d (list (d (n "board-game-traits") (r "^0.3.0") (d #t) (k 0)))) (h "109c8lfcb2g1a5ra08ss7g67na2aw07sxzvkmbldlkqxn6pi61xd")))

(define-public crate-pgn-traits-0.5.0 (c (n "pgn-traits") (v "0.5.0") (d (list (d (n "board-game-traits") (r "^0.4.0") (d #t) (k 0)))) (h "03rg9n4d07pivgw0zrmpaqsjxdf5l6llyzpvpbpqphmch046xnfz")))

