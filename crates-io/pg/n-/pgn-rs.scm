(define-module (crates-io pg n- pgn-rs) #:use-module (crates-io))

(define-public crate-pgn-rs-0.0.1 (c (n "pgn-rs") (v "0.0.1") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0ybv1gw6w0bgdgzr14zljd4h399ib9njmg13ypz24cj5sxk9v5w1")))

(define-public crate-pgn-rs-0.0.2 (c (n "pgn-rs") (v "0.0.2") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "1flhzpmkhbx3xc1k2n0ypzlq466bdyl80h5cpq9fsa20j92q9s6f")))

