(define-module (crates-io pg n- pgn-lexer) #:use-module (crates-io))

(define-public crate-pgn-lexer-0.1.0 (c (n "pgn-lexer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^3.2") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0g2g2xjkrmpxf53gjxgkqzsmvkcrjs3kxm1wr2kwdyyw5c9dwywa")))

(define-public crate-pgn-lexer-0.1.1 (c (n "pgn-lexer") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "080jjzxahsby5j4xflg1c3j4iqk05fdq8c30r5diiag1lz15xqjn")))

(define-public crate-pgn-lexer-0.2.0-alpha (c (n "pgn-lexer") (v "0.2.0-alpha") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.2") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0w8v6pqvbnxzrkr1g4a84jsjizq7p60vwsw1zwaczwkwwvwc1wy8")))

