(define-module (crates-io pg _m pg_mapper) #:use-module (crates-io))

(define-public crate-pg_mapper-0.1.0 (c (n "pg_mapper") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10803qy384w2xabl5c5g37a6lxzzvap5hq1znb2x13dawimfl39i")))

(define-public crate-pg_mapper-0.2.0 (c (n "pg_mapper") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (k 2)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1pszksvqfswhxj6r6fwjxmqsnv8hbvk1jbzk62hn65f7sklgxijs")))

(define-public crate-pg_mapper-0.2.1 (c (n "pg_mapper") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (k 2)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "17y3vj96sf52jrm57k10l28qzwghcr0l45rm55z16qn7vbwpgr5i")))

