(define-module (crates-io pg _m pg_multitenant) #:use-module (crates-io))

(define-public crate-pg_multitenant-0.1.0 (c (n "pg_multitenant") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgrx") (r "=0.11.2") (d #t) (k 0)) (d (n "pgrx-tests") (r "=0.11.2") (d #t) (k 2)) (d (n "uuid") (r "^1.7.0") (d #t) (k 2)))) (h "0lsbbxm25hys3bl4c992db62qczr7mnf5bhzf8axmhkhb0ybajik") (f (quote (("pg_test") ("pg16" "pgrx/pg16" "pgrx-tests/pg16") ("default" "pg16"))))))

