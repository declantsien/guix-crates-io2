(define-module (crates-io pg _s pg_sgr) #:use-module (crates-io))

(define-public crate-pg_sgr-0.1.0 (c (n "pg_sgr") (v "0.1.0") (d (list (d (n "pg_sgr_from_row") (r "^0.1.0") (o #t) (k 0)))) (h "1555zhk3bbbbxi5jz2mkns6bwcgvnxkqddqsrpg6wpsqnc20mhrn") (f (quote (("tokio" "pg_sgr_from_row/tokio") ("sync" "pg_sgr_from_row/sync") ("default")))) (s 2) (e (quote (("from_row" "dep:pg_sgr_from_row"))))))

