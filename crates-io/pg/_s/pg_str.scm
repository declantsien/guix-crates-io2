(define-module (crates-io pg _s pg_str) #:use-module (crates-io))

(define-public crate-pg_str-0.2.0 (c (n "pg_str") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "pgx") (r "^0.1.21") (d #t) (k 0)) (d (n "pgx-macros") (r "^0.1.21") (d #t) (k 0)) (d (n "pgx-tests") (r "^0.1.21") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "str_slug") (r "^0.1.3") (d #t) (k 0)))) (h "0ryy2dprvnv2qzhdrywf7bmxizbc0zah14i1b1f85brndvyxscwj") (f (quote (("pg_test") ("pg13" "pgx/pg13" "pgx-tests/pg13") ("default" "pg13"))))))

(define-public crate-pg_str-0.2.1 (c (n "pg_str") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "any_ascii") (r "^0.3.0") (d #t) (k 0)) (d (n "pgx") (r "^0.1.21") (d #t) (k 0)) (d (n "pgx-macros") (r "^0.1.21") (d #t) (k 0)) (d (n "pgx-tests") (r "^0.1.21") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.1") (d #t) (k 0)) (d (n "str_slug") (r "^0.1.3") (d #t) (k 0)))) (h "12kfhmrwkr9wdzbij0abq31ca2g0zyp0h3f3k5yl23ph2djq7xrl") (f (quote (("pg_test") ("pg13" "pgx/pg13" "pgx-tests/pg13") ("default" "pg13"))))))

