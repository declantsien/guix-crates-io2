(define-module (crates-io pg ui pgui-api) #:use-module (crates-io))

(define-public crate-pgui-api-0.1.0 (c (n "pgui-api") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "010zl7gyjaigp7jhxcrrj3j9jh1vl07xssqz0ywlklchzczi5zp9")))

(define-public crate-pgui-api-0.1.1 (c (n "pgui-api") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.3.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1wsak9mmcy0f5swgpimg2fhhn5gijb12g2wk9rncfawgvnbcmn45")))

