(define-module (crates-io pg fp pgfplots) #:use-module (crates-io))

(define-public crate-pgfplots-0.1.0 (c (n "pgfplots") (v "0.1.0") (h "1izdx1g5ih15jywrq8qpgw3cf8b400y0189xq2jfkm2yk9dzxrs3")))

(define-public crate-pgfplots-0.2.0 (c (n "pgfplots") (v "0.2.0") (d (list (d (n "opener") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "0pqy96j0m9r8hzip2w8ls1c31cc2r2270hpw1lrbjcfbnfxwm8s0") (s 2) (e (quote (("inclusive" "dep:tectonic" "dep:tempfile" "dep:opener"))))))

(define-public crate-pgfplots-0.3.0 (c (n "pgfplots") (v "0.3.0") (d (list (d (n "opener") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "18rn3lr5dbh6lrl86pwfl8x7s9div6cn92vmqjmf86hjyx69rv6x") (s 2) (e (quote (("inclusive" "dep:tectonic" "dep:tempfile" "dep:opener"))))))

(define-public crate-pgfplots-0.3.1 (c (n "pgfplots") (v "0.3.1") (d (list (d (n "opener") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "135lhp9ar6nlyl3ixvfvlz0cj806a3abcdgxs3n0v871b4xbg8xc") (s 2) (e (quote (("inclusive" "dep:tectonic" "dep:tempfile" "dep:opener"))))))

(define-public crate-pgfplots-0.4.0 (c (n "pgfplots") (v "0.4.0") (d (list (d (n "opener") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "08bj1fn28xqw3q6gpa0cclkf6rgapkrhn4jk64iqxbmqgpm325yn") (s 2) (e (quote (("inclusive" "dep:tectonic" "dep:tempfile" "dep:opener"))))))

(define-public crate-pgfplots-0.4.1 (c (n "pgfplots") (v "0.4.1") (d (list (d (n "opener") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tectonic") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)))) (h "16ijahfg861k1jhlbf4m8sdqvwni2hy0csy0vmmxakp9j89vls0p") (s 2) (e (quote (("inclusive" "dep:tectonic" "dep:tempfile" "dep:opener"))))))

(define-public crate-pgfplots-0.5.0 (c (n "pgfplots") (v "0.5.0") (d (list (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tectonic") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fhwryyxb9ijlzqiyq422srg43ypql1s5fkla4j4w8rlxp0yvlhc")))

(define-public crate-pgfplots-0.5.1 (c (n "pgfplots") (v "0.5.1") (d (list (d (n "opener") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tectonic") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s71hffv4a9826sz2x8fial3szpyhgf5n473636jcybarbg4bknh")))

