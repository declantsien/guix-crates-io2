(define-module (crates-io pg r2 pgr2junit) #:use-module (crates-io))

(define-public crate-pgr2junit-0.1.0 (c (n "pgr2junit") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "patch") (r "^0") (d #t) (k 0)) (d (n "quick-junit") (r "^0.3") (d #t) (k 0)) (d (n "scanf") (r "^1") (d #t) (k 0)))) (h "14k3dmajk8id7sw76ardqb1f532ka5axfzzhmadm0m65bz63pzkf")))

(define-public crate-pgr2junit-0.0.1 (c (n "pgr2junit") (v "0.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "patch") (r "^0") (d #t) (k 0)) (d (n "quick-junit") (r "^0.3") (d #t) (k 0)) (d (n "scanf") (r "^1") (d #t) (k 0)))) (h "0mn6i1cdkqkbrc60isjvhx9s4f23lw69wlaq7ax40xmbhxrvwp7g")))

(define-public crate-pgr2junit-0.1.1 (c (n "pgr2junit") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "patch") (r "^0") (d #t) (k 0)) (d (n "quick-junit") (r "^0.3") (d #t) (k 0)) (d (n "scanf") (r "^1") (d #t) (k 0)))) (h "03wg480cbdknz6ngzgm6php22y92ch6q0ky89kyg6spb0myx3kvv")))

