(define-module (crates-io pg ru pgrustxn-sys) #:use-module (crates-io))

(define-public crate-pgrustxn-sys-0.0.7 (c (n "pgrustxn-sys") (v "0.0.7") (d (list (d (n "rustfmt") (r "^0.4") (d #t) (k 2)))) (h "11pcfj3ca8yggp9nbfw0sr0gnsck62nq2adzcy00hlcjafxps7ij") (y #t)))

(define-public crate-pgrustxn-sys-0.0.8 (c (n "pgrustxn-sys") (v "0.0.8") (d (list (d (n "rustfmt") (r "^0.4") (d #t) (k 2)))) (h "0n3l1jvi065d5zsvyclqbyrkkhlxzm6dzp24zx9r5gn1s6waa94d") (y #t)))

