(define-module (crates-io pg s- pgs-files) #:use-module (crates-io))

(define-public crate-pgs-files-0.0.3 (c (n "pgs-files") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "1s3sxl3z6yq8plf1qlf8b4xcb1n8a7jsjk3fipjgja5abjnzvd9h")))

(define-public crate-pgs-files-0.0.4 (c (n "pgs-files") (v "0.0.4") (d (list (d (n "libc") (r "^0.1.9") (d #t) (k 0)))) (h "0zf676dmky7lhrrxxma6m85xxc54aj7r2gm20a9cski4gwcwy2n1")))

(define-public crate-pgs-files-0.0.5 (c (n "pgs-files") (v "0.0.5") (d (list (d (n "libc") (r "^0.1.9") (d #t) (k 0)))) (h "1h39lq01llsgqgz30h6pzdkzf4578naygiiw7ib4gv4zxzgi406n")))

(define-public crate-pgs-files-0.0.6 (c (n "pgs-files") (v "0.0.6") (d (list (d (n "libc") (r "^0.1.9") (d #t) (k 0)))) (h "04444j69mx8id92lk5bd3m6d4wi20ypixx9cihcg2rymy4rkgrwd")))

(define-public crate-pgs-files-0.0.7 (c (n "pgs-files") (v "0.0.7") (d (list (d (n "libc") (r "^0.1.9") (d #t) (k 0)))) (h "05gpz8jk290lh38zz5mh7rp8yhw4p2iiwlprb5la39b7g0vpsyva")))

