(define-module (crates-io pg ca pgcat_config) #:use-module (crates-io))

(define-public crate-pgcat_config-2.0.0-alpha1 (c (n "pgcat_config") (v "2.0.0-alpha1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0c1ylj489f9czgp3rh2q1ckc3lkcyfkk8nm8ich7z9kvnk4qs5s3")))

(define-public crate-pgcat_config-2.0.0-alpha2 (c (n "pgcat_config") (v "2.0.0-alpha2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0z8viay5zvac4zs41vx8mxvw7id08z8ckx39fjg70dpm29wszm3j")))

(define-public crate-pgcat_config-2.0.0-alpha10 (c (n "pgcat_config") (v "2.0.0-alpha10") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0mqpl7az5nw58w8i84khc81z60sm785hlagr2bk45s7vn3b5j23r") (y #t)))

(define-public crate-pgcat_config-2.0.0-alpha3 (c (n "pgcat_config") (v "2.0.0-alpha3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "150bsghnjgga32jglcf59fri1vbl42wlxmdcx0f05m40q5691lfn")))

(define-public crate-pgcat_config-2.0.0-alpha4 (c (n "pgcat_config") (v "2.0.0-alpha4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "18yfsmjg9s8y9bwdlj234w3vywqrf6iqgisw5ckbgks5lilr4pgs")))

(define-public crate-pgcat_config-2.0.0-alpha5 (c (n "pgcat_config") (v "2.0.0-alpha5") (d (list (d (n "rand") (r ">=0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.5") (f (quote ("all"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0d8b3lr898lcaam8v9fmsr7i80fr39x5lnnz53jlzzjv3gjyhnd1")))

