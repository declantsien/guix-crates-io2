(define-module (crates-io pg n4 pgn4) #:use-module (crates-io))

(define-public crate-pgn4-0.1.0 (c (n "pgn4") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iwv96g2j9hnsayyzkrc63prwr0zmqciry3k8nqlh51asjvrc2qg")))

(define-public crate-pgn4-0.2.0 (c (n "pgn4") (v "0.2.0") (d (list (d (n "fen4") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14vjf7hjyk9dp7w18c30ppg97dn2l9smblzzlqv9nsrxwzjihlq0")))

(define-public crate-pgn4-0.3.0 (c (n "pgn4") (v "0.3.0") (d (list (d (n "fen4") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07zscrq7vjm1mmw6xb4ik9c01kb000pnyxn4hljh3c13wgm45nvr")))

(define-public crate-pgn4-0.3.1 (c (n "pgn4") (v "0.3.1") (d (list (d (n "fen4") (r ">=0.4, <0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jfkh8yyvk7wljbwjdbrsxg6y388ppb3rw32wbz9g2vha0ls8lay")))

(define-public crate-pgn4-0.3.2 (c (n "pgn4") (v "0.3.2") (d (list (d (n "fen4") (r ">=0.4, <0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jvs89nhkldar4wwwlfpm1bz89gzpgbys4n96ypf0iiwbrirwnjk")))

(define-public crate-pgn4-0.3.3 (c (n "pgn4") (v "0.3.3") (d (list (d (n "fen4") (r ">=0.4, <0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0812yxqmggn1mvxlyg20zrzppb1280jji6hndmmwlr6cv8zbqf8k")))

