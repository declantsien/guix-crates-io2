(define-module (crates-io pg n_ pgn_filter) #:use-module (crates-io))

(define-public crate-pgn_filter-1.0.0 (c (n "pgn_filter") (v "1.0.0") (d (list (d (n "gecliht") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "00r14d87p7va9f0y0dpx295gxb1f4vi8k6lzadm1dln132747ax4")))

(define-public crate-pgn_filter-1.1.0 (c (n "pgn_filter") (v "1.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1jn4yaqpfl0cfvql4gc21x1mwg7iklbh552268a05446hlk8dq40")))

