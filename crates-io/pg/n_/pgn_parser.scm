(define-module (crates-io pg n_ pgn_parser) #:use-module (crates-io))

(define-public crate-pgn_parser-0.2.1 (c (n "pgn_parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "18rnw2cdjysmf59n548cpzqadaldnbj35213dcf2irrlpb7qk56j")))

(define-public crate-pgn_parser-0.2.2 (c (n "pgn_parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "1d5xzgdkwphgqjqzzdqyi3r3kh5rwygcb595djlz3fk7ivvyzm62")))

