(define-module (crates-io pg _w pg_wire) #:use-module (crates-io))

(define-public crate-pg_wire-0.1.0 (c (n "pg_wire") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0py6xn3dli3hzlwxvycrsw0vm0dgr4lk40qnlmyswhqq5rwv2y4m") (y #t)))

(define-public crate-pg_wire-0.2.1 (c (n "pg_wire") (v "0.2.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "15vivgqrhcshfgdh9hz0pzjvr71cmgksbh05c7hb2g8hr5sfzv25") (y #t)))

(define-public crate-pg_wire-0.3.0 (c (n "pg_wire") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1h2zzmmrjvyw88agrkzy7didhhnnbb2p0w9iqr2a2qw8izk1a8l0") (y #t)))

(define-public crate-pg_wire-0.3.1 (c (n "pg_wire") (v "0.3.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1437fm090nz3s78v9glxr0qzkg06cv3a27jkx0b9ixwznaan5s6p") (y #t)))

(define-public crate-pg_wire-0.4.0 (c (n "pg_wire") (v "0.4.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "058920997z25177b6wzk97bdn476i1zwqsyfmaqsfjrq60gsznwf") (y #t)))

(define-public crate-pg_wire-0.5.0 (c (n "pg_wire") (v "0.5.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "13bah2i7pkd8v7qhxn1aw4r10ayihag930pxy9bjvasiwpxd8w8k") (y #t)))

(define-public crate-pg_wire-0.6.0 (c (n "pg_wire") (v "0.6.0") (h "1pkrjmyk2v49crx0qf8sw3bzpvsy1nk64ind3c1iw40hqvm64zwm") (y #t)))

(define-public crate-pg_wire-0.7.0 (c (n "pg_wire") (v "0.7.0") (h "1mr05imax1b1a56yc028m2xfls5nfkc6pv9kkq92j67h2zig4kl4") (y #t)))

(define-public crate-pg_wire-0.8.0 (c (n "pg_wire") (v "0.8.0") (d (list (d (n "async-io") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "pg_wire_payload") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "rstest") (r "^0.7.0") (d #t) (k 2)) (d (n "smol") (r "^1.2.5") (d #t) (k 2)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "io-util" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-native-tls") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0is6c0v8phicpgxw5qgs7kym7z0pfb25hr84wk9wm22rwch29vfw") (f (quote (("tokio_net" "tokio" "tokio-native-tls") ("mock_net") ("default") ("async_io" "async-io" "blocking")))) (y #t)))

(define-public crate-pg_wire-0.8.1 (c (n "pg_wire") (v "0.8.1") (d (list (d (n "async-io") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.7") (d #t) (k 0)) (d (n "pg_wire_payload") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "rstest") (r "^0.7.0") (d #t) (k 2)) (d (n "smol") (r "^1.2.5") (d #t) (k 2)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("net" "io-util" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-native-tls") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0c6yijs3jarysn99wc8af06k4bkyhxmsp6516638amksw3an1vh6") (f (quote (("tokio_net" "tokio" "tokio-native-tls") ("mock_net") ("default") ("async_io" "async-io" "blocking")))) (y #t)))

