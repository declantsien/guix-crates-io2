(define-module (crates-io pg a2 pga2d) #:use-module (crates-io))

(define-public crate-pga2d-0.0.1 (c (n "pga2d") (v "0.0.1") (h "1sgzyghfhi3bhyr1677ha4iyycc22sq7sj956mgkiih97jfc5qcs")))

(define-public crate-pga2d-0.0.2 (c (n "pga2d") (v "0.0.2") (h "0kkjrrsk9fzk6lmh242hhd3gckphm87sfb622g0ghd1l9xnj2y5a")))

