(define-module (crates-io pg x- pgx-named-columns) #:use-module (crates-io))

(define-public crate-pgx-named-columns-0.1.0 (c (n "pgx-named-columns") (v "0.1.0") (d (list (d (n "pgx") (r "^0.3.3") (f (quote ("pg13"))) (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xpjbc2srlhwfkicja91kgqjzsqs4207c9b6vkh6hpbqpcxjmc4d")))

