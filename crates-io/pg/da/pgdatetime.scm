(define-module (crates-io pg da pgdatetime) #:use-module (crates-io))

(define-public crate-pgdatetime-0.1.0 (c (n "pgdatetime") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1szrzb9g3vkz64qkdn4z3vvylq5xr23pj8h2azhypswyzdb887ap")))

(define-public crate-pgdatetime-0.2.0 (c (n "pgdatetime") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hwbcbq4qd4s30x0xlmsws3jxwjcmq76rj9rmsha191cfwwpnnaa")))

(define-public crate-pgdatetime-0.3.0 (c (n "pgdatetime") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kpcjyv4igmfwv63mpsgih6ffn8zm6czx2zbmfn1pqjbskxmh8ma")))

