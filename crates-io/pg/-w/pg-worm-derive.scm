(define-module (crates-io pg -w pg-worm-derive) #:use-module (crates-io))

(define-public crate-pg-worm-derive-0.1.0 (c (n "pg-worm-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1lff6lkf3da1f5ybpriavv08a84d4dhcs3ji8sgki0rkny18zz0w")))

(define-public crate-pg-worm-derive-0.2.0 (c (n "pg-worm-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zvmbnq801p3kcdffl4qnb8nrxk24xq16yk180fy88gflaim15b0")))

(define-public crate-pg-worm-derive-0.3.0 (c (n "pg-worm-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0p5sdly55pss4cnyqa72v61f4bwzgv7m0lksik6vxq54ymz73az0")))

(define-public crate-pg-worm-derive-0.4.0 (c (n "pg-worm-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0by7w1m3glf0aw7zvy87qrqvb5qmi5jrgv08dc9ifb1c10rx39bf")))

(define-public crate-pg-worm-derive-0.5.0 (c (n "pg-worm-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0kn6mafd0nga2xgjihcsypa5hbayz03mqfi85pdx7zrddj2q676a")))

(define-public crate-pg-worm-derive-0.6.0 (c (n "pg-worm-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sqqcialjnwljfh5brz87nxyximhkz44rj5b8sdqw1965b7s5g7q")))

(define-public crate-pg-worm-derive-0.7.0 (c (n "pg-worm-derive") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lsz1zpa6iy1dbdy1i4i75yjnabyprqmyzfxvbgm8aq2882yi1cl") (f (quote (("uuid") ("time") ("serde-json"))))))

