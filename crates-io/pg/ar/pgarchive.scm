(define-module (crates-io pg ar pgarchive) #:use-module (crates-io))

(define-public crate-pgarchive-0.0.0 (c (n "pgarchive") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1v2aghn09nzqx8accc05ndj04cq0dy1bgmk1ci5vqp6cci8j5b84")))

(define-public crate-pgarchive-0.0.1 (c (n "pgarchive") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "06f5wancxhbw8h62agh7qjhwd3isx9ljzqza3nmvlimpj1xzf3ms")))

(define-public crate-pgarchive-0.2.0 (c (n "pgarchive") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "18i87cn020a13cc09yci2jlw62jyfihbcdqs4z11lidhvkqm7bbr")))

(define-public crate-pgarchive-0.3.0 (c (n "pgarchive") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ss9gds5mva4425vxagfj61a4f916rr6ab8fpq1n5bc2sdgvyriv")))

