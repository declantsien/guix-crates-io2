(define-module (crates-io pg _r pg_rnd_numeric) #:use-module (crates-io))

(define-public crate-pg_rnd_numeric-1.0.0 (c (n "pg_rnd_numeric") (v "1.0.0") (d (list (d (n "pgx") (r "^0.6.1") (d #t) (k 0)) (d (n "pgx-tests") (r "^0.6.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ws5djldwwnw7n17ggc1j12rfjw2fy5rmmva0lmimm596khc4qjh") (f (quote (("pg_test") ("pg15" "pgx/pg15" "pgx-tests/pg15") ("pg14" "pgx/pg14" "pgx-tests/pg14") ("pg13" "pgx/pg13" "pgx-tests/pg13") ("default" "pg14"))))))

