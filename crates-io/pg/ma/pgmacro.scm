(define-module (crates-io pg ma pgmacro) #:use-module (crates-io))

(define-public crate-pgmacro-0.1.0 (c (n "pgmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0r6l49d635c5hqmdgqs1hwvws3yrqjpckrkh1lsa65qm41z28z2a")))

(define-public crate-pgmacro-0.1.1 (c (n "pgmacro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0msdskk0is897gx3qqycmndgzqjhy6xdq8ag75baiqgf6af29m07")))

(define-public crate-pgmacro-0.1.2 (c (n "pgmacro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00iqvi871h40bgnhfhg78jc3lgq0qm78jz4a5hkq7xn3177xz2np")))

(define-public crate-pgmacro-0.1.3 (c (n "pgmacro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0xfhl91arnbbgp0nzd4r04nn2rh5579h3zp75718n4al2lxpqb6c")))

(define-public crate-pgmacro-0.1.4 (c (n "pgmacro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1sm0rfkkd7lc1hl70pa2wdyy2v6qlnvrxzl09snnwdd8qqmyy3xl")))

(define-public crate-pgmacro-0.1.5 (c (n "pgmacro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w0knxfvjyxzy66i5i4j1hk63fhiga3gjn3nd75f5v25admg3253")))

(define-public crate-pgmacro-0.1.6 (c (n "pgmacro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02zkrr9fmbjdyqd6fm3hrs8hw9zhxvqf6kmpks7xq8x2hywilddg")))

(define-public crate-pgmacro-0.1.7 (c (n "pgmacro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jqsy034hywhz5xmkzwvraz1ch1ab39f3dz2f6hy8b63xywhzkvv")))

(define-public crate-pgmacro-0.1.9 (c (n "pgmacro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zipxy3phq41s507flymiyn7qsfk8j02a74nqfhlyqli47vayw92")))

(define-public crate-pgmacro-0.1.10 (c (n "pgmacro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gh99rcvy56ml1iv678ci1r56q1rmddasqkk7i1fxg99l8f18dj8")))

(define-public crate-pgmacro-0.1.12 (c (n "pgmacro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01184v15jr31df7qv6n1pk8sddwzb4jxsqsi212xlv586z2fjhh0")))

(define-public crate-pgmacro-0.1.13 (c (n "pgmacro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "135lidc8q9ld3p53vgywpsim1wqgz3wjscidysdlqrllm0njy9x8")))

