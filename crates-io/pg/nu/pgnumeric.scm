(define-module (crates-io pg nu pgnumeric) #:use-module (crates-io))

(define-public crate-pgnumeric-0.1.0 (c (n "pgnumeric") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)))) (h "1m2win1kyrddraj8si1kfp19y52mwdiqb98kzkhazaldkf8vgrvw") (f (quote (("big-endian-varlen"))))))

(define-public crate-pgnumeric-0.1.1 (c (n "pgnumeric") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)) (d (n "strtod") (r "^0.0.1") (d #t) (k 0)))) (h "0aq3zzs41k10fk7wrm5bqxc4q1ym34ankzwp1529kys8vi40lrrh") (f (quote (("big-endian-varlen"))))))

(define-public crate-pgnumeric-0.2.0 (c (n "pgnumeric") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)) (d (n "strtod") (r "^0.0.1") (d #t) (k 0)))) (h "17ad5qap4gmnh1fm8lfiijgm080r8gcx2fg6mslm7b0y1r245hjf") (f (quote (("big-endian-varlen"))))))

(define-public crate-pgnumeric-0.2.1 (c (n "pgnumeric") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.3") (d #t) (k 0)) (d (n "strtod") (r "^0.0.1") (d #t) (k 0)))) (h "1dpajf4wj5wicjgw8a7z8bika3v6bmppfqy6i8gdb809dwljwyhr") (f (quote (("big-endian-varlen"))))))

