(define-module (crates-io pg sq pgsql_builder) #:use-module (crates-io))

(define-public crate-pgsql_builder-0.0.1 (c (n "pgsql_builder") (v "0.0.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "modifier") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.11") (d #t) (k 0)))) (h "1ngad1qggc9djvv03dhl2sqcc1q12a9mrh5sx5yzn2a70nh06l9g")))

