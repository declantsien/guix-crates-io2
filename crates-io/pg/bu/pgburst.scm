(define-module (crates-io pg bu pgburst) #:use-module (crates-io))

(define-public crate-pgburst-0.1.0 (c (n "pgburst") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)))) (h "0kkmnhbgsd1rdf9gx2w00yccx247k5vz6p2gbwlgqh10gn8z41pv")))

(define-public crate-pgburst-0.1.2 (c (n "pgburst") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)))) (h "07askmammj1jmh21y4r0cb69ibshfpmznqqvmiy20kv3r2rprxxv")))

(define-public crate-pgburst-0.2.0 (c (n "pgburst") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)))) (h "0kxf0zjgk8sazsbrwb2pb0c0lmn0bysmwvri1v1x5g194andxx6x")))

(define-public crate-pgburst-0.2.1 (c (n "pgburst") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)))) (h "13r1xxv52wqadmvw4jnkz68n3996rmnwszv0jnq86fafln8iia8i")))

