(define-module (crates-io pg np pgnparse) #:use-module (crates-io))

(define-public crate-pgnparse-0.1.0 (c (n "pgnparse") (v "0.1.0") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0djcjmj01mpfma6lbcalhsjv5pj79wxnp39lqcd0wns0cxzh93kx")))

(define-public crate-pgnparse-0.1.1 (c (n "pgnparse") (v "0.1.1") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0492xpd9ibr0f4b39fs0ck4749hg6pmf9nwzf3rbs8zmn385wlav")))

(define-public crate-pgnparse-0.1.2 (c (n "pgnparse") (v "0.1.2") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0gkyk5z4v2ys4pik14nv5w0fm23nixm8z3v8rhrvvp1l8fyhrh8z")))

(define-public crate-pgnparse-0.1.3 (c (n "pgnparse") (v "0.1.3") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0x95azxz9fm46a9wdhwacka9bfrhsps8951mfdyza8smaf840l5z")))

(define-public crate-pgnparse-0.1.4 (c (n "pgnparse") (v "0.1.4") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "04wfb2x07ab4p5p7bx5zhjxmfp4k44s47lvvg79ahpn8kfw67xaj")))

(define-public crate-pgnparse-0.1.5 (c (n "pgnparse") (v "0.1.5") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0rbxpzhwhjl55w197a28c6mwrq7kdr64pb8rr217w7hjm591v01z")))

(define-public crate-pgnparse-0.1.6 (c (n "pgnparse") (v "0.1.6") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "1bbjnwr1i9jby9cyz2bir58wa56b1dnahmdcs0lc5ljjd4z3iqrd")))

(define-public crate-pgnparse-0.1.7 (c (n "pgnparse") (v "0.1.7") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "1mkylrd6g2pq2sxkjy3jzgm05qgb68f2dzi8ryhz5b1izvc0sn5n")))

(define-public crate-pgnparse-0.1.8 (c (n "pgnparse") (v "0.1.8") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "1dq42bzrysry2gwfvpcw0lvqd5ap99cwry6svyl4yibrf7hf44r1")))

(define-public crate-pgnparse-0.1.9 (c (n "pgnparse") (v "0.1.9") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0x3cicdxqhalksvhiqq5h55iv2x8a1fmac9m0jmkhpkn1r5j1mxk")))

(define-public crate-pgnparse-0.1.10 (c (n "pgnparse") (v "0.1.10") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0h7xrqvyram4bfrgzchgzdq6d3n4wcziyb5v0rfpximnhdn731bf")))

(define-public crate-pgnparse-0.1.11 (c (n "pgnparse") (v "0.1.11") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0jxsilfzspv0wv19nb0d8z0vf01hyw8d25c5zfmngnys48vx3q8r")))

(define-public crate-pgnparse-0.1.12 (c (n "pgnparse") (v "0.1.12") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0x54vpjd52vd9fsqckqlhz5rcgv0sa8jk4rsxr12m6y9s5slp4y3")))

(define-public crate-pgnparse-0.1.13 (c (n "pgnparse") (v "0.1.13") (d (list (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0y2ilq936akw1rh2b2xb385bpn1hn8syf51aafpl3vjikfi06xy1")))

(define-public crate-pgnparse-0.1.14 (c (n "pgnparse") (v "0.1.14") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "0bkjpancx9mmvjdylrk2p49g49w3nqkhbj9pkwhplrimxh45pw1c")))

(define-public crate-pgnparse-0.1.15 (c (n "pgnparse") (v "0.1.15") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "shakmaty") (r "^0.17.1") (d #t) (k 0)))) (h "1a7hyfca8496gssv4czfaqwfac672dqaid70f4d4hwlz9lak6lns")))

