(define-module (crates-io pg ty pgtypes) #:use-module (crates-io))

(define-public crate-pgtypes-0.1.0 (c (n "pgtypes") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.25.0") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0k1g2cw1g1zm5rxqnbaxmfwg10iigc438qih3bpnn6rysldnpfp9")))

