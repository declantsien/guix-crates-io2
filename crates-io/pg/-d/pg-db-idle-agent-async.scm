(define-module (crates-io pg -d pg-db-idle-agent-async) #:use-module (crates-io))

(define-public crate-pg-db-idle-agent-async-0.1.0 (c (n "pg-db-idle-agent-async") (v "0.1.0") (d (list (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "sqlx") (r "^0.7.4") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1anfiyizs12c55icd2zal5d11r6452sqny1wzk54s3fgkrxk9i87")))

