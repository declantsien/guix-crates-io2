(define-module (crates-io pg -d pg-db-idle-agent) #:use-module (crates-io))

(define-public crate-pg-db-idle-agent-0.0.1 (c (n "pg-db-idle-agent") (v "0.0.1") (h "00i1qhaqhhnn8xf98ydgnjx8rfcm0jip1mkdsycbv9il9kk1f4h2") (y #t)))

(define-public crate-pg-db-idle-agent-0.0.2 (c (n "pg-db-idle-agent") (v "0.0.2") (h "0jr651qb7rpnwqpd42ynv4yqx0wagw3fzpwklrmabq9xwdln26ah") (y #t)))

(define-public crate-pg-db-idle-agent-0.0.3 (c (n "pg-db-idle-agent") (v "0.0.3") (d (list (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "sqlx") (r "^0.7.4") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p365c59hnmg4npq7z3xxb3kmgljr05gy679pa6qmrd88zagfgp4") (y #t)))

(define-public crate-pg-db-idle-agent-0.0.4 (c (n "pg-db-idle-agent") (v "0.0.4") (d (list (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "sqlx") (r "^0.7.4") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z5g9fgyrmkx453k5rg7n7yd1a3k22mssidw23vig36kxrkkxy71") (y #t)))

