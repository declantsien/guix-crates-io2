(define-module (crates-io pg v- pgv-cli) #:use-module (crates-io))

(define-public crate-pgv-cli-0.1.0 (c (n "pgv-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pgv_rs") (r "^0.1.2") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "05368wbszbya4ghggvsrwy65zkvp5xnq9iqn68s6jdhhiv4j14mb")))

(define-public crate-pgv-cli-0.1.1 (c (n "pgv-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pgv_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0vyn78siq8c61bnwsdncdfs3yzz95x1gzd84wymwya0cfv7xbc3h")))

(define-public crate-pgv-cli-0.1.2 (c (n "pgv-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pgv_rs") (r "^0.1.5") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0r44vlahxf339x24ssai70nimzhb9aix2a6zhmwrlydss85fih3w")))

(define-public crate-pgv-cli-0.1.3 (c (n "pgv-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pgv_rs") (r "^0.1.7") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0gbibfzzn37qdsg4cnjvljcrjmmapp8qpwh6hdn187g9f6159cj3")))

(define-public crate-pgv-cli-0.1.4 (c (n "pgv-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pgv_rs") (r "^0.1.10") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "16l1q6mmis5ad7mr8qaghma8mfl6spqnp0r41ggv9p3vpclp5q58")))

