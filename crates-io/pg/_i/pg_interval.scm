(define-module (crates-io pg _i pg_interval) #:use-module (crates-io))

(define-public crate-pg_interval-0.0.1 (c (n "pg_interval") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0dam285wpcjvl71fvghx3588f7sxmx8149jgly7fqvrpdfy7wyvm") (f (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.1.0 (c (n "pg_interval") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0slarqvhf6glzwpff1b0a1b07az5865aq5vmyi90k3dzzsdzddca") (f (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.1.1 (c (n "pg_interval") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)))) (h "18wx0w5zc5v3zbny2k39b1pfgyaszgjyhi723wh106sbsw89f6y5") (f (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.2.0 (c (n "pg_interval") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1awiqkvg7wyzz3rmjrjcjja8rwwn0h3g3bhw6mh845djjp7wz05b") (f (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.3.0 (c (n "pg_interval") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (o #t) (d #t) (k 0)))) (h "00jqwhhp165kh4z8xxm5cgr323qrng2hxsb3p22s54ac9c58fig4") (f (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.4.1 (c (n "pg_interval") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12mryplx59hfx26pc5y8shs045s5ff9pjyn93kpaamwccnylsda7") (f (quote (("postgres" "postgres-types") ("default" "postgres"))))))

(define-public crate-pg_interval-0.4.2 (c (n "pg_interval") (v "0.4.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1whds37v95l3awjcf46kg0l64ypiivnwnrghiq24na2y8q5n8ipy") (f (quote (("postgres" "postgres-types") ("default" "postgres"))))))

