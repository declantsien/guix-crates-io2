(define-module (crates-io pg ba pgbar) #:use-module (crates-io))

(define-public crate-pgbar-0.1.0 (c (n "pgbar") (v "0.1.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0pgxwac7apdzr390zx7ri8af62pzr9hrvdbx5r1qvd71qrb6nvsw") (y #t)))

(define-public crate-pgbar-0.2.0 (c (n "pgbar") (v "0.2.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1j3cng9h669gwygi0lrd5882s5gz589l2lk9nydzwv1l0qkfawm0") (y #t)))

(define-public crate-pgbar-0.3.0 (c (n "pgbar") (v "0.3.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0y82n0xwl3qfn8igfm0wiwa9d6jcjw7p54ry8sdswkfrqgqrzz4b") (y #t)))

(define-public crate-pgbar-0.3.1 (c (n "pgbar") (v "0.3.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "002nzsnv1ya105nl3nq0xy5ckkkpgmqqhx24awdhyzni6qg95izq")))

