(define-module (crates-io pg -m pg-migrator) #:use-module (crates-io))

(define-public crate-pg-migrator-0.1.0 (c (n "pg-migrator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)))) (h "0nyn6bc2f74fbnda25rm84sci5p9q85s0a2nnzv5w4iij1cvlhlp") (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-pg-migrator-0.1.1 (c (n "pg-migrator") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)))) (h "13nlcy0cada8qs19m2yigbq4jiwhbj0i2bbyrjyz6agbqas4yljg") (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

(define-public crate-pg-migrator-0.2.1 (c (n "pg-migrator") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (f (quote ("with-chrono-0_4"))) (o #t) (k 0)))) (h "11316pm3a9lvvmg1fvpmmbps7s66923id39d1nmxksmsi8vfld0h") (s 2) (e (quote (("tokio-postgres" "dep:tokio-postgres") ("postgres" "dep:postgres"))))))

