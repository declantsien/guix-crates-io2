(define-module (crates-io pg -c pg-connection-string) #:use-module (crates-io))

(define-public crate-pg-connection-string-0.0.1 (c (n "pg-connection-string") (v "0.0.1") (h "1xcfrh679w9i94swv1pvd8v7ff9yf9v2936psqn0ijgzir7fznhs")))

(define-public crate-pg-connection-string-0.0.2 (c (n "pg-connection-string") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive" "color" "std"))) (o #t) (k 0)) (d (n "jacklog") (r "^0.2.0") (o #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng" "std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "1bb877xk7p7z14qjngqwypjb21a0ffympwpbcwrlnjlrryf0q1zb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_yaml") ("cli" "dep:clap" "dep:jacklog" "anyhow/backtrace" "dep:rand"))))))

