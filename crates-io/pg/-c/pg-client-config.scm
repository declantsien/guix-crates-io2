(define-module (crates-io pg -c pg-client-config) #:use-module (crates-io))

(define-public crate-pg-client-config-0.1.0 (c (n "pg-client-config") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0sdn4mdxbrk5b4g7cybsl26q1069p44g4v3d521wcim4vc6rkac8") (f (quote (("with-passfile") ("default" "with-passfile"))))))

(define-public crate-pg-client-config-0.1.1 (c (n "pg-client-config") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "157q3da1rr93n5apdxkhxg6k2a1vdkl0bsp3wm8prir7w94g5bpx") (f (quote (("with-passfile") ("default" "with-passfile"))))))

(define-public crate-pg-client-config-0.1.2 (c (n "pg-client-config") (v "0.1.2") (d (list (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "18as1l1y18xm62l1c2g66nllrbarl06lxy77q7rck6abgmw4w0ly") (f (quote (("with-passfile") ("default" "with-passfile"))))))

