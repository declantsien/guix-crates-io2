(define-module (crates-io pg en pgen) #:use-module (crates-io))

(define-public crate-pgen-0.2.1 (c (n "pgen") (v "0.2.1") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1483g8pf9ihl1bmmq2n5y1xs1vp0xwkg9hr0wrb1y5f7irq5mki8")))

(define-public crate-pgen-0.2.2 (c (n "pgen") (v "0.2.2") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0rwnyifd5hbyr245p8aw1n90zr4yhrikxyi0kp4kwlc7jlgajwzg")))

(define-public crate-pgen-0.2.3 (c (n "pgen") (v "0.2.3") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1ymi0v3xnm8ksz5z3pbl7vy70h7y04xcxzvphwx1c8c4zhhgydbz")))

(define-public crate-pgen-0.2.4 (c (n "pgen") (v "0.2.4") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "089mjfz097nk177xq43fhknbq4wydnd1qq6nq07zavya2i8i4zv4")))

(define-public crate-pgen-0.2.5 (c (n "pgen") (v "0.2.5") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0qvrj26yz17xdv3iv6pwzhh6sg7a4qyaglvyqdpc3macgmv889bn")))

(define-public crate-pgen-0.3.0 (c (n "pgen") (v "0.3.0") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1gk4gcz2q93mqrkiz0kwg6qyw6ax0rq2p8b9ziq97fsx8i4gs6xn")))

(define-public crate-pgen-0.4.0 (c (n "pgen") (v "0.4.0") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0l41q64a6l8xslxyk4g3mqbz3l38z8m81bqpd6afaarc012j4mw3")))

(define-public crate-pgen-1.0.0 (c (n "pgen") (v "1.0.0") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1icwjbwmwc2kwrjwswfgy35axqh2a8m72abxd7m24w6m0a044nz8")))

(define-public crate-pgen-1.0.1 (c (n "pgen") (v "1.0.1") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0rg6dxnrm90cyw8dzf0xp8kflqiyr64yhmwpkf8zxgkjya6yir63")))

(define-public crate-pgen-1.0.2 (c (n "pgen") (v "1.0.2") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0klwzf5a8cdl8a91cmvb6ipl8kflv31n9n3hz8hylljp8ly88lv0")))

(define-public crate-pgen-1.0.3 (c (n "pgen") (v "1.0.3") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1m149bxffz2pv11f5py6f28gkv173v2p8w3mzfn3dw6km0zvp8f1")))

(define-public crate-pgen-1.0.4 (c (n "pgen") (v "1.0.4") (d (list (d (n "clap") (r "~2.29.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1mnw2yzv433331m5adrllvk4h4bb71skksx17kl74q2wqbawbwf4")))

(define-public crate-pgen-1.0.5 (c (n "pgen") (v "1.0.5") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1hn3xmy955wczh9sq30c4npf31234dqisvlg3jrafwqhz6dg9pk1")))

(define-public crate-pgen-1.1.0 (c (n "pgen") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0zl1ak8q7s2d0vhxqwsmgvjbqm6118m02z8agyc3659w0hdjgg9w")))

(define-public crate-pgen-1.1.1 (c (n "pgen") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0c8cqljx95pp2d47xni824b4ph4m4dngw2ahkn2dvmrzmd7vji7c")))

(define-public crate-pgen-1.1.2 (c (n "pgen") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0pgc26isqi71p1ih94vbnhj910lsyg247dsmkyvykv0kkcax5m51")))

(define-public crate-pgen-1.1.3 (c (n "pgen") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1xf788zjbj4s8s0awmsk8z56ll119cmymn2md5y4gfixvqd3zyv2")))

(define-public crate-pgen-1.1.4 (c (n "pgen") (v "1.1.4") (d (list (d (n "clap") (r "^4.1") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "106h49vqvbn7jmh95ryifwwwf17sj9zc1pjy1yvx0xv6fsirhcxw")))

(define-public crate-pgen-1.2.0 (c (n "pgen") (v "1.2.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zqwvvdyf8hdaw1vwbqkqcprrvb3p8k2c787iwzf7jjch6sggdhp")))

(define-public crate-pgen-1.3.0 (c (n "pgen") (v "1.3.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1p7xzp11qv01yq8d4kw59s1iy985ajkn7css8xsxd1jaq2jl533s")))

(define-public crate-pgen-2.0.0-alpha.1 (c (n "pgen") (v "2.0.0-alpha.1") (d (list (d (n "bip39-lexical-data") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)) (d (n "eff-lexical-data") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10wa1fimrn7gjlr6idw40a26liy29b3x1868f9fpzlwnkv71y01j")))

(define-public crate-pgen-2.0.0 (c (n "pgen") (v "2.0.0") (d (list (d (n "bip39-lexical-data") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "help" "usage" "error-context"))) (k 0)) (d (n "eff-lexical-data") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qyjlvzqxgkkzsz3qjclb60ls60nywvwq2vxz020lb2rndjkbgq2")))

