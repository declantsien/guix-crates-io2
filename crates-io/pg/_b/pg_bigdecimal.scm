(define-module (crates-io pg _b pg_bigdecimal) #:use-module (crates-io))

(define-public crate-pg_bigdecimal-0.1.0 (c (n "pg_bigdecimal") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)))) (h "0sjilmbf71l32gsg4lqwpq7a9m628k7456savfv79lzy5x6bih51")))

(define-public crate-pg_bigdecimal-0.1.1 (c (n "pg_bigdecimal") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0gi16d79jigyn9qhfnwflhy8pc7jr2qagw11jznxh81g05xaymli")))

(define-public crate-pg_bigdecimal-0.1.2 (c (n "pg_bigdecimal") (v "0.1.2") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0dp3c0k2g4iq4zx81q9rjkvabrr06yg42fjl1pp44aia42yf1lx0")))

(define-public crate-pg_bigdecimal-0.1.3 (c (n "pg_bigdecimal") (v "0.1.3") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0lssdpz1izjxshm2imy4jqvzbi2f12nmrhw3pymvyms107ybfy3g")))

(define-public crate-pg_bigdecimal-0.1.4 (c (n "pg_bigdecimal") (v "0.1.4") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1pw8y7fipf41v1pinlvlzj1ivw171pwnzhbp7iklph491g66gvxa")))

(define-public crate-pg_bigdecimal-0.1.5 (c (n "pg_bigdecimal") (v "0.1.5") (d (list (d (n "bigdecimal") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)))) (h "05h2irycj1wvjfnac164q5ii4q2ymxvna8zaq1iaya25qya5m1gr")))

