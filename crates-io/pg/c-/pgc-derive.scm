(define-module (crates-io pg c- pgc-derive) #:use-module (crates-io))

(define-public crate-pgc-derive-0.1.0 (c (n "pgc-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1mi5prdf5mafisky6sz6fkhl2m0z2s3rmkklvf0vf1nxggad7w58")))

(define-public crate-pgc-derive-0.1.1 (c (n "pgc-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "12k52q8q08hmmz15iyr0f049lmaawwr2cqmz8jv6sinlsmpl9az3")))

