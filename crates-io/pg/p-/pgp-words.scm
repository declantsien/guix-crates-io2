(define-module (crates-io pg p- pgp-words) #:use-module (crates-io))

(define-public crate-pgp-words-1.0.0 (c (n "pgp-words") (v "1.0.0") (h "1zjypsxwl1bpsrmxw4x5nn7rkw7wn0pnwazc1vv5wq3l8cq61fsn")))

(define-public crate-pgp-words-1.1.0 (c (n "pgp-words") (v "1.1.0") (h "0iq9y8slzxvhjrdfyms2q0kvppqn6waf6n5hfcw1a4b02miv8b95")))

