(define-module (crates-io pg id pgident) #:use-module (crates-io))

(define-public crate-pgident-0.0.1 (c (n "pgident") (v "0.0.1") (h "1wpph6rnd1052irqv4d7nwxnka6lv44nlkyvc006wcrmsrw9g8w7")))

(define-public crate-pgident-0.0.2 (c (n "pgident") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12m3626llmgjk115h2k8xxhlalgrczz9p1plkx6xs0l07gdp8ml9")))

(define-public crate-pgident-0.0.3 (c (n "pgident") (v "0.0.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08cbdznhj5jn84ag4k8jrnjr6i3swnxi044c6a4bac1s6qrgjyz0")))

