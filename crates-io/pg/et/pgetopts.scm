(define-module (crates-io pg et pgetopts) #:use-module (crates-io))

(define-public crate-pgetopts-0.1.0 (c (n "pgetopts") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "01qzza6q82636cx0ardz3s74c5yh66xl80gl2b9grm9mqwnl334l")))

(define-public crate-pgetopts-0.1.1 (c (n "pgetopts") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "086rxfjjs8m1pv1z080i4idwh20y6i5a2xval8p7spcx23fqsqac")))

(define-public crate-pgetopts-0.1.2 (c (n "pgetopts") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "07jz55rpvz0xakdl46pjqk67clggx9l5jylfvb3pmzs1ikmm39i7")))

