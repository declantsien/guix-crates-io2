(define-module (crates-io pg #{-i}# pg-interval-sql-json-binding) #:use-module (crates-io))

(define-public crate-pg-interval-sql-json-binding-1.0.0 (c (n "pg-interval-sql-json-binding") (v "1.0.0") (d (list (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0c4zbbhwwdq2pwjly0iq7nw5dg4ay49yln9a8hhks00a975xb21g")))

(define-public crate-pg-interval-sql-json-binding-1.1.0 (c (n "pg-interval-sql-json-binding") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0lnc5qfdv943k3xv4v4acd1yv71h0a2yz7skvqnancsm7fm1fb7l")))

(define-public crate-pg-interval-sql-json-binding-1.2.0 (c (n "pg-interval-sql-json-binding") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1xsih0z4z4jplx19ydxzp7mcnn2f5plw3x79kap6xq00czzalyx3")))

