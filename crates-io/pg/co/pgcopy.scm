(define-module (crates-io pg co pgcopy) #:use-module (crates-io))

(define-public crate-pgcopy-0.0.1 (c (n "pgcopy") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1mah6cf3mv89xwn91fiks3zixi1s7ylf3ng32y3bikjwbznlnj76") (f (quote (("with-uuid" "uuid") ("with-chrono" "chrono") ("default") ("all" "with-uuid" "with-chrono"))))))

(define-public crate-pgcopy-0.0.2 (c (n "pgcopy") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "eui48") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0rpkxi0hnv27pmfs4j1bhn70jyadzhn18gwa5garaxnp9h8jjb96") (f (quote (("with-uuid" "uuid") ("with-eui48" "eui48") ("with-chrono" "chrono") ("default") ("all" "with-uuid" "with-chrono" "with-eui48"))))))

