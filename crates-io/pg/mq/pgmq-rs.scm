(define-module (crates-io pg mq pgmq-rs) #:use-module (crates-io))

(define-public crate-pgmq-rs-0.0.1-alpha (c (n "pgmq-rs") (v "0.0.1-alpha") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0323wzi5jyij5fs9q2jwfmqy8v601fban4f144hwwb6cdww4df0d") (y #t)))

