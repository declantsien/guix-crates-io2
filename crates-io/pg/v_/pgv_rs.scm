(define-module (crates-io pg v_ pgv_rs) #:use-module (crates-io))

(define-public crate-pgv_rs-0.1.0 (c (n "pgv_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "0zib6rlnsb9qdxb4jkf7vic01l3fwjvh0rqddbygrdjxi9ckpmy5")))

(define-public crate-pgv_rs-0.1.1 (c (n "pgv_rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "18xqx41sz8akrc0wr67yfa7q0mn3757jxj9i0pnz0xlpzgf1dq9w")))

(define-public crate-pgv_rs-0.1.2 (c (n "pgv_rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1rjciqbs805bc3fh40c7qcslaii9k8zf0x7zn71lglrlyd54a3kh")))

(define-public crate-pgv_rs-0.1.3 (c (n "pgv_rs") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1k2xd588wsbsrsj1rnz9q4sx40l98zy3qcln1xm0aky8flm4vc18")))

(define-public crate-pgv_rs-0.1.4 (c (n "pgv_rs") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "0kznf8im3fy8fvz950m1bk8ngypyagn31kcj2n1lb3qy24gwhkv2")))

(define-public crate-pgv_rs-0.1.5 (c (n "pgv_rs") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "0sfpcr1jw5kkwl9k06wl9r6mm0lbh39v3rqv78pbp7cg8i9kdb2n")))

(define-public crate-pgv_rs-0.1.6 (c (n "pgv_rs") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "0z38mi9ppqyqdzdxr7s8099cy738rl1gj7qksbsblxk8w9axlys7")))

(define-public crate-pgv_rs-0.1.7 (c (n "pgv_rs") (v "0.1.7") (d (list (d (n "bumpalo") (r "^3.12.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1kq5j4sddzl2al5z9ypifz7rgz3j44b14da01yyv64hm6rwzp1h4")))

(define-public crate-pgv_rs-0.1.8 (c (n "pgv_rs") (v "0.1.8") (d (list (d (n "bumpalo") (r "^3.12.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1f5if6zz65s5fcw1y2hym0sxknr50sra31903kcq6hn1y30l4mgy")))

(define-public crate-pgv_rs-0.1.9 (c (n "pgv_rs") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "1q16ri2dqvs2pxjgk9jfiynjd4l2ynsf6dsp522mqq12s09mi9q5")))

(define-public crate-pgv_rs-0.1.10 (c (n "pgv_rs") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 2)))) (h "0qqa55l7ylwdvrg2g3bbpxh748wxriijbf5jbcvd1sk856makvwg")))

