(define-module (crates-io pg db pgdb) #:use-module (crates-io))

(define-public crate-pgdb-0.1.0 (c (n "pgdb") (v "0.1.0") (d (list (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "process_guard") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "0smmjdjlwid5xjzanahvcwmrx90gqjcsgyx7fv54s8nx57a3vr6r")))

(define-public crate-pgdb-0.1.1 (c (n "pgdb") (v "0.1.1") (d (list (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "process_guard") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "1yn397skvc032afclz1dkb5zxr0vibing7qzmchgf3x7hjnqai94")))

(define-public crate-pgdb-0.1.2 (c (n "pgdb") (v "0.1.2") (d (list (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "process_guard") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "1lcq2vg73hgm2a3ka1dvrz0yndiyffn9by3gml1irfdrqmgcnk8v")))

(define-public crate-pgdb-0.2.0 (c (n "pgdb") (v "0.2.0") (d (list (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "process_guard") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "07vz15bxldimc74j75jx61cl0snpp0dqdsmbvgyr2pwvxq19n9rq")))

(define-public crate-pgdb-0.3.0 (c (n "pgdb") (v "0.3.0") (d (list (d (n "hex_fmt") (r "^0.3.0") (d #t) (k 0)) (d (n "process_guard") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 0)))) (h "1dwz5vgykyjxd4h3xq8yz04did2g1lg33mm51zp5jsb7apk5h25c")))

