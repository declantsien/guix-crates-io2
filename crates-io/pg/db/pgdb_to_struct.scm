(define-module (crates-io pg db pgdb_to_struct) #:use-module (crates-io))

(define-public crate-pgdb_to_struct-0.1.0 (c (n "pgdb_to_struct") (v "0.1.0") (d (list (d (n "app_properties") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0bxdz8rnipqwnrin8fgpn9q3g6xdwy18nh2a51kcr1fmw88pr6rm")))

(define-public crate-pgdb_to_struct-0.1.1 (c (n "pgdb_to_struct") (v "0.1.1") (d (list (d (n "app_properties") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "1mm1nz9c72r6swp0w47dagxjymss9jjp70d7r7y8c7f4304fpim0")))

(define-public crate-pgdb_to_struct-0.1.2 (c (n "pgdb_to_struct") (v "0.1.2") (d (list (d (n "app_properties") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "19cd1daknf8l7qvsvmwd53pj2xd0fj8vpw02di45cyff4ps4zixm")))

(define-public crate-pgdb_to_struct-0.1.3 (c (n "pgdb_to_struct") (v "0.1.3") (d (list (d (n "app_properties") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.1") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "01p2zdlzwbavcf35q72gp113ffgbhyrv7i798aw55yqlw1m9i32s")))

