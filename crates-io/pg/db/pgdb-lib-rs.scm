(define-module (crates-io pg db pgdb-lib-rs) #:use-module (crates-io))

(define-public crate-pgdb-lib-rs-0.1.0 (c (n "pgdb-lib-rs") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-async-std-native-tls" "postgres" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "1r3xq0wyjp6a9245f633ysgrrinwsmy4af6bs5icvyyydlyi09ns")))

(define-public crate-pgdb-lib-rs-0.1.1 (c (n "pgdb-lib-rs") (v "0.1.1") (d (list (d (n "dotenvy") (r "^0.15.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-async-std-native-tls" "postgres" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "18arbgnwl3ghvm5lni1mmwwf3x03cqspf5n14qh4ywyd6bzsd3r0")))

(define-public crate-pgdb-lib-rs-0.1.2 (c (n "pgdb-lib-rs") (v "0.1.2") (d (list (d (n "dotenvy") (r "^0.15.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-async-std-native-tls" "postgres" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 0)))) (h "15ymb1n0zifxlsk8hkvwqf8ifzs9iv0ap01jm7285rz7d8rjhzki")))

