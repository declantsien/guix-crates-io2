(define-module (crates-io pg db pgdb_cli) #:use-module (crates-io))

(define-public crate-pgdb_cli-0.1.2 (c (n "pgdb_cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "pgdb") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0rmimdljsy7x5vddrqgp17lnladrjvl1i0j3n7v428z3p74663dp")))

(define-public crate-pgdb_cli-0.2.0 (c (n "pgdb_cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "pgdb") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "16lb1fa2z00zjzy2k7099fb2kgncfd9bs0cgp2dyrn2cdicr55s5")))

(define-public crate-pgdb_cli-0.3.0 (c (n "pgdb_cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "pgdb") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0hg0w8p0q4g7vslxz4rygy7wwlkbms3agfidn42r6z91sv84w77m")))

