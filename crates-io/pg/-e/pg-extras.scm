(define-module (crates-io pg -e pg-extras) #:use-module (crates-io))

(define-public crate-pg-extras-0.0.1 (c (n "pg-extras") (v "0.0.1") (d (list (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (f (quote ("db-postgres"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.33") (d #t) (k 0)))) (h "0z8vyc7p2qsv28ys72mhbkmqbdzs1cardvcp9lc06vva8pfbzs8y")))

(define-public crate-pg-extras-0.0.2 (c (n "pg-extras") (v "0.0.2") (d (list (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (f (quote ("db-postgres"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.33") (d #t) (k 0)))) (h "18rwxwxxh487mbg2267ji73cjr471ql2cfp1953hbbdhrmj10k39")))

(define-public crate-pg-extras-0.0.3 (c (n "pg-extras") (v "0.0.3") (d (list (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (f (quote ("db-postgres"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.33") (d #t) (k 0)))) (h "0adj4fmzhmlgsah0i27fnczrizlp8ggp4b53iy44a5g3s1x2b1ki")))

(define-public crate-pg-extras-0.1.0 (c (n "pg-extras") (v "0.1.0") (d (list (d (n "pg_interval") (r "^0.4.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (f (quote ("db-postgres"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ig31iyaq445chnpdvrrsqaafxiyvsclgg5ljmqkn0c77xrih186")))

(define-public crate-pg-extras-0.2.0 (c (n "pg-extras") (v "0.2.0") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "bigdecimal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3wzrynqnhyvin584xl248vkn5bgcaw1hvhdh7qdi133wxwam5r")))

(define-public crate-pg-extras-0.3.0 (c (n "pg-extras") (v "0.3.0") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "bigdecimal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qj7w0sy5inya9nvw6q8izz1dyl10nqp4x396ri3d74wsawkag4m")))

(define-public crate-pg-extras-0.3.1 (c (n "pg-extras") (v "0.3.1") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "bigdecimal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m23q7il61jj86ij34gkrh9z7308d88r0m1nmsp87djijllqwlkq")))

(define-public crate-pg-extras-0.3.2 (c (n "pg-extras") (v "0.3.2") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "bigdecimal"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14rgb0vackdg98yfvh7c99jcq8xrnfnan4wcd4r0ksf697is5yvy")))

