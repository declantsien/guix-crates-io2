(define-module (crates-io pg -e pg-extend) #:use-module (crates-io))

(define-public crate-pg-extend-0.1.0 (c (n "pg-extend") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "1fbbvnhnjsmgs81726p82w0pm2ksflahr6cvhc6r7hgp9v4ks5nd")))

(define-public crate-pg-extend-0.2.0 (c (n "pg-extend") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "1j5v0rmivp473cwx9zk78bsd3sjmc1xghfv70hld8mdcxx8ngpfa")))

(define-public crate-pg-extend-0.2.1 (c (n "pg-extend") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "no-panic") (r "^0.1.6") (d #t) (k 0)))) (h "1v2f4xjcnw2x0lahv1wcjqn2myvhd6kplhwrkql6009vlqfdxqzd")))

