(define-module (crates-io pg -e pg-extern-attr) #:use-module (crates-io))

(define-public crate-pg-extern-attr-0.1.0 (c (n "pg-extern-attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing"))) (d #t) (k 0)))) (h "0z4i1scyav8cj3hq86s4ajwz6f4ahbsk3a58f3zsx6x25iwi958h")))

(define-public crate-pg-extern-attr-0.2.0 (c (n "pg-extern-attr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing"))) (d #t) (k 0)))) (h "0gwfmmgp4ryarcx6s54la7x6gzckawfdcccffhqza8zr353gwsif")))

(define-public crate-pg-extern-attr-0.2.1 (c (n "pg-extern-attr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing"))) (d #t) (k 0)))) (h "0a1gl0ppfr9cywswq6vq1bjz3irp09ayq67krh30y81b2j4y1xxm")))

(define-public crate-pg-extern-attr-0.2.2 (c (n "pg-extern-attr") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "fold" "parsing"))) (d #t) (k 0)))) (h "0zv11a0lih7kilivrfib3rfcaqqj52b505x7720qn13sknbvc4zr")))

