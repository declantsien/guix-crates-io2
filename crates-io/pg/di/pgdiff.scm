(define-module (crates-io pg di pgdiff) #:use-module (crates-io))

(define-public crate-pgdiff-0.1.0 (c (n "pgdiff") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive-deref-rs") (r "^0.1") (d #t) (k 0)) (d (n "elephantry") (r "^3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 2)) (d (n "similar") (r "^2.2") (d #t) (k 2)))) (h "0w8mcfqsmsdqbm1pgww2607qbrivsybahysby1xzv3n3n79nks8w")))

