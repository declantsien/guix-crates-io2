(define-module (crates-io p0 f_ p0f_api) #:use-module (crates-io))

(define-public crate-p0f_api-0.1.0 (c (n "p0f_api") (v "0.1.0") (h "1vbnzifvabxzslk98pkgvknwphpx9lfraawlcw369wlajym06ikl")))

(define-public crate-p0f_api-0.1.1 (c (n "p0f_api") (v "0.1.1") (h "1cmkk6xla87brwnmqzg18l3rycma992cnv3zfpkmyf7s3ga1qngy")))

(define-public crate-p0f_api-0.1.2 (c (n "p0f_api") (v "0.1.2") (h "05g6vrliyz1xfakplkl3v34kp4dmwkdx1232nkmh08lxcjkx8b1i")))

