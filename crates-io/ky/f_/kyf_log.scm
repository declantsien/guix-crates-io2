(define-module (crates-io ky f_ kyf_log) #:use-module (crates-io))

(define-public crate-kyf_log-0.1.0 (c (n "kyf_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "09nnhvss395nbdy79aa8zg42585yzj85923a6lv2bfsg92jpm75m")))

