(define-module (crates-io ky th kythera-common) #:use-module (crates-io))

(define-public crate-kythera-common-0.1.0 (c (n "kythera-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0.0") (d #t) (k 0)) (d (n "frc42_dispatch") (r "^3.1.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0c96144wnki8p3359qafdm6w9y21cwk1gzv83d8b262cm6awk9j2")))

