(define-module (crates-io ky th kythera-actors) #:use-module (crates-io))

(define-public crate-kythera-actors-0.1.0 (c (n "kythera-actors") (v "0.1.0") (h "17kdcd6sjnyrwh19yp35ghqa313cbia22q6ciz71qlayz8l7997n") (f (quote (("testing") ("default"))))))

(define-public crate-kythera-actors-0.2.0 (c (n "kythera-actors") (v "0.2.0") (h "0zkf24bsvqsxxp5ggw5n7lw4q1829014ms9md6x8dam7dlamglsm") (f (quote (("testing") ("default"))))))

