(define-module (crates-io ky th kythera-fvm) #:use-module (crates-io))

(define-public crate-kythera-fvm-0.1.0 (c (n "kythera-fvm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cid") (r "^0.8.5") (k 0)) (d (n "fvm") (r "^3.1.0") (f (quote ("testing"))) (k 0)) (d (n "fvm_ipld_blockstore") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^3.1.0") (d #t) (k 0)) (d (n "kythera-common") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.1") (k 0)))) (h "0b6nb898hpka3cmby9ziad8gwp0mi4sf145cl0wrj4z9nqb66p9g")))

(define-public crate-kythera-fvm-0.2.0 (c (n "kythera-fvm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cid") (r "^0.8.5") (k 0)) (d (n "fvm") (r "^3.1.0") (f (quote ("testing"))) (k 0)) (d (n "fvm_ipld_blockstore") (r "^0.1.1") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^3.1.0") (d #t) (k 0)) (d (n "kythera-common") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "multihash") (r "^0.16.1") (k 0)))) (h "06idfdyyh7002cmiipyc86jb74z7qf4n4g6646wlh2x29kfp1f0q")))

