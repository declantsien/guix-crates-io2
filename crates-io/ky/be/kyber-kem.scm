(define-module (crates-io ky be kyber-kem) #:use-module (crates-io))

(define-public crate-kyber-kem-0.1.0 (c (n "kyber-kem") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.5") (d #t) (k 0)))) (h "02k847h8kyi17kfzj16prwd69pwfy3lpv7v227f4kip8pv8z3rba")))

(define-public crate-kyber-kem-0.1.1 (c (n "kyber-kem") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.5") (d #t) (k 0)))) (h "0pmnl89yh5l5rqs6lh2r8m4kzxx1csih3f9gr3y33k7nnk81g4v9")))

(define-public crate-kyber-kem-0.1.3 (c (n "kyber-kem") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "subtle") (r "^2.5") (d #t) (k 0)))) (h "1i50vdav0j2q6f9lrrz91d66apj1nn5gsql0ka1wlqqsmxnd0v9l")))

