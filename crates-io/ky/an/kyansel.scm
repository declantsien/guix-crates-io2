(define-module (crates-io ky an kyansel) #:use-module (crates-io))

(define-public crate-kyansel-0.1.0 (c (n "kyansel") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (f (quote ("sync"))) (d #t) (k 2)))) (h "1gsrdgmwhjpi7im2lhhiwkqc2fhl37nz6vii86g8vxn6biyf19j0")))

(define-public crate-kyansel-0.2.0 (c (n "kyansel") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures01") (r "^0.1") (o #t) (k 0) (p "futures")) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio01") (r "^0.1") (f (quote ("sync"))) (d #t) (k 2) (p "tokio")))) (h "00zwrjvb7a1jl0y7zrsg5rl1w8d45wkpmmvrpwyj9vzdki107k1y") (f (quote (("futures_01" "futures01"))))))

(define-public crate-kyansel-0.2.1 (c (n "kyansel") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures01") (r "^0.1") (o #t) (k 0) (p "futures")) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio01") (r "^0.1") (f (quote ("sync"))) (d #t) (k 2) (p "tokio")))) (h "0x8y53q4myp4slim2cqnhag5ka8ial3n270842vq5frdpladgqnm") (f (quote (("futures_01" "futures01"))))))

(define-public crate-kyansel-0.3.0 (c (n "kyansel") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures_01") (r "^0.1") (o #t) (k 0) (p "futures")) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio_01") (r "^0.1") (f (quote ("sync"))) (d #t) (k 2) (p "tokio")))) (h "0d8nxgd8zpfx4hl8wvvnc62bigmsa6al4h9jq851x2mzfb6wpxkb")))

(define-public crate-kyansel-0.3.1 (c (n "kyansel") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures_01") (r "^0.1") (o #t) (k 0) (p "futures")) (d (n "tokio") (r "^0.2.0-alpha.6") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio_01") (r "^0.1") (f (quote ("sync"))) (d #t) (k 2) (p "tokio")))) (h "16py02ns44py8f2f1zs1ikccwh5mjz2b6ir7fdqwxy03qhm2dfds")))

