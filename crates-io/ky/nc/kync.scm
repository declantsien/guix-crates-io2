(define-module (crates-io ky nc kync) #:use-module (crates-io))

(define-public crate-kync-0.1.0 (c (n "kync") (v "0.1.0") (d (list (d (n "kync_test_plugin") (r "^0.1.0") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "18bpifk7fkbaji3ym8qv8cysy93ak1yk1px1mm8i0ww6plwg625n") (y #t)))

(define-public crate-kync-0.1.1 (c (n "kync") (v "0.1.1") (d (list (d (n "kync_test_plugin") (r "^0.1.0") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "1cqyzfjkvfjz83hqcmdrr1s8dc1sh9mwm9q3bljpxbv0l670acx9") (y #t)))

(define-public crate-kync-0.1.2 (c (n "kync") (v "0.1.2") (d (list (d (n "kync_test_plugin") (r "^0.1.0") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "1ayyrlq4sl07qbs09z2jiaflaxij172xr4d3dmzmgz8rymxmlcpp") (y #t)))

(define-public crate-kync-0.1.3 (c (n "kync") (v "0.1.3") (d (list (d (n "kync_test_plugin") (r "^0.1.1") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "0mc30pwz231h3rsmd52imlaga6q2raqcv2q1kcpkidwmx555ly6n") (y #t)))

(define-public crate-kync-0.1.4 (c (n "kync") (v "0.1.4") (d (list (d (n "kync_test_plugin") (r "^0.1.2") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "159rxddapi74p1jpi2s9j8h3nw62mk326068x79hi4aplj3dr6jy") (y #t)))

(define-public crate-kync-0.1.5 (c (n "kync") (v "0.1.5") (d (list (d (n "kync_test_plugin") (r "^0.1.2") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "19jrzfag5ar9x5n4iz2w1qixl2cxq53q8xpsgld5rs6x4dn4xs3j") (y #t)))

(define-public crate-kync-0.1.6 (c (n "kync") (v "0.1.6") (d (list (d (n "kync_test_plugin") (r "^0.1.2") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "15z8lsvfi3fn9lzr04k3qcxw91bk3r4ppy4101ydyqm53c52p4f0") (y #t)))

(define-public crate-kync-0.1.7 (c (n "kync") (v "0.1.7") (d (list (d (n "kync_test_plugin") (r "^0.1.4") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "0b4biw7iaaklywms8v2605gz6zassj3v64dvzsw3z1mngah37mip") (y #t)))

(define-public crate-kync-0.1.8 (c (n "kync") (v "0.1.8") (d (list (d (n "kync_test_plugin") (r "^0.1.4") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "1m8k35jahzys2bxyj4c1l03g8wdk2n3qic2vhq7lr5h8nmnfcibp") (y #t)))

(define-public crate-kync-0.2.0 (c (n "kync") (v "0.2.0") (d (list (d (n "kync_test_plugin") (r "^0.2.1") (d #t) (k 2)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1i97sawib3j9arkqaq7jbln88fbjdqxnfhf7flrwa84nyasvdniq") (y #t)))

