(define-module (crates-io ky nc kync_test_plugin) #:use-module (crates-io))

(define-public crate-kync_test_plugin-0.1.0 (c (n "kync_test_plugin") (v "0.1.0") (h "0j6143saqi6km370nd8w99v2j3bj1xkj8m9j6wdbwxyicfvrbygh") (y #t)))

(define-public crate-kync_test_plugin-0.1.1 (c (n "kync_test_plugin") (v "0.1.1") (h "0x9djkyp9j7sgjznmrpzig8kpajl18x0lmhby13blkvkxfxlnk2s") (y #t)))

(define-public crate-kync_test_plugin-0.1.2 (c (n "kync_test_plugin") (v "0.1.2") (h "1iac1611m0d96kh57nma2bjp1hwf7d36ndm8ncylsbzbwgihsqqn") (y #t)))

(define-public crate-kync_test_plugin-0.1.3 (c (n "kync_test_plugin") (v "0.1.3") (h "16sxbplp4l8pigqw9a60pyqk33i322hkl5bb0q5kbgs8yjdiyi9g") (y #t)))

(define-public crate-kync_test_plugin-0.1.4 (c (n "kync_test_plugin") (v "0.1.4") (h "18iiky57njxp8ri1g47c1dxngdmqg85yysj3adrdavcz0w232b7i") (y #t)))

(define-public crate-kync_test_plugin-0.2.0 (c (n "kync_test_plugin") (v "0.2.0") (h "188l6b16mpr5c48cylni4acq127zcdimc973d5dicn1mss73y28g") (y #t)))

(define-public crate-kync_test_plugin-0.2.1 (c (n "kync_test_plugin") (v "0.2.1") (h "1wn5gnp82ba6fgj6fsn0gs9ra7jrmfylxvfg4a3dvmpwjkxnslv6") (y #t)))

