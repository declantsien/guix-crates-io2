(define-module (crates-io ky ot kyoto_main) #:use-module (crates-io))

(define-public crate-kyoto_main-0.1.0 (c (n "kyoto_main") (v "0.1.0") (d (list (d (n "kyoto_data") (r "^0.1.0") (d #t) (k 0)) (d (n "kyoto_network") (r "^0.1.0") (d #t) (k 0)) (d (n "kyoto_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (d #t) (k 0)))) (h "1551piydcql4ijb598msn288b3m247bz43is80605pypy92x3fgc")))

