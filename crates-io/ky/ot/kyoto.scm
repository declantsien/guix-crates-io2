(define-module (crates-io ky ot kyoto) #:use-module (crates-io))

(define-public crate-kyoto-0.0.1 (c (n "kyoto") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (d #t) (k 0)))) (h "14hyyrk5n18rw588i67a1wiy8ss5igk6qmc7yzri0zbd2nyx25hh")))

