(define-module (crates-io ky ot kyoto_protocol) #:use-module (crates-io))

(define-public crate-kyoto_protocol-0.0.1 (c (n "kyoto_protocol") (v "0.0.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kpaikcrk7mvla8y3zypgwn1gkhsw6ax51n8jgaar3kjsns8chvr")))

(define-public crate-kyoto_protocol-0.1.0 (c (n "kyoto_protocol") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n4v0ak720gh7dhafafrb36bass4w6a8wfmd6r8bnkakhmiwyl7n")))

