(define-module (crates-io ky ot kyotocabinet) #:use-module (crates-io))

(define-public crate-kyotocabinet-0.0.1 (c (n "kyotocabinet") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0zs2j88yv4hqlmlvva2h9hnnv4c8w1wcriam7x0b0yy38lca26ax")))

(define-public crate-kyotocabinet-0.0.2 (c (n "kyotocabinet") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "08l62m9j9k2sj8njl8kiwc8i0lxsy1s61kd3ywzm99z24cc1mi68")))

(define-public crate-kyotocabinet-0.0.3 (c (n "kyotocabinet") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "12sbbngi6k91mv1p9g7vbj5nnxgr1lf51hpglk9jkh8by7fi070y")))

(define-public crate-kyotocabinet-0.0.5 (c (n "kyotocabinet") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0qh71xybrinqrk4k613y9lsbh33fgcs5ghdr48djahgilymplb4x")))

(define-public crate-kyotocabinet-0.0.6 (c (n "kyotocabinet") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0ykd4ihhyw36c6b4mka55n49hi49hrf0pc62xay2sridj8cby417")))

