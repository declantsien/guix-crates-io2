(define-module (crates-io ky ot kyoto_machine) #:use-module (crates-io))

(define-public crate-kyoto_machine-0.1.0 (c (n "kyoto_machine") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "kyoto_data") (r "^0.1.0") (d #t) (k 0)) (d (n "kyoto_protocol") (r "^0.1.0") (d #t) (k 0)))) (h "0lrgl8m05n5kzpzvpyy1a4yxs07693rkkgxdlfnqzha32g28ma27")))

