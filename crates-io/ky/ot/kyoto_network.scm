(define-module (crates-io ky ot kyoto_network) #:use-module (crates-io))

(define-public crate-kyoto_network-0.1.0 (c (n "kyoto_network") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "kyoto_data") (r "^0.1.0") (d #t) (k 0)) (d (n "kyoto_machine") (r "^0.1.0") (d #t) (k 0)) (d (n "kyoto_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (d #t) (k 0)))) (h "0ns9m1jfzplv2ky9nr2px5552l06bafklb5sr9zxa16izs1f4cwg")))

