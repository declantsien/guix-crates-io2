(define-module (crates-io ky ot kyoto_data) #:use-module (crates-io))

(define-public crate-kyoto_data-0.1.0 (c (n "kyoto_data") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "kyoto_protocol") (r "^0.1.0") (d #t) (k 0)))) (h "06p04pd5i860apra7z8j0nm2rwvdzn4gz71g0rxsa97sxj40plv0")))

