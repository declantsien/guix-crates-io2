(define-module (crates-io ky j_ kyj_rpn_calc) #:use-module (crates-io))

(define-public crate-kyj_rpn_calc-0.1.1 (c (n "kyj_rpn_calc") (v "0.1.1") (h "0a1a0lxm4zazic2d49sddk09x1zdl8jykxmyl0y04ckcmcd7vhx7")))

(define-public crate-kyj_rpn_calc-0.1.2 (c (n "kyj_rpn_calc") (v "0.1.2") (h "1fdq3ynbqq4mhjhdy6qn9by3bllz46wsfph4ihnpdpr1x288qnhc")))

