(define-module (crates-io ky un kyun) #:use-module (crates-io))

(define-public crate-kyun-0.2.0 (c (n "kyun") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0j44f7f60m3jbg69kcnim5ka1in8d1caam8n2bdrzanldsjd7nyh")))

(define-public crate-kyun-0.2.1 (c (n "kyun") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1z9sxsnxbdvp6d7lj6nk05pkls7869jkn09fm86fxyyr0w1myk6r")))

