(define-module (crates-io ky -s ky-sync) #:use-module (crates-io))

(define-public crate-ky-sync-0.1.0 (c (n "ky-sync") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (f (quote ("colors"))) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a168hg5rk8zx8zq99rm5mgbwbs0wmad0g22lynk05iskbb14ddw")))

