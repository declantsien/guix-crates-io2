(define-module (crates-io ky tz kytz) #:use-module (crates-io))

(define-public crate-kytz-0.1.0 (c (n "kytz") (v "0.1.0") (d (list (d (n "argon2") (r "^0.5.2") (d #t) (k 0)) (d (n "bessie") (r "^0.0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "z32") (r "^1.0.2") (d #t) (k 0)) (d (n "zeroize") (r "^1.7.0") (d #t) (k 0)))) (h "09qj9p828ysk7sxmcq5dgaix90x5y2v6lgzn6rcyhdrj6g674dsk")))

