(define-module (crates-io ky te kyte) #:use-module (crates-io))

(define-public crate-kyte-0.1.0 (c (n "kyte") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "04fg5c4fvbg17z3mm3l7p22sd7dn9503ai1mxsbnx7nipnd7xsfs")))

