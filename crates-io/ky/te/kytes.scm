(define-module (crates-io ky te kytes) #:use-module (crates-io))

(define-public crate-kytes-0.1.0 (c (n "kytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "z32") (r "^1.0.2") (d #t) (k 0)))) (h "1chpngh46hgl841wk5br7psnxkzysnigj7myn8zp1j0q87i2ykgs")))

