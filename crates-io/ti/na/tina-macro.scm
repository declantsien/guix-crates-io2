(define-module (crates-io ti na tina-macro) #:use-module (crates-io))

(define-public crate-tina-macro-0.0.1 (c (n "tina-macro") (v "0.0.1") (h "0jkn4dmsqxnwbdpf0lnha3lwl8jysiw9faghfpn7sy538dkpicwp") (f (quote (("dyn-async-trait-send"))))))

(define-public crate-tina-macro-0.0.2 (c (n "tina-macro") (v "0.0.2") (h "1gymhglvfydi44jc7yfwwcksbvx7l0i2v047bg060562nlzi2dbk") (f (quote (("dyn-async-trait-send"))))))

