(define-module (crates-io ti na tina-build) #:use-module (crates-io))

(define-public crate-tina-build-0.3.15 (c (n "tina-build") (v "0.3.15") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "prost-build") (r "^0.12.2") (d #t) (k 0)) (d (n "protobuf") (r "^3.3.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 0)))) (h "0j4kj3vs1vzqczp6773l4m9n0jljc17m9pxvqiix4wq2bkb4hm95")))

