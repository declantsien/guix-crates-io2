(define-module (crates-io ti ta titanium-common) #:use-module (crates-io))

(define-public crate-titanium-common-0.1.0 (c (n "titanium-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.7") (d #t) (k 0)))) (h "0l4x5cl41g804w6j99q2g012s32gsd1crmlqv73yxxhvg122c415")))

