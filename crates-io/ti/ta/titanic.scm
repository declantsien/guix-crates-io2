(define-module (crates-io ti ta titanic) #:use-module (crates-io))

(define-public crate-titanic-0.1.0 (c (n "titanic") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1mf6xa6nkpwf59ghjl611qsl5b8x2i12qz013z38nf65pca8xxyz")))

