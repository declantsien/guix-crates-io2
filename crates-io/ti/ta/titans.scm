(define-module (crates-io ti ta titans) #:use-module (crates-io))

(define-public crate-titans-0.2.0 (c (n "titans") (v "0.2.0") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w1rwpjfnjaqc7pj7idvs8kadicmq8kqysjw8y2135r9p58ia3l2")))

(define-public crate-titans-0.2.1 (c (n "titans") (v "0.2.1") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ciaxsgjgyhrxwdj8jlmz5h2z11xc7g9cfby7prqm217ngc1hmkq")))

(define-public crate-titans-0.2.2 (c (n "titans") (v "0.2.2") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zrn8jldw6fgaqh69f819sqj4pcfzy3wby598n9j3prdg3bq9ns0") (y #t)))

(define-public crate-titans-0.2.3 (c (n "titans") (v "0.2.3") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cr9awzgqgrqh9czxqkkavzvgsflx3bp6p9sa5cc0m6r5rgl470s") (y #t)))

(define-public crate-titans-0.2.4 (c (n "titans") (v "0.2.4") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1y1z9id62b7gyf03n976jsfhzih9pzaklsgrn8fsljw79bradqps") (y #t)))

(define-public crate-titans-0.2.5 (c (n "titans") (v "0.2.5") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xsyii4gvhdcxll3fyw391cg52qaz7sj2fkdqhsg78jf6rndgizc") (y #t)))

(define-public crate-titans-0.2.6 (c (n "titans") (v "0.2.6") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2gcp8i25qdm7isw2n380lxaaj828cj4ivrmm1zb044d7wxixzj")))

(define-public crate-titans-0.2.7 (c (n "titans") (v "0.2.7") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fdsh6g434x6v02864d5g3qzvjnbjnvxvfplkv7zcbz9b7ir22y5")))

(define-public crate-titans-0.2.8 (c (n "titans") (v "0.2.8") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1iw018xd6y6ycb8rbska7zbc8mdgwldsh79kwzsy57h3z07fsv50")))

(define-public crate-titans-0.2.9 (c (n "titans") (v "0.2.9") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1picgcpgjarvyxyq3ly1i49kg7fzjqsaqi90rvrrmcz243l7wdyi")))

(define-public crate-titans-0.3.0 (c (n "titans") (v "0.3.0") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "01kfp4biq3125pg0czr1ha09j9lwhb9hjigr8z40nsvqkbmmvq26")))

(define-public crate-titans-0.3.1 (c (n "titans") (v "0.3.1") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0jaq0mafahn9215xcd5cwp1jvwk9xqkhin1608769wf99i5lyqmw")))

(define-public crate-titans-0.3.2 (c (n "titans") (v "0.3.2") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "064mqv4h0xlwvcp604nramrwhqqkgnf7ilgcjadmf89aky4hkxpy")))

(define-public crate-titans-0.3.3 (c (n "titans") (v "0.3.3") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1ilr9vvwrfgg1vvxrlq5dvpca5swgsbmmy4mzvlsrqjrdkg2q734")))

(define-public crate-titans-0.3.4 (c (n "titans") (v "0.3.4") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "137crfn227l970dp7mgfip4p99zfy6rybgk6cv5mab1145pchw10")))

(define-public crate-titans-0.3.5 (c (n "titans") (v "0.3.5") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "099k3g46f26jinfvcc6z6j6m1568br7fp2hhva1li23fj73hm13f")))

(define-public crate-titans-0.3.6 (c (n "titans") (v "0.3.6") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "14gab3b91aib4vvh91bmwsywm31haywkscfwb93bj6iazgj7ipbf")))

(define-public crate-titans-0.3.7 (c (n "titans") (v "0.3.7") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1vwinwqc9y7ma45ymr9vlklqklkrfpm7pfijsjaadc60zr58c16y")))

(define-public crate-titans-0.3.8 (c (n "titans") (v "0.3.8") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0vnzxl3cszrqx1j4r3gy5z0k8bsid2zbzwyncfanf3jbf637nx97")))

(define-public crate-titans-0.3.9 (c (n "titans") (v "0.3.9") (d (list (d (n "isahc") (r "^1.7.2") (f (quote ("text-decoding" "json"))) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "11wvm3cbpphp9a0ray54nzlvxvgrba50bgwqxzzyhbvh5sq1q787")))

(define-public crate-titans-0.4.0 (c (n "titans") (v "0.4.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (f (quote ("text-decoding" "json"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1ac3gipc233px90160zc187jbs6rxxmlmpb59by14s5mqp8r6c9j")))

(define-public crate-titans-0.4.1 (c (n "titans") (v "0.4.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (f (quote ("text-decoding" "json"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0b7dm25ibydw2p059lpj5k7jzi22a1ansqzd9m2z5pn5yyjvchqj")))

(define-public crate-titans-0.4.2 (c (n "titans") (v "0.4.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (f (quote ("text-decoding" "json"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "01rd7hcg6s060kr3sjc8sdm1prfhcl61prvw729xid4334yn7c9v")))

