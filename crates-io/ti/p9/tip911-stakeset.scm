(define-module (crates-io ti p9 tip911-stakeset) #:use-module (crates-io))

(define-public crate-tip911-stakeset-0.0.2 (c (n "tip911-stakeset") (v "0.0.2") (d (list (d (n "imbl") (r "^2.0.0") (d #t) (k 0)) (d (n "melstructs") (r "^0.3.1") (d #t) (k 0)) (d (n "novasmt") (r "^0.2.19") (d #t) (k 0)) (d (n "stdcode") (r "^0.1.13") (d #t) (k 0)) (d (n "tmelcrypt") (r "^0.2.6") (d #t) (k 0)))) (h "10ckiqqsa4izn4dzxwqbk8g5zi5fmngil7pdcrzx5d79229xyawx")))

