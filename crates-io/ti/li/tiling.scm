(define-module (crates-io ti li tiling) #:use-module (crates-io))

(define-public crate-tiling-0.1.0 (c (n "tiling") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14.0") (f (quote ("png"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "png") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fg6bgm3z8ivvyjk47n3llll7qswg8c1msrz7zjl6bvvp5ld70rg")))

