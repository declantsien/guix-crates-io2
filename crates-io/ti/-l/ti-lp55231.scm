(define-module (crates-io ti -l ti-lp55231) #:use-module (crates-io))

(define-public crate-ti-lp55231-1.0.0 (c (n "ti-lp55231") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)))) (h "047crn91rvf69w2grzy739pyfppq2gzx4acy0l1n49w5nlqchvk7")))

