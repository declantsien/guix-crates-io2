(define-module (crates-io ti c8 tic80) #:use-module (crates-io))

(define-public crate-tic80-0.1.0 (c (n "tic80") (v "0.1.0") (d (list (d (n "bilge") (r "^0.2.0") (d #t) (k 0)) (d (n "buddy-alloc") (r "^0.4.1") (d #t) (t "cfg(target_family=\"wasm\")") (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cstr") (r "^0.2.11") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.12") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "tic80-sys") (r "^0.1.0") (d #t) (k 0) (p "tic80-sys")))) (h "1mn0firgv0rgyjszv9ib0alj7749avz30fhn845lcf3l5pjlvsrv") (f (quote (("default"))))))

