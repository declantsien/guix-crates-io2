(define-module (crates-io ti ed tiedcrossing-byte) #:use-module (crates-io))

(define-public crate-tiedcrossing-byte-0.1.0 (c (n "tiedcrossing-byte") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("std"))) (o #t) (k 0)))) (h "16sh5ncqrw9fpw1drvlc8cfvdg98lr9q4crq6dir9sjfybhyn258")))

