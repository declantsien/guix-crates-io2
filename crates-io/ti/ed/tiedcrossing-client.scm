(define-module (crates-io ti ed tiedcrossing-client) #:use-module (crates-io))

(define-public crate-tiedcrossing-client-0.1.0 (c (n "tiedcrossing-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "drawbridge-jose") (r "^0.1.0") (d #t) (k 0) (p "tiedcrossing-jose")) (d (n "drawbridge-type") (r "^0.1.0") (d #t) (k 0) (p "tiedcrossing-type")) (d (n "http") (r "^0.2.6") (k 0)) (d (n "mime") (r "^0.3.16") (k 0)) (d (n "rustls") (r "^0.20.6") (k 0)) (d (n "serde_json") (r "^1.0.79") (f (quote ("std"))) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "tls"))) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)) (d (n "webpki-roots") (r "^0.22.3") (k 0)))) (h "16snzi9da85c6wqxi5sl96yldiccy431m0fznn1vjnliij4ssfzc")))

