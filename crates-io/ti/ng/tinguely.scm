(define-module (crates-io ti ng tinguely) #:use-module (crates-io))

(define-public crate-tinguely-0.0.1 (c (n "tinguely") (v "0.0.1") (d (list (d (n "mathru") (r "^0.2.1") (f (quote ("native"))) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "067kib73wdka2mpcrl7j4nw6vzd5q81654nqr51svv02d2qadb7x")))

(define-public crate-tinguely-0.1.0 (c (n "tinguely") (v "0.1.0") (d (list (d (n "mathru") (r "^0.2.1") (f (quote ("native"))) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1fc3mcljf8bv7psvwssn7jkl6f8c446iwz16gdk8jmgswysngixk")))

(define-public crate-tinguely-0.1.1 (c (n "tinguely") (v "0.1.1") (d (list (d (n "mathru") (r "^0.7.3") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0j6vd9gk3apdy44xjg3xmx08jy2izfmpzjd30n50wb79ddmncigj") (f (quote (("openblas" "mathru/openblas") ("netlib" "mathru/netlib") ("native" "mathru/native") ("intel-mkl" "mathru/intel-mkl") ("default" "native") ("blaslapack") ("accelerate" "mathru/accelerate"))))))

