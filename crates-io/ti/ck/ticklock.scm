(define-module (crates-io ti ck ticklock) #:use-module (crates-io))

(define-public crate-ticklock-0.0.1 (c (n "ticklock") (v "0.0.1") (h "1i228dh9y3jh6yn4mq44hshz8iq0bl9g3jqy5f5hk0qhmb9x750x")))

(define-public crate-ticklock-0.0.2 (c (n "ticklock") (v "0.0.2") (h "05vm5dy9km2angjcdpq9f5ysnnlkkmkj8dsc6ja7viyc92ai0fdj")))

(define-public crate-ticklock-0.0.3 (c (n "ticklock") (v "0.0.3") (h "1n4rhnlk6dw0z6b4pxzwk3qpaf5cp4ndj2fgis0zvh115wj7fc0h")))

(define-public crate-ticklock-0.0.4 (c (n "ticklock") (v "0.0.4") (h "1ala1p7gdn9p7ind8969583d3w3a1yg2k87bvwd0g57xdbx2xm6l")))

(define-public crate-ticklock-0.0.5 (c (n "ticklock") (v "0.0.5") (d (list (d (n "cast") (r "^0.2.2") (k 0)))) (h "1pqzh479yi0kk9m3njgjck6mywwazll2adl9afz05bnbb7jb3nvf")))

(define-public crate-ticklock-0.0.6 (c (n "ticklock") (v "0.0.6") (d (list (d (n "cast") (r "^0.2.2") (k 0)))) (h "1yw00h2w0vl05iqwck7jgiy0pr4nsgiiydk5yc8yd0xg9jpwhqrb")))

(define-public crate-ticklock-0.0.7 (c (n "ticklock") (v "0.0.7") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0dgl45005ssccpkrjblcpzlpy25wjklzg4b57vbbnxcd2f1fi6hc")))

(define-public crate-ticklock-0.0.8 (c (n "ticklock") (v "0.0.8") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1d9j6w8h2bnlpx6gsnslvql9iqa36cvni6lgp5mjnr39km3gy278")))

