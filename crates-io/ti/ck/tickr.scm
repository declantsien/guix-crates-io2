(define-module (crates-io ti ck tickr) #:use-module (crates-io))

(define-public crate-tickr-0.1.0 (c (n "tickr") (v "0.1.0") (h "0r10xk1g3p246r20k6hfyjida54diyn3g8wnq8kxrbvvbqd5snp6")))

(define-public crate-tickr-0.1.1 (c (n "tickr") (v "0.1.1") (h "056c338m52xagnbayrab3xmyjcd1jr0zyafqipi5rwjw0pqz3rbr")))

(define-public crate-tickr-0.1.2 (c (n "tickr") (v "0.1.2") (h "10gmc9cxffyp1c7xbidjfqxg3wn5srji6qc3b170q543dk11brfj")))

(define-public crate-tickr-0.2.0 (c (n "tickr") (v "0.2.0") (h "0hkmdk3zl92l00gdagpi82p06h0xrdcqi3wcgjina0v6zp3nfgpp")))

