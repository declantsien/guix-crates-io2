(define-module (crates-io ti ck tick) #:use-module (crates-io))

(define-public crate-tick-0.0.0 (c (n "tick") (v "0.0.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "eventual") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.4") (d #t) (k 0)))) (h "198pz7q306y3ri1g4hakb0wxdnf8cnvk07ayg7rb592l0r28yz7q")))

(define-public crate-tick-0.0.1 (c (n "tick") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "slab") (r "^0.1") (d #t) (k 0)))) (h "03jzjspv0ym5ryfkxhnbp7899cqv2n5gj0j7kli3j1kda3k9g785")))

