(define-module (crates-io ti ck ticket2ride) #:use-module (crates-io))

(define-public crate-ticket2ride-0.2.1 (c (n "ticket2ride") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1r2pv1pda0fqyq5mghq8fwx1hiksr1i0j7bg577v5f7dm8z33f4r") (y #t)))

(define-public crate-ticket2ride-0.2.2 (c (n "ticket2ride") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1dh9wdnizc67ip1j0qpaybl10gpmb9dq9jyifj4i52iq9nlbp7bc") (y #t)))

(define-public crate-ticket2ride-0.2.3 (c (n "ticket2ride") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "10d8ywlqwz9nrsvk8c949ppxswy9w5lwqk5vwcmxnzpcdx7r6z1b") (y #t)))

(define-public crate-ticket2ride-0.2.4 (c (n "ticket2ride") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "060sljxgpq3l1l7j4298qnlhcwy6p3b5wvs19yp2xp7ddjhhn7ik") (y #t)))

(define-public crate-ticket2ride-0.2.5 (c (n "ticket2ride") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "11y86z3is6ywwai22q25z3piv9f78mxhz5va31pj5jy322c2xsnz")))

(define-public crate-ticket2ride-0.2.6 (c (n "ticket2ride") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1pf3k4h2xdfjmg2q90rkrd0qzrzpz4d9wxm194fi2fbfcwyynfk9")))

(define-public crate-ticket2ride-0.2.7 (c (n "ticket2ride") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.19.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0hf9h5c4r26xfrm307xfcyizy0bkkvcxfrv1lk4bpl18l1a6hzv2")))

(define-public crate-ticket2ride-0.2.8 (c (n "ticket2ride") (v "0.2.8") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.19.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1zm931azc7ng5p4f7zkyvnvjnsx9s6gyfa6nx2ayxhzjffv52vmb")))

(define-public crate-ticket2ride-0.3.0 (c (n "ticket2ride") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.19.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1h0cch692fj0y4j8i75qh66zr8ipsfv0614hjxwzv0d9msklw9jy")))

(define-public crate-ticket2ride-0.3.1 (c (n "ticket2ride") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.19.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "15wyck7gqhd4qgy5bb8yff9j668xxc5vwx9p6biszwxvjbr14acg")))

