(define-module (crates-io ti ck ticky) #:use-module (crates-io))

(define-public crate-ticky-0.1.0 (c (n "ticky") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)))) (h "1sf8sh966k620knpl91xd4y1047cy20ggninm7airbz29fg2c88l") (y #t) (s 2) (e (quote (("derive_more" "dep:derive_more"))))))

(define-public crate-ticky-1.0.0 (c (n "ticky") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "hifitime") (r "^3.8.0") (o #t) (k 0)))) (h "0lsyhmj9sncil92f7dmxd5k659k36g70apk34b23p77q3541xzyw") (f (quote (("stdtime" "std") ("default" "std" "stdtime")))) (y #t) (s 2) (e (quote (("std" "hifitime?/std") ("hifitime" "dep:hifitime") ("derive_more" "dep:derive_more"))))))

(define-public crate-ticky-1.0.1 (c (n "ticky") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "hifitime") (r "^3.8.0") (o #t) (k 0)))) (h "083ak4k7vl4gmhc6g367z1q6qi7n4j20xh1kfzjpgiv95m0bkhv9") (f (quote (("stdtime" "std") ("default" "std" "stdtime")))) (s 2) (e (quote (("std" "hifitime?/std") ("hifitime" "dep:hifitime") ("derive_more" "dep:derive_more"))))))

(define-public crate-ticky-1.0.2 (c (n "ticky") (v "1.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "hifitime") (r "^3.8.0") (o #t) (k 0)))) (h "0jayya7rsp4nf651jprda0853szclf3p43yaxzam9541alcyqn0w") (f (quote (("stdtime" "std") ("default" "std" "stdtime")))) (s 2) (e (quote (("std" "hifitime?/std") ("hifitime" "dep:hifitime") ("derive_more" "dep:derive_more"))))))

