(define-module (crates-io ti ck ticktime) #:use-module (crates-io))

(define-public crate-ticktime-0.0.1 (c (n "ticktime") (v "0.0.1") (h "0zmhpmg91lbki24s4qsdkkybpbcvkygi3aahrj011dxxls9f87ky")))

(define-public crate-ticktime-0.0.2 (c (n "ticktime") (v "0.0.2") (h "14fl95lyga5dqrwnrmhaqy4f5693vbli8ckpr1mmh62mfmbg7fzm")))

(define-public crate-ticktime-0.1.0 (c (n "ticktime") (v "0.1.0") (h "1if4nmjjmlr7mzfdlpk75gckzqg314m9xgd88bfb2hjas45kry8d")))

