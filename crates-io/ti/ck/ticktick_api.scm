(define-module (crates-io ti ck ticktick_api) #:use-module (crates-io))

(define-public crate-ticktick_api-0.1.0 (c (n "ticktick_api") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b2cwyxj7p51jwid4sncply6vs5sn85w0plg6wlcfnw8b6yvzxj7")))

(define-public crate-ticktick_api-0.1.1-alpha (c (n "ticktick_api") (v "0.1.1-alpha") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09381n847ysasny8sbh1hzr2n13j8pg88dsk3p4i19dzaa57l0v4")))

