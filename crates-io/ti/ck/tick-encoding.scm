(define-module (crates-io ti ck tick-encoding) #:use-module (crates-io))

(define-public crate-tick-encoding-0.1.0 (c (n "tick-encoding") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "divan") (r "^0.1.11") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "1avcsikb2x27dhrwlay4kak7hsgl25v35kbkjd7mdz71ymv795p7") (f (quote (("safe") ("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "dep:thiserror")))) (r "1.70.0")))

(define-public crate-tick-encoding-0.1.1 (c (n "tick-encoding") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "divan") (r "^0.1.11") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "0b96j6175z1jya5rgn3h4faniq0f5ayblh953bfvcmyzgfl4g2kw") (f (quote (("safe") ("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "dep:thiserror")))) (r "1.70.0")))

(define-public crate-tick-encoding-0.1.2 (c (n "tick-encoding") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "divan") (r "^0.1.11") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (o #t) (d #t) (k 0)))) (h "1kzgz7jqix180pj3m89pwx6dyn7il0xa525z9vqcvmnnfywrn5gh") (f (quote (("safe") ("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "dep:thiserror")))) (r "1.70.0")))

