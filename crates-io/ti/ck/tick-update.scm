(define-module (crates-io ti ck tick-update) #:use-module (crates-io))

(define-public crate-tick-update-0.1.0 (c (n "tick-update") (v "0.1.0") (h "10c13vff619v1xb8qp30s6bwj0f2k01y105pir9bnmqy4spxpaim")))

(define-public crate-tick-update-0.1.1 (c (n "tick-update") (v "0.1.1") (h "0scsrkj0b06iga46vh6643a382bjccv7p2a1ws1zyzixjb4w0mi8")))

