(define-module (crates-io ti ck tick_clock) #:use-module (crates-io))

(define-public crate-tick_clock-0.1.0 (c (n "tick_clock") (v "0.1.0") (d (list (d (n "clock_source") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)))) (h "15ipvqb8gpmmdr34yh2jadjhb7y6khy0zdjcy8sn2karjwkkkpxw")))

(define-public crate-tick_clock-0.1.1 (c (n "tick_clock") (v "0.1.1") (d (list (d (n "clock_source") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)))) (h "06g5xshmfj8lxl65bwwyv8lxyvkschrna386aprfdizyy79qi1mq")))

(define-public crate-tick_clock-0.1.2 (c (n "tick_clock") (v "0.1.2") (d (list (d (n "clock_source") (r "^0.2") (f (quote ("custom"))) (d #t) (k 0)))) (h "147d5szrlhn8x1ss5gqzsf2g14mwxsqpw6kag3cwwyavbahjw9v9")))

