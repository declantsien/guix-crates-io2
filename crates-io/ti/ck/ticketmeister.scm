(define-module (crates-io ti ck ticketmeister) #:use-module (crates-io))

(define-public crate-ticketmeister-0.1.0 (c (n "ticketmeister") (v "0.1.0") (d (list (d (n "api-request-utils-rs") (r "^0.2.0") (d #t) (k 0)))) (h "12cax5mg1islbbfdlyzns6razbink7c3dbd2hdmcllmg57i58w46") (f (quote (("inventory") ("discovery"))))))

(define-public crate-ticketmeister-0.1.1 (c (n "ticketmeister") (v "0.1.1") (d (list (d (n "api-request-utils-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "05dxaim37fihb2gpbxzrfi95pc06lpcvs7lvwkxp1xzrjkqv8l1f") (f (quote (("inventory") ("default")))) (s 2) (e (quote (("discovery" "dep:lazy_static"))))))

(define-public crate-ticketmeister-0.1.2 (c (n "ticketmeister") (v "0.1.2") (d (list (d (n "api-request-utils-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0a9972zk3ny3z5szgr6c7r3mx29y23j4blp0hmwpb5458ngmips3") (f (quote (("inventory") ("default")))) (s 2) (e (quote (("discovery" "dep:lazy_static"))))))

