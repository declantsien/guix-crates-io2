(define-module (crates-io ti ck ticket) #:use-module (crates-io))

(define-public crate-ticket-0.1.0 (c (n "ticket") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "machine-uid") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1k9a876nqyyfdapns479j8yg1wfm7di0dxfdqyxl36zvar6l45mm")))

(define-public crate-ticket-0.2.0 (c (n "ticket") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "machine-uid") (r "^0.3.0") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)))) (h "0rnw46msqysvqsf0bcz6gkl2m39r24k713hh3z4zfjw51hk6l4w1")))

