(define-module (crates-io ti ck ticked) #:use-module (crates-io))

(define-public crate-ticked-0.1.0 (c (n "ticked") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.14.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "14ffw05b2mhm6rj7fas3sbvych8d5vyymyspak4gfvqw0k2irgf4")))

(define-public crate-ticked-0.1.1 (c (n "ticked") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.14.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "05pxmgmzryafivaq6sbh99gdhcfqsb6k4zg0bh9z923xvv5bxbr3")))

