(define-module (crates-io ti ck tick_counter) #:use-module (crates-io))

(define-public crate-tick_counter-0.2.0 (c (n "tick_counter") (v "0.2.0") (h "15lfjfjny68z8l664is5ii8asy5wcscdsar7v7pycj6iydcgl73b")))

(define-public crate-tick_counter-0.2.1 (c (n "tick_counter") (v "0.2.1") (h "0ahk7908q04lvnqpgpqdcw793660sfvay026zjih58n2kjg8bnal")))

(define-public crate-tick_counter-0.3.0 (c (n "tick_counter") (v "0.3.0") (h "0wlwgg4aqbligi894h1zs13yysz5nxqkzqwji82dyvjyw8hdvrng")))

(define-public crate-tick_counter-0.3.1 (c (n "tick_counter") (v "0.3.1") (h "1cf5hng59q4ayfdyzm3zzwq9r09wyps5255qimgyakq1bvkz2fza")))

(define-public crate-tick_counter-0.3.2 (c (n "tick_counter") (v "0.3.2") (h "0grc0ilfkp78m6p6rbk0hxnk054gn586b8x6lxpa2faby1saz527")))

(define-public crate-tick_counter-0.3.3 (c (n "tick_counter") (v "0.3.3") (h "0bjz89s3jfjiz9imgm9cq1w1waab4g5xa122c46nh7vn3yksrrip")))

(define-public crate-tick_counter-0.3.4 (c (n "tick_counter") (v "0.3.4") (h "0gisx2314niscp30dn7ymaw8mdn4vk70c7q51y8blcl5lrrn3wgw")))

(define-public crate-tick_counter-0.4.0 (c (n "tick_counter") (v "0.4.0") (h "0kq5v2762sphhvrc7k3cjfankzfljy5kdpxzpr6ld0mcnsgqzn25")))

(define-public crate-tick_counter-0.4.1 (c (n "tick_counter") (v "0.4.1") (h "1ww8xlsl6xiy820wnq4mhav9hqp66l6i8n1irgwpii500lvywyjv")))

(define-public crate-tick_counter-0.4.2 (c (n "tick_counter") (v "0.4.2") (h "0vw8mfnv9883jkqc3jl614xqcmghwq58d63vidhvh1d3zygy497c")))

(define-public crate-tick_counter-0.4.3 (c (n "tick_counter") (v "0.4.3") (h "15snxpbsp57g9px1i6gm7bfa12qihwz86rql6gs27qcwx0sxw0yv")))

(define-public crate-tick_counter-0.4.4 (c (n "tick_counter") (v "0.4.4") (h "1f07r9i6gvg2pi2mravmqmavv05ax9pz904w2c35zlfy2rqpv2zz")))

(define-public crate-tick_counter-0.4.5 (c (n "tick_counter") (v "0.4.5") (h "1gdsdcivrgw21srcnxm0vcaacq0w2s0b9cnb34099anhhq4k3w9p")))

