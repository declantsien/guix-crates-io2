(define-module (crates-io ti c_ tic_tac_toe_rust) #:use-module (crates-io))

(define-public crate-tic_tac_toe_rust-0.1.0 (c (n "tic_tac_toe_rust") (v "0.1.0") (h "1lz1lxw2bn87j29xmj2mgqhmgngbgn8d86far0ap7kg763jsgm0f")))

(define-public crate-tic_tac_toe_rust-0.2.1 (c (n "tic_tac_toe_rust") (v "0.2.1") (h "0mvifglyjq8na3vs3cpkxkjd5a6lfqycqi7f755yyalkrz0wkfbg")))

(define-public crate-tic_tac_toe_rust-0.3.0 (c (n "tic_tac_toe_rust") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cc7p7vhancbkn2xg2p00wki1rqfz14sbq0m83545l5gwnszw8va")))

(define-public crate-tic_tac_toe_rust-0.3.1 (c (n "tic_tac_toe_rust") (v "0.3.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "134819967fg59cvbzppg6fl9fxgvr2k4rdd0qknh0qs5fp8fy2ay")))

(define-public crate-tic_tac_toe_rust-0.4.0 (c (n "tic_tac_toe_rust") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "191clwwsdf9njymyy7pa7zdaggy86agcfbj54f7sk3cjy8fbfcng")))

