(define-module (crates-io ti c_ tic_tac_toe) #:use-module (crates-io))

(define-public crate-tic_tac_toe-0.1.2 (c (n "tic_tac_toe") (v "0.1.2") (d (list (d (n "clap") (r "^2.22.1") (d #t) (k 0)) (d (n "conrod") (r "^0.49") (f (quote ("piston" "piston2d-graphics" "glium"))) (d #t) (k 0)) (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "074i404h3im0vdyws29blrizvzy4zjf6mcw7qqw9pyvb3xrsyzh8")))

(define-public crate-tic_tac_toe-0.1.3 (c (n "tic_tac_toe") (v "0.1.3") (d (list (d (n "clap") (r "^2.22.1") (d #t) (k 0)) (d (n "conrod") (r "^0.49") (f (quote ("piston" "piston2d-graphics" "glium"))) (d #t) (k 0)) (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "14wvq6y3ybs62z4dj7cl32qs9ynqihj68xaj4k2c1xc6yfb9anx5")))

