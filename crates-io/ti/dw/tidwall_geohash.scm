(define-module (crates-io ti dw tidwall_geohash) #:use-module (crates-io))

(define-public crate-tidwall_geohash-0.9.0 (c (n "tidwall_geohash") (v "0.9.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1d11papjn7l1f6gn0981n2fif09976vxygmwk06va0pifd55bdg5") (y #t)))

(define-public crate-tidwall_geohash-0.9.1 (c (n "tidwall_geohash") (v "0.9.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1p8wdadhdq4xg0xbzsi4a4zk0nqs7z3ydcx0cgca1xl3k2l0i8nv")))

