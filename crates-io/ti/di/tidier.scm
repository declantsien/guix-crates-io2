(define-module (crates-io ti di tidier) #:use-module (crates-io))

(define-public crate-tidier-0.1.0 (c (n "tidier") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1v0g9yj8pyn46ssg24jfmlwcjknbvy5k74xgs3qcpg7s5s1b2pg5")))

(define-public crate-tidier-0.1.1 (c (n "tidier") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1dfjnzyp02s3grk7i0yw4n0ys48af6ac5cbninpi2cxbkcdqmxn3")))

(define-public crate-tidier-0.2.0 (c (n "tidier") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "18hlczmajv52k3kwg1pklmnmkaj2hvxa70f92g0izp45h6ahkv9d")))

(define-public crate-tidier-0.2.1 (c (n "tidier") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1gxb251fmiig857jqpdmn46p880iwdzjnl2v8w81s79if9wi2cbn")))

(define-public crate-tidier-0.2.2 (c (n "tidier") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1lk9a454qrk9yw89gpcw36hv4ymd99iqfzikwhf71q7bjnp1x0j4")))

(define-public crate-tidier-0.3.0 (c (n "tidier") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0nxg6da4yvm5yr5szii7675hwzcn488xm45z9jx12ldgm6xpnm2v")))

(define-public crate-tidier-0.3.1 (c (n "tidier") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0jqwd8ka1mg2zxwcamhcans6bk4w0m3i4l24z1szz5k2l773g1cb")))

(define-public crate-tidier-0.3.2 (c (n "tidier") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1rb6m01pzrn8d8j0j9yyql9gwlxwn8krb6q5bxrqk8v8n9bn6lyf")))

(define-public crate-tidier-0.3.3 (c (n "tidier") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0kwk4r6kgyz06rjy95ch1ibc1armc645d63hk680sfdc0phdpjk6")))

(define-public crate-tidier-0.4.0 (c (n "tidier") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1d53rf6myl6l7zx6z679m3fbhspzmw7lwsa5ldkj4pa5fgv2j459")))

(define-public crate-tidier-0.4.2 (c (n "tidier") (v "0.4.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0niyd63wp5jkwnm73d4f7h4i8kxllcxn8aa9cgls7nj3zgxmfysn")))

(define-public crate-tidier-0.5.0 (c (n "tidier") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0cps6xnbrginj1f00ps0665kr1pp6b44mb5kdvxrlj23m89yn4yi")))

(define-public crate-tidier-0.5.1 (c (n "tidier") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.8.1") (d #t) (k 0)))) (h "169ychdvpg0vr3qz6wmhiqzwmydaivk9h5za6xygjli6pnblb2my")))

(define-public crate-tidier-0.5.2 (c (n "tidier") (v "0.5.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "errno") (r "^0.3.8") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "tidy-sys") (r "^0.8.1") (d #t) (k 0)))) (h "15db8m7b5ajk467574a21j2jzaqcw4kz0xgqx7b3c6w7iwqp2pnz")))

