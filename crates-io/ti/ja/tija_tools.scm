(define-module (crates-io ti ja tija_tools) #:use-module (crates-io))

(define-public crate-tija_tools-0.1.0 (c (n "tija_tools") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v9jc5sngd54xgbx7xmml4sw2had3i8csf34fp1c8s8mlis1b03d") (y #t)))

(define-public crate-tija_tools-0.1.1 (c (n "tija_tools") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pl427w0y7lm8lx2wfnkml89i8hjnsd1rwbdqjb353bal90rvzri") (y #t)))

(define-public crate-tija_tools-0.1.2 (c (n "tija_tools") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lvsx55srl5j4ny7m0xi3fk8b4dyh2qcdvrhrzk1kpd5dkvp4g53") (y #t)))

(define-public crate-tija_tools-0.1.3 (c (n "tija_tools") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07f8k6kkqvp4i8y4g16ds4ga5ixbyis8ifirgs1al3dhhp57jnv4") (y #t)))

(define-public crate-tija_tools-0.1.4 (c (n "tija_tools") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11ax1q869qcckr4pmwxzcz317qsk3hgda6xmjsfh7n4f36vf6rfw")))

