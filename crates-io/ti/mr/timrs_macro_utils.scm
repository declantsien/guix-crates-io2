(define-module (crates-io ti mr timrs_macro_utils) #:use-module (crates-io))

(define-public crate-timrs_macro_utils-1.1.0-rc.3 (c (n "timrs_macro_utils") (v "1.1.0-rc.3") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0vid9fmhi4sh9nmfr2vvx6g8g0q1j8gz2wdlj5gal7kfayfrgd89")))

(define-public crate-timrs_macro_utils-1.1.0-rc.4 (c (n "timrs_macro_utils") (v "1.1.0-rc.4") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1nq28jwb07198bsiswfy2q719lw6kh82mszjhcpvmg0ri4dwcfxg")))

(define-public crate-timrs_macro_utils-1.1.0-rc.5 (c (n "timrs_macro_utils") (v "1.1.0-rc.5") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0s4dx3aipqn6ay0i262r4ws2452dcncbl4y91vjm45w4j6ihm7h2")))

(define-public crate-timrs_macro_utils-1.1.0-rc.6 (c (n "timrs_macro_utils") (v "1.1.0-rc.6") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "03if5pyjfy9mkl0rwina6gxs0g2v505rdw9mqfvf7mma0gbyfq04")))

