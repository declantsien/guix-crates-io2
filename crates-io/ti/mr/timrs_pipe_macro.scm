(define-module (crates-io ti mr timrs_pipe_macro) #:use-module (crates-io))

(define-public crate-timrs_pipe_macro-1.1.0-rc.5 (c (n "timrs_pipe_macro") (v "1.1.0-rc.5") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)) (d (n "timrs_macro_utils") (r "^1.1.0-rc.5") (d #t) (k 0)))) (h "1bq9521cxc76m7yhpdyalvbjh2aif149r5igbq27gvhf2grwrcx1")))

(define-public crate-timrs_pipe_macro-1.1.0-rc.6 (c (n "timrs_pipe_macro") (v "1.1.0-rc.6") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)) (d (n "timrs_macro_utils") (r "^1.1.0-rc.5") (d #t) (k 0)))) (h "1a3k0a0ic7jj41bl4ynpp61x1zfx4cpsj97anwz44i21lgjrraxz")))

