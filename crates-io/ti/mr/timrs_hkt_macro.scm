(define-module (crates-io ti mr timrs_hkt_macro) #:use-module (crates-io))

(define-public crate-timrs_hkt_macro-1.0.0-rc.1 (c (n "timrs_hkt_macro") (v "1.0.0-rc.1") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9xhzjqld6gx0lw9ldg3mxnh12vv0c2wh5pbivf6p79qp40q0ym")))

(define-public crate-timrs_hkt_macro-1.0.0-rc.2 (c (n "timrs_hkt_macro") (v "1.0.0-rc.2") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1z8phc121kv09xd5zamwkmfi8ky88q48vj39xy5993bs4wvsa11m")))

(define-public crate-timrs_hkt_macro-1.0.0-rc.3 (c (n "timrs_hkt_macro") (v "1.0.0-rc.3") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0j024yi1w7yzfj949wwa3hv1gyckcz804b133qdrc3wjcd6i17")))

(define-public crate-timrs_hkt_macro-1.0.0-rc.4 (c (n "timrs_hkt_macro") (v "1.0.0-rc.4") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6d5jqc962q7wv45cvjcy9kh0d48d8hwmc5v9l55mvl1pv44h22")))

(define-public crate-timrs_hkt_macro-1.0.0-rc.5 (c (n "timrs_hkt_macro") (v "1.0.0-rc.5") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0dpqkgj47xkknfvq9cfvb9gl1y2pva9bscmdn77m471dgh7gm9dj")))

(define-public crate-timrs_hkt_macro-1.0.0-rc.6 (c (n "timrs_hkt_macro") (v "1.0.0-rc.6") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1azch97b4f7nlxil41cmpxx2hl2s2vnmvd86vcggbp7mckwvv5yn")))

(define-public crate-timrs_hkt_macro-1.0.0 (c (n "timrs_hkt_macro") (v "1.0.0") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0dmyzbdrlwjbnqh64wshwk8k1vfs7i2pxgmszv9dqvjj05gikrqf")))

(define-public crate-timrs_hkt_macro-1.0.1-rc.1 (c (n "timrs_hkt_macro") (v "1.0.1-rc.1") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "08lfcw7sxs1jj1q6cm38ngaw0rh45m258yn8apzsfhajqf1l9a06")))

(define-public crate-timrs_hkt_macro-1.0.1 (c (n "timrs_hkt_macro") (v "1.0.1") (d (list (d (n "prettyplease") (r "^0.2.20") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0ygifg9qhnk4fm0hmr81m6fhrndg3myp5ff335h402s2z7rq3wwx")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.1 (c (n "timrs_hkt_macro") (v "1.1.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1nzyp0jy7ml2m1ry1qx8ax6lkxinjs0f1qai12jxndmdap8h6qis")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.2 (c (n "timrs_hkt_macro") (v "1.1.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0k68dg11s29czxxkd6qg4b5jqb632ml0sa8w31rizh7gz00p2818")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.3 (c (n "timrs_hkt_macro") (v "1.1.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "00kdnh0nh900lxg316k0xksqwh2zry0v0r28s2jx2f59qksnsjw3")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.4 (c (n "timrs_hkt_macro") (v "1.1.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1qdyqg2yj9qzv0qw7rgv39shg98m009fbabh4wlbivq8ssj314zc")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.5 (c (n "timrs_hkt_macro") (v "1.1.0-rc.5") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)) (d (n "timrs_macro_utils") (r "^1.1.0-rc.5") (d #t) (k 2)))) (h "1rwwh7b65npsjhgpawnxc2b07wf1hmr7sin3blxh7c72w04mpyj8")))

(define-public crate-timrs_hkt_macro-1.1.0-rc.6 (c (n "timrs_hkt_macro") (v "1.1.0-rc.6") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)) (d (n "timrs_macro_utils") (r "^1.1.0-rc.5") (d #t) (k 2)))) (h "11gj0j4dmxkc94jxzvay3bkrp3v01lnv81vfafnfgfcw1lh2vrcr")))

