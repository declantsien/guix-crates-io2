(define-module (crates-io ti zo tizol) #:use-module (crates-io))

(define-public crate-tizol-0.1.0 (c (n "tizol") (v "0.1.0") (d (list (d (n "apodize") (r "^0.3.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "hodges") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.5.0") (d #t) (k 1)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.0") (d #t) (k 0)) (d (n "scarlet") (r "^1.0.0") (d #t) (k 0)) (d (n "strider") (r "^0.1.3") (d #t) (k 0)))) (h "1n55cr7cavrjf2mcix5pmgxcjg056c35n7vznvx6cxzh54a9gq8l")))

