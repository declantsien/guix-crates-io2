(define-module (crates-io ti mp timpack-cli) #:use-module (crates-io))

(define-public crate-timpack-cli-0.1.0 (c (n "timpack-cli") (v "0.1.0") (h "0swv8bkp798kqqg4fglm8hsf780znhi2f81bpnx39h4z5q47psx2")))

(define-public crate-timpack-cli-0.2.0 (c (n "timpack-cli") (v "0.2.0") (h "08xc0pafv55m0qy3rxmrw0750ji4v74nmpjrmh1nshvp1jmdfn9z")))

(define-public crate-timpack-cli-0.2.1 (c (n "timpack-cli") (v "0.2.1") (h "1aa3qxrbfrai2v0212r115nn9xfaapzkw2mrdchwm1lbhbkmf1k2")))

(define-public crate-timpack-cli-0.3.0 (c (n "timpack-cli") (v "0.3.0") (d (list (d (n "which") (r "^3.0.0") (d #t) (k 0)))) (h "1y3pvkad8r3r6cymjsdmrvg4hjhbhby72gxrxpb25k189i52ii51")))

(define-public crate-timpack-cli-0.3.1 (c (n "timpack-cli") (v "0.3.1") (d (list (d (n "which") (r "^3.0.0") (d #t) (k 0)))) (h "1wpinm2h8m330nnp50045is6jjr8q54gy2klrxxjpqbdgkjzd705")))

