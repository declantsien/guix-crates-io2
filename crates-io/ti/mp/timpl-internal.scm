(define-module (crates-io ti mp timpl-internal) #:use-module (crates-io))

(define-public crate-timpl-internal-0.1.0-alpha.1 (c (n "timpl-internal") (v "0.1.0-alpha.1") (h "0y902qnq9sf3qwnfcn7zd1m89azai2rzikzg9x67f7yamc2rmdr8")))

(define-public crate-timpl-internal-0.1.0-alpha.2 (c (n "timpl-internal") (v "0.1.0-alpha.2") (h "0kg6swnp3iglzfdy9hazr1s8n017shma7zzaza3jw4smiy561vb3")))

