(define-module (crates-io ti mp timpl) #:use-module (crates-io))

(define-public crate-timpl-0.1.0-alpha.1 (c (n "timpl") (v "0.1.0-alpha.1") (d (list (d (n "timpl-decl") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "timpl-internal") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "timpl-proc") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "09kgfvny4bqwi1r6qy3jqp0hlhyw47n9q7dsaxb4alhz5azzn9y0")))

(define-public crate-timpl-0.1.0-alpha.2 (c (n "timpl") (v "0.1.0-alpha.2") (d (list (d (n "timpl-decl") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "timpl-internal") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "timpl-proc") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "09sxbr0j5s1q8aslmwlhhdz11z3168jj2kcca1wsfgwq6ibfrni3")))

