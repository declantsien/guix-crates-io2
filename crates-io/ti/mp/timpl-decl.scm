(define-module (crates-io ti mp timpl-decl) #:use-module (crates-io))

(define-public crate-timpl-decl-0.1.0-alpha.1 (c (n "timpl-decl") (v "0.1.0-alpha.1") (d (list (d (n "timpl-proc") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0fwyi2w9gn47m6cynbvac68nagqwv0jq82rp57sl9vhkx5fjbak6")))

(define-public crate-timpl-decl-0.1.0-alpha.2 (c (n "timpl-decl") (v "0.1.0-alpha.2") (d (list (d (n "timpl-proc") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0bhpgf8jkr60mzn4z28hh4diz322f8lan1a5rfgqg8g8dxpscpvx")))

