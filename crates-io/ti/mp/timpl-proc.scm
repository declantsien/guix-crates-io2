(define-module (crates-io ti mp timpl-proc) #:use-module (crates-io))

(define-public crate-timpl-proc-0.1.0-alpha.1 (c (n "timpl-proc") (v "0.1.0-alpha.1") (d (list (d (n "timpl-internal") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "00w23h9nixf1687sw83wxx2gkz51a4p9wc4nspjbs6ndac85f3yn")))

(define-public crate-timpl-proc-0.1.0-alpha.2 (c (n "timpl-proc") (v "0.1.0-alpha.2") (d (list (d (n "timpl-internal") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "188n7kimjx2pn2zib5y984a7qqrixa9r50sispq7k8dscf8vnb8v")))

