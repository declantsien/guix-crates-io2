(define-module (crates-io ti an tianyi_api) #:use-module (crates-io))

(define-public crate-tianyi_api-0.1.0 (c (n "tianyi_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 2)))) (h "158nv71r1cyfzsisdswk14i7j4kvsa18vz6syip7bl02x5x1z377")))

(define-public crate-tianyi_api-0.1.1 (c (n "tianyi_api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 2)))) (h "0byxqr04r1cmxnbfh6ypy3frcxvh0fgkdwavimq8157bq2nbnwis")))

