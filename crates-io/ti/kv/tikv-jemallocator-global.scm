(define-module (crates-io ti kv tikv-jemallocator-global) #:use-module (crates-io))

(define-public crate-tikv-jemallocator-global-0.4.0 (c (n "tikv-jemallocator-global") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1ib3lyn9s7x6av073gvcbia9jpfvyl52cvp26avbmam7bwf9qpf8") (f (quote (("force_global_jemalloc" "tikv-jemallocator") ("default"))))))

(define-public crate-tikv-jemallocator-global-0.5.0 (c (n "tikv-jemallocator-global") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\", target_os = \"netbsd\", target_os = \"openbsd\"))") (k 0)) (d (n "tikv-jemallocator") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "07x1vvmc2f5n0rkwr3dgb8mb6dc2xmd0qi8bkfkj2yzq6nslg062") (f (quote (("force_global_jemalloc" "tikv-jemallocator") ("default"))))))

