(define-module (crates-io ti kv tikv-jemalloc-ctl) #:use-module (crates-io))

(define-public crate-tikv-jemalloc-ctl-0.4.0 (c (n "tikv-jemalloc-ctl") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "tikv-jemalloc-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (d #t) (k 2)))) (h "175x0p659q6s1a2bwl2i0y28qv888nj0ybz68dv8zlmibagcwhdl") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-tikv-jemalloc-ctl-0.4.1 (c (n "tikv-jemalloc-ctl") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "tikv-jemalloc-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (d #t) (k 2)))) (h "1pqs08dgnnrgbib8syldxmm8cjgyz80scs9i8jgn6mw86gj8137j") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-tikv-jemalloc-ctl-0.4.2 (c (n "tikv-jemalloc-ctl") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tikv-jemalloc-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.4.0") (d #t) (k 2)))) (h "0s9yjvck4mn4rvhm7y5hr6svxhgrmgycwixkrjp6v2xzxi33r0zb") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-tikv-jemalloc-ctl-0.5.0 (c (n "tikv-jemalloc-ctl") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tikv-jemalloc-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.0") (d #t) (k 2)))) (h "1h8n88mg2p7h288mj61i7bz2d72fh10f0ih1gbzis5ab5xbhcxz3") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-tikv-jemalloc-ctl-0.5.4 (c (n "tikv-jemalloc-ctl") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tikv-jemalloc-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.0") (d #t) (k 2)))) (h "0763cba891c9r8r8d8iqw5pfcq409w6l7fbnyzvm8yw0gp9gx6v1") (f (quote (("use_std" "libc/use_std") ("disable_initial_exec_tls" "tikv-jemalloc-sys/disable_initial_exec_tls") ("default"))))))

