(define-module (crates-io ti kv tikv-openssl-src) #:use-module (crates-io))

(define-public crate-tikv-openssl-src-111.10.3+1.1.1g (c (n "tikv-openssl-src") (v "111.10.3+1.1.1g") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "04rmj94ddwpgaw38rnrc6z6n557zzx41r3h4dyzg9jh9l4b0zd10") (f (quote (("weak-crypto") ("seed") ("idea") ("default") ("camellia"))))))

