(define-module (crates-io ti kv tikv-client-proto) #:use-module (crates-io))

(define-public crate-tikv-client-proto-0.0.99 (c (n "tikv-client-proto") (v "0.0.99") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.8") (f (quote ("secure" "prost-codec" "use-bindgen"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-derive") (r "^0.7") (d #t) (k 0)) (d (n "protobuf") (r "^2.8") (d #t) (k 0)) (d (n "protobuf-build") (r "^0.12") (f (quote ("grpcio-prost-codec"))) (k 1)))) (h "12gjhpr1cfbsjlz4fwhqhqjscbhan4ixfjdrjx43gv49m7cky0bz")))

(define-public crate-tikv-client-proto-0.1.0 (c (n "tikv-client-proto") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "grpcio") (r "^0.8") (f (quote ("secure" "prost-codec" "use-bindgen"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-derive") (r "^0.7") (d #t) (k 0)) (d (n "protobuf") (r "^2.8") (d #t) (k 0)) (d (n "protobuf-build") (r "^0.12") (f (quote ("grpcio-prost-codec"))) (k 1)))) (h "16vx8039x30sv6x4ad5m0qpiw3rx23341pxkdrxvh68wq5ipqw7r")))

