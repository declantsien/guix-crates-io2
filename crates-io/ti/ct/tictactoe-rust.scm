(define-module (crates-io ti ct tictactoe-rust) #:use-module (crates-io))

(define-public crate-tictactoe-rust-0.1.3 (c (n "tictactoe-rust") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "02kf5wqc3zdpv37wzpr0bjb0kkx238x66p36hp6ygrl4fizddvpa")))

(define-public crate-tictactoe-rust-0.1.4 (c (n "tictactoe-rust") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0akfn11m0yzbdsiwy59fwapac32asyrlwwn4jb3pxl3rym1myj7j")))

