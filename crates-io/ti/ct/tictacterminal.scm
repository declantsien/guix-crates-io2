(define-module (crates-io ti ct tictacterminal) #:use-module (crates-io))

(define-public crate-tictacterminal-1.0.0 (c (n "tictacterminal") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0qq9n3sril9850myiplmvbnzqdns2v4qb33bl6splh38xpw6iami")))

(define-public crate-tictacterminal-1.0.1 (c (n "tictacterminal") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0v64xhjlw26dbxvzalihhqapy53lavs40rkcyrwxgllf5jy77fli")))

(define-public crate-tictacterminal-1.0.2 (c (n "tictacterminal") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "1f03pkwh1fkcdywx9a7b017l4n5jwa4j3l2mwdaqxfq1b6hin0fq")))

(define-public crate-tictacterminal-1.2.2 (c (n "tictacterminal") (v "1.2.2") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0lmamn6cc6g2c0sg4cpj2ncl6r0zl7x62mr3x2xrvwf2qhy2vxsl")))

(define-public crate-tictacterminal-2.0.0 (c (n "tictacterminal") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1ghsknfkrkxqwlf3imz204kqhy8caciqrdgpgpp9p0v8rb70sdvm")))

(define-public crate-tictacterminal-2.0.1 (c (n "tictacterminal") (v "2.0.1") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "07m5hh60xznimnqq3pnz5hbydf21qh9f6ivs2fwbxynp0l4f7saa")))

(define-public crate-tictacterminal-2.0.2 (c (n "tictacterminal") (v "2.0.2") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "04i5bw29lrjyvzd5mgdjyn49832brn46qzkw0m1ddkxcdqqrvbaj")))

(define-public crate-tictacterminal-2.0.3 (c (n "tictacterminal") (v "2.0.3") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1ndm3iydwz94ghnsgg2myv6sd1f19akqh491xcljx28dmi3dg43w")))

(define-public crate-tictacterminal-2.0.4 (c (n "tictacterminal") (v "2.0.4") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "1h1z2fw4yicnibnawmcv9ma8y9fppfml25qz3r52d70qy347kfzh")))

(define-public crate-tictacterminal-2.0.5 (c (n "tictacterminal") (v "2.0.5") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "01dkjvsvcdmzw45frjwf575bbfwa4j1p5s9kb95fxw48lbyxnx3a")))

