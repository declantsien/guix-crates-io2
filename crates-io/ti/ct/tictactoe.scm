(define-module (crates-io ti ct tictactoe) #:use-module (crates-io))

(define-public crate-tictactoe-0.1.2 (c (n "tictactoe") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "0ljh61r7k1aznznyjir5d3vn3cqr773nhcjyb5snws50w3ky7vzw")))

(define-public crate-tictactoe-0.1.3 (c (n "tictactoe") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "06c2g6zzzk3nab437659ddqh91famllsh7v8qg8bwxag7v2m122w")))

(define-public crate-tictactoe-0.1.4 (c (n "tictactoe") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "14scdl6grsh7cllr7wqfprzj5zx61mhxsyddyf2f6f8ddzpw0k8c")))

