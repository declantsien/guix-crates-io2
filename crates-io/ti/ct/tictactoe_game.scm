(define-module (crates-io ti ct tictactoe_game) #:use-module (crates-io))

(define-public crate-tictactoe_game-1.0.0 (c (n "tictactoe_game") (v "1.0.0") (d (list (d (n "array2d") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rprompt") (r "^2.1.1") (d #t) (k 0)))) (h "1yw9v10zrix4whlfbvx4gjpz5xy7n8q1jfsa99g4sw7pyd3cpxzk")))

(define-public crate-tictactoe_game-1.0.1 (c (n "tictactoe_game") (v "1.0.1") (d (list (d (n "array2d") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rprompt") (r "^2.1.1") (d #t) (k 0)))) (h "0iqf401v5vh2skvnxgds7r2kxxxsr9zkn3blh8x7pypr8ddq9vc0")))

(define-public crate-tictactoe_game-1.0.2 (c (n "tictactoe_game") (v "1.0.2") (d (list (d (n "array2d") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "std_rng"))) (k 0)) (d (n "rprompt") (r "^2.1.1") (d #t) (k 0)))) (h "1yw2mijs6dw0l3j63bin7whmnny8n7shcvnwg8kxgc6jvk4ahbvj")))

