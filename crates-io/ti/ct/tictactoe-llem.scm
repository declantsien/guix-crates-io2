(define-module (crates-io ti ct tictactoe-llem) #:use-module (crates-io))

(define-public crate-tictactoe-llem-0.2.0 (c (n "tictactoe-llem") (v "0.2.0") (h "0rwjp9sm657rgnay9krxidgy62bb292dvp53cf5ri6qfh6lnz4ds")))

(define-public crate-tictactoe-llem-0.2.5 (c (n "tictactoe-llem") (v "0.2.5") (h "16496f7bcaq8hknmh7pl0p6ry5rd8gm4aby0nwl2m10aid8h97s2")))

