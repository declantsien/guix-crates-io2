(define-module (crates-io ti ct tictoc) #:use-module (crates-io))

(define-public crate-tictoc-0.1.0 (c (n "tictoc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0qlqvx69z784m7hc957jvf575yzsc55cnv71lzfv75q9spg2bhwm") (f (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1.1 (c (n "tictoc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "084jrix6ps10gb3ipxy429girbphyivpzdkbwzy7ry7ypqcs97b5") (f (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1.2 (c (n "tictoc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "083d1rl98fqyd14b8jgsjqla5yj1q1625vmgwbs6bdf21niz4ak1") (f (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1.3 (c (n "tictoc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1x9lrj4kyakqmw3nqm6cgslfhm9j8kc103h2ky03b361p2km10ms") (f (quote (("localtime" "chrono") ("default" "chrono" "chrono/unstable-locales"))))))

(define-public crate-tictoc-0.1.4 (c (n "tictoc") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1p59p2rmp228aya29792silbb5aycka0cjn2ar2d5zvj99nrjfdw") (f (quote (("localtime" "chrono" "chrono/unstable-locales") ("default" "chrono" "chrono/unstable-locales"))))))

