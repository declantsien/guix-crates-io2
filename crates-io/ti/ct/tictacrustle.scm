(define-module (crates-io ti ct tictacrustle) #:use-module (crates-io))

(define-public crate-tictacrustle-1.0.0-next.1 (c (n "tictacrustle") (v "1.0.0-next.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0q8dscl7hs0mhrhn4i0431ds3qyh3ss067zgkzmg8d9pf8mz4ss2")))

(define-public crate-tictacrustle-1.0.0-next.2 (c (n "tictacrustle") (v "1.0.0-next.2") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "07vmxalazscfnk22izjrdbrff4hpl2q39dyykci3v6lsm3y3kkdy")))

