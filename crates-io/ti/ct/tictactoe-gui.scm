(define-module (crates-io ti ct tictactoe-gui) #:use-module (crates-io))

(define-public crate-tictactoe-gui-0.1.0 (c (n "tictactoe-gui") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1g9r09cb6g04sa3vb55iq9wlcafksvgkz389qsv5azzn4q7rfi2c")))

(define-public crate-tictactoe-gui-0.1.1 (c (n "tictactoe-gui") (v "0.1.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0x73b5n16l1k23aq7vfy9jvfaysvyi02mwfx7qmkjhwaah764mhi")))

(define-public crate-tictactoe-gui-0.1.2 (c (n "tictactoe-gui") (v "0.1.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1m2z2467hpcyrmsxki4cff3cvfdgbnkz8j9bc9rwgpdvj1r25gfz")))

