(define-module (crates-io ti ct tictactoe-rs) #:use-module (crates-io))

(define-public crate-tictactoe-rs-0.1.0 (c (n "tictactoe-rs") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)))) (h "0rr289m0hc9k4wln1xglf5xgvhjya76vj7mp381vq6sn7amg4b0i") (y #t)))

