(define-module (crates-io ti ts tits-rs) #:use-module (crates-io))

(define-public crate-tits-rs-0.1.0 (c (n "tits-rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "11zl63mn9p3b9x9wg41lya6vwpc6kgnvbwp05c9haw35h4q5vjjz") (y #t)))

(define-public crate-tits-rs-0.0.1 (c (n "tits-rs") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "08gashw4bih86ich4i17s9c86gxxxpxyzzl408hcd0b5lhxzrnwx")))

(define-public crate-tits-rs-0.0.2 (c (n "tits-rs") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "1gqx0fsx2hk46yzd5cqhh3fgnrxqrw01r5ryqmlyjw4ivwavb5k7")))

