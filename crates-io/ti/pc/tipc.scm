(define-module (crates-io ti pc tipc) #:use-module (crates-io))

(define-public crate-tipc-0.1.0 (c (n "tipc") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "errno") (r "^0.2.6") (d #t) (k 0)))) (h "0la7ir91xx343rwvmkjlxc742zss4pkprrrmlgp4my46arqs39ck")))

(define-public crate-tipc-0.1.1 (c (n "tipc") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "errno") (r "^0.2.6") (d #t) (k 0)))) (h "1yb3iqaccring7vl4qwk84zxbdzdjxfmd3hkd9nkk65mslm45g6f")))

(define-public crate-tipc-0.1.2 (c (n "tipc") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "errno") (r "^0.2.6") (d #t) (k 0)))) (h "1l5maykcxxz166gzxqgk9z8gh37q2p4kvf5jvzmdnw5zs3ln84id")))

