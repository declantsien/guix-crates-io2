(define-module (crates-io ti lp tilper) #:use-module (crates-io))

(define-public crate-tilper-0.0.1 (c (n "tilper") (v "0.0.1") (h "1qgb8v5jin18mjpm6362qilzsh7pa9z2136z7hxf690gzwc838ns")))

(define-public crate-tilper-0.0.2 (c (n "tilper") (v "0.0.2") (h "1x225cp3skf38n4jjygsfsg8fv7ny7v0aznc5z2nyns0r2w3qg7j")))

(define-public crate-tilper-0.0.3 (c (n "tilper") (v "0.0.3") (h "1mdfhkfwmy7r43ny76v6fsmn9ag81n26iqjlv2mw2d74d9dpja7j")))

