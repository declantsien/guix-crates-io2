(define-module (crates-io ti pp tippytap) #:use-module (crates-io))

(define-public crate-tippytap-0.1.0 (c (n "tippytap") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "05jsm1w7cis0nscifv31ysswrj6rjassw6adjgbbbpj1j0vqhj9r")))

(define-public crate-tippytap-0.2.0 (c (n "tippytap") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "14df8b08wixya3dwa0i2bnmx9d1gpql5j8ph6z735zvzc71h5jqy")))

