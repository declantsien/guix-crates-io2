(define-module (crates-io ti nf tinfo) #:use-module (crates-io))

(define-public crate-tinfo-0.0.1 (c (n "tinfo") (v "0.0.1") (d (list (d (n "regex") (r "^0.1.17") (d #t) (k 0)))) (h "0ahnrxbkdnsf2vak85hzlch9qwl7imlmksxs04i1a7k6xq8bizhs")))

(define-public crate-tinfo-0.1.0 (c (n "tinfo") (v "0.1.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "0jc6k9pmdwk2andd2lxqznf14hilyfjxv4lvwqmvc6f3g48f8yk0")))

(define-public crate-tinfo-0.2.0 (c (n "tinfo") (v "0.2.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "1wdyxpl4sqi07c51mr8j1xmahm5q7sls0qyfrdmzmhaa2c7lnsv0")))

(define-public crate-tinfo-0.3.0 (c (n "tinfo") (v "0.3.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "12lijk3bf2pp9mqz0vpr94a5gk38cn8lgyibqggz7db5jbjrq1y6")))

(define-public crate-tinfo-0.4.0 (c (n "tinfo") (v "0.4.0") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "1jmv9vdf3jvx8qk6mq2i4hph5yylhnrahma3qjbjcq68f5xx66ir")))

(define-public crate-tinfo-0.5.0 (c (n "tinfo") (v "0.5.0") (d (list (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "0.*") (d #t) (k 0)))) (h "0v5w39yn0wz4hhz0d79cf7lki8dmxr13afnaffn6nfl35aaj3zra")))

(define-public crate-tinfo-0.6.0 (c (n "tinfo") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0syn7nicdrc6mdaq6mkpybfg5hkshy1y9aglmghy58hx5j1fcyk4")))

