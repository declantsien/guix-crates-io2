(define-module (crates-io ti nf tinf) #:use-module (crates-io))

(define-public crate-tinf-0.10.0 (c (n "tinf") (v "0.10.0") (h "1ik3d3mdyq9y4q19nyzjw2kdrjg0b1az6cvz1ck2nxdm8xf76wpb")))

(define-public crate-tinf-0.11.0 (c (n "tinf") (v "0.11.0") (h "059308jyf4w27w7vrcvjz7kvjinqvnm1kjsxk34fnv85iyfy5nip")))

(define-public crate-tinf-0.11.1 (c (n "tinf") (v "0.11.1") (h "0rzhcq11rnj2hzrq291zcwvv6wmkpsprz93qzwcs46f79p4p48cm")))

(define-public crate-tinf-0.11.2 (c (n "tinf") (v "0.11.2") (h "0rycs8nm4zy6ac5h5h2qxddy7crrfqyw08d4zck4a6dc7z6mbkmw")))

(define-public crate-tinf-0.12.0 (c (n "tinf") (v "0.12.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1nc9q3c5k3r8phrv7hpzw6ivna95h4j90d3b49rqilqav9i8mpqp")))

(define-public crate-tinf-0.12.2 (c (n "tinf") (v "0.12.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1x0qpnhz8hd75byf61r7158c0l6wi9ksyv21vzmgmr3z4475ax90")))

(define-public crate-tinf-0.12.3 (c (n "tinf") (v "0.12.3") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1qpa2qjgcy938r47sgy32rn7l5j6q7czh52w01jdr4x14j37h3ih")))

(define-public crate-tinf-0.12.4 (c (n "tinf") (v "0.12.4") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0nhz8idfsypc59lggslrpghmn2f2kixss7gld6zjmp4mcsxhqb10")))

(define-public crate-tinf-0.12.5 (c (n "tinf") (v "0.12.5") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "06d6hab93ghkpj3bq8gdz07pkvvsh8m07zssmn5cc7saq15swx9w")))

(define-public crate-tinf-0.13.0 (c (n "tinf") (v "0.13.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0959v6z79yd0g4a2vcnd7vwvm19wpcfg6ay6lgdl8i05ma1rmkv1")))

(define-public crate-tinf-0.14.0 (c (n "tinf") (v "0.14.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0gd1cdyiaybvpv6wvjxxg87sxvlxh9zlg4bn2ibyys4fqlpsz37l")))

