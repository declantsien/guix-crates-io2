(define-module (crates-io ti xo tixonome) #:use-module (crates-io))

(define-public crate-tixonome-0.1.0 (c (n "tixonome") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1a65bpgkx5zqi3rlqjn9y086vyszzjgsc2pksl9f6qj2j0g4fqrm")))

(define-public crate-tixonome-0.2.0 (c (n "tixonome") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1l652kh9blmjkqj34bv08srysvy4jclzr48h5z6yx9xmjhai2knv")))

(define-public crate-tixonome-0.2.1 (c (n "tixonome") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "079kwr3z2b8pwp5nsacvm5vpyrzq80ij5p7sv1783nq3f719y8xp")))

(define-public crate-tixonome-0.3.0 (c (n "tixonome") (v "0.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1zvqlanj38ykdqvlfk21ad86qpb8jjvbxf96g7m4qpcs9ryr8asi")))

(define-public crate-tixonome-0.3.1 (c (n "tixonome") (v "0.3.1") (d (list (d (n "die") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1m0x0l8x01xy1nj1am458h9dafnvm10y1fscch2fwalw5c1bg3cg")))

(define-public crate-tixonome-0.3.2 (c (n "tixonome") (v "0.3.2") (d (list (d (n "die") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1dh0n8yrrjb3cfxi5a9k0rz451j8jsfpr33lpawnap3k59jkrdl4")))

