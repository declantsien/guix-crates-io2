(define-module (crates-io ti #{64}# ti64) #:use-module (crates-io))

(define-public crate-ti64-0.1.0 (c (n "ti64") (v "0.1.0") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1hjdxvv2kncnymczsrp852zm175w6a5pqlbl1yg7zk7hl12qap1v")))

(define-public crate-ti64-0.1.1 (c (n "ti64") (v "0.1.1") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1bhjn7m3rrb39sdzb4c3x3f4j684z05ncpllg0f7yfnwg8dmqw1r")))

(define-public crate-ti64-0.1.2 (c (n "ti64") (v "0.1.2") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "1n41cg7bzhpxmk90a1sd5g4l86kpskafky9l0xhqhblbp903bnk0")))

(define-public crate-ti64-0.1.3 (c (n "ti64") (v "0.1.3") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "parsing"))) (d #t) (k 0)))) (h "0cgqaf2zzbzgydbg2k9yvfcj3wxlyqyxc3c7nc038x54g1biav2s")))

(define-public crate-ti64-0.1.4 (c (n "ti64") (v "0.1.4") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "parsing"))) (d #t) (k 0)))) (h "19qm21ydb27a58nrvyr26barkswwhmla58qd5j05xah7kvjiq9q8")))

(define-public crate-ti64-0.1.5 (c (n "ti64") (v "0.1.5") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "parsing"))) (d #t) (k 0)))) (h "1s2cxvrwg97kh0yr857bp2k1s41981pv6h1av9x0imyqwx36fz6v")))

