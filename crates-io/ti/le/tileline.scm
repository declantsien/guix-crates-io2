(define-module (crates-io ti le tileline) #:use-module (crates-io))

(define-public crate-tileline-0.1.0 (c (n "tileline") (v "0.1.0") (d (list (d (n "builder-pattern") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)))) (h "0n0zisn27n6fll6gihvwj775yxfr9nlgdr7fvyk3w8rm93x2nrfc") (s 2) (e (quote (("year_line" "dep:chrono"))))))

