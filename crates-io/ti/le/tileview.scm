(define-module (crates-io ti le tileview) #:use-module (crates-io))

(define-public crate-tileview-0.1.0 (c (n "tileview") (v "0.1.0") (d (list (d (n "pty-process") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1jf3hwpsj2cggkgdpf39hbzfyf4knvlb6b371wppfrnzbj3wgwq3")))

(define-public crate-tileview-0.1.1 (c (n "tileview") (v "0.1.1") (d (list (d (n "pty-process") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1c5ycamn9fwcprggfdilr339lwa6zq75qqz0h1dzia0dc0gs84si")))

(define-public crate-tileview-0.1.2 (c (n "tileview") (v "0.1.2") (d (list (d (n "pty-process") (r "^0.4.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "01zjb80gmqv2zkfx7qnn99wmzsnszdm1idmh9lh2bz3y5zls9nqj")))

