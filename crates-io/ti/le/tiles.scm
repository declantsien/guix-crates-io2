(define-module (crates-io ti le tiles) #:use-module (crates-io))

(define-public crate-tiles-0.0.0 (c (n "tiles") (v "0.0.0") (h "1a9ni53zi4x73jxynabk39bbz5c8kw92pj0z0c9xywv6cm7h3q2y") (y #t)))

(define-public crate-tiles-0.0.1 (c (n "tiles") (v "0.0.1") (h "0y8267hlmc9as5s44km7wga4zk1fvdpc3wmp75lxrhqhcq947xb0") (y #t)))

