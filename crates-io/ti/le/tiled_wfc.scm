(define-module (crates-io ti le tiled_wfc) #:use-module (crates-io))

(define-public crate-tiled_wfc-0.1.0 (c (n "tiled_wfc") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "nd_matrix") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "144ydi4sj2z31a3dpk29aqxzyv8kflahdk3qz3qandsiypapb7zk")))

(define-public crate-tiled_wfc-0.2.0 (c (n "tiled_wfc") (v "0.2.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "nd_matrix") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00rnvhyjj7n0l4xd85mak39d7qgsaqlr9wzsm3mplap7avi68chp")))

(define-public crate-tiled_wfc-0.3.0 (c (n "tiled_wfc") (v "0.3.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "nd_matrix") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1d1s8ih8g7abwfcccmswfqh916qnwjcakllwg9mzs0h23mfb99sg")))

(define-public crate-tiled_wfc-0.4.0 (c (n "tiled_wfc") (v "0.4.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "nd_matrix") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0gmyan00m3cv0cbz94i98flvf84h35v69d46w7zyfhqjkn1a301w")))

