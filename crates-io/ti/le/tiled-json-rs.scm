(define-module (crates-io ti le tiled-json-rs) #:use-module (crates-io))

(define-public crate-tiled-json-rs-0.1.0 (c (n "tiled-json-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g1spbm0wq4d7kfw4qvvvfi78hpf8q2gfqfrdyj1a149mvfnhzdz")))

(define-public crate-tiled-json-rs-0.2.0 (c (n "tiled-json-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hs9w9fi8aqjgz9309nh8wz62h0s2fpzybz6k1zy4sx1q7xfld6v")))

(define-public crate-tiled-json-rs-0.2.1 (c (n "tiled-json-rs") (v "0.2.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vbcbsznrxd7ps22239s8slx077xbrxhx5jc4v8kfxvfx8l35j4p")))

(define-public crate-tiled-json-rs-0.2.2 (c (n "tiled-json-rs") (v "0.2.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xxc45awp0r0lb6cxv1443jzxqpjg11xr1yg682csldbizsddhlf")))

(define-public crate-tiled-json-rs-0.2.3 (c (n "tiled-json-rs") (v "0.2.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sxbxkrrldp9pxy92026327a4xhy1b3d192xdb47bdcm0i0ykm22")))

(define-public crate-tiled-json-rs-0.2.4 (c (n "tiled-json-rs") (v "0.2.4") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdchz3swnx1yh25q676amwqfa8821v0xqarflcv24mjip51v6y0")))

(define-public crate-tiled-json-rs-0.2.5 (c (n "tiled-json-rs") (v "0.2.5") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "062zi9cc2dpcx82rviv0hv3dqa2hkjwdg2d8gmvq6d0w9saphmj3")))

(define-public crate-tiled-json-rs-0.2.6 (c (n "tiled-json-rs") (v "0.2.6") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "011pn8z1dcm3fmkn2hd1v4qv4n6ld95x17jfrsnds871gjx295b7")))

