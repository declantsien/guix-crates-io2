(define-module (crates-io ti le tile-downloader) #:use-module (crates-io))

(define-public crate-tile-downloader-0.1.0 (c (n "tile-downloader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hl6mnvz12m2yr4gy7lc6sr2vf0whv28ncs1kgjlpk42g3h8xfcp")))

