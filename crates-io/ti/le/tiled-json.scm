(define-module (crates-io ti le tiled-json) #:use-module (crates-io))

(define-public crate-tiled-json-0.1.0 (c (n "tiled-json") (v "0.1.0") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1lf7gb9yjdrjxxq83zyzxyi2wngjg79svl6q21rfglw4lnlfsgbr")))

(define-public crate-tiled-json-0.1.1 (c (n "tiled-json") (v "0.1.1") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1hn5ik2ydpvrkrj8l49yzkrmy8n0dxbv2319fm3z9ani8x0lfm7s")))

(define-public crate-tiled-json-0.1.2 (c (n "tiled-json") (v "0.1.2") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "0cvq3wbga65d72na43ya5rgdyfw26nn58yh6jpj36b9p7mpqxicb")))

(define-public crate-tiled-json-0.1.4 (c (n "tiled-json") (v "0.1.4") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "1f6h70s41g8z1mdg2p4srrr99frmhb4dp56xagk2bhasca3iadpy")))

