(define-module (crates-io ti le tileable-volume-noise) #:use-module (crates-io))

(define-public crate-tileable-volume-noise-0.3.0 (c (n "tileable-volume-noise") (v "0.3.0") (d (list (d (n "glam") (r ">=0.21, <=0.24") (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "08bh37hn59myhj3bwyz1x0xhzhs9k3ss0kq3jz7gnidr6k32l1n2") (s 2) (e (quote (("images" "dep:image"))))))

