(define-module (crates-io ti le tile_bitmasking) #:use-module (crates-io))

(define-public crate-tile_bitmasking-0.1.0 (c (n "tile_bitmasking") (v "0.1.0") (d (list (d (n "glam") (r "^0.24.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1ffr862v0kihdkrlvkp25c0j55y37aps06ysfzkg75wx93kl8i5h")))

