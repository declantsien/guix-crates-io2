(define-module (crates-io ti le tiled) #:use-module (crates-io))

(define-public crate-tiled-0.1.0 (c (n "tiled") (v "0.1.0") (d (list (d (n "flate2") (r "~0.0.3") (d #t) (k 0)) (d (n "rust-xml") (r "~0.1.1") (d #t) (k 0)))) (h "1vj75mddbd2ihmkjrsn4j2hrzam1bnw0xbl6wf4z854khb3bch0g")))

(define-public crate-tiled-0.1.1 (c (n "tiled") (v "0.1.1") (d (list (d (n "flate2") (r "~0.0.3") (d #t) (k 0)) (d (n "rust-xml") (r "~0.1.1") (d #t) (k 0)))) (h "1p9d6rg7g1nxfxmab0x52y6nqag46w71q32zml024npbiiv4a3p0")))

(define-public crate-tiled-0.1.2 (c (n "tiled") (v "0.1.2") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "0icvny1jwk4wi6ssjikb49fcfm577ww62nk0f0sr7y2d2kka8xzb")))

(define-public crate-tiled-0.1.3 (c (n "tiled") (v "0.1.3") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1l06z4kja7r59jc960h12r42gmnmnikx9ns3vr4dw47jy0hf0fz7")))

(define-public crate-tiled-0.1.4 (c (n "tiled") (v "0.1.4") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.26") (d #t) (k 0)))) (h "1gk1yjr3x5vac65m4294nvxpyy3k9idj0my4g5jgqfgnnwir9c3c")))

(define-public crate-tiled-0.1.5 (c (n "tiled") (v "0.1.5") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0sw7nbchbbnm30ppxiyq9lmg0n8435nlyix75lf1imizb7z4hhan")))

(define-public crate-tiled-0.2.0 (c (n "tiled") (v "0.2.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1br7gpszwdbkcdcnhb2hasnmf5j27bc1cs7pa4hz52fqzhj7gl7k")))

(define-public crate-tiled-0.3.0 (c (n "tiled") (v "0.3.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "00imr2cyaq3am59xawci2rxf62x14hjgqi1rlg3divs4yx1hgif5")))

(define-public crate-tiled-0.4.0 (c (n "tiled") (v "0.4.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0g574yj64vb088nj7ykflq3hhlkdl6wc2ivn5ygfwn3vqq4kjpl3")))

(define-public crate-tiled-0.5.0 (c (n "tiled") (v "0.5.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0jdbj3wxwd2wn9k185imm52wwaq88nyxkz0zvya3mvly4hkwcl71")))

(define-public crate-tiled-0.5.1 (c (n "tiled") (v "0.5.1") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0dssw391viqv93vp8b1isi0qvpnfp8aihxc7i9hxx4rh441zyzpq")))

(define-public crate-tiled-0.6.0 (c (n "tiled") (v "0.6.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1hc7m3j8fb73l9p0h4r1ayv8x307iyji4817hk7p1kgr35lrs6jr")))

(define-public crate-tiled-0.7.0 (c (n "tiled") (v "0.7.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1zcf4f3dw4jgxgwb6cmv81ffj0z822z0gdy585b9zds7hzgv7pwi")))

(define-public crate-tiled-0.7.1 (c (n "tiled") (v "0.7.1") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "18xqhw26awy4ywqcmsm7xiky3qqcbf513szphwf7l9l2hcbajihq")))

(define-public crate-tiled-0.7.2 (c (n "tiled") (v "0.7.2") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "098asa2bq6crgadkjvarsi71372q65c2kr664v5ifhwypzagdxw4")))

(define-public crate-tiled-0.7.3 (c (n "tiled") (v "0.7.3") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "14a1m70hgdx0vbljlg2mad5wcf6ssx43jqsngvwjr7crfs732yal")))

(define-public crate-tiled-0.7.4 (c (n "tiled") (v "0.7.4") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "149irfdrmggjidbfbklfycgvr50734hfikr4bs77hx8lj4j8xmak")))

(define-public crate-tiled-0.8.0 (c (n "tiled") (v "0.8.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "07c3ymcfrjk92vpd42yqbhffp74viagbdhc28kc9cby6wf1dnhry")))

(define-public crate-tiled-0.8.1 (c (n "tiled") (v "0.8.1") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0killvnxw9a06kfrshb7kf5wqmks9qbdyykjmrswfk6k15816g70")))

(define-public crate-tiled-0.8.2 (c (n "tiled") (v "0.8.2") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "05038ckcs8kwghzmyvzhv0lxk67v6s3wkxdhn7y2fzwn8g3b1ygg") (y #t)))

(define-public crate-tiled-0.9.1 (c (n "tiled") (v "0.9.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "11fvzybz34p89b5gk33i6m6rzsq415a9rjfzgpffzi3jm5qrxpzd")))

(define-public crate-tiled-0.9.2 (c (n "tiled") (v "0.9.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1yvl639dbbf3ysirqaw4828xs2lz5bxdk0bdwn52j4x45n03nl9a")))

(define-public crate-tiled-0.9.3 (c (n "tiled") (v "0.9.3") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)) (d (n "zstd") (r "^0.5") (d #t) (k 0)))) (h "1znfmhxgy8r4whdw50zrhbbhvqwkidlnxizymw28m233x5zb8bhn")))

(define-public crate-tiled-0.9.4 (c (n "tiled") (v "0.9.4") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)) (d (n "zstd") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0vwxjzj33s8l99wac5y2m6i812fj7aidn5qs8v5mk0cxxap30b4d") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.9.5 (c (n "tiled") (v "0.9.5") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "libflate") (r "^0.1.18") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)) (d (n "zstd") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0qzylmvlx0aip5yaasdzf39wkimhmnbjl3nbr5vd9zi5jhc7d6wc") (f (quote (("default" "zstd")))) (y #t)))

(define-public crate-tiled-0.10.0 (c (n "tiled") (v "0.10.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0qlssh3pr0rlgv4zjaq64lnb8ndipb2w7cppspm62pi4pgxra65n") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.10.1 (c (n "tiled") (v "0.10.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0rfj2si55k2x66zfkmhgncbd1mqnyvyjnyk9hy52yxrcmc55zldz") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.10.2 (c (n "tiled") (v "0.10.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.16") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0qjc6hzs1lycw98ixkdwx06aivzdq1w5rq7zxv55jbi0lns36d9v") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.10.3 (c (n "tiled") (v "0.10.3") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1cs1mscbc0zk1xbkbg91x2pp33aljbhwgf41sdnf1wy9isany1f8") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.11.0 (c (n "tiled") (v "0.11.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "14pjrxvp4xiddhdrr39j0x5vpc2vadcb4fpcswy6c2sb3r74gld8") (f (quote (("default" "zstd"))))))

(define-public crate-tiled-0.11.1 (c (n "tiled") (v "0.11.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7") (d #t) (k 2)) (d (n "libflate") (r "^1.1.2") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.12.0") (o #t) (k 0)))) (h "0nvbdkkqp0rvpcvr420s9g4pqwyhk23irs58a7lp8z98pq1287hq") (f (quote (("wasm" "zstd/wasm") ("default" "zstd"))))))

(define-public crate-tiled-0.11.2 (c (n "tiled") (v "0.11.2") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "ggez") (r "^0.9.3") (d #t) (k 2)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "sfml") (r "^0.20.0") (f (quote ("graphics"))) (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)) (d (n "zstd") (r "^0.12.0") (o #t) (k 0)))) (h "1b6yjg0g3d2916344k5r836n5idwqmwyy3xfnzsxnz2wzzvb6j60") (f (quote (("wasm" "zstd/wasm") ("default" "zstd"))))))

