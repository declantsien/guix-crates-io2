(define-module (crates-io ti le tilejson) #:use-module (crates-io))

(define-public crate-tilejson-0.1.0 (c (n "tilejson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iqqhiv44wr8m3gqpbv40z3dfqkprpfp41421w2mn5smazssn5r1")))

(define-public crate-tilejson-0.2.0 (c (n "tilejson") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zdsnkr9gss6fis32z7hfdq8sxps2g4slll1hmg1vmqbj6qmph7k")))

(define-public crate-tilejson-0.2.1 (c (n "tilejson") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nkll702183awlp2wlh19kpcq4251wf9bb7hj9s5f0azvfs3zg70")))

(define-public crate-tilejson-0.2.2 (c (n "tilejson") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0508pfsh8frwgkzlmizwqj17pbwkf18cn6xzf1kia5ia6rvn21lg")))

(define-public crate-tilejson-0.2.3 (c (n "tilejson") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iybl5jg4im982czxmfqqkkyh9c7dl456mw0g9likl6kpa3ycj0v")))

(define-public crate-tilejson-0.2.4 (c (n "tilejson") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15sfrx8mic5j9w5lh0q6nq1bfw7ipz620nvp18yzlg9g40vkzcd1")))

(define-public crate-tilejson-0.3.0 (c (n "tilejson") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)))) (h "0knb6942mk8r0z3kz1ajl4ma3npjfh8wjs277im0b8a39gw0msqy")))

(define-public crate-tilejson-0.3.1 (c (n "tilejson") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)))) (h "1cyy6d33nysqj497rd0kapwwq6arq8h20bqypp57hycay5p6520f")))

(define-public crate-tilejson-0.3.2 (c (n "tilejson") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)))) (h "0yka9h6y6syf8sbzk8pfh9d7fkfhh5cr6m9k98qg5f22i5qby3v7")))

(define-public crate-tilejson-0.3.3 (c (n "tilejson") (v "0.3.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)))) (h "1jd9j3jnj3yn082lgqqjngx0dcpr3n5370arhc3b9wk0xbji4qm1")))

(define-public crate-tilejson-0.3.4 (c (n "tilejson") (v "0.3.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03dc2s4wslbvy25ks2y9k23l536x2fv6s1w9757hldlm5r8963w6")))

(define-public crate-tilejson-0.4.0 (c (n "tilejson") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15c9sbjgb6k8aq0g1bwb088n3c6riwn0ifjpf0ij92jgswrx0rh3")))

(define-public crate-tilejson-0.4.1 (c (n "tilejson") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xa4v70gdy4jyq7y0vhpqrlmc0r5y97agl4hvfbdxjf9w2yshmj4")))

