(define-module (crates-io ti le tiledb-sys) #:use-module (crates-io))

(define-public crate-tiledb-sys-0.1.0 (c (n "tiledb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "1bvqd38x747zwj3asks4swk10y9y15l2dlhnhp70pcilvqzi8173") (y #t) (l "tiledb")))

(define-public crate-tiledb-sys-0.1.1 (c (n "tiledb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)))) (h "018lxw60kh2iarzyl0iq0q2g9bacb0bhnpy9lbh2qmhpbgmnxqzv") (l "tiledb")))

