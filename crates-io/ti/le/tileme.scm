(define-module (crates-io ti le tileme) #:use-module (crates-io))

(define-public crate-tileme-0.0.2 (c (n "tileme") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "hotkey") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winvd") (r "^0.0.20") (d #t) (t "cfg(windows)") (k 0)))) (h "12n1zxia5z92wqnv6q0l4wvybnff8x7a3r6k09w9s400b2xzch72")))

(define-public crate-tileme-0.0.3 (c (n "tileme") (v "0.0.3") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "hotkey") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "windef" "winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winvd") (r "^0.0.20") (d #t) (t "cfg(windows)") (k 0)))) (h "0x163493pg7jphp9kf3ji1nyq5nf1k1daafxqk6v27vp63k0v728")))

