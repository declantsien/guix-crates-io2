(define-module (crates-io ti le tiles7800) #:use-module (crates-io))

(define-public crate-tiles7800-0.2.0 (c (n "tiles7800") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "xml_dom") (r "^0.2") (d #t) (k 0)))) (h "06nqd1fcrg88s8hhx96lksm79z6ks2y7fb60j7q31v5y0swgy8zk")))

(define-public crate-tiles7800-0.3.0 (c (n "tiles7800") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "xml_dom") (r "^0.2") (d #t) (k 0)))) (h "0dam4vmfd9j6197vndx988j0rv3ira2ly7cv2gv137sl1glckns7")))

