(define-module (crates-io ti le tile_net) #:use-module (crates-io))

(define-public crate-tile_net-1.0.0 (c (n "tile_net") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "06vz6ik95807prk7n13mghy00mwyvdyah3zdlcdwfzq5c8ap2mh9") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.1.0 (c (n "tile_net") (v "1.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "1fxl6ahzanf37r2n3lvj6gnhll7bllh393x4vbznjlq32s129iqs") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.1.1 (c (n "tile_net") (v "1.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "0ggvq8vwzpv3i48360vzs0837mywhd8s51jgnlw9ql93hld4izcd") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.2.2 (c (n "tile_net") (v "1.2.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "1gjzz84gy5mmgx02prhpkfqxj76y275vn0x3cqi3s7c8n59n6kb4") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.2.1 (c (n "tile_net") (v "1.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "08vzm3lb4z7iid04ww5z0w1n8picigh0czmdzrp8q4mzyixjkpi6") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.2.0 (c (n "tile_net") (v "1.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "150n7ga6q5qhpcfq7pdb1vlkx8i6ax1z4kpiwjirlz6kc8349scr") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.2.3 (c (n "tile_net") (v "1.2.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11.2") (d #t) (k 2)))) (h "0l0xrwdz00hlnq7nlkdy1h6d30xp8g7dywb9cyfdj3zwmb8hin6s") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.3.1 (c (n "tile_net") (v "1.3.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "1ylzh4gpsmr2hvr05r9zi4aqvpwhagmv706s843i9k03xpvn9rcc") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.3.0 (c (n "tile_net") (v "1.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "0a7mfvb18qq7bz9fvmxzfnqz1ms5qdbfqwhlxmavcqpba3x3h42m") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.3.2 (c (n "tile_net") (v "1.3.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "1csfmkbid267z2158ddlm09wkb97mn057pdcb2b8h79hd4f1zb0i") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-1.3.3 (c (n "tile_net") (v "1.3.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "0zv7v2k77cpjrdf4ck2il9lkil8i3mjzjdn083z5zyhfgpp229a0") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.0 (c (n "tile_net") (v "2.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "128jzhkngh69id1jrf1kqgjwrw91ns1pb3vghwpmfqblv52pagfa") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.1 (c (n "tile_net") (v "2.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "0mkla8z88s3bkn3a0i2rcl77jcv84j9zgkqyqyf00n60wqqj5vnb") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.3 (c (n "tile_net") (v "2.0.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "1gyg9l63hdkqmjikly077cq2maglwkmn7wdvilbcpfm7yhc2gvm8") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.2 (c (n "tile_net") (v "2.0.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "0amdsafivdr75dxvlh57njlzzpi5c3y3702fkfm0b7sfs8fbg091") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.4 (c (n "tile_net") (v "2.0.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "01agms6mh697jgfndb1c78ibpwkcs4mk1fr731bzc3q3zmhj0fgm") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-tile_net-2.0.5 (c (n "tile_net") (v "2.0.5") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "interleave") (r "^1.0") (d #t) (k 0)) (d (n "sfml") (r "^0.11") (d #t) (k 2)))) (h "19cq2q1kgn0pbn1pjz3p3dgk2rmvf3g0d8nxkg14iza88v2553ih") (f (quote (("dev" "clippy") ("default"))))))

