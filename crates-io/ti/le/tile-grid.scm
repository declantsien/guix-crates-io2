(define-module (crates-io ti le tile-grid) #:use-module (crates-io))

(define-public crate-tile-grid-0.1.0 (c (n "tile-grid") (v "0.1.0") (h "1yh03misz9m7fz1arx6dcgsi736sxjshjqa9l8hfwg62spcwi0gb")))

(define-public crate-tile-grid-0.1.1 (c (n "tile-grid") (v "0.1.1") (h "0z4k1far99swyfd0aqicc3ns8k3nbs9d10vvlx301bsiqhsc4dsp")))

(define-public crate-tile-grid-0.2.0 (c (n "tile-grid") (v "0.2.0") (h "1j570q81zwicaqhvrx3fxida7nnh1qwfsg0syarmq9hzxngc5jzb")))

(define-public crate-tile-grid-0.2.1 (c (n "tile-grid") (v "0.2.1") (h "05qm57z8l8ca25a812aib0yrir1qayhy5rvd12k0p7h9m7qkimjz")))

(define-public crate-tile-grid-0.2.2 (c (n "tile-grid") (v "0.2.2") (h "0v4wm0mkxqzl5l7a2aqj13ilwa6d3acv6wwj8waf70lhpv005knj")))

(define-public crate-tile-grid-0.3.0 (c (n "tile-grid") (v "0.3.0") (h "0b2ciyvmccz1y9ijsymjfc2p6jscbh95k733gxqgxknanmjpx4w3")))

(define-public crate-tile-grid-0.4.0 (c (n "tile-grid") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "^0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1ykrpdwgd2d2wdqx1phn92f7xspqis733fni31ip53rppxyv6myl") (f (quote (("projtransform" "proj"))))))

(define-public crate-tile-grid-0.5.0 (c (n "tile-grid") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "^0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0v10vbhlwl5m46cjnrihrr6zxjvpwzrgxn73lp94zzz7d3mllr79") (f (quote (("projtransform" "proj"))))))

(define-public crate-tile-grid-0.5.1 (c (n "tile-grid") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "^0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "118n4w28ih9fjxv80vkcfn9g8y81acd97jvr6wilv3bdci7nwvqm") (f (quote (("projtransform" "proj"))))))

(define-public crate-tile-grid-0.5.2 (c (n "tile-grid") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "^0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0f90s0hvxcr0hsfnfzsb9vrq3bqmdimi1n8fhwpg99px93swwmwv") (f (quote (("projtransform" "proj"))))))

(define-public crate-tile-grid-0.6.0 (c (n "tile-grid") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ogcapi-types") (r "^0.1.0") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "=0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rhn5x1238cf33jgxmri24a7hbn80c7prc6qzanbgbqcdnlccyxs") (f (quote (("projtransform" "proj")))) (y #t) (r "1.65")))

(define-public crate-tile-grid-0.6.1 (c (n "tile-grid") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ogcapi-types") (r "^0.2.0") (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proj") (r "=0.27.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0bc4hilrg2v722i2cwycz3vqqnpp0n1jcjyfwbxq9kqjm2x3qv59") (f (quote (("projtransform" "proj")))) (r "1.65")))

