(define-module (crates-io ti le tiled_quad) #:use-module (crates-io))

(define-public crate-tiled_quad-0.1.0 (c (n "tiled_quad") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)) (d (n "tiled") (r "^0.10.2") (d #t) (k 0)))) (h "0r8xvx6ivs3lpnhaw45iqdcjp8x8vvpr3nz9j094l5y47milii6n")))

(define-public crate-tiled_quad-0.2.0 (c (n "tiled_quad") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3.24") (d #t) (k 0)) (d (n "tiled") (r "^0.10.2") (d #t) (k 0)))) (h "13ykny6f7mdylhlfqnsbnk427m2r132kxvknwshy76hdx0cpvr5z")))

