(define-module (crates-io ti le tilecoding) #:use-module (crates-io))

(define-public crate-tilecoding-0.1.0 (c (n "tilecoding") (v "0.1.0") (h "1dr9k9ww604qvgz1irc06nmdf2n8mg42mqnsix82pdaydj5xpgrq")))

(define-public crate-tilecoding-0.1.1 (c (n "tilecoding") (v "0.1.1") (h "0728y0ihb557cij61d0m1mzcz5r9ljlzn0pc3z3v9ppfa4fl38yl")))

(define-public crate-tilecoding-0.1.2 (c (n "tilecoding") (v "0.1.2") (h "1z23kbg5qma6ph8pxnmxsjs5qagsh6k3fs223dkjpys80899fa02")))

(define-public crate-tilecoding-0.2.0 (c (n "tilecoding") (v "0.2.0") (h "0h1fps5rs04hxfz88xrjb14886d83i7q96b5nz5payhblcyr9mjd")))

(define-public crate-tilecoding-0.3.0 (c (n "tilecoding") (v "0.3.0") (h "1sr6a6liaz0km2m5pbs37hmp61jwqai76r67vs74dr6m218j0ygq")))

