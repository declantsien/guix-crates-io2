(define-module (crates-io ti le tile-buffer) #:use-module (crates-io))

(define-public crate-tile-buffer-0.1.0 (c (n "tile-buffer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1g2rr2sjk98whcrvfq4j4pb0n67csv6j45n5bgd40876i15mym21")))

