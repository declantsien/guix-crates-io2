(define-module (crates-io ti le tiled-parser) #:use-module (crates-io))

(define-public crate-tiled-parser-0.1.0 (c (n "tiled-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dfc361qg3fddhk4l9f39xbavax8hgh2zl4qsv4rplqgb12ccb9d")))

(define-public crate-tiled-parser-0.1.1 (c (n "tiled-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "081calw9ykvanmq7dr4crvgz6z8p0y72g6f0f4hyqi98va9nmy0p")))

(define-public crate-tiled-parser-0.1.2 (c (n "tiled-parser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "098dilbr21fll970jjsqzaxxb7x176760ijan8qlr067bl0camsq")))

