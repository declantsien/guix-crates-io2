(define-module (crates-io ti le tilenet_ren) #:use-module (crates-io))

(define-public crate-tilenet_ren-0.1.0 (c (n "tilenet_ren") (v "0.1.0") (d (list (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "tile_net") (r "^1.2.3") (d #t) (k 0)))) (h "056707mh7h1jjrqlaarn2bynd558d5fnl00c2salbm1149y97wvd")))

(define-public crate-tilenet_ren-0.3.0 (c (n "tilenet_ren") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "tile_net") (r "^2") (d #t) (k 0)))) (h "06l28y78ssksc0mj2kkfcvdwfkzylsls95ra5lqznqd7rf6h3bsf") (f (quote (("dev" "clippy") ("default"))))))

