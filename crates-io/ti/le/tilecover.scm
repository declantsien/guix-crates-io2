(define-module (crates-io ti le tilecover) #:use-module (crates-io))

(define-public crate-tilecover-0.1.0 (c (n "tilecover") (v "0.1.0") (h "1c2dfs1qq2d4xlbypk6mkyz2zpp3d3jkm7mp4admv4rqy33q084m")))

(define-public crate-tilecover-0.1.1 (c (n "tilecover") (v "0.1.1") (d (list (d (n "geo") (r "^0.7.4") (d #t) (k 0)))) (h "0ygxv4715445sx78zclhdlbwhs2k0i4z1zym0bmljaf715j2zxls")))

(define-public crate-tilecover-0.2.0 (c (n "tilecover") (v "0.2.0") (d (list (d (n "geo") (r "^0.7.4") (d #t) (k 0)))) (h "1cg6h5402w6vr179fsly37x6r6wf5mpnx81jfrg3ay16ffxkwz13")))

(define-public crate-tilecover-0.3.0 (c (n "tilecover") (v "0.3.0") (d (list (d (n "geo") (r "^0.7.4") (d #t) (k 0)))) (h "1s57psdba3fbrwcbj5ygdihn2adk3zkjys3j9mjfaf2s7wwwkicw")))

(define-public crate-tilecover-0.4.0 (c (n "tilecover") (v "0.4.0") (d (list (d (n "geo") (r "^0.7.4") (d #t) (k 0)))) (h "0sprh68syavpjfl5yr6xyhv9xyv0z8136his2hx6zcnjwnggzwb7")))

(define-public crate-tilecover-0.4.1 (c (n "tilecover") (v "0.4.1") (d (list (d (n "geo") (r "^0.7.4") (d #t) (k 0)))) (h "1i889ncg1wr1xa7r8pxpymprxq5n36y9s22mm98n3plzinv6713i")))

(define-public crate-tilecover-1.0.0 (c (n "tilecover") (v "1.0.0") (d (list (d (n "geo") (r "^0.9.1") (d #t) (k 0)))) (h "0jbfmr7m5j7z075miw7ra8jg5n7nn74d0slwhhn9pn0q3hgd1wvm")))

(define-public crate-tilecover-1.0.1 (c (n "tilecover") (v "1.0.1") (d (list (d (n "geo") (r "^0.9.1") (d #t) (k 0)))) (h "04y9gn8bjrra2sp7hkw6yi5mq80pkk0b1chgx6hli7gjay7x2kjd")))

(define-public crate-tilecover-1.1.0 (c (n "tilecover") (v "1.1.0") (d (list (d (n "geo") (r "^0.12.0") (d #t) (k 0)))) (h "08c0a2kx1l5bzjzfniqsa3gwf4ii3nfcpl3zjbf9lm1smh2233yf")))

(define-public crate-tilecover-1.1.1 (c (n "tilecover") (v "1.1.1") (d (list (d (n "geo") (r "^0.12.0") (d #t) (k 0)))) (h "1lqs23cqqwfb7j7dc6jfs5j20zw28vbl0ybf35a88918rdr17mpl")))

(define-public crate-tilecover-2.0.0 (c (n "tilecover") (v "2.0.0") (d (list (d (n "geo") (r "^0.14.0") (d #t) (k 0)))) (h "0gdmyqhb7a7v8962yg2v87whg74zygqmgpzbpvyfrza2mlqgyksk")))

