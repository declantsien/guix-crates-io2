(define-module (crates-io ti ms timsort) #:use-module (crates-io))

(define-public crate-timsort-0.1.0 (c (n "timsort") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0z7a4jwwxpa73qpzl7p9lmf56lg2yzrbqidv49d1c21lrggsgjdl")))

(define-public crate-timsort-0.1.1 (c (n "timsort") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0w5dljq99wmdz2vqr7il9rc8z3ib7rlf5hbq5zdav5r9pxl560ws")))

(define-public crate-timsort-0.1.2 (c (n "timsort") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1f6zpr0mlx6zspx71q2i9rg67i8a7jzy8kwz473z3bbkpf1zmd1w")))

(define-public crate-timsort-0.1.3 (c (n "timsort") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "006w5flqyydv0f1n2qjcwj6xaf11p49xv51s73h6p99bdppyi733")))

