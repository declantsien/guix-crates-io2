(define-module (crates-io ti of tioft_rust_get_http) #:use-module (crates-io))

(define-public crate-tioft_rust_get_http-0.1.0 (c (n "tioft_rust_get_http") (v "0.1.0") (d (list (d (n "hyper") (r "^0.12.9") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3.0") (d #t) (k 0)))) (h "0x5fdl95f66v0ars18xjw2xk8xwsgvvp148pwppipw1sksbdrz0n") (y #t)))

