(define-module (crates-io ti d- tid-rs) #:use-module (crates-io))

(define-public crate-tid-rs-0.1.0 (c (n "tid-rs") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0bx136p15l1q2wj2c3pqb7k2vydc991m4nlc0hjqd9k0983kjkwn")))

(define-public crate-tid-rs-0.1.1 (c (n "tid-rs") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0zx70a2jx7dkdq2zq6qqa29sb7a8y8cvixk5acyrz12csgm2l9xz")))

