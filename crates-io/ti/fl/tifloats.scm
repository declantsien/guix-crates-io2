(define-module (crates-io ti fl tifloats) #:use-module (crates-io))

(define-public crate-tifloats-0.1.0 (c (n "tifloats") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0v1wly307360mdraixs2g9jf7y7ig3dgcyw9zw8qs1jpc4ics8in")))

(define-public crate-tifloats-0.2.0 (c (n "tifloats") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0d8il6ix0djqzzw4h1fvz9rviikb98ym0mc7bkknc35s1k9fjayc")))

(define-public crate-tifloats-0.2.1 (c (n "tifloats") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0mj4c66nywm8id2gw2l4j49fvc3149z5kslnkzzzp3ld4qwy38zz")))

(define-public crate-tifloats-1.0.0 (c (n "tifloats") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "07d4iky7i36x68fw98xhk78mnifdbvk5asrvl8zxgp2myngq8djp")))

(define-public crate-tifloats-2.0.0 (c (n "tifloats") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "18rjbw6gk4x1j8x803qvc01pyi262qh1ayda0cf6zp9hq83xm96d")))

(define-public crate-tifloats-2.1.0 (c (n "tifloats") (v "2.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "11ah2zlaxb6bwvy2s0lzm7rwclmr0il3z064y64pkibgmrg0laz1")))

(define-public crate-tifloats-2.1.1 (c (n "tifloats") (v "2.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "18dcb7m9d7y0c0bsy6slhbnr0ivbjpaw1n7phys7ivxc8cjspnm6")))

(define-public crate-tifloats-2.1.2 (c (n "tifloats") (v "2.1.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "02sghigj82h0gfyy0pmcab18252g1j303vsll8vbnxrgzlxs6537")))

(define-public crate-tifloats-2.2.0 (c (n "tifloats") (v "2.2.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1lrq8p8xh9g18bw663dfh4xwk0a38q1zhxyb04pisk7c7m0zv05v")))

