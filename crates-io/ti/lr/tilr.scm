(define-module (crates-io ti lr tilr) #:use-module (crates-io))

(define-public crate-tilr-0.4.3 (c (n "tilr") (v "0.4.3") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1cqb7y8p9gy3r0qb7wwz4zlvzgrcgb4qkdwr8540hgaga0940mdc")))

(define-public crate-tilr-0.4.4 (c (n "tilr") (v "0.4.4") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "044kp5bcrq1b9166fk6qx6axcgyvjjfjq4c88dqypikdbj4g3cy8")))

(define-public crate-tilr-0.5.0 (c (n "tilr") (v "0.5.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1g8yqjfim4b6jn8c2gwcfchr3im6csr4g067ack0ykl1zw1cy3sp")))

(define-public crate-tilr-0.5.1 (c (n "tilr") (v "0.5.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1ac74cbvvm2k1jpj5ar3d0myw6sfp6wa4rxqpspivcksx6k1hpb6")))

(define-public crate-tilr-0.5.2 (c (n "tilr") (v "0.5.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "07pf1x9n0aiql3sdm4g5knzdb16vgbkzw564k10723pyra8xs8w6")))

(define-public crate-tilr-0.5.3 (c (n "tilr") (v "0.5.3") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "0368035y3ah6b9d0r3aljccxqk6xhysi6v89s74samda8yvxkqk6")))

(define-public crate-tilr-0.5.4 (c (n "tilr") (v "0.5.4") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1hq21qr4qkmi8xk6l7qxkgygmkgm2navdqvsd6k3f4yflv1dd85k")))

(define-public crate-tilr-0.5.5 (c (n "tilr") (v "0.5.5") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1hm1s0dvchbkxchfk56cdczz2ivc5bc0m2hmzwwxzwffd6axxxac")))

(define-public crate-tilr-0.5.6 (c (n "tilr") (v "0.5.6") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "0m5hcwwzrbb7xa9dr22j4li568wqy7qvcvc17dck69yp0hbiqz2a")))

(define-public crate-tilr-0.5.9 (c (n "tilr") (v "0.5.9") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "0fpkdf1nznjivzvcrqsd9dyb10qmc4m5yqxx713vvs25sz516mfz")))

(define-public crate-tilr-0.5.10 (c (n "tilr") (v "0.5.10") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 0)))) (h "1c99kxzdwhrkdn9sv6s0am19ai7h6551sfm1nna97fpjx2c1v75j")))

