(define-module (crates-io ti er tiered-result) #:use-module (crates-io))

(define-public crate-tiered-result-0.1.0 (c (n "tiered-result") (v "0.1.0") (d (list (d (n "nullable-result") (r "^0.7.0") (f (quote ("try_trait"))) (k 0)))) (h "15g40mm6kcnx7b5m7azcww5fdm41wb0sgrfsrhpa546jmz27312d") (f (quote (("std" "nullable-result/std"))))))

