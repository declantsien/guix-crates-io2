(define-module (crates-io ti de tide-serve) #:use-module (crates-io))

(define-public crate-tide-serve-0.0.1 (c (n "tide-serve") (v "0.0.1") (d (list (d (n "async-std") (r "^1.6.3") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "09n5m5p0640k8dhvny9b6j82r76mznvqhcgf12pq14g53fbmcp9x")))

(define-public crate-tide-serve-0.0.2 (c (n "tide-serve") (v "0.0.2") (d (list (d (n "async-std") (r "^1.6.3") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.1") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "064wd161ampivzal8wg3fkr2af1pif267xpwg7fgvn6447zk68j1")))

(define-public crate-tide-serve-0.0.4 (c (n "tide-serve") (v "0.0.4") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.3") (d #t) (k 0)) (d (n "http-client") (r "^6.0.0") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.1.2") (d #t) (k 0)))) (h "1drhyd9plydpcg3lr8f3fv37s125yr38bxg4arha7lbbldwa3q9b")))

(define-public crate-tide-serve-0.0.5 (c (n "tide-serve") (v "0.0.5") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.4") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.1.3") (d #t) (k 0)))) (h "1j6vri2bkx7vmdf4cdprfs882j9mr61hdhiklswfpd957nv44aph")))

(define-public crate-tide-serve-0.0.6 (c (n "tide-serve") (v "0.0.6") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.5") (d #t) (k 0)) (d (n "http-client") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.1.4") (d #t) (k 0)))) (h "1xc16r5vwcqd4d70jf0kma9nsq6g5ag1y7wd7vs50jypnrbq5h43")))

(define-public crate-tide-serve-0.0.7 (c (n "tide-serve") (v "0.0.7") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.5") (d #t) (k 0)) (d (n "http-client") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.1.4") (d #t) (k 0)))) (h "1s0wbjvnr8fgz89smlahrq9yrzdmmaakbg80sg386s8vlg7w92ay")))

(define-public crate-tide-serve-0.0.8 (c (n "tide-serve") (v "0.0.8") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.5") (d #t) (k 0)) (d (n "http-client") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.1.6") (d #t) (k 0)))) (h "0msgaj404x4f9fyk28m3gls48fqan3mgm77d7qwrs75p5rihk807")))

(define-public crate-tide-serve-0.0.9 (c (n "tide-serve") (v "0.0.9") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.6") (d #t) (k 0)) (d (n "http-client") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.2.0") (d #t) (k 0)))) (h "1x588ml1p1j92h1iq5nwqrgmdxra11f095d30jphhi7n0r2gxdxi")))

(define-public crate-tide-serve-0.0.10 (c (n "tide-serve") (v "0.0.10") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.6") (d #t) (k 0)) (d (n "http-client") (r "^6.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.2.0") (d #t) (k 0)))) (h "0b42ppl6ib5iryal4xxwriar53agpcrzddqkjib8zlaici0i16sl")))

(define-public crate-tide-serve-0.0.11 (c (n "tide-serve") (v "0.0.11") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "driftwood") (r "^0.0.6") (d #t) (k 0)) (d (n "http-client") (r "^6.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-rustls") (r "^0.3.0") (d #t) (k 0)))) (h "0j3jl1xyn272zampnizwhs57mblksdva9rrfan8w93bhhn85if60")))

