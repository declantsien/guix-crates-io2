(define-module (crates-io ti de tide-openssl) #:use-module (crates-io))

(define-public crate-tide-openssl-0.1.0 (c (n "tide-openssl") (v "0.1.0") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.3.2") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std-openssl") (r "^0.6.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "093yvqzfrfcl9bg34f7yxqb1x4cmfkq65r06pawjcib4lzqfdjlz")))

(define-public crate-tide-openssl-0.1.1 (c (n "tide-openssl") (v "0.1.1") (d (list (d (n "async-dup") (r "^1.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.3") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "async-std-openssl") (r "^0.6.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)))) (h "0w8hmp15xr0rbqs25yxq57aa59rd1hmd68jvy3xd6qrphq1p58sw")))

