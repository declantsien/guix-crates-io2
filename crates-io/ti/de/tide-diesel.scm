(define-module (crates-io ti de tide-diesel) #:use-module (crates-io))

(define-public crate-tide-diesel-0.1.0 (c (n "tide-diesel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "extras"))) (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)))) (h "1m7qzlxli1sqhnf8r8kjsm97ykzdbcbga5bqd54jakfp7x628zv4")))

(define-public crate-tide-diesel-0.1.1 (c (n "tide-diesel") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "extras"))) (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)))) (h "0swvpv9m7h5j0c0l3hx52z58kji9pkjqjnljlb1ifikhbpnnzy6f")))

(define-public crate-tide-diesel-0.1.2 (c (n "tide-diesel") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "extras"))) (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)))) (h "1zcyzliambwz73x7aby00ml6lv45harnkyh21hj5xd0lzg2v8v51")))

