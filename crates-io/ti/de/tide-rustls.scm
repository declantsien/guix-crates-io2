(define-module (crates-io ti de tide-rustls) #:use-module (crates-io))

(define-public crate-tide-rustls-0.1.0 (c (n "tide-rustls") (v "0.1.0") (d (list (d (n "async-dup") (r "^1.2.1") (d #t) (k 0)) (d (n "async-h1") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-tls") (r "^0.9.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "tide") (r "^0.12.0") (d #t) (k 0)))) (h "0j7kk7cvmizja7qbqcn9c2rgm3l6a1084071xgnyhhjsvz4qgdnb")))

(define-public crate-tide-rustls-0.1.1 (c (n "tide-rustls") (v "0.1.1") (d (list (d (n "async-dup") (r "^1.2.1") (d #t) (k 0)) (d (n "async-h1") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-tls") (r "^0.9.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "tide") (r "^0.12.0") (d #t) (k 0)))) (h "0i8pr5fz6saqq5ws6nr2jdgplzmnmz26ay3gd241r7b8hzifhgqd")))

(define-public crate-tide-rustls-0.1.2 (c (n "tide-rustls") (v "0.1.2") (d (list (d (n "async-dup") (r "^1.2.1") (d #t) (k 0)) (d (n "async-h1") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-tls") (r "^0.9.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "0bpcpz5g7rqzhs70w5hkbi15rj66czy1nrdy3n0ykqxla6wb5fsp")))

(define-public crate-tide-rustls-0.1.3 (c (n "tide-rustls") (v "0.1.3") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.1.2") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-tls") (r "^0.10.0") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (k 0)))) (h "04w0m7h7046m1y2mfp3pf0krq22xi3q15ahs3pyi0rwg25qs07ax")))

(define-public crate-tide-rustls-0.1.4 (c (n "tide-rustls") (v "0.1.4") (d (list (d (n "async-dup") (r ">=1.2.2, <2.0.0") (d #t) (k 0)) (d (n "async-h1") (r ">=2.1.2, <3.0.0") (d #t) (k 0)) (d (n "async-std") (r ">=1.7.0, <2.0.0") (d #t) (k 0)) (d (n "async-tls") (r ">=0.10.0, <0.11.0") (d #t) (k 0)) (d (n "rustls") (r ">=0.18.1, <0.19.0") (d #t) (k 0)) (d (n "tide") (r ">=0.15.0, <0.16.0") (k 0)))) (h "0niriqiz8jhkly8mdr5h267lyxs1ycax5x9gr1jw9hv0rfkznd1b")))

(define-public crate-tide-rustls-0.1.5 (c (n "tide-rustls") (v "0.1.5") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.2.0") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "async-tls") (r "^0.10.2") (d #t) (k 0)) (d (n "rustls") (r "^0.19.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (k 0)))) (h "1xhczh5kwp46d64v89w0904ddydynyzny7g8scvmrvggim3k43l9")))

(define-public crate-tide-rustls-0.1.6 (c (n "tide-rustls") (v "0.1.6") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.2.0") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "async-tls") (r "^0.11.0") (d #t) (k 0)) (d (n "rustls") (r "^0.19.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (k 0)))) (h "1idn2bpl4f2mgfpzzjbk24ppa43wg1a6a9b2admbjfj68gnswbwb")))

(define-public crate-tide-rustls-0.2.0 (c (n "tide-rustls") (v "0.2.0") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.2.0") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "async-tls") (r "^0.11.0") (d #t) (k 0)) (d (n "rustls") (r "^0.19.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "08p0vg3510vl0dy71hrxhm7sdwhgc6fhrq97pgmz058jjqhqbgz3")))

(define-public crate-tide-rustls-0.3.0 (c (n "tide-rustls") (v "0.3.0") (d (list (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-h1") (r "^2.3.2") (d #t) (k 0)) (d (n "async-rustls") (r "^0.2.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "rustls") (r "^0.19.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "1dz6zmvg13y2y0nbh0lw3bvvjidkm17rsx5fjjkhp10inrlbb1bs")))

