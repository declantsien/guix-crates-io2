(define-module (crates-io ti de tide-jwt) #:use-module (crates-io))

(define-public crate-tide-jwt-0.1.0 (c (n "tide-jwt") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "0n8y6zq5y368jzi1vsir08d4pnmz5sdc8003vh9s6shr4vvi4p37")))

(define-public crate-tide-jwt-0.1.1 (c (n "tide-jwt") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "1mfd6fr6rmwagwh4hblm8ip1d7330rfhxypglqssq7w19wskvzkp")))

