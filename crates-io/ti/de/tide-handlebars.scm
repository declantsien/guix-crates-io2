(define-module (crates-io ti de tide-handlebars) #:use-module (crates-io))

(define-public crate-tide-handlebars-0.1.0 (c (n "tide-handlebars") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "1rjfx14kny6yfg9z0wlafca8pshgc1frxpssmywjza2xqa8hs0d0")))

(define-public crate-tide-handlebars-0.2.0 (c (n "tide-handlebars") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "17vh5mxpvxakmvp9hq2r1z82hmwi193fq59lfd6vidg6ibk3n6mm")))

(define-public crate-tide-handlebars-0.3.0 (c (n "tide-handlebars") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "tide") (r "^0.12.0") (d #t) (k 0)))) (h "1bpa3xb5bbm5vzsynpyv711g5xvxkrzhslcj7mk1mvdc6xfdvwp4")))

(define-public crate-tide-handlebars-0.5.0 (c (n "tide-handlebars") (v "0.5.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "06s67kl633p9crxvz0q788lwp6wbaxy6d5zl1kw8ihcmna78m4iy")))

(define-public crate-tide-handlebars-0.6.0 (c (n "tide-handlebars") (v "0.6.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 0)))) (h "1lpzvmfjsrslg4j1qxwrr43hmhckg9p05z8d3gnjk4nwckw5p6c3")))

(define-public crate-tide-handlebars-0.7.0 (c (n "tide-handlebars") (v "0.7.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.2.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "18bywskcwg01p0fxxhq9r9zlvi2q0k1nwq82mbcpsr9083vwslai")))

(define-public crate-tide-handlebars-0.8.0 (c (n "tide-handlebars") (v "0.8.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.5.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "0z0980fafsqa33hlpiaimhckqd5napm6ph9dm6ha74nvqhjqrxw5")))

(define-public crate-tide-handlebars-0.9.0 (c (n "tide-handlebars") (v "0.9.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "handlebars") (r "^3.5.2") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "http-types") (r "^2.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0rnfwn8pwq0c5xlndgmfxyrg324ldvgprhxl97ngp7dh323jxmxv")))

