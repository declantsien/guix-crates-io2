(define-module (crates-io ti de tide-fluent-routes) #:use-module (crates-io))

(define-public crate-tide-fluent-routes-0.1.0 (c (n "tide-fluent-routes") (v "0.1.0") (d (list (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1c3hjs138ijlanz25jhi7qjnrilfbzgn8rd0lxjncfdl1cg59dgh")))

(define-public crate-tide-fluent-routes-0.1.1 (c (n "tide-fluent-routes") (v "0.1.1") (d (list (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1hrf6lsg0dsg82znqmz941wh148n32xbl7kq8rniypd208msf06z")))

(define-public crate-tide-fluent-routes-0.1.2 (c (n "tide-fluent-routes") (v "0.1.2") (d (list (d (n "tide") (r "^0.14.0") (d #t) (k 0)))) (h "01qsxkvs2l00ljnxf5g0xnl42fszrayd7fsr0phcl8j6v8mfhhbj")))

(define-public crate-tide-fluent-routes-0.1.3 (c (n "tide-fluent-routes") (v "0.1.3") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 0)))) (h "09hvdwxkrii0slv0cvj2sq1pmbpdfx6kzc66injjkzfha772w6s4")))

(define-public crate-tide-fluent-routes-0.1.4 (c (n "tide-fluent-routes") (v "0.1.4") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "0l76vrnn2300dw40n5kwpddgvyc3ffk85i0qbad1pxn6f6w6m3qn")))

(define-public crate-tide-fluent-routes-0.1.5 (c (n "tide-fluent-routes") (v "0.1.5") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 0)))) (h "018sfiwcwzqkiybspbxyr2pfp87kn0930byqhkjijhh3ai62gps2")))

