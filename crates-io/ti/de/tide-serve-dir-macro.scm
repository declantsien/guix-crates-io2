(define-module (crates-io ti de tide-serve-dir-macro) #:use-module (crates-io))

(define-public crate-tide-serve-dir-macro-0.1.0 (c (n "tide-serve-dir-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0hjmyi4lf1nfnc216jpi2swcqgj3l0hhrclfcsg85h6a9qw581xm")))

(define-public crate-tide-serve-dir-macro-0.1.1 (c (n "tide-serve-dir-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0s1lk43hwx2dazl5k5avk3cxgzsddn4qp6l0wmr87j00ps1gshxs")))

(define-public crate-tide-serve-dir-macro-0.1.2 (c (n "tide-serve-dir-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "12vfik810b4nkdqvrqxhgi86hry3wp108hdjgc2fi1fn2inqfs85")))

