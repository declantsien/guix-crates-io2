(define-module (crates-io ti de tide-jsx) #:use-module (crates-io))

(define-public crate-tide-jsx-0.1.0 (c (n "tide-jsx") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide-jsx-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0aaiq0x93j0a41hrslysxzmnnkxb7ygg0iizn17y02mpzkwb8k53")))

(define-public crate-tide-jsx-0.2.0 (c (n "tide-jsx") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide-jsx-impl") (r "^0.1.0") (d #t) (k 0)))) (h "01782nbhzf60nkaaa3880k882k5pq7wwnk0shiqdbyv50bfw1cyx")))

(define-public crate-tide-jsx-0.2.1 (c (n "tide-jsx") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide-jsx-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0rr7mzip5wd18q48xz4s5l6rl965b822w0gww0296j5fhl3x61lb")))

(define-public crate-tide-jsx-0.3.0 (c (n "tide-jsx") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-jsx-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14mna6h0kpyglpk05zcfw0iai9v5j5bdfwmg47grdwm6vkdapyda")))

(define-public crate-tide-jsx-0.3.1 (c (n "tide-jsx") (v "0.3.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-jsx-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19mkxc7q6hfkp30agbxb0if6l55jdasy5fsx5664kic2vb27h8qb")))

(define-public crate-tide-jsx-0.3.2 (c (n "tide-jsx") (v "0.3.2") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-jsx-impl") (r "^0.2.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08aws7s13s39737ps2b8kj7nnr2734xlwgf2qx0xlgssqhcfdgmx")))

(define-public crate-tide-jsx-0.3.3 (c (n "tide-jsx") (v "0.3.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-jsx-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1i4wv9ygq4z4zxlv7k59wfa0m18g879yama07n930qjnhyz7bxam")))

(define-public crate-tide-jsx-0.4.0 (c (n "tide-jsx") (v "0.4.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)) (d (n "tide-jsx-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1mszfx09dhb4h92b0fr7zdwybbrjm7mqf3rbc36hakzhg8l0caj7")))

