(define-module (crates-io ti de tide-content-type-filter) #:use-module (crates-io))

(define-public crate-tide-content-type-filter-0.1.0 (c (n "tide-content-type-filter") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "tide-testing") (r "^0.1.3") (d #t) (k 2)))) (h "1272phy0jbv5z9bckfrvym39g3lsc7vwlf2x6fz6nywiv1v68p08")))

(define-public crate-tide-content-type-filter-0.2.0 (c (n "tide-content-type-filter") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "tide-testing") (r "^0.1.3") (d #t) (k 2)))) (h "0y779zdgjmm7g5ma2wykcy13mnv8wl8vvbprxj6dpiqqh1i26p0w")))

(define-public crate-tide-content-type-filter-0.3.0 (c (n "tide-content-type-filter") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "tide-testing") (r "^0.1.3") (d #t) (k 2)))) (h "1q63a9mc2nyap8rvwakfdq6kgpmyyrjslp5a180qcibqira7v79q")))

