(define-module (crates-io ti de tide-auth) #:use-module (crates-io))

(define-public crate-tide-auth-0.1.0 (c (n "tide-auth") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "http-types") (r "^2.10") (d #t) (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 0)))) (h "0d9xgwzyxpnwlq63gwibwxc2jkpgi4lqyvrs86yr9w8aav340qih")))

