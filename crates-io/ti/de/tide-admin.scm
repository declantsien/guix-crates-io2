(define-module (crates-io ti de tide-admin) #:use-module (crates-io))

(define-public crate-tide-admin-0.1.0 (c (n "tide-admin") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.4.2") (f (quote ("postgres" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (f (quote ("builtins"))) (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)) (d (n "tide-tera") (r "^0.2.3") (d #t) (k 0)))) (h "0q4m35i2630klhj8v349qhrzm45v69ghdxsbxvgwc43rjyg7vrk3")))

