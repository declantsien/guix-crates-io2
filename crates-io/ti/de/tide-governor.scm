(define-module (crates-io ti de tide-governor) #:use-module (crates-io))

(define-public crate-tide-governor-1.0.0 (c (n "tide-governor") (v "1.0.0") (d (list (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "1dnr717vyvph0ad7ancblkk0v941hg3lzyji5kmz9s2a9rngpdhp")))

(define-public crate-tide-governor-1.0.1 (c (n "tide-governor") (v "1.0.1") (d (list (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "19900kgg2ygn7rpzsm5gghhp03v0vpp4d8r86m09pzanfvf25yjn")))

(define-public crate-tide-governor-1.0.2 (c (n "tide-governor") (v "1.0.2") (d (list (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "0aapx0zs3pijn167x6vnpwcdh8jb55h9j33sizbg4asl5avj24gx")))

(define-public crate-tide-governor-1.0.3 (c (n "tide-governor") (v "1.0.3") (d (list (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0yvfrf8222biyb8c6dxhchp5c65rsdxalqlnnqhmwvbald0diklv")))

