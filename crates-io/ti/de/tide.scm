(define-module (crates-io ti de tide) #:use-module (crates-io))

(define-public crate-tide-0.0.0 (c (n "tide") (v "0.0.0") (h "0k8slcc3pflg9cryr1v2wlcaclfzxvw802h3qps7c4qhh0rq8p5i")))

(define-public crate-tide-0.0.1 (c (n "tide") (v "0.0.1") (d (list (d (n "basic-cookies") (r "^0.1.2") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.12") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.0") (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "path-table") (r "^1.0.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0kyngfxyhr3mj59q8dnmad3583df0hv273p7yfrzgdqp5jakmpgd")))

(define-public crate-tide-0.0.3 (c (n "tide") (v "0.0.3") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.12") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.0") (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "path-table") (r "^1.0.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0waqp3dfsrs1iij7jpgk846p9phbw7pi6kncxbcc76akflm2wiiv")))

(define-public crate-tide-0.0.4 (c (n "tide") (v "0.0.4") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.12") (f (quote ("compat"))) (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12.0") (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "path-table") (r "^1.0.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0fn8p8z6qlxcw5d68rrajscdpd6j8xrjz4l53j75wi5iifwf8hnm")))

(define-public crate-tide-0.0.5 (c (n "tide") (v "0.0.5") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.1.4") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "path-table") (r "^1.0.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1gcy04afprs8g9blm7sghz5kdwbbfanrw39gvli6j6gi55dacf8k") (f (quote (("hyper" "http-service-hyper") ("default" "hyper"))))))

(define-public crate-tide-0.1.0 (c (n "tide") (v "0.1.0") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.1.4") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.1.0") (d #t) (k 2)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.1") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0k78qchnv7ikhhw0gizqm12lx3rpnjrpz2kg45jwiw2595xxhqg8") (f (quote (("hyper" "http-service-hyper") ("default" "hyper"))))))

(define-public crate-tide-0.1.1 (c (n "tide") (v "0.1.1") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.1.5") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.1.1") (d #t) (k 2)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4.5") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1lf1331hhqa41a1068cl5n1ysgdhv5l3987393drim2nhl7kv9ka") (f (quote (("hyper" "http-service-hyper") ("default" "hyper"))))))

(define-public crate-tide-0.2.0 (c (n "tide") (v "0.2.0") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "http-service") (r "^0.2.0") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.2.0") (d #t) (k 2)) (d (n "juniper") (r "^0.10.0") (d #t) (k 2)) (d (n "multipart") (r "^0.15.3") (f (quote ("server"))) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.5") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1isnpsw0wpfn0d5ip86iwragcbjlpj9bf5dvvz68n3zaz5sbgbbd") (f (quote (("hyper" "http-service-hyper") ("default" "hyper"))))))

(define-public crate-tide-0.3.0 (c (n "tide") (v "0.3.0") (d (list (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "cookie") (r "^0.12.0") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "http-service") (r "^0.3.1") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.3.1") (d #t) (k 2)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "multipart") (r "^0.16.1") (f (quote ("server"))) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.6.1") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "04qzicq7dzb0b6d9c5xg7wir2j54hrx1srvdarhz6zynndian8w0") (f (quote (("unstable") ("hyper" "http-service-hyper") ("default" "hyper"))))))

(define-public crate-tide-0.4.0 (c (n "tide") (v "0.4.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.4.0") (d #t) (k 2)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^1.0.3") (d #t) (k 2)))) (h "02mvadrhsqrjc9f64v2g1nsjcr3767apypkv6pv5fw7va329kp07") (f (quote (("unstable") ("hyper-server" "http-service-hyper") ("default" "hyper-server"))))))

(define-public crate-tide-0.5.0 (c (n "tide") (v "0.5.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.0.1") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.4.0") (d #t) (k 2)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^1.0.3") (d #t) (k 2)))) (h "0qjgxj1105shg9g6p1yzj22hpqy36c64a8vsdx8w7lpgh3ahmrvj") (f (quote (("unstable") ("hyper-server" "http-service-hyper") ("docs" "unstable") ("default" "hyper-server"))))))

(define-public crate-tide-0.5.1 (c (n "tide") (v "0.5.1") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.0.1") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.4.0") (d #t) (k 2)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^1.0.3") (d #t) (k 2)))) (h "1shfmysnz0khxgwf2mgpqlrsx2gcgyqd2k31l88yd0fvj4crpj8k") (f (quote (("unstable") ("hyper-server" "http-service-hyper") ("docs" "unstable") ("default" "hyper-server"))))))

(define-public crate-tide-0.6.0 (c (n "tide") (v "0.6.0") (d (list (d (n "async-std") (r "^1.0.1") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.0.1") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "cookie") (r "^0.12.0") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.1.19") (d #t) (k 0)) (d (n "http-service") (r "^0.4.0") (d #t) (k 0)) (d (n "http-service-hyper") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.4.0") (d #t) (k 2)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^1.0.3") (d #t) (k 2)))) (h "0fdply7b4576xv9m2kh69xqgzyzl9zngw3ixf097j45f928cj6g6") (f (quote (("unstable") ("hyper-server" "http-service-hyper") ("docs" "unstable") ("default" "hyper-server"))))))

(define-public crate-tide-0.7.0 (c (n "tide") (v "0.7.0") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "cookie") (r "^0.12.0") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http-service") (r "^0.5.0") (d #t) (k 0)) (d (n "http-service-h1") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.5.0") (d #t) (k 2)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^2.0.0-alpha.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "05ci1ir3ga1zbyqma5myl52znrzbm8357nk3l9246vspy1rfmzfs") (f (quote (("unstable") ("h1-server" "http-service-h1") ("docs" "unstable") ("default" "h1-server"))))))

(define-public crate-tide-0.8.0 (c (n "tide") (v "0.8.0") (d (list (d (n "async-sse") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "cookie") (r "^0.12.0") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "femme") (r "^1.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http-service") (r "^0.5.0") (d #t) (k 0)) (d (n "http-service-h1") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.5.0") (d #t) (k 2)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^2.0.0-alpha.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1fli3pqq3g1bffrgc90pz96mkczsw5b6z9igkkwq6s2kp0zkhl25") (f (quote (("unstable") ("h1-server" "http-service-h1") ("docs" "unstable") ("default" "h1-server"))))))

(define-public crate-tide-0.8.1 (c (n "tide") (v "0.8.1") (d (list (d (n "async-sse") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "basic-cookies") (r "^0.1.3") (d #t) (k 2)) (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "cookie") (r "^0.12.0") (f (quote ("percent-encode"))) (d #t) (k 0)) (d (n "femme") (r "^1.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.0") (f (quote ("compat"))) (d #t) (k 2)) (d (n "http") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "http-service") (r "^0.5.0") (d #t) (k 0)) (d (n "http-service-h1") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "http-service-mock") (r "^0.5.0") (d #t) (k 2)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 2)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.1") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 2)) (d (n "surf") (r "^2.0.0-alpha.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "169aj7klsznysml1jxpwc8m2cnkrfyvd1n1b02c18xiwzvr5m5fg") (f (quote (("unstable") ("h1-server" "http-service-h1") ("docs" "unstable") ("default" "h1-server"))))))

(define-public crate-tide-0.9.0 (c (n "tide") (v "0.9.0") (d (list (d (n "async-h1") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^2.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "femme") (r "^2.0.1") (d #t) (k 0)) (d (n "http-types") (r "^2.0.0") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.3") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_qs") (r "^0.5.0") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.2") (f (quote ("h1-client"))) (k 2)))) (h "05d5qylbb5i739r0qvm2n56fd2vf7iz510ixc6q4s23gjckhqqp9") (f (quote (("unstable") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server") ("__internal__bench"))))))

(define-public crate-tide-0.10.0 (c (n "tide") (v "0.10.0") (d (list (d (n "async-h1") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^3.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "femme") (r "^2.0.1") (d #t) (k 0)) (d (n "http-types") (r "^2.0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.3") (f (quote ("h1-client"))) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1364k3gl1ylgqh9p0yvs78vrl41rg1l8isk3bxkc1vshcrv2g1mc") (f (quote (("unstable") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server") ("__internal__bench"))))))

(define-public crate-tide-0.11.0 (c (n "tide") (v "0.11.0") (d (list (d (n "async-h1") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^3.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "femme") (r "^2.0.1") (d #t) (k 0)) (d (n "http-types") (r "^2.0.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.3") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0hdminldgfm53vzznlifplrjazkzibds0pkijcpjq5d9d7rhf6qm") (f (quote (("unstable") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server") ("__internal__bench"))))))

(define-public crate-tide-0.12.0 (c (n "tide") (v "0.12.0") (d (list (d (n "async-h1") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "femme") (r "^2.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http-types") (r "^2.2.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.3") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1w1zjvwyfx28zkf9wjgj4xrwa1qw5qpyb3ak61dsg80zv1zhpl9l") (f (quote (("unstable") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server") ("__internal__bench"))))))

(define-public crate-tide-0.13.0 (c (n "tide") (v "0.13.0") (d (list (d (n "async-h1") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "femme") (r "^2.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "http-types") (r "^2.2.1") (d #t) (k 0)) (d (n "juniper") (r "^0.14.1") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.3") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1yf8sc8psmgk09081alddwymd3k43m76x6xmrc4fv2asbziw9i5p") (f (quote (("unstable") ("sessions" "async-session") ("logger") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "logger" "sessions") ("__internal__bench"))))))

(define-public crate-tide-0.14.0 (c (n "tide") (v "0.14.0") (d (list (d (n "async-h1") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "femme") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (k 0)) (d (n "http-types") (r "^2.5.0") (d #t) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.10") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0dh3h0sid62zq68mbskw443xyv0kf2lip2dj8is9l8g7cllsjyj5") (f (quote (("unstable") ("sessions" "async-session") ("logger" "femme") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "logger" "sessions") ("__internal__bench"))))))

(define-public crate-tide-0.15.0 (c (n "tide") (v "0.15.0") (d (list (d (n "async-h1") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "femme") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (k 0)) (d (n "http-types") (r "^2.5.0") (d #t) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.10") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0d7p0zzk52v970s3imy0xn6rfxivksq012bcdd2gci86k84wlcn8") (f (quote (("unstable") ("sessions" "async-session" "cookies") ("logger" "femme") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "cookies" "logger" "sessions") ("cookies"))))))

(define-public crate-tide-0.15.1 (c (n "tide") (v "0.15.1") (d (list (d (n "async-h1") (r "^2.1.2") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "femme") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (k 0)) (d (n "http-types") (r "^2.5.0") (d #t) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (f (quote ("kv_unstable_std"))) (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.10") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0sz66hlzwwcz6zmpi791h9q6bd4l766dzrv7gbmir7bvgqz7b2c8") (f (quote (("unstable") ("sessions" "async-session" "cookies") ("logger" "femme") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "cookies" "logger" "sessions") ("cookies"))))))

(define-public crate-tide-0.16.0 (c (n "tide") (v "0.16.0") (d (list (d (n "async-h1") (r "^2.3.0") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "femme") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (k 0)) (d (n "http-types") (r "^2.10.0") (f (quote ("fs"))) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (f (quote ("kv_unstable_std"))) (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1q6yl5wkmy8csbg6024mxvw5m1zai5sgaiwhad5p7k6j1lzmfnf4") (f (quote (("unstable") ("sessions" "async-session" "cookies") ("logger" "femme") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "cookies" "logger" "sessions") ("cookies" "http-types/cookies"))))))

(define-public crate-tide-0.17.0-beta.1 (c (n "tide") (v "0.17.0-beta.1") (d (list (d (n "async-h1") (r "^2.3.0") (o #t) (d #t) (k 0)) (d (n "async-session") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "async-sse") (r "^4.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "femme") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "http-client") (r "^6.1.0") (k 0)) (d (n "http-types") (r "^2.11.0") (f (quote ("fs"))) (k 0)) (d (n "juniper") (r "^0.14.2") (d #t) (k 2)) (d (n "kv-log-macro") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.13") (f (quote ("kv_unstable_std"))) (d #t) (k 0)) (d (n "logtest") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "portpicker") (r "^0.1.0") (d #t) (k 2)) (d (n "routefinder") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (f (quote ("h1-client"))) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0lddqvpva646pm5pzwv8kbj6bzpxwdky273kg9h9vbv6xgxqba65") (f (quote (("unstable") ("sessions" "async-session" "cookies") ("logger" "femme") ("h1-server" "async-h1") ("docs" "unstable") ("default" "h1-server" "cookies" "logger" "sessions") ("cookies" "http-types/cookies"))))))

