(define-module (crates-io ti de tide-prometheus) #:use-module (crates-io))

(define-public crate-tide-prometheus-0.1.0 (c (n "tide-prometheus") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "tide-testing") (r "^0.1.3") (d #t) (k 2)))) (h "09qc19m3hldscq9zz8a0q55prg6f873p9zzlz2ddyyndzpbsmncr") (f (quote (("process" "prometheus/process") ("default"))))))

