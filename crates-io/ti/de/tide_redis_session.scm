(define-module (crates-io ti de tide_redis_session) #:use-module (crates-io))

(define-public crate-tide_redis_session-0.1.0 (c (n "tide_redis_session") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "redis") (r "^0.22.0") (f (quote ("aio" "async-std-comp"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1cgiyi3ylbdb53g27la6kjrjfr33l9jn84vqi9byk31anjbr0q7k")))

