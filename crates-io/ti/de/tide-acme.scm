(define-module (crates-io ti de tide-acme) #:use-module (crates-io))

(define-public crate-tide-acme-0.1.0 (c (n "tide-acme") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.1.4") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 2)) (d (n "tide-rustls") (r "^0.3.0") (d #t) (k 0)))) (h "05bj1njzs12gjcxy05la6m36krj42xjbjdqcg6rbq2q9qsbw9k3z")))

(define-public crate-tide-acme-0.1.1 (c (n "tide-acme") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.1.6") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 2)) (d (n "tide-rustls") (r "^0.3.0") (d #t) (k 0)))) (h "1ky89djs6y9zvsfcdx8fga30qmcxpgm34zlnfnwzi98iad464qzq")))

(define-public crate-tide-acme-0.2.0-beta.1 (c (n "tide-acme") (v "0.2.0-beta.1") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rustls-acme") (r "=0.3.0-beta.4") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 2)) (d (n "tide-rustls") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (k 0)))) (h "01bwmhnj2i60pflxf127mznhf45qysq3cary0na10fl5j1km1fzl") (y #t)))

(define-public crate-tide-acme-0.2.0 (c (n "tide-acme") (v "0.2.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rustls-acme") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 2)) (d (n "tide-rustls") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (k 0)))) (h "0rkyn7ni69hsdrcvqfis0hy3pjqdxs0amixysh7i69j0ram307ry")))

