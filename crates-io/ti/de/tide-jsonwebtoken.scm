(define-module (crates-io ti de tide-jsonwebtoken) #:use-module (crates-io))

(define-public crate-tide-jsonwebtoken-0.1.0 (c (n "tide-jsonwebtoken") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 2)))) (h "0gda4g66lc1gni0996gybxw8pfwxypyhy7j5854cxjg97sxs1yl2")))

(define-public crate-tide-jsonwebtoken-0.1.1 (c (n "tide-jsonwebtoken") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.16") (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 2)))) (h "11s23dvradn0sjqzs64q4pl0jw8kq5zvnv64wp8jknr24rd5il3j")))

