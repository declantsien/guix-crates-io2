(define-module (crates-io ti de tide-delay) #:use-module (crates-io))

(define-public crate-tide-delay-0.0.1 (c (n "tide-delay") (v "0.0.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.15") (d #t) (k 0)))) (h "1691911kqa39dya1iibk0832q9qsm1214ka0q14g9xp5kr1vy6xp")))

