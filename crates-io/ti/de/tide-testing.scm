(define-module (crates-io ti de tide-testing) #:use-module (crates-io))

(define-public crate-tide-testing-0.1.0 (c (n "tide-testing") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (k 0)) (d (n "tide") (r "^0.14.0") (k 0)))) (h "1ljpwdkqy656y8fxjxy813y1mg9xsgkya9ymj2v4rp8rr0xb0l91")))

(define-public crate-tide-testing-0.1.1 (c (n "tide-testing") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (k 0)) (d (n "tide") (r "^0.14.0") (k 0)))) (h "0hbwcqwl8g8s7ddbxq95hbppifikszvxj1h2kmml9jsmfhwbrcah")))

(define-public crate-tide-testing-0.1.2 (c (n "tide-testing") (v "0.1.2") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (k 0)) (d (n "tide") (r "^0.15.0") (k 0)))) (h "06l86ljj8zqsgqf9i06d81074v26x8wqifbvl659rj6j0xc78l6q")))

(define-public crate-tide-testing-0.1.3 (c (n "tide-testing") (v "0.1.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "04ai94rbjf4vv4r134vdfb0h11n7rxfq5xrw2zj0bln6vqrylnas")))

