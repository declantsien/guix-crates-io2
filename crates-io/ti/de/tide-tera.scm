(define-module (crates-io ti de tide-tera) #:use-module (crates-io))

(define-public crate-tide-tera-0.1.0 (c (n "tide-tera") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.3.1") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "056afhffiz6mr8q2qlw6qjryjih1rmnsgi0d5iqcmh126djw0z4k")))

(define-public crate-tide-tera-0.1.1 (c (n "tide-tera") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.3.1") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "0hx8y9aadx3dx7ag296c5n3yvr2vxw0r9wa3gdcxdbmm3pmwmrsr")))

(define-public crate-tide-tera-0.2.0 (c (n "tide-tera") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "0afl8yb4cg3f1d95xk1f23ml2k3aibav6hz5lsi84x3ffr8ggpyp")))

(define-public crate-tide-tera-0.2.1 (c (n "tide-tera") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 0)))) (h "1wichxmw0aiqbnvc17zmvzv2r8iyk4xl4msdfxxldf48fxiqgxvb")))

(define-public crate-tide-tera-0.2.2 (c (n "tide-tera") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "tide") (r "^0.14.0") (k 0)) (d (n "tide") (r "^0.14.0") (d #t) (k 2)))) (h "18nhjwsx43qiy4396dnvrj4shn23adc1szqhfpwdqldsiiy25zf0")))

(define-public crate-tide-tera-0.2.3 (c (n "tide-tera") (v "0.2.3") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 2)))) (h "1h3bbjrv2yq76x78sd004h09xq67p93xzhjikxnapw96a3jiwi2q")))

(define-public crate-tide-tera-0.2.4 (c (n "tide-tera") (v "0.2.4") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 2)))) (h "0lyggcf5hqwj6msbvidzfw3naafdlyqaf04bxw20wcvm5c1k1v0j")))

