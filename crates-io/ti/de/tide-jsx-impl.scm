(define-module (crates-io ti de tide-jsx-impl) #:use-module (crates-io))

(define-public crate-tide-jsx-impl-0.1.0 (c (n "tide-jsx-impl") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00b8xd96kv58kacg06zn260raq86razdskhx3yf3ld7rq25bq2bz")))

(define-public crate-tide-jsx-impl-0.2.0 (c (n "tide-jsx-impl") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8b6ffj9hf5pqx2ss3qha2zlqxrr0nnn396a20y0087jpb6ki0j")))

(define-public crate-tide-jsx-impl-0.2.1 (c (n "tide-jsx-impl") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05ippn6j7jak7a2zijbgq13vx0wmyvylxfvkj446f0wh1giwfyka")))

(define-public crate-tide-jsx-impl-0.2.2 (c (n "tide-jsx-impl") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13y62s34ig73yaj7vak37klk186y5plyx8n27rm0dwmf13ygybqw")))

(define-public crate-tide-jsx-impl-0.3.0 (c (n "tide-jsx-impl") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fkjk0g4ms03ak6wlw9nygs3wpgfzys3phiypkdgs0p66rf52syp")))

