(define-module (crates-io ti de tide-etag) #:use-module (crates-io))

(define-public crate-tide-etag-0.1.0 (c (n "tide-etag") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lz_fnv") (r "^0.1.2") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (k 0)))) (h "0wxn9y5sqziyvqh5l571c4wlm4pya48idk3jg57fy3mb8mfsrpq5")))

