(define-module (crates-io ti de tide-websockets-ext) #:use-module (crates-io))

(define-public crate-tide-websockets-ext-0.1.0 (c (n "tide-websockets-ext") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http-types") (r "^2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tide") (r "^0.16") (d #t) (k 2)) (d (n "tide-websockets") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0b7a9n385g40hddsza91b0lb1g3wxmj3wlrkh8lj89b7ikvra0hd")))

