(define-module (crates-io ti br tibrv-sys) #:use-module (crates-io))

(define-public crate-tibrv-sys-0.1.0 (c (n "tibrv-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0fc4cipsqs4q20bfhvhz60x8s3x77riyqsbv1mpb9svdangxdzfx")))

(define-public crate-tibrv-sys-0.2.0 (c (n "tibrv-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1r5vj53wl3zxklbhlw3gxw39y61w7rdihhrxblyqdzpjmd72fsc2")))

(define-public crate-tibrv-sys-0.3.0 (c (n "tibrv-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0hp06kjm6gma5k20wv9nfhc8lchdwxxx5hbg5wwmgyl4jr7kmdp5") (l "tibrv")))

(define-public crate-tibrv-sys-0.4.0 (c (n "tibrv-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "09hr9a83078d8z9kfmv42xl908iry46x8ih15nhgszhisvainadf") (f (quote (("tibrv_8_4" "tibrv_8_3") ("tibrv_8_3" "tibrv_8_2") ("tibrv_8_2")))) (l "tibrv")))

(define-public crate-tibrv-sys-0.5.0 (c (n "tibrv-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0lf9knc8s1xrzyijqw0qc56wg4kq8ki4iy9nwx8gvvfs35m07rx2") (f (quote (("tibrv_8_4" "tibrv_8_3") ("tibrv_8_3" "tibrv_8_2") ("tibrv_8_2")))) (l "tibrv")))

(define-public crate-tibrv-sys-0.6.0 (c (n "tibrv-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "116vxkpyvypnnjcfxgkvnipjj8c2a5yn5hlk8df0kwry9rdf92xh") (f (quote (("tibrv_8_4" "tibrv_8_3") ("tibrv_8_3" "tibrv_8_2") ("tibrv_8_2")))) (l "tibrv")))

