(define-module (crates-io ti nd tindi) #:use-module (crates-io))

(define-public crate-tindi-0.1.0 (c (n "tindi") (v "0.1.0") (h "1h1w7wl9dg9fjf1cbggcb0rw17lax5gqr3r0p7fl25dqngqcfx7h")))

(define-public crate-tindi-0.2.0 (c (n "tindi") (v "0.2.0") (h "0gqk8z7ds9g8gssw5c1dhnykj3hsf4rx8prrsidn8iwlm1616x9s")))

(define-public crate-tindi-0.3.0 (c (n "tindi") (v "0.3.0") (h "13bgzq5kv4622cw74h2p82fww453fx3j6k06xsj2hm8x1b1lz570")))

(define-public crate-tindi-0.4.0 (c (n "tindi") (v "0.4.0") (h "13nnnfn73dclxyv35pkwyj3fvjin5dy01zkz8zkh984jb51al93x")))

(define-public crate-tindi-0.4.1 (c (n "tindi") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "15hwzz3hy7hlfp0dp203byafh6kn3cakvg124pk5s79hxx8ifghm")))

(define-public crate-tindi-0.5.0 (c (n "tindi") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x48c4dkzmr8lcw3xpk17ah5vwny63ni5ckak84pdih7scz06dgi")))

(define-public crate-tindi-0.5.1 (c (n "tindi") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2csyzilr6i14invqv1wqrk3bkfnwn3yync7bwyq7irsqvc3pw2")))

(define-public crate-tindi-0.5.2 (c (n "tindi") (v "0.5.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gbp651l2svkvm527s6lf3kdd2n4gi33ys4g3krvbs36rmr9pfqw")))

(define-public crate-tindi-0.6.0 (c (n "tindi") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m93kv3g4j29cnm25yhh4gy8sdb9c8lmlhjfkkalddgn40fxy25f")))

(define-public crate-tindi-0.6.1 (c (n "tindi") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1837gpjcmb8316v2idpkz2fq9qxy0kg1iq5frcp0a5qsfidn25xh")))

(define-public crate-tindi-0.6.2 (c (n "tindi") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "00hdq2xv41330k7j9gwdf6qlmalqbs2k9b6zrkrmdcwn1dnjv1sn")))

(define-public crate-tindi-0.7.0 (c (n "tindi") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "16xi2vfcmhrs652wgyxysqk9i24wd67hj7gsw5qrcypz91mhs4da")))

