(define-module (crates-io ti nd tindex) #:use-module (crates-io))

(define-public crate-tindex-0.3.0 (c (n "tindex") (v "0.3.0") (h "0kk8wiypa50ycmrmj20vkgw2z94m0cx61qh1v3jyhn9zbjihkba7")))

(define-public crate-tindex-0.3.1 (c (n "tindex") (v "0.3.1") (h "1yryvj1yj59yw25dz5srawyr8ibkfimlg8sq253iswxgwzkf5f39")))

(define-public crate-tindex-0.4.0 (c (n "tindex") (v "0.4.0") (h "0fpqwb18vg0jl6075dfivq9vnyml8s11zm68y0lgxzl7kahc6f8c")))

(define-public crate-tindex-0.5.0 (c (n "tindex") (v "0.5.0") (h "0k5db159qkmrvfr05zf69c9jw36qvbq0r94qc0imx1xzsflz5v7k")))

(define-public crate-tindex-0.5.1 (c (n "tindex") (v "0.5.1") (h "04yf418v3aqz76qhb44mvnv1cl53qsrpn046j1diypw214lfyxhr")))

(define-public crate-tindex-0.5.2 (c (n "tindex") (v "0.5.2") (h "027r6cgmxk0zcbz71k08rz7h4v2ji9aa2gmjzds122im99pk0zkw")))

(define-public crate-tindex-0.5.3 (c (n "tindex") (v "0.5.3") (h "1vdqajm3596fai72b7irk864iqqb7z5x85b7w5amrl2v0fa8lkqm")))

(define-public crate-tindex-0.5.4 (c (n "tindex") (v "0.5.4") (h "11n554hxvlvik0mhybdvnwcgyhb424zl7ibv8s6s6scnmgbfzab6")))

(define-public crate-tindex-0.5.5 (c (n "tindex") (v "0.5.5") (h "119iks3bc8mqcgb4flxqib9xzh4svs10nsxjz80vzkb34z9wrgwh")))

(define-public crate-tindex-0.5.6 (c (n "tindex") (v "0.5.6") (h "1r5kl0rpx29ngyq3fnvhy084zjwwm7mmvzvkpbf5f54y0cfh3ky2")))

(define-public crate-tindex-0.5.7 (c (n "tindex") (v "0.5.7") (h "1ralrn96smny0rc8pgzv3lg9fwgcxmxx50bd8c06m0hvfvg7ypfs")))

(define-public crate-tindex-0.5.8 (c (n "tindex") (v "0.5.8") (h "1dgd0r84cp9qmsvgnc60frc9rwm163wgngw0ki1mcfziszw6an7p")))

(define-public crate-tindex-0.5.9 (c (n "tindex") (v "0.5.9") (h "1w7706gqzqz3k7mqid5rvchw1jc691m56ds3ay6mr9m56gdl1xyj")))

