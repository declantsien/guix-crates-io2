(define-module (crates-io ti ni tiniestsegmenter) #:use-module (crates-io))

(define-public crate-tiniestsegmenter-0.1.0 (c (n "tiniestsegmenter") (v "0.1.0") (h "070irb363crnxys5zqidwm6v79zqsz9d1gyp9bv37wf7pv515p9g") (y #t)))

(define-public crate-tiniestsegmenter-0.1.1 (c (n "tiniestsegmenter") (v "0.1.1") (h "0zrhl6wg8qq7sbqddhi4nswlnhgd41n3r6h4d6z588llzyq8zjca")))

