(define-module (crates-io ti ni tini) #:use-module (crates-io))

(define-public crate-tini-0.1.1 (c (n "tini") (v "0.1.1") (h "07wskcs9l5x42xphbajnsnsfsmq508yfvfyba286yrajn3qq1fkn")))

(define-public crate-tini-0.2.0 (c (n "tini") (v "0.2.0") (h "0bhhmzxha48sx1hrzmbwi6yfni1516qdkbmsmhmdyxkk4rlamvhi")))

(define-public crate-tini-0.3.0 (c (n "tini") (v "0.3.0") (h "1brbi3vp7xzyh1bi6fg225sqaf02dnnxagm6ldf8q0f7l08y35c5")))

(define-public crate-tini-0.4.0 (c (n "tini") (v "0.4.0") (h "1a1qmxj0z12ndh20fl57zv26ak8j05d62f7kvbzamyixw3sqppji")))

(define-public crate-tini-0.9.0 (c (n "tini") (v "0.9.0") (h "04irg0miank0cxvdkhxqz0xpcqd6sdbwpiqcsx0phmsc6rpcx23f")))

(define-public crate-tini-1.0.0 (c (n "tini") (v "1.0.0") (h "0gg8sg4g1lgswnr43vjnzkq4155fygjvabxh3cayzgvwk4pji92n")))

(define-public crate-tini-1.1.0 (c (n "tini") (v "1.1.0") (h "0w16bb38js4axqz6kq809sd9nyk1smdysdmv9xa10n5qwi2k4q59")))

(define-public crate-tini-1.2.0 (c (n "tini") (v "1.2.0") (h "057pvrvi0sl6v37nz99s34s4qm7lag9w3c878nlpy3yzf8fprmg9")))

(define-public crate-tini-1.3.0 (c (n "tini") (v "1.3.0") (h "0njnsm9klxbix0lvwha7j9nkm96g2jjh8cl8amgyn188bx6dy170")))

