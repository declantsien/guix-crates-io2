(define-module (crates-io ti co tico) #:use-module (crates-io))

(define-public crate-tico-1.0.0 (c (n "tico") (v "1.0.0") (h "0r4lfgkav2l16rdnycmz16cw78mvsl9sqs17zcarqp90w2zkzmxr")))

(define-public crate-tico-2.0.0 (c (n "tico") (v "2.0.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "12dfa8g0r7gcn12i4k2cihk7cwn3lifvzp3pfhh69bwi1s3lyl10")))

