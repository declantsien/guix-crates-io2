(define-module (crates-io ti ps tips) #:use-module (crates-io))

(define-public crate-tips-0.1.0 (c (n "tips") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0fyq42mchxdvpv74az1d35j278srd19i74pqh41agjhb691sfy4z")))

(define-public crate-tips-0.1.1 (c (n "tips") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17dmd0b7l7zxsa0n29l07fvafqs15s4r0hc6d8g3gsknggahq9p7")))

