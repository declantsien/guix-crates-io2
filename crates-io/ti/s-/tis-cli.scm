(define-module (crates-io ti s- tis-cli) #:use-module (crates-io))

(define-public crate-tis-cli-0.1.0 (c (n "tis-cli") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0nfn41mwndw6zd74xxkb8qmysvancmxxma201m8lxmrh8m69dq1d")))

(define-public crate-tis-cli-0.1.1 (c (n "tis-cli") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "16c2d84dn15gj2g2gl9dggayw3h7lvqwy0n1zzzbrz2di3rl7l0s")))

(define-public crate-tis-cli-0.1.2 (c (n "tis-cli") (v "0.1.2") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "012xi553dgybvq2p217zkwk49y7bm07swvn06a6cgijrx4bhm5b3")))

(define-public crate-tis-cli-0.1.3 (c (n "tis-cli") (v "0.1.3") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1yymzjs4sdjhjyqkq2sckyqw7fdh639sdq6in9y9g8pfvj65m0wb")))

(define-public crate-tis-cli-0.1.4 (c (n "tis-cli") (v "0.1.4") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "04b9kk9x52rf5r7mndaffnmm2vcvjik7dkzh3x2nji9ybc57nqpv")))

(define-public crate-tis-cli-0.1.5 (c (n "tis-cli") (v "0.1.5") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1fgvyn5kyisk212hri6zq25nx29dks9b04181akv0nfczwsakcdj")))

(define-public crate-tis-cli-0.1.6 (c (n "tis-cli") (v "0.1.6") (d (list (d (n "enum-iterator") (r "^1.4.1") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "08l0mvzmbsh6wkpqy7hnvhrnkgvlp8cz7lw850jgh7qi803l95dq")))

