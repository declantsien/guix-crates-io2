(define-module (crates-io ti ff tiff_tags_nightly) #:use-module (crates-io))

(define-public crate-tiff_tags_nightly-1.0.0 (c (n "tiff_tags_nightly") (v "1.0.0") (h "0gwaa594gdj9g2fqzblvmcaphfcpqdchh9qppp23zl186ar7mxx7") (y #t)))

(define-public crate-tiff_tags_nightly-1.1.0 (c (n "tiff_tags_nightly") (v "1.1.0") (h "06hfcj5mx2pv178mq6684y1gi2bzgcjcsl0majcm6a39c94805lf") (y #t)))

(define-public crate-tiff_tags_nightly-1.1.1 (c (n "tiff_tags_nightly") (v "1.1.1") (h "1hi0wcbs0hwzljg7aspcg97lgxdbx39sbffva4mi6v112qz4368s") (y #t)))

(define-public crate-tiff_tags_nightly-1.2.0 (c (n "tiff_tags_nightly") (v "1.2.0") (h "00457i9srwb3c2y2jdv9hbihnwr3gq111423sqmq9g5chhf85jpr") (y #t)))

