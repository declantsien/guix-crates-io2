(define-module (crates-io ti ff tiff-encoder) #:use-module (crates-io))

(define-public crate-tiff-encoder-0.1.0 (c (n "tiff-encoder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "1br06yj3x4jw04vcck8hlr5n3h3ldzj3355340wc66sfmbh8zdpc")))

(define-public crate-tiff-encoder-0.1.1 (c (n "tiff-encoder") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "0vpc1zq1nv794nj2s8p02c170g9px5i9d6vvqfyz01xj4r2i70yi")))

(define-public crate-tiff-encoder-0.1.2 (c (n "tiff-encoder") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "1r0awwaawd2dlivbp4f2r7dk51jx029iad6g153sjmnl56isa6sn")))

(define-public crate-tiff-encoder-0.2.0 (c (n "tiff-encoder") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "0xaj03raz6fwivnsakiwzipjdm584sg66yzvngcjmvjbhgv6m4ly")))

(define-public crate-tiff-encoder-0.2.1 (c (n "tiff-encoder") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "08343jw61q102r4arl1l66zbbv8j4vbpnqvi1nw76m980pjdw5iw")))

(define-public crate-tiff-encoder-0.3.0 (c (n "tiff-encoder") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "0vlhf17dn1zbiprwiq97qh1amfjyzsbwa3nbkw8igsqfly5brn5l")))

(define-public crate-tiff-encoder-0.3.1 (c (n "tiff-encoder") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "1dqhx8w623q1287ca3887f5za52qjvzj41asx7zvs587qsrh3vs4")))

(define-public crate-tiff-encoder-0.3.2 (c (n "tiff-encoder") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "0r44jpvr7s4bm3k85h58c3md9gnw64nqj2i5ss4drzjrip7b9pmy")))

