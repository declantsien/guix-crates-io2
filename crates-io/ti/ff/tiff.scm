(define-module (crates-io ti ff tiff) #:use-module (crates-io))

(define-public crate-tiff-0.1.0 (c (n "tiff") (v "0.1.0") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "lzw") (r "0.10.*") (d #t) (k 0)) (d (n "num-derive") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1rl4jc5cpk14lc7267s3xw5gckp9186irnyarnxi39zyh2p2im3c")))

(define-public crate-tiff-0.2.0 (c (n "tiff") (v "0.2.0") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "lzw") (r "0.10.*") (d #t) (k 0)) (d (n "num-derive") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1s0f4qzfd5snry7v5s24mpvxknpc7x2mmn12k2pyfabhb4iz90pp")))

(define-public crate-tiff-0.2.1 (c (n "tiff") (v "0.2.1") (d (list (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "lzw") (r "0.10.*") (d #t) (k 0)) (d (n "num-derive") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "1hmkllhgjiaraxzpwnw5f69v5k6fjkkrdcdx1b9czc9ws57nrk52")))

(define-public crate-tiff-0.2.2 (c (n "tiff") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1kn7psgpacns337vvqh272rkqwnakmjd51rc7ygwnc03ibr38j0y")))

(define-public crate-tiff-0.3.0 (c (n "tiff") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "01vnb9d7qsjjvgdzzshavqrk2xx5rf5v0mkysj5kivgrff64qfbb")))

(define-public crate-tiff-0.3.1 (c (n "tiff") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0zgmbny2f8rssqmjdfvnysy0vqwcvlwl6q9f5yixhavlqk7w5dyp")))

(define-public crate-tiff-0.4.0 (c (n "tiff") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1bbp7c4k5k808r6q070y371rllf38wcsck3dcpcb27nv53j528q0")))

(define-public crate-tiff-0.5.0 (c (n "tiff") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)))) (h "0bzzvxcx21pzryxgd7x7a1himiqs2y4k55754wzlr56sqj3qlfrz")))

(define-public crate-tiff-0.6.0-alpha (c (n "tiff") (v "0.6.0-alpha") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)))) (h "0cqaycgpw69k5ijj7l3hpd0bbphs44g9lgjzqbp0028893jxpsfm")))

(define-public crate-tiff-0.6.0 (c (n "tiff") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "jpeg") (r "^0.1.17") (k 0) (p "jpeg-decoder")) (d (n "miniz_oxide") (r "^0.4.1") (f (quote ("no_extern_crate_alloc"))) (d #t) (k 0)) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "07d4jp2j62zfhyrfq6zi3cmyfn03i7k8jh95q0i3g5x868zlxsxb")))

(define-public crate-tiff-0.6.1 (c (n "tiff") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "jpeg") (r "^0.1.17") (k 0) (p "jpeg-decoder")) (d (n "miniz_oxide") (r "^0.4.1") (f (quote ("no_extern_crate_alloc"))) (d #t) (k 0)) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "0ds48vs919ccxa3fv1www7788pzkvpg434ilqkq7sjb5dmqg8lws")))

(define-public crate-tiff-0.7.0 (c (n "tiff") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.1.17") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "1yn3ffhrb9jlgdkhqgp1as0wpv98dn4fv03c1857q64pp2sll3j0")))

(define-public crate-tiff-0.7.1 (c (n "tiff") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.1.17") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "1cp0ivmahzi2l57d5rjjbcgff71na1na3x68vwwwxdlck6760iq2")))

(define-public crate-tiff-0.7.2 (c (n "tiff") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.2.4") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "11i2smxqa35a921pqs3x5xl7kf3cav3fhqd4xiqafiplhq4xmykw")))

(define-public crate-tiff-0.7.3 (c (n "tiff") (v "0.7.3") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.2.4") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "0rfbiz9ybj075iv6xdw1kmv7kdwqv3wxa2dk3qr1kqni68p6cnbj")))

(define-public crate-tiff-0.7.4 (c (n "tiff") (v "0.7.4") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.2.4") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "1841879wnn7srxp6wm7lc7y69y7b0lfpim03iamy70sya4if8wcz")))

(define-public crate-tiff-0.8.0 (c (n "tiff") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.3.0") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "1zc6z16bxkqcl79nz754i01dag66v484g09hmqcnq58a60lyyzgi") (r "1.61.0")))

(define-public crate-tiff-0.8.1 (c (n "tiff") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.3.0") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "0wg4a6w8sakyy0mggblg340mx8bgglx9hwsxsn8g5fpjkx7k6jbl") (r "1.61.0")))

(define-public crate-tiff-0.9.0 (c (n "tiff") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.3.0") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "04b2fd3clxm0pmdlfip8xj594zyrsfwmh641i6x1gfiz9l7jn5vd") (r "1.61.0")))

(define-public crate-tiff-0.9.1 (c (n "tiff") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "jpeg") (r "^0.3.0") (k 0) (p "jpeg-decoder")) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "0ghyxlz566dzc3scvgmzys11dhq2ri77kb8sznjakijlxby104xs") (r "1.61.0")))

