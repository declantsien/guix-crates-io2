(define-module (crates-io ti ff tiffin) #:use-module (crates-io))

(define-public crate-tiffin-0.0.1 (c (n "tiffin") (v "0.0.1") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("fs" "env" "dir" "user" "mount"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0mldqsi1n3d022jjl6m65jg8fkfdfcffg0zy426bcgisfffqd92s")))

