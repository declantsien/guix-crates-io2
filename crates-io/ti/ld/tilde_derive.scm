(define-module (crates-io ti ld tilde_derive) #:use-module (crates-io))

(define-public crate-tilde_derive-0.0.1 (c (n "tilde_derive") (v "0.0.1") (h "0llnlyrwx53xcgdhknksyn025g29yglhkq4dgfynibwvspg2m2dd")))

(define-public crate-tilde_derive-0.0.2 (c (n "tilde_derive") (v "0.0.2") (d (list (d (n "tilde") (r "^0.0.2") (d #t) (k 2)))) (h "0kqc3d2aj4vv4v7slz1065jy091s4gd8sryx5qaclz7ifmx7rac1")))

(define-public crate-tilde_derive-0.0.3 (c (n "tilde_derive") (v "0.0.3") (d (list (d (n "tilde") (r "^0.0.3") (d #t) (k 2)))) (h "1g3sz4nl1jdxwj5fx8lhbynyk239sj6fzfxy49g5g6pspz8ybnyj")))

(define-public crate-tilde_derive-0.0.4 (c (n "tilde_derive") (v "0.0.4") (d (list (d (n "tilde") (r "^0.0.4") (d #t) (k 2)))) (h "0gxgf3sqbl3rcznajhpbkicsibbdczb9viqksdmyw6gys6bp39ak")))

