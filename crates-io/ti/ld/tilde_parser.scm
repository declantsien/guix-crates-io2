(define-module (crates-io ti ld tilde_parser) #:use-module (crates-io))

(define-public crate-tilde_parser-0.1.0 (c (n "tilde_parser") (v "0.1.0") (d (list (d (n "tilde-expand") (r "^0.1.1") (d #t) (k 0)))) (h "0qb7cyh9pilq0fks5bgm776vb7wz6xbakn7bh8bfn886fkga3sll")))

(define-public crate-tilde_parser-0.1.1 (c (n "tilde_parser") (v "0.1.1") (d (list (d (n "tilde-expand") (r "^0.1.1") (d #t) (k 0)))) (h "1lcjicgdjbrw6zw67607javy9lhyh43n07ml5iqd1nnbhj4lnvbc")))

