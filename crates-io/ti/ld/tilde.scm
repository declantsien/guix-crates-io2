(define-module (crates-io ti ld tilde) #:use-module (crates-io))

(define-public crate-tilde-0.0.1 (c (n "tilde") (v "0.0.1") (h "1wrbayv4bdh4qdgqb8g0ixaq1wr2sppnav0y3cfh1w3ifmr7j6jd")))

(define-public crate-tilde-0.0.2 (c (n "tilde") (v "0.0.2") (h "1hjwkbq18n6r12yvm832wgr61q4anawh35x1sks3k6h4y73rdjs9")))

(define-public crate-tilde-0.0.3 (c (n "tilde") (v "0.0.3") (h "1sm8aq0ybpnjx2ir4wdgh4z72ml6c1zryqrzz1y09aid1vl8gk1f")))

(define-public crate-tilde-0.0.4 (c (n "tilde") (v "0.0.4") (h "10jssv42wq9wc0w00gv4z40sciln1yrrr7fmyjncasdajc4vac14")))

