(define-module (crates-io ti dd tiddly-wiki-server) #:use-module (crates-io))

(define-public crate-tiddly-wiki-server-0.1.1 (c (n "tiddly-wiki-server") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "axum") (r "^0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (f (quote ("serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 0)) (d (n "tower-http") (r "^0.3") (f (quote ("fs"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "fmt"))) (d #t) (k 0)))) (h "02z1gmwlaprfmx7m71a7vhvicmd5d8qqpciqp4zfg9rqzw9fzwj2")))

