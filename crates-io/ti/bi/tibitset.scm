(define-module (crates-io ti bi tibitset) #:use-module (crates-io))

(define-public crate-tibitset-0.1.0 (c (n "tibitset") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xqca3h56izhyb3daa9fg3ck807dklnkq9f94yvz7zspr51byv5c") (f (quote (("std") ("default" "std"))))))

