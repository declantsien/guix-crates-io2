(define-module (crates-io ti gh tightness) #:use-module (crates-io))

(define-public crate-tightness-0.1.0 (c (n "tightness") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1shs4j0wqys9hc12dypyzr9pllh74g9qh13yfw6wx1yxliqp61w9") (f (quote (("unsafe_access"))))))

(define-public crate-tightness-0.1.1 (c (n "tightness") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1blwk8gd6jddd4zjlyi2ac8kb29fqaks7ymzzla2vi6znyik550r") (f (quote (("unsafe_access"))))))

(define-public crate-tightness-0.1.2 (c (n "tightness") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p3jj9v5w342xbwxfa1a7ngh96c3jyqlc1i5fbwx6z36m434xx6p") (f (quote (("unsafe_access"))))))

(define-public crate-tightness-1.0.0 (c (n "tightness") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zbha63gybifxwf3x575kv0zdpsg8gw3valj8b9qqsz7an780641") (f (quote (("unsafe_access"))))))

(define-public crate-tightness-1.0.1 (c (n "tightness") (v "1.0.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rpkqvkvngiscr2zcj0r26rp8rbwmz52v8l39s44kjw2av1702pl") (f (quote (("unsafe_access"))))))

