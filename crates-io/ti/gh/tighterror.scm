(define-module (crates-io ti gh tighterror) #:use-module (crates-io))

(define-public crate-tighterror-0.0.1 (c (n "tighterror") (v "0.0.1") (h "1f5yx1mc78gw2qj6qg0z0lyi472vfj99zmqj3v7vv1b4bf3q9a4h")))

(define-public crate-tighterror-0.0.2 (c (n "tighterror") (v "0.0.2") (h "1g0inlla86r43kxmhdpzgkfad65vijdxcbc49qq76wybm1z0i2dc")))

(define-public crate-tighterror-0.0.3 (c (n "tighterror") (v "0.0.3") (h "087wvhfdjag5mzk1w4i5dr005hwiv0m6wm32yxinms6qry4wwii1")))

(define-public crate-tighterror-0.0.4 (c (n "tighterror") (v "0.0.4") (h "0m26xfa8gg74dn1a77x3spr5akrgxbn86vy3k5bf1y1d28cfr8gy")))

(define-public crate-tighterror-0.0.5 (c (n "tighterror") (v "0.0.5") (h "055222lsbky5p39pwx013mhvpnvvwhpskd8wkxpc3d6f7iqw4i56")))

(define-public crate-tighterror-0.0.6 (c (n "tighterror") (v "0.0.6") (h "18mzi9rrbanh32jsvdn67sc8vr6xca25khafhg1bi37il83722cx")))

(define-public crate-tighterror-0.0.7 (c (n "tighterror") (v "0.0.7") (h "1ksx5hdcvx01vfadk8dz9y0i7brpy6fazinlnlfmjgpc5yyf03w8")))

(define-public crate-tighterror-0.0.8 (c (n "tighterror") (v "0.0.8") (h "0wlpwrn3b4zizpvb375pn5nab5ckcsa7dhmfb1xm4d5zvqcjz3f1")))

(define-public crate-tighterror-0.0.9 (c (n "tighterror") (v "0.0.9") (h "1y4gxx4w5i4vhlpyigiw36cvxv1kpl4wdw3v2s5fqbzwrlpvlwlr")))

(define-public crate-tighterror-0.0.10 (c (n "tighterror") (v "0.0.10") (h "07lcpblyg9ag3w7487hryhk137l064mli9b2g4g9dg0jfgi7phq0")))

(define-public crate-tighterror-0.0.11 (c (n "tighterror") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0srgk74f65r6x0j5xz2h7mjgzvxcgsgavwbnyww3spkivhx3g5ic")))

(define-public crate-tighterror-0.0.12 (c (n "tighterror") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1pnwq8h27lcah9mqvky860x2g8y4vj85awv1b9f3y61zz50f2ikw")))

(define-public crate-tighterror-0.0.13 (c (n "tighterror") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "059ypmfs0x55gnbsfycpqmn9mvgnifz4wq1gxdj3ylcf432j5wsw")))

(define-public crate-tighterror-0.0.14 (c (n "tighterror") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1ph12jsq8g2jpn4jajdivgwbk9n3kpiln1ywk46nz34pjqfv9532")))

(define-public crate-tighterror-0.0.15 (c (n "tighterror") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1xqj3ps60mbhdvql0vnh5d05xpd1vzarivin45di24xi84v344kd")))

