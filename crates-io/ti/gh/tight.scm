(define-module (crates-io ti gh tight) #:use-module (crates-io))

(define-public crate-tight-0.1.0 (c (n "tight") (v "0.1.0") (h "1q4iv2apfp9avnk9vqmycyx8452jd02fxdh67dmyzh0xfswi4vga")))

(define-public crate-tight-0.1.1 (c (n "tight") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1jh345bprjplil4ayzz9k5p9wimmk1qa10ibvgq7ldvz8rj765vl")))

(define-public crate-tight-0.1.2 (c (n "tight") (v "0.1.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16mjqifis8pnyfi9ahxry7m4kpnyr469w0ra09yxd487cnkfnlsm")))

(define-public crate-tight-0.1.3 (c (n "tight") (v "0.1.3") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07qgwfaigrr4hn2grmvf7i3m8n2lgczfjxmby11wk0bs5l8090li")))

(define-public crate-tight-1.0.0 (c (n "tight") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l9mnf4lz80k617c5sxxld47nrk99iyvhy2kzhqn24mpy1bkk4iq")))

(define-public crate-tight-1.0.1 (c (n "tight") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "scan_fmt") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c8jyxgqn8rln0l984qw1yzfjmgih6k0ay2xyjfj6q7zxgh3bmzv")))

