(define-module (crates-io ti nk tinkoff-api) #:use-module (crates-io))

(define-public crate-tinkoff-api-1.0.0 (c (n "tinkoff-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "04sbhcq62vpyj5ahjbz06z46hdri2z5hhhh915sck2lv71pazygn")))

