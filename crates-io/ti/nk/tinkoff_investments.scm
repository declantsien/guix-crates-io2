(define-module (crates-io ti nk tinkoff_investments) #:use-module (crates-io))

(define-public crate-tinkoff_investments-0.1.0 (c (n "tinkoff_investments") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0vq03pd0pmkjxzici0z2bc3ijg20h8xnwcgi5ls6lpaxsyyhrri3") (y #t)))

