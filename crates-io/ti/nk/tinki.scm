(define-module (crates-io ti nk tinki) #:use-module (crates-io))

(define-public crate-tinki-0.1.0 (c (n "tinki") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "console_log") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sauron") (r "^0.24.0") (f (quote ("with-dom"))) (d #t) (k 0)) (d (n "spongedown") (r "^0.4.1") (d #t) (k 0)) (d (n "url_path") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.60") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0cm6r2c3xgmmc0855qwsngknyw6h6v3rxrivdlvp6hbz4vqnf9r3")))

