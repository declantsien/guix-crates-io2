(define-module (crates-io ti nk tinkoff-invest-types) #:use-module (crates-io))

(define-public crate-tinkoff-invest-types-0.1.0 (c (n "tinkoff-invest-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00grsgvs3cklld1avdmbivh6n1yqfqw7wq0dmqidc4zxg9wkh78w") (y #t)))

(define-public crate-tinkoff-invest-types-0.2.0 (c (n "tinkoff-invest-types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zq6y1l3arha8qz5gl42qcmn3ywxfzv4aravkbdcf1h3al6lbk6z") (y #t)))

(define-public crate-tinkoff-invest-types-0.3.0 (c (n "tinkoff-invest-types") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr0kx98w29gsczp1danf92c5dgxc9qyjmj6xifgjffr7zmdcjs2") (y #t)))

(define-public crate-tinkoff-invest-types-0.4.0 (c (n "tinkoff-invest-types") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06035a76a7j6bpzv8ql8w91wsh59255sffppdix0imcz9qnv6pil") (y #t)))

(define-public crate-tinkoff-invest-types-0.5.0 (c (n "tinkoff-invest-types") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z04i0cx3lzk6ihy89c9s9nwxc035hv0s2wh41ajasplwrxcfdww") (y #t)))

(define-public crate-tinkoff-invest-types-0.6.0 (c (n "tinkoff-invest-types") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vq25fak0s12xiysfqsr67aikcr2ccpfpwzba56lwwcmrykiynkn") (y #t)))

(define-public crate-tinkoff-invest-types-0.7.0 (c (n "tinkoff-invest-types") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r2lw8dn24fv1ylvs4688nmiqi12gw7pj6wcdfljf8g43alfrsk4") (y #t)))

(define-public crate-tinkoff-invest-types-0.8.0 (c (n "tinkoff-invest-types") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19dm8qcxvza8jhinz4c6c7f3zjcb91x9bpgwjbfvmxkiqk7ly67v") (y #t)))

(define-public crate-tinkoff-invest-types-0.9.0 (c (n "tinkoff-invest-types") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hdcv0ly3p9j3j2j37j706cliq5aklq161wybs8w7d0g6lsvdmy9") (y #t)))

(define-public crate-tinkoff-invest-types-0.10.0 (c (n "tinkoff-invest-types") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f8cqxndzg20d2ivn89ffyv075sm4hz454lxb16cycw1k2z2ddik") (y #t)))

(define-public crate-tinkoff-invest-types-0.11.0 (c (n "tinkoff-invest-types") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zf79sk5nw86zd42pgxin8ch2sfvy6yz7m47848qcbv2amf80yff") (y #t)))

(define-public crate-tinkoff-invest-types-0.12.0 (c (n "tinkoff-invest-types") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08hgww739bgd1n8g5hka2gp9q7az0kw5d9qa9qw5sk8jd1ysaqic") (y #t)))

(define-public crate-tinkoff-invest-types-0.13.0 (c (n "tinkoff-invest-types") (v "0.13.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f87mg9b4zfcfa3ayqr11q331hjaf73x5r3dg09k19x7hkb1vary") (y #t)))

(define-public crate-tinkoff-invest-types-0.14.0 (c (n "tinkoff-invest-types") (v "0.14.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iw86si2wdvzvd46g4fg7hdi1rrbqjmfyrr6vsrh8hr6nksyp0ra") (y #t)))

(define-public crate-tinkoff-invest-types-0.15.0 (c (n "tinkoff-invest-types") (v "0.15.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rbqq5j0v2ih80n3cfhrrdcp99k6mvkwrvd3idzayq0kb6bbalwd") (y #t)))

(define-public crate-tinkoff-invest-types-0.16.0 (c (n "tinkoff-invest-types") (v "0.16.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09xgxss0wafc1qwfdc7sn8l4pjr9chcp6b16czzr41d80w7m3hzq")))

(define-public crate-tinkoff-invest-types-2.0.0 (c (n "tinkoff-invest-types") (v "2.0.0") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "1szw4hqa5q8vm3fp63590pq17qnymxqrc1pq3z10mqhma483v290") (y #t)))

(define-public crate-tinkoff-invest-types-2.0.1 (c (n "tinkoff-invest-types") (v "2.0.1") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "1b5d5yn3si4mwgk10axlvxd34q1gwdwjklyakkyaxi4zrbjz10rv") (y #t)))

(define-public crate-tinkoff-invest-types-2.1.0 (c (n "tinkoff-invest-types") (v "2.1.0") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "1b86fp5dv8yhgjrki0iahagkj6rq5yr5v1sd7b0zj6lcnvawj9fa") (y #t)))

(define-public crate-tinkoff-invest-types-2.1.1 (c (n "tinkoff-invest-types") (v "2.1.1") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6.2") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "0vf174k3pm2shscx3645ibaang5v8pffkxpxc2185blxx72n9ygs")))

(define-public crate-tinkoff-invest-types-2.2.0 (c (n "tinkoff-invest-types") (v "2.2.0") (d (list (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tonic") (r "^0.7.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.1") (d #t) (k 1)))) (h "1vr2rwqj6m3haxwrjiylix7mspj1jczijigsahcykqs745lyij0k")))

(define-public crate-tinkoff-invest-types-2.3.0 (c (n "tinkoff-invest-types") (v "2.3.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "1h11h541qjk7j3ski4yvjiqxz6y1wixlfjzyp9i9vr13f1lmv2pa")))

(define-public crate-tinkoff-invest-types-2.4.0 (c (n "tinkoff-invest-types") (v "2.4.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "0bqmy8f2i0dd03yzcczylnhv96ci7r4wxzwqz761wqzd18z7dcks")))

(define-public crate-tinkoff-invest-types-2.5.0 (c (n "tinkoff-invest-types") (v "2.5.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "0bzchlb72dr0m7y43q1vdih08hxdab29y6r6nmghl08fyxfsv38l")))

(define-public crate-tinkoff-invest-types-2.6.0 (c (n "tinkoff-invest-types") (v "2.6.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "04gx9pcxx55naq7qdk5q22q9w6winfb8d6a71v7vy3qdw66lcmbg")))

(define-public crate-tinkoff-invest-types-2.7.0 (c (n "tinkoff-invest-types") (v "2.7.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1hczlgqm0lmmm14qhql43v18qsik119ps9rpyspmg739dxpvzcz0")))

(define-public crate-tinkoff-invest-types-2.8.0 (c (n "tinkoff-invest-types") (v "2.8.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "06hzwaq1ay3v9iqqzrpli020qr9zfphqi3wm8iz81m55pga8yjjq")))

(define-public crate-tinkoff-invest-types-2.9.0 (c (n "tinkoff-invest-types") (v "2.9.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0mirkia4qrk4q8w8n4f7j579pf4yy6ngnk7n1zdd2v6h4hfjc4zi")))

(define-public crate-tinkoff-invest-types-2.10.0 (c (n "tinkoff-invest-types") (v "2.10.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0wvf6szg0j7jv8xc8ab9rwjkkxxiihy7lz9b822cjh8r7sr6wn5a")))

(define-public crate-tinkoff-invest-types-2.11.0 (c (n "tinkoff-invest-types") (v "2.11.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0sybs7g46ak75pcqg77pps7ybpyk7b6nmirwifg7m3inavdfa5zd")))

(define-public crate-tinkoff-invest-types-2.12.0 (c (n "tinkoff-invest-types") (v "2.12.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0yrv99ivh21fzazhg0q91srn6vm17jjbdfh47hlap1ygd7qsswm9")))

(define-public crate-tinkoff-invest-types-2.13.0 (c (n "tinkoff-invest-types") (v "2.13.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "14fggjs05ixk9r8gszpx3aa8y0508y8cmhnp3f1l9pa0flgwlwpx")))

(define-public crate-tinkoff-invest-types-2.13.1 (c (n "tinkoff-invest-types") (v "2.13.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "1kswyxbc2rjkxj86rfbkzmj8vx3wdcmnqyxwf75dwpj8ljlyk7jv")))

(define-public crate-tinkoff-invest-types-2.14.0 (c (n "tinkoff-invest-types") (v "2.14.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)))) (h "0jfh92grms598y3r8fwrs9z1a7cfvpyqijvyd2cr96cvlv32mmqg")))

(define-public crate-tinkoff-invest-types-2.14.1 (c (n "tinkoff-invest-types") (v "2.14.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "14n0mi7gij4hxb5vqj3h9ymag8zlzzi80pr57xqw7q3l2146z6l6")))

(define-public crate-tinkoff-invest-types-2.15.0 (c (n "tinkoff-invest-types") (v "2.15.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (d #t) (k 1)))) (h "1ipjn1vwfbbza9v4q52ra81ncvymiyngp7ijlg2nrjx7wn2cq261")))

(define-public crate-tinkoff-invest-types-2.16.0 (c (n "tinkoff-invest-types") (v "2.16.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (d #t) (k 1)))) (h "04nr2bw91if8qwrqzl21d9yfigywlsgy8l01bayn7av3smsavqzk")))

