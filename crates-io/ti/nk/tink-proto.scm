(define-module (crates-io ti nk tink-proto) #:use-module (crates-io))

(define-public crate-tink-proto-0.1.0 (c (n "tink-proto") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l920vgchsks3s6d2lwzkxkvcsxv915bsxp1fdbsxlr6v0574l4k") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.1.1 (c (n "tink-proto") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1anpd6hqhwlp8nl3hxh6jw0sk2w63n6zppjbnp68w0h32dan72ng") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.0 (c (n "tink-proto") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17r5gs8dljblvxvc1z9vq4jxm2rxh2y8i3q007m0h6nvsbwpl2bg") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.1 (c (n "tink-proto") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (d #t) (k 1)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m3rxkps122xffawdkhdmx680qxpln152xb9zjjm4zyppb1xlihn") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.2 (c (n "tink-proto") (v "0.2.2") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (d #t) (k 1)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qngc9rk4ch4zhrpwxd8qyq4x48fw98byc6c2jzap3sfsraf5rkk") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.3 (c (n "tink-proto") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (d #t) (k 1)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h8qwya8qa3r5vcd6k49pca7xwdxzfk7im082grdlpn9nhm1n3ww") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.4 (c (n "tink-proto") (v "0.2.4") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "101wr0j1wmgsifz1cw0fhxizwjsykdkanx1id3jcvjz6zgswlfa3") (f (quote (("json" "base64" "serde") ("default"))))))

(define-public crate-tink-proto-0.2.5 (c (n "tink-proto") (v "0.2.5") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)))) (h "0xcqvv8ll4s9cplpncpibdjwd501wx4c4bawgx22n2mcbf1yq0gs") (f (quote (("json" "base64" "serde") ("default"))))))

