(define-module (crates-io ti nk tinkoffpay) #:use-module (crates-io))

(define-public crate-tinkoffpay-1.0.0 (c (n "tinkoffpay") (v "1.0.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hl8vh68z9939qb5sg8mmjvvy827ifyxwllgfbmk4w0brxzs7vci")))

(define-public crate-tinkoffpay-1.0.1 (c (n "tinkoffpay") (v "1.0.1") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8x4hfijsixn6fd82l5lhr14fzy0bmpz06fixmqgwxiywvrizyy")))

(define-public crate-tinkoffpay-1.0.2 (c (n "tinkoffpay") (v "1.0.2") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hqf0q9zm1rx7hgwyw0k1lhi0pl33pnwxgccxv6l6d55kh3fbskd")))

(define-public crate-tinkoffpay-1.0.3 (c (n "tinkoffpay") (v "1.0.3") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vi81y8sbi1nzs5j8b1csnphwn6j9ijflppkqax5ihp760jdgczb")))

