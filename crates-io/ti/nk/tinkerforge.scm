(define-module (crates-io ti nk tinkerforge) #:use-module (crates-io))

(define-public crate-tinkerforge-2.0.0 (c (n "tinkerforge") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1qqkb6c31la16rvkjn7hrrmdz52s3w4mvsa36nq8fi85p7ms5wp4") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.1 (c (n "tinkerforge") (v "2.0.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0k33fynzs4w94r493ws413a4kw576qpwnzi6wkhlagsdhkyp1h2n") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.2 (c (n "tinkerforge") (v "2.0.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1lxzzw5cvq2vpz9d4zcl9rsphml71rabkira5yqf9bldbhz1dcy8") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.3 (c (n "tinkerforge") (v "2.0.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "042iwnif2123xl10695vn42nabpsskhx97hjgfdw45pwk035v1cw") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.4 (c (n "tinkerforge") (v "2.0.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1hclwnhfr7ry7ag5c7xqv84ybg6phwv38y0mv21jmnzwl5hqbbgm") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.5 (c (n "tinkerforge") (v "2.0.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0i26sjry47774c9awg7whna8vbww0hc00q083f8ivyr63dgrfjpz") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.7 (c (n "tinkerforge") (v "2.0.7") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1w20m3ikm3rjly3x3zmvx87wmbyypd3qn2ybarya0xp5f3vvx967") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.8 (c (n "tinkerforge") (v "2.0.8") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0pl0k0qrh6v7kkma03g30gm533zjs4q6bw4vc4whxgpihsg2bgqg") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.9 (c (n "tinkerforge") (v "2.0.9") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1hgcrqvdl6ln0z7bwvpl2hdlsw1nz78d6k17fyi0hn9dzrwl7lrg") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.10 (c (n "tinkerforge") (v "2.0.10") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1f6jyxyqbnnz3g7g2zdr3dp6fa558965wljz0rcvapzgn3mw6ab4") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.11 (c (n "tinkerforge") (v "2.0.11") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "1g3qpy1lbhici957nph78fyjib7sdrwix1qcywx8rij196cxmwzv") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.12 (c (n "tinkerforge") (v "2.0.12") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0a5z3jc5p9d6wmcgw7jfbjc0in867piwhvpngbd59kjj6cfq5z4m") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.13 (c (n "tinkerforge") (v "2.0.13") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0dihvrda67dsgkwlgqbwn3mvnbns112ify0hisjc730rqgir04pj") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.14 (c (n "tinkerforge") (v "2.0.14") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "16np5dxsv4xn7d5ycgri370n7bfik91ag6p1hhfq9bzdrirb1bny") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.15 (c (n "tinkerforge") (v "2.0.15") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0ynmvargblk9nm4kj3mjm85blkpcbgxppmzx4wm73sy30953ld6q") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.16 (c (n "tinkerforge") (v "2.0.16") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "01z72wryacb115dn5xym57l3671jr82bfasgln5wq1va87hw30wm") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.17 (c (n "tinkerforge") (v "2.0.17") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0d04m7sx7p0nnmwcyxwsh74kg64q5kqwy60h370apzzrys2v8mwa") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.18 (c (n "tinkerforge") (v "2.0.18") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)))) (h "0614n8gcwjclhqw0l9gx8qq8b2g7bir2wlmya1369zhl3kxk1nah") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.19 (c (n "tinkerforge") (v "2.0.19") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)))) (h "1vh1if23jg6clpygjk03rgj5kfd5l4kw7gkc9p8anb0jqbrw7pzn") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.20 (c (n "tinkerforge") (v "2.0.20") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)))) (h "1cc3fmzls381mjm0s87d0v7sdhf70rw6c3ls582k0386qqg6fax4") (f (quote (("fail-on-warnings"))))))

(define-public crate-tinkerforge-2.0.21 (c (n "tinkerforge") (v "2.0.21") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)))) (h "19jnah7w1g833n1i49wjvarh2qqr39pd8ndn62v8g73drd79r6jw") (f (quote (("fail-on-warnings"))))))

