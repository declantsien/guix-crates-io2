(define-module (crates-io ti rs tirse) #:use-module (crates-io))

(define-public crate-tirse-0.0.0 (c (n "tirse") (v "0.0.0") (d (list (d (n "byteorder") (r "1.2.*") (o #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 2)))) (h "110wg5xblbd7xna2jsmjdpnzddxd9616c7bb5rd3algndccgssi8") (f (quote (("std" "byteorder/std" "serde/std") ("default" "byteorder" "serde"))))))

(define-public crate-tirse-0.0.1 (c (n "tirse") (v "0.0.1") (d (list (d (n "byteorder") (r "1.2.*") (o #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 2)))) (h "1hh7pnsbrijcrivx9lapg3z80lwnypz8xbyflfhmxwihdnfx4cm3") (f (quote (("std" "byteorder/std" "serde/std") ("default" "byteorder" "serde"))))))

(define-public crate-tirse-0.0.2 (c (n "tirse") (v "0.0.2") (d (list (d (n "byteorder") (r "1.2.*") (o #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 2)))) (h "0k3jgkh3kanwcb912kvy74c4y72j4cw4b776qvcc6fxxfcp1vbd0") (f (quote (("std" "byteorder/std" "serde/std") ("default" "byteorder" "serde"))))))

(define-public crate-tirse-0.1.0 (c (n "tirse") (v "0.1.0") (d (list (d (n "byteorder") (r "1.2.*") (o #t) (k 0)) (d (n "serde") (r "1.0.*") (o #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 2)))) (h "0g7vc0k8c17n9jlb3xsimar9x806nxn1xxcdaxffma01irzsfdc9") (f (quote (("std" "byteorder/std" "serde/std") ("default" "byteorder" "serde"))))))

(define-public crate-tirse-0.2.0 (c (n "tirse") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "09f6fn6lqzg00g0blx5yygimr9xf88ywq01ka2dyyqf2cb1mwa3h") (f (quote (("std" "byteorder" "serde/std") ("default" "byteorder" "serde"))))))

(define-public crate-tirse-0.2.1 (c (n "tirse") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ngf7hzvfzfl9r73nbdg54vazbwcvlf9slcc4yrrjgh2n2n8kpqa") (f (quote (("std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.2.2 (c (n "tirse") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0yjbcv21hqhci472zch910c7k66kkiqyv4gpszdxsfj9dqx4pqkq") (f (quote (("std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.2.3 (c (n "tirse") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1rgm7289dbdg2b754lmshpaswrjisiif4dck3zlas2rnnl9j90zw") (f (quote (("std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.2.5 (c (n "tirse") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1dqdczycb4g8hqmdmdl8m9j2nq00il2vqfdrkdahm4z0lhxqz265") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.2.6 (c (n "tirse") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1avd4wn280qs5w7hi0wdyqdv8a2k0dd1aywqr6n9v975cvxh6f9c") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.3.0 (c (n "tirse") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bfr3id1m97bm659khv2nyi4m71gxkr3bdaksx4vp9k6h0a8vlhl") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.4.0 (c (n "tirse") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "10gxnr7qflwmn0pvzirxgjhi5dzg95ml9bbkvmr5f0ds3iq4ikjv") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.4.1 (c (n "tirse") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ymp5vp5h7m2mznd93m32nlc4a1xjky8js6q3l5hv1zzqc35kivq") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.5.0 (c (n "tirse") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0dx2ghiin5y6585rlvy8jk92rpg9bvw9gxa3c6sq8w2kps2s1hlm") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.5.1 (c (n "tirse") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.3") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "02y0gg2s82sxrkj54rpv690cxnwncbm3abq9c47pqppj9ph4d28p") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

(define-public crate-tirse-0.6.0 (c (n "tirse") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3") (o #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1x1qssd31vkyvskkgvsp2kglq7lwpfj57ypsxg4wyj3sfrnmyf9g") (f (quote (("use_std" "byteorder/std" "serde/std" "either/use_std") ("default" "byteorder" "serde" "either"))))))

