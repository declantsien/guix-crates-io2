(define-module (crates-io ti nc tincture) #:use-module (crates-io))

(define-public crate-tincture-0.1.0 (c (n "tincture") (v "0.1.0") (h "0jd60l9bddyyh105f9cg3dnw990hgw65hwvkp4cvr9df8q3zfqkf") (y #t)))

(define-public crate-tincture-0.1.1 (c (n "tincture") (v "0.1.1") (h "0c0gignra2jlqldp861lg52hyw9qi7s4pvcxycv3qgz2p6jwjl31") (y #t)))

(define-public crate-tincture-0.2.0 (c (n "tincture") (v "0.2.0") (h "0b6sdl0k0rgld615fssnrlqcp19p6gnkvrx1yyxz2s8vxx8ng24b") (y #t)))

(define-public crate-tincture-0.3.0 (c (n "tincture") (v "0.3.0") (h "0rb6j564f0hp69g5ywg6hzwgvzc1r006knr8hy2zm2n48g9470kw") (y #t)))

(define-public crate-tincture-0.4.0 (c (n "tincture") (v "0.4.0") (h "17dd95f2xkq921y2cdya2nvyq9qgqjh5fd0ranfz1wb7bmyca713") (y #t)))

(define-public crate-tincture-0.4.1 (c (n "tincture") (v "0.4.1") (h "0z2vwlxv87n6f4nw9pnpn05cj5axcrfdllpzfpn4whilyz7fd6rr") (y #t)))

(define-public crate-tincture-0.4.2 (c (n "tincture") (v "0.4.2") (h "0v14522cdkwimxpqrkkaz583jmikh22d6jgj829yj8h362l08h3d") (y #t)))

(define-public crate-tincture-0.4.3 (c (n "tincture") (v "0.4.3") (h "07priwy4akgfdbm7rs4frarlvl24m30jfkrfyf66bz0nx81x6phc")))

(define-public crate-tincture-0.5.0 (c (n "tincture") (v "0.5.0") (h "14pn6866bxyp8ah697vhwfjjgalfbhcff8q2f5lz2bjfpz1i7xq5")))

(define-public crate-tincture-1.0.0 (c (n "tincture") (v "1.0.0") (h "1nmqxhx88aj2q4895pw0qgmihxfyfq8fgqkvlkxw628920qidm19")))

