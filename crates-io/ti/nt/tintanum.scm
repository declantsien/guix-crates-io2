(define-module (crates-io ti nt tintanum) #:use-module (crates-io))

(define-public crate-tintanum-0.1.0 (c (n "tintanum") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.6.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 2)) (d (n "xdg") (r "^2.5.2") (d #t) (k 2)) (d (n "zbus") (r "^3.14.1") (d #t) (k 0)))) (h "1s0n7vk2s0q416ky5gdx6mjlsa691fijfzhbq8rhgj5gff8wzfvs")))

