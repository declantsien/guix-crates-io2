(define-module (crates-io ti nt tint) #:use-module (crates-io))

(define-public crate-tint-1.0.0 (c (n "tint") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1ysama0rxhid6wp7jznz48vymnfr880ixvg59jdbxfiirmmlhcr4")))

(define-public crate-tint-1.0.1 (c (n "tint") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "15k89pp3ddxczfnv2j995yvy02mfpmjrmxmv7mkp8c2acrq4bwks")))

