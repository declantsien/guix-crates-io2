(define-module (crates-io ti kt tiktoken) #:use-module (crates-io))

(define-public crate-tiktoken-1.0.1 (c (n "tiktoken") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.30") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.30") (d #t) (k 0)))) (h "1n7sfh6hqrdmlkk1r7vqv0xf0kl8yvhg1ya38yqz18535d5xbn70")))

