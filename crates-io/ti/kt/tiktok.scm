(define-module (crates-io ti kt tiktok) #:use-module (crates-io))

(define-public crate-tiktok-0.0.1 (c (n "tiktok") (v "0.0.1") (h "0g05dnb3zjy6bjysvp3rb8gsmkhr1akym9skwbb3hma4gwrhdfiv")))

(define-public crate-tiktok-0.0.2 (c (n "tiktok") (v "0.0.2") (h "0zj2rwgcc23iraq2r6wkc9fqi7kz075g7rnj7n86705w2j5s45a9")))

