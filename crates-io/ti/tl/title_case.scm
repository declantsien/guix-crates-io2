(define-module (crates-io ti tl title_case) #:use-module (crates-io))

(define-public crate-title_case-0.1.1 (c (n "title_case") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "ucfirst") (r "^0.2.1") (d #t) (k 0)))) (h "0zljzfqxd48xkr0vhhx321idigqpzhi21j87m1rbkyhigh92cjx0")))

(define-public crate-title_case-0.1.2 (c (n "title_case") (v "0.1.2") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "ucfirst") (r "^0.2.1") (d #t) (k 0)))) (h "1mg8sh7ladndv4d7qdxpxhc029lc79ixd5xl8dy7ihh37h9mzv6l")))

