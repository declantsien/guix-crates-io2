(define-module (crates-io ti tl titlefmt) #:use-module (crates-io))

(define-public crate-titlefmt-0.3.0 (c (n "titlefmt") (v "0.3.0") (d (list (d (n "id3") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^4.2.0") (d #t) (k 0)))) (h "1rb1qr8kxr5k1svzwj9qg7ddzbwsrlkv9ss98njjicdnq8an3p7c") (f (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac"))))))

(define-public crate-titlefmt-0.3.1 (c (n "titlefmt") (v "0.3.1") (d (list (d (n "id3") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^4.2.0") (d #t) (k 0)))) (h "1dnwgm8fq6pnyza37rp8494lfllywfvcmj6k392cdi26mngk7f13") (f (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac"))))))

(define-public crate-titlefmt-0.4.0 (c (n "titlefmt") (v "0.4.0") (d (list (d (n "id3") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "metaflac") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "nom") (r "^4.2.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "17bqvr6jzhjhnjr96f65j1ipb35zxqyga0ma24iapddgpqdalqag") (f (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac") ("default" "unicode-normalization"))))))

