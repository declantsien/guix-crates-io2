(define-module (crates-io ti tl titlecase) #:use-module (crates-io))

(define-public crate-titlecase-0.9.0 (c (n "titlecase") (v "0.9.0") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1c7i1i4wynz3pllxghwsrz67yj1szj6p9k6r1a7s9rihmrjhmz2z")))

(define-public crate-titlecase-0.9.1 (c (n "titlecase") (v "0.9.1") (d (list (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1zsckvvf1kvpxbwrj8h12mc08gqrjhi7vamaxwqs487v3visg65z")))

(define-public crate-titlecase-0.10.0 (c (n "titlecase") (v "0.10.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1j0naf5784bfiqc4vy6q31y1ya884v78p1yc3ilyjq8wfsd3jl06")))

(define-public crate-titlecase-1.1.0 (c (n "titlecase") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "05qizspxihjhmzsd9y6kfxzrss4jl4y042wni4m2yk62rw8f8rgm")))

(define-public crate-titlecase-2.0.0 (c (n "titlecase") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "00l8h1qkiggb6970wk73lq2769802j2nwpi7gk0fd4p8fqd2yypa")))

(define-public crate-titlecase-2.1.0 (c (n "titlecase") (v "2.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0iz09f9hv3kdpjbyg36b3p0dixpqc1s6ry8c31r4sckg2l5wycc4")))

(define-public crate-titlecase-2.2.0 (c (n "titlecase") (v "2.2.0") (d (list (d (n "joinery") (r "<3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0jba1p2dqfy8rbj71l3h7qnyl9hfdnjdkblxrwmrqwyviz685316")))

(define-public crate-titlecase-2.2.1 (c (n "titlecase") (v "2.2.1") (d (list (d (n "joinery") (r "<3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0hi0hkh2x78rvq7rhdgdzsgwcnlpvvb59hgnifsgwz01vf67lf9q")))

(define-public crate-titlecase-3.0.0 (c (n "titlecase") (v "3.0.0") (d (list (d (n "regex") (r "^1.10") (f (quote ("std" "perf" "unicode-perl"))) (k 0)))) (h "05650b7k1pj0yddd150bdnk91jqnsq50cyxg462yqm7fnv3j6cmv") (r "1.70.0")))

(define-public crate-titlecase-3.1.1 (c (n "titlecase") (v "3.1.1") (d (list (d (n "regex") (r "^1.10") (f (quote ("std" "perf" "unicode-perl"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "06cya9x174k6w1197gv0za0i4d48k7a1fdyfb7c4gzs87w87jinc") (r "1.70.0")))

(define-public crate-titlecase-3.2.0 (c (n "titlecase") (v "3.2.0") (d (list (d (n "regex") (r "^1.10") (f (quote ("std" "unicode-perl"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "1yinddgqqhf6m7gln5x1m04yrslcr90p0j1i7n6mqf75q89f7idc") (f (quote (("perf" "regex/perf") ("default" "perf")))) (r "1.70.0")))

