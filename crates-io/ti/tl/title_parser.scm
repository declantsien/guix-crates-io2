(define-module (crates-io ti tl title_parser) #:use-module (crates-io))

(define-public crate-title_parser-0.1.0 (c (n "title_parser") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vxi6831iwxp5x3zmlfp05ykavfn4kxh16jhnbvgyh22sfbxgd0r")))

(define-public crate-title_parser-0.1.1 (c (n "title_parser") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kcsnqqz0fmvzps99lv6wpy9602dh1hl7k1gcs1mv2ba0v2rsxw5")))

(define-public crate-title_parser-0.1.2 (c (n "title_parser") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04vzzhs1h4fr1wdfw2pa1q1gds5ak2kmc4zzxwlzzq63y52s5ydr")))

