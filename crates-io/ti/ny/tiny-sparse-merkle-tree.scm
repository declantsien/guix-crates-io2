(define-module (crates-io ti ny tiny-sparse-merkle-tree) #:use-module (crates-io))

(define-public crate-tiny-sparse-merkle-tree-0.4.0 (c (n "tiny-sparse-merkle-tree") (v "0.4.0") (d (list (d (n "array-bytes") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (o #t) (d #t) (k 0)))) (h "02b4w47n50g04y28g8hq2w08b4c4gjwrk032mpcn78qzgicccs3r") (f (quote (("keccak" "tiny-keccak/keccak") ("debug"))))))

