(define-module (crates-io ti ny tiny2) #:use-module (crates-io))

(define-public crate-tiny2-0.1.0 (c (n "tiny2") (v "0.1.0") (d (list (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "errno") (r "^0") (d #t) (k 0)) (d (n "glob") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1qby08lw2n9y7gl8if8ivjck7ryfghn4lmnlxg883y550jh4x9xw")))

