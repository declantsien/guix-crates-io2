(define-module (crates-io ti ny tiny_file_server) #:use-module (crates-io))

(define-public crate-tiny_file_server-0.1.0 (c (n "tiny_file_server") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7") (d #t) (k 0)))) (h "07sq64hjjjj8n7sjssq2fmzplq2kf7zk0k2s40kdzciwak3gp0lc")))

(define-public crate-tiny_file_server-0.1.1 (c (n "tiny_file_server") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.7") (d #t) (k 0)))) (h "0wgfqackn0pc67hq85nlix34zn8f5q61dv1w6m7i7spkvfggn51h")))

(define-public crate-tiny_file_server-0.1.2 (c (n "tiny_file_server") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "1jmgcsspbw4jz128as9ckp8f7c1vnlw49bhvvbhh5ipjhqknhc5n")))

(define-public crate-tiny_file_server-0.1.3 (c (n "tiny_file_server") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "0w4fz9bmd3qclwnp9q5yl40lpkf56lsclzdhwvinmw6kis6ay8xx")))

(define-public crate-tiny_file_server-0.1.4 (c (n "tiny_file_server") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "01gff04pwvqv5qx5crwm1xpzifq4dfm1k60dqrh4z2njxgi37844")))

(define-public crate-tiny_file_server-0.1.5 (c (n "tiny_file_server") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "1s394pzap586b5hm18p5sq2a5p32d6i66mfa68x5qlkl1wzcwpws")))

