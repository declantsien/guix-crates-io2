(define-module (crates-io ti ny tiny_ecs) #:use-module (crates-io))

(define-public crate-tiny_ecs-0.9.0 (c (n "tiny_ecs") (v "0.9.0") (h "05a98rcyc52krl1v1b3gmkj75y82djydndkp3qd70jrn3k4s6v02")))

(define-public crate-tiny_ecs-0.9.1 (c (n "tiny_ecs") (v "0.9.1") (h "11dpi7xq6zp1y2qh369zm6wzqwca6wba9839qdfri8a4343v2qhz")))

(define-public crate-tiny_ecs-0.9.2 (c (n "tiny_ecs") (v "0.9.2") (h "1j1mkrdhbjnhqkxxi4g35ql9c2cf4iwqdqm5aac8pav86wn5fw1p")))

(define-public crate-tiny_ecs-0.9.3 (c (n "tiny_ecs") (v "0.9.3") (h "0cra3afdjba48andqqgfvvrkv8n0ngxwn9brd257r9dzc52yvw9h")))

(define-public crate-tiny_ecs-0.9.4 (c (n "tiny_ecs") (v "0.9.4") (h "13ksjcqvs0ag932kx4l7hnnydkqn7crkwdrsp3x2xd35xai60wcc")))

(define-public crate-tiny_ecs-0.9.5 (c (n "tiny_ecs") (v "0.9.5") (h "1ih7sba087aa58cfgcvm4lka5gjv5mls6i6h0b5ahhyxwxdabbly")))

(define-public crate-tiny_ecs-0.9.6 (c (n "tiny_ecs") (v "0.9.6") (h "1gxjhshb0hs4kg8xfldx92695zp6b255qlqy8kl18z7zz38fmnyb")))

(define-public crate-tiny_ecs-0.10.0 (c (n "tiny_ecs") (v "0.10.0") (h "0m18n2fsfwaxq6cqpysq3bz9p4r23dbav2picy466v4dwyzfxfdb")))

(define-public crate-tiny_ecs-0.11.0 (c (n "tiny_ecs") (v "0.11.0") (h "1203na17ss7ff2xzhxjjjpp14i363klp5zb1bfm1qc2xa3i9labq")))

(define-public crate-tiny_ecs-0.13.3 (c (n "tiny_ecs") (v "0.13.3") (h "0ii77qxhmckb64i7ah9qrnz40xkr6sgbs9pc198gr9xdz04sijsf")))

(define-public crate-tiny_ecs-0.13.4 (c (n "tiny_ecs") (v "0.13.4") (h "1vq7y825ki166x02ay9l3zbyi7q3as5g55za7lcw69nv304iqd18")))

(define-public crate-tiny_ecs-0.15.2 (c (n "tiny_ecs") (v "0.15.2") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1xjkpmjrgh3pfyv2cazp20gv96xgd5pj1891d9m1n7pb0xc24jvz") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.15.3 (c (n "tiny_ecs") (v "0.15.3") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "19m9a1dgag6rqvkgl410s6c7hbrvf2fl8rlckd3y5snmrv2zq02d") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.15.4 (c (n "tiny_ecs") (v "0.15.4") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1afrkss0b6p3cpmj6a66zz15gl3d32zms5mhda994hnn4ys9xcgj") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.15.5 (c (n "tiny_ecs") (v "0.15.5") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1l85jksjylyhhz5jhbbc3rq9ms7r8gkxqyd32jar1siqppvggkl1") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.16.0 (c (n "tiny_ecs") (v "0.16.0") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0ivd33sv6ai78gh899wbnxkfq9xbj35ishxr4mkqzj6h19p8i136") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.0 (c (n "tiny_ecs") (v "0.17.0") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1n8q8nikd48n46j0gyp4smrppw2bwrbnzywni0knwq1g6p9qd8h1") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.1 (c (n "tiny_ecs") (v "0.17.1") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "092ks7s6ai0r59xj5517gl8dzkdfhb0nprppp3hyyfnb6vr6pxvn") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.2 (c (n "tiny_ecs") (v "0.17.2") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "11vgj78lrmk1ymkf7pg43zkd0jvnd9am0imc09i7m85nc1vyyd6p") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.3 (c (n "tiny_ecs") (v "0.17.3") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "09p3d5yn6336x9j4jdvznvkxzc5g0fhqgvwk4d5rckj38qakns7b") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.4 (c (n "tiny_ecs") (v "0.17.4") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0cwq5bj1s67yw99dz73nsk2rilj42dslwj1wzf5knan4jf7hda91") (f (quote (("default") ("bitmask_max_32"))))))

(define-public crate-tiny_ecs-0.17.5 (c (n "tiny_ecs") (v "0.17.5") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1gsqy2i5h433ay86m95bzk8al4ls4yicgx340mry21b61rmpbqqy") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.17.6 (c (n "tiny_ecs") (v "0.17.6") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0i00iawyb4xzrxwzzpm85hps7v4b9vimh6306jn773jlh7bpc5sy") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.18.0 (c (n "tiny_ecs") (v "0.18.0") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "00jnfrrlkpmw51fcpnbiv8qm5k7wn1xg3kb1zcq845jb4iyczksj") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.18.1 (c (n "tiny_ecs") (v "0.18.1") (d (list (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "08rjw8i4n8yy2fif5dcslm7pb1rdbmk3aqn4z3m1gaa87hj7hy6p") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.0 (c (n "tiny_ecs") (v "0.19.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1yg94ih71xxpgqqxapv8cfqwzsxhs2qqkmxgga5jfc4vjkdya6m4") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.1 (c (n "tiny_ecs") (v "0.19.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "034c284kmba3vl2mn4fk3vwxxrr3r3fs621s00mqajmba25yvpdg") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.2 (c (n "tiny_ecs") (v "0.19.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0xp79419hhq7fashyvmzgbzmazp6b1cn1hhzwzs4489n3c01fmpm") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.3 (c (n "tiny_ecs") (v "0.19.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0j8qm57fyymv687ha6ayp0v2bn49iq9plfli824ql6q4gjxvcbl2") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.4 (c (n "tiny_ecs") (v "0.19.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "00ywf7l2mqjgphmqqpis3p7wwrpmxhc2gkngc1sqi7h1gfsa0k2a") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.5 (c (n "tiny_ecs") (v "0.19.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "persist-o-vec") (r "^0.3.1") (d #t) (k 0)))) (h "0d8yvw9bych01rqljpadj9gdhf1bf5h3xblccmqdkxlnqp8vv9v6") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

(define-public crate-tiny_ecs-0.19.6 (c (n "tiny_ecs") (v "0.19.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.8") (d #t) (k 0)) (d (n "persist-o-vec") (r "^0.3.1") (d #t) (k 0)))) (h "094biwbicr61qz3vnvix4fnadjxc4x5h0zv3252x31f3w40j7qwg") (f (quote (("default" "component_max_63") ("component_max_63") ("component_max_31") ("component_max_127"))))))

