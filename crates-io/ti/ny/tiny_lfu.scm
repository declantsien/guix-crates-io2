(define-module (crates-io ti ny tiny_lfu) #:use-module (crates-io))

(define-public crate-tiny_lfu-0.1.0 (c (n "tiny_lfu") (v "0.1.0") (d (list (d (n "biometrics") (r "^0.1.1") (d #t) (k 0)) (d (n "bloomcalc") (r "^0.1.0") (d #t) (k 0)))) (h "1nl3bvpimn02pg0vlksa7mk93l8vah925n08g4wcrk7hisk79yi4")))

(define-public crate-tiny_lfu-0.1.1 (c (n "tiny_lfu") (v "0.1.1") (d (list (d (n "biometrics") (r "^0.2") (d #t) (k 0)) (d (n "bloomcalc") (r "^0.1") (d #t) (k 0)))) (h "1gqbrpc854k0w6x32l44bc9b2krjql72cax5kwwl22mkrc8n27gs")))

(define-public crate-tiny_lfu-0.2.0 (c (n "tiny_lfu") (v "0.2.0") (d (list (d (n "biometrics") (r "^0.4") (d #t) (k 0)) (d (n "bloomcalc") (r "^0.2") (d #t) (k 0)))) (h "1jb2d4zkfjcgps45r41x8n70jxlpfy0d8s68cilhr8mdyyxkrflc")))

(define-public crate-tiny_lfu-0.3.0 (c (n "tiny_lfu") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)) (d (n "bloomcalc") (r "^0.3") (d #t) (k 0)))) (h "17qypgndzdp4qxn0xpy8aljzjg4x2238a6pci7nfwj6sv1zj5394")))

