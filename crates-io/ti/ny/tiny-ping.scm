(define-module (crates-io ti ny tiny-ping) #:use-module (crates-io))

(define-public crate-tiny-ping-0.3.0 (c (n "tiny-ping") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)))) (h "0vrfcf446xgq77n9cn1mivl3ladnfp3v6l2qx2ajdckhqjz9m1h7")))

(define-public crate-tiny-ping-0.3.1 (c (n "tiny-ping") (v "0.3.1") (d (list (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)))) (h "0d271y3m8gikcc0xayimwch80ib0cdli6qpz4d5vh8dylkcl13bj")))

(define-public crate-tiny-ping-0.3.2 (c (n "tiny-ping") (v "0.3.2") (d (list (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)))) (h "14wnvva9s2agq6malr1wdqs8nknh5k6aq9c5c2lz0d8x04j9qm5n")))

(define-public crate-tiny-ping-0.3.3 (c (n "tiny-ping") (v "0.3.3") (d (list (d (n "socket2") (r "^0.3") (d #t) (k 0)))) (h "0y76psdh0yxzdyqpdvfasiwfmkk1wd7x1hc52469zijckg6g8ms2")))

(define-public crate-tiny-ping-0.4.0 (c (n "tiny-ping") (v "0.4.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "08vnhhmxvc7jp3yriqq3sav5c8h82kc8qdkrchb478bgqlnh1zfs")))

(define-public crate-tiny-ping-0.4.1 (c (n "tiny-ping") (v "0.4.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "1j8ghc9m7l5xhjzjxldjma7231zmsgqiscq76wyn5z9h3iv8955h")))

(define-public crate-tiny-ping-0.4.2 (c (n "tiny-ping") (v "0.4.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.27") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "1qdiq84rs3ms24fic4mdiqfwq4fyvj8fn4bsmvick4g41azf30yn")))

(define-public crate-tiny-ping-0.5.0 (c (n "tiny-ping") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("net" "rt" "time"))) (d #t) (k 0)))) (h "09zy1p8kyskphabk6v3zv6i0rphpskv6hakh35gfnxsy8i1iamaz")))

