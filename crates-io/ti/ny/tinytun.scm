(define-module (crates-io ti ny tinytun) #:use-module (crates-io))

(define-public crate-tinytun-0.0.1 (c (n "tinytun") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "h2") (r "^0.3.18") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("client" "http1"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.0") (f (quote ("http1"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("io-std" "net"))) (d #t) (k 0)))) (h "0sgb40c9y548ssqvawqz1h3b98rm9pk9cr9hf3q5d78h75wfdzcj")))

(define-public crate-tinytun-0.1.0 (c (n "tinytun") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "h2") (r "^0.3.18") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("client" "http1"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.24.0") (f (quote ("http1"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("io-std" "net"))) (d #t) (k 0)))) (h "0crbkhhvxwpbjajz2avgzqvv7ky1gdcrgf81wsspssm6wa9nr9iz")))

