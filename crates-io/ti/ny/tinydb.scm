(define-module (crates-io ti ny tinydb) #:use-module (crates-io))

(define-public crate-tinydb-0.0.0 (c (n "tinydb") (v "0.0.0") (h "1z26gxk9pcsby53fl1wmql8w3n6g820i8i1ii057wf3jx2crxzav")))

(define-public crate-tinydb-0.0.1 (c (n "tinydb") (v "0.0.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yvilhw554v3g4lrv1zycm7l44hgxsdm11v7xbs6ymfslmalvary")))

(define-public crate-tinydb-0.0.2 (c (n "tinydb") (v "0.0.2") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14rd5iqnfwsm8ahrzavfd6y2g7s459flc51an3calgi9gf2a0429")))

(define-public crate-tinydb-0.0.3 (c (n "tinydb") (v "0.0.3") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iifycyh7wnd1ifzgvw7rjmqrpwqrbl42fhk45r96a5w44iv41is")))

(define-public crate-tinydb-0.0.4 (c (n "tinydb") (v "0.0.4") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i2aslkiyih0pb14zk962sl018yyr6m5zsacaq98h129ciya46q3")))

(define-public crate-tinydb-0.0.5 (c (n "tinydb") (v "0.0.5") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1idjmxhj96qn05zbbags0qzjn96b13ypi1lsrzndv3vb4fa8417q")))

(define-public crate-tinydb-0.0.6 (c (n "tinydb") (v "0.0.6") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04jrbmzpl21cs16l9027kg0nss3zwwja8ccy5h3hb4zfrxw8mgmc")))

(define-public crate-tinydb-0.0.7 (c (n "tinydb") (v "0.0.7") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0illmmb754pyv0v1kd8mny4gzq56pf9rn4rqf81zxmzvb895h09l")))

(define-public crate-tinydb-1.0.0 (c (n "tinydb") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01km2llvxq35pqd4mkhb1sy403kik5z5rxms3q8gil8kqcz4pmp3")))

