(define-module (crates-io ti ny tiny_http_sccache) #:use-module (crates-io))

(define-public crate-tiny_http_sccache-0.7.0 (c (n "tiny_http_sccache") (v "0.7.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chunked_transfer") (r "^1") (d #t) (k 0)) (d (n "fdlimit") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "sha1") (r "^0.6.0") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "02szs62cariw00cr515mb5kqkqkcrmarb7hr45dh86kc0a7j7c47") (f (quote (("ssl" "openssl") ("default")))) (y #t)))

