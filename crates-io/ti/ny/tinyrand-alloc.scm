(define-module (crates-io ti ny tinyrand-alloc) #:use-module (crates-io))

(define-public crate-tinyrand-alloc-0.3.0 (c (n "tinyrand-alloc") (v "0.3.0") (d (list (d (n "tinyrand") (r "^0.3.0") (d #t) (k 0)))) (h "1cgbhz85r9bp0ar4w7382gdxyj7bz0ryzaglcf1m071pjfm3s20q")))

(define-public crate-tinyrand-alloc-0.4.0 (c (n "tinyrand-alloc") (v "0.4.0") (d (list (d (n "tinyrand") (r "^0.4.0") (d #t) (k 0)))) (h "1czxw90pddcr051iplkc6clffc362qxwyvxmjlvjkaanb3afgm63")))

(define-public crate-tinyrand-alloc-0.5.0 (c (n "tinyrand-alloc") (v "0.5.0") (d (list (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "1f26kb4ckn7ykgb4blad8ryr4pz1dpmx0axxkhkips9v2zhh66ii")))

