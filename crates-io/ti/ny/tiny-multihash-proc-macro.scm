(define-module (crates-io ti ny tiny-multihash-proc-macro) #:use-module (crates-io))

(define-public crate-tiny-multihash-proc-macro-0.1.0 (c (n "tiny-multihash-proc-macro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "15a1clk5pzc7kg6rwpnd81m2sgw6js3vjv0nh48qq5i78fahwb91")))

