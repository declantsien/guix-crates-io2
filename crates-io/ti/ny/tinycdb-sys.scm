(define-module (crates-io ti ny tinycdb-sys) #:use-module (crates-io))

(define-public crate-tinycdb-sys-0.0.1 (c (n "tinycdb-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0wnrzfpimg9msxr2n6qvjnpayx1ysgdpz3r3j2a7n3bdic8zcj9j")))

(define-public crate-tinycdb-sys-0.0.2 (c (n "tinycdb-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "16bjymlsmrx7jh2k0c1xsafvvv7nnxsyh4b1fj2cx8xpjrpdx7k9")))

