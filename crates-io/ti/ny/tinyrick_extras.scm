(define-module (crates-io ti ny tinyrick_extras) #:use-module (crates-io))

(define-public crate-tinyrick_extras-0.0.1 (c (n "tinyrick_extras") (v "0.0.1") (d (list (d (n "tinyrick") (r "^0.0.6") (d #t) (k 0)))) (h "0zn6pgwwqfqa0ayqngpdcbl7bihnx7x4nmb4mhgfn7bqjxjaxr5n") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.2 (c (n "tinyrick_extras") (v "0.0.2") (d (list (d (n "tinyrick") (r "^0.0.8") (d #t) (k 0)))) (h "09zsc4vj2l4cm26w9a2gglrqd17ld64slcbjh1kcysnh5ya8gxqd") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.3 (c (n "tinyrick_extras") (v "0.0.3") (d (list (d (n "tinyrick") (r "^0.0.9") (d #t) (k 0)))) (h "1i6vmqk0l0gsm4yn80wly5rhhi324jf6f39mzhrri5g555fgj2gb") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.4 (c (n "tinyrick_extras") (v "0.0.4") (d (list (d (n "tinyrick") (r "^0.0.9") (d #t) (k 0)))) (h "116az47zj4xyfd8ija712c7jnnm45my8fz8jx9yk1ixj5paf81qv") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.5 (c (n "tinyrick_extras") (v "0.0.5") (d (list (d (n "tinyrick") (r "^0.0.9") (d #t) (k 0)))) (h "1k6dm2dgllw34qp34k6dkgw4i700f3dz0qnh99xbw2iv8chwy37x") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.6 (c (n "tinyrick_extras") (v "0.0.6") (d (list (d (n "tinyrick") (r "^0.0.9") (d #t) (k 0)))) (h "06q43j4qw7zr01s37ydcjn5djf33gxrdk3qxzg47cibr8xm4k6m5") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.7 (c (n "tinyrick_extras") (v "0.0.7") (d (list (d (n "tinyrick") (r "^0.0.13") (d #t) (k 0)))) (h "05rxj36mygc27ab19jjxqhyflr11iw83h1navd2vswmlxfflzlzi") (f (quote (("letmeout"))))))

(define-public crate-tinyrick_extras-0.0.8 (c (n "tinyrick_extras") (v "0.0.8") (d (list (d (n "tinyrick") (r "^0.0.13") (d #t) (k 0)))) (h "1a7qm307jqcs4py0s0cnal50a9vp8hgkh6warmxcdfbra3vk3bja") (f (quote (("letmeout"))))))

