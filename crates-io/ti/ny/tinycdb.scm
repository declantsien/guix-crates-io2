(define-module (crates-io ti ny tinycdb) #:use-module (crates-io))

(define-public crate-tinycdb-0.0.1 (c (n "tinycdb") (v "0.0.1") (h "0cjp53mdwpzn8pa771fwx36jzki9dkxifjsz7jwp72zqlmhznvgc")))

(define-public crate-tinycdb-0.0.2 (c (n "tinycdb") (v "0.0.2") (h "0bk3y00zlxw9dvswhh95wr1vwcbg6hrijfmp7axibfsv4qlxjfyz")))

(define-public crate-tinycdb-0.0.3 (c (n "tinycdb") (v "0.0.3") (h "0cwfjwjz58q9dlqb6np2dpc64yxdzs5zi46midbyzllvcac2z3fh")))

(define-public crate-tinycdb-0.0.4 (c (n "tinycdb") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lz4") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "tinycdb-sys") (r "^0.0.1") (d #t) (k 0)))) (h "17arcyqf6dvni0m8a0qizpg3bdky30sf5qjbwrhr3nyr2wzfzbbw")))

(define-public crate-tinycdb-0.0.5 (c (n "tinycdb") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "lz4") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)) (d (n "tinycdb-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1al0b8qvbhdplplik5ja4ldk3bi0nwhym1asgpsx273gv1i7a6yf")))

(define-public crate-tinycdb-0.0.6 (c (n "tinycdb") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lz4") (r "^1.9") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "tinycdb-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1rgrdjdgss6z4rb4sgazika1gby8fry9ipwkk4gp517zazkwrlfn")))

(define-public crate-tinycdb-0.0.7 (c (n "tinycdb") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lz4") (r "^1.9") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "tinycdb-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0baij6hmpmc1ffk619a74q6cgf0l2nnn2i4af09gbz8cwvwxadqw")))

