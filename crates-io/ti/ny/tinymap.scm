(define-module (crates-io ti ny tinymap) #:use-module (crates-io))

(define-public crate-tinymap-0.1.0 (c (n "tinymap") (v "0.1.0") (d (list (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "18zm2hjhlsv6b55xwhyibbsrqgd80bxfnkgkijvmiawxy4l66njl")))

(define-public crate-tinymap-0.2.0 (c (n "tinymap") (v "0.2.0") (d (list (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "1fm8ml076iahmjjq0wbfaflxrhj5h2x1kmv6k3k924hcygf855bg")))

(define-public crate-tinymap-0.2.1 (c (n "tinymap") (v "0.2.1") (d (list (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "16hy4dv8s0pkjbb9lnaiciw9g6kg8ghjpw76vl0k8k6m27bhl1pq")))

(define-public crate-tinymap-0.2.2 (c (n "tinymap") (v "0.2.2") (d (list (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "0gngl3dp7xilyqs2cw0ndslz8qmcxhfzjpb16p9aznk396j8bgnh")))

(define-public crate-tinymap-0.2.3 (c (n "tinymap") (v "0.2.3") (d (list (d (n "tinyvec") (r "^0.4.1") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "07ifk051izcgbjv4h6dj6v4zl06r0ar32n3v15qhkz55w0fk42n8")))

(define-public crate-tinymap-0.2.4 (c (n "tinymap") (v "0.2.4") (d (list (d (n "tinyvec") (r "^1.0.0") (f (quote ("nightly_const_generics"))) (d #t) (k 0)))) (h "1mhcb3m76djqhjmxylyz92d8l129gwkfs5lcqcrhh796qx585ib9")))

(define-public crate-tinymap-0.4.0 (c (n "tinymap") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.4") (f (quote ("rustc_1_55"))) (d #t) (k 0)))) (h "0hzzfv4z8k6f1i7w3jmab95kcgs24h3kldrzz0hxzkayfzx2ask0") (f (quote (("default") ("alloc" "hashbrown")))) (r "1.61")))

