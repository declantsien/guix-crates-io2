(define-module (crates-io ti ny tiny_fail) #:use-module (crates-io))

(define-public crate-tiny_fail-0.1.0 (c (n "tiny_fail") (v "0.1.0") (h "0qfzzscwk14y96rsy60gsfxzagzjaca31scxgbq478rbh6kgds6c")))

(define-public crate-tiny_fail-0.2.0 (c (n "tiny_fail") (v "0.2.0") (h "0fvyqm621g3cypmv79zn0glxswwx8sn3n5dg4g4fzjl83bnay5d4")))

(define-public crate-tiny_fail-0.2.1 (c (n "tiny_fail") (v "0.2.1") (h "0jc7svxkwazjbvxd75b8vba1jawfcbf4x0zgy1kny4mk35j70mg4")))

