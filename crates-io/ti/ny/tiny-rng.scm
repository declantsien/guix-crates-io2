(define-module (crates-io ti ny tiny-rng) #:use-module (crates-io))

(define-public crate-tiny-rng-0.1.0 (c (n "tiny-rng") (v "0.1.0") (h "0s4sdvgpc188xvwn66j67d491wrqwgn8xrxd44pv968bl574sgmy")))

(define-public crate-tiny-rng-0.2.0 (c (n "tiny-rng") (v "0.2.0") (h "1pblg7zx51j55aw6ndyp7s0y6i10yv695vy4wdzcd80pcql2hdnn")))

