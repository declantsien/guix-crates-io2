(define-module (crates-io ti ny tiny-terminal-snake) #:use-module (crates-io))

(define-public crate-tiny-terminal-snake-0.1.0 (c (n "tiny-terminal-snake") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "19r94fwlkn67v4bsj3kfnwp241m0jzb7850a2vzm7rqp0skwsdaw")))

(define-public crate-tiny-terminal-snake-0.1.1 (c (n "tiny-terminal-snake") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0vq8k7h38rl0gk5f083b6lbwqvx67kxx8f7snh6s28ijy3scyp8n")))

(define-public crate-tiny-terminal-snake-0.1.2 (c (n "tiny-terminal-snake") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0g2lrqfll0jrdf8ga009ny51r5si327n3485d5mbl3v8f7izyvmq")))

(define-public crate-tiny-terminal-snake-0.1.3 (c (n "tiny-terminal-snake") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1z0anksglfjl28k4m3zlp2sm080d81a7mzb8f9w91sbhk1lv5cnm")))

(define-public crate-tiny-terminal-snake-0.1.5 (c (n "tiny-terminal-snake") (v "0.1.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1clmk2gdzrs2px3rx5pv0dngk0wmcfz01hzaq3bh6lqbf3q7rg08")))

(define-public crate-tiny-terminal-snake-0.1.6 (c (n "tiny-terminal-snake") (v "0.1.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0zi85lnxyn4xvizc3s1ny4yih2j81cxymd05jyrmrpyly1dhlybg")))

(define-public crate-tiny-terminal-snake-0.1.7 (c (n "tiny-terminal-snake") (v "0.1.7") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0fgr8pbb9avp4v89p90prpq55v6jw76czw8aswkz6nmxfkwl6ghs")))

(define-public crate-tiny-terminal-snake-0.2.0 (c (n "tiny-terminal-snake") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1jmi3z3i19brwcpfggjh3rynarzdwy1rhq123d7d00ckz0fay4g5")))

(define-public crate-tiny-terminal-snake-0.2.1 (c (n "tiny-terminal-snake") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^2") (d #t) (k 0)))) (h "1h34kn2whxdc8x3dcpl95b011zs5k9995wbdmqr9fz6ixal9v2d4")))

