(define-module (crates-io ti ny tinypointers) #:use-module (crates-io))

(define-public crate-tinypointers-0.1.0 (c (n "tinypointers") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "00fkyvh0916bwgj5kkdqbv64b04zn0y3bpkb2n24bgpvqqy3pwvp") (f (quote (("default" "2byteid") ("2byteid") ("1byteid")))) (y #t)))

(define-public crate-tinypointers-0.2.0 (c (n "tinypointers") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "04igfb1yc040wid9wms9b2bafjid5n0jdqvx43kn96wb675w106x") (f (quote (("default" "2byteid") ("2byteid") ("1byteid")))) (y #t)))

(define-public crate-tinypointers-0.2.1 (c (n "tinypointers") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0w1dradjwyn49wmk8m4d5jdf8lcwlj9n5gm0zlg6rkmcbj3im8zj") (f (quote (("default" "2byteid") ("2byteid") ("1byteid")))) (y #t)))

(define-public crate-tinypointers-0.2.2 (c (n "tinypointers") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1z6zmbmfv29m0ygy8sdzyy7g395zfcbkhwhgngpf49wi2mnnz7na") (f (quote (("default" "2byteid") ("2byteid") ("1byteid"))))))

