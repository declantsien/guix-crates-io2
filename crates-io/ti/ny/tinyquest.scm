(define-module (crates-io ti ny tinyquest) #:use-module (crates-io))

(define-public crate-tinyquest-0.1.0 (c (n "tinyquest") (v "0.1.0") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1aw4qi24l0fdzfgbjg3j9mv95h143gikdrvzcaqcqm1qzshbqdw7") (f (quote (("support" "rustls" "webpki" "webpki-roots"))))))

(define-public crate-tinyquest-0.2.0 (c (n "tinyquest") (v "0.2.0") (d (list (d (n "chunked_transfer") (r "^1.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1p8w075gs56k564k8dlfbhbwg4cj3kxi0xzliys4mnpj98s9sl65") (f (quote (("support" "rustls" "webpki" "webpki-roots"))))))

(define-public crate-tinyquest-0.3.0 (c (n "tinyquest") (v "0.3.0") (d (list (d (n "chunked_transfer") (r "^1.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "rustls") (r "^0.18.1") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1jbvdxlydv8bg5ic1ws7gylkag03sk09ngvs5a612nl2i0ax2mjc") (f (quote (("support" "rustls" "webpki" "webpki-roots"))))))

(define-public crate-tinyquest-0.4.0 (c (n "tinyquest") (v "0.4.0") (d (list (d (n "chunked_transfer") (r "^1.2") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "0fnw0gan78p4zdbxaxcx74qh3yj1a9w4pkbpjma6ircrxii7kaqd")))

(define-public crate-tinyquest-0.4.1 (c (n "tinyquest") (v "0.4.1") (d (list (d (n "chunked_transfer") (r ">=1.2.0, <2.0.0") (d #t) (k 0)) (d (n "http") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "native-tls") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "073n51j9hb1snv754sbrgw3z46lsf4md17yvbd588q7brark8b6m")))

