(define-module (crates-io ti ny tiny-bitstream) #:use-module (crates-io))

(define-public crate-tiny-bitstream-0.1.0 (c (n "tiny-bitstream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "11yshdn798mh7ygcjzngfmv175n8896s0ynr9yb864wazbzfdakp") (f (quote (("checked"))))))

