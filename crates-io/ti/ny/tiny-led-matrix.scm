(define-module (crates-io ti ny tiny-led-matrix) #:use-module (crates-io))

(define-public crate-tiny-led-matrix-0.1.0 (c (n "tiny-led-matrix") (v "0.1.0") (h "1p0ayv2zafl1hvzpsjfqs01bzxpq232ia8ddhhfa8lyn532491j3")))

(define-public crate-tiny-led-matrix-1.0.0 (c (n "tiny-led-matrix") (v "1.0.0") (h "1qrpwnin2hls14bzxxax4bs25qky17f0iva5mvcmfvpbbifw6mch")))

(define-public crate-tiny-led-matrix-1.0.1 (c (n "tiny-led-matrix") (v "1.0.1") (h "08402cryi9aa7hifqdd2wvssbca9kms8slygzk2sakclzigr0qlc")))

(define-public crate-tiny-led-matrix-1.0.2 (c (n "tiny-led-matrix") (v "1.0.2") (h "0aw8g3rjgjrm4316nimbrc0zfml9piz0vxqkg5y4l5c6nqkwf657")))

