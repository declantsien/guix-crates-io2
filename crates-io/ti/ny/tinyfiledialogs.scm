(define-module (crates-io ti ny tinyfiledialogs) #:use-module (crates-io))

(define-public crate-tinyfiledialogs-2.5.7 (c (n "tinyfiledialogs") (v "2.5.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "129dncg3ch4g49m9ydjp2iwj7fm3r20ws8ikbavhrs7dza4938aq")))

(define-public crate-tinyfiledialogs-2.5.9 (c (n "tinyfiledialogs") (v "2.5.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0cbmgylg96cxj88pjigyf2p71nc7bgdx7k2gbwyskb3irmc16h0x")))

(define-public crate-tinyfiledialogs-3.0.4 (c (n "tinyfiledialogs") (v "3.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1p66n68m472w3a292zdyqgdgiq0b98fi5hv14dd8kqm9jmrmyanr")))

(define-public crate-tinyfiledialogs-3.3.5 (c (n "tinyfiledialogs") (v "3.3.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1j600mh38gvlycdx2v0msnnr19zq1yx53dm7lrp74g295k53766p")))

(define-public crate-tinyfiledialogs-3.3.6 (c (n "tinyfiledialogs") (v "3.3.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0j3x9bavx6iq89wkywrcnzyznishdpqlx6kci7pziki18hjwga5f")))

(define-public crate-tinyfiledialogs-3.3.9 (c (n "tinyfiledialogs") (v "3.3.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0m35s01332rk419nfb65w79il6v19wjc4614v14h6hr1dl6vb3l4")))

(define-public crate-tinyfiledialogs-3.3.10 (c (n "tinyfiledialogs") (v "3.3.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1531m8wfciwjdvshl0qpc1vaz1gn2dzns7z5asssin9p7xnb4py4")))

(define-public crate-tinyfiledialogs-3.8.3 (c (n "tinyfiledialogs") (v "3.8.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ssfllapvwx77m5cbyyjxfdxhvq9mbxr4pna1kbsgdxwbhvv4icm")))

(define-public crate-tinyfiledialogs-3.9.0 (c (n "tinyfiledialogs") (v "3.9.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1sfx2wddmd6dg8vwn53iwlabrynn6gzfbnvhskhy89iwd9i7gidd")))

(define-public crate-tinyfiledialogs-3.9.1 (c (n "tinyfiledialogs") (v "3.9.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0s55kkzai2rn3fnvwia4lgmpp9d57zgrdb6pqqn6wmm68fya0pz2") (f (quote (("windows-hidpi") ("default" "windows-hidpi"))))))

