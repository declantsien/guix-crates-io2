(define-module (crates-io ti ny tiny_ram_db) #:use-module (crates-io))

(define-public crate-tiny_ram_db-0.1.0 (c (n "tiny_ram_db") (v "0.1.0") (h "0bq8m6spklml8k3ydk750nsf4991wlw6fk6hnfva80hrmbf2p4h8")))

(define-public crate-tiny_ram_db-0.1.1 (c (n "tiny_ram_db") (v "0.1.1") (h "0bsmlzhlm253x111vhi3xy99vv3l0bjlgipdi3jffzzs7c6m0pcp")))

(define-public crate-tiny_ram_db-0.1.2 (c (n "tiny_ram_db") (v "0.1.2") (h "1zwyj2qwbc68d8xw0m6jsd23d0m95fkkdsdpds6z7x62iv76saav")))

(define-public crate-tiny_ram_db-0.1.3 (c (n "tiny_ram_db") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)))) (h "0g1f4n5xdxmzx95jh3zdfa7vsm8dykmrhn0srqism1sz5kd9xzrs")))

(define-public crate-tiny_ram_db-0.1.4 (c (n "tiny_ram_db") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1shsw5k7sn1pgxic5d9hyvnsd403iv1nk0l03khkgh2xsb4s248v")))

(define-public crate-tiny_ram_db-0.1.5 (c (n "tiny_ram_db") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0cn8fjgcwz4qrz34kvvkb9pz4ll392wap3qc19maplsah9kk14jv")))

(define-public crate-tiny_ram_db-0.1.6 (c (n "tiny_ram_db") (v "0.1.6") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0kga1imgkx71fs1wlns77wkd0zfhyvd61ma5arnsd4ffiyrbr0h1")))

(define-public crate-tiny_ram_db-0.1.7 (c (n "tiny_ram_db") (v "0.1.7") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "16vz2z5d0kl57icv8s66gjcd65d0jabajfv1l8qd30g37d1cy337")))

(define-public crate-tiny_ram_db-0.1.8 (c (n "tiny_ram_db") (v "0.1.8") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1c6s3zp4n3sakn8nmfgn1w7zzg3znqg0awv060qd77ar7ky09nks")))

(define-public crate-tiny_ram_db-0.1.9 (c (n "tiny_ram_db") (v "0.1.9") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0fl6j2j2c52fm157icsfajbwzaain7w5gklag5vhdd4w70wdya8c")))

(define-public crate-tiny_ram_db-0.1.10 (c (n "tiny_ram_db") (v "0.1.10") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "08j53hpdc1ipajmsqf8861jbar6fqvr1gk57inbn2qawf49wfh0d")))

(define-public crate-tiny_ram_db-0.1.11 (c (n "tiny_ram_db") (v "0.1.11") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0yhidx6pfjfhh9s0kpwlqnzmdyx88z4f37wkhflp6d21skg722vk")))

(define-public crate-tiny_ram_db-0.1.12 (c (n "tiny_ram_db") (v "0.1.12") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0wbpdgp43hvysjmdc821495fcq59pnpmpigvmgwykyyasdsq3hmi")))

(define-public crate-tiny_ram_db-0.1.13 (c (n "tiny_ram_db") (v "0.1.13") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0ri8c88bgjalyy1wrl7s631lsdw1yyyzhl93hqsabdwzpgfc5kz9")))

(define-public crate-tiny_ram_db-0.1.14 (c (n "tiny_ram_db") (v "0.1.14") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0iajljjcz0bwr2vinsigz2jmm5j652lmhc01ik9aah501k83abjl")))

