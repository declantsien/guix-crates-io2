(define-module (crates-io ti ny tinygif) #:use-module (crates-io))

(define-public crate-tinygif-0.0.1 (c (n "tinygif") (v "0.0.1") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nu-pretty-hex") (r "^0.80.0") (d #t) (k 2)))) (h "0xcr343p86iq2s14ffrk9wlnk0q84mz7hjb90s0yn9f6xn6c7mpk")))

(define-public crate-tinygif-0.0.2 (c (n "tinygif") (v "0.0.2") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nu-pretty-hex") (r "^0.80.0") (d #t) (k 2)))) (h "1gj6b8032h9qlf12ibas54994d3gcm166vysa36fvpgz3ffli7gm")))

(define-public crate-tinygif-0.0.3 (c (n "tinygif") (v "0.0.3") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nu-pretty-hex") (r "^0.81.0") (d #t) (k 2)))) (h "1v2rfamr84wh831gnnck5rw7zygjfjqgw46x7ncjm1xfzjxw8864") (f (quote (("8k"))))))

(define-public crate-tinygif-0.0.4 (c (n "tinygif") (v "0.0.4") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "nu-pretty-hex") (r "^0.81.0") (d #t) (k 2)))) (h "0j520z8316gz5kar0z9j1sabyjrx448ci4ykx26ii1h9f5ml1w89") (f (quote (("8k"))))))

