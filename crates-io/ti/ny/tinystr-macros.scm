(define-module (crates-io ti ny tinystr-macros) #:use-module (crates-io))

(define-public crate-tinystr-macros-0.1.0 (c (n "tinystr-macros") (v "0.1.0") (d (list (d (n "tinystr") (r "^0.3") (d #t) (k 0)))) (h "0c5c361iv5h9brfmhrmz1s7456px5acdd5aqjkazcssfs2playn9")))

(define-public crate-tinystr-macros-0.2.0 (c (n "tinystr-macros") (v "0.2.0") (d (list (d (n "tinystr-raw") (r "^0.1") (d #t) (k 0)))) (h "19kp7v3a8448nmrrj0m94wax2mq8gaz2kigfb0mmqckw258vwraz")))

