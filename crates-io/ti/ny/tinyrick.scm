(define-module (crates-io ti ny tinyrick) #:use-module (crates-io))

(define-public crate-tinyrick-0.0.2 (c (n "tinyrick") (v "0.0.2") (h "0j68ibxwzcddv051mwn6gpa98hixh6av7fxhc2smi3xxkcvylbal")))

(define-public crate-tinyrick-0.0.3 (c (n "tinyrick") (v "0.0.3") (h "1bgdbf2sm5hdvmcxqrm0zsj41ar76pa01kc45a4qah7g94mr8z9h")))

(define-public crate-tinyrick-0.0.4 (c (n "tinyrick") (v "0.0.4") (h "1nb5hv9liy3xmr7by6xkgw4j7bcr2dr0vzqyvvqjahzbi1vpccnh")))

(define-public crate-tinyrick-0.0.5 (c (n "tinyrick") (v "0.0.5") (h "1vzbiki0kpv6mzvfbfbzp7sab74jfqqzp6w3v2r6fbv2gd697k6i")))

(define-public crate-tinyrick-0.0.6 (c (n "tinyrick") (v "0.0.6") (h "0w1wclsj1qnx6nn8sf7c8ax0axgdazwrs3lkpha1z07nkgr529fd")))

(define-public crate-tinyrick-0.0.7 (c (n "tinyrick") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1dnrqv2mpfj82y18aqbxphfd260rsg54krj7k67zn3brv2cdjjqr")))

(define-public crate-tinyrick-0.0.8 (c (n "tinyrick") (v "0.0.8") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0z5jqzfg1l0g28xhycmywqqpn71sq6rx2zvzawvbh2himygbh8cc")))

(define-public crate-tinyrick-0.0.9 (c (n "tinyrick") (v "0.0.9") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1572sxrxfzmzvzfwcr2vx0srp3q7hajlvk6ifci7i2fjj96sqkjh")))

(define-public crate-tinyrick-0.0.11 (c (n "tinyrick") (v "0.0.11") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1w2ip6qwg3kkc0507da1pc48n4y6zw34bf57mbmn331dvy2pds97")))

(define-public crate-tinyrick-0.0.12 (c (n "tinyrick") (v "0.0.12") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04rmr7s9bqdjszq6ff85sb40a0bh6lz5l3r2ah7882z1iy73di7p")))

(define-public crate-tinyrick-0.0.13 (c (n "tinyrick") (v "0.0.13") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zq2632a5aj1lyf8qhy60bxvxr9kcm46yisaf78211a3jkjcs6c2")))

(define-public crate-tinyrick-0.0.14 (c (n "tinyrick") (v "0.0.14") (d (list (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07wbf3rp7r542shfkp23kwvqcsb2l8my22qngc07cgy0b71lyfqb")))

