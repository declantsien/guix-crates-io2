(define-module (crates-io ti ny tinyjson) #:use-module (crates-io))

(define-public crate-tinyjson-0.0.1 (c (n "tinyjson") (v "0.0.1") (h "1jbavzmapr1zp10dz4hr2xby62l1mr7wibg4gpvc75ammman8ck2")))

(define-public crate-tinyjson-0.0.2 (c (n "tinyjson") (v "0.0.2") (h "06yah90dibsnkqlqv59j17z08mskjwzgd959c6a3jf1cfd0yh2ng")))

(define-public crate-tinyjson-0.1.0 (c (n "tinyjson") (v "0.1.0") (h "0mvfp1khmmzm1azs66rwqf9w8nf8l11m388vgh4j2ir6zfam4kpy")))

(define-public crate-tinyjson-1.0.0 (c (n "tinyjson") (v "1.0.0") (h "0pz4xbbn6yrni7vlnnqflhr6f4vagx9dkfb0889b12vgfpq4sfn0")))

(define-public crate-tinyjson-1.1.0 (c (n "tinyjson") (v "1.1.0") (h "04sc7qldrnn7x8jsgphr9ryl5vf7vzc7hchibrd4sjkw2ihz7h34")))

(define-public crate-tinyjson-2.0.0 (c (n "tinyjson") (v "2.0.0") (h "0aq9nb9jcmqr90knfb0w470qh7f82jipvdy89hkvnl5rm127ahvm")))

(define-public crate-tinyjson-2.0.1 (c (n "tinyjson") (v "2.0.1") (h "0xjikr7c9zzpakrz8b2xj9a1wi5h5wzyir2zhhj9c1jzkcm2v332")))

(define-public crate-tinyjson-2.0.2 (c (n "tinyjson") (v "2.0.2") (h "1rvgf5k3iv5rsbbkzw0bihdlm3s1a21n263ywzs7zifd3b4i64b2")))

(define-public crate-tinyjson-2.0.3 (c (n "tinyjson") (v "2.0.3") (h "0xzy1vsy69axn43i3bn8jj1i7svj63vd0d3qhckwpm1knh4k123z")))

(define-public crate-tinyjson-2.1.0 (c (n "tinyjson") (v "2.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1zkhrc7cyaczh92f7x03k1pr0g1qp7w6ani6nbpiifibdjg5yrn2")))

(define-public crate-tinyjson-2.1.1 (c (n "tinyjson") (v "2.1.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0nmpkqf7al123l7s4gwdzh5kipy3r0i4jj70931s5lydmy4n9isk")))

(define-public crate-tinyjson-2.2.0 (c (n "tinyjson") (v "2.2.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "01i5sl27vhvn5zk7i8kc2xn3ajwicqcqf1kam9wbdxfgc55dcwxd")))

(define-public crate-tinyjson-2.3.0 (c (n "tinyjson") (v "2.3.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "05js2iw7risvp8ghlc0g8zirraj4n01pj2q2z6kgcw4kkzd090qs")))

(define-public crate-tinyjson-2.4.0 (c (n "tinyjson") (v "2.4.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1cgz1sicngkhf6swrxf75rwd8b6n0zr6shdkgmgq2b8bpx78yfs9")))

(define-public crate-tinyjson-2.5.0 (c (n "tinyjson") (v "2.5.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1fw7sv9q8f145bnvj9yll41bby5slyl3h4hc9xzp119h90iz1cml")))

(define-public crate-tinyjson-2.5.1 (c (n "tinyjson") (v "2.5.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0nkjvnkh5wbykb2ap29d3h3phali2g7kj7nhah8xb3rcx8smgfcs")))

