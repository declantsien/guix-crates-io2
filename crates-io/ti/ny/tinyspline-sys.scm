(define-module (crates-io ti ny tinyspline-sys) #:use-module (crates-io))

(define-public crate-tinyspline-sys-0.1.0 (c (n "tinyspline-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1m5msbyh0kknbw5gjxrapglpmgwx2vsv8lr8g32v8jh2yybwy6ww") (l "libtinyspline")))

(define-public crate-tinyspline-sys-0.2.0 (c (n "tinyspline-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (f (quote ("parallel"))) (d #t) (k 1)))) (h "13kyjbva77aj33rxbqykrgsjay6accrpb3wlxgj6cq00jq0zm7hp") (l "libtinyspline")))

