(define-module (crates-io ti ny tinysegmenter) #:use-module (crates-io))

(define-public crate-tinysegmenter-0.1.0 (c (n "tinysegmenter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "maplit") (r "^0.1.2") (d #t) (k 0)))) (h "0dmwkc8ihj6wg47bi4w0j8p2wlynmc3xm0l4r0ikvnjhh7v7yxky")))

(define-public crate-tinysegmenter-0.1.1 (c (n "tinysegmenter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "15v4lgy0w6s144771j3g0m5h7qvdx2s9m99pv7rblw6l2xfnjm8p")))

