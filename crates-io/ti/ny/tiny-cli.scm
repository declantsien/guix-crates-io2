(define-module (crates-io ti ny tiny-cli) #:use-module (crates-io))

(define-public crate-tiny-cli-0.1.0 (c (n "tiny-cli") (v "0.1.0") (d (list (d (n "unix-print") (r "^0.1.0") (d #t) (k 2)))) (h "0kacgzji754m8nlk7rm41y9a2ricq2a3jqiypiimk62w3b1m6jcn")))

(define-public crate-tiny-cli-0.1.1 (c (n "tiny-cli") (v "0.1.1") (d (list (d (n "unix-print") (r "^0.1.0") (d #t) (k 2)))) (h "01xnyl671hxhi5sz3v2f2xfmp9xm7jxghykx34f31m1gybfn7ild")))

(define-public crate-tiny-cli-0.1.2 (c (n "tiny-cli") (v "0.1.2") (d (list (d (n "unix-print") (r "^0.1.0") (d #t) (k 2)))) (h "022w2dibhmwq8d8b5isbzryg5awf8y342w75mmglhiggydziywdi")))

(define-public crate-tiny-cli-0.2.0 (c (n "tiny-cli") (v "0.2.0") (h "1r85kna7j07cplir17y8qhvr9rkq82sp5l74xclbjp4wvm07jiwd")))

(define-public crate-tiny-cli-0.2.1 (c (n "tiny-cli") (v "0.2.1") (d (list (d (n "tiny-std") (r "^0.2.4") (f (quote ("alloc" "cli"))) (d #t) (k 2)))) (h "1x7kvvxdsxpwqnd18rrc2n7x54xy8g2v44s2j5r7xn0iaq384x10")))

