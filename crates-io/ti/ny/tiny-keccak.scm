(define-module (crates-io ti ny tiny-keccak) #:use-module (crates-io))

(define-public crate-tiny-keccak-0.1.0 (c (n "tiny-keccak") (v "0.1.0") (h "0nj254zzxsqsw21yixfycqc0im6d2bc60sydiaizspmfgygd6wzv")))

(define-public crate-tiny-keccak-0.2.0 (c (n "tiny-keccak") (v "0.2.0") (h "1i2hzf8v9ykrskn0gmg1bzvccx6834gjm0yr36202l04ygd4bzp9")))

(define-public crate-tiny-keccak-0.3.0 (c (n "tiny-keccak") (v "0.3.0") (h "1i274irhivcvj76q49cjb7hn7qgpxg7zl78023dgwwjqsj1q3kc1")))

(define-public crate-tiny-keccak-1.0.0 (c (n "tiny-keccak") (v "1.0.0") (h "0aw9mmrz7gzb712d76dk9mdw8xfjj0jdpk4v11wdirbsg8748iam")))

(define-public crate-tiny-keccak-1.0.1 (c (n "tiny-keccak") (v "1.0.1") (h "01csdzgy4hlvqdz7zpljrm9p7r2ivmjwlzximvlm76jascdxbcnm")))

(define-public crate-tiny-keccak-1.0.2 (c (n "tiny-keccak") (v "1.0.2") (h "1m5sxaq66w7az85g903wz8qhfmmab1y9j43pnzqi5y8857pr9g6c")))

(define-public crate-tiny-keccak-1.0.3 (c (n "tiny-keccak") (v "1.0.3") (h "0r9kkr865312r0yzn1xxklsghswpcirip91qd45z8ama7b0s4prj")))

(define-public crate-tiny-keccak-1.0.4 (c (n "tiny-keccak") (v "1.0.4") (h "1i25bypymgm77lny5w07jyjn9ydrcmydxdkbm03a2f9ri9jnrsgq")))

(define-public crate-tiny-keccak-1.0.5 (c (n "tiny-keccak") (v "1.0.5") (h "0w4jmffvk13a74622akch7r2q87nhl71hcmbwjxa0b1990qg9bpp")))

(define-public crate-tiny-keccak-1.1.0 (c (n "tiny-keccak") (v "1.1.0") (h "1a31n16k4zkxhrc3978k1zvfz4ywz049zka9ixsck45nwgz1ha7y")))

(define-public crate-tiny-keccak-1.1.1 (c (n "tiny-keccak") (v "1.1.1") (h "0i5nj6vhb1y4d0ly2iz7709abvjqas7msyrwapz54lvg43m79hla")))

(define-public crate-tiny-keccak-1.2.0 (c (n "tiny-keccak") (v "1.2.0") (h "0hk1wlmbnrg84a0yci130qvd5crcgmvgmczhivs3hqjkhg4nilvq")))

(define-public crate-tiny-keccak-1.2.1 (c (n "tiny-keccak") (v "1.2.1") (h "0hdaz46hi0ibqag8q8lzjvv41vrvzzbqkcbpdwh9kr3fm8zifl0b")))

(define-public crate-tiny-keccak-1.2.2 (c (n "tiny-keccak") (v "1.2.2") (h "0f4agpnm9dvhqq1qjl9n7wycrmw03apvnbgjv05l41lxk04c79na")))

(define-public crate-tiny-keccak-1.3.0 (c (n "tiny-keccak") (v "1.3.0") (h "04krggwlg429rgvvyyyzqq2glnsk67akmhjddhclz41r9n861wdp")))

(define-public crate-tiny-keccak-1.3.1 (c (n "tiny-keccak") (v "1.3.1") (h "14l229k2l2s1pqx4spyjwin4n96p5qhglpnan063w1p4g6ni4bfm")))

(define-public crate-tiny-keccak-1.4.0 (c (n "tiny-keccak") (v "1.4.0") (h "1l518qb015768w37f9y5c29hknb0sfjj1df928pmgjj74rsl34iy")))

(define-public crate-tiny-keccak-1.4.1 (c (n "tiny-keccak") (v "1.4.1") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)))) (h "00cc94j90yipjlg4xdk4k0r5jbx4c39qwhghy798yni7xgaix4aq")))

(define-public crate-tiny-keccak-1.4.2 (c (n "tiny-keccak") (v "1.4.2") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)))) (h "0kx44g9awfdlmfaav9r9nsi68iz1qxn4v2iqrlgphq6vzdhm45z9")))

(define-public crate-tiny-keccak-1.4.3 (c (n "tiny-keccak") (v "1.4.3") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)))) (h "06ka8a5xri57b4lxrhmwszbpc0lq2mdarfdnc11gmiq1p2qfpgfv")))

(define-public crate-tiny-keccak-1.4.4 (c (n "tiny-keccak") (v "1.4.4") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "174pbfd7zr9hhi57x40p7q7lk1akj2gkkfcx2aqnn0a63dnca49z")))

(define-public crate-tiny-keccak-1.5.0 (c (n "tiny-keccak") (v "1.5.0") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1chiisrsql4pfwh5r7nz055ciqj7ch24m0nvrr6a8x5vd4f052hx") (f (quote (("keccak") ("k12") ("default" "keccak"))))))

(define-public crate-tiny-keccak-2.0.0-alpha (c (n "tiny-keccak") (v "2.0.0-alpha") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "00kx2ds3spabh0m13x61av2dl3njrvxkj20qb40a2lv8zhxjpja9") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

(define-public crate-tiny-keccak-2.0.0-alpha2 (c (n "tiny-keccak") (v "2.0.0-alpha2") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "11v4h5y407r2kiwady0hldjcnzzws3xs9nszr971wjd99bskyzzi") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

(define-public crate-tiny-keccak-2.0.0-alpha3 (c (n "tiny-keccak") (v "2.0.0-alpha3") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1g9g3v3qafbg3w88achykz3834qxfc5ygl04ydsr1w87amlm0a4q") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

(define-public crate-tiny-keccak-2.0.0 (c (n "tiny-keccak") (v "2.0.0") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "01lxydvw06ybra1fnhdg9x9zq9m6wvpjwnsa8y9pi2n176v99vbc") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

(define-public crate-tiny-keccak-2.0.1 (c (n "tiny-keccak") (v "2.0.1") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "0sbzg2wqvbrfwy3svbf62fwvnxaj9jslq9y1jnbck6v1918wllr9") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

(define-public crate-tiny-keccak-2.0.2 (c (n "tiny-keccak") (v "2.0.2") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "0dq2x0hjffmixgyf6xv9wgsbcxkd65ld0wrfqmagji8a829kg79c") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default") ("cshake"))))))

