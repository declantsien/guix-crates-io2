(define-module (crates-io ti ny tinybrain) #:use-module (crates-io))

(define-public crate-tinybrain-0.1.0 (c (n "tinybrain") (v "0.1.0") (h "1kdng0wdl6ssvvbqjz35mvxpxjdmwrzb3kz99p46rjn1bii591mc")))

(define-public crate-tinybrain-0.1.1 (c (n "tinybrain") (v "0.1.1") (h "093aq8cy36g4yz5zl8g6zk41wj1i13xdb8n26l468iyn57l527nm")))

(define-public crate-tinybrain-0.1.2 (c (n "tinybrain") (v "0.1.2") (h "0bysdxawv4lz52h7w5ak46cz2zyczx02j448s1b5z0sdavs915vp")))

