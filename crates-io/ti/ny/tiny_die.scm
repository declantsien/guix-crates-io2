(define-module (crates-io ti ny tiny_die) #:use-module (crates-io))

(define-public crate-tiny_die-0.1.0 (c (n "tiny_die") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16nzjgrcc00pyck3qim2d4hjk8zs6b94gbwkdmsxh1img561c6y1")))

(define-public crate-tiny_die-0.2.0 (c (n "tiny_die") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1vpfrm577s8y3jk1qby1dmrjqhh3nszhfvqjpcci2xw1fpc3ykym")))

(define-public crate-tiny_die-0.2.1 (c (n "tiny_die") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04jy4a916q6aavmybhwhj9ri18mi5qrymy2wa95nxzg3xpdpqf1l")))

(define-public crate-tiny_die-0.2.2 (c (n "tiny_die") (v "0.2.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ja3m7xhq69pw31hsgdlm33njcy2w85gaqs8rv7l8qbrcyr654lb")))

