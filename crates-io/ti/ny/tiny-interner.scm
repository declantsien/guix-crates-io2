(define-module (crates-io ti ny tiny-interner) #:use-module (crates-io))

(define-public crate-tiny-interner-0.1.1 (c (n "tiny-interner") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "0p363anqxa725c1aw7218gpjaa6rf8xgm3ni8hakxqv9pblk4shh")))

(define-public crate-tiny-interner-0.1.2 (c (n "tiny-interner") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "0mdajqvjkc38di7yzjy4bp067m5m00cv1pfyl20s0fkjk5ncjbp9")))

(define-public crate-tiny-interner-0.1.3 (c (n "tiny-interner") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "06gs5nv801j9pk154m0cjih93lffa4wlbdfs77mxfhf1127l1nqb")))

(define-public crate-tiny-interner-0.1.5 (c (n "tiny-interner") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)))) (h "0hvf4cw5pv3wjxsr1l7lizxs4m0426vsb1vmc7m5gbjngll04prb")))

