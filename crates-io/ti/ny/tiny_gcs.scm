(define-module (crates-io ti ny tiny_gcs) #:use-module (crates-io))

(define-public crate-tiny_gcs-0.1.0 (c (n "tiny_gcs") (v "0.1.0") (h "0f1k7ixn1xl4ma62jwrp6izw5nrjyz31lz593pf59mi04g95rb3z")))

(define-public crate-tiny_gcs-0.2.0 (c (n "tiny_gcs") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "http") (r "^0.1.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1gs6flhchyqva6sjzr6qj6fqlyfz7idnlpz3nvv9i82k719xpjjm")))

