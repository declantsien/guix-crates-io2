(define-module (crates-io ti ny tinyppm) #:use-module (crates-io))

(define-public crate-tinyppm-0.1.0 (c (n "tinyppm") (v "0.1.0") (h "07sby41h7knyqviy6jcag11rbc6jqhyg9f3y52nks4xc3b2vp99c")))

(define-public crate-tinyppm-0.1.1 (c (n "tinyppm") (v "0.1.1") (h "0dr7p5xrx7rj5l5a2jlw9zkcnxrkn4l3p11wj5cgr5w00893jzhq")))

(define-public crate-tinyppm-0.1.2 (c (n "tinyppm") (v "0.1.2") (h "0b045ggmk3kh33byn1a82czs1ccjfn4gf465w51crg7m5753gjvp")))

(define-public crate-tinyppm-0.1.3 (c (n "tinyppm") (v "0.1.3") (h "1ckahrsc8pf6wglvz0bc70rdi8hmqbd0m30j5bgmgy9sayk54dar")))

(define-public crate-tinyppm-0.1.4 (c (n "tinyppm") (v "0.1.4") (h "0dn1q22nkf5f5p9cfa40r1z58pmmijfkz05i8pamwi61mqa5m3zi")))

(define-public crate-tinyppm-0.1.5 (c (n "tinyppm") (v "0.1.5") (h "1dvyfizlaxarjk8nkcsjiigyyxh20nhd8sznfczjhfgyszxpdk83")))

(define-public crate-tinyppm-0.1.6 (c (n "tinyppm") (v "0.1.6") (h "0lcdlpii28sv1jfrjfp71skbbf33hqbx4js3d7i5b2rnnw2ci52j")))

(define-public crate-tinyppm-0.2.0 (c (n "tinyppm") (v "0.2.0") (h "171g3ph1rydz0fnda62i4s0dkrsi8lazsbcf1shv8bfrgpfjdm0f")))

