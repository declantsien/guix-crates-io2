(define-module (crates-io ti ny tiny-db) #:use-module (crates-io))

(define-public crate-tiny-db-0.1.0 (c (n "tiny-db") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fanzy48bxd4g42ws4r6xzhwvzp2rikh8avgm7jjgg42w76dvklg")))

