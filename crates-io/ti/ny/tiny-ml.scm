(define-module (crates-io ti ny tiny-ml) #:use-module (crates-io))

(define-public crate-tiny-ml-0.1.0 (c (n "tiny-ml") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s7zzll24a61544v6aqzbk0i91wsjf3nj4zbkf37bvdzn21cbzbh")))

(define-public crate-tiny-ml-0.1.1 (c (n "tiny-ml") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1a7qdi2aa6wj3griggq1cdmwfdbhnzyqgzgn767gnhvrj5l1pgm9")))

(define-public crate-tiny-ml-0.2.0 (c (n "tiny-ml") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "13s5in3r7ziygj2z3ynj8lnlsj2z3vzz4bkb5jj4vz6y9g4ljbhl")))

(define-public crate-tiny-ml-1.0.0 (c (n "tiny-ml") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n4was0c8hlbvdcih4624pgnziy5ji1lqxicik655m1wj8r55f3b") (f (quote (("serialization" "serde") ("parallelization" "rayon") ("default" "parallelization"))))))

