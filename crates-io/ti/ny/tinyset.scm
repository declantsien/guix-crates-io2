(define-module (crates-io ti ny tinyset) #:use-module (crates-io))

(define-public crate-tinyset-0.0.1 (c (n "tinyset") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "057kxjavfcxs2x4xh0ammsnr8dm25ssqn9hkpzhq70fxs0sagmza")))

(define-public crate-tinyset-0.0.2 (c (n "tinyset") (v "0.0.2") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0rj0jk16h0l3pz88bw1mmfp47fdgwg78qi5ii47s5p2694fms48h")))

(define-public crate-tinyset-0.0.3 (c (n "tinyset") (v "0.0.3") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1vgy9rdn67q4pwnb6j2sj1hpakpk8f29rlqhix6cq1cmyn5pvh0n")))

(define-public crate-tinyset-0.0.4 (c (n "tinyset") (v "0.0.4") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0my4l0mmvdgcj80qlj9s23ygcc4z105hv65g5m9x9ia5sf0fx8wy")))

(define-public crate-tinyset-0.0.5 (c (n "tinyset") (v "0.0.5") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1198mnca4cqic47l3vvx3hfh67raa4g1iha1a3jj7x60cpwbkhnh")))

(define-public crate-tinyset-0.0.6 (c (n "tinyset") (v "0.0.6") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "19w0s22ym7smp8r27brz8gg97cy9hbxwqjfa0nhkd71jalfm22r6")))

(define-public crate-tinyset-0.0.7 (c (n "tinyset") (v "0.0.7") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "167z44jli5w409yqw6hkf49agm8kmlpmsrgdxwlh3jqlsp153wnp")))

(define-public crate-tinyset-0.0.8 (c (n "tinyset") (v "0.0.8") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1hylx43rbrka3wvww8wn3707cr9ckx81c9y3kvlww0rn1n00fncm")))

(define-public crate-tinyset-0.0.9 (c (n "tinyset") (v "0.0.9") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1mga2gzbihzpalj1wgf4x34ip99j8gxwjarn4ssmm7wnhwlpwpj5")))

(define-public crate-tinyset-0.0.10 (c (n "tinyset") (v "0.0.10") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0w8fykyd1p77l0sb4hdxvcmdfpkbqib0r8n5i0y0gj8629k4673j")))

(define-public crate-tinyset-0.0.11 (c (n "tinyset") (v "0.0.11") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0zni1v41nh9dv3jgz06xlmmgqpf3a051qclwbyyyhb3h30dsm65y")))

(define-public crate-tinyset-0.0.12 (c (n "tinyset") (v "0.0.12") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1g3z9pwl0m539cjmkkvg31q1h8qvvss1qysbwk9nyhz7wyw7c89l")))

(define-public crate-tinyset-0.1.0 (c (n "tinyset") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "00q4dg9lnl39fbjyl5vsax7bn29aacwhdxx6wx55qzlakmx3g1yq")))

(define-public crate-tinyset-0.2.0 (c (n "tinyset") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "17h3yjfwn3r2m643s4qlik5djvc530njbxby2kknxw40qql0z5m4")))

(define-public crate-tinyset-0.2.1 (c (n "tinyset") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "07igrqbw2fdxrr62p4qk4diiy61558zmndbwymnvjih1ghx7zall")))

(define-public crate-tinyset-0.2.2 (c (n "tinyset") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0b22x9k0ssw1hy5zj9harq69ys4ddg0pygidfrrry15wfsdwlaif")))

(define-public crate-tinyset-0.3.0 (c (n "tinyset") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "02hy4lxm3vwl0qrr6g85p5n5b0x6c0w9k30f7d9j1vfl1424lbwq")))

(define-public crate-tinyset-0.3.1 (c (n "tinyset") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1i0pl3v1r8al8g2yy6al9qv0xrp8b1bydf5xj7sqxcpqjxp0a85n")))

(define-public crate-tinyset-0.3.2 (c (n "tinyset") (v "0.3.2") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "15cps5xsnvggzkzvsw0v5nfblknif893rxxkk1q074ffj433n334")))

(define-public crate-tinyset-0.3.3 (c (n "tinyset") (v "0.3.3") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0fw875zcly2xzz1nzf3kmgbwdk2x3cz9v4zrgxnkrld84r4rcn3v")))

(define-public crate-tinyset-0.3.4 (c (n "tinyset") (v "0.3.4") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "16aryby0rvpwf89zmxx77i3b9yckdarxknqmva66rjmzacj3d7l1")))

(define-public crate-tinyset-0.3.5 (c (n "tinyset") (v "0.3.5") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "00g3i16vkxvw5zcqpg878jw8fissx072yh6cyflaljkgrvfqag9w")))

(define-public crate-tinyset-0.4.0 (c (n "tinyset") (v "0.4.0") (d (list (d (n "easybench") (r "^1.0.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0nmyvcranmr207wvj0zdnhy0js9vj5rkh4zq56v8whv7i7953kyd")))

(define-public crate-tinyset-0.4.1 (c (n "tinyset") (v "0.4.1") (d (list (d (n "easybench") (r "^1.0.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0x88r9f8i90wk7xir23hbdr3hq19rvbr9shipdxgwvf8ylvncdd6")))

(define-public crate-tinyset-0.4.2 (c (n "tinyset") (v "0.4.2") (d (list (d (n "easybench") (r "^1.0.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "02f0sg8nvrxkfflspmwblxhl8js31gc9z43xxccs664qqrj5ximv")))

(define-public crate-tinyset-0.4.3 (c (n "tinyset") (v "0.4.3") (d (list (d (n "easybench") (r "^1.0.0") (d #t) (k 2)) (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0cyf6c3v8ag01nrxjw913bjy2vp1mb3qf5f0yj0dqywr90wvzh5s")))

(define-public crate-tinyset-0.4.4 (c (n "tinyset") (v "0.4.4") (d (list (d (n "easybench") (r "^1.0.0") (d #t) (k 2)) (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1z31p0gz6vsqmjw4qirc1y99vqw7xlqg9jlj77b48cd6c04m8kvq") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.5 (c (n "tinyset") (v "0.4.5") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "10sm623gvi2iw6r5pvdszkan7sk95vm3xdamd2250ikhxdxkcidy") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.6 (c (n "tinyset") (v "0.4.6") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1wmg4lb58d2gpd7256drqwhm2acdn8vmbaglqrgaf7chgffwnmgb") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.7 (c (n "tinyset") (v "0.4.7") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "1dfz3q71wchrxh8mvysvd08vha1znnsmsn3wr7klgv4vyk0a9yhx") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.8 (c (n "tinyset") (v "0.4.8") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "15kqrayd974jjfj7n0h8w64aqijkc0sna5hd6dhvmwbn900gq81k") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.9 (c (n "tinyset") (v "0.4.9") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "01mnjk3jg216nzfn27k7lk57fdsbzn7a9p4bcy9xp2ncxm3cmqrl") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.10 (c (n "tinyset") (v "0.4.10") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "134789665z4s98zdch5swpj7s2dfs8zd5gw6ivdcbzlnlx7j1irs") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.11 (c (n "tinyset") (v "0.4.11") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0nvn4l8r9gr7l7x0a519srd166ialhb5lcf88jm9pkg1cpfy54np") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.12 (c (n "tinyset") (v "0.4.12") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "09i6w0y6fwi2fl5r6cshxnd60dlsrkq438f3f8wpyk42w0vl59cs") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.13 (c (n "tinyset") (v "0.4.13") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "05gwh4ryvlpnafb90bp5shsjam2yj5hbfxvjf2jw9x5n2l5as2ls") (f (quote (("default" "rand"))))))

(define-public crate-tinyset-0.4.14 (c (n "tinyset") (v "0.4.14") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "0h8p05qqff97z9bmsq088zkb0way4afx7ryb5vq4wpw12fbqrrl4") (f (quote (("default" "rand") ("compactserde" "serde"))))))

(define-public crate-tinyset-0.4.15 (c (n "tinyset") (v "0.4.15") (d (list (d (n "heapsize") (r "^0.4.2") (d #t) (k 2)) (d (n "id-set") (r "^0.2.2") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "roaring") (r "^0.6.0") (d #t) (k 2)) (d (n "scaling") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stats_alloc") (r "^0.1.8") (d #t) (k 2)))) (h "180ayfjirvmkxsz8rqq1chx1p27fak3plxr20ba9m06kvm3pwhg2") (f (quote (("default" "rand") ("compactserde" "serde"))))))

