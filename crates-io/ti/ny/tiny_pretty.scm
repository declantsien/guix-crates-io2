(define-module (crates-io ti ny tiny_pretty) #:use-module (crates-io))

(define-public crate-tiny_pretty-0.1.0 (c (n "tiny_pretty") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0fywjd3nbwkl7m3rsh5fb9kbf1qcxf53znvn47wxfvwc24qkh5qi") (s 2) (e (quote (("unicode-width" "dep:unicode-width"))))))

(define-public crate-tiny_pretty-0.1.1 (c (n "tiny_pretty") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ybif7vrcajs4k05ic6fbvcazdijgrjg84hcscf3zp1x9qw2xp9l") (s 2) (e (quote (("unicode-width" "dep:unicode-width"))))))

(define-public crate-tiny_pretty-0.2.0 (c (n "tiny_pretty") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cn3c0bsm0ffknwrhfd08dy8c1hs7y870qppyz3bk04iakq4cgsb") (s 2) (e (quote (("unicode-width" "dep:unicode-width"))))))

