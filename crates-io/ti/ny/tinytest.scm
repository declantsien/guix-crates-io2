(define-module (crates-io ti ny tinytest) #:use-module (crates-io))

(define-public crate-tinytest-0.1.0 (c (n "tinytest") (v "0.1.0") (h "0liw4ign14gnrqg2cznmnysch6sx4lb1np1i926kabxbhmmqx836")))

(define-public crate-tinytest-0.1.1 (c (n "tinytest") (v "0.1.1") (h "1xca7mlkas7ripx6r6lsxlzh6nxplbsh574wmn419khg0mg07nn4")))

