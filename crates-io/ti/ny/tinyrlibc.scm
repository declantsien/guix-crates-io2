(define-module (crates-io ti ny tinyrlibc) #:use-module (crates-io))

(define-public crate-tinyrlibc-0.1.1 (c (n "tinyrlibc") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "143fpsxwkmmn4fgz12jzrjrg309mgfmbyygzmacy47wddvhvk5j0") (f (quote (("lp64") ("lp32"))))))

(define-public crate-tinyrlibc-0.1.2 (c (n "tinyrlibc") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1a21labm0hwk06hnsnb0gla283kgyldysjj3mbf7201h3iinhph0") (f (quote (("lp64") ("lp32"))))))

(define-public crate-tinyrlibc-0.2.0 (c (n "tinyrlibc") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wvnxflarri190z1mb9fgpndcpnbv8yjklmxa5jfw6y8cnm918c4") (f (quote (("lp64") ("lp32"))))))

(define-public crate-tinyrlibc-0.2.1 (c (n "tinyrlibc") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0d8ws1nf077iavp7nbyjsvvc1al02qv3l69vzgg4sapdk12cccha") (f (quote (("lp64") ("lp32"))))))

(define-public crate-tinyrlibc-0.2.2 (c (n "tinyrlibc") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08ihkphk28c6pjs2nj0ai4c6cxcx6ckb6rhcg63y1dlbfxqr2b5v") (f (quote (("lp64") ("lp32"))))))

(define-public crate-tinyrlibc-0.3.0 (c (n "tinyrlibc") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1in9cxk01b9jahrkb4g6fihyg98jvzbw4f3nb4j7g6xv8s23cman")))

(define-public crate-tinyrlibc-0.4.0 (c (n "tinyrlibc") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "portable-atomic") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "static-alloc") (r "^0.2.4") (d #t) (k 2)))) (h "06skns9nppd7hxjlh32w0bz76ljm9z8dm7a7ps6x0dxdsxxcpai7") (f (quote (("utoa") ("strtoumax") ("strtoull") ("strtoul") ("strtoll") ("strtol") ("strtoimax") ("strstr") ("strncpy") ("strncmp") ("strncasecmp") ("strlen") ("strcpy") ("strcmp") ("strchr") ("snprintf") ("signal-cs" "portable-atomic/critical-section") ("itoa") ("isupper") ("isspace") ("isdigit") ("isalpha") ("default" "all") ("atoi") ("alloc") ("all" "abs" "strcmp" "strncmp" "strncasecmp" "strcpy" "strncpy" "strlen" "strtol" "strtoul" "strtoll" "strtoull" "strtoimax" "strtoumax" "strstr" "strchr" "atoi" "utoa" "itoa" "snprintf" "isspace" "isdigit" "isalpha" "isupper") ("abs")))) (s 2) (e (quote (("signal" "dep:portable-atomic"))))))

