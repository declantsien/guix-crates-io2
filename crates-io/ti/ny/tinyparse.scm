(define-module (crates-io ti ny tinyparse) #:use-module (crates-io))

(define-public crate-tinyparse-0.1.0 (c (n "tinyparse") (v "0.1.0") (h "0d4lflq99af8y0a1dbv9vd105jfzfjs5z6w3iqnzlwhdf5rw06bw")))

(define-public crate-tinyparse-0.1.1 (c (n "tinyparse") (v "0.1.1") (h "0hmxwjb9sgh4ak9lfriz5i3fzcdp7llx6k0hg6j8fi8s8ph0jbp0") (y #t)))

(define-public crate-tinyparse-0.2.1 (c (n "tinyparse") (v "0.2.1") (h "09jayxmgc8w24qlmqsa0x9j60c7zbgng4sx0sjjj92305dnrhns2")))

(define-public crate-tinyparse-0.2.2 (c (n "tinyparse") (v "0.2.2") (h "1v81v074mdywcndi1qmsq27jwpmz6dlbd3rlwa90giza76jb241f")))

(define-public crate-tinyparse-0.2.3 (c (n "tinyparse") (v "0.2.3") (h "0hdv2n955kj4pgh478l63n00k00zqbnbdqm105j4sbzpdc4lfmqh")))

