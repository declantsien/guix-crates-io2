(define-module (crates-io ti ny tinypool) #:use-module (crates-io))

(define-public crate-tinypool-0.1.0 (c (n "tinypool") (v "0.1.0") (h "1dilcfrfqasf4z1fp882b7apmz12ysp5cmv89awaviqv8ismvxqg")))

(define-public crate-tinypool-0.2.0 (c (n "tinypool") (v "0.2.0") (h "0sxyrclmhxqfffmkx5wlcm859yyjmivvspklwpbbmyar43b0aiir")))

