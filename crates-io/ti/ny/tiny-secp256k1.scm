(define-module (crates-io ti ny tiny-secp256k1) #:use-module (crates-io))

(define-public crate-tiny-secp256k1-0.1.0 (c (n "tiny-secp256k1") (v "0.1.0") (d (list (d (n "secp256k1") (r "^0.6") (d #t) (k 2)))) (h "1f717al5h0dmhd94vl4ql724qwibgvxlwhqlfm9jqjpynsr74zsd") (y #t)))

(define-public crate-tiny-secp256k1-0.1.1 (c (n "tiny-secp256k1") (v "0.1.1") (h "11imlgc5srzwdzy0nz9xd36561nx00n589ws4dfm6fjd4xn3k46x") (r "1.50")))

