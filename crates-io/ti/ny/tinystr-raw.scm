(define-module (crates-io ti ny tinystr-raw) #:use-module (crates-io))

(define-public crate-tinystr-raw-0.1.0 (c (n "tinystr-raw") (v "0.1.0") (h "1d9lk5njwr7yhp9bclx5484hn51c96vpvkpl9f2cfxv6d4xpvg8p")))

(define-public crate-tinystr-raw-0.1.1 (c (n "tinystr-raw") (v "0.1.1") (h "1nyfghlryl6mvs8a583x87xfpbzwf5j37xzbmsn9cj42ylmdsxdr") (f (quote (("std") ("default" "std"))))))

(define-public crate-tinystr-raw-0.1.2 (c (n "tinystr-raw") (v "0.1.2") (h "1n4f75x7igdcgnyc8zwhckn1lkwmqy74hzgqd771pfafxaqbskac")))

(define-public crate-tinystr-raw-0.1.3 (c (n "tinystr-raw") (v "0.1.3") (h "159q0341hmkcspn3jq7g5x0iw9f3ml45djfaypzlwpj8n3w7xy0h") (f (quote (("std"))))))

