(define-module (crates-io ti ny tinyurl) #:use-module (crates-io))

(define-public crate-tinyurl-0.1.0 (c (n "tinyurl") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0wpvp4gdrzk54mflf397syazfp1b6gc0vvqjkrl6zd30sxsji4ww")))

(define-public crate-tinyurl-0.1.1 (c (n "tinyurl") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1zf2zify4s1mmzh35kq69sy6idp7paj123aysz0vc3hmf7gkalgz")))

