(define-module (crates-io ti ny tinyvec_string) #:use-module (crates-io))

(define-public crate-tinyvec_string-0.1.0 (c (n "tinyvec_string") (v "0.1.0") (d (list (d (n "tinyvec") (r "^0.3") (d #t) (k 0)))) (h "0vvrkij4gjdvhfa7h7fh9pcq5fpmha4g779k0lxw70z8zq7jp451") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.1.1 (c (n "tinyvec_string") (v "0.1.1") (d (list (d (n "tinyvec") (r "^0.3") (d #t) (k 0)))) (h "03bivw230s6p95c3vgdkkh7077rl2h5cv73gwkxwnkq3lkldg4ad") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.1.2 (c (n "tinyvec_string") (v "0.1.2") (d (list (d (n "tinyvec") (r "^0.3") (d #t) (k 0)))) (h "1wqglgd3sjal10cvxp8dvz3zyyphn7wz8b5hpny7kma13q23bbjg") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.1.3 (c (n "tinyvec_string") (v "0.1.3") (d (list (d (n "tinyvec") (r "^0.3") (d #t) (k 0)))) (h "0g54jhknyms2ciwjry791wjipn3yzdik712jnvn746qn56gi9m18") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.1.4 (c (n "tinyvec_string") (v "0.1.4") (d (list (d (n "tinyvec") (r "^0.4") (d #t) (k 0)))) (h "1igrfwyy3rkzsxkprwmzrqdabs7jq56vnwjipfjy47gfw65qkmyd") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.2.0 (c (n "tinyvec_string") (v "0.2.0") (d (list (d (n "tinyvec") (r "^1") (d #t) (k 0)))) (h "0qm82jz6snvmgfaf25mdq4vhlfy18wfyyd6cfr8fvgbdyadfn0vw") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc"))))))

(define-public crate-tinyvec_string-0.3.0 (c (n "tinyvec_string") (v "0.3.0") (d (list (d (n "tinyvec") (r "^1") (d #t) (k 0)))) (h "0xl8555dkzhrg07vmx2pfwimharc7wlbbynj93jwbdfnyb76w3dk") (f (quote (("std" "alloc") ("default") ("alloc" "tinyvec/alloc")))) (y #t)))

(define-public crate-tinyvec_string-0.3.1 (c (n "tinyvec_string") (v "0.3.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (d #t) (k 0)) (d (n "tinyvec_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01z42kfqr3zwp212cazdz617zjfwqfj6k561vxk3fk9qqrzq6np0") (f (quote (("std" "alloc") ("rustc_1_57" "rustc_1_55") ("rustc_1_55" "tinyvec/rustc_1_55") ("rustc_1_40" "tinyvec/rustc_1_40") ("default") ("alloc" "tinyvec/alloc" "tinyvec_macros"))))))

(define-public crate-tinyvec_string-0.3.2 (c (n "tinyvec_string") (v "0.3.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tinyvec") (r "^1.5") (d #t) (k 0)) (d (n "tinyvec_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13yi29nf2zcnj6wilx9lfcfq6vsmiznrfid2l81z2r20p4512a5y") (f (quote (("std" "alloc") ("rustc_1_57" "rustc_1_55") ("rustc_1_55" "tinyvec/rustc_1_55") ("rustc_1_40" "tinyvec/rustc_1_40") ("default") ("alloc" "tinyvec/alloc" "tinyvec_macros"))))))

