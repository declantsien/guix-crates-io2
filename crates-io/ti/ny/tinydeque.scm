(define-module (crates-io ti ny tinydeque) #:use-module (crates-io))

(define-public crate-tinydeque-0.1.0 (c (n "tinydeque") (v "0.1.0") (d (list (d (n "tinyvec") (r "^1.0.1") (d #t) (k 0)))) (h "12lz2xj3fv7z6a9227lzxxn2ak4zxwc42myrq8k9j1w37cxibzgp") (f (quote (("default") ("alloc"))))))

(define-public crate-tinydeque-0.1.1 (c (n "tinydeque") (v "0.1.1") (d (list (d (n "tinyvec") (r "^1.0.1") (d #t) (k 0)))) (h "17j645ls0bsspgnyid9ncrhl2m2iq7cn5piqk2hk6kf0b4qz1diq") (f (quote (("default") ("alloc"))))))

