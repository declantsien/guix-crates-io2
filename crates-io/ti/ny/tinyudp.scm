(define-module (crates-io ti ny tinyudp) #:use-module (crates-io))

(define-public crate-tinyudp-0.1.0 (c (n "tinyudp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "04j2lhjgwmx59wbb3jbw1ghxc6nrm1bhkk8kpjpwhvvwlc7gm5yl")))

(define-public crate-tinyudp-0.2.0 (c (n "tinyudp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0px0fmgwnf5k328pfxw7laaa536zkqrzzhwazz82v6770ra46qy4")))

(define-public crate-tinyudp-0.2.1 (c (n "tinyudp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "15zzplqvl5q166g5c6jkm0dmsbqkhy1r67szcvmrarm68pdsa54g")))

(define-public crate-tinyudp-0.3.0 (c (n "tinyudp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0d77zwnyizq4cnsfj4c7f8s3sikmdffpyx6j0cib5c4gj7668pcp")))

(define-public crate-tinyudp-0.4.0 (c (n "tinyudp") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0dknwa975bwc8icxg17igv2hhr2px7b2cgx45y9lpk2sn486yxb9")))

