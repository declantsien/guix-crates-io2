(define-module (crates-io ti ny tinyio) #:use-module (crates-io))

(define-public crate-tinyio-0.1.0 (c (n "tinyio") (v "0.1.0") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 2)) (d (n "tinyio-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tinyio-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0r7jlzg2wz2hj00x642wclxbirpwpfli1ygx487yabvyzmgz4qqf")))

