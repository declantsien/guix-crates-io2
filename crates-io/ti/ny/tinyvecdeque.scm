(define-module (crates-io ti ny tinyvecdeque) #:use-module (crates-io))

(define-public crate-tinyvecdeque-0.1.0 (c (n "tinyvecdeque") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "tinyvec") (r "^1.6") (d #t) (k 0)))) (h "13a3627rky628sn79c41ibsjs5la0wgp2fm77y2malj9dsp0zi20") (f (quote (("std" "tinyvec/std") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "tinyvec/serde"))))))

(define-public crate-tinyvecdeque-0.1.2 (c (n "tinyvecdeque") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "16a0rd98hx8q2j8n39rcdrz6al3nxlrh8ggnbw92bwvs783xnk0z") (f (quote (("std" "alloc" "either/use_std") ("default") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56")))

