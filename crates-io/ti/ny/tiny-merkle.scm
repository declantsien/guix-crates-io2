(define-module (crates-io ti ny tiny-merkle) #:use-module (crates-io))

(define-public crate-tiny-merkle-0.1.0 (c (n "tiny-merkle") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3" "keccak"))) (d #t) (k 2)))) (h "131c99kpa3wapav4rd4z223q986sh5l8msx09bdalg7x682zyfjk")))

(define-public crate-tiny-merkle-0.1.1 (c (n "tiny-merkle") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3" "keccak"))) (d #t) (k 2)))) (h "0pkzlxsbvznf33ly6p01sqdrg40l3ywqrghv04zdvwjlvhx2k3mf")))

(define-public crate-tiny-merkle-0.1.2 (c (n "tiny-merkle") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3" "keccak"))) (d #t) (k 2)))) (h "03zwjrfil0xsyidi2vlhycphrfl5rv9zdpdzy8kz9sd20vsd3vl4")))

(define-public crate-tiny-merkle-0.2.0 (c (n "tiny-merkle") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3" "keccak"))) (d #t) (k 2)))) (h "1gn5v9flhfc5bwx17z6i84dkiyv132k7xy1fcsgl10yzaq65anir") (f (quote (("std") ("rayon") ("default" "std"))))))

(define-public crate-tiny-merkle-0.3.0 (c (n "tiny-merkle") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3" "keccak"))) (d #t) (k 2)))) (h "1dzwjc1m07x609yi1dvqvnlhmj8dfwmz80bkkhmhylr57rd00abs") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

