(define-module (crates-io ti ny tinyresp) #:use-module (crates-io))

(define-public crate-tinyresp-0.0.1 (c (n "tinyresp") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0qx8q78l728hks4x0dd8axdmdbax73cr808cbdmcy00wv3bnksg6")))

(define-public crate-tinyresp-0.1.0 (c (n "tinyresp") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "02ggn5adma8a052zsjkarv24g3a2p5jgrkx9ry1m8ixmdj272jjq")))

(define-public crate-tinyresp-0.2.0 (c (n "tinyresp") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1a73c34qf6yzwd18wkxslyd2szbrc46kbic4xvgigzwvphgmbm6r")))

(define-public crate-tinyresp-0.2.1 (c (n "tinyresp") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0sl128s6v7jm0ksax3gxw0wqiirirvzqh367pn3x5zrgk18vd7yv")))

