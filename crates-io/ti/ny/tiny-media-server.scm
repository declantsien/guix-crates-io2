(define-module (crates-io ti ny tiny-media-server) #:use-module (crates-io))

(define-public crate-tiny-media-server-0.1.0 (c (n "tiny-media-server") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json" "serde_json"))) (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.2") (f (quote ("tera"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kibw7n15khw4civfpcmfv0z2yphzg826f38gyin1z4wwj8wm2ll")))

