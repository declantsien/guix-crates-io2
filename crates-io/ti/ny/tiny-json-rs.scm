(define-module (crates-io ti ny tiny-json-rs) #:use-module (crates-io))

(define-public crate-tiny-json-rs-0.1.0 (c (n "tiny-json-rs") (v "0.1.0") (d (list (d (n "tiny_json_derive") (r "^0.1.0") (d #t) (k 0)))) (h "14vc61j46kyywgj3wy9zhcyncqnp0z2v9a668k77j9f79ljw8pl5") (y #t)))

(define-public crate-tiny-json-rs-0.2.0 (c (n "tiny-json-rs") (v "0.2.0") (d (list (d (n "tiny_json_derive") (r "^0.1.1") (d #t) (k 0)))) (h "19rmq55y0sh80yzl73yfxdbn34s2vzybshqf5wva6nn7l7n1xc7z") (y #t)))

(define-public crate-tiny-json-rs-0.2.1 (c (n "tiny-json-rs") (v "0.2.1") (d (list (d (n "tiny_json_derive") (r "^0.1.1") (d #t) (k 0)))) (h "15r483vx9i75cxk6lmv0xipp3inpaxxxirzmyxwxkwi7kchxhv0n") (y #t)))

(define-public crate-tiny-json-rs-0.2.3 (c (n "tiny-json-rs") (v "0.2.3") (d (list (d (n "tiny_json_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0js4kgpkpx4psgn1gjb2iwm1pz1lg6q17cnjysijxyx5ajap9rca") (y #t)))

(define-public crate-tiny-json-rs-0.2.4 (c (n "tiny-json-rs") (v "0.2.4") (d (list (d (n "tiny_json_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0pg6bscds20p788i82nz8nvg1m72nc1y6p29imqvjlq3lb8wfzrq")))

(define-public crate-tiny-json-rs-0.2.5 (c (n "tiny-json-rs") (v "0.2.5") (d (list (d (n "tiny_json_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0c8znicsfmpfm1vyqkl4f23w41kh9vx2i7mha1ldj3cmkj3zcbcj")))

