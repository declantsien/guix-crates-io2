(define-module (crates-io ti ny tinycache) #:use-module (crates-io))

(define-public crate-tinycache-0.1.0 (c (n "tinycache") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (o #t) (d #t) (k 0)))) (h "13xjszqq0qv3a4gb3fr8v90anjsfqx6l7ixwyy5p1mfn196xi4nd") (f (quote (("default" "tracing")))) (s 2) (e (quote (("tracing" "dep:tracing"))))))

