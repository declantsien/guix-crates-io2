(define-module (crates-io ti ny tinyparse_macro) #:use-module (crates-io))

(define-public crate-tinyparse_macro-1.2.3-alpha (c (n "tinyparse_macro") (v "1.2.3-alpha") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nwcnaalvm10j2i7jyf6nc65s0mnlldka955pqib5xmzlk3pd9zv")))

(define-public crate-tinyparse_macro-0.3.3 (c (n "tinyparse_macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08vmwck2i3xv6rfxrzby2251cik1yw2f6x5dk9mx3mhjj94bab1k")))

