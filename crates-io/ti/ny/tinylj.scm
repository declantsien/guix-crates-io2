(define-module (crates-io ti ny tinylj) #:use-module (crates-io))

(define-public crate-tinylj-0.1.1 (c (n "tinylj") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "luajit-src") (r "^210") (d #t) (k 1)))) (h "03j63f55p18inzib6vy5ihv93586rfsvx8v5726r4sw034b585fb") (f (quote (("lua52compat"))))))

(define-public crate-tinylj-0.1.2 (c (n "tinylj") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "luajit-src") (r "^210") (d #t) (k 1)))) (h "1szjbdgw6g71qqnw8h8h9xlzc9436wcv95s4w14rpjv5j4kammxj") (f (quote (("std") ("lua52compat") ("default" "std"))))))

(define-public crate-tinylj-0.1.3 (c (n "tinylj") (v "0.1.3") (d (list (d (n "chlorine") (r "^1.0") (f (quote ("int_extras"))) (d #t) (k 0)) (d (n "luajit-src") (r "^210.4") (d #t) (k 1)))) (h "1q9da54iq53phavbz3r63h921lc3r4vsmrh3nwjca675nv4d7g62") (f (quote (("std") ("lua52compat") ("default" "std"))))))

