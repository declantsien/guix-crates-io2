(define-module (crates-io ti ny tiny-start) #:use-module (crates-io))

(define-public crate-tiny-start-0.1.0 (c (n "tiny-start") (v "0.1.0") (d (list (d (n "rusl") (r "^0.1.0") (k 0)))) (h "0amh4zzx71mgkygmd8vpca9a41jbjq851wvzq6hcg1977p9ksx2m") (f (quote (("start") ("mem-symbols") ("default") ("aux" "start"))))))

(define-public crate-tiny-start-0.1.1 (c (n "tiny-start") (v "0.1.1") (d (list (d (n "rusl") (r "^0.2.1") (k 0)))) (h "0cmy9ap1f9h7dwc5qnvaqmv8jsvv7zzqfy3nbkann56rr4n3nkap") (f (quote (("start") ("mem-symbols") ("default") ("aux" "start"))))))

(define-public crate-tiny-start-0.1.2 (c (n "tiny-start") (v "0.1.2") (d (list (d (n "rusl") (r "^0.2.2") (k 0)))) (h "1g02hw16d6wdwla6dl78qq6g1jc1g5ki2hpa6m0003kmrcs9p305") (f (quote (("start") ("mem-symbols") ("default") ("aux" "start"))))))

(define-public crate-tiny-start-0.1.3 (c (n "tiny-start") (v "0.1.3") (d (list (d (n "rusl") (r "^0.3.0") (k 0)))) (h "0z40ndvcckpaiw149wb4kxm32ipwr94szjrfxl6js3a3xqvml1fy") (f (quote (("start") ("mem-symbols") ("default") ("aux" "start"))))))

