(define-module (crates-io ti ny tiny-web-macro) #:use-module (crates-io))

(define-public crate-tiny-web-macro-0.1.0 (c (n "tiny-web-macro") (v "0.1.0") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0i7jb8nr68i08xxs95zlx2z6kcn9x3rbx05m8qbz8jq9xw7ywdvz") (y #t)))

(define-public crate-tiny-web-macro-0.1.1 (c (n "tiny-web-macro") (v "0.1.1") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12kkh9rh0i30dm9anzfrnjj5g9vk172c71blpqbc2smqdqms7ajs") (y #t)))

(define-public crate-tiny-web-macro-0.1.2 (c (n "tiny-web-macro") (v "0.1.2") (d (list (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06kf1fzc3vands47ls7zys65dcsgmfgxb6bkjc5162137gh7jfna") (y #t)))

(define-public crate-tiny-web-macro-0.1.3 (c (n "tiny-web-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02qv3j19fwm3dbhs5igb6wjq44f1kynwdffns49v3affrprz1nxg") (y #t)))

(define-public crate-tiny-web-macro-0.1.4 (c (n "tiny-web-macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "123jvi19rx8ywykshw94r339lxjdg9pqfka1929vknflz336z2bq") (y #t)))

(define-public crate-tiny-web-macro-0.1.5 (c (n "tiny-web-macro") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "018jn2h69xhzi9vvpf0v78pdjvgdbryawh8p23k9vb05vcz5n0k7")))

