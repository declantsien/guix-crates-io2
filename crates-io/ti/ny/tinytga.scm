(define-module (crates-io ti ny tinytga) #:use-module (crates-io))

(define-public crate-tinytga-0.1.0 (c (n "tinytga") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.3") (k 0)))) (h "01rzw0ii2k42r6hbra8xqbh72xlmv56mc7cgi0fm8jqz5h2qb56w")))

(define-public crate-tinytga-0.2.0 (c (n "tinytga") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.1") (k 0)))) (h "0bhs5llyhg7f2f28kqn44asrl1nrk78imnw6g47l1xw9qdgbqlz5")))

(define-public crate-tinytga-0.3.0 (c (n "tinytga") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.6.0-alpha.3") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.0-alpha.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "0ab71x817y8i36rlsxpycm220cxzyh7mnmhja5pv44b0av23i1di") (f (quote (("graphics" "embedded-graphics")))) (y #t)))

(define-public crate-tinytga-0.3.1 (c (n "tinytga") (v "0.3.1") (d (list (d (n "embedded-graphics") (r "^0.6.0-beta.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "1dai8pbckj1dbikgq5ms912bz5wdnwqsvf0128s4wmky7s1j19kb") (f (quote (("graphics" "embedded-graphics"))))))

(define-public crate-tinytga-0.3.2 (c (n "tinytga") (v "0.3.2") (d (list (d (n "embedded-graphics") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "1s7amq5x3cy00qzpnnjf2cvhb1w7x23k80n73fq5svawny1smzpy") (f (quote (("graphics" "embedded-graphics"))))))

(define-public crate-tinytga-0.4.0-alpha.1 (c (n "tinytga") (v "0.4.0-alpha.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-alpha.2") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1rxxfg2hxwbi1r15fcyz96x3zbffnrvcsrh59yrabrhwkq4hqic9")))

(define-public crate-tinytga-0.4.0-beta.1 (c (n "tinytga") (v "0.4.0-beta.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1bzzbmdbc3qkmclpg0lig91d08s4zmq0qqyj6nhc6z70dyz57hyk")))

(define-public crate-tinytga-0.4.0 (c (n "tinytga") (v "0.4.0") (d (list (d (n "embedded-graphics") (r "^0.7.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0pyz7s2ljnwjgaacfjv7zzgyv4bwsxij7iimfn64ak274998bxp4")))

(define-public crate-tinytga-0.4.1 (c (n "tinytga") (v "0.4.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0asxh9f8sszgvqqjsd0ic02gs8aq4949mwajn255ypmkg00cqvbm")))

(define-public crate-tinytga-0.5.0 (c (n "tinytga") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1m9599hphv3l5ff8xqaqvnlv7hccxxpayplia42lvjrac6ykjy27")))

