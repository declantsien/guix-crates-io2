(define-module (crates-io ti ny tiny_id) #:use-module (crates-io))

(define-public crate-tiny_id-0.1.0 (c (n "tiny_id") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1fzl3lqba5p4qw7yhpkzczavklhq7qm14a9fc0mv6qf2n19mj68r") (f (quote (("default" "rand" "serde"))))))

(define-public crate-tiny_id-0.1.1 (c (n "tiny_id") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "14z6yrqds8sqi7xr9dypfafhsh1gigrjbf0p9ag0isxg58fdk7qn") (f (quote (("default" "rand" "serde"))))))

(define-public crate-tiny_id-0.1.2 (c (n "tiny_id") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0w7bab3cwilxayrz76bi28d4fl7rl9i87wdhp6j3crgj570w6nh5") (f (quote (("default" "rand" "serde"))))))

(define-public crate-tiny_id-0.1.3 (c (n "tiny_id") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wqq5nl1igdfilirh14frjhl77ghj904g60zr978i7mr4j1zzpd6") (f (quote (("default" "rand" "serde"))))))

(define-public crate-tiny_id-0.1.4 (c (n "tiny_id") (v "0.1.4") (d (list (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 2)))) (h "0xas3w0hjix3xz7s1491h1r49jswa6h2s17qhnzcjlw7yzh3ji7b") (f (quote (("serialize" "rand_chacha/serde1" "rand/serde1" "serde") ("default" "getrandom" "serialize"))))))

(define-public crate-tiny_id-0.1.5 (c (n "tiny_id") (v "0.1.5") (d (list (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 2)))) (h "0vmz84srnvl8n5cgi1lklhyl93zgk7caq9cn3f9lf2zp7gl1hn1f") (f (quote (("serialize" "rand_chacha/serde1" "rand/serde1" "serde") ("default" "getrandom" "serialize"))))))

(define-public crate-tiny_id-0.1.6 (c (n "tiny_id") (v "0.1.6") (d (list (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 2)))) (h "14zvch2xdhw4vb46lhkjj6h02lfawgs4rvw5j41yd4s2d6iqdxi6") (f (quote (("serialize" "rand_chacha/serde1" "rand/serde1" "serde") ("js" "getrandom/js") ("default" "getrandom" "serialize"))))))

