(define-module (crates-io ti ny tinyprof) #:use-module (crates-io))

(define-public crate-tinyprof-0.0.1 (c (n "tinyprof") (v "0.0.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lfbz9x19kvbl085kmj3p6zih7cg4psf4d3ra11m2g1455szbcc2")))

