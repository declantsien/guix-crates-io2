(define-module (crates-io ti ny tinyvec_macros) #:use-module (crates-io))

(define-public crate-tinyvec_macros-0.1.0 (c (n "tinyvec_macros") (v "0.1.0") (h "0p5zvgbas5nh403fbxica819mf3g83n8g2hzpfazfr56w6klv9yd")))

(define-public crate-tinyvec_macros-0.1.1 (c (n "tinyvec_macros") (v "0.1.1") (h "081gag86208sc3y6sdkshgw3vysm5d34p431dzw0bshz66ncng0z")))

