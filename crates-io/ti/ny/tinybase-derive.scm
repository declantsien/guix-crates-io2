(define-module (crates-io ti ny tinybase-derive) #:use-module (crates-io))

(define-public crate-tinybase-derive-0.1.2 (c (n "tinybase-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05pzwsm5azx9mrpx030vllzd7naw4kdz2nk8bhnvd939bhakygkp")))

(define-public crate-tinybase-derive-0.1.3 (c (n "tinybase-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0623c79anrq7fzpia4xh4l7jjkws4pifppipcy1d4j9ni1g8j6h9") (y #t)))

(define-public crate-tinybase-derive-0.1.4 (c (n "tinybase-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zalk59683j28806s11blwf36q9qa1c055p7lh65icd698rdzhld") (y #t)))

(define-public crate-tinybase-derive-0.1.5 (c (n "tinybase-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yjpzd07zz575a6iwy7qryw2jjw1yc3qk7igckb67s0g9hk4w1j9")))

