(define-module (crates-io ti ny tiny-lz4-decoder-sys) #:use-module (crates-io))

(define-public crate-tiny-lz4-decoder-sys-1.0.0 (c (n "tiny-lz4-decoder-sys") (v "1.0.0") (d (list (d (n "lz4-builder") (r "^1.0.0") (d #t) (k 0)))) (h "0zq893vq4cvqv5wlly983r3b1xcx3735lh0ah48pcsma1m2ajhn5") (y #t)))

(define-public crate-tiny-lz4-decoder-sys-1.0.1 (c (n "tiny-lz4-decoder-sys") (v "1.0.1") (d (list (d (n "lz4-builder") (r "^1.0.1") (d #t) (k 0)))) (h "0bnwvi53wp4jihq1cry5z0k18vj3jncg5vd33751xywjkipg89s2")))

