(define-module (crates-io ti ny tinybit) #:use-module (crates-io))

(define-public crate-tinybit-0.1.1 (c (n "tinybit") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.19.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "euclid") (r "^0.22.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0z700k482n15l0h8fbs3rh44wabargd430vyhnh0gwhipvii167p") (y #t)))

(define-public crate-tinybit-0.2.0 (c (n "tinybit") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (o #t) (d #t) (k 0)))) (h "0v5xrf2x28zblj8cllfw2xpzs0hci8rp34kp551594768rbrh7q7") (f (quote (("derive" "serde" "serde_json" "crossterm/serde") ("default")))) (y #t)))

(define-public crate-tinybit-0.3.0 (c (n "tinybit") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (o #t) (d #t) (k 0)))) (h "0ajiiwjfb6vzyf8q6wzksvqihjydy6k85vwwpcyfgc7vz1ahilj5") (f (quote (("derive" "serde" "serde_json" "crossterm/serde") ("default")))) (y #t)))

