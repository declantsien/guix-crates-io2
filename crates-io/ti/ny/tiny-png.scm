(define-module (crates-io ti ny tiny-png) #:use-module (crates-io))

(define-public crate-tiny-png-0.0.0 (c (n "tiny-png") (v "0.0.0") (d (list (d (n "dioxus") (r "^0.2.3") (d #t) (k 0)) (d (n "imagequant") (r "^4.0.0") (d #t) (k 0)))) (h "1baszci8skp4llak94i83giy0xic3kshbdifl4kfskj88z0mr5wk") (f (quote (("default"))))))

(define-public crate-tiny-png-0.1.0 (c (n "tiny-png") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image-reducer") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)))) (h "095syq7vwn9g4jf026fbcr5xy085ya6np4xv3rlwhdc62v5fa9jg") (f (quote (("default"))))))

