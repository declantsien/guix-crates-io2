(define-module (crates-io ti ny tiny_json_derive) #:use-module (crates-io))

(define-public crate-tiny_json_derive-0.1.0 (c (n "tiny_json_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w90qlsypn7diqiblf1qdyz8plk7f8jjdpsrzab5f3hhxzbi6cip")))

(define-public crate-tiny_json_derive-0.1.1 (c (n "tiny_json_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x3yza3m38r3v5wzvwf6jml15ymdr6dgndz334s30v44am9yqvmz")))

(define-public crate-tiny_json_derive-0.1.2 (c (n "tiny_json_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17nap9yr36zcn26hi0rd73r01nb8b3km0rd1fkwxvya3f06ljmi3")))

