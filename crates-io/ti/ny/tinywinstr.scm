(define-module (crates-io ti ny tinywinstr) #:use-module (crates-io))

(define-public crate-tinywinstr-0.0.1 (c (n "tinywinstr") (v "0.0.1") (h "047bl12dfkgv01sb2hssvpzywcm08lam9k4qfd9wjs9lhnkzws7n")))

(define-public crate-tinywinstr-0.0.2 (c (n "tinywinstr") (v "0.0.2") (h "1dflqn1qwnrl7ywxyjnc21xraajazbijfg4xvmjz5z30s4w5g741")))

(define-public crate-tinywinstr-0.0.3 (c (n "tinywinstr") (v "0.0.3") (h "1agc43rinq3c6m7lq2zpj6zlh9xzsq66lmmxc14rgh71wvp856hx")))

(define-public crate-tinywinstr-0.0.4 (c (n "tinywinstr") (v "0.0.4") (h "0jc3mdv4jn4ppknzks55i3pbb2w96r3a2m3168csp805nrm7fif9")))

(define-public crate-tinywinstr-0.0.5 (c (n "tinywinstr") (v "0.0.5") (h "1i5n55ppq09cpfbykqxn6jcw9yhz6wzgdskr0m1zxyws45rnpiwk")))

