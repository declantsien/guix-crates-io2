(define-module (crates-io ti ny tinytemplate) #:use-module (crates-io))

(define-public crate-tinytemplate-1.0.0 (c (n "tinytemplate") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s96ykhrk20hh42g5zjfm4ri5wknj1n08cvbrk6ki1rma82hggpy")))

(define-public crate-tinytemplate-1.0.1 (c (n "tinytemplate") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05b1jllddgya0n3qn1jms7nizaid0y3krg87p19gnji7jj40hmbn")))

(define-public crate-tinytemplate-1.0.2 (c (n "tinytemplate") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "084w41m75i95sdid1wwlnav80jsl1ggyryl4nawxvb6amigvfx25")))

(define-public crate-tinytemplate-1.0.3 (c (n "tinytemplate") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06ipxjwl1w6synvql8b50qxbqv0w04agvmmfqcdynr9ygmkcd8sp")))

(define-public crate-tinytemplate-1.0.4 (c (n "tinytemplate") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14a8zs5xvgz11jg9n4nygpiml5hs4zfhkwxrp3ff0cwlr5dbrr25")))

(define-public crate-tinytemplate-1.1.0 (c (n "tinytemplate") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0by1k1hdz6jgv4ykd0izirwsm6p3qb6s9g1jb4ffqg500ihcfgbd")))

(define-public crate-tinytemplate-1.2.0 (c (n "tinytemplate") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x6a4fb0wgk5m5fwc30jm02pdxafvqqcbbamqk8a41mddxhsibd2")))

(define-public crate-tinytemplate-1.2.1 (c (n "tinytemplate") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g5n77cqkdh9hy75zdb01adxn45mkh9y40wdr7l68xpz35gnnkdy")))

