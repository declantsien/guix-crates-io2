(define-module (crates-io ti ny tinydyn) #:use-module (crates-io))

(define-public crate-tinydyn-0.1.0 (c (n "tinydyn") (v "0.1.0") (d (list (d (n "tinydyn_derive") (r "^0.1.0") (d #t) (k 0)))) (h "09d7f91pv9g4cm1k8dmvgm36cvqzbv34skiplm0q9g8n8rkq6yp8")))

(define-public crate-tinydyn-0.1.1 (c (n "tinydyn") (v "0.1.1") (d (list (d (n "tinydyn_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0ds8ripjr0m6fsx26384pmgp2xfyrhrp2dfl8bg6np697zbdav7y")))

