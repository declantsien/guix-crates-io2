(define-module (crates-io ti ny tiny-crypto) #:use-module (crates-io))

(define-public crate-tiny-crypto-0.1.0 (c (n "tiny-crypto") (v "0.1.0") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "libaes") (r "^0.7.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "0kh6vcl0r0y2p5yv24jhf6vcs1l1c8zw5v8n6ckdh4wdbm30p477")))

(define-public crate-tiny-crypto-0.1.1 (c (n "tiny-crypto") (v "0.1.1") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "libaes") (r "^0.7.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "09xy8nklj3l0kqkg10kdidp6vx8z8whcw6dznp9gyllh6xcrgmgj")))

(define-public crate-tiny-crypto-0.1.2 (c (n "tiny-crypto") (v "0.1.2") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "libaes") (r "^0.7.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1plk4p4c26v4p5x7fllq8fvp3qaaaymjx5dqdr17ldn5h5m9j0sz")))

(define-public crate-tiny-crypto-0.1.3 (c (n "tiny-crypto") (v "0.1.3") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "libaes") (r "^0.7.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1rlcygblizs51x1x3qa90kvcxg49m8li3zkcf7rs0p67mb9yg723")))

