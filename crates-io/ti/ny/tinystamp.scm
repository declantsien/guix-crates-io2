(define-module (crates-io ti ny tinystamp) #:use-module (crates-io))

(define-public crate-tinystamp-0.1.0 (c (n "tinystamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "time") (r "^0.3.34") (f (quote ("formatting"))) (d #t) (k 2)))) (h "1yqw5l9i1l1hl9lvl701x9z3igxlfaifiz3a2avs87sdiy5lnrr5") (f (quote (("std") ("default" "std"))))))

