(define-module (crates-io ti ny tiny-artnet-bytes-no-atomic) #:use-module (crates-io))

(define-public crate-tiny-artnet-bytes-no-atomic-1.2.1 (c (n "tiny-artnet-bytes-no-atomic") (v "1.2.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "02y84cz74zn4assk4miyv2z5lcmbi2255vzbv4rcbmnighr0jlyw") (f (quote (("std") ("default" "std"))))))

(define-public crate-tiny-artnet-bytes-no-atomic-1.0.1 (c (n "tiny-artnet-bytes-no-atomic") (v "1.0.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1nqj4kghdhx21sh6nyslk0ivqqiphd4b0qy52ybhq93w0zqzcp07") (f (quote (("std") ("default" "std"))))))

(define-public crate-tiny-artnet-bytes-no-atomic-1.0.2 (c (n "tiny-artnet-bytes-no-atomic") (v "1.0.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "13adrsa6gzypdzl0r564nkcva9ms0pyaqns8af0sh9asimn9gi7g") (f (quote (("std") ("default" "std"))))))

(define-public crate-tiny-artnet-bytes-no-atomic-1.2.2 (c (n "tiny-artnet-bytes-no-atomic") (v "1.2.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1wwaljds090mqvsn427krfamb49vgfg7bs5yy65dczwa77q5fv62") (f (quote (("std") ("default" "std"))))))

