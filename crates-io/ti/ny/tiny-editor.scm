(define-module (crates-io ti ny tiny-editor) #:use-module (crates-io))

(define-public crate-tiny-editor-0.1.0 (c (n "tiny-editor") (v "0.1.0") (h "13c643g4nbihdf520gimzswvq66j08dbfpa8g4fa3wkg5papk8r7") (y #t)))

(define-public crate-tiny-editor-0.1.1 (c (n "tiny-editor") (v "0.1.1") (h "139pwsvi1ckaszfpxkzd2nh7q50wqdl6l18ckvmiv5ddlp19kmdr") (y #t)))

