(define-module (crates-io ti ny tinysyscall) #:use-module (crates-io))

(define-public crate-tinysyscall-0.1.0 (c (n "tinysyscall") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 2)))) (h "1mbshn7bjv29wij2kn1hnywfxv9jjqrp21ly1r3m0m493fi5ilp3")))

(define-public crate-tinysyscall-0.1.1 (c (n "tinysyscall") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 2)))) (h "0zpfdhhyz0bik1yhsgbc9s355sd01hsp8mlkc3y5kacp3wjdq9c6")))

