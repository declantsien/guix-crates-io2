(define-module (crates-io ti ny tinysh) #:use-module (crates-io))

(define-public crate-tinysh-0.0.1 (c (n "tinysh") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xsfq5rs7c69h71lszffdppws4k10qw5qgmk00wbw1ncynv2z0r9")))

(define-public crate-tinysh-0.0.2 (c (n "tinysh") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "114v4civfs42vjlgcb8rbfy2fgm6iryz9pxizayd3781xicl06zs")))

