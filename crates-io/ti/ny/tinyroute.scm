(define-module (crates-io ti ny tinyroute) #:use-module (crates-io))

(define-public crate-tinyroute-0.1.0 (c (n "tinyroute") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("rt-multi-thread" "net" "io-util" "macros" "sync" "time"))) (d #t) (k 0)))) (h "01ya1sim7a54r1qyrkv8n8px6jv7g8rdvfx3qrhjfzlh9z5ns3l0")))

