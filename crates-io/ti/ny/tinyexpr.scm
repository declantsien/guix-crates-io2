(define-module (crates-io ti ny tinyexpr) #:use-module (crates-io))

(define-public crate-tinyexpr-0.1.0 (c (n "tinyexpr") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "1py5j30ia3bwm2zrf8f45zp6k3prm9f00rcmscyh1w9dm21qm2h6")))

(define-public crate-tinyexpr-0.1.1 (c (n "tinyexpr") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "0z3b8nzjc2di4kmsa5jx6yimiqv3nvihx1kk073l27a0kynqc92i")))

