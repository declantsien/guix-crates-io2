(define-module (crates-io ti ny tinysearch-shared) #:use-module (crates-io))

(define-public crate-tinysearch-shared-0.1.0 (c (n "tinysearch-shared") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)))) (h "10f2pbk3m5xplf5r56jfgg20sg4j100g81kqghbffrlplyb1ccmq")))

(define-public crate-tinysearch-shared-0.2.0 (c (n "tinysearch-shared") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)))) (h "08gifp8ps5r0br9pbay2m48b37755gs0fipizabd69m8ml339x5m")))

(define-public crate-tinysearch-shared-0.2.1 (c (n "tinysearch-shared") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.1") (d #t) (k 0)))) (h "0n7a7z0fvgd0b4imsy7r7ikqlady5g12kzm9ximzq4yhiybpvvzj")))

(define-public crate-tinysearch-shared-0.6.0 (c (n "tinysearch-shared") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "1gxd0j6p19g29ycw46zf5pcv9zijayzmgcvjl7hjbwj8vi8bkac1")))

(define-public crate-tinysearch-shared-0.6.1 (c (n "tinysearch-shared") (v "0.6.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "0s2ndkh5ypksrrkqr70p9smxjizifh39b7hdv1hlbzv3xgjiyl92")))

(define-public crate-tinysearch-shared-0.6.2 (c (n "tinysearch-shared") (v "0.6.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "1jhfhgjyl7k24iijnzih3zqa9p5m7i842hvdhnah675148alac60")))

(define-public crate-tinysearch-shared-0.6.3 (c (n "tinysearch-shared") (v "0.6.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "0qgbjh70xill81hyjygim7n3marac0mh9xfpzfmyylyr0s637nzj")))

(define-public crate-tinysearch-shared-0.6.4 (c (n "tinysearch-shared") (v "0.6.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "0xxy06bwdpbql77c12jvl0ygsvdwhxrfq8nlm3gm3w3vb0k6cv88")))

(define-public crate-tinysearch-shared-0.7.0 (c (n "tinysearch-shared") (v "0.7.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xorf") (r "^0.7.2") (f (quote ("serde"))) (k 0)))) (h "0x0f4kr4x2dg14alxk60pnbm48acah7h72i4r0a855sssgkfc42f")))

