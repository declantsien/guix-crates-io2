(define-module (crates-io ti ny tiny-serve) #:use-module (crates-io))

(define-public crate-tiny-serve-0.1.0 (c (n "tiny-serve") (v "0.1.0") (d (list (d (n "livid-server") (r "^0.2") (d #t) (k 0)))) (h "1jn25cpz2v3fcxw0c5acvaqq12qsx4kp8xdwml90rxpgvxl19f8h")))

(define-public crate-tiny-serve-0.1.1 (c (n "tiny-serve") (v "0.1.1") (d (list (d (n "livid-server") (r "^0.2") (d #t) (k 0)))) (h "0dpp6fqcbd0xg8vyjvvdmqvvs19590zqiyr0a0w8wdi5xg5kdavn")))

