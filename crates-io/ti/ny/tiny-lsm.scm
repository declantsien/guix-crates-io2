(define-module (crates-io ti ny tiny-lsm) #:use-module (crates-io))

(define-public crate-tiny-lsm-0.1.0 (c (n "tiny-lsm") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1ab203v9mij1vqgxibzpiq7k5kb74m8cmjphxvx0c7w8n1sqa0nw")))

(define-public crate-tiny-lsm-0.1.1 (c (n "tiny-lsm") (v "0.1.1") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1l6cn31g5gb24hkcqrbarwyljs3gnh4v24r7zmscs0ixv3a1313c")))

(define-public crate-tiny-lsm-0.1.2 (c (n "tiny-lsm") (v "0.1.2") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1q5iq38gygybsngdlb3gvjxfbvjmdr6c5z7j9j6p1f7rgfgsqd07")))

(define-public crate-tiny-lsm-0.1.4 (c (n "tiny-lsm") (v "0.1.4") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "0fflfmwzddnkvvw7q25blyxzs0mizfsbvqfgck4skbwdd1v5d3xk")))

(define-public crate-tiny-lsm-0.2.1 (c (n "tiny-lsm") (v "0.2.1") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "fuzzcheck") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "11bc7jry8lbhshipcdhlsjygi36zgza89ly7rll83yn4daqfsbja")))

(define-public crate-tiny-lsm-0.2.2 (c (n "tiny-lsm") (v "0.2.2") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "fuzzcheck") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "01hdiag2sarbml41w9xm8ympfw98v9q9py16jmq8806z4w916qr4")))

(define-public crate-tiny-lsm-0.2.3 (c (n "tiny-lsm") (v "0.2.3") (d (list (d (n "crc32fast") (r "^1.2.2") (d #t) (k 0)) (d (n "fuzzcheck") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1ylx1wbq9n8jxifriz94zwgq2wvfm9h0m5nlfsb1lnpkf4y5xrqb")))

(define-public crate-tiny-lsm-0.2.4 (c (n "tiny-lsm") (v "0.2.4") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "14ap3yhfbq4rfi73zvivxb29hbvc1ysnh3vn0fw30hrs4lfsy54x")))

(define-public crate-tiny-lsm-0.3.0 (c (n "tiny-lsm") (v "0.3.0") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "15z31c0g8kxgl0hp8magv0dk2m9drgjzscpbpfdrx5nvkv5j5n4w")))

(define-public crate-tiny-lsm-0.3.1 (c (n "tiny-lsm") (v "0.3.1") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1ywjmq6mk49an6dvscmpahqmjw52jlsyci33ls4jrb9ds2yd2d9a")))

(define-public crate-tiny-lsm-0.4.0 (c (n "tiny-lsm") (v "0.4.0") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "15vdlpqcizr2hy19xbjs93yjcpp8ngrnd2b3ylaa68039mb3s649")))

(define-public crate-tiny-lsm-0.4.1 (c (n "tiny-lsm") (v "0.4.1") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "09d3g38rv30lvr8ddbm55ldbkfx07h7k25zlhvr9qnn9zrl8hvic")))

(define-public crate-tiny-lsm-0.4.3 (c (n "tiny-lsm") (v "0.4.3") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "06ligyhb65wd1csmy3g6qnbphd6vvwqjdbwgc15bygms94khf6yz")))

(define-public crate-tiny-lsm-0.4.4 (c (n "tiny-lsm") (v "0.4.4") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1vrikjwj6y4ss030vgdcxyawbcf76xpwi6dqzm2mg6jl1ywx2cc7") (f (quote (("no_fuzz"))))))

(define-public crate-tiny-lsm-0.4.5 (c (n "tiny-lsm") (v "0.4.5") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "01znhq6jpbd33j1fhwg6v15pjsvr748zwgf1l7bvf5k9blmdnq5m") (f (quote (("no_fuzz"))))))

(define-public crate-tiny-lsm-0.4.6 (c (n "tiny-lsm") (v "0.4.6") (d (list (d (n "crc32fast") (r "^1.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fuzzcheck") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "100h98yhrimjp0ifypppvf97wmbgjdbwrymqck8m0h41ry338gvk") (f (quote (("no_fuzz"))))))

