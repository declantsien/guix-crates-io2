(define-module (crates-io ti ny tinybox) #:use-module (crates-io))

(define-public crate-tinybox-0.1.0 (c (n "tinybox") (v "0.1.0") (h "0hmsfkriz7krphz1lzq4xik561bwqfrkm5gc1r1ga6c46yaa60kj")))

(define-public crate-tinybox-0.2.0 (c (n "tinybox") (v "0.2.0") (h "0jan5qa3cl05f4rmfdgjzkz94a34dnkm5ympgirdb58wfvb1l3bb")))

(define-public crate-tinybox-0.3.0 (c (n "tinybox") (v "0.3.0") (h "0hz6cqy33f8snr76nsivg5ghyan6hrfmw70scaq2397jqif3w5cs")))

(define-public crate-tinybox-0.3.1 (c (n "tinybox") (v "0.3.1") (h "1s9vfzgpyn98nxzb4ich0lkgj7ail1xlzm9plfcsqsxkc7ss0wwv")))

