(define-module (crates-io ti ny tiny_lib) #:use-module (crates-io))

(define-public crate-tiny_lib-0.0.1 (c (n "tiny_lib") (v "0.0.1") (h "18b632z0vvq97pvq3m041q53xzk1bikqjkbzi187yn9fmrzsq0l1")))

(define-public crate-tiny_lib-0.0.2 (c (n "tiny_lib") (v "0.0.2") (h "02ik5sanw0gnyszlcj7c0yrw32vkbqa0yrgg269ly050p0vjg49n")))

