(define-module (crates-io ti ny tiny_tiff) #:use-module (crates-io))

(define-public crate-tiny_tiff-0.1.0 (c (n "tiny_tiff") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "1nzd2vckcg0kz1g0ir7gnyknaiv6m9s9qmx1f5iwrfj82j2gqk9f")))

(define-public crate-tiny_tiff-0.2.0 (c (n "tiny_tiff") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "19dy8ss5yknba0ixb6g6wvd942zdlygjhxmazgl06gb78rhs32rp")))

(define-public crate-tiny_tiff-0.2.1 (c (n "tiny_tiff") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "1zjxycns9n3z8mlvd7x4g4a0ishnpnhbqy884j5nz0qsp25b0mb5")))

(define-public crate-tiny_tiff-0.3.0 (c (n "tiny_tiff") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "1z7c964k43k6vr1p6gd8wzakzj0nhfzsbm9ar1jhkqbnhicsxx88")))

(define-public crate-tiny_tiff-0.4.0 (c (n "tiny_tiff") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "10sxn839vpa5277x9w14rb9l0zwf15n2v41i60qr8q1mrj6gz9s7")))

(define-public crate-tiny_tiff-0.5.0 (c (n "tiny_tiff") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0qarw80rn1s2md2absnki2yv4421703008xwy0dcirlxmw5vma0k")))

(define-public crate-tiny_tiff-0.5.1 (c (n "tiny_tiff") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "14x9r99cvl7imin5vcf57agh8q16dygkdjgpxb0hh518vcl8ihnw") (y #t)))

(define-public crate-tiny_tiff-0.5.2 (c (n "tiny_tiff") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0yms8mf0m30yhflc0nim8p2sifgha6sa3s19qc9alkqcqg7brn4p")))

