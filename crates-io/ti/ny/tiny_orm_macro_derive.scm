(define-module (crates-io ti ny tiny_orm_macro_derive) #:use-module (crates-io))

(define-public crate-tiny_orm_macro_derive-0.2.0 (c (n "tiny_orm_macro_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hfaz1y9bh08k1bbfhbsd3ncdbk5zq4p8j6q15c18vkrjdj5nqi6")))

(define-public crate-tiny_orm_macro_derive-0.2.1 (c (n "tiny_orm_macro_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "019vm0py3621p12nyszm5yzjqnh0j1qqna7cbca82yl7dw4bp0yz")))

(define-public crate-tiny_orm_macro_derive-0.2.2 (c (n "tiny_orm_macro_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d4mm71bh6sfjh7kjds35ydf0j59h66p2rl4cmdzaip6y7myyjmg")))

(define-public crate-tiny_orm_macro_derive-0.2.3 (c (n "tiny_orm_macro_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cc4frfxzi3370xm2l7ca8fa6cxmgycpa5p94hnxsrz4w8zbnk4q")))

(define-public crate-tiny_orm_macro_derive-0.2.4 (c (n "tiny_orm_macro_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "099yv56x3rvyfv6axzlii4b9qqaynx3sm9dnkxbwxjqb3smxbd28")))

(define-public crate-tiny_orm_macro_derive-0.2.5 (c (n "tiny_orm_macro_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j13l03jn1ix7r5547iy13aw975l160f2r0fh7kxr6816xw53a56")))

(define-public crate-tiny_orm_macro_derive-0.2.6 (c (n "tiny_orm_macro_derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pn7ij87014586lxhmlyb4zad8j2rjqcmyxvid4phrm7bxbmihsc")))

(define-public crate-tiny_orm_macro_derive-0.2.7 (c (n "tiny_orm_macro_derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sji6m7y5s3ikwhwv57hcb4wg8c48i81qcip98i7ns17yxm4n17p")))

(define-public crate-tiny_orm_macro_derive-0.2.8 (c (n "tiny_orm_macro_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19z6al9l1mxb7rdf7dblfvv4gmx84pl43lmkmnbivh2qqpbacsgl")))

(define-public crate-tiny_orm_macro_derive-0.2.9 (c (n "tiny_orm_macro_derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gzazv0j5ac8xqmkq9dycxkiqgzwzd6f4gyxxkadxca9dyg08kbp")))

(define-public crate-tiny_orm_macro_derive-0.2.10 (c (n "tiny_orm_macro_derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xlg5mlhgx07m9df6dsy32azxaiwq7jjbb36gprs4dcw9rrjwfck")))

(define-public crate-tiny_orm_macro_derive-0.3.0 (c (n "tiny_orm_macro_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08n4iiqqkkvvbaad6dhyciyfs5m6jp3f1z49m229ln15vf9rmkfa")))

(define-public crate-tiny_orm_macro_derive-0.3.1 (c (n "tiny_orm_macro_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g62sb9qacjnfz16khrkyjgmv660gxjpnni4g6arsjpqryfbyh10")))

(define-public crate-tiny_orm_macro_derive-0.4.0 (c (n "tiny_orm_macro_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lbs86si6w2hd3dw2lxplwvvxkwb38kml7pywkbr1sg9vc3pndw0")))

(define-public crate-tiny_orm_macro_derive-0.4.1 (c (n "tiny_orm_macro_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cvmwikzv5x7lbd2fyiy542gjs2dxgfy2bsianbn1vphfav99jrv")))

(define-public crate-tiny_orm_macro_derive-0.4.2 (c (n "tiny_orm_macro_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ss55gd7pg98z5vlcay1s00inv868v7h3k0b7mpdcxpbpcv9dxrn")))

(define-public crate-tiny_orm_macro_derive-0.4.3 (c (n "tiny_orm_macro_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h6sszcgn9fm61l01667zf1v2lly8g588d46xwarqd9bw8hqf1xa")))

(define-public crate-tiny_orm_macro_derive-0.4.4 (c (n "tiny_orm_macro_derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f7vf76nwfnwkcxaiiijvb91f8lpymvyhkai6053qsk515kayhff") (y #t)))

(define-public crate-tiny_orm_macro_derive-0.4.5 (c (n "tiny_orm_macro_derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18k79spqkpqxgbsqvr3lapr2i4njz7yb4sh9l28q7nivi6vq4v3i")))

