(define-module (crates-io ti ny tiny_skia_display) #:use-module (crates-io))

(define-public crate-tiny_skia_display-0.1.0 (c (n "tiny_skia_display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.2") (d #t) (k 0)) (d (n "orbclient") (r "^0.3.31") (d #t) (k 2)) (d (n "tiny-skia") (r "^0.5.1") (d #t) (k 0)))) (h "1l98fxqy3rpmywx265p8mc60aqwhshs1d7rcpzhz5fb3kd7syrd6")))

