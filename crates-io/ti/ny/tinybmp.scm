(define-module (crates-io ti ny tinybmp) #:use-module (crates-io))

(define-public crate-tinybmp-0.1.0 (c (n "tinybmp") (v "0.1.0") (d (list (d (n "nom") (r "^4.2.1") (k 0)))) (h "047sp4b77c5an0zlzch7vgch36vc13q3m2mg8lismc9ngvg3ygjh")))

(define-public crate-tinybmp-0.1.1 (c (n "tinybmp") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.0") (k 0)))) (h "0c2j1ypyalrkdq3f4wy4pym728mj2vp9vi3js3b6f597idzjpl9h")))

(define-public crate-tinybmp-0.2.0 (c (n "tinybmp") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.6.0-alpha.3") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.0-alpha.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "0116j0bsxax5pigd3sl3sgl2ih14m2xrfan64zhkgrf7liybxvxl") (f (quote (("graphics" "embedded-graphics")))) (y #t)))

(define-public crate-tinybmp-0.2.1 (c (n "tinybmp") (v "0.2.1") (d (list (d (n "embedded-graphics") (r "^0.6.0-beta.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "0m8b6rsav1knl9w9rdrhdzxdy9ja6333p90vlkqb6vf112gsh5cl") (f (quote (("graphics" "embedded-graphics"))))))

(define-public crate-tinybmp-0.2.2 (c (n "tinybmp") (v "0.2.2") (d (list (d (n "embedded-graphics") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "0qw0r9xx24zs9pcfxdyyqvrwwhsazash1wlc8blqq0bzgpilcsaw") (f (quote (("graphics" "embedded-graphics"))))))

(define-public crate-tinybmp-0.2.3 (c (n "tinybmp") (v "0.2.3") (d (list (d (n "embedded-graphics") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.0") (k 0)))) (h "0p8v05zv8870bb9x0w64sclgvsk7k7irb2xrqdh8y4qp754m47kq") (f (quote (("graphics" "embedded-graphics"))))))

(define-public crate-tinybmp-0.3.0-alpha.1 (c (n "tinybmp") (v "0.3.0-alpha.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-alpha.2") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "1lvna3qsbk83cca0p14p8rlk3l3dzinv91c1ss98himfib8rvzq9")))

(define-public crate-tinybmp-0.3.0-beta.1 (c (n "tinybmp") (v "0.3.0-beta.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "02y6ih9nd9vlv6v1qsmlcln471cnqb2i2n30fmk0n95qs2b9xwya")))

(define-public crate-tinybmp-0.3.0-beta.2 (c (n "tinybmp") (v "0.3.0-beta.2") (d (list (d (n "embedded-graphics") (r "^0.7.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "1avvwz261wi0kqad3ya0rx0vwc7kqqix8gdh7p3yrksdgdj8qy9c")))

(define-public crate-tinybmp-0.3.0 (c (n "tinybmp") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.7.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "1mjrvxhpj05qaysr781nrkcrs4fpx74r99ypw94skmaqpa5mwg4r")))

(define-public crate-tinybmp-0.3.1 (c (n "tinybmp") (v "0.3.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "0rq95j74295b7xrbjnf4d2zh1pfiiv5idrgr0rwmjsrp9wiicp9z")))

(define-public crate-tinybmp-0.3.2 (c (n "tinybmp") (v "0.3.2") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "0svsh50p7qw78ivnzg32c4l15jc54gbzhvbfxpzb868r6jqqg5k2")))

(define-public crate-tinybmp-0.3.3 (c (n "tinybmp") (v "0.3.3") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (k 0)))) (h "0f4qyji4qgp7qg307b2804x07fbq5a80j0k1q8np2c6x93p3m1s4")))

(define-public crate-tinybmp-0.4.0 (c (n "tinybmp") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)))) (h "0hz7p8h2vbk7xzbllwfgvl5ydxliczajg0m04si6imvmg589r58f")))

(define-public crate-tinybmp-0.5.0 (c (n "tinybmp") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)))) (h "0q6wr1zsjghyqxbgkrq7rchfz07gjk35975xbbqmy5w2wc0c0z0r")))

