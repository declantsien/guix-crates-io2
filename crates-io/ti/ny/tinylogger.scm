(define-module (crates-io ti ny tinylogger) #:use-module (crates-io))

(define-public crate-tinylogger-0.1.0 (c (n "tinylogger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "029j2bwq8i53glakg88r7cgq1sf2h6m8ivk0r4vmwmk949q55lz1")))

(define-public crate-tinylogger-0.1.1 (c (n "tinylogger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g9f2hh7gargd628cxlsc7k0jpbi3l7jnqmvwyl6mgvqy1bd4bz9")))

