(define-module (crates-io ti ny tiny-native-scheduler) #:use-module (crates-io))

(define-public crate-tiny-native-scheduler-0.1.0 (c (n "tiny-native-scheduler") (v "0.1.0") (d (list (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0prgzl40rjg1yjmirm1rsfhscbbyfcafqly0iz61n8asmq6q8wm7")))

(define-public crate-tiny-native-scheduler-0.1.1 (c (n "tiny-native-scheduler") (v "0.1.1") (d (list (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sjr0y5jch0gh7ic1kirklkkipsy3vbd8j063kszbvlw480sjmqm")))

