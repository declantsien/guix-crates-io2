(define-module (crates-io ti ny tiny-game-framework) #:use-module (crates-io))

(define-public crate-tiny-game-framework-0.1.0 (c (n "tiny-game-framework") (v "0.1.0") (h "18365xz5rfhzqn0ziq32h6banp4lc2jcvifh3s16nvqznpmxk55l") (y #t)))

(define-public crate-tiny-game-framework-0.0.1 (c (n "tiny-game-framework") (v "0.0.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "06bf39k79rzylp814p2lak6l7j0p0cqj25jwyxszy1xik7ll1yrf")))

(define-public crate-tiny-game-framework-0.0.11 (c (n "tiny-game-framework") (v "0.0.11") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0bamc0p03fznckyxa6y4q329q36crcrwwqnravsis42snjzwl8hy")))

(define-public crate-tiny-game-framework-0.0.12 (c (n "tiny-game-framework") (v "0.0.12") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0alpd47bszai167qpbpxwccxpp0g1nqpc4xxzicpnc1a3fbf762a")))

(define-public crate-tiny-game-framework-0.0.131 (c (n "tiny-game-framework") (v "0.0.131") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0aq6amav9ljsjm1wcyp6krld4q4zs3y6b4rc74hbwy4a1ia26asf")))

(define-public crate-tiny-game-framework-0.0.132 (c (n "tiny-game-framework") (v "0.0.132") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0apw78h6ajw3ab8v2x7w8fidhba444j9vm26kinbv6k5al96fr8w")))

(define-public crate-tiny-game-framework-0.0.132000000001 (c (n "tiny-game-framework") (v "0.0.132000000001") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hm03r2ddbpx4160znw4r10a0x895pgn1d17m5zb0cnkp22l3vzq") (y #t)))

(define-public crate-tiny-game-framework-0.0.133 (c (n "tiny-game-framework") (v "0.0.133") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hl40wdmpbhpfznslq2ii8mfkng5i7n6kyrpss63dmqdjkv2qz6d")))

(define-public crate-tiny-game-framework-0.0.1335 (c (n "tiny-game-framework") (v "0.0.1335") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y9sxq438lphibbb8n5vg87gvf2hg9bkq7dy0xd3sqw7ryjf7f9d") (y #t)))

(define-public crate-tiny-game-framework-0.0.134 (c (n "tiny-game-framework") (v "0.0.134") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "glfw") (r "^0.55.0") (d #t) (k 0)) (d (n "imgui") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y9k1z1ygnkmfnxcr96czgv83z2j6pxmpz4hccw0pp273lxzpwmh")))

