(define-module (crates-io ti ny tinyecs) #:use-module (crates-io))

(define-public crate-tinyecs-0.0.1 (c (n "tinyecs") (v "0.0.1") (h "06p73lc797s70gxfin38i858yzywjzjp2vj4axg810m93zkcgb9i")))

(define-public crate-tinyecs-0.0.2 (c (n "tinyecs") (v "0.0.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0hywrp9j5jjvg7ib1qj12lp0pinyymh8dmay1g531n2345wbwi4w")))

(define-public crate-tinyecs-0.0.3 (c (n "tinyecs") (v "0.0.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tinyprof") (r "^0.0.1") (d #t) (k 0)))) (h "1w5qvf0qhypa8qj1j0nr57xa1dhf9qyvqy2s4vbmlnm7wf1bdjr6") (f (quote (("prof"))))))

