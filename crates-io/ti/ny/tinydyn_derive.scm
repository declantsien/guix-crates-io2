(define-module (crates-io ti ny tinydyn_derive) #:use-module (crates-io))

(define-public crate-tinydyn_derive-0.1.0 (c (n "tinydyn_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0yx702rsz9wwyszmm20wyx10zcqkwxbqcq6q3wp33s3wn9030rcx")))

(define-public crate-tinydyn_derive-0.1.1 (c (n "tinydyn_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0q8hqxfv7m2ms4a0kjll7jvpbrf283fqpm1qncrpkccxxvnpr074")))

