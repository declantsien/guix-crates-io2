(define-module (crates-io ti ny tinybitvec) #:use-module (crates-io))

(define-public crate-tinybitvec-0.1.0 (c (n "tinybitvec") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)))) (h "098xqfirc2xc68c2lyvhqzn3n65v38v2i6wcl28c576grz0i5xg6")))

