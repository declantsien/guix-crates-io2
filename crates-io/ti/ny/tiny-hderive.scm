(define-module (crates-io ti ny tiny-hderive) #:use-module (crates-io))

(define-public crate-tiny-hderive-0.1.0 (c (n "tiny-hderive") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "ethsign") (r "^0.3") (f (quote ("secp256k1-rs"))) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6") (d #t) (k 2)))) (h "14yblqc6rcy1z87b1q5rhnxrfm8siw407vw2z7ah7c45v5ak1fb1")))

(define-public crate-tiny-hderive-0.2.0 (c (n "tiny-hderive") (v "0.2.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "ethsign") (r "^0.3") (f (quote ("secp256k1-rs"))) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.2") (d #t) (k 0)) (d (n "memzero") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6") (d #t) (k 2)))) (h "18nk1la2fywangsbmj8h7svx29rnrllw0v3zziji0ji45867ckrx")))

(define-public crate-tiny-hderive-0.2.1 (c (n "tiny-hderive") (v "0.2.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "ethsign") (r "^0.3") (f (quote ("secp256k1-rs"))) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.2") (d #t) (k 0)) (d (n "memzero") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6") (d #t) (k 2)))) (h "0s5f7sj0izkp9kw97m0f75dw397cpfid3wbgqpvzvp39fd17jd44")))

(define-public crate-tiny-hderive-0.3.0 (c (n "tiny-hderive") (v "0.3.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "ethsign") (r "^0.3") (f (quote ("secp256k1-rs"))) (k 2)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "memzero") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6") (d #t) (k 2)))) (h "0h30syzhfkmhjpnkzp4vngs8bmhrjhdw3fpvyjrd8f15k6j79f01")))

