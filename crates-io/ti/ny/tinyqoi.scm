(define-module (crates-io ti ny tinyqoi) #:use-module (crates-io))

(define-public crate-tinyqoi-0.1.0 (c (n "tinyqoi") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3.0") (d #t) (k 2)))) (h "0a2iavsii7p9asvdlsjqx0a1zwc8vkshf6mhaawv6mkzfllbsc44")))

(define-public crate-tinyqoi-0.2.0 (c (n "tinyqoi") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)))) (h "0qpgzwqsa9gfm2hbw68x6x1f5iwwrq57vkp9rdxp2wwgk4ss1qjd")))

