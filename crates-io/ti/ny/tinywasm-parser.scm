(define-module (crates-io ti ny tinywasm-parser) #:use-module (crates-io))

(define-public crate-tinywasm-parser-0.0.2 (c (n "tinywasm-parser") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.0.2") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "18a1lf43gb4msbsskm1gn644qnly3bn6q7girrfj1szdciz683ry") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.0.3 (c (n "tinywasm-parser") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.0.3") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "0y6a86vy4crf982yaa35hyd8d7y6hxhzgalcg70xz3wqm3cnrdjz") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.0.4 (c (n "tinywasm-parser") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.0.4") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "1j3rx3w9kkbba8ii3zsqzk1ml7bqpk501s279c4hxx4cysz0wsl3") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.0.5 (c (n "tinywasm-parser") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.0.5-alpha.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "1dcp6w3h8v2zi310f2fjv7qy0g89kvr6m59b4my8ajp5i6lbv350") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.1.0 (c (n "tinywasm-parser") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "03r06lfv3kxqa85li6klka07jlciw3fyfkbd8dq3r7m52d3dqzv7") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.2.0 (c (n "tinywasm-parser") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "0r4vpcrw2y786q0sbmfqr3gpncd2fjxk58qbvrkw1m963fgkqfhw") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.3.0 (c (n "tinywasm-parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.3.0-alpha.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "15xg5m5cc721ypcmm389ayx9k77n254qamzrajrqkiwv588vkkgj") (f (quote (("std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.4.0 (c (n "tinywasm-parser") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.4.0") (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "02hl42dpr3bk5ddil41z1mnrs03pca8p1w2vpx223gp4bm50iq8g") (f (quote (("std" "tinywasm-types/std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.4.1 (c (n "tinywasm-parser") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.4.0") (k 0)) (d (n "wasmparser") (r "^0.100") (k 0) (p "wasmparser-nostd")))) (h "0r9mj29f9wvkrh4xdlhxam1qlkrma88azrqc2ak227dxdsjar8h8") (f (quote (("std" "tinywasm-types/std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.5.0 (c (n "tinywasm-parser") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.5.0") (k 0)) (d (n "wasmparser") (r "^0.200.3") (k 0) (p "tinywasm-wasmparser")))) (h "19ayqn9vc83ybx8nfxdqzlxvvd2cwlmzy7k7f9ny3cpsrl7qc1js") (f (quote (("std" "tinywasm-types/std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.6.0 (c (n "tinywasm-parser") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.6.0") (k 0)) (d (n "wasmparser") (r "^0.202.0") (k 0) (p "tinywasm-wasmparser")))) (h "04y2iifxx5rcaik1xpn2pj86jy36il3rpi63bl0asifdp522q1dh") (f (quote (("std" "tinywasm-types/std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.6.1 (c (n "tinywasm-parser") (v "0.6.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.6.0") (k 0)) (d (n "wasmparser") (r "^0.207") (f (quote ("validate"))) (k 0)))) (h "0xprfl2vny17byrh6wayivb6rmj247bw1spz5yhz4sgzwg0nk71r") (f (quote (("std" "tinywasm-types/std" "wasmparser/std") ("logging" "log") ("default" "std" "logging"))))))

(define-public crate-tinywasm-parser-0.7.0 (c (n "tinywasm-parser") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tinywasm-types") (r "^0.7.0") (k 0)) (d (n "wasmparser") (r "^0.207") (f (quote ("validate"))) (k 0)))) (h "1ba7pb6kn6qaa729h2swhifw5h1iyz0nmy3sybl33xqkngykcy5m") (f (quote (("std" "tinywasm-types/std" "wasmparser/std") ("logging" "log") ("default" "std" "logging"))))))

