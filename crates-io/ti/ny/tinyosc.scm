(define-module (crates-io ti ny tinyosc) #:use-module (crates-io))

(define-public crate-tinyosc-0.0.1 (c (n "tinyosc") (v "0.0.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0i2jhrrp5wni3fxb1kl9kbfryjm18wpci98x05s7jardm5yv236d")))

(define-public crate-tinyosc-0.0.2 (c (n "tinyosc") (v "0.0.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1vlsr24xcmvy34hhlky49jgrikyiic6ykcjrmi917ksa7yhq7hqg")))

(define-public crate-tinyosc-0.0.3 (c (n "tinyosc") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)))) (h "0wv3v8b5arl8nsnqc8mpx3g2lg55xr1s2j5f3hhv2z9wbjpvqyzc")))

