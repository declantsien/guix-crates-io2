(define-module (crates-io ti ny tiny-tmpl) #:use-module (crates-io))

(define-public crate-tiny-tmpl-0.1.1 (c (n "tiny-tmpl") (v "0.1.1") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0lis0wi82mnar9lpcln9jslcxgg6xk3704q46974nh3f21b9fzwd")))

(define-public crate-tiny-tmpl-0.1.2 (c (n "tiny-tmpl") (v "0.1.2") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0sk4mckdfa5rg433f824c027lcjhaz33d2g898agn486zdq5pbqd")))

(define-public crate-tiny-tmpl-0.1.3 (c (n "tiny-tmpl") (v "0.1.3") (d (list (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)))) (h "0sdy8bzxxbl89d8y51bsz0wjjmr6nxqa3r169i2ni23q0hzxq7w4")))

