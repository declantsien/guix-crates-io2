(define-module (crates-io ti ny tiny-skia-path) #:use-module (crates-io))

(define-public crate-tiny-skia-path-0.7.0 (c (n "tiny-skia-path") (v "0.7.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0v7sf1g9avk91npxk41byswpm5msz8yh3jv7adc3vr1f1hpx6561") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.8.0 (c (n "tiny-skia-path") (v "0.8.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "1jynjivnj41jp9hsaclz2qqvn51zagwr6h2y5wd5invfk5kch8ah") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.8.1 (c (n "tiny-skia-path") (v "0.8.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "170p76l8pvlbxq3sslyq7ay41z06zz27d351pdck7189yk0mpm3b") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.8.2 (c (n "tiny-skia-path") (v "0.8.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "015126gl3bx1dfv5f2d87vlqwkxv5m2dmvgnpsm2sh34rr9mhrmx") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.8.3 (c (n "tiny-skia-path") (v "0.8.3") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "03f258mdwnhjmlf28lgiw8w5nsavh16ymnimr58qzjcg0nnfvdd4") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.8.4 (c (n "tiny-skia-path") (v "0.8.4") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "0z37bzd222q3dlahmaxp2mf1pg5v9qyz3x0j3phs0myxyg9vbgxd") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.9.0 (c (n "tiny-skia-path") (v "0.9.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "0iqwhkhrlbzdn64217c0zl8w6a8fkfvd0mis6n211j8svb6b1b7p") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.10.0 (c (n "tiny-skia-path") (v "0.10.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "0718wy4s962wgwz2x2j08w6qr3m6mvm5c98srrx6ihlsr0sslq1g") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.11.0 (c (n "tiny-skia-path") (v "0.11.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "1xa8a0wp5gf4dqz437wg036mqvyn13f6ymh8c2l0kxj29zx3z3vd") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.11.1 (c (n "tiny-skia-path") (v "0.11.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "19qwipy9vz713laxi4h7x1vcx1f509vzbnijifydkni0vv8j78wk") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.11.2 (c (n "tiny-skia-path") (v "0.11.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "0ixnrrzqi7wjb74d74ihydr4562ylm7klp56c4cpxz08jxdqdhva") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.11.3 (c (n "tiny-skia-path") (v "0.11.3") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "0hg5a0i7f505l3ix5a9ywjhjbsgqq856hwgic6pslaq5j255xqsx") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

(define-public crate-tiny-skia-path-0.11.4 (c (n "tiny-skia-path") (v "0.11.4") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "14ywbdfakvacl6rxxmzbnycplaxpc6i2linh2yqk0sp8qb07z7lw") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

