(define-module (crates-io ti ny tiny-bench) #:use-module (crates-io))

(define-public crate-tiny-bench-0.1.0 (c (n "tiny-bench") (v "0.1.0") (h "01vprl5nxwr4zk8zsf9kj1i4ms7y61xsiln6gh2rmcpjsdq9i8g4") (f (quote (("timer") ("default" "timer" "bench") ("bench"))))))

(define-public crate-tiny-bench-0.1.1 (c (n "tiny-bench") (v "0.1.1") (h "0ch97ncwp17hrbdgisdj36hqwvb2rbhrr6nvy6drv0nlmypsx3qk") (f (quote (("timer") ("default" "timer" "bench") ("bench"))))))

(define-public crate-tiny-bench-0.2.0 (c (n "tiny-bench") (v "0.2.0") (h "1dwakd7jgwpf3izw245shwy5b1mj97883m3925icqn4wc5m3cp8b") (f (quote (("timer") ("default" "timer" "bench") ("bench"))))))

(define-public crate-tiny-bench-0.3.0 (c (n "tiny-bench") (v "0.3.0") (h "0j2rsyspqbk89y6zjndpc6d36ljir1ymyj15vv6jxhaphl6q9nng") (f (quote (("timer") ("default" "timer" "bench") ("bench"))))))

