(define-module (crates-io ti ny tiny_update_notifier) #:use-module (crates-io))

(define-public crate-tiny_update_notifier-1.0.0 (c (n "tiny_update_notifier") (v "1.0.0") (d (list (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.8") (d #t) (k 0)))) (h "0ff5wq82g7hyvpb1ldzpwp5y3xd670sjncxz4qcga79kancjyc6n")))

(define-public crate-tiny_update_notifier-1.0.1 (c (n "tiny_update_notifier") (v "1.0.1") (d (list (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.8") (d #t) (k 0)))) (h "1c5i322bbs3h79adsy1pbh0n9gb5nhq4yh6ky6bfqv818pdq22r6")))

(define-public crate-tiny_update_notifier-1.1.0 (c (n "tiny_update_notifier") (v "1.1.0") (d (list (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.8") (d #t) (k 0)))) (h "15kvbf6qhdkj7yc9v52x2b1dw15vnwg8ixxzriic9prggqygr1db")))

(define-public crate-tiny_update_notifier-1.1.1 (c (n "tiny_update_notifier") (v "1.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.6.0") (d #t) (k 0)))) (h "09dmy8pm4mags5zfbns6g9czmggpl871p29dszxiq6kw2hiywzpa")))

(define-public crate-tiny_update_notifier-2.0.0 (c (n "tiny_update_notifier") (v "2.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "16fcv3cwqd1y5hbhmrdjbqxmb9h3vaxxr2sjr521y8bv5kh19xaz")))

(define-public crate-tiny_update_notifier-2.0.1 (c (n "tiny_update_notifier") (v "2.0.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1cf96qpgwyz5drx8j1yhr95rdr502aj5b3z34wcss2fybksyhsnw")))

(define-public crate-tiny_update_notifier-2.1.0 (c (n "tiny_update_notifier") (v "2.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1d37hiwfwic97w49xq8mzgpnlp0gikwri1i1219gdhlqpiir4jn1")))

(define-public crate-tiny_update_notifier-2.2.0 (c (n "tiny_update_notifier") (v "2.2.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0vrwy0z68c1wrs9a32qy8lxcmqavh65nf4sx61hq80wjdvsnd9pf")))

