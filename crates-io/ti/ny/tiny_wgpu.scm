(define-module (crates-io ti ny tiny_wgpu) #:use-module (crates-io))

(define-public crate-tiny_wgpu-0.1.0 (c (n "tiny_wgpu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0gjkfyanybnlnvymy9r474gw722l3qb6cp9qmj89xvrfbjvj2z8x")))

(define-public crate-tiny_wgpu-0.1.1 (c (n "tiny_wgpu") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1ffk4ks48ddrc41gbajkvdc0y1w53d998kzv0l4d5wa7ikghxsb2")))

(define-public crate-tiny_wgpu-0.1.2 (c (n "tiny_wgpu") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "017jii9sy6h5fj4l5wswj1q3i9l4y3l0sfk17hpd9jcv813dg1dq")))

(define-public crate-tiny_wgpu-0.1.3 (c (n "tiny_wgpu") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0wzkhlv845dfwblyzxajgqfn52l83y908l1znbv0058ggh0yjskp")))

(define-public crate-tiny_wgpu-0.1.4 (c (n "tiny_wgpu") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "17kkg5vlf7h5ymzbcnc71h1n6nfvqj863vh3daizig10d36wfk12")))

(define-public crate-tiny_wgpu-0.1.5 (c (n "tiny_wgpu") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1a72x83fgrq2fpsjfkbw5knfd4irbsa273fr3v90gi5hps19cljg")))

(define-public crate-tiny_wgpu-0.1.6 (c (n "tiny_wgpu") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "05qym60bsn28r4zdd4qmxziz55mbf772ph3kw3gzy0xrnbh5ckl1")))

(define-public crate-tiny_wgpu-0.1.7 (c (n "tiny_wgpu") (v "0.1.7") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0g0da00fzbvjn3y082vs1gygpz09p82a8rhba1ifmj30fnwzsk9x")))

(define-public crate-tiny_wgpu-0.1.8 (c (n "tiny_wgpu") (v "0.1.8") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "0xmaz2m4v0v1cqfy535i2y7lhvkjvnp4k6fi5fmc1v46hd9q9bds")))

(define-public crate-tiny_wgpu-0.1.9 (c (n "tiny_wgpu") (v "0.1.9") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1lb648ziai9ckcpnd95cp1x1dja9mvyyr35kkv7c093varid5ck9")))

(define-public crate-tiny_wgpu-0.1.10 (c (n "tiny_wgpu") (v "0.1.10") (d (list (d (n "bytemuck") (r "^1.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 2)))) (h "1n8pv2sq0dgjqq5q3ixg90700647mq7jpn2swzw7apm68qq267k6")))

