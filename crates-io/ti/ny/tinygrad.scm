(define-module (crates-io ti ny tinygrad) #:use-module (crates-io))

(define-public crate-tinygrad-0.0.0 (c (n "tinygrad") (v "0.0.0") (h "1f6cq5rjdx0v4i86k6f97wgqpk2c5ffngxaz1mysnars6g8mnfjf")))

(define-public crate-tinygrad-0.1.0 (c (n "tinygrad") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "061wcnxikc7bgndzfgc362lvsf5hfi72qd6p7bmral6lrvlm32dw")))

