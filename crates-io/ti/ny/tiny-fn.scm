(define-module (crates-io ti ny tiny-fn) #:use-module (crates-io))

(define-public crate-tiny-fn-0.1.0 (c (n "tiny-fn") (v "0.1.0") (h "02zlcils3midqg707agidx9jx942bjfgq3229jfnq9hkkk4kcaj3") (f (quote (("example")))) (y #t)))

(define-public crate-tiny-fn-0.1.1 (c (n "tiny-fn") (v "0.1.1") (h "0rgl7bny3asab4mlhsvc589vf5r0v7qzkccn2afa10jh0j9l6wh8") (f (quote (("example")))) (y #t)))

(define-public crate-tiny-fn-0.1.2 (c (n "tiny-fn") (v "0.1.2") (h "1b73inkh0ixznzqjjhw4lgq3lsw0hjw3nxws19j654l75xj4q2wn") (f (quote (("example")))) (y #t)))

(define-public crate-tiny-fn-0.1.3 (c (n "tiny-fn") (v "0.1.3") (h "18hab4v3fb7lca6slknn2jclc8xkr6b5qmjivmfp1lf3h7vmka5x") (f (quote (("example"))))))

(define-public crate-tiny-fn-0.1.4 (c (n "tiny-fn") (v "0.1.4") (h "1rb86yri2l03fsfx82hbbrs1qvwmqmnrlj9p8sd5vjxlkhlwza1k") (f (quote (("example"))))))

(define-public crate-tiny-fn-0.1.5 (c (n "tiny-fn") (v "0.1.5") (h "1fm89zv2aq8ws6g6h0k1w0kja1a675c1wb692mdcc5lrw0rjqyxg") (f (quote (("example"))))))

(define-public crate-tiny-fn-0.1.6 (c (n "tiny-fn") (v "0.1.6") (h "0hi9xszmg21d2nwa83r36xwvia2p2k10j58lhai7g0ycx3ak34r1") (f (quote (("example"))))))

