(define-module (crates-io ti ny tinyurl-rs) #:use-module (crates-io))

(define-public crate-tinyurl-rs-0.1.0 (c (n "tinyurl-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1sh7sp6ygf76kw8a29szwf6zcsw54vj6z34saqf2npqz4i335lj1")))

(define-public crate-tinyurl-rs-0.1.1 (c (n "tinyurl-rs") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "032kwiinzkzgskzyj6yzsfcgm5aza85pnjk26wq1kam2kdxnjylp")))

