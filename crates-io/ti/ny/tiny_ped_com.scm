(define-module (crates-io ti ny tiny_ped_com) #:use-module (crates-io))

(define-public crate-tiny_ped_com-0.1.0 (c (n "tiny_ped_com") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "094rjpg4xrlxk2hmhyx17i6d27g96kkfzaffmp6ysfn233z7vhv3")))

