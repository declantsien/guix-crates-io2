(define-module (crates-io ti ny tiny_future) #:use-module (crates-io))

(define-public crate-tiny_future-0.3.0 (c (n "tiny_future") (v "0.3.0") (d (list (d (n "etrace") (r "^0.2.8") (d #t) (k 0)))) (h "1h69w3y0n58x558x91b02bglfzlw6xc32q8lf6caax6b7s6skj6y")))

(define-public crate-tiny_future-0.3.1 (c (n "tiny_future") (v "0.3.1") (d (list (d (n "etrace") (r "^1.0.0") (d #t) (k 0)))) (h "0nig1il73mk3bwyqnprlfci9xw7ylh1pgy5kfphcs7wfwwhbxnrv")))

(define-public crate-tiny_future-0.3.2 (c (n "tiny_future") (v "0.3.2") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0zyyn0rjdhrncx6vf7xrss3c2qm8s9nszkvznyh853img1cvdn3x")))

(define-public crate-tiny_future-0.4.0 (c (n "tiny_future") (v "0.4.0") (h "1z6v5d3glyar23w6zr6q5sag85xmk58sszhmrpwgyyan6mgc2046")))

(define-public crate-tiny_future-0.5.0 (c (n "tiny_future") (v "0.5.0") (h "01kwl1jxcql63yk6z4wj9nrxn6sbdj911jy6mkhcdnan1mg5ciaf") (f (quote (("default"))))))

(define-public crate-tiny_future-0.5.1 (c (n "tiny_future") (v "0.5.1") (h "1qn16xajvq535lls3c5kydq282calx9gqslfr7fk0rf7gp4a5lyq") (f (quote (("default"))))))

