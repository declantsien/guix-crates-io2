(define-module (crates-io ti ny tinybuf) #:use-module (crates-io))

(define-public crate-tinybuf-0.1.0 (c (n "tinybuf") (v "0.1.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1j7sx1wb78vi3q7rpsfilahig4hrdqh926yh2mlzwbd3h3a0qm4c")))

(define-public crate-tinybuf-0.1.1 (c (n "tinybuf") (v "0.1.1") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1ip7n2v89jhkwmfwrc9mfxgxn4k7hdhvpk270xxzm6ff4bjrb74m")))

(define-public crate-tinybuf-0.2.0 (c (n "tinybuf") (v "0.2.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1kls7z9zr8h8jl14lcwlh4fwhdw2wllwgkvhd6ldk9ip0zmkkfgy") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinybuf-0.2.1 (c (n "tinybuf") (v "0.2.1") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0b0a57h4qfdwsgq8hw9w62h13bzxrkcg9m8kglmys57in9z1c5y2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinybuf-0.2.2 (c (n "tinybuf") (v "0.2.2") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "03r2ia0shjkkbz0i5ns0k9952gsnkk9zkinjznhv52s01y76f764") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinybuf-0.2.3 (c (n "tinybuf") (v "0.2.3") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1vf6z3hxjlf84bfwbn7pyi27z57w6q0qvawlci709h9xmmlrsk14") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinybuf-0.3.0 (c (n "tinybuf") (v "0.3.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1a5c49injnqg7dhsrlwfp0a7hi3fbvsja6pcmlyq62sjfgh03kin") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

