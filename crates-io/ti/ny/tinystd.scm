(define-module (crates-io ti ny tinystd) #:use-module (crates-io))

(define-public crate-tinystd-0.1.0 (c (n "tinystd") (v "0.1.0") (h "02g6c636wvvsyhqzw6g307vy1y0xh151gp5d8xmag1y0kljgwzyn")))

(define-public crate-tinystd-0.2.0 (c (n "tinystd") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1qbvd6jrrd35k9n64i7lgw20x83fh2lzsw4xs6y2av1ngw3ixcxs")))

