(define-module (crates-io ti ny tinyhttp-codegen) #:use-module (crates-io))

(define-public crate-tinyhttp-codegen-0.1.0 (c (n "tinyhttp-codegen") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0w07nns1wy0pg2h3w5wml2qfm9vw9hm887hbp9grzdslb4q09cl8") (y #t)))

(define-public crate-tinyhttp-codegen-0.1.1 (c (n "tinyhttp-codegen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1qs1w87jvsf51vcbqj96f6nx95n252j22i1c8b3ml6ikk8h9gq54") (y #t)))

(define-public crate-tinyhttp-codegen-0.1.2 (c (n "tinyhttp-codegen") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0yng2qibx6pl5v66v6kidjx0bmpqjqlxi16w1l8qmxn27w6b7w7z")))

(define-public crate-tinyhttp-codegen-0.2.0 (c (n "tinyhttp-codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.2.0") (d #t) (k 0)))) (h "1sahjd6fy40jvm8h69chp91m48r94synmln6bl8ggkkpwlyjhr3s")))

(define-public crate-tinyhttp-codegen-0.2.4 (c (n "tinyhttp-codegen") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.2.0") (d #t) (k 0)))) (h "1ypv3g2yldaaydrd70wb85j9whhgq38d9fshnbrlhv1avbxc9b48")))

(define-public crate-tinyhttp-codegen-0.2.5 (c (n "tinyhttp-codegen") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.2.5") (k 0)))) (h "0pzpf83qcqqs46zb8n28dh7ziza9grxrj4rzw7hmlwcm4ximf1kq")))

(define-public crate-tinyhttp-codegen-0.3.0 (c (n "tinyhttp-codegen") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.0") (d #t) (k 0)))) (h "094sy9v5dibrhfhqi4fhh0jpbnxl5q3qsnkgxwl02rlar6x7v4az")))

(define-public crate-tinyhttp-codegen-0.3.2 (c (n "tinyhttp-codegen") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.2") (d #t) (k 0)))) (h "1llrzajy0a72pwk0406092w1pr66prv6s663zniqrw1n3y6fmgmv")))

(define-public crate-tinyhttp-codegen-0.3.4 (c (n "tinyhttp-codegen") (v "0.3.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.4") (d #t) (k 0)))) (h "1z49xz3a3w4ha3k3xnk59jlxpk3gaa094b8zy97aa3mydb0yq3dk")))

(define-public crate-tinyhttp-codegen-0.3.5 (c (n "tinyhttp-codegen") (v "0.3.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.5") (d #t) (k 0)))) (h "03l9zgmjp4wlykz0iq64ki9jlli3sxllq9czfszvjbv7l6m20vw5")))

(define-public crate-tinyhttp-codegen-0.3.6 (c (n "tinyhttp-codegen") (v "0.3.6") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.6") (d #t) (k 0)))) (h "1gdf5djzjdbxd3x2g6mpjd0f5zih0p1rqmnc444ifaj1bndj32fa")))

(define-public crate-tinyhttp-codegen-0.3.7 (c (n "tinyhttp-codegen") (v "0.3.7") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.7") (d #t) (k 0)))) (h "1zy0x0a2g3lahbnr5a6bp37x2y177i13s3yqf2grs352b6zxi2gv")))

(define-public crate-tinyhttp-codegen-0.3.8 (c (n "tinyhttp-codegen") (v "0.3.8") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.3.8") (d #t) (k 0)))) (h "05kdwgky2zng09m6g4cl30l4kh8m9xlkblxffz7d64qk0sl0f8sd")))

(define-public crate-tinyhttp-codegen-0.4.0-rc1 (c (n "tinyhttp-codegen") (v "0.4.0-rc1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.4.0-rc1") (d #t) (k 0)))) (h "04vg605q6yma1fbil8v3s4715w9iy3jxrl13ziig1plii75as2ck")))

(define-public crate-tinyhttp-codegen-0.4.0-rc2 (c (n "tinyhttp-codegen") (v "0.4.0-rc2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.4.0-rc2") (d #t) (k 0)))) (h "1i31a19bkxamsqhpi75v8djzprwvcvj2b43hkcw71fsiisgc070h")))

(define-public crate-tinyhttp-codegen-0.4.0-rc3 (c (n "tinyhttp-codegen") (v "0.4.0-rc3") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.4.0-rc3") (k 0)))) (h "14rwj28yfl1d1pzx8v6fmhs8ffh5m3rgzh8fw2s67sy02hpsdzb2")))

(define-public crate-tinyhttp-codegen-0.4.0-rc4 (c (n "tinyhttp-codegen") (v "0.4.0-rc4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.4.0-rc4") (k 0)))) (h "0xiv99z083w3hpsjjk1xr4b5j62cpz07zpww37syvy6ifda8ywa3")))

(define-public crate-tinyhttp-codegen-0.4.0-rc5 (c (n "tinyhttp-codegen") (v "0.4.0-rc5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tinyhttp-internal") (r "^0.4.0-rc5") (k 0)))) (h "03jk020kyg3h5qvkqiqxw7nqrshmvv3slrmzssp71kvbnyq16b7v")))

