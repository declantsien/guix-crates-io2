(define-module (crates-io ti ny tinyring) #:use-module (crates-io))

(define-public crate-tinyring-0.1.0 (c (n "tinyring") (v "0.1.0") (h "1dpvkgxp35mcjv84ijjn6gj5japqrnj2mnpjgvv6vaxyfv6maffs") (y #t)))

(define-public crate-tinyring-0.2.0 (c (n "tinyring") (v "0.2.0") (h "1qv6gcbdhwk8mrfanjbkg3cyl13gk95xjys9sq000ig1xmysp9wp") (y #t)))

(define-public crate-tinyring-0.3.0 (c (n "tinyring") (v "0.3.0") (h "1ll7fw1fawkyqp4qsbjjndi1mfq3kmyy6a61xm253j0f3hg1azk3") (y #t)))

(define-public crate-tinyring-0.4.0 (c (n "tinyring") (v "0.4.0") (d (list (d (n "do_syscall") (r "^1.0.1") (d #t) (k 0)))) (h "04h2cach2wf30n8hf3dl4lyh6f9iqbxvk96qnb8c8xd6wkh04dhg") (y #t)))

(define-public crate-tinyring-1.0.0 (c (n "tinyring") (v "1.0.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "rawring") (r "^1") (d #t) (k 0)))) (h "1av4h1p64lrxyz301izzswvnkdhbka3aiqs780r4bbwp5p1a2w2k") (y #t)))

