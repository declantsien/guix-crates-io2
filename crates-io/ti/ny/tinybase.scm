(define-module (crates-io ti ny tinybase) #:use-module (crates-io))

(define-public crate-tinybase-0.1.0 (c (n "tinybase") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1hb8slpzfhrvl5qkw273k3by4m4p2kpbpfgq96lvaski3wmnnhi5")))

(define-public crate-tinybase-0.1.1 (c (n "tinybase") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0r8lnghj308wh2ycr2y3hl58ig4b5g7p6m1wqy1rg560y88q9pzy")))

(define-public crate-tinybase-0.1.2 (c (n "tinybase") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.1") (d #t) (k 2)))) (h "0kvqqhk9dg0h75n3q1mycvfnnzl1nwjzk34b16yxjqim49x2m2k5") (f (quote (("derive" "tinybase-derive") ("default"))))))

(define-public crate-tinybase-0.1.4 (c (n "tinybase") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.4") (d #t) (k 2)))) (h "0k6s95f2himi7mi2avj4lhkw4awjmvmdh0jymqsrdprcy87syjv1") (f (quote (("derive" "tinybase-derive") ("default")))) (y #t)))

(define-public crate-tinybase-0.1.5 (c (n "tinybase") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tinybase-derive") (r "^0.1.5") (d #t) (k 2)))) (h "0fcn2d9gzbjzhazzs2acb877v9627mg3p9rs335spzixmk1ncdr7") (f (quote (("derive" "tinybase-derive") ("default"))))))

