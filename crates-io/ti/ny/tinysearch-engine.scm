(define-module (crates-io ti ny tinysearch-engine) #:use-module (crates-io))

(define-public crate-tinysearch-engine-0.1.0 (c (n "tinysearch-engine") (v "0.1.0") (h "0pnwf5jk8z8j005dxglmgj23ai7wz60qygl8y9667alabg5sjgnw")))

(define-public crate-tinysearch-engine-0.1.1 (c (n "tinysearch-engine") (v "0.1.1") (h "0rfpliaq2f3hmjd7xhn48xg85igs561w2ng1z98y4a6hhx8bpqs2")))

(define-public crate-tinysearch-engine-0.1.2 (c (n "tinysearch-engine") (v "0.1.2") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "1lvdm7y3i98ghpgsarmqm9yc05nn0dxyajqh4mbf1ic3kxd3zqia")))

(define-public crate-tinysearch-engine-0.1.3 (c (n "tinysearch-engine") (v "0.1.3") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "18im8n7yyaixxvrphsv49ijbvajp14cspgjnbs8c685dxvrj91lx")))

(define-public crate-tinysearch-engine-0.1.4 (c (n "tinysearch-engine") (v "0.1.4") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "0b0p2c466sn5gpwrbqqyhavrzybf6lpz57arc6a932bz5144665n")))

(define-public crate-tinysearch-engine-0.2.0 (c (n "tinysearch-engine") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "14kl9z2w15y2vhs3k7np2avccysjlz6llpyrwjs8nfiig07a0zds")))

(define-public crate-tinysearch-engine-0.2.1 (c (n "tinysearch-engine") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.3") (d #t) (k 0)))) (h "1cf4k8ig9g3632bfdz9j9vfnysnz9qgxsmn5848ch122xq52dkiz")))

(define-public crate-tinysearch-engine-0.2.2 (c (n "tinysearch-engine") (v "0.2.2") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "tinysearch-cuckoofilter") (r "^0.4.0") (d #t) (k 0)) (d (n "tinysearch-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.4") (d #t) (k 0)))) (h "1mmnd3vwvi33s0kqppwlbn5067qd5m566ncdzfkbybhv0rd90sqr")))

