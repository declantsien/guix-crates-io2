(define-module (crates-io ti ny tiny-game-loop) #:use-module (crates-io))

(define-public crate-tiny-game-loop-0.1.0 (c (n "tiny-game-loop") (v "0.1.0") (h "1qlyn84lr14m0f5c4m1fri8s1nlrk8m8vy8d9sf7562yyhr7wkj6")))

(define-public crate-tiny-game-loop-0.2.0 (c (n "tiny-game-loop") (v "0.2.0") (h "1fija1304k01d3zr99z45k251ygysffhxcpid4l16a43333y9k98")))

