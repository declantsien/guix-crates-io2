(define-module (crates-io ti ny tinydraw) #:use-module (crates-io))

(define-public crate-tinydraw-0.1.0 (c (n "tinydraw") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "0y0wm3vjrj60gvkvjc3h7f9c5ijr1h12qh7z2iq5sgjjbh12c22h")))

(define-public crate-tinydraw-0.1.1 (c (n "tinydraw") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)))) (h "09kfarcdqxdcr8wyiviknb3mkf7agr03y7wm51bzwjmw6j08zwhv")))

