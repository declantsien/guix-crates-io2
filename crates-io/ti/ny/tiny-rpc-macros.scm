(define-module (crates-io ti ny tiny-rpc-macros) #:use-module (crates-io))

(define-public crate-tiny-rpc-macros-0.1.0 (c (n "tiny-rpc-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1wv0yp5g0n4z5lscgq3qkjn0i42ifpa9ah3xy839hqnb1sad6jfh")))

(define-public crate-tiny-rpc-macros-0.1.1 (c (n "tiny-rpc-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "16r8jv9bzgmingbq7ya4x14yy2l42v0w88gx5kpqwvcs0qvjadky")))

(define-public crate-tiny-rpc-macros-0.2.0 (c (n "tiny-rpc-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0jhy20nr7p5a58h1wwpis1p42slpvnfxcq3f7nzcihjmvffwnbdr")))

(define-public crate-tiny-rpc-macros-0.3.0 (c (n "tiny-rpc-macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1d7v9h070cg4l2pj8krzjm2h22f4xq3d2j5wllh72s2w555ndmbz")))

(define-public crate-tiny-rpc-macros-0.3.2 (c (n "tiny-rpc-macros") (v "0.3.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0xk05qwia555dxinq72gsc0z0hbmaawfcgbb7k8zdy9my8k13fcg")))

