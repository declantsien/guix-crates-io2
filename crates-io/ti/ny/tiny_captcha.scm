(define-module (crates-io ti ny tiny_captcha) #:use-module (crates-io))

(define-public crate-tiny_captcha-0.1.1 (c (n "tiny_captcha") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 0)))) (h "055ncp5dmwi1cbsfh6a2x7gxqh0qqjk0axa4gmynij9k4j7152fj")))

(define-public crate-tiny_captcha-0.1.2 (c (n "tiny_captcha") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 0)))) (h "0hzj890z1jfzqaywjh1xnnj02iiwsqy8ys49djlgwbwja4jr35fh")))

(define-public crate-tiny_captcha-0.1.3 (c (n "tiny_captcha") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 0)))) (h "0hmy7wpm1wr8xywhm0q09hyfgi423w728sm14lq8fflgl8ckn5jr")))

(define-public crate-tiny_captcha-0.1.4 (c (n "tiny_captcha") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 0)))) (h "12d26384q2wz16fzq1nva6sx3jyv9599aci7rkbxnw7nssld2nx1")))

