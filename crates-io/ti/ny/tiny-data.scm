(define-module (crates-io ti ny tiny-data) #:use-module (crates-io))

(define-public crate-tiny-data-0.1.0 (c (n "tiny-data") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lsfc899fnsyafvd6zi6gx9radh3imq781b62ddws19fg3cggzz4")))

