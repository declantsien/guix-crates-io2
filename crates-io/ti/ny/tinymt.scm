(define-module (crates-io ti ny tinymt) #:use-module (crates-io))

(define-public crate-tinymt-1.0.0 (c (n "tinymt") (v "1.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "06i7s8piqpv295fdnm8p3rl49r91g21rjz73w0928nkf06ycm1pm") (y #t)))

(define-public crate-tinymt-1.0.1 (c (n "tinymt") (v "1.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1rmmbqjf7q52965f04d3gxxvvd116a8rkb15r1qc9sj423dv3lfl")))

(define-public crate-tinymt-1.0.2 (c (n "tinymt") (v "1.0.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0plazfka1vwg0bxd40ddmk7q2s9ssj6rr57c0pl2wijfgwdd15v7")))

(define-public crate-tinymt-1.0.3 (c (n "tinymt") (v "1.0.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1y0ryk05rfy2yvw02vxy0262mr429k7x4rs666vsmvbnp6ssgncw")))

(define-public crate-tinymt-1.0.4 (c (n "tinymt") (v "1.0.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "09fvlwsdl6v4mh6zs9nj4ygf0hq00z9mr1axvxr4b917ygrp3dqh")))

(define-public crate-tinymt-1.0.6 (c (n "tinymt") (v "1.0.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1dphnh1fcvsd1qgh1wy1m0hsfrxgmwrcsncwfygmk9hbysm6xzji")))

(define-public crate-tinymt-1.0.7 (c (n "tinymt") (v "1.0.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1dm062c3mlpa8wwb9vxnb1rba11xkk00ivxxki8wg429z6b395cv")))

(define-public crate-tinymt-1.0.8 (c (n "tinymt") (v "1.0.8") (d (list (d (n "pprof") (r "^0.11") (f (quote ("flamegraph"))) (d #t) (t "cfg(not(windows))") (k 2)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 2)))) (h "11lhxwp1l6a39k5zxha0jwbm10prpijrknaxr0d843pm0vlx5q8p")))

(define-public crate-tinymt-1.0.9 (c (n "tinymt") (v "1.0.9") (d (list (d (n "pprof") (r "^0.11") (f (quote ("flamegraph"))) (d #t) (t "cfg(not(any(target_family = \"windows\", target_family = \"wasm\")))") (k 2)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (k 2)))) (h "01l6l9vwj8ww20yqr4dd7zg6jrcyq9ci70cz0h8x6l1w9k9zjhln")))

