(define-module (crates-io ti ny tinyid) #:use-module (crates-io))

(define-public crate-tinyid-1.0.0 (c (n "tinyid") (v "1.0.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r09qy5zw95bvwlgkr1lid53a8kp1yhbf9afcvfz89jsrncdyj7x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinyid-1.0.1 (c (n "tinyid") (v "1.0.1") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04zlraxi5fz7samm90nmx0vna5hxd09nbky6zalyn81z7yay8plh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinyid-1.0.2 (c (n "tinyid") (v "1.0.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sxmwsshqh9ncdir08k8wbjpbdkh7jjj839q9xr3gpzbw8cpbqqb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tinyid-1.0.3 (c (n "tinyid") (v "1.0.3") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1swfrxidqmppraxl5i5gslss2zsylmndgqr9clzsvz73qkdc1qlc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

