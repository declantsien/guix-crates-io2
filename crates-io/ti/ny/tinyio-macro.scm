(define-module (crates-io ti ny tinyio-macro) #:use-module (crates-io))

(define-public crate-tinyio-macro-0.1.0 (c (n "tinyio-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1ci3nfdh8zgl06cwda4na41lc8d0fp0a05819lpnrwy1pf2nramm")))

