(define-module (crates-io ti ny tinyio-core) #:use-module (crates-io))

(define-public crate-tinyio-core-0.1.0 (c (n "tinyio-core") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m151md6zp3dkrlvlqjb22md1ng456jp7skqbhfykg0yr1rghh92")))

