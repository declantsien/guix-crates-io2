(define-module (crates-io ti ny tinydancer-program-library) #:use-module (crates-io))

(define-public crate-tinydancer-program-library-0.1.0 (c (n "tinydancer-program-library") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "blake3") (r "=1.3.1") (d #t) (k 0)))) (h "1mviclnkc0fqmv793nwifm7ry6wwh0hqsxac15az6w617bpznd10") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

