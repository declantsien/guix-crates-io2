(define-module (crates-io ti ny tiny-ordered-float) #:use-module (crates-io))

(define-public crate-tiny-ordered-float-0.3.0 (c (n "tiny-ordered-float") (v "0.3.0") (h "0qhzvzxyvd2fl5rk2m4ixqy7cpycb1fsdb9b9hydr7xwd5db980k")))

(define-public crate-tiny-ordered-float-0.4.0 (c (n "tiny-ordered-float") (v "0.4.0") (h "0xb528ghk04jsq1pcwgayg6hhimlflbncayqqb52xd8vp0hgwhrs")))

