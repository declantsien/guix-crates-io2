(define-module (crates-io ti ny tinyget) #:use-module (crates-io))

(define-public crate-tinyget-1.0.0 (c (n "tinyget") (v "1.0.0") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny_http") (r "^0.7") (d #t) (k 2)))) (h "1in466s1kzxgs7j07dc9sii5w7fm7s1v84cn8nsvm9d5a06ymjpr") (f (quote (("https" "native-tls"))))))

(define-public crate-tinyget-1.0.1 (c (n "tinyget") (v "1.0.1") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 2)))) (h "11sffpih8ahpymrfv2kzpvp558xj0mk773bx6a377fah1rz8n3dv") (f (quote (("https" "native-tls"))))))

(define-public crate-tinyget-1.0.2 (c (n "tinyget") (v "1.0.2") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 2)))) (h "0s40k9gi0nq8c7dy0fvgx287i0x301pf5a1wwqz9zjdlvlrp90m9") (f (quote (("https" "native-tls"))))))

