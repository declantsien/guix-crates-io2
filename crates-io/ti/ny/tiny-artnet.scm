(define-module (crates-io ti ny tiny-artnet) #:use-module (crates-io))

(define-public crate-tiny-artnet-0.1.0 (c (n "tiny-artnet") (v "0.1.0") (d (list (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 2)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (f (quote ("alloc"))) (k 0)) (d (n "tiny-artnet-bytes-no-atomic") (r "^1.2.1") (k 0)))) (h "1ndr5fr18v3xq8yivp9q29fcrpkijgq8ljsi8hz8jzg5qwg62y1b")))

(define-public crate-tiny-artnet-0.1.1 (c (n "tiny-artnet") (v "0.1.1") (d (list (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 2)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (f (quote ("alloc"))) (k 0)) (d (n "tiny-artnet-bytes-no-atomic") (r "^1.2.2") (k 0)))) (h "1ahvrkb7zmr35nm3bk06ipj41035b9d638ijn0zm5lnxq84ghf7z")))

(define-public crate-tiny-artnet-0.1.2 (c (n "tiny-artnet") (v "0.1.2") (d (list (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 2)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (f (quote ("alloc"))) (k 0)) (d (n "tiny-artnet-bytes-no-atomic") (r "^1.2.2") (k 0)))) (h "1ijq7m08hjp90600xn6dd7wdzf34nr4krzas945dvwwx1dn010w6")))

(define-public crate-tiny-artnet-0.1.3 (c (n "tiny-artnet") (v "0.1.3") (d (list (d (n "local-ip-address") (r "^0.4.8") (d #t) (k 2)) (d (n "mac_address") (r "^1.1.4") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (f (quote ("alloc"))) (k 0)) (d (n "tiny-artnet-bytes-no-atomic") (r "^1.2.2") (k 0)))) (h "0k45mvs1kglzz6zdwqzdbknmx68mpbjvymxvm6kqm9y8qgpafna0")))

