(define-module (crates-io ti ny tiny-timer) #:use-module (crates-io))

(define-public crate-tiny-timer-0.1.0 (c (n "tiny-timer") (v "0.1.0") (d (list (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0f37081rk0p3ivwjxmj9djylrjv0m76dqsakc9y5c7a3p3cfr5gb")))

(define-public crate-tiny-timer-0.1.1 (c (n "tiny-timer") (v "0.1.1") (d (list (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1j6av4khyhi6kdiyf0g4gq3zs0r2cfqwwg0jrxssjdph2b358ngw")))

