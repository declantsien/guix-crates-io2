(define-module (crates-io ti ny tinyinst) #:use-module (crates-io))

(define-public crate-tinyinst-0.1.0 (c (n "tinyinst") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1vdvzq2xjbqfw3cbivr72p0msyws1ndq1axd581xk9x9vqy8dpd6")))

