(define-module (crates-io ti ny tinychip) #:use-module (crates-io))

(define-public crate-tinychip-0.1.0 (c (n "tinychip") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0mrlhg75q5fpvqzdaxp9q6rfk9nkk7zjn3599c9ijralhn97jfyx")))

(define-public crate-tinychip-0.1.1 (c (n "tinychip") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0ixqinblf48hy31i27fnlcv1wmygh18bw0wlsg3h0bx8vl7czwhq")))

