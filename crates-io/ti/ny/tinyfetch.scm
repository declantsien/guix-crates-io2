(define-module (crates-io ti ny tinyfetch) #:use-module (crates-io))

(define-public crate-tinyfetch-0.1.0 (c (n "tinyfetch") (v "0.1.0") (d (list (d (n "mockito") (r "^0.26.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "000f0sn4n01jplxfkasr8grl293iy66w8wxi9ba6dwdsvxh0mhmc")))

