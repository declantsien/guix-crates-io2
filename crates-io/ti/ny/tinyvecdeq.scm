(define-module (crates-io ti ny tinyvecdeq) #:use-module (crates-io))

(define-public crate-tinyvecdeq-0.1.0 (c (n "tinyvecdeq") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "tinyvec") (r "^1.6") (d #t) (k 0)))) (h "1a8wc3i2g7108jnr2sj4x1hifbsi3xmi5garkx4raffhr38yyksy") (f (quote (("std" "tinyvec/std") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "tinyvec/serde"))))))

(define-public crate-tinyvecdeq-0.1.1 (c (n "tinyvecdeq") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1y2002xk658my0cy7y0xcx7z8b3643465v0lf1xl6gklv0cxp3l9") (f (quote (("std" "alloc" "either/use_std") ("default") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56")))

(define-public crate-tinyvecdeq-0.1.2 (c (n "tinyvecdeq") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "04di7w6kmb3fik8vpp4549vw9d4hj2yqy3sm632wrdzpa5hp4l8q") (f (quote (("std" "alloc" "either/use_std") ("default") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56")))

(define-public crate-tinyvecdeq-0.1.3 (c (n "tinyvecdeq") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0jbkawqh1prrvj652w2s0mv6y63abnj2f1csh00382qg5gfq58jx") (f (quote (("std" "alloc" "either/use_std") ("default") ("alloc")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56")))

