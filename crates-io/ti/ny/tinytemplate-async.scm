(define-module (crates-io ti ny tinytemplate-async) #:use-module (crates-io))

(define-public crate-tinytemplate-async-1.0.0 (c (n "tinytemplate-async") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qhdfbp9wk2r1v1x9h91karm1ifj0srhfs6j3k3whdzv7g3v1zg2")))

(define-public crate-tinytemplate-async-1.0.1 (c (n "tinytemplate-async") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r818i9yld46jp10b7wv6b2sq5mvqml2na6x6qivkbvh706s2sc3")))

(define-public crate-tinytemplate-async-1.1.1 (c (n "tinytemplate-async") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wp12zs18svcw25lrn76p0bkx4wax404hsfv7zy5hfxqkk2rlyzb")))

(define-public crate-tinytemplate-async-1.1.2 (c (n "tinytemplate-async") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "136kaazn7vv2zmh2ibmi5f3r8l6hsaiwiqnaiqrwilg54n8n45pc")))

