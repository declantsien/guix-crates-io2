(define-module (crates-io ti qu tique) #:use-module (crates-io))

(define-public crate-tique-0.1.0 (c (n "tique") (v "0.1.0") (d (list (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.11") (d #t) (k 0)))) (h "0k6y8viqw7qfbikgrjvkcvnx570i0m2vv888a4y2cmi1r0rqavv8") (f (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.1.1 (c (n "tique") (v "0.1.1") (d (list (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.11") (d #t) (k 0)))) (h "0sf6gw9cz0y2zmiyhnv9hx0kipjij5jjd2clg0vkhpnvbmirqbwq") (f (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.2.0 (c (n "tique") (v "0.2.0") (d (list (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.11") (d #t) (k 0)))) (h "1nvhpy9hhr9rlmvpi9z7ik1n6gnizvc4wlbb1cbkpz3ysn0llf5l") (f (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.3.0 (c (n "tique") (v "0.3.0") (d (list (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "tantivy") (r "^0.12") (d #t) (k 0)))) (h "07d0hxb1rq1b9xn8322sd7mrsid6jhkdj1m6machrv0if68k6ri1") (f (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.4.0 (c (n "tique") (v "0.4.0") (d (list (d (n "nom") (r "^5") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "tantivy") (r "^0.12") (d #t) (k 0)))) (h "11gyav6pznwwpwwd7bsjvs4npnmfwp5zj0zrxvx83xnrc14k0vw7") (f (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.5.0 (c (n "tique") (v "0.5.0") (d (list (d (n "nom") (r "^6") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.14") (d #t) (k 0)))) (h "1yijph1ra7iidw5ynxxwiqw3dl3bw3k33mr3fybpzrc2h3dgc5i0") (f (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.6.0 (c (n "tique") (v "0.6.0") (d (list (d (n "nom") (r "^6") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.15") (d #t) (k 0)))) (h "126ah4rd67a8830g3cs0gw153ys7hl5asjff8kwkw4cllsvy0vrd") (f (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.7.0 (c (n "tique") (v "0.7.0") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.16") (d #t) (k 0)))) (h "0lm0267ikdi827hr506ycm9mbqhl1gxli5256f78s9dqlhwnydj3") (f (quote (("queryparser" "nom") ("default"))))))

