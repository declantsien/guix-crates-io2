(define-module (crates-io ti wi tiwi) #:use-module (crates-io))

(define-public crate-tiwi-0.1.0 (c (n "tiwi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)))) (h "1cgqjli4yh227f5jls78shkcmszqj6k0fl1nxy222ykzl0zm2gap")))

