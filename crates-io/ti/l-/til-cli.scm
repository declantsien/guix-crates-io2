(define-module (crates-io ti l- til-cli) #:use-module (crates-io))

(define-public crate-til-cli-0.1.0 (c (n "til-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)))) (h "14ksllpb8i0cr4wnh2d2s2fkn3yr7knrcqphp1r3m8wl5z8sajkn")))

(define-public crate-til-cli-0.1.1 (c (n "til-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)))) (h "1vga2v0l4w7hnr4j6sk8qirrxsx7x9j5n622wcvp2hd4d31z5258")))

