(define-module (crates-io ti mg timg) #:use-module (crates-io))

(define-public crate-timg-1.0.0 (c (n "timg") (v "1.0.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "0.24.*") (d #t) (k 0)) (d (n "imageproc") (r "0.23.*") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1hf874221f241v2nvwhq2skddx01dwczapi0sk34625whiap7rv0")))

(define-public crate-timg-1.0.1 (c (n "timg") (v "1.0.1") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "0.24.*") (d #t) (k 0)) (d (n "imageproc") (r "0.23.*") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1jzh5j80y18b0l34i0m8ac6hc02dljr9i76372gwg883x64yrzyv")))

(define-public crate-timg-1.1.0 (c (n "timg") (v "1.1.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "0.24.*") (d #t) (k 0)) (d (n "imageproc") (r "0.23.*") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0npvsar0n19sgkgaplgwlwgbjcxjyvc9sw9cy3kjigpzl3z9sbqb")))

(define-public crate-timg-1.2.0 (c (n "timg") (v "1.2.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "0.24.*") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0l3w3y1p8x4xgxab3pgp56l0xrsgfq4ixc00gn747qb69g1nciay")))

(define-public crate-timg-2.0.0 (c (n "timg") (v "2.0.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "raw_tty") (r "^0.1.0") (d #t) (k 0)) (d (n "term_lattice") (r "^0.4.1") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0k3ld2rlwdkab9b3s7bnrh95yi5z8337fy8y39j29w3m6qbnyf0s")))

(define-public crate-timg-2.0.1 (c (n "timg") (v "2.0.1") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "raw_tty") (r "^0.1.0") (d #t) (k 0)) (d (n "term_lattice") (r "^0.4.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "10hd061hpxma8gd9wnbyc6lzwqsvicy7m4vlaxkrdf6ndf05ck5n")))

(define-public crate-timg-2.1.0 (c (n "timg") (v "2.1.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "raw_tty") (r "^0.1.0") (d #t) (k 0)) (d (n "term_lattice") (r "^0.4.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0j7q0vnawyl2915s82kavam6szy9ssbzvfz6a3rpngjvvp5lz8c6")))

