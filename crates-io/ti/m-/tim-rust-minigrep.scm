(define-module (crates-io ti m- tim-rust-minigrep) #:use-module (crates-io))

(define-public crate-tim-rust-minigrep-0.1.0 (c (n "tim-rust-minigrep") (v "0.1.0") (h "05xjfk2vdwmjfavalvckpx3sxwwm9arvzxj2ra6r2ks3acd3r3bq")))

(define-public crate-tim-rust-minigrep-0.1.1 (c (n "tim-rust-minigrep") (v "0.1.1") (h "0jzvrkvybfby353aws1xd3hhmna6zjwissn62x9zkqcfp1imli3p")))

