(define-module (crates-io ti xm tixml2svd) #:use-module (crates-io))

(define-public crate-tixml2svd-0.1.0 (c (n "tixml2svd") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "19bsx7rbd5wv7dll8dqg8mifxj88iiwcj2zjga20vkjaanm28zl8")))

(define-public crate-tixml2svd-0.1.1 (c (n "tixml2svd") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1c5im6fz5jn22zz1pqnfcphwqiqyy240qk4k7gqs5k9l2hlfv498")))

(define-public crate-tixml2svd-0.1.2 (c (n "tixml2svd") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0z90pf994l9ba383gc3bipb5rkdrg4qrzwjbc92h13766pdr13z5")))

(define-public crate-tixml2svd-0.1.3 (c (n "tixml2svd") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1333vv51ywm56z4yx2z6w187dmrcrlfa8khiz0479kbwbz7g1k89")))

(define-public crate-tixml2svd-0.1.4 (c (n "tixml2svd") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "unicode-bom") (r "^1.1.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "19gvsxa2aljif233i8a882ggslsviisag2w9gw4y9za1z6clqmcs")))

