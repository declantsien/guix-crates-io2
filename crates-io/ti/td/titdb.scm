(define-module (crates-io ti td titdb) #:use-module (crates-io))

(define-public crate-titdb-0.1.0 (c (n "titdb") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "18wh77pdr5sm19fbdq0xwygvfqypb005qsj4dpbs3bkvd0k6a4q2") (f (quote (("default")))) (s 2) (e (quote (("rand" "dep:rand"))))))

