(define-module (crates-io ti jm tijme-calendar) #:use-module (crates-io))

(define-public crate-tijme-calendar-0.1.0 (c (n "tijme-calendar") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0iwyqhb82nk8ik6s64pja1v6d127spdwrsfr8lzwls4gw3rh2jpq")))

(define-public crate-tijme-calendar-0.1.1 (c (n "tijme-calendar") (v "0.1.1") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1k9ir600h5mym50m2za9ypbv5pw3nv145vnq694s2iq7zl6rchhb")))

