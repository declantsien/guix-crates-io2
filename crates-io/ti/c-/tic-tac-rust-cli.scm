(define-module (crates-io ti c- tic-tac-rust-cli) #:use-module (crates-io))

(define-public crate-tic-tac-rust-cli-0.1.0 (c (n "tic-tac-rust-cli") (v "0.1.0") (d (list (d (n "tic-tac-rust") (r "^0.1.3") (d #t) (k 0)) (d (n "update-notifier") (r "^0.1.6") (d #t) (k 0)))) (h "1w3xbk8nhi65ravy733gjc8p7hj94g6faxmx9z7wajvp1511s1sq")))

(define-public crate-tic-tac-rust-cli-0.1.1 (c (n "tic-tac-rust-cli") (v "0.1.1") (d (list (d (n "tic-tac-rust") (r "^0.1.3") (d #t) (k 0)) (d (n "update-notifier") (r "^0.1.6") (d #t) (k 0)))) (h "0i6d421kmx1ycrz1f0b3rpkqjhm97jbdffzsg4spgzs6bkiy52y8")))

(define-public crate-tic-tac-rust-cli-0.1.2 (c (n "tic-tac-rust-cli") (v "0.1.2") (d (list (d (n "tic-tac-rust") (r "^0.1.3") (d #t) (k 0)) (d (n "update-notifier") (r "^0.1") (d #t) (k 0)))) (h "0bl4cxx8690xzy9dpv9sakf5f2lhci1bplpl276zgmp29795ni65")))

(define-public crate-tic-tac-rust-cli-0.1.3 (c (n "tic-tac-rust-cli") (v "0.1.3") (d (list (d (n "tic-tac-rust") (r "^0.1.3") (d #t) (k 0)) (d (n "update-notifier") (r "^0.1") (d #t) (k 0)))) (h "155yn732hl81kmz3s7zc33j9vlbikw8aadzvn36v60b0nfczd7wv")))

