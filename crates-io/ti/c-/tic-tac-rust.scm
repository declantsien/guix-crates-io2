(define-module (crates-io ti c- tic-tac-rust) #:use-module (crates-io))

(define-public crate-tic-tac-rust-0.1.3 (c (n "tic-tac-rust") (v "0.1.3") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "15nq79vywxiykfxs1gxq9ynsmn7hr9rjyb121qpk9cl4vjlrv5zd") (f (quote (("default" "console_error_panic_hook"))))))

