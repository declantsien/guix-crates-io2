(define-module (crates-io ti c- tic-tac-terminal) #:use-module (crates-io))

(define-public crate-tic-tac-terminal-1.0.0 (c (n "tic-tac-terminal") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "116psgrwzfjzxzpxxfphkw342b8mi6ih8n1s5jr0k4813dx6anyf")))

(define-public crate-tic-tac-terminal-1.0.1 (c (n "tic-tac-terminal") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "166bzz0iqn8ixiyvwvdqnk7m0nx53m758xb90pgjh6nmm32689sz")))

(define-public crate-tic-tac-terminal-1.0.2 (c (n "tic-tac-terminal") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kci6415l7crc1z8ls13x40xhvz67qbvpjc27zmg8kkwpq0d6ymp")))

