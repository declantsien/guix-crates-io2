(define-module (crates-io ti mf timfmt) #:use-module (crates-io))

(define-public crate-timfmt-0.2.0 (c (n "timfmt") (v "0.2.0") (d (list (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.6") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1q6sz04s0x9wp6plsccrn3ikxyzf1a8lql1njfcna957mvwp3i3k")))

