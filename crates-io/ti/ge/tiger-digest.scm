(define-module (crates-io ti ge tiger-digest) #:use-module (crates-io))

(define-public crate-tiger-digest-0.1.0 (c (n "tiger-digest") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.5.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1.1") (d #t) (k 2)))) (h "0hb0whl2f6cirp7npg39yjlb69wb20znad3z5bz5avqz4gizimps")))

(define-public crate-tiger-digest-0.1.1 (c (n "tiger-digest") (v "0.1.1") (d (list (d (n "block-buffer") (r "^0.5.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1.1") (d #t) (k 2)))) (h "0ibm4mjw20xlv9j84c35psi2zgjbk1y0fmfwwcf2xfxrnj8pw1k8")))

