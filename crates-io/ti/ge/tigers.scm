(define-module (crates-io ti ge tigers) #:use-module (crates-io))

(define-public crate-tigers-0.1.0 (c (n "tigers") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "160himxx282bj6g7vkrya9l5xqs6nwq59srrp45fi3bz6dfxyw6n")))

(define-public crate-tigers-0.1.1 (c (n "tigers") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "195351z92pfjf2ynym2mc64z3wvlfz3bsyx61hb2m6w0jmg59mrv")))

(define-public crate-tigers-0.1.2 (c (n "tigers") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0ag5c5dbn27jzz0gj1fx42djnrdbxxlw4pcvj4hji9ilxp77s7fb")))

(define-public crate-tigers-0.1.3 (c (n "tigers") (v "0.1.3") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0m0x95gv785bvxlqmkygph7rgdlilfkysysldbq8icsy20sc2cqh")))

(define-public crate-tigers-0.1.4 (c (n "tigers") (v "0.1.4") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1c4ylxj2a1585smyzbc8zaqqwsbkaxic7b7lhx2419hfqq8smpfj")))

(define-public crate-tigers-0.1.5 (c (n "tigers") (v "0.1.5") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "091l6n05rksdljshym2v63rj5p5mswjxd71zpv438dfpv4qsdkx1")))

