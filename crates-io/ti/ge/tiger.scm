(define-module (crates-io ti ge tiger) #:use-module (crates-io))

(define-public crate-tiger-0.0.0 (c (n "tiger") (v "0.0.0") (h "1xicc7166ffq6s9fbzn2zcdzkrmpm3avh0rfldq5vqqkj5a5y0s9") (y #t)))

(define-public crate-tiger-0.1.0 (c (n "tiger") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "01bhc7h8kxc5kjqx9sqrb3g8h4f9av6hpxzyihjq7pprphf56gj4") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-tiger-0.2.0 (c (n "tiger") (v "0.2.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0crzywriw90m0k47zsyplrvfv0gpp0wb07ly95frphyjgs1lqivn") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-tiger-0.2.1 (c (n "tiger") (v "0.2.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "1v8myygr98kryk5rjywwfjdqyfl7r5ll7cxydlw08fyp9b7bp6jp") (f (quote (("std" "digest/std") ("default" "std"))))))

