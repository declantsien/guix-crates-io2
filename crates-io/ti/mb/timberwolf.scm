(define-module (crates-io ti mb timberwolf) #:use-module (crates-io))

(define-public crate-timberwolf-0.2.0 (c (n "timberwolf") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "04vw5i5iq0h6ia02kd9nqnifh6bpr4gnayh7qk40d2b0fz9lz0ji")))

(define-public crate-timberwolf-0.3.0 (c (n "timberwolf") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0p6z0lig9vkgiaayxc9m8q2zw0h9ay3n1zxarwkp5f26pydk9i5q")))

(define-public crate-timberwolf-0.3.1 (c (n "timberwolf") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "101frj4d3xd1xq710c9zrnz4qj2fvfmapx84x71x9rknmzxbbx9d")))

(define-public crate-timberwolf-0.3.2 (c (n "timberwolf") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0qnzcgjcdnm5x95vpp8j5w25ng264sg75a6w8j4k7z7r16h82ckf")))

(define-public crate-timberwolf-0.4.0 (c (n "timberwolf") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "winit") (r "^0.19.1") (d #t) (k 0)))) (h "12k3zm502cbfrimmrdm5ndfqxapkqp2yiqv9flyxpfrlrf809c8p")))

(define-public crate-timberwolf-0.5.0 (c (n "timberwolf") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "rendy") (r "^0.5.1") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "03pf1qaizyafphdgn12n7yldhvp12pqmjfd4mzbx6yvjfx0dga0f")))

