(define-module (crates-io ti mb timbre) #:use-module (crates-io))

(define-public crate-timbre-0.1.0 (c (n "timbre") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.34.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "186q5kx48lk708qj5p4wwiyqd6v59nh7azcwwz83nf43p49vavbd")))

(define-public crate-timbre-0.1.1 (c (n "timbre") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.34.0") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "1hygq3q9ghx0ch2i0wdmffg4nvw1k5fqqragzjdxm30vl3fcv2qf") (f (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.1.2 (c (n "timbre") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.34.0") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)))) (h "1j3ccqm306wgb93kyvv985vninwck9px0ahbzfa8fji5xv5n16l0") (f (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.2.0 (c (n "timbre") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.0") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-chrome") (r "^0.2.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 2)))) (h "1qb2i7qsaicpm05dwjfpanfrzgza6mnjc84gd5k9b8qrf4gi4fdk") (f (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.3.0 (c (n "timbre") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.0") (d #t) (k 0)) (d (n "sdl2-sys") (r "=0.34.2") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.19") (d #t) (k 0)) (d (n "tracing-chrome") (r "^0.2.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2.11") (d #t) (k 2)))) (h "1iksdb0rxinzi96z3b7ca500pmnl327lk0ycg3lah0kxiirs136i") (f (quote (("default" "sdl2/bundled"))))))

