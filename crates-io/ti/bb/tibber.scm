(define-module (crates-io ti bb tibber) #:use-module (crates-io))

(define-public crate-tibber-0.1.0 (c (n "tibber") (v "0.1.0") (d (list (d (n "graphql_client") (r "^0.10.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ah341izh73zh1d0dmaqhs71cj1h3zi5mdxcv8idsg7ma671m5b0")))

(define-public crate-tibber-0.2.0 (c (n "tibber") (v "0.2.0") (d (list (d (n "graphql_client") (r "^0.10.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ldx94lfhpdf01arsh2j61p97b088i68mf6l855nifkf4nr3j0gl")))

(define-public crate-tibber-0.3.0 (c (n "tibber") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ri8ryr929gg4rj0z58nqym1vvdv8yykahlynk0sqmnfpf090j7a")))

(define-public crate-tibber-0.4.0 (c (n "tibber") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jyii4z7rmbcqkyxjyxdz1k31b8dkmn9mp5ix3avvwpqprdg7c9p")))

(define-public crate-tibber-0.5.0 (c (n "tibber") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "graphql_client") (r "^0.10.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nibkdr80v902nagmqbryc0rshvjhcx7i3x5c4zksh9clh7mz1cb")))

