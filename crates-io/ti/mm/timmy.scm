(define-module (crates-io ti mm timmy) #:use-module (crates-io))

(define-public crate-timmy-0.1.0 (c (n "timmy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7.3") (f (quote ("chrono"))) (d #t) (k 0)))) (h "11i4a216b96i2g31pmcv77sdlvfpz8s7qzhacqgz27a6a8gnia04")))

(define-public crate-timmy-0.2.0 (c (n "timmy") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7.3") (f (quote ("chrono"))) (d #t) (k 0)))) (h "1fjyacq6xyihqix2gr63zdkqfkwh74ji4cmmwk5bgcjszawxqgvf")))

(define-public crate-timmy-0.3.0 (c (n "timmy") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.7.3") (f (quote ("chrono"))) (d #t) (k 0)))) (h "10g05ic7s018m2cvmmznk04f93dxyrdkb4l5cpy20xid9ng7c6rq")))

