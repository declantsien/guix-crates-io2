(define-module (crates-io ti s6 tis620) #:use-module (crates-io))

(define-public crate-tis620-0.0.1 (c (n "tis620") (v "0.0.1") (h "1kllnn7ap1rc65gansasn1sinlgzqvwwrazdy6xwkir3ginq5m9y")))

(define-public crate-tis620-0.0.2 (c (n "tis620") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "17calnvqcb1yj3b3vkr3n3fwwcm8mj4znvzd8j23fbfliw6qmb2f")))

(define-public crate-tis620-0.0.3 (c (n "tis620") (v "0.0.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0iqsdq0av1kwabfa8qbqmk1gg7fh4x356w82nvvgkx7358563dm4")))

(define-public crate-tis620-0.0.4 (c (n "tis620") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1xlhz078aqbblvx9mn20rkcr44ppqbkaqmn2hpzy98qnjgz7r70k")))

(define-public crate-tis620-0.0.5 (c (n "tis620") (v "0.0.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0fqjn7pafv6vw6pfp4plqrcla965rrxyfrmlbzb1id758dlrmhmi")))

(define-public crate-tis620-0.0.6 (c (n "tis620") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1gf5nz7vn0dgfa9i7hykdmqhdma9xp7arm09nw8rhj3kh9074155")))

(define-public crate-tis620-0.0.7 (c (n "tis620") (v "0.0.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1akq3d35pn2z64xyr70lp7mw43w0inglgg2lmql4wz9qnjgh1c1i")))

(define-public crate-tis620-0.1.0 (c (n "tis620") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0kiz4v7higbm4qb4fbjwpw7yn9yyfwcv92527y62ky5iggh321ly")))

(define-public crate-tis620-0.1.1 (c (n "tis620") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "15r9xmdwq47mqz15nr4s6v7rk4g6kq5m6vkzqlzbr0p7ijvhmbwd")))

