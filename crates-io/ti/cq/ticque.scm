(define-module (crates-io ti cq ticque) #:use-module (crates-io))

(define-public crate-ticque-0.1.0 (c (n "ticque") (v "0.1.0") (d (list (d (n "concurrent-queue") (r "^2.5") (d #t) (k 0)) (d (n "onetime") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ya19l2w7851lnd1ydwcw55fql53lkzblznzkkxjnvh3hdpcm8kp")))

(define-public crate-ticque-0.1.1 (c (n "ticque") (v "0.1.1") (d (list (d (n "concurrent-queue") (r "^2.5") (d #t) (k 0)) (d (n "onetime") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gxn3gcia2n32wxdkh6jqic4l2h3am7q6j9sjx00rv9ffi1zg4n9")))

(define-public crate-ticque-0.1.2 (c (n "ticque") (v "0.1.2") (d (list (d (n "concurrent-queue") (r "^2.5") (d #t) (k 0)) (d (n "onetime") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12xjyh1fz2p1cka2f6xsvnc4zra2c60rjaq8jih1zznskyx6djsl")))

(define-public crate-ticque-0.1.3 (c (n "ticque") (v "0.1.3") (d (list (d (n "concurrent-queue") (r "^2.5") (d #t) (k 0)) (d (n "onetime") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10gzn5936rw6d9w1jhlzhxhrcvfngmghj7cxm0k6vqhhlmh58m10")))

