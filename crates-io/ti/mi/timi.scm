(define-module (crates-io ti mi timi) #:use-module (crates-io))

(define-public crate-timi-0.1.0 (c (n "timi") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "termion") (r "^1.0") (d #t) (k 0)))) (h "1bzkmjmpb64zsnjmg141ii4hy3w6cr5k84f6bs5l9035l9dcfhlp")))

(define-public crate-timi-0.2.0 (c (n "timi") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "12djjgzbwg9m9j04l2dfd835110l1w7ld2k3zddvidd0mjg3d1sa")))

(define-public crate-timi-0.2.1 (c (n "timi") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "09qv2axsl7hq4j0x63pwbw1y9j5540hs1lmk90q76xx0bhzz0wa4")))

