(define-module (crates-io ti mi timing-shield) #:use-module (crates-io))

(define-public crate-timing-shield-0.1.0 (c (n "timing-shield") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "1l3y24gh2can4k1s539b036i4vj7w33wwj1vrfljyr270a8abx2d")))

(define-public crate-timing-shield-0.1.1 (c (n "timing-shield") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "08gi60439ydmbyw39bhllxdliyxbam9xa56cddd90b5l1j65g4bf")))

(define-public crate-timing-shield-0.1.2 (c (n "timing-shield") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "0cgn0442yy50p2dfajwqyd3n4qzd1g0afnfr3kypazrdyd9kf0pn")))

(define-public crate-timing-shield-0.2.0 (c (n "timing-shield") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0hqrw56111r56gdjb4m2pwb8lj3zsg9qmrmk8ygl5nk3ks31p30j")))

(define-public crate-timing-shield-0.3.0 (c (n "timing-shield") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "06jz74h89q27kav6r48r2cg1sp85zw454k2rsn7jxjc0h5jsw7ny")))

