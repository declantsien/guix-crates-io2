(define-module (crates-io ti mi timing) #:use-module (crates-io))

(define-public crate-timing-0.1.0 (c (n "timing") (v "0.1.0") (h "050pcwd1a7iprhhkbhhr0ic7x1zv1bhk388jqiba38djw0yykxy4")))

(define-public crate-timing-0.1.1 (c (n "timing") (v "0.1.1") (h "1aqs9wpv292hgsfva77km6wbj5rh0w11ni944lipl1lfvflw5kmp")))

(define-public crate-timing-0.2.1 (c (n "timing") (v "0.2.1") (h "0fbgavahzrxkfwmnpblbzqp6ndwai2jj77mdckw3hvkijpdki7jx")))

(define-public crate-timing-0.2.3 (c (n "timing") (v "0.2.3") (h "1w2b0wwm9g0f01riz0zf8ywighih34lm5ma99qmhx5mj18w77y8f")))

