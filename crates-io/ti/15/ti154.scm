(define-module (crates-io ti #{15}# ti154) #:use-module (crates-io))

(define-public crate-ti154-0.1.0 (c (n "ti154") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0s35x3xqsdwmgr3qk45y65axc5lrkcw97gsvgqb6a53fz1zsnjn9")))

(define-public crate-ti154-0.1.1 (c (n "ti154") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11ssyxmdad0bdvrbcdz32b19gh8f9c7zjgam3lppywv89w4m68zi")))

(define-public crate-ti154-0.1.2 (c (n "ti154") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fqkbrjay6691vnhvxf2wbskih4z0zgf53hvlfsiw2j27b1zvzaj")))

(define-public crate-ti154-0.2.0 (c (n "ti154") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1b5zhr9plxjfzq9mrp8qj7463kni7qblynkl9f619y7ql7dar8q9")))

(define-public crate-ti154-0.2.1 (c (n "ti154") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0znfrw281n009qdnycm9jpa82hr5827jzqx062bg1c8lai4dzadw")))

(define-public crate-ti154-0.2.2 (c (n "ti154") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p5ljgj5j7halybwq8ay1i0kkwzknc403nssis5i7pzkpjp4yn2x")))

(define-public crate-ti154-0.3.0 (c (n "ti154") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vibfg2f7kmvrz7mg3yfn92ycy8pjd5y91ksn2079issxqgcqsdi")))

(define-public crate-ti154-0.3.1 (c (n "ti154") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1kxx8rxw6lxfvficzly5d84njbrznps4xjsc5knawasnr79yd4ml")))

(define-public crate-ti154-0.3.2 (c (n "ti154") (v "0.3.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1lgk4irrpn9lzmafkca1xizbjk73fr4g62kl0z4khzsj00c8fdas")))

(define-public crate-ti154-0.3.3 (c (n "ti154") (v "0.3.3") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cj15m2gfmc0iqg5h0i9r3nf2q9qbb7wcwya55nsw2x00bk4bbz9")))

(define-public crate-ti154-0.3.4 (c (n "ti154") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h3pxa2xxfsyrbawpcjz4bqmcp17xsf96h4f73frmac13hpbdlva")))

