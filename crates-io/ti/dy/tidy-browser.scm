(define-module (crates-io ti dy tidy-browser) #:use-module (crates-io))

(define-public crate-tidy-browser-0.1.0 (c (n "tidy-browser") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "decrypt-cookies") (r "^0.5") (d #t) (k 0)) (d (n "miette") (r "^7") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "macros" "tracing"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "02j9g71l3k5rz5b7ifn1dcihjr1a51yq3g2s7cx3as0m3vgvz4c2")))

