(define-module (crates-io ti dy tidy-sys) #:use-module (crates-io))

(define-public crate-tidy-sys-0.1.0 (c (n "tidy-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0sc3is1ircndidqbwv5rhwyqwd19v96gdmcpchgarvnxmx85disr")))

(define-public crate-tidy-sys-0.1.1 (c (n "tidy-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0k4hqf7b0c1palgbyb690dah12n523vk3r8pk7qrnmc1ninb2fpk")))

(define-public crate-tidy-sys-0.2.0 (c (n "tidy-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1697g1kyzzw12ij4pspwy1p64bkxfyyl1mq57vh2c1mfzd0fmfdd")))

(define-public crate-tidy-sys-0.3.0 (c (n "tidy-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1dkq3zyh7ph6jhcb0zj5rhgrrh2wfy7m882g9kd83pam48mj0jkh") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.3.1 (c (n "tidy-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1a5xwva53rvfhhl3i3k3vb796lwrdmy9pf2a25i4dyqxb886n3f2") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.4.0 (c (n "tidy-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "128v7z3829npqf4zgpwc6mbgvcd6xwgqzwbm54pvkkrxn37bszmd") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.4.1 (c (n "tidy-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0pmi7r4hza1w27gd71x58klcr60xjbzk718jlzby9dsxg0mgnw77") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.5.0 (c (n "tidy-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1cs2qwr5ff97x8wjxh8sllygx4vcll319j325fljizfkk2jddpb4") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.6.0 (c (n "tidy-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0dxh003g9irnxhlps49yxr6air537n4crrbzm6171alj3bx7kd3j") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.7.0 (c (n "tidy-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "07qhigqmdpiphpnm09wx227q6k2m4f2rl38d9phxfyrgfsgpgam4") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.7.1 (c (n "tidy-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0wyqicp6cwh7xgbhjiniqkzn69wqgn85fl7qh3i4m1la1f2rs225") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.8.0 (c (n "tidy-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0xnk7gamjsqaznrcdffayfhj7gwsjd3yvnn5f8m15s6wb5q5m5m2") (f (quote (("localization") ("default"))))))

(define-public crate-tidy-sys-0.8.1 (c (n "tidy-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1wxy83ddd1lvz9c511pbgi3kcvmlrld0fhw3cj460i3mzxf938v1") (f (quote (("localization") ("default"))))))

