(define-module (crates-io ti dy tidyvcf) #:use-module (crates-io))

(define-public crate-tidyvcf-0.1.0 (c (n "tidyvcf") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles-vcf") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0dhgis7dqajacg80dif2w86nvx6hx20ivajg21rzy9vlna0syl2s")))

(define-public crate-tidyvcf-0.1.1 (c (n "tidyvcf") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles-vcf") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0myvy0pc9gqmpqgn834p47pxgq2p0mi7ly0zaafmmyy61r0p71zk")))

(define-public crate-tidyvcf-0.2.0 (c (n "tidyvcf") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.8.0") (d #t) (k 0)) (d (n "noodles-vcf") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1m854rn1w48qik44kan6z5r3q9bjl9j3273ncv2cyr86vzjk6ddw")))

(define-public crate-tidyvcf-0.2.1 (c (n "tidyvcf") (v "0.2.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles-bgzf") (r "^0.8.0") (d #t) (k 0)) (d (n "noodles-vcf") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0r5i06psf4s44qwchxa61rscm6q9am42yv0z48jqpvbnciqp7002")))

(define-public crate-tidyvcf-0.2.2 (c (n "tidyvcf") (v "0.2.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles") (r "^0.21.0") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1i3zkgg96d2jka4ziqwb9sq9cgx4jj53vqwqwcr9vdm6d5w5hz5h")))

(define-public crate-tidyvcf-0.2.3 (c (n "tidyvcf") (v "0.2.3") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles") (r "^0.31.1") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "18n1mk4bx57mfs3kd47ic6bkjlqm999k1k1c1jqbvzmrnlsq3zrn")))

(define-public crate-tidyvcf-0.2.4 (c (n "tidyvcf") (v "0.2.4") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "noodles") (r "^0.31.1") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1ajcm7m0yw8zw8yrvqw5ykn01nm4zrjzgpvdd1i500kjx67rh3iq")))

(define-public crate-tidyvcf-0.2.5 (c (n "tidyvcf") (v "0.2.5") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "noodles") (r "^0.49.0") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "05bhr1grx4c34knyw7skzsn0wv34q9pzk4n9sx79c0n4p6lph9hr")))

(define-public crate-tidyvcf-0.3.0 (c (n "tidyvcf") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles") (r "^0.49.0") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0rxxihvb505mk713crgxpikb8kp5avvwbn95w4cfshis21gls900")))

(define-public crate-tidyvcf-0.4.0 (c (n "tidyvcf") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "noodles") (r "^0.49.0") (f (quote ("vcf" "bgzf"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0rc3w5j55rlc8rxgmfbic54afmxryqijv5ry8gnf31xsws58prmb")))

