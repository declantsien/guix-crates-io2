(define-module (crates-io ti dy tidy-builder) #:use-module (crates-io))

(define-public crate-tidy-builder-0.1.0 (c (n "tidy-builder") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lrva5wydkksfwjkgbal75zrfsh6bxwwgfsaaqmsy1y5rakyh8fk")))

