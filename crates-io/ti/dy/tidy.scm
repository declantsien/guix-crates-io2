(define-module (crates-io ti dy tidy) #:use-module (crates-io))

(define-public crate-tidy-1.0.0 (c (n "tidy") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "081nbmg6kgkrnrvqn6gqk10zdc4zai83i942m8474lyn65wl5k3c")))

(define-public crate-tidy-1.0.1 (c (n "tidy") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pqhd9pvj107k6lpwf2pq7mwq0r13zyk948gw5yfcabhzjk03a58")))

