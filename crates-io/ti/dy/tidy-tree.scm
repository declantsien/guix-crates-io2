(define-module (crates-io ti dy tidy-tree) #:use-module (crates-io))

(define-public crate-tidy-tree-0.1.0 (c (n "tidy-tree") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tinyset") (r "^0.4.10") (d #t) (k 0)))) (h "0ppwsyx95kfq70339n2ajmb56fw1farxm8hn1gih0nplikp9al1q")))

