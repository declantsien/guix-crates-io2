(define-module (crates-io ti me timeout-readwrite) #:use-module (crates-io))

(define-public crate-timeout-readwrite-0.1.0 (c (n "timeout-readwrite") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0ab6ccs0xjf5jdz861yv4gaqvd8gl0dkqpxrn3bynkzxpzdagdi0")))

(define-public crate-timeout-readwrite-0.2.0 (c (n "timeout-readwrite") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1dm12cjhkbdg8n57v70nl9j6l2858lk2clvq907dqbkzsqq6xbq4")))

(define-public crate-timeout-readwrite-0.2.1 (c (n "timeout-readwrite") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)))) (h "07xv3qdp2bi8mi0v6fsi1xni52mi0g4is6qgv6n8yvx7ldwp6x4c")))

(define-public crate-timeout-readwrite-0.3.0 (c (n "timeout-readwrite") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.16.0") (d #t) (k 0)))) (h "1p80vm3kwigmap2x626r898nlc7qbf46gcbz7r5xanpl5lzh0cpj")))

(define-public crate-timeout-readwrite-0.3.1 (c (n "timeout-readwrite") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1g7rd5frwm0b6m33bpj7m4idcq7dhbx121lqqz5i5hz1ccvfiy8m")))

(define-public crate-timeout-readwrite-0.3.2 (c (n "timeout-readwrite") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1klxc4cmcxxps6q8hmlzdihic803rbabndxcz3jn4dg8kqkvdsb1")))

(define-public crate-timeout-readwrite-0.3.3 (c (n "timeout-readwrite") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "nix") (r "^0.26.0") (f (quote ("poll"))) (k 0)))) (h "0glz6j1vha8v0vzl34nnyqgiaj34b2n50hlac49g3l6v1bf2sc9p")))

