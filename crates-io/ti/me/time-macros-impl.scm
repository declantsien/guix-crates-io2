(define-module (crates-io ti me time-macros-impl) #:use-module (crates-io))

(define-public crate-time-macros-impl-0.1.0 (c (n "time-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18cvhv877a46wrsj57l8v0x9rhgnhmhxw2crr5gmnmvzaghcz1z9")))

(define-public crate-time-macros-impl-0.1.1 (c (n "time-macros-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "standback") (r "^0.2") (k 0)) (d (n "syn") (r "^1") (f (quote ("proc-macro" "parsing" "printing" "derive"))) (k 0)))) (h "1ymqhvnvry3giiw45xvarlgagl8hnd6cz4alkz32fq5dvwgbxhz5")))

(define-public crate-time-macros-impl-0.1.2 (c (n "time-macros-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "standback") (r "^0.2") (k 0)) (d (n "syn") (r "^1.0.15") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0bs8xc3qbndk4nw6vwnmh5bwail6vwji4hd1aqzly6a33cd18g7x")))

