(define-module (crates-io ti me timeflaketiny-rs) #:use-module (crates-io))

(define-public crate-timeflaketiny-rs-0.2.0 (c (n "timeflaketiny-rs") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "timeflake-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "0n60sf4zi1hq9p47ljm5bcsmr1gnhyjd878yf30jwzqln8br69s7") (y #t)))

(define-public crate-timeflaketiny-rs-0.2.1 (c (n "timeflaketiny-rs") (v "0.2.1") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "timeflake-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "1p0sqgbpx4fgvz35qlzidpvbvgs1q3ilrfm8akgn9j19hhrvrlx0") (y #t)))

(define-public crate-timeflaketiny-rs-0.2.2 (c (n "timeflaketiny-rs") (v "0.2.2") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "timeflake-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "138x5r41p3b9ajnms18idg5lc2mgvnij26jrf6md57hjai0a7aw2") (y #t)))

(define-public crate-timeflaketiny-rs-0.2.3 (c (n "timeflaketiny-rs") (v "0.2.3") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "timeflake-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "159352syf5i3xc713mmvncmx4z8b0dpkkwaf1zhd99432m869jm1")))

