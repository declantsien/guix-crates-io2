(define-module (crates-io ti me timelib) #:use-module (crates-io))

(define-public crate-timelib-0.1.0 (c (n "timelib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1yqpgvv6bix6xw6dvb3nlw25af928zm5zlaknkzxw59hcbzpxlzq")))

(define-public crate-timelib-0.1.1 (c (n "timelib") (v "0.1.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0wqmhf797wiwjlkxsk5g0ii6frgil84h48y23j3ldxsfcb1hg5qz")))

(define-public crate-timelib-0.1.2 (c (n "timelib") (v "0.1.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1aia8n5allx7nnjgk80gh4j48075rkwlqz0mypz1h8jgnn93y37h")))

(define-public crate-timelib-0.2.0 (c (n "timelib") (v "0.2.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0p4cj3lcm17svm0bgrjjdm2w1ng74yp1hfg3zy6zjw8g5gq1x6db") (f (quote (("re2c"))))))

(define-public crate-timelib-0.2.1 (c (n "timelib") (v "0.2.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0z9rc7xkg4hraf6nrzgmvqcrdnj5n7xypw2wlp7bsk8mjj0l1wrc") (f (quote (("re2c"))))))

(define-public crate-timelib-0.3.0 (c (n "timelib") (v "0.3.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1cwzqfm9jnsdxm12p20msx9jgkglm54dqvf0hpq0ypmammav9gca") (f (quote (("re2c"))))))

(define-public crate-timelib-0.3.1 (c (n "timelib") (v "0.3.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18r81py50rb5ivq61vag11kzs4f5sxp1vnx61yswp3q39yvlnv2b") (f (quote (("re2c"))))))

(define-public crate-timelib-0.3.2 (c (n "timelib") (v "0.3.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10zr25i4dnbcrs62swmk0yv07lyijxf2gfgy5q7wq3y048l33x35") (f (quote (("re2c"))))))

(define-public crate-timelib-0.3.3 (c (n "timelib") (v "0.3.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1pwazah7asgs8h5wqf8aglplc0w6liz5isxxldp37clq7yj7cqwf") (f (quote (("re2c"))))))

(define-public crate-timelib-0.3.4 (c (n "timelib") (v "0.3.4") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1izx20mmz901dabasjr56v8h2sh01x4576spp0sfampj2fzvzzyx") (f (quote (("re2c"))))))

