(define-module (crates-io ti me timediff) #:use-module (crates-io))

(define-public crate-timediff-0.1.0 (c (n "timediff") (v "0.1.0") (h "1bvmbjgxrdr09wnmbbih4s0vnj4dxn5i4g415ag6wbb6r7w53wax") (y #t)))

(define-public crate-timediff-0.2.0 (c (n "timediff") (v "0.2.0") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "1jvhymbfh5azxp5hhi91x0bnx5imqn1x8v587mha2fydiyh3riv0")))

(define-public crate-timediff-0.2.1 (c (n "timediff") (v "0.2.1") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "0m2wavkjvgd1bqqgh0r9hgah90q4jh803svsyj860n0kbw6dvg09")))

(define-public crate-timediff-0.2.2 (c (n "timediff") (v "0.2.2") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "17734aniqmwhx2iplgl2hhv9b3njzgyqhfz062s4y331lmb348xx")))

(define-public crate-timediff-0.2.3 (c (n "timediff") (v "0.2.3") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "1kb24jry0m6gki4800gfcpy643gkfl1n8ac3px5q8wrvjkkrghrf")))

