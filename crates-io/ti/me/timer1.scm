(define-module (crates-io ti me timer1) #:use-module (crates-io))

(define-public crate-timer1-0.1.0 (c (n "timer1") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.7.5") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0a241xh5lygxmpilnhq73fpcia9azsw5lzc3afc286l4vlascg5z")))

