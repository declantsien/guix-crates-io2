(define-module (crates-io ti me timewarrior-rs) #:use-module (crates-io))

(define-public crate-timewarrior-rs-0.4.4 (c (n "timewarrior-rs") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "198yh8sy8lb8wp4dwwxbram3ayhsb6fc8qvdg7i7mqih6ii6zn7i")))

