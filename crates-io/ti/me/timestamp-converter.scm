(define-module (crates-io ti me timestamp-converter) #:use-module (crates-io))

(define-public crate-timestamp-converter-0.1.0 (c (n "timestamp-converter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)))) (h "1y4ys14ra3axyy1xvs6x92qjvlvd31cqbdy2fp3nh5855kzx4w4q")))

(define-public crate-timestamp-converter-0.1.1 (c (n "timestamp-converter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)))) (h "1806n3xf78jdxi1v1xm8ry0957644hp62mipr54066z76f75cd7h")))

(define-public crate-timestamp-converter-0.1.2 (c (n "timestamp-converter") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)))) (h "0fknk7j23h9pcrbf1kxm3p5rwhlfviz7acmbibdzg0c7n1gd2l9z")))

(define-public crate-timestamp-converter-0.1.3 (c (n "timestamp-converter") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)))) (h "0ybj9g0y0mv7zizsnwhh0swddrs5sqx59gi44jqfd7560jgdl757")))

