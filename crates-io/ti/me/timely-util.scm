(define-module (crates-io ti me timely-util) #:use-module (crates-io))

(define-public crate-timely-util-0.1.0 (c (n "timely-util") (v "0.1.0") (d (list (d (n "abomonation") (r "^0.7.3") (d #t) (k 0)) (d (n "abomonation_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "timely") (r "^0.12.0") (d #t) (k 0)))) (h "1g3xs8snjv4f05kf400fdvnb9mqk6bvvz65ic3lz71sf0cac9819")))

(define-public crate-timely-util-0.1.1 (c (n "timely-util") (v "0.1.1") (d (list (d (n "abomonation") (r "^0.7.3") (d #t) (k 0)) (d (n "abomonation_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "timely") (r "^0.12.0") (d #t) (k 0)))) (h "0ghl4zh5px0z1yq3afl24lr3az4bafp87hcqcximrl1f7rjk62wb")))

