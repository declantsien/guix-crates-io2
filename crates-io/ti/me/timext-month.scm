(define-module (crates-io ti me timext-month) #:use-module (crates-io))

(define-public crate-timext-month-0.3.0 (c (n "timext-month") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "056v4ng3wjvlirzh6sy41rk4w9ry9hvlgc6dd8wyk4dpmfx6nzmb") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

(define-public crate-timext-month-0.3.1 (c (n "timext-month") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0d2cpz8rl62iqkpfqhri5fypapld1mi8znfhbb3yx1qb21rkd8n1") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

(define-public crate-timext-month-0.3.2 (c (n "timext-month") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0akkgxvbz2l3sg6jdj6910yk6pnybngnj2lhvmrwqhffnx41mlwm") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

