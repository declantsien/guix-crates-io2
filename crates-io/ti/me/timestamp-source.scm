(define-module (crates-io ti me timestamp-source) #:use-module (crates-io))

(define-public crate-timestamp-source-0.1.0 (c (n "timestamp-source") (v "0.1.0") (h "08k5vwmmk74wyxk2ifs8v5vb2mymx2z6vlz3znxaf163kcii0fr8")))

(define-public crate-timestamp-source-0.1.1 (c (n "timestamp-source") (v "0.1.1") (h "1kblgk2g4dv3j4zhrvbjk0c379n34947j3rvsmxdf7fam1924yar")))

(define-public crate-timestamp-source-0.1.2 (c (n "timestamp-source") (v "0.1.2") (h "1d7jvhaqy22caz48i0j1yksh7ah7a5fgii1lwz8i1i8d71xdy4sm")))

(define-public crate-timestamp-source-0.1.3 (c (n "timestamp-source") (v "0.1.3") (d (list (d (n "spin") (r "^0.9.4") (d #t) (k 0)))) (h "1p52kkqwdpfhdn3dldd5smd08qd5j7b0im1xg4qrcxk0fmv2m9wd")))

