(define-module (crates-io ti me timeywimey) #:use-module (crates-io))

(define-public crate-timeywimey-0.1.0 (c (n "timeywimey") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "017lvd7sl27wm4zp36ibvs3h2mfah1dbsc81cvn64x3xpl59bkfh")))

