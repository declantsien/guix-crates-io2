(define-module (crates-io ti me time_sigil) #:use-module (crates-io))

(define-public crate-time_sigil-0.0.1 (c (n "time_sigil") (v "0.0.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-cron-scheduler") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1w3bnj22a91zmgj58npzxil8vriq3fn7jacxr5jmrwi5aq3z85hb")))

(define-public crate-time_sigil-0.0.2 (c (n "time_sigil") (v "0.0.2") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-cron-scheduler") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vvvn926p390vf71pvmnhwjxljw9rfnlglfcrfw9p6q142g2bsvd")))

