(define-module (crates-io ti me time_period) #:use-module (crates-io))

(define-public crate-time_period-0.1.0 (c (n "time_period") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "17jd3nvgkfb0lk6m0ri4572n65dcsbx5n99cmf4y6zlj2kal5ll4")))

(define-public crate-time_period-0.2.0 (c (n "time_period") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "0qj8l3f7759f0d1r5px0pdb6k5xsmjh4958lkn6hr3iwillngmnf")))

(define-public crate-time_period-1.0.0 (c (n "time_period") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "svg") (r "^0.13.0") (d #t) (k 2)))) (h "04hk9pbbih3fwjwafgv4c10gvgvwsyw6qnjnf5db9bcxdh62h79p")))

