(define-module (crates-io ti me timewarp) #:use-module (crates-io))

(define-public crate-timewarp-0.2.0 (c (n "timewarp") (v "0.2.0") (d (list (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)))) (h "0gr1figiar4lc23jfj6ngvdw3b4i8cf1374gszm6ap8j7ndm7agj")))

(define-public crate-timewarp-0.3.0 (c (n "timewarp") (v "0.3.0") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1s802vnw3hh2h9l4vmcjqrskvfjmzyxy64jz4fvhshclbbkw6h0b")))

(define-public crate-timewarp-0.3.1 (c (n "timewarp") (v "0.3.1") (d (list (d (n "pest") (r "^2.6.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6.0") (d #t) (k 0)))) (h "1i6mdqv8lh51wzhl1b4hpl69nccm2hjws41nx70l5kg9nqdpa8jz")))

(define-public crate-timewarp-0.3.2 (c (n "timewarp") (v "0.3.2") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1n9f4qp7za48yd0awbgs9njfb6hn00hdjc5dgf5bax9fqh7702id")))

(define-public crate-timewarp-0.4.0 (c (n "timewarp") (v "0.4.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "10v5af6sipwr7gd1bfsvfrvr168gdp8qqkv4wz9yq79ysknapqd9")))

