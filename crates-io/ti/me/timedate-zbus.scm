(define-module (crates-io ti me timedate-zbus) #:use-module (crates-io))

(define-public crate-timedate-zbus-0.1.0 (c (n "timedate-zbus") (v "0.1.0") (d (list (d (n "zbus") (r "^3.8.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "zbus") (r "^3.8.0") (f (quote ("tokio"))) (k 2)))) (h "1dw5q7phm54vi1b3ihgxnpxbz4528fc1ajmck5wy5ng1ib255430")))

