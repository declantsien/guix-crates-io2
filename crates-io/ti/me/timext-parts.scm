(define-module (crates-io ti me timext-parts) #:use-module (crates-io))

(define-public crate-timext-parts-0.3.0 (c (n "timext-parts") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1ssdjhzgbflh83rl3x06v88qz26b8rkv9qwpc5zs1w4b9c7fcvz9") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

(define-public crate-timext-parts-0.3.1 (c (n "timext-parts") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0hqaar4qkf0nif9nyava4wr8dgqbscn7m8rq099sa1mmy67j29gv") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

(define-public crate-timext-parts-0.3.2 (c (n "timext-parts") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1c9vl0s3bvhqpkkjxnm5h5fb00dbjn5glrxrrhh35kfy0465k88k") (y #t) (s 2) (e (quote (("serde" "dep:serde" "time/serde") ("rand" "dep:rand" "time/rand"))))))

