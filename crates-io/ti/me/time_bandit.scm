(define-module (crates-io ti me time_bandit) #:use-module (crates-io))

(define-public crate-time_bandit-0.1.0 (c (n "time_bandit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "1987d8cl7ibm48iyqwpfajml0x254q6pj1icawk48f9yzqfjj5b3")))

(define-public crate-time_bandit-0.1.11 (c (n "time_bandit") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "1h4p0dj0any82h2bazj2023mcxwrd0knyp1fkx08izhcs69g078z")))

