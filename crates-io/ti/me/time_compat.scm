(define-module (crates-io ti me time_compat) #:use-module (crates-io))

(define-public crate-time_compat-1.0.1 (c (n "time_compat") (v "1.0.1") (h "1q9h6047i4ddganjrvm2d5sssx119y0qkvpdqsppy2agpinyh4px")))

(define-public crate-time_compat-1.0.2 (c (n "time_compat") (v "1.0.2") (h "01v01fjaf4gjsplzs1i8sdkm8gbwaw5xym78m1vs3xl9hb8z2y35")))

