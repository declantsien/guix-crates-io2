(define-module (crates-io ti me timegraph-wasm) #:use-module (crates-io))

(define-public crate-timegraph-wasm-0.1.3 (c (n "timegraph-wasm") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0") (d #t) (k 0)) (d (n "timegraph-identity") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "139v8bzk9xjz1gd2692vc7hz6vm25rp2ks06vnx86cldy9d0zbc4")))

