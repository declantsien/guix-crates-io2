(define-module (crates-io ti me timer) #:use-module (crates-io))

(define-public crate-timer-0.1.0 (c (n "timer") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0p6f1a3mq0f8ga4m266jx446l0yf7chmas58qalv3dh9iash2x2v")))

(define-public crate-timer-0.1.1 (c (n "timer") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1p8r77lmichg36d9jms8yp705p161k8s5npdz9hbdi355nhfv2g3")))

(define-public crate-timer-0.1.2 (c (n "timer") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1dqqakr7mkbzs9kjggxj7nfdp3x16qzrlnq9sgbjvpvgsrpppvbq")))

(define-public crate-timer-0.1.3 (c (n "timer") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "0wfq07zm81zrr13dkj74qsclavznzjsiamcizig9mdr3g5z1crhf")))

(define-public crate-timer-0.1.4 (c (n "timer") (v "0.1.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "0jgjqm48lqk8zxq7y66vkwi6r08cdr8j1hr6cnzfmn23d0ijy30v")))

(define-public crate-timer-0.1.5 (c (n "timer") (v "0.1.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "023dqv5mk8pm02pm0nlm4g5nf4acn444i1i6smg42cda49kvsj9y")))

(define-public crate-timer-0.1.6 (c (n "timer") (v "0.1.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "1cs69725bznjk1fy3v61pwfyi5m42lj8vqqlwpwy4m80qjg2llm9")))

(define-public crate-timer-0.2.0 (c (n "timer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0srhqyp7fr91d1i43aqs7wc6yn1i3kdkh1pm05bicdw961v23m1i")))

