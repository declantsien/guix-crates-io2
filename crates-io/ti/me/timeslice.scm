(define-module (crates-io ti me timeslice) #:use-module (crates-io))

(define-public crate-timeslice-0.1.0 (c (n "timeslice") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "esp-idf-hal") (r "^0.42") (o #t) (d #t) (k 0)) (d (n "esp-idf-svc") (r "^0.47") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0vs1i3145cmms6rmhn3arqj1mj9f4avkw6hi7lgbai91yhgk2z4w") (f (quote (("meas") ("hal-dummy") ("default" "hal-dummy" "meas")))) (s 2) (e (quote (("hal-espidf" "dep:esp-idf-hal" "dep:esp-idf-svc"))))))

(define-public crate-timeslice-0.2.0 (c (n "timeslice") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "esp-idf-hal") (r "^0.42") (o #t) (d #t) (k 0)) (d (n "esp-idf-svc") (r "^0.47") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0kafdq0r0wpli4gdjs1gkjcyvfh6ici6d03c7dwf02qrz6ig763b") (f (quote (("meas") ("hal-dummy") ("default" "hal-dummy" "meas")))) (s 2) (e (quote (("hal-espidf" "dep:esp-idf-hal" "dep:esp-idf-svc"))))))

(define-public crate-timeslice-0.3.0 (c (n "timeslice") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "esp-idf-hal") (r "^0.43") (o #t) (d #t) (k 0)) (d (n "esp-idf-svc") (r "^0.48") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0w73m4lnnd9hwqarhk35w2gh9vada6sn8qvydcyjgq86i1qd8vpr") (f (quote (("meas") ("hal-dummy") ("default" "hal-dummy" "meas")))) (s 2) (e (quote (("hal-espidf" "dep:esp-idf-hal" "dep:esp-idf-svc"))))))

