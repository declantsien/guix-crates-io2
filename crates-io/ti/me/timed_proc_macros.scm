(define-module (crates-io ti me timed_proc_macros) #:use-module (crates-io))

(define-public crate-timed_proc_macros-0.1.0 (c (n "timed_proc_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "075qvcwyb64l705hmk6cg36r2lxj5fx64znljivzw8cyw7hrjkx6")))

(define-public crate-timed_proc_macros-0.1.1 (c (n "timed_proc_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "04vh35k1gd8w4q2d271mlbzf86jdinpcmw8r2jfxd9c0jd02sg67")))

(define-public crate-timed_proc_macros-0.2.0 (c (n "timed_proc_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0shnpz9m4r1j10wrnp22wn8mfcxj0xwlnfpvcb26h67cylgqshd2")))

