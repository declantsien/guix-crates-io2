(define-module (crates-io ti me timemachine) #:use-module (crates-io))

(define-public crate-timemachine-0.1.0 (c (n "timemachine") (v "0.1.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "12d6hi6kgpy8hzrxgycsy2z0xzfskrq2kzw5cdmi0b4axmn3ya42")))

(define-public crate-timemachine-0.2.0-alpha (c (n "timemachine") (v "0.2.0-alpha") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.4-alpha") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1bdjxbg4n0c5fcfqgj5dfmvyc4c2x6r0blw74xncj6rz5jc7q3ay")))

(define-public crate-timemachine-0.2.0 (c (n "timemachine") (v "0.2.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.4-alpha") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1glmhr75r65xzd5d6m4i7gapmsh1gv13hi1ysbpn0zyhnnjmxpbw")))

(define-public crate-timemachine-0.2.1 (c (n "timemachine") (v "0.2.1") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.4-alpha") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0npqi354i3r27dpdh62fhzz99541hc1ljlf29yfagcj6v6xjdcna")))

(define-public crate-timemachine-0.3.0 (c (n "timemachine") (v "0.3.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0i3xs763qiwn5xhxjksbm72q3qhk7hxr90792vmx4x7hdavi4mqi")))

(define-public crate-timemachine-0.3.1-alpha (c (n "timemachine") (v "0.3.1-alpha") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "08whgjmq0xi1fwfgx99n6635cd2mdgmjjysp5rnr2zym4wfnab7a")))

(define-public crate-timemachine-0.3.1 (c (n "timemachine") (v "0.3.1") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1gw94rx5jk3cpd3paal5fan0krrprrp8fhbkwy248n2cn3spj07c")))

(define-public crate-timemachine-0.3.2 (c (n "timemachine") (v "0.3.2") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 2)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "napchart") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0f7v49hzjgf3kbwxs4nrghsnn6csfcf22jynniv4p24sbvlx9dhw") (f (quote (("graphviz"))))))

