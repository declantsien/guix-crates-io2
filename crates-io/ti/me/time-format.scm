(define-module (crates-io ti me time-format) #:use-module (crates-io))

(define-public crate-time-format-1.0.0 (c (n "time-format") (v "1.0.0") (h "03z4w225qc44i93m9kvvr35fahm0vc54bjdq7x4w7zw89a1fv4dd")))

(define-public crate-time-format-1.0.1 (c (n "time-format") (v "1.0.1") (h "0rkwwmz6jrpz9h1vyjcplarhn068g6zn5r85hr18qsmqh62kpi8y")))

(define-public crate-time-format-1.0.2 (c (n "time-format") (v "1.0.2") (h "12am9b8ksbz63r2900nb9zb29xgmszkdidjxqga7jkra3n97bami")))

(define-public crate-time-format-1.1.0 (c (n "time-format") (v "1.1.0") (h "1vj5fmm74zbzip8qkj98d6na9h3fdcbhidbi443wwg4dda5gnfb4")))

(define-public crate-time-format-1.1.1 (c (n "time-format") (v "1.1.1") (h "0bia2fw4j1whkphsxrx41rpvpa1m01xmw3dkis98nyrmirczsa7s")))

(define-public crate-time-format-1.1.2 (c (n "time-format") (v "1.1.2") (h "15pi7665c69fsjxfxflx3c91z3aiqzgvdmrpw283lxyj98wyrfj2")))

