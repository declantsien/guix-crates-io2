(define-module (crates-io ti me timetable) #:use-module (crates-io))

(define-public crate-timetable-0.1.0 (c (n "timetable") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "geodate") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "transitfeed") (r "^0.3.0") (d #t) (k 0)) (d (n "zip") (r "^0.2.6") (d #t) (k 0)))) (h "1kxf2xg05si41l8qp2bymp389hxvqvls63d3cplqyqiy8k04wsb0")))

