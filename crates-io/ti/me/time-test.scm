(define-module (crates-io ti me time-test) #:use-module (crates-io))

(define-public crate-time-test-0.1.0 (c (n "time-test") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "14rm4ibqxgv1q5blji41ngsqr7cz8n26q113v9424hr38fkgy8ix")))

(define-public crate-time-test-0.2.0 (c (n "time-test") (v "0.2.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02zsnrpyqh157ybzvna4934bjilakx7lpzabgpysjw9zzk2pfa39")))

(define-public crate-time-test-0.2.1 (c (n "time-test") (v "0.2.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09jm54xzhiq7jq9ws01ilbjgvf3j9spnq36jdc0gi9n2hca1xgn5")))

(define-public crate-time-test-0.2.2 (c (n "time-test") (v "0.2.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0vaaxskalr3gjyb4wbc5m7hg01xxy5wdgzra4kf4f5kc58gh6g28")))

(define-public crate-time-test-0.2.3 (c (n "time-test") (v "0.2.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sz9g4gpaa5m8bj5fsi3azh5bldf4n7lczx2kassydfyq187h0m0")))

