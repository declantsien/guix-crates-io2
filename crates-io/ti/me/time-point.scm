(define-module (crates-io ti me time-point) #:use-module (crates-io))

(define-public crate-time-point-0.1.0 (c (n "time-point") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1zd9aswd6bm5pg1x3sl7rm3bjhss43hszy22yk920mb2606cfkbj")))

(define-public crate-time-point-0.1.1 (c (n "time-point") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "057v0d7jah6a18yhkl33whpw93vmby2fdyaf9gf6igkainamqlq6")))

