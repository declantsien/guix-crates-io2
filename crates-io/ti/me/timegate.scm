(define-module (crates-io ti me timegate) #:use-module (crates-io))

(define-public crate-timegate-0.1.0 (c (n "timegate") (v "0.1.0") (d (list (d (n "sled") (r "^0.31.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1a3sgbwchfj4pp88d02f216xzz2jrg6q3jpijc95d76aj0y0hvr4")))

