(define-module (crates-io ti me timeline) #:use-module (crates-io))

(define-public crate-timeline-0.1.0 (c (n "timeline") (v "0.1.0") (d (list (d (n "clap") (r "~2.2.1") (d #t) (k 0)) (d (n "encoding") (r "~0.2") (d #t) (k 0)) (d (n "gif") (r "~0.8") (d #t) (k 0)) (d (n "image") (r "~0.9") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "~0.7.2") (d #t) (k 1)) (d (n "serde_json") (r "~0.7") (d #t) (k 0)) (d (n "syntex") (r "~0.31") (d #t) (k 1)))) (h "1dw4455flv3mk708vq9dgw68jwr97jr0vqa6x3wg0zccx2mw22a9")))

