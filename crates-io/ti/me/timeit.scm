(define-module (crates-io ti me timeit) #:use-module (crates-io))

(define-public crate-timeit-0.1.0 (c (n "timeit") (v "0.1.0") (d (list (d (n "time") (r "^0.1.26") (d #t) (k 0)))) (h "1in53hq5x4z3svij8grh9kqg0n1q5dgan7vp51gd4x3h7s2x9zbc")))

(define-public crate-timeit-0.1.1 (c (n "timeit") (v "0.1.1") (d (list (d (n "time") (r "^0.1.26") (d #t) (k 0)))) (h "0zb1idmag0br0gcgxr4qz9ahlx4y0kl0zw50m58vkmbd8qg69cfx")))

(define-public crate-timeit-0.1.2 (c (n "timeit") (v "0.1.2") (d (list (d (n "time") (r "^0.1.26") (d #t) (k 0)))) (h "19dzxqk5zwbm64w129gyh9ykjw4kcidrdncxav5zmrzyxpsl4acb")))

