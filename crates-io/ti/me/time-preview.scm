(define-module (crates-io ti me time-preview) #:use-module (crates-io))

(define-public crate-time-preview-0.0.0 (c (n "time-preview") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.0") (f (quote ("std" "minwinbase" "minwindef" "ntdef" "profileapi" "sysinfoapi" "timezoneapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.0") (f (quote ("std" "processthreadsapi" "winbase"))) (d #t) (k 2)))) (h "1z9j3kli43kykkbhln84rz8837wp38mh9zz8f1650klm9ky3632s")))

