(define-module (crates-io ti me time_main) #:use-module (crates-io))

(define-public crate-time_main-0.1.0 (c (n "time_main") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0k5g2i29hyk08nn1s18mxfghz32i2h5km51af582n65zk0d3qqzn")))

(define-public crate-time_main-0.1.1 (c (n "time_main") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "01lhc1jmpghf6pgi28h9fxx8qn7y6i381xsakxyg35wvjijk1lxr")))

