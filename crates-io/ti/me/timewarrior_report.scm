(define-module (crates-io ti me timewarrior_report) #:use-module (crates-io))

(define-public crate-timewarrior_report-0.1.0 (c (n "timewarrior_report") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1h49hmlrysbf0ldrjgi53hk4i6ir6g0g02jwi4ngipavgvk0997q")))

