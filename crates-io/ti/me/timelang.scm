(define-module (crates-io ti me timelang) #:use-module (crates-io))

(define-public crate-timelang-0.1.0 (c (n "timelang") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1vlpnsfzgsmhjvck826hjyyc7vsc10qj49vsl3wc05cwks26xwhy")))

(define-public crate-timelang-0.1.1 (c (n "timelang") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0ll7zg76krjb925fxs7zcwya78ajfp8i5rbspz9564ahvaqjg1ak")))

(define-public crate-timelang-0.1.2 (c (n "timelang") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1xk3g1h83in0sdmnch08zr9hcvz6y77zkz78zj246ja4n3j3d49y")))

(define-public crate-timelang-0.1.3 (c (n "timelang") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "19rgw8xlnlvnbg71p43jw9f876yqqjxlgxm7k2rhi3h0qpidphis")))

