(define-module (crates-io ti me timed_cache) #:use-module (crates-io))

(define-public crate-timed_cache-0.1.0 (c (n "timed_cache") (v "0.1.0") (h "1pdx7874nd7j609552nmy98vc68b9y95g8j12xqgd0awzh3cwg9l")))

(define-public crate-timed_cache-0.1.1 (c (n "timed_cache") (v "0.1.1") (h "1lxxhcy5dfyimxhsahvqqd508ww1d6jynv96s2zfzab5qwji2rwg")))

