(define-module (crates-io ti me timer-utils) #:use-module (crates-io))

(define-public crate-timer-utils-0.0.1 (c (n "timer-utils") (v "0.0.1") (d (list (d (n "object-collection") (r "^0.0.1") (d #t) (k 0)))) (h "1bvlsadhzra65ilpqh6ahb5fn5yg00b24pv04jdrm39vcjfqm6yp")))

(define-public crate-timer-utils-0.0.2 (c (n "timer-utils") (v "0.0.2") (d (list (d (n "object-collection") (r "^0.0.2") (d #t) (k 0)))) (h "07487qx0br4rsi0zwq1p518pl1sybvwngss28mqga0snzrh78vwm")))

(define-public crate-timer-utils-0.0.3 (c (n "timer-utils") (v "0.0.3") (d (list (d (n "object-collection") (r "^0.0.3") (d #t) (k 0)))) (h "0jykfa4i8mj2pim2z3l3m99py7bmyj9mjq03dpa0lif88xr12hj9")))

(define-public crate-timer-utils-0.0.4 (c (n "timer-utils") (v "0.0.4") (d (list (d (n "object-collection") (r "^0.0.5") (d #t) (k 0)))) (h "0kn3bl49kn4y14smc8w5024ng25hadq72an8qcrpibg4cyg3qdkm")))

