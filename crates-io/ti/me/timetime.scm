(define-module (crates-io ti me timetime) #:use-module (crates-io))

(define-public crate-timetime-0.1.0 (c (n "timetime") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "1z5w2kd6qmbjyv85z918g598prilikhchdm4lqdm2vzwpp2wpyx9")))

