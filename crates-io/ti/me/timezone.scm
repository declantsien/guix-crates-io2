(define-module (crates-io ti me timezone) #:use-module (crates-io))

(define-public crate-timezone-0.0.0 (c (n "timezone") (v "0.0.0") (h "0slgx2gd0ymjpj1bb7mrbmqdc07wa47gc5g85f9x17lx4cllwh98")))

(define-public crate-timezone-1.0.0 (c (n "timezone") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0004rh3b91lrzf19bk0lm181p6qn4y9fkk4a52dsa9q5gzs3bhn3")))

