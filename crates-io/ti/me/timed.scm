(define-module (crates-io ti me timed) #:use-module (crates-io))

(define-public crate-timed-0.1.0 (c (n "timed") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)))) (h "04bsjz53vn680k52v985s3y0d3r9hvak3by630kxgx8xm73l7dj7")))

(define-public crate-timed-0.1.1 (c (n "timed") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)))) (h "0baady9hsn0xvmyawz6kjqqak32567czlppgnvkmjgzrxalmdlnp")))

(define-public crate-timed-0.1.2 (c (n "timed") (v "0.1.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full"))) (d #t) (k 0)))) (h "0s6646b8z4r2y8qm5216vsamdg1g85y6n5ha92ilzy4dzcmbfkdf")))

(define-public crate-timed-0.1.3 (c (n "timed") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "timed_proc_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1999w78qnsbii1aa28511mxy4yp27y9i4d1vzjgh3pwahhm1azlv")))

(define-public crate-timed-0.1.4 (c (n "timed") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "timed_proc_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0ac5hqgvs4f90j41yr1ikgvh08wwc07l9g45njs0vaczl448kv7s")))

(define-public crate-timed-0.1.5 (c (n "timed") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "timed_proc_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1qwqdajls9cqkb4v11wsdv1abfakah3yd5z4psg03gc0dxd6mi2d")))

(define-public crate-timed-0.2.0 (c (n "timed") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)) (d (n "timed_proc_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0511q2y3s6k7p1pf4i4g1lbmbkc98mgrwz7yn81bl1hgpav60nni")))

(define-public crate-timed-0.2.1 (c (n "timed") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)) (d (n "timed_proc_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0gbqbbm7bh6ai38acf43phr1x6zrgs3fary4iy172g76v1s61428")))

