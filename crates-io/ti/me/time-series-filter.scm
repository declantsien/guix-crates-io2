(define-module (crates-io ti me time-series-filter) #:use-module (crates-io))

(define-public crate-time-series-filter-0.1.0 (c (n "time-series-filter") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "14irvdqqph46c7rcq14z3a87iav6lmaslhsxb5c0bk56qgi4vw6k")))

(define-public crate-time-series-filter-0.1.2 (c (n "time-series-filter") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (f (quote ("libm"))) (k 0)))) (h "1qa8l8hz6cj2dfpn12irplpqm798an50i848i19yi5zvk2qz0qmm")))

