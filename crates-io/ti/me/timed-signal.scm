(define-module (crates-io ti me timed-signal) #:use-module (crates-io))

(define-public crate-timed-signal-0.1.0 (c (n "timed-signal") (v "0.1.0") (h "0zf0sg7qwsj1i0az4vdyzpbpwlzzbsg8f36p016hqykr80mkclai") (r "1.62")))

(define-public crate-timed-signal-0.2.0 (c (n "timed-signal") (v "0.2.0") (h "0mp3iprspi139zz4w672wfv2zi4bv2nv1kx3cs6bp4b76lf48pfx") (r "1.62")))

