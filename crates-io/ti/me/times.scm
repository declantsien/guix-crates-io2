(define-module (crates-io ti me times) #:use-module (crates-io))

(define-public crate-times-0.1.0 (c (n "times") (v "0.1.0") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 0)))) (h "1zvv6ixcp737mm1rz0b399pwmvsc8ixzwypqqsmfb1c3hnxzj8f4")))

(define-public crate-times-0.1.1 (c (n "times") (v "0.1.1") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^0") (d #t) (k 0)))) (h "0aq3djzfn4k337k2yz2j2gni38v8cihhz2v70wn38s3zp7kfdhpb")))

(define-public crate-times-0.1.2 (c (n "times") (v "0.1.2") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "09gw8vc51qfh0dw2wicv60zjmlfmjp14fjdxns3szjhvcjkw7j85")))

(define-public crate-times-0.1.3 (c (n "times") (v "0.1.3") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "0ra2cfp9hmbqp865cbmfflp47vhhv50x5h3ik7zrm1h371fx14g9")))

(define-public crate-times-0.1.4 (c (n "times") (v "0.1.4") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "0nwyv49d4vy4mj6mpdlzswyl7157yqa3ah5lm8ajnck4nkivkkxv")))

(define-public crate-times-0.1.5 (c (n "times") (v "0.1.5") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "07wah7hs57ds3x16inld3gcswm7k5sa9650gz3jddcyja49zf1a4")))

(define-public crate-times-0.1.6 (c (n "times") (v "0.1.6") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "12cqd4i9nn7mzjb0djw8kvf7v4iqzwkfhpl6rjx0wq8x66yiq6vc")))

(define-public crate-times-1.0.0 (c (n "times") (v "1.0.0") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "0wzdp5hk97iziirc137x3c40l4wsy0146q2c29dgm5jsclw98mvh")))

(define-public crate-times-1.0.1 (c (n "times") (v "1.0.1") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "0v9mjva4allsf8v2k0vicwv15938cbq5idbbr3l15dql75hihzc9")))

(define-public crate-times-1.0.2 (c (n "times") (v "1.0.2") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1") (d #t) (k 0)) (d (n "medians") (r "^1") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "0r2z0562srsg5w36zybs1pd3m9ba8py6m0j0wqcy70l4cb3ili5j")))

(define-public crate-times-1.0.3 (c (n "times") (v "1.0.3") (d (list (d (n "devtimer") (r "^4") (d #t) (k 0)) (d (n "indxvec") (r "^1.4.2") (d #t) (k 0)) (d (n "medians") (r "^1.0.4") (d #t) (k 0)) (d (n "ran") (r "^1") (d #t) (k 0)))) (h "01zm02ck6c7akxxizwrvaz7l4yldjxf1y5zivz3rnjyz1zaanl33")))

(define-public crate-times-1.0.4 (c (n "times") (v "1.0.4") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "14r6yv6kxfljc152a7wqk89mpclkjg25inipbcp4wd90zp2mmr3h")))

(define-public crate-times-1.0.5 (c (n "times") (v "1.0.5") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "000cw5q03bp0m540f8k86pasnrdxcwrbigh8yylj72fr9c7q71f5")))

(define-public crate-times-1.0.6 (c (n "times") (v "1.0.6") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^1.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "1mk7bck825xaqqv5kqr59cgl3yzsz4wvh32b5ndjpj4cr79cy6sy")))

(define-public crate-times-1.0.7 (c (n "times") (v "1.0.7") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.0") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "0lfmx21ra9bl366vp452h2h4kxlxjxr8fq5dgvbv5csphb5grvc2")))

(define-public crate-times-1.0.8 (c (n "times") (v "1.0.8") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "1dfp524b12hnb7afn99dz19ldq7gvs343941rrhf5jfz3hrw92gf")))

(define-public crate-times-1.0.9 (c (n "times") (v "1.0.9") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.4") (d #t) (k 0)) (d (n "medians") (r "^2.1") (d #t) (k 0)) (d (n "ran") (r "^1.0") (d #t) (k 0)))) (h "0340pphish4davl480fzrrxndzv7wcww9j2m1cvqv07bn4l1a69i")))

(define-public crate-times-1.0.10 (c (n "times") (v "1.0.10") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.5") (d #t) (k 0)) (d (n "medians") (r "^2.2") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)))) (h "12v3yn7692h95d8wm7s305j1fhd9bimfs9xjphjy5bm186ail7f5")))

(define-public crate-times-1.0.11 (c (n "times") (v "1.0.11") (d (list (d (n "devtimer") (r "^4.0") (d #t) (k 0)) (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)))) (h "09zi4lzil4cmfg8gg3ss751pd7971h4vbdnvjlczc27vii49gicl")))

(define-public crate-times-1.0.12 (c (n "times") (v "1.0.12") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)))) (h "1x0r9q0yac13jjwizq3msbir683nxnv87lh9aq9s0dfv6mif2r4q") (y #t)))

(define-public crate-times-1.0.13 (c (n "times") (v "1.0.13") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^2.3") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)))) (h "01c0lxydh6ysc1gg5sz4p81pcqrxfbfyb69ysmaxsjzlxdbdr5ff")))

(define-public crate-times-1.0.14 (c (n "times") (v "1.0.14") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^1.1") (d #t) (k 0)))) (h "0wbf691c05l7n8fh15xzjc205x3bpy8ny5cdrwziz6b06nlhwrzc")))

(define-public crate-times-1.0.15 (c (n "times") (v "1.0.15") (d (list (d (n "indxvec") (r "^1.8") (d #t) (k 0)) (d (n "medians") (r "^3.0") (d #t) (k 0)) (d (n "ran") (r "^2.0") (d #t) (k 0)))) (h "1cch2fxpd1n4z36rbrr46kl8yy6jvg9nz7l3r6j169zq23q9hxcg")))

