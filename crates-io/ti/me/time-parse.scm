(define-module (crates-io ti me time-parse) #:use-module (crates-io))

(define-public crate-time-parse-0.1.0 (c (n "time-parse") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)))) (h "0lay2532jqvk14wn0zkq12n6q1gbj3zyxmkxdlm0lr78cnqx89an")))

(define-public crate-time-parse-0.1.1 (c (n "time-parse") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1ixmmk97hr93xz0rah0jkd9qn2wylrnmjf7winnilv9n71pm9bvq")))

(define-public crate-time-parse-0.1.2 (c (n "time-parse") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0yxziplicsavnrz7cap5l5vbfwwp4dnciyb7wyrvvp0n08l1hmc6")))

(define-public crate-time-parse-0.2.0 (c (n "time-parse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0fm8l9ahvm4ryr1n3svdyzi0j1hbmp9llq6xgbs867pz7v70q5m1")))

