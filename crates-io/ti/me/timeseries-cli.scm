(define-module (crates-io ti me timeseries-cli) #:use-module (crates-io))

(define-public crate-timeseries-cli-0.1.0 (c (n "timeseries-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dzpv4413nv0gfybr3sj729ks22klqc63qv5g1fkl3qj5rdiizz4")))

(define-public crate-timeseries-cli-0.2.0 (c (n "timeseries-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1psgvq7cdn5zs6f3aqxksxllj2w5glfm30h7zm8xhjyrca3yr9wy")))

(define-public crate-timeseries-cli-0.3.0 (c (n "timeseries-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0kdbxa7xpkpkwslc4dk65vwx1q3pvxn6yq9sdhkiidf8hl6ykwv0")))

(define-public crate-timeseries-cli-0.4.0 (c (n "timeseries-cli") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1knmpbvvfgc3n467l5zhwbwnmc046mq5ldm1yg62dpxrf74blf9a")))

(define-public crate-timeseries-cli-0.5.1 (c (n "timeseries-cli") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16fpj81k5z9aawl3bcd3y3bm26iv4j1gc90dzmpzh5vawx88jr4l")))

(define-public crate-timeseries-cli-0.6.0 (c (n "timeseries-cli") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1m7grdzhqky8r12i2fpxshl4h0js9jadvcwd1rdpl1nbd27a5w6j")))

