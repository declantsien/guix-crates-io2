(define-module (crates-io ti me timer-util) #:use-module (crates-io))

(define-public crate-timer-util-0.1.0 (c (n "timer-util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1z03j9wpfg5k1f9r1l3nhan3jkqlnyn1avsqrjrca5bz0v03kp99")))

(define-public crate-timer-util-0.1.1 (c (n "timer-util") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "custom-utils") (r "^0.5.0") (f (quote ("logger"))) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0713dxnbxmyxvhsk7hvl4ama4z5f2zh44psj57s3k2jyv62avqvy")))

(define-public crate-timer-util-0.1.2 (c (n "timer-util") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "custom-utils") (r "^0.5.0") (f (quote ("logger"))) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "formatting" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1np7hi3b224dpj5vaz2ar6h2ymcb2zbhqlgn7jlszz4k20vlih4f")))

(define-public crate-timer-util-0.2.0 (c (n "timer-util") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.5.0") (f (quote ("logger"))) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "10mn7ai3xva2i34ndcfb7dyb5h2aa3rhvy9c0kwkgly4jfd2fcp5")))

(define-public crate-timer-util-0.2.1 (c (n "timer-util") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.5.0") (f (quote ("logger"))) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0ybbv5xpdz438gr0i81f7psli1g5hvls3r1h27551y0m6cw5p174")))

(define-public crate-timer-util-0.3.0 (c (n "timer-util") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "14clziazw6c3399hxz8l6dxdpc5k54y0ywak1i456wb4z76ircx8")))

(define-public crate-timer-util-0.3.1 (c (n "timer-util") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0599vsf16ra454jg319n6jmsd77mygfkfz8q0wljxrjmgdf2ha46")))

(define-public crate-timer-util-0.3.2 (c (n "timer-util") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1wz2ml5ay3w8ail7sxdg2r5v9y126v05dhiasvzcw15mgvzlrmjf")))

(define-public crate-timer-util-0.3.3 (c (n "timer-util") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0sprl49k4rnr9kdpz8fzx726vmdgjf7fvg2dj7hpsn5q40wa7d2x")))

(define-public crate-timer-util-0.3.4 (c (n "timer-util") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1fivbywq6hww0za4d57cmpacs4jzkjx73yjlwxiiy40118qsm7ay")))

(define-public crate-timer-util-0.3.5 (c (n "timer-util") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "045g31pdf5gclcfh31rvzg75h4m2ky179h5vcwxa7sp5bfiwk7va")))

(define-public crate-timer-util-0.3.6 (c (n "timer-util") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "custom-utils") (r "^0.8.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0hk2279a57nn720m810g3qhixv18f1ma6566fcd073bbnp36bqv2")))

