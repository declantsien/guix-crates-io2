(define-module (crates-io ti me timely-communication-master) #:use-module (crates-io))

(define-public crate-timely-communication-master-0.13.0-dev.1 (c (n "timely-communication-master") (v "0.13.0-dev.1") (d (list (d (n "abomonation") (r "^0.7") (d #t) (k 0)) (d (n "abomonation_derive") (r "^0.5") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "timely-bytes-master") (r "=0.13.0-dev.1") (d #t) (k 0)) (d (n "timely-logging-master") (r "=0.13.0-dev.1") (d #t) (k 0)))) (h "0m5r5jsv27qhsbwd4k4rn5gxmnj7sab5433qhiymk8d93fhq3qgr") (f (quote (("default" "getopts"))))))

