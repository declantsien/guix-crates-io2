(define-module (crates-io ti me timerius) #:use-module (crates-io))

(define-public crate-timerius-0.1.1 (c (n "timerius") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.7.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "0p92792kfa829lvxz0md2b9vc1lxc71jmglh65fz85djc4c4spqv")))

