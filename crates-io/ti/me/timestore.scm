(define-module (crates-io ti me timestore) #:use-module (crates-io))

(define-public crate-timestore-0.0.1 (c (n "timestore") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caos") (r "^0.0.5") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glommio") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "01c8xgmvn9w7mdx18y7xllq4hhkchrmg2vf2qb3hsj8bkijf8lvf")))

(define-public crate-timestore-0.0.2 (c (n "timestore") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caos") (r "^0.0.5") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glommio") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0drrb8yl30wd6725lamkg5i3wcgaj0jwva2023dfvvwm94m90ffb")))

(define-public crate-timestore-0.0.3 (c (n "timestore") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caos") (r "^0.0.5") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glommio") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0l31zhs0szi6aq8x3k862l0r93ccs7ylhznpvpm22d2gcj5r454c")))

(define-public crate-timestore-0.0.4 (c (n "timestore") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caos") (r "^0.0.7") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glommio") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "16phdk2nknx3lhazb227pgg70cpc5a3m0p26z17y575mkldcyfav")))

(define-public crate-timestore-0.0.5 (c (n "timestore") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caos") (r "^0.0.7") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "glommio") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0765gh6ascnn9j33ajjbsx5fw2km7r2hh6x84zsf0pyaqnndi71i")))

