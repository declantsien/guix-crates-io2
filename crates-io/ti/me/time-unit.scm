(define-module (crates-io ti me time-unit) #:use-module (crates-io))

(define-public crate-time-unit-0.1.0 (c (n "time-unit") (v "0.1.0") (h "067f2lwnhhgf6a22xcmlmscbqa41qp5vkfqsin7kcfy5kaaz2s45")))

(define-public crate-time-unit-0.1.1 (c (n "time-unit") (v "0.1.1") (h "0yjjpdkvxzmdpdxjpf8x8791j5kn9556shras46h18yrn916s14x")))

(define-public crate-time-unit-0.1.2 (c (n "time-unit") (v "0.1.2") (h "0zgdz6rhl4zvy8m3ms456ayar6vfwaj8b1vhwvqqa59k2x9xb4ss")))

