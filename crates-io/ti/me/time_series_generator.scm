(define-module (crates-io ti me time_series_generator) #:use-module (crates-io))

(define-public crate-time_series_generator-0.2.0 (c (n "time_series_generator") (v "0.2.0") (d (list (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "11ph1pkn411nqcpvmk216kxdrnx4fmja4c9ix5yidpyhkpnvy62l")))

(define-public crate-time_series_generator-0.3.0 (c (n "time_series_generator") (v "0.3.0") (d (list (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "05k1lvdzi0xrgbq9wd1qd1ra8amr3n01w46shpi3yb4n6z794ndc")))

(define-public crate-time_series_generator-0.3.1 (c (n "time_series_generator") (v "0.3.1") (d (list (d (n "nanorand") (r "^0.7") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "05mm3xc6rqjh361cpc3l1l9b2adsdlzxga5cikj3q7pmyzv8p1q1")))

(define-public crate-time_series_generator-0.3.2 (c (n "time_series_generator") (v "0.3.2") (d (list (d (n "nanorand") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0ff31gdfb4d600y4d9br4qnp3699cigk1bi2mb04ng4iji0hjgsr")))

(define-public crate-time_series_generator-0.3.3 (c (n "time_series_generator") (v "0.3.3") (d (list (d (n "nanorand") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1l4vydh35ndnzvmzmzl4c2ki3ar4950yw98jfja452g0qyn6pxsl")))

