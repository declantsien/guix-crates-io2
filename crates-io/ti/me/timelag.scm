(define-module (crates-io ti me timelag) #:use-module (crates-io))

(define-public crate-timelag-0.1.0 (c (n "timelag") (v "0.1.0") (h "12gy73w0z03yid1zixx6y6zv4i5iifpvwsksw6788h5inpc7nb4f")))

(define-public crate-timelag-0.2.0 (c (n "timelag") (v "0.2.0") (h "1bk7im0xk80x839gi8zb5p7cilm5iimzk0dcrf952qvagzw1ck7i")))

(define-public crate-timelag-0.3.0 (c (n "timelag") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)))) (h "1gyvhb0m62gk92hakybsq16317w6gyqava2kpgjf34qxrvg6yhm5") (f (quote (("default")))) (s 2) (e (quote (("ndarray" "dep:ndarray"))))))

(define-public crate-timelag-0.4.0 (c (n "timelag") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)))) (h "17v0lvs93vnnvf5kdf7a6hha0i4nxgzys0k7ylm2289r1k06zgjp") (f (quote (("default")))) (s 2) (e (quote (("ndarray" "dep:ndarray"))))))

