(define-module (crates-io ti me timely-container-master) #:use-module (crates-io))

(define-public crate-timely-container-master-0.13.0-dev.1 (c (n "timely-container-master") (v "0.13.0-dev.1") (d (list (d (n "columnation-master") (r "=0.1.0-dev.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gydln70dpld1jq0zp4l19k7k0fjw9mpwp1qncm59qh4cldgh2xn")))

