(define-module (crates-io ti me timebar) #:use-module (crates-io))

(define-public crate-timebar-0.1.0 (c (n "timebar") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "08ci1bwdszsxkzx0vkfa37nl39xpq0yiksjj3m2bvspbpg3pp912")))

(define-public crate-timebar-0.1.1 (c (n "timebar") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "09qjs05ab2hpryshbsybqhm4pf5p0f7bbizplqyxflm2gmfbkqba")))

(define-public crate-timebar-0.1.2 (c (n "timebar") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0yqz5hbhaayr13q4vvpa22mb28p855hck1824b4r5c4af89x7wyb")))

(define-public crate-timebar-0.1.3 (c (n "timebar") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0hysi3f4p6fn29jpwyl9iy0jqh3ij8r61qq3j08aizcnaaj2pgcw")))

(define-public crate-timebar-0.1.4 (c (n "timebar") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1wld1shxza3z9lcrlrh6v4dm5gsd3ak4jp51b04ag7zx5cdicf1d")))

(define-public crate-timebar-0.2.0 (c (n "timebar") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "14wlh884zwzkl4pl25829v2g6368x60wpqm6kgm9jh7bh8vi59b9")))

(define-public crate-timebar-0.2.1 (c (n "timebar") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ig0i1bxv0hpy4rxnmxdc0vnxfpryjz29rahjf60568xxx5y787j")))

(define-public crate-timebar-0.2.2 (c (n "timebar") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "08jih0nn6bvl24bajbrdwfy63b5nna91srmfzpdvp2qs3ix3glxf")))

(define-public crate-timebar-0.2.3 (c (n "timebar") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0brvdn3mdphqqcra08mksynwmrj9rkkjb7myzr0n3qj7x7nj40a2")))

(define-public crate-timebar-0.2.4 (c (n "timebar") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0k6zh6j1z2hp4pq2ff5j42bmhhgkwvsf57aqwgcgcmqi72c94cvq")))

