(define-module (crates-io ti me time_series_utils) #:use-module (crates-io))

(define-public crate-time_series_utils-0.1.0 (c (n "time_series_utils") (v "0.1.0") (d (list (d (n "auto-impl-ops") (r "^0.2.1") (d #t) (k 0)))) (h "1a39i25wsghd80smfxs0hssh0ps2765vshp7ssnk93nicc7q4v7y")))

(define-public crate-time_series_utils-0.1.1 (c (n "time_series_utils") (v "0.1.1") (d (list (d (n "auto-impl-ops") (r "^0.2.1") (d #t) (k 0)))) (h "0k4583k4crkcc8ngsby2c5sjzrv8a1q8n3d5cjszkychypn9f8c1")))

