(define-module (crates-io ti me time_value) #:use-module (crates-io))

(define-public crate-time_value-0.1.0 (c (n "time_value") (v "0.1.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "13jm8n0hjb27b02q3xai76cbcwkwqbid1s4vnqh8c2c7fgkxql7l") (y #t)))

(define-public crate-time_value-0.2.0 (c (n "time_value") (v "0.2.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "18z0kkzff6yap85zm51saj7ks3fx3bx2s4j796riav7iq82yr5af") (y #t)))

(define-public crate-time_value-0.3.0 (c (n "time_value") (v "0.3.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0bx74mbcfnyh0lzfkadikincx0zn7qiz716dadf7z8zadn4324wv") (y #t)))

(define-public crate-time_value-0.3.1 (c (n "time_value") (v "0.3.1") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1zszd0fgnzmlmkymf64cgf6xr7pjlrnnm67fcf5y6z8l6b33mzb1") (y #t)))

(define-public crate-time_value-0.3.2 (c (n "time_value") (v "0.3.2") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1jc770i8x1alp0z7lxp24aa1m9vpm8banp97glcdgx4vnf89i7b0") (y #t)))

(define-public crate-time_value-0.4.0 (c (n "time_value") (v "0.4.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "1l2gm6df4g6jxjbr5plccbl9cnsn3h0ffxdiaskin3lc9qbqw2lg") (y #t)))

(define-public crate-time_value-0.4.1 (c (n "time_value") (v "0.4.1") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "0fp2yyr6b4vq5df4hqdw1x5ah89q3jvg850m1jjy1rmas574a9zf") (y #t)))

(define-public crate-time_value-0.4.2 (c (n "time_value") (v "0.4.2") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "11m1kxlqymb4cf82xpnnizwwahc6klx3sifd06y1n6m8wyfmr38a") (y #t)))

(define-public crate-time_value-0.6.0 (c (n "time_value") (v "0.6.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "19b9l3v49iw1g1b8j4ngfpgp5hqr12wz3vdndlhlbcfndikwmmgr") (y #t)))

(define-public crate-time_value-0.7.0 (c (n "time_value") (v "0.7.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "0zqycqzb0hb2092idqdjwmwb5npz0knbscgfw4jpz95xmd9c8gmn") (y #t)))

(define-public crate-time_value-0.8.0 (c (n "time_value") (v "0.8.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)))) (h "10vrshl5n2w4i3pr0vg0fz1k6p6ivkyig1h29bjqys5816fj4hkd")))

