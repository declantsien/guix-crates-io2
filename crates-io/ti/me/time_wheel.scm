(define-module (crates-io ti me time_wheel) #:use-module (crates-io))

(define-public crate-time_wheel-0.1.0 (c (n "time_wheel") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0zzpl1sqlwxwjml9l21i8ryhah74l6b67434m7gm5ypxxl4bnpw9")))

(define-public crate-time_wheel-0.1.1 (c (n "time_wheel") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0l1l4rilx5n3823icsiaimin0m3lpawgh2kadka6dfw6smj43q0y")))

(define-public crate-time_wheel-0.1.2 (c (n "time_wheel") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "19wfpld1wi7giayw9wqzvslmn9gajpi57ygnb1kwrcmhfdxbwdiv")))

