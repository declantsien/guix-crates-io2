(define-module (crates-io ti me timeme) #:use-module (crates-io))

(define-public crate-timeme-0.1.0 (c (n "timeme") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "0rr40qd4yxslifharlp3j870inkmryzbb6fd4m53ky1vgq9hdkyj")))

(define-public crate-timeme-0.2.0 (c (n "timeme") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "11mx8s31qkhk1v9s9q0g5mq7zlvm7462kmz6r6689xfr5rba7ljw")))

(define-public crate-timeme-0.2.3 (c (n "timeme") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "1dnkwzqd18zxn6i0x6mlcdxkmla2ymazf7fnh8k8mc0d53mzk5vg")))

(define-public crate-timeme-0.2.4 (c (n "timeme") (v "0.2.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "11pq8q9klryswywvhnxni1vg54r865b5fxq466nhmq1cyyb8qk05")))

