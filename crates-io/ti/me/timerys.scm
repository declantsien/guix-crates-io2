(define-module (crates-io ti me timerys) #:use-module (crates-io))

(define-public crate-timerys-0.1.0 (c (n "timerys") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 0)) (d (n "iced") (r "^0.12.1") (f (quote ("advanced" "tokio" "wgpu"))) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "1096ihv3850iw244177bnms2nwmdbmnwg641axpp7n2jw57xnkkl")))

