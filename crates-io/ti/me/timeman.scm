(define-module (crates-io ti me timeman) #:use-module (crates-io))

(define-public crate-timeman-1.0.0 (c (n "timeman") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "color"))) (d #t) (k 0)))) (h "0dav8f78k7zlkzc3s5v0msyzqc87n5m0a7ri3x4sq54w89wpfy35")))

