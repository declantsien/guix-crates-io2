(define-module (crates-io ti me time_range) #:use-module (crates-io))

(define-public crate-time_range-0.0.3-alpha.0 (c (n "time_range") (v "0.0.3-alpha.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-log") (r "^0.2") (f (quote ("log" "trace"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log" "attributes"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1a43dhf7a4haydpxpgqwlfv001gnhs66gsfj1aci2691mnnpijms") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

