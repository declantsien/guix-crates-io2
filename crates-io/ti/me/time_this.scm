(define-module (crates-io ti me time_this) #:use-module (crates-io))

(define-public crate-time_this-0.1.0 (c (n "time_this") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "14161df1m4ah69k85vrxcn09xbqwhmhfqrqqamxk9hc1bgckn285")))

(define-public crate-time_this-0.2.0 (c (n "time_this") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1hijl76rv43glabyxxsfk5lqzabby4kpmkq4njcs5xihp0fcnknd")))

(define-public crate-time_this-0.2.1 (c (n "time_this") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0ymwk102w5qp8cpjb362qirv27z02g4x9qirslmcrvghsx8fslpa")))

(define-public crate-time_this-0.2.2 (c (n "time_this") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "12ij94sylpqvvavgvwpz25cxsqhijr8l35j923zkh9zd5c9kry11")))

(define-public crate-time_this-0.2.3 (c (n "time_this") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "00gszpq3h8n014y4cc1nf4qik6jxh12i7i88wfg5zz4s5qpg6qjl")))

(define-public crate-time_this-0.2.4 (c (n "time_this") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "1hza39i0p560amyz29434miakw38fj9k56s0w3jg36rg91510ymq")))

(define-public crate-time_this-0.2.5 (c (n "time_this") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0s3xdr0zksir034bvgzwy8drnc6z063rz5ci811aandggqk51kvw")))

