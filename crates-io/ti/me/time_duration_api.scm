(define-module (crates-io ti me time_duration_api) #:use-module (crates-io))

(define-public crate-time_duration_api-0.1.0 (c (n "time_duration_api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "15dqfnidhaxcpkzxmmnifvx959zcvdgfip300l20x9r8abdpnm0x")))

(define-public crate-time_duration_api-0.1.1 (c (n "time_duration_api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1gscqm2zl2x3y45aacvxl0gsr0ag6b2pcdzmxiqqb4l4d3mnm57w")))

(define-public crate-time_duration_api-0.1.2 (c (n "time_duration_api") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1zv6dhx1caklxqdlss5jfqim5gfg7fqjrmc25zpcd6ldl78sfq6s")))

(define-public crate-time_duration_api-0.1.3 (c (n "time_duration_api") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "0nf19zaxvmyk2ni2qvyl1dhz6ykmzawznmnrqbya6c1fkyc859l8")))

(define-public crate-time_duration_api-0.1.4 (c (n "time_duration_api") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "040bfy4gdh9j7arzqxsdvndfjq6zq0qqhdw25cclw7k8q8c3sc5v")))

(define-public crate-time_duration_api-0.1.5 (c (n "time_duration_api") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "0b8cs4c51ir3sg75hyrw8qp49b4sag06ggkjhjaa6wfwrmjas8ap")))

(define-public crate-time_duration_api-0.1.6 (c (n "time_duration_api") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)))) (h "01msvkv1qz7ky3akdh5kqd622blm681n1fayf32v9634xaiskfia")))

