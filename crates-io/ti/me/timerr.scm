(define-module (crates-io ti me timerr) #:use-module (crates-io))

(define-public crate-timerr-0.1.0 (c (n "timerr") (v "0.1.0") (h "1hf28rfvakxhcnsyri0j29fpz26rbdd6xckg2z884dd9gadmlydh")))

(define-public crate-timerr-0.1.1 (c (n "timerr") (v "0.1.1") (h "0knjg575dvy18l31zpqnkbsryhhy0rg13g81ysl0rhdfcwnamhb9")))

