(define-module (crates-io ti me timeboost-rs) #:use-module (crates-io))

(define-public crate-timeboost-rs-0.1.0 (c (n "timeboost-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1rzrfgycdsd2x4hqlyq758w9vcnfsqw1jic4w77szcakcn5d6bqn")))

(define-public crate-timeboost-rs-0.1.1 (c (n "timeboost-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0jbv876jm2w55rlfajn13ic4vf4iq4nq2jjjfmi9sa48wdwh4815")))

(define-public crate-timeboost-rs-0.1.3 (c (n "timeboost-rs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "13im6hshhcnqzqh39afj6y1cbd7pyib8jaf7lx52nadb11db0vv2")))

(define-public crate-timeboost-rs-0.1.4 (c (n "timeboost-rs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "147rgcz0sg2icizv4m488j4wb8n25l9yc5aa6nzsp9rfpawmlb9b")))

(define-public crate-timeboost-rs-0.1.5 (c (n "timeboost-rs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1gk6gyar2cyn6r7qiknb25rpwigzab2wm46wq2i9nykm0603plxm")))

