(define-module (crates-io ti me time-core) #:use-module (crates-io))

(define-public crate-time-core-0.0.0-alpha.0 (c (n "time-core") (v "0.0.0-alpha.0") (h "08m6igr318y7h2qfvj5dvlzmwyq19ak1iadn33xgbhfpg266jw92") (y #t)))

(define-public crate-time-core-0.1.0 (c (n "time-core") (v "0.1.0") (h "1z803zwzyh16nk3c4nmkw8v69nyj0r4v8s3yag68mvya38gkw59f") (r "1.60.0")))

(define-public crate-time-core-0.1.1 (c (n "time-core") (v "0.1.1") (h "1yz6d246zbmx9v6wpfg1jyfjlsgagirz7km96pr1mp6snkpzn03k") (r "1.65.0")))

(define-public crate-time-core-0.1.2 (c (n "time-core") (v "0.1.2") (h "1wx3qizcihw6z151hywfzzyd1y5dl804ydyxci6qm07vbakpr4pg") (r "1.67.0")))

