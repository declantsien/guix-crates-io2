(define-module (crates-io ti me time_server_utils) #:use-module (crates-io))

(define-public crate-time_server_utils-0.1.0 (c (n "time_server_utils") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ldwgna3lhxzlw4c2v6fcpph8gm1pi6g2y6kw7l064kywy1r392d")))

