(define-module (crates-io ti me timeflake-rs) #:use-module (crates-io))

(define-public crate-timeflake-rs-0.1.0 (c (n "timeflake-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1wmq19hn60bslp3fiqxsr90kgghmins15w48a4602fi1b3hlpb5n")))

(define-public crate-timeflake-rs-0.2.0 (c (n "timeflake-rs") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "101m217r9vm4b6fi49qp9hndxprzqcf6s25nxljrpq9q9pi8pr4x")))

(define-public crate-timeflake-rs-0.2.1 (c (n "timeflake-rs") (v "0.2.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0f0hmprkk3l2j7bgs9wdyyk0qdjgmsdwc5ympkl9xzkij9awmqna")))

(define-public crate-timeflake-rs-0.2.2 (c (n "timeflake-rs") (v "0.2.2") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1axl0sqmcrc36h1n5w4cwshji24lq34d9cdd91lzisnj5vg9xd2m")))

(define-public crate-timeflake-rs-0.2.3 (c (n "timeflake-rs") (v "0.2.3") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "10ikdh24d917pklq347dzl23cci7jr8p2hif5769klsnz8xsx5jb")))

(define-public crate-timeflake-rs-0.2.4 (c (n "timeflake-rs") (v "0.2.4") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "1xgpgn38xplbqbbm0zknhjxsnn7abxvnrpczqiyma670gh41dsaf")))

(define-public crate-timeflake-rs-0.3.0 (c (n "timeflake-rs") (v "0.3.0") (d (list (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (d #t) (k 0)))) (h "13kly1phxv8h6vc6z3b9g7bxwyphn3ysrnp2xzrx45dbc84dhvyb")))

