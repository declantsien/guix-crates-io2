(define-module (crates-io ti me time-check-loop) #:use-module (crates-io))

(define-public crate-time-check-loop-0.1.0 (c (n "time-check-loop") (v "0.1.0") (h "174fv6s12ww0knbvf2na29dwdf292zccgg8gngplyp1fsq30dwqy")))

(define-public crate-time-check-loop-0.1.1 (c (n "time-check-loop") (v "0.1.1") (h "16xx7ag4frwpd1khhxg9n4050fkywzg35xd4qjqnmk0kv5z5sqs0")))

