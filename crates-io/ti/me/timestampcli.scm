(define-module (crates-io ti me timestampcli) #:use-module (crates-io))

(define-public crate-timestampcli-0.1.0 (c (n "timestampcli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1fyl18h2az43kvk3bgm4lia05n5qc4g8ba8hn3il1ssr5n8nchaj")))

(define-public crate-timestampcli-0.1.1 (c (n "timestampcli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1m7nq72x6dxd7bvqjzix2rjrc8zr4n3lc58r453apba999ck97bm")))

(define-public crate-timestampcli-0.2.0 (c (n "timestampcli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0pagizgqpzc77cy54n4x7p4p6rbsnf60vqv4vaxcdb9hhhabhggi")))

