(define-module (crates-io ti me timer_heap) #:use-module (crates-io))

(define-public crate-timer_heap-0.1.0 (c (n "timer_heap") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)))) (h "0mgwvni6vcaxfddq4bclpkbdxkh5wifb0g101vp7zvhz2m0pifxr")))

(define-public crate-timer_heap-0.1.1 (c (n "timer_heap") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)))) (h "0yq7ja6q69caj3dw62ssfnh272wfvcn2bqm4xxmj57nz0z4hhw88")))

(define-public crate-timer_heap-0.2.0 (c (n "timer_heap") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)))) (h "1c1890ii5iabm8mbvzqp6p0bvbn8akwrhfz82h2npra0rl7vbnws")))

(define-public crate-timer_heap-0.3.0 (c (n "timer_heap") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)))) (h "0vpxgp3wk5n97yp0g180p07xszl803xxjq163jbhrmqmpz8w8wgy")))

