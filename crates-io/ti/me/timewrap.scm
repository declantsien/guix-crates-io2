(define-module (crates-io ti me timewrap) #:use-module (crates-io))

(define-public crate-timewrap-0.1.0 (c (n "timewrap") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "1f44w51fskzwc9ydm5qzcl1lja8qp7ajni5ipsbviay54yk4cnd6") (f (quote (("runtime" "tokio") ("default" "runtime")))) (y #t)))

(define-public crate-timewrap-0.2.0 (c (n "timewrap") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0ffkqchg18y7lgnh97wn9glihm3rxwng5hfnaxwqv36xj3ikjhqj") (f (quote (("drive_block" "futures") ("default")))) (y #t)))

(define-public crate-timewrap-0.2.1 (c (n "timewrap") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "10qjss5v47211hkq8ddga3cax5hlc2xaps13r8j5kv541lpsjdd0") (f (quote (("drive_block" "futures") ("default")))) (y #t)))

(define-public crate-timewrap-0.2.2 (c (n "timewrap") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0k7dg07s0cv1ngahjjqmrcc4gh2xy4bfx23ra6xhd1gpw2wjmjxr") (f (quote (("drive_block" "futures") ("default")))) (y #t)))

(define-public crate-timewrap-0.2.3 (c (n "timewrap") (v "0.2.3") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "00jalnal66mkx155sk822smlrdx6ab6ic1a0h5jk7lamqwkycl2r") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default")))) (y #t)))

(define-public crate-timewrap-0.3.0 (c (n "timewrap") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1sllgdh6m33hfax24hzsg7z5vbydlyzg1spn4as4f30r8b21mn1n") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default"))))))

(define-public crate-timewrap-0.3.1 (c (n "timewrap") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1r3qaq0hafdy7by2inf8dcwkv65q4ghp4ix1b41hj111p96wdvpa") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default"))))))

(define-public crate-timewrap-0.3.2 (c (n "timewrap") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "10nivxbp7rbmd53pk9qwr43myrib1j0aah44s2wi8dbn9874r8dk") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default"))))))

(define-public crate-timewrap-0.3.3 (c (n "timewrap") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1n7xmwfkvwa6pwq0parvghajmn7zrj8m3672vk4v8z7n4w0m447r") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default"))))))

(define-public crate-timewrap-0.3.4 (c (n "timewrap") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0mvlhhzhr47vkwj46r4xg00d0lipj6ykgwwzz4k8bfqinly034gf") (f (quote (("full" "drive_block" "drive_shared") ("drive_shared" "futures") ("drive_block" "futures") ("default"))))))

