(define-module (crates-io ti me timer-queue) #:use-module (crates-io))

(define-public crate-timer-queue-0.0.0 (c (n "timer-queue") (v "0.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "02ccgd2xhs01s9akjki8jm1qpdsxkq5828cxigqlm0hpx6ixhz7l")))

(define-public crate-timer-queue-0.1.0 (c (n "timer-queue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.7") (k 0)))) (h "0f7h851xqw0vag9lgk94xbvx5c0n96zrh512fvsnz0rxqhlnqx8k")))

