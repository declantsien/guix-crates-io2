(define-module (crates-io ti me timey) #:use-module (crates-io))

(define-public crate-timey-0.1.0 (c (n "timey") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "1l4w5h9bna3qy90r7jhw91gwricb7bk8772j2r69bsa5m368s03d")))

(define-public crate-timey-0.2.0 (c (n "timey") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)))) (h "0g0a4fl08zlyzsl728smkpfxkc7ghv5xyaa1vnxb6gs8vi9ijqg1")))

