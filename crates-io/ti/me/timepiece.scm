(define-module (crates-io ti me timepiece) #:use-module (crates-io))

(define-public crate-timepiece-0.2.0 (c (n "timepiece") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0wf51nddzky6qznbkd8vn72z8fj9q84dv3v8z27ppcy1mcdf0r3j")))

(define-public crate-timepiece-0.3.0 (c (n "timepiece") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)))) (h "14sz5rsrqzljdagnbg7v7z9sjmx3rfxqaz8l9q1ya5sq5zqyc19i")))

(define-public crate-timepiece-0.3.1 (c (n "timepiece") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "0c43zdi529rrbv4i81qsdwx54h22wkixdbw5lsdidcnfd3hvd827")))

(define-public crate-timepiece-0.3.2 (c (n "timepiece") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "09m5vcn2aa19hngaw85rx71vi30gzimjvxj6zbj9viyfbggd1jis")))

(define-public crate-timepiece-0.4.0 (c (n "timepiece") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "0s2c7fzkq71467bnyzm3qx9jnrwf4aavki8vxh7vhbc44gm0cmam")))

(define-public crate-timepiece-0.5.0 (c (n "timepiece") (v "0.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1hm2nswb64jh22g4a0n688qlpk4y00gkn24m3qpqy237nnx4v4xj")))

(define-public crate-timepiece-0.5.1 (c (n "timepiece") (v "0.5.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1h3a0703prrx989cmsb3mcsw28655z9qpc2la3r8575v9905xmlx")))

(define-public crate-timepiece-0.5.2 (c (n "timepiece") (v "0.5.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1sikvx8564bl9qxllyav6x63rkw7dxi4hdz3h9mix4ppwclw2myr")))

(define-public crate-timepiece-0.5.3 (c (n "timepiece") (v "0.5.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "0r77rdwdyaq0chkxr49g18a12g0i5yjd3izsr681xch78p8qqdq2")))

(define-public crate-timepiece-0.6.2 (c (n "timepiece") (v "0.6.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "0cw38mvpgbls0z66ddbhcrgw7bzpr5zb4hgn701h0zgh4jlbik9g")))

(define-public crate-timepiece-0.6.3 (c (n "timepiece") (v "0.6.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1q2ic4x2lbq4ghlvjf4y0p83nznbchi507hknzfvyq22d7cpl6gr")))

(define-public crate-timepiece-0.7.0 (c (n "timepiece") (v "0.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)))) (h "1ysnhkj4nnxks5w9dmrli2mx8l9l4w04dbaghhf4k00mn2dghs90") (f (quote (("default")))) (s 2) (e (quote (("notify" "dep:notify-rust"))))))

(define-public crate-timepiece-0.7.2 (c (n "timepiece") (v "0.7.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)))) (h "0il8sx1y0g7m07jmg9wvh6dx1zhrnpk619vx6dzv6cvay4jkpa5r") (f (quote (("default")))) (s 2) (e (quote (("notify" "dep:notify-rust"))))))

(define-public crate-timepiece-0.7.3 (c (n "timepiece") (v "0.7.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)))) (h "0kibjjdwp82w7xv7vbxbrq3rxmbxdl0yvkspdyiicfba45viv64l") (f (quote (("default")))) (s 2) (e (quote (("notify" "dep:notify-rust"))))))

(define-public crate-timepiece-0.7.5 (c (n "timepiece") (v "0.7.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)))) (h "0wscpfc662sf62a0y3chg9273ml9hhibzc1mzvnbsaglvnqdvj6h") (f (quote (("default")))) (s 2) (e (quote (("notify" "dep:notify-rust"))))))

(define-public crate-timepiece-0.7.6 (c (n "timepiece") (v "0.7.6") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (o #t) (d #t) (k 0)))) (h "1mvwc63xlspsj5752si41xclqf9n0vy4wwm827vzg2kss3x2p2k0") (f (quote (("default")))) (s 2) (e (quote (("notify" "dep:notify-rust"))))))

