(define-module (crates-io ti me timetracker) #:use-module (crates-io))

(define-public crate-timetracker-0.1.0 (c (n "timetracker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0b81ada2z44z5ydghcycv8dnmslrmj5hcal6i5jwknn09sw77lh4")))

