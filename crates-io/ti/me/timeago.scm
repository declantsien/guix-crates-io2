(define-module (crates-io ti me timeago) #:use-module (crates-io))

(define-public crate-timeago-0.0.1 (c (n "timeago") (v "0.0.1") (h "148a4yaqf9a6rlxvc1r0bxy33pi2skv23bcbdfvp28swd3c1vrcc")))

(define-public crate-timeago-0.0.2 (c (n "timeago") (v "0.0.2") (h "0bz79yspj7371pzgbkb2sl5sfhp19r3lbb8g215hrkmms2xcianb")))

(define-public crate-timeago-0.1.0 (c (n "timeago") (v "0.1.0") (d (list (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1yrnpnk5qhsww0ll91g9h03p302jf0iza26w43m9gj0y23a55yn5") (f (quote (("translations") ("default" "translations"))))))

(define-public crate-timeago-0.1.1 (c (n "timeago") (v "0.1.1") (d (list (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "09y5s7lf6lqw974vw4anvrla6x1zwjmpih7x0i3lxd7wbylddbdm") (f (quote (("translations") ("default" "translations"))))))

(define-public crate-timeago-0.1.2 (c (n "timeago") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "03c0bfkl6ani46bqlblzfx62vhw7ab6385rw3zjnd562xmz9c0ac") (f (quote (("translations") ("default" "translations"))))))

(define-public crate-timeago-0.1.3 (c (n "timeago") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1j5y33lijxcwvpndxcjxkgqwksbr090h4al18nrzfkisnrqsa27d") (f (quote (("translations") ("default" "translations"))))))

(define-public crate-timeago-0.1.4 (c (n "timeago") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1a4q6vvgdp3i4n1y1lzc4dw85yyjr6f68y7fq9q4q9cqpwm0mzjb") (f (quote (("translations") ("default" "translations"))))))

(define-public crate-timeago-0.1.5 (c (n "timeago") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1hjzwxk62rlikv782p0fg50gmnvxzmyv2misqrv7i0nj8pggaqsy") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.2.0 (c (n "timeago") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^1") (o #t) (d #t) (k 0)))) (h "0g0bba27s2jk34j5bawap6m3abymr0laa2d88n5idj7n536y76dn") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.2.1 (c (n "timeago") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^1") (o #t) (d #t) (k 0)))) (h "1z3wjs2k60jrl19xkrv2dpv49h5qrrnlk1gaznnn9mljmkqz7wmg") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.2.2 (c (n "timeago") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^1") (o #t) (d #t) (k 0)))) (h "16k8nd0caahg1nma8xrmpzcplvg6xglkgszpvlbsl4spk1bzxfw6") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.3.0 (c (n "timeago") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^1") (o #t) (d #t) (k 0)))) (h "0s473ys6qqyyf9xb3awcbnrychjhsyjjli4y9mns126c5n929pj7") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.3.1 (c (n "timeago") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^1") (o #t) (d #t) (k 0)))) (h "09ivk2a4w7d04h2wna2z758h90ijgy6i2x60b81mrcggazg2vhvf") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.4.0 (c (n "timeago") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^2") (o #t) (d #t) (k 0)))) (h "14s9hdmsx0ha2gcfvgzadj0d8qjmf7gy5yh1i92rjp95fknn1gbi") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.4.1 (c (n "timeago") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^2") (o #t) (d #t) (k 0)))) (h "1d7wniw3f8bibiyby81spgjiaq1dfsazk2xrx9sgpkb14fadr0jh") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

(define-public crate-timeago-0.4.2 (c (n "timeago") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "isolang") (r "^2") (o #t) (d #t) (k 0)))) (h "1rnh92sh1l4jbjvz4g7xvcvmfh7nk5k7mm2w56pnm9z0kmc0wwd1") (f (quote (("translations") ("default" "translations" "isolang" "chrono"))))))

