(define-module (crates-io ti me timely_logging) #:use-module (crates-io))

(define-public crate-timely_logging-0.7.0 (c (n "timely_logging") (v "0.7.0") (h "10dahgwzdr4ar6as411y7b9amp1zmq0qk8j0m4qqyg1z6vhgx6dw")))

(define-public crate-timely_logging-0.7.1 (c (n "timely_logging") (v "0.7.1") (h "1f0qs1z4gb6nixydng683injf0x2vkcwy1ncggxffjw7a17xjm3f")))

(define-public crate-timely_logging-0.9.0 (c (n "timely_logging") (v "0.9.0") (h "0kg0cs60ccbahjyq1vzplvlkch6jl8rd23dyd9bh51g1ij2pdyv2")))

(define-public crate-timely_logging-0.10.0 (c (n "timely_logging") (v "0.10.0") (h "1qppyyn8a51c5bxpyvda3d4cyb6jfymr9anp2w9f2csmcihjnxv0")))

(define-public crate-timely_logging-0.11.0 (c (n "timely_logging") (v "0.11.0") (h "1a6vh8hcffjkfp27ykaldmapbgz9xqr95vmpn6zmgzhd2a9x89ya")))

(define-public crate-timely_logging-0.11.1 (c (n "timely_logging") (v "0.11.1") (h "0i7f7d16fbrk61b3jch3m7rw5s5rn5zcpm2slbhkqdkm7ivwzpgh")))

(define-public crate-timely_logging-0.12.0 (c (n "timely_logging") (v "0.12.0") (h "0n7qrm69xjw25b2dc8mv7jd5j8lmzn5fnifjg4a87b17i3r6l17r")))

