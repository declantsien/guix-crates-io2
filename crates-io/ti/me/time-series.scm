(define-module (crates-io ti me time-series) #:use-module (crates-io))

(define-public crate-time-series-0.1.0 (c (n "time-series") (v "0.1.0") (h "0706brhw9an6ycfbi1xgkzdvmvi55s67wfq3swg2z9kjvwhbmbxd")))

(define-public crate-time-series-0.2.0 (c (n "time-series") (v "0.2.0") (h "06i1wdw5grla9lsxddcwhs7yqk5gbycwahfhyfwc687by3ky8c6w")))

