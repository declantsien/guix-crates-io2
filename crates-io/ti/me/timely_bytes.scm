(define-module (crates-io ti me timely_bytes) #:use-module (crates-io))

(define-public crate-timely_bytes-0.7.0 (c (n "timely_bytes") (v "0.7.0") (h "12gb7k4vr5lh92jlix9l1sj2nm8h4xhrddc3bhwg1b6pc5k2mr93")))

(define-public crate-timely_bytes-0.9.0 (c (n "timely_bytes") (v "0.9.0") (h "0h31jp5zkcfbppbjlkvjxcax5y3v9a7wjdrk3v4shlfcjx846qck")))

(define-public crate-timely_bytes-0.10.0 (c (n "timely_bytes") (v "0.10.0") (h "031h9ib0xwgd05c52k2x9dvpdqa9fz2ydxvlkma957cjpq5sl7i2")))

(define-public crate-timely_bytes-0.11.0 (c (n "timely_bytes") (v "0.11.0") (h "1i65hy16rjr6clr26lbsc4pzpc3wfn7igspzcpic6chmkw6mkank")))

(define-public crate-timely_bytes-0.12.0 (c (n "timely_bytes") (v "0.12.0") (h "1kx2yavia6ly6jvwfclhyfxjqm2zd4a354gv7avbh6dh6mdm92hz")))

