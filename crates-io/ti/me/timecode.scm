(define-module (crates-io ti me timecode) #:use-module (crates-io))

(define-public crate-timecode-0.1.0 (c (n "timecode") (v "0.1.0") (h "1sr21hx4nh57aph4hzvbfs5r99llby2ra8drx75i4rxmy7nbaydq")))

(define-public crate-timecode-0.1.1 (c (n "timecode") (v "0.1.1") (h "1g3ya78lz3pmf3sr2dfvha5f44x72cdascs4hiw59mpz7hxpll9b")))

(define-public crate-timecode-0.1.2 (c (n "timecode") (v "0.1.2") (h "0rg88lzblab35j8lm5mwvywmxyqy64d0js9m2qc5v06qfwv9pwi4")))

(define-public crate-timecode-0.1.3 (c (n "timecode") (v "0.1.3") (d (list (d (n "serde") (r "^0.9.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.8") (d #t) (k 0)))) (h "0c1n3p3zrgv433132m59rln8l3a99qgghvlfrbp872bfbh7rx8dd")))

(define-public crate-timecode-0.1.4 (c (n "timecode") (v "0.1.4") (d (list (d (n "alga") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "09bi5j6g5f4wf1h1irmizg6gv57qlld81c7rj8rqcr82nyfmsifk")))

(define-public crate-timecode-0.1.5 (c (n "timecode") (v "0.1.5") (d (list (d (n "alga") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1khln4nyymns8f7qzffkqd1xslc0i195almi84qp487xcnhxvclp")))

(define-public crate-timecode-0.2.0 (c (n "timecode") (v "0.2.0") (d (list (d (n "alga") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gmvidbm61a81sa6q9j0q9siz33kyd42lq64c9dz67xg8d47mlrn")))

(define-public crate-timecode-0.2.2 (c (n "timecode") (v "0.2.2") (d (list (d (n "alga") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00kc1yyknpfs4axnq93ql6cqawri5s05qs000ax3d5grjxdvwav7")))

(define-public crate-timecode-0.2.3 (c (n "timecode") (v "0.2.3") (d (list (d (n "alga") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fy0p9zdyd6dm5cn3hvvpccrvwyyqv8c2ah040ml65jrb4sq6d4s")))

(define-public crate-timecode-0.3.0 (c (n "timecode") (v "0.3.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0gfyq01nrs64y60xqh86j89vjnfxd86f46b36sfnc40gcylqn8sf")))

(define-public crate-timecode-0.4.0 (c (n "timecode") (v "0.4.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gr5bshl8z4nby5cc488xwc1hq2m5l6kqwnfbvhg7sylfs0xxgdr")))

(define-public crate-timecode-0.4.1-alpha.0 (c (n "timecode") (v "0.4.1-alpha.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rni0k4ql2d09w3hjx3ykazn3d1458zrys9yi85pz8gr5gcyk0rs")))

(define-public crate-timecode-0.4.1 (c (n "timecode") (v "0.4.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1v504fdpj07dy0yk7qbyqw2kcvzf79xvkrjsyrdz3h2k29mypwrp")))

(define-public crate-timecode-0.4.2 (c (n "timecode") (v "0.4.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sb150syxi2wp9i05nswzvvyayprysmr9m3s54fmwjzl4qq8hq6f")))

(define-public crate-timecode-0.5.0 (c (n "timecode") (v "0.5.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rqjv10n41714ybpnn9frvnh2j4prwp6qzmrakpf6v46wljv2f9z")))

(define-public crate-timecode-0.5.1 (c (n "timecode") (v "0.5.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06z98pfncfs0n50navrbaqrb44gd8bjzsdjrl5yhc1jxikzmgabd")))

(define-public crate-timecode-0.5.2 (c (n "timecode") (v "0.5.2") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0c97l7598x24qk1znq8vdk0hrr2pghwr6cziq97ii47ii58fdl16")))

(define-public crate-timecode-0.6.0 (c (n "timecode") (v "0.6.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "frame-rate") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1sk438iyhhfddw4mjdymb6gmci6ifglzqli3wjxrayjb61xy9j2k")))

(define-public crate-timecode-0.7.0 (c (n "timecode") (v "0.7.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "frame-rate") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1mhg2gr7dp5wrwjxaiynpibklnlaa4qw75ph1ng9159rhzd38942")))

(define-public crate-timecode-0.7.1 (c (n "timecode") (v "0.7.1") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "frame-rate") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1p3sidl0r5rxm40q3znpk7x85q6gph8jldq8z0g2fv3p8lsdihh4")))

(define-public crate-timecode-0.7.2 (c (n "timecode") (v "0.7.2") (d (list (d (n "frame-rate") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0fhai985nl99a9lrmysj4d24qrlrswzpy3jybxlhq8fhss71q2fs")))

