(define-module (crates-io ti me timed-queue) #:use-module (crates-io))

(define-public crate-timed-queue-0.1.0 (c (n "timed-queue") (v "0.1.0") (h "133qf4nkjjmnznlay0y9cgvbi593m715wj559dg1y4h1s9vj1940")))

(define-public crate-timed-queue-0.2.0 (c (n "timed-queue") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1876ih7mvmrvh52ifyn9grpkghp07ihjfrcvazd15dfy5klcpa4a")))

(define-public crate-timed-queue-0.2.1 (c (n "timed-queue") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhgm7cnrwsz7ybvdm1p3mx5c4hmf9bfcsl1yg6dh4vfm7rdcmjx")))

(define-public crate-timed-queue-0.2.2 (c (n "timed-queue") (v "0.2.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9zdax83kj2xlz1f95rqkrhndqdvisk4ck225ik1gid8cr1k8an")))

(define-public crate-timed-queue-0.2.3 (c (n "timed-queue") (v "0.2.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hx5k9xlvb5n5f7nh817xl3mk5l594aii5271ah0bxrwn4irzdh7")))

