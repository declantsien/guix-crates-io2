(define-module (crates-io ti me timely_sort) #:use-module (crates-io))

(define-public crate-timely_sort-0.1.1 (c (n "timely_sort") (v "0.1.1") (h "0scxjzwfnk0av4m2p3s047iy7pwgagx3k3b8n052lmrqphnhgrns")))

(define-public crate-timely_sort-0.1.2 (c (n "timely_sort") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "184dlw27qs3y0r3ls2y597kqlrqb032yv1z25l1wg1wdblcyr29p")))

(define-public crate-timely_sort-0.1.3 (c (n "timely_sort") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "1ncifdq789pi1ap32dcb0xnbajq27afrh2g2jlcc71zhwf6rvaf7")))

(define-public crate-timely_sort-0.1.4 (c (n "timely_sort") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "0glx7zzx1kq0xn0n7qan3d9yqq6s0dfya0ppbjffn79hka2ijpnk")))

(define-public crate-timely_sort-0.1.5 (c (n "timely_sort") (v "0.1.5") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "0mlvqmmphv4v4y8lx5r268p4ribwfg8wq3gvn85f73zyfmk952y9")))

(define-public crate-timely_sort-0.1.6 (c (n "timely_sort") (v "0.1.6") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "1cylpm9kq31vpw05b9d4jd7493hr5ls9dmh9lfgf1xl5mfbv9r7n")))

