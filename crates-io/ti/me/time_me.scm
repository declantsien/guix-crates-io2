(define-module (crates-io ti me time_me) #:use-module (crates-io))

(define-public crate-time_me-0.1.0 (c (n "time_me") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i67syp1647i02i5ndg54ci5vsimn9ar4zpv8kxq9km7ds657f0f")))

(define-public crate-time_me-0.1.1 (c (n "time_me") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "cargo-husky") (r "^1.0") (f (quote ("run-cargo-fmt" "run-cargo-clippy"))) (d #t) (k 2)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bfcayn3dnf1124a1hhxxs52160hq5p7c6ila2m1mdzm4d5wmlhk")))

(define-public crate-time_me-0.1.2 (c (n "time_me") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "cargo-husky") (r "^1.0") (f (quote ("run-cargo-fmt" "run-cargo-clippy"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.7") (d #t) (k 0)))) (h "0scrbch8csnd8vvm6xwsg3jlkwylwi0axhz9crr6xpgha57z47ds")))

