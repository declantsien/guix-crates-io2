(define-module (crates-io ti me timeit-tool) #:use-module (crates-io))

(define-public crate-timeit-tool-0.1.3 (c (n "timeit-tool") (v "0.1.3") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0851ffmaspa65s8zd7yivc2xhmpk0wpyq0nilm8cdpzl8ip3drpc")))

(define-public crate-timeit-tool-0.1.4 (c (n "timeit-tool") (v "0.1.4") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11zqjgm4dy0wwv9xg9jnq34h2ai07pghlmimj4cx3349lrfxgqqv")))

