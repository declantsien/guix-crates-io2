(define-module (crates-io ti me timecmp) #:use-module (crates-io))

(define-public crate-timecmp-0.1.0 (c (n "timecmp") (v "0.1.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "floating-duration") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1jhyq0avi0rlm0g8pwzwanrpxpdnwlc4lrqczz62d009rgj52q9n")))

