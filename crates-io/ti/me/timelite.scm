(define-module (crates-io ti me timelite) #:use-module (crates-io))

(define-public crate-timelite-1.0.0 (c (n "timelite") (v "1.0.0") (h "0xp1ipppg3vyjnqc15avgrch9vv8zsxj17816h6sr6l2ds5naffq") (y #t)))

(define-public crate-timelite-1.0.1 (c (n "timelite") (v "1.0.1") (h "04hawmwqzw4lwpc225m0g2isa8hfmnnd4db7vs03k0ypbbb3pcn3") (y #t)))

(define-public crate-timelite-1.0.2 (c (n "timelite") (v "1.0.2") (h "0pvqcx19bcawi92hayn9z2m0l6h3683pwyi2wvi3m3hi66z1fgqm") (y #t)))

(define-public crate-timelite-1.0.3 (c (n "timelite") (v "1.0.3") (h "17ysqx33dwfm352n4xz9wj3f5v2s0v2kdfc5zrn48bripw2j59j1") (y #t)))

(define-public crate-timelite-1.0.4 (c (n "timelite") (v "1.0.4") (h "1y5dk6w6b9dy93a6frx45j1wrp6xv04pxxnmbxrd2z7j9nkgq92m") (y #t)))

