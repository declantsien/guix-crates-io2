(define-module (crates-io ti me timeskwire) #:use-module (crates-io))

(define-public crate-timeskwire-0.1.0 (c (n "timeskwire") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lbk8sd6zhdq5vr3w8aqns79n6s6mqfdcp3wpvnpxabym8lp2x4l")))

