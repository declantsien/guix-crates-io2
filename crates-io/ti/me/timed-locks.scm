(define-module (crates-io ti me timed-locks) #:use-module (crates-io))

(define-public crate-timed-locks-0.1.0 (c (n "timed-locks") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "0wabim555gy4nisylbm2p2a4gg1wnpl8zv7qq4rnq660i848w30c")))

(define-public crate-timed-locks-0.1.1 (c (n "timed-locks") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "0axy5gkxmr32p1wdy2g24l2vdicqfkzm09k7732crhp0l59bc2p7")))

