(define-module (crates-io ti me timeago-cli) #:use-module (crates-io))

(define-public crate-timeago-cli-0.1.0 (c (n "timeago-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0iq82bpxzb9xqwgfk9vn3cbcnhplkw1c18y9gp55z08m2y4jk5md")))

(define-public crate-timeago-cli-0.1.1 (c (n "timeago-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1llkcwla93bdjwgy147f8h1jfw6y69xdpbsxad86wzcqh322qqgv")))

(define-public crate-timeago-cli-0.1.2 (c (n "timeago-cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xf6qj5q4jzs2vdwdkdxwqq1c51gxjmnlhsyl5h4m3plm6qli96d")))

(define-public crate-timeago-cli-0.1.3 (c (n "timeago-cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0vjygk5j33p74i2q4bq0s7wqbqbwvaf7p0phs83kcz72khxix936")))

(define-public crate-timeago-cli-0.1.4 (c (n "timeago-cli") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0g7zkaq0x1ffkz75zikbc8ymzsip9ppsjp52l7pjklqhs1sv0b0g")))

(define-public crate-timeago-cli-0.1.5 (c (n "timeago-cli") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dxpz6syv65wwjcinm0fr214pvzz9i3w32h5dyj3vix73sgk682l")))

(define-public crate-timeago-cli-0.2.0 (c (n "timeago-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0sx81mfrp0zj78ilx3p71myn1bin97ppzg3hcckvcikc9ivs5dw0")))

(define-public crate-timeago-cli-0.3.0 (c (n "timeago-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xy4gq6hrf71gl7nr19dg041cyih1bqx0kpidnqxc9gj3h3zpj52")))

(define-public crate-timeago-cli-0.4.0 (c (n "timeago-cli") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1nx4nrflg7h5n225fxzxcjqlz3an0l1cs9n0q771fw8898crbvck")))

(define-public crate-timeago-cli-0.4.1 (c (n "timeago-cli") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "075k6c9gcnz8lr6hdn62pqx207cjffrg4015s1dp339ssc50q4q2")))

(define-public crate-timeago-cli-0.4.2 (c (n "timeago-cli") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1r0wgxd2dxqgdpwwg1d1fansc2awnby5bhzm97h3jfv5p5cgng0z")))

(define-public crate-timeago-cli-0.4.3 (c (n "timeago-cli") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1pvs4f9g22lj8yk6fx9m3zgs1pqqk8wr78219nayz5bqdxw92dzp")))

(define-public crate-timeago-cli-0.5.0 (c (n "timeago-cli") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "17yaba7l60zsl6qyrd28wslpd9rqb9q4jwiihgn33hbqyafw7851")))

