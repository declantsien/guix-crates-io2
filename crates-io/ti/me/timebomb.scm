(define-module (crates-io ti me timebomb) #:use-module (crates-io))

(define-public crate-timebomb-0.1.0 (c (n "timebomb") (v "0.1.0") (d (list (d (n "pulse") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0c50mna9ar3vp96kvcn9yq7zl3pr8z07bw2c1fkr2chv7rvxap28")))

(define-public crate-timebomb-0.1.1 (c (n "timebomb") (v "0.1.1") (d (list (d (n "pulse") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0zl0jxvwmkwf23dw9w5rmfwa879bgbraj9b4w780bhdh5hppygmb")))

(define-public crate-timebomb-0.1.2 (c (n "timebomb") (v "0.1.2") (d (list (d (n "pulse") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0fl8bxi9bf5bv44i1afii63695cx4jlki869v0kp01ipnvs8c23z")))

