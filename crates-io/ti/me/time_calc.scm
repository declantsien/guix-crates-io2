(define-module (crates-io ti me time_calc) #:use-module (crates-io))

(define-public crate-time_calc-0.9.0 (c (n "time_calc") (v "0.9.0") (h "0k3656r2b5rxfl8q6m45k0x4rd9hr01s0pvvxizmk18xr5ma8cxa")))

(define-public crate-time_calc-0.9.1 (c (n "time_calc") (v "0.9.1") (h "1i67krzxy3hcb8dvbfclprdpfa8qmi8cm0yihivr69a2phivaqqb")))

(define-public crate-time_calc-0.9.2 (c (n "time_calc") (v "0.9.2") (h "1jf0bszlfkpxw9z9fz686x7kgddqp4rv036zscvjffhd1w4h1phr")))

(define-public crate-time_calc-0.9.3 (c (n "time_calc") (v "0.9.3") (h "080na2gbjfggwkaf1zq8fzj3s7w9axpnzxxiqppbkb609c8a9gaw")))

(define-public crate-time_calc-0.9.4 (c (n "time_calc") (v "0.9.4") (d (list (d (n "rustc-serialize") (r "^0.1.1") (d #t) (k 0)))) (h "1lv0rja5h3gba5qy5i88894j2ql79pbjbfcf9r4a6g6a0m4np64j")))

(define-public crate-time_calc-0.9.5 (c (n "time_calc") (v "0.9.5") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "0yz9297q3jciqmrgzrvpg6dbjc2h5gw42yl957gsxdvfbs46i8kd")))

(define-public crate-time_calc-0.9.6 (c (n "time_calc") (v "0.9.6") (d (list (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "152j2q0md81mcia3v2ps80a1f9x6q73mk8mv0vmn1x0kxriyfh19")))

(define-public crate-time_calc-0.9.7 (c (n "time_calc") (v "0.9.7") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1zff1chjz7vg8pazjrrcqymafiwggbdnq867qx68izdd17164baq")))

(define-public crate-time_calc-0.9.8 (c (n "time_calc") (v "0.9.8") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0xqy9p6v7q5gkviayasw1c0fvr4zadqw0958wplmy5z4mfc4b6gy")))

(define-public crate-time_calc-0.9.9 (c (n "time_calc") (v "0.9.9") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0hnvk789rkm2qf26x9xj9d1w8s5m20ybw0vqbifp8mf8zbhy6nhd")))

(define-public crate-time_calc-0.9.10 (c (n "time_calc") (v "0.9.10") (d (list (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "13vgsq3j2fw33r43gj0fqhm8yr6vzzrzvp0hkwj5gqr55hc0f5g5")))

(define-public crate-time_calc-0.9.11 (c (n "time_calc") (v "0.9.11") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ddvzb78c487d12bbxq35vbxj5i1kxbdyma1wamr83v6nk1km61s")))

(define-public crate-time_calc-0.9.12 (c (n "time_calc") (v "0.9.12") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "04k9czbj7zi5m5vmdalyagismgq1c1kcr2ql2lhnnwmfca71921d")))

(define-public crate-time_calc-0.9.13 (c (n "time_calc") (v "0.9.13") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1sqcwr30ak3vrvd6vkl59z6k2qak8s2s1kcw74m5nsm89y7cayl3")))

(define-public crate-time_calc-0.10.0 (c (n "time_calc") (v "0.10.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1y3zs7m7n1v9f0fpxg4xsrnpxrasc5y23c5r71jr77nzvf9hd9vl")))

(define-public crate-time_calc-0.10.1 (c (n "time_calc") (v "0.10.1") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0rfkl15iz6grvh9l98k178yqjapbzwqbswibbaq23hjziirwj78w")))

(define-public crate-time_calc-0.11.0 (c (n "time_calc") (v "0.11.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "00qggrx2hn7q8xdkyiq9w5prmmda8zmdmmcby7p49n3hjjfssp7h") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-time_calc-0.11.1 (c (n "time_calc") (v "0.11.1") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0v84zvzglkz3jaiac1qcxzc29rki3ricxrnyc53rxplyiwm91iam") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-time_calc-0.12.0 (c (n "time_calc") (v "0.12.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0z1ganx4jjkgc7g7hj31fskpj24a6s36wqhqnn5c02s85ym86xpw") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-time_calc-0.12.1 (c (n "time_calc") (v "0.12.1") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "19hqd1d45nqphgnsw4495kr54ckb92kxv1liya43sv4h7sijkf4q") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-time_calc-0.13.0 (c (n "time_calc") (v "0.13.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "10wy8g6f32ylrnmnh1jrdcf6y729shn6cjzl4v1nm3if7wqw5sl7")))

