(define-module (crates-io ti me timed_function) #:use-module (crates-io))

(define-public crate-timed_function-0.1.0 (c (n "timed_function") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0qi82v4vs5y4480v8nlj0bsjp0d8nnwj0ifispvqg855q73js5a2")))

