(define-module (crates-io ti me timeln) #:use-module (crates-io))

(define-public crate-timeln-0.1.0 (c (n "timeln") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1z7wnigf6lba6hg1k5z25hriny6gwq80cafbyib8li6mfyxlhhv4")))

(define-public crate-timeln-0.1.3 (c (n "timeln") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1ly9q1w7a343z8vv7pbiy1y0d29q29jn0fb9wqbdh50n73vwk9rr")))

(define-public crate-timeln-0.1.4 (c (n "timeln") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0rqf8rzmr7jz84hwvid9p7arl9mmmdgalhx7c6y85rnl31hxn5m2")))

