(define-module (crates-io ti me timeless-partialeq) #:use-module (crates-io))

(define-public crate-timeless-partialeq-0.1.0 (c (n "timeless-partialeq") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "09zmplimsylbdhgk8pnsi2cjbf2arxs9pfgv9i61p28k3g83l00m")))

(define-public crate-timeless-partialeq-0.1.1 (c (n "timeless-partialeq") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1cw0q61iyi5pf4k0pgypg94s2va17pjjbhk2n5w61csrv8k935nf")))

(define-public crate-timeless-partialeq-0.2.0 (c (n "timeless-partialeq") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 2)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("parsing" "derive"))) (d #t) (k 0)))) (h "18dpyzkk06yrq75y6w8x62bf10xcgnirqhj9l7kn13835ahhwafk")))

