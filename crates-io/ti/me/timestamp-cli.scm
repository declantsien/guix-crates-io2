(define-module (crates-io ti me timestamp-cli) #:use-module (crates-io))

(define-public crate-timestamp-cli-0.1.0 (c (n "timestamp-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jlvcb9spybpdf4r4cf1amh93p048qnzgcfghsknb5bhmwiw4dg8")))

