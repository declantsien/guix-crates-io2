(define-module (crates-io ti me time-protocol) #:use-module (crates-io))

(define-public crate-time-protocol-1.1.0 (c (n "time-protocol") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 2)) (d (n "eframe") (r "^0.18.0") (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 2)) (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 2)))) (h "1pnjxqf4hr2613ibjd1dnb85j323rirqk8zsifyxplg3gnr5ynvf") (y #t)))

(define-public crate-time-protocol-1.0.0 (c (n "time-protocol") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 2)) (d (n "eframe") (r "^0.18.0") (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 2)) (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 2)))) (h "0z918i07xp6amc8aaq5bg6cqzxlws4kxkg9lrbvdzmg7aiv3mr51")))

(define-public crate-time-protocol-1.1.1 (c (n "time-protocol") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 2)) (d (n "eframe") (r "^0.18.0") (d #t) (k 2)) (d (n "egui") (r "^0.18.1") (d #t) (k 2)) (d (n "image") (r "^0.24.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 2)))) (h "12q4kpmb9515panl71d2lckgc5js4k5dfn9ax9zfb1f3iqbg4nyl")))

