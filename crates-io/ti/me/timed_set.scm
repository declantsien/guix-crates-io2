(define-module (crates-io ti me timed_set) #:use-module (crates-io))

(define-public crate-timed_set-0.0.1 (c (n "timed_set") (v "0.0.1") (h "031905wq7dv2gfa03zbfxy7rpb8dmx7rhq2nmg0s76c6ilvsj3gm")))

(define-public crate-timed_set-0.0.2 (c (n "timed_set") (v "0.0.2") (h "1diamn1j6rwys3kmnby1aqsmwwhpfmhmv9paypx6712ggpm8q3ry")))

(define-public crate-timed_set-0.0.3 (c (n "timed_set") (v "0.0.3") (h "09zrrmld0g0xkw0i348vjs5kjd89vqxl5mrhkkpl7pnkvx02y4jg")))

(define-public crate-timed_set-0.0.4 (c (n "timed_set") (v "0.0.4") (h "06a3jrbr114irm7ifrffcyf83hpd8k0739ngrx43hin8k7pbhzg5")))

