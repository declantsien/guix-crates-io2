(define-module (crates-io ti me timeturner) #:use-module (crates-io))

(define-public crate-timeturner-0.1.0 (c (n "timeturner") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0m9jmay5nhgjvzy085dx37icw9d024jwma32l24ympx715vnqpj8")))

(define-public crate-timeturner-0.2.0 (c (n "timeturner") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "159aig5hfsbbgd9khp3pyjgdwh4yg073z8r9jkcn8qggivkd2wr4")))

(define-public crate-timeturner-1.0.0 (c (n "timeturner") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dh3fr3xqcscw0asypqjy705m1nmz3hds21549z8vz16nz5rzcag")))

(define-public crate-timeturner-1.0.1 (c (n "timeturner") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1pvsasxcnydi7xcgx3p7aa53kjv5l37v3aychzc4zwa9jqykhrdw")))

(define-public crate-timeturner-1.1.0 (c (n "timeturner") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0crx3mg5qcsvrair17k7a5mz21xkmfnvhj84rqw4asm2d41vji5l")))

(define-public crate-timeturner-1.2.0 (c (n "timeturner") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17bdjcz2rpdp0073472ba52w3hzx7cqcyani96a949j2k48mgccm")))

(define-public crate-timeturner-1.5.0 (c (n "timeturner") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dffxr6jj663ysxsz8mgikfm4fkdyni31yw7lyjixyf9ccdv7v8y")))

(define-public crate-timeturner-1.6.0 (c (n "timeturner") (v "1.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0z274kcfib4x1bmcpbdij8ypxggfxzhicf236sav69sccg0brf3z")))

(define-public crate-timeturner-1.7.1 (c (n "timeturner") (v "1.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h7gblzn438fj4ni5w3xzxk3fbkqzynxlk3v09k6ri41z3z5s3pb")))

