(define-module (crates-io ti me timeseries) #:use-module (crates-io))

(define-public crate-timeseries-0.1.0 (c (n "timeseries") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)))) (h "1zl9m3xvbvypxpf6gggjsbx4c1z8hhdav1z495qdxa2jxyi4d9pj")))

(define-public crate-timeseries-0.1.1 (c (n "timeseries") (v "0.1.1") (h "0ik9chv467z0icm7rvl23sfqjjqnalkshfcv1swk3rxqjqa7wz91")))

(define-public crate-timeseries-0.1.2 (c (n "timeseries") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0fa0n4cwf8czdk9zrs0piqr9cgsch2ba1r63p894kjbg3psy1q61")))

(define-public crate-timeseries-0.1.3 (c (n "timeseries") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ny1bm7ws4w9isl5f50lalrkvbw0n72any8qvzjpy1kwj5rs2cy5")))

(define-public crate-timeseries-0.1.5 (c (n "timeseries") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "01ifdd4a8y1xxz26sfvssy94n0a9fixlr49z2l51m94hkdwwwj1j")))

(define-public crate-timeseries-0.2.0 (c (n "timeseries") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.31") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1pkzd6sm25qbf9bz27gr6nydi4w2qdw16qaj8af2zr3qvm7l4xmm")))

(define-public crate-timeseries-0.2.1 (c (n "timeseries") (v "0.2.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.31") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z991n0zjdhxd0hd0hxxdkslnazq40lv28wxhc72fc86fv12ad6r")))

