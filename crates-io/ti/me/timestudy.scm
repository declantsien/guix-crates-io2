(define-module (crates-io ti me timestudy) #:use-module (crates-io))

(define-public crate-timestudy-0.1.0 (c (n "timestudy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1h5bc2yc4p3ji11p5bh49gq75r95fxnm75xk3fgqg5l477673vp3")))

(define-public crate-timestudy-0.2.0 (c (n "timestudy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "177329k4f79xzclz8ipizjb7jql73irs3xq0z2zjcmwr7027ksf5")))

(define-public crate-timestudy-0.3.0 (c (n "timestudy") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qmkkb9d39qr10ayqb2qngn1syvadbbyp0nmqx7ibnvch17llc1c")))

(define-public crate-timestudy-0.4.0 (c (n "timestudy") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00jshr48rqx6ypmgxr33vbgvy3fvm6ldlpgcsdh5wm4lrz6jj2m8")))

(define-public crate-timestudy-0.5.0 (c (n "timestudy") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09015cvh3gs1h3zzc6j1lybihpi9aq44zqhhgllh847dpliqvrd8") (f (quote (("inttests"))))))

(define-public crate-timestudy-0.6.1 (c (n "timestudy") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zv6mkgd3vi1lx4wj1k3vag3f4mc5bj5iprwzq80r04jgcsniy9g") (f (quote (("inttests"))))))

(define-public crate-timestudy-0.7.0 (c (n "timestudy") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0674ppxnhkm2zrdjr6xz3irqizz46svpx8w4rpq2amha5w5plx7q") (f (quote (("inttests"))))))

(define-public crate-timestudy-0.7.1 (c (n "timestudy") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06l6gp8nq6f4a43mavdfq2dfk8wb30a8bsn1s68rb903xb2m5nkb") (f (quote (("inttests"))))))

(define-public crate-timestudy-0.7.2 (c (n "timestudy") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gb0dbhgvrjg30rrkx8snww8csh34bb6vdyzw9plbcx3i1hwydw8") (f (quote (("inttests"))))))

(define-public crate-timestudy-0.7.3 (c (n "timestudy") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w555z6hpli8r99fkz3rjwfm48b9ih5q9xdfjrsdlhzsnqkj6pci") (f (quote (("test-utils"))))))

(define-public crate-timestudy-0.8.0 (c (n "timestudy") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gjcx8nd61hz9xbn8sd1f77c02qnfz966lmfmwkc8m3rycp06b7k") (f (quote (("test-utils"))))))

(define-public crate-timestudy-0.9.0 (c (n "timestudy") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mll313z499s3n7x7csl8n3l1fz6p6ibl8va8aamgf0cqmmyas4v") (f (quote (("test-utils"))))))

(define-public crate-timestudy-0.10.0 (c (n "timestudy") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05mv372qzdg2aa708f8wfim6p3synb7sp8fr4fw4l3gn421b0bza") (f (quote (("test-utils"))))))

(define-public crate-timestudy-0.10.1 (c (n "timestudy") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dk3gj1si5qv1gwjfcpv4dg8r7xb0x18jzgvxcz1a569vf5di181") (f (quote (("test-utils")))) (y #t)))

(define-public crate-timestudy-0.10.2 (c (n "timestudy") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19lp0ikp69xx97vj4dzg2pzm9kmjpfcn7y5829d0p0ima3sxmfb8") (f (quote (("test-utils")))) (y #t)))

(define-public crate-timestudy-0.10.3 (c (n "timestudy") (v "0.10.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jx60a4r0dd1sd1rrqb5wqgrp0yhi6fg32mvidwc3wl7pslr82nn") (f (quote (("test-utils"))))))

(define-public crate-timestudy-0.10.4 (c (n "timestudy") (v "0.10.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hqg0lhfrhprnsrr5yls5qf5zps0qki5wkx8lz75v9mcsrrwlib7") (f (quote (("test-utils"))))))

