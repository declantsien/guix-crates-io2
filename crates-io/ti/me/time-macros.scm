(define-module (crates-io ti me time-macros) #:use-module (crates-io))

(define-public crate-time-macros-0.0.0-alpha.0 (c (n "time-macros") (v "0.0.0-alpha.0") (h "0328ax45ps7wrvqmqh6dj815vs557mqjdcjjddwml967xivq70ic") (y #t)))

(define-public crate-time-macros-0.1.0 (c (n "time-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "time-macros-impl") (r "^0.1") (d #t) (k 0)))) (h "0bdbxjgbxb81xgy08h5dh4qvwy95sy9x8g1y31g11g4my3lvdscs")))

(define-public crate-time-macros-0.1.1 (c (n "time-macros") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "time-macros-impl") (r "^0.1") (d #t) (k 0)))) (h "1wg24yxpxcfmim6dgblrf8p321m7cyxpdivzvp8bcb7i4rp9qzlm")))

(define-public crate-time-macros-0.2.0-alpha.0 (c (n "time-macros") (v "0.2.0-alpha.0") (d (list (d (n "standback") (r "^0.3.1") (f (quote ("msrv-1-46"))) (k 0)))) (h "1c21dbx36ccfz8d1c7b5b5hwiardfiypgm847pgr2l1fhdiir1q6") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.0-alpha.1 (c (n "time-macros") (v "0.2.0-alpha.1") (h "0rglr47jmnjk52d93b59mlxgc8d4dd17f3y371nw773lkwa4n8rn") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.0 (c (n "time-macros") (v "0.2.0") (h "0wlw96ddypbq9dd7rhmap4cb3mp8vjhd8g0ps3zm4ks5jlnqqx8h") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.1 (c (n "time-macros") (v "0.2.1") (h "0hr8k0ky8p467scjgj1yqmwwfcfzwfh2j4xmlf06ha82c10m7884") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.2 (c (n "time-macros") (v "0.2.2") (h "1nszl1kchvqg3zapwnjpnk8vpi3cz7mk3cc80wpiabcqh5gcmivj") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.3 (c (n "time-macros") (v "0.2.3") (h "1mj7pv8y9j2csrh1l8aabras36pgysbnfy18330srh4g8sihrsr5") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.4 (c (n "time-macros") (v "0.2.4") (h "14h712p63k121cwi80x8ydn99k703wkcw2ksivd7r0addwd7nra2") (f (quote (("large-dates"))))))

(define-public crate-time-macros-0.2.5 (c (n "time-macros") (v "0.2.5") (d (list (d (n "time-core") (r "=0.1.0") (d #t) (k 0)))) (h "0nqff5j5170llixk05vb3x76xri63x9znavxmrica4nq64c81fv5") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.60.0")))

(define-public crate-time-macros-0.2.6 (c (n "time-macros") (v "0.2.6") (d (list (d (n "time-core") (r "=0.1.0") (d #t) (k 0)))) (h "1chnpb27nishwa4rn4acr2l9ha5wxqw2dikmqnay99scafgzjryr") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.60.0")))

(define-public crate-time-macros-0.2.7 (c (n "time-macros") (v "0.2.7") (d (list (d (n "time-core") (r "=0.1.0") (d #t) (k 0)))) (h "0z4bsdw11cghv3rpy65qdrgmmkvgslf5zq0y3qw0zjvdvswawq54") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.62.0")))

(define-public crate-time-macros-0.2.8 (c (n "time-macros") (v "0.2.8") (d (list (d (n "time-core") (r "=0.1.0") (d #t) (k 0)))) (h "0dkgswnm5bj88wi80p94c9808dbdzis363fnwnw19a0xwxbsd07x") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.63.0")))

(define-public crate-time-macros-0.2.9 (c (n "time-macros") (v "0.2.9") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "16yh6g3k9rcksa94jyakvwkp6vcy7hldf4g2vcwby1sz1aa50a9p") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.65.0")))

(define-public crate-time-macros-0.2.10 (c (n "time-macros") (v "0.2.10") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "1i566vxybz24i2rdax2d0m86fk2m45rarrapnxk6gj7kjyl1bfln") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.65.0")))

(define-public crate-time-macros-0.2.11 (c (n "time-macros") (v "0.2.11") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "1kcjzd32prdqg0lda041qihl7483grsrggymiw3vnf8nk4f52wgb") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.12 (c (n "time-macros") (v "0.2.12") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "0lbldy232ag8ganwn492v70rp3livjqix947v44lhfkbxmlm9ikm") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.13 (c (n "time-macros") (v "0.2.13") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "1yg8wgynbh663y2rpijhjd49q2vvs0q55dwl5cwksc79aa3jagbk") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.14 (c (n "time-macros") (v "0.2.14") (d (list (d (n "time-core") (r "=0.1.1") (d #t) (k 0)))) (h "0wn52hwaq1hy4r5yijzkdi4j40zvqapbpcjsjdkyyy4l6d22z50s") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.15 (c (n "time-macros") (v "0.2.15") (d (list (d (n "time-core") (r "=0.1.2") (d #t) (k 0)))) (h "086frcn3m2yh3fji3wv9r35c3zjxjq8ngam7xnngiqd9vdl0vmsa") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.16 (c (n "time-macros") (v "0.2.16") (d (list (d (n "time-core") (r "=0.1.2") (d #t) (k 0)))) (h "0gx4ngf5g7ydqa8lf7kh9sy72rd4dhvpi31y1jvswi0288rpw696") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.17 (c (n "time-macros") (v "0.2.17") (d (list (d (n "num-conv") (r "^0.1.0") (d #t) (k 0)) (d (n "time-core") (r "=0.1.2") (d #t) (k 0)))) (h "0x3pahhk2751c6kqqq9dk6lz0gydbnxr44q01wpjlrz687ps78vv") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

(define-public crate-time-macros-0.2.18 (c (n "time-macros") (v "0.2.18") (d (list (d (n "num-conv") (r "^0.1.0") (d #t) (k 0)) (d (n "time-core") (r "=0.1.2") (d #t) (k 0)))) (h "1kqwxvfh2jkpg38fy673d6danh1bhcmmbsmffww3mphgail2l99z") (f (quote (("serde") ("parsing") ("large-dates") ("formatting")))) (r "1.67.0")))

