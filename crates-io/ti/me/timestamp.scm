(define-module (crates-io ti me timestamp) #:use-module (crates-io))

(define-public crate-timestamp-0.1.0 (c (n "timestamp") (v "0.1.0") (d (list (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "11b7g4bx0rlf311z0fri0zwn470qwyw4ajimnv1y52cgaixlnlvj")))

(define-public crate-timestamp-0.1.1 (c (n "timestamp") (v "0.1.1") (d (list (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0hpxjx55p5nnpkpw64vgv14y1751dwbmqdccc533pvczh5rl6gr0")))

