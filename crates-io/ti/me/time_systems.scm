(define-module (crates-io ti me time_systems) #:use-module (crates-io))

(define-public crate-time_systems-0.1.0 (c (n "time_systems") (v "0.1.0") (h "04k976p0bkp3dh2q9ph8jj141c2ygyfzg5ppfxzr9bfd9pf9lavq")))

(define-public crate-time_systems-0.1.1 (c (n "time_systems") (v "0.1.1") (h "0imlr118b86b6b0r1xb87ixc6f9m5pbhna8r47vm6xyz39g0mgdw")))

