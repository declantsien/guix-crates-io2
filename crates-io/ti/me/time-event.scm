(define-module (crates-io ti me time-event) #:use-module (crates-io))

(define-public crate-time-event-0.0.1 (c (n "time-event") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1xawi1l6gqqyl4sijcm3r5qz668da66y8df13kc81pj0ag9xw0rl")))

