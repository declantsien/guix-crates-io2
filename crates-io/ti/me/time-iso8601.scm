(define-module (crates-io ti me time-iso8601) #:use-module (crates-io))

(define-public crate-time-iso8601-0.1.0 (c (n "time-iso8601") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("serde" "formatting" "macros"))) (d #t) (k 0)))) (h "084x68lfmcwhidchg07cnyqappmq7jcrdgg2xv64knaqgsdpch27")))

