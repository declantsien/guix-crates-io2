(define-module (crates-io ti me time-graph-macros) #:use-module (crates-io))

(define-public crate-time-graph-macros-0.1.0 (c (n "time-graph-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16cqkpzr26jqq6qil74ngy3b8dqb11jwqjhck5sgjfcj7mhw8d5p")))

(define-public crate-time-graph-macros-0.3.0 (c (n "time-graph-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r9w3s4vzw88xk9n15kpam80n2354zkyzrsc1d2pm1q4l86hpfbm")))

