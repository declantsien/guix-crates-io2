(define-module (crates-io ti me time-humanize) #:use-module (crates-io))

(define-public crate-time-humanize-0.1.0 (c (n "time-humanize") (v "0.1.0") (h "1fk4n0np2prllqs78lav8dhplm8a2jr3xm62k8b5wf4ylbfwd57a")))

(define-public crate-time-humanize-0.1.1 (c (n "time-humanize") (v "0.1.1") (h "03xbrlspbzsw67mhzydggfrbvwcsdzdan4jxx892v9yr8hd3ljsp")))

(define-public crate-time-humanize-0.1.2 (c (n "time-humanize") (v "0.1.2") (d (list (d (n "time") (r "^0.3.5") (f (quote ("formatting"))) (o #t) (d #t) (k 0)))) (h "12mrdv7j6nb4cb6krf727f93h4wkb1vxgcjqrxfdnm1nyp6lah09")))

(define-public crate-time-humanize-0.1.3 (c (n "time-humanize") (v "0.1.3") (d (list (d (n "time") (r "^0.3.5") (f (quote ("formatting"))) (o #t) (d #t) (k 0)))) (h "0is6k8kjjc8mhi37mc52zdqmpm0rf6i40kj9sny01hgpnhcx0ciy")))

