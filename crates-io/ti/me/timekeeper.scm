(define-module (crates-io ti me timekeeper) #:use-module (crates-io))

(define-public crate-timekeeper-0.1.0 (c (n "timekeeper") (v "0.1.0") (h "04p8xh4npin2lq2p2rbhgrclc51nhnpxi5a1nwmmq86srvz0hbiw")))

(define-public crate-timekeeper-0.1.1 (c (n "timekeeper") (v "0.1.1") (h "059440ckdgzpaazan2pi9vhafaxaj50yzx0iy4nyjaa9y1ny2ndr")))

(define-public crate-timekeeper-0.1.2 (c (n "timekeeper") (v "0.1.2") (h "1sbnyk0dlhalwwfafqm1wg0kxkkl8065xpylwidvf3rp81qhq7zj")))

(define-public crate-timekeeper-0.2.0 (c (n "timekeeper") (v "0.2.0") (h "0b4jpi63d9wr9khsxnj6q1xkdbqfzlfxg35bi14jqcwwpp4ng8sb")))

(define-public crate-timekeeper-0.2.1 (c (n "timekeeper") (v "0.2.1") (h "0xicq3kn85i66j8sgclmxzmbwn3f1jf3bayjnzsl0x9rsxxaylrl")))

(define-public crate-timekeeper-0.2.2 (c (n "timekeeper") (v "0.2.2") (h "0jx2k7kl8j7mb0p80yjjk40cw9lwmicv1jc3d843irh6d09b73n2")))

(define-public crate-timekeeper-0.2.3 (c (n "timekeeper") (v "0.2.3") (h "0kfprc6vnmcrxv4820q80lqld77crh7va7n35rfw12an2912j8al") (f (quote (("disable_timekeeper"))))))

(define-public crate-timekeeper-0.2.4 (c (n "timekeeper") (v "0.2.4") (h "1lbd687nwsb79lgw4jbfc5k9nmqkgvca5cymc5ywd6krmdqiyczy") (f (quote (("enable_timekeeper") ("default" "enable_timekeeper"))))))

(define-public crate-timekeeper-0.3.0 (c (n "timekeeper") (v "0.3.0") (d (list (d (n "libc") (r ">= 0.2.32") (d #t) (k 0)))) (h "1rxw0kjzwmi2fwahkan33sbdgdixaa61xza7nw9ah86n0619pfx5") (f (quote (("enable_timekeeper") ("default" "enable_timekeeper"))))))

(define-public crate-timekeeper-0.3.1 (c (n "timekeeper") (v "0.3.1") (d (list (d (n "libc") (r ">= 0.2.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12hwbbb7jxg95mbiw26mq2j7wkxalg50ilm9inwq46pjs7dwb060") (f (quote (("enable_timekeeper") ("default" "enable_timekeeper"))))))

(define-public crate-timekeeper-0.3.2 (c (n "timekeeper") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qhh73x8arpmkndr8p9whgqz1lzd02w5rvbqmvndm1gqpj1rp8gl") (f (quote (("enable_timekeeper") ("default" "enable_timekeeper"))))))

