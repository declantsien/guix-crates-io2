(define-module (crates-io ti me time-condition) #:use-module (crates-io))

(define-public crate-time-condition-0.1.0 (c (n "time-condition") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "evalexpr") (r "^11.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1f0ybypk6s0y98f9h4nnkx6wyfk155v2c8zwr33id6mw9d3vjql2") (r "1.67")))

