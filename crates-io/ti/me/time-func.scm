(define-module (crates-io ti me time-func) #:use-module (crates-io))

(define-public crate-time-func-0.1.0 (c (n "time-func") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mindtree_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_closure") (r "^0.3.2") (d #t) (k 0)))) (h "15n8nx2cci518p4bw80zsbxn8j7pmkrribhm0ypbffjjpyjpmaba")))

(define-public crate-time-func-0.1.1 (c (n "time-func") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mindtree_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_closure") (r "^0.3.2") (d #t) (k 0)))) (h "1np6l6mpvr4bvl0d7frb4l14rwxqcw98xy0wsy2vd5gppw9x00m8")))

(define-public crate-time-func-0.1.2 (c (n "time-func") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mindtree_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_closure") (r "^0.3.2") (d #t) (k 0)))) (h "1nhz0c9ak1q05pr0grc01dd2mxlanghxp3kqdbi3ivhy52zxlf30")))

(define-public crate-time-func-0.1.3 (c (n "time-func") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mindtree_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_closure") (r "^0.3.2") (d #t) (k 0)))) (h "11f0mqis9wq6igrflxm3xnpir3f2vprmxc400dwp67a9gxhnzj23")))

(define-public crate-time-func-0.1.4 (c (n "time-func") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mindtree_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_closure") (r "^0.3.2") (d #t) (k 0)))) (h "01wj08mnjs0sr25b9r9giwfwvmal4fx5bbjgjf9nmrav54pbj2pi")))

