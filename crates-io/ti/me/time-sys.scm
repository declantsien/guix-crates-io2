(define-module (crates-io ti me time-sys) #:use-module (crates-io))

(define-public crate-time-sys-0.0.0 (c (n "time-sys") (v "0.0.0") (d (list (d (n "linux-api") (r "^0.0.1") (d #t) (k 0)))) (h "13ifvnyarrhap53f6wa3lkxyd1xkjqrldw2ljw6dm2da6br2gamn")))

(define-public crate-time-sys-0.0.1 (c (n "time-sys") (v "0.0.1") (d (list (d (n "linux-api") (r ">= 0.0.2") (d #t) (k 0)))) (h "0jj608rrdm8dr7a88hmvap6v0s27z7sjyk8hf6frppjb9ka00x1r")))

(define-public crate-time-sys-0.0.2 (c (n "time-sys") (v "0.0.2") (d (list (d (n "linux-api") (r ">= 0.0.2") (d #t) (k 0)))) (h "0hwpc7vmmd5gqszziah3gyhmcbmbbsq2k5zz5pl1fypgczi3vxfx")))

