(define-module (crates-io ti me timevec) #:use-module (crates-io))

(define-public crate-timevec-0.1.0 (c (n "timevec") (v "0.1.0") (h "146j5kc1inmdg7znhbn30y12m5icrxrackj0ypp58cdy5h69wd88")))

(define-public crate-timevec-0.2.0 (c (n "timevec") (v "0.2.0") (h "1lgzici781gm675bsdak6118qpyvrl1qhj664vxmf32zp64148xb")))

