(define-module (crates-io ti me timemoji) #:use-module (crates-io))

(define-public crate-timemoji-0.1.0 (c (n "timemoji") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1giwcsp0kgi50d2zkcblrlwyzdmna6ynq2sc9kb3cqzi6z5j505w")))

(define-public crate-timemoji-0.2.0 (c (n "timemoji") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1aahl5m0czdh21rwm19glyc4z8p6ccll4shv6k1k2y0l8a8cbri9")))

