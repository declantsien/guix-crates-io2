(define-module (crates-io ti me time2) #:use-module (crates-io))

(define-public crate-time2-0.1.0 (c (n "time2") (v "0.1.0") (d (list (d (n "muldiv") (r "^0.1") (d #t) (k 0)))) (h "1cqivlkm72kjfkinrxb100rydy33l3gq4aqv3hdfbimjs9nr02w0") (f (quote (("beta"))))))

(define-public crate-time2-0.2.0 (c (n "time2") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1yh45a8plm0djj00kp0q2jf435abg7qkbz49gambncx7b7qdlylr") (f (quote (("beta"))))))

