(define-module (crates-io ti me timescan) #:use-module (crates-io))

(define-public crate-timescan-0.1.0 (c (n "timescan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0kcmhc4zhzp0nm56p231z5dgaf2k5wm8n3mx75c93k25a0wkh730")))

(define-public crate-timescan-0.1.1 (c (n "timescan") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "104a8jsjp4g35nckb7sc3pxz6l96aprszigbj0bfdr5y7bhflcgj")))

(define-public crate-timescan-0.1.2 (c (n "timescan") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "05dz1mcc8m5za06i1lmssvlz07g6ihl55cq6i4p4kzhykdj3j78g")))

