(define-module (crates-io ti me timerfd) #:use-module (crates-io))

(define-public crate-timerfd-0.1.0 (c (n "timerfd") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "00idrcf9yqcifwc4494p7a4m9zg1sgk5jpl13s9yxdvxq9yjnkzh")))

(define-public crate-timerfd-0.2.0 (c (n "timerfd") (v "0.2.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0jwv3k3drbxj9px9z4rrggxmqfr6r6m90vn3g325z3inqkffq3hf")))

(define-public crate-timerfd-1.0.0 (c (n "timerfd") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gmbls00azx7ms474ngn5av7afch6cxaj55fnp83kyj6wli7i6kc")))

(define-public crate-timerfd-1.1.0 (c (n "timerfd") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)))) (h "0nimc4yfpq58ayk17wp951pjsngay13wpg7wzn9r33a76h9920zx")))

(define-public crate-timerfd-1.1.1 (c (n "timerfd") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)))) (h "0gd9am1k8sr6pwxrddnry59fhd1s3mhya6j0b612ksq2najvn8q1")))

(define-public crate-timerfd-1.2.0 (c (n "timerfd") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)))) (h "14gwkm2c38bm76ccpp4g20qqs77h86d1l81594i76pb751k3xd8b")))

(define-public crate-timerfd-1.3.0 (c (n "timerfd") (v "1.3.0") (d (list (d (n "rustix") (r "^0.34.2") (d #t) (k 0)))) (h "0gz3yx5l0b3cryczzlmj0pqphk39b6iz4n9gjlv733jvjry5my19")))

(define-public crate-timerfd-1.4.0 (c (n "timerfd") (v "1.4.0") (d (list (d (n "rustix") (r "^0.36.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0gs86d5h2caiykfdcnbyq2z2ljaz1z2n4nx27khbpwi5z9p96r06")))

(define-public crate-timerfd-1.4.1 (c (n "timerfd") (v "1.4.1") (d (list (d (n "rustix") (r "^0.37.0") (f (quote ("time"))) (d #t) (k 0)))) (h "0whm4yiinc14i1r50psn0x4b4ki3899iz5s6zq9fljynxx7c7gi8")))

(define-public crate-timerfd-1.5.0 (c (n "timerfd") (v "1.5.0") (d (list (d (n "rustix") (r "^0.37.0") (f (quote ("time"))) (d #t) (k 0)))) (h "18jf23sh843nfvvcdg5gwmnfy099f6fhpnz8w9xmq2xdhdyx8grx")))

(define-public crate-timerfd-1.6.0 (c (n "timerfd") (v "1.6.0") (d (list (d (n "rustix") (r "^0.38.0") (f (quote ("std" "time"))) (k 0)))) (h "148ha49mzc1jg1bgq6cjkdggvnczbdp4f3spicnglzngd3iq5r44")))

