(define-module (crates-io ti me timeout_io) #:use-module (crates-io))

(define-public crate-timeout_io-0.2.0 (c (n "timeout_io") (v "0.2.0") (d (list (d (n "etrace") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.2.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.1") (d #t) (k 0)))) (h "0yh9avafl5hi3nrwgb214dp3b240552njls40af8gdl8pf3z977n") (y #t)))

(define-public crate-timeout_io-0.2.1 (c (n "timeout_io") (v "0.2.1") (d (list (d (n "etrace") (r "^1.0.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.2.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.1") (d #t) (k 0)))) (h "04vzzqxpy3cmb32725y3kihd10j8lx9ksm3z73d8wh7ndj537x3z") (y #t)))

(define-public crate-timeout_io-0.2.2 (c (n "timeout_io") (v "0.2.2") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.2.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "1a7w01cip7bpz31qqy2rfbyi5f6v6f7r45z5x6prakd7znl25pfr") (y #t)))

(define-public crate-timeout_io-0.2.3 (c (n "timeout_io") (v "0.2.3") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.2.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "0xpx7bipb0ic5bwkc7vqvs2z0rn3ajxhphavgqri19h86k0938by") (y #t)))

(define-public crate-timeout_io-0.2.4 (c (n "timeout_io") (v "0.2.4") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "1xx4hmgf9bp5nmbrp20l4hmjhr1lx8d85qds3qywll78062h9jsi") (y #t)))

(define-public crate-timeout_io-0.2.5 (c (n "timeout_io") (v "0.2.5") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "04bgls2f8mlqvxn45wfn1v1dyv51zmx76al0gl7x2mz3pgdcqcdi") (y #t)))

(define-public crate-timeout_io-0.2.6 (c (n "timeout_io") (v "0.2.6") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "slice_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "0z3b45r28vwxbwbsgyq2nbpymvwq3756wi9sbrgalcl0dmsvwwx1") (y #t)))

(define-public crate-timeout_io-0.2.7 (c (n "timeout_io") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "etrace") (r "^1.1.1") (d #t) (k 0)) (d (n "slice_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "tiny_future") (r "^0.3.2") (d #t) (k 0)))) (h "00c04kqwzpm0xf1mcj2fa83g64hna2da4m6w34g96ihxbplm61s5") (y #t)))

(define-public crate-timeout_io-0.3.0 (c (n "timeout_io") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0cg0aw2kp203xazy1i7097p2d4yvhgzi7p6dwrix93iy77666blf") (y #t)))

(define-public crate-timeout_io-0.4.0 (c (n "timeout_io") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1v5hwaj3kyq18z0mm5rylk72q4lmc64qjl8sbp7w94i2grpfydms") (y #t)))

(define-public crate-timeout_io-0.4.1 (c (n "timeout_io") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "1qqrzf4mb92hgw31vi030xzljg72gwsa8i1dzaiipbnkiy3k1v4g") (y #t)))

(define-public crate-timeout_io-0.4.2 (c (n "timeout_io") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)))) (h "11b1xs0mmllnis2sp5xsqbz71v0c8ljn5ybjgglymnwkxqhz4jzl") (y #t)))

(define-public crate-timeout_io-0.5.0 (c (n "timeout_io") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04x2ghz1ccfdk0yha8px7lkmkjjlk8lwdmsmdnjvbapk35mgs1sl") (y #t)))

(define-public crate-timeout_io-0.6.0 (c (n "timeout_io") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q2w2ndd39rxg24k97wsk3cafnxdq5z787zk1rvyw1r5z4lyv29d") (y #t)))

