(define-module (crates-io ti me timeouts) #:use-module (crates-io))

(define-public crate-timeouts-0.1.0 (c (n "timeouts") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "os_clock") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "macros" "sync" "rt-threaded"))) (d #t) (k 0)))) (h "1v6qhx74h3g14lghpr5l2syq83khcj5v23jkyjmf0yrzql39q3cf")))

