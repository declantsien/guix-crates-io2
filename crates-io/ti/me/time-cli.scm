(define-module (crates-io ti me time-cli) #:use-module (crates-io))

(define-public crate-time-cli-0.1.0 (c (n "time-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1m3dvg48q97yszz9d58fmmbcaqzs6s09jggl9jmfvxnfivf4178j")))

(define-public crate-time-cli-0.1.1 (c (n "time-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "01g6rbz68s22y3ixvx06z6fsyk4sl401pw24lk960bdls4d4fh2n")))

