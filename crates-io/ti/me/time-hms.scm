(define-module (crates-io ti me time-hms) #:use-module (crates-io))

(define-public crate-time-hms-0.1.0 (c (n "time-hms") (v "0.1.0") (h "17jngry38iwc9zl94l8k00hfqhp0mlv2qz2ijpy15yi5n92gmsms")))

(define-public crate-time-hms-0.1.1 (c (n "time-hms") (v "0.1.1") (h "0mmxi633qlid1frbps94ml9s812jcy8pyb5x1724gxd6jsm00g99")))

(define-public crate-time-hms-0.1.2 (c (n "time-hms") (v "0.1.2") (h "1pxqbb2r68gvyri713gkdsll6z46cid5b9ms221q7l7g8bbaaasm")))

(define-public crate-time-hms-0.1.3 (c (n "time-hms") (v "0.1.3") (h "0kxrminifq4zgbsrpbnxnbb58ikhnbnwq82viydzb8vm2089d4vr")))

(define-public crate-time-hms-0.2.1 (c (n "time-hms") (v "0.2.1") (h "1y2lznjh8nc9c9cj6z84zq0lv4xnb8rd0w3xb0hhdm0jr70byv69")))

(define-public crate-time-hms-0.3.0 (c (n "time-hms") (v "0.3.0") (h "0jmdlzhkmlx6pzyqddyx7x4vl7pk0ha6glnpc953v9wzzh2z6sbn")))

(define-public crate-time-hms-0.3.1 (c (n "time-hms") (v "0.3.1") (h "1frxcd0y7q4nq7lziiqyskwixk2rvgbviyamgmx8s4bdjpl8dbir")))

