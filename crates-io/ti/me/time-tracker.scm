(define-module (crates-io ti me time-tracker) #:use-module (crates-io))

(define-public crate-time-tracker-0.1.0 (c (n "time-tracker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rev_lines") (r "^0.2.1") (d #t) (k 0)))) (h "1cbrfyfbjxl5i60xq39v330w7fm4bs3aixzcsmxrbzrdsv6z8c90")))

