(define-module (crates-io ti da tidal) #:use-module (crates-io))

(define-public crate-tidal-0.1.0 (c (n "tidal") (v "0.1.0") (d (list (d (n "hyper") (r "^0.11.27") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.18") (d #t) (k 0)) (d (n "tidalcache") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6.5") (d #t) (k 0)))) (h "0lqmsfrfyxncb85v4cmxshrjndkqz5gv4qfi2ndiy44xnyb87fjk")))

