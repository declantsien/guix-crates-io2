(define-module (crates-io ti vi tivilsta) #:use-module (crates-io))

(define-public crate-tivilsta-0.1.0 (c (n "tivilsta") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1l2sck9g2xg7yl0haxwbzviaakmjcah4l4gqvjkzqkda9vgq8g65")))

(define-public crate-tivilsta-0.2.0 (c (n "tivilsta") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0ssafprw23igfm2qp53nrppdyl8a5j2zp6iq7zj5fn23b98pfk86")))

(define-public crate-tivilsta-0.3.0 (c (n "tivilsta") (v "0.3.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "16kyv045xwilnpankxw3y3k2izkrkbv17r7nszc774by7wi4d6hw")))

(define-public crate-tivilsta-0.4.0 (c (n "tivilsta") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "urlparse") (r "^0.7") (d #t) (k 0)))) (h "0ynzcc5dryd4a3b97vs64rkj0k2cj834s7xxdknp5c2crxnj9i5r")))

(define-public crate-tivilsta-0.5.0 (c (n "tivilsta") (v "0.5.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "idna") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "urlparse") (r "^0.7") (d #t) (k 0)))) (h "04xz3zp9l9jk51mibzqfmn4z1w504hfd8z3nazrbrgfns8b43xp2")))

