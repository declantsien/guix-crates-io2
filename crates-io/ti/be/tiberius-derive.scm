(define-module (crates-io ti be tiberius-derive) #:use-module (crates-io))

(define-public crate-tiberius-derive-0.0.1 (c (n "tiberius-derive") (v "0.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tiberius") (r "^0.7.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0g7yk7pqilfdliv9k2rf241iy0j2kfsq0qv3vjxvf2kwzj3kqcyg")))

(define-public crate-tiberius-derive-0.0.2 (c (n "tiberius-derive") (v "0.0.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tiberius") (r "^0.7.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "13rhahm6l1by0b5fjxx6z43n2hl8pknhnw17hwzzjpgq453ag5w0")))

