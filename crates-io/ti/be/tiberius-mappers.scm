(define-module (crates-io ti be tiberius-mappers) #:use-module (crates-io))

(define-public crate-tiberius-mappers-0.1.0 (c (n "tiberius-mappers") (v "0.1.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1863fqc12msirczmbwyv1450fw9glzlq86jsm9rkdqccggjv9s32")))

(define-public crate-tiberius-mappers-0.2.0 (c (n "tiberius-mappers") (v "0.2.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.2.0") (d #t) (k 0)))) (h "134m7rwc4rgbnn1bk7a06clyid9pm6nsrvfxna6qi1nvp1kqplv1")))

(define-public crate-tiberius-mappers-0.3.0 (c (n "tiberius-mappers") (v "0.3.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.3.0") (d #t) (k 0)))) (h "110mjdc002py6czign2c4ipzqr6z43pk5kkkimqqvyzsz11fnyp3")))

(define-public crate-tiberius-mappers-0.4.0 (c (n "tiberius-mappers") (v "0.4.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1qk1kp68ysqlxn7h1dk1w6nm4474s38y8hnggb5b0mbqzgjpj3ra")))

(define-public crate-tiberius-mappers-0.5.0 (c (n "tiberius-mappers") (v "0.5.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.5.0") (d #t) (k 0)))) (h "0kar5zpd42ilsm768y5sq982l2lk5pki7zx227cmqw8f7w3skjyq")))

(define-public crate-tiberius-mappers-0.6.0 (c (n "tiberius-mappers") (v "0.6.0") (d (list (d (n "tiberius") (r "^0.12.2") (k 0)) (d (n "tiberius-mappers-derive") (r "^0.6.0") (d #t) (k 0)))) (h "05rqrhpxyfn0mwvdiyaxj80c26c7bijy6g1237dz25c15qncsd28")))

