(define-module (crates-io ti be tiberius-mappers-derive) #:use-module (crates-io))

(define-public crate-tiberius-mappers-derive-0.1.0 (c (n "tiberius-mappers-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1qqf5ffgiyqkg0ipdlkmzd14a9bx7cmjrklpfs5vvfdlbyw4wnlq")))

(define-public crate-tiberius-mappers-derive-0.2.0 (c (n "tiberius-mappers-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1qhxi7j53m5d2ay4jhzcjcy9y4hnn8kq2shgswdvrzdk6f5bzc32")))

(define-public crate-tiberius-mappers-derive-0.3.0 (c (n "tiberius-mappers-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1wgrw54xsk33l4grmalg9s24i9bkqdaqih3478zi0jih2c68m5bf")))

(define-public crate-tiberius-mappers-derive-0.4.0 (c (n "tiberius-mappers-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "06wnk3gzf410b1b28cdpkzgvxmm5wjh56f3zpdbza1vkxrk1vilg")))

(define-public crate-tiberius-mappers-derive-0.5.0 (c (n "tiberius-mappers-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "190kflc9ybh7grcxnfqba9nx1lfbzz2x8frvhz1933jlwvxsyk6n")))

(define-public crate-tiberius-mappers-derive-0.6.0 (c (n "tiberius-mappers-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "11c5j2wk9c63w5wjy3yk5hbqp17xmwn6v2j00sn1myq5l25c9kk0")))

