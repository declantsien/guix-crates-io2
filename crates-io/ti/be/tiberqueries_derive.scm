(define-module (crates-io ti be tiberqueries_derive) #:use-module (crates-io))

(define-public crate-tiberqueries_derive-0.1.0 (c (n "tiberqueries_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0syg48g6gzard011djq766wfipg8ppcv1cspcig9xwys3bf0198p")))

(define-public crate-tiberqueries_derive-0.1.2 (c (n "tiberqueries_derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08d68lrc86pzi5djfckxigpd6flrallc18l8807vvifi9kr0inmw")))

(define-public crate-tiberqueries_derive-0.1.3 (c (n "tiberqueries_derive") (v "0.1.3") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xhvni1l1hapsxwv42imkddzglc54dm1h9778ycci07rmh9c8149")))

