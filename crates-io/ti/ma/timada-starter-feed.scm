(define-module (crates-io ti ma timada-starter-feed) #:use-module (crates-io))

(define-public crate-timada-starter-feed-0.3.1 (c (n "timada-starter-feed") (v "0.3.1") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.0") (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (f (quote ("prost"))) (d #t) (k 1)))) (h "0bgdmsvwx8zwmgwsh3r5w7axlx92bvyf5h4gp6wfi8rqrdd2kw59") (f (quote (("proto")))) (y #t)))

