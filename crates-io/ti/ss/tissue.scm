(define-module (crates-io ti ss tissue) #:use-module (crates-io))

(define-public crate-tissue-0.0.0 (c (n "tissue") (v "0.0.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "tao") (r "^0.25") (d #t) (k 0)) (d (n "wry") (r "^0.37") (d #t) (k 0)))) (h "1vpzxb6vlpgh4ihnic6c8gl61fij2vgb1y6r5163zad2ivxcdphm")))

