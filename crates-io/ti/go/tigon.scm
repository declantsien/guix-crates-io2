(define-module (crates-io ti go tigon) #:use-module (crates-io))

(define-public crate-tigon-0.1.0 (c (n "tigon") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.23") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tungstenite") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sn4dhd19k28rp45k3r4i9xkh3xmllpkrl9cdjk4rnjch1s7q608")))

