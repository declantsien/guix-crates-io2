(define-module (crates-io pb db pbdb) #:use-module (crates-io))

(define-public crate-pbdb-0.1.0 (c (n "pbdb") (v "0.1.0") (d (list (d (n "pbdb-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "04ddkx5q8ld3yx3dd51bxl0ldn4cad0rfw7cppbv3qgnjvvdph3z")))

(define-public crate-pbdb-0.2.0 (c (n "pbdb") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "pbdb-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yrp60szhrjvy2928diqp0jbfjf3b3mf1fbxzmshr2y6b43ycbhq")))

(define-public crate-pbdb-0.3.0 (c (n "pbdb") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "pbdb-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.17.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gdzp6xxzq455gkxc93z8sdvn9i0wz7ryg8hrb9zysclhas3jm0i")))

