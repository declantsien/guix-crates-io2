(define-module (crates-io pb db pbdb-macros) #:use-module (crates-io))

(define-public crate-pbdb-macros-0.1.0 (c (n "pbdb-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.8.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0jp073nkgqcxbirs2yr4dz2jx4qp5lv0rkznyz9pjh97rl9z5qsz")))

(define-public crate-pbdb-macros-0.2.0 (c (n "pbdb-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)))) (h "08ga2vhc8n0fw1yw2z6jyzks6ij89ardlsd8bzdby0whbp6mjypx")))

(define-public crate-pbdb-macros-0.3.0 (c (n "pbdb-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)))) (h "0jda4yfqmsnbyxlqrvcm3prq7rw9lw1f46yk0gi8r204dqhbac2v")))

