(define-module (crates-io pb f2 pbf2graph) #:use-module (crates-io))

(define-public crate-pbf2graph-0.3.0 (c (n "pbf2graph") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "osmpbf") (r "^0.3.0") (d #t) (k 0)))) (h "1rj5g0k4s7j8pkxykb2nz846y7f61yccmbsndg54d3kx7cwzlmwq")))

(define-public crate-pbf2graph-0.4.0 (c (n "pbf2graph") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "osmpbf") (r "^0.3.0") (d #t) (k 0)))) (h "131knm6dam3km1yv58h1sp535clwr8p01bsg7vvvmrxqxm9sqrqr")))

