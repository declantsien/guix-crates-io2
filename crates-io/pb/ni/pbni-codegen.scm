(define-module (crates-io pb ni pbni-codegen) #:use-module (crates-io))

(define-public crate-pbni-codegen-0.1.0 (c (n "pbni-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "046vvnwyb5kapkczfyw50cx8rgmhpixj561glr76qxnfsilknsx5") (f (quote (("visualobject") ("nonvisualobject") ("global_function") ("default" "global_function" "nonvisualobject" "visualobject"))))))

