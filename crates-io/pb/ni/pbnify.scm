(define-module (crates-io pb ni pbnify) #:use-module (crates-io))

(define-public crate-pbnify-0.1.0 (c (n "pbnify") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1s7jr77qk26ff62gmy7nf04x2hxs84gqnf9i4h2bkbncgr4d3b2w")))

(define-public crate-pbnify-0.2.0 (c (n "pbnify") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0azcgrs2ki609sdvsj6qq84yyg1xmcngsn9n1wig9raswm8v7a5l")))

(define-public crate-pbnify-0.2.1 (c (n "pbnify") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0c9nnb4gsagl96dk6pgb1hpzhjvd6bfsid6g90njykz67q4ml2vb")))

