(define-module (crates-io pb s- pbs-sys) #:use-module (crates-io))

(define-public crate-pbs-sys-0.0.1 (c (n "pbs-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)))) (h "1ya06b0xmpw0s78hasjv1ngw8z4053w0vxzpirdyq2ca0ch8sxzd") (f (quote (("static") ("default"))))))

(define-public crate-pbs-sys-0.0.2 (c (n "pbs-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 0)) (d (n "linked_list_c") (r "^0.1.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)))) (h "0w9k15v400lg03xlsihh2qxpbycsils2qgar8x6ja9xcq4h7n41l") (f (quote (("static") ("default"))))))

