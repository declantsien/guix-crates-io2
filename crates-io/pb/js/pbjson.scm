(define-module (crates-io pb js pbjson) #:use-module (crates-io))

(define-public crate-pbjson-0.1.0 (c (n "pbjson") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "160xp2g1adkrr7dni4lnbyhlypck09p176nj6mc6bb1s7brizpnv")))

(define-public crate-pbjson-0.2.0 (c (n "pbjson") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b6v9bjq5n69mfsdxm0lm7a1rdmkgafdzpliljz3hmhdv4gjb15z")))

(define-public crate-pbjson-0.2.1 (c (n "pbjson") (v "0.2.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13s91ygfzd0w2am8c5fffc546scmwk6826w1vmv756zgprjlkxhj")))

(define-public crate-pbjson-0.2.2 (c (n "pbjson") (v "0.2.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nfc619zylziblby9k06ai3qv6ir8snc7h5pbhx8ic5bfmq7x20r")))

(define-public crate-pbjson-0.2.3 (c (n "pbjson") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f60mn7rb4k59w1sxc9b249gaxkx9i33xqlz00rz7jkk80yrp7h7")))

(define-public crate-pbjson-0.3.0 (c (n "pbjson") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08ph3b6khwhyvlcp4fl0z3mwniqgg4sg396qymdg6l1wp5hhlv6q")))

(define-public crate-pbjson-0.3.1 (c (n "pbjson") (v "0.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07qc6i9ifd2pw9840b05ldzif8mbhii6725p6nvf9qi560qk45sd")))

(define-public crate-pbjson-0.3.2 (c (n "pbjson") (v "0.3.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h366c89a57shqv1md825fd2shss58gn0xsg5iqm5x09s18mpaqm")))

(define-public crate-pbjson-0.4.0 (c (n "pbjson") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17gwzwnib2cs1fh4gyacknwaqb7fz9sk16qpjr5dz81czjpfk7sr")))

(define-public crate-pbjson-0.5.0 (c (n "pbjson") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nhms9x3qwjgbclppy0h0b4kh83hx7a4l5qy6all06x99z3dc1ig")))

(define-public crate-pbjson-0.5.1 (c (n "pbjson") (v "0.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xk72k0rz9b9kp0mrx44dcihlanajy6vri3hji7m3aqy7k4rm3q4")))

(define-public crate-pbjson-0.6.0 (c (n "pbjson") (v "0.6.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "143vazvr2dc3jx7nyxmz6313rkqszxn9swnzlljjsapcn0cwfc0h")))

