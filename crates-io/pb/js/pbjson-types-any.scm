(define-module (crates-io pb js pbjson-types-any) #:use-module (crates-io))

(define-public crate-pbjson-types-any-0.2.3 (c (n "pbjson-types-any") (v "0.2.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "pbjson-any") (r "^0.2") (d #t) (k 0)) (d (n "pbjson-build-any") (r "^0.2") (d #t) (k 1)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-wkt") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0wkyr3psc0jxm7hgdan2jcmsdkc3pvf6ayd0aq71js4l1f74hf0r")))

