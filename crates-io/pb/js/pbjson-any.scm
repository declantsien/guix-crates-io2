(define-module (crates-io pb js pbjson-any) #:use-module (crates-io))

(define-public crate-pbjson-any-0.2.3 (c (n "pbjson-any") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "prost-wkt") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1hfazhpyvplc0n3c6nq85bfl7nyrarpby4hhsh6d3wrzr45d73i9")))

