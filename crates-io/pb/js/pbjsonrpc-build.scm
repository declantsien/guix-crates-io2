(define-module (crates-io pb js pbjsonrpc-build) #:use-module (crates-io))

(define-public crate-pbjsonrpc-build-0.0.1 (c (n "pbjsonrpc-build") (v "0.0.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1rszw2xjh70qbl3ygvxfbrdj3606lckmbbhxf20hw6441g8q58i1")))

(define-public crate-pbjsonrpc-build-0.0.2 (c (n "pbjsonrpc-build") (v "0.0.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1w7ff05178i98836p8qhlffrlyyrfd3z0y0lby9y5crbxfmv5jqc")))

(define-public crate-pbjsonrpc-build-0.0.3 (c (n "pbjsonrpc-build") (v "0.0.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0360n1dfn6rarv0f5dal74318aay1ffbhvvsi38hnmhvvdhjdq6r")))

(define-public crate-pbjsonrpc-build-0.1.0 (c (n "pbjsonrpc-build") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0bzp03nxscsgyccqvhi1g7nqaykh8h7xlhpv8yjzydjbkrsy1sr8")))

(define-public crate-pbjsonrpc-build-0.1.1 (c (n "pbjsonrpc-build") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1m30x94k3879npsss5f3r1rr9ml28s07zdqnkylmzm29wfqcr350")))

(define-public crate-pbjsonrpc-build-0.2.0 (c (n "pbjsonrpc-build") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1qw8jrwcy7pncld03qdbpmahwsypd257l6mvj8922hfs9m39mf4x")))

(define-public crate-pbjsonrpc-build-0.2.1 (c (n "pbjsonrpc-build") (v "0.2.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1ffl68893liyn56b9yhyhwxgdyjhn39xvbl41wsmbcrx7g9b12gz")))

(define-public crate-pbjsonrpc-build-0.17.0 (c (n "pbjsonrpc-build") (v "0.17.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1zbj0w025nr78lyhm9pr9z03cllrdlh8xln78apz302lpirjp6g5")))

(define-public crate-pbjsonrpc-build-0.18.0 (c (n "pbjsonrpc-build") (v "0.18.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "00zins45akknfhmx3p11har5y23hyrjsmxi5i8ffj4l6wnr24ya4")))

(define-public crate-pbjsonrpc-build-0.18.1 (c (n "pbjsonrpc-build") (v "0.18.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0v82dnzx12sz425vnrz0rr85384gv0aiwi02kkx8zn2kw35z80lf")))

