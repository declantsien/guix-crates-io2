(define-module (crates-io pb js pbjson-build-any) #:use-module (crates-io))

(define-public crate-pbjson-build-any-0.2.3 (c (n "pbjson-build-any") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1qa39kvzphfrj2idfx725vzqgkgi9qw0g9pmk98y69v4d1zx8ls1")))

