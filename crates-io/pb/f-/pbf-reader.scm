(define-module (crates-io pb f- pbf-reader) #:use-module (crates-io))

(define-public crate-pbf-reader-0.0.3 (c (n "pbf-reader") (v "0.0.3") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "18sqv509d06fxbbhy8ar49qfxkymvma696wlnnf94xjkd5hf0ggi")))

(define-public crate-pbf-reader-0.0.4 (c (n "pbf-reader") (v "0.0.4") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "1f0plb00rmwvhrwjkyhp472jybsdiv3xbaynrwqgvv98m6qn14ik")))

(define-public crate-pbf-reader-0.1.1 (c (n "pbf-reader") (v "0.1.1") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "piston_window") (r "^0.49.0") (d #t) (k 2)) (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "14biz86m3496pdz174dk8drac0c4cy9lj042i4p07ymnx3vdcg98")))

(define-public crate-pbf-reader-0.1.2 (c (n "pbf-reader") (v "0.1.2") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "piston_window") (r "^0.49.0") (d #t) (k 2)) (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "08cgma2lxrm34106mk1j5x76a96z8irhxdd3da2xsrb67m8s8pcp")))

(define-public crate-pbf-reader-0.1.3 (c (n "pbf-reader") (v "0.1.3") (d (list (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "piston_window") (r "^0.57.0") (d #t) (k 2)) (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "14sh4y4yyb4gjxz4ph4xqg8vgvaia68zsiw9g98y7map2j0af33a")))

(define-public crate-pbf-reader-0.1.4 (c (n "pbf-reader") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.74.0") (d #t) (k 2)) (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)))) (h "06cla9jkqhpvkp43lp1nyf5xxdbmb7wlvcl8dkqblmr8032g7p6y")))

(define-public crate-pbf-reader-0.1.7 (c (n "pbf-reader") (v "0.1.7") (d (list (d (n "flate2") (r "^1.0.4") (d #t) (k 0)) (d (n "piston_window") (r "^0.82.0") (d #t) (k 2)) (d (n "protobuf") (r "^2.1.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.1.1") (d #t) (k 1)))) (h "1ycp5g8fmq50g7sqkwzv9isqz26jdc6ricaqkmd5xzvf1k3dg1bg")))

(define-public crate-pbf-reader-0.1.8 (c (n "pbf-reader") (v "0.1.8") (d (list (d (n "flate2") (r "^1.0.4") (d #t) (k 0)) (d (n "piston_window") (r "^0.82.0") (d #t) (k 2)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.2.0") (d #t) (k 1)))) (h "1jcwyfyapikshb0glxy5f5fdy643w3lywlskcrbn06098f9ryw5b")))

(define-public crate-pbf-reader-0.1.9 (c (n "pbf-reader") (v "0.1.9") (d (list (d (n "cbindgen") (r "^0.9.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.11") (d #t) (k 0)) (d (n "piston_window") (r "^0.103.0") (d #t) (k 2)) (d (n "protobuf") (r "^2.8.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.8.0") (d #t) (k 1)))) (h "1kxb0907y467z3n7fqmn3zp5imv4bvwghvbcxq8m9235d8pwm8vl")))

