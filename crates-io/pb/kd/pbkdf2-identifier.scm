(define-module (crates-io pb kd pbkdf2-identifier) #:use-module (crates-io))

(define-public crate-pbkdf2-identifier-0.0.1 (c (n "pbkdf2-identifier") (v "0.0.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0srps6v1a9l5avn1wlvpqf20zrqhqam3yfxpsa2kp40qbp4i1whd")))

(define-public crate-pbkdf2-identifier-0.0.2 (c (n "pbkdf2-identifier") (v "0.0.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1k0szr8h5xp0drm1kaj7qgd90hjgwqvkfyn08dr70flhkqqpphcx")))

(define-public crate-pbkdf2-identifier-0.0.3 (c (n "pbkdf2-identifier") (v "0.0.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1ibdpydmx6vw78i7v223zn1z6sqds4dfsjwyq8g24ha1j75ck0sj")))

(define-public crate-pbkdf2-identifier-0.0.4 (c (n "pbkdf2-identifier") (v "0.0.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0k0zzpjnd3zabqhjrjhwnbx3a9jwhpjahncxibcpv2b818q38cfn")))

(define-public crate-pbkdf2-identifier-0.0.5 (c (n "pbkdf2-identifier") (v "0.0.5") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "10rmhxd05wcd18z0arwcvrq1cc761niws6zgw4w7sbj8ing6ncn2")))

(define-public crate-pbkdf2-identifier-0.0.6 (c (n "pbkdf2-identifier") (v "0.0.6") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "08yz1c1jlzjf7vxyvihnf1aj9rdpqbwj8fbk5m24jhigdp0fhdl7")))

