(define-module (crates-io pb kd pbkdf2-identifier-cli) #:use-module (crates-io))

(define-public crate-pbkdf2-identifier-cli-0.0.0 (c (n "pbkdf2-identifier-cli") (v "0.0.0") (h "0ydagk5fwrsrindzwgr5h6lc531c2ng0pdhpi0vsaml128vcrb82")))

(define-public crate-pbkdf2-identifier-cli-0.0.3 (c (n "pbkdf2-identifier-cli") (v "0.0.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "pbkdf2-identifier") (r "^0.0.3") (d #t) (k 0)))) (h "0c9bmj9s2x814prmn80qvcig8iyyri5n53llriirxzihbv27acn2")))

(define-public crate-pbkdf2-identifier-cli-0.0.4 (c (n "pbkdf2-identifier-cli") (v "0.0.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "pbkdf2-identifier") (r "^0.0.4") (d #t) (k 0)))) (h "0wc1ayizyalvn8yb10pqz41lrgvcainng6jvxxy6ar16bhpbh5jq")))

(define-public crate-pbkdf2-identifier-cli-0.0.6 (c (n "pbkdf2-identifier-cli") (v "0.0.6") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "pbkdf2-identifier") (r "^0.0.6") (d #t) (k 0)))) (h "1n0jhp0jphr90krxarbisfrxxf51gxm8qx2yyjik0bh3maqn8ksb")))

