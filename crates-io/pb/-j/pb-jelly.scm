(define-module (crates-io pb -j pb-jelly) #:use-module (crates-io))

(define-public crate-pb-jelly-0.0.1 (c (n "pb-jelly") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pa8z2wxqahwk3hasppas82szs2iff78n99m1lzslj996yq8kcxp")))

(define-public crate-pb-jelly-0.0.2 (c (n "pb-jelly") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r8a303p2w62c0hbirzsaq0fh65w4dxbxivhfpyjdx7yy73rh39g") (y #t)))

(define-public crate-pb-jelly-0.0.3 (c (n "pb-jelly") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "09jvhrhr0lygihv1fx282fabgfi0v275myr6f5150zrjikhjv1mn")))

(define-public crate-pb-jelly-0.0.4 (c (n "pb-jelly") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "07l4b9576hp3hixkp4a8sj1y0423fbv5smz84fqww46ipjpk7sm8")))

(define-public crate-pb-jelly-0.0.5 (c (n "pb-jelly") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m6lybivpp3mjj1v7372nhmkhzf9nyiyll17wal56g4jsrd3avmz")))

(define-public crate-pb-jelly-0.0.6 (c (n "pb-jelly") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10aprc9za93ndgamx6vc4pmdmzv0bwmv3vkzr7p0hgag2ww5sk5z")))

(define-public crate-pb-jelly-0.0.7 (c (n "pb-jelly") (v "0.0.7") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06f5ycg8n0mml8fyzwhkfi288m572147hdxnva7vxjq16mi1blhx")))

(define-public crate-pb-jelly-0.0.8 (c (n "pb-jelly") (v "0.0.8") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ndaan29jm64dmvr45ahp0rjb4041k72yxk9bq7cn9c4nsb1ya24")))

(define-public crate-pb-jelly-0.0.9 (c (n "pb-jelly") (v "0.0.9") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c6x25lck02sva05r56rx0gdb0zyfby5427yj7lixy8glx480mc7")))

(define-public crate-pb-jelly-0.0.10 (c (n "pb-jelly") (v "0.0.10") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cykyqiwbrrx35c8ikgqvyb3b0567mb6fg37l6hqif0czadph39w")))

(define-public crate-pb-jelly-0.0.11 (c (n "pb-jelly") (v "0.0.11") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04dz31rax5c158n4i3yp02yw4bkrkhbs5md87miid04mfvbwcx4b")))

(define-public crate-pb-jelly-0.0.12 (c (n "pb-jelly") (v "0.0.12") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "compact_str") (r "^0.5") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yar2z7x23mpmpkcfi1a88cm2fvcjc1wrwy662j89m25cqb0yaqj")))

(define-public crate-pb-jelly-0.0.13 (c (n "pb-jelly") (v "0.0.13") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "compact_str") (r "^0.5") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v41cdnay81r809sylc3cqvrhbmrhqc2w7h1zmk4mskr1mggk2zh")))

(define-public crate-pb-jelly-0.0.14 (c (n "pb-jelly") (v "0.0.14") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "compact_str") (r "^0.5") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11wli42pxiknxnck6cg4qfvm0ydpr9ppv9lvzdbgdgcb2gp3bd3x")))

(define-public crate-pb-jelly-0.0.15 (c (n "pb-jelly") (v "0.0.15") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "compact_str") (r "^0.5") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kv1rj4ml0yfi60kjnd23idk226v25avn1d3d5wlqfmij11ryar2")))

(define-public crate-pb-jelly-0.0.16 (c (n "pb-jelly") (v "0.0.16") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "compact_str") (r "^0.5") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01h7xfyj9ll73c8s9i83mk9lcasmjd13jaaqbdzdhbiga2i439yz")))

