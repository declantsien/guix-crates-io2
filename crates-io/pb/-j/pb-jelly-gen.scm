(define-module (crates-io pb -j pb-jelly-gen) #:use-module (crates-io))

(define-public crate-pb-jelly-gen-0.0.1 (c (n "pb-jelly-gen") (v "0.0.1") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kqb838avx9zl8jjvypqav2xc61n56ji5vm94ji6l2vmhcz92kzm")))

(define-public crate-pb-jelly-gen-0.0.2 (c (n "pb-jelly-gen") (v "0.0.2") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1q14rmsqll7xgsnyadpfg4ip4s4x1ikzbij7p29ffb32y5spx060") (y #t)))

(define-public crate-pb-jelly-gen-0.0.3 (c (n "pb-jelly-gen") (v "0.0.3") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0szv16wa3wl9bc08s4wcyx3xm9wmhl8qmx298ypfx9izgxbchf3n")))

(define-public crate-pb-jelly-gen-0.0.4 (c (n "pb-jelly-gen") (v "0.0.4") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "022kzlb8s02r0y9537yrqzbwlx8n92mdnl3nz0qnhia12rvbwdli")))

(define-public crate-pb-jelly-gen-0.0.5 (c (n "pb-jelly-gen") (v "0.0.5") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "05jd7rx5g5s1axy05b2pk2bzl4xscdbfjmsr6alwn4lj5nxxnmh7")))

(define-public crate-pb-jelly-gen-0.0.6 (c (n "pb-jelly-gen") (v "0.0.6") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0s59a445813v5nahcs7qmr5l2dgzlw141bysa3gfdzl99im7dmhz")))

(define-public crate-pb-jelly-gen-0.0.7 (c (n "pb-jelly-gen") (v "0.0.7") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01ybx4239yf8l5a96b0iqxnpp0d8jqlrf67x5d6czrfjnax3x430")))

(define-public crate-pb-jelly-gen-0.0.8 (c (n "pb-jelly-gen") (v "0.0.8") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ch1bacndfpqdbjl84xcj0pmp1ivm65k8994qdsj87c7wd6k0rlz")))

(define-public crate-pb-jelly-gen-0.0.9 (c (n "pb-jelly-gen") (v "0.0.9") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0vpjx0q8ab3n5dryy2dg4ldr0v2350xldmkc0d3a5138w4w2mxk9")))

(define-public crate-pb-jelly-gen-0.0.10 (c (n "pb-jelly-gen") (v "0.0.10") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0q2ppl0ikpdbm72d8hhqssv54p056wdpbwdwk600j156x5mljcr5")))

(define-public crate-pb-jelly-gen-0.0.11 (c (n "pb-jelly-gen") (v "0.0.11") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "127kxkj5iwa5rj58ilbwdkhiyb2bcmxri0sc5xn0sbrpqx2q6mz3")))

(define-public crate-pb-jelly-gen-0.0.12 (c (n "pb-jelly-gen") (v "0.0.12") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1i31knx817bm11c89nksf326wc2q5jbmcjf2fc1jc7yi4p0p4fcs")))

(define-public crate-pb-jelly-gen-0.0.13 (c (n "pb-jelly-gen") (v "0.0.13") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ggsngs5p80x6kwydv12150cnlylz0kqibw2wgis14i43jnyjl0z")))

(define-public crate-pb-jelly-gen-0.0.14 (c (n "pb-jelly-gen") (v "0.0.14") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18zd3vh1sajx92an27ykb9bs9290cbpbjydzccyy22c85nydk304")))

(define-public crate-pb-jelly-gen-0.0.15 (c (n "pb-jelly-gen") (v "0.0.15") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "024k98ih7gr20hr2k3szfagdbwbw73l7i3869n7xdpqgar4r667a")))

(define-public crate-pb-jelly-gen-0.0.16 (c (n "pb-jelly-gen") (v "0.0.16") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "00kwxn2rrnk30y4lvb6v9wn2m0xaiaaaka9g0jwxrhrpk0sr9wpw")))

