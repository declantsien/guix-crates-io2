(define-module (crates-io pb ix pbix) #:use-module (crates-io))

(define-public crate-pbix-0.1.0 (c (n "pbix") (v "0.1.0") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0ya06rfxsw6m90i8llhr6kh1vc7vm90idrr7c9hkax0y9ys2p8n5") (s 2) (e (quote (("rayon" "dep:rayon"))))))

