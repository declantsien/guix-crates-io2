(define-module (crates-io pb mx pbmx) #:use-module (crates-io))

(define-public crate-pbmx-0.1.0 (c (n "pbmx") (v "0.1.0") (h "1zckq1y0xizrfqiqk004h5v73jgci2w1rpdql81aa0gmd8s4n3as") (y #t)))

(define-public crate-pbmx-0.1.1 (c (n "pbmx") (v "0.1.1") (h "1hw1sfhyasnxzsfbk298bq825nml4ipm4v3symys6cfk1q6w3qda") (y #t)))

(define-public crate-pbmx-0.1.2 (c (n "pbmx") (v "0.1.2") (h "14r2hy129nmxw1p17vi239yf28857m8kjy27qh4y0ibl7grpcan7")))

