(define-module (crates-io pb ar pbar) #:use-module (crates-io))

(define-public crate-pbar-0.1.1 (c (n "pbar") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "1clxqxvy0gpr3h7qkrxw36fk7zfdbf0prghrqzq1g72ah60pmy5q")))

