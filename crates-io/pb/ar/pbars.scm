(define-module (crates-io pb ar pbars) #:use-module (crates-io))

(define-public crate-pbars-0.1.0 (c (n "pbars") (v "0.1.0") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "1mszwbmr3camk6fdmh5jlm1idvgy03k1q4ka6nw782n9c1144amk")))

(define-public crate-pbars-0.1.1 (c (n "pbars") (v "0.1.1") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "1mkdy7cpzkvrx9jr3ss5wpdqy2xyr9glz2fs2mjxy1qqz16sapl4") (f (quote (("default" "crossterm"))))))

(define-public crate-pbars-0.1.2 (c (n "pbars") (v "0.1.2") (d (list (d (n "better_term") (r "^1.0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "1gflc21sxgp3napnsfxy4l4mn9zv3aczfv12crrzsgb03wrzkkir") (f (quote (("default" "crossterm"))))))

