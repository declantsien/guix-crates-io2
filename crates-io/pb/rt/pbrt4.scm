(define-module (crates-io pb rt pbrt4) #:use-module (crates-io))

(define-public crate-pbrt4-0.1.0 (c (n "pbrt4") (v "0.1.0") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jyw6mj514nr514gw68z09jzisz598bgyy70zv0i5pvpjv61n4cc")))

(define-public crate-pbrt4-0.2.0 (c (n "pbrt4") (v "0.2.0") (d (list (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02fk3jwkiani4rlhwp3sd927pfdadi0nfswynxrf4raldlxiccmv")))

