(define-module (crates-io pb rt pbrt) #:use-module (crates-io))

(define-public crate-pbrt-0.1.0 (c (n "pbrt") (v "0.1.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1g1rbdd8z2kvby6imgkyb1kiabjvysbkk6711d0f9rnjs7fmn7ay")))

(define-public crate-pbrt-0.1.1 (c (n "pbrt") (v "0.1.1") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1xqgb3r2x4vcpl4bsdcmqzi9h09b09cwin615i75v4n687ss66nb")))

(define-public crate-pbrt-0.1.2 (c (n "pbrt") (v "0.1.2") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.4.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0akpxcmb1bib716mgjp1i0s5iycp2di4qnhfwazwn0k1g2y9cvwa") (f (quote (("sampled-spectrum") ("float-as-double"))))))

(define-public crate-pbrt-0.1.3 (c (n "pbrt") (v "0.1.3") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.4.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0n5jm6lzxlfpvj3qzanmf8g1s2qjkmgdzgp283dxgzrnrwiikcb5") (f (quote (("sampled-spectrum") ("float-as-double"))))))

(define-public crate-pbrt-0.1.4 (c (n "pbrt") (v "0.1.4") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.4.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0sb3kgh856wqr9gxd3wxqcsgrz860d0vvyln1p363n18v29qdrnj") (f (quote (("uble") ("sampled-spectrum") ("float-as-double"))))))

(define-public crate-pbrt-0.1.5 (c (n "pbrt") (v "0.1.5") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.2.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.4.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0ynhndcydnrnqd3hj8kwzrwlb45a42minq6iywrfbipnx32hjc6x") (f (quote (("uble") ("sampled-spectrum") ("float-as-double"))))))

