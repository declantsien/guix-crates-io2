(define-module (crates-io pb co pbcodec) #:use-module (crates-io))

(define-public crate-pbcodec-0.0.1 (c (n "pbcodec") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1v6svr9kl3kybl4gi2cjx8n8hqsp5w4mrx7xx5w3xppwbdppvynk")))

(define-public crate-pbcodec-0.0.2 (c (n "pbcodec") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0cyyc0j3mh0m5gd9bfnks9jddfplpjw5p8aii0n6a7jf1k85pwi8")))

(define-public crate-pbcodec-0.0.3 (c (n "pbcodec") (v "0.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1fqj6v7sxp3vaqid74sq7f6ma41dj3j4xzmicgzk4vi3wd80xchp")))

(define-public crate-pbcodec-0.0.4 (c (n "pbcodec") (v "0.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0afd4d3m6kszjd2gqa1942zdsv2wddadyrp9a9k6h4c4sdj9ajpd")))

(define-public crate-pbcodec-0.0.5 (c (n "pbcodec") (v "0.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0zag6kh2cpgw83b7aiva7wi13941qhwp1p49nsaz2b0911kqkkfl")))

(define-public crate-pbcodec-0.0.6 (c (n "pbcodec") (v "0.0.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "16x12ibzfpaivxqvwq0hs3pnx7iki2i7zk7ns2s2lpj1r998p79h")))

(define-public crate-pbcodec-0.0.7 (c (n "pbcodec") (v "0.0.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "19fajdlaws4s8y779knpjvp5avqygnhs88dfdmw222gndvpvyfr7")))

(define-public crate-pbcodec-0.0.8 (c (n "pbcodec") (v "0.0.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1nai7ql0yqjdiqnn4f7c7abnnfksnni9aj288cj5psmi3j6xl2as")))

