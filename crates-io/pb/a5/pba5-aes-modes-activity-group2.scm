(define-module (crates-io pb a5 pba5-aes-modes-activity-group2) #:use-module (crates-io))

(define-public crate-pba5-aes-modes-activity-group2-0.1.0 (c (n "pba5-aes-modes-activity-group2") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)))) (h "17676sh9m3xaxk6aivyq8956g59j92b1vy6385v8cmd1r7mbdzjd")))

(define-public crate-pba5-aes-modes-activity-group2-0.1.1 (c (n "pba5-aes-modes-activity-group2") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)))) (h "14w1x2qfh9nyhm51lcp3swm2n54p03d028lv5b5ym6sglybggqig")))

