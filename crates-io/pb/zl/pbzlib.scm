(define-module (crates-io pb zl pbzlib) #:use-module (crates-io))

(define-public crate-pbzlib-0.1.0 (c (n "pbzlib") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde-protobuf") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0y7r20qiby5j0gg0vkvqca8xkri3p7yx5v1q84mn91nl2cm4b45v")))

(define-public crate-pbzlib-0.1.1 (c (n "pbzlib") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "jsonpath_lib") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "protobuf") (r "^2.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde-protobuf") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "0hhg1mxvi02lmvy5hskkibrkdhvq6dy4rhd8xf37ml309amzkpa9") (f (quote (("cli" "clap" "indicatif" "jsonpath_lib"))))))

