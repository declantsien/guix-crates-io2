(define-module (crates-io pb li pblib-rs) #:use-module (crates-io))

(define-public crate-pblib-rs-0.1.0 (c (n "pblib-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "splr") (r "^0.17.1") (f (quote ("incremental_solver"))) (d #t) (k 2)))) (h "0b8l3shmgkbwycwl29vw53qp8vgq18d5nra0dxp0a1pcnz8053f6") (l "libcpblib.a")))

