(define-module (crates-io pb -r pb-rs) #:use-module (crates-io))

(define-public crate-pb-rs-0.2.0 (c (n "pb-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "04x2hl66jccrvd8xl34xlwdwxpc4zvc37bgr114kdxq18kgia8wa")))

(define-public crate-pb-rs-0.3.1 (c (n "pb-rs") (v "0.3.1") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0lriznbmbrdm49csnfk0x0mnab9cl2innpnhlbk4frgdrm37wpjv")))

(define-public crate-pb-rs-0.3.2 (c (n "pb-rs") (v "0.3.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "10c00zb8kjyckmfkwdz7n4i2k9lks5wlvaw3kn96yv3p2fjrzf0r")))

(define-public crate-pb-rs-0.4.0 (c (n "pb-rs") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1izxak61877nxgrb9v0wqlvpqh41f448xicyn0bbrkkx546b4rvr")))

(define-public crate-pb-rs-0.4.1 (c (n "pb-rs") (v "0.4.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1a0c45w13a38p3fvmhgvg2z7l9fgp9qgyb6yzlin40fizab1sg4w")))

(define-public crate-pb-rs-0.5.0 (c (n "pb-rs") (v "0.5.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1hrf5jcha44ibj7qhpzzzlb1fcwrrfvyn5fss5rml9ba5r3csxiy")))

(define-public crate-pb-rs-0.6.0 (c (n "pb-rs") (v "0.6.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "0fphnnphggbjl0w9f2gsw6ms3wbbpz9y5gwbwxfxhn6dvfkc0kbz")))

(define-public crate-pb-rs-0.7.0 (c (n "pb-rs") (v "0.7.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "0bgm8c38zk5r4k2hjvjhs9bdj5jnvwzi6saq2fdzjhs3dj46gm67")))

(define-public crate-pb-rs-0.8.0 (c (n "pb-rs") (v "0.8.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "0mbndy6m1xiiirchb45ihrq57pn6mym9xigdc23nim619b0jgll2") (f (quote (("generateImplFromForEnums") ("default"))))))

(define-public crate-pb-rs-0.8.2 (c (n "pb-rs") (v "0.8.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "1ikxqhxmcm3h1lndqy2h5nl0fmbxq5yrmzz2y0ww2vhfpdf1cvz0") (f (quote (("generateImplFromForEnums") ("default"))))))

(define-public crate-pb-rs-0.8.3 (c (n "pb-rs") (v "0.8.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "082cl7krf6v0cjdm1v13kgwzryi776mn24bp1idsapycsxyc0g9k") (f (quote (("generateImplFromForEnums") ("default"))))))

(define-public crate-pb-rs-0.9.0 (c (n "pb-rs") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0dz72hyz57zg32l32znpf8i4qi8d3q0lzwmybxa88cr3h9fbki69") (f (quote (("generateImplFromForEnums") ("default"))))))

(define-public crate-pb-rs-0.9.1 (c (n "pb-rs") (v "0.9.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1wxb5gdmmsq8r6l0y5z4l7qqy49gfz8lkvprnzlp7dx95z0cb6ak") (f (quote (("generateImplFromForEnums") ("default"))))))

(define-public crate-pb-rs-0.10.0 (c (n "pb-rs") (v "0.10.0") (d (list (d (n "clap") (r "^2.33.1") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "0zk0w8i98rc5k45y6fmh5pn7jcrrwc7iq04q4larddb5kkgk8jim") (f (quote (("std" "clap" "env_logger") ("generateImplFromForEnums") ("default" "std"))))))

