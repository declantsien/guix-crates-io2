(define-module (crates-io pb si pbsim-rs) #:use-module (crates-io))

(define-public crate-pbsim-rs-0.1.0 (c (n "pbsim-rs") (v "0.1.0") (h "1ap6zf5krchh61cyvgwhcjn5w91i62dmsjwyzqrf0mgnb5ji7jj8") (y #t)))

(define-public crate-pbsim-rs-0.1.1 (c (n "pbsim-rs") (v "0.1.1") (h "0knyn6fwaivf73pbys0j4aaj0cy293ww0ma9wy6p0lc36mp39hcl") (y #t)))

