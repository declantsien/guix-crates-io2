(define-module (crates-io pb #{-i}# pb-imgsize) #:use-module (crates-io))

(define-public crate-pb-imgsize-0.2.4 (c (n "pb-imgsize") (v "0.2.4") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1j3vqvkrkjpqmki00wrd8b8x2k33jhpwx5vvbcn8hkkvgjvc9x6b")))

(define-public crate-pb-imgsize-0.2.5 (c (n "pb-imgsize") (v "0.2.5") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "1pzk8f22v9r8xcf237ay7xa3zjgaay522cp3fghixi0bpva28gp6")))

