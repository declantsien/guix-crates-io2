(define-module (crates-io pb to pbtools) #:use-module (crates-io))

(define-public crate-pbtools-0.1.0 (c (n "pbtools") (v "0.1.0") (h "0za44nd7bfrm1nlqjhfvwkgfl23p9xdv1ig7qzp3gzkf2cp1g00v")))

(define-public crate-pbtools-0.2.0 (c (n "pbtools") (v "0.2.0") (h "18iddv7qgg5y7gm894lcx6056897s0kbv2rm0bbh371mx1jmx73f")))

(define-public crate-pbtools-0.3.0 (c (n "pbtools") (v "0.3.0") (h "155x64sa0c547y20hkwbccp1fzqkwxqarkcz8fmwlw095ynq9rcr")))

(define-public crate-pbtools-0.4.0 (c (n "pbtools") (v "0.4.0") (h "08h42702nwdgm9chh41xrx0xp39r448rqav0ff552yy71mgrza7d")))

(define-public crate-pbtools-0.5.0 (c (n "pbtools") (v "0.5.0") (h "1j97nmna0m8f4a3ccq4b0c67j8fzdaf2aj0428a00irsdlfnxfpr")))

(define-public crate-pbtools-0.6.0 (c (n "pbtools") (v "0.6.0") (h "06f2j4hbxks3d8x3vbpnyamf8fisrr8las0k4qb3mxnpgf73dr3y")))

(define-public crate-pbtools-0.7.0 (c (n "pbtools") (v "0.7.0") (h "0s371y0zrmkjbmj2gzsjwn53wac2vfbgf1wpq4qfd4wbi08w2fsf")))

(define-public crate-pbtools-0.8.0 (c (n "pbtools") (v "0.8.0") (h "09awd6bg7hqcnkwmclcxr5n1cmabykw2mxjvwvr0rnsidngiibzh")))

(define-public crate-pbtools-0.9.0 (c (n "pbtools") (v "0.9.0") (h "1s4yif49xsa0fczzxp2a5b72360509zq27gpbk11mwi0wcbsph3m")))

(define-public crate-pbtools-0.10.0 (c (n "pbtools") (v "0.10.0") (h "1qcandhf6g8bk6m2a80g75ig22pyrzcyrsrws8jykk66ngcasy6c")))

(define-public crate-pbtools-0.11.0 (c (n "pbtools") (v "0.11.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0qa0mnicxglp7azgcjjd61kmhfrnzw6klqm47znbs4razq0b1g6f")))

(define-public crate-pbtools-0.12.0 (c (n "pbtools") (v "0.12.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "06rjb6j3svm0wcs4686jdjr4zbvq7g9a0c1kwlk1mcdalds9pkxq")))

(define-public crate-pbtools-0.13.0 (c (n "pbtools") (v "0.13.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1lggccvxfpyif3113d2z0xik5psrmh5l0rryrn1hy8xxik16xpgb")))

(define-public crate-pbtools-0.14.0 (c (n "pbtools") (v "0.14.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1qc8zn88jpsb7wh2cibj1hsmyp09jbbhadkvmw14cyciixamx1p7")))

(define-public crate-pbtools-0.15.0 (c (n "pbtools") (v "0.15.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "04hpgrpqy918jzvfz0m1aizk25zfp199yxyrv51qcl4vwfznmq9m")))

