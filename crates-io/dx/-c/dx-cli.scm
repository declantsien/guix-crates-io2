(define-module (crates-io dx -c dx-cli) #:use-module (crates-io))

(define-public crate-dx-cli-0.1.0 (c (n "dx-cli") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1y14s4b7676i86f18w1clznaj69al0bkhzfx8044vc363z70pgnl")))

(define-public crate-dx-cli-0.1.2 (c (n "dx-cli") (v "0.1.2") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1qan652b5fbdsb72iv7nw8x845qjlh48jx4sbpm5i50g9mxq33wv")))

(define-public crate-dx-cli-0.1.3 (c (n "dx-cli") (v "0.1.3") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1av565ilk0x1vkx1vd74qxjnmb66gxmx05c7j8jmpswdh2rchplg")))

(define-public crate-dx-cli-0.1.4 (c (n "dx-cli") (v "0.1.4") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vhp3pw235v8i3h0mrqak9zn1qsnbmavlansww57wr0lhjcwklfk")))

(define-public crate-dx-cli-0.1.5 (c (n "dx-cli") (v "0.1.5") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "19a1rhba2hv30j0hgd1dn85hs8adhxajwz1r4dfrha9615b1175d")))

(define-public crate-dx-cli-0.2.0 (c (n "dx-cli") (v "0.2.0") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1jipfrfgf14p2l77wm7l65ldml2g3xzcsa8zda8s7r94g7c66rq7")))

(define-public crate-dx-cli-0.3.1 (c (n "dx-cli") (v "0.3.1") (d (list (d (n "bytesize") (r "^1.3.0") (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "12nyzpafyqxbrg0d40vpxyjf8m8yyx5cl3s9gsvx6is0x1mn35mr")))

