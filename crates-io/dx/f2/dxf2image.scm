(define-module (crates-io dx f2 dxf2image) #:use-module (crates-io))

(define-public crate-dxf2image-0.1.0 (c (n "dxf2image") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitmapx") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "dxf") (r "^0.4") (d #t) (k 0)) (d (n "svgx") (r "^0.1.0") (d #t) (k 0)))) (h "17vp8vg9fmkxzlr12x6h6947sa9qilrkl7fcz17zl43ha6jfi28a") (f (quote (("png" "bitmapx") ("default"))))))

(define-public crate-dxf2image-0.1.1 (c (n "dxf2image") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dxf") (r "^0.4") (d #t) (k 0)) (d (n "resvg") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "svgx") (r "^0.1.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "usvg") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0z0wc542bnqcpjnsipqmpf3dah22njlqbf3hjs5x20i1a4rvn3by") (f (quote (("png" "resvg" "usvg" "tiny-skia") ("default"))))))

