(define-module (crates-io dx r_ dxr_derive) #:use-module (crates-io))

(define-public crate-dxr_derive-0.1.0 (c (n "dxr_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0b0af5vi3ir2xnrlp7995jp6xzvmv5w747fw0kbs5vlyr0lskyg4") (r "1.56.0")))

(define-public crate-dxr_derive-0.1.1 (c (n "dxr_derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1kz4jazjda7fkx5inkfahb23p8d890r16ckj02xn3hhps13qkz4z") (r "1.56.0")))

(define-public crate-dxr_derive-0.2.0 (c (n "dxr_derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "17lhxvcsjmismv6nk8833rpzx5ic6347bwyfarsn22yvmr9lfmvv") (r "1.56.0")))

(define-public crate-dxr_derive-0.2.1 (c (n "dxr_derive") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "018g7sryxi9g170xzb001iq5lnmhckm5ahg2wpsa6s96gjc23iqv") (r "1.56.0")))

(define-public crate-dxr_derive-0.3.0 (c (n "dxr_derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1klqjfda8912j98vg3h4l9wa1q7yy6pbiphda3qwymdb4s9mkkjp") (r "1.56.0")))

(define-public crate-dxr_derive-0.3.1 (c (n "dxr_derive") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0j13dq4mc07sxhdda27zwhmrqsh00jdjgqjwm41y5kq6mfv5c8sy") (r "1.56.0")))

(define-public crate-dxr_derive-0.4.0 (c (n "dxr_derive") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0kfb9ckqlj617qg5ppnsdk8jl86zjgwkrfpifmh84knm01nig3lm") (r "1.60.0")))

(define-public crate-dxr_derive-0.5.0 (c (n "dxr_derive") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0xq349hmfkwypxra40q3xrzmzjqrlg8smrkism4d5b1pwkikh88b") (r "1.61.0")))

(define-public crate-dxr_derive-0.5.1 (c (n "dxr_derive") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0ar6zk24a2g3a9h8aysr9jcmqiylv5h5l89ly3qmq6kp6ql12lpg") (r "1.61.0")))

(define-public crate-dxr_derive-0.5.2 (c (n "dxr_derive") (v "0.5.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "017sy4pmwrymjibr0r6qih8ckw7r16d08phgnk4xmqmb53i3mnmc") (r "1.61.0")))

(define-public crate-dxr_derive-0.5.3 (c (n "dxr_derive") (v "0.5.3") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0iv9nr3x4128jvm7naja555yp8k3ax3pjv1rn16hxqgml6d9qw6x") (r "1.63.0")))

(define-public crate-dxr_derive-0.5.4 (c (n "dxr_derive") (v "0.5.4") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "03w5r3ycipg2fv41khb590g2wm43cnab54va7qdx3cb9kv00igjv") (r "1.63.0")))

(define-public crate-dxr_derive-0.6.0-beta.1 (c (n "dxr_derive") (v "0.6.0-beta.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1j5nzkv6fnh19as7hx4jnrjpcycaplhl5lk7l44xvwd29zc843m3") (r "1.65.0")))

(define-public crate-dxr_derive-0.6.0-beta.2 (c (n "dxr_derive") (v "0.6.0-beta.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0fq8ak7i164pdzy10ysazkr70m5id202mrz9zd7ybr6ch71wjja4") (r "1.65.0")))

(define-public crate-dxr_derive-0.6.0 (c (n "dxr_derive") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "09hmqv4yrfb6ipy1jvcgxd7qzpziw3b3i1yavzqh4q5632kbqkx7") (r "1.65.0")))

(define-public crate-dxr_derive-0.6.1 (c (n "dxr_derive") (v "0.6.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "06d9kw9d5q0vzc9lahfpl3in8fvcj89ajhz4ysmf1rbh5ayjvcff") (r "1.65.0")))

(define-public crate-dxr_derive-0.6.2 (c (n "dxr_derive") (v "0.6.2") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v0yz9n0mi7m8bnmmwjd45l8bvdpkrwzghrpxdg1wbcz1lg28iyl") (r "1.67.0")))

(define-public crate-dxr_derive-0.5.5 (c (n "dxr_derive") (v "0.5.5") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0q2z0qdkvpg97q7p0vbwywzs4qr8lj9p0h2rvma4fki53d549vwq") (r "1.63.0")))

