(define-module (crates-io dx t- dxt-rs) #:use-module (crates-io))

(define-public crate-dxt-rs-0.1.0 (c (n "dxt-rs") (v "0.1.0") (h "1lcz6lhl87wqv0pmlxx49s6zdzbk96678m41y8g6vmaxdv5mixv2")))

(define-public crate-dxt-rs-0.1.1 (c (n "dxt-rs") (v "0.1.1") (h "01br12nzgh8s21p4fvqbvp4r6as3y8sp3125958fd2msr8v314y0")))

