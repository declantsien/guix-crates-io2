(define-module (crates-io dx cl dxcllistener) #:use-module (crates-io))

(define-public crate-dxcllistener-0.1.0 (c (n "dxcllistener") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2.2") (d #t) (k 2)) (d (n "dxclparser") (r "^1.0.0") (d #t) (k 0)))) (h "1vrfqxg1s6srqm3gi1lx7wqphs1zy4b3l4cnhykpawl64sijnp3h")))

(define-public crate-dxcllistener-0.2.0 (c (n "dxcllistener") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.2.2") (d #t) (k 2)) (d (n "dxclparser") (r "^1.0.0") (d #t) (k 0)))) (h "1akmh8qk4ii9nigwgbzh7kxwbml8i2qggjiblnzra32xbd5xahpc")))

(define-public crate-dxcllistener-0.2.1 (c (n "dxcllistener") (v "0.2.1") (d (list (d (n "ctrlc") (r "^3.2.3") (d #t) (k 2)) (d (n "dxclparser") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "16gw28aylnp53dyqd9wn9wx0l2p4wxnzi24q7hap14mdlfnr6sc8")))

(define-public crate-dxcllistener-0.3.0 (c (n "dxcllistener") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.2.3") (d #t) (k 2)) (d (n "dxclparser") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0rkns3c9kvrxaxvispighxyg8w9i4vljkb8ddb158yf2h24s76q7")))

(define-public crate-dxcllistener-0.4.0 (c (n "dxcllistener") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("time" "rt" "net" "sync" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z780b2fs0c24cmrw70nsp8r861m6xsd991ydb4rbkm5lw2mx1w4")))

(define-public crate-dxcllistener-1.0.0 (c (n "dxcllistener") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("time" "rt" "net" "sync" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "180pa3fywimcgwaag08knhw6zyv2yizrn2r4r8i3ng9li81gcg3g")))

(define-public crate-dxcllistener-1.0.1 (c (n "dxcllistener") (v "1.0.1") (d (list (d (n "socket2") (r "^0.4.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("time" "rt" "net" "sync" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "0f1m29w9rl4pv6s6lxkwr0j1rk5wglkd9diabj90c204pcrvjjyb")))

(define-public crate-dxcllistener-1.0.2 (c (n "dxcllistener") (v "1.0.2") (d (list (d (n "socket2") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("time" "rt" "net" "sync" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "11y22m71wzb9cbc7a3vph28mmfbzim1pmf98w5hzq87ancsam752")))

(define-public crate-dxcllistener-1.0.3 (c (n "dxcllistener") (v "1.0.3") (d (list (d (n "socket2") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("time" "rt" "net" "sync" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("signal" "rt-multi-thread"))) (d #t) (k 2)))) (h "14m9ayihnp49864zs95crnmd88nz86a14s2s46124w0nhas36jyg")))

