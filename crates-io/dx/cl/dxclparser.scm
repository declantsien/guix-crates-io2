(define-module (crates-io dx cl dxclparser) #:use-module (crates-io))

(define-public crate-dxclparser-1.0.0 (c (n "dxclparser") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b3qvqggj82shqbgwqx6nqgmkybqid3j6575lbjjnflrqf34kn98")))

(define-public crate-dxclparser-1.0.1 (c (n "dxclparser") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0111c4j411lmxsbcls1pl5km208w522i0zlxajyqc2kan3s3zbsi")))

