(define-module (crates-io dx gu dxguid-sys) #:use-module (crates-io))

(define-public crate-dxguid-sys-0.0.1 (c (n "dxguid-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0bd097mmi35g98f7kd6cd91dhxnjc6q7vasy0rna8wrc0kh1n7rs")))

(define-public crate-dxguid-sys-0.2.0 (c (n "dxguid-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "07xa6h6bcsm3bc33b4nlr00is8ankxp6n6dv81bl4yxc80y2dzb7")))

