(define-module (crates-io dx gi dxgi-sys) #:use-module (crates-io))

(define-public crate-dxgi-sys-0.0.1 (c (n "dxgi-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0lbrdszcchyc4xn3hjyffs0yh60sb9hb8vpj61j5jxiixlcinqkk")))

(define-public crate-dxgi-sys-0.2.0 (c (n "dxgi-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0hn285sa03c23dq75adf743d97fk8q6a4g5lsb23r2jszn6sfwxm")))

