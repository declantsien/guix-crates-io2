(define-module (crates-io dx gi dxgi-win) #:use-module (crates-io))

(define-public crate-dxgi-win-0.1.0 (c (n "dxgi-win") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0fa0agc22z3dq04lsz1sr8xiijaza9pfk7gad9ly3h1ykmj763gr") (y #t)))

(define-public crate-dxgi-win-0.1.1 (c (n "dxgi-win") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1h77zmppnrfkbvfyac6wkgx50dvhw4h111z7rg1drjsvra5w7rz4") (y #t)))

(define-public crate-dxgi-win-0.1.2 (c (n "dxgi-win") (v "0.1.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "16jgcg4mz47zd00b118dfa0z3x2bkpxrbpyfj39aqvv7m8lz1mqc") (y #t)))

(define-public crate-dxgi-win-0.1.3 (c (n "dxgi-win") (v "0.1.3") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0c18xb4ikvv18zpfkihlvy7x72l5hl8r9gff2rld81fqdwi36xyr") (y #t)))

(define-public crate-dxgi-win-0.1.4 (c (n "dxgi-win") (v "0.1.4") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1q8i2w0fjyd5ri87cxw85bwh4qikxgj720lh3yyrv6pggb4qyidp") (y #t)))

(define-public crate-dxgi-win-0.2.0 (c (n "dxgi-win") (v "0.2.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0hsqy17wxfjrx2gckvz5qgwwlw0dxfijphpjmfrg24dq2giabyvk") (y #t)))

(define-public crate-dxgi-win-0.2.1 (c (n "dxgi-win") (v "0.2.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "18xrllc4fjsbla9rgf02dwlra9hdx2mnzrlyypdcnlmxjvvj21p3") (y #t)))

