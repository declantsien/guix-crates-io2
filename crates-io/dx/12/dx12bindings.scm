(define-module (crates-io dx #{12}# dx12bindings) #:use-module (crates-io))

(define-public crate-dx12bindings-0.1.0 (c (n "dx12bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0k4bpp77699pz8akrabdskw7h3s71ahbjspi7jdrs8arwv8x6lfs") (y #t)))

(define-public crate-dx12bindings-0.0.1 (c (n "dx12bindings") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "08zw71n93k46bjpv5vs2dh3vqvwmmm7d8d3ckd7k1mfcp1pgyv22") (y #t)))

(define-public crate-dx12bindings-0.1.1 (c (n "dx12bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1b2zrfknhd8saxv83fmqxl1dqdvwgqml0c06azy30wl9xigchvma") (y #t)))

(define-public crate-dx12bindings-0.1.2 (c (n "dx12bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0z63g0k9fyysb81a8clyqaznspcljzlcs3949vg4a5pbb6xddrj7") (y #t)))

(define-public crate-dx12bindings-0.1.3 (c (n "dx12bindings") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1b1qvd3yz72p0ajg2axk0qkyh68a0dlyym8ddcy55i2cmqp0b4cc") (y #t)))

(define-public crate-dx12bindings-0.1.5 (c (n "dx12bindings") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "find-winsdk") (r "^0.2.0") (d #t) (k 1)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pixwrapper") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "08r6ar67plk7s0mv3g79kamg77r3kbfk3nwqdjhchn7120sxpbrd") (y #t)))

