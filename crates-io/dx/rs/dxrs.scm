(define-module (crates-io dx rs dxrs) #:use-module (crates-io))

(define-public crate-dxrs-0.0.0 (c (n "dxrs") (v "0.0.0") (h "16n46ypri677pwiziznc0j0ykl3x24mhynlw629d928gir1wy6vz") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

(define-public crate-dxrs-0.0.1 (c (n "dxrs") (v "0.0.1") (h "0rn2x9828vcc3dlzv60yqmx6d5nvibrwag97hvs7g0ksy3sn2ih9") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

