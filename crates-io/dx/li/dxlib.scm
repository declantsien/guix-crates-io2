(define-module (crates-io dx li dxlib) #:use-module (crates-io))

(define-public crate-dxlib-0.0.1 (c (n "dxlib") (v "0.0.1") (h "1jbm30gdkj28rbyrgfdhvywidfvr5lxyg00zjmyrvpy1wf1i9kbw") (y #t)))

(define-public crate-dxlib-0.0.2 (c (n "dxlib") (v "0.0.2") (h "1z4pzmp7affcwwxh79lwb6rg5g6kcgbxcl3qhbijwk6nqv0a0yd9") (y #t)))

(define-public crate-dxlib-0.0.3 (c (n "dxlib") (v "0.0.3") (h "03g4kygq55c6pdsvap6j3yqybs5hbm3s0n5dr7h12j27yhvdgnfg") (y #t)))

(define-public crate-dxlib-0.0.4 (c (n "dxlib") (v "0.0.4") (h "028mrdiqya00jyxil97yb2896l5xqd3kcmppyxqwcj5xjwv58cic") (y #t)))

(define-public crate-dxlib-0.0.5 (c (n "dxlib") (v "0.0.5") (h "1crfpg0k8kkvkycs1nvhj2ndif8s2var9c1139kqhnjwdny0mjig") (y #t)))

(define-public crate-dxlib-0.0.6 (c (n "dxlib") (v "0.0.6") (h "14hylkjzk0xpwzshq4df61x0bchm8w47kya7dwq12j33lbvnznhg") (y #t)))

(define-public crate-dxlib-0.0.7 (c (n "dxlib") (v "0.0.7") (h "1v7sqikh142cywl1s8if2zszdiy7z8ss8k9xq52ya3qzwg966518") (y #t)))

(define-public crate-dxlib-0.0.8 (c (n "dxlib") (v "0.0.8") (h "0dl2vs2f2frjp8j0iaaf8l5j11shn62ghz8zpl5xaky1pwn2zf6r") (y #t)))

(define-public crate-dxlib-0.0.9 (c (n "dxlib") (v "0.0.9") (h "1c3886lr1cmv992yr7wi7j0pw3hy116pqzibfdp2sy41nxjwpk99") (y #t)))

(define-public crate-dxlib-0.0.10 (c (n "dxlib") (v "0.0.10") (h "04fhnr1wplw9jz906v64xgz5m36cbzaqbmqk2bh4w4qpx0kzjd40") (y #t)))

(define-public crate-dxlib-0.1.1 (c (n "dxlib") (v "0.1.1") (h "1dl0mmna2010qdfypa4lh8adlanabwxs8nv5fiaww690iizmpyp3") (y #t)))

(define-public crate-dxlib-0.1.2 (c (n "dxlib") (v "0.1.2") (h "1cc683f7s0maiiki73k9n6kay8wh1g9bmz1i49kjcpm30vjc0rvb") (y #t)))

(define-public crate-dxlib-0.1.3 (c (n "dxlib") (v "0.1.3") (h "1082zbrj1gw4cmdgnnil8cg7rkc96gc97jyjhkfyi8684d26zxr3") (y #t)))

(define-public crate-dxlib-0.2.0 (c (n "dxlib") (v "0.2.0") (h "04p9bmr5i4yk9j0jc0xcrcl1f6i4s6m0xfv8y7h9nfdr23lfp2i3") (y #t)))

(define-public crate-dxlib-0.2.1 (c (n "dxlib") (v "0.2.1") (h "1xz276g504i3554pk99aqw7av3089cc8ph8bn87gxjc35bhjnpjr") (y #t)))

(define-public crate-dxlib-0.2.2 (c (n "dxlib") (v "0.2.2") (h "0qg0xl4x48xw9i133z6i93psnb5js31mn3n532f72fldryiv9zz0") (y #t)))

(define-public crate-dxlib-0.2.4 (c (n "dxlib") (v "0.2.4") (h "1wpk08gw7di0y7lf9rcaal75xrr18nyv5kbkj6xjvvlpjphqh0gl") (y #t)))

(define-public crate-dxlib-0.2.3 (c (n "dxlib") (v "0.2.3") (h "19qcxi1a3nl5pv5rjdxhhkqldx3jyzc3s1qa86x1ng0zm4wzdrdv") (y #t)))

(define-public crate-dxlib-0.2.5 (c (n "dxlib") (v "0.2.5") (h "19afgxx6hi6ca7d9a5cm6g6vsl1is9yin1sw3sjfs9kiz09991yb") (y #t)))

(define-public crate-dxlib-0.2.6 (c (n "dxlib") (v "0.2.6") (h "12zwkag52hb24s8njdsrzx8ky69lpgrx2q3m840izhy1mazsrkjk") (y #t)))

(define-public crate-dxlib-0.2.7 (c (n "dxlib") (v "0.2.7") (h "1l3kj4scd2p76bkqqbbrcxk8qcgqwdpdmknhniavz6f8albr89jz") (y #t)))

(define-public crate-dxlib-0.2.8 (c (n "dxlib") (v "0.2.8") (h "0rx6qsdsmsw8f5azp8jydin8ag0b9914grq8cy8b3689xx177xvl") (y #t)))

(define-public crate-dxlib-0.3.0 (c (n "dxlib") (v "0.3.0") (h "1mag2mw2j09jwi6cizfansg6zli88jva104g9bzx7d03548038q9") (y #t)))

(define-public crate-dxlib-0.3.1 (c (n "dxlib") (v "0.3.1") (h "1sriqkhvzrnhcl3i57q2r9nbb2369x3bccpmglysvdgdkvg1xjy4") (y #t)))

(define-public crate-dxlib-0.3.2 (c (n "dxlib") (v "0.3.2") (h "14bm5xll58vqgdgr98w3zs4cwd4jidklbhzax5pkkwyk9cpf17z1") (y #t)))

(define-public crate-dxlib-0.3.3 (c (n "dxlib") (v "0.3.3") (h "179vk2sya0z4hg4ry7spzcsvda7rv9knjf73y7j4v17zc6nx8dwp") (y #t)))

(define-public crate-dxlib-0.3.4 (c (n "dxlib") (v "0.3.4") (d (list (d (n "Fullerene") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0csm7xchgn4awx1rjbchi536x69h0rjwdwl1dy6smafvsvkhw7wx") (y #t)))

(define-public crate-dxlib-0.3.5 (c (n "dxlib") (v "0.3.5") (d (list (d (n "Fullerene") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1z95m2zm0cbhhj5f557srly0dnihfk1cqwi574zwrk17wg3l8cdq") (y #t)))

(define-public crate-dxlib-0.3.6 (c (n "dxlib") (v "0.3.6") (d (list (d (n "Fullerene") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "17b6r1ryl5lhrwrgh53z3l0c1aaknb7pqf1x9flp4j99zlmsl6pq") (y #t)))

(define-public crate-dxlib-0.3.7 (c (n "dxlib") (v "0.3.7") (d (list (d (n "Fullerene") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1xij75v6h44bjd1k6hpssh34phv3hrxplljfxs01b43fxar8lw9b") (y #t)))

(define-public crate-dxlib-0.3.8 (c (n "dxlib") (v "0.3.8") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1b656i6n0v1mn35gxdyfaar2z5g23fyvpa0amp2an7s5sfs1zawg") (y #t)))

(define-public crate-dxlib-0.3.9 (c (n "dxlib") (v "0.3.9") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "16mz9imxyar4rm89p0xij8g78k4xk9rhr5awi3pzd4s335i1ibpi") (y #t)))

(define-public crate-dxlib-0.3.10 (c (n "dxlib") (v "0.3.10") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1zlh0ml7x4nqql02m1n7a84jw5snvqyw8h7ld7mls8kywmysigcs") (y #t)))

(define-public crate-dxlib-0.3.11 (c (n "dxlib") (v "0.3.11") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "02qzbs404xdf0659bm8qqlqyi25hy1b06g8sld34z8g1hrbfy1id") (y #t)))

(define-public crate-dxlib-0.3.12 (c (n "dxlib") (v "0.3.12") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1dk3cx01kq2pxvh93nbnxvvwjngs6j7r7i2xln4im1ia1di7fm6m") (y #t)))

(define-public crate-dxlib-0.3.13 (c (n "dxlib") (v "0.3.13") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0pbmkki8p3wwwnjbfdydc8sjs7fi6yj49v6ivnkvlgb68r2q0i81") (y #t)))

(define-public crate-dxlib-0.3.14 (c (n "dxlib") (v "0.3.14") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "13m5m1qhj0sl37pqw2jqdi1qcg9npzzvzyfimrshx9s9nb2rs2zv") (y #t)))

(define-public crate-dxlib-0.3.15 (c (n "dxlib") (v "0.3.15") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0j2h284bpx42bza210w2hhj40bv3imywnz01rrqcxzmjrnpc1xvs") (y #t)))

(define-public crate-dxlib-0.3.16 (c (n "dxlib") (v "0.3.16") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "06grx7c09n60p240qjsnbha3lj8lncw1bx3x6yg9c8z2bfgmbpp7") (y #t)))

(define-public crate-dxlib-0.3.17 (c (n "dxlib") (v "0.3.17") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1xm6majvlbam8p61j6m0568xaldgl184w7jmvvsn7dxp7a05gbrp") (y #t)))

(define-public crate-dxlib-0.3.18 (c (n "dxlib") (v "0.3.18") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1gvjdqfb65rjr15gvybqd5csxpr3hgcgh72fcp7fvb3j2z0pvba1") (y #t)))

(define-public crate-dxlib-0.4.0 (c (n "dxlib") (v "0.4.0") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "04jq8rgrjms5dkc7v4n7km6iac4219bzkbi4qm7dlg0xgnxdips8") (y #t)))

(define-public crate-dxlib-0.4.1 (c (n "dxlib") (v "0.4.1") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1g6wx5zj9sx5q3ldd742ax3v00ajsgadq748yv2xmlfpxj83dm3i") (y #t)))

(define-public crate-dxlib-0.4.2 (c (n "dxlib") (v "0.4.2") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "16g9yzv8c4xlwh47ashrj0ah7hzm2mls04d2ixfb54rdx1w1d2n4") (y #t)))

(define-public crate-dxlib-0.4.3 (c (n "dxlib") (v "0.4.3") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "04g516ypysqmmf821jk8c6c6kh9waj0771zp5h335x80kplv262f") (y #t)))

(define-public crate-dxlib-0.4.4 (c (n "dxlib") (v "0.4.4") (d (list (d (n "Fullerene") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0k74v8blp4f365735hlglry6bvfly3g3jyg408pkf5wijl4ml8jn") (y #t)))

(define-public crate-dxlib-0.4.5 (c (n "dxlib") (v "0.4.5") (d (list (d (n "Fullerene") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1364c4nd66h60rdj674wmdi99wxkrqpnn00w0b1v9b3rk15w7ax7")))

(define-public crate-dxlib-0.5.1 (c (n "dxlib") (v "0.5.1") (d (list (d (n "Fullerene") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "1dpr2af6yfzkk5x951a01crpi2rclyarhwdzvb113k7570qmf757")))

