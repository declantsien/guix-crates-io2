(define-module (crates-io dx li dxlib_sample_0001) #:use-module (crates-io))

(define-public crate-dxlib_sample_0001-0.0.1 (c (n "dxlib_sample_0001") (v "0.0.1") (d (list (d (n "dxlib") (r "^0.0.1") (d #t) (k 0)))) (h "1pcwd0d0fy64ibcjrrgxkz3magbgbx9x1lskk3ji37pqyg2vs8p9") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.2 (c (n "dxlib_sample_0001") (v "0.0.2") (d (list (d (n "dxlib") (r "^0.0.2") (d #t) (k 0)))) (h "1ki7vpqk1vp4w5dxd4hsmbjl7fwidr3qil91jyq4pm6fmnk4giza") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.3 (c (n "dxlib_sample_0001") (v "0.0.3") (d (list (d (n "dxlib") (r "^0.0.3") (d #t) (k 0)))) (h "0w5fhh78xz5knhl2aa772qmg5bf7v0yy3fapj7643xb1587sw2a3") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.4 (c (n "dxlib_sample_0001") (v "0.0.4") (d (list (d (n "dxlib") (r "^0.0.4") (d #t) (k 0)))) (h "0rb1nn7n38r6avcc080ax82sj2i9v1wm8fczgc395jsdbp2plw14") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.7 (c (n "dxlib_sample_0001") (v "0.0.7") (d (list (d (n "dxlib") (r "^0.0.7") (d #t) (k 0)))) (h "1bjlpbih77bv0f9gv50z75mlmml0wf1ziglij4j8g77qiybjpadw") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.8 (c (n "dxlib_sample_0001") (v "0.0.8") (d (list (d (n "dxlib") (r "^0.0.8") (d #t) (k 0)))) (h "0wmdcpdrsmwdg3r4p5k9x12j7a8hjc978fgdj6bx0hksgrdimgyk") (y #t)))

(define-public crate-dxlib_sample_0001-0.0.9 (c (n "dxlib_sample_0001") (v "0.0.9") (d (list (d (n "dxlib") (r "^0.0.9") (d #t) (k 0)))) (h "0ii4mnyivw418sxwqlw0sqwlh665cnvysv7b0xv670jx1vws7cqg") (y #t)))

(define-public crate-dxlib_sample_0001-0.1.1 (c (n "dxlib_sample_0001") (v "0.1.1") (d (list (d (n "dxlib") (r "^0.1.1") (d #t) (k 0)))) (h "0mn0llpma9qgs51r3yyay8c3k497fv9wgapjiw3r7k6201h0cz6p") (y #t)))

(define-public crate-dxlib_sample_0001-0.1.2 (c (n "dxlib_sample_0001") (v "0.1.2") (d (list (d (n "dxlib") (r "^0.1.2") (d #t) (k 0)))) (h "1gq1qskyv6vjiksbknp0qsqc9ifhxc9jmabpjb7v72vy79bqqa19") (y #t)))

(define-public crate-dxlib_sample_0001-0.2.0 (c (n "dxlib_sample_0001") (v "0.2.0") (d (list (d (n "dxlib") (r "^0.2.0") (d #t) (k 0)))) (h "05nc20g6vw6r3pwvwk7f55yacsxr0v51qrxp3ss1gwwgk3czw9mh") (y #t)))

(define-public crate-dxlib_sample_0001-0.2.2 (c (n "dxlib_sample_0001") (v "0.2.2") (d (list (d (n "dxlib") (r "^0.2.2") (d #t) (k 0)))) (h "0sd4b62zkl1px6n0pczyrnad9k10r16jrv2bwql4i9wc0jwj1r4k") (y #t)))

(define-public crate-dxlib_sample_0001-0.2.8 (c (n "dxlib_sample_0001") (v "0.2.8") (d (list (d (n "dxlib") (r "^0.2.8") (d #t) (k 0)))) (h "16mmd1sk8gpvx36mvwc21llha5wh2b72w9c9vrp4895y038bim4i") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.0 (c (n "dxlib_sample_0001") (v "0.3.0") (d (list (d (n "dxlib") (r "^0.3.0") (d #t) (k 0)))) (h "1rkmafg8abgr8gvdbhhw0d7pb773hn85irglinibhmwpfj7ishf8") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.1 (c (n "dxlib_sample_0001") (v "0.3.1") (d (list (d (n "dxlib") (r "^0.3.0") (d #t) (k 0)))) (h "1l36fnpyf8ns0r8rr24k9c6zjji2g5nnikvbwsvsr9g3bwg5rnah") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.3 (c (n "dxlib_sample_0001") (v "0.3.3") (d (list (d (n "dxlib") (r "^0.3.3") (d #t) (k 0)))) (h "1rc9h3niafsxfkyrfv4n48swna0kqxcc0yh11wi5v0jlj4fpqjcl") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.4 (c (n "dxlib_sample_0001") (v "0.3.4") (d (list (d (n "Fullerene") (r "^0.1.1") (d #t) (k 0)) (d (n "dxlib") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0c7jfyv26bkfbx8p362blzjjd6yggm54bc49nlsmvv63qcg6alxi") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.5 (c (n "dxlib_sample_0001") (v "0.3.5") (d (list (d (n "Fullerene") (r "^0.1.2") (d #t) (k 0)) (d (n "dxlib") (r "^0.3.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0sb65p8lwyvn3fd00xlz96ph93galca0yx77lfzz4nqjrsvfn3fn") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.8 (c (n "dxlib_sample_0001") (v "0.3.8") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "10d50m0b1fx7qjyy6lhfbdsci30x7fbkadn5i25fk23r6awfi2wr") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.17 (c (n "dxlib_sample_0001") (v "0.3.17") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.3.17") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0mg5g5lzqr0sgbqvv1wi1zmns4wf5mgpsza6w80ya27wfa5ps1ms") (y #t)))

(define-public crate-dxlib_sample_0001-0.3.18 (c (n "dxlib_sample_0001") (v "0.3.18") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.3.18") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1k3iydsaq8vp9fk5mlfp3sqrmm6ig63yi2m15vsppam85j9j75ml") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.0 (c (n "dxlib_sample_0001") (v "0.4.0") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0qqqkrw1jzngs70k713bhk1j7wl1yyyaxwsz7lgncyja0nbgqvlh") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.1 (c (n "dxlib_sample_0001") (v "0.4.1") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "05p22zxs2ixfxdif62fhrlkzzy443sh1s956nnmmf7xgzl8spnmd") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.2 (c (n "dxlib_sample_0001") (v "0.4.2") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1l390risc4z6zwz64cfg77jg6a7gfidnx3j7vkb8cy2rqlq8shcg") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.3 (c (n "dxlib_sample_0001") (v "0.4.3") (d (list (d (n "Fullerene") (r "^0.1.5") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1gh7k6zxij33l3v230m4yn5iz0wd5qrkq5ny7sk2cc27rgnfvqzs") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.4 (c (n "dxlib_sample_0001") (v "0.4.4") (d (list (d (n "Fullerene") (r "^0.2") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1fvspndfbwym78sb9pzs2prgy9ncqpnm7li6sih47kxywphdrk0h") (y #t)))

(define-public crate-dxlib_sample_0001-0.4.5 (c (n "dxlib_sample_0001") (v "0.4.5") (d (list (d (n "Fullerene") (r "^0.2") (d #t) (k 0)) (d (n "dxlib") (r "^0.4.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0mjb6ra72lkv4n1j6si3yqj7r6s1n4gyml8n9jrssnlfravisiqb")))

(define-public crate-dxlib_sample_0001-0.5.1 (c (n "dxlib_sample_0001") (v "0.5.1") (d (list (d (n "Fullerene") (r "^0.3") (d #t) (k 0)) (d (n "dxlib") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "0vj7nbg4v8kb71f6pa2p146qw0b37pmfz09sr9qqsr9hz68c1nky")))

