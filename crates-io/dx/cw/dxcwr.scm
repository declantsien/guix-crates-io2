(define-module (crates-io dx cw dxcwr) #:use-module (crates-io))

(define-public crate-dxcwr-0.1.0 (c (n "dxcwr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("implement" "Win32_Foundation" "Win32_Security" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D_Dxc" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "19bw79j7miz4b22sfp10mgp18y63v6ga3a55ls8zxcps9p535iy1")))

(define-public crate-dxcwr-0.1.1 (c (n "dxcwr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("implement" "Win32_Foundation" "Win32_Security" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D_Dxc" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "1cmmfy27jwykq0dqidfq230z8pzqp7qid160iyhz2rz5w6zdkgc4")))

