(define-module (crates-io dx fe dxfeed) #:use-module (crates-io))

(define-public crate-dxfeed-0.1.0 (c (n "dxfeed") (v "0.1.0") (d (list (d (n "libdxfeed-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0zdx6bq9pv7z61xypyyjpya1awizah8hfgpcmvz0iff1lbrzdar5")))

(define-public crate-dxfeed-0.1.1 (c (n "dxfeed") (v "0.1.1") (d (list (d (n "libdxfeed-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0gfydpjsjwivyjqq13mb2yv587whvg9lyjc5sqciyd2n4nj36yzs")))

(define-public crate-dxfeed-0.1.2 (c (n "dxfeed") (v "0.1.2") (d (list (d (n "libdxfeed-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "10kf630ccwda284dq23hln4574wzaz38q2vyc20d357iii9anx5i")))

(define-public crate-dxfeed-0.1.3 (c (n "dxfeed") (v "0.1.3") (d (list (d (n "libdxfeed-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1mnpsf0zarrr3praddqwsq1h22bici9y2jj9b38iqhvsp5y6v1rj")))

(define-public crate-dxfeed-0.1.4 (c (n "dxfeed") (v "0.1.4") (d (list (d (n "libdxfeed-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1k97b5jbmkcsxsc72wf9yirqiri2iw5x3f6rvkpjzkyixv9bbinb")))

(define-public crate-dxfeed-0.1.5 (c (n "dxfeed") (v "0.1.5") (d (list (d (n "libdxfeed-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0724xr43adfdcirl902j2gpbd2i7rjpi2bpc91npk2na0yig1x2p")))

(define-public crate-dxfeed-0.1.6 (c (n "dxfeed") (v "0.1.6") (d (list (d (n "libdxfeed-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0r1fspksv7pgid1wdknbl4ra2aq75lrcz48bl983r36rdh1m9h9a")))

(define-public crate-dxfeed-0.1.7 (c (n "dxfeed") (v "0.1.7") (d (list (d (n "libdxfeed-sys") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0bgr9p7jh29gacllp2f6djhsckhw5llviiqvm9s4mz1n3yr6b22g")))

(define-public crate-dxfeed-0.1.8 (c (n "dxfeed") (v "0.1.8") (d (list (d (n "libdxfeed-sys") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0bvm82n0nqa77pjm9f64y2m86bykg68r6skikl4k6y4i1784xly9")))

(define-public crate-dxfeed-0.1.9 (c (n "dxfeed") (v "0.1.9") (d (list (d (n "libdxfeed-sys") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "023ffa85yg27dv58ygjx1hjwliax9nh0sbzq9g84zpb0xy9iigz1")))

(define-public crate-dxfeed-0.1.10 (c (n "dxfeed") (v "0.1.10") (d (list (d (n "libdxfeed-sys") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1cfm1qah2rfvwzc95p78jhj8vvgjrms9l769wkmr9k4cls18b3cr")))

(define-public crate-dxfeed-0.1.11 (c (n "dxfeed") (v "0.1.11") (d (list (d (n "libdxfeed-sys") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "18hjgwg6iyrk467pj03rzqavy2k7hx490ny0246nncjjrzrcrcqq")))

(define-public crate-dxfeed-0.2.0 (c (n "dxfeed") (v "0.2.0") (d (list (d (n "libdxfeed-sys") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "06970fx9scrmpf6shrq2hcl0g5qxpqpjgz3j8k4ycqb00xp82f02")))

(define-public crate-dxfeed-0.2.1 (c (n "dxfeed") (v "0.2.1") (d (list (d (n "libdxfeed-sys") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "1jn141r6iwhxzdhjxk5nka3dp14wk2imf9k6n7f2alsd357mfa5d")))

(define-public crate-dxfeed-0.2.2 (c (n "dxfeed") (v "0.2.2") (d (list (d (n "libdxfeed-sys") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "0hf6ph30sfwpa3xi5hw1lgb5i0rfad7d1q68nf3kdjyw2dawlrx3")))

(define-public crate-dxfeed-0.2.3 (c (n "dxfeed") (v "0.2.3") (d (list (d (n "libdxfeed-sys") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "027my4nlcxhi54rv7a6knjjq25s9k05fmflb7fxs9i7hvsa36xkf")))

