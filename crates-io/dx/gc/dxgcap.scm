(define-module (crates-io dx gc dxgcap) #:use-module (crates-io))

(define-public crate-dxgcap-0.0.2 (c (n "dxgcap") (v "0.0.2") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "10fvplzcdwy9zrg029p01l90d2s5p8w8j6ky6xs3dpi5can4ijx5")))

(define-public crate-dxgcap-0.0.3 (c (n "dxgcap") (v "0.0.3") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1jii019sijswd3kplx1pjhn1qa5mg5vyar30i8wrmczmspzl63jg")))

(define-public crate-dxgcap-0.0.4 (c (n "dxgcap") (v "0.0.4") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1zkk146qc2lc5jy05hg6wflpmcgyv3slj2v3lyxj3ij5xbggyvg4")))

(define-public crate-dxgcap-0.0.5 (c (n "dxgcap") (v "0.0.5") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "06l63p68vh9bpgn56q1c2pcca3rnw9dk0hfq8xnazpqaj5ncv8kk")))

(define-public crate-dxgcap-0.0.6 (c (n "dxgcap") (v "0.0.6") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1k7rlkak9ag5ax9jjcv861nyl06yccgy69x5ak2423samf9w89a9")))

(define-public crate-dxgcap-0.0.7 (c (n "dxgcap") (v "0.0.7") (d (list (d (n "d3d11-win") (r "*") (d #t) (k 0)) (d (n "dxgi-win") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "099z9xp3pps9kbq4mkvq0sh6vzxa1jzdwn2vd3y2y2wi3j85idb2")))

(define-public crate-dxgcap-0.0.8 (c (n "dxgcap") (v "0.0.8") (d (list (d (n "d3d11-win") (r "^0.2.1") (d #t) (k 0)) (d (n "dxgi-win") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.4") (d #t) (k 0)))) (h "16y5da5l4wgccfljnw7d12nv6a3g4v2ddj64463rp0gc6rbnm5pp")))

(define-public crate-dxgcap-0.1.0 (c (n "dxgcap") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0b2wql9hp8iy74763xip4hjp6xw750fkjnzyl9v7n6iw3nbk71fr")))

(define-public crate-dxgcap-0.2.0 (c (n "dxgcap") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0bf33yyhma2k8izmkmj6hqy6lw7pw7csw74y12bas0ml6k0mkhwg")))

(define-public crate-dxgcap-0.2.1 (c (n "dxgcap") (v "0.2.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0im2kk4zc901acm47nnvyr3mjwnvml7gfqfkdaykj3f0y6b8nyyf")))

(define-public crate-dxgcap-0.2.2 (c (n "dxgcap") (v "0.2.2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0m7vjqbxdghs10bmplwk1fjyc5kcsmdvrwhidv1albjka072bmir")))

(define-public crate-dxgcap-0.2.3 (c (n "dxgcap") (v "0.2.3") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "1c0clm6zj2r05s6k14yqp7whr7f0kqm8qc8hm804macik91mvr4q")))

(define-public crate-dxgcap-0.2.4 (c (n "dxgcap") (v "0.2.4") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0m66iaxzbik87p4aw6zqvs1imsndzsmn6z40sb4ff2f5a7gz78qq")))

