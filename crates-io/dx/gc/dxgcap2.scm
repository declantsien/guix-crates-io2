(define-module (crates-io dx gc dxgcap2) #:use-module (crates-io))

(define-public crate-dxgcap2-0.1.0 (c (n "dxgcap2") (v "0.1.0") (d (list (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0am8d3jxia9kf5zw01c9jhy9sil1j2vja7cr8chmnhvr4nyafn4f")))

(define-public crate-dxgcap2-0.1.1 (c (n "dxgcap2") (v "0.1.1") (d (list (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "1cjmvy75n0648vq29v75gvmzw1nysc7hy9nlk12jc3r8ia9dalnv")))

(define-public crate-dxgcap2-0.1.2 (c (n "dxgcap2") (v "0.1.2") (d (list (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0c8kdr8ps7ah0apwb4cybq2xy5n7z233xd6aaw2h7rzyngklrfi4")))

(define-public crate-dxgcap2-0.1.3 (c (n "dxgcap2") (v "0.1.3") (d (list (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0d75mrcq2zmcxcwyh1pddxlcj9ai8q4iq68nm22a6jff703pfwgd")))

(define-public crate-dxgcap2-0.1.4 (c (n "dxgcap2") (v "0.1.4") (d (list (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("d3d11" "d3dcommon" "dxgi" "dxgi1_2" "dxgitype" "ntdef" "unknwnbase" "windef" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "18rx718pwdwbwk1f4m71p168696mm2ng5wajcdcfk2z45m35k5vd")))

