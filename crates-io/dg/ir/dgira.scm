(define-module (crates-io dg ir dgira) #:use-module (crates-io))

(define-public crate-dgira-0.1.0 (c (n "dgira") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "gouqi") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0a5jxl449p2k6na46s3wc2dnc9c8mpvzjfq1rql9gjs6k849ymx7")))

(define-public crate-dgira-0.1.1 (c (n "dgira") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "gouqi") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1v1pzqvr4a3krrbhdx6asmmc0p2y46qwxq8hin4m1lyn3b82mqa5")))

(define-public crate-dgira-0.1.2 (c (n "dgira") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "gouqi") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "15gsr065l1gl3jzmwn70czkd4kwhgv2pc0by6dvfnr6ibp3anfvx")))

(define-public crate-dgira-0.1.3 (c (n "dgira") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "gouqi") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0qwkd9zxwqdnghz5avlqfns02y7wfqli06z0bpx4cdkg2248ipgp")))

