(define-module (crates-io dg e- dge-gen) #:use-module (crates-io))

(define-public crate-dge-gen-0.1.0 (c (n "dge-gen") (v "0.1.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1blcz2m37knagxy1hqkapnk0kb8nschmskrwnh6rc9fnhpb7c9jq")))

(define-public crate-dge-gen-0.2.0 (c (n "dge-gen") (v "0.2.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1zgkkaf496fd7axnjc8b6q78c4dxdwwpa9mp88p9f0xpmgsh06sa")))

