(define-module (crates-io dg ut dgut-reqwest) #:use-module (crates-io))

(define-public crate-dgut-reqwest-0.1.0 (c (n "dgut-reqwest") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbkp4mdxpm525blns9d2z2a0pr4qwkivf3i45l4cdphkn1h6l7c") (y #t)))

(define-public crate-dgut-reqwest-0.1.1 (c (n "dgut-reqwest") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "1nlriv0866kksldml96hb3w75nq5ypzs8dqn8axd6ysm5dy63bkl") (y #t)))

