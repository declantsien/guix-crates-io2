(define-module (crates-io dg #{65}# dg6502) #:use-module (crates-io))

(define-public crate-dg6502-0.1.0 (c (n "dg6502") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "03vhzm2jkyz7897pn2ykid0dgm2y316jkaaqij42y24hz7pdnn59")))

