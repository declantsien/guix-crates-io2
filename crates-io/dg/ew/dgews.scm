(define-module (crates-io dg ew dgews) #:use-module (crates-io))

(define-public crate-dgews-0.0.1 (c (n "dgews") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi"))) (d #t) (k 0)))) (h "10p51c8r1wpjv0z68vkwzyqw1s8grpjcvnppd3vwsaw558dfx8f1") (y #t)))

(define-public crate-dgews-0.0.2 (c (n "dgews") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0imx1m14yz2nzg9dwdbs6k6c8ahvjfaplf3msxmriihl67vgvply") (y #t)))

(define-public crate-dgews-0.0.3 (c (n "dgews") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0p588qdcs394w8axmp0wabsizpydpqjpg63g1j5xl0myknwrlfgr") (y #t)))

(define-public crate-dgews-0.0.4 (c (n "dgews") (v "0.0.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0r112gh1fd982xlsdmgsfljn41bmcmb8gc5jckxznnfn0g1lhiyn")))

(define-public crate-dgews-0.0.5 (c (n "dgews") (v "0.0.5") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0ihrvvfwlz6945xc4ss5jih2wnya4sv629rjmzii5dah3fsvhds8")))

(define-public crate-dgews-0.0.6 (c (n "dgews") (v "0.0.6") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0f0qkqrps7m68z75m7y86qfb2wkwpr0b7rjqdcgqvd8kacf57i6j")))

(define-public crate-dgews-0.0.7 (c (n "dgews") (v "0.0.7") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "03pr1rhkrzvj9sblnxwi1wx4lsp8q6p0lgrasqckk493z5r5c11z")))

(define-public crate-dgews-0.0.8 (c (n "dgews") (v "0.0.8") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1xq6w1jzyl4a5k3p439gfyn9dr6g0g0p7ggck94ach20bs73m4y5")))

(define-public crate-dgews-0.0.9 (c (n "dgews") (v "0.0.9") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1dgp0n4km7zqqiqfis72a1q8kq624mc5kv87pqb96mqsq6gafjgn")))

(define-public crate-dgews-0.0.10 (c (n "dgews") (v "0.0.10") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1zmc74gj1zrsinhvlddixhw9f4xm3d1l7rm91jingfx6dq74jabd")))

(define-public crate-dgews-0.0.11 (c (n "dgews") (v "0.0.11") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "19s5aw0nvjnqp1nqxa0597rxqkr9qzsgz5il9rmcx9z4l8i9zqw0")))

(define-public crate-dgews-0.0.12 (c (n "dgews") (v "0.0.12") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1jw2d8mhkbalfrpbj8jsw9xci2yk53lbx1b4x644c55fhz80qrd9")))

(define-public crate-dgews-0.0.13 (c (n "dgews") (v "0.0.13") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1imwabf9a2iv18yhlx4sv2aqgn4nrxrwls8bchgg07p1v8b53zqy")))

(define-public crate-dgews-0.0.14 (c (n "dgews") (v "0.0.14") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "12bwh2plrakagjfbkc1qb81lhydgsq2w8plp1pfxwm2bgx2h8fl5")))

(define-public crate-dgews-0.0.15 (c (n "dgews") (v "0.0.15") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0x2xgdn2misj12qp1zis6z3hgdgsflk5pf42b3z1a9wzf3cj7x3w")))

(define-public crate-dgews-0.1.0 (c (n "dgews") (v "0.1.0") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0yp28qq96kj010ywiycjl11zsd4w9p6ng52nmsa83hr6nfwzb3qy")))

(define-public crate-dgews-0.1.1 (c (n "dgews") (v "0.1.1") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0m3vl90pkdclg086wiih1f1fvgxya82jhc0kchqsfp9i7z4aa5hl")))

(define-public crate-dgews-0.1.2 (c (n "dgews") (v "0.1.2") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "0skik87j6ap6inpx702vl5g3641nbd1czp9pzrngdzhr7gnjgar7") (y #t)))

(define-public crate-dgews-0.1.3 (c (n "dgews") (v "0.1.3") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "1bappxbbac57lyaqsy7d3lw53ry1hagzmhbdc6dbh37rhmnfb2kl")))

(define-public crate-dgews-0.1.4 (c (n "dgews") (v "0.1.4") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "01v8yw8k6hj23jp5dlgrn0l0g0k5qdf1wpm7nhai879311h77f7j")))

(define-public crate-dgews-0.1.5 (c (n "dgews") (v "0.1.5") (d (list (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "wingdi" "winuser" "libloaderapi" "dwmapi"))) (d #t) (k 0)))) (h "09vcklvf3dwafi37wg29k28v1cpfjcma2ajshv020cb5wqfc8mgd")))

