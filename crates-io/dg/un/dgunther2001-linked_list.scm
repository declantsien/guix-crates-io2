(define-module (crates-io dg un dgunther2001-linked_list) #:use-module (crates-io))

(define-public crate-dgunther2001-linked_list-0.1.0 (c (n "dgunther2001-linked_list") (v "0.1.0") (h "0cj1w4cx7khxzn7j785vxbnyawazww7hxzmkbzd82fcnwzdm562k")))

(define-public crate-dgunther2001-linked_list-1.0.6 (c (n "dgunther2001-linked_list") (v "1.0.6") (h "00xm1z2bvrzmc9m76lha0vjdcgi977yzfcc6k0g33ykic7g6wh6d")))

(define-public crate-dgunther2001-linked_list-1.0.7 (c (n "dgunther2001-linked_list") (v "1.0.7") (h "0w3nrawqyqf1ik2pkkxm0psxiwsdnm6dhpx5g1sxavf9kgvzxmyp")))

(define-public crate-dgunther2001-linked_list-1.0.8 (c (n "dgunther2001-linked_list") (v "1.0.8") (h "0f0qzph1651fv5vxxz90dx08swxzhny25vhc71gc8dnh9ivpfc36")))

