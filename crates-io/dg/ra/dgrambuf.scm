(define-module (crates-io dg ra dgrambuf) #:use-module (crates-io))

(define-public crate-dgrambuf-0.1.0 (c (n "dgrambuf") (v "0.1.0") (h "1g61ksc2zz6g7irmh6zjzxf9z32yvhqgzs4gj5qmfr0q1am0z7mi")))

(define-public crate-dgrambuf-0.2.0 (c (n "dgrambuf") (v "0.2.0") (h "0asgzjb03g2rayzg2vrnzydpsx4qa0vpjdyn1hj34hvgg1702kgc")))

