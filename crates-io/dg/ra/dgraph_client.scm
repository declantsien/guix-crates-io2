(define-module (crates-io dg ra dgraph_client) #:use-module (crates-io))

(define-public crate-dgraph_client-0.1.0 (c (n "dgraph_client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "grpcio") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "slog") (r "^2") (d #t) (k 2)) (d (n "slog-async") (r "^2") (d #t) (k 2)) (d (n "slog-term") (r "^2") (d #t) (k 2)))) (h "1w0j25jkzp1fyhr1b059ja09cw09d5z7js7l3i9n10761sy6a0a8")))

