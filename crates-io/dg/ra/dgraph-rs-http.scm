(define-module (crates-io dg ra dgraph-rs-http) #:use-module (crates-io))

(define-public crate-dgraph-rs-http-0.1.0 (c (n "dgraph-rs-http") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1q96cabpff09y3ck5ch4jhl3gp7q90dkr9l5bqfgjxhls3mby5vr") (y #t)))

(define-public crate-dgraph-rs-http-0.1.1 (c (n "dgraph-rs-http") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "078i11ygrx3hfah8ggppax5jq1wjb30ixlli6806z096qhm42bzw")))

