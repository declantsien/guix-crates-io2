(define-module (crates-io dg ra dgraph_query_lib) #:use-module (crates-io))

(define-public crate-dgraph_query_lib-0.1.0 (c (n "dgraph_query_lib") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mixjn4knpxr7s0yjzyh91wrfzp57xcccl45i6c3fqarn79iv03z")))

