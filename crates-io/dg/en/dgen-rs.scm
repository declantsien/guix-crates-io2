(define-module (crates-io dg en dgen-rs) #:use-module (crates-io))

(define-public crate-dgen-rs-0.1.0 (c (n "dgen-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11c9hlpg7d8hmbj6299wi5pfi66qfai2anrsqxpzg47wi8ddfbw9")))

