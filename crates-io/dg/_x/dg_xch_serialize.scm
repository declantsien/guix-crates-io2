(define-module (crates-io dg _x dg_xch_serialize) #:use-module (crates-io))

(define-public crate-dg_xch_serialize-1.0.2 (c (n "dg_xch_serialize") (v "1.0.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0sfzsv2dd9bcdwrbgiq8ljcnj2m51p7crwmrrhm8rwh7yc0y626b")))

(define-public crate-dg_xch_serialize-1.0.4 (c (n "dg_xch_serialize") (v "1.0.4") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1g6b41xnp54faqs5jd9c7xfp57287gzpq5fgr0v7hafpm17y8mlh")))

(define-public crate-dg_xch_serialize-1.0.5 (c (n "dg_xch_serialize") (v "1.0.5") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "17f38ysl5ylpy6vx5ifb0m58w9nn5zb9krjvj6kxpkhn6z4gkdl8")))

(define-public crate-dg_xch_serialize-1.0.7 (c (n "dg_xch_serialize") (v "1.0.7") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1aiz28ql752szypjmb3xf16zyj1by2yh3l5whbgn6vcq5c06ws5m")))

(define-public crate-dg_xch_serialize-1.0.9 (c (n "dg_xch_serialize") (v "1.0.9") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11haqb9gchsy6v3q60j18amns8ma5m9mpvhaav85hwjaflk97zna")))

(define-public crate-dg_xch_serialize-1.1.0 (c (n "dg_xch_serialize") (v "1.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1sdlc9lkkdaqdv6cxmwxrgj7a9b145pfc09p3d53cbayysal7js7")))

(define-public crate-dg_xch_serialize-1.1.1 (c (n "dg_xch_serialize") (v "1.1.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1z4kgfpfla2izbmbkbdp2liwaf76cfqsvisvvsl0fj4gv82v1w5q")))

(define-public crate-dg_xch_serialize-1.1.2 (c (n "dg_xch_serialize") (v "1.1.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1g2gxv8giqpf5jaxim5zw9nlaj31vgqhzgcl6lr9p7md62b7lp28")))

(define-public crate-dg_xch_serialize-1.1.3 (c (n "dg_xch_serialize") (v "1.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1wcmcm4dg4rackkykq8r8ds1km7aj11szw3ylp2b02zk6fgkp7bl")))

(define-public crate-dg_xch_serialize-1.2.0 (c (n "dg_xch_serialize") (v "1.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1flkrjdy9rfllrn5hcbmrc5fczyks6hy2dk2va63cpfnncwbkdrl")))

(define-public crate-dg_xch_serialize-1.2.1 (c (n "dg_xch_serialize") (v "1.2.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1445hh1h2v2piwd6zxfgw82fcqckniz35pbj3vqlyksj59bd78gp")))

(define-public crate-dg_xch_serialize-2.0.0 (c (n "dg_xch_serialize") (v "2.0.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0f3dg8pfzwlxg17dxnc5mblc5dm74chav31yx40nx6fjjarglfdr")))

(define-public crate-dg_xch_serialize-2.0.1 (c (n "dg_xch_serialize") (v "2.0.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1r3mkfpjglpmvqhqdrzjf7niaga0dmxs25icyka48qm1kkli4pac")))

(define-public crate-dg_xch_serialize-2.0.2 (c (n "dg_xch_serialize") (v "2.0.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "12yanvk799x1561dqdj48fm5vxs0dg8j8681zbfcp9id4x0lcb84")))

(define-public crate-dg_xch_serialize-2.0.3 (c (n "dg_xch_serialize") (v "2.0.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1271m2ix1fhp2js01szb9k5ra07jy4fp20g99p99c77qjckqfcz9")))

