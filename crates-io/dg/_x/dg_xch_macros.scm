(define-module (crates-io dg _x dg_xch_macros) #:use-module (crates-io))

(define-public crate-dg_xch_macros-1.0.2 (c (n "dg_xch_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rrb0ikzqi32fsys3d4nr61r89cwpa82ky1jb7xz9l8rimdjnrrb")))

(define-public crate-dg_xch_macros-1.0.4 (c (n "dg_xch_macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1whgqp2w4wgj85adyvk209jcpdg65a4fl8kh9i6d8xlrw7hzwg8v")))

(define-public crate-dg_xch_macros-1.0.5 (c (n "dg_xch_macros") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13wrh8p2y5pfr72lrz6kf5mh6vbffhvw489yfpmngzyjj1iz7pf8")))

(define-public crate-dg_xch_macros-1.0.7 (c (n "dg_xch_macros") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0afc6i5bxwzgjd99fki10vbgngycbhzrrxs4ygp2cj7gmic3dv0d")))

(define-public crate-dg_xch_macros-1.0.9 (c (n "dg_xch_macros") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1788kv50sx1bn57hakz5mbvx8hz96s6zmfp0py7lc6mzcag1p4ch")))

(define-public crate-dg_xch_macros-1.1.0 (c (n "dg_xch_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1wa4igf666y7c2mqyni1j3s3n6z2jd3j69s5khxbz0wp7jr0d0fh")))

(define-public crate-dg_xch_macros-1.1.1 (c (n "dg_xch_macros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0gfp8psm9h2bqxxynfr6xds3qj6pshzsn64zpcbvnmqzgl9ax4sh")))

(define-public crate-dg_xch_macros-1.1.2 (c (n "dg_xch_macros") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0vbr0nfqgzxdc2sagb8mmz5vp5ja7lv0hwvbw4ggikkf0qc0sb1r")))

(define-public crate-dg_xch_macros-1.1.3 (c (n "dg_xch_macros") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0cr4ppf8p6fkaba7l2nc0mkyhjhxwdamdi38qra5p2rvfy3rv7q2")))

(define-public crate-dg_xch_macros-1.2.0 (c (n "dg_xch_macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0wq14j7fvhx2bpdgpblk1hjp9ks1nhfqwhz6xizwwjznv8rrvbnm")))

(define-public crate-dg_xch_macros-1.2.1 (c (n "dg_xch_macros") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0vp7i04j6qyjjvbd8bysap8djqnxiifcaipks1fryak8lq4j2miz")))

(define-public crate-dg_xch_macros-2.0.0 (c (n "dg_xch_macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "08rs6q43x2p2f8k78klrdky44nqli4x28sv0rbzshm2fc1q5a4qh")))

(define-public crate-dg_xch_macros-2.0.1 (c (n "dg_xch_macros") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0yl9gymzji1x8v5baxq3ha2154cvadnrsnflkhwiwqmll04rbv9p")))

(define-public crate-dg_xch_macros-2.0.2 (c (n "dg_xch_macros") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0bjpj96cyss7mssqm9aq4y0mvq9y2gbrhn6mmz4rncvajyiv826k")))

(define-public crate-dg_xch_macros-2.0.3 (c (n "dg_xch_macros") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "000xh24w1n0b1yfh7yidcyfp5dhicqkj9vpv91njkxj1hvaw1fjs")))

