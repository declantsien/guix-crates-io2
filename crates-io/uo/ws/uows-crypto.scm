(define-module (crates-io uo ws uows-crypto) #:use-module (crates-io))

(define-public crate-uows-crypto-0.1.0 (c (n "uows-crypto") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "09ih6d3ivyl6hy4v9dw663250aliml4jkir1nqssjhvcp0bx0iik") (y #t)))

(define-public crate-uows-crypto-0.1.1 (c (n "uows-crypto") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "1575d0pa8d5av9cx3pi35z09lvx198ymx9y61d8vc01ql1jylxb4") (y #t)))

(define-public crate-uows-crypto-0.1.2 (c (n "uows-crypto") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "17x6b7cdbvyisza4i3h47rls6gyfxwjpijgdk83vna0mc7agdnbj") (y #t)))

(define-public crate-uows-crypto-0.1.3 (c (n "uows-crypto") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "1vlhx41xvpshkhdh7symfjzr9av95jwgl5641s324g85zy8gnnbm") (y #t)))

(define-public crate-uows-crypto-0.1.4 (c (n "uows-crypto") (v "0.1.4") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "0q6yc78myx13f6i5d82khxk3yy56jy1zv4dl8dig76lm8cy58iy4") (y #t)))

(define-public crate-uows-crypto-0.1.5 (c (n "uows-crypto") (v "0.1.5") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "0qjvqj1p5kapfkwvc6v5hdj7sxar3ivnk6szw7lnzx517qqzc9pa") (y #t)))

(define-public crate-uows-crypto-0.2.0 (c (n "uows-crypto") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "19k9nrd7q0jq9iwzgmirss061mmzl64rvgihzb80vd49bpzh550y") (y #t)))

(define-public crate-uows-crypto-0.2.1 (c (n "uows-crypto") (v "0.2.1") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "0przc29258g3cwn5hv9w5x7y4jy53g8b5a5l0cajky54wxmx4ika") (y #t)))

(define-public crate-uows-crypto-0.2.2 (c (n "uows-crypto") (v "0.2.2") (d (list (d (n "aes-gcm") (r "^0.8") (d #t) (k 0)))) (h "1b91yhmc1g7kg32d569rpi5kpvhyanxwr0z11nyviyl3msa5i4c5") (y #t)))

