(define-module (crates-io yy id yyid) #:use-module (crates-io))

(define-public crate-yyid-0.1.0 (c (n "yyid") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10hdvndf9x9y400n1sy674zi8zg83gshw0rv9zypi5yd43w28xqh")))

(define-public crate-yyid-0.1.1 (c (n "yyid") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "18skasbl84iibvr890sr43c904jr560arxzgjrp6h1mvhap41cb9")))

(define-public crate-yyid-0.1.2 (c (n "yyid") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03fbxskc78kps5w4vc7jvxbpjsym106r8qmf3qvp7g2g8426lqal")))

(define-public crate-yyid-0.1.3 (c (n "yyid") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ms1snw282g84pq0k8hbq8sy03jhdgdrlb0v7z6ms1ls9jidcl8n")))

(define-public crate-yyid-0.2.0 (c (n "yyid") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0kw4bs0g3sb73p4wm1gb09q5pn7y0dsgbci202y0vx0qr0406vf8")))

(define-public crate-yyid-0.2.1 (c (n "yyid") (v "0.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ypw3fn5vrpwv08pv77778i3fc4s2607m0s558w6nj7n3r54n4jz")))

(define-public crate-yyid-0.2.2 (c (n "yyid") (v "0.2.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "035f1498wa99awyxciyhj7vyxx4xdaa5xali4hpnasrn8acwjxcm")))

(define-public crate-yyid-0.2.3 (c (n "yyid") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10012jkjrgvap9ax0a6yrlqjimkdb73i7w0v0w9bhczjbzmzp2rb")))

(define-public crate-yyid-0.2.4 (c (n "yyid") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "15ihjcl4yxmfx781jirdjzwhs6v2ry55g9fndrfyix1hpbhxmwr7")))

(define-public crate-yyid-0.3.0 (c (n "yyid") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19ck7v439x70fvmq5211v88s4ddcsggaa8269ql35a5mgmf55wby")))

(define-public crate-yyid-0.4.0 (c (n "yyid") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dc6f3g43f38438bcgrj7pk0g0q60zz8l6flv9pch3hpchvpykil")))

(define-public crate-yyid-0.5.0 (c (n "yyid") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n3y6kvcq7kxgrnqwbgw6sgqxzqj8a6ap6sigk4gdjyk6xr7fa2n")))

(define-public crate-yyid-0.6.0 (c (n "yyid") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1808ywys6mr6nljpm8v9333lg4q7j36s99r2yk9x2wvb7kr8z767") (f (quote (("std"))))))

(define-public crate-yyid-0.7.0 (c (n "yyid") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "08d6v1pafwa5dwb2hidj8p7adihaicpn36ixrwx0jbj6n0yyjsd0") (f (quote (("std") ("fast-rng" "rand") ("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

