(define-module (crates-io nj id njid) #:use-module (crates-io))

(define-public crate-njid-0.1.0 (c (n "njid") (v "0.1.0") (d (list (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06828k5js7ska78rgqh376aw1pin4m32zrgqrgmr5yfxglxanikg") (y #t)))

