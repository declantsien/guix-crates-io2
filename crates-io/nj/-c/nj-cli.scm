(define-module (crates-io nj -c nj-cli) #:use-module (crates-io))

(define-public crate-nj-cli-0.1.0 (c (n "nj-cli") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (k 0)))) (h "030pyp7f14rp5jcp4cn5hh3nsdr97y74fghx6ygbkfa2pq5jakr8")))

(define-public crate-nj-cli-0.1.1 (c (n "nj-cli") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (k 0)))) (h "1bhpwzfnc0bad2m6gryckhajk9bx34yv7y39xs4ac2iggvkx5g58")))

(define-public crate-nj-cli-0.1.2 (c (n "nj-cli") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (k 0)))) (h "0bqsd6gdxfmrla7lfcr748m84kb271r6h485hfv5vfpsaxfj5ilg")))

(define-public crate-nj-cli-0.2.0 (c (n "nj-cli") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (k 0)))) (h "0h640gs5ipvqpnsvkjw60ac6s5m23z8lfk9ahwmg6acm7yh7f8dd")))

(define-public crate-nj-cli-0.3.0 (c (n "nj-cli") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (k 0)))) (h "0nck7f8g1cwcwh997wx9blaaqpmfi6028hlr4ldv3dggywka13kp")))

(define-public crate-nj-cli-0.4.0 (c (n "nj-cli") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "10ki84xnk43li9rcd48plg1i0h0xwzymlhbjkwv8ab7caysg6yzl")))

(define-public crate-nj-cli-0.4.1 (c (n "nj-cli") (v "0.4.1") (d (list (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1s5isi1l3zpx16c8dlpls86z34zl4qwnwnjxm1v4gw2hh8a2g7gz")))

(define-public crate-nj-cli-0.4.2 (c (n "nj-cli") (v "0.4.2") (d (list (d (n "cargo_metadata") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0zy1rjzz96565zpylacj48kz82m7gwck1r79bkhn7139vmqlzg1n")))

(define-public crate-nj-cli-0.4.3 (c (n "nj-cli") (v "0.4.3") (d (list (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "1jgsp7bsrkq2la7qn037w0xn3r3zx09xq47wgg39jigdnv1y2605")))

