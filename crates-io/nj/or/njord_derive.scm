(define-module (crates-io nj or njord_derive) #:use-module (crates-io))

(define-public crate-njord_derive-0.1.0 (c (n "njord_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "05fl4dr23y6x9pb6aasz01mhivhggb3ps78bib9s73ar2n1j3b64")))

(define-public crate-njord_derive-0.2.0 (c (n "njord_derive") (v "0.2.0") (d (list (d (n "njord") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1k7bfjpamblysldx3czqvnkw8j5kmhy4haz8gp9sg4kv4dmb2303") (r "1.74.0")))

(define-public crate-njord_derive-0.2.1 (c (n "njord_derive") (v "0.2.1") (d (list (d (n "njord") (r "^0.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0yplxmchgpgc7fh39rp29a3li6lrfzriq1p0afhb7g1xgvlrc0sl") (r "1.74.0")))

(define-public crate-njord_derive-0.3.0 (c (n "njord_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0jxalbsbmbc2fnjp7843qxnswf9rk200r20xckd22pfbfnmxvwf7") (r "1.77.1")))

