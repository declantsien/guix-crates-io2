(define-module (crates-io nj or njord) #:use-module (crates-io))

(define-public crate-njord-0.1.0 (c (n "njord") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "njord_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "njord_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "0602jvpf7889gdrrlv6d67nff488337x2686z0cyn4n19ipmgkps")))

(define-public crate-njord-0.2.0 (c (n "njord") (v "0.2.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "njord_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "njord_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0z92js6x3flqcr2l6w8d3ww69cwwia08z5arvj1xi75n8p9yzwm2") (f (quote (("derive" "njord_derive") ("default" "derive")))) (r "1.74.0")))

(define-public crate-njord-0.2.1 (c (n "njord") (v "0.2.1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "njord_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "njord_derive") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1cwmdn4pndgsdld20x39zz3qdbcrqjdw717d4j32scdqca7xvrah") (f (quote (("derive" "njord_derive") ("default" "derive")))) (r "1.74.0")))

(define-public crate-njord-0.3.0 (c (n "njord") (v "0.3.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "njord_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "njord_derive") (r "^0.3.0") (d #t) (t "cfg(any())") (k 0)) (d (n "njord_derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0pwppggmlv871s4098dhb47yb1dgaqgw7i7mpi939li10dcg2866") (f (quote (("sqlite") ("default" "sqlite")))) (r "1.77.1")))

