(define-module (crates-io nj ec nject) #:use-module (crates-io))

(define-public crate-nject-0.1.0 (c (n "nject") (v "0.1.0") (d (list (d (n "nject-macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1m0z1ayiwa4jg0wsj6gh3bhsvg96vywzx4psgqs0ln5nkhj7ax1n") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.2.0 (c (n "nject") (v "0.2.0") (d (list (d (n "nject-macro") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0psr0zarw1v77fzwy8fp6lqd1z9g3hvwnzgyck69cdj575v6z4v8") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.2.1 (c (n "nject") (v "0.2.1") (d (list (d (n "nject-macro") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nnm2vbhnd85hrcaw93lzj6dl2k37y1gdk8pbjqkvnkplhvfpbqs") (f (quote (("default" "macro")))) (y #t) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.2.2 (c (n "nject") (v "0.2.2") (d (list (d (n "nject-macro") (r "^0.2") (o #t) (d #t) (k 0)))) (h "06m6j6a5zh15jaxzv0sgxcyqp611ghhi7l4zmn3xjps0g1g2fnkw") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.2.3 (c (n "nject") (v "0.2.3") (d (list (d (n "nject-macro") (r "^0.2") (o #t) (d #t) (k 0)))) (h "19p9hhny1cdm84shriqdn29fawbq488s64zpv585xyll9fg927b1") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.3.0 (c (n "nject") (v "0.3.0") (d (list (d (n "nject-macro") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1irjk10qq8gr5hwnm2233v2n4lj2l07fzlmypd4g55fg6z5y3xjr") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.3.1 (c (n "nject") (v "0.3.1") (d (list (d (n "nject-macro") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "1nfhackv54ap49yz183jbs3vxcs5ypgz3gvvhkhxkg5c43fd6hwx") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.3.2 (c (n "nject") (v "0.3.2") (d (list (d (n "nject-macro") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "18bcj1dwnqx4jzhh47wmp11vbij26bl9s8v9qinwdb4xac9v1cd0") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.4.0 (c (n "nject") (v "0.4.0") (d (list (d (n "nject-macro") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1nkivqwg5cyrs469bwvdc2lbns1njd6wam7vxps8r2a884acgp1v") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.4.1 (c (n "nject") (v "0.4.1") (d (list (d (n "nject-macro") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1bxkw16pm6jhkivadakfany066njn1y6p5kg08gw7qih7rzzzvv0") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.4.2 (c (n "nject") (v "0.4.2") (d (list (d (n "nject-macro") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1zpc2k8f0kbwfg3myd11bmvyvzxddvy1kdfrbwhgqp2hc7fms854") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

(define-public crate-nject-0.4.3 (c (n "nject") (v "0.4.3") (d (list (d (n "nject-macro") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0941z8kfj5zg3hk04d6qsna44yrhnz888rv926cqpr0xhwaiiv3x") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:nject-macro")))) (r "1.60")))

