(define-module (crates-io nj ec nject-macro) #:use-module (crates-io))

(define-public crate-nject-macro-0.1.0 (c (n "nject-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gb28dm8zy3jkx2kc9gcwndi787hydk7b2wxahgwlpalf3arrp1q") (r "1.60")))

(define-public crate-nject-macro-0.2.0 (c (n "nject-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2b95pykl179xs2pqzgfk87nk4bz1d9pzm88liw6as5cyq736hf") (r "1.60")))

(define-public crate-nject-macro-0.2.1 (c (n "nject-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rp9nqzg0rshrfmng4z35laki2ad92zlk6yig2hs1xwzcnlyxiff") (y #t) (r "1.60")))

(define-public crate-nject-macro-0.2.2 (c (n "nject-macro") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0707gj2hxrgym0d1f6708y8aa62fap7qp9wmzxkz4cpdqr6fgf4x") (r "1.60")))

(define-public crate-nject-macro-0.2.3 (c (n "nject-macro") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hg2qx7nzpyv8rq660wsmyj9w1zi6r0sgxhxpqz7lwcbzvvwfyw2") (r "1.60")))

(define-public crate-nject-macro-0.3.0 (c (n "nject-macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "044vxq82vv3vav80400qmib1yrpghjslwdwblvwsbp4nyy1s6yjv") (r "1.60")))

(define-public crate-nject-macro-0.3.1 (c (n "nject-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kzy3739aagvvzkvsnazcc4x5w1p8rfpmwc4jaqyj5p8kdh9p8zj") (r "1.60")))

(define-public crate-nject-macro-0.3.2 (c (n "nject-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ipgdw98mizrpwr961rd910963dh0pib17j8hl04rdfyz27hn6mk") (r "1.60")))

(define-public crate-nject-macro-0.4.0 (c (n "nject-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06x6kp794pb5n661pv8x3f31aja2il8zir6ccgqw0q2dcj62pbpr") (r "1.60")))

(define-public crate-nject-macro-0.4.1 (c (n "nject-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fmy47naqshpmlh1jmakc9pncdvwlh4cwx870bm438a1lqrawa09") (r "1.60")))

(define-public crate-nject-macro-0.4.2 (c (n "nject-macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2l2hmavyfl54l66qsi746zcagi3picm4n089lz2n84qma4dyhs") (r "1.60")))

(define-public crate-nject-macro-0.4.3 (c (n "nject-macro") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gblmg9hcdp5361g4zg7m9xmvnz683zcz887ll932h6dvqb0gn8w") (r "1.60")))

