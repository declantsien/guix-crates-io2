(define-module (crates-io nj -b nj-build) #:use-module (crates-io))

(define-public crate-nj-build-0.1.0 (c (n "nj-build") (v "0.1.0") (h "0swqim1w63841yl0025cy44iflnrbm0q8qjb6bpdh3wjkh39c6g7")))

(define-public crate-nj-build-0.2.0 (c (n "nj-build") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1917lf1mgril49ak8syc7b4s1wqkm9dqddn3g42n3kn040sbh5kg")))

(define-public crate-nj-build-0.2.1 (c (n "nj-build") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (t "cfg(windows)") (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02mk2gdw5k4350fdv23gfivxigrx0mx1nz67nb14xvnxjaqvn3rv")))

(define-public crate-nj-build-0.2.2 (c (n "nj-build") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.58") (d #t) (t "cfg(windows)") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1026h4g98yfl3h82zzk6hg169lrr8kb34729w1xarvadgdm1rshl")))

(define-public crate-nj-build-0.2.3 (c (n "nj-build") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.58") (d #t) (t "cfg(windows)") (k 0)) (d (n "http_req") (r "^0.7.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1wabfff73pajr21cnxh9xk49rn0g0j2v2bhv58rrl54w8ykppldp")))

(define-public crate-nj-build-0.3.0 (c (n "nj-build") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.58") (d #t) (t "cfg(windows)") (k 0)) (d (n "http_req") (r "^0.9.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1jp9nnj4dj97ahscm9ndrj95c5x1gsnq8sx8j32j22ia2r0g31zm")))

