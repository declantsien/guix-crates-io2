(define-module (crates-io nj -s nj-sys) #:use-module (crates-io))

(define-public crate-nj-sys-0.1.0 (c (n "nj-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0xj2d2ls4yjz20cxvi5v37gllbpki4qk4kkh2ixvjhr9bf2bwd6c")))

(define-public crate-nj-sys-1.0.0 (c (n "nj-sys") (v "1.0.0") (h "1m5k2qxga4pbaa5hxas095vcg87h8irn7y5prqgpyj2n9vs8cm16")))

(define-public crate-nj-sys-2.0.0 (c (n "nj-sys") (v "2.0.0") (h "1kc2q4138g3pc8nz9qw9ywvq360rl4c3lrfv8cl21jzpvfj3wnkn")))

(define-public crate-nj-sys-3.0.0 (c (n "nj-sys") (v "3.0.0") (h "0lfdn7vf22bvll09vzi2idcqvzhvnvwq5qkk88i4yalakw78zrwd")))

(define-public crate-nj-sys-4.0.0 (c (n "nj-sys") (v "4.0.0") (h "1balym721iqjs5wgas9i04ch864l5sb54ds8l79nsqcd34y8jpqf")))

