(define-module (crates-io nj -d nj-derive) #:use-module (crates-io))

(define-public crate-nj-derive-0.1.0 (c (n "nj-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0f0sksv2zvcl7y46prx3rw8pccnzv782jcbqsjlj54888fc1wiqr")))

(define-public crate-nj-derive-1.0.0 (c (n "nj-derive") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0qywxb408ya92zvp11gp5l0j42laf2nlx61pkh00lkrgll1v878p")))

(define-public crate-nj-derive-2.0.0 (c (n "nj-derive") (v "2.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "flv-future-aio") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0kqxpqfigf01lc000qj59ckrk5jgijrchclx4qwcqnq4s6disa71")))

(define-public crate-nj-derive-2.0.1 (c (n "nj-derive") (v "2.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "flv-future-aio") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0wm1fhxwrpqx50lq45iqs0qlv3aw6cr90mnlwn14j1d66mnjk5rw")))

(define-public crate-nj-derive-2.1.0 (c (n "nj-derive") (v "2.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "flv-future-aio") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0xrkg5bia6pw7xiqcccjp320l83iaqyz25wjnv22w2kqfhm12h9m")))

(define-public crate-nj-derive-2.1.1 (c (n "nj-derive") (v "2.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "flv-future-aio") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "00561bhs40az627s7rs0363i1jp64ks4sdai1vqada09j96dx1xh")))

(define-public crate-nj-derive-3.0.0 (c (n "nj-derive") (v "3.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "flv-future-aio") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "08q1hj3r1bkgj2f7gdcvkjs8npcahavcfczf227m1rd2f2ajvw16")))

(define-public crate-nj-derive-3.1.0 (c (n "nj-derive") (v "3.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fluvio-future") (r "^0.2.0") (f (quote ("timer"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1qqqf2hfybz944rj8d4j667yqvi0c41dcmm2flhp6hn3c3afs4k9")))

(define-public crate-nj-derive-3.2.0 (c (n "nj-derive") (v "3.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fluvio-future") (r "^0.2.0") (f (quote ("timer"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1f8az2s400a1agl2m16gjn9wf501yc1g0rj5qw4xh140bcby19k7")))

(define-public crate-nj-derive-3.4.1 (c (n "nj-derive") (v "3.4.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fluvio-future") (r "^0.3.0") (f (quote ("timer"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "09dhaxsn1svddkcs0jdyjpi19ccr1fyya0wmaripp104h4jd6fd6")))

(define-public crate-nj-derive-3.4.2 (c (n "nj-derive") (v "3.4.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "fluvio-future") (r "^0.6.0") (f (quote ("timer"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0czw90wv3gan9ar4cazcaq3ryl6hv4jk257ahr2dp733rr1pj489")))

