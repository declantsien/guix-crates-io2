(define-module (crates-io km s_ kms_rs) #:use-module (crates-io))

(define-public crate-kms_rs-0.1.0 (c (n "kms_rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_kms") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "10qsan5in4gs2z53w3cd99wfyxqd2dnidwzxyhzf8amj6h3ijchx")))

(define-public crate-kms_rs-0.2.0 (c (n "kms_rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_kms") (r "^0.45.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "0rc832mcra1h1azrw72qyf3a0gfa2hj4ba0yak1lsr01b7ydc99m")))

