(define-module (crates-io km l_ kml_to_fgfp) #:use-module (crates-io))

(define-public crate-kml_to_fgfp-0.1.0 (c (n "kml_to_fgfp") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1j507f86f53i8w0h0pyxwq0smz7wfrdgg6bgc2qxgyrhc1crirsg")))

(define-public crate-kml_to_fgfp-0.2.0 (c (n "kml_to_fgfp") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0p2rf15fgz7rzdr0z1558fdmz6dc72gx54bx3gmg0sdpqnbcb840")))

(define-public crate-kml_to_fgfp-0.3.1 (c (n "kml_to_fgfp") (v "0.3.1") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1l47dvpmp0jaly3sg2j3mgd238w35676v3p68pbdbikbvfn8b001")))

