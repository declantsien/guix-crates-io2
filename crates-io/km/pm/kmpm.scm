(define-module (crates-io km pm kmpm) #:use-module (crates-io))

(define-public crate-kmpm-0.1.0 (c (n "kmpm") (v "0.1.0") (h "0jr4mjb6aqsz64q60x6b2sdn7yp5njdws98nm0jkviiaz01ycinf")))

(define-public crate-kmpm-0.1.1 (c (n "kmpm") (v "0.1.1") (h "1yr2am7785vahnzrgirgipqvw3qghab4x28n2lw9k861847fv74z")))

(define-public crate-kmpm-0.1.2 (c (n "kmpm") (v "0.1.2") (h "14m6rc23lmcqs00ag4pz86dlx681vvj137n30d737mzskxvar0ig")))

(define-public crate-kmpm-0.2.0 (c (n "kmpm") (v "0.2.0") (h "1x2w7m7c7jrbp8wlsxyyxxwzkgh16s4vg895rwhmz7f6906przna")))

(define-public crate-kmpm-0.2.1 (c (n "kmpm") (v "0.2.1") (h "0n8m76qzk9pilk46g9vg663ilzsrnamnjlhxjhajw5i0pcrv6k18")))

(define-public crate-kmpm-0.2.2 (c (n "kmpm") (v "0.2.2") (h "02phrkbj46ql0s1hl5vnikaj0ibdcij9hy77ab978qgbd1k11vgg")))

