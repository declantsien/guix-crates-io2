(define-module (crates-io km ea kmeans-color-gpu-cli) #:use-module (crates-io))

(define-public crate-kmeans-color-gpu-cli-0.1.0 (c (n "kmeans-color-gpu-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 0)) (d (n "kmeans-color-gpu") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1qvsm9bfsna8aji7ll2nb75d0p56wak4wnqw3f5ny8sg7dsbaskx")))

