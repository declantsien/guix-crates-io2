(define-module (crates-io km ea kmeans) #:use-module (crates-io))

(define-public crate-kmeans-0.1.0 (c (n "kmeans") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0") (d #t) (k 0)) (d (n "packed_simd") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "superslice") (r "^1") (d #t) (k 0)))) (h "0jyva5v8228rwcn811daccwlfm8299vs7cmlpvn6c1sbyazcmwbh") (y #t)))

(define-public crate-kmeans-0.2.0 (c (n "kmeans") (v "0.2.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "085jr5h4sdavkxk3j6mqfdlga3jfc8017lmn51mfqr78z7x873kp")))

(define-public crate-kmeans-0.2.1 (c (n "kmeans") (v "0.2.1") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.9") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0lh3ww7gi5advs5fl1pzw3wixk4j67kr2nai3arz3gflib8wdk3n")))

