(define-module (crates-io km ea kmeans-color-gpu) #:use-module (crates-io))

(define-public crate-kmeans-color-gpu-0.1.0 (c (n "kmeans-color-gpu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (f (quote ("derive" "extern_crate_alloc"))) (d #t) (k 0)) (d (n "gif") (r "^0.12") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 2)) (d (n "kmeans-color-gpu-preprocessor") (r "^0.1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (f (quote ("as-bytes"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)))) (h "16xvvg3gwhhp3vzypxm33g7csyc7m3q5ww1ycjl5b4l785blh91j")))

