(define-module (crates-io km ea kmeans-color-gpu-preprocessor) #:use-module (crates-io))

(define-public crate-kmeans-color-gpu-preprocessor-0.1.0 (c (n "kmeans-color-gpu-preprocessor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "naga") (r "^0.13") (f (quote ("validate" "wgsl-in"))) (o #t) (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "0qjibgikfm7y5gdfq668dqxi5k9z45bbwqv3m6zlzha5cn4v69ci") (f (quote (("validate" "naga" "codespan-reporting"))))))

