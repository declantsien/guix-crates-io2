(define-module (crates-io km er kmers) #:use-module (crates-io))

(define-public crate-kmers-0.2.1 (c (n "kmers") (v "0.2.1") (d (list (d (n "noodles") (r "^0.52.0") (f (quote ("core" "fasta"))) (d #t) (k 2)))) (h "137kssyv4nvx0fivjvcbqidbgrkl3ffr2m7ai3hs34mxgzslly5q")))

(define-public crate-kmers-0.2.2 (c (n "kmers") (v "0.2.2") (d (list (d (n "noodles") (r "^0.52.0") (f (quote ("core" "fasta"))) (d #t) (k 2)))) (h "14x0v6w2jgywqa6wl7921ixrpmd194n49msgxip1d3w9wqfl60j4")))

