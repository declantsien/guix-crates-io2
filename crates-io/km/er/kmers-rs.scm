(define-module (crates-io km er kmers-rs) #:use-module (crates-io))

(define-public crate-kmers-rs-0.1.0 (c (n "kmers-rs") (v "0.1.0") (h "0snhwrj9m0hsxhf7nn399z3mr2yh5x9lhf3zkmkpras6mir7kgmz")))

(define-public crate-kmers-rs-0.2.0 (c (n "kmers-rs") (v "0.2.0") (h "1x9a9f2wwp7f5yxn7i0imz76yj5wybbr5j7ll6pd50arl46db493")))

