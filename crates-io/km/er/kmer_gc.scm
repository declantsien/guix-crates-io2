(define-module (crates-io km er kmer_gc) #:use-module (crates-io))

(define-public crate-kmer_gc-0.1.0 (c (n "kmer_gc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1ifib53sd5kgg3fcilyxgs90hw6npl4pyg8j5lgpx4w1ihy6pgp2")))

(define-public crate-kmer_gc-0.1.1 (c (n "kmer_gc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "196nvfyxaj6y9pxdg3rmd7pqkvg8jlh1m0gx7vwa82m3lm41jfgd")))

(define-public crate-kmer_gc-0.1.2 (c (n "kmer_gc") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1yhp5kbn6zpb48m5shcgl672aswx38a4f7bf54zws3z8qrvzna49")))

