(define-module (crates-io km er kmer-papa) #:use-module (crates-io))

(define-public crate-kmer-papa-0.1.0 (c (n "kmer-papa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "mutexpect") (r "^0.2") (d #t) (k 0)) (d (n "pattern_partition_prediction") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "0af6ap4d0zi18xig301g9cjgw7vpaxkpv0gwic5q5l0a5kzy6m1d") (y #t)))

(define-public crate-kmer-papa-0.1.1 (c (n "kmer-papa") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "mutexpect") (r "^0.2") (d #t) (k 0)) (d (n "pattern_partition_prediction") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabfile") (r "^0.2") (d #t) (k 0)) (d (n "twobit") (r "^0.1") (d #t) (k 0)))) (h "15y9kn0h3bb6isakis0m3kcaj1m9qia1i997284bb9jmdlfczd4z")))

(define-public crate-kmer-papa-0.3.1 (c (n "kmer-papa") (v "0.3.1") (h "0v81lrn1bv55i5iw24jxvy7n1p9a3p3ix8crfy4va375gd76jiv5")))

