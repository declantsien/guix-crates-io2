(define-module (crates-io km er kmerhll) #:use-module (crates-io))

(define-public crate-kmerHLL-0.1.0 (c (n "kmerHLL") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (d #t) (k 0)) (d (n "needletail") (r "^0.5.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "streaming_algorithms") (r "^0.3.0") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1ac5jcj7bg4rv8kgbidrcicszl759816xg4lrg1psafqb7gcqbm6")))

