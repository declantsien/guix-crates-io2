(define-module (crates-io km er kmer_lookup) #:use-module (crates-io))

(define-public crate-kmer_lookup-0.1.1 (c (n "kmer_lookup") (v "0.1.1") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "17dsmyiavl767xlwfs1d5vz5l9zpcjil9w74i6hvq3alas7lc1a2")))

(define-public crate-kmer_lookup-0.1.2 (c (n "kmer_lookup") (v "0.1.2") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1qw6va3rv71428r9r3lv69cacylkj6bz8lg878hrl1pwm0hwa70r")))

(define-public crate-kmer_lookup-0.1.3 (c (n "kmer_lookup") (v "0.1.3") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "05q7sy10pafcyfcfjjcpnjd6xqjlhfpvnbaxrkibxrs5k529s7iw")))

(define-public crate-kmer_lookup-0.1.4 (c (n "kmer_lookup") (v "0.1.4") (d (list (d (n "debruijn") (r "^0.3.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1dg0gxa031p54k6sl9hmwzw132727nmx5fm6firvma6crp5a9nsl")))

(define-public crate-kmer_lookup-0.1.5 (c (n "kmer_lookup") (v "0.1.5") (d (list (d (n "debruijn") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1zcdjc8fcbb3cd0b8vgr4ji706kyf8rqpdv2w95vg7iswhir5fgq")))

