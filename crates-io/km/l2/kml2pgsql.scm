(define-module (crates-io km l2 kml2pgsql) #:use-module (crates-io))

(define-public crate-kml2pgsql-0.1.0 (c (n "kml2pgsql") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1qdf0dr90mr5in57nw76p0fiqks3fn2awkamnkfl7skba91ychnn")))

(define-public crate-kml2pgsql-0.1.1 (c (n "kml2pgsql") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0jhfr5h6p0kmr9b5m2dy862jwglmyn88s6k0bkm1x9hy5sxczcgm")))

(define-public crate-kml2pgsql-0.2.0 (c (n "kml2pgsql") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "wkb") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1jv0zrssrkz1m8zhq24ra7yb5rxshiajg20kdp5z1ww2s4n6wyyg")))

(define-public crate-kml2pgsql-0.2.1 (c (n "kml2pgsql") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.2") (d #t) (k 0)) (d (n "wkb") (r "^0.7.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "01pck52xp5aazmr0hhz8pfqbjcyvharad3qv8j7bywc9mlb6lxb0")))

