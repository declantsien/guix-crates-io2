(define-module (crates-io km od kmod-sys) #:use-module (crates-io))

(define-public crate-kmod-sys-0.1.0 (c (n "kmod-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)))) (h "1swn0m4qgq6kbrh34c9iyvz0bqa9f598gsd4y14ap1gsgawqlxpf")))

(define-public crate-kmod-sys-0.1.1 (c (n "kmod-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)))) (h "1cjzj8y1qjqbn76d9vdgsla4x0x6p5v92zvq5x22y52jv8hjl64h")))

(define-public crate-kmod-sys-0.1.2 (c (n "kmod-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)))) (h "0pybm5hgykw286rnay8648y2ajxqc7alnn7vvw8z3kv9dv8dxnzn")))

(define-public crate-kmod-sys-0.2.0 (c (n "kmod-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1hfzpv6qcz1cbzw8bg2658ipw1ahbxsk6mr1j8q55ppm61ssncvz") (l "kmod")))

