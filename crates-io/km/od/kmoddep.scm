(define-module (crates-io km od kmoddep) #:use-module (crates-io))

(define-public crate-kmoddep-0.1.0 (c (n "kmoddep") (v "0.1.0") (h "1mpfm42x6gbgmbsvhq39wq8yzw60s83jwzqxpff4m8xjg4yryy5w") (y #t)))

(define-public crate-kmoddep-0.1.1 (c (n "kmoddep") (v "0.1.1") (h "1yi90r4mf9mzfvf9za2pxnbr4kmhz7cwyarpgpswn7b3jk7adj0b") (y #t)))

(define-public crate-kmoddep-0.1.2 (c (n "kmoddep") (v "0.1.2") (h "0qdv1jp649dai3k02f6k3jgh6cg0nbw76dm2rrjn9ryq8vzg6zwf")))

(define-public crate-kmoddep-0.1.3 (c (n "kmoddep") (v "0.1.3") (h "0avcrggw30s77pqnlczfqlmj6mza1i55cri1cqslvks24dffc949")))

