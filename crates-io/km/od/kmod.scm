(define-module (crates-io km od kmod) #:use-module (crates-io))

(define-public crate-kmod-0.1.0 (c (n "kmod") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l6vc20zcn4xan9iam77a0865cmzbhan866hdlv1ivsjpr26k1wr")))

(define-public crate-kmod-0.2.0 (c (n "kmod") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reduce") (r "^0.1") (d #t) (k 0)))) (h "0idbshy8g62jgvsj8m9jq5l8g3y875ljj2s2fvxxlrhhlnib8lpx")))

(define-public crate-kmod-0.2.1 (c (n "kmod") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reduce") (r "^0.1") (d #t) (k 0)))) (h "1w98720rv49xr6pw6kzyns2mpqlbsvx3ia4c6jsd5n7r7ag77x0c")))

(define-public crate-kmod-0.3.0 (c (n "kmod") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reduce") (r "^0.1") (d #t) (k 0)))) (h "0iblpvrkipg6ga81hr5l7gz3mga61nw1773sw0rwfy5amdfqgr53")))

(define-public crate-kmod-0.3.1 (c (n "kmod") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reduce") (r "^0.1") (d #t) (k 0)))) (h "06pdx36ic3vdf5lpmgyac760dfn3gy2fgj719sj5vniqm4kdllcf")))

(define-public crate-kmod-0.3.2 (c (n "kmod") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reduce") (r "^0.1") (d #t) (k 0)))) (h "10jwcrab0axgk1a5i3jr0aga1s88xjzlym538dr78h184an6w8cv")))

(define-public crate-kmod-0.4.0 (c (n "kmod") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v9sh374w49hpihlkyqmb49jmmdbxs21m3i5dqvksxiadpd180vl")))

(define-public crate-kmod-0.5.0 (c (n "kmod") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kmod-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1h9nlz94cxiw42wchcs9d2palgjyp9nh8z2pgd1pk441b2mc33mh")))

