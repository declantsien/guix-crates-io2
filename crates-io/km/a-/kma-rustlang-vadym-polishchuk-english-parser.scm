(define-module (crates-io km a- kma-rustlang-vadym-polishchuk-english-parser) #:use-module (crates-io))

(define-public crate-kma-rustlang-vadym-polishchuk-english-parser-0.2.0 (c (n "kma-rustlang-vadym-polishchuk-english-parser") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1gb6xg56lglr7blhkq4xiwn1pn9bdmymxcg85bh7bcbg47dhmxpk")))

