(define-module (crates-io km ai kmail) #:use-module (crates-io))

(define-public crate-kmail-0.1.0 (c (n "kmail") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "email_address") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h7b8cq9i7yf9pghw3i2947fsfdc16023lpq6yjsqpbs9xz1nzbb")))

(define-public crate-kmail-0.1.1 (c (n "kmail") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "email_address") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iw0xhck2smw5ajjwc36p5yvgqkr26d3363bi6jcm20k0j0qxlaz")))

(define-public crate-kmail-0.1.2 (c (n "kmail") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "email_address") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "047nq5akqz5hwd420cxawmc4n13gs7qd76jqpi75k56ixdnvajzq")))

