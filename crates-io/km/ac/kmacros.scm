(define-module (crates-io km ac kmacros) #:use-module (crates-io))

(define-public crate-kmacros-1.0.0 (c (n "kmacros") (v "1.0.0") (d (list (d (n "kmacros_shim") (r "^1.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^1.0") (o #t) (d #t) (k 0)))) (h "199w7cn810fvqg23mkwf2dpfyr8jj1rwjmcnslnpnfck9m0bk4cp") (f (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3.0.0 (c (n "kmacros") (v "3.0.0") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1rwlyvmz4q455zwq9a194jpwm3xlspffhih69n74y83903a15zqy") (f (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3.0.1 (c (n "kmacros") (v "3.0.1") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1fb5incj2znb2ixp81kg161fbwqlx5zg01ahnb7ys7fk58cbaas6") (f (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3.0.2 (c (n "kmacros") (v "3.0.2") (d (list (d (n "kmacros_shim") (r "^3.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^3.0") (o #t) (d #t) (k 0)))) (h "10qdwd56062kiimiz8qdzky0wf2sy5a218gzv23aaghb9al4r86a") (f (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-4.0.0 (c (n "kmacros") (v "4.0.0") (d (list (d (n "kmacros_shim") (r "^4.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^4.0") (o #t) (d #t) (k 0)))) (h "1n2gzdpnm50njqsal33m7q5pvmq0k85j8mp9s5xk3rzrj7pnqnb6") (f (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-5.0.0 (c (n "kmacros") (v "5.0.0") (d (list (d (n "kmacros_shim") (r "^5.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^5.0") (o #t) (d #t) (k 0)))) (h "1wlfqkjd2jlz3rs48jv8f4jhm615myzfw2cf8z2n9wv3ild9dgih") (f (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std") ("default" "proc"))))))

(define-public crate-kmacros-5.1.0 (c (n "kmacros") (v "5.1.0") (d (list (d (n "kmacros_shim") (r "^5.1") (d #t) (k 0)) (d (n "kproc_macros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "05zxaxrmlwzc1i0apbj7i40552zac2pzd7r1j9392zk5d55v27id") (f (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std") ("default" "proc"))))))

(define-public crate-kmacros-6.0.0 (c (n "kmacros") (v "6.0.0") (d (list (d (n "enum-kinds") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "kmacros_shim") (r "^6.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^6.0") (o #t) (d #t) (k 0)))) (h "095pg2rd278qmjlj3l63lvzz0az182gxip2wfcmhcpwg52zjfd4x") (f (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std" "enum-kinds/no-stdlib") ("kinds" "enum-kinds") ("default" "proc" "kinds"))))))

(define-public crate-kmacros-6.1.0 (c (n "kmacros") (v "6.1.0") (d (list (d (n "enum-kinds") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "kmacros_shim") (r "^6.0") (d #t) (k 0)) (d (n "kproc_macros") (r "^6.0") (o #t) (d #t) (k 0)))) (h "0jwwdzsxajl2y6kn72bmhvsjzhk8r4iai628bb4nqm9hpyznf6y1") (f (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std" "enum-kinds/no-stdlib") ("kinds" "enum-kinds") ("default" "proc" "kinds"))))))

