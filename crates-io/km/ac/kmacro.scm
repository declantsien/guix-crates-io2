(define-module (crates-io km ac kmacro) #:use-module (crates-io))

(define-public crate-kmacro-0.1.0 (c (n "kmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00w8bmr38qa7fvk9sk5alwwfc9w91dghcvf35ch4i8zpjr9yxbjv")))

