(define-module (crates-io km mp kmmp-structure) #:use-module (crates-io))

(define-public crate-kmmp-structure-0.1.0 (c (n "kmmp-structure") (v "0.1.0") (h "1vw5445ylpbn1ghw0w21qag6343n7j8pmqhpfv6728b3cidai5f2") (y #t)))

(define-public crate-kmmp-structure-0.1.1 (c (n "kmmp-structure") (v "0.1.1") (h "0vhhhja7g1k3vylbx8abzk6g3is4p6qjgp6z5qpw466aaghcbllz") (y #t)))

(define-public crate-kmmp-structure-0.1.2 (c (n "kmmp-structure") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1193wxl4b3i12jf09ancpfv4gi70n1zaj6ckfnkb1kx8ifjhdjl8")))

