(define-module (crates-io km mp kmmp-project-manager) #:use-module (crates-io))

(define-public crate-kmmp-project-manager-0.1.0 (c (n "kmmp-project-manager") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "05x6hz6vkx05pvax6kh5h11shd117x7p18wwjs5zjjrkc105hi4g") (f (quote (("generate"))))))

