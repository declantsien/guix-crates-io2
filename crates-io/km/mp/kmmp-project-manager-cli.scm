(define-module (crates-io km mp kmmp-project-manager-cli) #:use-module (crates-io))

(define-public crate-kmmp-project-manager-cli-0.1.0 (c (n "kmmp-project-manager-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kmmp-project-manager") (r "^0.1.0") (f (quote ("generate"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "07z14m1bp4hbzxqdd4wis3lsqs86j9c81cf6xzr0cgk9c1g2jxwk")))

