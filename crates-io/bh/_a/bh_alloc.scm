(define-module (crates-io bh _a bh_alloc) #:use-module (crates-io))

(define-public crate-bh_alloc-0.1.0 (c (n "bh_alloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mb0m6wim8hmvqsdaw1j4h61g3rqi0iwzlhshl11mynnwijgvlhf")))

(define-public crate-bh_alloc-0.1.1 (c (n "bh_alloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x82q72jwrz5bxnv9j38pf3z16k56csn0pq9n1ikdxhlsdcsqydl")))

(define-public crate-bh_alloc-0.2.0 (c (n "bh_alloc") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0wrpvpqpx2nwxn4xffxrw76adaasmqvfjlw39s2ygqzkrh9z56dy")))

(define-public crate-bh_alloc-0.2.2 (c (n "bh_alloc") (v "0.2.2") (d (list (d (n "hashbrown") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h2gj4fwc2nr4501qmzrsniiwrqj8i8lsqpn1mvj2i4j9xbzlpc6")))

(define-public crate-bh_alloc-0.2.3 (c (n "bh_alloc") (v "0.2.3") (d (list (d (n "hashbrown") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1782dm29j56hjm65vb3b21b19wvzwyga98f2nrix8j8sxyh3mcqd")))

(define-public crate-bh_alloc-0.2.4 (c (n "bh_alloc") (v "0.2.4") (d (list (d (n "hashbrown") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0g9sc2a6prhxbxwfdi5p4hjql5i4g41699cz94wki70b47bwl223")))

