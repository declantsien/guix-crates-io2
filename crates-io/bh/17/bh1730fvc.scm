(define-module (crates-io bh #{17}# bh1730fvc) #:use-module (crates-io))

(define-public crate-bh1730fvc-0.1.0 (c (n "bh1730fvc") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (t "cfg(all(target_arch = \"arm\"))") (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "00nvrazbdfxjvvy73yx8x85n5cjxqrd6sy94ghkkj4qj0jllhpxy")))

(define-public crate-bh1730fvc-0.1.1 (c (n "bh1730fvc") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (t "cfg(all(target_arch = \"arm\"))") (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "19djhbvglnq7vj4jdd5j2ny7xanfpw42jy6skxp9s8lxl9yqmxwi")))

(define-public crate-bh1730fvc-0.2.0 (c (n "bh1730fvc") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (t "cfg(all(target_arch = \"arm\"))") (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1ilsrn6zif2ga50iww283qfxxcwav8cnpdan8bhh6f4zyi5mrp33") (f (quote (("async")))) (s 2) (e (quote (("unittesting" "dep:embedded-hal-mock"))))))

