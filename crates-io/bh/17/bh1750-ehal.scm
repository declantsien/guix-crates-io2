(define-module (crates-io bh #{17}# bh1750-ehal) #:use-module (crates-io))

(define-public crate-bh1750-ehal-0.0.1 (c (n "bh1750-ehal") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "13qlzsqs53bcjjn0lhyqjswf3cbjsh87lnjshjh4923rp45125bj")))

(define-public crate-bh1750-ehal-0.0.2 (c (n "bh1750-ehal") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0adxr222qcpv0grwzpcabiv826src5pgzwjrf7g4khvpv1q6hrm5")))

