(define-module (crates-io bh ts bhtsne) #:use-module (crates-io))

(define-public crate-bhtsne-0.1.0 (c (n "bhtsne") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1n803qd6aib7hmc8pbb5f3nlz7hk0cl32876q427nir82jny8895")))

(define-public crate-bhtsne-0.2.0 (c (n "bhtsne") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "00h145mgnw7wlh0365zrhzvfjv3r166kqzz97nfxp725326nqm2f")))

(define-public crate-bhtsne-0.2.1 (c (n "bhtsne") (v "0.2.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0vg79z2p8g70181vggg00k9qrp0r8bgnd3pdj0h6wrwd0l4qifgm")))

(define-public crate-bhtsne-0.3.0 (c (n "bhtsne") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "1lp6agib249m831s034xw31f63zwdzs51vdq74zl6r7j73xw9w7q")))

(define-public crate-bhtsne-0.3.1 (c (n "bhtsne") (v "0.3.1") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "00b5159kb1hwk76nz5nf2w331j1262d3py5v9ys45v9yml1fa992") (f (quote (("default" "csv"))))))

(define-public crate-bhtsne-0.4.0 (c (n "bhtsne") (v "0.4.0") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "1q0znpz1z5kgjahaajifc5wa3pnl1yryr3mnq22w3kl2pmb1gghc") (f (quote (("default" "csv"))))))

(define-public crate-bhtsne-0.4.1 (c (n "bhtsne") (v "0.4.1") (d (list (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1njlfm379ck3kncyv280clj6yy8wp9mp3bb15885s7gnhmbbglkf") (f (quote (("default" "csv"))))))

(define-public crate-bhtsne-0.5.0 (c (n "bhtsne") (v "0.5.0") (d (list (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pdqselect") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "091pmibx9mskjjixyhi0zd94awvmvlyx9c6y6jjxvb8c17sxhmny") (f (quote (("default" "csv"))))))

(define-public crate-bhtsne-0.5.1 (c (n "bhtsne") (v "0.5.1") (d (list (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0m4bzl56zvx4vx8mivwzrfyzzf7470brz10hmhjx31gc5nwfjk3g") (f (quote (("default" "csv"))))))

(define-public crate-bhtsne-0.5.2 (c (n "bhtsne") (v "0.5.2") (d (list (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1wgy3akiraws5znfc37fjzfwvdldmd3w9zyh2n9162k7g2fl8ckk") (f (quote (("default" "csv"))))))

