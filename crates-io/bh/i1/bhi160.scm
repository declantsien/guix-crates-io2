(define-module (crates-io bh i1 bhi160) #:use-module (crates-io))

(define-public crate-bhi160-0.1.0 (c (n "bhi160") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11") (d #t) (k 0)))) (h "1w5kw47jnv11cn448z2vncnl2zns7yp41xmixk6qyxi3mvpqbgza") (s 2) (e (quote (("log" "dep:log"))))))

