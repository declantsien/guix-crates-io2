(define-module (crates-io bh ar bharat-cafe) #:use-module (crates-io))

(define-public crate-bharat-cafe-0.1.0 (c (n "bharat-cafe") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0r1r9c8rawif4dfqq2x0k0p3bjhxfmhc3rv8i6ad0v5fcka020nr")))

(define-public crate-bharat-cafe-0.1.2 (c (n "bharat-cafe") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12fbnjjz6549vf0rhw1yp2lj8hs9dqnbgnxn90kqghimrag45awj")))

(define-public crate-bharat-cafe-0.1.4 (c (n "bharat-cafe") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0waf6mdyyfym139hbvlx7paxf8hk19m1jc4fxiykfqgp86fnlp35")))

(define-public crate-bharat-cafe-0.2.0 (c (n "bharat-cafe") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vs2dng0rydwdp9prgsh1bqf8xk58dwg5mali599xf5h2nm39qlx")))

(define-public crate-bharat-cafe-0.2.1 (c (n "bharat-cafe") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xkz0n14pn8n2zrw2246d393fc7hri691764svfmbrzka7yn5qa8")))

