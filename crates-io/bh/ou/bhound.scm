(define-module (crates-io bh ou bhound) #:use-module (crates-io))

(define-public crate-bhound-0.1.0 (c (n "bhound") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "0667xi8l3150v17y9ja0pic29jm27nk3pc1fg0n3949fm1x02lsl")))

(define-public crate-bhound-0.1.1 (c (n "bhound") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "1v2v2vmg93kvq7l20xzmxgv02c5mshp65n2nxnvr4rwlag5z4myx")))

(define-public crate-bhound-0.1.2 (c (n "bhound") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "0iypagyy4x334ri2lyh3xhca3f0z55b00m027k8r7j0hm8k269k7")))

(define-public crate-bhound-0.1.3 (c (n "bhound") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "0jq3dip30iwwwda4l3axcdqynw0qgp9vbid14bakidhk84ns7wvg")))

(define-public crate-bhound-0.1.4 (c (n "bhound") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "011s8payi06sdp4fxwj70f4wz72sf2pbdn231qvij1qmn8m4qc9z")))

(define-public crate-bhound-0.1.5 (c (n "bhound") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "neo4rs") (r "^0.5.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "074vq26nsnsvgjpxxasbpc6iprdbmc6i7v73kk8nm9cbq1c618pj")))

