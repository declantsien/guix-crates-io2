(define-module (crates-io cx mr cxmr-api-clients) #:use-module (crates-io))

(define-public crate-cxmr-api-clients-0.0.1 (c (n "cxmr-api-clients") (v "0.0.1") (d (list (d (n "cxmr-api") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-api-binance") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-api-clients-errors") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-api-poloniex") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-http-client") (r "^0.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "0z1h3mrv9rs9bp22avgg5f507b8w4fxbdc2b0cm6n7qxl380ripw")))

