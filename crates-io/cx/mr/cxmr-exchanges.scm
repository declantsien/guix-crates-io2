(define-module (crates-io cx mr cxmr-exchanges) #:use-module (crates-io))

(define-public crate-cxmr-exchanges-0.0.1 (c (n "cxmr-exchanges") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "11a1v1m3572kvcsw6iimjp4qdgibyr7f40wzaf6j3lh15bgzhy55")))

