(define-module (crates-io cx mr cxmr-telegram) #:use-module (crates-io))

(define-public crate-cxmr-telegram-0.0.1 (c (n "cxmr-telegram") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-broker") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures-async-stream") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "telegram-bot-async") (r "^0.8.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzw4gi343rgp50bjkr68dpdlzs7w3g6rqd2mng8wqxidhkw3w1b")))

