(define-module (crates-io cx mr cxmr-feeds) #:use-module (crates-io))

(define-public crate-cxmr-feeds-0.0.1 (c (n "cxmr-feeds") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "187dchsz4ghbdjl1985wjyjar9bqlq938wr1fpmg8bbvm6j4z4xl")))

