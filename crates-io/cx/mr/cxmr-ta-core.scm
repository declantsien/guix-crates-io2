(define-module (crates-io cx mr cxmr-ta-core) #:use-module (crates-io))

(define-public crate-cxmr-ta-core-0.1.5 (c (n "cxmr-ta-core") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^0.15.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "05ji6wz5iqf70sa0b8j2wy5k649xmw5yqziw32r09lb1fwqs4jyk")))

