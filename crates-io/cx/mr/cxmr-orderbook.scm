(define-module (crates-io cx mr cxmr-orderbook) #:use-module (crates-io))

(define-public crate-cxmr-orderbook-0.0.1 (c (n "cxmr-orderbook") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-feeds") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1kk47kdsqfs7qk8gn893xqjn236820g9x6pzkrc8rnm1ljj7iiav")))

