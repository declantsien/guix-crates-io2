(define-module (crates-io cx mr cxmr-ta) #:use-module (crates-io))

(define-public crate-cxmr-ta-0.0.1 (c (n "cxmr-ta") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-feeds") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-orderbook") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-ta-core") (r "^0.1.5") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0p5r8ihh4lc1bb8iaw1badsw1xywklff0h4v6xhs1n7si5lwhfdj")))

