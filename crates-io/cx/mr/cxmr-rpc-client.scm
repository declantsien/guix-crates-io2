(define-module (crates-io cx mr cxmr-rpc-client) #:use-module (crates-io))

(define-public crate-cxmr-rpc-client-0.0.1 (c (n "cxmr-rpc-client") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "cxmr-api-clients-errors") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-http-client") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-orderbook") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-rpc") (r "^0.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1azpv4q17xnwjwyy3h38x8v4i46symrf0ys0y4ga2d082yraqdsx") (y #t)))

