(define-module (crates-io cx mr cxmr-candles) #:use-module (crates-io))

(define-public crate-cxmr-candles-0.0.1 (c (n "cxmr-candles") (v "0.0.1") (d (list (d (n "cxmr-feeds") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-ta") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1vwcrpyl8rysjkjfb8b31d8jvp48sbg0h2m1h01li07yd3473spk") (f (quote (("default" "analysis") ("analysis" "cxmr-ta"))))))

