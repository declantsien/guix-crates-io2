(define-module (crates-io cx mr cxmr-pubsub) #:use-module (crates-io))

(define-public crate-cxmr-pubsub-0.0.1 (c (n "cxmr-pubsub") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-feeds") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-tectonic") (r "^0.0.1") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "16z0zypk59v5y5wha6z1sw2gk97ngsg0y73873mrq31wfgry7s1i") (f (quote (("vendored-zmq" "zmq/vendored") ("default" "vendored-zmq"))))))

