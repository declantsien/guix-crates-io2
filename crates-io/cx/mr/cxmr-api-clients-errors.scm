(define-module (crates-io cx mr cxmr-api-clients-errors) #:use-module (crates-io))

(define-public crate-cxmr-api-clients-errors-0.0.1 (c (n "cxmr-api-clients-errors") (v "0.0.1") (d (list (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-http-client") (r "^0.0.1") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1xlvi92jynb3qal0jgm7jj5imivhq9vqsi32vb4dz1czxspc0mk8") (y #t)))

