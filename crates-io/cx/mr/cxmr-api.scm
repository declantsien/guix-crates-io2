(define-module (crates-io cx mr cxmr-api) #:use-module (crates-io))

(define-public crate-cxmr-api-0.0.1 (c (n "cxmr-api") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1gp5wqdbxnnf49p6mm8z4gzpi37ya43a5mf086iwc1lxgidzcfdh")))

