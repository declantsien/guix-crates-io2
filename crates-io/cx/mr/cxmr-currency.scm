(define-module (crates-io cx mr cxmr-currency) #:use-module (crates-io))

(define-public crate-cxmr-currency-0.0.1 (c (n "cxmr-currency") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0484jvx49gl3rpfqjgn9k3qj1r3wwwahs2qf30w6yiq3q4qrvpyv")))

