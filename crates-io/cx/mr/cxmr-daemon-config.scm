(define-module (crates-io cx mr cxmr-daemon-config) #:use-module (crates-io))

(define-public crate-cxmr-daemon-config-0.0.1 (c (n "cxmr-daemon-config") (v "0.0.1") (d (list (d (n "cxmr-api") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-telegram") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.0") (d #t) (k 0)))) (h "08v50r3scx05hq9g032whk049vh2mywgp9z142c8fri88c8jngqw") (f (quote (("pubsub") ("default") ("chat" "cxmr-telegram"))))))

