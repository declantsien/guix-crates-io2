(define-module (crates-io cx mr cxmr-rpc-server) #:use-module (crates-io))

(define-public crate-cxmr-rpc-server-0.0.1 (c (n "cxmr-rpc-server") (v "0.0.1") (d (list (d (n "cxmr-balances") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-broker") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-util-servers") (r "^0.0.1") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (f (quote ("runtime" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "yew-router-min") (r "^0.8.0") (k 0)))) (h "12ws71h67jf5q2cbphx944wbw1dm22cpxmh7dxjxgmnyqyjl19f8") (y #t)))

