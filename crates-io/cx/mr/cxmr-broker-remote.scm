(define-module (crates-io cx mr cxmr-broker-remote) #:use-module (crates-io))

(define-public crate-cxmr-broker-remote-0.0.1 (c (n "cxmr-broker-remote") (v "0.0.1") (d (list (d (n "cxmr-api") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-api-clients") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-broker") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-currency") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-http-client") (r "^0.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0grh9bjg4cy3k6sidn3np21rn91ahixpm08gwh8zyj7qbs49qcfm") (y #t)))

