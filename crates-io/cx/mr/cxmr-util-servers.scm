(define-module (crates-io cx mr cxmr-util-servers) #:use-module (crates-io))

(define-public crate-cxmr-util-servers-0.0.1 (c (n "cxmr-util-servers") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper") (r "^0.13.1") (f (quote ("runtime" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1i03zwqq2lj2jx64q1hys1qgvvxr5fvb3f18k3800ppy3wxy4lla")))

