(define-module (crates-io cx mr cxmr-rpc) #:use-module (crates-io))

(define-public crate-cxmr-rpc-0.0.1 (c (n "cxmr-rpc") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "cxmr-exchanges") (r "^0.0.1") (d #t) (k 0)) (d (n "cxmr-orderbook") (r "^0.0.1") (d #t) (k 0)))) (h "09r5pba7x0p8ngzfiylagf83w71fs3jfx49aqyzbiphiv8xikv71") (y #t)))

