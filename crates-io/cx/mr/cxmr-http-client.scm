(define-module (crates-io cx mr cxmr-http-client) #:use-module (crates-io))

(define-public crate-cxmr-http-client-0.0.1 (c (n "cxmr-http-client") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "err-convert-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (f (quote ("runtime" "stream"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4.0") (d #t) (k 0)))) (h "13zsrqliynfdlglxyn41yha3psnrzqj0zydagw3nlli80v7hl4ls")))

