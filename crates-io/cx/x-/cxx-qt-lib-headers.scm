(define-module (crates-io cx x- cxx-qt-lib-headers) #:use-module (crates-io))

(define-public crate-cxx-qt-lib-headers-0.4.0 (c (n "cxx-qt-lib-headers") (v "0.4.0") (h "15v24cri28nk4j3ak0q60syfzqm3g7kibqljs560f1kskn8q6y2c")))

(define-public crate-cxx-qt-lib-headers-0.4.1 (c (n "cxx-qt-lib-headers") (v "0.4.1") (h "050lldjhs6fz88qvqhb92h4s48iw0fb2dajqs43jk3fikfn0aj77")))

(define-public crate-cxx-qt-lib-headers-0.5.0 (c (n "cxx-qt-lib-headers") (v "0.5.0") (h "02r2gnzcxzcd987ym6j256y0hhq0cswrk6bgzprkv5rb35h5dpn7") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

(define-public crate-cxx-qt-lib-headers-0.5.1 (c (n "cxx-qt-lib-headers") (v "0.5.1") (h "025qfal0frmhci36ad739s8j5kj4kp59xgyanszd1mmgpi1k7fl3") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

(define-public crate-cxx-qt-lib-headers-0.5.2 (c (n "cxx-qt-lib-headers") (v "0.5.2") (h "07x8gdxcxaxjpwm35v314kag0g0s3p12xqpd2fyh2gdzh4z5n57k") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

(define-public crate-cxx-qt-lib-headers-0.5.3 (c (n "cxx-qt-lib-headers") (v "0.5.3") (h "030i0ra7ak15bwbl9iafv2kcp8b5l8k9wyyj6q2x086m3grppynp") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

(define-public crate-cxx-qt-lib-headers-0.6.0 (c (n "cxx-qt-lib-headers") (v "0.6.0") (h "1x092v61ak83a1rs2qq37prsksb9hiqsqd18fyyk6ss2l667k25a") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

(define-public crate-cxx-qt-lib-headers-0.6.1 (c (n "cxx-qt-lib-headers") (v "0.6.1") (h "130b8irl4kqk9jx2vk6c4caarddw6wmgdwr4nx9mmz3wnyvfmgcs") (f (quote (("qt_qml") ("qt_gui") ("default"))))))

