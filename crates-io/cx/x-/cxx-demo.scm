(define-module (crates-io cx x- cxx-demo) #:use-module (crates-io))

(define-public crate-cxx-demo-0.0.1 (c (n "cxx-demo") (v "0.0.1") (d (list (d (n "cxx") (r "^0.3") (d #t) (k 0)) (d (n "cxx-build") (r "=0.4.0-rc1") (d #t) (k 1)))) (h "1fl0krjq1188g470fjid15z0h4zm4591fc2backsm5sh8qsx1cgy")))

(define-public crate-cxx-demo-0.0.2 (c (n "cxx-demo") (v "0.0.2") (d (list (d (n "cxx") (r "^0.3") (d #t) (k 0)) (d (n "cxx-build") (r "=0.4.0-rc2") (d #t) (k 1)))) (h "01li9gqihn554ghbjhh6qh5q56l6404lz5056s42ya979cayvazg")))

(define-public crate-cxx-demo-0.0.3 (c (n "cxx-demo") (v "0.0.3") (d (list (d (n "cxx") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.4") (d #t) (k 1)))) (h "1r8h5qs4y0vvq9cpdlb5hv84fcn39sih48wly3j66y9sx8ax4i3v")))

(define-public crate-cxx-demo-0.0.4 (c (n "cxx-demo") (v "0.0.4") (d (list (d (n "cxx") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.4") (d #t) (k 1)))) (h "1yscra4dg8vxlgjwly21hj1znljq5kd9grjy5ss9jzkd1g5nlx9k")))

(define-public crate-cxx-demo-0.0.5 (c (n "cxx-demo") (v "0.0.5") (d (list (d (n "cxx") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cxx-build") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0ybdvgy29f0b1c4i4l6dkzl055n9h2c94wqymr2ksvdhp0b9sc44")))

(define-public crate-cxx-demo-0.0.6 (c (n "cxx-demo") (v "0.0.6") (d (list (d (n "cxx") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cxx-build") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0qsn5xih5bh01nrwp7gdmz24j2hjfnka8vbhcqwccqgq1df9g6qh")))

(define-public crate-cxx-demo-0.0.7 (c (n "cxx-demo") (v "0.0.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1mmniggxdjwy0al8361j6db5i08wjfqr85j862dnnz91kindhx7z")))

