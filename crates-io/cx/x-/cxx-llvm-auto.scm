(define-module (crates-io cx x- cxx-llvm-auto) #:use-module (crates-io))

(define-public crate-cxx-llvm-auto-0.0.0 (c (n "cxx-llvm-auto") (v "0.0.0") (d (list (d (n "cxx") (r "^1.0") (f (quote ("c++20"))) (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx-llvm-build") (r "^0.0") (d #t) (k 1)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 1)))) (h "1cgk2ysni85dz3kfya7ffxh345s1yv8bs13ljc8541y7nadsa4pp") (l "cxx-llvm-auto")))

(define-public crate-cxx-llvm-auto-0.0.2 (c (n "cxx-llvm-auto") (v "0.0.2") (d (list (d (n "cxx") (r "^1.0") (f (quote ("c++20"))) (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx-llvm-build") (r "^0.0") (d #t) (k 1)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 1)))) (h "08gb90k7j16ij7j6dj5ngmh9fz7r5ih809zmyp21smc06nmxqilj") (l "cxx-llvm-auto")))

(define-public crate-cxx-llvm-auto-0.0.3 (c (n "cxx-llvm-auto") (v "0.0.3") (d (list (d (n "cxx") (r "^1.0") (f (quote ("c++20"))) (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 0)) (d (n "cxx-auto") (r "^0.0") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx-llvm-build") (r "^0.0") (d #t) (k 1)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 1)))) (h "1h0dsysaniavvc85wzvn8944x6dcw3z0mzs62xh84rfp20np1m4b") (l "cxx-llvm-auto")))

