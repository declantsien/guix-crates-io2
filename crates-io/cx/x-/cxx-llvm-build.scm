(define-module (crates-io cx x- cxx-llvm-build) #:use-module (crates-io))

(define-public crate-cxx-llvm-build-0.0.0 (c (n "cxx-llvm-build") (v "0.0.0") (d (list (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)))) (h "0h1bqq7r1ymck7w2h83bs1nw0b5dx53bn2mkfpj1nqj6335kydck")))

(define-public crate-cxx-llvm-build-0.0.2 (c (n "cxx-llvm-build") (v "0.0.2") (d (list (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)))) (h "0q3sszm2q36xirgaq15xbx2w6dh831pn3hf07w0s17gw2va93v39")))

(define-public crate-cxx-llvm-build-0.0.3 (c (n "cxx-llvm-build") (v "0.0.3") (d (list (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)))) (h "13c1va9h0s09apqz4jhradvngzqy55nqrmvfi70qxachilllw7rs")))

