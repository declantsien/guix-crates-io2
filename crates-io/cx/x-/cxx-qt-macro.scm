(define-module (crates-io cx x- cxx-qt-macro) #:use-module (crates-io))

(define-public crate-cxx-qt-macro-0.6.0 (c (n "cxx-qt-macro") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0.95") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03ckaplg1sgk2h6cdmqgjlvyzl3ncixx029dnpspdcnk099bmsaj")))

(define-public crate-cxx-qt-macro-0.6.1 (c (n "cxx-qt-macro") (v "0.6.1") (d (list (d (n "cxx") (r "^1.0.95") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0k363fv97j50s0zv6jb3n4mn6lyjxrr0v5l4n0ck9c03iik8m7k9")))

