(define-module (crates-io cx x- cxx-async-macro) #:use-module (crates-io))

(define-public crate-cxx-async-macro-0.1.0 (c (n "cxx-async-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkjkya9j9nfzrxy05xkxyp8vkkd0ccwzpi4rfzg29zx361jkljy")))

(define-public crate-cxx-async-macro-0.1.1 (c (n "cxx-async-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ql4d8a8x1pp39jwgxc372m37xbm978n77nkiq4isn0hp4n6vcy2")))

