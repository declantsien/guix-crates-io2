(define-module (crates-io cx x- cxx-qt) #:use-module (crates-io))

(define-public crate-cxx-qt-0.1.0 (c (n "cxx-qt") (v "0.1.0") (d (list (d (n "cxx-qt-gen") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0v4085x8j91yclw0jdkcm7fhmhnxs45fln80slwgrcnycdfaxqda")))

(define-public crate-cxx-qt-0.2.0 (c (n "cxx-qt") (v "0.2.0") (d (list (d (n "cxx-qt-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1g0nck934pbp5a7q2947zg6gsh4vagc4rzc72zka005mwfrrgxrx")))

(define-public crate-cxx-qt-0.2.1 (c (n "cxx-qt") (v "0.2.1") (d (list (d (n "cxx-qt-gen") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0sgsr1j073mvlpa842lga988hayif7flr0iaprn61i6k3anq2h64")))

(define-public crate-cxx-qt-0.3.0 (c (n "cxx-qt") (v "0.3.0") (d (list (d (n "cxx-qt-gen") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vfv323hghjfm9h15fl2sakbrh0vyhz8vnqgjk72lc9wmvdpjpqf")))

(define-public crate-cxx-qt-0.4.0 (c (n "cxx-qt") (v "0.4.0") (d (list (d (n "cxx-qt-gen") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ddd2z14g1as88dckrawpb49ydfwzipb6ksrjhzz32d5cfh2gc4r")))

(define-public crate-cxx-qt-0.4.1 (c (n "cxx-qt") (v "0.4.1") (d (list (d (n "cxx-qt-gen") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "164x30qqh1amc934r13if9hd2bax7y3vxfw1dg220264wr90s0kk")))

(define-public crate-cxx-qt-0.5.0 (c (n "cxx-qt") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0.83") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "14iki0w1drhgcrdghwyyl0c2wfyz3nkvbb4pjcy03bvxj2ws7m57")))

(define-public crate-cxx-qt-0.5.1 (c (n "cxx-qt") (v "0.5.1") (d (list (d (n "cxx") (r "^1.0.83") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ncp7d0mh09zb78gnjlarc624yml66dhpblkdzxgs8dwpi6bi4sw")))

(define-public crate-cxx-qt-0.5.2 (c (n "cxx-qt") (v "0.5.2") (d (list (d (n "cxx") (r "^1.0.83") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "104l3s7mmipf6x63v2zbdw53fpapszxpl1b0f34ril2pg7ip5hq8")))

(define-public crate-cxx-qt-0.5.3 (c (n "cxx-qt") (v "0.5.3") (d (list (d (n "cxx") (r "^1.0.83") (d #t) (k 2)) (d (n "cxx-qt-gen") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "084jv1kpxd317zf4d8fjvhwgdvpcsj2nvrxw3br8qzv81l0gl0i0")))

(define-public crate-cxx-qt-0.6.0 (c (n "cxx-qt") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0.95") (d #t) (k 0)) (d (n "cxx") (r "^1.0.95") (d #t) (k 2)) (d (n "cxx-qt-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1v4l9l4f0s97v052rgfz7ixwcnz72d511kblqcisgvpzgxa8x9m9")))

(define-public crate-cxx-qt-0.6.1 (c (n "cxx-qt") (v "0.6.1") (d (list (d (n "cxx") (r "^1.0.95") (d #t) (k 0)) (d (n "cxx") (r "^1.0.95") (d #t) (k 2)) (d (n "cxx-qt-macro") (r "^0.6.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1391j5q8b272k8shz34h0i6j1m30q2njy39varyd3dl8fpd6rah8")))

