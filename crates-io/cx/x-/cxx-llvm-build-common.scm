(define-module (crates-io cx x- cxx-llvm-build-common) #:use-module (crates-io))

(define-public crate-cxx-llvm-build-common-0.0.0 (c (n "cxx-llvm-build-common") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0izmw993vip0yypaa42qih9xg5ld5h79h7aib8bjl28niblrbh9h")))

(define-public crate-cxx-llvm-build-common-0.0.2 (c (n "cxx-llvm-build-common") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1gcd9dmwrim1dnjyvrs3y8cg74fc6ps5j47gwvznay56yfcanw7w")))

(define-public crate-cxx-llvm-build-common-0.0.3 (c (n "cxx-llvm-build-common") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1r58sk11sh4wgn3yla07fiblyd8qzrnjrqwvyx28216igz25p23p")))

