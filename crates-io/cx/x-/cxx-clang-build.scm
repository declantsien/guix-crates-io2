(define-module (crates-io cx x- cxx-clang-build) #:use-module (crates-io))

(define-public crate-cxx-clang-build-0.0.0 (c (n "cxx-clang-build") (v "0.0.0") (d (list (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "cxx-llvm-build") (r "^0.0") (d #t) (k 0)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)))) (h "1rr4i10bprjf12lv70alq9cw57wfyjyy7gk05bhqd2l6h8rhd8nw")))

(define-public crate-cxx-clang-build-0.0.1 (c (n "cxx-clang-build") (v "0.0.1") (d (list (d (n "cxx-build") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "cxx-llvm-build") (r "^0.0") (d #t) (k 0)) (d (n "cxx-llvm-build-common") (r "^0.0") (d #t) (k 0)) (d (n "normpath") (r "^1.1") (d #t) (k 0)))) (h "0yqx34b67sxkidhkgjqf9hvghgd22884w1a4p3vvq4m6fa27y1f8")))

