(define-module (crates-io cx x- cxx-juce) #:use-module (crates-io))

(define-public crate-cxx-juce-0.1.0 (c (n "cxx-juce") (v "0.1.0") (h "0kbmm307zv5kjvv603xm99drw04ggqkgvbyprm24c26cxaaccq50")))

(define-public crate-cxx-juce-0.2.0 (c (n "cxx-juce") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0.82") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0.82") (d #t) (k 1)))) (h "0ygyhjnlfg1yzryjb67n9irjjk9qnyincbnvxadfkbqd76iayzxc") (f (quote (("asio"))))))

(define-public crate-cxx-juce-0.3.0 (c (n "cxx-juce") (v "0.3.0") (d (list (d (n "cxx") (r "^1.0.82") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0.82") (d #t) (k 1)))) (h "0hnqsjlbr4lma48iggi3kprhbxfxbbvlwgssdfn3cny9l3kvkbrd") (f (quote (("asio"))))))

(define-public crate-cxx-juce-0.4.0 (c (n "cxx-juce") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0.82") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0.82") (d #t) (k 1)))) (h "05gm85774kavcs9nhgc4qgzxrxi8qzx0xiihckm3g409ydffic4q") (f (quote (("asio"))))))

(define-public crate-cxx-juce-0.5.0 (c (n "cxx-juce") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0.82") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0.82") (d #t) (k 1)))) (h "1kqdkd718mak5ndxb6srgfh5qhz2c3r6ja3lxbdqzh29kikv3aay") (f (quote (("asio"))))))

(define-public crate-cxx-juce-0.6.0 (c (n "cxx-juce") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0.82") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0.82") (d #t) (k 1)))) (h "1nh9x3dfd51b4la30vbhb2jhkw9ygjmz297nnfqqkm0mgj7w9b14") (f (quote (("asio"))))))

