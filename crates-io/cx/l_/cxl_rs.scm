(define-module (crates-io cx l_ cxl_rs) #:use-module (crates-io))

(define-public crate-cxl_rs-0.0.1 (c (n "cxl_rs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09bcypfmpbng5msbjxs9fv17b3sj4kghn0hddv0fcfx8kprvzgx9")))

(define-public crate-cxl_rs-0.0.2 (c (n "cxl_rs") (v "0.0.2") (d (list (d (n "fletcher") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kwz5cg9yqm98cmsh9aghiz4xl14gz0fav3fk9mk9cnfyz5a534x")))

(define-public crate-cxl_rs-0.0.3 (c (n "cxl_rs") (v "0.0.3") (d (list (d (n "fletcher") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1r8ln2sdl75fc6q7ngv09a877bzz16cxx8lm3pn9bjh2zdzl5q2g")))

(define-public crate-cxl_rs-0.0.4 (c (n "cxl_rs") (v "0.0.4") (d (list (d (n "fletcher") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g1x78j2byn9b9qxpdc7dp4h97z94xpf2s1xl1x0vpmaw45zjmxq")))

(define-public crate-cxl_rs-0.0.5 (c (n "cxl_rs") (v "0.0.5") (d (list (d (n "fletcher") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0s35gmwc3fjq31wzizdpm88dppjxgggbxp5rfc7dg81g88ajy43s")))

