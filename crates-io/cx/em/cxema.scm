(define-module (crates-io cx em cxema) #:use-module (crates-io))

(define-public crate-cxema-0.1.0 (c (n "cxema") (v "0.1.0") (h "1lz6b33337nmadsnywbn548kjrwgsphaksya1s3mb97v5b2s15sw") (y #t)))

(define-public crate-cxema-0.1.1 (c (n "cxema") (v "0.1.1") (h "0vq31arlfg990nm4bfhnnpk8wixhd472v56ywg0b2jv5hmcr2vnd")))

