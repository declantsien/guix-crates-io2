(define-module (crates-io cx xb cxxbridge-flags) #:use-module (crates-io))

(define-public crate-cxxbridge-flags-0.3.7 (c (n "cxxbridge-flags") (v "0.3.7") (h "0m0vvgjvng05bj68bs423m0czfsv8aa7ykl7xfsy99x3zj44g4if") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.3.8 (c (n "cxxbridge-flags") (v "0.3.8") (h "0z8w4cgxlfkkr0m2vg2bmczp5102mgc9ahfxjhah00k9x1y63a4y") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.3.9 (c (n "cxxbridge-flags") (v "0.3.9") (h "1wlh8wnr2j6hgsn0ccr9p4f3x8cqzvz04n9h1zsdfjcz8x3ywyfi") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.0 (c (n "cxxbridge-flags") (v "0.4.0") (h "0wdmn0hpcamxj2q40hnz2h3b1hnfnz6mj391rylxcijkq98l2k0h") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.1 (c (n "cxxbridge-flags") (v "0.4.1") (h "01n6k6s39ahik7dkjq8yjga0nww699m9405gp1rkpn1hxhhqyzlk") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.2 (c (n "cxxbridge-flags") (v "0.4.2") (h "1bilkd9bjf5hl1ygnsw580hpwaqxq5aj7n9fi795j59g809q341r") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.3 (c (n "cxxbridge-flags") (v "0.4.3") (h "0mwid1mzdypsviz5jcv0zkqk14brmirg2vdz0kx29cfli7nq90yk") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.4 (c (n "cxxbridge-flags") (v "0.4.4") (h "1hp3ws2jgp6f4p2fh1dgnv3808kxdmcazvdmpi4q8h5d5ckxnwf7") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.5 (c (n "cxxbridge-flags") (v "0.4.5") (h "1k2a8f8282jfi2917qv0mbfprg4srxd5pag3fjfb1nnchk1w3svn") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.6 (c (n "cxxbridge-flags") (v "0.4.6") (h "0pjgwq4nn95d9z49qr1x3gydbrgdw93s811bcpnx85grfjkq8h6q") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.4.7 (c (n "cxxbridge-flags") (v "0.4.7") (h "1k4jx7av3kd1iibj3cfqw12kay0fh5n3p6qbz9nv02dia21prp0q") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.0 (c (n "cxxbridge-flags") (v "0.5.0") (h "0zl8w68r11ym2rh43jvrvr13brir5fwbx17lqkfc7r75v49vkpdm") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.1 (c (n "cxxbridge-flags") (v "0.5.1") (h "001whkd33ihv9y4y3s6z4gv78xwv08dfvf9fmydw8a0kkx3r58c9") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.2 (c (n "cxxbridge-flags") (v "0.5.2") (h "0ymwz7wqha2sly9lbzimzbyc13hd48q64vsqk2pr20i18kns8xmw") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.3 (c (n "cxxbridge-flags") (v "0.5.3") (h "0gay23drlbpjqvwwkywpjrl8mrp43l34g769w57afpk4791k8jr0") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.4 (c (n "cxxbridge-flags") (v "0.5.4") (h "0p8b7ala8q4jxn99mmm2dnbk2xgkbg6gi6y9g2is883bpfba1dqy") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.5 (c (n "cxxbridge-flags") (v "0.5.5") (h "00596wq46apaj5bjm3zsr5w3w94ax5xs56zpsah3sc619kmibkrp") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.6 (c (n "cxxbridge-flags") (v "0.5.6") (h "1q1yggxwi5rg1zmp5plm1zdkrzfsgmgj9dk87m6k33sw222gp1gf") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.7 (c (n "cxxbridge-flags") (v "0.5.7") (h "1iqqwz3v93pig3qf31cvskf0bm8fac5r66iv9v3pvl04ymg2kdbr") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.8 (c (n "cxxbridge-flags") (v "0.5.8") (h "1r9bv25wxi8zlafm8871x60ip0l96yzdaas42jz58kpkgyfz89bk") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.9 (c (n "cxxbridge-flags") (v "0.5.9") (h "1i9i3ajpksxsa86whh4arg2dkfb2594bxh1f7461c2n0vzgxhvwg") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-0.5.10 (c (n "cxxbridge-flags") (v "0.5.10") (h "0jfwsm85s5kalgqbqlg1kq79zcb5zwk375h0qw7ycz5i6v3c8j0k") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.0 (c (n "cxxbridge-flags") (v "1.0.0") (h "1x1acjn693r3957kqr73zdgmmjfb86jjcas0ayq40b4fagza3nks") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.1 (c (n "cxxbridge-flags") (v "1.0.1") (h "1s8ik4lw4lffn4k18xg9ffygd4m4dffb7xd1irwi03smkmhyig4s") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.2 (c (n "cxxbridge-flags") (v "1.0.2") (h "17rslim9dxs02x0352m5d6fa3cw9kwn1j3inv37lm3b3bmw919qz") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.3 (c (n "cxxbridge-flags") (v "1.0.3") (h "1rj90wa9p172gc0z6bbb87wb5n8d5y88bhxgric1wqvmsladwxj9") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.4 (c (n "cxxbridge-flags") (v "1.0.4") (h "16aif8hfyh60a5z5brfyvx25mg1903w3d59grfya8lnqsh60xna7") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.5 (c (n "cxxbridge-flags") (v "1.0.5") (h "1dlmv2kzfr6dfcnn8v6q1lfa8l8c2b1vv19ida9makzjn1smnmx6") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.6 (c (n "cxxbridge-flags") (v "1.0.6") (h "1285d5ckq0fhn3i0zlppilg63nfbmh0rgxz23b9dw5rkbfamy9xv") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.7 (c (n "cxxbridge-flags") (v "1.0.7") (h "156fg0cxsn0h8pnfw8cf0gp079d2dgd6z6pnwz3r26ix30r2gycq") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.8 (c (n "cxxbridge-flags") (v "1.0.8") (h "15nsbyd39l6qxyza515yyl21pg128g0r50sfxs731k3yzd317dax") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.9 (c (n "cxxbridge-flags") (v "1.0.9") (h "14j9547s3wa9z2mj7lfv6m7pnip5jgjbm5wa1ixya5lymf4zckff") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.10 (c (n "cxxbridge-flags") (v "1.0.10") (h "0iw8b974cxx5sl9vpg0pxw760s1w2palbi4h4vlcz4ablv2j5pzq") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.11 (c (n "cxxbridge-flags") (v "1.0.11") (h "1927n0ikz941xa7gq5lk8cpcs8ia9zm02bbrh2iwylzms0kcf7p2") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.12 (c (n "cxxbridge-flags") (v "1.0.12") (h "0n8hndagm1irr2vkd8a34ik6xi0fswxxnsh3ij59vmzqlczhccqd") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.13 (c (n "cxxbridge-flags") (v "1.0.13") (h "1l980zb9hdav1lzn09cb7ip9n1fh2ji8cc5b3bkadamsl42xhfzy") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.14 (c (n "cxxbridge-flags") (v "1.0.14") (h "02zi6a4gksdgkq95664c4arzrygx5fqnh9kypksv57cbxaqlzwjb") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.15 (c (n "cxxbridge-flags") (v "1.0.15") (h "1gkkfb18h2c4rz5rwycnp3sylv22p6cq9ddya72g9d182fb438j3") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.16 (c (n "cxxbridge-flags") (v "1.0.16") (h "1vsqkgjsv91ap9zl39rlar4fbg76zzj2k7gzkcznb6qiv0jkqrxd") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.17 (c (n "cxxbridge-flags") (v "1.0.17") (h "19d3y5j6x7blhszh06hp31v7zm70l0w2zhrirkbx916gi5innjnl") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.18 (c (n "cxxbridge-flags") (v "1.0.18") (h "0cmbk25dk3f6p4ijg6a4h855mva54kgn4lclraw48wvnl9qzwfsz") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.19 (c (n "cxxbridge-flags") (v "1.0.19") (h "11qbgpxffi938naz6yyixhxxax3xbhwr14s26ny203hpbi4rshxn") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.20 (c (n "cxxbridge-flags") (v "1.0.20") (h "1q5d88slqby22fl8xbydgw4p08laxv35zpfg4f91ikd54yrqx33b") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.21 (c (n "cxxbridge-flags") (v "1.0.21") (h "0z2wrfbnz77zbsar8w2cim57c4jz2mmqdn1v3lpz118kqlxr5r6f") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.22 (c (n "cxxbridge-flags") (v "1.0.22") (h "1nm2737l0walkkhhmzavcg2pvlpkk23av7lhfrhc4rnj2wicqfpi") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.23 (c (n "cxxbridge-flags") (v "1.0.23") (h "1w3f5g45d5y2cz20pbpn3hh5i8zvmbrcb1bwb15479fyzb84wslg") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.24 (c (n "cxxbridge-flags") (v "1.0.24") (h "14y5c2fmqb6parr240lzfwifyk2pjjsn26x99qzpfyfvh566pq88") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.25 (c (n "cxxbridge-flags") (v "1.0.25") (h "1vd0595xglwpba4lk66xwhcq8rqf9ng6az59zwnagr86i96djiag") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.26 (c (n "cxxbridge-flags") (v "1.0.26") (h "1yjnxkx5rmzk0gwda9n0lpklagvagdrvr9y3gmv5x2rqkv5sqz8p") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.27 (c (n "cxxbridge-flags") (v "1.0.27") (h "1n9s2hwairmvkmhinw4fk7hykng61hc00sj4a55pyikp3kg5lzc6") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.28 (c (n "cxxbridge-flags") (v "1.0.28") (h "111k5gvp0gyx2rz374afjk5ppfrvd04g80fwvyrvx8s5bawrwhcj") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.29 (c (n "cxxbridge-flags") (v "1.0.29") (h "0qkhrb3fkzmgx3acn24fqnqdsgv761pz8ahqxi1kpck6qb7f582r") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.30 (c (n "cxxbridge-flags") (v "1.0.30") (h "0as1mmzj629i3q7nbxdp0r9vfm1vk0ygrz302a32c6y46zjr1vlb") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.31 (c (n "cxxbridge-flags") (v "1.0.31") (h "0szjp2liddc1ycas25q49i98i963pblbg9ajxv14ch286csijf71") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.32 (c (n "cxxbridge-flags") (v "1.0.32") (h "0i5qhf2h721r28mqq85nmsmp4nm4dmxqi0vrgzrvhgb8ybmk28xb") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.33 (c (n "cxxbridge-flags") (v "1.0.33") (h "0qvjhwlpdsxn4bsiswk3lpjhwi23c20rihih9xcvf73d6lvix1fa") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.34 (c (n "cxxbridge-flags") (v "1.0.34") (h "1qlpp5mmmayirgnzfpv3ira43vq6nh5pl452cd7xy8vawwdlc8fa") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.35 (c (n "cxxbridge-flags") (v "1.0.35") (h "0m5495hnv33c8kbkbwqy3dkx7n8lp9hpipk7x6qxbkxcadq8z2hk") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.36 (c (n "cxxbridge-flags") (v "1.0.36") (h "194gmig73l3facqrkjr1fgiyvyn52hx7m7my8aphkvg6qn2icway") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.37 (c (n "cxxbridge-flags") (v "1.0.37") (h "16s542pnp36gls585883iipmjmlkl5b9870cd75hv8dh8x43ijhd") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.38 (c (n "cxxbridge-flags") (v "1.0.38") (h "083k4rfpj2filghcpfgk9ngr7l8yxb9v6d38k5vag8rn6mkdbv3f") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.39 (c (n "cxxbridge-flags") (v "1.0.39") (h "1y5w7vqa3asn6pvv9i1cpqxa7b784jxvw4lh2d71mignba3414dr") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.40 (c (n "cxxbridge-flags") (v "1.0.40") (h "14mqyp0bwv7gmbmsh2ddbnffcnwx5wcrx10s11y3m75mzs4prmr4") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.41 (c (n "cxxbridge-flags") (v "1.0.41") (h "14l6rsp1hgzr7zlfjx3zjpx6g2frl5h7xq9pz6657kh0ax7bk64j") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.42 (c (n "cxxbridge-flags") (v "1.0.42") (h "02xg1p8v2x2fs8655gxz9ka7p7k6m22h33hxryz9bwrf7zyhpjbi") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.43 (c (n "cxxbridge-flags") (v "1.0.43") (h "1k1wqcck1w0fimvl324df4x98h2agz5mj2w8an4nalxazs3gpnis") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.44 (c (n "cxxbridge-flags") (v "1.0.44") (h "095f6w2cl6grqcz0x2xj41nq7jsgg032vhzmvh304djmn77a76r3") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.45 (c (n "cxxbridge-flags") (v "1.0.45") (h "0s6giwx3rvgxdljj9gg4cxk4g5pihbcgkqf0icv5lrbzhf8a8hs8") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.46 (c (n "cxxbridge-flags") (v "1.0.46") (h "0dj75schrbraxfbnpka3ajj037l8020l0nhl8wsr38q5pc7m5g0y") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.47 (c (n "cxxbridge-flags") (v "1.0.47") (h "11hb0zk71m3sz8sk26x0vv68cch1f4bqr7wh8vday5bn02srhr2b") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.48 (c (n "cxxbridge-flags") (v "1.0.48") (h "06965k4zbqz1g9wckzl6rrgcfi7iddix5wpvfpxi86pqph9hamil") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.49 (c (n "cxxbridge-flags") (v "1.0.49") (h "18cv8a8sgyiwfqspdyfq18jizf0rlhg90ibdl0zp8jhcv498s6gr") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.50 (c (n "cxxbridge-flags") (v "1.0.50") (h "0j6g39kyp8296vm68li0h3ggymfy4d9pw2anhsjabplag806l55p") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.51 (c (n "cxxbridge-flags") (v "1.0.51") (h "080bxs97f5kg6fg7wbxgzd6461kq4blvhan6srwajj1sw6hcii0k") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.52 (c (n "cxxbridge-flags") (v "1.0.52") (h "07d4m46qw5flgc27sadb7nwps4qp4p5s01ijcm8ns27dsqwdxhpc") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.53 (c (n "cxxbridge-flags") (v "1.0.53") (h "1b3r7d9p28fqy8a429f4bfsx26qam1kl0symqslfglqgskbzcad4") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.54 (c (n "cxxbridge-flags") (v "1.0.54") (h "1w2y3h3k7jbx8sjk3ykwl7gp2gdqmlrv66qgrirjn2n7macxz5hk") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.55 (c (n "cxxbridge-flags") (v "1.0.55") (h "1x7m6yqgy0fnr2y5bf09asfih1a1cds3lzhxxgq4rgj0b3ibvm24") (f (quote (("default") ("c++20") ("c++17") ("c++14"))))))

(define-public crate-cxxbridge-flags-1.0.56 (c (n "cxxbridge-flags") (v "1.0.56") (h "1qrkl9jccvmjqi8q1w7iiwlaqqdffahcanbihv42rfabs6a3yb2r") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.57 (c (n "cxxbridge-flags") (v "1.0.57") (h "1nhyas3x8dgd7lanaqvmndl55yijmh2nwf92r9ja5wwhmqm2c67i") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.58 (c (n "cxxbridge-flags") (v "1.0.58") (h "0mmanyj6h3hws7m8w7vcgzr47d5n0c1qfs0c249njiir9lbhld9n") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.59 (c (n "cxxbridge-flags") (v "1.0.59") (h "06v4pb1yv2mzjhvhcyawgfysckswjxfnjw44w630qb5paqgma6ni") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.60 (c (n "cxxbridge-flags") (v "1.0.60") (h "09dii9bgr0ab1svhw1dps0jgak2yljjbs132jd3g6vachk278i1r") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.61 (c (n "cxxbridge-flags") (v "1.0.61") (h "1mngb8qq5jj1fva4gp2dsissrvxmgn251p180qhiq8mcm7pwyjkv") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.62 (c (n "cxxbridge-flags") (v "1.0.62") (h "1cynva455fa9f1dvcyd0xgv5gfmn4d24vcjbq4awv8693mq3682d") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.63 (c (n "cxxbridge-flags") (v "1.0.63") (h "0dim2r3856dm3kfm500ql6wv3hmxhk3dcdb1cy33vj5havaxli0v") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.64 (c (n "cxxbridge-flags") (v "1.0.64") (h "1r4azd5vm5h312qyx5dl54j1hvgnrqxnn15shmycs09z3l92kihw") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.65 (c (n "cxxbridge-flags") (v "1.0.65") (h "08wjvqvvrhjmqi5sx1y2x04x90q348n50bjppy6kl86m3f65q4k3") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.66 (c (n "cxxbridge-flags") (v "1.0.66") (h "19rxdkj7jfrqnnqf4f5xf0p0hic1l2bhn2jn2bgp2r38qqj04rqa") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.67 (c (n "cxxbridge-flags") (v "1.0.67") (h "1ifsw3r9v5rwhh0gjbjwddbpsfv1nx477b7234wad07axx1v97lr") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.68 (c (n "cxxbridge-flags") (v "1.0.68") (h "0lq84whswryvql2a30p51x41p8qfrlvhphlfrsjbaz2idl6av51q") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.69 (c (n "cxxbridge-flags") (v "1.0.69") (h "1n84n73ha4d0d5wh6l08f66b0ssv7l58npbxvh7sj28xq44xncki") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.70 (c (n "cxxbridge-flags") (v "1.0.70") (h "0p1icv4jbjqbfq0n19hi6j7wz28savzmvgqzv3xbixqfv19wxi7i") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.71 (c (n "cxxbridge-flags") (v "1.0.71") (h "0j6xgqwaib31qzczsk0rhn5dqvwkzi0xa5vqpirwjdf9zm7jpvqg") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.72 (c (n "cxxbridge-flags") (v "1.0.72") (h "0xm3x13bc21rar1g9xkxracnhvgjz5q35q5gq1lrnnphc51aiprg") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.73 (c (n "cxxbridge-flags") (v "1.0.73") (h "1kqsjcs0sm2yz430w09gc9g59bh2qp36m368nmyjg05g2my7hszl") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.74 (c (n "cxxbridge-flags") (v "1.0.74") (h "0ypwy1j9lngvji4p3zsqypfwzddz8mxvb6pla7wd3l0a8s33l9ak") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.75 (c (n "cxxbridge-flags") (v "1.0.75") (h "021jrxqix1c226l4vhkdz13nzg185v5gvs04228fbmpgfcarn1nj") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.76 (c (n "cxxbridge-flags") (v "1.0.76") (h "0fn3fakb7cw99ryc361zzspvqg49j5jyqvn1adxzj3301adx1grf") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.77 (c (n "cxxbridge-flags") (v "1.0.77") (h "014c71mlldy6lczfypigjad7mamgqmip7ysanvb6i7laiych9vg8") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.78 (c (n "cxxbridge-flags") (v "1.0.78") (h "177a4zv3nd1n1cxmhq3z4gdsyf9c1qzkd31c26ayq74dp1h6992n") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.79 (c (n "cxxbridge-flags") (v "1.0.79") (h "0y5b90s9xfdkf2i6kh97rx3ygz3m3hxqvv6z2a0dlgjm02dikllr") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.80 (c (n "cxxbridge-flags") (v "1.0.80") (h "0sn5gh26cmzsqb2k3q3zxw4mvlxnsi0pqr06113g2jj79d13f9g7") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.81 (c (n "cxxbridge-flags") (v "1.0.81") (h "07ygmapwbvnjz3li45yzy81c7m3ahm13nz10n734v4scg7j218lc") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.82 (c (n "cxxbridge-flags") (v "1.0.82") (h "0wfgqzi67fp025h36cb79cn6l6gxdx3iy9yb4zpyx7b6y4m9l2l2") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.83 (c (n "cxxbridge-flags") (v "1.0.83") (h "04ivanh4p00r8nk8hmavc9s403sb6d47c5rj1x76glbza4whl15c") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.84 (c (n "cxxbridge-flags") (v "1.0.84") (h "1d0ahw41n09gd69xxf3nmbk62lzf9q94hbnxc5xiq7034xiqnxxy") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.85 (c (n "cxxbridge-flags") (v "1.0.85") (h "0ngvkpwvxdp6ymnqrrccybklbdv7dhygil075gnr9rg4zmif38v9") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.86 (c (n "cxxbridge-flags") (v "1.0.86") (h "073qx3gnf8df9xzfy9xfcz9b79m0638x4a0isfq2fb527g4hpdb1") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.48")))

(define-public crate-cxxbridge-flags-1.0.87 (c (n "cxxbridge-flags") (v "1.0.87") (h "1smaldilxyifgsw5gl194sfkkz7nmjwi116a3bc1ww53wff13is3") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.88 (c (n "cxxbridge-flags") (v "1.0.88") (h "0wg9ki4zr3hkg41wpvbl92yjip9vy0il4bd0rlgbq2fgi83vnsy2") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.89 (c (n "cxxbridge-flags") (v "1.0.89") (h "12pb3wzdjqfkvrjlbmzfp3dkbn133yb3jjsbdq6v9aacl5dx7p28") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.90 (c (n "cxxbridge-flags") (v "1.0.90") (h "00yw155f72g8pnwqyzfskfapzblajmdr59mxrmpdqw2wgisycz25") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.91 (c (n "cxxbridge-flags") (v "1.0.91") (h "1gvrwi7ghbl76zpnhyr2mc9q1r72z7hq1bq3av52ka0pnyw9ivx2") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.92 (c (n "cxxbridge-flags") (v "1.0.92") (h "15zcakzjxjjv0y9hlq3fghqxsfr18fiva2wc3kzwr58fcld1q84h") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.93 (c (n "cxxbridge-flags") (v "1.0.93") (h "0ccxpl7wnvlv4synsr3sd46z112k1dgxaabdb49cn5c9n0p2xigd") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.94 (c (n "cxxbridge-flags") (v "1.0.94") (h "1fzpqw12j8maki4m19fxrnf6xi2n988q9fgv799qq1p4wwm1fi3r") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.95 (c (n "cxxbridge-flags") (v "1.0.95") (h "1fx7ckmpc7wrc4z5smb4pwiv7r9lvs0h0pf6nkilzs1q29178848") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.96 (c (n "cxxbridge-flags") (v "1.0.96") (h "05qp3x3zj24d7fi8kyfb9xnqpss06xlqwcph2wxb406v96xfa4wn") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.97 (c (n "cxxbridge-flags") (v "1.0.97") (h "1j1n3r0z78n5sbij38ws87dzganrwc8maj188z6qq03wjpnicf4d") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.98 (c (n "cxxbridge-flags") (v "1.0.98") (h "0q7iflpgwyqpf10blvbv6prnblzz7j32zqpbhnkls2yhlmql087r") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.99 (c (n "cxxbridge-flags") (v "1.0.99") (h "1frcxmlfv5bc07h41wiv8ayixccds7cgp2hxdhq71q5nq8f7r657") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.100 (c (n "cxxbridge-flags") (v "1.0.100") (h "0mv9p3z7qisbzkzjm1wrw7prcw67jc51gdzsym1b76vfgflaarjr") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.101 (c (n "cxxbridge-flags") (v "1.0.101") (h "04zdvpn2rpnll19chy3mwdd0r1ff4n1szkjnivyamr3jf0an540d") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.102 (c (n "cxxbridge-flags") (v "1.0.102") (h "1l2ihynlw088jlh0ldw78f7nh1rfx5jshvr8hb4bd6pxh169z8bq") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.103 (c (n "cxxbridge-flags") (v "1.0.103") (h "0l7sjzsixmxqsz5g8s7fhj3rlyqjx17phw93lzcf1iq9rh4xlq7d") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.104 (c (n "cxxbridge-flags") (v "1.0.104") (h "15fmj06w2saj9csf6wak84nvjns1w4lwm4gsgizyifipp6xng5j0") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.105 (c (n "cxxbridge-flags") (v "1.0.105") (h "03ml9cq1mvbfadr3i020s9nzj8xvgb40979600n9yjywm8wc5s6n") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.106 (c (n "cxxbridge-flags") (v "1.0.106") (h "028qch471c80gc4105i3bdq2865azp5zm8afmn945cxlvzhzb9p2") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.107 (c (n "cxxbridge-flags") (v "1.0.107") (h "18hxm7dp8k58zsxniaspg11nw0ymwzpk1viw8zzy56123ng8v210") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.108 (c (n "cxxbridge-flags") (v "1.0.108") (h "0jsmflcm66pfqibrbsiby3ys7clj0zsnsiirnrz2pfh837zhl0yp") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.109 (c (n "cxxbridge-flags") (v "1.0.109") (h "0f99bn3mxvmdl3nxv9nnai9z1cyxhqd7vbcc9k3z187yxhkmhhcl") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.110 (c (n "cxxbridge-flags") (v "1.0.110") (h "0i3zhmskpkfrkhsqxsllcr9basmc3wsxcnvzyrihs1b1zivx3z86") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.111 (c (n "cxxbridge-flags") (v "1.0.111") (h "112qpq61izivz6ykdcd89hign2jablncp9adg2bjc97a6jzay4c5") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.112 (c (n "cxxbridge-flags") (v "1.0.112") (h "0iz513l1mvf6jj8l20hrn4dj2l1giv0ngilmj99qq8d5xch1na22") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.113 (c (n "cxxbridge-flags") (v "1.0.113") (h "14w7mzjf0lhh5lghzvq3yx975m4avi79ivshfpxxklv8gi3b0h5g") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.114 (c (n "cxxbridge-flags") (v "1.0.114") (h "04p106c6khzp0zlp9j9k55nhc9fabi4zv2ca5kw8kjkc34kiv65n") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.115 (c (n "cxxbridge-flags") (v "1.0.115") (h "0p3lhg2r2i8x8k7sgczny9cal9davggl94ggxz29g93bsmhysgvg") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.116 (c (n "cxxbridge-flags") (v "1.0.116") (h "1p0qsqk991hm2f1d51nhk4sf4hakhc4clmmdymwgpm615z8877jq") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.117 (c (n "cxxbridge-flags") (v "1.0.117") (h "0hsw3ha0ds1lfq1wv9vdnp6hsdc0fzkjx72zjdd5yl0qzp2b8zks") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.118 (c (n "cxxbridge-flags") (v "1.0.118") (h "04adqzgwr4sfdwday9qn1f1f3fcj6dmm05fh3k0i2y7nbn3wysl8") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.119 (c (n "cxxbridge-flags") (v "1.0.119") (h "1rfgpv5ilmc27nw27famdnx7x0ngpgi0r3k1c5y4p9gs5wsgfzx8") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.120 (c (n "cxxbridge-flags") (v "1.0.120") (h "11v2vl9vrwl9b32lxllbrpdx1gxggmx6s0nwv36lkqlplv3il6kh") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.121 (c (n "cxxbridge-flags") (v "1.0.121") (h "1h2mi3grwnidr5lj09yq0r1lksp6i1p3vsfrw40sbd1g5vfwm3dy") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.60")))

(define-public crate-cxxbridge-flags-1.0.122 (c (n "cxxbridge-flags") (v "1.0.122") (h "0g7i1lkmfq1wfp3k6dkz8pcv7nbjcafbndlzrc51qvw49ad7k338") (f (quote (("default") ("c++20") ("c++17") ("c++14")))) (r "1.63")))

