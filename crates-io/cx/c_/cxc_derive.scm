(define-module (crates-io cx c_ cxc_derive) #:use-module (crates-io))

(define-public crate-cxc_derive-0.1.0 (c (n "cxc_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14bvcb8njf39ivpwhxnqgycn64wanq2z9rbb3iai1bk5k7f38a3j")))

(define-public crate-cxc_derive-0.2.0 (c (n "cxc_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxy9xkhf0jyv60zfif027iaf8avh96glaiyb2gb5673bdz93z1a")))

