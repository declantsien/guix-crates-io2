(define-module (crates-io vb #{64}# vb64) #:use-module (crates-io))

(define-public crate-vb64-0.1.0 (c (n "vb64") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "15a325s1yjvridn69wxq2jihank02sfgcy1aszirdmppnm5byf75")))

(define-public crate-vb64-0.1.1 (c (n "vb64") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "09bjpq30csj221j20rdh37k48f80x5pd156n6y10r17ps1qyhz1y")))

(define-public crate-vb64-0.1.2 (c (n "vb64") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "09y761141qisrxh16q250gqq8jl84lr3wjqxh06f9x8ps97s89na")))

