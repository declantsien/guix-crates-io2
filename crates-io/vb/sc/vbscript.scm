(define-module (crates-io vb sc vbscript) #:use-module (crates-io))

(define-public crate-vbscript-0.1.0 (c (n "vbscript") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "00xnzr32iywxr4sgjch0yx39cdnyh4pv79zvja718zpi87490axr")))

(define-public crate-vbscript-0.2.0 (c (n "vbscript") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "17fg68da93cwcwgbdl87k016d918ra62v40j60jcz361ss5x6akc")))

