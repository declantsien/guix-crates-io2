(define-module (crates-io vb an vban) #:use-module (crates-io))

(define-public crate-vban-0.1.0-alpha1 (c (n "vban") (v "0.1.0-alpha1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ja8ziwkpfyjmrckvamd8m6z48pn2mv5n2vw92jcjrlr7nv1h28s")))

