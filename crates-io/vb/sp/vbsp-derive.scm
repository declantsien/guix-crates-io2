(define-module (crates-io vb sp vbsp-derive) #:use-module (crates-io))

(define-public crate-vbsp-derive-0.1.0 (c (n "vbsp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)) (d (n "syn_util") (r "^0.4.2") (d #t) (k 0)))) (h "1bdsgaalr679aq9x68ihp74xscj0ggmwjzrq09sinjzkmmznipks") (f (quote (("__vbsp_as_self"))))))

