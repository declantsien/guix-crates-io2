(define-module (crates-io vb do vbdot) #:use-module (crates-io))

(define-public crate-vbdot-0.1.0 (c (n "vbdot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "relative-path") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (f (quote ("serde"))) (d #t) (k 0)))) (h "00hgvf0xmwms1vjqn5mmdqs1yj4dcf81mb5p21zipx8384syzxly")))

(define-public crate-vbdot-0.1.1 (c (n "vbdot") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "relative-path") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (f (quote ("serde"))) (d #t) (k 0)))) (h "1cc4nzp559rs1x3ra6b1icp1ymhhl0bf3qwwdcfqkpyw2irzwrdh")))

