(define-module (crates-io vb lk vblk) #:use-module (crates-io))

(define-public crate-vblk-0.1.0 (c (n "vblk") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 2)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "0mgb1mmphlln82mx8w59nxnx8521z743sdqcgpdy0iklr67hkr35")))

(define-public crate-vblk-0.1.1 (c (n "vblk") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 2)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "1rjy6sjk9i7cazcf54qp33firydfkgy4zh26i3s2cvsjwb6h34yj")))

(define-public crate-vblk-0.1.2 (c (n "vblk") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 2)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "zerocopy") (r "^0.2") (d #t) (k 0)))) (h "1n8vq21f02206sbpnzcfw109n1gc6xic6xahp4nzgpvlk71qfqla")))

