(define-module (crates-io vb ox vboxhelper) #:use-module (crates-io))

(define-public crate-vboxhelper-0.1.0 (c (n "vboxhelper") (v "0.1.0") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0zcr7wnkz2432bw4rzjwfdwqa8z4i5mzc3qg9v6qwmlhfclaj286")))

(define-public crate-vboxhelper-0.1.1 (c (n "vboxhelper") (v "0.1.1") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1bbl8jw5q2l4ynwj76smmp0f3il7jbz46vkaz0hkamwspqsg2c8y")))

(define-public crate-vboxhelper-0.1.3 (c (n "vboxhelper") (v "0.1.3") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0img87197fr9bg8p50pg4ypmcvnl2xqpfiw5x8lh2356nk86kynq")))

(define-public crate-vboxhelper-0.1.6 (c (n "vboxhelper") (v "0.1.6") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0gddd3gzn0m02a3nmdhc08gnbjd55sh4smm8lp61sprnv29xqzky")))

(define-public crate-vboxhelper-0.1.8 (c (n "vboxhelper") (v "0.1.8") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1ygvd7mxr3cbb4knyg7c9vd6ypddy98laz1b0l8320apiwvxdabl")))

(define-public crate-vboxhelper-0.2.0 (c (n "vboxhelper") (v "0.2.0") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1y2a91piydqg4i3ilh5wc5vdlvp2zc12kj665p0a4ana6w127dvm")))

(define-public crate-vboxhelper-0.3.0 (c (n "vboxhelper") (v "0.3.0") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1rfw5q0656zv0y4my71hdb56ig4z4498d16zn949wygvc8zmamjh") (y #t)))

(define-public crate-vboxhelper-0.3.1 (c (n "vboxhelper") (v "0.3.1") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "05mqc812b6bkbwfsf5027bv4k6hjd640n1blk5dhi903zdsxxgz9") (y #t)))

(define-public crate-vboxhelper-0.3.2 (c (n "vboxhelper") (v "0.3.2") (d (list (d (n "eui48") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "01nl1kbz8fh6lqb3c2990rq44pd4mfl8x3rh285l7j7hzghxl73c")))

(define-public crate-vboxhelper-0.3.3 (c (n "vboxhelper") (v "0.3.3") (d (list (d (n "eui48") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1kbknpcbnjp4dhcmlzahvpmf0nrlbhs4p1yfqgcwmw2g5s1hxqff")))

