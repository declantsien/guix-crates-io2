(define-module (crates-io r6 #{4d}# r64drive) #:use-module (crates-io))

(define-public crate-r64drive-0.1.0 (c (n "r64drive") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "safe-ftdi") (r "^0.1.0") (d #t) (k 0)))) (h "0hwif1rsglmxqp2w10j2n6brgw6y6iw1nj4pbmrkx1l57hwy588m")))

(define-public crate-r64drive-0.2.0 (c (n "r64drive") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "safe-ftdi") (r "^0.2.1") (d #t) (k 0)))) (h "03klkyzsmb1j26ngh5gvjh6l5c03il5696kv6v7q1lb6hsm0gc6a")))

(define-public crate-r64drive-0.2.2 (c (n "r64drive") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "safe-ftdi") (r "^0.2.1") (d #t) (k 0)))) (h "1hjygyhx6yfy77x25i7bpf3mryd33xbnmjqndxshc3ciidg49myj")))

