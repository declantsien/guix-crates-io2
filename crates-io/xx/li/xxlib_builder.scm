(define-module (crates-io xx li xxlib_builder) #:use-module (crates-io))

(define-public crate-xxlib_builder-0.1.0 (c (n "xxlib_builder") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dbhk25qdsjbd4pl416svxk6x4l6i9l0bvx2422kw5z6z4c0v5xd")))

(define-public crate-xxlib_builder-0.1.1 (c (n "xxlib_builder") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "192fvlmkqrwcdpzbc3dbs2i91y2il19xcrz5fqs78asy6nffwn7f")))

(define-public crate-xxlib_builder-0.2.0 (c (n "xxlib_builder") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ypawkxik7wsh3dwvcygnrxa332w12ngis6hskj5b3pi04dh9kqr")))

(define-public crate-xxlib_builder-0.2.1 (c (n "xxlib_builder") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aa2s7m2qzkn51aykc32ixfy2wkwjixcjjn75z1w58gm3cw72naw")))

(define-public crate-xxlib_builder-0.2.2 (c (n "xxlib_builder") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v6yxn7h6yl0jg4bd4fsifkb5lpl62nkc6mkrff5g4bdbn2q2lsj")))

(define-public crate-xxlib_builder-0.2.3 (c (n "xxlib_builder") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q3mn2isbbjmq5im15f882dryg3lvaslz3vwamh54ablqsz3nj61")))

