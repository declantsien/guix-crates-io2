(define-module (crates-io xx ht xxhttp) #:use-module (crates-io))

(define-public crate-xxhttp-0.1.0 (c (n "xxhttp") (v "0.1.0") (h "104f7n3jqy6ck51kj52svld1labfyrx74nypbb1bkq2s0291r1k1")))

(define-public crate-xxhttp-0.1.1 (c (n "xxhttp") (v "0.1.1") (h "0agcykvmd73a14ydzx18rpg630qkxk58jf5pvghbd83bd7n31jc4")))

(define-public crate-xxhttp-0.1.2 (c (n "xxhttp") (v "0.1.2") (h "035jmklb2xvmxa4j7cr18x3k94hkcjlb3pgb9mmggnd9cnw24x8q")))

(define-public crate-xxhttp-0.1.3 (c (n "xxhttp") (v "0.1.3") (h "01zpb4bgq8cx5y9p0b8idajd2xd5zakz9warswc04gmfvp0bp287")))

(define-public crate-xxhttp-0.1.4 (c (n "xxhttp") (v "0.1.4") (h "1yj8nflghad1lv3rq3vdpzvq46b16ld1lgkilqzwmmmsprjwmh39")))

