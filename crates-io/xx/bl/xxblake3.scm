(define-module (crates-io xx bl xxblake3) #:use-module (crates-io))

(define-public crate-xxblake3-0.0.1 (c (n "xxblake3") (v "0.0.1") (d (list (d (n "blake3") (r "^0.3.8") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.0") (d #t) (k 0)))) (h "1l6h305skfxfyq0lmz2kbk5qnljrx07hh58x2ay36g80qw7hw733")))

(define-public crate-xxblake3-0.0.2 (c (n "xxblake3") (v "0.0.2") (d (list (d (n "blake3") (r "^1.2.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "0hmsr1xpvri9mswji2r234iskffbbqw2ng8gch2mcx19s1v2av14")))

(define-public crate-xxblake3-0.0.3 (c (n "xxblake3") (v "0.0.3") (d (list (d (n "blake3") (r "^1.2.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "0r5f89c4f31m89xr0yrarsibw0ygrdi08br798r7022k48x4fzql")))

(define-public crate-xxblake3-0.0.4 (c (n "xxblake3") (v "0.0.4") (d (list (d (n "blake3") (r "^1.2.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.1") (d #t) (k 0)))) (h "0a6cgvnc498kv861rs3s5n143hz8h32wa3q417ka10lc0v82pdn1")))

(define-public crate-xxblake3-0.0.5 (c (n "xxblake3") (v "0.0.5") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.2") (d #t) (k 0)))) (h "15mm2r0czyj0d2n02hsjsd4hvmmiag67lkw3lnlhbvgmgw7f87fc")))

(define-public crate-xxblake3-0.0.6 (c (n "xxblake3") (v "0.0.6") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "00v36gzi1yl2mqdk2j6vxylzfbfd4yhxblbqba2h5ncldqmk2rms")))

(define-public crate-xxblake3-0.0.7 (c (n "xxblake3") (v "0.0.7") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "10shqkaag3f2m92ycbw0hpzmsk5j1d9x9g6piqk04d8iya7davlh")))

(define-public crate-xxblake3-0.0.8 (c (n "xxblake3") (v "0.0.8") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0615x64daz25sz8hivksmn88hvjpqcr29csdkyj1iavq35ni5s6v")))

(define-public crate-xxblake3-0.0.9 (c (n "xxblake3") (v "0.0.9") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1vlrivkg146qnzav63z0n7lc37sqj5bx27f74i63fiiswljg8k5h")))

