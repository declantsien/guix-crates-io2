(define-module (crates-io xx ha xxhash) #:use-module (crates-io))

(define-public crate-xxhash-0.0.1 (c (n "xxhash") (v "0.0.1") (h "0vx03fvsm5d9c0h00ddiby6mmf4sj5jzvlqvhffnv54gmlw1z0c0")))

(define-public crate-xxhash-0.0.2 (c (n "xxhash") (v "0.0.2") (h "0ymbin8kyc8mdi45rb0n4ddzzw33mx3n7v6i0pqjjgmxidifygwm")))

(define-public crate-xxhash-0.0.3 (c (n "xxhash") (v "0.0.3") (h "06d28v7v9fyr5vhzigy2cbjyz1zn714pnw2i31hjra1si3vqsl2s")))

(define-public crate-xxhash-0.0.4 (c (n "xxhash") (v "0.0.4") (h "0343x43z3h6jsxq89dppg96gvbvgx7pa9zd5y8iv3lvkha5ssxc2")))

(define-public crate-xxhash-0.0.5 (c (n "xxhash") (v "0.0.5") (h "1pj1rzgjb7xdb6i608qp6bw8zgmbv26fik2fhr4fp9z3v81s2mmr")))

(define-public crate-xxhash-0.0.6 (c (n "xxhash") (v "0.0.6") (h "111j4rdypyzvij7lr7rz0bv4llwj6gyb2ygwdv2i4a59q4s83phg")))

(define-public crate-xxhash-0.0.8 (c (n "xxhash") (v "0.0.8") (h "14azdq4irwyh8sq2z37nmmansimqcmg99bcrd7sq70scg8857ijw")))

