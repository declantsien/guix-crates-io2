(define-module (crates-io xx ha xxhash-c-sys) #:use-module (crates-io))

(define-public crate-xxhash-c-sys-0.8.0 (c (n "xxhash-c-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1n9plnppyiirixyn47lcydkw779l14bn99dh4wbdw3hgx646v31y") (y #t)))

(define-public crate-xxhash-c-sys-0.8.0+1 (c (n "xxhash-c-sys") (v "0.8.0+1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1sp607h4znd4iri548knv5wmgisn57dcv9nd8h8b958zl0hlhgfp") (y #t)))

(define-public crate-xxhash-c-sys-0.8.1 (c (n "xxhash-c-sys") (v "0.8.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0ym6pk6fv8cmqqnnsppkhdxqxdjw517jil3zw1csvssvgjqd64w1") (y #t)))

(define-public crate-xxhash-c-sys-0.8.2 (c (n "xxhash-c-sys") (v "0.8.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1qza589qp0qy0z00506l2q3r2nrv6s6v08k1cpj406qwq9299gny")))

(define-public crate-xxhash-c-sys-0.8.3 (c (n "xxhash-c-sys") (v "0.8.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1rd3m2dz3qk20c734hcyjjlvciv4g37idqc5g9a57bxyz7wq4lib")))

(define-public crate-xxhash-c-sys-0.8.4 (c (n "xxhash-c-sys") (v "0.8.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0iv07nz05ynclrb18snmq5ry7y5jgl372qqg0fn43snklb6l8336")))

(define-public crate-xxhash-c-sys-0.8.5 (c (n "xxhash-c-sys") (v "0.8.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0rxa6zyp7faw9gdq66fy02c9dnh1js34kdanr84aflmy1894zkb3") (f (quote (("no_std")))) (y #t)))

(define-public crate-xxhash-c-sys-0.8.6 (c (n "xxhash-c-sys") (v "0.8.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "13g8cy4w91nwmficbmkrqp40axpwidcna588gzxr47m0ld99jh6j") (f (quote (("no_std"))))))

