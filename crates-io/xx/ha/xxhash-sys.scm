(define-module (crates-io xx ha xxhash-sys) #:use-module (crates-io))

(define-public crate-xxhash-sys-0.1.0 (c (n "xxhash-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l44slbnnzawyfalcwd928wx4bqbs827cjriigis7zjrw0d29dv5")))

