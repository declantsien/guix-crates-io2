(define-module (crates-io xx ha xxhash2) #:use-module (crates-io))

(define-public crate-xxhash2-0.1.0 (c (n "xxhash2") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xxhash-sys") (r "^0.1") (d #t) (k 0)))) (h "10xihhbnxpnn6mzqghsv3nm7dw8p8pnjbksk2jf3fw038f3qhdnx")))

