(define-module (crates-io xx ha xxhash-rust) #:use-module (crates-io))

(define-public crate-xxhash-rust-0.8.0-beta.1 (c (n "xxhash-rust") (v "0.8.0-beta.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "1xibinxz4kz0z72vj7fzgs2h4925ylk2ylihrbi2hgq4178wqdfz") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0-beta.2 (c (n "xxhash-rust") (v "0.8.0-beta.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "1fga3x2acfhamv0cp3m0nknfzskzzkdswg3fyr49znd4jb85ipsg") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0-beta.3 (c (n "xxhash-rust") (v "0.8.0-beta.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0nfhihqxv4pspimv4lvyfsqcqv61pxm14xn0iml51x03sbls64nd") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0-beta.4 (c (n "xxhash-rust") (v "0.8.0-beta.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0hn5kvd611g1gga9ws70ka1b0spsnm4mf7grgqjljlnsgq8wa7m6") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0-beta.5 (c (n "xxhash-rust") (v "0.8.0-beta.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0blfh4m46mfzrjm53vq49yx5d9wm6j7xm7vwkrj0rbv1s0p0yha6") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0-beta.6 (c (n "xxhash-rust") (v "0.8.0-beta.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "1qqfw3vxzwnjgws5cv4h5wwkjfpkq5vkasc8a31v8n0v3jvdypmq") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.0 (c (n "xxhash-rust") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0fkpnsgm4zk2nbsfd4q1d31yj4axm2zir5m4cxl07ahz34aj89kd") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.1 (c (n "xxhash-rust") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0pf8q4ky10w28rifxbsvbzpvrd9hfq9j64bgfg81ml0fv4b2xfcl") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (y #t)))

(define-public crate-xxhash-rust-0.8.2 (c (n "xxhash-rust") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "1v9dk6shls1rsmidf2dxdi3460bn7ingqgvn5mf7prgnxmdy2xg5") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.3 (c (n "xxhash-rust") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "0bbhci48fj83v08sbhk981xc26ckwb2wpnlzdbhbjcb73i222vw8") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.4 (c (n "xxhash-rust") (v "0.8.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "097gmzmbf47p0k80b8q8xi62rzx4961isq5vhh8xcxrk81xnp8c3") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.5 (c (n "xxhash-rust") (v "0.8.5") (d (list (d (n "getrandom") (r "^0") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 2)))) (h "1ryjsyz6g8m4h9b1h6brgcghlhjga1l5fx7xs6w6wa7c9vm18j87") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.6 (c (n "xxhash-rust") (v "0.8.6") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.3") (d #t) (k 2)))) (h "0w0dx17w0ydp3n09a1bkh7m8rqwq5gf3zl149cfxfs2ddka72nkk") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.7 (c (n "xxhash-rust") (v "0.8.7") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.4") (d #t) (k 2)))) (h "0yz037yrkn0qa0g0r6733ynd1xbw7zvx58v6qylhyi2kv9wb2a4q") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.8 (c (n "xxhash-rust") (v "0.8.8") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.4") (d #t) (k 2)))) (h "0q9xl4kxibh61631lw9m7if7pkdvq3pp5ss52zdkxs6rirkhdgjk") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.9 (c (n "xxhash-rust") (v "0.8.9") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.4") (d #t) (k 2)))) (h "0wsfw9ni9a9xh65hxq9vgy0isqpglyyfin3864n1is55llmrr4pj") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8.10 (c (n "xxhash-rust") (v "0.8.10") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.6") (d #t) (k 2)))) (h "00zfsfigb6zh0x8aaickkkyd3vyjgnrq36ym04lil7my4lgahzcj") (f (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

