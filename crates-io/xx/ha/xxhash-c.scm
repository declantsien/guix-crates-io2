(define-module (crates-io xx ha xxhash-c) #:use-module (crates-io))

(define-public crate-xxhash-c-0.8.0 (c (n "xxhash-c") (v "0.8.0") (d (list (d (n "get-random-const") (r "^2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.0") (d #t) (k 0)))) (h "162a4zqzxqxd96lrzi0fqdn3w1hwga2y8b9kabm8dir993fzp25m")))

(define-public crate-xxhash-c-0.8.1 (c (n "xxhash-c") (v "0.8.1") (d (list (d (n "get-random-const") (r "^2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0rkg9f3gjj36dgninl9s45wm8h7pl6p4c96j89bn7dd4dkvig2im")))

(define-public crate-xxhash-c-0.8.2 (c (n "xxhash-c") (v "0.8.2") (d (list (d (n "get-random-const") (r "^2") (d #t) (k 2)) (d (n "xxhash-c-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1jc8y6xv9bnmcbzmdwm347rz48a17gx1a7i778ssn0vga7qzyzmb")))

