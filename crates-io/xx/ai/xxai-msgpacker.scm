(define-module (crates-io xx ai xxai-msgpacker) #:use-module (crates-io))

(define-public crate-xxai-msgpacker-0.4.3 (c (n "xxai-msgpacker") (v "0.4.3") (d (list (d (n "msgpacker-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "1bsppcv4mv063239qad1fd0x0b84197zqn10njq4sly0ja0ihdmz") (f (quote (("strict") ("std" "alloc") ("derive" "msgpacker-derive") ("default" "std" "derive") ("alloc"))))))

(define-public crate-xxai-msgpacker-0.4.4 (c (n "xxai-msgpacker") (v "0.4.4") (d (list (d (n "msgpacker-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3") (d #t) (k 2)))) (h "182zcr1db1wgawhbii6lrlzlc9rkilgzzlhb333dlwhnv026yahs") (f (quote (("strict") ("std" "alloc") ("derive" "msgpacker-derive") ("default" "std" "derive") ("alloc"))))))

