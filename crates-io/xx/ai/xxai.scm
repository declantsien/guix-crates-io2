(define-module (crates-io xx ai xxai) #:use-module (crates-io))

(define-public crate-xxai-0.1.0 (c (n "xxai") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base-x") (r "^0.2.11") (d #t) (k 0)))) (h "1pgjyi3wivxnnwmypg90bva0migw1l8v6fj0hg4c10i5xypzn3rf")))

(define-public crate-xxai-0.1.1 (c (n "xxai") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base-x") (r "^0.2.11") (d #t) (k 0)))) (h "1af551ybha69sf671q5disnn7zzjvlaf9ygw14xzxgp6zvaqhmhd")))

(define-public crate-xxai-0.1.2 (c (n "xxai") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base-x") (r "^0.2.11") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.23") (d #t) (k 0)) (d (n "psl") (r "^2.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (d #t) (k 0)))) (h "0fqb7c76hycirrzh4p7yaqwk090dvb829c2yjdspiggkz9zjj6i1")))

