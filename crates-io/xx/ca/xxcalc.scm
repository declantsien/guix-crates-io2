(define-module (crates-io xx ca xxcalc) #:use-module (crates-io))

(define-public crate-xxcalc-0.1.0 (c (n "xxcalc") (v "0.1.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "040b58d30bb156vdxq007kahwcb2m56pbjn6vdc1rpp9ya2s9jns") (y #t)))

(define-public crate-xxcalc-0.1.1 (c (n "xxcalc") (v "0.1.1") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "0s21jmbw1cnydhvxp5bqadf3v79blyr7s6bml0cac5clx4rwy1iv")))

(define-public crate-xxcalc-0.2.0 (c (n "xxcalc") (v "0.2.0") (d (list (d (n "rustyline") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "170mvbg3ws000285889hqddib6nxh2l98awsl1c2q79iaw71ixgk") (f (quote (("interactive" "rustyline"))))))

(define-public crate-xxcalc-0.2.1 (c (n "xxcalc") (v "0.2.1") (d (list (d (n "rustyline") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.2.1") (d #t) (k 0)))) (h "1g6drkcvc2xxhi5i34mh2vg059rhpi2hnwbzrl3cgf321x12a9sc") (f (quote (("interactive" "rustyline"))))))

