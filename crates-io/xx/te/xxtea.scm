(define-module (crates-io xx te xxtea) #:use-module (crates-io))

(define-public crate-xxtea-0.1.0 (c (n "xxtea") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1sri9qlzax971zdaa74m1vsc54rldbyixm1w7ymi6ddpbcpzi7ry")))

(define-public crate-xxtea-0.1.1 (c (n "xxtea") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ic99jj8wb84185ff3p6b2z67jk7cnr2mqbz7835jwbgjlgyy3hk")))

(define-public crate-xxtea-0.2.0 (c (n "xxtea") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "17628xpfmv99di5x4s928svmwkfk9yfyij8sv4h0bk07y2mma9p5")))

