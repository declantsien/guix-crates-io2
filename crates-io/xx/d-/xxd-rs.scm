(define-module (crates-io xx d- xxd-rs) #:use-module (crates-io))

(define-public crate-xxd-rs-0.2.0 (c (n "xxd-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "1zkshc87dbmgkpgbc6xkc8bvylyjf77jr8c60kxqlmshpirab6j6")))

(define-public crate-xxd-rs-0.2.2 (c (n "xxd-rs") (v "0.2.2") (d (list (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "1vznn4arbmac7497glwaph5w03xr664lqzwis94kf5rsq6iip06j")))

(define-public crate-xxd-rs-0.2.3 (c (n "xxd-rs") (v "0.2.3") (d (list (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "0m15jv8z5zrq3sliqsnnsggcd85c1v3hyla86r4qfkflcvm2ian0")))

(define-public crate-xxd-rs-0.2.4 (c (n "xxd-rs") (v "0.2.4") (d (list (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)))) (h "0a4inbz9jakqwpw1gk1p4cmcnnqarj41fsq9pg1wzmxs5h5hirr8")))

(define-public crate-xxd-rs-0.2.5 (c (n "xxd-rs") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)))) (h "0d1xk4qch0hvhjv1l3i8fqxm695a674knpzhsxvq2m2gd5npk9ni")))

(define-public crate-xxd-rs-0.2.7 (c (n "xxd-rs") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)))) (h "1kq3691a61vhmqpf9rkn08a0wsgil5r6qzi792d5m63cfj8k3c0y")))

(define-public crate-xxd-rs-0.3.0 (c (n "xxd-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 0)) (d (n "clap") (r "^2.16.2") (d #t) (k 1)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)))) (h "1jqjxrmxvxb05rb4npvxxsbx54iwwvq674qv6am6a4qcrk56vz4a")))

