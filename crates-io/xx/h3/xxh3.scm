(define-module (crates-io xx h3 xxh3) #:use-module (crates-io))

(define-public crate-xxh3-0.0.0 (c (n "xxh3") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.16") (f (quote ("extension-module" "abi3-py37"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1c1lnaq811mlq7lzgk6fjxx5np317qwlxrhbxrlx6d38nlcv56pc") (f (quote (("python" "pyo3")))) (r "1.59")))

(define-public crate-xxh3-0.1.0 (c (n "xxh3") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.16") (f (quote ("extension-module" "abi3-py37"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "15fvrypvnf8hhmglnl2yhvprmvmpml4n7qclzka8y4lqixya41p1") (f (quote (("python" "pyo3")))) (r "1.59")))

(define-public crate-xxh3-0.1.1 (c (n "xxh3") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.16") (f (quote ("extension-module" "abi3-py37"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "00gdb8g08gig4pfcvbq6mskimp8g69pdpyfvgsg7lclxli8iwaca") (f (quote (("python" "pyo3")))) (r "1.59")))

