(define-module (crates-io tl e5 tle5012) #:use-module (crates-io))

(define-public crate-tle5012-0.0.1 (c (n "tle5012") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0i169qpjhzimd80nriradz1dzn8wjj3ry2ad9skr9hl18d8c31lz")))

(define-public crate-tle5012-0.1.0 (c (n "tle5012") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.0") (d #t) (k 2)) (d (n "serialio") (r "^0.1.2") (d #t) (k 2)) (d (n "stm32f7") (r "^0.7.0") (f (quote ("stm32f7x7" "rt"))) (d #t) (k 2)) (d (n "stm32f7x7-hal") (r "^0.2.1") (d #t) (k 2)))) (h "0cc1jdzqxfsh5ygvk2hz87hdq5y13yrb7qjv74fs4f37z8ah66jw")))

