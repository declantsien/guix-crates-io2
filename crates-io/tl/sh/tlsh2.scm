(define-module (crates-io tl sh tlsh2) #:use-module (crates-io))

(define-public crate-tlsh2-0.1.0 (c (n "tlsh2") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)))) (h "18digg8ydq3m2gayc26fl847ldpwcvnzgjhfi1k9nm9rcq4pdfcn")))

(define-public crate-tlsh2-0.2.0 (c (n "tlsh2") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)))) (h "017mnfskndb91dj19bxdphmkgs9sh8mvy0z71kvjapg3wabxlzkx") (f (quote (("diff"))))))

(define-public crate-tlsh2-0.2.1 (c (n "tlsh2") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)))) (h "1dni4yqvcc5s6cv33hkmv0dgnxz68yvpm0n2nigwcp9b2lqh00s0") (f (quote (("diff"))))))

(define-public crate-tlsh2-0.3.0 (c (n "tlsh2") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 2)))) (h "1aq1sfqq4kjfir8m54zkcq2a6f7vsd63c8dc9i630k2nvykdcaa1") (f (quote (("diff"))))))

