(define-module (crates-io tl mc tlmcmddb-cli) #:use-module (crates-io))

(define-public crate-tlmcmddb-cli-0.1.0 (c (n "tlmcmddb-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tlmcmddb") (r "^0.1.0") (d #t) (k 0)) (d (n "tlmcmddb-csv") (r "^0.1.0") (d #t) (k 0)))) (h "11ldxp6b806ns3f96x1a4j2xp45ca8iqgbqz8bivga15ymsydgvj")))

(define-public crate-tlmcmddb-cli-0.2.0 (c (n "tlmcmddb-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tlmcmddb") (r "^0.2.0") (d #t) (k 0)) (d (n "tlmcmddb-csv") (r "^0.2.0") (d #t) (k 0)))) (h "0smzdjim62gsspxqxiqjplhzhh478phjyl6ps501d7nmjgnds4ii")))

(define-public crate-tlmcmddb-cli-2.5.0 (c (n "tlmcmddb-cli") (v "2.5.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tlmcmddb") (r "^0.2.0") (d #t) (k 0)) (d (n "tlmcmddb-csv") (r "^0.2.0") (d #t) (k 0)))) (h "1rr1pw2d3jgarn9nv9l2rjv3p228dn1km38f0b1d7w0qwi6r5hz0")))

(define-public crate-tlmcmddb-cli-2.5.1 (c (n "tlmcmddb-cli") (v "2.5.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tlmcmddb") (r "^2.5") (d #t) (k 0)) (d (n "tlmcmddb-csv") (r "^2.5") (d #t) (k 0)))) (h "0ygnnh3z95a9i38szm7lh19aa1ph3kqf2km2jjdwjcw7j9svnhdj")))

