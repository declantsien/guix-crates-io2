(define-module (crates-io tl mc tlmcmddb) #:use-module (crates-io))

(define-public crate-tlmcmddb-0.1.0 (c (n "tlmcmddb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rxg2mn1ni454955bbnhk4zqlinigj83kz1anj9lic6c3rjc1z5g")))

(define-public crate-tlmcmddb-0.2.0 (c (n "tlmcmddb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0clibfx9s16pz6ypddcn882bc5h8354mmi9s882if5mkihvnklra")))

(define-public crate-tlmcmddb-2.5.0 (c (n "tlmcmddb") (v "2.5.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nwnkh07n37kln5z4iwiss78h2hs866lh9pxf93y3s32qym23bnm")))

(define-public crate-tlmcmddb-2.5.1 (c (n "tlmcmddb") (v "2.5.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "125113k0vl7rqb586xvf9y0rg172rqbf3xl1p4n3snm1ksy56wss")))

