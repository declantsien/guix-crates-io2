(define-module (crates-io tl mc tlmcmddb-csv) #:use-module (crates-io))

(define-public crate-tlmcmddb-csv-0.1.0 (c (n "tlmcmddb-csv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tlmcmddb") (r "^0.1.0") (d #t) (k 0)))) (h "1k5iiavv9p4i5zqimz719x9snbgrmbf3kwb2lvwz0rdf9p6xij5s")))

(define-public crate-tlmcmddb-csv-0.2.0 (c (n "tlmcmddb-csv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tlmcmddb") (r "^0.2.0") (d #t) (k 0)))) (h "0bq32rz00477wgpyh9v6p5jngxwnjas1y6s2524pffliylh43jka")))

(define-public crate-tlmcmddb-csv-2.5.0 (c (n "tlmcmddb-csv") (v "2.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tlmcmddb") (r "^0.2.0") (d #t) (k 0)))) (h "0hg15n05rqi1xpy16fc8sxzb647zyd86glcgb95vr5vj69pc8rwg")))

(define-public crate-tlmcmddb-csv-2.5.1 (c (n "tlmcmddb-csv") (v "2.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tlmcmddb") (r "^2.5") (d #t) (k 0)))) (h "19amkzb9334fx55n5ykac1pmvngybwhrzvamf1cysa8d5bykjqhn")))

