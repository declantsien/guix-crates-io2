(define-module (crates-io tl is tlisp) #:use-module (crates-io))

(define-public crate-tlisp-0.0.1 (c (n "tlisp") (v "0.0.1") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "154zbmzz80a91y0pd722811zi37fw83zjd641cfcjc4jqrmbl5xx") (f (quote (("build-binary" "linefeed"))))))

(define-public crate-tlisp-0.0.2 (c (n "tlisp") (v "0.0.2") (d (list (d (n "dyn-fmt") (r "^0.4.0") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "02x9j2pqigsizwbl8fi9x3zmwl74aili79va5dx4fvm89g0d5bc4") (f (quote (("bin" "linefeed"))))))

