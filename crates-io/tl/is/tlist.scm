(define-module (crates-io tl is tlist) #:use-module (crates-io))

(define-public crate-tlist-0.5.0 (c (n "tlist") (v "0.5.0") (d (list (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1yqd6543mlw5hn9bk36b495j37izs64k11ci0p2zdng3hwvbva15")))

(define-public crate-tlist-0.5.1 (c (n "tlist") (v "0.5.1") (d (list (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "19rnw4w0pkxwgb63vd6cvycdswq8gwpfarcgfrzvi6x9rxj8gw3b")))

(define-public crate-tlist-0.6.0 (c (n "tlist") (v "0.6.0") (d (list (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1pqqmdf7kgp8ikzs9hs92k179ambhq3gmrls04lfxikz7v7wn0dn")))

(define-public crate-tlist-0.6.1 (c (n "tlist") (v "0.6.1") (d (list (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0281lnzablmjzdr7m457wb7j2aqv1x0mys0mw86q18as4bv85l6h")))

(define-public crate-tlist-0.6.2 (c (n "tlist") (v "0.6.2") (d (list (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1y99s20y1xnsv5yykkl1a9k7airw7vnxdvmx4fw80g9f1jzx01wd")))

(define-public crate-tlist-0.7.0 (c (n "tlist") (v "0.7.0") (d (list (d (n "typenum") (r "^1.16.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.16.0") (d #t) (k 2)))) (h "0kpbpma94ansfypvszs495kk7hwgslv9fgwm7l2hpxb39j5g7wi9") (f (quote (("doc" "typenum")))) (s 2) (e (quote (("typenum" "dep:typenum"))))))

