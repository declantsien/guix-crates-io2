(define-module (crates-io tl v4 tlv493d_a1b6_raspberry) #:use-module (crates-io))

(define-public crate-tlv493d_a1b6_raspberry-0.1.0 (c (n "tlv493d_a1b6_raspberry") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 0)))) (h "0has2rkp57a2xr1hczdlysvi93hm5jmpdc9q4q7aqlvdw0k8xjgs")))

(define-public crate-tlv493d_a1b6_raspberry-0.2.0 (c (n "tlv493d_a1b6_raspberry") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 0)))) (h "1bnzkr1ks1azwwv6k64a82zdnbfxbs8riz16djj4xnqxcc9k64si")))

(define-public crate-tlv493d_a1b6_raspberry-0.3.0 (c (n "tlv493d_a1b6_raspberry") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 0)))) (h "0l97ax60yisfp2zdpmp0wwxhvaqwlw1rxp18l9drh4ypqmmvym5c")))

(define-public crate-tlv493d_a1b6_raspberry-0.3.1 (c (n "tlv493d_a1b6_raspberry") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rppal") (r "^0.13.1") (f (quote ("hal"))) (d #t) (k 0)))) (h "01jir4n1rns4710qa41c4w6zwp0g697363q3scjqwnkmy0q4sra4")))

