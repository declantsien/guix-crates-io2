(define-module (crates-io tl re tlrepo) #:use-module (crates-io))

(define-public crate-tlrepo-0.1.0 (c (n "tlrepo") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.5") (k 0)) (d (n "thread_local") (r "^1.0.1") (d #t) (k 0)))) (h "14l8izkr3as6m6q1g0yi0rzxgr5azqikq0gskq5dlbx2k8rmrbaa")))

(define-public crate-tlrepo-0.1.1 (c (n "tlrepo") (v "0.1.1") (d (list (d (n "git2") (r "^0.13.5") (k 0)) (d (n "thread_local") (r "^1.0.1") (d #t) (k 0)))) (h "0mh4ryaw8dgwy4c2xvgjgzmpimik8070rf752p2a2bwcqk77xk10")))

(define-public crate-tlrepo-0.2.0 (c (n "tlrepo") (v "0.2.0") (d (list (d (n "git2") (r "^0.14") (k 0)) (d (n "thread_local") (r "^1.0.1") (d #t) (k 0)))) (h "1kfbwlia5qhrci4xgbsq1q5jp7nf5kpv2bpr0nh9kymzazcmnsir")))

(define-public crate-tlrepo-0.3.0 (c (n "tlrepo") (v "0.3.0") (d (list (d (n "git2") (r "^0.15") (k 0)) (d (n "thread_local") (r "^1.1.4") (d #t) (k 0)))) (h "0dxh4wyks0x89szkz0nlb1fjm837dc10xc8ahp1ajbr40xmaail2")))

(define-public crate-tlrepo-0.4.0 (c (n "tlrepo") (v "0.4.0") (d (list (d (n "git2") (r "^0.16.1") (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "0jw0pi6wpxzx89xqhmfml95kv8yxjvgdym9vv5vbz5dl8sfd03jh")))

(define-public crate-tlrepo-0.5.0 (c (n "tlrepo") (v "0.5.0") (d (list (d (n "git2") (r "^0.17.1") (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1cwyh8p2hl91vr3xifaxv56k6ls7smmfz46kfvxswk4pcq7kj13x")))

(define-public crate-tlrepo-0.6.0 (c (n "tlrepo") (v "0.6.0") (d (list (d (n "git2") (r "^0.18.1") (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "1shn5557j8wliz5i85zf16cgr27drplzzni7g2niqx6k8nc1ka9z")))

