(define-module (crates-io tl -p tl-proto-proc) #:use-module (crates-io))

(define-public crate-tl-proto-proc-0.1.0 (c (n "tl-proto-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0d59mgzpqsg4g8arm1h5fgzngmb0nypcabk6lpx5g4i503zj19kv")))

(define-public crate-tl-proto-proc-0.1.1 (c (n "tl-proto-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "068wzlzqznfv4103na76j04dnc7ws0wm3yhzzk55chqij9c9hs9i")))

(define-public crate-tl-proto-proc-0.1.2 (c (n "tl-proto-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0yirllfncxi7fsnvn36d7afzzda09bhhdnjl40d7vv6ayn0nlb6x")))

(define-public crate-tl-proto-proc-0.1.3 (c (n "tl-proto-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1l736dlc2v22pg7f620lfa76rzfhcs6y70nhnd7896nsgbipc351")))

(define-public crate-tl-proto-proc-0.1.5 (c (n "tl-proto-proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "17wajvzv8lglb9k17bbswra03444x92zyls9q2q6zcll2mx6q7c7")))

(define-public crate-tl-proto-proc-0.1.6 (c (n "tl-proto-proc") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1nv281g6s89acnkzrp7w3kwdaj0f0ldgm7rcrg1k8kvvixwqqa41")))

(define-public crate-tl-proto-proc-0.1.7 (c (n "tl-proto-proc") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1y0wa1mgi5367drxkyyhc90rwmn5r6by7na2pzifxjqaw5ykis9s")))

(define-public crate-tl-proto-proc-0.1.8 (c (n "tl-proto-proc") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0zdxw3xcn2b7jaxywrc4wqnif0jgfkiq36fxynnxry2nb7acskv5")))

(define-public crate-tl-proto-proc-0.1.9 (c (n "tl-proto-proc") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1jvw97y2w6bpajpcxawfcik9dr1c1zixcdcaima41mjbm72shggf")))

(define-public crate-tl-proto-proc-0.1.10 (c (n "tl-proto-proc") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0x0v067a7j5cm6p8qi86i69gnx4zcnq6znz3g5b7jh76jaykicpw")))

(define-public crate-tl-proto-proc-0.1.11 (c (n "tl-proto-proc") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1jvlqqdjbjsy3y6zahig6i263gv2ld3kq0kgkl6vjz07cb63cl2h")))

(define-public crate-tl-proto-proc-0.2.0 (c (n "tl-proto-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "118f3g06y0bj8jppm6s1a0gsj2pasg3nns0kxm6bx36lhznfi66x")))

(define-public crate-tl-proto-proc-0.3.0 (c (n "tl-proto-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "047byjmg5cm5wa1kiz73anxlfwqn9ynzawhw1q96gskyl5k1kbrl")))

(define-public crate-tl-proto-proc-0.3.3 (c (n "tl-proto-proc") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1jrh69dz7mgplzn6dkjvxpk3yhzj6zhf9d511jh9sg2xbvdfbzrw")))

(define-public crate-tl-proto-proc-0.3.5 (c (n "tl-proto-proc") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.1") (d #t) (k 0)))) (h "03xb3rzhm4byl1949n5ql0pm8s9zi0f483mh8445g57y38j8scq1")))

(define-public crate-tl-proto-proc-0.3.6 (c (n "tl-proto-proc") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.1.2") (d #t) (k 0)))) (h "06v0rfkn5sb7scypy425bvzwpyhxv3ddp7syhj95f256r8l10gwl")))

(define-public crate-tl-proto-proc-0.3.7 (c (n "tl-proto-proc") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.1.2") (d #t) (k 0)))) (h "1229snmxrvwbc0g1890x4ixn7mli3y01lbjhwwggglrx8nlc9pq2")))

(define-public crate-tl-proto-proc-0.3.8 (c (n "tl-proto-proc") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.1.2") (d #t) (k 0)))) (h "06a2fb8048vlcgywqc35pj4db7vp4b9jnhv0k27wrd0sb4572x44")))

(define-public crate-tl-proto-proc-0.3.9 (c (n "tl-proto-proc") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.1.2") (d #t) (k 0)))) (h "0bpgl45x21x15261n51nzd3iz8cijj66x9jivrzq1vc282p6mykd")))

(define-public crate-tl-proto-proc-0.3.11 (c (n "tl-proto-proc") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "0cz1biqdd4ylm0ia7gv8ngk0qvaq5ndax3m723n4x0rb5irz081c")))

(define-public crate-tl-proto-proc-0.3.14 (c (n "tl-proto-proc") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "0n7lszyvq2hw7sdcfpbbq0j8b5s2zd8jhcr91iddc8zhs1p8hsqp")))

(define-public crate-tl-proto-proc-0.4.0 (c (n "tl-proto-proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "12daizibwwsrjrlicyzgb13pr8pbj16l092yz3zfyb1wzmzgz947")))

(define-public crate-tl-proto-proc-0.4.1 (c (n "tl-proto-proc") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "00mz3i2fp3n76hjn5dai6mf475g57p3z5vqdla0ajrv6hjvdzg2s")))

(define-public crate-tl-proto-proc-0.4.3 (c (n "tl-proto-proc") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "1b107h9vd1wkjmmi242mzq0qv4yy6z0r1dy2hyqih1jyhvj1vxdk")))

(define-public crate-tl-proto-proc-0.4.6 (c (n "tl-proto-proc") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "tl-scheme") (r "^0.2.0") (d #t) (k 0)))) (h "1yx001nix6db3vcb7xnibwy5rlqmm5w31pb40nj39km349laygjs")))

