(define-module (crates-io tl s_ tls_3d_to_2d) #:use-module (crates-io))

(define-public crate-tls_3d_to_2d-0.1.0 (c (n "tls_3d_to_2d") (v "0.1.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "tls_read_hancock_bin") (r "^0.1.1") (d #t) (k 0)))) (h "03azwirr4zszail6ipwqn6vpcwj6ij2qrgva8b28kcsfv1nimqll")))

(define-public crate-tls_3d_to_2d-0.1.1 (c (n "tls_3d_to_2d") (v "0.1.1") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "tls_read_hancock_bin") (r "^0.1.1") (d #t) (k 0)))) (h "10kx2df9kv9ag1im8fhzfkkhdshszgf2p8pmcqg0mvz3g8gn9l2y")))

(define-public crate-tls_3d_to_2d-0.2.0 (c (n "tls_3d_to_2d") (v "0.2.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "tls_read_hancock_bin") (r "^0.1.1") (d #t) (k 0)))) (h "03mgq5c2ly4nhbdhd2d2i55cx5b9gdhsz7g6mxf6z4qfn5y2483h")))

(define-public crate-tls_3d_to_2d-0.3.0 (c (n "tls_3d_to_2d") (v "0.3.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "tls_read_hancock_bin") (r "^0.1.1") (d #t) (k 0)))) (h "015cv92jkd97syd76xnis8pxkbjz763l240cai5yphlyh5fyn49x")))

