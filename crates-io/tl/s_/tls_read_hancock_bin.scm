(define-module (crates-io tl s_ tls_read_hancock_bin) #:use-module (crates-io))

(define-public crate-tls_read_hancock_bin-0.1.0 (c (n "tls_read_hancock_bin") (v "0.1.0") (h "147mc53xw3fn1d5cizywdw3lwn4mh54v0k5sykqwmqpnhcc6hanh")))

(define-public crate-tls_read_hancock_bin-0.1.1 (c (n "tls_read_hancock_bin") (v "0.1.1") (h "0qyxsf25psc6yaqjdlvc6kj31z4bds3ckkh1pw8ssa62zsrpjy8h")))

