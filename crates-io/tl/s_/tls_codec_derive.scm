(define-module (crates-io tl s_ tls_codec_derive) #:use-module (crates-io))

(define-public crate-tls_codec_derive-0.1.0 (c (n "tls_codec_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03fyf59m6wvy6nxdr127rbfkwsn34sdqrlfzd8a3ppwm70m58vfx") (y #t)))

(define-public crate-tls_codec_derive-0.1.1 (c (n "tls_codec_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0nlv9f9zhsca3r2bicydfbdqy5a02ncslqawpf0k9vpzl46q5wsf")))

(define-public crate-tls_codec_derive-0.1.2 (c (n "tls_codec_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0na0b3qs0l8lzaw427v4ygaia2gm8qsq78skzhkn7fv0qb9bim3d")))

(define-public crate-tls_codec_derive-0.2.0-pre.1 (c (n "tls_codec_derive") (v "0.2.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03pyb9pss04fdblgn2h0m1ipws08hl1cqjhsbsjyzhlymn112qqg") (r "1.56")))

(define-public crate-tls_codec_derive-0.2.0-pre.2 (c (n "tls_codec_derive") (v "0.2.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "079anylybhpbn75nhf2ha12yrwshayra6vq17q8n7h0flm3shcyj") (r "1.56")))

(define-public crate-tls_codec_derive-0.2.0-pre.3 (c (n "tls_codec_derive") (v "0.2.0-pre.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0a1dlpa63vpny4g3d1pxnxmg984iy869jh6lxs5g44x0w637swbm") (r "1.56")))

(define-public crate-tls_codec_derive-0.2.0-pre.4 (c (n "tls_codec_derive") (v "0.2.0-pre.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0xl4ai3ld53mblsj729wj4y7l6b8iwfaj2jqfdsyxm1m8kdjbyqf") (r "1.56")))

(define-public crate-tls_codec_derive-0.2.0 (c (n "tls_codec_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "119a9p64psxgs3mx2p00x2096r7xymwfdgbin0sibv7ghsf731x7") (r "1.56")))

(define-public crate-tls_codec_derive-0.3.0-pre.1 (c (n "tls_codec_derive") (v "0.3.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "112yvbyzd7czxihs2ywmvmw21kgjp67ypk3wxgqjhyv85vllz5ri") (r "1.60")))

(define-public crate-tls_codec_derive-0.3.0-pre.2 (c (n "tls_codec_derive") (v "0.3.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0yyzzyr7vijkd154sfza0vda37ya9gfyi9d0yxbhhvfyr256kfmm") (r "1.60")))

(define-public crate-tls_codec_derive-0.3.0-pre.4 (c (n "tls_codec_derive") (v "0.3.0-pre.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1nv500skbrvvc5adwg5vkn16id1mixgwgjsk2fxamfa54lzpz2mq") (r "1.60")))

(define-public crate-tls_codec_derive-0.3.0 (c (n "tls_codec_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09df9m2qgrxarrpbd5vx80f593m66hk2k06cwzksn2hji02489ij") (r "1.60")))

(define-public crate-tls_codec_derive-0.4.0-pre.1 (c (n "tls_codec_derive") (v "0.4.0-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "11qdk9baamdhk72191rwvy1b8r20hw7mm39gx0pgaclalwyb9qrd") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.0-pre.2 (c (n "tls_codec_derive") (v "0.4.0-pre.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0k1mijpqzqxdxc7a65wc4j70hs7h38siqgb507i6xba937l8g563") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.0-pre.3 (c (n "tls_codec_derive") (v "0.4.0-pre.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0kavcfk7b2lqyvj5k1kjvmyrj8ryxgijwl26dxm84wl1481w1ihf") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.0-pre.4 (c (n "tls_codec_derive") (v "0.4.0-pre.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vb2k37dalnh122dzgcsj72dlhfradn173bgv1clis7ks7gqmd6h") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.0 (c (n "tls_codec_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05cf5ww401mdl3hwzn01z39zp1w4kka2xrschg4g3q2lg8z0xq6q") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.1-pre.1 (c (n "tls_codec_derive") (v "0.4.1-pre.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kl35ym6z82fifll9m8iyx0jf10rzzvm3nwy9ycbwga48cr1vsds") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

(define-public crate-tls_codec_derive-0.4.1 (c (n "tls_codec_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1704w8zpgpj40yjgq9dddnnfzmq44p63n0606c1g6y8fcm2zb7ld") (f (quote (("std") ("default" "std") ("conditional_deserialization" "syn/full")))) (r "1.60")))

