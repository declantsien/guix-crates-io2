(define-module (crates-io tl s_ tls_wrap_common) #:use-module (crates-io))

(define-public crate-tls_wrap_common-0.1.0 (c (n "tls_wrap_common") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0mmy2189ihbi39b1n64rdsjpmr8ppk6j7cqxzdssqmnjd11n806k") (r "1.60.0")))

