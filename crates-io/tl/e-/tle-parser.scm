(define-module (crates-io tl e- tle-parser) #:use-module (crates-io))

(define-public crate-tle-parser-0.1.0 (c (n "tle-parser") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "0qa8ml0q0a0jrn9jxxr6f8sd49jhwwac11sxfi432glygk7gih6k")))

(define-public crate-tle-parser-0.1.1 (c (n "tle-parser") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "1sjpqk78c16zznbrif7ghc84gdp0xva4idrj9s5liw7ydm367l6a")))

(define-public crate-tle-parser-0.1.2 (c (n "tle-parser") (v "0.1.2") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "0agb7d32f9rcpyd1gh5jf8fijqfqkz7g906h1663i9v9mgqz8rz8")))

(define-public crate-tle-parser-0.1.3 (c (n "tle-parser") (v "0.1.3") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pp8788080rzfncc9p0npvrfwphx8ikczvmywiqxhmdc86cjiqw4")))

