(define-module (crates-io tl in tline) #:use-module (crates-io))

(define-public crate-tline-0.1.0 (c (n "tline") (v "0.1.0") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "hdf5-sys") (r "^0.8") (f (quote ("static"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "physical_constants") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00gvnp6q2l68p49rrimsx1hwshckisfya6vd3aq0h7k322ffx1pw")))

