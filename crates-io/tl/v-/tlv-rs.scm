(define-module (crates-io tl v- tlv-rs) #:use-module (crates-io))

(define-public crate-tlv-rs-0.1.0 (c (n "tlv-rs") (v "0.1.0") (d (list (d (n "bin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0a0hl1hnzxch6p36slc5lxp0cb7wdh2qgcz9949d6syxbglw3ybj") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug")))) (y #t)))

(define-public crate-tlv-rs-0.1.1 (c (n "tlv-rs") (v "0.1.1") (d (list (d (n "bin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "000x3ndryi28qgd4ca63p39m77wkqkzhd1la1nvszp7l6dncy8yi") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug"))))))

(define-public crate-tlv-rs-0.1.2 (c (n "tlv-rs") (v "0.1.2") (d (list (d (n "bin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0kiv5pip3ws5r9i7nclsnvwlw4kcnliixxjajbc01rvx0r507m80") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug"))))))

(define-public crate-tlv-rs-0.2.0 (c (n "tlv-rs") (v "0.2.0") (d (list (d (n "bin-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "try_take") (r "^0.1.0") (d #t) (k 0)))) (h "0mprlvv88gih4d2jkdnrlg46sr8g56dgyvz8wjq1xkbs268az3gl") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug")))) (y #t)))

(define-public crate-tlv-rs-0.2.1 (c (n "tlv-rs") (v "0.2.1") (d (list (d (n "bin-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "try_take") (r "^0.1.0") (d #t) (k 0)))) (h "1w0wc4zvdgjjzs135hsdp9s2jkkvd1v48ydafj9lpx78qsympawi") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug"))))))

(define-public crate-tlv-rs-0.1.3 (c (n "tlv-rs") (v "0.1.3") (d (list (d (n "bin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0jzps8i8d95jhqs8kbw2x1fh96lmhy1mq4m7vkr3bff7gyv5xf9d") (f (quote (("write") ("read") ("default" "read" "write" "debug") ("debug"))))))

(define-public crate-tlv-rs-0.2.2 (c (n "tlv-rs") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "macro-bits") (r "^0.1.4") (d #t) (k 2)) (d (n "no-panic") (r "^0.1.26") (d #t) (k 0)) (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (k 0)))) (h "12vaw26y90m5rifv4bzk5qa95qhw16fk8hs92mlk1hm7cjba7r4h") (f (quote (("no_panic") ("alloc"))))))

(define-public crate-tlv-rs-0.2.3 (c (n "tlv-rs") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "macro-bits") (r "^0.1.4") (d #t) (k 2)) (d (n "no-panic") (r "^0.1.26") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (k 0)))) (h "0iwfbpgnl2ww5ikf1d58bnmd8pnwalbh46bha0s7q9c6s9lyyjqn") (f (quote (("no_panic") ("alloc"))))))

(define-public crate-tlv-rs-0.2.4 (c (n "tlv-rs") (v "0.2.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)) (d (n "macro-bits") (r "^0.1.4") (d #t) (k 2)) (d (n "no-panic") (r "^0.1.26") (d #t) (k 0)) (d (n "scroll") (r "^0.12.0") (k 0)))) (h "1l2fsr44fv6knhb9sk36cqdw882sns3r3m60p1564qvcn7fjqr8i") (f (quote (("no_panic") ("alloc"))))))

