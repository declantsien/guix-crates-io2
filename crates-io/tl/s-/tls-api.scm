(define-module (crates-io tl s- tls-api) #:use-module (crates-io))

(define-public crate-tls-api-0.1.1 (c (n "tls-api") (v "0.1.1") (h "0bzb1b0lcmw0a4f7ixqxsmpr7i6lisnrh8mx6ywj41jhd5m30gs3")))

(define-public crate-tls-api-0.1.2 (c (n "tls-api") (v "0.1.2") (h "0qgiwn8hh1crfaa0k63wmkc7q177n01l1yf2n9k4nbcfnpn7nk84")))

(define-public crate-tls-api-0.1.3 (c (n "tls-api") (v "0.1.3") (h "07fs3pjp94dvscg8b2f7h5aya9hc4assqg6b8mykwvm0d3ar7645")))

(define-public crate-tls-api-0.1.4 (c (n "tls-api") (v "0.1.4") (h "1cyxr23rymrhvx665630fqsirvn228jyd3jmwwl40r3rji761zal")))

(define-public crate-tls-api-0.1.5 (c (n "tls-api") (v "0.1.5") (h "02mcwm9dykckpy9fbsy185c2x1izrmicjv7c9vm4gxxk40gyiwbh")))

(define-public crate-tls-api-0.1.6 (c (n "tls-api") (v "0.1.6") (h "0myl65w8v9vqrjcwa161hjjdn68n4fz487cc31bhdq8cfqphd8df")))

(define-public crate-tls-api-0.1.7 (c (n "tls-api") (v "0.1.7") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "1k6mlsd1mwyx4dg2qczbfwml0nzch8za8cs64b7203l5slzkkrfs")))

(define-public crate-tls-api-0.1.8 (c (n "tls-api") (v "0.1.8") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0rmr9bdbfzdshwa43hnan9qcppdg13igm682if8fqyq4mqgh1zl0")))

(define-public crate-tls-api-0.1.9 (c (n "tls-api") (v "0.1.9") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0j26kavip5f24h97gg2qxk0gxjjww966apwah6hckbjd9hphhsbk")))

(define-public crate-tls-api-0.1.10 (c (n "tls-api") (v "0.1.10") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0g61y2504zrdw5zbsr5yc478n78f5riyz6krirsrfv92wjnxb9vx")))

(define-public crate-tls-api-0.1.11 (c (n "tls-api") (v "0.1.11") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "10kvvsznswcqh8qiz055kmms494d2d5djb26i0dgad1zdcbnsily")))

(define-public crate-tls-api-0.1.12 (c (n "tls-api") (v "0.1.12") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0n5vi8mkv99rnzwc1r9c58c4jkplm3sqz9pls68s3cs8a1jfb51w")))

(define-public crate-tls-api-0.1.13 (c (n "tls-api") (v "0.1.13") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "0wrqnd5lnfm5b0wjsj7wpxrimqrkmnaqvak5mq4qp21shr0cq80d")))

(define-public crate-tls-api-0.1.14 (c (n "tls-api") (v "0.1.14") (d (list (d (n "log") (r "0.*") (d #t) (k 0)))) (h "067c3rywz39k1414ljgf8hyyc64r25gj5j2cv3m1qc157pwllhm2")))

(define-public crate-tls-api-0.1.15 (c (n "tls-api") (v "0.1.15") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cd38yakcfr2vrk34s3nkbkn41gdqhlb01r53b382xxkwfhpj2hc")))

(define-public crate-tls-api-0.1.16 (c (n "tls-api") (v "0.1.16") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1n2nj7pcwn6bxbxk5pvdqlnr2zabkavr6i96cz5wp7sbqpxx7sgk")))

(define-public crate-tls-api-0.1.17 (c (n "tls-api") (v "0.1.17") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17r3bggyfwzsgav522r0kyd66sql7n00nrgvcmsbhb1fg4mdjp35")))

(define-public crate-tls-api-0.1.18 (c (n "tls-api") (v "0.1.18") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "028xx0vk2yyckyy7jyziv4m901mrfgxybygqshaxwi7pk2lp588s")))

(define-public crate-tls-api-0.1.19 (c (n "tls-api") (v "0.1.19") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jg8fc1j9v1qhlwhjjil1f1gj86fs34facj986n68c18vs5ywcxl")))

(define-public crate-tls-api-0.1.20 (c (n "tls-api") (v "0.1.20") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10livf2rg4qh0cb0fyw8rhpdy721qx2g59dkmlahg8a0vwpzwlp4")))

(define-public crate-tls-api-0.1.21 (c (n "tls-api") (v "0.1.21") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kbk03421vm6w13z20xgp12r3gq8ab03rdf7zcnpxxvrpsvjsfpq")))

(define-public crate-tls-api-0.1.22 (c (n "tls-api") (v "0.1.22") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l8kxdzz2jbdixkc686fg0dkbx27g5bqgm7vawiii585g9w07704")))

(define-public crate-tls-api-0.2.0 (c (n "tls-api") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1a3rcrgfyhdsavwc4x7wiwhinr3ghjxfn3fy9iab7xj2zfpsy3qj")))

(define-public crate-tls-api-0.2.1 (c (n "tls-api") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1md4gs1g6r92gvi8h65yppsvdzm9wi1snc0zxsn2y400h43cc59y")))

(define-public crate-tls-api-0.3.0 (c (n "tls-api") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.0") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "0vl5jxbyx92wncxryvx1hv4qj4vfj7r25sa24adp7f7adms1kkda")))

(define-public crate-tls-api-0.3.1 (c (n "tls-api") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "1iyfzkiz2f85jc0zzf56gcqdbqhm3k4rh2jf6rpxhas7diymmwsc")))

(define-public crate-tls-api-0.3.2 (c (n "tls-api") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "13fdx2hgrnx07gwnjjh5ryxjjihrf6ybb4s6xylcd6rjr392j7f1")))

(define-public crate-tls-api-0.4.0 (c (n "tls-api") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "18wqzqn8908pk7gy23ynd7z5ihzb9pm8mw7w95rhi937q43l3fsf")))

(define-public crate-tls-api-0.5.0 (c (n "tls-api") (v "0.5.0") (d (list (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)))) (h "1b357v8gvmczkhadww9gzy36sd4kjbj58fb1c2anbld1ba740mic") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-0.6.0 (c (n "tls-api") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)))) (h "0r6525lsgiy24pqs6l9n21ipgqly0f5rh9k2a2xc7z8am4cmq0f0") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-0.7.0 (c (n "tls-api") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "1q3xxqva0mziamv9xfjzrgrllvkr9lf7zwclky7skm66vmsfvpdp") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-0.8.0 (c (n "tls-api") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "03xxlnywj1y7811jdg1xsrs0f1gcc4k8yf7a6lrsasjc054ypb8d") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-0.9.0 (c (n "tls-api") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)) (d (n "webpki") (r "^0.22.0") (d #t) (k 0)))) (h "18sk9xgd17zwvhn2np5052crgnb3411z5j9hl7la63d6n3gv7lb6") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

