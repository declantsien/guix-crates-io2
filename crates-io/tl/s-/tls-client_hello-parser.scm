(define-module (crates-io tl s- tls-client_hello-parser) #:use-module (crates-io))

(define-public crate-tls-client_hello-parser-0.1.0 (c (n "tls-client_hello-parser") (v "0.1.0") (d (list (d (n "rustls") (r "^0.18") (k 0)))) (h "1cbgj74jdwbssj565qkcjpiwzsycv9ywwaz4q9jbf3b2s3fi7vr3")))

(define-public crate-tls-client_hello-parser-0.2.0 (c (n "tls-client_hello-parser") (v "0.2.0") (d (list (d (n "rustls") (r "^0.20") (k 0)) (d (n "webpki") (r "^0.22") (f (quote ("alloc"))) (k 0)))) (h "0a775jnz22xlhf7l9yj36nswwylqgpw86kh45y9f48r43gnz427i")))

(define-public crate-tls-client_hello-parser-0.2.1 (c (n "tls-client_hello-parser") (v "0.2.1") (d (list (d (n "rustls") (r "^0.20") (k 0)) (d (n "webpki") (r "^0.22") (f (quote ("alloc"))) (k 0)))) (h "0g88qbf4lmdz9wlzhnf4k8vk45fsrx46m31bdwg12mfnrzz2w0iy")))

