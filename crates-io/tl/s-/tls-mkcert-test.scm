(define-module (crates-io tl s- tls-mkcert-test) #:use-module (crates-io))

(define-public crate-tls-mkcert-test-0.1.0 (c (n "tls-mkcert-test") (v "0.1.0") (d (list (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.22") (d #t) (k 0)))) (h "1njnvmmycwr710cp65ylf0fd4biawpkl4qg65k7h25ssrqrbcba7")))

