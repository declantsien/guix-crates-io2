(define-module (crates-io tl s- tls-tunnel) #:use-module (crates-io))

(define-public crate-tls-tunnel-0.1.0 (c (n "tls-tunnel") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22.0") (d #t) (k 0)) (d (n "webpki") (r "^0.21.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.0") (d #t) (k 0)))) (h "1j1g8s8w01d21sqlj6v2llqkbsdn2qnkfr5v9czixm3fjiig1pfj")))

(define-public crate-tls-tunnel-0.1.1 (c (n "tls-tunnel") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webpki") (r "^0.21.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.0") (d #t) (k 0)))) (h "1n3kjz3j8k51z3pvca8x10i38j0jm3c6mki42gl608f2m80wz2pa")))

(define-public crate-tls-tunnel-0.1.2 (c (n "tls-tunnel") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webpki") (r "^0.21.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.0") (d #t) (k 0)))) (h "1w0k7lm87zrdbrbcc107sdhp30hp2251y2va8ivfiyhrhqxai0i5")))

(define-public crate-tls-tunnel-0.1.3 (c (n "tls-tunnel") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("rt-multi-thread" "macros" "io-util" "net"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "webpki") (r "^0.21.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21.0") (d #t) (k 0)))) (h "0l4n1bl3mw7hc4i8yx20b968jdjg0qvir6b0qm7ddd4mlbhnw48z")))

