(define-module (crates-io tl s- tls-api-2) #:use-module (crates-io))

(define-public crate-tls-api-2-0.11.0 (c (n "tls-api-2") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "pem") (r "^3.0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("io-util" "net"))) (o #t) (d #t) (k 0)))) (h "1ixbl8n88wxmvqlm5skj3wg4k8fh74gzg8jvr6ransr5581bnji9") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util") ("default" "runtime-tokio"))))))

