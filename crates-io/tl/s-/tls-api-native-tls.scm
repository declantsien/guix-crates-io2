(define-module (crates-io tl s- tls-api-native-tls) #:use-module (crates-io))

(define-public crate-tls-api-native-tls-0.1.1 (c (n "tls-api-native-tls") (v "0.1.1") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.1") (d #t) (k 0)))) (h "1f2csqfn8cjwy4y7higq6p3fk97jp5jmfl7fr07kknqxcp37q8nj")))

(define-public crate-tls-api-native-tls-0.1.2 (c (n "tls-api-native-tls") (v "0.1.2") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.2") (d #t) (k 0)))) (h "0dhinqv7raqaxjwm7a1wln1iadc44anm7m711g2bnf3ilprrq9cs")))

(define-public crate-tls-api-native-tls-0.1.3 (c (n "tls-api-native-tls") (v "0.1.3") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.3") (d #t) (k 0)))) (h "1b5idgfaq1i4x87r8lwwkg489cq5srhl51w0yfiygvj5f9yybpa3")))

(define-public crate-tls-api-native-tls-0.1.4 (c (n "tls-api-native-tls") (v "0.1.4") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.4") (d #t) (k 0)))) (h "0n429wkc7pzlf12886is25zwkym2mias51qy1b2v8k5mcj0hr1w9")))

(define-public crate-tls-api-native-tls-0.1.5 (c (n "tls-api-native-tls") (v "0.1.5") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.5") (d #t) (k 0)))) (h "0k32dlhmfwnwk38r2g3fp6z85cpqwl2cc9iqy1vb5v3i2603app2")))

(define-public crate-tls-api-native-tls-0.1.6 (c (n "tls-api-native-tls") (v "0.1.6") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.6") (d #t) (k 0)))) (h "0bgb4zs5dsavsf991phw003xgig107zjrnx0mk9xl35wcl8h077i")))

(define-public crate-tls-api-native-tls-0.1.7 (c (n "tls-api-native-tls") (v "0.1.7") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.7") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.7") (d #t) (k 2)))) (h "1pxqyfip367mmrwx3fk9272kfs619wpk43x77i9xnl712anw47d2")))

(define-public crate-tls-api-native-tls-0.1.8 (c (n "tls-api-native-tls") (v "0.1.8") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.8") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.8") (d #t) (k 2)))) (h "0cj5iccn8kmbnlnjspxbv9y3hqk6bmp5yhv2l6a5n1d8lwynxlz6")))

(define-public crate-tls-api-native-tls-0.1.9 (c (n "tls-api-native-tls") (v "0.1.9") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.9") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.9") (d #t) (k 2)))) (h "16pwxablicb75phyis8svqpn64z2gnbx002d3y9fsyk7lb56cz78")))

(define-public crate-tls-api-native-tls-0.1.10 (c (n "tls-api-native-tls") (v "0.1.10") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.10") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.10") (d #t) (k 2)))) (h "1l15vpgsb3b5mrc4gzjzc5b047ncwcq5mmbizz5i0ab7fzkp8lsd")))

(define-public crate-tls-api-native-tls-0.1.11 (c (n "tls-api-native-tls") (v "0.1.11") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.11") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.11") (d #t) (k 2)))) (h "1vbpzl1kq2r1nx8zdsxbg9vfck6kri2dakb6ymjwziqvaj7fwjkp")))

(define-public crate-tls-api-native-tls-0.1.12 (c (n "tls-api-native-tls") (v "0.1.12") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.12") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.12") (d #t) (k 2)))) (h "0h7n0mr7wys9xdff4d1xmxk1j0b68h0my4vd4in05pmalxqcjxsa")))

(define-public crate-tls-api-native-tls-0.1.13 (c (n "tls-api-native-tls") (v "0.1.13") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.13") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.13") (d #t) (k 2)))) (h "10rn4xdf5ri4h4smqnl3c76nz619qf1ysg94zk8n1q9bmixsqzd1")))

(define-public crate-tls-api-native-tls-0.1.14 (c (n "tls-api-native-tls") (v "0.1.14") (d (list (d (n "native-tls") (r "0.*") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.14") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.14") (d #t) (k 2)))) (h "0lanh3xv4klys9j5iai3myqaz0jznclizcrmnjml3mvapi2xfcrx")))

(define-public crate-tls-api-native-tls-0.1.19 (c (n "tls-api-native-tls") (v "0.1.19") (d (list (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "tls-api") (r "^0.1.19") (d #t) (k 0)) (d (n "tls-api-test") (r "^0.1.19") (d #t) (k 2)))) (h "0zdc2fd3mzdzq7h0f9gligsf9fnvlr57qpr2wj3y1l6b4ws7vnkf")))

(define-public crate-tls-api-native-tls-0.1.20 (c (n "tls-api-native-tls") (v "0.1.20") (d (list (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "tls-api") (r "= 0.1.20") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.1.20") (d #t) (k 2)))) (h "1wzfzzbpc6ss7bjns5lbrw1r92f8jhhngz2xkav6kh0m51czyiw9")))

(define-public crate-tls-api-native-tls-0.1.21 (c (n "tls-api-native-tls") (v "0.1.21") (d (list (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "tls-api") (r "= 0.1.21") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.1.21") (d #t) (k 2)))) (h "11kcivx22nf7xk257n8m0c4w0nw3ars1j6ldwcgk2vvpmkrpqgf8")))

(define-public crate-tls-api-native-tls-0.1.22 (c (n "tls-api-native-tls") (v "0.1.22") (d (list (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "tls-api") (r "= 0.1.22") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.1.22") (d #t) (k 2)))) (h "0qz7bhh8pjv1jrbnzajy1rd08ns21aaxz192as9j2kyav9ffiggm")))

(define-public crate-tls-api-native-tls-0.2.0 (c (n "tls-api-native-tls") (v "0.2.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.2.0") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.2.0") (d #t) (k 2)))) (h "1dlgq5lwgjxiy4pgbby31pnyfmk7z9yk4xgjwksj16466gm8k54h")))

(define-public crate-tls-api-native-tls-0.2.1 (c (n "tls-api-native-tls") (v "0.2.1") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.2.1") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.2.1") (d #t) (k 2)))) (h "1s5kib4qh5cn8s5bnrl2rzr5547c40072ph0f0qbm514d6az3ji1")))

(define-public crate-tls-api-native-tls-0.3.0 (c (n "tls-api-native-tls") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.0") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.3.0") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.3.0") (d #t) (k 2)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "01hnjrpyahdvkspbkgs9w0ibybnfmjnjgwxw6z3am30qqas1wqjq")))

(define-public crate-tls-api-native-tls-0.3.1 (c (n "tls-api-native-tls") (v "0.3.1") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.3.1") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.3.1") (d #t) (k 2)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "0s33dmhxqckwhsryrbh35ah5kndppzxf9b0bc0n9p3syzzhizpw4")))

(define-public crate-tls-api-native-tls-0.3.2 (c (n "tls-api-native-tls") (v "0.3.2") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.3.2") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.3.2") (d #t) (k 2)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "1ijli04wmvnf6jjh0jsp7840px861c9iynvr1l8xlv5sdsyazvj0")))

(define-public crate-tls-api-native-tls-0.4.0 (c (n "tls-api-native-tls") (v "0.4.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tls-api") (r "= 0.4.0") (d #t) (k 0)) (d (n "tls-api-test") (r "= 0.4.0") (d #t) (k 2)) (d (n "tokio") (r "~0.2.6") (d #t) (k 0)))) (h "1qbwa6l0hp6iix1zvm54rj3fwhksdqfqhjq6rw05d5zdngvq0pnb")))

(define-public crate-tls-api-native-tls-0.5.0 (c (n "tls-api-native-tls") (v "0.5.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("alpn"))) (d #t) (k 0)) (d (n "test-cert-gen") (r "=0.5.0") (k 2)) (d (n "tls-api") (r "=0.5.0") (k 0)) (d (n "tls-api-test") (r "=0.5.0") (k 0)) (d (n "tls-api-test") (r "=0.5.0") (k 1)) (d (n "tls-api-test") (r "=0.5.0") (k 2)) (d (n "tokio") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0p5qhacs62r44qnp9i6wpmbrgahp3cnv2xas5bcvgdycj565i95a") (f (quote (("runtime-tokio" "tokio" "tls-api/runtime-tokio" "tls-api-test/runtime-tokio") ("runtime-async-std" "async-std" "tls-api/runtime-async-std" "tls-api-test/runtime-async-std") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-native-tls-0.6.0 (c (n "tls-api-native-tls") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("alpn"))) (d #t) (k 0)) (d (n "test-cert-gen") (r "=0.6.0") (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tls-api") (r "=0.6.0") (k 0)) (d (n "tls-api-test") (r "=0.6.0") (k 0)) (d (n "tls-api-test") (r "=0.6.0") (k 1)) (d (n "tls-api-test") (r "=0.6.0") (k 2)) (d (n "tokio") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "014k9gdr835fra6a06f9pv28wxnxmnpqh1c8bnzrvbi6m1v90nr8") (f (quote (("runtime-tokio" "tokio" "tls-api/runtime-tokio" "tls-api-test/runtime-tokio") ("runtime-async-std" "async-std" "tls-api/runtime-async-std" "tls-api-test/runtime-async-std") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-native-tls-0.7.0 (c (n "tls-api-native-tls") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("alpn"))) (d #t) (k 0)) (d (n "test-cert-gen") (r "=0.7.0") (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tls-api") (r "=0.7.0") (k 0)) (d (n "tls-api-test") (r "=0.7.0") (k 0)) (d (n "tls-api-test") (r "=0.7.0") (k 1)) (d (n "tls-api-test") (r "=0.7.0") (k 2)) (d (n "tokio") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "1d29yb2izfxfwzih8fqmzxyrbcrw5xbq1j83z14yb92ibd0dniy5") (f (quote (("runtime-tokio" "tokio" "tls-api/runtime-tokio" "tls-api-test/runtime-tokio") ("runtime-async-std" "async-std" "tls-api/runtime-async-std" "tls-api-test/runtime-async-std") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-native-tls-0.8.0 (c (n "tls-api-native-tls") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("alpn"))) (d #t) (k 0)) (d (n "test-cert-gen") (r "=0.8.0") (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tls-api") (r "=0.8.0") (k 0)) (d (n "tls-api-test") (r "=0.8.0") (k 0)) (d (n "tls-api-test") (r "=0.8.0") (k 1)) (d (n "tls-api-test") (r "=0.8.0") (k 2)) (d (n "tokio") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0w2gj3nvxi0nad5bpnsvrjd7kvz0pw6awbbxk92937k9lkvy0ys5") (f (quote (("runtime-tokio" "tokio" "tls-api/runtime-tokio" "tls-api-test/runtime-tokio") ("runtime-async-std" "async-std" "tls-api/runtime-async-std" "tls-api-test/runtime-async-std") ("default" "runtime-tokio"))))))

(define-public crate-tls-api-native-tls-0.9.0 (c (n "tls-api-native-tls") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (f (quote ("alpn"))) (d #t) (k 0)) (d (n "test-cert-gen") (r "=0.9.0") (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tls-api") (r "=0.9.0") (k 0)) (d (n "tls-api-test") (r "=0.9.0") (k 0)) (d (n "tls-api-test") (r "=0.9.0") (k 1)) (d (n "tls-api-test") (r "=0.9.0") (k 2)) (d (n "tokio") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "1xq64ajl53x0yqkh0n36fn9i7kvgg49j0m7a5ifhl4jq4kd5p4vv") (f (quote (("runtime-tokio" "tokio" "tls-api/runtime-tokio" "tls-api-test/runtime-tokio") ("runtime-async-std" "async-std" "tls-api/runtime-async-std" "tls-api-test/runtime-async-std") ("default" "runtime-tokio"))))))

