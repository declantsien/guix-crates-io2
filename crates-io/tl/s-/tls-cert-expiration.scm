(define-module (crates-io tl s- tls-cert-expiration) #:use-module (crates-io))

(define-public crate-tls-cert-expiration-0.1.2 (c (n "tls-cert-expiration") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "certeef") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1vw8gcykakvgqljcicm72v9slmw1r21nrn7r5n3j2l24lwzj4y2z")))

