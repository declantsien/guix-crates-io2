(define-module (crates-io tl s- tls-decrypt) #:use-module (crates-io))

(define-public crate-tls-decrypt-0.1.0 (c (n "tls-decrypt") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.64") (d #t) (k 0)))) (h "0vb2c78zvmxqmhk4v7jxx5pg64hh3i1aw43msx2h5d5vyqypm89k")))

(define-public crate-tls-decrypt-0.1.2 (c (n "tls-decrypt") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.64") (d #t) (k 0)))) (h "1mb6f62g15hbbi6ki1zncxmxnx87fgy4kis1nyiha39yhc189bb2")))

(define-public crate-tls-decrypt-0.1.3 (c (n "tls-decrypt") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.64") (d #t) (k 0)))) (h "1bif4jqidwck47q36dagx5bz0phawb9i7n41x328h1l66ygr57zi")))

