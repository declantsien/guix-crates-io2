(define-module (crates-io tl ki tlkit-expand) #:use-module (crates-io))

(define-public crate-tlkit-expand-0.1.0 (c (n "tlkit-expand") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0f3vnas9pdpgn9hj7d8snpwzqlbkci7b5jjs4ifvp43c1xf6lybp")))

(define-public crate-tlkit-expand-0.2.0 (c (n "tlkit-expand") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0g5myrarcpnnlc804vnan6234zafrkzv1k17si4bzi5gb73i786w")))

(define-public crate-tlkit-expand-0.2.1 (c (n "tlkit-expand") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hy30jsbh0b9khjkac78rh89kb8c2dl0yq84yajnc970qjsa81rq")))

(define-public crate-tlkit-expand-0.2.3 (c (n "tlkit-expand") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mi24z8z5nqafldpzdmbnwa5yv847id4a8gkjgyx4i04s21iacw7")))

(define-public crate-tlkit-expand-0.2.6 (c (n "tlkit-expand") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11nmz1whpq11ky4j6bhs2rz2s99wmiwlrl7m0k24aanf0j6g7ma7")))

(define-public crate-tlkit-expand-0.2.7 (c (n "tlkit-expand") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "078cm1nnz7ah1b05dpsh7jfks6cc838svxd9av95iq9bcwdygjcx")))

(define-public crate-tlkit-expand-0.2.8 (c (n "tlkit-expand") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0hy7k87l1xqkbrp7m0czx68p510a7139mrncavj4wfkihn49i5y9")))

(define-public crate-tlkit-expand-0.2.9 (c (n "tlkit-expand") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0kkf4d0jzj4dlnl8c5m9zx1gbapzkl8lirxy2gwzxycj5z1b8q0m")))

(define-public crate-tlkit-expand-0.3.0 (c (n "tlkit-expand") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "procmeta") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00qy2vdsy7fwi1mclrxvig46287km61bf55cbd909314jq12qw5d")))

