(define-module (crates-io tl ki tlkit) #:use-module (crates-io))

(define-public crate-tlkit-0.1.0 (c (n "tlkit") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.1.0") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.1.0") (d #t) (k 0)))) (h "0308falwvcd193jgb6h2bck5r77cp6wax3g9ymnj1n03fdc2cvrz")))

(define-public crate-tlkit-0.2.0 (c (n "tlkit") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.1.0") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.1.0") (d #t) (k 0)))) (h "0zlnmp0sq6yc7cdgvz2xilir1c90zg8gf48br3mjcxngs8ynfmyx")))

(define-public crate-tlkit-0.2.1 (c (n "tlkit") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.1") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.1") (d #t) (k 0)))) (h "0s6s1wyna77g3nvzqr18hcngk5p9ssxqh2gwl9bc9lgi8c20vbw9")))

(define-public crate-tlkit-0.2.3 (c (n "tlkit") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.3") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.3") (d #t) (k 0)))) (h "0p928s3p36xphp584nm6lnw6ahh17y4s53fcrsvcdnpnlhmz47i6")))

(define-public crate-tlkit-0.2.4 (c (n "tlkit") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.3") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.3") (d #t) (k 0)))) (h "1ay2rm5mz6l232rj9gmkxmgzr57ll5vn2gzf7pf7rpdgkcha6gjd")))

(define-public crate-tlkit-0.2.5 (c (n "tlkit") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.3") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.4") (d #t) (k 0)))) (h "13slkpl8vb7zm9nmzyl12lmpzgakbzra53jksljzmpidniw9c1hn")))

(define-public crate-tlkit-0.2.6 (c (n "tlkit") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.7") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.6") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.6") (d #t) (k 0)))) (h "06vblk36q91i2da3h0v1ihhyabc4mzyz6xsjsl6l2j75ql6l30mq")))

(define-public crate-tlkit-0.2.7 (c (n "tlkit") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.2.9") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.7") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.7") (d #t) (k 0)))) (h "101bs4k99jb26shm1ldhk4ldl71q7pd0n4xj1x8p5bsg9w19gvqy")))

(define-public crate-tlkit-0.2.8 (c (n "tlkit") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.8") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.8") (d #t) (k 0)))) (h "0892yrv5vaalklpagjp9q9sxxpjkvbgyzaspc82fwbaswzzn12r9")))

(define-public crate-tlkit-0.2.9 (c (n "tlkit") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.2.9") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.2.9") (d #t) (k 0)))) (h "0fgyrjjdi6dhsykf7y2iwnrcy38kam31i7jpahfg14f3466b7h8w")))

(define-public crate-tlkit-0.3.0 (c (n "tlkit") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "procmeta") (r "^0.3.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tlkit-expand") (r "^0.3.0") (d #t) (k 0)) (d (n "tlkit-proc") (r "^0.3.0") (d #t) (k 0)))) (h "0fnab1wziza87122qclw8dsziwabrignbh4852wgwdr25b0iqljr")))

