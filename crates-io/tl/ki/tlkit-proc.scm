(define-module (crates-io tl ki tlkit-proc) #:use-module (crates-io))

(define-public crate-tlkit-proc-0.1.0 (c (n "tlkit-proc") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.1.0") (d #t) (k 0)))) (h "0r7yfbdmv1wi4ps79b268gb1dfakixhnkbfdps2hhhq2894dki40")))

(define-public crate-tlkit-proc-0.2.0 (c (n "tlkit-proc") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.1.0") (d #t) (k 0)))) (h "0yipvh28b0gad8ml3g293bpznvsi5hpv2v0yk9qfzpynd4z3qa91")))

(define-public crate-tlkit-proc-0.2.1 (c (n "tlkit-proc") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.1") (d #t) (k 0)))) (h "1ynnapl0s28ycsh3kmqsrnanchmgx7mh9n5p4l0mrdzx42ilxr6j")))

(define-public crate-tlkit-proc-0.2.3 (c (n "tlkit-proc") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.3") (d #t) (k 0)))) (h "1shb7f1lffcwg8ipxl6ab9gdbhq5grhrrn533imqixq842yvsk4d")))

(define-public crate-tlkit-proc-0.2.4 (c (n "tlkit-proc") (v "0.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.3") (d #t) (k 0)))) (h "0qzhkcgj512yp8d5lkfdzk6g2sz4qiqmsmr2vsfpqr5nczhc0nyq")))

(define-public crate-tlkit-proc-0.2.6 (c (n "tlkit-proc") (v "0.2.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.6") (d #t) (k 0)))) (h "05472d4f2gjfcvaampizvnw8qdmn17cjm5l9jw2gy3h7bgdp41yq")))

(define-public crate-tlkit-proc-0.2.7 (c (n "tlkit-proc") (v "0.2.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.7") (d #t) (k 0)))) (h "17lwb14jlasqkrgza915id242hky5s2yhrzmpwpiahjvcldirwx7")))

(define-public crate-tlkit-proc-0.2.8 (c (n "tlkit-proc") (v "0.2.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.8") (d #t) (k 0)))) (h "1j85j04s9h0y2kv0w6fyifnb7x35yb9994523b4a0y3dsgdqawbv")))

(define-public crate-tlkit-proc-0.2.9 (c (n "tlkit-proc") (v "0.2.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.2.9") (d #t) (k 0)))) (h "1kpcc75nlfw9d9w2ilaaxph56arc71580pwjakfnzbps9ix3pnci")))

(define-public crate-tlkit-proc-0.3.0 (c (n "tlkit-proc") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tlkit-expand") (r "^0.3.0") (d #t) (k 0)))) (h "1wyn6w5wldrynxw81940d1w1h1qfs12l8sq1krci5nva1bgz2bdz")))

