(define-module (crates-io tl ua tlua) #:use-module (crates-io))

(define-public crate-tlua-0.5.0 (c (n "tlua") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0jq5103i87w6323w41jhjzskhlbz1x1k30dpwpsqfagl4bcqsqd0")))

(define-public crate-tlua-0.5.1 (c (n "tlua") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1mqblvzqd50mjwjcl5xgwpddpzpmnh5blawbwqiimnraknv0x51m")))

(define-public crate-tlua-0.6.0 (c (n "tlua") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1") (d #t) (k 0)))) (h "08x22954421yccanb9fr8dr0lm18rg4p55wzqj5p2kn99vyp6ds7")))

(define-public crate-tlua-0.6.1 (c (n "tlua") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1") (d #t) (k 0)))) (h "1lxzwkr8xrq95896iw536hav87xxw9vxmacqljjkbd21vlnhq0bh")))

(define-public crate-tlua-0.6.2 (c (n "tlua") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1") (d #t) (k 0)))) (h "1dwn27v1m4zvd7b4h0rc8kzzpkkpxd42g9xw57gfqs02l9gmf2lq")))

(define-public crate-tlua-0.6.3 (c (n "tlua") (v "0.6.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1") (d #t) (k 0)))) (h "1pn5kykgzjw9nxd6avgw43z3n4sczwn5xndczfhdm35qlw90g74l")))

(define-public crate-tlua-0.6.4 (c (n "tlua") (v "0.6.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tlua-derive") (r "^0.1") (d #t) (k 0)))) (h "0x06zn8gjmvjjk2qp1sfd5aixj1qqgw2mjbvfx1yf0abs3w7sf1b") (r "1.59")))

(define-public crate-tlua-1.0.0 (c (n "tlua") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linkme") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tester") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tlua-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1rabriq5jivq0lgins75ymmg0y6rqg772c2ah7n4s5sargypjhzp") (f (quote (("test" "linkme" "tester")))) (r "1.61")))

(define-public crate-tlua-2.0.0 (c (n "tlua") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linkme") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tester") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tlua-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1mbx2sygr2jgsgkmjfks00h1a4dz60kv620z3l6ypcqb8kva35qr") (f (quote (("test" "linkme" "tester")))) (r "1.61")))

(define-public crate-tlua-3.0.0 (c (n "tlua") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linkme") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tester") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tlua-derive") (r "^0.2.0") (d #t) (k 0)))) (h "09qrgw9icvcsd2s7h4yl4bx5rc078gyk01fh40yvlinivbwncllc") (f (quote (("test" "linkme" "tester")))) (r "1.61")))

