(define-module (crates-io tl ua tlua-derive) #:use-module (crates-io))

(define-public crate-tlua-derive-0.1.0 (c (n "tlua-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z949v7gqd65hk482wn1ldfw0fa8y84l8wqfns1vd0gv71xb6586")))

(define-public crate-tlua-derive-0.1.1 (c (n "tlua-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k7fmdgmsggldkam1by5lgvzyd80sb07xnmlr38mwhgzj7mspjjz")))

(define-public crate-tlua-derive-0.1.2 (c (n "tlua-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("visit"))) (d #t) (k 0)))) (h "0w8bi0bbg26d2mzy8wnn7jidza8l6al6h5153zrkp0wv5s45g8bz")))

(define-public crate-tlua-derive-0.2.0 (c (n "tlua-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ldjf6dm2ysb0549bmbs48h7r1gf9zpwhpv0havnj5p6i9i5cygb") (r "1.61")))

