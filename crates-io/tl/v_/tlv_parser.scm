(define-module (crates-io tl v_ tlv_parser) #:use-module (crates-io))

(define-public crate-tlv_parser-0.0.1 (c (n "tlv_parser") (v "0.0.1") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "02vdc3khapjk80z794q09lr77m2h4pnz49hzn8r1m092cpliw0n3")))

(define-public crate-tlv_parser-0.0.2 (c (n "tlv_parser") (v "0.0.2") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "1fw4wwfc9vwmxaywj619pzl4h0mxrz8k9yxz3g0yq347ys7h8w1r")))

(define-public crate-tlv_parser-0.1.1 (c (n "tlv_parser") (v "0.1.1") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "06z8kjzv20kajgkypk5xwr1lmqacm051mbh8zz2g5rwv9hnw7k4k")))

(define-public crate-tlv_parser-0.1.2 (c (n "tlv_parser") (v "0.1.2") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "09gzpmnrq0a0rhzp8i17ngz4jq0x17zybl6mydbkn4116f1gl35i")))

(define-public crate-tlv_parser-0.1.3 (c (n "tlv_parser") (v "0.1.3") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "1xcpivkhfrw7vg71wdh9f6iarlm4kbbxrhfgzrli8z24z8zs0189")))

(define-public crate-tlv_parser-0.1.4 (c (n "tlv_parser") (v "0.1.4") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0") (d #t) (k 0)))) (h "183n4n5yyivczj0nqjcanq3ffj0yrdl8484rcn5wn331jdizbwx9")))

(define-public crate-tlv_parser-0.1.5 (c (n "tlv_parser") (v "0.1.5") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "10d5d2wnb2x5v2wwxk54vzcllzxzngfhnh4cmnrldj6n434nsikv")))

(define-public crate-tlv_parser-0.1.6 (c (n "tlv_parser") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "18r9kznh8d268ccmx3rmiw2p1lncf1ddgm7yi6qwq4imyrag4w2g")))

(define-public crate-tlv_parser-0.1.7 (c (n "tlv_parser") (v "0.1.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1adc5wgfy0k47dmqziigx06804cnqjmqibqv0mvq04hmm5fp1jyh")))

(define-public crate-tlv_parser-0.1.8 (c (n "tlv_parser") (v "0.1.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "03bjvvgbv6zkxafld7gbzzsaqrzwqigcynfi2k63lwfxa3bc9q7c")))

(define-public crate-tlv_parser-0.2.0 (c (n "tlv_parser") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1flga4mvdlizmsk1zfv2w50s3mm9sv8xmhq92c8h8ws7z64rmi48")))

(define-public crate-tlv_parser-0.3.0 (c (n "tlv_parser") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0") (d #t) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "0a7wi1l7aay47181c577p2p9mqlci9ry2j1lw3r8nkjh2s0g9g21")))

(define-public crate-tlv_parser-0.4.0 (c (n "tlv_parser") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1mjpwic1vg2m85wsvlax7whn112xsvk3y7d56va8fyqss8qh1dxi") (f (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.5.0 (c (n "tlv_parser") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "0x69xxh52x7wl3c4ymgv57icjnrp3n4c3v0pi4r4icrfndckc15x") (f (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.5.1 (c (n "tlv_parser") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "0hz1akjvws2zicl1cmpdrg9ddrix7k1x3aslrs8rf72a4wly9v70") (f (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.6.0 (c (n "tlv_parser") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1ccxm0nsv9pcq5nx6gkm6nypx8m1212maqrkmbppxgmmx6nxfvbr")))

(define-public crate-tlv_parser-0.7.0 (c (n "tlv_parser") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1733brg24lbd7za4plnxik7sq5as7wxwz1b4rwlhp5lc20f7iydn")))

(define-public crate-tlv_parser-0.7.1 (c (n "tlv_parser") (v "0.7.1") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1h7hkrkd0cylnjkv880xid2mhanrgs7ll4k78wirgsf1lvywbf5q")))

(define-public crate-tlv_parser-0.8.0 (c (n "tlv_parser") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "1544w6vkgjid45iv0vq5lz8rrx9mz54566h5szb589wz721mbzx3")))

(define-public crate-tlv_parser-0.9.0 (c (n "tlv_parser") (v "0.9.0") (d (list (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "16gq26dnprcn8nwhrinr363gdkb0x65ryh3whki9vl7dgbi3vs2a")))

