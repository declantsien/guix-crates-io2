(define-module (crates-io tl ru tlru) #:use-module (crates-io))

(define-public crate-tlru-0.1.0 (c (n "tlru") (v "0.1.0") (d (list (d (n "sn_fake_clock") (r "~0.4.0") (d #t) (k 2)))) (h "0rv9ivi971nd8hwx739yagbf9kg9xwp4k667kvg60ab94m1k496c") (y #t)))

(define-public crate-tlru-0.1.1 (c (n "tlru") (v "0.1.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "sn_fake_clock") (r "~0.4.0") (d #t) (k 2)))) (h "1569c38yq694gwvvkvwmy44mypijcn0n6zbjmw8i8r0b0si7k7p6") (y #t)))

