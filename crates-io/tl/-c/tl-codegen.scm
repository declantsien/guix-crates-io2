(define-module (crates-io tl -c tl-codegen) #:use-module (crates-io))

(define-public crate-tl-codegen-0.1.0 (c (n "tl-codegen") (v "0.1.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)))) (h "1f0yhi9mswa1ag4pd05xx873rf5bkx0wm7g5d0j9chr3426xdc56")))

