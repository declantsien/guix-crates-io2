(define-module (crates-io tl he tlhelp32) #:use-module (crates-io))

(define-public crate-tlhelp32-1.0.0 (c (n "tlhelp32") (v "1.0.0") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("handleapi" "tlhelp32"))) (d #t) (k 0)))) (h "1b1r70i15db1gycqy7alin07p5f0cq8rlr5r8cppw01mwmg5vfpr")))

(define-public crate-tlhelp32-1.0.1 (c (n "tlhelp32") (v "1.0.1") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("handleapi" "tlhelp32"))) (d #t) (k 0)))) (h "005hmdiq8150jbzazfwrivxs0ss74dqakv1him5gxspslz83sxap")))

(define-public crate-tlhelp32-1.0.2 (c (n "tlhelp32") (v "1.0.2") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("handleapi" "tlhelp32"))) (d #t) (k 0)))) (h "1947d23hprzxsbzs67s72rgr4m0bizlskjx7vlym2rsvi0ay9yix")))

(define-public crate-tlhelp32-1.0.3 (c (n "tlhelp32") (v "1.0.3") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32"))) (d #t) (k 0)))) (h "09dadfm10srgrs3hhr4q0niz1xxafb1y7m98f4bmzgb14ycnx4ym")))

