(define-module (crates-io tl dr tldr-news) #:use-module (crates-io))

(define-public crate-tldr-news-0.1.0 (c (n "tldr-news") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "1yywx891yxzf2k2b2r24wkkx01jwxi64wd8pzs0gkdxx1ppl6d0g")))

(define-public crate-tldr-news-0.1.1 (c (n "tldr-news") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "0yy4wmk3pijidwwrb27b9qb6yhz7whji2w2pnklmnz841sdlks2y")))

(define-public crate-tldr-news-0.1.2 (c (n "tldr-news") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "0dw5gzafi659g01nczb9lrnhda530mqna5hm70wlicz3h17sf15k")))

