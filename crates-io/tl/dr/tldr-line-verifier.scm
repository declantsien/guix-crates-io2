(define-module (crates-io tl dr tldr-line-verifier) #:use-module (crates-io))

(define-public crate-tldr-line-verifier-0.1.0 (c (n "tldr-line-verifier") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1gvyq5xq1bj9y8hbkb0p544mrgyk8sy1bd9x1fnwjfzgl4z25yfb")))

(define-public crate-tldr-line-verifier-0.1.1 (c (n "tldr-line-verifier") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0hdvmx3g61rr0bnygdj0qzj4xzr2jr8yb5vm7sr7rxlm9xf92lsc")))

(define-public crate-tldr-line-verifier-0.1.2 (c (n "tldr-line-verifier") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1x2w40qxmrrrk4hikg2y0lfmc1h4c08qk08nsyxlai17v47f4w37")))

