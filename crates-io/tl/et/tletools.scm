(define-module (crates-io tl et tletools) #:use-module (crates-io))

(define-public crate-tletools-0.1.0 (c (n "tletools") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "nom") (r "^7.1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "sgp4") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tle-parser") (r "^0.1.3") (d #t) (k 2)))) (h "0zcz6i641f7hh016wrw1zx27bchkh27f1avkrd8x5ni13vxy3yb7")))

