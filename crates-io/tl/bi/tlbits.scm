(define-module (crates-io tl bi tlbits) #:use-module (crates-io))

(define-public crate-tlbits-0.1.0 (c (n "tlbits") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g5fwpmwpi4ks1afcx37zwawmsz8bbh81p1npcgkiks7vfnll8iq")))

(define-public crate-tlbits-0.1.1 (c (n "tlbits") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nvv0ac2mgszgcqv61fvah7n35c5hczc46dd6dikq8c3f5r6ygy6")))

(define-public crate-tlbits-0.2.0 (c (n "tlbits") (v "0.2.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qcz1s3r90r53kr6amvgqq845dvwa0jbfjz3yxkg061imjm427v5")))

(define-public crate-tlbits-0.2.1 (c (n "tlbits") (v "0.2.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jc1gd5ir3kq6ym2xqv2alhvzxlkj30gy1pqpibl14181gvi1nvq")))

(define-public crate-tlbits-0.2.2 (c (n "tlbits") (v "0.2.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0blz0gbxxcfp4capcl2cb47iw4hidpyl08pcvk0lffw8438lak7j")))

(define-public crate-tlbits-0.2.3 (c (n "tlbits") (v "0.2.3") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03nglliafag27nvsdcmxlfhzldnwsds9chfvxsnqxm70j2gf6wm5")))

(define-public crate-tlbits-0.2.4 (c (n "tlbits") (v "0.2.4") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14zqafr50k9yldadkf86qlr18lqrh9wh247a194wdsb7niqnvvyn")))

(define-public crate-tlbits-0.2.5 (c (n "tlbits") (v "0.2.5") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03liaxj2cbwrzygkwp0w68029vsbnkd3xhcadc891craqpxsn0ca")))

(define-public crate-tlbits-0.2.6 (c (n "tlbits") (v "0.2.6") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dj834d41zxbhqrq6sarr5bafdmfzhnhmbh3kd03rj0dixnp4a0f")))

(define-public crate-tlbits-0.2.7 (c (n "tlbits") (v "0.2.7") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "055cjcw3vnxl3inlnc91v97qhiljl6dw4l3cx8fpfzzcj4h761wk")))

(define-public crate-tlbits-0.2.8 (c (n "tlbits") (v "0.2.8") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w85c2qvp9d7y8sy2jnz815nwdy3xw43pb5kjac1270j875ir1sj")))

(define-public crate-tlbits-0.2.9 (c (n "tlbits") (v "0.2.9") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vq5wjizg0wp1sb32y4k05aa9f2apzwngi6v7jllsj9m20r3llhr")))

(define-public crate-tlbits-0.2.10 (c (n "tlbits") (v "0.2.10") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b7gl1icl7ivjrk739qqbmm5hqmag4ad5ilbb10s77vmayldbhra")))

(define-public crate-tlbits-0.2.11 (c (n "tlbits") (v "0.2.11") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mpivrlfkhpqr4zsv13as1zbhw6pk6avxn9n4rp8axrp6lflbzgl")))

(define-public crate-tlbits-0.2.14 (c (n "tlbits") (v "0.2.14") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p7837xfgzdwq9v4dxx27m08hss1xmsbs3a3bpc677ihlds4xmsm")))

(define-public crate-tlbits-0.2.15 (c (n "tlbits") (v "0.2.15") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rwm0ka3qj37f9p3c421b7w1c3afzdb7n1bmci51ml2l0s92p256")))

(define-public crate-tlbits-0.2.16 (c (n "tlbits") (v "0.2.16") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "181jn9kfx6qkrbcxmymqfkz4m3dy2qg2ww8hv5g5kndvb1kas4g2")))

(define-public crate-tlbits-0.2.17 (c (n "tlbits") (v "0.2.17") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sw25wgh2afprkpnrbd6jggqklqg6h3zm7wrx8pg0lks2gvxq24x")))

(define-public crate-tlbits-0.2.18 (c (n "tlbits") (v "0.2.18") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hmrs3xgxgy8rz5g4iv8cqv4ma75zp73nis3nc37z3y9vi9i0mj3")))

(define-public crate-tlbits-0.2.19 (c (n "tlbits") (v "0.2.19") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "impl-tools") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z6199fkcm57k7awn2avfvhd1b5liiaq5zz5m08lq0jrc6xf3iar")))

