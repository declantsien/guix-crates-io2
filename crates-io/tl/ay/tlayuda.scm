(define-module (crates-io tl ay tlayuda) #:use-module (crates-io))

(define-public crate-tlayuda-0.1.4 (c (n "tlayuda") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "004kzfx02ssks7mc8jfrb8p52krm8ly9dlm3kv15a48a4slwankf")))

(define-public crate-tlayuda-0.1.5 (c (n "tlayuda") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zh43fr8yf077h2z8gvpgkx5pqfw8k0787m4ra2949amx0b6x6v4") (f (quote (("allow_outside_tests"))))))

(define-public crate-tlayuda-0.1.6 (c (n "tlayuda") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w81xvv4ilq28bdhjg7xjmj8dwr3n918wksanlg6kbkqpsn3fvai") (f (quote (("allow_outside_tests"))))))

