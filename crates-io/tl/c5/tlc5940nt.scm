(define-module (crates-io tl c5 tlc5940nt) #:use-module (crates-io))

(define-public crate-tlc5940nt-0.1.0 (c (n "tlc5940nt") (v "0.1.0") (d (list (d (n "gpio") (r "^0.4.1") (d #t) (k 0)))) (h "1im1ch34g4n13a60zdnlrszglx2109jh93d6d0m7xidkf2agm565") (y #t)))

(define-public crate-tlc5940nt-0.1.1 (c (n "tlc5940nt") (v "0.1.1") (h "0palp67wdl3nlwz1plgv9bd5pb9vnwl5s7g95c3dwwjimfnl3995") (y #t)))

