(define-module (crates-io tl c5 tlc59xxx) #:use-module (crates-io))

(define-public crate-tlc59xxx-0.1.0 (c (n "tlc59xxx") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.15.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1ya5hdbbv1w3yf5v0xqnf8qx9v4srfgmfgcb5m6f2ij766digzk2")))

(define-public crate-tlc59xxx-0.1.1 (c (n "tlc59xxx") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.15.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "18bswsry1nci0v3jpbc8aqkpxkif6s565hnh4d9z5myj9zl3r0l3")))

