(define-module (crates-io tl sf tlsf) #:use-module (crates-io))

(define-public crate-tlsf-1.0.0 (c (n "tlsf") (v "1.0.0") (d (list (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "embed-doc-image") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1npk7xq1q1ddskg5nwcdl3bwrb28ynfpdh59hq32kh5lp6z4qa8j") (s 2) (e (quote (("internal-doc-images" "dep:embed-doc-image"))))))

(define-public crate-tlsf-1.1.0 (c (n "tlsf") (v "1.1.0") (d (list (d (n "cov-mark") (r "^1.1.0") (d #t) (k 2)) (d (n "embed-doc-image") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1jdrcdz9vbnmjk100s5q55f2zlrsczxkn3nk84sz05p6qliil1zl") (s 2) (e (quote (("internal-doc-images" "dep:embed-doc-image"))))))

