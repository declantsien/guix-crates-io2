(define-module (crates-io tl -s tl-scheme) #:use-module (crates-io))

(define-public crate-tl-scheme-0.1.0 (c (n "tl-scheme") (v "0.1.0") (d (list (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rflyfhixvvmcb6yj16d4wnzlkwm381nrjnqh7qxfab2awh007lb")))

(define-public crate-tl-scheme-0.1.1 (c (n "tl-scheme") (v "0.1.1") (d (list (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ijwk50cx5djpwf4ap85mc07drx48maq9ifpsaw3p9r4iq9sxhd3")))

(define-public crate-tl-scheme-0.1.2 (c (n "tl-scheme") (v "0.1.2") (d (list (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p9ckrdpwf3ni244zp2qbb91dy439hba9m0rg1rx7jzgk6wjfdkn")))

(define-public crate-tl-scheme-0.2.0 (c (n "tl-scheme") (v "0.2.0") (d (list (d (n "crc") (r "^3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mhsw11ca5zn1if3g1skjpa8lzv64bijp2zdsm2bxip101pb7gwz")))

