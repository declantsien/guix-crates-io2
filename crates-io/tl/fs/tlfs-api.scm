(define-module (crates-io tl fs tlfs-api) #:use-module (crates-io))

(define-public crate-tlfs-api-0.1.0 (c (n "tlfs-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "ffi-gen") (r "^0.1.5") (d #t) (k 1)) (d (n "ffi-gen-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "tlfs") (r "^0.1.0") (d #t) (k 0)))) (h "1y9y4zhffpy0fn32x2fy7vrnrghkyb24d7zzsqcaj1l5rli6brwq") (f (quote (("default" "capi" "futures") ("capi"))))))

