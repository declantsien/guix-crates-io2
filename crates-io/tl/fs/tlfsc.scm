(define-module (crates-io tl fs tlfsc) #:use-module (crates-io))

(define-public crate-tlfsc-0.1.0 (c (n "tlfsc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "tlfs-crdt") (r "^0.1.0") (d #t) (k 0)))) (h "1w33wkz5fnh29ak3vazh1xcl0pysrda4dmv99djhjilkpsw3sjx4")))

