(define-module (crates-io tl de tldextract-rs) #:use-module (crates-io))

(define-public crate-tldextract-rs-0.0.0 (c (n "tldextract-rs") (v "0.0.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tld_extract") (r "^0.1.0") (f (quote ("serde" "reqwest"))) (d #t) (k 0)))) (h "1xg1wqplhz2vg969y47b34wziyjyq1ylm49jjg2qjaix2009qvys")))

(define-public crate-tldextract-rs-0.1.1 (c (n "tldextract-rs") (v "0.1.1") (d (list (d (n "idna") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "native-tls" "native-tls-vendored"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bvallxqkh3lm9kd8zg5avqzhvg01mpxpcw7dxamqvqa5y7bzxp6")))

