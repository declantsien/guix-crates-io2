(define-module (crates-io tl d_ tld_extract) #:use-module (crates-io))

(define-public crate-tld_extract-0.1.0 (c (n "tld_extract") (v "0.1.0") (d (list (d (n "idna") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "native-tls" "native-tls-vendored"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m77imif1j2ps1g98yhaljnvyv2pywvc74w78sv1m76dwq5k0lf9")))

