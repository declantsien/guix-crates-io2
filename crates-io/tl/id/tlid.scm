(define-module (crates-io tl id tlid) #:use-module (crates-io))

(define-public crate-tlid-0.1.0 (c (n "tlid") (v "0.1.0") (h "1jg11d6a3vjih0zb5x1dwig1gjkd9pmdiiqcbgavsyni2iyd1hfd") (f (quote (("std") ("log") ("default" "std"))))))

(define-public crate-tlid-0.2.0 (c (n "tlid") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "160ljdlk2p56lx8g56jv24qws62qa68b6v5i81vb3k422fz47jha") (f (quote (("std") ("log") ("default" "std" "num-traits"))))))

(define-public crate-tlid-0.2.1 (c (n "tlid") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07ja0sddcs66dcymgjb844mwgkg9rdmirmm9wm6q5sm2gxsa6n21") (f (quote (("std") ("log") ("default" "std" "num-traits"))))))

(define-public crate-tlid-0.2.2 (c (n "tlid") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07pn2wbj8hbsxzpichs3fvhpbirff6xfz1j0h20jkc0mwaf3y0yz") (f (quote (("std") ("log") ("default" "std" "num-traits"))))))

