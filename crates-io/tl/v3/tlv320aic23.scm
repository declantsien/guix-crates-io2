(define-module (crates-io tl v3 tlv320aic23) #:use-module (crates-io))

(define-public crate-tlv320aic23-0.1.0 (c (n "tlv320aic23") (v "0.1.0") (d (list (d (n "defmt") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0lbzfbazij5kbnm7f7cf635w8y28wqbiygjpc0q2jc9hjp4rjp5a") (f (quote (("defmt-log" "defmt"))))))

