(define-module (crates-io tl og tlog) #:use-module (crates-io))

(define-public crate-tlog-0.1.0 (c (n "tlog") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.0") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1334klcq1cvja921apwrmhhppnl7kdl18729r50kwcvy4nf4wydz")))

(define-public crate-tlog-0.1.1 (c (n "tlog") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.0") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0z4hfq7jh9s59dv643fys5s1iz4j60ssf7g17l01zgvl60apwxjz")))

(define-public crate-tlog-0.1.2 (c (n "tlog") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.0") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0y8bbqm6bvx38fp3dhlc75mxhhrplw4pir4wv940kjavsryc8djz")))

(define-public crate-tlog-0.1.3 (c (n "tlog") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.0") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1l1w9b4isb9xan4hiyg6y0l5kdhxvrk9s1kw4d1bnh48dg1ddrwb")))

