(define-module (crates-io tl na tlnat) #:use-module (crates-io))

(define-public crate-tlnat-0.1.0 (c (n "tlnat") (v "0.1.0") (h "0smgmzqjxva0fpl9kl17bkwi3i1gzpmp0lsn63dbw77g0xgpf9dj")))

(define-public crate-tlnat-0.1.1 (c (n "tlnat") (v "0.1.1") (h "01qz4h52y2wxl5kwx3hcjvfyikiaw3lrby7r2s4xx54v4lxajv4b")))

(define-public crate-tlnat-0.1.2 (c (n "tlnat") (v "0.1.2") (h "01zxi8a9fdz8kgkdarq5w05g6pgr9wvdwqs56s9nflqjvsvj2rlw")))

