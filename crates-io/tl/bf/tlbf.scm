(define-module (crates-io tl bf tlbf) #:use-module (crates-io))

(define-public crate-tlbf-0.1.0 (c (n "tlbf") (v "0.1.0") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "12bgdj2cnfkfyz1y1qm3sd8mg0syd2x69wa9ph46j18g03vrz7nc") (y #t)))

(define-public crate-tlbf-0.1.1 (c (n "tlbf") (v "0.1.1") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "0hi7wq68qff2vyfwdissalzdsv3kvns4lsdbl4vzanzdc4qrzbw7") (y #t)))

(define-public crate-tlbf-0.1.2 (c (n "tlbf") (v "0.1.2") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "1lxh2xdrj8p94bfih4qq9iavwb0fm42vydi7rmhmzbry48xf9jzf") (y #t)))

(define-public crate-tlbf-0.2.0 (c (n "tlbf") (v "0.2.0") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "1rvf8bsippjjmybz6jxmf8idz1v0iy7byhsfxz9qb8bk5qhsrxzy")))

(define-public crate-tlbf-0.3.0 (c (n "tlbf") (v "0.3.0") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "0mgnsrdjmj0klwjigdshzl1qldml1876dqjpfmlzailisghrilbk")))

(define-public crate-tlbf-0.3.1 (c (n "tlbf") (v "0.3.1") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "0cgngq7hv0hmrnr0ym59xs4p5xk5lpp0dzgg0bw66pkcdqbw3lw9")))

(define-public crate-tlbf-0.3.2 (c (n "tlbf") (v "0.3.2") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "13xz13qw6xbdpw8xvmb8zjzb7s0kayh80r42kwjm503iv54khp2f") (y #t)))

(define-public crate-tlbf-0.3.3 (c (n "tlbf") (v "0.3.3") (d (list (d (n "ghost") (r "^0.1.16") (d #t) (k 0)))) (h "0wvmdx9vq7974v0887ggi7y6jnaddvlfh81yy6mbx3rvqh5ircz8")))

