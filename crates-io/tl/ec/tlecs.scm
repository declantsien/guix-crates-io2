(define-module (crates-io tl ec tlecs) #:use-module (crates-io))

(define-public crate-tlecs-0.1.0 (c (n "tlecs") (v "0.1.0") (h "0nrlhfqzsrb0kw0pyx794ldmaahdii3dvb7bjpx76l28icrbgqrn") (y #t) (r "1.65")))

(define-public crate-tlecs-0.1.1 (c (n "tlecs") (v "0.1.1") (h "17bikg6961clljzazvhyvhmxwdb27r76hxhww98p63fjkj4wngi2") (y #t) (r "1.65")))

(define-public crate-tlecs-0.1.2 (c (n "tlecs") (v "0.1.2") (h "1012hzfj2ncfxrbrb7gws7m3haaqwgpmfgk0jgw43i5643vcfvv2") (y #t) (r "1.65")))

(define-public crate-tlecs-0.1.3 (c (n "tlecs") (v "0.1.3") (h "1qlcbcv67icsgqrjaqr9x8pjn42gbrnasfshvgx7m651vw6jmc4b") (r "1.65")))

(define-public crate-tlecs-0.1.4 (c (n "tlecs") (v "0.1.4") (h "1vbf7b9bh2ccrky9gnx9ws5r67gx3l5qp6m19nlbrm72dqqvfpsy") (r "1.65")))

