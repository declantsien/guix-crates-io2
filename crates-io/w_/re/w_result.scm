(define-module (crates-io w_ re w_result) #:use-module (crates-io))

(define-public crate-w_result-0.1.0 (c (n "w_result") (v "0.1.0") (d (list (d (n "log") (r "~0.3.5") (d #t) (k 0)))) (h "10qnympnbx85vwjn6vf487mgacw2y4m1d2cfif9bgaf9l68qydzg")))

(define-public crate-w_result-0.1.1 (c (n "w_result") (v "0.1.1") (d (list (d (n "log") (r "~0.3.5") (d #t) (k 0)))) (h "1f7s7fq93fgy9mjnn6pcgzfyb7a0l3dwdzjh08qjnpzr9fp2prbd")))

(define-public crate-w_result-0.1.2 (c (n "w_result") (v "0.1.2") (d (list (d (n "log") (r "~0.3.5") (d #t) (k 0)))) (h "153pc3zjfxv2b3l2wi6p0q57q2a5yna6gi95cfj89wxnc2qvrb25")))

