(define-module (crates-io nw n_ nwn_nasher_types) #:use-module (crates-io))

(define-public crate-nwn_nasher_types-0.1.0 (c (n "nwn_nasher_types") (v "0.1.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "0an4wifc0q6gdr23xj1ba3dhpha7vqs5xykx1rq48bh3axl820i0")))

(define-public crate-nwn_nasher_types-0.2.0 (c (n "nwn_nasher_types") (v "0.2.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "1xph2qhk2sgzv3y0nkq98vnvb45zxp6p9jnysx0p6dmi78ck8rvq")))

(define-public crate-nwn_nasher_types-0.2.1 (c (n "nwn_nasher_types") (v "0.2.1") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "0xngh8q40r9q7w78s09r7g2d1ylg82da2js8nn4w73kfqbrqdn85")))

(define-public crate-nwn_nasher_types-0.2.2 (c (n "nwn_nasher_types") (v "0.2.2") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "0my6vcrxmxhwdbd97z09sdj1i6nghc861y6gbm6j1mh6q7h6dfcq")))

(define-public crate-nwn_nasher_types-0.3.0 (c (n "nwn_nasher_types") (v "0.3.0") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "09vwi10hwhda5sl5zw89437307q2g66b5v62qgypylrw6z1dds4p")))

(define-public crate-nwn_nasher_types-0.3.1 (c (n "nwn_nasher_types") (v "0.3.1") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.29") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termdiff") (r "^3.1.2") (d #t) (k 2)))) (h "0ysjz7zdlaid1fjwwbv8s2fyrp5kw8k2bghvnv8rv6wz1qgwwlx3")))

