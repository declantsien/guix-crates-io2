(define-module (crates-io nw -s nw-sys) #:use-module (crates-io))

(define-public crate-nw-sys-0.0.0 (c (n "nw-sys") (v "0.0.0") (h "17xkggj7389grxmax25w2ywryzhlakfh6mibhbl75v4sbzafaqpa")))

(define-public crate-nw-sys-0.1.0 (c (n "nw-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "124395dwddd7cb2fn4kpk9gaqvgc22b901sj5cszglcqlk4z00gv")))

(define-public crate-nw-sys-0.1.2 (c (n "nw-sys") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "1r8110xi23jnkvzmalvig6wwjgbcbfa0bhng0skxrhqnskn8xmlf")))

(define-public crate-nw-sys-0.1.3 (c (n "nw-sys") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "0qs6yz430z69fy1ih2ajz8zz57z2k320xiwh256716w96yzw89gr") (f (quote (("markers") ("default" "markers"))))))

(define-public crate-nw-sys-0.1.4 (c (n "nw-sys") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "1dpkgxhz1avrhgbq5jcljwqqd3h3sags9xaj1hq35cn6fjajlvvr") (f (quote (("markers") ("default" "markers"))))))

(define-public crate-nw-sys-0.1.5 (c (n "nw-sys") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "0z4j58584jp3cx8gw36aa1xlxz4i2wpaz2l1712y9mm75583gq6x") (f (quote (("markers") ("default" "markers"))))))

(define-public crate-nw-sys-0.1.6 (c (n "nw-sys") (v "0.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console" "Document" "Window" "HtmlElement" "HtmlIFrameElement"))) (d #t) (k 0)))) (h "0yfa03g8k1wwrsgzx0xdhzddk6mgxyb87v8rlkp6bx3mrvwbpg4f") (f (quote (("markers") ("default" "markers"))))))

