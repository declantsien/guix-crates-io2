(define-module (crates-io nw bv nwbview) #:use-module (crates-io))

(define-public crate-nwbview-0.1.0 (c (n "nwbview") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "rfd") (r "^0.10") (d #t) (k 0)))) (h "0rw647as2wpzza58bsh7kfddly3b8fiwgggrg0g6h7rk0fp1rbhq")))

(define-public crate-nwbview-0.2.0 (c (n "nwbview") (v "0.2.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "rfd") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0ylk3f84b23pfa2671m16afzh4x3gxwg3pjhzijx8mk1lnrxwci3")))

(define-public crate-nwbview-0.2.1 (c (n "nwbview") (v "0.2.1") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "05c6qzgw00fhsagz17jrnkcs3c29f9s36g29f26agqslqw1ly781") (y #t)))

(define-public crate-nwbview-0.2.2 (c (n "nwbview") (v "0.2.2") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "195g06yv2kwpych4i9vzizrcik392jkqymws66v02qpa2i8w7chp") (y #t)))

(define-public crate-nwbview-0.2.3 (c (n "nwbview") (v "0.2.3") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1xxg6ynvhf1rygdjza3swv4n7rkxhqvimkgyv0x9gy1zhbqgjsxq") (y #t)))

(define-public crate-nwbview-0.2.4 (c (n "nwbview") (v "0.2.4") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1rmxx561p1xrc2qvg5bkw1873jvp4b4hld7j62w8j2y5dghp1kkr")))

