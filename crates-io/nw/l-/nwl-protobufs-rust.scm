(define-module (crates-io nw l- nwl-protobufs-rust) #:use-module (crates-io))

(define-public crate-nwl-protobufs-rust-0.1.0 (c (n "nwl-protobufs-rust") (v "0.1.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "14rjc1avqd0v27x7gwxd3iqjiv293r820q30bkz7wlzc23s53m0z") (y #t)))

