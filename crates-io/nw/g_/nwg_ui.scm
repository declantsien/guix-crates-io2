(define-module (crates-io nw g_ nwg_ui) #:use-module (crates-io))

(define-public crate-nwg_ui-1.0.0 (c (n "nwg_ui") (v "1.0.0") (d (list (d (n "nwg") (r "^1.0.12") (f (quote ("all" "flexbox"))) (d #t) (k 0) (p "native-windows-gui")) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "0yf55cnasjmcsgn2q6i8m81mj33ppmg1l8lnxsjcwp3c9ll576dd")))

(define-public crate-nwg_ui-1.0.1 (c (n "nwg_ui") (v "1.0.1") (d (list (d (n "nwg") (r "^1.0.12") (f (quote ("all" "flexbox"))) (d #t) (k 0) (p "native-windows-gui")) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "10zxm2rjqs62yz9rs163x5nddqzk1r06v0g3c1wjs4jldhc1049i")))

