(define-module (crates-io nw s- nws-forecast-zones) #:use-module (crates-io))

(define-public crate-nws-forecast-zones-0.1.0 (c (n "nws-forecast-zones") (v "0.1.0") (h "1gv5mvf439aaicc8d502bk2mzxrlkzh6xr6mlgcvj5g818nmdx5l")))

(define-public crate-nws-forecast-zones-0.1.1 (c (n "nws-forecast-zones") (v "0.1.1") (h "1pxhaxrvkzapn3p0d54i91fi79jsbfipixv55c5m1lqc6pv04345")))

(define-public crate-nws-forecast-zones-0.2.0 (c (n "nws-forecast-zones") (v "0.2.0") (h "00blkznmgli9d1hmfjj7qwyx1dz9bw6hvjchfl5b8jljfsc1why3")))

(define-public crate-nws-forecast-zones-0.3.0 (c (n "nws-forecast-zones") (v "0.3.0") (h "13n1vga9vmwyxff0y7cqbmzv4n762z4m7sd6f4hc4x0vdgkj6ksl")))

