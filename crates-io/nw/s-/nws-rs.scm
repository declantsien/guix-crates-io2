(define-module (crates-io nw s- nws-rs) #:use-module (crates-io))

(define-public crate-nws-rs-0.0.1 (c (n "nws-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "16fda4ba0cwww4h7k963rp723hvmffd0gh7774bfcad6yyqrrysj")))

