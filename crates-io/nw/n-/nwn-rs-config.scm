(define-module (crates-io nw n- nwn-rs-config) #:use-module (crates-io))

(define-public crate-nwn-rs-config-8193.34.0-alpha.1 (c (n "nwn-rs-config") (v "8193.34.0-alpha.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "0p7p8r68jn6knmch5l713ffq30y9gmjrwmjypvdqx9xck205xkac")))

