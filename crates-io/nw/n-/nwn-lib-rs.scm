(define-module (crates-io nw n- nwn-lib-rs) #:use-module (crates-io))

(define-public crate-nwn-lib-rs-0.1.0 (c (n "nwn-lib-rs") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0izyn92mgil91c3qaxlbpyg9ynvkz5xbbrx6dw9dvmmjc1mwfkix")))

(define-public crate-nwn-lib-rs-0.1.1 (c (n "nwn-lib-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0imfrz4j4cwd0ppz3p2n9z3r82grx6s8igz18a7z79yhxcdddv0d")))

(define-public crate-nwn-lib-rs-0.1.2 (c (n "nwn-lib-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k4c36fldalx7ky3k4c1clvqm6an1bi7g0r6b1vrck07a9yjrjc7")))

(define-public crate-nwn-lib-rs-0.2.0 (c (n "nwn-lib-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0jvk7d8djilqp6plgw1r1wkgv50mn04906qgx71x2821201g5ipv")))

(define-public crate-nwn-lib-rs-0.3.0 (c (n "nwn-lib-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0v9rligq463kxjxd4l2r2qpm71nj1jkngnhslclqjclcxkhad6dx")))

(define-public crate-nwn-lib-rs-0.3.2 (c (n "nwn-lib-rs") (v "0.3.2") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib-ng"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0pf1jdh5qjdd6mp4kgbxic9bj7sqmbswjr67ck11zv6af85q2x5n")))

(define-public crate-nwn-lib-rs-0.3.3 (c (n "nwn-lib-rs") (v "0.3.3") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib-ng"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1i1xw07awf24dvayfmjr3hjyzwfkicvclc16hz1zx703mdbkw2mf")))

(define-public crate-nwn-lib-rs-0.3.4 (c (n "nwn-lib-rs") (v "0.3.4") (d (list (d (n "base64ct") (r "^1.6.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib-ng"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "git-version") (r "^0.3.9") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0ii6zw3869y9bf9h9s8ldhbi2c93shqc60r2vqh1m5ws75s1wnjn")))

