(define-module (crates-io nw n- nwn-rs-masterlist) #:use-module (crates-io))

(define-public crate-nwn-rs-masterlist-8193.34.0-alpha.1 (c (n "nwn-rs-masterlist") (v "8193.34.0-alpha.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "085pygmsqfvmrrapj59b4smjpjn43mqyp9aq14ifkpp09xxv15af")))

(define-public crate-nwn-rs-masterlist-8193.34.1 (c (n "nwn-rs-masterlist") (v "8193.34.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jwlaqviigks2kv8yd4v4v6r9q14xy306fqa2n8ym59a23i66r0f")))

