(define-module (crates-io nw n- nwn-rs-runtime) #:use-module (crates-io))

(define-public crate-nwn-rs-runtime-8193.34.0-alpha.1 (c (n "nwn-rs-runtime") (v "8193.34.0-alpha.1") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nwn-rs-config") (r "^8193.34.0-alpha.1") (d #t) (k 0)) (d (n "nwn-rs-types") (r "^8193.34.0-alpha.1") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.12.0") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.12.0") (d #t) (k 1)))) (h "0y1mn8jy6cxpi4g86yrn8ggcz8l12q16f6z2xdjdv5sd6zdibmmm")))

