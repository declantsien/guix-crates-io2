(define-module (crates-io nw n- nwn-rs-types) #:use-module (crates-io))

(define-public crate-nwn-rs-types-8193.34.0-alpha.1 (c (n "nwn-rs-types") (v "8193.34.0-alpha.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "fmmap") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zstd") (r "^0.11.2") (d #t) (k 0)))) (h "1zrnc2b3d3lcqyb7a27mm54j66wc6f3claqx5hgdw0hyjcyppsfp")))

(define-public crate-nwn-rs-types-8193.34.1 (c (n "nwn-rs-types") (v "8193.34.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "fmmap") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zstd") (r "^0.11.2") (d #t) (k 0)))) (h "0736zscngwm8ri3fkjhbggc86vp4j4mq4g6zpy8f5hdskdmqkpmd")))

