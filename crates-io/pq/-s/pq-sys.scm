(define-module (crates-io pq -s pq-sys) #:use-module (crates-io))

(define-public crate-pq-sys-0.1.0 (c (n "pq-sys") (v "0.1.0") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)))) (h "054n9lnrs59yjna3p1pcfwfslwq2p0x6nzxkmcgbq7zwg1agdah1")))

(define-public crate-pq-sys-0.1.1 (c (n "pq-sys") (v "0.1.1") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)))) (h "1q60pi4imkzjl3qyphlrj2n0j5hxbhpc5savl5yhvpjsssl4xdwr")))

(define-public crate-pq-sys-0.2.0 (c (n "pq-sys") (v "0.2.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "10aa2hsl19k1kpwig655zm4i0xxg9z97g38gpi5qafigsp2pbfyq")))

(define-public crate-pq-sys-0.2.1 (c (n "pq-sys") (v "0.2.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0cb8dmbkl3rlkhmlvs60jhixgb7plwvy9hlmxmjl47lkwx8w66ff")))

(define-public crate-pq-sys-0.2.2 (c (n "pq-sys") (v "0.2.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1kspqp3ydh2p07jls23dkqf2lyzy2qznqbk6wqlcvvpsjxqyynr1")))

(define-public crate-pq-sys-0.2.3 (c (n "pq-sys") (v "0.2.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0ss3a6c6s6g494nprzdv56xh13m17dbwzkr9viyrrq7hlvd9q36m")))

(define-public crate-pq-sys-0.2.4 (c (n "pq-sys") (v "0.2.4") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "074i98y6d66wvn2claw79nqk7s5x7ainw1dxlziss312lmlrmydy")))

(define-public crate-pq-sys-0.2.5 (c (n "pq-sys") (v "0.2.5") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0kdzsmms28583lq26v0r2vyv4486mcq109rbb94rl9fsf0qxkh7g")))

(define-public crate-pq-sys-0.2.6 (c (n "pq-sys") (v "0.2.6") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "176w4m7r418yfbk67l7ra7526bafzb9chrlda8x0566r4x7d80cl")))

(define-public crate-pq-sys-0.2.7 (c (n "pq-sys") (v "0.2.7") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "07a8ifw92jbb70sqh5n51293mxjgckmx7nn6mpq43b1xrmsnvk9k")))

(define-public crate-pq-sys-0.3.0 (c (n "pq-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "1m32p4g2yh96p2bfns7hj6inlp20j17s01y7sqj92chah0ddl5m4")))

(define-public crate-pq-sys-0.3.1 (c (n "pq-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "1si2i4rh5lkcfm22ans38hmqjdbz5gkz6xwxs3qfbscl2076knsx")))

(define-public crate-pq-sys-0.3.2 (c (n "pq-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "0rfic8z0015v2rc9njcnh1v173nlfx97v7xwxx2qm2sbwi4zagv7")))

(define-public crate-pq-sys-0.4.0 (c (n "pq-sys") (v "0.4.0") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "1yabh283ncajahkq60ysgnc0n4iw077jxlay2jqmqcj67464x409") (y #t)))

(define-public crate-pq-sys-0.4.1 (c (n "pq-sys") (v "0.4.1") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "17ih6m4jcr9w8rg09033azcrhqwv4xfx8azzkm80s7y6x9pzj8c7")))

(define-public crate-pq-sys-0.4.2 (c (n "pq-sys") (v "0.4.2") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "1v7hmlh0yak5ggkambsjqpg7jwxhzgjf7cixrgai7w664i431fdq")))

(define-public crate-pq-sys-0.4.3 (c (n "pq-sys") (v "0.4.3") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "00xrcn7zlk52laccmci06c5x71db9dv9xx48yx6iymyq8bc6nf3g")))

(define-public crate-pq-sys-0.4.4 (c (n "pq-sys") (v "0.4.4") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0m4smnd7dplm469ywz7kpxrlykjxgjx4f79q59xinfprbrbmxysd")))

(define-public crate-pq-sys-0.4.5 (c (n "pq-sys") (v "0.4.5") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1w6j8a30rankd2pmcy55gdmghiv2my3vm00gsis13i2g937ac8wk") (l "pq")))

(define-public crate-pq-sys-0.4.6 (c (n "pq-sys") (v "0.4.6") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1npz9756283pjq3lcpwss8xh1rw4sx8f6dz8cxdg90h5bbp5xhka") (l "pq")))

(define-public crate-pq-sys-0.4.7 (c (n "pq-sys") (v "0.4.7") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1l8nl1b4dd2hah3lds21fi7dcgwmd2nqlaf5l9rgjm65irnmv11v") (l "pq")))

(define-public crate-pq-sys-0.4.8 (c (n "pq-sys") (v "0.4.8") (d (list (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.6") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1gfygvp69i5i6vxbi9qp2xaf75x09js9wy1hpl67r6fz4qj0bh1i") (l "pq")))

(define-public crate-pq-sys-0.5.0 (c (n "pq-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "pq-src") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0fl7pzbn8j43s5kmjh57qgplbi3lpn7jxvp2x177hqfgsgnlm5gb") (f (quote (("default") ("bundled" "pq-src")))) (l "pq") (s 2) (e (quote (("buildtime_bindgen" "dep:bindgen"))))))

(define-public crate-pq-sys-0.6.0 (c (n "pq-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "pq-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.6") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0jscim24a922vcmlr8sahsj4llaq6kw1y8gphl9agq9qhzxf6xjm") (f (quote (("default") ("bundled" "pq-src")))) (l "pq") (s 2) (e (quote (("buildtime_bindgen" "dep:bindgen"))))))

