(define-module (crates-io pq -s pq-src) #:use-module (crates-io))

(define-public crate-pq-src-0.1.0 (c (n "pq-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0qg4z6v1pcf08ny7yvb1q4qhwx6iym1fxbjixdwr03kssycq7j2p") (f (quote (("with-asan") ("default"))))))

(define-public crate-pq-src-0.1.1 (c (n "pq-src") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0qnix76hnjj7h9jpi3pqk7dx9p31indqknysbslnr89jizwz4ys2") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

(define-public crate-pq-src-0.1.2 (c (n "pq-src") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0s8sfc54z2pm7b1d71bqbgbyr2amaw60h07s9sfvhzsmvjbzsa0y") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

(define-public crate-pq-src-0.1.3 (c (n "pq-src") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1b9i9mkshm58gwj3jrb5wrj56jrmh54a26mqp2b52j236wj9ngqz") (f (quote (("with-asan") ("default")))) (y #t) (l "pq_sys_src")))

(define-public crate-pq-src-0.1.4 (c (n "pq-src") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1qv7jr687y1jng3l0hnhc0mgyqdhvgjc5lp2ij0s1l32aywmm9jk") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

(define-public crate-pq-src-0.1.5 (c (n "pq-src") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1qc06kq3siclzsl8y3g862prkm2cj7f8mg5kxa7k1pkdrxynqyxl") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

(define-public crate-pq-src-0.1.6 (c (n "pq-src") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-src") (r "^300.0.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 1)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1fgz8m973wr9jd1xbxfbsybwfd51mvqb2hxfb3cjzf6dw2jsb9a4") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

(define-public crate-pq-src-0.2.0 (c (n "pq-src") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "07dgq76xdvp0y247km67apf3vfzqbhlpai8rz2lp7g95wkdmwxhc") (f (quote (("with-asan") ("default")))) (l "pq_sys_src")))

