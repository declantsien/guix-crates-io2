(define-module (crates-io pq cr pqcrypto-internals-wasi) #:use-module (crates-io))

(define-public crate-pqcrypto-internals-wasi-0.2.1 (c (n "pqcrypto-internals-wasi") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18jxfdv1jwixl0zhbsv0ca2f8mnnysshd4lqpcj3p4h5d7402r0m") (y #t) (l "pqcrypto_internals")))

(define-public crate-pqcrypto-internals-wasi-0.2.2 (c (n "pqcrypto-internals-wasi") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q0kshzczf8l3ihcjzcakn1kvdb5l4lyr7fhmiai78908ayacmjd") (y #t) (l "pqcrypto_internals_wasi")))

(define-public crate-pqcrypto-internals-wasi-0.2.3 (c (n "pqcrypto-internals-wasi") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ri8yrly7dcpshag665sk64b947dhhycb7fci1s0ixr8ngk70006") (y #t) (l "pqcrypto_internals_wasi")))

(define-public crate-pqcrypto-internals-wasi-0.2.4 (c (n "pqcrypto-internals-wasi") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01hjaddqm3ygckzc62lnfhj3yjkpyrixk8q6ld6ia44pr9q5gr7b") (l "pqcrypto_internals_wasi")))

(define-public crate-pqcrypto-internals-wasi-0.2.5 (c (n "pqcrypto-internals-wasi") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q5w5bi3j9v05gmx20srkgfjpak5kas59mzpqr9902z7lixplvca") (l "pqcrypto_internals")))

