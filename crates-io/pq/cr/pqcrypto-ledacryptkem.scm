(define-module (crates-io pq cr pqcrypto-ledacryptkem) #:use-module (crates-io))

(define-public crate-pqcrypto-ledacryptkem-0.0.1 (c (n "pqcrypto-ledacryptkem") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "1md0badik727qircir23jcp537c0mrqkgw1hvj2kgx594xch721s") (y #t)))

(define-public crate-pqcrypto-ledacryptkem-0.0.2 (c (n "pqcrypto-ledacryptkem") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "15ryvgnzafkxysxkzaghmpm3liy3vzr1dzr0vqnlp6a3wk6j9qrm") (y #t)))

(define-public crate-pqcrypto-ledacryptkem-0.0.3 (c (n "pqcrypto-ledacryptkem") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "079x9rw5565mfswmr0nca7q1j2x6n5bg8rwbyp4gm2lsw307zzjv") (y #t)))

(define-public crate-pqcrypto-ledacryptkem-0.0.4 (c (n "pqcrypto-ledacryptkem") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "17vyqgc015c7ll2m8dfcxvdqkwrfqbh5kr8rvr0n9z12nngwd2p7") (f (quote (("default") ("cryptographically-insecure")))) (y #t)))

