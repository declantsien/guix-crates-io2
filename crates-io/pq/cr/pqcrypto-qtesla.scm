(define-module (crates-io pq cr pqcrypto-qtesla) #:use-module (crates-io))

(define-public crate-pqcrypto-qtesla-0.1.0 (c (n "pqcrypto-qtesla") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1brm2273hvb9bckqdrsisqfyn019pllmq3jvkk0c0isgsny08hbn")))

(define-public crate-pqcrypto-qtesla-0.1.1 (c (n "pqcrypto-qtesla") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0bd9r54dnr7a161rvs4mj3s1cg3xndriccav4gnd4vk3ix5yfjf3")))

