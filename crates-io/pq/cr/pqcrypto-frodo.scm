(define-module (crates-io pq cr pqcrypto-frodo) #:use-module (crates-io))

(define-public crate-pqcrypto-frodo-0.1.0 (c (n "pqcrypto-frodo") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1d4yd8dfas2qk7jhb8hx32r4mkqi1h7zxfzfib5b4ax2bb67qm2i")))

(define-public crate-pqcrypto-frodo-0.1.1 (c (n "pqcrypto-frodo") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1m597fdq7al17jd8pcwfzsnm0611f7a127gh8i9hnw17d6j8l0qx")))

(define-public crate-pqcrypto-frodo-0.1.2 (c (n "pqcrypto-frodo") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0w0h6jdxidgqjpag107i29cwqrrl5zxa630w4xhw1v78vy4pgg83")))

(define-public crate-pqcrypto-frodo-0.2.0 (c (n "pqcrypto-frodo") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.1.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0lzjy6g7a93mxczbm98180lsk4qdjg7ph8wid18ihhfr3m8s6dya")))

(define-public crate-pqcrypto-frodo-0.2.1 (c (n "pqcrypto-frodo") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.1.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1by5wwn2vz0zy9janz6zdh624k27dm18jxwdlv9srkazaxd9siqg")))

(define-public crate-pqcrypto-frodo-0.2.2 (c (n "pqcrypto-frodo") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.1.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0y4dbc7asdd0knqn5x3nmz6sbkmfjbk07w53j1gvmgzdkxdfqywk")))

(define-public crate-pqcrypto-frodo-0.3.0 (c (n "pqcrypto-frodo") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.0") (d #t) (k 0)))) (h "1gsvnxd85k7jg1ikacl82mszscs6iq56nsys3v3d1lihswnpn0sj")))

(define-public crate-pqcrypto-frodo-0.4.0 (c (n "pqcrypto-frodo") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.0") (d #t) (k 0)))) (h "07wf1x2shnrdfayjf3912a4cz3dbw7vqi4idisfj49fzrd1k9c12")))

(define-public crate-pqcrypto-frodo-0.4.1 (c (n "pqcrypto-frodo") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.0") (d #t) (k 0)))) (h "0fjmlb6wrkg6if8rr564wmsd5nlcqnakiqsl3v7hc5nax9w9n08d")))

(define-public crate-pqcrypto-frodo-0.4.2 (c (n "pqcrypto-frodo") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "1ws8nv7i0ncm4syln7728wc72sxqdgh8wlal8fwl5z4b96cldfdg")))

(define-public crate-pqcrypto-frodo-0.4.3 (c (n "pqcrypto-frodo") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "0zkzmgsri4nqk23mgf8aavjp4ixys39639h14wc15cq7jpg60zr9")))

(define-public crate-pqcrypto-frodo-0.4.4 (c (n "pqcrypto-frodo") (v "0.4.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "025pqss85g3gv1w9x3v4rz1g89sgh3jzmsns0zlidn4mrw6p241c")))

(define-public crate-pqcrypto-frodo-0.4.5 (c (n "pqcrypto-frodo") (v "0.4.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "14hikxil0wq3z9i1kdnh8kv1v99pbxcbgv1r5pqg1sv0zlgdi13s")))

(define-public crate-pqcrypto-frodo-0.4.6 (c (n "pqcrypto-frodo") (v "0.4.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "0y6nbjyjc61p9wmwfsmqwvwax1cyrnnjvc3rdb04siwxz2pwn48r")))

(define-public crate-pqcrypto-frodo-0.4.7 (c (n "pqcrypto-frodo") (v "0.4.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (f (quote ("const-generics"))) (o #t) (d #t) (k 0)))) (h "1046mn7r3hffs8pv2gsdgl4r7cd1hh0wcrzwh5yfjdqfqbljccqj") (f (quote (("serialization" "serde" "serde-big-array") ("default"))))))

(define-public crate-pqcrypto-frodo-0.4.9 (c (n "pqcrypto-frodo") (v "0.4.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.2") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (f (quote ("const-generics"))) (o #t) (d #t) (k 0)))) (h "0snq4xzlfqn00gcwk6np9y1yrn2kngwk53a0s1r3lklayc56ffsb") (f (quote (("std" "pqcrypto-traits/std") ("serialization" "serde" "serde-big-array") ("default" "std"))))))

(define-public crate-pqcrypto-frodo-0.4.10 (c (n "pqcrypto-frodo") (v "0.4.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.2") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (f (quote ("const-generics"))) (o #t) (d #t) (k 0)))) (h "1fjh14dwx1sxbsiymwdl30k6r29grrrjfy3y32dnlmxxjf56lm98") (f (quote (("std" "pqcrypto-traits/std") ("serialization" "serde" "serde-big-array") ("default" "std"))))))

(define-public crate-pqcrypto-frodo-0.4.11 (c (n "pqcrypto-frodo") (v "0.4.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-internals") (r "^0.2") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (f (quote ("const-generics"))) (o #t) (d #t) (k 0)))) (h "1lmm80ifmihy6giylxl5mz8l62rdcqnal76mx17vvh7lq2gjs4k5") (f (quote (("std" "pqcrypto-traits/std") ("serialization" "serde" "serde-big-array") ("default" "std"))))))

