(define-module (crates-io pq cr pqcrypto-newhope) #:use-module (crates-io))

(define-public crate-pqcrypto-newhope-0.1.0 (c (n "pqcrypto-newhope") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "1y2rlw167gjyghqlj35c4qlqh3ag5wfwln4i3v95ywrnchnjyc5f")))

(define-public crate-pqcrypto-newhope-0.1.1 (c (n "pqcrypto-newhope") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "0m75rq9sfxzagx5psqbwz5xdpf9ib6ywc4wgyvg7x8x9z4rqmk8y")))

(define-public crate-pqcrypto-newhope-0.1.2 (c (n "pqcrypto-newhope") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "1m6bgsmkdhmsv9slcd6lbzq9lw8qk7i192wskxpnbsnba04rh9pr")))

