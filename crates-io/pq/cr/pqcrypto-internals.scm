(define-module (crates-io pq cr pqcrypto-internals) #:use-module (crates-io))

(define-public crate-pqcrypto-internals-0.1.0 (c (n "pqcrypto-internals") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "14x4jcbxdwxfkxg9f4xpl4qcdr6b7yqn70f94lzg7r70kx8s1hdg")))

(define-public crate-pqcrypto-internals-0.2.1 (c (n "pqcrypto-internals") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "116a9qqgvslcbf55c4n2zff7qlb96xy5b48gndpx6x98p4sp6ff5") (l "pqcrypto_internals")))

(define-public crate-pqcrypto-internals-0.2.2 (c (n "pqcrypto-internals") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xyq7fgx68kw9s8cwdbiwbc58hb2pz3lh578qljds051snadyhki") (l "pqcrypto_internals")))

(define-public crate-pqcrypto-internals-0.2.3 (c (n "pqcrypto-internals") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ic60m2ib62b4zrvxg1589wvfn629i1xnigf9akm313mwcs3xcb4") (l "pqcrypto_internals")))

(define-public crate-pqcrypto-internals-0.2.4 (c (n "pqcrypto-internals") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cj64c35d8dhwbfp42h0b91f7si1g63dgzvfllwm2n4z4g0cn9q1") (l "pqcrypto_internals")))

(define-public crate-pqcrypto-internals-0.2.5 (c (n "pqcrypto-internals") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ipv96543y0g71qkwzir0cigl7rd565vcj3pvvk868mydbn4plyr") (l "pqcrypto_internals")))

