(define-module (crates-io pq cr pqcrypto-threebears) #:use-module (crates-io))

(define-public crate-pqcrypto-threebears-0.1.0 (c (n "pqcrypto-threebears") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "18r0ah8xaa0p2ppwdpkj33swcz3wxl7g3091cazgyr9k4mk3c1nl")))

(define-public crate-pqcrypto-threebears-0.2.0 (c (n "pqcrypto-threebears") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.2") (d #t) (k 0)))) (h "0lqrjsm7rngkhmc36v266hddd1ibsjycqc5sjs597n6jwszqkwxw")))

