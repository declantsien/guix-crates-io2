(define-module (crates-io pq -k pq-kem) #:use-module (crates-io))

(define-public crate-pq-kem-0.1.0 (c (n "pq-kem") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rac") (r "^1.0") (d #t) (k 0)))) (h "08wklcb4k200jmnd06wzr413gz1sq2y32vn4k5swya0vlx2zazs4")))

(define-public crate-pq-kem-0.2.0 (c (n "pq-kem") (v "0.2.0") (d (list (d (n "rac") (r "^1.1") (d #t) (k 0)))) (h "07gysp9iws6ncgj3b8l22fn1y0sg6c67xc5l19i11m9ckrql2wcd")))

(define-public crate-pq-kem-0.3.0 (c (n "pq-kem") (v "0.3.0") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "rac") (r "^1.1") (d #t) (k 0)))) (h "1b5dwhcb2qpca49qabw8g9kkrzlgl3jw5yf4jzjwpp50q035gac1")))

(define-public crate-pq-kem-0.4.0 (c (n "pq-kem") (v "0.4.0") (d (list (d (n "rac") (r "^1.3") (d #t) (k 0)))) (h "0gva35c4h56c968g6n3c8l6nwy8w1ylq9ip9lp9psq4m1xixqp1s")))

(define-public crate-pq-kem-0.5.0 (c (n "pq-kem") (v "0.5.0") (d (list (d (n "rac") (r "^1.3") (d #t) (k 0)))) (h "1wydgfc96l4is0n0za7g5c8ky0hjxsfyxgy6visr2s98vh1rl0an")))

