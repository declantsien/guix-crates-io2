(define-module (crates-io pq -b pq-bincode) #:use-module (crates-io))

(define-public crate-pq-bincode-1.0.0-alpha.0 (c (n "pq-bincode") (v "1.0.0-alpha.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 2)) (d (n "queue-file") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fb9afi9iipvv0y80qkwc395q23aicmr95pnlll5x6rjld097lnj")))

(define-public crate-pq-bincode-1.0.0-alpha.1 (c (n "pq-bincode") (v "1.0.0-alpha.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 2)) (d (n "queue-file") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "04dwi2k50bdykfjl2y9w8navcd8fbivzcq12cfyq06vb489j2cqr") (y #t)))

(define-public crate-pq-bincode-1.0.0-alpha.2 (c (n "pq-bincode") (v "1.0.0-alpha.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (f (quote ("serde"))) (d #t) (k 2)) (d (n "queue-file") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kashc3cig7piicxc7hi8crbxw944lga4hv6g9335qmq7a7f0mw")))

