(define-module (crates-io pq -t pq-tree) #:use-module (crates-io))

(define-public crate-pq-tree-0.1.0 (c (n "pq-tree") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "enum-map") (r "^2.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0q4dw105fa3dc6lmigx73wwngffmja78yr5gfqm330s1dz08m22q")))

