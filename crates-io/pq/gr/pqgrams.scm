(define-module (crates-io pq gr pqgrams) #:use-module (crates-io))

(define-public crate-pqgrams-0.9.0 (c (n "pqgrams") (v "0.9.0") (h "0b8p8li4gbcnm6g5cjrzxd1ff2v1yyyxywjy4q32y0bcaavxj3ik")))

(define-public crate-pqgrams-0.9.1 (c (n "pqgrams") (v "0.9.1") (h "0jfwprxlnb615rgpbx4cmy208q3dsczi86js9k1lnag03qca78cf")))

(define-public crate-pqgrams-0.9.2 (c (n "pqgrams") (v "0.9.2") (h "11n65hbp61jnj7i2l774j8jgdj7d3sjq80fycblsymsbxhvpdyfd")))

