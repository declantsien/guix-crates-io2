(define-module (crates-io pq ca pqcat) #:use-module (crates-io))

(define-public crate-pqcat-0.1.0 (c (n "pqcat") (v "0.1.0") (d (list (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "parquet" "dtype-full"))) (d #t) (k 0)))) (h "0npl80vbmckxrw9m01lyy1q3clgl0vvgmsskbffj4vh9xkg0qv75")))

(define-public crate-pqcat-0.1.1 (c (n "pqcat") (v "0.1.1") (d (list (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "parquet" "dtype-full" "timezones"))) (d #t) (k 0)))) (h "0jih9kgr0r09ddz525hm1y6plw8mdf5ii8r38cs7ppyd4ahblh9y")))

