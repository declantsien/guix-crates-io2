(define-module (crates-io pq c_ pqc_dilithium_edit) #:use-module (crates-io))

(define-public crate-pqc_dilithium_edit-0.2.0 (c (n "pqc_dilithium_edit") (v "0.2.0") (d (list (d (n "pqc_core") (r "^0.3.0") (f (quote ("load"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (k 0)))) (h "1hbmz59wi0pggkq9md4kisdfc2srfy0slnxlflbxhr2mx77f54nh") (f (quote (("random_signing") ("mode5") ("mode3") ("mode2") ("aes")))) (r "1.50")))

