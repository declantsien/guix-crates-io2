(define-module (crates-io pq c_ pqc_core) #:use-module (crates-io))

(define-public crate-pqc_core-0.1.0 (c (n "pqc_core") (v "0.1.0") (h "02l2fkkbyjnafji5qm2spj1lg1svchqn525fdwrzjmbq2bsl6mzz") (f (quote (("load"))))))

(define-public crate-pqc_core-0.2.0 (c (n "pqc_core") (v "0.2.0") (d (list (d (n "zeroize") (r "^1.5.7") (o #t) (d #t) (k 0)))) (h "0bfrh8xglvd4cqhxydijcx2y4nhg9irmwvp527hn3cni0lpfac9l") (f (quote (("zero" "zeroize") ("unique_feature") ("load"))))))

(define-public crate-pqc_core-0.3.0 (c (n "pqc_core") (v "0.3.0") (d (list (d (n "zeroize") (r "^1.5.7") (o #t) (k 0)))) (h "1haczpav6vwd92wxcl4lgakwmn820qspmbhmwqsgpgrkps20nj1g") (f (quote (("zero" "zeroize") ("unique_feature") ("load") ("batch_default"))))))

