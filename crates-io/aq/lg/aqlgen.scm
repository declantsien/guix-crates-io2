(define-module (crates-io aq lg aqlgen) #:use-module (crates-io))

(define-public crate-aqlgen-0.1.0 (c (n "aqlgen") (v "0.1.0") (d (list (d (n "aqlgen-renderer") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "141p8qrbhhasjfz49bkpwh98pv4vg1glc90ksl654myiv3f4iq5m")))

(define-public crate-aqlgen-0.2.0 (c (n "aqlgen") (v "0.2.0") (d (list (d (n "aqlgen-renderer") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "01pqlwfy4jf57bak6xkvhja2x1dld11pgcsr1x32hnmyizaw7gw7")))

(define-public crate-aqlgen-0.2.1 (c (n "aqlgen") (v "0.2.1") (d (list (d (n "aqlgen-renderer") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1c6g28mzy7qxz90a1af8s9jydgl9bif818l58hc66n9d4xzhh0wr")))

(define-public crate-aqlgen-0.3.0 (c (n "aqlgen") (v "0.3.0") (d (list (d (n "aqlgen-renderer") (r "0.*") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1x8qgv2szqp4xxnfgcswhvrjn2a4n2k9q77apfjh2hfyn144v7d2")))

(define-public crate-aqlgen-0.3.1 (c (n "aqlgen") (v "0.3.1") (d (list (d (n "aqlgen-renderer") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "0csny16fqb2nr9szaij5dpr8bikz4mqkq1fgf7hz0kp7z8sbs0js")))

(define-public crate-aqlgen-0.3.2 (c (n "aqlgen") (v "0.3.2") (d (list (d (n "aqlgen-renderer") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1mgl3c1rik54n1aciwvifm9wfmjv258597slifr21l8bjagf7mn6")))

(define-public crate-aqlgen-0.4.0 (c (n "aqlgen") (v "0.4.0") (d (list (d (n "aqlgen-renderer") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "166rj1x8xfn9bs5nfil6adbp11rm5k2pfvpkq3ka43aprh0kvkbv") (y #t)))

(define-public crate-aqlgen-0.5.0 (c (n "aqlgen") (v "0.5.0") (d (list (d (n "aqlgen-renderer") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "14y5s2bwwm6sh5a2518irx032g9m41sirw2chyya4l72vb17dn5h")))

(define-public crate-aqlgen-0.6.0 (c (n "aqlgen") (v "0.6.0") (d (list (d (n "aqlgen-renderer") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1i1s1v6vnisd8c5l4bfdp8c0a3nrg3lmkarjr88kd1qkdg0b60d4")))

(define-public crate-aqlgen-0.6.1 (c (n "aqlgen") (v "0.6.1") (d (list (d (n "aqlgen-renderer") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1vmzmxhjsxfbnq9243i0lbdvmamc9zkx32cm0zp8gwdh4ssp0d8y")))

(define-public crate-aqlgen-0.7.0 (c (n "aqlgen") (v "0.7.0") (d (list (d (n "aqlgen-renderer") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1dy2fciwp1ivr5n1g650cw411ww82a8qp7311qcn0qx96s3sw4xl")))

(define-public crate-aqlgen-0.7.1 (c (n "aqlgen") (v "0.7.1") (d (list (d (n "aqlgen-renderer") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "10pbki4jfp2axp03a668j06c7mlknf696k1hqpsxhb2z0gxb5z8w")))

(define-public crate-aqlgen-0.8.0 (c (n "aqlgen") (v "0.8.0") (d (list (d (n "aqlgen-renderer") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "clap_derive") (r "^3.1") (d #t) (k 0)))) (h "1lgva5sw75bqlfb8ak60q7vr0i5w5wn911wambdjjkin6nb4ihjk")))

