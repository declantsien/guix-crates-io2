(define-module (crates-io aq ue aqueue_trait) #:use-module (crates-io))

(define-public crate-aqueue_trait-0.1.0 (c (n "aqueue_trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.14") (d #t) (k 2)) (d (n "tracing-attributes") (r "^0.1.8") (d #t) (k 2)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0265iz8k0yx8s73qrzhwwb9wrs7sblisp0wrrxq75p00lfrmv8is") (y #t)))

