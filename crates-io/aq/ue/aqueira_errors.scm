(define-module (crates-io aq ue aqueira_errors) #:use-module (crates-io))

(define-public crate-aqueira_errors-0.0.1 (c (n "aqueira_errors") (v "0.0.1") (d (list (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11adnj3zwggd7jgc34k1sr4l9pxz9mi7m6qh56brzdcprm10jws3") (y #t)))

