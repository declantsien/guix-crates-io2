(define-module (crates-io aq ue aqueiralibrary) #:use-module (crates-io))

(define-public crate-aqueiralibrary-0.1.0 (c (n "aqueiralibrary") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0691z2qil66xw9ip5vbnb1ddz6ga0njyscyahhai40dvr4irwaxd")))

(define-public crate-aqueiralibrary-0.1.1 (c (n "aqueiralibrary") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhfr95xqdrw3p6bb390k3akz0z24cgbm524rfmypc238qsqxg8q")))

