(define-module (crates-io aq ue aqueduct) #:use-module (crates-io))

(define-public crate-aqueduct-0.0.0 (c (n "aqueduct") (v "0.0.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "scsys") (r "^0.1.24") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12lhx6l1rdi5v2idr57idjjf7z9nm8sas2d8gfw2p041yb69m9n1") (f (quote (("full" "core") ("default" "core") ("core"))))))

