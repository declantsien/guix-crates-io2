(define-module (crates-io aq ue aquestalk_rs) #:use-module (crates-io))

(define-public crate-aquestalk_rs-0.1.0 (c (n "aquestalk_rs") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "141qxav02lawahv223x2gz5xz405364g34fpkiljb4an5nglnw7d")))

(define-public crate-aquestalk_rs-0.1.1 (c (n "aquestalk_rs") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1gfn1fn5nmghfg3v2nn90b4cwvil1w0hsgxa08z83frf1mvsifi5")))

(define-public crate-aquestalk_rs-0.2.0 (c (n "aquestalk_rs") (v "0.2.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0gqkd9his0wl3d3rlmwm3fyjb9zlmwakn2fsqq9729x3arkfw1ag")))

(define-public crate-aquestalk_rs-0.2.1 (c (n "aquestalk_rs") (v "0.2.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1a7yms28f89gg4pwiggxwyjzsi494pzjj8k8ickbdlcrfbgnim5q")))

(define-public crate-aquestalk_rs-0.2.2 (c (n "aquestalk_rs") (v "0.2.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "19pj5kxsjag8h7gkc226wbgva31fcn0y7jx5w7d865s953sh0b0x")))

(define-public crate-aquestalk_rs-0.3.0 (c (n "aquestalk_rs") (v "0.3.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0sxvjw3drqf0b1cqhllxqnzazavsjg08mkq5mazl4gc8c06bxzvr")))

(define-public crate-aquestalk_rs-0.3.1 (c (n "aquestalk_rs") (v "0.3.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1rdr7k2zqzb6hx4rksqwvp8if6c4i2ziwhdx34lsrvrdpkmb76cw")))

(define-public crate-aquestalk_rs-0.3.2 (c (n "aquestalk_rs") (v "0.3.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "148iyvsas963vc3chi1b9l0if4lr3jggr9pi8kir0mrg09w1cazm")))

(define-public crate-aquestalk_rs-0.3.3 (c (n "aquestalk_rs") (v "0.3.3") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "10kaii7amjdfdaihzaf3ssw4a6yvgvxvq6sbfr41qpcmiz758bnd")))

(define-public crate-aquestalk_rs-0.3.4 (c (n "aquestalk_rs") (v "0.3.4") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "safety_breaker") (r "^0.1") (d #t) (k 0)))) (h "06mvpaifrwr6x0cqb0spnbqgi8mfmrljk3w5llfg948a7lbay6wc")))

