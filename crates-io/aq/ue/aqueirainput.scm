(define-module (crates-io aq ue aqueirainput) #:use-module (crates-io))

(define-public crate-AqueiraInput-0.0.1 (c (n "AqueiraInput") (v "0.0.1") (d (list (d (n "hyper") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s5lm77rdw0abhfzbwqpgm6i3ip1sy13vmmr520qzlrcv94wk44m") (y #t)))

