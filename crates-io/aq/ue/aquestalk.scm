(define-module (crates-io aq ue aquestalk) #:use-module (crates-io))

(define-public crate-aquestalk-0.1.0 (c (n "aquestalk") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0nnncfv64y0v5ya2dkf59gdcz647ncyqwrcpgljb0g95c6r6mcyy") (y #t)))

(define-public crate-aquestalk-0.1.1 (c (n "aquestalk") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "0k4v4gzs5w9vlvbvpgi764rwjdjyb78wl6az32br3drzcsy9xvcd")))

(define-public crate-aquestalk-0.1.2 (c (n "aquestalk") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1wf30m25hnqsx5hx9xi8qhs91s8j279pg233bsw495wriyrs0c11")))

(define-public crate-aquestalk-1.0.0 (c (n "aquestalk") (v "1.0.0") (d (list (d (n "libaquestalk-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "13mcd33b7rp3ng8kk3sb9qzc4gbyridzaqlbw8d69lah1b5c7nqq") (f (quote (("aquestalk1" "libaquestalk-sys")))) (y #t)))

(define-public crate-aquestalk-1.0.0-alpha (c (n "aquestalk") (v "1.0.0-alpha") (d (list (d (n "libaquestalk-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1pyjsxfbhpkypv6vgxxrwiilyvg3ri0m8ryzi9n52b6c8mz2y67d") (f (quote (("aquestalk1" "libaquestalk-sys"))))))

