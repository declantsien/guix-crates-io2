(define-module (crates-io aq -c aq-cli) #:use-module (crates-io))

(define-public crate-aq-cli-0.1.0 (c (n "aq-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1z823w6885x5w22cfnaknz46dvkswyq26g2afzprmg3cr3hri67r")))

(define-public crate-aq-cli-0.2.0 (c (n "aq-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0x92immzvpgccdmj5dwnbyp90rk8qsvjyani1v04sf0295j74fzf")))

(define-public crate-aq-cli-0.3.0 (c (n "aq-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0hs81v5xscp355q5g1cl672kydlk8gmr5sg755805866m6fk2h0g")))

