(define-module (crates-io aq -n aq-ng) #:use-module (crates-io))

(define-public crate-aq-ng-0.1.4 (c (n "aq-ng") (v "0.1.4") (d (list (d (n "accessibility-ng") (r "^0.1.6") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zx03dvy8zipsnczp6z0h4w5qrlkc6a1l99kzzgixrji9wzr7pl1")))

