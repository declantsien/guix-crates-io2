(define-module (crates-io aq ua aqua_state) #:use-module (crates-io))

(define-public crate-aqua_state-0.2.0 (c (n "aqua_state") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1c0ybcglrc5092z3c6dbjv0x7kl1jdljd0nxm2wzwsk7yppxs605") (y #t)))

(define-public crate-aqua_state-0.3.0 (c (n "aqua_state") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "13xha3l2mwxh9qs0zb6jzqi56hbdkdqcd343g6qdvpanlmxfxhy2") (y #t)))

(define-public crate-aqua_state-0.4.0 (c (n "aqua_state") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0s5fd2hkvr187ggfdm1lg10fn4b8icx5gs0ph8k2kv1jlgwmhayc") (y #t)))

(define-public crate-aqua_state-0.5.0 (c (n "aqua_state") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "10z4b5x2khbh9rw9py2jy0s753x092ckxja69qy606f1kz7hibbn") (y #t)))

(define-public crate-aqua_state-0.6.0 (c (n "aqua_state") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "078m05717wqw4chggx95n5zziwchmm5lsha6lxs60yadk3z69pk3") (y #t)))

