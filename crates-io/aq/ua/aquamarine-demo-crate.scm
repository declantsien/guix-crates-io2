(define-module (crates-io aq ua aquamarine-demo-crate) #:use-module (crates-io))

(define-public crate-aquamarine-demo-crate-0.1.0 (c (n "aquamarine-demo-crate") (v "0.1.0") (d (list (d (n "aquamarine") (r "^0.1.0") (d #t) (k 0)))) (h "05mkaaddg2p698s9wl40mmjkfdzvkslxm8hb2mirwjxri59l5xf3")))

(define-public crate-aquamarine-demo-crate-0.1.1 (c (n "aquamarine-demo-crate") (v "0.1.1") (d (list (d (n "aquamarine") (r "^0.1.1") (d #t) (k 0)))) (h "01sqmyjw4sfgb96sppvbav7694b3c7g4iklnks25z1m2b2iq0pjx")))

(define-public crate-aquamarine-demo-crate-0.1.2 (c (n "aquamarine-demo-crate") (v "0.1.2") (d (list (d (n "aquamarine") (r "^0.1.2") (d #t) (k 0)))) (h "1xrw8ql94dwhrcr4blzs8f2qam3ifcihfsr0xa6plchj6k61qiy7")))

(define-public crate-aquamarine-demo-crate-0.1.4 (c (n "aquamarine-demo-crate") (v "0.1.4") (d (list (d (n "aquamarine") (r "^0.1.4") (d #t) (k 0)))) (h "1l20fy9p64y4b6bby7j64knxv0l5rx4g1nvkbxpy0s261r1bl9xi") (y #t)))

(define-public crate-aquamarine-demo-crate-0.1.5 (c (n "aquamarine-demo-crate") (v "0.1.5") (d (list (d (n "aquamarine") (r "^0.1.5") (d #t) (k 0)))) (h "03sxkaaq6mr0i5vkwnp6hsai2x968lfl435jbzs2xdi15jbn2zb7")))

(define-public crate-aquamarine-demo-crate-0.1.6 (c (n "aquamarine-demo-crate") (v "0.1.6") (d (list (d (n "aquamarine") (r "^0.1.6") (d #t) (k 0)))) (h "1a8qdl83ckmmg0kydxjn0hy8dlf21y9j7xqag4xzp432grrs0k4a")))

(define-public crate-aquamarine-demo-crate-0.1.7 (c (n "aquamarine-demo-crate") (v "0.1.7") (d (list (d (n "aquamarine") (r "^0.1.7") (d #t) (k 0)))) (h "0i254r92ippc006z50dds5q6irlkh11b1c8ab361ggv52qd1sdfy") (y #t)))

(define-public crate-aquamarine-demo-crate-0.1.8 (c (n "aquamarine-demo-crate") (v "0.1.8") (d (list (d (n "aquamarine") (r "^0.1.8") (d #t) (k 0)))) (h "15amlal8mhygv0gwsmcsj87vs8hxh4l0nwd8ly54zny9lxsp8byq")))

(define-public crate-aquamarine-demo-crate-0.1.9 (c (n "aquamarine-demo-crate") (v "0.1.9") (d (list (d (n "aquamarine") (r "^0.1.9") (d #t) (k 0)))) (h "1hp0w5g8brwiqm1jy0s1sckp7ifn13a4srqrjcb03rmx9bh4iv0c")))

(define-public crate-aquamarine-demo-crate-0.1.10 (c (n "aquamarine-demo-crate") (v "0.1.10") (d (list (d (n "aquamarine") (r "^0.1.10") (d #t) (k 0)))) (h "1xnndpsi3hak417npqmkgivj0shj8c9hv9dmiq6ja3kcs7mkhadf")))

(define-public crate-aquamarine-demo-crate-0.1.11 (c (n "aquamarine-demo-crate") (v "0.1.11") (d (list (d (n "aquamarine") (r "^0.1.11") (d #t) (k 0)))) (h "1cqnpx6v8k956k4vbrhvg79w9r1kg543yzvnzsk3d0lkq3k2y52f")))

(define-public crate-aquamarine-demo-crate-0.1.12 (c (n "aquamarine-demo-crate") (v "0.1.12") (d (list (d (n "aquamarine") (r "^0.1.12") (d #t) (k 0)))) (h "0f25rlgci9339a2a47fjxzgb6icwh1ixppa5f6hy37ia7xnkb092")))

(define-public crate-aquamarine-demo-crate-0.2.0 (c (n "aquamarine-demo-crate") (v "0.2.0") (d (list (d (n "aquamarine") (r "^0.2.0") (d #t) (k 0)))) (h "1dawn6qnzh4qrqfhn919c62sg7v0iph3zamnhw7w2mc17cgwvp3j")))

(define-public crate-aquamarine-demo-crate-0.2.1 (c (n "aquamarine-demo-crate") (v "0.2.1") (d (list (d (n "aquamarine") (r "^0.2.1") (d #t) (k 0)))) (h "1wprygcrw9n5cfw82ymcfd5pparh0dgpx7xmnq5balwxms43ckyl")))

(define-public crate-aquamarine-demo-crate-0.2.2 (c (n "aquamarine-demo-crate") (v "0.2.2") (d (list (d (n "aquamarine") (r "^0.2.2") (d #t) (k 0)))) (h "0gz5kckj61hbhf18qyqgkdgia6xgyj0l7glrjpbw28773x0lp2bc")))

(define-public crate-aquamarine-demo-crate-0.3.0 (c (n "aquamarine-demo-crate") (v "0.3.0") (d (list (d (n "aquamarine") (r "^0.3.0") (d #t) (k 0)))) (h "015ra9ipz1xh39v0py8l30qwx5i0hqgnvm65r5sdk26v8h70gw49")))

(define-public crate-aquamarine-demo-crate-0.3.1 (c (n "aquamarine-demo-crate") (v "0.3.1") (d (list (d (n "aquamarine") (r "^0.3.1") (d #t) (k 0)))) (h "1119iggiv8668m08cl2hqkdnm3middiqa99cp5b0sww54lwb4vyz")))

(define-public crate-aquamarine-demo-crate-0.3.1-1 (c (n "aquamarine-demo-crate") (v "0.3.1-1") (d (list (d (n "aquamarine") (r "^0.3.1") (d #t) (k 0)))) (h "0s0x8fhcdcmx4rnjfzjilvzzbfqnr73p8rx7qas3zmr5zfjh2y1m")))

(define-public crate-aquamarine-demo-crate-0.3.2 (c (n "aquamarine-demo-crate") (v "0.3.2") (d (list (d (n "aquamarine") (r "^0.3.1") (d #t) (k 0)))) (h "1nkmap71pnqqwz0a7ddlak526j05shwsy5c4pvkrsimdgbfpcdr2")))

(define-public crate-aquamarine-demo-crate-0.3.3 (c (n "aquamarine-demo-crate") (v "0.3.3") (d (list (d (n "aquamarine") (r "^0.3.1") (d #t) (k 0)))) (h "1rksmv8dp4gild917lklj3nfl03iwj6ng67nalmvxzw0vlmk6bji")))

(define-public crate-aquamarine-demo-crate-0.4.0 (c (n "aquamarine-demo-crate") (v "0.4.0") (d (list (d (n "aquamarine") (r "^0.4.0") (d #t) (k 0)))) (h "0dmirxddprxvp6z9vv4mzlbd1w53yl3a4qkxjgrlngrvizii2n2f") (y #t)))

(define-public crate-aquamarine-demo-crate-0.5.0 (c (n "aquamarine-demo-crate") (v "0.5.0") (d (list (d (n "aquamarine") (r "^0.5.0") (d #t) (k 0)))) (h "1yyzryn4msbkdb4wcssgdbyqffn5dav8a5zdgywpxwsag801n5rd")))

