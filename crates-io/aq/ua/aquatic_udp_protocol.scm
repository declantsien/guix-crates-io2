(define-module (crates-io aq ua aquatic_udp_protocol) #:use-module (crates-io))

(define-public crate-aquatic_udp_protocol-0.1.0 (c (n "aquatic_udp_protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0xa1mp99i3y10jqzaja8yaf1l366killgg6z5q276l5w4iih5hw4")))

(define-public crate-aquatic_udp_protocol-0.2.0 (c (n "aquatic_udp_protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0zwl7y8ncg1brvmsnapkrjxp729axsl91k9plqvv6hjdj8kry50n")))

(define-public crate-aquatic_udp_protocol-0.8.0 (c (n "aquatic_udp_protocol") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0hbkfa9cbd94ysc0ixf3if9bgdnip9idl93m4h6x5xr10549p4dj") (r "1.64")))

(define-public crate-aquatic_udp_protocol-0.9.0 (c (n "aquatic_udp_protocol") (v "0.9.0") (d (list (d (n "aquatic_peer_id") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "17iqi5fbbm5d5rlzim8j4prpk9sjvh48y4i46pivvz7mc98hxy8a") (r "1.64")))

