(define-module (crates-io aq ua aqua-ipfs-distro) #:use-module (crates-io))

(define-public crate-aqua-ipfs-distro-0.5.11 (c (n "aqua-ipfs-distro") (v "0.5.11") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "166jfslj009919036x7qxp6sq1fvxgixj5yl0fqx5w23ihh4k8bh")))

(define-public crate-aqua-ipfs-distro-0.5.13 (c (n "aqua-ipfs-distro") (v "0.5.13") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "09jrxbkjpvm8nlw1qihrmp6hzdr19l9fbayy6i2fyiizrbqfcmjr")))

(define-public crate-aqua-ipfs-distro-0.5.14 (c (n "aqua-ipfs-distro") (v "0.5.14") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1p01258rdib4nxdq2xky30l6yf4p3rixpaknd0c1ji82mr95yzrf")))

(define-public crate-aqua-ipfs-distro-0.5.17 (c (n "aqua-ipfs-distro") (v "0.5.17") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "00qdnpqw1vwpc44vqsnkcaskabkiaahmgsyzmg6b8czlf0jz5asb")))

(define-public crate-aqua-ipfs-distro-0.5.20 (c (n "aqua-ipfs-distro") (v "0.5.20") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "037swnn9wkxqfh846q4q9y9vb4njf6p950ihkl15agag0ac845v3")))

(define-public crate-aqua-ipfs-distro-0.5.24 (c (n "aqua-ipfs-distro") (v "0.5.24") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0ggrbxmi2pxh9anpq8ckdb89h0v6dkjqfv4s52aqm2sxdsmfwww6")))

(define-public crate-aqua-ipfs-distro-0.5.26 (c (n "aqua-ipfs-distro") (v "0.5.26") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0kgkx648njakbfa9sw25h4f0n0k2k0rp1imfj7qf7jh4zblhgrny")))

(define-public crate-aqua-ipfs-distro-0.5.27 (c (n "aqua-ipfs-distro") (v "0.5.27") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1k61rj6ax7cv8p46m27rv14j5l1w4fn44vp7171dm98dhfcq8mqd")))

(define-public crate-aqua-ipfs-distro-0.5.28 (c (n "aqua-ipfs-distro") (v "0.5.28") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0aw415gssgpli2hqcg0853pmsdj70k5sq6vh8bz82x0lnck7gjra")))

(define-public crate-aqua-ipfs-distro-0.5.29 (c (n "aqua-ipfs-distro") (v "0.5.29") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0jyi0zvjqjsriqj5kjg6nl9fqrngpxdpvx3apxfjf4l0lcpqfsr6")))

(define-public crate-aqua-ipfs-distro-0.5.30 (c (n "aqua-ipfs-distro") (v "0.5.30") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0pskpfbckdlr8cs8dlcb60wfiw8v1kaa3v3x9bq2lnz9sshyc4m6")))

(define-public crate-aqua-ipfs-distro-0.5.31 (c (n "aqua-ipfs-distro") (v "0.5.31") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1wkscisiz8nnk4lmy4hc6f2byyyczr2hjpz0nsmmp6kp8swmq02l")))

(define-public crate-aqua-ipfs-distro-0.6.0 (c (n "aqua-ipfs-distro") (v "0.6.0") (d (list (d (n "built") (r "^0.7.0") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "07xj3ggigrcg2mvw0s8nynp2pjvvy7sx06id1rks398ysc6mp2rv")))

