(define-module (crates-io aq ua aqua_error) #:use-module (crates-io))

(define-public crate-aqua_error-0.1.0 (c (n "aqua_error") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "19gskki9qv1cpm80r37mzvya2w0x5i90pfnl3fzhscgf1pcc593z")))

(define-public crate-aqua_error-0.2.0 (c (n "aqua_error") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0r5m5dy2ixqhgnvh4qngyssv8hc3867h7350rxpi23vfgakrrfb8")))

(define-public crate-aqua_error-0.4.0 (c (n "aqua_error") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0fv13az550gh9ckxbapmfc3b6plfdmmvyfivmyfm9v8rr8vhhsdm")))

(define-public crate-aqua_error-0.5.0 (c (n "aqua_error") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1qd1ni2z9q76l24c0rwcdi8jf2hz3709rx2y9izkk4qvhw9snbq8")))

(define-public crate-aqua_error-0.6.0 (c (n "aqua_error") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0il5yciarjf41mi8k9jx37s4z1nmlf5yxjgqmvzzslvdwidzwyzm")))

(define-public crate-aqua_error-0.7.0 (c (n "aqua_error") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0l9qc0qw1smifk04v68yac9fpdj8by6yn85kmmhsdbxdrgxyd6c4")))

(define-public crate-aqua_error-0.8.0 (c (n "aqua_error") (v "0.8.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1ixcmszim8ym8wnqmcw2dy3a25lqlbz3smph71v2c63f101z63cf")))

(define-public crate-aqua_error-0.9.0 (c (n "aqua_error") (v "0.9.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0fhrjvpfjn23dlxhl0ccbpkg5xhz930ib00plw0kadqvymj61fcf")))

(define-public crate-aqua_error-0.12.0 (c (n "aqua_error") (v "0.12.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1yznivb3bkrnw2zrz1iw9jch18jh3a65n5r9dv6jhx72k37w6rj4")))

(define-public crate-aqua_error-0.13.0 (c (n "aqua_error") (v "0.13.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1fryplv7fyd153fzhkcwpkg9fqirvhwpv124zw6944623h6vzrb8")))

(define-public crate-aqua_error-0.14.0 (c (n "aqua_error") (v "0.14.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "03xh23kvdk9prq71ifbd6ib2jcnqqm7hvsprxpsmmf2a4an2cwpz")))

(define-public crate-aqua_error-0.15.0 (c (n "aqua_error") (v "0.15.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1asmfmb63xrhnaa4nnax1k28467159q78j7rnpn9gf5jzfwrfi84")))

(define-public crate-aqua_error-0.16.0 (c (n "aqua_error") (v "0.16.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1kxs5znmj3yhyxnknxggz54xq5a59al1wd8s3gq08059y2slxbrl")))

(define-public crate-aqua_error-0.17.0 (c (n "aqua_error") (v "0.17.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1mlxb5dgyzvihq473jqf034bshc4awsfs2szihh36qd430cfb4hc")))

(define-public crate-aqua_error-0.18.0 (c (n "aqua_error") (v "0.18.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0jws06yh1mdp793ys9wrgaygfa9drca4ha8gka04xppv6nx43c19")))

(define-public crate-aqua_error-0.20.0 (c (n "aqua_error") (v "0.20.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0zhzy3n56r5ndb5bi9h6f2gcyj66s7h9kqanqkyispvww59kih8d")))

(define-public crate-aqua_error-0.21.0 (c (n "aqua_error") (v "0.21.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0gr2smn37nciz4y16q3xn2wn3jsa7w6jmpnyfpzi0r85pl307a16")))

