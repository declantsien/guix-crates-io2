(define-module (crates-io aq ua aquatic_toml_config) #:use-module (crates-io))

(define-public crate-aquatic_toml_config-0.2.0 (c (n "aquatic_toml_config") (v "0.2.0") (d (list (d (n "aquatic_toml_config_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02dbkm9yyc13x0sjjbfwflx1lb5q6pnjlm8mma4lbrm57cwv6brg")))

(define-public crate-aquatic_toml_config-0.8.0 (c (n "aquatic_toml_config") (v "0.8.0") (d (list (d (n "aquatic_toml_config_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0mh6854xcdi4jcmrcjix2l3jv8skp2bfhmqj862685lcwjbb16h4") (r "1.64")))

(define-public crate-aquatic_toml_config-0.9.0 (c (n "aquatic_toml_config") (v "0.9.0") (d (list (d (n "aquatic_toml_config_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1a595dh5ah0fgizkqqjd30gr6yd1sp9amvddrsw9p1flbvgikqv7") (r "1.64")))

