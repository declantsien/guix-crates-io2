(define-module (crates-io aq ua aquatic_peer_id) #:use-module (crates-io))

(define-public crate-aquatic_peer_id-0.9.0 (c (n "aquatic_peer_id") (v "0.9.0") (d (list (d (n "compact_str") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "09bgq266hvs6lx96cwb9yram7f2pmzmwcj9qf4jwn792vxrjlwzh") (f (quote (("default" "quickcheck")))) (r "1.64")))

