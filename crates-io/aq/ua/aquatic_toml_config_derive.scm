(define-module (crates-io aq ua aquatic_toml_config_derive) #:use-module (crates-io))

(define-public crate-aquatic_toml_config_derive-0.2.0 (c (n "aquatic_toml_config_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kqambzyr4wfzbigmdbx7vi0v33v2v9kdi26dqzfz2nk835ylzbl")))

(define-public crate-aquatic_toml_config_derive-0.8.0 (c (n "aquatic_toml_config_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1chalpbwigxh427060ag46300l4l0miwv3xp0r0im1p4svc50gca") (r "1.64")))

(define-public crate-aquatic_toml_config_derive-0.9.0 (c (n "aquatic_toml_config_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c378dagmz1djgvxbllda30hr33cfy8p6qz2wymgpsz6ibg7ciga") (r "1.64")))

