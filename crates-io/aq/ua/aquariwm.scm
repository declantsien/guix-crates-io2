(define-module (crates-io aq ua aquariwm) #:use-module (crates-io))

(define-public crate-aquariwm-0.1.0 (c (n "aquariwm") (v "0.1.0") (h "0lna6zhnir5a2qbhkb2yw001lvcly7ckq6ykz307kjn9pyd8hjlm") (y #t)))

(define-public crate-aquariwm-0.0.0 (c (n "aquariwm") (v "0.0.0") (h "12rd4kjrh4prnxxc569cchsdam6dqy6i4m4fgi106jm28hpf094z")))

