(define-module (crates-io aq ua aqua-ipfs-distro-test) #:use-module (crates-io))

(define-public crate-aqua-ipfs-distro-test-0.5.12 (c (n "aqua-ipfs-distro-test") (v "0.5.12") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0wql03faa6886jiwnw778az1wpls8qi5qa66vm5rvk16a27pmaxw")))

(define-public crate-aqua-ipfs-distro-test-0.5.12-1 (c (n "aqua-ipfs-distro-test") (v "0.5.12-1") (d (list (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0pagi7w7n0b0sbb6zr0b40f77ymviwlpkqlifbjaq8766aqr55g8")))

