(define-module (crates-io aq ua aquatic_cli_helpers) #:use-module (crates-io))

(define-public crate-aquatic_cli_helpers-0.1.0 (c (n "aquatic_cli_helpers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hbnx7kdayxljm146hm7g4zjicbhvf3lim425rkfv9im8243mi21")))

(define-public crate-aquatic_cli_helpers-0.2.0 (c (n "aquatic_cli_helpers") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "aquatic_toml_config") (r "^0.2.0") (d #t) (k 0)) (d (n "git-testament") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^2") (f (quote ("stderr"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0021vm4fdpcnccdhqgbq9vygg9w0lisf8k18wlgcdd6phgvn8rjb")))

