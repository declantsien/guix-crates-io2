(define-module (crates-io aq ua aqua-interpreter-interface) #:use-module (crates-io))

(define-public crate-aqua-interpreter-interface-0.3.0 (c (n "aqua-interpreter-interface") (v "0.3.0") (d (list (d (n "fluence") (r "^0.2.18") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "0jhc2g4jxj19dgrn6vyn3zfp526ncllghcxip76hy8xj815phws5")))

(define-public crate-aqua-interpreter-interface-0.3.1 (c (n "aqua-interpreter-interface") (v "0.3.1") (d (list (d (n "fluence") (r "^0.2.18") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "0y1jl60502qjjbb8i1g4hf87sv3h2g5hkgl1d6f6lcz1by59pi7l")))

(define-public crate-aqua-interpreter-interface-0.4.0 (c (n "aqua-interpreter-interface") (v "0.4.0") (d (list (d (n "fluence") (r "^0.4.2") (f (quote ("logger"))) (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "1da2i5n7lmc4izaskb5dfga05mlf3cl9b61im4xahfssd88wiy1w")))

(define-public crate-aqua-interpreter-interface-0.5.0 (c (n "aqua-interpreter-interface") (v "0.5.0") (d (list (d (n "fluence") (r "^0.6.1") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "0lmcyb4pkf2yngap3jwq8dqclfmxr4jimbgwpvxi3akbv57c9bnx")))

