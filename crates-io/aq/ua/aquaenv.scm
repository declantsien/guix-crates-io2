(define-module (crates-io aq ua aquaenv) #:use-module (crates-io))

(define-public crate-aquaenv-0.1.0 (c (n "aquaenv") (v "0.1.0") (d (list (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "plist") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1rxslssayc9n1y2nh5ym1bi1mkd9ga28hhh3y6w2jlpp8xq1vrf7")))

(define-public crate-aquaenv-0.1.1 (c (n "aquaenv") (v "0.1.1") (d (list (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "plist") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00nkjpqzbx9vz0zhj4zb9grn6prlqspms0vlc22nl39lpkqsp8z8") (y #t)))

(define-public crate-aquaenv-0.1.2 (c (n "aquaenv") (v "0.1.2") (d (list (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "plist") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1w5iwcdm1ls51ipjlvv9fifwv346mfmyck7lsan7na5b4ngdbnh5")))

(define-public crate-aquaenv-0.1.3 (c (n "aquaenv") (v "0.1.3") (d (list (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "plist") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1pf3ynq34zcavjsx60jlhgrm7qxwg4wbcfqbcbn32ny9lw5d1gp9")))

