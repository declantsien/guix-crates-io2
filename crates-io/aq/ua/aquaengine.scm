(define-module (crates-io aq ua aquaengine) #:use-module (crates-io))

(define-public crate-aquaengine-0.0.1 (c (n "aquaengine") (v "0.0.1") (d (list (d (n "clippy") (r "^0") (d #t) (k 2)))) (h "1rwskq3af4s3i258n10z3v4prdj5r833aw0h9khqlpslb14f585i") (y #t)))

(define-public crate-aquaengine-0.0.2 (c (n "aquaengine") (v "0.0.2") (d (list (d (n "clippy") (r "^0") (d #t) (k 2)))) (h "1c6qgvn61mxp5fh02a7f9vmpx74zz8zcfm8mwbnl80jpjz05fq3m")))

