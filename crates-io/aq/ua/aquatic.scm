(define-module (crates-io aq ua aquatic) #:use-module (crates-io))

(define-public crate-aquatic-0.1.0 (c (n "aquatic") (v "0.1.0") (d (list (d (n "aquatic_cli_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "aquatic_http") (r "^0.1.0") (d #t) (k 0)) (d (n "aquatic_udp") (r "^0.1.0") (d #t) (k 0)) (d (n "aquatic_ws") (r "^0.1.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)))) (h "1qgcc80v8n21c8wf3477dhpy2f6q2wzx0l15pmvnq32z21swg1ar")))

(define-public crate-aquatic-0.2.0 (c (n "aquatic") (v "0.2.0") (d (list (d (n "aquatic_cli_helpers") (r "^0.2.0") (d #t) (k 0)) (d (n "aquatic_http") (r "^0.2.0") (d #t) (k 0)) (d (n "aquatic_udp") (r "^0.2.0") (d #t) (k 0)) (d (n "aquatic_ws") (r "^0.2.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)))) (h "03c3j30m2pbg45gdbi59z5gfyy39d1yp8xs1dn25i1lwhjrsflzs")))

