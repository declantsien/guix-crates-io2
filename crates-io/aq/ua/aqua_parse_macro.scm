(define-module (crates-io aq ua aqua_parse_macro) #:use-module (crates-io))

(define-public crate-aqua_parse_macro-0.11.0 (c (n "aqua_parse_macro") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q4zpnp6vrpsplc1aagixsczqzb245pbafpv7yymxxdrf96lsyyp")))

(define-public crate-aqua_parse_macro-0.12.0 (c (n "aqua_parse_macro") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1alg5f8bw7w8zs7al01cllnjgg116cr18hkwjgh7qjnakcykx02k")))

(define-public crate-aqua_parse_macro-0.13.0 (c (n "aqua_parse_macro") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0141c3cxykaqqfsy99d4vncr3qkml4jhnbf6wq5rp17m8rcqchri")))

(define-public crate-aqua_parse_macro-0.14.0 (c (n "aqua_parse_macro") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wy0xkvpqh8wnqzrr93ygpc9pk8wab3s4x4yad3vg670bflnany9")))

(define-public crate-aqua_parse_macro-0.16.0 (c (n "aqua_parse_macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a2wf9950k8jaibmm54229w472znyggazfwqraw4ckj8jbf5q4qm")))

