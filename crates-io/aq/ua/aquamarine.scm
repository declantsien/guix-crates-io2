(define-module (crates-io aq ua aquamarine) #:use-module (crates-io))

(define-public crate-aquamarine-0.0.0 (c (n "aquamarine") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ix052gnajal04kav0532n0nqwainpkanpl0qcaibr44nsdhddfn") (y #t)))

(define-public crate-aquamarine-0.1.0 (c (n "aquamarine") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0a03g5dq0as1i01447y94xmzg5hpj0wi7m99akkz3niplbkfx0gg")))

(define-public crate-aquamarine-0.1.1 (c (n "aquamarine") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wdsip2jm6blq9m8a8qr41invgrjq11hlx0v6p3av61xvzf9ygl8")))

(define-public crate-aquamarine-0.1.2 (c (n "aquamarine") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b6shv38972c6wpsb8w4wpzxx9pirsjaa7n4ls6wlbh2qkhc1z4m")))

(define-public crate-aquamarine-0.1.3 (c (n "aquamarine") (v "0.1.3") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03s3q6s6cl81z2p3z8s598kp4d4rnsnb19d1ba0riirq79i99q14")))

(define-public crate-aquamarine-0.1.4 (c (n "aquamarine") (v "0.1.4") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i8py74bwjjgzi7df6cz32kgcqzk1zkbh2hsnw2l4xn34hj42gf3")))

(define-public crate-aquamarine-0.1.5 (c (n "aquamarine") (v "0.1.5") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i8d7dv1hv8pbr2hxzs548p8qz4kjb9gs9nlb7k0fkjwi4pl91fc")))

(define-public crate-aquamarine-0.1.6 (c (n "aquamarine") (v "0.1.6") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fa434kkwgxdwfm6bqjn5r1fymn552lf9brxn166z0v2484w050r")))

(define-public crate-aquamarine-0.1.7 (c (n "aquamarine") (v "0.1.7") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1y7r4fj78axnv56pi29blzii2j0p0k1d412mmgcbhj2cba9a8zsy") (y #t)))

(define-public crate-aquamarine-0.1.8 (c (n "aquamarine") (v "0.1.8") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15whcf6qxq5l0cyaqhwk07by4pb8nhmfhkg307wffxjz030avc92")))

(define-public crate-aquamarine-0.1.9 (c (n "aquamarine") (v "0.1.9") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i35f5qgcrj6p9dz8fmmdszq0yv9z5c2bw9k1qmwd0zrc9lzykyl")))

(define-public crate-aquamarine-0.1.10 (c (n "aquamarine") (v "0.1.10") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1aghp2kl3rin6d5bfhnk7ci9rdfi1niqns5i1fbxigxni9k72sbr")))

(define-public crate-aquamarine-0.1.11 (c (n "aquamarine") (v "0.1.11") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gnzxzzv5klvjfrbbdxcbv00ydf7hlcrh6a2db94b2qwlnr4rqcn")))

(define-public crate-aquamarine-0.1.12 (c (n "aquamarine") (v "0.1.12") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gy45b85y4aj98gyj3k2ncbjfbf4y61mjfr477m8x3j712bw6hd9")))

(define-public crate-aquamarine-0.2.0 (c (n "aquamarine") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g75xkw5vlxgzc2p6mq8s4jy8gf9aibra5wr6m6yk3c11z6h11rm")))

(define-public crate-aquamarine-0.2.1 (c (n "aquamarine") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yis9h2d5czv7anxqslyzxkyhs6x4j3fjfv8frpilxrz0ydvlz0x")))

(define-public crate-aquamarine-0.2.2 (c (n "aquamarine") (v "0.2.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03204ric232d8x7291in894sp0w2mmp50qgc8fy57x3pvpq11wsv")))

(define-public crate-aquamarine-0.3.0 (c (n "aquamarine") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z93xidwdjc5lhzirlhqxkbdxiz599wjzfgjivcwks8jvfjri7bm")))

(define-public crate-aquamarine-0.3.1 (c (n "aquamarine") (v "0.3.1") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jcisp1aa3xzdmk3r5i4wdmxjc0f8rmlw9p895a74gciaq4rzmyq")))

(define-public crate-aquamarine-0.3.2 (c (n "aquamarine") (v "0.3.2") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18fivdzpsfg5hl7hrsij8z8aw9w2pj3wa7xzqwchgscwqi9jjxfz")))

(define-public crate-aquamarine-0.3.3 (c (n "aquamarine") (v "0.3.3") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0q37asf6pirrjjjjr1k6z70gmcpb6fc51cgahvbn744zpamh5nni")))

(define-public crate-aquamarine-0.4.0 (c (n "aquamarine") (v "0.4.0") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1vwl4mpfy8as6ri2c55fh1rdg9965nd0b3ynjk7bch029p8q0jq7")))

(define-public crate-aquamarine-0.5.0 (c (n "aquamarine") (v "0.5.0") (d (list (d (n "include_dir") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0kizryj8h9zgwrb0q5q5f1c6bg56gnbg19wan5g06icj6141bk11")))

