(define-module (crates-io aq ua aquascope_workspace_utils) #:use-module (crates-io))

(define-public crate-aquascope_workspace_utils-0.1.0 (c (n "aquascope_workspace_utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gzkwfy68c7y3kppag0aab9xjyb2n4bqbkjhyisqzgfracbmd39b")))

(define-public crate-aquascope_workspace_utils-0.1.1 (c (n "aquascope_workspace_utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gnn888bd9l1qafjgmlp7pr54jr719vkmllh77hib5mchylcaprk")))

(define-public crate-aquascope_workspace_utils-0.1.2 (c (n "aquascope_workspace_utils") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05s03vvw2wxaqza2pjqfn63y1cx7xk42aqv1shjb7prjklsrjvn6")))

(define-public crate-aquascope_workspace_utils-0.1.3 (c (n "aquascope_workspace_utils") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dzks04qjhcri9hagmxvcsc9pyhan9z3ds6hj6xw52044rz5jf6a")))

(define-public crate-aquascope_workspace_utils-0.1.4 (c (n "aquascope_workspace_utils") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13dhc0xpc1kvny708fj4m7c2l1csc99cllfpaf88z3kcd6667pc4")))

(define-public crate-aquascope_workspace_utils-0.2.0 (c (n "aquascope_workspace_utils") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0z0ykig8avanchdy09k1snpsy4yd49ic3ap5xyp7avh7xjg6f7ff")))

(define-public crate-aquascope_workspace_utils-0.3.0 (c (n "aquascope_workspace_utils") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "10ss4grblv2ra9dp3hi12kjwyfmgd7s1scfqw24j5ffj39724v6m")))

(define-public crate-aquascope_workspace_utils-0.3.1 (c (n "aquascope_workspace_utils") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0c5rqjmvvacqlbh6mm0rc4i28ff3b1rl2dz68x54k0syc5f7m170")))

(define-public crate-aquascope_workspace_utils-0.3.2 (c (n "aquascope_workspace_utils") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0l7c2dwwbk1pr8xwy9mcxsdxpl8hx3s90qm07vgw8y1ybr5xi5s0")))

