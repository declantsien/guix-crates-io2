(define-module (crates-io aq ua aqua_parse) #:use-module (crates-io))

(define-public crate-aqua_parse-0.21.0 (c (n "aqua_parse") (v "0.21.0") (d (list (d (n "aqua_error") (r "^0.1.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "147qcm8fs5d8cb8yap9aq3s8hd8b92anxhymrm7945glijd4cn7k") (f (quote (("pattern"))))))

(define-public crate-aqua_parse-0.22.0 (c (n "aqua_parse") (v "0.22.0") (d (list (d (n "aqua_error") (r "^0.1.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1g1wfvlf2p48v1g7309kh6h0ihllyw3k9a4svr3m1i44wnpqqqnm") (f (quote (("pattern"))))))

(define-public crate-aqua_parse-0.23.0 (c (n "aqua_parse") (v "0.23.0") (d (list (d (n "aqua_error") (r "^0.1.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1mrkzvyvip9vbjq5ljmrsw14k5r5jl57ryymvq9f0isxzljghwd3") (f (quote (("pattern"))))))

(define-public crate-aqua_parse-0.24.0 (c (n "aqua_parse") (v "0.24.0") (d (list (d (n "aqua_error") (r "^0.2.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0jzlf5x0gvfz1sjsv4n4ajvv9i920gb677mvdrxj2c0h3pbrgxdj") (f (quote (("pattern"))))))

(define-public crate-aqua_parse-0.25.0 (c (n "aqua_parse") (v "0.25.0") (d (list (d (n "aqua_error") (r "^0.2.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.14.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "15dh43my80myl68cr9db976jfw9kwg8chxf1b77k0ynsmddn7y0r") (f (quote (("pattern"))))))

(define-public crate-aqua_parse-0.27.0 (c (n "aqua_parse") (v "0.27.0") (d (list (d (n "aqua_error") (r "^0.17.0") (d #t) (k 0)) (d (n "aqua_parse_macro") (r "^0.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "14nizncqr36kjfy59jia8xq1jwlgk4a2qs07xkl22l4qfyzdyhv8") (f (quote (("pattern"))))))

