(define-module (crates-io vf ox vfox) #:use-module (crates-io))

(define-public crate-vfox-0.1.0-alpha.0 (c (n "vfox") (v "0.1.0-alpha.0") (d (list (d (n "mlua") (r "^0.9") (f (quote ("lua54" "vendored"))) (d #t) (k 0)))) (h "0zwxx03qli0xd3bpsnp93v4cqhyrqdchq39f7zk6hqzs560s8llh")))

(define-public crate-vfox-0.1.0-alpha.1 (c (n "vfox") (v "0.1.0-alpha.1") (d (list (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "mlua") (r "^0.9") (f (quote ("async" "lua54" "macros" "serialize" "vendored"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0mmwrsfh5gcymj9w006aa29i8zn1g5y9zyhyf0f3xr199dbqrihz")))

