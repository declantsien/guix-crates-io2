(define-module (crates-io vf io vfio-ioctls) #:use-module (crates-io))

(define-public crate-vfio-ioctls-0.1.0 (c (n "vfio-ioctls") (v "0.1.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 0)) (d (n "kvm-bindings") (r "~0") (o #t) (d #t) (k 0)) (d (n "kvm-ioctls") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0") (d #t) (k 0)) (d (n "vfio-bindings") (r "~0") (d #t) (k 0)) (d (n "vm-memory") (r ">=0.6") (f (quote ("backend-mmap"))) (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "1i1aw85a71p2i4k995iwz8lsns4iia6lma6iq7nahr11hiwar2q6") (f (quote (("kvm" "kvm-ioctls" "kvm-bindings") ("default" "kvm"))))))

