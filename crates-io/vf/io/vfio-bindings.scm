(define-module (crates-io vf io vfio-bindings) #:use-module (crates-io))

(define-public crate-vfio-bindings-0.1.0 (c (n "vfio-bindings") (v "0.1.0") (d (list (d (n "vmm-sys-util") (r "^0.1.1") (d #t) (k 0)))) (h "0rmgmgia7g9vy685nxx51x1j3fba95syxlkdcg1cldfvmsmpj0yz") (f (quote (("vfio-v5_0_0"))))))

(define-public crate-vfio-bindings-0.2.0 (c (n "vfio-bindings") (v "0.2.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.2.0") (o #t) (d #t) (k 0)))) (h "19q4hvhg919ppa97lsldsz9393jvz63qq4zviid7z8xxy93ga8aa") (f (quote (("vfio-v5_0_0") ("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-vfio-bindings-0.3.1 (c (n "vfio-bindings") (v "0.3.1") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.8.0") (o #t) (d #t) (k 0)))) (h "1kjb8q7746fwgaa9p1l9h3z638xyskmks6fagm8713s89i09ni23") (f (quote (("vfio-v5_0_0") ("fam-wrappers" "vmm-sys-util"))))))

