(define-module (crates-io vf sy vfsys) #:use-module (crates-io))

(define-public crate-vfsys-0.0.1 (c (n "vfsys") (v "0.0.1") (h "0pl08lm31adbvwdvgy2gs9gx0sc8c0pqj82aazvz8fhb4f8rhgfw")))

(define-public crate-vfsys-0.0.2 (c (n "vfsys") (v "0.0.2") (h "07xhkv4lq2zi87r3y9kmjav9gn3pn9ncn6112c9j91zpsqxm9gii")))

(define-public crate-vfsys-0.0.3 (c (n "vfsys") (v "0.0.3") (h "0zc1pxx1mg7c6qcxyqrk0j51y2xbhd1b0ywxkyz78h89iakhaj2b")))

(define-public crate-vfsys-0.1.7 (c (n "vfsys") (v "0.1.7") (h "12zah9pr37gd8ydjxp46iji2wjsvpshbl85qgal2a6pp4s93vfip")))

(define-public crate-vfsys-0.1.8 (c (n "vfsys") (v "0.1.8") (h "1qks34am5750qprv40imzmv9l3a386046gzqx7wp5bgdydqvm87p")))

(define-public crate-vfsys-0.1.9 (c (n "vfsys") (v "0.1.9") (h "1wcv9qgc9g12aj14akwdwlvwlz8shrw44xh6ln1nsdi57fay98sx")))

(define-public crate-vfsys-0.1.10 (c (n "vfsys") (v "0.1.10") (h "1984m6dg6nnkaxxja88k0f1hvsip9rpib5m75122f73n2683wq14")))

