(define-module (crates-io vf xp vfxpreopenexr-sys) #:use-module (crates-io))

(define-public crate-vfxpreopenexr-sys-0.0.1 (c (n "vfxpreopenexr-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0z0cb993yiysd215kwqw5niimfijqjpn9lwikipg9j3338fncm2w")))

(define-public crate-vfxpreopenexr-sys-0.0.2 (c (n "vfxpreopenexr-sys") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "049bd3nyrinnz71vjdqhqhx8frysx0hq1v0lrgc0lnbp09lp7w5l")))

(define-public crate-vfxpreopenexr-sys-0.0.3 (c (n "vfxpreopenexr-sys") (v "0.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "170km62xsgzm950jlvlvx0l3xdsz9xpcqiz2mmcxslksmpb6inbv")))

(define-public crate-vfxpreopenexr-sys-0.0.4 (c (n "vfxpreopenexr-sys") (v "0.0.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "181vgv6k8gmg8r0g2yddff083lcwrpzd9k0lgl3yxrhz20gdmmpw")))

(define-public crate-vfxpreopenexr-sys-0.0.5 (c (n "vfxpreopenexr-sys") (v "0.0.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0adf9cxn4bxjm28i7yrxqppap4klzrisi7q7pl53hax6mwd3ymxx")))

(define-public crate-vfxpreopenexr-sys-0.0.6 (c (n "vfxpreopenexr-sys") (v "0.0.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.5.4") (d #t) (k 1)))) (h "1gfrvd81awp3qwldhyc7h1wr13a5r7byddjgyknimg2dp3ifsjss")))

