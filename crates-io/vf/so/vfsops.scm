(define-module (crates-io vf so vfsops) #:use-module (crates-io))

(define-public crate-vfsops-0.1.0 (c (n "vfsops") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kstat") (r "^0.1") (d #t) (k 0)))) (h "1s9asracnxdw9351pgs8nlp5qmsp8y19m0g6f74g52pq9wnnf0pv")))

(define-public crate-vfsops-0.1.1 (c (n "vfsops") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "kstat") (r "^0.1") (d #t) (k 0)))) (h "13cmh7xvrdrmwrr8c6nds65an1bs8njlpd5qny2j5mlgxij4s4bp")))

