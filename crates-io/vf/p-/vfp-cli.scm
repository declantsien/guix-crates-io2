(define-module (crates-io vf p- vfp-cli) #:use-module (crates-io))

(define-public crate-vfp-cli-0.1.0 (c (n "vfp-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vfp") (r "^0.1.0") (d #t) (k 0)))) (h "0k9mxzqdy3d1xvkxkphgv2vk39i1f5w6zxs6m59hy6r5kx57v3az")))

