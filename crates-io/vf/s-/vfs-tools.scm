(define-module (crates-io vf s- vfs-tools) #:use-module (crates-io))

(define-public crate-vfs-tools-0.1.0 (c (n "vfs-tools") (v "0.1.0") (d (list (d (n "resiter") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1bdv0vdnahclc7vf46a86l7cbxc7f61ad7fjc2p62n4p5ya7y2wf")))

