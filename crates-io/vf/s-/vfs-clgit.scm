(define-module (crates-io vf s- vfs-clgit) #:use-module (crates-io))

(define-public crate-vfs-clgit-0.1.0 (c (n "vfs-clgit") (v "0.1.0") (d (list (d (n "git") (r "^0.1.1") (d #t) (k 0) (p "clgit")) (d (n "vfs04") (r "^0.4") (o #t) (d #t) (k 0) (p "vfs")))) (h "0h0ga7iq6hi0gyxxb69gk9dhxqizwcbv8z0jqmhz60an3xr7r919") (f (quote (("default" "vfs04"))))))

