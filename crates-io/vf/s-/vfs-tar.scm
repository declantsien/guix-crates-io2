(define-module (crates-io vf s- vfs-tar) #:use-module (crates-io))

(define-public crate-vfs-tar-0.1.0 (c (n "vfs-tar") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar-rs") (r "^0.4") (d #t) (k 2) (p "tar")) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1pigvsbbndgv85qpnnlh0aqrj5c6xbr3cjyxgaqp0my7dci9ykl5")))

(define-public crate-vfs-tar-0.2.0 (c (n "vfs-tar") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar-rs") (r "^0.4") (d #t) (k 2) (p "tar")) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "0vpm8ppkpla8hm3j5mlkh0w8ka9z4929wwdh6h9zyzsmanm149jm")))

(define-public crate-vfs-tar-0.2.1 (c (n "vfs-tar") (v "0.2.1") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "tar-rs") (r "^0.4") (d #t) (k 2) (p "tar")) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1hhkknr89a9z1nv9qvprg3kxr1500c4f7dp71c3h4j9jb10h3bgl")))

(define-public crate-vfs-tar-0.3.0 (c (n "vfs-tar") (v "0.3.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tar-parser2") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1pxpan37mgic92phwrdnhmqr63n0c7qwd411dyvjggsxd4qixq2y")))

(define-public crate-vfs-tar-0.3.1 (c (n "vfs-tar") (v "0.3.1") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tar-parser2") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "07zgm7hgshh2bxp0p68k105zsidd1jnxlbx5q1hdad2x0wx0hq0z")))

(define-public crate-vfs-tar-0.3.2 (c (n "vfs-tar") (v "0.3.2") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tar-parser2") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "1khxhngil8nyc54m9r38n2q3n7m7z5yy0glvi6kj4vrx9vqbdi7v")))

(define-public crate-vfs-tar-0.4.0 (c (n "vfs-tar") (v "0.4.0") (d (list (d (n "memmap2") (r "^0.5") (f (quote ("stable_deref_trait"))) (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tar-parser2") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "vfs") (r "^0.9") (d #t) (k 0)))) (h "0lpxz029ciqwl926w73icb3bk49vz3iy48v3jvjng980kqhflvnb") (f (quote (("default")))) (s 2) (e (quote (("mmap" "dep:memmap2"))))))

(define-public crate-vfs-tar-0.4.1 (c (n "vfs-tar") (v "0.4.1") (d (list (d (n "memmap2") (r "^0.9") (f (quote ("stable_deref_trait"))) (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 2)) (d (n "tar-parser2") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)) (d (n "vfs") (r "^0.10") (d #t) (k 0)))) (h "19y8m906glydv1q29p7x0747pfil0l5iir5q9g8p5db6ww6nnmdc") (f (quote (("default")))) (s 2) (e (quote (("mmap" "dep:memmap2"))))))

