(define-module (crates-io vf s- vfs-zip) #:use-module (crates-io))

(define-public crate-vfs-zip-0.1.0 (c (n "vfs-zip") (v "0.1.0") (d (list (d (n "flate2") (r "<1.0.16") (d #t) (k 0)) (d (n "vfs04") (r "^0.4") (o #t) (d #t) (k 0) (p "vfs")) (d (n "zip") (r "^0.5") (d #t) (k 0) (p "zip")))) (h "1i73wrf8vlrl3x05dk7dfba33069bk20655xqa2sjjscqja64f48") (f (quote (("default" "vfs04"))))))

(define-public crate-vfs-zip-0.2.0 (c (n "vfs-zip") (v "0.2.0") (d (list (d (n "flate2") (r "<1.0.16") (d #t) (k 0)) (d (n "vfs04") (r "^0.4") (o #t) (d #t) (k 0) (p "vfs")) (d (n "zip") (r "^0.5") (k 0) (p "zip")))) (h "0zhn0j8m7z438k7j3sa7n86lpx2v6hg62vfyhk2nzbw22bbp0z5r") (f (quote (("zip-time" "zip/time") ("zip-deflate" "zip/deflate") ("zip-bzip2" "zip/bzip2") ("default" "vfs04" "zip-deflate" "zip-bzip2"))))))

(define-public crate-vfs-zip-0.2.1 (c (n "vfs-zip") (v "0.2.1") (d (list (d (n "flate2") (r "<1.0.16") (d #t) (k 0)) (d (n "vfs04") (r "^0.4") (o #t) (d #t) (k 0) (p "vfs")) (d (n "zip") (r "^0.5.3") (k 0) (p "zip")))) (h "0lmnkcmk52l040p640w0zv5mlfmclnq5mrx69hwcl4nd2jb3d2nn") (f (quote (("zip-time" "zip/time") ("zip-deflate" "zip/deflate") ("zip-bzip2" "zip/bzip2") ("default" "vfs04" "zip-deflate" "zip-bzip2"))))))

