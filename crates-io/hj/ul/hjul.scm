(define-module (crates-io hj ul hjul) #:use-module (crates-io))

(define-public crate-hjul-0.1.0 (c (n "hjul") (v "0.1.0") (d (list (d (n "mio") (r "^0.6.19") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "spin") (r "^0.5.1") (d #t) (k 0)))) (h "14ykydydbbzc1lhzafldc32nbiz90yx573hmqr4cjaiwrjjg34rz")))

(define-public crate-hjul-0.1.1 (c (n "hjul") (v "0.1.1") (d (list (d (n "mio") (r "^0.6.19") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "spin") (r "^0.5.1") (d #t) (k 0)))) (h "175qpj2jk01mi6l46xci1jdr9xsswq93dy23h70sksv88rbby4xk")))

(define-public crate-hjul-0.1.2 (c (n "hjul") (v "0.1.2") (d (list (d (n "mio") (r "^0.6.19") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "spin") (r "^0.5.1") (d #t) (k 0)))) (h "0xx5xccg0nnphzr5rybqcrc4hzy2w7rw5f1053ys6ig6qffzp7za")))

(define-public crate-hjul-0.2.0 (c (n "hjul") (v "0.2.0") (d (list (d (n "mio") (r "^0.6.19") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "spin") (r "^0.5.1") (d #t) (k 0)))) (h "1z65z07fp3wvzb85rx95q7b75jyzyh4snkg770b82w7218jnh58m")))

(define-public crate-hjul-0.2.1 (c (n "hjul") (v "0.2.1") (d (list (d (n "mio") (r "^0.6.19") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0.5") (d #t) (k 0)) (d (n "spin") (r "^0.5.1") (d #t) (k 0)))) (h "0yg16767d6m96z1867qbja4anwvjqwjg2yfhvj4jwf1hwc86sc7k")))

(define-public crate-hjul-0.2.2 (c (n "hjul") (v "0.2.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-extras") (r "^2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "104ihqrk5c4pwm2daxd6cacrdz7abqmid512vnir8kdzi6axahxm")))

