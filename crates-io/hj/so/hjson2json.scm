(define-module (crates-io hj so hjson2json) #:use-module (crates-io))

(define-public crate-hjson2json-0.1.0 (c (n "hjson2json") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1j0snsmsy9aj4372dsv3c6lfjb7lm55dc8jb4glhz2i5qk6bkbpc")))

