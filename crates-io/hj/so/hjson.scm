(define-module (crates-io hj so hjson) #:use-module (crates-io))

(define-public crate-hjson-0.1.0 (c (n "hjson") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "00q3x71acl70zialsyncxrg2abkmnda3y27vqrsxfwx4cvdj4a87")))

(define-public crate-hjson-0.1.1 (c (n "hjson") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0qjfwfky68xqy1dsrl8hz6dxsqsiff92jwbxpwx6vprkram0ha42")))

(define-public crate-hjson-0.8.0 (c (n "hjson") (v "0.8.0") (d (list (d (n "docopt") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "1c4hd7mwg0b1ywxda61awrhvf3x69l5b4gfighyhq6dxar149hbz")))

(define-public crate-hjson-0.8.1 (c (n "hjson") (v "0.8.1") (d (list (d (n "docopt") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "027n7jn6f4b23h71zq1gb31ly728qd8amzvavwisxv9fvpnsgysx")))

(define-public crate-hjson-0.8.2 (c (n "hjson") (v "0.8.2") (d (list (d (n "docopt") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde-hjson") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "0c75zmq2qmxspzmf2vbisfsc8hqryff4izsa6p4d2xzcchxdfj88")))

