(define-module (crates-io nq ue nqueens) #:use-module (crates-io))

(define-public crate-nqueens-0.1.0 (c (n "nqueens") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1w3l6fbp24ri138vihsjvninmifmgw3ydvysc70vb72k7zxyxjny")))

(define-public crate-nqueens-0.1.1 (c (n "nqueens") (v "0.1.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "0dnxxls1d43n7pc8k2q6q1r09ypp25qapkw26b1s6mhars4k8im3")))

(define-public crate-nqueens-0.1.2 (c (n "nqueens") (v "0.1.2") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1c2bx55n5zg8l4g8cszz2hsah6aaggh96n8hy0p949bcq0q4ka1k")))

