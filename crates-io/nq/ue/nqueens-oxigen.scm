(define-module (crates-io nq ue nqueens-oxigen) #:use-module (crates-io))

(define-public crate-nqueens-oxigen-1.0.0 (c (n "nqueens-oxigen") (v "1.0.0") (d (list (d (n "oxigen") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0sg4ak11mm6kmckp5j8mm3clx2ciczdg3la90y8my22zplz1xn6i") (y #t)))

(define-public crate-nqueens-oxigen-1.1.0 (c (n "nqueens-oxigen") (v "1.1.0") (d (list (d (n "oxigen") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1wc2idf4dm1gwh50hhc6c3vc4zcq6fff2k86134c1x92rcqd5sfk") (y #t)))

(define-public crate-nqueens-oxigen-1.2.0 (c (n "nqueens-oxigen") (v "1.2.0") (d (list (d (n "oxigen") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0pydq7442pxc43fd8zjhw3lxcps35s54shqnp470lsgflb5g9wqm")))

(define-public crate-nqueens-oxigen-2.0.0 (c (n "nqueens-oxigen") (v "2.0.0") (d (list (d (n "oxigen") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1dw00j24jr87hbwmlhj0xs5wry2131rwx59kydgqaxd6k4g9qrg4")))

(define-public crate-nqueens-oxigen-2.1.0 (c (n "nqueens-oxigen") (v "2.1.0") (d (list (d (n "oxigen") (r "^2.1") (f (quote ("global_cache"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1y4pcj5bkahmldvkbhz1lrj6kkjj12dwlrqrr0xg75yc74hawqh9")))

(define-public crate-nqueens-oxigen-2.2.0 (c (n "nqueens-oxigen") (v "2.2.0") (d (list (d (n "oxigen") (r "^2.2.0") (f (quote ("global_cache"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0sgk0ghwqbfkpah3g9mk927jchgjxhyyaiwnnmzriblhgp98zr3v")))

