(define-module (crates-io qj ac qjack_macros) #:use-module (crates-io))

(define-public crate-qjack_macros-0.1.0 (c (n "qjack_macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10vbxzq2mix0sfjcpz6wnyimrlz13b203r2qw8pljgv16nm6s288")))

(define-public crate-qjack_macros-0.2.0 (c (n "qjack_macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00sr09ld37fai4h7jd8r974n32jnylmq9fqaa33wfrvxldyjgk57")))

