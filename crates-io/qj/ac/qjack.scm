(define-module (crates-io qj ac qjack) #:use-module (crates-io))

(define-public crate-qjack-0.1.0 (c (n "qjack") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "qjack_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (d #t) (k 0)))) (h "1rjii5n69gzf74w01j28d8d9k5h27j74rwj8gdmaw6nqplp6k87k") (f (quote (("rt_tokio" "sqlx/runtime-tokio-native-tls") ("rt_async-std" "sqlx/runtime-async-std-native-tls") ("default" "rt_tokio" "db_postgres") ("db_sqlite" "sqlx/sqlite") ("db_postgres" "sqlx/postgres") ("db_mysql" "sqlx/mysql"))))))

(define-public crate-qjack-0.1.1 (c (n "qjack") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "qjack_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (d #t) (k 0)))) (h "1jjma2kfs8sxbws5w51wz91ryxvkzkhgdi07chd2lnrypnyswggi") (f (quote (("rt_tokio" "sqlx/runtime-tokio-native-tls") ("rt_async-std" "sqlx/runtime-async-std-native-tls") ("default") ("db_sqlite" "sqlx/sqlite") ("db_postgres" "sqlx/postgres") ("db_mysql" "sqlx/mysql"))))))

(define-public crate-qjack-0.1.2 (c (n "qjack") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "qjack_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (d #t) (k 0)))) (h "02cq5p131s0c1c1pscxm765xsjb6v9vpfj9mqd479y3mg63fs0wn") (f (quote (("rt_tokio" "sqlx/runtime-tokio-native-tls") ("rt_async-std" "sqlx/runtime-async-std-native-tls") ("db_sqlite" "sqlx/sqlite") ("db_postgres" "sqlx/postgres") ("db_mysql" "sqlx/mysql"))))))

(define-public crate-qjack-0.1.3 (c (n "qjack") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "qjack_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (d #t) (k 0)))) (h "141f0zqvfp3vaiaznysjvsf3pyvxc2r2li62p58f4j4nsgqb94nd") (f (quote (("rt_tokio" "sqlx/runtime-tokio-native-tls") ("rt_async-std" "sqlx/runtime-async-std-native-tls") ("db_sqlite" "sqlx/sqlite") ("db_postgres" "sqlx/postgres") ("db_mysql" "sqlx/mysql"))))))

