(define-module (crates-io qj s- qjs-derive) #:use-module (crates-io))

(define-public crate-qjs-derive-0.1.1 (c (n "qjs-derive") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "qjs") (r "^0.1") (d #t) (k 2)) (d (n "qjs-derive-support") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "1xrglms5d08x90ppp0razj1yxb3w9nhy0y3lw0909hkxp6386g0p")))

