(define-module (crates-io qj s- qjs-ng) #:use-module (crates-io))

(define-public crate-qjs-ng-0.1.0 (c (n "qjs-ng") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c1rx231flnz688d3h0h59zfy5wfwv3yyjfdm2jqmg2bpvl80gwj")))

(define-public crate-qjs-ng-0.0.1 (c (n "qjs-ng") (v "0.0.1") (d (list (d (n "qjs-ng-sys") (r "^0.0.4") (d #t) (k 0)))) (h "10hxangl86shc3c51gfz51g8ykybb45sc3mi048rzkxzmqwjy6x0")))

(define-public crate-qjs-ng-0.0.2 (c (n "qjs-ng") (v "0.0.2") (d (list (d (n "qjs-ng-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0pfs3mpnp02gkygx6ijv960ppd6iz5sfdcii29fgjyr9nwcl5719")))

(define-public crate-qjs-ng-0.1.2 (c (n "qjs-ng") (v "0.1.2") (d (list (d (n "qjs-ng-sys") (r "^0.0.4") (d #t) (k 0)))) (h "01w5fvqdrzqardc6qbj5cni2ag667w6jb1h0cnjndhzr8dllnw3v")))

