(define-module (crates-io qj s- qjs-derive-support) #:use-module (crates-io))

(define-public crate-qjs-derive-support-0.1.1 (c (n "qjs-derive-support") (v "0.1.1") (d (list (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ls6a0dg3dvh4n5kkxfx9l3qja4csa9rhr13k19nvxak4p2lraac")))

