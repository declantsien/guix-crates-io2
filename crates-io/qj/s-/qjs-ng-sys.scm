(define-module (crates-io qj s- qjs-ng-sys) #:use-module (crates-io))

(define-public crate-qjs-ng-sys-0.0.4 (c (n "qjs-ng-sys") (v "0.0.4") (d (list (d (n "bindgen-rs") (r "^0.55.1") (o #t) (d #t) (k 1) (p "bindgen")) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (o #t) (d #t) (k 1)))) (h "0g34b0r4w5qfvqb5halqdhxajlfsr26bw7586j5vp9k7pq51ji7f") (f (quote (("update-bindings" "bindgen") ("parallel") ("logging" "pretty_env_logger") ("exports") ("bindgen" "bindgen-rs"))))))

