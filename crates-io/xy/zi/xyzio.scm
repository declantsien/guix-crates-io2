(define-module (crates-io xy zi xyzio) #:use-module (crates-io))

(define-public crate-xyzio-0.1.0 (c (n "xyzio") (v "0.1.0") (h "19hw4829qrlhcwd6fz97z7zbgm8vjjdykhcb4595psi4yc7bgka4")))

(define-public crate-xyzio-0.2.0 (c (n "xyzio") (v "0.2.0") (h "0j6ddffx7bcgn3bnw1mw5zydg7q0b9c05cwm6q40j9xh19gqqx5q")))

(define-public crate-xyzio-0.3.0 (c (n "xyzio") (v "0.3.0") (h "02rdi7ff9i81xmzvfikri05prdd8hyxw6nc405v37kr9ibm2qjqy")))

(define-public crate-xyzio-0.3.1 (c (n "xyzio") (v "0.3.1") (h "1z2aiggxc2sq7mlkjfz3sjgw5kk53m6918h0hbcg49k4a24wq4db") (f (quote (("double_precision"))))))

