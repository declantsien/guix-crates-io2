(define-module (crates-io xy z_ xyz_validator) #:use-module (crates-io))

(define-public crate-xyz_validator-0.1.0 (c (n "xyz_validator") (v "0.1.0") (h "0if966pm9a875nhmvz9ffsr0xwx6r917fxjcrn8qmscb7hg2qzr5")))

(define-public crate-xyz_validator-0.1.1 (c (n "xyz_validator") (v "0.1.1") (h "1d0xvbsyy3yw8l24b79iix3zz1vk704aqn4mcvbsingyml53dqj3")))

(define-public crate-xyz_validator-0.2.0 (c (n "xyz_validator") (v "0.2.0") (h "0mb17b1zy5nsk47r8qksbad2i4qn0ljra3zkvzhlx1q7a7q9r0ig")))

(define-public crate-xyz_validator-0.2.1 (c (n "xyz_validator") (v "0.2.1") (h "1nqviq2ijs431ly0ad2zl578hv3yla0dvb5cdjbmdw045xcf9a3c")))

(define-public crate-xyz_validator-0.2.2 (c (n "xyz_validator") (v "0.2.2") (h "03c0r9m0d9lnbw77m7mj0dy7da88f1q196ix0b50bflc9449mmbq")))

(define-public crate-xyz_validator-0.3.0 (c (n "xyz_validator") (v "0.3.0") (h "1inpd84z6i6xw5awy6fszk966k8bzziyxgn5y7j3y0cj9jdd9mcg")))

(define-public crate-xyz_validator-0.4.0 (c (n "xyz_validator") (v "0.4.0") (h "0f750hiz2lmpjahzpy5kaa4x7bn4gnh3d8307qjzrybgj3g9mlzl")))

(define-public crate-xyz_validator-0.4.1 (c (n "xyz_validator") (v "0.4.1") (h "0h95qslwjdsrl9p2zdns012sy4jvbjsqpwqzrjp0nnzrrpfs8cqd")))

