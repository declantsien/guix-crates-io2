(define-module (crates-io xy le xylem-codegen) #:use-module (crates-io))

(define-public crate-xylem-codegen-0.1.0 (c (n "xylem-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "14i9hmyap03341x6cr6mqs2w76n1cdz5lqp4mw9klgmz5pxbm42l")))

(define-public crate-xylem-codegen-0.2.0 (c (n "xylem-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1srqwwn986v3j3yq8hik6ipc0x41ds2mcpcs22zkz5yg7cy2z0ba")))

(define-public crate-xylem-codegen-0.2.1 (c (n "xylem-codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1z31js6qj3pv9zvl0hbpy075ch0n1gafk7394s049khw65lr1lvs")))

(define-public crate-xylem-codegen-0.2.2 (c (n "xylem-codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1z03kw9sdygadzfpbfabd50i4dmzskaicf79lhdv8230h4s3fg2j")))

(define-public crate-xylem-codegen-0.2.3 (c (n "xylem-codegen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "07q1yw1m6mvri7k3payy6n6ms6cgyqsh7fb7ia7851akxpr01s2v")))

(define-public crate-xylem-codegen-0.2.4 (c (n "xylem-codegen") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0nd21a314r6am6mg0wrm6sfhlhrdv1y5hmk580wyv5pm4yx1zps3")))

(define-public crate-xylem-codegen-0.2.5 (c (n "xylem-codegen") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0vjmlslb934zl8ndla6iaicbfx8i6dr857xm7azaw93vn3qrygx5")))

(define-public crate-xylem-codegen-0.2.6 (c (n "xylem-codegen") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0qa6jkk0b8pjdji801pblhj1mzzfcclplj1bd198717flm1pmhds")))

(define-public crate-xylem-codegen-0.2.7 (c (n "xylem-codegen") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0fia9b6a0dvx3qrbysc4gd9c8cxffk4x4h4m55j79bdh1igk5fwi")))

