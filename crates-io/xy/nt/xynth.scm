(define-module (crates-io xy nt xynth) #:use-module (crates-io))

(define-public crate-xynth-0.1.0 (c (n "xynth") (v "0.1.0") (h "0qvnxbjhv8kqndyky9rgdigbm745p3df2kawayk0flpa2nr86agn")))

(define-public crate-xynth-0.1.1 (c (n "xynth") (v "0.1.1") (h "1c1qqw4svdzjmca8ll89ixhjycq7l49lb98m1iwv63z483c6z31h")))

(define-public crate-xynth-0.1.2 (c (n "xynth") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1319k67j3qrksrkjbj3xkv3hcy6y6qss0hcs56zm8rwk70y5s9dw")))

(define-public crate-xynth-0.1.3 (c (n "xynth") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1dlrkhkq3p2sqrs6g9v81pcvamjyid95dyhw819yk7xn17di74f6")))

(define-public crate-xynth-0.1.4 (c (n "xynth") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0ic898dcrgf8w2dzl22d6lgkq2z8s93mqrnsaavsy5l8mpm7wg1k")))

(define-public crate-xynth-0.1.5 (c (n "xynth") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0x62sfwbd395xs2yvi9rd405cwyv373lqzi65aswfhp38hlh7pws")))

(define-public crate-xynth-0.1.6 (c (n "xynth") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1fnf2fry8mzf6ly0cssnwrpdqzdqk9m0wvlfvhqp1mjxj1ylfx4r")))

(define-public crate-xynth-0.1.7 (c (n "xynth") (v "0.1.7") (h "1a2xk4dmq7hyx5lfpsj1zxcycykh0vbmjxdq087daj2ay7h3hk9q")))

