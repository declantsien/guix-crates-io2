(define-module (crates-io xy ra xyraith) #:use-module (crates-io))

(define-public crate-xyraith-0.1.0 (c (n "xyraith") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.2.0") (f (quote ("concolor"))) (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.4") (f (quote ("label"))) (d #t) (k 0)))) (h "0sbgdcya9yhxrc70ygsb6ixbkln3lylaafr531crc2c4dngxliwz")))

