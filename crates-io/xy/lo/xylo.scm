(define-module (crates-io xy lo xylo) #:use-module (crates-io))

(define-public crate-xylo-0.1.0 (c (n "xylo") (v "0.1.0") (h "0jhvskn053m4a4krjyqpikkx08zzp0ymhfafnc4ryvb6444xlhg0")))

(define-public crate-xylo-0.1.1 (c (n "xylo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (f (quote ("crossbeam-channel"))) (d #t) (k 0)))) (h "0d585n5ggsd75x4zwsk65lc2zh838nwpjg2gkqfxirk0kpygjjhx")))

