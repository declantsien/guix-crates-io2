(define-module (crates-io xy zv xyzvec) #:use-module (crates-io))

(define-public crate-xyzvec-0.1.0 (c (n "xyzvec") (v "0.1.0") (d (list (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0s4w9j4l2as7180438mi6vdm5irw6nhpb1m29pvhzsay3dm5apx8")))

(define-public crate-xyzvec-0.1.1 (c (n "xyzvec") (v "0.1.1") (d (list (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0jw1cmzljgpf4kb4miq3dggw3vhk6h3rvbfrvs2597k7s8k52h75")))

(define-public crate-xyzvec-0.1.3 (c (n "xyzvec") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "fixed") (r "^1.25.1") (d #t) (k 2)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0ybcn5ajrf624vlb92sy6z9rcw2k6z45zjsjdiq9d0n4iy9p6zak")))

(define-public crate-xyzvec-0.1.4 (c (n "xyzvec") (v "0.1.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "fixed") (r "^1.25.1") (d #t) (k 2)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "1syf545zvx6qfbnl3q6b7hnfcfab2ry95mb3k11bgbnq9nqsmdz7")))

(define-public crate-xyzvec-0.1.5 (c (n "xyzvec") (v "0.1.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "fixed") (r "^1.25.1") (d #t) (k 2)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0fyapwhdlicpbwywrdnsm4kxny43056zslnx9gb6k79wdf6gvz55")))

(define-public crate-xyzvec-0.1.6 (c (n "xyzvec") (v "0.1.6") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "10cpl1p7glpglkkwi932n21sh68nrjr6iiinkh6qq6gfg8xrhr73") (s 2) (e (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1.7 (c (n "xyzvec") (v "0.1.7") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0i7i81x5dj3righygq9374nwb0w5qqicfdmh0r8zmid0a321idbb") (s 2) (e (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1.8 (c (n "xyzvec") (v "0.1.8") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0028zx1z0kfnp1cjx844p39v26knb5fblfyxwhbi7jmfsbcwkiqy") (s 2) (e (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1.9 (c (n "xyzvec") (v "0.1.9") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0if39i8qmpqxc33636kzcf1ypcgkaj2qv4whpjcfhmf3l4b3wywm") (s 2) (e (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1.10 (c (n "xyzvec") (v "0.1.10") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "19zzjs2yygq2cfn425xrgw77pwhyjx8z99db3rv24a3v6adlygpz") (s 2) (e (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1.11 (c (n "xyzvec") (v "0.1.11") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1.25.1") (d #t) (k 0)) (d (n "fmt") (r "^0.1.0") (d #t) (k 0)))) (h "0h90fyhkfh41cr0fifxh21gzbbfwfaqmr8yrvy1f36wfacvmsqg5") (s 2) (e (quote (("cordic" "dep:cordic"))))))

