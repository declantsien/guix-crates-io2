(define-module (crates-io xy io xyio) #:use-module (crates-io))

(define-public crate-xyio-0.1.0 (c (n "xyio") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "http-muncher") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1nbrxpzx91mq1v5msh794hkvss031395k8npmy5hilwfd9n8wy91")))

