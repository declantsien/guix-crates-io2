(define-module (crates-io xy z- xyz-parse) #:use-module (crates-io))

(define-public crate-xyz-parse-0.1.2 (c (n "xyz-parse") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("rust_decimal" "extension-module" "abi3-py37"))) (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.35") (k 0)))) (h "0k1w43zc8vpngkrlv75zw1jrv2ilyb3p8qi14m2f0j7agh3z9rk1") (f (quote (("python" "pyo3") ("default"))))))

(define-public crate-xyz-parse-0.1.3 (c (n "xyz-parse") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("rust_decimal"))) (o #t) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.35") (k 0)))) (h "0fxih7g9jdqpsg4ls1d6jhys4d2c2izhqc5cwmaka6vplnb0sbhw") (f (quote (("pyo3-lib" "pyo3" "pyo3/extension-module" "pyo3/abi3-py37") ("default"))))))

