(define-module (crates-io w6 #{5c}# w65c02s) #:use-module (crates-io))

(define-public crate-w65c02s-0.9.0 (c (n "w65c02s") (v "0.9.0") (h "00bawnssxlm5ayq6waliw3djm0dm7gxglj8lzmpv3qpwxagya303")))

(define-public crate-w65c02s-0.9.1 (c (n "w65c02s") (v "0.9.1") (h "17kz2a3389kbfg6m9cfiwf73r33dk26a638hblngrqs750l2ygr4")))

(define-public crate-w65c02s-0.9.2 (c (n "w65c02s") (v "0.9.2") (h "10k3kxrniq2hd709ygpqlmdfqvlrccrkvapsnsc3l8xr92qigy8j")))

