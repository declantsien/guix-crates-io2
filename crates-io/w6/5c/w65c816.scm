(define-module (crates-io w6 #{5c}# w65c816) #:use-module (crates-io))

(define-public crate-w65c816-0.1.0 (c (n "w65c816") (v "0.1.0") (h "1xi0jgpv9naprskjj34w5v3bhhjfhgw247bryr5sazjc8wxvf6v3") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.1 (c (n "w65c816") (v "0.1.1") (h "1pcz9cjbif2qwfmvsv3ph03n0cpa9lhs7yn5qaxygrz16vbp561r") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.2 (c (n "w65c816") (v "0.1.2") (h "0092v19igzc32dllpbbv26ig4ljqkr2izbg2p9axdxiggqxc3ayl") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.3 (c (n "w65c816") (v "0.1.3") (h "12dfap6lawz1nyvhfmrqhs10pgba7hz03crc98nlbiz1sggvb5a8") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.4 (c (n "w65c816") (v "0.1.4") (h "1kgkznlcdxbi4myjkwqy5qn743g0j2wy1fy6sahhh5yva93q8qgl") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.5 (c (n "w65c816") (v "0.1.5") (h "07n7halj8cryb6ga272kp0afx3gz5zgzay32kp1wwbpix9znssjd") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.6 (c (n "w65c816") (v "0.1.6") (h "0hagj9qvbq7m362c4c1hhjnp31y523869rnjzyj6m3h7cwq9ly5h") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.7 (c (n "w65c816") (v "0.1.7") (h "0zm5j6glhkb6ad6r10hhp84is5zbmgjy83m75ka9mivvnih76yd4") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.8 (c (n "w65c816") (v "0.1.8") (h "0xf5rgjrss8z8x1pqbdv5970bsyaydvb59zkqxbpcwfxp3yhjgbw") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.9 (c (n "w65c816") (v "0.1.9") (h "10vrb98j5gxyf5argz71p3gj5xlwq40d6843hd71caqng0fz6mg3") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.10 (c (n "w65c816") (v "0.1.10") (h "08mhvxw2v5mf880wzbmpzmw31vq6yzww2sxn1phqqqzkqvi1zkcs") (f (quote (("nightly") ("default" "nightly")))) (y #t)))

(define-public crate-w65c816-0.1.11 (c (n "w65c816") (v "0.1.11") (h "1wfyhachhb4l687yyxl9yzmj670dk8nyyyckwcmfyavka1rlqbv5") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.12 (c (n "w65c816") (v "0.1.12") (h "0rwv9ksgj8hd0k396ah4vdzh3landaxmflsnq4clzidihn433648") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.13 (c (n "w65c816") (v "0.1.13") (h "1x34y4xv0n7hrdsbdrgxdzx8rbw9if5b2p32c8maivpk51kcml8y") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1.14 (c (n "w65c816") (v "0.1.14") (h "1giy8zipw7l363bwmlsc1pph6qnpwm06sm8z7ximhla1q8cpdjl9") (f (quote (("nightly") ("default"))))))

