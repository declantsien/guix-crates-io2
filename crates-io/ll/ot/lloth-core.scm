(define-module (crates-io ll ot lloth-core) #:use-module (crates-io))

(define-public crate-lloth-core-0.1.0 (c (n "lloth-core") (v "0.1.0") (d (list (d (n "alkahest") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "scoped-arena") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1h11b2r60p31b018456j3jqcn0b5sx3hg13kxcgxwjs153qvz210") (f (quote (("tcp" "tokio/net" "futures-task")))) (y #t)))

