(define-module (crates-io ll -r ll-rs) #:use-module (crates-io))

(define-public crate-ll-rs-0.0.0 (c (n "ll-rs") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0jxkv136dp5pxklh95j0jxqasfswnawsv1fqg2gb7ijc3fp74js3") (y #t)))

(define-public crate-ll-rs-0.0.1 (c (n "ll-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "15jfkv1gfwlhigh05xji62qlmzhwnds6bwaj7gpbi38z2a0la0w3") (y #t)))

(define-public crate-ll-rs-0.0.2 (c (n "ll-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1nc36y1krvyf3312fhwk2h89xag3pair3syq3cmhz2bhd3230dhm") (y #t)))

(define-public crate-ll-rs-0.0.3 (c (n "ll-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "00sv4v866yw5jmspc0xvq2si5vsd19ha0xzii9mx4wsk6kpmxmi6") (y #t)))

(define-public crate-ll-rs-0.0.4 (c (n "ll-rs") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "17ns0j0dlx0bdqiqk97p31nzn264hmm0b1w2hc56irn2wj0aqjag") (y #t)))

(define-public crate-ll-rs-0.0.5 (c (n "ll-rs") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1isjq6j5a8a4vj01579hhmn5rm1cj348v103kbc6211jssk3nwk5") (y #t)))

(define-public crate-ll-rs-0.0.6 (c (n "ll-rs") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "159zjrbrg1hiblmr41r10yxn13idhxql71pjy1y4wiqab3gwcp09") (y #t)))

(define-public crate-ll-rs-0.0.7 (c (n "ll-rs") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1xb1fr9z5lb4ng5jw0jqcxs67g7sfpkym1w9yf85gf6nlds1p5zc") (y #t)))

(define-public crate-ll-rs-0.0.8 (c (n "ll-rs") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1iard1mihyzzqlkyb75gj2l16lhnafzzx89j9p86qiar95hay9ml") (y #t)))

(define-public crate-ll-rs-0.0.9 (c (n "ll-rs") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0pwq3q85120glyfx4cb0m8fcg2a40fdjzkj06gaw5qwff978amqn")))

(define-public crate-ll-rs-0.1.0 (c (n "ll-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "17har558a4zidll6bi1k4xjy2wjzq61mjqlac2xl7k2hj6bgjv4a")))

