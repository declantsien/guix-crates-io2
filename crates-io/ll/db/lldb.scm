(define-module (crates-io ll db lldb) #:use-module (crates-io))

(define-public crate-lldb-0.0.1 (c (n "lldb") (v "0.0.1") (d (list (d (n "lldb-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0q6ld11pxq51iyvvpdbmf4sqd9vr8d7pabdvijnjvgk1xlq267yq")))

(define-public crate-lldb-0.0.2 (c (n "lldb") (v "0.0.2") (d (list (d (n "lldb-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1b2gc942bdy22cnwbc7hlnf32z1ycjg5p08xq7jsrkvrqcvdpl6d")))

(define-public crate-lldb-0.0.3 (c (n "lldb") (v "0.0.3") (d (list (d (n "lldb-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0a3qv54cnivzbmrsx5s4ymnqs3n6cszbhh2xmff1a2qm5bvqzldd")))

(define-public crate-lldb-0.0.4 (c (n "lldb") (v "0.0.4") (d (list (d (n "lldb-sys") (r "^0.0.10") (d #t) (k 0)))) (h "13w0683rdc54hkfnpwz19lxziipfbm2p9pcs9d4qnailin4jdjqc")))

(define-public crate-lldb-0.0.5 (c (n "lldb") (v "0.0.5") (d (list (d (n "lldb-sys") (r "^0.0.11") (d #t) (k 0)))) (h "10fa6rsh8jczh6x1kfhdmw7ncisiipg54jgdi6mkmjd6iswv52pl")))

(define-public crate-lldb-0.0.6 (c (n "lldb") (v "0.0.6") (d (list (d (n "juniper") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.14") (d #t) (k 0)))) (h "1flarfdmhvbkps4dnzcq44ayhks75m07flx6mzxfg1cfs8nff7i0") (f (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.7 (c (n "lldb") (v "0.0.7") (d (list (d (n "juniper") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.18") (d #t) (k 0)))) (h "021x0xg0ykxgfv5vng8vaj9nqqrxmh4jlhy28r5n3bqsq1lb0882") (f (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.8 (c (n "lldb") (v "0.0.8") (d (list (d (n "juniper") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.20") (d #t) (k 0)))) (h "1l7nclmk6wlnjixdhlx6q2nm0zln1q6xx1182jkhx4lql43qwn5s") (f (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.9 (c (n "lldb") (v "0.0.9") (d (list (d (n "juniper") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.27") (d #t) (k 0)))) (h "0665y9kiyis0csq8a1bf3ncbsrs13dq4mad6m49hijqg5gcrizj2") (f (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.10 (c (n "lldb") (v "0.0.10") (d (list (d (n "juniper") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.27") (d #t) (k 0)))) (h "01ic49d7gwrifjq8wdn8a2vzgzqww841mi1b39sfjid03mppcykl") (f (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.11 (c (n "lldb") (v "0.0.11") (d (list (d (n "juniper") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lldb-sys") (r "^0.0.29") (d #t) (k 0)))) (h "1cll6hdwb9zmkgy99y2l6dbmn796497n6z370hw08vza476kmbbf") (f (quote (("graphql" "juniper"))))))

