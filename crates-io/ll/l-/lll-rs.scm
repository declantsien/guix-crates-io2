(define-module (crates-io ll l- lll-rs) #:use-module (crates-io))

(define-public crate-lll-rs-0.1.0 (c (n "lll-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rug") (r "1.*") (d #t) (k 0)))) (h "0ilid6nalxqvshlcfk3bsf194mrjda9fhiylk6kk3qbmw8qpmpd8")))

(define-public crate-lll-rs-0.2.0 (c (n "lll-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "1.*") (d #t) (k 0)))) (h "1dsvs3yqi1n16ahn80m1py1c0bqfmpxc3yg3pfyngqs1lmqiz1cr")))

