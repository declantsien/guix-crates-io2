(define-module (crates-io ll -n ll-neighbors) #:use-module (crates-io))

(define-public crate-ll-neighbors-0.1.0 (c (n "ll-neighbors") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1bam846qr36ij66d3bcaz6a9wyixac4c4bln08ydwwlx9kk4zxz3")))

(define-public crate-ll-neighbors-0.1.1 (c (n "ll-neighbors") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1zasx11p4v173kbvrzx1frdc9q4h59h56h31npwcjj5v104376zi")))

(define-public crate-ll-neighbors-0.1.2 (c (n "ll-neighbors") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0867ayk4k1bpvrhh8y0ijdlzsxp2fa55b38636ydb3mycvyxsy3s")))

(define-public crate-ll-neighbors-0.1.3 (c (n "ll-neighbors") (v "0.1.3") (d (list (d (n "macaddr") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "08d3nb0sdvvp7asjjnyq2a594fm5m5byymxy3aaa70pkqljbkvdn")))

(define-public crate-ll-neighbors-0.1.4 (c (n "ll-neighbors") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1cyx30p0xf9pmf0dqqcqqags1dwikqxj10cphvr1cgd5n52p21cv")))

(define-public crate-ll-neighbors-0.1.5 (c (n "ll-neighbors") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "11pyd01cypp88l5aqndyacg34k76xczqqm2wdac57k5j75l8x2br")))

(define-public crate-ll-neighbors-0.1.6 (c (n "ll-neighbors") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "12k5mpdq9nqghn0yj6ybvha8v5qpm8a8d4d1ff7g7ysq1dfb6671")))

