(define-module (crates-io ll m- llm-gpt2) #:use-module (crates-io))

(define-public crate-llm-gpt2-0.1.0-rc1 (c (n "llm-gpt2") (v "0.1.0-rc1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc1") (d #t) (k 0)))) (h "04pwknbd3ijhh1wg1qb4cfc0sn413434kv5zxjl0s395yhsa2lss")))

(define-public crate-llm-gpt2-0.1.0-rc2 (c (n "llm-gpt2") (v "0.1.0-rc2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc2") (d #t) (k 0)))) (h "1pwp6y68kfrh163bk15kdh8swq6c6dw642db8clfh0imzjj25jrj")))

(define-public crate-llm-gpt2-0.1.0-rc3 (c (n "llm-gpt2") (v "0.1.0-rc3") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc3") (d #t) (k 0)))) (h "1p0zw8bipswnyscmni7z59782wil845dfkngxk84vwwga4llxf8k")))

(define-public crate-llm-gpt2-0.1.0-rc4 (c (n "llm-gpt2") (v "0.1.0-rc4") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc4") (d #t) (k 0)))) (h "03zfi31k7z8zbd9wmjs3vfpmb67xkviv448i8dlw0cds4036s76s")))

(define-public crate-llm-gpt2-0.1.1 (c (n "llm-gpt2") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.1") (d #t) (k 0)))) (h "1br20l68i5agwh9klp3m6i22ajasnf3g5ap9yk00a0zxhxxcp995")))

