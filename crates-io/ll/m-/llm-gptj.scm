(define-module (crates-io ll m- llm-gptj) #:use-module (crates-io))

(define-public crate-llm-gptj-0.1.0-rc1 (c (n "llm-gptj") (v "0.1.0-rc1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc1") (d #t) (k 0)))) (h "142v8433hc1574fviqwmr8rmpwpfx0jjfb3hp8av0qh7sxg8gq8k")))

(define-public crate-llm-gptj-0.1.0-rc2 (c (n "llm-gptj") (v "0.1.0-rc2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc2") (d #t) (k 0)))) (h "15jgwavhgbdavhav5jl8pic2xbflav7clsqamkxbkdcmdwmkd79l")))

(define-public crate-llm-gptj-0.1.0-rc3 (c (n "llm-gptj") (v "0.1.0-rc3") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc3") (d #t) (k 0)))) (h "11y6qlmj6xnqc2kv85xdizqk85251s14skap2pjf5kamrw27gc31")))

(define-public crate-llm-gptj-0.1.0-rc4 (c (n "llm-gptj") (v "0.1.0-rc4") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc4") (d #t) (k 0)))) (h "1v9jybkn3w18p2bqma2iq6pgf05d6506811l91j7ky28frf3b66r")))

(define-public crate-llm-gptj-0.1.1 (c (n "llm-gptj") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.1") (d #t) (k 0)))) (h "0lhixnk9mpixqkhir7v8hmlhk97n1x9j7rnl1cldgqa409h2vsz6")))

