(define-module (crates-io ll m- llm-chain-macros) #:use-module (crates-io))

(define-public crate-llm-chain-macros-0.12.3 (c (n "llm-chain-macros") (v "0.12.3") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0bp1mz80wm0agsk4z2byj13fcsa6dazzra2rcz95yq2zn5li8k39")))

(define-public crate-llm-chain-macros-0.13.0 (c (n "llm-chain-macros") (v "0.13.0") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0p9wflbwl38fs96rv0hsfiwfg18mm6j0q72sqqcq5n364y3pw6fr")))

