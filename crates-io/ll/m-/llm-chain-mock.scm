(define-module (crates-io ll m- llm-chain-mock) #:use-module (crates-io))

(define-public crate-llm-chain-mock-0.13.0 (c (n "llm-chain-mock") (v "0.13.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "llm-chain") (r "^0.13.0") (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "19x1fw4zyrirz3dkfz58gabr0gjzhi1d12mphlihark3yk0djrwb")))

