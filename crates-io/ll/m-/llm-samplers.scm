(define-module (crates-io ll m- llm-samplers) #:use-module (crates-io))

(define-public crate-llm-samplers-0.0.2 (c (n "llm-samplers") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hja8qwwk00x2aq66vfi9ym20r21j97n81wra3485s1g33l388z3")))

(define-public crate-llm-samplers-0.0.3 (c (n "llm-samplers") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zn5xp1sgc7qyf33gsasf7gxr2xlhizxyam8klysj7i27rbyfmph") (f (quote (("default" "rand"))))))

(define-public crate-llm-samplers-0.0.4 (c (n "llm-samplers") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mcwd8k10jn7yb3mq9xpd949jgvj8qbcw00q7ml0wb7by1gf4pm8")))

(define-public crate-llm-samplers-0.0.5 (c (n "llm-samplers") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wiq777na9bb60a65wwnp6dbkxabcwxdq73xzzannhrlp6czzdgf")))

(define-public crate-llm-samplers-0.0.6 (c (n "llm-samplers") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cjlyzsysjvv9y56vwgng7pkn9wsrlqnli82aimdr71w246zclvm")))

(define-public crate-llm-samplers-0.0.7 (c (n "llm-samplers") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15pq3i0k8z4qv5a20zdg534bxivpm9sis5snrc17g7nqdijxz1by")))

