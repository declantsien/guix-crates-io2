(define-module (crates-io ll m- llm-chain-milvus) #:use-module (crates-io))

(define-public crate-llm-chain-milvus-0.13.0 (c (n "llm-chain-milvus") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "llm-chain") (r "^0.13.0") (k 0)) (d (n "milvus-sdk-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 2)) (d (n "uuid") (r "^1.3.3") (d #t) (k 0)))) (h "1zd3b4rx80qpp6av0nb5z4iybasy91z5vgyn24p65wfig9fjkrxn")))

