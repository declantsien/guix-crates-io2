(define-module (crates-io ll m- llm-lsp) #:use-module (crates-io))

(define-public crate-llm-lsp-0.0.0 (c (n "llm-lsp") (v "0.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.6") (d #t) (k 0)) (d (n "lsp-types") (r "^0.95.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "018vw7sswfyjfmygmnk1la81ili2ym4mjd3cya1fzg6vv87n9r4i")))

