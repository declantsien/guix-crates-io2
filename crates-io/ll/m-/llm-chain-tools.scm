(define-module (crates-io ll m- llm-chain-tools) #:use-module (crates-io))

(define-public crate-llm-chain-tools-0.1.1 (c (n "llm-chain-tools") (v "0.1.1") (d (list (d (n "llm-chain") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 2)))) (h "0jmk01xzcs71wma55dzwf8q0g6yl6mpgrp3ih1z3q8k3pdjwyjg9")))

(define-public crate-llm-chain-tools-0.1.2 (c (n "llm-chain-tools") (v "0.1.2") (d (list (d (n "llm-chain") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 2)))) (h "0vdbh72bzkkhlraig8af5cnmmzki2ay0g4zwxagnmd06p0992gnc")))

(define-public crate-llm-chain-tools-0.1.3 (c (n "llm-chain-tools") (v "0.1.3") (d (list (d (n "llm-chain") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 2)))) (h "1rhnh9gfkg3nszxdyy05r40m4z49wg9ldjqki62c5182026y4897")))

(define-public crate-llm-chain-tools-0.1.4 (c (n "llm-chain-tools") (v "0.1.4") (d (list (d (n "llm-chain") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 2)))) (h "08v1iwq3if56fw6yjrdr48l9vyi61jnl2w29kwq68xfqdskrzi05")))

(define-public crate-llm-chain-tools-0.4.0 (c (n "llm-chain-tools") (v "0.4.0") (d (list (d (n "llm-chain") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 2)))) (h "1dpiy0im9sj2jpib9w9gjp83wkz9rg9fnpxpsfpkaqqqwrcv2pxk")))

