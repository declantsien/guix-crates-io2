(define-module (crates-io ll m- llm-bloom) #:use-module (crates-io))

(define-public crate-llm-bloom-0.1.0-rc1 (c (n "llm-bloom") (v "0.1.0-rc1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc1") (d #t) (k 0)))) (h "0yks1xgg5nwm4lfrwjl0sphpk3b4cgp6d1g400im6g305brwjwrs")))

(define-public crate-llm-bloom-0.1.0-rc2 (c (n "llm-bloom") (v "0.1.0-rc2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc2") (d #t) (k 0)))) (h "0czyrss5m4q9ncsfhg7a8vyvfqhimpggp59fyla0azj53xgasgp3")))

(define-public crate-llm-bloom-0.1.0-rc3 (c (n "llm-bloom") (v "0.1.0-rc3") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc3") (d #t) (k 0)))) (h "0ri6ac0ws10gpabksx5m5wi8wqzvcyppz6s6p21h04gnny4sxgk1")))

(define-public crate-llm-bloom-0.1.0-rc4 (c (n "llm-bloom") (v "0.1.0-rc4") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.0-rc4") (d #t) (k 0)))) (h "035vyi6q0lwg00c83xjl66hqzlqncll6jfinhhfzw0gszl19lm3x")))

(define-public crate-llm-bloom-0.1.1 (c (n "llm-bloom") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "llm-base") (r "^0.1.1") (d #t) (k 0)))) (h "1av589nwdjg6h2yfxwmjnjnbc579bjbapwkrmc3pyj4blv23xm9g")))

