(define-module (crates-io ll m- llm-minify) #:use-module (crates-io))

(define-public crate-llm-minify-0.1.0 (c (n "llm-minify") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1q5nsdspi78m5vck9328fh1rx3wjmclwi4f15qvzji59b1bcyqip")))

(define-public crate-llm-minify-0.1.1 (c (n "llm-minify") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0z6r3n5q67hvimp5m6ks97m28fql01c7assmy4i3wnfzdlib40va")))

