(define-module (crates-io ll ir llir) #:use-module (crates-io))

(define-public crate-llir-0.1.0 (c (n "llir") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0md2h3q8i5dyx9nc5kkfkna4hllc21z35qbjv9sgyn5012j201x4")))

(define-public crate-llir-0.1.1 (c (n "llir") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "1lk511nd8aim1vpfwlrvvx6x6imh53v5igvx1xx8sfq5xshzirp8")))

(define-public crate-llir-0.1.2 (c (n "llir") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "1m9l488is81zq7qs8n6abn1fx553zra7pwzxd2wmrs8772qbsqrm")))

(define-public crate-llir-0.1.3 (c (n "llir") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0dswlzjn7vd40cm8z3h2qzacf2drcsw7b371gjgplcj7ga4yrqib")))

(define-public crate-llir-0.1.4 (c (n "llir") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0zx8ifp087yicmkjg2y919b7ilqx83qx1m0fczihfyja96jy18n3")))

(define-public crate-llir-0.1.5 (c (n "llir") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0igsr1kp0ygn1rvzw4kqps19d15fkd5bhv4488d19amcz91gj3gg")))

(define-public crate-llir-0.1.6 (c (n "llir") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0by20fkinkjinxdjrs1bxhybz00skdck4r626z8chf0wsivzi4ic")))

(define-public crate-llir-0.1.7 (c (n "llir") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0n4hmbha9l17h5zyaqg972skkp8h4y19hszcfgc9jbfl2bs6rsyk")))

(define-public crate-llir-0.1.8 (c (n "llir") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "07k4dq38f659z6vxkrjjhymbgkj3d6k3jdby8l1w62zrikzci752")))

(define-public crate-llir-0.1.9 (c (n "llir") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "1w6s6d96kkz8mqzy3xhk2fsappkcivypls4a25bf7mivvqa7s128")))

(define-public crate-llir-0.1.10 (c (n "llir") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "166n171nb98kwi79y34246x4rbx2w8ggix7ixg9zmm3q23ak20k3")))

(define-public crate-llir-0.1.11 (c (n "llir") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "14n53rfmvxv18xhfa3nkjffp14c5sc7ywc87gcm8pnf8qr4gffah")))

(define-public crate-llir-0.2.0 (c (n "llir") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0fhk4x7ni52r2dizjsnri6hagsjaw7rb7rbq1rfsr138bwff847k")))

(define-public crate-llir-0.2.1 (c (n "llir") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "0rf1lflfw8xpjks6sb42jmwfsbggd24hfx3c528kwh9s1m0awsmg")))

(define-public crate-llir-0.2.2 (c (n "llir") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (d #t) (k 0)))) (h "1nci4z3x9nqil8f0d5p9zvvqij4c7dwy9v4wpmqcmpqa0zriii3a")))

