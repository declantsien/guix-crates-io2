(define-module (crates-io ll am llama-sys) #:use-module (crates-io))

(define-public crate-llama-sys-0.1.0 (c (n "llama-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1iznwkh6v6wm2r85azcvfxg76djz3vygnp25qxdlgfaah7k5jrbr") (y #t)))

(define-public crate-llama-sys-0.1.1 (c (n "llama-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "148wr6nbkpa2wzqysryxdfvwqfxiibr7zaqvg0bmibijvcncab7a")))

(define-public crate-llama-sys-0.1.2 (c (n "llama-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kjsil1jsx6g3hqirx50wgn966fi3j0ivk4s3f3rkg8flqbm3xb1")))

