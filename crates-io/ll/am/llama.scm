(define-module (crates-io ll am llama) #:use-module (crates-io))

(define-public crate-llama-0.0.0 (c (n "llama") (v "0.0.0") (d (list (d (n "llvm-sys") (r "^80") (d #t) (k 0)))) (h "0pkqrlqf4kdi8z6747n6yn8cfp0n3aaakhc1nmgy9rhc7g4dskif")))

(define-public crate-llama-0.1.0-beta (c (n "llama") (v "0.1.0-beta") (d (list (d (n "llvm-sys") (r "^80") (d #t) (k 0)))) (h "07a5rgl07ab1b3abc2f1c0h12g5xd7kvk0gb78n10k015b025aq4")))

(define-public crate-llama-0.1.0-beta.1 (c (n "llama") (v "0.1.0-beta.1") (d (list (d (n "llvm-sys") (r "^80") (d #t) (k 0)))) (h "1j13m8lixk2pxwr8iygzdw001gy0rzsn6clsxk9dnjh25yp2a9qx")))

(define-public crate-llama-0.1.0 (c (n "llama") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x0r1n074hyzlvcg7g3hmkgkcbizdcl0nz82r8g1bc6sd8ny8my2")))

(define-public crate-llama-0.1.1 (c (n "llama") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1irr15rxirmk21w19yw6m565xryk5r47in2ii419kbvr7wkqb5rp") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.1.2 (c (n "llama") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zh3ag04n88dbj8b47qxm3zlzbh6cqj8y0y01ac7rn5np1czh30z") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.2.0 (c (n "llama") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g43jlg5y419dgzsag1l0kbl3j5782m46mxgwaz1ll323lydy177") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.3.0 (c (n "llama") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1547qz2viajlv694dwi47b9a7qqa2iikhhskim5k6mrnma87vfmd") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.3.1 (c (n "llama") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x2j1vbz9cwx6hnlrdyrfcp3r9n7gcbcyrac42xfq8ahn7filyai") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.4.0 (c (n "llama") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xxk2ayq68mabjwhc29yc47dim6abcnd70rr30q2p870ay832sxn") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.5.0 (c (n "llama") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^100") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00jqiwy5gk2nnzg8hkmh4biw329ydkpkcy44526mlppac9w86163") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.7.0 (c (n "llama") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^110") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1idv7a13rmxn73wkqsl5n0blv323zc9bfsncb0v2slma7cbv52ms") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.8.0 (c (n "llama") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^120") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sffdvg4sj1wlbxl2s4dlnsa92niswwnxxp24c0x451b0l776i4m") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.9.0 (c (n "llama") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19vk0kdsrsp3yfixmsyzdv6prqhfa833rjl70c2ajy8lsbrh3j6m") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.10.0 (c (n "llama") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14fk1irjnhwma3vafwfrr9xs4azy5qkx2px8l0hlvd5v8gpxfs04") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.11.0 (c (n "llama") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0grah56l3yfcv5r94bpzzycjilrm5nia1g6fx30lyxbd0s4rpc4v") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.11.1 (c (n "llama") (v "0.11.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qn5mh5fj7bz7pzvwzmhgi6zim26gq94ps35gv7vs4irfr7srqm8") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.11.2 (c (n "llama") (v "0.11.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13274dam7kpdg6dhy690xamqjb1yd3qls5rxxk9qnhb1jpdhda36") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.12.0 (c (n "llama") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06f9hjl4bkvqrixlkkmbf15khbhp6qy4h8hr54ddxnwx1zkb7hz3") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.14.0 (c (n "llama") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^140.0.2") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1llrwx5c6pd0mf6zzqrkabq1iaz1y8z56qb0vpg97jxykv674b3g") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.14.1 (c (n "llama") (v "0.14.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^140.0.2") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ybl62chgj9z0x97w7fvrcscj532kdffv3l7ydc02w4jjywkgfgm") (f (quote (("docs-rs"))))))

(define-public crate-llama-0.14.2 (c (n "llama") (v "0.14.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "llvm-sys") (r "^140.0.2") (f (quote ("no-llvm-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0120hrcni6gxlqs8zsmr681930iy2dzgn1dgm3va7ybvvfvagsb4") (f (quote (("docs-rs"))))))

