(define-module (crates-io ll am llama2_rs) #:use-module (crates-io))

(define-public crate-llama2_rs-0.1.0 (c (n "llama2_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wide") (r "^0.7.11") (d #t) (k 0)))) (h "172q66kvf0aam79f1n0m5pk18il7f8dhxjipvjd9vvhxw8zx06jb") (y #t)))

(define-public crate-llama2_rs-0.1.1 (c (n "llama2_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wide") (r "^0.7.11") (d #t) (k 0)))) (h "13mk7wanq7xbaqii2838bv9lx0r5i0pzqdggbj2262s0zs1phaqx")))

