(define-module (crates-io ll am llame) #:use-module (crates-io))

(define-public crate-llame-0.1.0 (c (n "llame") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "ollama-rs") (r "^0.1.5") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "parking_lot"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0py4wxzikjlh5x5gxb4dzzckpv44qhwj7g6hikg6302356a8vsx5")))

