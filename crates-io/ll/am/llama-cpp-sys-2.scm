(define-module (crates-io ll am llama-cpp-sys-2) #:use-module (crates-io))

(define-public crate-llama-cpp-sys-2-0.1.0 (c (n "llama-cpp-sys-2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1jx9ninkr9flhngz9pwdixf6zw4njiiszhav6f6m4w7pcwvxvxk7") (f (quote (("cublas"))))))

(define-public crate-llama-cpp-sys-2-0.1.3 (c (n "llama-cpp-sys-2") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0wblw2w63cslakdfzbaff8vnapikfic8574sp92giwd46cyypv7r") (f (quote (("cublas"))))))

(define-public crate-llama-cpp-sys-2-0.1.4 (c (n "llama-cpp-sys-2") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "14cpz0g6qmg46kzzajf8ip2fmgh1xa9kv97fyjy720nsiygi23vk") (f (quote (("cublas"))))))

(define-public crate-llama-cpp-sys-2-0.1.5 (c (n "llama-cpp-sys-2") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1frww8g61bvxi5smcl4f07yyx0f0y49nb4akncwickdbcx783qnf") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.6 (c (n "llama-cpp-sys-2") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0bv057jnvnrpj5bgrb1z3jc0fwlb8sj3rngwhq41cmdz32gy14xh") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.7 (c (n "llama-cpp-sys-2") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0blkcbrja3isnbn91snfl6xb4sngc0akmw9974fd8z8jhvd24g86") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.8 (c (n "llama-cpp-sys-2") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1gn651xdw68a7mkl96x2zn5vraybmaqq3zr9rwxj1sjr2yx7alra") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.9 (c (n "llama-cpp-sys-2") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0nzrv58ahhghj2b194wwy6iv01nmr00bk6s574imjrfaryv484vi") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.10 (c (n "llama-cpp-sys-2") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1n34ar560dq2y9l216bz0rxs9nw4plsk9xbaln5cx07yigyqfp69") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.11 (c (n "llama-cpp-sys-2") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1glzaf44b7cb57wswmlamf0qfbzb3bknqvz4p4x2bfby65gxhm67") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.12 (c (n "llama-cpp-sys-2") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "083mni2rzng6jfc6rhry16adajvgnb47pw7wzsx8d3vqhks1w4kx") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.13 (c (n "llama-cpp-sys-2") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "07g7652mix6iazniz8baypsnfmlci3zdl3kzya97z5i4xdvxp67x") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.14 (c (n "llama-cpp-sys-2") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1kqijbbxcrdj18697pl8mawmxds7akb3n5afqf3ip28gmjzx3vy4") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.15 (c (n "llama-cpp-sys-2") (v "0.1.15") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0p4jyhqwja48x3s53a3jvl54qgajacnpbpfssls4hr6730wh7fwp") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.16 (c (n "llama-cpp-sys-2") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0ibfs7njnkw4xdx3drgfk99lhjnhd9hqvb5ilbnwnb88m48q4iss") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.17 (c (n "llama-cpp-sys-2") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "010fzyf2pr0gv91p7ci04inzdcc2gxkxcq7wv7yikcrjc6yc49z5") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.18 (c (n "llama-cpp-sys-2") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "06mdvlf4s1zyv9dqqr0sn86j0327c5w2gqr46yl9fv0k32z03l6a") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.19 (c (n "llama-cpp-sys-2") (v "0.1.19") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1vdksxx7lbx5dbk9583sb1g7m96pjj613fghzw74m0fj8xg06k8j") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.20 (c (n "llama-cpp-sys-2") (v "0.1.20") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1hwna807knwz4zgzbhi4jw8aqjxdmwnzyh3b66bhjca346lqlbkj") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.21 (c (n "llama-cpp-sys-2") (v "0.1.21") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "09iqkw15xdza0bslm3klkshn3lf4criywvlfqk6abm1fp9a1xdy0") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.22 (c (n "llama-cpp-sys-2") (v "0.1.22") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0vy0k2vvjlimi8m93y5lcx1yipvw666y3x9ximgbk7fhvlmzv9sg") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.25 (c (n "llama-cpp-sys-2") (v "0.1.25") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "08kifs8kl5mxw1rfhw6acc73z0zas6y2j2glckl32yz5svnskx8y") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.26 (c (n "llama-cpp-sys-2") (v "0.1.26") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0y6pc0hdryyh7xkkrrzpxjvxl5hv91jiazpd6n4s9cv946ykm883") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.27 (c (n "llama-cpp-sys-2") (v "0.1.27") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "07r81l22a23nxvjzqp4wzyzcxlff2sij74pckwq9b9zc6gwa6i1q") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.28 (c (n "llama-cpp-sys-2") (v "0.1.28") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1224hzci78a7zmyb45mawnr6ljp9l7wn9gcnkyjfzm48psw9mxrd") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.29 (c (n "llama-cpp-sys-2") (v "0.1.29") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0gnllcvfa7mcvbwcgbd76360x7hgzjl8rjcj7h6wx6vsgwb2413q") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.30 (c (n "llama-cpp-sys-2") (v "0.1.30") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0apxg0fqwzb6qcxvd1na26yvv2i84ha5m2frcchi72vaigximpk1") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.31 (c (n "llama-cpp-sys-2") (v "0.1.31") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0dncxfab595s9dxy672pndrv90wsni0f1bna3fcrhqpdmxakm0g1") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.32 (c (n "llama-cpp-sys-2") (v "0.1.32") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0v0c3y9pgq5h0f5knzppyhh8b2p9lapndz2ffarnrsa0s1nmsd4i") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.33 (c (n "llama-cpp-sys-2") (v "0.1.33") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1vg2xclcfc4rmzr9lr9bvvh8c9cnkh4gfwnxh8y2ij7703jckd1v") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.34 (c (n "llama-cpp-sys-2") (v "0.1.34") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1hprzkxj06rjmfs32pwz3rddazxqnw72v0a451rc0gh9rc9r0vjm") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.35 (c (n "llama-cpp-sys-2") (v "0.1.35") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1m4k58bkccisawasppgzkzf567yn3rmd7a2p17vaclnxmqkvyfdi") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.36 (c (n "llama-cpp-sys-2") (v "0.1.36") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "01fr885g50nrn4ba8nbgpj9h45ndd9rw9ygvf6bv1mdmcarc3fgd") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.37 (c (n "llama-cpp-sys-2") (v "0.1.37") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.88") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1friyl0kas915nr7y292vasfi6a6q03ljffr375cp246690s8gyg") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.38 (c (n "llama-cpp-sys-2") (v "0.1.38") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "14hvf4a7pqyg428mjpwkfwd8j46jhz9b4hkq19vcbbw3m5imaf7w") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.39 (c (n "llama-cpp-sys-2") (v "0.1.39") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0294v3c2y31vfkdv0b2xjivjs3i5ymfryh9yhpjbg662i9prxmg7") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.40 (c (n "llama-cpp-sys-2") (v "0.1.40") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "17ijmrnp4zqz84djldchmmps33ivd427kkwcx9r0qka70hk812lv") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.41 (c (n "llama-cpp-sys-2") (v "0.1.41") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "091mc0gyy2lfvlwrh3bkfycwwf5sav1j8l17nj28j77g8lmsf13l") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.42 (c (n "llama-cpp-sys-2") (v "0.1.42") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1r3vvbf515xcm2vinxmhml68iz782sclajks1njjfllq4xdfid03") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.43 (c (n "llama-cpp-sys-2") (v "0.1.43") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "12xhiw4bfp6p5pagrzxjg3q7iryginscsk846vns5gf893s2v1il") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.44 (c (n "llama-cpp-sys-2") (v "0.1.44") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "09lnc9zm43h1djnk46gjya33a8131mm87bjf267b4k9p15d51qw8") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.45 (c (n "llama-cpp-sys-2") (v "0.1.45") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0m3qhjfibs4fv3gx8j5gxi6x7v44sqq653wnvld3xy4ggwabkssa") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.46 (c (n "llama-cpp-sys-2") (v "0.1.46") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1igbch92jqbrfxm4mps77np947b6lgmhq2w156kzksj4qbqkap6y") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.47 (c (n "llama-cpp-sys-2") (v "0.1.47") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0dlwrgifbk674xd70i2r9r78rnsrsg25624zq5f6vhszfnn6mwni") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.48 (c (n "llama-cpp-sys-2") (v "0.1.48") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.91") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0jmk62q664fx6g10aab998mzaafi9dsjkc0gs0fk8rprzsvg9g6v") (f (quote (("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.49 (c (n "llama-cpp-sys-2") (v "0.1.49") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.91") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0ds1ssiwpcrsvgyc5cndczxljld471fb2d24i3f7yfpvcfm0dg3l") (f (quote (("metal") ("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.50 (c (n "llama-cpp-sys-2") (v "0.1.50") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1jkk4hnvdk283nbr4jywc8v5lf6y24k33spxm625w5a5zw8wmvb0") (f (quote (("metal") ("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.51 (c (n "llama-cpp-sys-2") (v "0.1.51") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1n8l10mm681ky0fc94nimfgb2sa1i7zpknzrgpnr183n1kcpdhvg") (f (quote (("metal") ("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.52 (c (n "llama-cpp-sys-2") (v "0.1.52") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1w1dfnlhzj0v4w5frkdxakf3fhnizqkcdc9pv6sfk7si00xfnawc") (f (quote (("metal") ("cublas")))) (l "llama")))

(define-public crate-llama-cpp-sys-2-0.1.53 (c (n "llama-cpp-sys-2") (v "0.1.53") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.94") (f (quote ("parallel"))) (d #t) (k 1)))) (h "19vys5g1r8c2lk4adqjnqblwvwc1ab8k9xwp3sh3b2cw6xxhzv0i") (f (quote (("metal") ("cublas")))) (l "llama")))

