(define-module (crates-io ll am llama-cpp-sys-3) #:use-module (crates-io))

(define-public crate-llama-cpp-sys-3-0.5.0 (c (n "llama-cpp-sys-3") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17m2c5v17gbc1h2g1dh3zzslxcr7dgqgcp4zm643ykffdikv853z") (f (quote (("native") ("cuda_f16" "cuda") ("cuda"))))))

(define-public crate-llama-cpp-sys-3-0.5.1 (c (n "llama-cpp-sys-3") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18rsf0gr1d9gk7v3ckwsdss76iv33sqcsqiqv695imxncd7qd1gl") (f (quote (("native") ("cuda_f16" "cuda") ("cuda"))))))

(define-public crate-llama-cpp-sys-3-0.5.2 (c (n "llama-cpp-sys-3") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kwjczjl57bykvdm9flxr5f570x3hak6y74d3hjq867idd5k6bxc") (f (quote (("native") ("cuda_f16" "cuda") ("cuda"))))))

