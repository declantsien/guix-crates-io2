(define-module (crates-io ll am llama-build) #:use-module (crates-io))

(define-public crate-llama-build-0.1.0 (c (n "llama-build") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1gc565yyi9j2qrbp2yfpg8i23x804ac89dxy4n620l43adw1h1n2")))

(define-public crate-llama-build-0.2.0 (c (n "llama-build") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "13bz4ry74aw3z1ikxm2qjkj0q05nm2cnnkn60bajlda8936i3lq4")))

(define-public crate-llama-build-0.3.0 (c (n "llama-build") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1vi49pkl2p7bsk9z2bn0h66xywm81dq0syxhi40mk8far7l9336s")))

(define-public crate-llama-build-0.4.0 (c (n "llama-build") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1aw46zffjk4bmpvzvrss4in6yznnwg6idsqqi403mz7g1fkfcyg6")))

(define-public crate-llama-build-0.5.0 (c (n "llama-build") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0v6fcvajxs6ay76l77yxhahl5sy8pmwk9fwp12jvgf3w1w5g81w3")))

(define-public crate-llama-build-0.7.0 (c (n "llama-build") (v "0.7.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1i1bv2xgja9912as7a2y7v7sq9zjg765c4nax2w6sjhnppvgdms3")))

(define-public crate-llama-build-0.8.0 (c (n "llama-build") (v "0.8.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "08aqm2q5rr14zvcxa968a1w224ix0s37780i97fsav05dqq9hq4r")))

(define-public crate-llama-build-0.9.0 (c (n "llama-build") (v "0.9.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1jqlla4cgxgdgqwk4wpd12kzwwjpl25y11clk77ci651312w9jlg")))

(define-public crate-llama-build-0.10.0 (c (n "llama-build") (v "0.10.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0s0wss47c9pnmswh6h63yhsfb72a1sxqjb40hzrjvc4f0l04h6xx")))

(define-public crate-llama-build-0.11.0 (c (n "llama-build") (v "0.11.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0gmf9mjp6wxa4m22ag41yggc0y93w3l1mws2y8m4z1rs3wpgfla7")))

(define-public crate-llama-build-0.11.1 (c (n "llama-build") (v "0.11.1") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0lbm9ic8zzqi7bx81vzvqzxl7g1ix4id1hmzcr0d8kjg3hcr3y7x")))

(define-public crate-llama-build-0.11.2 (c (n "llama-build") (v "0.11.2") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "1w0cv21ls4i0plv5vfvxgi26lisj69fzcgnai7zbaw9dvkkrm1dl")))

(define-public crate-llama-build-0.12.0 (c (n "llama-build") (v "0.12.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0gxswvmsvxbh68yyjhq1kxzawi2w7in4dq7aw7qggjhg5bdpbc8c")))

(define-public crate-llama-build-0.14.0 (c (n "llama-build") (v "0.14.0") (d (list (d (n "cc") (r "^1") (d #t) (k 0)))) (h "0qw4xla43v28gg3870c763f6ia5v9rqihpaag82l5n7p7kddj1mm")))

