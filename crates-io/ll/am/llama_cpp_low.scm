(define-module (crates-io ll am llama_cpp_low) #:use-module (crates-io))

(define-public crate-llama_cpp_low-0.3.0 (c (n "llama_cpp_low") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1zl83h1vmnzcvanisvjghm5ms108x9r1yiwjgkdsabl5pcwrsdb9") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.0-13 (c (n "llama_cpp_low") (v "0.3.0-13") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "17w90kpdxd8g8v427grhksyqlr9ravzxbiiaf2a53xwfklh2l0lf") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.1-alpha.0 (c (n "llama_cpp_low") (v "0.3.1-alpha.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1pl5jxrf9wf3bx17b5wd6sap9hz7p5sv1ink9rbmd673h9r5dn2q") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.1 (c (n "llama_cpp_low") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1f75hc7dqinqkry2466r17h046fxa3znzqfnrincjcnwvqd89pnv") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.2 (c (n "llama_cpp_low") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1qggs3awbblxp8xk4lfal97va0gzxb8gh64wrbd2810s97fbrndm") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.3 (c (n "llama_cpp_low") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1gd8r9qgsk9ww2m0s35f26lix43w36raw4w4am4jr9cs87vfl3xk") (f (quote (("default" "cuda") ("cuda"))))))

(define-public crate-llama_cpp_low-0.3.5 (c (n "llama_cpp_low") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "04cjrfbgbmvwvi9dny2d3kcg4hrp95k69g51i5p84vz7sph0irk9") (f (quote (("default" "cuda") ("cuda"))))))

