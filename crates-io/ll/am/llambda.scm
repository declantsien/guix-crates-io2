(define-module (crates-io ll am llambda) #:use-module (crates-io))

(define-public crate-llambda-0.1.0 (c (n "llambda") (v "0.1.0") (d (list (d (n "aws_lambda_events") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "netlify_lambda_http") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1kw7v5vrsaxh1k8sc796bbq3ci3r9f992dnqyajfqqnc7nnsxlqn")))

(define-public crate-llambda-0.1.1 (c (n "llambda") (v "0.1.1") (d (list (d (n "aws_lambda_events") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "netlify_lambda_http") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1wad9ls9vbkcybzppay88ng74zqc87msxlbnyc05cmm6f6mck8hq")))

(define-public crate-llambda-0.1.2 (c (n "llambda") (v "0.1.2") (d (list (d (n "aws_lambda_events") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "netlify_lambda_http") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0c13rhkvfgy91z8jdrq8kd3jkdr93iyg342kygn4a4mw2d8s8vr3")))

