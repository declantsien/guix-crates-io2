(define-module (crates-io ll am llama_cpp_rs) #:use-module (crates-io))

(define-public crate-llama_cpp_rs-0.1.0 (c (n "llama_cpp_rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yq9l0hw8n7cvyjpb3d1n3rwi0axcxfsliwksq75gnmxn3a8g547")))

(define-public crate-llama_cpp_rs-0.1.1 (c (n "llama_cpp_rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j0j5p5wb4ffhmbdn61cpgfn2nhd3h7435cgb728ww5n8qmsbcsk")))

(define-public crate-llama_cpp_rs-0.1.2 (c (n "llama_cpp_rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0zy26ifhz5bjrv2ln440x51ra32wbdk8j30if75hw887a4fwyh8s")))

(define-public crate-llama_cpp_rs-0.2.0 (c (n "llama_cpp_rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gsyw4zd9hblmacmifmqgdypcw2jz93i434yb6ymqcd63wpf186z") (f (quote (("opencl") ("openblas") ("cuda") ("blis"))))))

(define-public crate-llama_cpp_rs-0.3.0 (c (n "llama_cpp_rs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ifs6fpgvmdjisd63kiaqhjrmmi4f5maq8q1g0k6h1blqk82s9x6") (f (quote (("opencl") ("openblas") ("metal") ("cuda") ("blis"))))))

