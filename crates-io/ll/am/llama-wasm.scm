(define-module (crates-io ll am llama-wasm) #:use-module (crates-io))

(define-public crate-llama-wasm-0.1.0 (c (n "llama-wasm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.15") (d #t) (k 0)) (d (n "llama") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hvaxs28irsrmg8ig0shxy8588mkv3fpp1xshsb6s9f5fywwa1yw")))

(define-public crate-llama-wasm-0.2.0 (c (n "llama-wasm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.15") (d #t) (k 0)) (d (n "llama") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15axm887fywam6ci2d4i79ard0945fmnjakzqmlb52m82baq6vkb")))

(define-public crate-llama-wasm-0.3.0 (c (n "llama-wasm") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.15") (d #t) (k 0)) (d (n "llama") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qp2s4xbn9lwk4rm6jq64xcp555ziaal7kisz06v84sw0wwdac1m")))

(define-public crate-llama-wasm-0.4.0 (c (n "llama-wasm") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.15") (d #t) (k 0)) (d (n "llama") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "068k91gsmkvklf8adwl7vr5ygsa1cbd2whz2a4fsm13zspw1v595")))

(define-public crate-llama-wasm-0.5.0 (c (n "llama-wasm") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.15") (d #t) (k 0)) (d (n "llama") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mcidyivaabkbpnw3d23cbhwi7v5yh09rr7jndg43kmqlwz3kw9b")))

