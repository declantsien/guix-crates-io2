(define-module (crates-io ll pr llpr) #:use-module (crates-io))

(define-public crate-llpr-0.1.0 (c (n "llpr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1jmkxmkg289xp5r1zk53mrx2l1zglw96n4asg3q1l7dggvqdz0y4") (y #t)))

(define-public crate-llpr-0.1.1 (c (n "llpr") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "10m10gchk3cxvmgrf8wfvgv5x8haqjf5m8nr5f6jxy2f9g2vvikl") (y #t)))

(define-public crate-llpr-0.1.2 (c (n "llpr") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0w68ck1dzd1whz3mqyhw4lqhyzljv2pjlcw04mllaqnm63fyb5zm")))

