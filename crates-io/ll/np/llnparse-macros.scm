(define-module (crates-io ll np llnparse-macros) #:use-module (crates-io))

(define-public crate-llnparse-macros-0.0.1 (c (n "llnparse-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "1p33bm2va0j2bfgmgff5dzw7w9xmwvvwg6ycdc49fw42h8jq4rv9") (y #t)))

