(define-module (crates-io ll d_ lld_rs) #:use-module (crates-io))

(define-public crate-lld_rs-120.0.1 (c (n "lld_rs") (v "120.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "05cf38z8clhh3sxr8kqf92fn5nv2afh0smb3dibx2b1jirq7nivx")))

(define-public crate-lld_rs-130.0.0 (c (n "lld_rs") (v "130.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "01c092z9f7j8km1zk3nz493s1l47j2nbj76jmw05k5ch6za1dhyq")))

(define-public crate-lld_rs-140.0.0 (c (n "lld_rs") (v "140.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "llvm-sys") (r "^140.0.3") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "13xzaf4dh5ljy9rrlsgy3dvs1va0cfxbwqawbz2pkbcjjqp6pbxs")))

