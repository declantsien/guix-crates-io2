(define-module (crates-io ll ms llmservice-flows) #:use-module (crates-io))

(define-public crate-llmservice-flows-0.1.0 (c (n "llmservice-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0v3ma267whnzn9xrkqy9qcg1y3haw5ns2gnlfmg4akpcyg1f1bwx")))

(define-public crate-llmservice-flows-0.1.1 (c (n "llmservice-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1k2jlixsx38k9ac743axiwr57qqndf24lbx7q47bw0zl6mfij7a5")))

(define-public crate-llmservice-flows-0.1.2 (c (n "llmservice-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0vkhki3gh5jlg23s3zqj4262vjw2x1w1pj8xzsfnjnffcvljviba")))

(define-public crate-llmservice-flows-0.1.3 (c (n "llmservice-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0p8ci9ykhk8fdx46l0j0n6abknwmw775isy7lvliw80jkr7mlxl6")))

(define-public crate-llmservice-flows-0.2.0 (c (n "llmservice-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "17iiplc1asibbsm764s7wq047vlc8qybkkivd7cga55q0lp2p7wc")))

