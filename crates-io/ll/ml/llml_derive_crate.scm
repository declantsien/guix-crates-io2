(define-module (crates-io ll ml llml_derive_crate) #:use-module (crates-io))

(define-public crate-llml_derive_crate-0.1.0 (c (n "llml_derive_crate") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0zpvl3h2xdy44ngar8zz7p6ifgwqskkg1pcyby1y3f59mfmd612z")))

