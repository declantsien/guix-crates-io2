(define-module (crates-io ll ml llml_simd_proc) #:use-module (crates-io))

(define-public crate-llml_simd_proc-0.1.0 (c (n "llml_simd_proc") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00aaqr7w12fcn3zpydnm731fr39ixhdk1qbkrxw5sbadhli1izr8")))

(define-public crate-llml_simd_proc-0.1.1 (c (n "llml_simd_proc") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rh8d0xhrbbmrvb84i1jxn6vad9z92m03sf35fsdh03s1vmvk49n")))

(define-public crate-llml_simd_proc-0.1.2 (c (n "llml_simd_proc") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m44mc8kd60gspki1yaa3fx06pic1pwihvw6wzq250s64hyqklg8")))

(define-public crate-llml_simd_proc-0.1.3 (c (n "llml_simd_proc") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07l777l5fanliqdg5854hkb7q0lwy5nql0lq657yf6d66b09wwhg")))

