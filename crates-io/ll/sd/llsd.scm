(define-module (crates-io ll sd llsd) #:use-module (crates-io))

(define-public crate-llsd-0.1.0 (c (n "llsd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1brpina5ssq5vxjv0agk6855bfhp7fcp79mxx628ag0n18dfh0lr")))

