(define-module (crates-io ll lr lllreduce) #:use-module (crates-io))

(define-public crate-lllreduce-0.0.1 (c (n "lllreduce") (v "0.0.1") (h "1q1dbjzkyw0kq7fb10dh46mcw3yvnj7gqbwdg61ys1qchqv3dbwj")))

(define-public crate-lllreduce-0.0.2 (c (n "lllreduce") (v "0.0.2") (h "1q0b12zp44fi4jflwkd8zc8n9yhc6dmsaklys1c6cfx8bcb7253j")))

