(define-module (crates-io ll uv lluvia) #:use-module (crates-io))

(define-public crate-lluvia-0.0.1 (c (n "lluvia") (v "0.0.1") (h "045iv7nq4vs4j6qmlq86iwmh0xn9lr0scbwamf84dz9px623n6yv")))

(define-public crate-lluvia-0.0.2 (c (n "lluvia") (v "0.0.2") (h "0rcdxzxv3ij46f15azzd1vnck96rsabp4xnvcml6h974lk5kwrfy")))

