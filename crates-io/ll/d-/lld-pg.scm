(define-module (crates-io ll d- lld-pg) #:use-module (crates-io))

(define-public crate-lld-pg-0.1.0 (c (n "lld-pg") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "06mjnpr6glml4m030pvkl5rxh8gpwv9ymq4manzwk5bqz1jpn8pz")))

(define-public crate-lld-pg-0.1.1 (c (n "lld-pg") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1zgz4zxy583digynr3cnr6yf4b50a6kkskp815slz24qjqwhg0jw")))

(define-public crate-lld-pg-0.1.2 (c (n "lld-pg") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0xn94vzq9mryqcj877mn2a5rp183p7knc4l1rs32zl2gv484az9a")))

