(define-module (crates-io ll mv llmvm-util) #:use-module (crates-io))

(define-public crate-llmvm-util-0.1.0 (c (n "llmvm-util") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (f (quote ("toml"))) (o #t) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "multilink") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "016m9j0wjyyr2n44skm487bj8383yi0yl62xfvcnvpzmhvv9899m") (y #t) (s 2) (e (quote (("logging" "dep:tracing-subscriber") ("config" "dep:config" "dep:serde"))))))

(define-public crate-llmvm-util-0.1.1 (c (n "llmvm-util") (v "0.1.1") (d (list (d (n "config") (r "^0.13") (f (quote ("toml"))) (o #t) (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "multilink") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "0ly99icli8caj4l3j1cqm96gq0zi41lpr4z89ngms275mvsqp4pb") (s 2) (e (quote (("logging" "dep:tracing-subscriber") ("config" "dep:config" "dep:serde"))))))

