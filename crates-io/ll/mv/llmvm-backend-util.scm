(define-module (crates-io ll mv llmvm-backend-util) #:use-module (crates-io))

(define-public crate-llmvm-backend-util-0.1.0 (c (n "llmvm-backend-util") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "llmvm-protocol") (r "^1.0") (f (quote ("http-server" "stdio-server"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1x1y827mpg71cjawsw2ryfw0p23lf77h4vvr9r8avz5lcnaghs2d")))

(define-public crate-llmvm-backend-util-0.1.1 (c (n "llmvm-backend-util") (v "0.1.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "llmvm-protocol") (r "^2.0") (f (quote ("http-server" "stdio-server"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qs5vg3rncl873m1hzzvjn2mbg3z7vszh4nxjvnvvs2rrl9fb72c")))

