(define-module (crates-io ll -c ll-colors) #:use-module (crates-io))

(define-public crate-ll-colors-0.1.0 (c (n "ll-colors") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0zj2qdaw2ib8ncb2zim58ckmbqp9s1chk4zvbrbxqdfmmmsa3zl7")))

