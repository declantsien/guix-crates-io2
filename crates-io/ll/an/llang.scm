(define-module (crates-io ll an llang) #:use-module (crates-io))

(define-public crate-llang-0.0.1 (c (n "llang") (v "0.0.1") (d (list (d (n "clap") (r "^2.23.3") (d #t) (k 0)) (d (n "llvm") (r "^0.0.1") (d #t) (k 0)) (d (n "llvm-sys") (r "^36") (d #t) (k 0)) (d (n "nom-lua") (r "^0.0.2") (f (quote ("graphviz"))) (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1m5wq8iwmysgkkzyiypcjjvyzbg81pyb6djg6mknrhpl20nm7v4x")))

