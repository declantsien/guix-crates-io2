(define-module (crates-io ll vm llvm-symbolizer-rust-wrapper) #:use-module (crates-io))

(define-public crate-llvm-symbolizer-rust-wrapper-0.1.0 (c (n "llvm-symbolizer-rust-wrapper") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (f (quote ("file_appender"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "which") (r "^5") (f (quote ("regex"))) (d #t) (k 0)))) (h "1jfpglgiibh2wmcki287ing9hi7wdbph65897gkdc4ak42s6rwx5")))

