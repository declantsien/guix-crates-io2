(define-module (crates-io ll vm llvm-plugin-inkwell-internals) #:use-module (crates-io))

(define-public crate-llvm-plugin-inkwell-internals-0.5.0 (c (n "llvm-plugin-inkwell-internals") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1mkjd3aa7kmj1jfi3kqvqmpyfgzzap37cpp64ywb7vw78b66ylr6") (f (quote (("nightly"))))))

(define-public crate-llvm-plugin-inkwell-internals-0.6.0 (c (n "llvm-plugin-inkwell-internals") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09ah08mvl5rpjvpppidlsgcbbdbl7hx7zzil38bpcpp6ps60qvqk") (f (quote (("nightly"))))))

(define-public crate-llvm-plugin-inkwell-internals-0.7.0 (c (n "llvm-plugin-inkwell-internals") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "03hix8fssqk93k8d7nsz6s854cm5k005ch25hlydibnwkjzhyk3b") (f (quote (("nightly"))))))

