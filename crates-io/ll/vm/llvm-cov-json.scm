(define-module (crates-io ll vm llvm-cov-json) #:use-module (crates-io))

(define-public crate-llvm-cov-json-0.1.0 (c (n "llvm-cov-json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "17f5l06mb1vvfm32sql3ypy3vwg3jvcg6h1z27m1sk6lyyjxm8yx")))

(define-public crate-llvm-cov-json-0.1.1 (c (n "llvm-cov-json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0n90jaxv28rvqcxs5ad20rbiasszgdd8kzgkmzwfnaqzsfp3g02l")))

