(define-module (crates-io ll vm llvm-bitcursor) #:use-module (crates-io))

(define-public crate-llvm-bitcursor-0.0.2-rc.2 (c (n "llvm-bitcursor") (v "0.0.2-rc.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17injc00i9w4zcczqpklf6bklpyfdlg5ly428zyqixps3khwqsk5") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.3 (c (n "llvm-bitcursor") (v "0.0.2-rc.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d9jfidywn25g6fb4crglx1rpcm744k703q70x70vsmsfd451rwv") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.4 (c (n "llvm-bitcursor") (v "0.0.2-rc.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "182n0fsi96w248kysvwnq3mh6lgi5ji0a7hm4ilhn3p3qmsdfwgp") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.5 (c (n "llvm-bitcursor") (v "0.0.2-rc.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05r31ghz8qldbckbrj31lsn28r6fcgpdnjmync28lvgkpc2k9qam") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.6 (c (n "llvm-bitcursor") (v "0.0.2-rc.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lg22gvsfz8z5lzg6xmscaraziz0d9a4lmdf73c0s6yxi3pc3fy9") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.9 (c (n "llvm-bitcursor") (v "0.0.2-rc.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "084ppf1gq28jmap2i29sm0ifbjfkxcvgmb6v5iivd34zxnbrq950") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2-rc.10 (c (n "llvm-bitcursor") (v "0.0.2-rc.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "102p9f87pzw26kb5ysj95i9qz7p9cq0wizrq8m5g261y37q62xnq") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.2 (c (n "llvm-bitcursor") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14g23m8kfavwc25bvrra7nzn63809yja8crc15af7zfrqkqf5yfz") (f (quote (("vbr") ("default" "vbr"))))))

(define-public crate-llvm-bitcursor-0.0.3 (c (n "llvm-bitcursor") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y2rdcvpkg3dfvhl9nb3jqfmb6cyjdxpmglik4lhs9in6ciqnhpv") (f (quote (("vbr") ("default" "vbr"))))))

