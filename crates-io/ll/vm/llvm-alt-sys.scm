(define-module (crates-io ll vm llvm-alt-sys) #:use-module (crates-io))

(define-public crate-llvm-alt-sys-0.1.4 (c (n "llvm-alt-sys") (v "0.1.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 1)))) (h "00j8mrqz1cw8c94ar6yv1iz00qah9yzyrb352f1c9p7jrgwbhqfv") (f (quote (("llvm-dylib")))) (y #t)))

