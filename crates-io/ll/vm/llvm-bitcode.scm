(define-module (crates-io ll vm llvm-bitcode) #:use-module (crates-io))

(define-public crate-llvm-bitcode-0.1.0 (c (n "llvm-bitcode") (v "0.1.0") (h "0x2gj4932r966n5m678f652h2c6na8sfgdsf7msfcgp83qgmnv5n")))

(define-public crate-llvm-bitcode-0.1.1 (c (n "llvm-bitcode") (v "0.1.1") (h "0mhgaqq4mxbgcn7kgrns6gg461cdd1nc74rpjdpz2vfhk91c35a2")))

(define-public crate-llvm-bitcode-0.1.2 (c (n "llvm-bitcode") (v "0.1.2") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1fskb2nzddg8m038kh2lm91gb5yg0l4j3rcnv44kz7f37kcxz5cb")))

(define-public crate-llvm-bitcode-0.1.3 (c (n "llvm-bitcode") (v "0.1.3") (d (list (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)))) (h "0r91jw0b4v5y1v9wkm0m5bv5vvrh89dyikf4qw4v297ryydh0yf8")))

