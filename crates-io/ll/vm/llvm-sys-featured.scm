(define-module (crates-io ll vm llvm-sys-featured) #:use-module (crates-io))

(define-public crate-llvm-sys-featured-0.1.0 (c (n "llvm-sys-featured") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "semver") (r "^0.10") (d #t) (k 1)))) (h "0qgx849yxvm7fmgmfs8ivqx1l541dlipsns8ha7k60hy3vfrkyak") (f (quote (("strict-versioning") ("no-llvm-linking") ("llvm-9") ("llvm-8") ("llvm-10") ("disable-alltargets-init")))) (l "llvm")))

(define-public crate-llvm-sys-featured-0.1.1 (c (n "llvm-sys-featured") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "semver") (r "^0.10") (d #t) (k 1)))) (h "03ar2fy564hkizqibjbzz6gvwvpvlv2aqirapq7yj8r8xcyai31b") (f (quote (("strict-versioning") ("no-llvm-linking") ("llvm-9") ("llvm-8") ("llvm-10") ("disable-alltargets-init")))) (l "llvm")))

