(define-module (crates-io ll vm llvm_stackmap) #:use-module (crates-io))

(define-public crate-llvm_stackmap-0.1.0 (c (n "llvm_stackmap") (v "0.1.0") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "goblin") (r "~0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q0cd87fh7zvhkwq8cznjmc1p60p5xzjamci0shx5lfqn6yll8li") (f (quote (("serde-support" "serde") ("from-elf" "goblin") ("default" "serde-support" "from-elf")))) (y #t)))

(define-public crate-llvm_stackmap-0.1.1 (c (n "llvm_stackmap") (v "0.1.1") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "goblin") (r "~0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "~1") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g5szmwnpysxrfkc2cqdfa6bw3x24sc0zjc1ymwk86qylyrbkx3p") (f (quote (("serde-support" "serde") ("from-elf" "goblin") ("default" "from-elf")))) (y #t)))

(define-public crate-llvm_stackmap-0.1.2 (c (n "llvm_stackmap") (v "0.1.2") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "goblin") (r "~0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10i55jxpfjmf2yx58c1y0ysj0dz8g4dwn7nihda29xdw774gfmsq") (f (quote (("serde-support" "serde") ("from-elf" "goblin") ("default" "from-elf")))) (y #t)))

(define-public crate-llvm_stackmap-0.1.3 (c (n "llvm_stackmap") (v "0.1.3") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "goblin") (r "~0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d0xns1w7gp3j1bqqgsmca6p3qjarg0j7h7v0mhaa41rxafnpr0x") (f (quote (("serde-support" "serde") ("from-elf" "goblin") ("default" "from-elf")))) (y #t)))

(define-public crate-llvm_stackmap-0.1.4 (c (n "llvm_stackmap") (v "0.1.4") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "goblin") (r "~0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0iddxzfa82rbfybbw6hz28ddlnqzbj20jhj7vvvmhcx7xs11plws") (f (quote (("from-elf" "goblin") ("default" "from-elf")))) (s 2) (e (quote (("serde" "dep:serde"))))))

