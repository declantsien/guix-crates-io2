(define-module (crates-io ll vm llvm-wrapper) #:use-module (crates-io))

(define-public crate-llvm-wrapper-0.1.0 (c (n "llvm-wrapper") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "llvm-sys") (r "^110.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1scn89xk0yh1177i9pg1c981f8pg4z6672d2d0cmxgr9yi3m4gdq")))

(define-public crate-llvm-wrapper-0.1.1 (c (n "llvm-wrapper") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "llvm-sys") (r "^110.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dsg04xdyjl6iasjdn0ad3jfm5vj4mww03g7qjizqzpqk2dl0xnh")))

(define-public crate-llvm-wrapper-0.1.11 (c (n "llvm-wrapper") (v "0.1.11") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "llvm-sys") (r "^110.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p10k3wx6dwnldrj71wkqf644dh182rqv8fg5llcvb2p7lm0qnmp")))

(define-public crate-llvm-wrapper-0.1.12 (c (n "llvm-wrapper") (v "0.1.12") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "llvm-sys") (r "^110.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ni4m5ciinhhc1ygihsd5x8xpwgzw4s0f3y6zdd2bw0shlrz5yz7")))

(define-public crate-llvm-wrapper-0.1.13 (c (n "llvm-wrapper") (v "0.1.13") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "llvm-sys") (r "^110.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ms427wx1frm04h3l94db6q1yk1jnl5w0hjiv5ksix5gn0x08hf0")))

