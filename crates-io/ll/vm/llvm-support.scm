(define-module (crates-io ll vm llvm-support) #:use-module (crates-io))

(define-public crate-llvm-support-0.0.2-rc.1 (c (n "llvm-support") (v "0.0.2-rc.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15s6brj9z1gws266pjfbhaa9az1cjpaf0cwx631v1r77rw7g8pb6")))

(define-public crate-llvm-support-0.0.2-rc.2 (c (n "llvm-support") (v "0.0.2-rc.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06ii5w4f5sj19jz8cmnznxskrczpx4qgj8ni3w1wi7zcqs2g5dq1")))

(define-public crate-llvm-support-0.0.2 (c (n "llvm-support") (v "0.0.2") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z468kjjm1f8q6wlnrwhaz2p50qx8qkjq17cjwff3qlylkmxpdba")))

(define-public crate-llvm-support-0.0.3 (c (n "llvm-support") (v "0.0.3") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m75fasm02vjy45xxcr9kmvmzm3k5rcv9ydzcdwrng08n3zdz9kn")))

