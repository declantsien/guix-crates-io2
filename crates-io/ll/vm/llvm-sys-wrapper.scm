(define-module (crates-io ll vm llvm-sys-wrapper) #:use-module (crates-io))

(define-public crate-llvm-sys-wrapper-0.1.0 (c (n "llvm-sys-wrapper") (v "0.1.0") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "1xw116x18acczwyas4y30cgavj1j6glcjv6fwns22vnw1jgmy3ws")))

(define-public crate-llvm-sys-wrapper-0.1.1 (c (n "llvm-sys-wrapper") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "11bglb36ypzj2h7b7c27i9mfv6lkcr0k55938daxzvy528w4ax18")))

(define-public crate-llvm-sys-wrapper-0.1.2 (c (n "llvm-sys-wrapper") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "15ni0w2m758nnywb643w81114n7jn5w6592kfzky6596nrzf1g1k")))

(define-public crate-llvm-sys-wrapper-0.2.0 (c (n "llvm-sys-wrapper") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0766lxl3qc5saclphqsyzwhd2yv3s94szqlik4f6dkzxwh0x48yn")))

(define-public crate-llvm-sys-wrapper-0.2.1 (c (n "llvm-sys-wrapper") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "05771xy2dw7sv4cflpqqr2w71pa428mvh07jxy90bmhqvjhx98xs")))

(define-public crate-llvm-sys-wrapper-0.2.2 (c (n "llvm-sys-wrapper") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0zdvk2vss547n0z9ffzzhxpw378p057314msx7kkyvfkbnzm3a1b")))

(define-public crate-llvm-sys-wrapper-0.2.3 (c (n "llvm-sys-wrapper") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0vbd1iaqlj5w29hjk8y2x9aqapb84kvncxl1y53l03p4rd9msgac")))

(define-public crate-llvm-sys-wrapper-0.2.4 (c (n "llvm-sys-wrapper") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0b12d83qynnxlmy6sc0n4ccj91c3jkzkxarqwfip3a0rppa1dwik")))

(define-public crate-llvm-sys-wrapper-0.2.5 (c (n "llvm-sys-wrapper") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "1aa2q6ijfg9ycpcg3qfha07sl5vqi03j6nqz7l5b5ij05m2ima1d")))

(define-public crate-llvm-sys-wrapper-0.2.6 (c (n "llvm-sys-wrapper") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "03a2f106yv5a7mzrar8ipy321i2lxgri7f8mg1n7im5v4svx2hym")))

(define-public crate-llvm-sys-wrapper-0.2.7 (c (n "llvm-sys-wrapper") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "1wa27qldkx9hb0kza7fwzkm7llycyzwqdf4adpv6k2k4id570y7a")))

(define-public crate-llvm-sys-wrapper-0.2.8 (c (n "llvm-sys-wrapper") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "02h8g2r639s0sld6vz7gxf8y4v00q72i6y5xrr162h388qyrh7ca")))

(define-public crate-llvm-sys-wrapper-0.2.9 (c (n "llvm-sys-wrapper") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0x7s6y9hhhdfwifc7kvr2sv6c51fyjm5g3js8zi1kv7wk27i9920")))

(define-public crate-llvm-sys-wrapper-0.6.0 (c (n "llvm-sys-wrapper") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "0bm6066h5cg90igfj7ksmxfjdmsrxgq65hr35z9b850qk10cj4y4")))

(define-public crate-llvm-sys-wrapper-0.6.1 (c (n "llvm-sys-wrapper") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^60") (d #t) (k 0)))) (h "11p4c5x9wyabaxii30jrrs0p84qxs9f9d9nqgpdwl40khmlfz66h")))

