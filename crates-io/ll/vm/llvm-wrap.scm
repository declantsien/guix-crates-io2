(define-module (crates-io ll vm llvm-wrap) #:use-module (crates-io))

(define-public crate-llvm-wrap-0.2.1 (c (n "llvm-wrap") (v "0.2.1") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "0zz0r94k9l6fcd4smqy59pjnfc07ykjykksbkvgjrlaf65r2qscv")))

(define-public crate-llvm-wrap-0.2.2 (c (n "llvm-wrap") (v "0.2.2") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "0c9qpzvhxsnw2jgil5rmzvhgwlx03q9xrgg4nqjl56pqaw5c83al")))

(define-public crate-llvm-wrap-0.2.3 (c (n "llvm-wrap") (v "0.2.3") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "12x87sxxv1cc4krabgrn0pgmb91dn42s4f2bkzhl3mbrnk6q7faa")))

(define-public crate-llvm-wrap-0.2.4 (c (n "llvm-wrap") (v "0.2.4") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "13xm8bwislqpxkg9vna4d64bpjjybc5978v3gl40f9i7wvq4y5wn")))

(define-public crate-llvm-wrap-0.2.5 (c (n "llvm-wrap") (v "0.2.5") (d (list (d (n "llvm-sys") (r "^50") (d #t) (k 0)))) (h "0l03hkbdy37k3jqw1l7jn7976r2wxlvr3irx5462vak58kv24c9n")))

