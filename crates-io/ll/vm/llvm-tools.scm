(define-module (crates-io ll vm llvm-tools) #:use-module (crates-io))

(define-public crate-llvm-tools-0.1.0 (c (n "llvm-tools") (v "0.1.0") (h "1bp2l8iwsx9w9nvrriacbjiprc2yn0m3m62fsx8w3yqgma889z7w")))

(define-public crate-llvm-tools-0.1.1 (c (n "llvm-tools") (v "0.1.1") (h "1brg624p3gmqvrjyhwv0q8mix4agjr3wnnhn4zqwlr84rb8fanwm")))

