(define-module (crates-io ll vm llvm-constants) #:use-module (crates-io))

(define-public crate-llvm-constants-0.0.2-rc.2 (c (n "llvm-constants") (v "0.0.2-rc.2") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)))) (h "1hn8dwssmnr16hqy4fggl1if6y03j8402i6bdwd025sphkd5r13c")))

(define-public crate-llvm-constants-0.0.2-rc.3 (c (n "llvm-constants") (v "0.0.2-rc.3") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)))) (h "1bk94nwkw62wcjjy5adpbkcq6j9grdrm47c7zd49nprf5rghdpkb")))

(define-public crate-llvm-constants-0.0.2-rc.4 (c (n "llvm-constants") (v "0.0.2-rc.4") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)))) (h "1m27g867k055zs1irqkbkqs1yzhlqw3kahxdnqzd136dvc3d9l84")))

(define-public crate-llvm-constants-0.0.2 (c (n "llvm-constants") (v "0.0.2") (d (list (d (n "num_enum") (r "^0.5.3") (d #t) (k 0)))) (h "0h6dnj7gi114kp7x8m3ak2psza2lwbcjzizk16v4ji66ijg573n8")))

