(define-module (crates-io ll vm llvm-alt) #:use-module (crates-io))

(define-public crate-llvm-alt-0.1.0 (c (n "llvm-alt") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0ibpyxlccs9sv3rkmasjlgvc93b7db4qwm66b6n7nsskrrnvs50x")))

(define-public crate-llvm-alt-0.1.1 (c (n "llvm-alt") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "068ggryzhibv7ahck404cf54r0r7qgchgbz2a74my1i1gm4j1h1z")))

(define-public crate-llvm-alt-0.1.2 (c (n "llvm-alt") (v "0.1.2") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0wjd5ra455l3v105l23xvg8s076dfb5yyxkrg4ln5gfvamlz49sb")))

(define-public crate-llvm-alt-0.1.3 (c (n "llvm-alt") (v "0.1.3") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0w00cl966ygi2d2l75s2kwk9k8vs90dagihhrbbw6h8dmvax3m4v")))

(define-public crate-llvm-alt-0.1.4 (c (n "llvm-alt") (v "0.1.4") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0dnzg01nzjm08ip3fb7r036hr97dniw1ymp94wxbardf2cpanck3")))

(define-public crate-llvm-alt-0.1.5 (c (n "llvm-alt") (v "0.1.5") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0swj2nskcmrr7a7bhp5kg3rbba80m159nz1yfxr8ppm6gvrrn8gw")))

(define-public crate-llvm-alt-0.1.6 (c (n "llvm-alt") (v "0.1.6") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "08s7ydscsxlzq50n12skj8pa1vsci5g49kkbap1x9cxrd33dq2vz")))

(define-public crate-llvm-alt-0.1.7 (c (n "llvm-alt") (v "0.1.7") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "02kv1y49xfwl1lfc0wwn4l9ahm4xs82n0pyfhmjdc303l6mhdcpy")))

(define-public crate-llvm-alt-0.1.8 (c (n "llvm-alt") (v "0.1.8") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0a5bak5hxb5crqry09b31y2i51kfzm2qgzj700vzzdbbd0qlgmfd")))

(define-public crate-llvm-alt-0.1.9 (c (n "llvm-alt") (v "0.1.9") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0ghyfsfpldl7bz66bams5chhdv3swa49f3p3j4sajpbqd5hacjgn")))

(define-public crate-llvm-alt-0.2.0 (c (n "llvm-alt") (v "0.2.0") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "001vwnaha7syvdqwlp6177ngydpgvkf8px3h0fim6j13ix3v4lp7")))

(define-public crate-llvm-alt-0.2.1 (c (n "llvm-alt") (v "0.2.1") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "1h1xyf5z2zf9njjv4wrr7avcci25q9d5vhy3zsc4qp052jk424yf")))

(define-public crate-llvm-alt-0.2.2 (c (n "llvm-alt") (v "0.2.2") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "11vf3p2h5qii42krxkyd99na0qxm7kkb0907hrngf7fnmk5wbbfy")))

(define-public crate-llvm-alt-0.2.3 (c (n "llvm-alt") (v "0.2.3") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0i5fmc1nrb66z2c5zamwk1lspdh4z3z57yrkqkb8mn9x5v57985m")))

(define-public crate-llvm-alt-0.2.4 (c (n "llvm-alt") (v "0.2.4") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "01ciy4rbgqw6c9vyqiylk10g9i3khri3l5pz8bcaf58yckbjy2jj")))

(define-public crate-llvm-alt-0.2.5 (c (n "llvm-alt") (v "0.2.5") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "14c36xmnpvzkccg52vdzxdqrklzym4w2n4l60w1rmv4j7p31x259")))

(define-public crate-llvm-alt-0.2.6 (c (n "llvm-alt") (v "0.2.6") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "12iblb0qbiqdk4pskd7mmblpa4kc6d0najq3spssvaadcvn6lxcm")))

(define-public crate-llvm-alt-0.2.7 (c (n "llvm-alt") (v "0.2.7") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "1s5hb3bj3pdc4p3pwgp27v7k7bj0w30ga86f872n399gsxx6yi2p")))

(define-public crate-llvm-alt-0.2.8 (c (n "llvm-alt") (v "0.2.8") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "01y24n4zyr7nymd09p7baw5ywrzz8g8ks74djajm0xsklsipnfq0")))

(define-public crate-llvm-alt-0.2.9 (c (n "llvm-alt") (v "0.2.9") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0x9g6xkj3qm4rmsahqzpqyq5fbjfj991vgdvhdq41jak1z472mpi")))

(define-public crate-llvm-alt-0.3.0 (c (n "llvm-alt") (v "0.3.0") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "04apyzbahxbcqm6ykq0n36iar2sd8l7kb79mz71yf5q3jcldby1m")))

(define-public crate-llvm-alt-0.3.1 (c (n "llvm-alt") (v "0.3.1") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-alt-sys") (r "*") (d #t) (k 0)))) (h "0vfss5qrb8r889p01xwnxg5jfi25lmk2i1b16fmwz47nkgk9zfrf")))

(define-public crate-llvm-alt-0.3.2 (c (n "llvm-alt") (v "0.3.2") (d (list (d (n "cbox") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "llvm-sys") (r "*") (d #t) (k 0)))) (h "196afv4pk546n0r23gkkijbv7ygh781sl6hglzgyzp4qfg2rbp12")))

(define-public crate-llvm-alt-0.4.0 (c (n "llvm-alt") (v "0.4.0") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "0.*") (d #t) (k 0)))) (h "1p3jiv43mb99i4v8c1ixsf8fk0wqk2drm7h02mmg7dxd48l3h65w")))

(define-public crate-llvm-alt-0.5.0 (c (n "llvm-alt") (v "0.5.0") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "0.*") (d #t) (k 0)))) (h "15g6ym21xkgc1icl983vkh5rhsnfsv1aikvkgjgkzvk7scnlv922")))

