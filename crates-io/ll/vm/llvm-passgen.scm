(define-module (crates-io ll vm llvm-passgen) #:use-module (crates-io))

(define-public crate-llvm-passgen-0.1.0 (c (n "llvm-passgen") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vk1zya324pbzk822ppnw891nxly92iivn31l025lyk1rkljmb06")))

(define-public crate-llvm-passgen-0.2.0 (c (n "llvm-passgen") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x80bdhd8wq9k6x6dgj0d0hp1b4pxxisj7nzbfyhina5vkclwm01")))

(define-public crate-llvm-passgen-0.2.1 (c (n "llvm-passgen") (v "0.2.1") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "handlebars") (r "^3.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h1f9cxpq0maick06hvs51yp44g4cyd70qvjs23m890r1map6j85")))

(define-public crate-llvm-passgen-0.3.0 (c (n "llvm-passgen") (v "0.3.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "handlebars") (r "^4.2.1") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f0fsf785jh12af77l4jnkj71kxnxhccw3rgw87gsyn7c8avg8rv")))

(define-public crate-llvm-passgen-0.4.0 (c (n "llvm-passgen") (v "0.4.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "handlebars") (r "^4.2.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.3.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14fm4sd92zk14m7rcjn8yvp9a5bfhyaxh4rpwjlc2cxb3p4z94b4")))

