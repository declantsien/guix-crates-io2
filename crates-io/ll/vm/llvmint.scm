(define-module (crates-io ll vm llvmint) #:use-module (crates-io))

(define-public crate-llvmint-0.0.1 (c (n "llvmint") (v "0.0.1") (d (list (d (n "simdty") (r "^0") (d #t) (k 0)))) (h "0kibm34drg99xbfjqfz564j68n45h6hgfq5dmb0qxnsbzxcbfhva")))

(define-public crate-llvmint-0.0.2 (c (n "llvmint") (v "0.0.2") (d (list (d (n "simdty") (r "^0") (d #t) (k 0)))) (h "1x4fjncklmaxf376kw22ba9xmhrm0d97ili1qlw1vvxiqnhz0bbp")))

(define-public crate-llvmint-0.0.3 (c (n "llvmint") (v "0.0.3") (d (list (d (n "simdty") (r "^0") (d #t) (k 0)))) (h "1pind0i737g957di4gkc0mddm1l38kprb45lpqmwlp1bn31cri3w") (f (quote (("unstable"))))))

