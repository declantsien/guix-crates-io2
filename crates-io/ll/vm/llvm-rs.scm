(define-module (crates-io ll vm llvm-rs) #:use-module (crates-io))

(define-public crate-llvm-rs-0.1.0 (c (n "llvm-rs") (v "0.1.0") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "^40.0.6") (d #t) (k 0)))) (h "1ybjyhghh8blzlrsn7mw4h1lq824i63yfa7p7cy5l1wlz47rp1dm") (f (quote (("expose_bindings") ("default"))))))

(define-public crate-llvm-rs-0.1.1 (c (n "llvm-rs") (v "0.1.1") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "^40.0.6") (d #t) (k 0)))) (h "159698byrqmrna0z87g4ba88klc6s6vzjw6jscrxar1wiyrbjfmd") (f (quote (("expose_bindings") ("default"))))))

(define-public crate-llvm-rs-0.1.2 (c (n "llvm-rs") (v "0.1.2") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "^40.0.6") (d #t) (k 0)))) (h "1cxmz977i98vmknf6y6ncj5mc4qbppwp6c4r9g4z9yag0v4nc13a") (f (quote (("expose_bindings") ("default"))))))

(define-public crate-llvm-rs-0.1.3 (c (n "llvm-rs") (v "0.1.3") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "^40.0.6") (d #t) (k 0)))) (h "1jp5lvx2iy48dfd1h37jk5fc74pdcs5zf5zfgw3l83nvw8lj03aw") (f (quote (("expose_bindings") ("default"))))))

(define-public crate-llvm-rs-0.2.1 (c (n "llvm-rs") (v "0.2.1") (d (list (d (n "cbox") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "llvm-sys") (r "^70") (d #t) (k 0)))) (h "0vq3ki0c7fwy4zxwbcamcpn1q9q65n91b1jv5qs984gixxm5xc0c") (f (quote (("expose_bindings") ("default"))))))

