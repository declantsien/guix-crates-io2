(define-module (crates-io ll vm llvm-scratch) #:use-module (crates-io))

(define-public crate-llvm-scratch-0.1.0 (c (n "llvm-scratch") (v "0.1.0") (h "10cdf8ljdi5vz5cwc3j4l5fjh3nlnz2qf39pdv26p5xh9651d8an")))

(define-public crate-llvm-scratch-0.1.1 (c (n "llvm-scratch") (v "0.1.1") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "10qizcmcwv2fnscjspafnljxj4s7bqi8fv439q6p8hb49mfgxycj")))

(define-public crate-llvm-scratch-0.1.2 (c (n "llvm-scratch") (v "0.1.2") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "17hi5xcxcal6f5s5v79jpg217840a63bc7bfc03idbbq28cmml13")))

(define-public crate-llvm-scratch-0.1.3 (c (n "llvm-scratch") (v "0.1.3") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "11w119jbph1ibzpm70cinfxk9fndvgpd829pg3nhrygpddvsw163")))

(define-public crate-llvm-scratch-0.1.4 (c (n "llvm-scratch") (v "0.1.4") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "1nhc3qr98xq7xhd769yv2gvj77xv75jhvzi6sp0r78h7inhrg1vx")))

(define-public crate-llvm-scratch-0.1.5 (c (n "llvm-scratch") (v "0.1.5") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0y1j1sjy7mfci8va759jz13lc6if6kj5klzi7fxn435l07j9qzk0")))

(define-public crate-llvm-scratch-0.1.6 (c (n "llvm-scratch") (v "0.1.6") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0fgmyq3d30bcjaqw9z95zd02gvqlinr42v58xlwkgdqm9hli3r1w")))

(define-public crate-llvm-scratch-0.1.7 (c (n "llvm-scratch") (v "0.1.7") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "1cml1iqqa0y6yvnx6xz3r5rz86xdj2cj0sqswkyg7ggljcr5hi0i")))

(define-public crate-llvm-scratch-0.1.8 (c (n "llvm-scratch") (v "0.1.8") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0iirxwx76higqycg3cmy597fn72g2x0cpr9yvbv9ap4rja9y2bbz")))

(define-public crate-llvm-scratch-0.1.9 (c (n "llvm-scratch") (v "0.1.9") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "10z1yr5ch6q5g1gkrln4csqax1wki6m2dglp20pchqk6fyjcdvjm")))

(define-public crate-llvm-scratch-0.1.10 (c (n "llvm-scratch") (v "0.1.10") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "1vh2yq0r0yc56qlxj6zndnhn7rh6v3dggrmww7vcifhs23fqlaqa")))

(define-public crate-llvm-scratch-0.1.11 (c (n "llvm-scratch") (v "0.1.11") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "1n1fflj2rbbbyypkkfc5hgmnbdzvwjz2ja4p77vngrq801m8shx7")))

(define-public crate-llvm-scratch-0.1.12 (c (n "llvm-scratch") (v "0.1.12") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0y273qaq97352z8hqxqwjrhfys6a6i9y9g3x45b7f9cm6cnp2c4h")))

(define-public crate-llvm-scratch-0.1.13 (c (n "llvm-scratch") (v "0.1.13") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "123g7w4sj5j7vknggnnnixql8w3k5x6c6zd9mcwqnxdjncb6kq71")))

(define-public crate-llvm-scratch-0.1.15 (c (n "llvm-scratch") (v "0.1.15") (d (list (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)))) (h "0ywssppczqqkjmlrlhifilcpg1gabw1n59678dn465z5h2gczsa0")))

