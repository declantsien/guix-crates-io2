(define-module (crates-io ll vm llvm-plugin-macros) #:use-module (crates-io))

(define-public crate-llvm-plugin-macros-0.1.0 (c (n "llvm-plugin-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c7pjf06fhxaykdjzdrkczbjf5w8vnml15iqbz11z857frn5rcf9")))

(define-public crate-llvm-plugin-macros-0.2.0 (c (n "llvm-plugin-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vi14x2vwqi0b2qjmwx9bjzf8mqg8xrn0nhkh2v0ia6i2hjp0f7v")))

