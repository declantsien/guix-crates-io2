(define-module (crates-io fi tn fitness_coder) #:use-module (crates-io))

(define-public crate-fitness_coder-0.1.0 (c (n "fitness_coder") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "10h8lf7y5d0549qwkx9hsig9z9hz2q07fijl43dsm8gyjgciik0l")))

(define-public crate-fitness_coder-0.1.1 (c (n "fitness_coder") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)))) (h "1ali6w0hhpdbiw46y829n5lqcrwc95y9rzya4g94rkjmlrp323aj")))

