(define-module (crates-io fi tn fitnesstrax-lib) #:use-module (crates-io))

(define-public crate-fitnesstrax-lib-0.1.0 (c (n "fitnesstrax-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dimensioned") (r "^0.7.0") (d #t) (k 0)) (d (n "emseries") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pa1p876lrkj9cds4fm3hlcnyfacvf96296jjw23vqz9vn2q00rb")))

