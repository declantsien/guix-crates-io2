(define-module (crates-io fi xi fixit) #:use-module (crates-io))

(define-public crate-fixit-0.1.0 (c (n "fixit") (v "0.1.0") (h "0vkmr095a6cqfgykymmg1xz9dj8xvp7pf3vl7h8qkqhfh4mx8bfj")))

(define-public crate-fixit-0.1.1 (c (n "fixit") (v "0.1.1") (h "1vdwcdby1wy0lb4wg33frjc9v622zlgx23kn2a1lihf0zw2j7ghm")))

