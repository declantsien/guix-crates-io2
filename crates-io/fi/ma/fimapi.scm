(define-module (crates-io fi ma fimapi) #:use-module (crates-io))

(define-public crate-fimapi-0.1.0 (c (n "fimapi") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0hrwaik9wdy93d6ly629lnibf9g47y4wkypj24llmy8pl915hsph")))

(define-public crate-fimapi-0.2.0 (c (n "fimapi") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("native-tls" "json"))) (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0lhxzz63n9150ml300sqkc08q0hgjvg5zc7kb6gy8l7rffk6x6s5")))

