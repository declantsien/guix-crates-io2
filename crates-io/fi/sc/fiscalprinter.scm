(define-module (crates-io fi sc fiscalprinter) #:use-module (crates-io))

(define-public crate-fiscalprinter-1.0.0 (c (n "fiscalprinter") (v "1.0.0") (h "1bfpq44gil23f352059xxk6s1limi3paclk0p34dcc42q3y22lsw")))

(define-public crate-fiscalprinter-1.0.1 (c (n "fiscalprinter") (v "1.0.1") (h "1170bykwz7fz6m6np0a3rr28zciajmsgkjhydy7kbj5rsijzqnjl")))

(define-public crate-fiscalprinter-1.0.2 (c (n "fiscalprinter") (v "1.0.2") (h "14hxx8jv6qwk6q0ladfvkky8wh931nb76cxzn9vidmxq3vzda6m8")))

(define-public crate-fiscalprinter-1.0.3 (c (n "fiscalprinter") (v "1.0.3") (h "0p5kdw2ha3l1bbf9c5rm6hziq0k6inrj7dfqhf9rf1w5975vcbmn")))

(define-public crate-fiscalprinter-1.0.4 (c (n "fiscalprinter") (v "1.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1qbcq1lxxpln8b1wvqxnyx3b6aphvdlgbzrcik0fl5g5kx22kyqv")))

(define-public crate-fiscalprinter-1.0.6 (c (n "fiscalprinter") (v "1.0.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1ycb59dd7gabxfhxja54raynd4wg2b3pg4687dxj3xc88s1jx305")))

(define-public crate-fiscalprinter-1.0.8 (c (n "fiscalprinter") (v "1.0.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1h93hmfywvq0s444yljagzmghczs31c2qf7k64as0qxkpahy2ixb")))

(define-public crate-fiscalprinter-1.0.9 (c (n "fiscalprinter") (v "1.0.9") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1vkxvmdka7kdl2442wbp4mmq20cw2a78417l3jk6g10ni2lki63s")))

(define-public crate-fiscalprinter-1.1.0 (c (n "fiscalprinter") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1s13z5p2ss9w372rhxv5axhamq545rsqakspmz1alnxkhh83cpkl")))

(define-public crate-fiscalprinter-1.1.1 (c (n "fiscalprinter") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "0w5zaah7nbr659yfq9zmb5y49hnpksyjv7wz4zy60jv3m05sh3ih")))

