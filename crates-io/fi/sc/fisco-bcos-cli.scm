(define-module (crates-io fi sc fisco-bcos-cli) #:use-module (crates-io))

(define-public crate-fisco-bcos-cli-0.1.0 (c (n "fisco-bcos-cli") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "fisco-bcos-service") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dg1i6idy9p0302wgp7dsd7zsnyqmj9mgwb6bh102wywhgmqs5j0")))

(define-public crate-fisco-bcos-cli-0.2.0 (c (n "fisco-bcos-cli") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "fisco-bcos-service") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12rf48zybl4139pdac5qh602yrmwqvph87x7n4xkgk6ycjkrb6jb")))

(define-public crate-fisco-bcos-cli-0.3.0 (c (n "fisco-bcos-cli") (v "0.3.0") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "fisco-bcos-service") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vm04qv9j6sim2vxzm171kfz3zbing4d5g5xv00yyab6pj49937r")))

(define-public crate-fisco-bcos-cli-0.4.0 (c (n "fisco-bcos-cli") (v "0.4.0") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "fisco-bcos-service") (r ">=0.4, <1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "181igz9sl5mafnfcq3n7n6i0l84l04qg0y0f28yjbzlp09h73c8f")))

