(define-module (crates-io fi ts fitsio-sys-bindgen) #:use-module (crates-io))

(define-public crate-fitsio-sys-bindgen-0.0.1 (c (n "fitsio-sys-bindgen") (v "0.0.1") (d (list (d (n "libbindgen") (r "^0.1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0ijmqanaj120jjpkm5wmdzsx94dxzalrs6d8qkfv4zxkvvv6hppw")))

(define-public crate-fitsio-sys-bindgen-0.0.2 (c (n "fitsio-sys-bindgen") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1flmbv1kphvdv6vvw5bxs7j3bql1hfr11v4x1kxnx0f6pqhzi8y1")))

(define-public crate-fitsio-sys-bindgen-0.0.3 (c (n "fitsio-sys-bindgen") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "block") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "pkg-config") (r "^0") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "18xqrg6igwb0jfm78d0mskrqxpxgc4kc7k1328rnxk6f1yr39pq6")))

(define-public crate-fitsio-sys-bindgen-0.0.5 (c (n "fitsio-sys-bindgen") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "block") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0v68nxag9k4rh51c0b0hlmlsiprq2cy5zcrc0qvwgmxpmb3s95ka")))

(define-public crate-fitsio-sys-bindgen-0.0.6 (c (n "fitsio-sys-bindgen") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "block") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0bgyk5pinppky9dk5wmb20sjlxci3bmrzxyx0zc1wwfbnzdmmacv")))

