(define-module (crates-io fi ts fits-rs) #:use-module (crates-io))

(define-public crate-fits-rs-0.1.0 (c (n "fits-rs") (v "0.1.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "03fw3xhqa2psjv3dnlq0pqacxzv76iv4g79sk5a9vwkjw9clrh3m")))

(define-public crate-fits-rs-0.2.0 (c (n "fits-rs") (v "0.2.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "08lq26harjr83pls999k3whq7y04bikwalyiwpnylwhqwgy9rv36")))

(define-public crate-fits-rs-0.3.0 (c (n "fits-rs") (v "0.3.0") (d (list (d (n "nom") (r "^3.1") (d #t) (k 0)))) (h "14fsamwzjirjyr4lwhh89pj6dxjrycs4zazi1g5m5bklnfw9hll9")))

