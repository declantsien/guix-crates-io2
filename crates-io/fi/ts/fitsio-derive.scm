(define-module (crates-io fi ts fitsio-derive) #:use-module (crates-io))

(define-public crate-fitsio-derive-0.1.0 (c (n "fitsio-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qiw1vk5xyq0xrvycv82gw1y23vjq269bcpq9ibabf7np92fw4ph")))

(define-public crate-fitsio-derive-0.2.0 (c (n "fitsio-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08blbgpgsxlqd1nqf0amxbh05h7ar4s0bkcikpnzfqjc703l937r")))

