(define-module (crates-io fi ts fitsio-sys) #:use-module (crates-io))

(define-public crate-fitsio-sys-0.1.0 (c (n "fitsio-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1cm1mqxdz9b8g6wagacl3yrxv23f46q8qhgs2vvxi45m3ay1x6ms")))

(define-public crate-fitsio-sys-0.1.1 (c (n "fitsio-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0f8q8ffkgxwyx7wr7knnq3jklm880q9m325azflfhrk29mdj4nqs")))

(define-public crate-fitsio-sys-0.1.2 (c (n "fitsio-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1n5jfwzv1y59zc33zxwd9pygwd8frlwd1a4011iwiafy2qysdwvq")))

(define-public crate-fitsio-sys-0.1.3 (c (n "fitsio-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0j5sh9ryzclyik58w6ialp6kl008hfkwshzv5g3qrawbg3w92624")))

(define-public crate-fitsio-sys-0.1.4 (c (n "fitsio-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0gvbr26g4k1w8vsfblqsjabk20f0sad3ymvhah7wsf0nmp0ln1ky")))

(define-public crate-fitsio-sys-0.2.0 (c (n "fitsio-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0z5a6by5n3sbrmblpvjx52qzd2akqr16q06vr86kzimnp7k3k69r")))

(define-public crate-fitsio-sys-0.2.1 (c (n "fitsio-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1gkmxz0a46cb58bsbk8j85vaz1jqz00hdwl851nkm2bbnpkca58w")))

(define-public crate-fitsio-sys-0.2.2 (c (n "fitsio-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1lw5f9sl5dja3cijjn25a1fz4zr0034c7fz6xxlsnl2wml7h2l2b")))

(define-public crate-fitsio-sys-0.2.3 (c (n "fitsio-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "198pvir8n863ls9arvcny8j8b9n2y30ci764r9vhajszm6n4liha")))

(define-public crate-fitsio-sys-0.3.0 (c (n "fitsio-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1m943gw14yys5ws444rpgrw90ramjcphrcbj4lwfjbiwfbqvfk7n")))

(define-public crate-fitsio-sys-0.4.0 (c (n "fitsio-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "pkg-config") (r "^0") (d #t) (k 1)) (d (n "tempdir") (r "^0") (d #t) (k 2)))) (h "15d5i32gr52nqjgxx2r9ccz2rcd1fmazx7pcj1mqxnxwyh0sx29i") (f (quote (("fitsio-src")))) (l "cfitsio")))

(define-public crate-fitsio-sys-0.5.0 (c (n "fitsio-sys") (v "0.5.0") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0fki70lkc0ix77im649az5wfqfzzrgd4l4vjkvl4a5hlbrmaww5s") (f (quote (("fitsio-src")))) (l "cfitsio")))

(define-public crate-fitsio-sys-0.5.1 (c (n "fitsio-sys") (v "0.5.1") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "031kmm3srk9xw90wrbg5bgddyqg4df29jlrpgqggzbmqnpy9q5hy") (f (quote (("fitsio-src")))) (l "cfitsio")))

(define-public crate-fitsio-sys-0.5.2 (c (n "fitsio-sys") (v "0.5.2") (d (list (d (n "autotools") (r ">=0.2.5") (d #t) (k 1)) (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1q8h2ivliqq1makybsbmv3h8pjzv85wq5r0nfm84kqvaz9haikca") (f (quote (("with-bindgen" "bindgen") ("fitsio-src")))) (l "cfitsio")))

