(define-module (crates-io fi ts fitsrotate_rs) #:use-module (crates-io))

(define-public crate-fitsrotate_rs-0.0.0 (c (n "fitsrotate_rs") (v "0.0.0") (d (list (d (n "fitsio") (r "^0.21.1") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1jq4q6p28nji6l4gmdnrsl5l2xpmc77bbv8ihs57cknazw45idjg")))

(define-public crate-fitsrotate_rs-0.0.1 (c (n "fitsrotate_rs") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.1") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "0xgjsrwgx79gn8kxgqlqgl7wd67s8kh8jkc018x07pirjppd22fr")))

(define-public crate-fitsrotate_rs-0.1.0 (c (n "fitsrotate_rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.2") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1flzpqyv2hq2167nwhi92gl0fhazz2fkqm4yhj4i4m3nqjzgpi9z")))

(define-public crate-fitsrotate_rs-0.1.1 (c (n "fitsrotate_rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.2") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1vg2gvbgydl0yqwlaa7n2l5g4z61ls4jaddkxz1mb80cpdm2fmnx")))

(define-public crate-fitsrotate_rs-0.1.6 (c (n "fitsrotate_rs") (v "0.1.6") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.2") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "15zv4zn5j3ms9j0bhjq3ima1mza58nzch09144iy86lm2c3l4azd")))

(define-public crate-fitsrotate_rs-0.1.7 (c (n "fitsrotate_rs") (v "0.1.7") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.2") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "0sah6bbnrllb41pwcifzv8x01314zpasya4xb59kq4h2233pb7ni")))

(define-public crate-fitsrotate_rs-0.1.8 (c (n "fitsrotate_rs") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fitsio") (r "^0.21.2") (f (quote ("array"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "0zpypk0g49yvr13xq3d43iidgickkvmy4p3mc2b69h11gl6l142k")))

