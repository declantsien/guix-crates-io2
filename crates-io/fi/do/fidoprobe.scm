(define-module (crates-io fi do fidoprobe) #:use-module (crates-io))

(define-public crate-fidoprobe-0.1.0 (c (n "fidoprobe") (v "0.1.0") (d (list (d (n "authenticator") (r "^0.4.0-alpha.24") (f (quote ("crypto_openssl"))) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rpassword") (r "^7.3.1") (d #t) (k 0)))) (h "1vl9wlp3i8k5g2f3g55slcmh79kvr19s8rx4j16dqrdrnma17pnr")))

