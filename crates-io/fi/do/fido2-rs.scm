(define-module (crates-io fi do fido2-rs) #:use-module (crates-io))

(define-public crate-fido2-rs-0.1.0 (c (n "fido2-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libfido2-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xv13pbwbmnzk2ab6hvkjhmjv6g84sxx9i8jbf32dsks1pjxw150")))

