(define-module (crates-io fi #{2b}# fi2b) #:use-module (crates-io))

(define-public crate-fi2b-1.0.0 (c (n "fi2b") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "07dqkh6iv58mlybd982bnk2shsdwjii4kkklz8qs5wf7ziq8r5cg")))

