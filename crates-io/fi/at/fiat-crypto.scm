(define-module (crates-io fi at fiat-crypto) #:use-module (crates-io))

(define-public crate-fiat-crypto-0.1.0 (c (n "fiat-crypto") (v "0.1.0") (h "18pg8hg545d920a4f5chgsmfph8lrlc48i9wb8pli5pfgzjd79qi")))

(define-public crate-fiat-crypto-0.1.1 (c (n "fiat-crypto") (v "0.1.1") (h "0h3mmd6a1qbm89p7077nz9g49r43r58v2032g589qyc3n6w9zmvz")))

(define-public crate-fiat-crypto-0.1.2 (c (n "fiat-crypto") (v "0.1.2") (h "1viy2gnxlpldlzxxn5vw7g4ih7cgb01j8zqbn1b4cxa661fz1fhb")))

(define-public crate-fiat-crypto-0.1.3 (c (n "fiat-crypto") (v "0.1.3") (h "0kv3fn73n16p8iljzaz460dqa298n06z1j5ixi92nmbv2wm3f1qk")))

(define-public crate-fiat-crypto-0.1.4 (c (n "fiat-crypto") (v "0.1.4") (h "0jcjy5pkcgb44yiwpxj0b50b0an88i0ls75jm1ild75502v8dlkv")))

(define-public crate-fiat-crypto-0.1.5 (c (n "fiat-crypto") (v "0.1.5") (h "0sv1gdks031dz375glwzxwfkyi0fzw7k7a00mnv5fn31jmqbjshg")))

(define-public crate-fiat-crypto-0.1.6 (c (n "fiat-crypto") (v "0.1.6") (h "11gg0hag5ak2g52qv5vvgky591fzgvvz2s2ilmlsk6adf0r2ncbj")))

(define-public crate-fiat-crypto-0.1.7 (c (n "fiat-crypto") (v "0.1.7") (h "0z1bdjiz7dxrjl19n1f6yydq8psrr8p1w3a1jjf09zwnyw0a7i96")))

(define-public crate-fiat-crypto-0.1.8 (c (n "fiat-crypto") (v "0.1.8") (h "0l3q9pc9y67m66npgix0drvb7n772s438bfza77zjf63j536srxh")))

(define-public crate-fiat-crypto-0.1.9 (c (n "fiat-crypto") (v "0.1.9") (h "1gg1avz5kaf5sy6z5kzig94yy1994ky2z748qyyjbd8lbjn1y6i3")))

(define-public crate-fiat-crypto-0.1.10 (c (n "fiat-crypto") (v "0.1.10") (h "0jck765r0wdlyxi6jg5xzwvvmjm5rcdhrh4z5k9za6fdkbb96giz") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.11 (c (n "fiat-crypto") (v "0.1.11") (h "12gyr01z3bkahbxcrj2nsdq42qbqgq12w32dvpr6a7930i0qfxi1") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.12 (c (n "fiat-crypto") (v "0.1.12") (h "0h0igif7z3cdkpfcqm28n8a6li7hvd1jcsr47iylnarzdpjd31y1") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.13 (c (n "fiat-crypto") (v "0.1.13") (h "1z0pw0pm8mxm7p3lvr4is84b42yxqwjrlhbgcis969cxpzv4qd9m") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.14 (c (n "fiat-crypto") (v "0.1.14") (h "1kismm295372lybd00akqhkzx687gy0vxn8a536ss2ydx0h9zv3h") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.16 (c (n "fiat-crypto") (v "0.1.16") (h "1m3ka0mydbqvli47rmbgs6crz5n3sq6nw1rgcqaqvb1lbh4k4qc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.17 (c (n "fiat-crypto") (v "0.1.17") (h "141wyw711ywk9xzvd7xsi5014qkv4y51zbpkg1j467bki2xza552") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.18 (c (n "fiat-crypto") (v "0.1.18") (h "1zjxv8sg6qci3zngkx599kzbhxc7lh85vnj5129rknad3v2z7cjl") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.19 (c (n "fiat-crypto") (v "0.1.19") (h "0m9rpk28wribkrmkbd86a2hawzrdd7maksij7b9qx761gknfdb4k") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1.20 (c (n "fiat-crypto") (v "0.1.20") (h "0xvbcg6wh42q3n7294mzq5xxw8fpqsgc0d69dvm5srh1f6cgc9g8") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.0 (c (n "fiat-crypto") (v "0.2.0") (h "1y6hcsphbczph34mvlvwzhxly0w1wyz5njpcssr0bvpkik8f4bb9") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.1 (c (n "fiat-crypto") (v "0.2.1") (h "0b8xvq0gn2231zlg06763a81xqv59z127wy9wldlhjvd0620r1yh") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.2 (c (n "fiat-crypto") (v "0.2.2") (h "1ixkvhrc97dfyvwc6kszza7h8jqjf4zk8k22ala1p3vprxm5i0d4") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.3 (c (n "fiat-crypto") (v "0.2.3") (h "1pg1swnq3mqmyzp5rlki0sh7c61q8zvbqb2gdfc88pkq3gz3g47n") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.4 (c (n "fiat-crypto") (v "0.2.4") (h "106zg5n1m4xbzagxfg4f74szpzf5s0zhc9y1s6x7909ih03nz9ak") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.5 (c (n "fiat-crypto") (v "0.2.5") (h "1dxn0g50pv0ppal779vi7k40fr55pbhkyv4in7i13pgl4sn3wmr7") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.6 (c (n "fiat-crypto") (v "0.2.6") (h "10hkkkjynhibvchznkxx81gwxqarn9i5sgz40d6xxb8xzhsz8xhn") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.7 (c (n "fiat-crypto") (v "0.2.7") (h "03w3ic88yvdpwbz36dlm7csacz4b876mlc0nbbwbc75y7apb21y0") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.8 (c (n "fiat-crypto") (v "0.2.8") (h "07p0gaynz13i9vr1133gix1nzapzh6bjq37478p42crvb5akqy9q") (f (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2.9 (c (n "fiat-crypto") (v "0.2.9") (h "07c1vknddv3ak7w89n85ik0g34nzzpms6yb845vrjnv9m4csbpi8") (f (quote (("std") ("default" "std"))))))

