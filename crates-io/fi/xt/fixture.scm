(define-module (crates-io fi xt fixture) #:use-module (crates-io))

(define-public crate-fixture-0.0.1 (c (n "fixture") (v "0.0.1") (h "0xzr9hdifdsvp77qnka8vs5rfpzxza19ps449vpy8aq0hawk133f")))

(define-public crate-fixture-0.0.2 (c (n "fixture") (v "0.0.2") (h "0ag19piqxcak27hfivjjm597dss95mam7i1jv7mh53im463aa3jp")))

(define-public crate-fixture-0.0.3 (c (n "fixture") (v "0.0.3") (h "06lbf4pll8dabnhg8nz6cjbbphb0k34lvnzdrhvc3x33c4vm9yb1")))

(define-public crate-fixture-0.1.0 (c (n "fixture") (v "0.1.0") (h "1aqrxx0wwk5h0yvbdla9lw8rc1wlcph26f0h302qphhw6nwb34pj")))

(define-public crate-fixture-0.2.0 (c (n "fixture") (v "0.2.0") (h "0a197llnp2xr44x556bm7kl6ia0syf7akr58s8di5pax152q38v0")))

(define-public crate-fixture-0.3.0 (c (n "fixture") (v "0.3.0") (h "14av28wis8jflg2c61h95r1agd9sp1v1ryk72z43mb13ybj2xxi1")))

(define-public crate-fixture-0.3.1 (c (n "fixture") (v "0.3.1") (h "0sv5aq42x8fg7s637754asm67y4y8ng5y0y5r72lmx67vir4l34g")))

