(define-module (crates-io fi xt fixtures) #:use-module (crates-io))

(define-public crate-fixtures-0.1.0 (c (n "fixtures") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full" "parsing" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0sm9yfa02j959fbr8az667x4hfhxmrfnfnlyh28b5nllfbnl3rlf")))

