(define-module (crates-io fi st fistinc-diesel-utils) #:use-module (crates-io))

(define-public crate-fistinc-diesel-utils-0.1.0 (c (n "fistinc-diesel-utils") (v "0.1.0") (d (list (d (n "actix-threadpool") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (f (quote ("chrono" "postgres" "r2d2"))) (d #t) (k 0)) (d (n "fistinc-errors") (r "^0.1.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "0mb1vjw5fn8wlrybg4jkbxz6kbcf72k09chskhxj93dm1l2a4xjy")))

(define-public crate-fistinc-diesel-utils-0.1.1 (c (n "fistinc-diesel-utils") (v "0.1.1") (d (list (d (n "actix-threadpool") (r "^0.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (f (quote ("chrono" "postgres" "r2d2"))) (d #t) (k 0)) (d (n "fistinc-errors") (r "^0.1.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "1a0xpfalwmzi0k56mc3m0glg5svaz9lrk1w6wv0gqf6qhf62r1py")))

