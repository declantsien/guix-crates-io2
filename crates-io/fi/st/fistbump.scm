(define-module (crates-io fi st fistbump) #:use-module (crates-io))

(define-public crate-fistbump-0.1.0 (c (n "fistbump") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "00k2xapg2x5srgl6c7x90jwpgmscvghzx8m13wicks3j4s8hv72k")))

(define-public crate-fistbump-1.0.0 (c (n "fistbump") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0ssmzpmys4ji048s0wzvjlrhaclfc1zlk90xzgr6jgbc7mcxzlyh")))

