(define-module (crates-io fi st fistinc-errors) #:use-module (crates-io))

(define-public crate-fistinc-errors-0.1.0 (c (n "fistinc-errors") (v "0.1.0") (d (list (d (n "actix-http") (r "^2.2.1") (d #t) (k 0)) (d (n "actix-threadpool") (r "^0.3.3") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "diesel") (r "^1.4.8") (d #t) (k 0)) (d (n "email_address") (r "^0.2.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)))) (h "1ij17816hbqww91lmkfs52ihlx74iby59jg6wpfjxnvcss7b22x8")))

