(define-module (crates-io fi to fito) #:use-module (crates-io))

(define-public crate-fito-0.1.0 (c (n "fito") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0d1ldx1l3jndprcx7kkcfgh0hmzkwvck9nchnkk9nfxbs37qsfsg")))

(define-public crate-fito-0.1.1 (c (n "fito") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1kwadw0l12xa4kgjh4a0aynx77zy77gilbdwfgds3c72dqhgssp8")))

