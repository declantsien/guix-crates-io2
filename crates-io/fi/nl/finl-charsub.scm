(define-module (crates-io fi nl finl-charsub) #:use-module (crates-io))

(define-public crate-finl-charsub-0.1.0 (c (n "finl-charsub") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05q49ri6hvhjix5z4h31jxb8m4p3shrkzvijxhhkpfwx3w8vaian") (y #t)))

(define-public crate-finl-charsub-1.0.0 (c (n "finl-charsub") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "162y9n6v47mm18lkvqyganvyma2jrzjkimyj21xr7ccyhp941b0s")))

