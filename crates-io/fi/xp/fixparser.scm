(define-module (crates-io fi xp fixparser) #:use-module (crates-io))

(define-public crate-fixparser-0.1.0 (c (n "fixparser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1c5n4xpqy1lc8h3b7y10l7kbyiryh20kwxl3b4mj3kr2h2w4igmy")))

(define-public crate-fixparser-0.1.1 (c (n "fixparser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "04imcksdppry1hxa7vn8iji399rcbdc6g3fijva8d8k400rj511w")))

(define-public crate-fixparser-0.1.2 (c (n "fixparser") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0g5rhgzcdvngia55bq7g0giyqqlvnbf5mdl2q34mwbc6bxmxphk4") (f (quote (("debugging"))))))

(define-public crate-fixparser-0.1.3 (c (n "fixparser") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "195i5xnl8rjr6dynkpgjnaw12g9viy7f2vffp2yj1y7sshfr5jg3") (f (quote (("debugging"))))))

(define-public crate-fixparser-0.1.4 (c (n "fixparser") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1kmp9wkdg241afsv5q1z07frw74aybwrlkll9q4m3z0bwizngma4") (f (quote (("debugging"))))))

(define-public crate-fixparser-0.1.5 (c (n "fixparser") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0bf9hdh08j4ks9zvmv796n0w7dy91mikyk6ygc60ymsdbc24c5na") (f (quote (("debugging"))))))

