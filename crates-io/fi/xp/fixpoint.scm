(define-module (crates-io fi xp fixpoint) #:use-module (crates-io))

(define-public crate-fixpoint-0.1.0 (c (n "fixpoint") (v "0.1.0") (h "1kspbs7x593aqd5ksld06gygzzc9rz7fd9vyl0kqkvqafk5iq1nr")))

(define-public crate-fixpoint-0.2.0 (c (n "fixpoint") (v "0.2.0") (h "0hlqv4wjrk9ncap91nfzfx4nrhn1s2ffdq9ry98c0vgnn6rrci4a")))

