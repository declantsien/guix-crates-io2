(define-module (crates-io fi -c fi-cli) #:use-module (crates-io))

(define-public crate-fi-cli-0.1.0 (c (n "fi-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "textplots") (r "^0.5") (d #t) (k 0)))) (h "0z8b0dzcjfignrpp9mw83gqlhk7gn6wrqcxvykvc6dn8165wwyv9")))

