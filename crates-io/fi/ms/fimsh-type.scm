(define-module (crates-io fi ms fimsh-type) #:use-module (crates-io))

(define-public crate-fimsh-type-0.1.0 (c (n "fimsh-type") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0mlgwj0h2fd5v9grx4p8vavd70xrmqz7860fr48f92n5ap62617p")))

