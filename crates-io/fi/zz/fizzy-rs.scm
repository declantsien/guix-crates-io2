(define-module (crates-io fi zz fizzy-rs) #:use-module (crates-io))

(define-public crate-fizzy-rs-0.0.7 (c (n "fizzy-rs") (v "0.0.7") (d (list (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)))) (h "16cv2i5lbd7p7mqzynv2w5ys7y0wgjn11nry6z1d9mjrv0drsnsx") (y #t)))

(define-public crate-fizzy-rs-0.0.8 (c (n "fizzy-rs") (v "0.0.8") (d (list (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)) (d (n "xdg-home") (r "^1.0.0") (d #t) (k 0)))) (h "0msgrzlxk76zj3yj0b6xr64mb7g593jwp08ch11lh5mc6as1zxkn")))

(define-public crate-fizzy-rs-0.0.9 (c (n "fizzy-rs") (v "0.0.9") (d (list (d (n "copypasta-ext") (r "^0.4.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "xdg-home") (r "^1.0.0") (d #t) (k 0)))) (h "1gspq7vslbdpk9fw87lbrgsqcazb0vys7mqay3zdwikp5rrj6fma")))

