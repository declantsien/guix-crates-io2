(define-module (crates-io fi zz fizzy) #:use-module (crates-io))

(define-public crate-fizzy-0.6.0-dev (c (n "fizzy") (v "0.6.0-dev") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wwscd2z22zk7l3y3cgpfvf1rdvy5is94xjadg2z341z8v18zasw")))

(define-public crate-fizzy-0.6.0 (c (n "fizzy") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "0g8cdpdfa291i7azzdfxci8zzmlrzs623kbv1swd3ipi9a9gar38")))

(define-public crate-fizzy-0.7.0 (c (n "fizzy") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "1ahg21k99ykp14x3mwknn03fx9bba2fb52z99kpwaj98r7ay9b1w")))

(define-public crate-fizzy-0.8.0 (c (n "fizzy") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "08yyfvzhcmcwr3z2xmnkrvn24mhp1w6kdixpa4swggrzbjm808ds")))

