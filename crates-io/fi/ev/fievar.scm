(define-module (crates-io fi ev fievar) #:use-module (crates-io))

(define-public crate-fievar-0.1.0 (c (n "fievar") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0vb10p6cmwa13qxgjaj609jxwz4kkdr5ab5nsvwpv9chd4h04rb0")))

(define-public crate-fievar-0.1.1 (c (n "fievar") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1v59q675pv21bcdmn03mdr5br68bsp1jv76djxb50206wh0xf11s")))

(define-public crate-fievar-0.1.2 (c (n "fievar") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "11lwdw7ivk9f8c1b4d3xxa960ay9zzkxdlfl37s2jpkzp37k678j")))

(define-public crate-fievar-0.2.0 (c (n "fievar") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1d8ln1xxm4anx5k92pvv7fawpr42xj2bb10nkywy3hh8pfbmkh18")))

