(define-module (crates-io fi i- fii-id) #:use-module (crates-io))

(define-public crate-fii-id-0.1.0 (c (n "fii-id") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qz4nkxcpf6g2bz5jwc2kpk4n3xigfmc49a16iaprn17glzf3zc9")))

(define-public crate-fii-id-1.0.0 (c (n "fii-id") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0mv1iwjkrzblrs5rypzjdl5dj8mvw5gqf5w9f6n4qh0x10fmm2pa")))

(define-public crate-fii-id-1.3.0 (c (n "fii-id") (v "1.3.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0vc63ckxyg3m4fr0i6fdd2myi0wwxaqf7dx9r394lbyvvwlnrmsa")))

(define-public crate-fii-id-1.3.1 (c (n "fii-id") (v "1.3.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0mqyf7cb5zk732zybswblyxkcclzdwx3ybn6c88v2fhizazn2h3q")))

(define-public crate-fii-id-1.4.0 (c (n "fii-id") (v "1.4.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0j2g51vlk9fplkzbknkp5a36lxf2l1db4css6pqp03g451zib2s3")))

