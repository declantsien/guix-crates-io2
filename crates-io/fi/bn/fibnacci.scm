(define-module (crates-io fi bn fibnacci) #:use-module (crates-io))

(define-public crate-fibnacci-0.2.0 (c (n "fibnacci") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "109s459mqx7c7cvgaq3j6z3sg9rv49yc2mxxf8qp4n8qhmxy2j7a")))

(define-public crate-fibnacci-0.3.0 (c (n "fibnacci") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "0ysry3p7idqfqfyciwzrmr8hwxlqfyvw8pkwcrm1qw45iapnr45p")))

(define-public crate-fibnacci-0.3.1 (c (n "fibnacci") (v "0.3.1") (d (list (d (n "cache-macro") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "09gvpdm9vr4mikwjb7l3wbfx2y4l8p736wp4gjb8j283hrmyr94n")))

(define-public crate-fibnacci-0.4.0 (c (n "fibnacci") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "0mi8iddp148akilfdxcw33zbsw53xviz0ndkzf8914jckwzx3xza")))

