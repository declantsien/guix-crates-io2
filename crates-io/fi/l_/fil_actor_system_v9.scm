(define-module (crates-io fi l_ fil_actor_system_v9) #:use-module (crates-io))

(define-public crate-fil_actor_system_v9-1.0.0 (c (n "fil_actor_system_v9") (v "1.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zvs6qwv3y1lq874ax8mkcilxv2gjpjmvq1yb3f2wjgc9ds30c8r")))

(define-public crate-fil_actor_system_v9-2.0.0 (c (n "fil_actor_system_v9") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xrzkw18zqlbgjmpx890kxpksl7s86ggvik9zls5z5n6qilavy71")))

