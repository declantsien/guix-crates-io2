(define-module (crates-io fi l_ fil_actor_account_v10) #:use-module (crates-io))

(define-public crate-fil_actor_account_v10-1.0.0 (c (n "fil_actor_account_v10") (v "1.0.0") (d (list (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z8g5plykn9lv85zxc2rh9ywlnli1p2cpspi9hx6rvsgpspld4ki")))

(define-public crate-fil_actor_account_v10-2.0.0 (c (n "fil_actor_account_v10") (v "2.0.0") (d (list (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cmxdazl1zk7lmd6pk0yxpd41bqhxwy7xfnrgwhsk285r8mkp6q7")))

