(define-module (crates-io fi l_ fil_actor_datacap_v11) #:use-module (crates-io))

(define-public crate-fil_actor_datacap_v11-2.0.0 (c (n "fil_actor_datacap_v11") (v "2.0.0") (d (list (d (n "fil_actors_runtime_v11") (r "^2.0.0") (d #t) (k 0)) (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "frc46_token") (r "^3.1.0") (d #t) (k 0)) (d (n "fvm_ipld_blockstore") (r "^0.1") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gmhld5ir58w80gbym48yz1l10n4sxmici2g742m5zc7w2gaqwww")))

