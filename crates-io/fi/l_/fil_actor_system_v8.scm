(define-module (crates-io fi l_ fil_actor_system_v8) #:use-module (crates-io))

(define-public crate-fil_actor_system_v8-1.0.0 (c (n "fil_actor_system_v8") (v "1.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dcp2lx0h0bv602mqrclf287hz54nniwp0qghg318lj6lwzg490d")))

(define-public crate-fil_actor_system_v8-2.0.0 (c (n "fil_actor_system_v8") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "083w08cd6dcgmkgcd34653r95nb0cxj5v9rplv36w55233phkh5v")))

