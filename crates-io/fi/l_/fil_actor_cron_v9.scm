(define-module (crates-io fi l_ fil_actor_cron_v9) #:use-module (crates-io))

(define-public crate-fil_actor_cron_v9-1.0.0 (c (n "fil_actor_cron_v9") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18v0l5b52nkbc407245dr4h8qc9hqgrfky0q7aq2kwx3skn2663m")))

(define-public crate-fil_actor_cron_v9-2.0.0 (c (n "fil_actor_cron_v9") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dc9wsr6wn8mfsf72lqnq9g8s7ws83fb341k65gskl05lqwqa09g")))

