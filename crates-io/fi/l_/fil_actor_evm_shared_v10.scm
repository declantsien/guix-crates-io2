(define-module (crates-io fi l_ fil_actor_evm_shared_v10) #:use-module (crates-io))

(define-public crate-fil_actor_evm_shared_v10-1.0.0 (c (n "fil_actor_evm_shared_v10") (v "1.0.0") (d (list (d (n "fil_actors_runtime_v10") (r "^1.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (k 0)))) (h "1va5vysbf3jfv6bwc6kzjb5lvsi3ypjzbxfpj1kcqn4shdg5b6yb")))

(define-public crate-fil_actor_evm_shared_v10-2.0.0 (c (n "fil_actor_evm_shared_v10") (v "2.0.0") (d (list (d (n "fil_actors_runtime_v10") (r "^2.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (k 0)))) (h "06lkjq99adxpnbw2jx4456la7jgpjksxq0g8p7k426j3ilk65pwx")))

