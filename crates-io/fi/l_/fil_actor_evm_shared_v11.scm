(define-module (crates-io fi l_ fil_actor_evm_shared_v11) #:use-module (crates-io))

(define-public crate-fil_actor_evm_shared_v11-2.0.0 (c (n "fil_actor_evm_shared_v11") (v "2.0.0") (d (list (d (n "fil_actors_runtime_v11") (r "^2.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (k 0)))) (h "1vbh1gpvsixnnpswkbn1x022pnbw7hz3kygpdyn8g5dkrri7n6cn")))

