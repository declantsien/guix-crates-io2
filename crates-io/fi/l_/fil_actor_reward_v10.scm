(define-module (crates-io fi l_ fil_actor_reward_v10) #:use-module (crates-io))

(define-public crate-fil_actor_reward_v10-1.0.0 (c (n "fil_actor_reward_v10") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bj6g6w04n2hwy4pmikv8p10xkzqx9d68gpzg5qh3zmkqh6iznzi")))

(define-public crate-fil_actor_reward_v10-2.0.0 (c (n "fil_actor_reward_v10") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i95q6xsvdmzyy42699ly97cchgq1apvv8iqf8s9i9zw0pddv2kc")))

