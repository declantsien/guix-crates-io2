(define-module (crates-io fi l_ fil_actor_reward_v8) #:use-module (crates-io))

(define-public crate-fil_actor_reward_v8-1.0.0 (c (n "fil_actor_reward_v8") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hcmd97prnlck8ps127bgx569bx9wx33a05dglgzhfc7nq3fjh27")))

(define-public crate-fil_actor_reward_v8-2.0.0 (c (n "fil_actor_reward_v8") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10vs53assawb2mwwsf8gjzxlxaq2pxfz8hny52p7zv7rqw128mqp")))

