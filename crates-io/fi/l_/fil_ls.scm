(define-module (crates-io fi l_ fil_ls) #:use-module (crates-io))

(define-public crate-fil_ls-0.1.2 (c (n "fil_ls") (v "0.1.2") (d (list (d (n "regex") (r "^1.7") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "02vi9gal27fqw0mxz9z46kvzl1gmhv9wwlrk78vpshryab8qc6ys")))

(define-public crate-fil_ls-0.1.3 (c (n "fil_ls") (v "0.1.3") (d (list (d (n "regex") (r "^1.7") (f (quote ("std" "unicode-perl"))) (k 0)))) (h "182p4ihcv4gpp45lnbkb8xwq176lmv4mgk10alhqsxpn5ysvk0a2")))

