(define-module (crates-io fi l_ fil_actor_paych_v8) #:use-module (crates-io))

(define-public crate-fil_actor_paych_v8-1.0.0 (c (n "fil_actor_paych_v8") (v "1.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b9y42q0xq6nl9qxpmz67mp26b99nrxsqacjpm4kzchl1fkqcxil")))

(define-public crate-fil_actor_paych_v8-2.0.0 (c (n "fil_actor_paych_v8") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fil_actors_runtime_v8") (r "^2.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05dr0z3mgqn7cnal8bqasp32mxc9c0lc4v8s9xl2vs18pf7nmdpb")))

