(define-module (crates-io fi l_ fil_actor_eam_v10) #:use-module (crates-io))

(define-public crate-fil_actor_eam_v10-1.0.0 (c (n "fil_actor_eam_v10") (v "1.0.0") (d (list (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wcnyiwp6symlp059h46rbdrsqph42nafziwqzc0ma01xvpa25s1")))

(define-public crate-fil_actor_eam_v10-2.0.0 (c (n "fil_actor_eam_v10") (v "2.0.0") (d (list (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1avv1q070diwisi0jvqc6fifppznsjkln54avzr4c3fp79hlhqfq")))

