(define-module (crates-io fi l_ fil_actor_system_v10) #:use-module (crates-io))

(define-public crate-fil_actor_system_v10-1.0.0 (c (n "fil_actor_system_v10") (v "1.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pg6r6hd646y77v1brwzd1hpx5ps60nk6lnw471x7cdinj5hmh9q")))

(define-public crate-fil_actor_system_v10-2.0.0 (c (n "fil_actor_system_v10") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bc915adi3p5mjx9pmaz2ysvcl2glh91zdr3dm09rpvm45kx7gp0")))

