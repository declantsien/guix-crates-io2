(define-module (crates-io fi l_ fil_actor_paych_v9) #:use-module (crates-io))

(define-public crate-fil_actor_paych_v9-1.0.0 (c (n "fil_actor_paych_v9") (v "1.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xi7093l95nyc4whi0jl3qz71wld77sv4gqfx99k3xnm1q3y167q")))

(define-public crate-fil_actor_paych_v9-2.0.0 (c (n "fil_actor_paych_v9") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fil_actors_runtime_v9") (r "^2.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h6zw38zwx0h6g5h8nniwxhjcsibhjdsqnvqryjyy017f0smvpdf")))

