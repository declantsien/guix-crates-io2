(define-module (crates-io fi l_ fil_actor_reward_v11) #:use-module (crates-io))

(define-public crate-fil_actor_reward_v11-2.0.0 (c (n "fil_actor_reward_v11") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rsc491pw0f7mc6w62ddwfp4a5yf3z9xqrsgsqhbj2pd1yf0y7f9")))

