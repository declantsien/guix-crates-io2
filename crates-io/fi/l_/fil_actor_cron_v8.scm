(define-module (crates-io fi l_ fil_actor_cron_v8) #:use-module (crates-io))

(define-public crate-fil_actor_cron_v8-1.0.0 (c (n "fil_actor_cron_v8") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x11bsiinzchbfgkygyc1vc65r8ymrky50sxn5imp5mqln2dvy5b")))

(define-public crate-fil_actor_cron_v8-2.0.0 (c (n "fil_actor_cron_v8") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zsqj2v0ycjivwbxkcawifbdj2kmfw8xp82hbrbd2ikypnqblvp4")))

