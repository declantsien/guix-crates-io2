(define-module (crates-io fi l_ fil_actor_ethaccount_v10) #:use-module (crates-io))

(define-public crate-fil_actor_ethaccount_v10-1.0.0 (c (n "fil_actor_ethaccount_v10") (v "1.0.0") (d (list (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01p6l5qk45cr9wgb6kyq3jl9r2sx84p9k7sxf3i7061mqmr08s23")))

(define-public crate-fil_actor_ethaccount_v10-2.0.0 (c (n "fil_actor_ethaccount_v10") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "017dbhfzz175ax1hzcbch39nfhh64asasgn4x5x2b64ymixdakg3")))

