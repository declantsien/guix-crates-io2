(define-module (crates-io fi l_ fil_actor_account_v9) #:use-module (crates-io))

(define-public crate-fil_actor_account_v9-1.0.0 (c (n "fil_actor_account_v9") (v "1.0.0") (d (list (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12m65ihcrblqd2vnp11y1f34dg4h6x9qj4gq46x40ncycqvjpmll")))

(define-public crate-fil_actor_account_v9-2.0.0 (c (n "fil_actor_account_v9") (v "2.0.0") (d (list (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a6yfml8r4x56hcmv6kxqnbfnzs181q7jn8yp1lzagfl1bb2crwz")))

