(define-module (crates-io fi l_ fil_actor_ethaccount_v11) #:use-module (crates-io))

(define-public crate-fil_actor_ethaccount_v11-2.0.0 (c (n "fil_actor_ethaccount_v11") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yj7f33hj3s7sd3jlmxkbsvs7rf3yb67m3i0ffhad0pm29pz8c1p")))

