(define-module (crates-io fi l_ fil_actor_reward_v9) #:use-module (crates-io))

(define-public crate-fil_actor_reward_v9-1.0.0 (c (n "fil_actor_reward_v9") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aklnn7hqmh4j90hrn64skfmx2f8413rdnfr3a9pplln2zwz18dn")))

(define-public crate-fil_actor_reward_v9-2.0.0 (c (n "fil_actor_reward_v9") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared") (r "^2") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zacgbbqvg5hrsg70iy5rvqhp9x0c4acdi753c6dz9m2ph6v6lc1")))

