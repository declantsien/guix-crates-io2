(define-module (crates-io fi l_ fil_actor_cron_v11) #:use-module (crates-io))

(define-public crate-fil_actor_cron_v11-2.0.0 (c (n "fil_actor_cron_v11") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ss0n6z018hl0v3448pnbskbjmrcrkjwcj4hw2fxljmdnll15400")))

