(define-module (crates-io fi l_ fil_logger) #:use-module (crates-io))

(define-public crate-fil_logger-0.1.0 (c (n "fil_logger") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.14.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0xnn7jwid4lm00qdw9x4xpwvgw0xvjhssvl23pdh4rsyykypy0d4")))

(define-public crate-fil_logger-0.1.1 (c (n "fil_logger") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.14.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0ymbn8q4zs1sivr26i0z20lwl6d59da2hb9qdzwji7mfwrbbwlcl")))

(define-public crate-fil_logger-0.1.2 (c (n "fil_logger") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.14.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1icjl7k727mvg690qhhw46m3qfnsxm6a0v4hsqmnf99bxiaza5wc")))

(define-public crate-fil_logger-0.1.3 (c (n "fil_logger") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.14.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "044dy3mqlm23w2fa1dq24s91lm1i35sxhcwx4w73dnkk9xccn4sv")))

(define-public crate-fil_logger-0.1.4 (c (n "fil_logger") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "06m01738cnd8yfw6fp5ygjc1bf2mhj4sfq25xzrclbldidngw8kz")))

(define-public crate-fil_logger-0.1.5 (c (n "fil_logger") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "1cya7dc4znb2g3awvbhw8g5jy4591r8ngdzxmvc791n5vxkpy0i9")))

(define-public crate-fil_logger-0.1.6 (c (n "fil_logger") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "0fpmzajkniq6x20k7bld6my6dp47wxcjd67i3hddpfbnvj1ip7ar")))

(define-public crate-fil_logger-0.1.7 (c (n "fil_logger") (v "0.1.7") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0jk937hdgm511gazdbylj045bvl5vmbfjfrs8iida6a7070z2y5q")))

