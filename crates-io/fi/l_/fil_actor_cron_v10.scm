(define-module (crates-io fi l_ fil_actor_cron_v10) #:use-module (crates-io))

(define-public crate-fil_actor_cron_v10-1.0.0 (c (n "fil_actor_cron_v10") (v "1.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x7yn8rvz0l4i3d9c7wa76pmbwi66i2cfxbc341gxfdiwm25vjcx")))

(define-public crate-fil_actor_cron_v10-2.0.0 (c (n "fil_actor_cron_v10") (v "2.0.0") (d (list (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vnln6p7qg5xvhj4kv6rbqb11b3pi02xzh5aadrjy0fv86lyhpk3")))

