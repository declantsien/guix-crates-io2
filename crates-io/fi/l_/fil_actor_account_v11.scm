(define-module (crates-io fi l_ fil_actor_account_v11) #:use-module (crates-io))

(define-public crate-fil_actor_account_v11-2.0.0 (c (n "fil_actor_account_v11") (v "2.0.0") (d (list (d (n "frc42_dispatch") (r "^3.0.0") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0blmcm5009gl39cmvkig92ynjz9k6aw72psqiss9x8d6via1428n")))

