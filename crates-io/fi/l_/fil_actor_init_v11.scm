(define-module (crates-io fi l_ fil_actor_init_v11) #:use-module (crates-io))

(define-public crate-fil_actor_init_v11-2.0.0 (c (n "fil_actor_init_v11") (v "2.0.0") (d (list (d (n "cid") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "fil_actors_runtime_v11") (r "^2.0.0") (d #t) (k 0)) (d (n "fvm_ipld_blockstore") (r "^0.1") (d #t) (k 0)) (d (n "fvm_ipld_encoding") (r "^0.3") (d #t) (k 0)) (d (n "fvm_shared3") (r "^3.2") (k 0) (p "fvm_shared")) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1547djl4242x997vd5b1qcbvlnkr194xnpkk1mk85j0l6p8rkycl")))

