(define-module (crates-io fi ti fitimer) #:use-module (crates-io))

(define-public crate-fitimer-0.1.0 (c (n "fitimer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "13ip8iqg2bi7cck5skinw8nhqqjqzz2xrhlnj4761wbmffllr6gz")))

(define-public crate-fitimer-0.2.0 (c (n "fitimer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "13194apbpznvzpgmwv0sj7mn502rwhv6fjg46kra044708hrn2c9")))

(define-public crate-fitimer-1.0.0 (c (n "fitimer") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "1kxv680vad36rsyz62sbs4lsv6yhf452b8qa8z4kgal1qdhbcyqr")))

(define-public crate-fitimer-1.1.0 (c (n "fitimer") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "01k6m9wccbkawfp2j3gdrpsmq1g5h0n0iaxp409cv35qyy6m1xsf")))

