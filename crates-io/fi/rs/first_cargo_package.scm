(define-module (crates-io fi rs first_cargo_package) #:use-module (crates-io))

(define-public crate-first_cargo_package-0.1.0 (c (n "first_cargo_package") (v "0.1.0") (h "00a17jix2hiccn9hz3l7s7rl9mng6blrw6w6k75fd72hcjx8mmj1")))

(define-public crate-first_cargo_package-0.1.1 (c (n "first_cargo_package") (v "0.1.1") (h "0glyjz1varaswywzn8ys1km0y9hbm0aph8mp8gwwd4fqczp1d9g6")))

