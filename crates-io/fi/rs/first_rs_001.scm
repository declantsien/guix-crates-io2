(define-module (crates-io fi rs first_rs_001) #:use-module (crates-io))

(define-public crate-first_rs_001-0.1.0 (c (n "first_rs_001") (v "0.1.0") (h "1bd0ls0nbzijjd912vy5lmviy6ng84axcrr9ivg28lrbsy559ypq")))

(define-public crate-first_rs_001-0.1.1 (c (n "first_rs_001") (v "0.1.1") (h "1df19gy0axzsz698kgnfzx2kcaim36ym1jn1762viz2w1v8x30s8") (y #t)))

