(define-module (crates-io fi rs first_order_logic) #:use-module (crates-io))

(define-public crate-first_order_logic-0.1.0 (c (n "first_order_logic") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03dxfgmr6jmdx8sgrlnplqakqrjraiibn5inl7a7mcy4bc3hyr3f") (f (quote (("syntax") ("semantics") ("default" "semantics" "syntax"))))))

