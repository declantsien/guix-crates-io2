(define-module (crates-io fi rs first_crate_yinuo) #:use-module (crates-io))

(define-public crate-first_crate_yinuo-0.1.0 (c (n "first_crate_yinuo") (v "0.1.0") (h "08gzana1iqjkbpncfp27imlmm190yz4hz4zfj9h6lwjfwyw46fwz")))

(define-public crate-first_crate_yinuo-0.2.0 (c (n "first_crate_yinuo") (v "0.2.0") (h "1zn9r4nddwmhkyr276j3zsi5craj0rh60v9qx2vr0dp19hrkm56b")))

