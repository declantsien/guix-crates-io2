(define-module (crates-io fi rs first_crate_test_12345) #:use-module (crates-io))

(define-public crate-first_crate_test_12345-0.1.0 (c (n "first_crate_test_12345") (v "0.1.0") (h "0m0r98js7hr039k6n4sgilr9h2aymlscn6m866ahrlhs0lgp2651")))

(define-public crate-first_crate_test_12345-0.1.2 (c (n "first_crate_test_12345") (v "0.1.2") (h "1c0shkyv7w6qh6arg9q5dws6pjadr0sbz7gaaxfcgjlwkc1pdglm")))

