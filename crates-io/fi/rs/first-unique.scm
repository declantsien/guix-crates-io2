(define-module (crates-io fi rs first-unique) #:use-module (crates-io))

(define-public crate-first-unique-0.1.0 (c (n "first-unique") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0d4p0gysjpq263c6db0s1zmrz1i8k8m49hd4pc0p5pp13cnr7w7s")))

