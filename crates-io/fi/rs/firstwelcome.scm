(define-module (crates-io fi rs firstwelcome) #:use-module (crates-io))

(define-public crate-firstwelcome-0.1.0 (c (n "firstwelcome") (v "0.1.0") (h "0dh10i5gzp54rq63n9c5x57qihpfy6lfqahgx4vpkn98qy0kjlhk")))

(define-public crate-firstwelcome-0.1.1 (c (n "firstwelcome") (v "0.1.1") (h "1i3h0sbxxn6frsa2bfmqc19fpjans8656iq2ffvfzzn3w369cckh")))

