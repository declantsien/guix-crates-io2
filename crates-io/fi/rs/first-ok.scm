(define-module (crates-io fi rs first-ok) #:use-module (crates-io))

(define-public crate-first-ok-0.1.0 (c (n "first-ok") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.17") (f (quote ("trust-dns"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "081i4nnwby816ml6ziff5bbm2d4b1f00c1iwfinfpphsacdq81pg")))

(define-public crate-first-ok-0.1.1 (c (n "first-ok") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.17") (f (quote ("trust-dns"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "1fa0znwvcpc019diajqz6g8dhg31mm4pjlj1c9rnm0ij0nijxx4q")))

(define-public crate-first-ok-0.1.2 (c (n "first-ok") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("trust-dns" "rustls-tls"))) (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "0s6vh1gdvwb0xx587kd6r7n226akxyxlmiwvjjfb4409ijv1wp4s")))

