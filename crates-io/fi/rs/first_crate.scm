(define-module (crates-io fi rs first_crate) #:use-module (crates-io))

(define-public crate-first_crate-0.1.0 (c (n "first_crate") (v "0.1.0") (h "1dl869vf9dqad1h7bg7wnh34wbfldi7pjm5a996czcy73q7khc5r") (y #t)))

(define-public crate-first_crate-0.1.1 (c (n "first_crate") (v "0.1.1") (h "0njsbzb9yvrwwzwpx0vkvwyvwg4sgvxa5myy5q96bjn1f6q6b46k") (y #t)))

(define-public crate-first_crate-0.1.2 (c (n "first_crate") (v "0.1.2") (h "101nliagv993m8k4madxb40v1v0rfhwj1jxmmn9srmlpg8qm8ig9") (y #t)))

(define-public crate-first_crate-0.1.3 (c (n "first_crate") (v "0.1.3") (h "1rbbp3kwspc1frwqkk4gmya1vqbqi9qzlmrrnlms31z0rc34wd4n") (y #t)))

(define-public crate-first_crate-0.1.4 (c (n "first_crate") (v "0.1.4") (h "18862ks31zf7nqld2l4jjpb2bn187jg6d76jdhnkhvgl0i039x3f") (y #t)))

(define-public crate-first_crate-0.1.5 (c (n "first_crate") (v "0.1.5") (h "13fvnqv3n7gx08530s8kldgjpgygqv8y5z8m6arw27abha79b47x") (y #t)))

(define-public crate-first_crate-0.1.6 (c (n "first_crate") (v "0.1.6") (h "16gnpjz8g267l1cnrf021jzvhyjh03i2k3pqyisc19qq9scnhm6n") (y #t)))

(define-public crate-first_crate-0.1.7 (c (n "first_crate") (v "0.1.7") (h "1555albrwrv5y4l0rwqxw1i94k406lyv0dyalh7d9zjahks3y2sb") (y #t)))

(define-public crate-first_crate-0.1.8 (c (n "first_crate") (v "0.1.8") (h "1blrnqs8g2836jh0c5agaccppip36c40wfndh3b96l1jhrzbnz9c") (y #t)))

(define-public crate-first_crate-0.1.9 (c (n "first_crate") (v "0.1.9") (h "1bhfk7d6yk1k7lh32lx1k44c6p0sdib5nd7p4l71rmp6zs1c51vv")))

(define-public crate-first_crate-0.2.0 (c (n "first_crate") (v "0.2.0") (h "1ykpvqyak6qn105jyki8ra4mpph5jq6cpyblcn2d30b6qjp6yfpa")))

