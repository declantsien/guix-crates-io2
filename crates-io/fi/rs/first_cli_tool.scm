(define-module (crates-io fi rs first_cli_tool) #:use-module (crates-io))

(define-public crate-first_cli_tool-0.1.0 (c (n "first_cli_tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "src") (r "^0.0.6") (d #t) (k 0)))) (h "02fsj0i7bdivvyixc5n58hl0j28k90hzhdzzzcrjd4yq7gsk0v9q")))

(define-public crate-first_cli_tool-0.1.1 (c (n "first_cli_tool") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "src") (r "^0.0.6") (d #t) (k 0)))) (h "1dkxgqq2d07ybcs6b0wf8p54wfnqszszjsdpladwl7zn4qbrpn88")))

