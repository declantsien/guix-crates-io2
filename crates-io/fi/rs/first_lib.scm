(define-module (crates-io fi rs first_lib) #:use-module (crates-io))

(define-public crate-first_lib-0.1.7 (c (n "first_lib") (v "0.1.7") (h "0v9cv5vyh2igx24p16d5xgpzzmz8bkjk65ijx9hr1h0almkf6bdf")))

(define-public crate-first_lib-0.1.10 (c (n "first_lib") (v "0.1.10") (h "063z8lc8vmcpmf3jak8wrkhll9q8xd344a4dg3k9j8psviw3zhjh")))

