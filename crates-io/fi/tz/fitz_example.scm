(define-module (crates-io fi tz fitz_example) #:use-module (crates-io))

(define-public crate-fitz_example-0.1.0 (c (n "fitz_example") (v "0.1.0") (h "1n34199jhp5i2n1yz0facb2m5azd41s1y8k7rdx7fllgmvnpfy0v") (y #t)))

(define-public crate-fitz_example-0.2.0 (c (n "fitz_example") (v "0.2.0") (h "13wz968fsnb4sxy2vg804n1ygr9wnq53wnxqlf285mx4a9dbssxi") (y #t)))

