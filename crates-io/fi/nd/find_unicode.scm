(define-module (crates-io fi nd find_unicode) #:use-module (crates-io))

(define-public crate-find_unicode-0.1.0 (c (n "find_unicode") (v "0.1.0") (d (list (d (n "skim") (r "^0.6.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0i11hsh4lsqcwdscpl4xbkf64hm18g6mmc82l2zag1dnvdcqd8z5")))

(define-public crate-find_unicode-0.2.0 (c (n "find_unicode") (v "0.2.0") (d (list (d (n "skim") (r "^0.6.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1gmqjx2agvzpk7q1lzknwcqv1pcjbvk57l8via35wv4ai20dqrng")))

(define-public crate-find_unicode-0.3.0 (c (n "find_unicode") (v "0.3.0") (d (list (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1mpm3hc955zcd37yr79230knhgbysplc8gkyln45jz42x1f6zlfn")))

(define-public crate-find_unicode-0.4.0 (c (n "find_unicode") (v "0.4.0") (d (list (d (n "skim") (r "^0.9.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "11y2zfji21bh4jx6s972wna8yg7h9gbhlbjz8nkb120yrgy1cp47")))

