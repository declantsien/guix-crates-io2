(define-module (crates-io fi nd findex-plugin) #:use-module (crates-io))

(define-public crate-findex-plugin-0.1.0 (c (n "findex-plugin") (v "0.1.0") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)))) (h "19k8mzgcw3gzyiyygckjysl50kr58aybpwfc8rh5vzvhnyndwfps") (s 2) (e (quote (("findex_internals" "dep:libloading"))))))

(define-public crate-findex-plugin-0.7.0 (c (n "findex-plugin") (v "0.7.0") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)))) (h "0h1ags51pzlscf84kfc3d66s99vmkz8d1r0hrgnkccv3as0v7h7h") (s 2) (e (quote (("findex_internals" "dep:libloading"))))))

(define-public crate-findex-plugin-0.7.2 (c (n "findex-plugin") (v "0.7.2") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)))) (h "0zn2zpzq65gw1nad75dl1p72hqzhgdsqs14101mmpdndlzmjwzi4") (s 2) (e (quote (("findex_internals" "dep:libloading"))))))

(define-public crate-findex-plugin-0.8.0 (c (n "findex-plugin") (v "0.8.0") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "gtk") (r "^0.15.1") (f (quote ("v3_22"))) (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xb00dzpbxiqpiizpjilmgaxibachdmf09yvfqwvbly05mhf9hgg") (s 2) (e (quote (("findex_internals" "dep:libloading" "dep:gtk" "dep:serde"))))))

(define-public crate-findex-plugin-0.8.1 (c (n "findex-plugin") (v "0.8.1") (d (list (d (n "abi_stable") (r "^0.11.1") (d #t) (k 0)) (d (n "gtk") (r "^0.15.1") (f (quote ("v3_22"))) (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l642nffyk62g3kpna0zz7m7nha76k8kgxbhk5vl64xlvbcn7wir") (s 2) (e (quote (("findex_internals" "dep:libloading" "dep:gtk" "dep:serde"))))))

