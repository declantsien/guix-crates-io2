(define-module (crates-io fi nd find-target) #:use-module (crates-io))

(define-public crate-find-target-0.0.0 (c (n "find-target") (v "0.0.0") (h "0vdc9mar4sl80gfvinf3fx0hxc9i08h9z8s30rk7qvfvd2pvr9i8") (f (quote (("default"))))))

(define-public crate-find-target-0.1.0 (c (n "find-target") (v "0.1.0") (h "068j9fky0n4zgldpv81a1gsncy606r1aapl93fxzbl3bwjwm5v9w") (f (quote (("default"))))))

(define-public crate-find-target-0.1.1 (c (n "find-target") (v "0.1.1") (h "1kbf542c5pifzw7svvxrdg2rs0n71y365qdg5rg89ihd79s61jg2") (f (quote (("default"))))))

