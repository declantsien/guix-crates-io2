(define-module (crates-io fi nd find-simdoc) #:use-module (crates-io))

(define-public crate-find-simdoc-0.1.0 (c (n "find-simdoc") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "all-pairs-hamming") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0yg79ky1a4w9qg4nkmyngjlp15yqkkwcwg3al5bi7bqkkd0flwcb")))

(define-public crate-find-simdoc-0.1.1 (c (n "find-simdoc") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "all-pairs-hamming") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "09azfr7nsawb0cxhqmdv843ssxd3rcb06pjhamyzbjfa01dkdv3f")))

