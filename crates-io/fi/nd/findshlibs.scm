(define-module (crates-io fi nd findshlibs) #:use-module (crates-io))

(define-public crate-findshlibs-0.1.0 (c (n "findshlibs") (v "0.1.0") (h "104g3ynp8lxnp2vr0lq9gjn9vyrr0a8q0y2jj5vzrswjrzf0pd6h")))

(define-public crate-findshlibs-0.2.0 (c (n "findshlibs") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0hdc513sv8wjk3rj45s5cwkwicgq1kh52pc9xhm601ai5irhkw66") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.3.0 (c (n "findshlibs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "0bv0q7grzw9frki6s9xaim4ddmbq3rj30na6jc3cn12m32kxvi0w") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.3.1 (c (n "findshlibs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "192mg0ha8k8jhmizgkfqdxywdda20f9isys4ba1jg6i7x8lw1bhl") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.3.2 (c (n "findshlibs") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.29.0") (k 1)) (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "182r4h6c8k05yp7wvbmz4my6wqikq1q232rcqz44qvy0cn4b1isr") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.3.3 (c (n "findshlibs") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.29.0") (k 1)) (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)))) (h "1zsniz518d8p4h92bmgz0q1gkp0hc9g7ach4g35adxiplqnkzigq") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.4.0 (c (n "findshlibs") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.36.0") (k 1)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0pfym8la0yngbp1zzwmahvspw1frrdsy8acxjlcw1r0lf4vip508") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.4.1 (c (n "findshlibs") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.39.0") (t "cfg(target_os = \"macos\")") (k 1)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "045csyaxhygdiwsr21mqcd9m4c3r270xg3vrv6rssaz5nzwmhzrg") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.5.0 (c (n "findshlibs") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1n2vagn0q5yim32hxkwi1cjgp3yn1dm45p7z8nw6lapywihhs9mi") (f (quote (("nightly"))))))

(define-public crate-findshlibs-0.7.0 (c (n "findshlibs") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "18d2vp4yrx7vp3c6a5dnlm6v8c1x96w31crgyk0l8cj4fmgcs2m0")))

(define-public crate-findshlibs-0.8.0 (c (n "findshlibs") (v "0.8.0") (d (list (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r ">=0.2.65, <0.3.0") (d #t) (k 0)))) (h "1jc7yvc830mr29gvqpmc73qshf84nvd00xvaryv91ky7m4cgrbrp")))

(define-public crate-findshlibs-0.9.0 (c (n "findshlibs") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("psapi" "memoryapi" "libloaderapi" "processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0y1zp7vf7pc008nmnkdm0c4jjgar4y3p4n97hb0cf08g0sycx599")))

(define-public crate-findshlibs-0.10.0 (c (n "findshlibs") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("psapi" "memoryapi" "libloaderapi" "processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1xjp7fb3fg6w526dcva5v3rjz16wy86r1yd0a8dplg9rmfh4asnw")))

(define-public crate-findshlibs-0.10.1 (c (n "findshlibs") (v "0.10.1") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("psapi" "memoryapi" "libloaderapi" "processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1124w7v4n6ar5ah49q2rkqnbn7cry160s8h9kljjsqqpz2rzv4fn")))

(define-public crate-findshlibs-0.10.2 (c (n "findshlibs") (v "0.10.2") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("psapi" "memoryapi" "libloaderapi" "processthreadsapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0r3zy2r12rxzwqgz53830bk38r6b7rl8kq2br9n81q7ps2ffbfa0")))

