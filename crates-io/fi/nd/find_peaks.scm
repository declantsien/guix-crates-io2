(define-module (crates-io fi nd find_peaks) #:use-module (crates-io))

(define-public crate-find_peaks-0.1.0 (c (n "find_peaks") (v "0.1.0") (h "0ngav4wb9vj6n8qlhlcip0qyjkpm8jhjkwf6ys153fzfnbdc9ld4")))

(define-public crate-find_peaks-0.1.1 (c (n "find_peaks") (v "0.1.1") (h "00r1hjzbbr544qqd1pfi7nx0nim9ik883gybv4nhxvsr3n83hww5")))

(define-public crate-find_peaks-0.1.2 (c (n "find_peaks") (v "0.1.2") (h "0b11nv7898hmb97w7hi107i8l2n5v9j0l63qqir3b7aqk01z3rch")))

(define-public crate-find_peaks-0.1.3 (c (n "find_peaks") (v "0.1.3") (h "0akjv9a8ymsbyf790aq38qgfb57x98gs4q2142hgn0dkba0wr6vb")))

(define-public crate-find_peaks-0.1.4 (c (n "find_peaks") (v "0.1.4") (d (list (d (n "pyo3") (r "^0.13") (d #t) (k 2)))) (h "0pfjmvk2idlqr8xc9jn8qrlcrcvmrifg1i4rll47f7i8smbyihs2")))

(define-public crate-find_peaks-0.1.5 (c (n "find_peaks") (v "0.1.5") (d (list (d (n "pyo3") (r "^0.13") (d #t) (k 2)))) (h "10mrbpnkr1hraywpm3qdn6ykn6q8217424n7n269l0crvzl6zfhp")))

