(define-module (crates-io fi nd find_mountpoint) #:use-module (crates-io))

(define-public crate-find_mountpoint-0.1.0 (c (n "find_mountpoint") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (k 0)))) (h "0yd13hmqdfkd4hnz1lrydih9sm1d8y771pknrykbzrydik0bxviq")))

(define-public crate-find_mountpoint-1.0.0 (c (n "find_mountpoint") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0yf5rhcbfapmk66zsx3agasi5324ak15rm6mk14h9prwiw1i39h7")))

(define-public crate-find_mountpoint-1.0.1 (c (n "find_mountpoint") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1qg694cp94q17a81dhmr6y3m9qaslal86bg1n9h05xfl65ylplx4")))

(define-public crate-find_mountpoint-1.0.2 (c (n "find_mountpoint") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1grcvz1d0krbkbbmdx1pkzs1drd359rmb5d7xybbslq7g499cqz5")))

(define-public crate-find_mountpoint-1.0.3 (c (n "find_mountpoint") (v "1.0.3") (d (list (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1qpxzgq02lzs80qagrrpn4x068p404swi423ykdl8va1ad2ywyw3")))

