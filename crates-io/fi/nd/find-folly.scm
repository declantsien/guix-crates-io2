(define-module (crates-io fi nd find-folly) #:use-module (crates-io))

(define-public crate-find-folly-0.1.0 (c (n "find-folly") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vq1ic87v4s3g6k7zhvrc9f6rf63b8nadaly13pr1qp1annzg2sd")))

