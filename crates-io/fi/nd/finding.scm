(define-module (crates-io fi nd finding) #:use-module (crates-io))

(define-public crate-finding-0.1.2 (c (n "finding") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rust_util") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0sq9ha2hnpfy6r4knwij2bwwqjclz4z172q63xn51n1fjyjvi831")))

(define-public crate-finding-0.1.3 (c (n "finding") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rust_util") (r "^0.1.0") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "18i67kinp4bv63y98a8dzni9bhjdr1lbswb439md2j7svnkk8qf5")))

