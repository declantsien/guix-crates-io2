(define-module (crates-io fi nd find_cuda_helper) #:use-module (crates-io))

(define-public crate-find_cuda_helper-0.1.0 (c (n "find_cuda_helper") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0n5ql5sx3cs1kaybg0ib64zh652ffzhwig9sh3pwswymyp8mh9i5")))

(define-public crate-find_cuda_helper-0.1.1 (c (n "find_cuda_helper") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0lqwb4gdlqzj01fafyzsch7zk7v9vp8nqd4ha6z7zr1syfk5mhm9")))

(define-public crate-find_cuda_helper-0.1.2 (c (n "find_cuda_helper") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1kxj2489h0bmbnhgicd7ixqz7cs0b05kwds7vc16szam44s1fm35")))

(define-public crate-find_cuda_helper-0.2.0 (c (n "find_cuda_helper") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1bdxg8bmcqvnxb43y6bn2xnhszyi9bm0kndagp3iml1xb5ffdygr")))

