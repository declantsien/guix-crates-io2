(define-module (crates-io fi nd findminhs) #:use-module (crates-io))

(define-public crate-findminhs-2.1.0 (c (n "findminhs") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derivative") (r "^2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0gzrfsf4q79y1vxsr5m70ygf31rrmicf3vyylhda34g7mmdpnzy4") (f (quote (("debug-skipvec"))))))

