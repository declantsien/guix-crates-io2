(define-module (crates-io fi nd find-places-db) #:use-module (crates-io))

(define-public crate-find-places-db-0.1.0 (c (n "find-places-db") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)))) (h "15av8pcyj1wcpnnw3mbayn6mmaxqym8g06wla7d78fbybg7p4q0m")))

(define-public crate-find-places-db-0.2.0 (c (n "find-places-db") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xhh8hfdxia92c4cdn8jj3hfragbxv7zdmkzxas1z8n2llwvz50m")))

