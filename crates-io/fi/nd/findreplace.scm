(define-module (crates-io fi nd findreplace) #:use-module (crates-io))

(define-public crate-findreplace-0.1.0 (c (n "findreplace") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1q8sgr382q27kr45ghk6w2vnvl5jj1gwmn03dl0cdyxpqhvcwbkf")))

