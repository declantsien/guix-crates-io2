(define-module (crates-io fi nd find_empty_block) #:use-module (crates-io))

(define-public crate-find_empty_block-0.1.0 (c (n "find_empty_block") (v "0.1.0") (d (list (d (n "positioned-io") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)))) (h "0avfq8kapixf1911dvsaghyc9w1xjl22sk9xjkrf1s850kxy668i")))

