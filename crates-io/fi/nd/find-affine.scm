(define-module (crates-io fi nd find-affine) #:use-module (crates-io))

(define-public crate-find-affine-0.1.0 (c (n "find-affine") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "approx") (r "^0.5.1") (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("std"))) (k 2)))) (h "0vn2i7i0c4m5n7lrsywd0lz49xg3p819zf6d3xzij3q0i32xwvyg")))

