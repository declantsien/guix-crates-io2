(define-module (crates-io fi nd finder_info_bin) #:use-module (crates-io))

(define-public crate-finder_info_bin-0.2.0 (c (n "finder_info_bin") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "finder_info") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1pzy54fqi16q233lhn0dg8j733xqxjsdinxwjjfqrpiyf4nzwf8h") (f (quote (("xattr" "libc") ("default"))))))

