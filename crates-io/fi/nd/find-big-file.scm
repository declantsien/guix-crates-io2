(define-module (crates-io fi nd find-big-file) #:use-module (crates-io))

(define-public crate-find-big-file-0.1.0 (c (n "find-big-file") (v "0.1.0") (h "0xsj6iaaa5zwaj3phkgfs25m54byhj4kz3yn9k63ljni3gnzdzmq")))

(define-public crate-find-big-file-0.1.1 (c (n "find-big-file") (v "0.1.1") (h "17aqibig0hb950q1afs1nx30lgci3w3la6hxyampharqinz9q4gr")))

(define-public crate-find-big-file-0.1.2 (c (n "find-big-file") (v "0.1.2") (h "0dxrna06dsp43aq3bj5pviwkzaq67a9vhahjxdhjalkpsdslfyly")))

(define-public crate-find-big-file-0.1.3 (c (n "find-big-file") (v "0.1.3") (h "0q7ggq4ms47ds47fhqsnx0ph9dws92wc2k0x9gc99vvjyz2y82mw")))

(define-public crate-find-big-file-0.1.4 (c (n "find-big-file") (v "0.1.4") (h "0i07gp2xcc250llc66f85q4wx84m967a7lgzvnvvbdzcp98biciq")))

(define-public crate-find-big-file-0.1.5 (c (n "find-big-file") (v "0.1.5") (h "193nc2g4aq0i1qram4jyfqk55fvw9d0qivhd4zfyszclz7l37qzq")))

(define-public crate-find-big-file-0.1.6 (c (n "find-big-file") (v "0.1.6") (h "0i61bkfiwfnmxznfbkjcb4125wxwrpp5bbbhcbsz9ym55619g95g")))

(define-public crate-find-big-file-0.1.7 (c (n "find-big-file") (v "0.1.7") (h "1j605bir3vrayvcii9av3kix58n1mfqw6jzq5m24mznxn02drwb3")))

