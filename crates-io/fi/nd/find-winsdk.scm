(define-module (crates-io fi nd find-winsdk) #:use-module (crates-io))

(define-public crate-find-winsdk-0.1.0 (c (n "find-winsdk") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.5") (f (quote ("serialization-serde"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0bdlwx0i99rkhm59qwkgwqcazkq0rb9g05xl1y42crnb6y5jracq")))

(define-public crate-find-winsdk-0.1.1 (c (n "find-winsdk") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.5") (f (quote ("serialization-serde"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "19899kdmc54w1zvs6kgbjdgjmvsz0nrzk7qzm92d7rl758vc75ia")))

(define-public crate-find-winsdk-0.2.0 (c (n "find-winsdk") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.5") (f (quote ("serialization-serde"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "17hx0zly3q2pwrf3rp7mph10cac67sn3nxibc7wc2w0mhxxz3jx8")))

