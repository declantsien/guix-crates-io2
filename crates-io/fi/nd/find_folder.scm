(define-module (crates-io fi nd find_folder) #:use-module (crates-io))

(define-public crate-find_folder-0.1.0 (c (n "find_folder") (v "0.1.0") (h "04qyhn58k25p1yavg43jj0q4bnfrgry4vjpjn3klywacqf1b76xl")))

(define-public crate-find_folder-0.1.1 (c (n "find_folder") (v "0.1.1") (h "04kjkwlypiar0ws22k8mv5il9506hcrnb00wicfa4y187w3kspyp")))

(define-public crate-find_folder-0.2.0 (c (n "find_folder") (v "0.2.0") (h "0grjpx94cvxggw5l0gsi9iz95ns24jn9h3s23vf171xpz93j6pi4")))

(define-public crate-find_folder-0.3.0 (c (n "find_folder") (v "0.3.0") (h "06xr2sjiyjrwxlgjpf8ig6pq0ayfjv6qxmmfakw5j2ssp67h2vcz")))

