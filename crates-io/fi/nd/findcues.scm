(define-module (crates-io fi nd findcues) #:use-module (crates-io))

(define-public crate-findcues-0.3.2 (c (n "findcues") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "riff") (r "^1.0.1") (d #t) (k 0)) (d (n "tinystr") (r "^0.5.0") (d #t) (k 0)))) (h "018h8616ynd85flslgw1jk2qq9w2i8rc4gamhxkf4fcramp1cbri")))

