(define-module (crates-io fi nd findrs) #:use-module (crates-io))

(define-public crate-findrs-0.2.2 (c (n "findrs") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "0vvbgs72m97wzr5a4rv1kvrvdk101zgs4agazdhslg60rkxwa091")))

(define-public crate-findrs-0.3.0 (c (n "findrs") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "1wxzjgdpdk39nnljn6p041gabj6dskxs7xjzmrc61gp8wwmcdmgi")))

(define-public crate-findrs-0.3.1 (c (n "findrs") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "1lv5xkqbz0gdy8s08j0frbgs1ygh1aizflkdpvjn7hnzb7z8lz9g")))

