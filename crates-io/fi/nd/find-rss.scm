(define-module (crates-io fi nd find-rss) #:use-module (crates-io))

(define-public crate-find-rss-0.0.1 (c (n "find-rss") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^0.12.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "09ibnyrzcwwbj300vv46g3kn9xii9qyhiajy2hmjkrkdh2xk1z7x")))

