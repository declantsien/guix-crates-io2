(define-module (crates-io fi nd find-subimage) #:use-module (crates-io))

(define-public crate-find-subimage-0.1.0 (c (n "find-subimage") (v "0.1.0") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0jl0blq2ny4qlkwsmrz445bnn57f3bp220s7wgd62y9pk1nmp0f4") (f (quote (("simdeez-default-new") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.1 (c (n "find-subimage") (v "0.1.1") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "00qv4c87ix67fwv07c23n8hhld5ncw1k2pkn5r392f74bvp3shl7") (f (quote (("simdeez-default-new") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.2 (c (n "find-subimage") (v "0.1.2") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0m802g1pjzshvnbwvjk6fhkvaiz5v2lhxd97pydl64dk4drx453f") (f (quote (("simdeez-default-new") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.3 (c (n "find-subimage") (v "0.1.3") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0qch6rirblryrf8g7ppzjfsxa5kvahnw1c69zmfrildx2hzsqsmm") (f (quote (("simdeez-default-new") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.4 (c (n "find-subimage") (v "0.1.4") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "1y4kvpwccn075njr0c6v46xy5pi6zjfg058271cqzwmaighj6b05") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.5 (c (n "find-subimage") (v "0.1.5") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0xn91i5325dmy2i0b5cm07z2v2g9wgnn1fp5b6v01qn2g6k4cp18") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.6 (c (n "find-subimage") (v "0.1.6") (d (list (d (n "opencv") (r "^0.61.2") (f (quote ("clang-runtime" "imgproc"))) (o #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "09llyqmscvii4phfkg73x0kb570fk02l9bjcksf2gv7j30zdlzs7") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.7 (c (n "find-subimage") (v "0.1.7") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "143cah2d6wd58zlbj93iqhlbhyzqd6ibggrh216fy8dpdk8zzf7a") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.8 (c (n "find-subimage") (v "0.1.8") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "1bsajg29nhx8qfm78c014a18am2zjkjvwlj1mi9pccxdg9fcdwv2") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.9 (c (n "find-subimage") (v "0.1.9") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "17vdi6973mgmnc24cz04qw9gzs8l06nly9y3z3r5z1d4w500y5qv") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.10 (c (n "find-subimage") (v "0.1.10") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "18hqwl3mz66fxfjaplamv2w9gi4swdsxyx9z2j25pspc3199akky") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.11 (c (n "find-subimage") (v "0.1.11") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "089496yha3b31gkb0gf0xkm1f71ckgfi7xlbw6pj9rl4kjhivckv") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

(define-public crate-find-subimage-0.1.12 (c (n "find-subimage") (v "0.1.12") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "opencv") (r "^0.61") (o #t) (d #t) (k 0)) (d (n "simdeez") (r "^1.0.7") (o #t) (d #t) (k 0)))) (h "0jijkcwljjfs9v4l9j0nsq94ixihx14wq04hrsvx4kqv375ibk3r") (f (quote (("simdeez-default-new") ("docs-only" "opencv/docs-only") ("default" "simdeez" "simdeez-default-new") ("checked-simdeez"))))))

