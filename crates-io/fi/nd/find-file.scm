(define-module (crates-io fi nd find-file) #:use-module (crates-io))

(define-public crate-find-file-0.1.0 (c (n "find-file") (v "0.1.0") (h "1ywrh3qmkg5zfxrwlvjs8i8wxhzd8ayz0v3704vr44xn7w3y60z9")))

(define-public crate-find-file-1.0.0 (c (n "find-file") (v "1.0.0") (h "1xnypxd7bd8dqnia8rpsv7gdpn2lpmfia0ql1sj386d23f6zalpb")))

(define-public crate-find-file-1.0.1 (c (n "find-file") (v "1.0.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "063ikgsl1v9xsqhyb3p2nln87sbn2pdqcc931hl6rpfalbyyd1km")))

(define-public crate-find-file-2.0.0 (c (n "find-file") (v "2.0.0") (d (list (d (n "clap") (r "2.*.*") (d #t) (k 0)))) (h "1iidbsz7kw2sdx1jz6ldavq9lj0sy50gjvrpa6nszxppmjm12c49")))

