(define-module (crates-io fi nd find-semantic-release-config) #:use-module (crates-io))

(define-public crate-find-semantic-release-config-1.0.0 (c (n "find-semantic-release-config") (v "1.0.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02xybaiy0dv5rgmbd0zxaidlgkhjbixiyyzh5qbb07gyzbv73aaz")))

(define-public crate-find-semantic-release-config-1.0.1 (c (n "find-semantic-release-config") (v "1.0.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0048rklkn7icq56bqi2d4pj3cw1vz2xxny4pjzsz3px3yaql0l1d")))

(define-public crate-find-semantic-release-config-1.0.2 (c (n "find-semantic-release-config") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ryvlmvg9z4fz9pqpyhzsaxffd77yfmvhdnii8s5xrdxg4fgrpxz")))

(define-public crate-find-semantic-release-config-1.0.3 (c (n "find-semantic-release-config") (v "1.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lqbw2rlnnqbwvjmn1caib91n88zg9i4iqgwfa43la96hvykcvda")))

(define-public crate-find-semantic-release-config-1.0.4 (c (n "find-semantic-release-config") (v "1.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pmmm04g98p0r4lzwlncimvwfkkysab6drqknkrwg26ahg33agxv")))

(define-public crate-find-semantic-release-config-1.0.5 (c (n "find-semantic-release-config") (v "1.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16iiav03w035km4s8f8q94izw80d7w53nk86yqacgs4r902j1j71")))

(define-public crate-find-semantic-release-config-1.0.6 (c (n "find-semantic-release-config") (v "1.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hkxcw1ha2crjz9m0d8pzf66817p9skq620pwfgrjh7jx14gdw6l")))

(define-public crate-find-semantic-release-config-1.0.7 (c (n "find-semantic-release-config") (v "1.0.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15spjqicn0djhlr0pw2i9d57kw3n8ywnzxk4j794pxk4ziwsrdqq")))

(define-public crate-find-semantic-release-config-1.0.8 (c (n "find-semantic-release-config") (v "1.0.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "132i0ywscnicgvj6hhk8yl73n8j9wfcas1wgf7p3jbn9wpzalgfl")))

