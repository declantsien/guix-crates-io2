(define-module (crates-io fi nd finder) #:use-module (crates-io))

(define-public crate-finder-0.1.0 (c (n "finder") (v "0.1.0") (h "1bpxiavfly57b34qcvhnpqqwaddjz3cx38zfcpsmsrvgzbqag743")))

(define-public crate-finder-0.1.1 (c (n "finder") (v "0.1.1") (h "029g62p6cminw0jssslkp44f2wca3m4frm9cz35jyxdjn6mqhk3g")))

(define-public crate-finder-0.1.2 (c (n "finder") (v "0.1.2") (h "1h3i2nfrzb8hsjyf4n9gq0j2vxj3gmdwa6n7mal1ix600l93fra2")))

(define-public crate-finder-0.1.3 (c (n "finder") (v "0.1.3") (h "0wgkxd2r0y2l1sa901drpyjp38md0blg40r2y3v3ysk30d6s2z1z")))

(define-public crate-finder-0.1.4 (c (n "finder") (v "0.1.4") (h "16zcfw92gbnskn1lv30mndr7mr4v1100f7b1jpyynclmw5sj0whz")))

(define-public crate-finder-0.1.5 (c (n "finder") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bk1fv4wmrkhmggkiw98g249xh638d6h9lvfqmd45j6zn9p3xwh0")))

(define-public crate-finder-0.1.6 (c (n "finder") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "06r16kl7mdhp9bxja4xqzvwidz88ml0b0pbhv9h09x2h7sbiwj69")))

