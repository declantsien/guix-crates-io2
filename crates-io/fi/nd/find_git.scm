(define-module (crates-io fi nd find_git) #:use-module (crates-io))

(define-public crate-find_git-1.0.0 (c (n "find_git") (v "1.0.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0mjzjkrgr0rlhg69m2pbsnfga0sxbpbsx1m2y7ghdj5n91hyy781")))

(define-public crate-find_git-1.0.1 (c (n "find_git") (v "1.0.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0r3d4cd9j3ipv09sa5lkpy918mzgvsplm4zagprz0w7b2qmvq656") (y #t)))

(define-public crate-find_git-1.0.2 (c (n "find_git") (v "1.0.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0ad08162qphq7dqppkmqbmybsmk1pqw6anxyw921bhz9fvsz0y8a") (y #t)))

(define-public crate-find_git-1.0.3 (c (n "find_git") (v "1.0.3") (d (list (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "16589m031f3hd261dcbc8h6dl5vd9667z8rayaggpqmwadmgrlsg") (y #t)))

(define-public crate-find_git-1.1.0 (c (n "find_git") (v "1.1.0") (d (list (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0bada9i1w2rwqj21a6zcrxxafayf45bbdqsfbvl110if2imcdwi9") (y #t)))

(define-public crate-find_git-1.1.1 (c (n "find_git") (v "1.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0d68858fslhjn3fj0ik5m63g8c1xrv94mwfpp8jvz16rpg8zfk9y")))

(define-public crate-find_git-1.2.0 (c (n "find_git") (v "1.2.0") (d (list (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0q9ikn4gd9jnql7igjx7q527s0q4zn190riy606yq0py2rs7a5xr")))

