(define-module (crates-io fi nd findfile) #:use-module (crates-io))

(define-public crate-findfile-0.2.0 (c (n "findfile") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "137s5xf0w1gsn9s1rzszin2s96g3ng9na5yk3xrj80ahcqkqlzg8")))

(define-public crate-findfile-0.2.1 (c (n "findfile") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "1xhdzg3kmn69cyjjpcvpbvma732hfqp6kk4czjrmqhhzgbjq4l7j")))

(define-public crate-findfile-0.2.2 (c (n "findfile") (v "0.2.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "1l77yqrdxnhrljzb31yy8rnbl97b5gwzdf4s3c2zwr5psm1swb5j")))

(define-public crate-findfile-0.2.3 (c (n "findfile") (v "0.2.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "0a7xyhxqhxkw8fl6wy6439c76w2dixb0kp486lw1q0b7ac61yk9j")))

(define-public crate-findfile-0.2.4 (c (n "findfile") (v "0.2.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "1a30c1579ri8pbdhgd4zqv9rvwz0kw1gl2kxq4g35nx82d7yzxvz")))

(define-public crate-findfile-0.2.5 (c (n "findfile") (v "0.2.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "1rv5xjwq2r9d2jrfx2vsplkais824fz240l72y5z6l3544vj1dh3")))

(define-public crate-findfile-0.2.6 (c (n "findfile") (v "0.2.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "1gfcdvfjyqgfkik0d2z7bxibydk6p26519iwcz6fsjhk6vgv2zd6")))

(define-public crate-findfile-0.2.7 (c (n "findfile") (v "0.2.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("color" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "0af86k1m0wfv5wzbfc0l8yb94gwc9d0bb08m83m6m0q9d36xch4n")))

(define-public crate-findfile-0.2.8 (c (n "findfile") (v "0.2.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("color" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "os_str_bytes") (r "^6.5") (d #t) (k 0)))) (h "15cwca0g4m9xn06fs3djawmbbc87qgi3ldpv72ck30bgrc848p8d")))

