(define-module (crates-io fi nd find-crate) #:use-module (crates-io))

(define-public crate-find-crate-0.1.0 (c (n "find-crate") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0vq9l4kc9l7l71gm4ajjgaimf2as1pisj9zn4d97k6cxbjjk60hj")))

(define-public crate-find-crate-0.1.1 (c (n "find-crate") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "08qwi7h5m3x674g9s0d762h3y8rxk95apf1fghgb4sc5jn64r2kn")))

(define-public crate-find-crate-0.1.2 (c (n "find-crate") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0w9bni5dd1q5hd3736byy0bmij5x2kkg446l8z91nnxb69jf1z6p")))

(define-public crate-find-crate-0.2.0 (c (n "find-crate") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "1mp0p9fhf40hrzk0cvh305jq5b68347y9mh6hhikszgv5a8qacld")))

(define-public crate-find-crate-0.3.0 (c (n "find-crate") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "1ryiipi6l7w7mcysydyqs8jf9nb4nl8vmf2p7qdhmixmj43jz18a")))

(define-public crate-find-crate-0.4.0 (c (n "find-crate") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 2)) (d (n "quote") (r "^0.6.8") (d #t) (k 2)) (d (n "serde") (r ">= 1.0.7, < 2") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0qng26hifmlhxy03mjqr0d60kgl518m1vcgnjifdjnznvq8i1kfp")))

(define-public crate-find-crate-0.5.0 (c (n "find-crate") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0f4wj8qawh8dya7fammsjs03brjq7z645xpmrliyh30csba4l778")))

(define-public crate-find-crate-0.6.0 (c (n "find-crate") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "semver") (r "^0.10") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1ykxlvjmmzwwb647i9f42iqznfzp87m4pn69icjc22g4n3z25y51")))

(define-public crate-find-crate-0.6.1 (c (n "find-crate") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "semver") (r "^0.10") (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1di351zas0765nkldm44nr98l7ygksvi1ifpwag68czzx141syh5")))

(define-public crate-find-crate-0.6.2 (c (n "find-crate") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 2)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "0fd6d6ynzxkczc641lvyx60pbhv726xr07zn23v3j66cf0vrlbg8")))

(define-public crate-find-crate-0.6.3 (c (n "find-crate") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 2)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "1ljpkh11gj7940xwz47xjhsvfbl93c2q0ql7l2v0w77amjx8paar")))

