(define-module (crates-io fi nd finder_info) #:use-module (crates-io))

(define-public crate-finder_info-0.1.0 (c (n "finder_info") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0m862am8037h52r34m19017d91rabnzinrblgk8x7kkvi3dvnqg8") (f (quote (("xattr" "libc") ("default"))))))

(define-public crate-finder_info-0.2.0 (c (n "finder_info") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jn635a2fgc4hlwy75mjv5lhr08hn3bb4g0xarvj25j5xbc1bpj3") (f (quote (("xattr" "libc") ("default"))))))

(define-public crate-finder_info-0.2.1 (c (n "finder_info") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "124m17vv86sxvkl37x1g8z000mzqjc580zdanwwjv9szdj2b67vb") (f (quote (("xattr" "libc") ("default"))))))

(define-public crate-finder_info-0.2.2 (c (n "finder_info") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "17nb1yvn9km5a0ajbr1djpari935q86xkwsm5r8233w9ihif0m9j")))

(define-public crate-finder_info-0.2.3 (c (n "finder_info") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "096ccc0zfxsar3qhi0n0cc72kh3hp6rd3cdc3vca056j8hz8n1q0")))

