(define-module (crates-io fi nd findr) #:use-module (crates-io))

(define-public crate-findr-0.1.0 (c (n "findr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.1") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.8") (d #t) (k 0)))) (h "0g8z4y3g5grgmpw3wg42zzpjl3kbc1aya1v0vm3rxsbvqnr2ld0k")))

(define-public crate-findr-0.1.1 (c (n "findr") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.1") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.8") (d #t) (k 0)))) (h "1rwsq6x1hdgvmn8mfy076gqfws1jjhxmzgy2rxsbjp8nmpkpscks")))

(define-public crate-findr-0.1.2 (c (n "findr") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.2") (d #t) (k 0)) (d (n "gitignore") (r "^1.0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3.1") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.8") (d #t) (k 0)))) (h "1197lzy0am0w35l36ikjfvjpgmcas5g29p8yffh460912dcg9yds")))

(define-public crate-findr-0.1.3 (c (n "findr") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.2") (d #t) (k 0)) (d (n "gitignore") (r "^1.0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3.1") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.8") (d #t) (k 0)))) (h "1w76qqnkls1w3f0zcb47p6mz1zj4jb1j5ncpsi8bs3xmvn0rlj0r")))

(define-public crate-findr-0.1.4 (c (n "findr") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "ignore") (r "^0.4.2") (d #t) (k 0)) (d (n "lapp") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)))) (h "00giv0zr3inh7fc474xy1hz7zkwng5viqvgl55wkf650hg8m5baa")))

(define-public crate-findr-0.1.5 (c (n "findr") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "ignore") (r "^0.4.2") (d #t) (k 0)) (d (n "lapp") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "rhai") (r "^0.8.2") (d #t) (k 0)))) (h "03ybmpgz9yc2481kaqidvf96kj74hqwpzawjlwri8cijap6lcba9")))

