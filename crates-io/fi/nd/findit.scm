(define-module (crates-io fi nd findit) #:use-module (crates-io))

(define-public crate-findit-0.1.0 (c (n "findit") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a3g74q0vx4lvmcd1a5py47fhbgp2kzq1kpq33vpxgc0lvlbsq6i") (y #t)))

(define-public crate-findit-0.1.1 (c (n "findit") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "045jfh75r2nnmc2myf40z4zjp2r9mpm2zkaj6njd3y8spdqim1j1")))

(define-public crate-findit-0.1.2 (c (n "findit") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0aslxr0d1j9czpwziwrqzvyk5d13aig6vvxkk6w2ss51kbagpyib")))

(define-public crate-findit-0.1.3 (c (n "findit") (v "0.1.3") (d (list (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16w0kjz7d5klizm2izpykglhp1xk46iaa49nam612056wlir9frk")))

