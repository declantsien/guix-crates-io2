(define-module (crates-io fi nd find-rs) #:use-module (crates-io))

(define-public crate-find-rs-0.1.0 (c (n "find-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0fq4xvhkxgy0knaq3k57c1jz7rnazp8ncxafnl2hr62x992ibs62")))

(define-public crate-find-rs-0.1.1 (c (n "find-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1qmxb8y85ynmamrwaidclaf5akcscxhxlf5sv6qas8jb5ibxzhsj")))

(define-public crate-find-rs-0.1.2 (c (n "find-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0m0fkdwnjb3mrswb4xnjppbjbc69612v66d3rfwq7x7i9fi57zsd")))

(define-public crate-find-rs-0.1.3 (c (n "find-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "16h9bkqkhdsbhclvf3m4yn207dczcjfmxjxw57771fha35c4ig02")))

