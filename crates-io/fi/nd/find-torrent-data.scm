(define-module (crates-io fi nd find-torrent-data) #:use-module (crates-io))

(define-public crate-find-torrent-data-0.1.2 (c (n "find-torrent-data") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lava_torrent") (r "^0.4") (d #t) (k 0)) (d (n "multimap") (r "^0.8") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0y611gfjn8y895pc453wlwz9038symlpcr5ag8an1z3zzwadgkib")))

(define-public crate-find-torrent-data-0.1.3 (c (n "find-torrent-data") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lava_torrent") (r "^0.5") (d #t) (k 0)) (d (n "multimap") (r "^0.8") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0scnr1kg22v47r2zymqqy2iwdrnl9cjznhdvags4vwgrxpz915bm")))

