(define-module (crates-io fi nd find-binary-version) #:use-module (crates-io))

(define-public crate-find-binary-version-0.1.0 (c (n "find-binary-version") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "01iz3xq55ncacq2z0k7rywdvxf2bn64b9lwhgbsi6nwizwjqba2n")))

(define-public crate-find-binary-version-0.1.1-alpha.0 (c (n "find-binary-version") (v "0.1.1-alpha.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "0a3p3lqsc42lbmp8rlwx1vl5ykvk1g85yysdam0zswjvzwwaz3fa")))

(define-public crate-find-binary-version-0.1.1 (c (n "find-binary-version") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "15l9kdhh96x4c8gvhv1qd5pl4fc8lry0cnqc9cshpm5mqgkj3dj1")))

(define-public crate-find-binary-version-0.2.0 (c (n "find-binary-version") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "1326p1xrx25rzb9rkcndw3m7g5rna2ih3l87s0ayz2sp3p45wv98")))

(define-public crate-find-binary-version-0.2.1 (c (n "find-binary-version") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1q6ca6ri6lsac2l9wdayq9fyp04f0ahayh4mv3r09zlww888zpjg")))

(define-public crate-find-binary-version-0.2.2 (c (n "find-binary-version") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "12bj366g3rbcblf6nq98840mmbahiiqjxvpf094zra3c2ygwvmir")))

(define-public crate-find-binary-version-0.3.0 (c (n "find-binary-version") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.4") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "05g5s60cf891jqrg6hw8n1nmimw6z4qsxccby71zspyzxzgmd926")))

(define-public crate-find-binary-version-0.3.1 (c (n "find-binary-version") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.5") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1b1mb3a6k7vp3dvvpjxfcsvd5qsqzpacj93smn5fjxvb9277zfwm")))

(define-public crate-find-binary-version-0.3.2 (c (n "find-binary-version") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.6") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1s3z0pmrxhdd6l5pnm3pwwqa9yi872kccx5ng78aqclp022cbvns")))

(define-public crate-find-binary-version-0.3.3 (c (n "find-binary-version") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0scpzp5wb6ykm8g2ljcq4lp6haisablld88nxqfahg6jqag3lvx0")))

(define-public crate-find-binary-version-0.3.4 (c (n "find-binary-version") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1wixq5bjyhdwy2ibjaq4d4vib174x440rh9isx58w1f65pxkhc06")))

(define-public crate-find-binary-version-0.3.5 (c (n "find-binary-version") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0izp16h8b17sb9mpb9glxkp09apjpm8rbbcgqwsyg8zqr8wccs51")))

(define-public crate-find-binary-version-0.3.6 (c (n "find-binary-version") (v "0.3.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0x1dkzxim3d9j8rqn8frbpn98myk5wqbnnv4r6pqbms3y678nwby")))

(define-public crate-find-binary-version-0.3.7 (c (n "find-binary-version") (v "0.3.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "026w4ifpbf7slcbk14cvzrky9fhngcnb0hzb3j99d6w30683qg0a")))

(define-public crate-find-binary-version-0.3.8 (c (n "find-binary-version") (v "0.3.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "00ms42w3bpv2zv0yxjvqi3cj7q8rcc5h53hx2c6nzlxq160ahlh5")))

(define-public crate-find-binary-version-0.3.9 (c (n "find-binary-version") (v "0.3.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1xxm94jjgcmmidlpmvpqh74fngc6xrgz10sdljb2k2jl4g5909iq")))

(define-public crate-find-binary-version-0.4.0 (c (n "find-binary-version") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.12") (f (quote ("tokio_support"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0l9823ccl1lrkxmbz91kz9b2q579nnsc6l46xsmw9b7q56wnvskn")))

(define-public crate-find-binary-version-0.5.0 (c (n "find-binary-version") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "compress-tools") (r "^0.14") (f (quote ("tokio_support"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0dbvyw3mjmz9j2riy0wm2w1n8xyr6g984gzvdnlmihwrvj9rplc3")))

