(define-module (crates-io fi dg fidget_math) #:use-module (crates-io))

(define-public crate-fidget_math-0.1.0 (c (n "fidget_math") (v "0.1.0") (d (list (d (n "fidget") (r "^0.2.7") (d #t) (k 0)))) (h "1nyy4g87mbj6692965g7j0841d2k5h8hvqi9cidir5j6vqs7xba8")))

(define-public crate-fidget_math-0.1.1 (c (n "fidget_math") (v "0.1.1") (d (list (d (n "fidget") (r "^0.2.7") (d #t) (k 0)))) (h "09ld16scrhlwyg8gnl4qfdv1paa5zy14w2mngjs7jw205d03sy0r")))

(define-public crate-fidget_math-0.3.0 (c (n "fidget_math") (v "0.3.0") (d (list (d (n "fidget") (r "^0.3") (d #t) (k 0)))) (h "10qpn5zvia2mpyvvnbg0ars2730acki1wkwc38xi3g2j53clay50")))

