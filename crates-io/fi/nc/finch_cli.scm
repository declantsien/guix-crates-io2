(define-module (crates-io fi nc finch_cli) #:use-module (crates-io))

(define-public crate-finch_cli-0.4.0 (c (n "finch_cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "finch") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1klcwajrga6q9sizn0rbil3p0dxw638af9cz3jdrpyzhl6qhysqd")))

(define-public crate-finch_cli-0.4.1 (c (n "finch_cli") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "finch") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1d4jxhl3ab2ywx1hnzy5zppwms5xmrggfgmshyvad1gp2hgc5ds8")))

(define-public crate-finch_cli-0.5.0 (c (n "finch_cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "finch") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "00bmi5iynynvfzcahj5c887pll6zakp9hdy0ginxz9si4bhpzqdv")))

