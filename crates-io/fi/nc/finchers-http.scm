(define-module (crates-io fi nc finchers-http) #:use-module (crates-io))

(define-public crate-finchers-http-0.11.0 (c (n "finchers-http") (v "0.11.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "finchers-core") (r "^0.11.0") (d #t) (k 0)) (d (n "finchers-ext") (r "^0.11.0") (d #t) (k 2)) (d (n "finchers-test") (r "^0.11.0") (d #t) (k 2)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "mime") (r "^0.3.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.4") (d #t) (k 0)))) (h "1fa90bmqx4q5dsrn13n2ggyvzngdkpk81qz5xyr9xyd2fraqjg8w")))

