(define-module (crates-io fi nc finchers-derive) #:use-module (crates-io))

(define-public crate-finchers-derive-0.11.0 (c (n "finchers-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0bj431g2n16xvb44mmia9wcalxlfvggxk27p0048q1lki137vg2l")))

