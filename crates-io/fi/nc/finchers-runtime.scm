(define-module (crates-io fi nc finchers-runtime) #:use-module (crates-io))

(define-public crate-finchers-runtime-0.11.0 (c (n "finchers-runtime") (v "0.11.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "finchers-core") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (f (quote ("compat"))) (k 0)) (d (n "scoped-tls") (r "^0.1.1") (d #t) (k 0)) (d (n "slog") (r "^2.2.3") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 0)))) (h "1kvxk7paq21l8wrz1bsj3qm6rr89gbndf2fw20lj5qfv39i1pazw")))

