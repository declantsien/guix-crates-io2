(define-module (crates-io fi nc finchers-test) #:use-module (crates-io))

(define-public crate-finchers-test-0.11.0 (c (n "finchers-test") (v "0.11.0") (d (list (d (n "finchers-core") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)))) (h "01dv0wvp61k3jici9dazxi5gvaqbzi04x6m2zls3lrwax1pgymij")))

