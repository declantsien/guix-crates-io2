(define-module (crates-io fi nc finchers-core) #:use-module (crates-io))

(define-public crate-finchers-core-0.11.0 (c (n "finchers-core") (v "0.11.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (o #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.1") (d #t) (k 0)))) (h "0hrjx100rpx959mn5602jcyc7rsrhv1ikfm5j7xb4w8j1h8yghga") (f (quote (("nightly") ("default" "hyper"))))))

