(define-module (crates-io fi fo fifo) #:use-module (crates-io))

(define-public crate-fifo-0.1.2 (c (n "fifo") (v "0.1.2") (h "007p5w0k7mg0iivagrhlfpc6vjymmgkp3d75b4zgmqi4hg5qgyg9")))

(define-public crate-fifo-0.1.3 (c (n "fifo") (v "0.1.3") (h "1rcmrpvf3j71x69vcqylk0grdagm8rcadds44g7xixkqvg21qp7v")))

(define-public crate-fifo-0.1.4 (c (n "fifo") (v "0.1.4") (h "0lm3lyyiqwvji6dcy1cznc7qiq4za9fcryyr8d2p6iv1aikll156")))

(define-public crate-fifo-0.1.5 (c (n "fifo") (v "0.1.5") (h "0dfz64xbwq2kk50yy4fa2saxqlp0djz1pxdlnjyzf497n3isihb4")))

(define-public crate-fifo-0.2.0 (c (n "fifo") (v "0.2.0") (h "1birrm30izmacdd9m3pl6fdxacpd332bwwzk5zipg8k3a04i1r5f")))

