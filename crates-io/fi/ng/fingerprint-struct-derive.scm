(define-module (crates-io fi ng fingerprint-struct-derive) #:use-module (crates-io))

(define-public crate-fingerprint-struct-derive-0.1.0 (c (n "fingerprint-struct-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "06bagygmlxl9n6b4nnalcbjpx5vqlcf8y6q5avs35njpv6sbvj3y") (r "1.63")))

