(define-module (crates-io fi ng fingers) #:use-module (crates-io))

(define-public crate-fingers-0.1.0 (c (n "fingers") (v "0.1.0") (h "1hifqrcyr1lv945yybsbm7dl8ms6js4lc9m8wqslpidjnmfc5wxn")))

(define-public crate-fingers-0.1.1 (c (n "fingers") (v "0.1.1") (h "0j62grcahiz5rkqfwghjhnw2jvbvjx86p5lii91bq7ihlbgnb1kw")))

(define-public crate-fingers-0.1.2 (c (n "fingers") (v "0.1.2") (h "1qd11qpkzgnn6s94h04l9s6dlmp4v7wnv4slv9xxam2885i7fkhh")))

