(define-module (crates-io fi ng fingerprint) #:use-module (crates-io))

(define-public crate-fingerprint-0.0.1 (c (n "fingerprint") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.0.3") (d #t) (k 0)))) (h "0hwzk44xlk4pj3x34a5jhywpmj2i3cz895h14v1nhwycz39f8n39") (f (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

(define-public crate-fingerprint-0.0.2 (c (n "fingerprint") (v "0.0.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "094b03rhlg2ljqnncw8pnhclkd194rr9lc07652l8vy9hg4rp9mp") (f (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

(define-public crate-fingerprint-0.0.3 (c (n "fingerprint") (v "0.0.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "19qimsw00xra4md5jdsfd0jfcdpasgx8srqxlg2kail8jspsw1ds") (f (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

