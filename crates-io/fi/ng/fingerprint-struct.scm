(define-module (crates-io fi ng fingerprint-struct) #:use-module (crates-io))

(define-public crate-fingerprint-struct-0.1.0 (c (n "fingerprint-struct") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 2)) (d (n "digest") (r "^0.10.5") (k 0)) (d (n "fingerprint-struct-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "mock-digest") (r "^0.1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (d #t) (k 2)))) (h "0lgx2lb8f0p4cass35w1ah0kk504zng256v2cdgfwr6skpv0k5pi") (f (quote (("std" "alloc") ("os") ("derive" "fingerprint-struct-derive") ("default" "std" "derive") ("alloc")))) (r "1.63")))

