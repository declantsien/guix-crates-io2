(define-module (crates-io fi ng fingertrees) #:use-module (crates-io))

(define-public crate-fingertrees-0.1.0 (c (n "fingertrees") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1n7qd6ypa7ycna7rq2aifvlq2l12si4xkw6vaywjsmzzwmrs6759")))

(define-public crate-fingertrees-0.1.1 (c (n "fingertrees") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0av20gai11srjfpd904djr9x2bvhycjizirr0ji4hnvnfl3rkx2c")))

(define-public crate-fingertrees-0.1.2 (c (n "fingertrees") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1jknjrh65annbffvvwycf4s9x18xwnadsh64f1zlndycj09c4zxm")))

(define-public crate-fingertrees-0.2.0 (c (n "fingertrees") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0ykgf42jmjimmkasi0agml46v80sin2g5c88f4j0mbvnbfhyhn5q")))

(define-public crate-fingertrees-0.2.1 (c (n "fingertrees") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "1k0sz5ghpq0hz2drvfh7073bj67fd38jnwb5xsni23k40y361jc6")))

(define-public crate-fingertrees-0.2.2 (c (n "fingertrees") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.2") (d #t) (k 2)))) (h "1amngnr972anp2r981w2kgp3jrcn6gmjnpfl28yk59028wymcxqm")))

(define-public crate-fingertrees-0.2.3 (c (n "fingertrees") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "07inrf5dzx5b14jbz9kk9mc7b4by8bcbgnj4hi8mj1jmzcx4yv05")))

(define-public crate-fingertrees-0.2.4 (c (n "fingertrees") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1845lkh72daax10pwf4k77y6zid2fmpb0fz297qpm34w8avvxp6g")))

(define-public crate-fingertrees-0.2.5 (c (n "fingertrees") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "08dz19y32njxfd14rznxhjrz8naw7sygf2m72554vic090yvrlbp")))

(define-public crate-fingertrees-0.2.6 (c (n "fingertrees") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1l3f1xml87ikjnhf7v1lv56ar80zdkwfk1x87cvi2s25967pvdjk")))

(define-public crate-fingertrees-0.2.7 (c (n "fingertrees") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0pdd8f2jciqqg0l7z1wimdngmy2dc4kwbkrhaj1zw5w6vg02bajk")))

(define-public crate-fingertrees-0.2.8 (c (n "fingertrees") (v "0.2.8") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0wzpqrx86m07cbpmqxxn8pny7fbv2fc47cxqpxqlgwgvfqp7ymgm")))

(define-public crate-fingertrees-0.2.9 (c (n "fingertrees") (v "0.2.9") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "12safqbqi4qq0ymx2riq826kjqqcggs1lrgg6hnsrh9315syiiwz")))

(define-public crate-fingertrees-0.2.10 (c (n "fingertrees") (v "0.2.10") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0zyfjljm1hl2nxbvmgx3508fk4s4sqwhks1cwidrgx9klbcrcpwc")))

(define-public crate-fingertrees-0.2.11 (c (n "fingertrees") (v "0.2.11") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1xsw2mpfg9js81njb9jhw9fcxjp03m8jnj204dvjg7fckrz9w102")))

