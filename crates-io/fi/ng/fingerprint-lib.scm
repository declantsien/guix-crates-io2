(define-module (crates-io fi ng fingerprint-lib) #:use-module (crates-io))

(define-public crate-fingerprint-lib-0.1.0 (c (n "fingerprint-lib") (v "0.1.0") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0qsaf618l9jsrzamx9jclm7i94pdm67nxjm7a4wws3vgc406xhfq")))

(define-public crate-fingerprint-lib-0.1.1 (c (n "fingerprint-lib") (v "0.1.1") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0hbzig1ysfhglw75nwqwxjzlhyi88bavmw8gp57fgw2d4pgnmq2b")))

(define-public crate-fingerprint-lib-0.1.2 (c (n "fingerprint-lib") (v "0.1.2") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "13mqhwfw9r8i2asxb558xjvawasak2yx9ryplxp0ajln6pn3gmc2")))

(define-public crate-fingerprint-lib-0.1.3 (c (n "fingerprint-lib") (v "0.1.3") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1ff2lvg54d938l3wjyl6mg3hklnc7b0c0820gzm8v5q9favslz1p")))

(define-public crate-fingerprint-lib-0.1.4 (c (n "fingerprint-lib") (v "0.1.4") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "0wnx6ynsxxqs2ikv4vakghm4ljai3lb9y1v5d0sb0x7iaq96208v")))

(define-public crate-fingerprint-lib-0.1.5 (c (n "fingerprint-lib") (v "0.1.5") (d (list (d (n "mac_address") (r "^1.1.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "1v61ffp2hq645xldzklh8l43m1qlwgrlwa3rlqjbrs28zafrjj0n")))

