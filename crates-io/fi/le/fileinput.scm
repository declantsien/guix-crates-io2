(define-module (crates-io fi le fileinput) #:use-module (crates-io))

(define-public crate-fileinput-0.1.0 (c (n "fileinput") (v "0.1.0") (h "1fq6sy1137kghdhp0f6ijx3f241b74rp3m6hhi02y3d64kwlrc5y")))

(define-public crate-fileinput-0.2.0 (c (n "fileinput") (v "0.2.0") (h "0r560g6fabgn73rcgj3j5g3h0vip3dj0p0w1ws6h2isaxq9bh564")))

(define-public crate-fileinput-0.3.0 (c (n "fileinput") (v "0.3.0") (h "06vsmh13d4lwrmrn31qrpdgx87h6wp09m6g57i3ingcg5d0318sk")))

