(define-module (crates-io fi le file-utils) #:use-module (crates-io))

(define-public crate-file-utils-0.1.0 (c (n "file-utils") (v "0.1.0") (h "033jrlcrgqf4g29y8b7j38n9s53nqi1kdgrb3fp3wlfgirdqmzbi")))

(define-public crate-file-utils-0.1.1 (c (n "file-utils") (v "0.1.1") (h "0x9m8fmv2vhk0a7iyyzgb37hs6xwfz9wkq35bn0c5fpqjwxwa15q")))

(define-public crate-file-utils-0.1.2 (c (n "file-utils") (v "0.1.2") (h "0lyc2v0d2pz851da28rvkd9c8h1a3d3amsp0k2njf04pp4cbga1i")))

(define-public crate-file-utils-0.1.3 (c (n "file-utils") (v "0.1.3") (h "0sbdll4q5j9w9z48n40vdnkc4yp2zjszp7xbyijjgavmhc1607qd")))

(define-public crate-file-utils-0.1.4 (c (n "file-utils") (v "0.1.4") (h "1zcdk36sd3fykn5sbz911ninbbza7ls9s4r2d28cvrdz96gn1x6q")))

(define-public crate-file-utils-0.1.5 (c (n "file-utils") (v "0.1.5") (h "1d1sp3vvxlvdfv8vs60y8ynmj5glaz60cp8cwyla4jkp7ljcssca")))

