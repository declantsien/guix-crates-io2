(define-module (crates-io fi le file_env_const) #:use-module (crates-io))

(define-public crate-file_env_const-0.1.0 (c (n "file_env_const") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0dfjx1mav0h4hv6s998vr15khxj5d05qx62ylia8377xlpa9vq0l")))

(define-public crate-file_env_const-0.2.0 (c (n "file_env_const") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1qqy18nd77byxckw27q304pskvw6fvg1dqicwkb9c8pris9nadvb") (f (quote (("log"))))))

(define-public crate-file_env_const-0.3.0 (c (n "file_env_const") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "15l37xdiwlcjn52bbsigxkr2kh4zn0j0ziai8s0gp8mvd350h3nc") (f (quote (("log"))))))

