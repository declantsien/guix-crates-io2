(define-module (crates-io fi le filetools) #:use-module (crates-io))

(define-public crate-filetools-0.1.0 (c (n "filetools") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "184hr8zwl10rxvkvwg7sma69z18kzs5zfqdkl67szyccy5l5ryfs")))

(define-public crate-filetools-0.1.1 (c (n "filetools") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0w0jlm4yi8y1jv37gyayxp5ivn23ff1wzb0gsbn17fpa51jg6xmr")))

(define-public crate-filetools-0.2.0 (c (n "filetools") (v "0.2.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02iqjg516ppfj2pr9kd90cp5ccl8al7nkxsnaz93s848fv6xcxav")))

(define-public crate-filetools-0.3.0 (c (n "filetools") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yk1wk4gzbc3dlzy7zcj43cd0p5zlksgxwqdsdpj1jap22hvd5k5")))

