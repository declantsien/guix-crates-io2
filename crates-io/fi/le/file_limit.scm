(define-module (crates-io fi le file_limit) #:use-module (crates-io))

(define-public crate-file_limit-0.0.1 (c (n "file_limit") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0fnqy1bmlqryamnihmafcm2y2spaygz42s1zjc0hlfdak7hjprfi") (y #t)))

(define-public crate-file_limit-0.0.2 (c (n "file_limit") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0xz2vwg6z7dfcwvbclmqpffzli332jjysd4j08qmac7b6nb2h2ij")))

