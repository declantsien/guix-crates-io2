(define-module (crates-io fi le filehasher) #:use-module (crates-io))

(define-public crate-filehasher-0.1.0 (c (n "filehasher") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "084jjvb26hn5q8f32ksmsq5wan0wp6yn995w0vplpcbfg6ijnq0g")))

(define-public crate-filehasher-0.2.0 (c (n "filehasher") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "00mhbypjl7i9dbfxyraxsl4vfiapfxq6b94mqs8d5538d7496h0l")))

(define-public crate-filehasher-0.3.0 (c (n "filehasher") (v "0.3.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0xzyinlqxaz8hd6vx82pkmqvx9sns3zb5blvbph2yyk7d95zg6kx")))

