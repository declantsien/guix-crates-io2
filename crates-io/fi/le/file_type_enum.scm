(define-module (crates-io fi le file_type_enum) #:use-module (crates-io))

(define-public crate-file_type_enum-0.9.0 (c (n "file_type_enum") (v "0.9.0") (h "0c5h3y3hbn2lbyp6z05h0s3gxj7zsmk4vl092v2r7simq23i3ihp")))

(define-public crate-file_type_enum-0.9.1 (c (n "file_type_enum") (v "0.9.1") (h "1z88g8p3950pqd4xpg9s7n1qgvln4cr9r8xwv4jgfsv08x2vbyfr")))

(define-public crate-file_type_enum-0.10.0 (c (n "file_type_enum") (v "0.10.0") (h "00w2g5x4n8c122fygg6z7v5ni4xl9f9501c0rkq86ks3c45d6hc1")))

(define-public crate-file_type_enum-0.10.1 (c (n "file_type_enum") (v "0.10.1") (h "0zv702h9kkf90zjdqn3h4gz2a81svgcmzsyg487k4y1hm9f4klmw")))

(define-public crate-file_type_enum-0.10.2 (c (n "file_type_enum") (v "0.10.2") (h "0kikcji254yavr9fpxq9y9bcsxrn61zv910i2ivcz097r3dzw367")))

(define-public crate-file_type_enum-0.10.3 (c (n "file_type_enum") (v "0.10.3") (h "13bx1xn0fzaz9p6f4hdj8lzs3nmyn0f04njzjaswglsq0sanjha1")))

(define-public crate-file_type_enum-0.11.0 (c (n "file_type_enum") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0m546pirbm0b84l41r3nrj7kkxva20kqw8mb4d9hn474w9w12cqv") (f (quote (("mode-t-conversion" "libc") ("default"))))))

(define-public crate-file_type_enum-0.11.1 (c (n "file_type_enum") (v "0.11.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bd3y6mjxllkslyzkbkk9wh5plmsxpd5hnqp9njm4i6afvqhy1in") (f (quote (("mode-t-conversion" "libc") ("default"))))))

(define-public crate-file_type_enum-1.0.0 (c (n "file_type_enum") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1hsv2s9b2jf1dg66blbhjdj4ldpnw9idwz0r05brm5j9bxc3p4l0") (f (quote (("mode-t-conversion" "libc") ("default")))) (y #t)))

(define-public crate-file_type_enum-1.0.1 (c (n "file_type_enum") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0w5ifnlzp37wx5r65vp9mhqgkjdqrn3y9jqxhcli3sqszda2hms0") (f (quote (("mode-t-conversion" "libc") ("default"))))))

(define-public crate-file_type_enum-2.0.0 (c (n "file_type_enum") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1s1rwwy351aiq43zd1c48jzkjajszhx14kmydv1bmlv4yprhgszq") (f (quote (("mode-t-conversion" "libc") ("default"))))))

(define-public crate-file_type_enum-2.0.1 (c (n "file_type_enum") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0bziszy3bvyvikpg7sczsinbvbphh8rh2siix8vxmcz53di1cf6s") (f (quote (("mode-t-conversion" "libc") ("default"))))))

