(define-module (crates-io fi le file_logger) #:use-module (crates-io))

(define-public crate-file_logger-0.1.0 (c (n "file_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)) (d (n "toml") (r "^0.1.23") (d #t) (k 0)))) (h "1b9g8c0l376z8l2pa4id5wajdl1pyf5v18hip4s9ma14zj46rw00")))

