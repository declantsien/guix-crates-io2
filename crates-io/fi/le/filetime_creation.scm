(define-module (crates-io fi le filetime_creation) #:use-module (crates-io))

(define-public crate-filetime_creation-0.1.0 (c (n "filetime_creation") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.17") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rjnwngb483z2fxfylv74576gabayvna1prl34acv7avibdg0hxy")))

(define-public crate-filetime_creation-0.1.1 (c (n "filetime_creation") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.18") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bybpwi6pp9q402mznspf73933cql0ndz2f8j3qdd2qs4vj9cb3y")))

(define-public crate-filetime_creation-0.1.2 (c (n "filetime_creation") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.19") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pckc4rmhidj42bdkdy4gr3ni29c11ns267qjk2xy4f663vdw8c3")))

(define-public crate-filetime_creation-0.1.3 (c (n "filetime_creation") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.19") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19l4ihjkk4gbqlfl0pk2vm6cww5k3ia6x8sz8ma1sr9ayhr0xrdk")))

(define-public crate-filetime_creation-0.1.4 (c (n "filetime_creation") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.20") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19k6whdgdhcq19jjg2qg9wcbrrpza6fpjxj31jm0bfaf1q4klbfr")))

(define-public crate-filetime_creation-0.1.5 (c (n "filetime_creation") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.21") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l4lbgsflvisizbpj462s01bzvg4zh8fhgcbqhf54di3c9kig5lx")))

(define-public crate-filetime_creation-0.1.6 (c (n "filetime_creation") (v "0.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.22") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bfl3ndvsy27h4zzczy07gzpanqfwaj8h1hcym4wvrmlb8yj3sis")))

(define-public crate-filetime_creation-0.1.7 (c (n "filetime_creation") (v "0.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10vk0zwwr91qypkkrhshvlw2zgjfyxhy29d5p15b8wrv5493n9sd")))

(define-public crate-filetime_creation-0.2.0 (c (n "filetime_creation") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0prsxylygvwwmycf5jdnwd258ck53iv8800cbgg5kraham3msny2")))

