(define-module (crates-io fi le file_appender) #:use-module (crates-io))

(define-public crate-file_appender-0.1.0 (c (n "file_appender") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1yaxhncvzddvwjj5bkd6srhlil97miih7lgaf0m98ihan2jgqlpz")))

