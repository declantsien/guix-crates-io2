(define-module (crates-io fi le filestatrec) #:use-module (crates-io))

(define-public crate-filestatrec-0.0.3 (c (n "filestatrec") (v "0.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "187igmgj6ck79xki3p9a9li13w1j19jpbabg8jfdkjsdiz2b7n2s")))

(define-public crate-filestatrec-0.0.4 (c (n "filestatrec") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)))) (h "0gaddn5iimb7w1hqjyqqig3mghyfn32fw9mc3jykyg0gb2wl2gm7")))

(define-public crate-filestatrec-0.0.5 (c (n "filestatrec") (v "0.0.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("fs"))) (k 0)))) (h "04v1vw8qlzinrqsrwi4vaqb0kn6iwnhslcalqsgsvjan3q3n68zv")))

(define-public crate-filestatrec-0.0.6 (c (n "filestatrec") (v "0.0.6") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("fs"))) (k 0)))) (h "101zxjknnymy40zx264lc63v0jn93mgk4v7mygrl2ym40lz2jnqd") (r "1.61")))

(define-public crate-filestatrec-0.0.7 (c (n "filestatrec") (v "0.0.7") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rustix") (r "^0.36.6") (f (quote ("fs"))) (d #t) (k 0)))) (h "1a3kxn5rlkgximgza4bivsrwifz5kmrykjrl37h63gnkr0g47vny") (r "1.61")))

(define-public crate-filestatrec-0.0.8 (c (n "filestatrec") (v "0.0.8") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "0k2lnx071k0cng12y7mi8iijqvaly8y0gdf1p8a0d718b3cdf29i") (r "1.61")))

(define-public crate-filestatrec-0.0.9 (c (n "filestatrec") (v "0.0.9") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "1c7hs5p65asvc9363rvj8gk4zpa2c81zzlz9gsviwd2b0dk6klas") (r "1.61")))

