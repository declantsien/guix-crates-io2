(define-module (crates-io fi le filepath) #:use-module (crates-io))

(define-public crate-filepath-0.1.0 (c (n "filepath") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06qi4dwp58fnz0smj7fcz0zzjmqwh8vmnpb9xif8hgxjkyq9jk0w")))

(define-public crate-filepath-0.1.1 (c (n "filepath") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zrrk6y703ynig9s7kww19px40agyl83c5vgn26ra9l28lc07m8d")))

(define-public crate-filepath-0.1.2 (c (n "filepath") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k2dwi46ldgmsmmlgxsbjqj92kxg8kdpx50r2wd2hiy1rrps3ypp")))

