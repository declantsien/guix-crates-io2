(define-module (crates-io fi le file_mutex) #:use-module (crates-io))

(define-public crate-file_mutex-0.1.0 (c (n "file_mutex") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrrvwf73skdhxx6jyfmcmrvlb94y9can5c4z2jrrnhffnydlcfg")))

(define-public crate-file_mutex-0.1.1 (c (n "file_mutex") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8idjqh426jzh7wz50pbizrs28gpdki6lpkw5g4qgzwnqmvdhcq")))

