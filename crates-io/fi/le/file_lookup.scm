(define-module (crates-io fi le file_lookup) #:use-module (crates-io))

(define-public crate-file_lookup-0.1.0 (c (n "file_lookup") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)))) (h "1p4pmh7n7ncriv024ynv4fgbanklsc22kpp3sywjm832zw18iqbi") (y #t)))

(define-public crate-file_lookup-0.1.1 (c (n "file_lookup") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)))) (h "1d1ncclr2f9dwd0lx851cik2gzvpyc3pxfhgg8hy6dx7dpl1j3d4") (y #t)))

(define-public crate-file_lookup-0.2.0 (c (n "file_lookup") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ln8aczrm5h5hdvrbd5p6q6pz5wspxx3aaga40yf87s80my7rkkf") (y #t)))

(define-public crate-file_lookup-0.2.1 (c (n "file_lookup") (v "0.2.1") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1x7q47k4m3cyagqkrh8dbmvcds9g96rc2gxgvdfkdydw628h2x00")))

(define-public crate-file_lookup-0.2.2 (c (n "file_lookup") (v "0.2.2") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0dbi9avshy008izz8dcw9ywz346kaa9wnq2df2yhagr5blvi16vw")))

