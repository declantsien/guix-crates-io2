(define-module (crates-io fi le file-serve) #:use-module (crates-io))

(define-public crate-file-serve-0.1.1 (c (n "file-serve") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "1knj8bdsk8yiz4456j42151whb5padjbkjhjibaawxl4pavp024l")))

(define-public crate-file-serve-0.2.0 (c (n "file-serve") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "1wd7fr890ayc8i0ylrh2w0ispqi0wkpwk9snmik9z95i4y0rr837")))

(define-public crate-file-serve-0.2.1 (c (n "file-serve") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "0nbwihd21vqj07shkhah1gj6ywb7kskh2jmlkihbbp5516xxsfp4")))

(define-public crate-file-serve-0.2.2 (c (n "file-serve") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "00yqsr831z6kq53s08sw31dnmccqng0f2asx5fh94xlq7lwvyzjl") (r "1.64.0")))

(define-public crate-file-serve-0.2.3 (c (n "file-serve") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0iaqqlg65kjgcyjhwbs3xk54hlmsxqplqjsbcmz0nisq11ffpnzq") (r "1.64.0")))

(define-public crate-file-serve-0.2.4 (c (n "file-serve") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0bqs0rbnb1254z6krw0cyp9xjica6jc12dnz8isvnw3wgayx1lx7") (r "1.70.0")))

(define-public crate-file-serve-0.3.0 (c (n "file-serve") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1639qqpvyrjn2mvhv55gr3100aybmnac5bqr3w3b44hiwj5p6qin") (r "1.70.0")))

(define-public crate-file-serve-0.3.1 (c (n "file-serve") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0flvcmyja6c4gwx091bp43w4g07s6bykvcxzml13ysl71j8pgawh") (r "1.73")))

(define-public crate-file-serve-0.3.2 (c (n "file-serve") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "1yz53y2ihq4i1bqbd95s97xf6yyd7q64c24vbyps2qwr5nvdafhy") (r "1.76")))

