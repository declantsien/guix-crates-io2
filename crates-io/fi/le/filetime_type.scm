(define-module (crates-io fi le filetime_type) #:use-module (crates-io))

(define-public crate-filetime_type-0.1.0 (c (n "filetime_type") (v "0.1.0") (h "1l4nsryab88yiajqhp53n4jsxzxf8cckhz4k4193312sbcx4hyxw")))

(define-public crate-filetime_type-0.1.1 (c (n "filetime_type") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "12dpl3vhv3550yl6g35q532q701wzj1bhbvwjpy2kaz4zbmshlbn")))

(define-public crate-filetime_type-0.1.2 (c (n "filetime_type") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "16bp6vbc1vhm8ppcj1an0pqqq2m7xfqmxpyfnrndc9ghsyr853nz")))

(define-public crate-filetime_type-0.1.3 (c (n "filetime_type") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1xxqm7zi78jp047ig3rgdvfniykyn9nyaxpjjm6i3jy7dckpshaf")))

