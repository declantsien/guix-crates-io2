(define-module (crates-io fi le filesvc-rs) #:use-module (crates-io))

(define-public crate-filesvc-rs-0.1.0 (c (n "filesvc-rs") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.3") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17gvavxz0g6qw62xaj24b5g6lradc4zvc57jzidc6jcqf88g0gw1")))

(define-public crate-filesvc-rs-1.0.0 (c (n "filesvc-rs") (v "1.0.0") (d (list (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 2)) (d (n "reqwest") (r "^0.12") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1xgik5fiaj6h5gkqchm95dcgy8jz65gbgiciv23bivr2906761dc")))

