(define-module (crates-io fi le file_cabinet) #:use-module (crates-io))

(define-public crate-file_cabinet-0.1.1 (c (n "file_cabinet") (v "0.1.1") (h "034xsfczvhp2kpjs78kxrdy64b40ipf0mqdjdwb7490fidr12fvv")))

(define-public crate-file_cabinet-0.1.2 (c (n "file_cabinet") (v "0.1.2") (h "1wfx65zqbqvmyca7yd63f3a575kq7kdm8pxgfi10gvik6k9rwc9p")))

(define-public crate-file_cabinet-0.1.3 (c (n "file_cabinet") (v "0.1.3") (h "0128wmyqvrvlmib4czalil2p02zn5kmp38ps5ysy9yinxkypd637")))

(define-public crate-file_cabinet-0.1.4 (c (n "file_cabinet") (v "0.1.4") (h "0sfb43lx4pm0h8xy6x54qpvi7c5zpf4hj7j1m55gp9d2zmbpmlmz")))

