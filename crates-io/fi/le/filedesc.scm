(define-module (crates-io fi le filedesc) #:use-module (crates-io))

(define-public crate-filedesc-0.1.0 (c (n "filedesc") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "100i6z49a12vbn88bs6z19s0780337mba845vl6912g30fmf8mmp")))

(define-public crate-filedesc-0.2.0 (c (n "filedesc") (v "0.2.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1lyqrdm9pxk2gl4b98mmnq984bp4ipfihprz2ic5cz4322r780bh")))

(define-public crate-filedesc-0.3.0 (c (n "filedesc") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0j7swpr1hb62yzglpny2ys4zx62gzjkmr01c2igi3k63b2hvjp50")))

(define-public crate-filedesc-0.3.1 (c (n "filedesc") (v "0.3.1") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1cfsygsnxa8c4mj509lg7lzkas2ym1p6azqgq9zc1cj71wrb0jyx")))

(define-public crate-filedesc-0.4.0 (c (n "filedesc") (v "0.4.0") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "03s76d3rzqk3ap1d2xkkmlcpmx7ci9r3n8rhq012qc3qcddks7sq")))

(define-public crate-filedesc-0.5.0 (c (n "filedesc") (v "0.5.0") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "13gfmwrnjyl9pwvxqznw80wssb18gxb4f2c0fw65pdd8ipmdzmhp")))

(define-public crate-filedesc-0.6.0 (c (n "filedesc") (v "0.6.0") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "034lfi96s03khzr1dwrc5dzw0qfid3dwl31gsjazadbmx1giy4sm")))

(define-public crate-filedesc-0.6.1 (c (n "filedesc") (v "0.6.1") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1qa5vm9ar124jisz4ydjxp2zapgv4wfjkn7c12w88zpz18nqd16p")))

(define-public crate-filedesc-0.6.2 (c (n "filedesc") (v "0.6.2") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0skx0d1kb86qqyz70zj2giwk6iyp3v10dqky76ag6xcysx3y4qxh")))

(define-public crate-filedesc-0.6.3 (c (n "filedesc") (v "0.6.3") (d (list (d (n "assert2") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "07da1m3kin0f02f036rfnc60v44q1h1g07picj7w9x4wdn30r5n4")))

