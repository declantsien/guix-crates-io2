(define-module (crates-io fi le file-metadata) #:use-module (crates-io))

(define-public crate-file-metadata-0.0.1 (c (n "file-metadata") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6.4") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "file-metadata-mditem-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "12y2a5mpn0lfzibxkish5w99xplmqf0qkq5lrc3cip51zmlvrwms")))

(define-public crate-file-metadata-0.0.2 (c (n "file-metadata") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6.4") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "file-metadata-mditem-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "0yyjkjd6ydbj1hs2wkizr0cs81w3kfs1d7i4yp8szc0qz93xz0cp")))

(define-public crate-file-metadata-0.0.3 (c (n "file-metadata") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6.4") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "file-metadata-mditem-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "0fif9yjfy7c029d7wl0y8zqx8qpvvwkhjxcj2ig0mg8kmpxzsgcc")))

