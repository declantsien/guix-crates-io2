(define-module (crates-io fi le fileidentifier) #:use-module (crates-io))

(define-public crate-fileidentifier-0.1.0 (c (n "fileidentifier") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (o #t) (d #t) (k 0)))) (h "0j65xn155hfy4dkpi0vmqq23zcfj0rbf774j5pxsa49lwllfdzld") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-fileidentifier-0.1.1 (c (n "fileidentifier") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (o #t) (d #t) (k 0)))) (h "0x4n93ximj9s9cjd1fms6mssz1r3p8p8g81xi1inxzn08vq5l4bw") (f (quote (("wasm" "wasm-bindgen"))))))

