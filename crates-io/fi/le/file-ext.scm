(define-module (crates-io fi le file-ext) #:use-module (crates-io))

(define-public crate-file-ext-1.0.0 (c (n "file-ext") (v "1.0.0") (d (list (d (n "rust-web-server") (r "^2.0.0") (d #t) (k 0)))) (h "0vppmsxa41ddpw36h19sbysx3nmvr04zgl2hcl4nfl74zssbns1f") (r "1.64")))

(define-public crate-file-ext-2.0.0 (c (n "file-ext") (v "2.0.0") (d (list (d (n "rust-web-server") (r "^2.0.0") (d #t) (k 0)))) (h "0v34pdd4myw8si51h7ji900rg7sqhhr7iahkrndpaspbxy6aj81l") (r "1.64")))

(define-public crate-file-ext-8.0.0 (c (n "file-ext") (v "8.0.0") (d (list (d (n "rust-web-server") (r "^8.0.0") (d #t) (k 0)))) (h "1in6yx9s5965hhkafm06pfbp9ciyy15spdnx4n07cl6zp79y4zn9")))

(define-public crate-file-ext-8.0.1 (c (n "file-ext") (v "8.0.1") (d (list (d (n "rust-web-server") (r "^8.0.0") (d #t) (k 0)))) (h "01j8nqpzvn4v23wvjv5j0rm4ldj1dbpjzfn9j1fvfyi4a9q0j87q") (r "1.65")))

(define-public crate-file-ext-9.0.0 (c (n "file-ext") (v "9.0.0") (h "0n1xmh1c57ykpx4zd5gjarypnl5xabmki9mcik464xw8g39h0hzw") (r "1.66")))

(define-public crate-file-ext-8.0.3 (c (n "file-ext") (v "8.0.3") (h "1i05yd6wkvssinlrrqvg1ij5lp0klr47rn2yzflmd9y6rli2gvkq") (r "1.65")))

(define-public crate-file-ext-9.0.1 (c (n "file-ext") (v "9.0.1") (h "0qg436pxp55ff58qk0nivc3b5skg6l1ncw4gq650z9ldvf917ira") (r "1.66")))

(define-public crate-file-ext-9.0.2 (c (n "file-ext") (v "9.0.2") (h "0vfigxsajq65vcmpk3l45yf8w1dh3xks5ian58x2rqpdwbqga2rj") (r "1.66")))

(define-public crate-file-ext-9.0.3 (c (n "file-ext") (v "9.0.3") (h "12ixlny9n8224pbhl39an9x293hrqgjzwajfyizr5hky88mgvcb9") (r "1.66")))

(define-public crate-file-ext-10.0.0 (c (n "file-ext") (v "10.0.0") (h "10hiy1sdbyac388viivfl4m93vk5kl6qf66zbb0l85w70l4jm2il") (r "1.68")))

(define-public crate-file-ext-11.0.0 (c (n "file-ext") (v "11.0.0") (h "0q63ch2rsb833pa7nqas8dpmcps564sgd441pzydfhsfm65mrllf")))

(define-public crate-file-ext-12.0.0 (c (n "file-ext") (v "12.0.0") (h "1ka2qg0b00snl5h3q7dhack4rla00g11mmzyfjgdkmfkr47yc79i")))

