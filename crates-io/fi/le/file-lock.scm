(define-module (crates-io fi le file-lock) #:use-module (crates-io))

(define-public crate-file-lock-0.0.1 (c (n "file-lock") (v "0.0.1") (h "1xn249sap233sq1njq6p62gf1jbdqv9aqwvm9rlcpqr9wn7cmgdc")))

(define-public crate-file-lock-0.0.2 (c (n "file-lock") (v "0.0.2") (h "01sndpvvxckml8sqpbvwzwixv2nwg0g4w4gqph5bk2z8kix4zjx5")))

(define-public crate-file-lock-0.0.3 (c (n "file-lock") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0n8km90xjll7wksddd5whr5d1zpkycv0k3vg4i5w1k3bqwc9dcxj")))

(define-public crate-file-lock-0.0.4 (c (n "file-lock") (v "0.0.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1w9k3dzyjbajl1n03bqb6qlc3dhifvllhifnsk0fxfswj3b4yhn3")))

(define-public crate-file-lock-0.0.5 (c (n "file-lock") (v "0.0.5") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1xmlq73kxys9vlzh7zwvqxpirlqqzad3x0fgml1rwjqabx4ddnba")))

(define-public crate-file-lock-0.0.6 (c (n "file-lock") (v "0.0.6") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "03ql735dfhhl8nsdcf7m0pigd6cikz3ip8y7zqrw6k3xkkidzsgm")))

(define-public crate-file-lock-0.0.7 (c (n "file-lock") (v "0.0.7") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1wwcjvnsvwxjmll5lgdq0wbhp0c1fybnqqnr0hpzk8vnvamiwin1")))

(define-public crate-file-lock-0.0.8 (c (n "file-lock") (v "0.0.8") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0zxq4hdkzssk6i9d9gkxbicg6592yhv2y5hnbasxv7ffwq15rpfa")))

(define-public crate-file-lock-0.0.9 (c (n "file-lock") (v "0.0.9") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1gk7l34071y5xfwmkyzcg526zxkmam9sffkw0zf8dlr8jd4zmmxb")))

(define-public crate-file-lock-0.0.10 (c (n "file-lock") (v "0.0.10") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0bpgfp3vpj5vgx9ss6vl3alhsh5pnyijnwbclhvbxrdrac59d1mz")))

(define-public crate-file-lock-0.0.11 (c (n "file-lock") (v "0.0.11") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1fx42g6l6m90cnwqr89h9fw0q16917z6b06424cz66bzbl8zjf6i")))

(define-public crate-file-lock-0.0.12 (c (n "file-lock") (v "0.0.12") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1px608cbr2bdmdnwxxfh72k33f6pb9c215qg4rn0q3y7bxvvdz36")))

(define-public crate-file-lock-0.0.13 (c (n "file-lock") (v "0.0.13") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ialgkwzp1p6xp152c40iinpc52v7axfmc77n2fcqs70wn6r5zg4")))

(define-public crate-file-lock-0.0.14 (c (n "file-lock") (v "0.0.14") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "183h0lrzdz2w868qnzfp8glixmkn4sjr0qy2j5bl62rnyssccn8l")))

(define-public crate-file-lock-0.0.15 (c (n "file-lock") (v "0.0.15") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "032i9iydmqqbsqnldkd8f0c16yqx9hmi9vm54nznpdfky63c1xdl")))

(define-public crate-file-lock-0.0.16 (c (n "file-lock") (v "0.0.16") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "0qa4z0bh4nqpcjsvl0pz0iz9ylihvyznga4hkmmgspx8j6vwlydf") (f (quote (("unstable") ("default"))))))

(define-public crate-file-lock-0.0.17 (c (n "file-lock") (v "0.0.17") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "127lypx3i2h1asqyczfc865r2sj87dmj3aq1hr0nn76gkn3g0p02") (f (quote (("unstable") ("default"))))))

(define-public crate-file-lock-1.1.18 (c (n "file-lock") (v "1.1.18") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "045ms373arzikvs02v4ilbpp0hhyggwps6wxypqpfalqjk4r37qn")))

(define-public crate-file-lock-1.1.19 (c (n "file-lock") (v "1.1.19") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1df1vlwzqbvg6j8nb7mlsj96j75ds7hnw1ka08jlz19k9qnx5c6l")))

(define-public crate-file-lock-1.1.20 (c (n "file-lock") (v "1.1.20") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "18ki56lv3yjid8k6yb02h5advxgs4jcny2q9xw64hh9pkciqcr5i")))

(define-public crate-file-lock-2.0.1 (c (n "file-lock") (v "2.0.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "mktemp") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "063h32wrc6w64pm1zfx5zcvf49iw5cxsxh3nij77y1af9gsr3mia")))

(define-public crate-file-lock-2.0.2 (c (n "file-lock") (v "2.0.2") (d (list (d (n "cc") (r "=1.0.72") (d #t) (k 1)) (d (n "libc") (r "=0.2.112") (d #t) (k 0)) (d (n "mktemp") (r "=0.4.1") (d #t) (k 0)) (d (n "nix") (r "=0.23.1") (d #t) (k 0)))) (h "1yi43lsqdgglmplzkpzhpjbi4h6bpca8fx0lqk73r6nldyg6m9nd")))

(define-public crate-file-lock-2.1.3 (c (n "file-lock") (v "2.1.3") (d (list (d (n "cc") (r "=1.0.72") (d #t) (k 1)) (d (n "libc") (r "=0.2.112") (d #t) (k 0)) (d (n "mktemp") (r "=0.4.1") (d #t) (k 0)) (d (n "nix") (r "=0.23.1") (d #t) (k 0)))) (h "0dbbp2vsf58mbqx0x52ngq5rbzl1hskrzz04cn08jlraw3phpydm")))

(define-public crate-file-lock-2.1.4 (c (n "file-lock") (v "2.1.4") (d (list (d (n "cc") (r "=1.0.72") (d #t) (k 1)) (d (n "libc") (r "=0.2.121") (d #t) (k 0)) (d (n "mktemp") (r "=0.4.1") (d #t) (k 0)) (d (n "nix") (r "=0.24.1") (f (quote ("process"))) (k 0)))) (h "09ak5rzagx8siir6cc8yn95bhb22z23py15isv318hb9qbksqs3d")))

(define-public crate-file-lock-2.1.5 (c (n "file-lock") (v "2.1.5") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "mktemp") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("process"))) (k 0)))) (h "0z3f8m9ai3al658mxiip3r9c7699pd2bhkvd6lx9bighp6zskwh0")))

(define-public crate-file-lock-2.1.6 (c (n "file-lock") (v "2.1.6") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "mktemp") (r "^0.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("process"))) (k 0)))) (h "15m46yljnjilpmyqbbzc14d6fsimggq3vlg63avm3ri434mgq588")))

(define-public crate-file-lock-2.1.7 (c (n "file-lock") (v "2.1.7") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 2)))) (h "1k44wixqx147p51wlb9xj4ahwkrl6qbyd50ysrj6606cfnjfn766")))

(define-public crate-file-lock-2.1.8 (c (n "file-lock") (v "2.1.8") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 2)))) (h "1y4rzj8i7gqy9nk38x702vi64z1zh9n48ccg09r0l4ang1ld31d5")))

(define-public crate-file-lock-2.1.9 (c (n "file-lock") (v "2.1.9") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 2)))) (h "0yv46qsmbbmx67f86mh5b3csypfqjywc3i5a90x7262l1h0yk6zm")))

(define-public crate-file-lock-2.1.10 (c (n "file-lock") (v "2.1.10") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 2)))) (h "1fs7pwcb4cm6dajwbcbza5f60fsp4rwk80gjzb2wjf5i4gs8yy7c")))

(define-public crate-file-lock-2.1.11 (c (n "file-lock") (v "2.1.11") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("process"))) (k 2)))) (h "0r76lqmwdk5zcizw0di8ymrdgwav5lg7mx6hj81ab7bl1bw4h2q4")))

