(define-module (crates-io fi le file_transfer) #:use-module (crates-io))

(define-public crate-file_transfer-0.1.0 (c (n "file_transfer") (v "0.1.0") (h "1g7jn24bqxn8irmrmcciwlrvdh6m31hn9yym9ilsgzayk81vg9vp")))

(define-public crate-file_transfer-0.1.1 (c (n "file_transfer") (v "0.1.1") (h "0jhd60ln4q2kl6maiwg7jqvmxln1gx08xkkpsjv2m2ayq7qnnnys")))

