(define-module (crates-io fi le filetreelist) #:use-module (crates-io))

(define-public crate-filetreelist-0.1.0 (c (n "filetreelist") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0407k9zgfsxn70lxw6l7qmsjdi75siggskqr7snvhv23zys1z508")))

(define-public crate-filetreelist-0.1.1 (c (n "filetreelist") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0729ifawc9w5fy77940x8vgdp1sa7zl8kf9g703c9yy9mq74rld1")))

(define-public crate-filetreelist-0.1.2 (c (n "filetreelist") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "scopetime") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cbdcqhzkfd6nq9z41v7cgafb4rw81zcijibdiyn8ijw8l71dnky")))

(define-public crate-filetreelist-0.2.0 (c (n "filetreelist") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "scopetime") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03ay6grxjr7a9hz6d021kxbldzch13wvrsgp8fhv6al9j8kkbpwm")))

(define-public crate-filetreelist-0.3.0 (c (n "filetreelist") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "scopetime") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xgm1n6mz7qd6kgq1vyz6qh6qmqhvh5gwhdiypyybpbsbpjszlp3")))

(define-public crate-filetreelist-0.4.0 (c (n "filetreelist") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "scopetime") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yk6jk6mws35s615lb5djfna2c2s6wggfxr3hvnjamq6ybsly47w")))

(define-public crate-filetreelist-0.5.0 (c (n "filetreelist") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "scopetime") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13w6kglzix09c41r96f4abamw47d46p7s44bk2cidc5fgv37f3pn")))

(define-public crate-filetreelist-0.5.1 (c (n "filetreelist") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i7lnk5ncbjg2nfgadx8phmv7ihaak5872cclrdv54bl620id132")))

