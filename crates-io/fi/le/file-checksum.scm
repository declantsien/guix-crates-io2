(define-module (crates-io fi le file-checksum) #:use-module (crates-io))

(define-public crate-file-checksum-2.0.0 (c (n "file-checksum") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("suggestions" "derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 2)))) (h "1zcgnfpdc06wqgqfr87nffvl62cwwwkbjfwkpm3s5aay4jvs982i")))

