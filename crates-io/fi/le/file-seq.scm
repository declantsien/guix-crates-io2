(define-module (crates-io fi le file-seq) #:use-module (crates-io))

(define-public crate-file-seq-0.1.1 (c (n "file-seq") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1ckx8z46akfhhm2h1v9wc43cz9p6imsqf9xn2sycgscxyf4wc1v2")))

(define-public crate-file-seq-0.1.2 (c (n "file-seq") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0lxqwzsm0x553v3xrh5jl1gb3qfcgmrx41cn51vwsap6sihd63wv")))

(define-public crate-file-seq-0.1.3 (c (n "file-seq") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0ijvn9xiwc8vf6m35wp1cw5fz7m99c9xys1shv4z0ji5pp93nq8v")))

(define-public crate-file-seq-0.2.0 (c (n "file-seq") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1dmns1srq8qmq7wz954gf70fa9azbd9p5w25vzhp2v90h0im54sq")))

