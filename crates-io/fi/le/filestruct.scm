(define-module (crates-io fi le filestruct) #:use-module (crates-io))

(define-public crate-filestruct-0.2.0 (c (n "filestruct") (v "0.2.0") (d (list (d (n "filestruct_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1b4rfw0sg5gfm3vf5d9k9n897255my02yl8p1q3mbh0cpanqm6qb") (r "1.56.1")))

(define-public crate-filestruct-0.3.0 (c (n "filestruct") (v "0.3.0") (d (list (d (n "filestruct_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1as7vgmmpg1kglsbgwkb3630gpwkxkk81mhkfqalwn33ic90v04j") (r "1.56.1")))

