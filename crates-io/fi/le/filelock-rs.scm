(define-module (crates-io fi le filelock-rs) #:use-module (crates-io))

(define-public crate-filelock-rs-0.1.0-beta.1 (c (n "filelock-rs") (v "0.1.0-beta.1") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "12zhch8wbc6bdnw77ap01kxls1h19irsym3bzk7chycdly78nlgc")))

(define-public crate-filelock-rs-0.1.0-beta.2 (c (n "filelock-rs") (v "0.1.0-beta.2") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0yq552sjd9n7zskb8q7mbxhpv32n63z2ca4sxf65xfkv0q27qhmn")))

