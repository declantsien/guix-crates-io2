(define-module (crates-io fi le file-uti) #:use-module (crates-io))

(define-public crate-file-uti-0.1.0 (c (n "file-uti") (v "0.1.0") (d (list (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00iyja6vs3p9zlrn4fxg50jjhwf58qnkbx0s7jlnqrgdxg003d4z")))

(define-public crate-file-uti-0.1.1 (c (n "file-uti") (v "0.1.1") (d (list (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bsgwqgbw1hj0vp6v0q5rjbbavdxf25hhpd97yisbxvf18vs4c47")))

