(define-module (crates-io fi le filetypes) #:use-module (crates-io))

(define-public crate-filetypes-0.1.0 (c (n "filetypes") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0648blv2nx029wa5hpdwp8ijh38kdlrcr1v20w7xzwxpnzdll9hp")))

(define-public crate-filetypes-0.1.1 (c (n "filetypes") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "19n55kva7309ss1z2z8hzc6npw9n67ks3lx92k1nbhplwzx2ikwr")))

(define-public crate-filetypes-0.1.2 (c (n "filetypes") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "04dhhwssrdw0jdy7i43rqlp81dippk4b3cbywa08695qryvisrg4")))

