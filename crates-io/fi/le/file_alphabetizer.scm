(define-module (crates-io fi le file_alphabetizer) #:use-module (crates-io))

(define-public crate-file_alphabetizer-0.1.0 (c (n "file_alphabetizer") (v "0.1.0") (h "0kkk13j1cbcv5hj70pdyljp5i22znfifyaq56q5gjx3vykyb5rzz")))

(define-public crate-file_alphabetizer-1.0.0 (c (n "file_alphabetizer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j5jpizii8nq4dwr3qm4w9lg137p3lnp5h3ahdk30d1ik6417y9h")))

(define-public crate-file_alphabetizer-2.0.0 (c (n "file_alphabetizer") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lvfk2g4xlhnnfjga5c8vnk0kmm8kksv3i04js4hlahk7gay7462")))

(define-public crate-file_alphabetizer-2.1.0 (c (n "file_alphabetizer") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ir2h66rxb3h83ag4ladw3g3c5js78r6wl0dfx4yvrqmkkrsr0f")))

(define-public crate-file_alphabetizer-2.1.1 (c (n "file_alphabetizer") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w4aqnmwlx7m4k3apg2b1fxrwvp7ia64fagp9wnwy9abmg0wg8q2")))

(define-public crate-file_alphabetizer-2.1.2 (c (n "file_alphabetizer") (v "2.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16p7wqyxn2ym8ajbx9yb91qrpb7r9ha02ma99k003kf9afk0935q")))

(define-public crate-file_alphabetizer-2.1.3 (c (n "file_alphabetizer") (v "2.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1skv4d46cjdh7v4bhf9dd5bdqwnvl7966809pyn0dch5374chnaa")))

