(define-module (crates-io fi le file-per-thread-logger) #:use-module (crates-io))

(define-public crate-file-per-thread-logger-0.1.0 (c (n "file-per-thread-logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.11") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "19rd029d0m13aja1cz8zimkqw8c1nnvjc05iwzfaf6xvgc349snj")))

(define-public crate-file-per-thread-logger-0.1.1 (c (n "file-per-thread-logger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.11") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "01s47740jf7r1kyz8pxzpx0hw4v2byllp320skz4aangbkmynlk1")))

(define-public crate-file-per-thread-logger-0.1.2 (c (n "file-per-thread-logger") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1s9g4xafi65wf3k19lz74w4kw7rwvfkw8dyjilb8awpg65dvf1c5")))

(define-public crate-file-per-thread-logger-0.1.3 (c (n "file-per-thread-logger") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1lcyc037z5s26a16a0s2hyjwsz2nli4vl0gl7phx0jv653q3ffcb")))

(define-public crate-file-per-thread-logger-0.1.4 (c (n "file-per-thread-logger") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "09j1xfbzzx8asxhlpkfvrb0c8v38s11d6pamkf9wxybi8gcy1nsg")))

(define-public crate-file-per-thread-logger-0.1.5 (c (n "file-per-thread-logger") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0bx0w7b6x99asmxwi8g2y6x4wl0cjsp91sqsqx0ywfabay865q91")))

(define-public crate-file-per-thread-logger-0.1.6 (c (n "file-per-thread-logger") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0riq6p1267g9316qs4avfsccqvdczs8h2qzl9ax020krv4jy9wl4")))

(define-public crate-file-per-thread-logger-0.2.0 (c (n "file-per-thread-logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1ww3yqf7mdpdwb9b75rcbmxw3zg6wnny9jn8604sz2dg6cfc4g4a")))

