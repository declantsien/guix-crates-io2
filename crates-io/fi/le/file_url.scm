(define-module (crates-io fi le file_url) #:use-module (crates-io))

(define-public crate-file_url-0.1.0 (c (n "file_url") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "typed-path") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0ldgdlnxqn6h29259ghkvb6sm2vyd9h3r5ir2b60660alpkpqg4r")))

(define-public crate-file_url-0.1.1 (c (n "file_url") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "typed-path") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0clhwgq8a44zhv51b2d064892cdzgvhbqpk1j28858f7l2lf3l0w")))

