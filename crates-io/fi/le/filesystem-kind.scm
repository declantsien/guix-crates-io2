(define-module (crates-io fi le filesystem-kind) #:use-module (crates-io))

(define-public crate-filesystem-kind-0.1.0 (c (n "filesystem-kind") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "066pd6c7rm9asi7lhlwnc53c7044zd64pbz05hwcv1rv91gw3x1g")))

(define-public crate-filesystem-kind-0.2.0 (c (n "filesystem-kind") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)) (d (n "libc") (r "^0.2.139") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "037j8ndasjmhlhvi62h5613w7js7r2zh2xcldirfd6l48jnp9jm5")))

