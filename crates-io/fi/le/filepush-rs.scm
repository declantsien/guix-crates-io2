(define-module (crates-io fi le filepush-rs) #:use-module (crates-io))

(define-public crate-filepush-rs-0.1.0 (c (n "filepush-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "0pkaxxd1bc5vmyx388xisi8lbk2xbd90s8gmf63h9347y1876f52")))

(define-public crate-filepush-rs-0.1.1 (c (n "filepush-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "0jfsxnb0spn70y6plpylygll4d8mbx5z7x7yj01arjfaq351ndxs")))

(define-public crate-filepush-rs-0.1.2 (c (n "filepush-rs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "0v645krm3gwrqi23shxxfyjdhcqvnapkgdq61b6zgfj0gymqcb6y")))

(define-public crate-filepush-rs-0.1.3 (c (n "filepush-rs") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "0ca0mjfwgyclpch79g8cy49h2mr8kjd4hlws5mnzb58rashw9gr1")))

