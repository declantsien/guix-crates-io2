(define-module (crates-io fi le file-structure) #:use-module (crates-io))

(define-public crate-file-structure-0.0.1 (c (n "file-structure") (v "0.0.1") (h "05p2d8m51adfcmr6qxbqwmi0ff3hbwlsgh23bkhh5qi1ifsm420x") (y #t)))

(define-public crate-file-structure-0.0.2 (c (n "file-structure") (v "0.0.2") (h "1ryi84w97awkn3y9gcr2qf63z4wl31jjrbv8w70m6fvc90ls38z6") (y #t)))

(define-public crate-file-structure-0.1.0 (c (n "file-structure") (v "0.1.0") (h "1chryjl6c94b691qnfafxl90m4lh6a92fmmvqminpx6pxmxsrplm")))

(define-public crate-file-structure-0.1.1 (c (n "file-structure") (v "0.1.1") (h "1sbcn5sxxkp6ql5ilbk9idj4k2k042m5rkqmjadi9j18y8r0q1w2")))

