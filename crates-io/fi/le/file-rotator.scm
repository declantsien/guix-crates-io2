(define-module (crates-io fi le file-rotator) #:use-module (crates-io))

(define-public crate-file-rotator-0.1.0 (c (n "file-rotator") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1bbj6nwa04ahx7m8xvfhdpjiib6r5k4wiy014k64wbh4hyrhkl08")))

(define-public crate-file-rotator-0.1.1 (c (n "file-rotator") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1jbf9dlmw6c62ypf4cm6y4q2gqpp5w3j9hmlc63m39c5mdi7y3fv")))

(define-public crate-file-rotator-0.2.0 (c (n "file-rotator") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1y94rn8snprlc7li6pv99r10cl70vwd850ip1n0a5k6878jx7sby")))

(define-public crate-file-rotator-0.2.1 (c (n "file-rotator") (v "0.2.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1rc7s4hmklg4apnj4lnm9d6xw1h98fqzgv0rdgfww8k54sagmv8x")))

(define-public crate-file-rotator-0.2.2 (c (n "file-rotator") (v "0.2.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "0swlbsmkq7cvh4zv6kdwhrzqbvap17si4bmawrbkpk7l69b90005")))

(define-public crate-file-rotator-0.4.1 (c (n "file-rotator") (v "0.4.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "14cbdqr6knkjnz1bl20digxw23h06h9hlnca295cyv0s0l3516j6")))

(define-public crate-file-rotator-0.5.0 (c (n "file-rotator") (v "0.5.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "1mxgzy037ivb83x4x9likfg1k23wgfl4pqgs66whn1km9i72lwdw")))

(define-public crate-file-rotator-0.5.1 (c (n "file-rotator") (v "0.5.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0hrbwjqmrgv52k7qhlj9a6s7mx6xy05n57gr9vvr1adgb8fk09rm")))

(define-public crate-file-rotator-0.6.0 (c (n "file-rotator") (v "0.6.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0na773gp8x0nqc17gw1kb8r70byvwppcbp05jwb5w970xfznwvii")))

(define-public crate-file-rotator-0.6.1 (c (n "file-rotator") (v "0.6.1") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0ccw7g6mr16xbjbgz92d229ijd32z93rxrgyfs9pmzpymvbc4l79")))

(define-public crate-file-rotator-0.6.2 (c (n "file-rotator") (v "0.6.2") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0kkmj0p634cx8p9fd6lqlqj591mvb4zfhhlgbyrinpmaxlihy3fa")))

