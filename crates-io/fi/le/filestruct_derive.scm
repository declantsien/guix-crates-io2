(define-module (crates-io fi le filestruct_derive) #:use-module (crates-io))

(define-public crate-filestruct_derive-0.2.0 (c (n "filestruct_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "12pawly0gr6xiiygdr2h919q44xcg41kdqawc6c5mhw202q7hnw9") (r "1.56.1")))

(define-public crate-filestruct_derive-0.3.0 (c (n "filestruct_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0mfv4g861gjb352ijlpz7maas42qyrkdmahjc56d2r089ncjfc3h") (r "1.56.1")))

