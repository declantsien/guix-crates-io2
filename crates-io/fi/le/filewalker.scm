(define-module (crates-io fi le filewalker) #:use-module (crates-io))

(define-public crate-filewalker-0.0.1 (c (n "filewalker") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v1q5a5yhdwmxgx6r30l0r01pgwm0cnnn2rjwmcap01p0ns987nj")))

