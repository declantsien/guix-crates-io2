(define-module (crates-io fi le file-guard) #:use-module (crates-io))

(define-public crate-file-guard-0.1.0 (c (n "file-guard") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.109") (d #t) (t "cfg(unix)") (k 0)) (d (n "vmap") (r "^0.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "minwinbase" "minwindef" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c4p3w0z2963l4hymjf3shsvb16053fizdy8dvf55yqdbsd4q01y")))

(define-public crate-file-guard-0.2.0 (c (n "file-guard") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.109") (d #t) (t "cfg(unix)") (k 0)) (d (n "vmap") (r "^0.6") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winerror" "minwinbase" "minwindef" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z20z66gdd13pn2g0z01bwj0ljcrc9avwx8jyvdxghsyz6n75vr1")))

