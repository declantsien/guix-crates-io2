(define-module (crates-io fi le filebuffer) #:use-module (crates-io))

(define-public crate-filebuffer-0.1.0 (c (n "filebuffer") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "1x6b9bzw8ks1gw5rnxbnn49by6zid3yj26xh8kzbffyh2p6c5jx7")))

(define-public crate-filebuffer-0.1.1 (c (n "filebuffer") (v "0.1.1") (d (list (d (n "gcc") (r "= 0.3.35") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "time") (r "= 0.1.35") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0ms2ck88789gy16pysl6sb2rfvszjb3nrll56kilxw3hvj7s3yk1")))

(define-public crate-filebuffer-0.2.0 (c (n "filebuffer") (v "0.2.0") (d (list (d (n "gcc") (r "= 0.3.35") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "time") (r "= 0.1.35") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1yqa1qcbx5p5byf0ni5kpgrw6sw0h0zmrlgdr3rbcad18ad6n4yc")))

(define-public crate-filebuffer-0.3.0 (c (n "filebuffer") (v "0.3.0") (d (list (d (n "gcc") (r "= 0.3.35") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "time") (r "= 0.1.35") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1bszs79k4lqs38gkwvps3xls4bjldv2pp9w0vqb0a8bklv8i73gs")))

(define-public crate-filebuffer-0.4.0 (c (n "filebuffer") (v "0.4.0") (d (list (d (n "gcc") (r "= 0.3.35") (d #t) (k 2)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 2)) (d (n "time") (r "= 0.1.35") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "memoryapi" "processthreadsapi" "sysinfoapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0193wnllaxj6n7xdmaar9s5vkcqlfq3m3sab12fylqs2szhvyh8v")))

(define-public crate-filebuffer-1.0.0 (c (n "filebuffer") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.86") (d #t) (t "cfg(unix)") (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "memoryapi" "processthreadsapi" "sysinfoapi" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02g24q7ynca6q8f918hxwlcnn83311gk32nzxgkj6g75jm4bp5y9")))

