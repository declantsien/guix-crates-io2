(define-module (crates-io fi le file_cache) #:use-module (crates-io))

(define-public crate-file_cache-0.0.2 (c (n "file_cache") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "err") (r "^0.0.5") (d #t) (k 0) (p "rmw_err")) (d (n "file_open_limit") (r "^0.0.4") (d #t) (k 0)) (d (n "stretto") (r "^0.5.1") (f (quote ("async"))) (d #t) (k 0)))) (h "1f1m3a40zvzmcwckib2i2937xjpigpzwym12mq2lpgkdm4xzz712")))

(define-public crate-file_cache-0.0.3 (c (n "file_cache") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "err") (r "^0.0.5") (d #t) (k 0) (p "rmw_err")) (d (n "file_open_limit") (r "^0.0.4") (d #t) (k 0)) (d (n "stretto") (r "^0.5.1") (f (quote ("async"))) (d #t) (k 0)))) (h "1lhjbdlr97rsn2d8rji3d98brfavw4f2qq3zh3ksgraaqvh65g36")))

(define-public crate-file_cache-0.0.4 (c (n "file_cache") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "err") (r "^0.0.5") (d #t) (k 0) (p "rmw_err")) (d (n "file_open_limit") (r "^0.0.4") (d #t) (k 0)) (d (n "stretto") (r "^0.5.1") (f (quote ("async"))) (d #t) (k 0)))) (h "18amq0lji36vqz8ljvb1588znzizlla7xd342rd076agg9h82l47")))

(define-public crate-file_cache-0.0.5 (c (n "file_cache") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "err") (r "^0.0.5") (d #t) (k 0) (p "rmw_err")) (d (n "file_open_limit") (r "^0.0.4") (d #t) (k 0)) (d (n "stretto") (r "^0.5.1") (f (quote ("async"))) (d #t) (k 0)))) (h "00rsxhkj7027nny1vc51v4av3vi0pa6mnwv81f74q9x7hxafd8l6")))

