(define-module (crates-io fi le filesorterx) #:use-module (crates-io))

(define-public crate-FileSorterX-1.0.0 (c (n "FileSorterX") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j4vcaw25l6ij4xxxpg50b9n2km31vs4j8xbjgajssc5dn6brq9x")))

(define-public crate-FileSorterX-1.1.0 (c (n "FileSorterX") (v "1.1.0") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "self_update") (r "^0.36.0") (d #t) (k 0)))) (h "1jn1mfn4w3y4cc4pwhwz0msbfww6894y42qa8g6ykmrkiavwq2rq")))

(define-public crate-FileSorterX-1.2.0 (c (n "FileSorterX") (v "1.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "self_update") (r "^0.36.0") (d #t) (k 0)))) (h "0vq5ynvq0cmva4mxshf5dfddr47wwzz4y12pybi7d49mi0n4pb13")))

(define-public crate-FileSorterX-1.2.1 (c (n "FileSorterX") (v "1.2.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "self_update") (r "^0.36.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1zbsfc2q4446896k0hs08ds7c5wp7lncap5zfimsxl69cn4dq24x")))

