(define-module (crates-io fi le file_scanner) #:use-module (crates-io))

(define-public crate-file_scanner-0.1.0 (c (n "file_scanner") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1hsdgc6ijpqky5is2ga5ywmn530y348pdwlwf33hvmppg6c5nkp1")))

(define-public crate-file_scanner-0.1.1 (c (n "file_scanner") (v "0.1.1") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "0b0n18v2xah5vk4f9769ncmj8k2r88ycq3wiiiif7m8l40dy27jn")))

(define-public crate-file_scanner-0.1.2 (c (n "file_scanner") (v "0.1.2") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "14kiir1r3zncq8yy6xg2nmjsh45576jglh755xvgycgygysnwrhw")))

(define-public crate-file_scanner-0.1.3 (c (n "file_scanner") (v "0.1.3") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "08h02kfvgwrp8zbymv9cjnjhpy816lswnz51gviqw799gi4r4gv0")))

(define-public crate-file_scanner-0.1.4 (c (n "file_scanner") (v "0.1.4") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "07nsa1z1bazjmc4jgf1qbqv1zpa14vj32nd6aj5bbc73cwgwwrh8")))

(define-public crate-file_scanner-0.2.0 (c (n "file_scanner") (v "0.2.0") (d (list (d (n "buf_redux") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "138axl9k4y5m6nnc9dd7rd0dmiz7y9h4j6hj4rhh813agn1054ry")))

