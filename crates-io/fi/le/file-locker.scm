(define-module (crates-io fi le file-locker) #:use-module (crates-io))

(define-public crate-file-locker-1.0.2 (c (n "file-locker") (v "1.0.2") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1v85i36kphnqf671y6vd7vlyzq5p3m84b4p5r0rjqavrdyhha2ln")))

(define-public crate-file-locker-1.0.3 (c (n "file-locker") (v "1.0.3") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1kifr6g7zzb8f49ibnma0hqzmv1m4bn43ji2fmddja8g9yqyi4cf")))

(define-public crate-file-locker-1.0.4 (c (n "file-locker") (v "1.0.4") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1ry2003qvi4bcsdlylhmz55l8596cijkf5451npwwb7n50frp1x6")))

(define-public crate-file-locker-1.0.5 (c (n "file-locker") (v "1.0.5") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0hglb0gwgckg383pj5rdnrfgldhkbqddvnlmlibfv20bdxb1azhl")))

(define-public crate-file-locker-1.1.0 (c (n "file-locker") (v "1.1.0") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "11872pymh91l7krqzrdqkhl6qkk35c42fkdqlqraxh5qgl6fxlkb")))

(define-public crate-file-locker-1.1.1 (c (n "file-locker") (v "1.1.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1ch5jbf7yr2idnxr6is5yvngd1q6gcks8anad0n53q9kh0wka2kh")))

(define-public crate-file-locker-1.1.2 (c (n "file-locker") (v "1.1.2") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)))) (h "0pvha37fwpglcs5l3icmjr286hipx07r0lw038gxbdbs6lvql779")))

