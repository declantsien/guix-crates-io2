(define-module (crates-io fi le file_wrap) #:use-module (crates-io))

(define-public crate-file_wrap-0.1.0 (c (n "file_wrap") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)))) (h "1mkr5vsf06hmj7dp85k46cr95x44vxs86nqqg5g6ir2lr9b2v13c")))

(define-public crate-file_wrap-0.2.0 (c (n "file_wrap") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1j4jd319gjm33r8bck6swipaq9silhbh9qpg4gbr7akc52ihxl6p") (f (quote (("use_failure" "failure" "failure_derive") ("default" "use_failure"))))))

