(define-module (crates-io fi le file-id) #:use-module (crates-io))

(define-public crate-file-id-0.1.0 (c (n "file-id") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1hx8zmiqpydj4b471nd1llj1jb8bmjxbwqmq1jy92bm8dhgfffz1") (r "1.60")))

(define-public crate-file-id-0.2.0 (c (n "file-id") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.89, <1.0.172") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1npmxbzcmd867lqlcci4hw7narqbyzxw5dxfykq2ax7lqzinz7yx") (r "1.60")))

(define-public crate-file-id-0.2.1 (c (n "file-id") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jdg9xq830hghzrqkbnx8nda58a7z6mh8b6vlg5mj87v4l2ji135") (r "1.60")))

