(define-module (crates-io fi le filename) #:use-module (crates-io))

(define-public crate-filename-0.1.0 (c (n "filename") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0c98py6zs436aqhrf9dnzgrfbdb2alnac9lw0rf88x7572gcvmcg")))

(define-public crate-filename-0.1.1 (c (n "filename") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1s7byy8c939qrm6hsjqq0vicfv6rkxb3yrhslg7zkggyxw1xzr33")))

