(define-module (crates-io fi le file-owner) #:use-module (crates-io))

(define-public crate-file-owner-0.1.0 (c (n "file-owner") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "03x1cbgvg9pnksj536idf3b9i919vy19086r7c9g1baz4gfl09gj")))

(define-public crate-file-owner-0.1.1 (c (n "file-owner") (v "0.1.1") (d (list (d (n "nix") (r ">=0.16") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0i6ccn7psr5hzg1hms5cp1dd6q1x3dy46i391hg1jnr1sqscgrz8")))

(define-public crate-file-owner-0.1.2 (c (n "file-owner") (v "0.1.2") (d (list (d (n "nix") (r ">=0.24") (f (quote ("user" "fs"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0366kwvmgin65dljd856kl7nj98asvzj8m3d93fqmbzzzk53xw1n")))

