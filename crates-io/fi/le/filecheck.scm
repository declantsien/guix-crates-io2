(define-module (crates-io fi le filecheck) #:use-module (crates-io))

(define-public crate-filecheck-0.0.1 (c (n "filecheck") (v "0.0.1") (d (list (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1k9ddada7nvcy3qz23wip8hkmpasj21ycrlwzpzgdld1lqc11x88")))

(define-public crate-filecheck-0.1.0 (c (n "filecheck") (v "0.1.0") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0b93rghbfcdfjqwmp5pxb4slbnk31hxysk7v01ngmwqi0kzkpyr2")))

(define-public crate-filecheck-0.2.0 (c (n "filecheck") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0n8gfhvir1wsbzdj0svzq2klg9hmx6bkh4nw3lxwkjy8yp9m70in")))

(define-public crate-filecheck-0.2.1 (c (n "filecheck") (v "0.2.1") (d (list (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0259rn993z6v0f74lhj08p735y6f9gyv4vw7z6c0lxrpjp09s76r")))

(define-public crate-filecheck-0.3.0 (c (n "filecheck") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "1jnbp4h95r89p3nq5l4g808hlfwmpfb51v6nk2jd3mcl04vswfib")))

(define-public crate-filecheck-0.4.0 (c (n "filecheck") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j9lycjd50ldks0jcklzrjmbbcwf2s1m2binhpv6whmbjiarimyy")))

(define-public crate-filecheck-0.5.0 (c (n "filecheck") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1amvf0jyhrrk16lvjwrxnrv46qv3k9xynw11p3w3aj3wgd10pq1g")))

