(define-module (crates-io fi le file-matcher) #:use-module (crates-io))

(define-public crate-file-matcher-0.2.4 (c (n "file-matcher") (v "0.2.4") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "04i3z4lxz9dvpqrz1q42dyvaaswiid7w4mcpz2qslqxrc1knscs3") (f (quote (("default" "all") ("all" "regex" "wildmatch"))))))

(define-public crate-file-matcher-0.3.0 (c (n "file-matcher") (v "0.3.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1rmrdrb6bzkv8mk8ln95541fbim1ryky22vfdj9k6in4gnwd13vy") (f (quote (("default" "all") ("all" "regex" "wildmatch"))))))

(define-public crate-file-matcher-0.4.0 (c (n "file-matcher") (v "0.4.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1qrmwvr8gj4z2zvd2ss1nwbwxhyc3i65m9gfxw1jx5hz6pda9dh9") (f (quote (("mover") ("default" "all") ("copier") ("all" "regex" "wildmatch" "copier" "mover"))))))

(define-public crate-file-matcher-0.5.0 (c (n "file-matcher") (v "0.5.0") (d (list (d (n "fs_extra") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1d0xbq984z2ny2sc5wa1xkrjbcbghqncg96ls72iaxqj6q1cdjn6") (f (quote (("mover" "fs_extra") ("default" "all") ("copier" "fs_extra") ("all" "regex" "wildmatch" "copier" "mover"))))))

(define-public crate-file-matcher-0.6.0 (c (n "file-matcher") (v "0.6.0") (d (list (d (n "fs_extra") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1ldflyxbjjc3v9xkw6ddyblzzmm4903kiipig1lv2fijbzysvg59") (f (quote (("mover" "fs_extra") ("default" "all") ("copier" "fs_extra") ("all" "regex" "wildmatch" "copier" "mover"))))))

(define-public crate-file-matcher-0.7.0 (c (n "file-matcher") (v "0.7.0") (d (list (d (n "fs_extra") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1sw5q2bxgnygxnr7dya87y5jybfcfw25fzqywq6pv3k9x6f44jnm") (f (quote (("mover" "fs_extra") ("default" "all") ("copier" "fs_extra") ("all" "regex" "wildmatch" "copier" "mover" "serde"))))))

