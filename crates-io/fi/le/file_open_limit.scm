(define-module (crates-io fi le file_open_limit) #:use-module (crates-io))

(define-public crate-file_open_limit-0.0.1 (c (n "file_open_limit") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "rlimit") (r "^0.8.3") (d #t) (k 0)))) (h "1aikl1ls28dy7p3cpcq5yr9r8iwzfqclh6nv8p9an3s25b18b97n")))

(define-public crate-file_open_limit-0.0.3 (c (n "file_open_limit") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "rlimit") (r "^0.8.3") (d #t) (k 0)))) (h "1crivljhcbvmy7h64pp03sr54zxcc3vm5pslfmn917n17k9hj8fw")))

(define-public crate-file_open_limit-0.0.4 (c (n "file_open_limit") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "rlimit") (r "^0.8.3") (d #t) (k 0)))) (h "18mgdb03kcqi9zscp15xmxmhdq0gnmj3b56vnbdp9rwzkcmncmaf")))

(define-public crate-file_open_limit-0.0.5 (c (n "file_open_limit") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "rlimit") (r "^0.8.3") (d #t) (k 0)))) (h "1fyd13ymhh6zd5zdb584nxaj6cqn83kbypxax3w8rrhkg35c58d2")))

