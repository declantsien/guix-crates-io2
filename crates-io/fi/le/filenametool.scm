(define-module (crates-io fi le filenametool) #:use-module (crates-io))

(define-public crate-filenametool-0.2.0 (c (n "filenametool") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p4zf4h45akrbkdbcvz2zvc8b0znyfmkclc99kr0cfv6kw6mghc1")))

(define-public crate-filenametool-0.2.1 (c (n "filenametool") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a6yq91gyy8fkwpcj825vkjm8sfhhfdrdwi1hrfc6fdsji8468db")))

