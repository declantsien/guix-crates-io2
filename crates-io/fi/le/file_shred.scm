(define-module (crates-io fi le file_shred) #:use-module (crates-io))

(define-public crate-file_shred-1.0.0 (c (n "file_shred") (v "1.0.0") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "number2name") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "11mhhrx62sqrmyiamk5i00hc3hmpb92f2m4xndr0z8x76cnddymm")))

(define-public crate-file_shred-1.1.0 (c (n "file_shred") (v "1.1.0") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "number2name") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1qjl3qv9fyz26wymvqgys960ww57dq1p3gaw9ql782dc3cypxnhm")))

(define-public crate-file_shred-1.1.2 (c (n "file_shred") (v "1.1.2") (d (list (d (n "filetime") (r "^0.2.20") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "number2name") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1k586yagrmxzshij7vy9rja89za8r6air9jd0d377g4m9jk1s274")))

(define-public crate-file_shred-1.1.3 (c (n "file_shred") (v "1.1.3") (d (list (d (n "filetime") (r "^0.2.21") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "number2name") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1di504fg4cspm3abid4ig5qg564r2bbr5pjp7ly04wgvhc4zngdr")))

