(define-module (crates-io fi le filers) #:use-module (crates-io))

(define-public crate-filers-0.1.0 (c (n "filers") (v "0.1.0") (d (list (d (n "clipars") (r "^1.0.1") (d #t) (k 0)))) (h "0h86kkfsxb3lpxh5xazfg57n5fbhz8198g982xzbrcjvpfja4c3a") (y #t)))

(define-public crate-filers-0.1.1 (c (n "filers") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clipars") (r "^1.2.0") (d #t) (k 0)))) (h "0447fhzl4qraismsm2133y92vkbxyncpzh7dqzq5w89s9bfx6mid") (y #t)))

(define-public crate-filers-0.1.2 (c (n "filers") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clipars") (r "^1.2.1") (d #t) (k 0)))) (h "1vxmnijx6plc2zj1n2fxd8jkja0vp15jgr9g0gsxb5ikpgg6xmss") (y #t)))

(define-public crate-filers-0.1.3 (c (n "filers") (v "0.1.3") (d (list (d (n "clipars") (r "^1.2.2") (d #t) (k 0)) (d (n "kern") (r "^0.2.1") (d #t) (k 0)))) (h "0vfp9y5nfs9qhcx09w9lli3x52qy2z0nsd8j00m3v8pi8kx9qlw3") (y #t)))

(define-public crate-filers-0.1.4 (c (n "filers") (v "0.1.4") (d (list (d (n "kern") (r "^0.3.0") (d #t) (k 0)))) (h "04ahdim12vjiygjc70i6blkp92kyzw8aaya2ias840q80ms82cll") (y #t)))

(define-public crate-filers-0.1.5 (c (n "filers") (v "0.1.5") (h "08yjcx3h3qcc513s4dvnhdfdlwklydsywj5z3fhp7qg73y4s5y1p") (y #t)))

(define-public crate-filers-0.1.6 (c (n "filers") (v "0.1.6") (h "1g39214likpldbdnkvmz8hzl6yrbxf619kp9hjivnmddm1vcgiav") (y #t)))

(define-public crate-filers-0.1.7 (c (n "filers") (v "0.1.7") (d (list (d (n "kern") (r "^1") (d #t) (k 0)))) (h "1zs9wivga4yzd72g27bmnp0709axjannqdyj6l9kyhbpk1z4dmi5") (y #t)))

(define-public crate-filers-0.1.8 (c (n "filers") (v "0.1.8") (d (list (d (n "kern") (r "= 1.0.3") (d #t) (k 0)))) (h "178z2c8dh9wqs7vxwmfk8xxxpn7gfd4pkgi4gy10xgrvqyzbk5ry") (y #t)))

(define-public crate-filers-0.1.9 (c (n "filers") (v "0.1.9") (d (list (d (n "kern") (r "= 1.0.3") (d #t) (k 0)))) (h "0ddlbrq9xk1af163kfvj1bnax2yv3ffmwj6hda8w2hi2qj31wkjq") (y #t)))

