(define-module (crates-io fi le fileql) #:use-module (crates-io))

(define-public crate-fileql-0.1.0 (c (n "fileql") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.12.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.14.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.14.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03jgy59cbmii0q0mjnwd1x92wn94grx3ncmdvbdrskwz1papp314")))

(define-public crate-fileql-0.2.0 (c (n "fileql") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.13.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.15.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.15.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08dsr5s0zzp4ard1n1jlrl89q8rvhvggq9r18zaqkjhls6di91z7")))

(define-public crate-fileql-0.3.0 (c (n "fileql") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gitql-ast") (r "^0.16.0") (d #t) (k 0)) (d (n "gitql-cli") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-engine") (r "^0.18.0") (d #t) (k 0)) (d (n "gitql-parser") (r "^0.17.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0260xq0l8ngcxa5p2dg4pvrqaayg0c0zmqcqzf0mns8xbb3ma5aw")))

