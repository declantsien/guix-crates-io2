(define-module (crates-io fi le file_access) #:use-module (crates-io))

(define-public crate-file_access-0.1.7 (c (n "file_access") (v "0.1.7") (h "1gniwn5pad91j1x97v9jqy4m3rld1slk4qg47hynlxjaz6br9857")))

(define-public crate-file_access-0.1.8 (c (n "file_access") (v "0.1.8") (h "0kqx8rkp2hsmas9nvrr2ljdb6kxsnhqxw9k2x57nmy9zl16cl726")))

(define-public crate-file_access-0.1.9 (c (n "file_access") (v "0.1.9") (h "0pblvhsc98dd6qhl1cfwvwc6b4p95bqm4s47jzci3b1vmbzi7i09")))

