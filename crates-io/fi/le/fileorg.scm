(define-module (crates-io fi le fileorg) #:use-module (crates-io))

(define-public crate-fileorg-0.1.0 (c (n "fileorg") (v "0.1.0") (h "0dlsn9d8yzcb14chyhvlgnz3rc0fhklrs214ccv84r3b2fqbrv0z")))

(define-public crate-fileorg-0.1.1 (c (n "fileorg") (v "0.1.1") (h "0hbq3mykb1nwm1wldjarwka1d1sl363xqbl21n7pa26hwr3yzggd")))

