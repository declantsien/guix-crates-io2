(define-module (crates-io fi le fileport) #:use-module (crates-io))

(define-public crate-fileport-0.1.0 (c (n "fileport") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "0a48vgvxslam6nn2gd2pv2gysjbymgl6fx2mbigfb6fpfw716x5k")))

(define-public crate-fileport-0.1.1 (c (n "fileport") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "178ym3jjymq1syd0drlcn8lf4nw801qpifd5dwxdfsbhzbjs1jsk")))

