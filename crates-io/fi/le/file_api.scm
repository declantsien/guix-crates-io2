(define-module (crates-io fi le file_api) #:use-module (crates-io))

(define-public crate-file_api-0.1.0 (c (n "file_api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1ry8rnbhd2pz0q33mp5zi0zd6g87wvsypdn2x142sd7kwiszcffn")))

(define-public crate-file_api-0.1.1 (c (n "file_api") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "15xh41anvidig1l5il40k37c8j845390ivsbdihaymg3xipd4mrc")))

(define-public crate-file_api-0.1.2 (c (n "file_api") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1k951lrd981apjn1ri1bn5vwnw2afky21qpzhph82p7z0r52rafp")))

(define-public crate-file_api-0.1.3 (c (n "file_api") (v "0.1.3") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1c02776cjg99jg733i25b5f5vd6wpi8yqqxdg25pcc7f6z2xyf1b")))

(define-public crate-file_api-0.1.4 (c (n "file_api") (v "0.1.4") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0bl60f6qy1dlcsx3dxh3vp0p9g35aa5j7h4n48mxnl0vgdldlwza")))

(define-public crate-file_api-0.1.5 (c (n "file_api") (v "0.1.5") (d (list (d (n "futures") (r "^0.1.10") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0gy0b9v6dn17d78kgak4zpmv5vmwwq4q8j28bbfd5fg7lb24c2aw")))

(define-public crate-file_api-0.2.0 (c (n "file_api") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "18038f717kvz6r5qcp7rfi103alk0m3mwbddkv7m5lzkxvqaaq81")))

(define-public crate-file_api-0.2.1 (c (n "file_api") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "16zmpr9d1x4x4jwjzxmffrnarzqspivw21d2x6qvc0f5y5sy78x2")))

(define-public crate-file_api-0.2.2 (c (n "file_api") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "12pyc5lcdng6f22synz1jvdhqrhvagrlk5qzxs8w2r1slf4dyrkm")))

(define-public crate-file_api-0.2.3 (c (n "file_api") (v "0.2.3") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1llr0fm4sd9pzzbbby53rr26drnll4495w86sp3admn90p6jpqvw")))

(define-public crate-file_api-0.2.4 (c (n "file_api") (v "0.2.4") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1fqdxc0dxraim404bsrgy98rv6p298i3bqcdsx3z0fdxm5v5zdra")))

(define-public crate-file_api-0.2.5 (c (n "file_api") (v "0.2.5") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0lbn0gnr9yw2pg58zf2amwj1sn5paczn0qznjic4mp285v2zhk7v")))

(define-public crate-file_api-0.2.6 (c (n "file_api") (v "0.2.6") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1ayad5iz5r2869ql9128cqf3yhczyl0aqv0gfb76v8mmbzid8aqd")))

(define-public crate-file_api-0.2.7 (c (n "file_api") (v "0.2.7") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "10zva03q1n37089n4qykllpi9hx9a4fcdfya2f6yfk1vx4nvg997")))

(define-public crate-file_api-0.3.0 (c (n "file_api") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1l6sx4x0skrqxm6l4iy6dl6djpvy5lx9dsykzcbvqchkfqipnfcb")))

(define-public crate-file_api-0.3.1 (c (n "file_api") (v "0.3.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "15fynng33sd9dwmg3vqks1059f5lg8fzynd9fpm7mqg6x54r0icb")))

(define-public crate-file_api-0.3.2 (c (n "file_api") (v "0.3.2") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "07wxy0ya3x2w3ws1k02klxwrq6z20773b4l90f9w2chpd49s2ard")))

(define-public crate-file_api-0.4.0 (c (n "file_api") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "12gk0rp84n92v89k4dh8nfrcwg7ahi9franygr7g7yrsyyl8y5sy")))

(define-public crate-file_api-0.5.0 (c (n "file_api") (v "0.5.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "1k1qbw3wzyk6rjvm377ksnl37jz5438ly5vxyli2jia8avbpz9xd")))

(define-public crate-file_api-0.5.1 (c (n "file_api") (v "0.5.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "0ysm1xhijbs2yrr3cvk8crc8wznq8dckygp2yd50x0jnc3ml5lyn")))

(define-public crate-file_api-0.5.2 (c (n "file_api") (v "0.5.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "13imk3lq2j5fhbkfxs1q1d5l3xcmsslskkwmh0kin3sqbwljb5gc")))

(define-public crate-file_api-0.5.3 (c (n "file_api") (v "0.5.3") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 2)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "0piarglys457n2zck5jvyjamqhfqj2dz6kg89dn538vls82mvzrl")))

(define-public crate-file_api-0.6.0 (c (n "file_api") (v "0.6.0") (d (list (d (n "futures") (r "^0.2.0") (d #t) (k 2)) (d (n "hyper") (r "^0.11.25") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "0az0hij3qcxiprn5zijc59dpn9mdq2cswsr1lg05x6mdz5aq6ydj")))

(define-public crate-file_api-0.6.1 (c (n "file_api") (v "0.6.1") (d (list (d (n "futures") (r "^0.2.0") (d #t) (k 2)) (d (n "hyper") (r "^0.11.25") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "033v3sbqfsc23bsshmq6lavd2zfk5cn0imwafmy9f2hk3xiwqxz7")))

(define-public crate-file_api-0.6.2 (c (n "file_api") (v "0.6.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.11.25") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "16js5spcpw0s8q2i5yksyljzw8fambbj3b5q46i87acqb6ppzx76")))

(define-public crate-file_api-0.6.3 (c (n "file_api") (v "0.6.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.11.25") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "1iiqx6qi6ibicq06vhsxzdysq52807686l014hdscc5b9gdwcl74")))

(define-public crate-file_api-0.6.4 (c (n "file_api") (v "0.6.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "< 0.9.18") (d #t) (k 0)))) (h "0d21chfbxz519m1d2bgzdzdgqpkxjsvds9bpliyfza7swbzwkmyx")))

(define-public crate-file_api-0.6.5 (c (n "file_api") (v "0.6.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "< 0.9.18") (d #t) (k 0)))) (h "0si1av0k0rj5dijqi8kwjam047v8i13liprxrzg2gmbjrk167l4w")))

(define-public crate-file_api-0.6.6 (c (n "file_api") (v "0.6.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyperx") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0zmcdrxmmxqpp6hk29kdy9b3ygibqgs6m6ybyjs3m1jy8dm127a1")))

