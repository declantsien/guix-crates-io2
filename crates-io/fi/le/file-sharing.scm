(define-module (crates-io fi le file-sharing) #:use-module (crates-io))

(define-public crate-file-sharing-0.1.0 (c (n "file-sharing") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "14xha6bzk1yb774751fwqqm14625r6k2axiy1qcvjfh475jqii3d")))

(define-public crate-file-sharing-0.1.1 (c (n "file-sharing") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.1") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0cprfxcfy8j8wgrlizva3kzfml524w97nq1awviw5m53cplqknrd")))

