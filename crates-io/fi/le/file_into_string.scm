(define-module (crates-io fi le file_into_string) #:use-module (crates-io))

(define-public crate-file_into_string-1.1.0 (c (n "file_into_string") (v "1.1.0") (h "12j0cp0cvy8bf99l6xha3yzxb2hahxw3l6d0a5yq5iqh62isvsfc")))

(define-public crate-file_into_string-1.1.1 (c (n "file_into_string") (v "1.1.1") (h "0safqbp0bqyqjrd2b51h68qc0dx194x3iv5r1f59sz8i11qpldln")))

