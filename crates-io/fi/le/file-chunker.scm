(define-module (crates-io fi le file-chunker) #:use-module (crates-io))

(define-public crate-file-chunker-0.1.0 (c (n "file-chunker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0wldj15cgqx0bv7y8c1mdvpipn70fkbpq24b8ds7dpgnhyl6s041")))

(define-public crate-file-chunker-0.1.1 (c (n "file-chunker") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0mn6nqkwcrz3z8qv38kmi1lipxzhsr9a4540s84hcn9npcjq90lj")))

