(define-module (crates-io fi le filecase) #:use-module (crates-io))

(define-public crate-filecase-0.1.0 (c (n "filecase") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "gtk4") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.4") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "1gd88glqdr8c33v1qy0z02m0bs7z90zv1b8pi65nfg10ls9343cs")))

(define-public crate-filecase-0.1.1 (c (n "filecase") (v "0.1.1") (d (list (d (n "gtk4") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.4") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "1vi2yx29qmpfywbzrm32n1lv8xypl50ywa4vxwisv49pd6ipdy6z")))

(define-public crate-filecase-0.1.2 (c (n "filecase") (v "0.1.2") (d (list (d (n "gtk4") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.4") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "05vcms0bzsviz9knp282464087kg2vib12ihyzs1bjf8qgfc2nd9")))

(define-public crate-filecase-0.1.3 (c (n "filecase") (v "0.1.3") (d (list (d (n "gtk4") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.4") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "1dc22klvr3ldwrp43jpvan4rs3nvpm4wl6f30ks61f33mq7b7qa8")))

(define-public crate-filecase-0.1.4 (c (n "filecase") (v "0.1.4") (d (list (d (n "gtk4") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.4.2") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "0ghm6jkfzgakjyzxw44nri0bxibxdbpk1jy913ki628gpp4gw0pl")))

(define-public crate-filecase-0.1.5 (c (n "filecase") (v "0.1.5") (d (list (d (n "gtk4") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "sourceview5") (r "^0.5.0") (d #t) (k 0)) (d (n "stateful") (r "^0.1") (d #t) (k 0)))) (h "15impvqzj80byfq1qvcj68l3l8rx0klcl556vqy9c00ysgqxnjxb")))

