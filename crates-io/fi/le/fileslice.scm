(define-module (crates-io fi le fileslice) #:use-module (crates-io))

(define-public crate-fileslice-0.1.0 (c (n "fileslice") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (o #t) (k 0)) (d (n "parquet") (r "^15.0.0") (o #t) (k 0)) (d (n "parquet") (r "^15.0.0") (f (quote ("zstd"))) (k 2)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "1bhampp1038il6ds4wcsf1apj2pgcn533gwqwjapj0zkvg0sacmn") (f (quote (("default" "parquet" "tar")))) (s 2) (e (quote (("parquet" "dep:parquet" "chrono"))))))

(define-public crate-fileslice-0.2.0 (c (n "fileslice") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (o #t) (k 0)) (d (n "parquet") (r "^15.0.0") (o #t) (k 0)) (d (n "parquet") (r "^15.0.0") (f (quote ("zstd"))) (k 2)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "0jlf24dpbc5w8nqha6mds7zyfvwnvm0450101p28ajrlwkzqa6f1") (f (quote (("default" "parquet" "tar")))) (s 2) (e (quote (("parquet" "dep:parquet" "chrono"))))))

(define-public crate-fileslice-0.2.1 (c (n "fileslice") (v "0.2.1") (d (list (d (n "parquet") (r "^27.0.0") (o #t) (k 0)) (d (n "parquet") (r "^27.0.0") (f (quote ("zstd"))) (k 2)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "0xpdrddvphhkwm0aa1i8xpm40z5ivx6xy7l73lxn3gssr854r9zd") (f (quote (("default" "parquet" "tar"))))))

(define-public crate-fileslice-0.3.0 (c (n "fileslice") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "parquet") (r "^46") (o #t) (k 0)) (d (n "parquet") (r "^46") (f (quote ("zstd"))) (k 2)) (d (n "tar") (r "^0.4.38") (o #t) (d #t) (k 0)))) (h "1pb77sgmapfih0fvvkfcz0v6dabwcyvg6g0gwhr9833kifw4d0qr") (f (quote (("default" "parquet" "tar")))) (s 2) (e (quote (("parquet" "dep:parquet" "bytes"))))))

(define-public crate-fileslice-0.4.0 (c (n "fileslice") (v "0.4.0") (d (list (d (n "bytes") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "parquet") (r "^49") (o #t) (k 0)) (d (n "parquet") (r "^49") (f (quote ("zstd"))) (k 2)) (d (n "tar") (r "^0.4.40") (o #t) (d #t) (k 0)))) (h "1gyjj6700g72qcy0q3gkxzraxi4h635iv4qwgwhscdijchfxmayw") (f (quote (("default" "parquet" "tar")))) (s 2) (e (quote (("parquet" "dep:parquet" "bytes"))))))

