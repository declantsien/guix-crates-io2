(define-module (crates-io fi le file_hashmap) #:use-module (crates-io))

(define-public crate-file_hashmap-0.1.0 (c (n "file_hashmap") (v "0.1.0") (d (list (d (n "cast") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jm462hc7pfw5ziyc1381004ygb362gy4ll2c91q73is4lns0p0s")))

