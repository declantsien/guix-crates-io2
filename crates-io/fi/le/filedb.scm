(define-module (crates-io fi le filedb) #:use-module (crates-io))

(define-public crate-filedb-0.1.0 (c (n "filedb") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0234iri0h9jnlfarg45s28phy26z1qs99vpn0gfpbrjjradaygcz")))

(define-public crate-filedb-0.1.1 (c (n "filedb") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "18vzh72irpza5sprapasi0b3jfg4paj43v1bwn7kzjp940gy5d9x")))

(define-public crate-filedb-0.1.2 (c (n "filedb") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "02cxs2gm4b5ki511463vmrh1fv1ws90nlln6qq2irb49lfdgf43l")))

