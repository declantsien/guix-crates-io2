(define-module (crates-io fi le filecmp) #:use-module (crates-io))

(define-public crate-filecmp-0.0.1 (c (n "filecmp") (v "0.0.1") (h "1hi3fh01dviwwsp371kx1v2cxgwvbpgw7myqmryds5z0k6j9j72y")))

(define-public crate-filecmp-0.0.2 (c (n "filecmp") (v "0.0.2") (h "0z3psagihiw9drmcw3ksqkmy92xhkymqz4g2ardbc7b460rhi38f")))

(define-public crate-filecmp-0.1.0 (c (n "filecmp") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f28vda0ksi00p4gyjvdsyspi25hmm4a0y03n5wiq53l7rnncqq5")))

(define-public crate-filecmp-0.2.0 (c (n "filecmp") (v "0.2.0") (d (list (d (n "clap") (r ">=2.0.0, <3.0.0") (d #t) (k 2)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)))) (h "1rjl5bpfxy3i5338lx6gz4jdc7ahzmzng0yhmlmwhwm78w553xps")))

