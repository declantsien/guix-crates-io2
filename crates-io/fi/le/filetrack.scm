(define-module (crates-io fi le filetrack) #:use-module (crates-io))

(define-public crate-filetrack-0.1.0 (c (n "filetrack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1f2bcx9wcwc5xy74hjbx7fhz602r4rk0znb6vkq9zk6i5wg4s08h")))

(define-public crate-filetrack-0.1.1 (c (n "filetrack") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "07nymimiwxci76zjks2x7jdf5xv5pyx2n9ylk0gb1ry4g97mhb86")))

(define-public crate-filetrack-0.1.2 (c (n "filetrack") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1x515dx16fm87wmgbhw3vqpdrdzw6iaw0r4vv1lvdja4nd33fmii")))

(define-public crate-filetrack-0.2.0 (c (n "filetrack") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1m1jvpyfyfp372d4h4ldhr4l675b3zzg4nnl71lwkcfh0x8hm58m")))

(define-public crate-filetrack-0.2.1 (c (n "filetrack") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0z2p8rlvjx09bdc3mcxl0wm5jpzjnkhv9wglgj4wf3hhpp86wpm5")))

(define-public crate-filetrack-0.2.2 (c (n "filetrack") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0dd3s2l2hz1wpkfhhsxpc4ld7dxvgyjchs9xw9bjmfamy45hmzsw")))

(define-public crate-filetrack-0.2.3 (c (n "filetrack") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0491n8i1yqkrlmgp9z1yj4b51qhvb3z5j75acn76p5zsxsyk7y8s")))

