(define-module (crates-io fi le file-size) #:use-module (crates-io))

(define-public crate-file-size-1.0.0 (c (n "file-size") (v "1.0.0") (h "0r2qvi0ncn2adsqq5mkcphnl8432wcs4a19inmng4sl3w1gc0x0c")))

(define-public crate-file-size-1.0.1 (c (n "file-size") (v "1.0.1") (h "1aizsp2r92wxlj1ma7igbsdskaydnlwz3yldk0wky8w4hih8d365")))

(define-public crate-file-size-1.0.2 (c (n "file-size") (v "1.0.2") (h "183jm95p5qychnc765pzhlglpnl3jr5gy5p856k99md7dz7x02nz")))

(define-public crate-file-size-1.0.3 (c (n "file-size") (v "1.0.3") (h "1cyj7067fs7ml8pjrwzjy3qrns3yxaxakf0na1v5fffk0l0z2i4m")))

