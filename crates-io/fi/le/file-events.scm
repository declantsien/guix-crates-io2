(define-module (crates-io fi le file-events) #:use-module (crates-io))

(define-public crate-file-events-0.1.0 (c (n "file-events") (v "0.1.0") (d (list (d (n "fsevent") (r "^2.1.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "inotify") (r "^0.9") (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "serde"))) (d #t) (k 2)))) (h "1jzl1im3a4dhzmwppi803hb5wnsaz75pwd26x7rblb8mlyfhkm45")))

(define-public crate-file-events-0.1.1 (c (n "file-events") (v "0.1.1") (d (list (d (n "fsevent") (r "^2.1.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "inotify") (r "^0.9") (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "serde"))) (d #t) (k 2)))) (h "13vlx878zl07zmz3b0c19g107rvvxdmg3ksdvs74qi405q79mskf")))

