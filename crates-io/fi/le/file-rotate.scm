(define-module (crates-io fi le file-rotate) #:use-module (crates-io))

(define-public crate-file-rotate-0.1.0 (c (n "file-rotate") (v "0.1.0") (h "02v5rv2lzb49n3plqv6nad3sbqhqq4s5qimk9qn19g5k7jmq966y")))

(define-public crate-file-rotate-0.1.1 (c (n "file-rotate") (v "0.1.1") (h "0f85qblchwbz9li3y02hlji1ks080ni5c8m0lxxayy57rz3c1yfy")))

(define-public crate-file-rotate-0.2.0 (c (n "file-rotate") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1gwmisrj07sgk72shxwnykwsckskajypvgd6z6kkh4px24ps8adn")))

(define-public crate-file-rotate-0.4.0 (c (n "file-rotate") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)))) (h "1grgh1qm01anhkh7m3zy3pc4cr5lhg3x1y3h5jj9pdnvb8zdr4mq")))

(define-public crate-file-rotate-0.5.0 (c (n "file-rotate") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0xyjlp63fzwsz64sh24ysq48637y8phw073sgyrdzwr5s52cmd7n") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.5.1 (c (n "file-rotate") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0b9kh8lb0jzh81kpq3iqkcxx2z0ww99yaqdhgw7id18pzqydrxz0") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.5.2 (c (n "file-rotate") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0gy4mkd7p1rv814l8s457a4nfha6l425885y84nb4gm7r3ky4lwb") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.5.3 (c (n "file-rotate") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "04d4aphhwsfr1lry8zz1kw1aivy7xniml1zgvch9j0bfw5p57hii") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.6.0 (c (n "file-rotate") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1l3jixpf0jj4q4k3d6w8c4kj3jiw74zh91z6dh0d872v67vis1zq") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.7.0-rc.0 (c (n "file-rotate") (v "0.7.0-rc.0") (d (list (d (n "chrono") (r "^0.4.20-rc.1") (o #t) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1kxxxi18mf5rypwl3b82lvj1zlwrcwr4kkk1mifdap5i85hvalw4") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.7.0 (c (n "file-rotate") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1rzdj84nb2bhlqxjgaxqka0cw9a7y99bjjb8wkri8h68y2kkb4ca") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.7.1 (c (n "file-rotate") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1jrsd8ix5cdc03n9qdsbcfiv0wy2nazlcgwjhps8irfg689scjgm") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.7.2 (c (n "file-rotate") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (o #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1q8iyzy28svajf27i0wbnr0ap339qkw2hnb7c5z1v002bicwry8g") (f (quote (("default" "chrono04") ("chrono04" "chrono"))))))

(define-public crate-file-rotate-0.7.3 (c (n "file-rotate") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0kw3jrq304p96rh86jgj5dycb7lcrf5ncwl600vfl3agqgvk9cag")))

(define-public crate-file-rotate-0.7.4 (c (n "file-rotate") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "074bc09xxkpb2sjfgh4l0nzcfx8z87i9hasyxn28grv1941c9fql")))

(define-public crate-file-rotate-0.7.5 (c (n "file-rotate") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0wcc8q5jb5xv5wr52bl3r1pnd1xxn90kbbjdfv5z65s5xk723wnx")))

(define-public crate-file-rotate-0.7.6 (c (n "file-rotate") (v "0.7.6") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1817qvmyla12v0f0p1ky8q7vhk8i7nb67m7pcc9mn7w088hxhgks")))

