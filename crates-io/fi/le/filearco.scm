(define-module (crates-io fi le filearco) #:use-module (crates-io))

(define-public crate-filearco-0.1.0 (c (n "filearco") (v "0.1.0") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "crc") (r "^1.5") (d #t) (k 0)) (d (n "memadvise") (r "^0.1") (d #t) (k 2)) (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "page_size") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "00rfhnr4d8qf7cx1jhpxkqwqy8f66macxy0scwq3lififf6ci14g") (f (quote (("binaries" "clap"))))))

