(define-module (crates-io fi le filecrate) #:use-module (crates-io))

(define-public crate-filecrate-0.1.0 (c (n "filecrate") (v "0.1.0") (h "0pfr6yjvxazxi1xnilba8n49qbl9ll93pxsdhv2mpwc7wydbgkln")))

(define-public crate-filecrate-0.1.1 (c (n "filecrate") (v "0.1.1") (h "1zdkiz27vgbw5z7b01p8g31rh9py1ii37zd2mwd316imp0y3gjc3")))

