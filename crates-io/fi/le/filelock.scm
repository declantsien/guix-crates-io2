(define-module (crates-io fi le filelock) #:use-module (crates-io))

(define-public crate-filelock-0.0.1 (c (n "filelock") (v "0.0.1") (h "1lifk6qsis89jm6d9yvb45gfp2481fh06dqrr33zhrqsmyycn6v5")))

(define-public crate-filelock-0.1.0 (c (n "filelock") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n20gjywhzhhxr1plmxg79nckjx3gdf281v9g1xm7145sskpjasx")))

(define-public crate-filelock-0.1.1 (c (n "filelock") (v "0.1.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wgy6wb4dkw32xrxcfkp1sdfcn4vj5b74lq2if62mhm04hzd9xvs")))

(define-public crate-filelock-0.2.0 (c (n "filelock") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0m872b2qcrwm3ad9vrji2v70w7clrqd3pxklf7ig0c3cv0xbms88")))

(define-public crate-filelock-0.3.0 (c (n "filelock") (v "0.3.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p1gsprd0dmzcwyp1vvhz6477pqvr7yzrjvy2prglb8nf7biycjd")))

