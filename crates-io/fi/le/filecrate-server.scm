(define-module (crates-io fi le filecrate-server) #:use-module (crates-io))

(define-public crate-filecrate-server-0.1.0 (c (n "filecrate-server") (v "0.1.0") (h "1l9n8byxrz9wzmvyrcwci4qzdvyffpjdrjcjx9dr1md9hr503a0m")))

(define-public crate-filecrate-server-0.1.1 (c (n "filecrate-server") (v "0.1.1") (h "0zabgqnsxv96npv127pn07sci39v8hl6x23yx0ylzwa7d8pcbf7k")))

(define-public crate-filecrate-server-0.1.2 (c (n "filecrate-server") (v "0.1.2") (h "1nx1s3r9caa6fv9yllcr6g1psnyphvjh4imppyvrxcfmp6nc9s9a")))

