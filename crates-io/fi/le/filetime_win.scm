(define-module (crates-io fi le filetime_win) #:use-module (crates-io))

(define-public crate-filetime_win-0.1.0-alpha1 (c (n "filetime_win") (v "0.1.0-alpha1") (d (list (d (n "comedy") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("minwinbase" "minwindef" "ntdef" "sysinfoapi" "timezoneapi"))) (d #t) (k 0)))) (h "1b9z6w5k78d4ph0z4ggi09qsv9qkg6d3xdjabgd3l0awqm8pjp6k") (f (quote (("filetime_serde" "serde" "serde_derive"))))))

(define-public crate-filetime_win-0.1.0-alpha2 (c (n "filetime_win") (v "0.1.0-alpha2") (d (list (d (n "comedy") (r "^0.1.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("minwinbase" "minwindef" "ntdef" "sysinfoapi" "timezoneapi"))) (d #t) (k 0)))) (h "0zjlsg1rbn5d9432iagqj6fc718vg4n98cl9frin5ywgfvp972pn") (f (quote (("filetime_serde" "serde" "serde_derive"))))))

(define-public crate-filetime_win-0.1.0 (c (n "filetime_win") (v "0.1.0") (d (list (d (n "comedy") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("minwinbase" "minwindef" "ntdef" "sysinfoapi" "timezoneapi"))) (d #t) (k 0)))) (h "1v1qzdxyjaf7sik113cxjivg7zq29zfagr6a9dwwpq2q9aypmhxq") (f (quote (("filetime_serde" "serde" "serde_derive"))))))

(define-public crate-filetime_win-0.2.0 (c (n "filetime_win") (v "0.2.0") (d (list (d (n "comedy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("minwinbase" "minwindef" "ntdef" "sysinfoapi" "timezoneapi"))) (d #t) (k 0)))) (h "0mxcb7r3vb29x833nxs49k7v6ps2ny4sqfy35jaxf14zgn6jqb5i") (f (quote (("filetime_serde" "serde" "serde_derive"))))))

