(define-module (crates-io fi le file_sql) #:use-module (crates-io))

(define-public crate-file_sql-0.0.1 (c (n "file_sql") (v "0.0.1") (h "1p2w2gpmwbkr06hsqasr0sa38yhqkh7q9hvj38v5wfg0gw55nv7m")))

(define-public crate-file_sql-0.1.0 (c (n "file_sql") (v "0.1.0") (h "0fhw1hm9bh35cbmahh793bv4dxrgicac1ivvnb10sqn8iymjjc1b")))

(define-public crate-file_sql-0.1.1 (c (n "file_sql") (v "0.1.1") (h "1yb40vyjhvazmzyj9552mcb0kzm3w12vfqcygj411m0jp4pb9rq3")))

(define-public crate-file_sql-0.1.2 (c (n "file_sql") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0pvccmwzhs6kw2jvz8ghyz46s98gqsqz4rlxn67nb8ci2jdqcipa")))

(define-public crate-file_sql-0.2.0 (c (n "file_sql") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0lc2phjichxq0nj2775rmg2rlwmykl2m21z2j4qik1x6xpgdhky8")))

(define-public crate-file_sql-0.2.1 (c (n "file_sql") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1qaj9vr1phhmyrbzqsddc93ljqvkwjmhwq181554z1bwj0hnip6z")))

