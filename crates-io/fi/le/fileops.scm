(define-module (crates-io fi le fileops) #:use-module (crates-io))

(define-public crate-fileops-0.1.1 (c (n "fileops") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive" "color"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.0.7") (d #t) (k 1)))) (h "1r7ayhk1ywbprxhr97fk101zvyzak0ll7fgg0yr25v2whmqhglg6")))

