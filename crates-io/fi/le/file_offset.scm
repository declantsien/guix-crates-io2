(define-module (crates-io fi le file_offset) #:use-module (crates-io))

(define-public crate-file_offset-0.1.0 (c (n "file_offset") (v "0.1.0") (h "0wl9fklskw34sh5qk8lgsp7783dxkx6dj94jg47r068g5iinx2d3")))

(define-public crate-file_offset-0.1.1 (c (n "file_offset") (v "0.1.1") (h "1k80k5j3sxshw823iwcp3hq8vr6qhhqn2f3i127xf07ic3bjxl2a")))

