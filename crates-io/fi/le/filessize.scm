(define-module (crates-io fi le filessize) #:use-module (crates-io))

(define-public crate-filessize-0.1.0 (c (n "filessize") (v "0.1.0") (d (list (d (n "comfy-table") (r "^6.0.0-rc.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "kib") (r "^4.0.0") (d #t) (k 0)))) (h "0wmdhxavxkvxxkvq4bh3bs4d0qlbxcw81bqrvws1jx1b0w4l5ygr")))

