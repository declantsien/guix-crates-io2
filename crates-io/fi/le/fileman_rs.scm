(define-module (crates-io fi le fileman_rs) #:use-module (crates-io))

(define-public crate-fileman_rs-0.1.0 (c (n "fileman_rs") (v "0.1.0") (h "03p8s3snj52ipxn48gr87xz09fcspg845fsqhrj4wbnim8qsxz7s")))

(define-public crate-fileman_rs-0.1.1 (c (n "fileman_rs") (v "0.1.1") (h "1lh05lsm0lpvd6p48qgwlk25l4p6dcr4rhkw5cnw9r6g1vgva13s")))

(define-public crate-fileman_rs-0.1.2 (c (n "fileman_rs") (v "0.1.2") (h "07wyz45drrwjcl5ik5s52q02nwgsj1gd4lp06h1bvdzs7ddpdsj1")))

(define-public crate-fileman_rs-0.1.3 (c (n "fileman_rs") (v "0.1.3") (h "14dic919xyrrsbmgc3pmfzvqfbrm5rkx0y83zvr4jjrpff70n095")))

(define-public crate-fileman_rs-0.1.4 (c (n "fileman_rs") (v "0.1.4") (h "0d27smyqv2dxp04yrjnrpc9rd8jykgs4m185hs3z0ayfwdv6kjd6")))

(define-public crate-fileman_rs-0.1.5 (c (n "fileman_rs") (v "0.1.5") (h "0bf24vkf96650xpmkla0vi0z1wlhz9nmvv2zms1h4dpj1ig937fk")))

(define-public crate-fileman_rs-0.1.6 (c (n "fileman_rs") (v "0.1.6") (h "0jdyczndn6pypw8i0bx96p4x6fl4krfysgz4qwdwi50hp1l336i5")))

(define-public crate-fileman_rs-0.2.0 (c (n "fileman_rs") (v "0.2.0") (d (list (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)))) (h "0bh4kipdvj5isdzsyvdvqi9s2qmdvqjahisc7wis9mgis10wsg97")))

(define-public crate-fileman_rs-0.2.1 (c (n "fileman_rs") (v "0.2.1") (d (list (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)))) (h "0wa320ynagj4a6kiq5shyhsfcfmvl6fsf3mqsscz6z92v5yh797z")))

(define-public crate-fileman_rs-0.2.2 (c (n "fileman_rs") (v "0.2.2") (d (list (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting" "parsing"))) (d #t) (k 0)))) (h "0ns34lqil9ak65imq05p9c9jq8la9zrl857wrg9i50nsh9b1z684")))

