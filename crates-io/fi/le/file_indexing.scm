(define-module (crates-io fi le file_indexing) #:use-module (crates-io))

(define-public crate-file_indexing-0.2.0 (c (n "file_indexing") (v "0.2.0") (h "10ml3n481ak1wfplg511xrw6260qxndlcmpv58pwf5v1vchg3dci")))

(define-public crate-file_indexing-0.2.1 (c (n "file_indexing") (v "0.2.1") (h "1na85pmy2az4x2l84dsvsya05g0j8p3s630wr7bn5z7g9fxyim3s")))

(define-public crate-file_indexing-0.3.0 (c (n "file_indexing") (v "0.3.0") (h "1alccpvcyjj9f0as98mh5i4mbwiq2w2hwpv200wniz2smgclpya4")))

(define-public crate-file_indexing-0.3.1 (c (n "file_indexing") (v "0.3.1") (h "0f8y68nccm1dzgfi327fs3ifk1b7p3pkg1wpnanrx2y7pvxhz5fz")))

(define-public crate-file_indexing-0.4.0 (c (n "file_indexing") (v "0.4.0") (h "1gwn3x87n1qfqwl9s44bihdvvmwn831jigimb0w15f60hz9l2l25")))

