(define-module (crates-io fi le file_system) #:use-module (crates-io))

(define-public crate-file_system-0.1.0 (c (n "file_system") (v "0.1.0") (h "0b02rc0kkycmqivg3zgd013fcnrjpc5pp2rqgcbs49wvwfg2nwhn") (y #t)))

(define-public crate-file_system-0.2.0 (c (n "file_system") (v "0.2.0") (h "1ffnacj6cbm9467xz45d4c1n68wijrbhz3a3s0pcqw4mih03a4v4") (y #t)))

(define-public crate-file_system-0.2.1 (c (n "file_system") (v "0.2.1") (h "0w5xks53ywrdkqgin98zsv5ikpa5cn1sijlliyz9x12b0s359x44") (y #t)))

(define-public crate-file_system-0.2.2 (c (n "file_system") (v "0.2.2") (h "1pvpyr02vr3vnwabmn2wxdhqm29iy19qk3x98qxqjkhz63xmi6b7") (y #t)))

(define-public crate-file_system-0.2.3 (c (n "file_system") (v "0.2.3") (h "08hxdda4z9i129fbp4r8baj32n2kzbv2vbx8rkb1hzv1n5l0b310") (y #t)))

(define-public crate-file_system-0.2.4 (c (n "file_system") (v "0.2.4") (h "19sxh1mwjscd69p6szv73143f0ysz3rsnjnfpzafy69bhq9ga49x") (y #t)))

(define-public crate-file_system-0.3.1 (c (n "file_system") (v "0.3.1") (h "1aif962z4acxjv94xl1acc2vncf7yhpv90vj3y17q801vzgvzgzr") (y #t)))

(define-public crate-file_system-0.3.2 (c (n "file_system") (v "0.3.2") (h "16r66mkb87xafdjxqbd94pldv3wlwsvsh07vn9zbhylphkl134qz")))

(define-public crate-file_system-0.3.3 (c (n "file_system") (v "0.3.3") (h "1wb2kwf8pdnxygj03kdwk863bd0mq3dzq0pm94rfxh5nj9ia1cnh")))

