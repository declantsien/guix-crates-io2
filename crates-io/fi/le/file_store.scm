(define-module (crates-io fi le file_store) #:use-module (crates-io))

(define-public crate-file_store-0.0.1 (c (n "file_store") (v "0.0.1") (d (list (d (n "file_node") (r "^0.0.1") (d #t) (k 0)) (d (n "fuse") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0nbgzad4x5znw4gg1k12gixcl8lwanb7mm5nzz93qjiil1l32p0j")))

