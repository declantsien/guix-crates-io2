(define-module (crates-io fi le filehash-rs) #:use-module (crates-io))

(define-public crate-filehash-rs-1.0.0 (c (n "filehash-rs") (v "1.0.0") (h "0vmq26vm3z2ggq2zlz4b0bvvqsn5z6iy3icq9zz1jwvlp657kqnm")))

(define-public crate-filehash-rs-1.1.0 (c (n "filehash-rs") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1f53p2i54mm23vn11id6mpyfqms7b835y8gs4hgkbrigw322q8jq")))

