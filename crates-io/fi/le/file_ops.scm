(define-module (crates-io fi le file_ops) #:use-module (crates-io))

(define-public crate-file_ops-0.1.0 (c (n "file_ops") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive" "color"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xmfb3s87m1613xdcs8840rik633n5v2myxk5w2b7kmhi0ijypvr")))

(define-public crate-file_ops-0.1.1 (c (n "file_ops") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive" "color"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.0.7") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0qrl6x9liy9299nb164hsx574vimya8zvihddwlfi48sqf2547gj")))

