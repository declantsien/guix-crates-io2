(define-module (crates-io fi le filenamefix) #:use-module (crates-io))

(define-public crate-filenamefix-0.1.0 (c (n "filenamefix") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1r4h5v8304vgl8wrd58cyz8f895xgglzvyjkkngxk0ylrna911pc")))

(define-public crate-filenamefix-0.1.1 (c (n "filenamefix") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0pm861bzhc4ap0fk3hxzh545cmdqnzwv6l948h9986avgxs8mih3")))

(define-public crate-filenamefix-0.1.2 (c (n "filenamefix") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0dl4lslai14v1kv85p5n03i7ajkk1rx6yw5hsq6xp53n7y7fazxr")))

(define-public crate-filenamefix-0.1.3 (c (n "filenamefix") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1m0z71q1bqd5hg8kxyzyadd75584wn4245s0x12sz628q514cp0p")))

(define-public crate-filenamefix-0.1.4 (c (n "filenamefix") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17ncykd9mzfyi0dxd4bn2x7p7yai51iz2p26s2zmly89ima0j7kh")))

