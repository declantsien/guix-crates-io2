(define-module (crates-io fi le filemagic) #:use-module (crates-io))

(define-public crate-filemagic-0.12.3 (c (n "filemagic") (v "0.12.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 2)))) (h "1v8pfwd78kv2knhkm12c4s389q6crhklnzdfh7ig0p6ir1m5gaz0")))

