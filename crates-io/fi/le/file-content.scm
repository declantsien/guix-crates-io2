(define-module (crates-io fi le file-content) #:use-module (crates-io))

(define-public crate-file-content-0.1.0 (c (n "file-content") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1rmxxsa4fwq3s5ly949j6p9psx1akc35nrvfyrg4a2fm1gkn4cl7")))

(define-public crate-file-content-0.2.0 (c (n "file-content") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1vjl97vf7yring5jqf9wfxj2xr5gmpgjmv86a4bp9hlhcj8fi704")))

(define-public crate-file-content-0.3.0 (c (n "file-content") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "179jb0qdxbvnm194miiqkccs0hp61paxjyzrlnlqg6383qdyndna")))

(define-public crate-file-content-0.3.1 (c (n "file-content") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "13v2lfn1k6z22q0lr3pgdc67cgvwayr2dm41a5nzlmwam3m21l58")))

