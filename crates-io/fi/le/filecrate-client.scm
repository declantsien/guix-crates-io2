(define-module (crates-io fi le filecrate-client) #:use-module (crates-io))

(define-public crate-filecrate-client-0.1.0 (c (n "filecrate-client") (v "0.1.0") (h "1jnzw9igrs0kp4ivyzbzgmzmx5iwww3xji6x65cdf6fcmbm0i96i")))

(define-public crate-filecrate-client-0.1.1 (c (n "filecrate-client") (v "0.1.1") (h "0xx92nwqjxn6m3cmbvsl1yi6443gd41df2ncq78vzxg64mgvq16z")))

(define-public crate-filecrate-client-0.1.2 (c (n "filecrate-client") (v "0.1.2") (h "14zmz0insmljdqbdhk1nd4a1ih32gsjh0riv3838snh0vviigwd6")))

