(define-module (crates-io fi le filesfinder) #:use-module (crates-io))

(define-public crate-filesfinder-0.1.0 (c (n "filesfinder") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0iz07rf4rsa8ngak9qxi424bxy9zd84bx6izjbgk2hvxzyjj53cy") (r "1.58.1")))

(define-public crate-filesfinder-0.2.0 (c (n "filesfinder") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lwgxr97ra294390nci4smc3nb7zdd3wfkd2bdvrjh84rv7g04v0") (r "1.58.1")))

(define-public crate-filesfinder-0.2.1 (c (n "filesfinder") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1apl11zar2dyyrbizsi3mxld2wy5gw2i0dxcdpski1fp98i0f1a9") (r "1.58.1")))

(define-public crate-filesfinder-0.3.0 (c (n "filesfinder") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p1cc0axy4lck19a83idwm5n5wrk76d6x161nlf0m1aswzmcbfcy") (r "1.58.1")))

(define-public crate-filesfinder-0.3.1-rc.1 (c (n "filesfinder") (v "0.3.1-rc.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gdpq4x92vyjiz0c9bhap4b305s443wjs0kdkb8v7bq4dk1k9x4d") (r "1.58.1")))

(define-public crate-filesfinder-0.3.1 (c (n "filesfinder") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h30kr592z0w80vi0gc6kk9iagw9mp7gfvbwgc0f9wfzsvixg5n5") (r "1.58.1")))

(define-public crate-filesfinder-0.3.2 (c (n "filesfinder") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nx62yzs05amx7yqv8g3rh0sw7kxaspqx6xdzbhjcmivqjqqfv3s") (r "1.58.1")))

(define-public crate-filesfinder-0.3.3 (c (n "filesfinder") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pw5rrz75pl2xdk571fabx6vj1x16d2pcrbii5kwzqlh3hlssjj0") (r "1.58.1")))

(define-public crate-filesfinder-0.3.4 (c (n "filesfinder") (v "0.3.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vzgi6l46mc6wqhh2wcgmf49cyrrqmz3gag687rzi2z2f1ymr4zs") (r "1.58.1")))

(define-public crate-filesfinder-0.3.5 (c (n "filesfinder") (v "0.3.5") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p13s8bw7mzas4jdyva9l0m9fgn869nj48a9a8yxc5m4m625lxa4") (r "1.58.1")))

(define-public crate-filesfinder-0.3.6 (c (n "filesfinder") (v "0.3.6") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15wafr64inm7rn0axp18ras6cjlzd5yhzr143x0w2d3awazdyhzj") (r "1.58.1")))

(define-public crate-filesfinder-0.3.7 (c (n "filesfinder") (v "0.3.7") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hjmgrsb9dvk4m7ikm449i9af6gl5h5szlnd8j90mrh114l2gr47") (r "1.58.1")))

(define-public crate-filesfinder-0.3.8 (c (n "filesfinder") (v "0.3.8") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yjr0c0bmncpgr35ya6b8q7x3w0vxy2x0rr9qkbf8aw165mg0250") (r "1.58.1")))

(define-public crate-filesfinder-0.4.0 (c (n "filesfinder") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1aqg7c58wnn86w3lrjsbb6qj8j084jv3j7q75681jkffm4lwmjxf") (r "1.63.0")))

(define-public crate-filesfinder-0.4.1 (c (n "filesfinder") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m16bq6zmb0qzg4xrf92agyl5faq6j3dm4i8kjz49cw3s0jspsp6") (r "1.63.0")))

(define-public crate-filesfinder-0.4.2 (c (n "filesfinder") (v "0.4.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vg3jm7wzahvnnxi4azq4zihkr55kb954krzz1k1h329wxcd50jb") (r "1.63.0")))

(define-public crate-filesfinder-0.4.3 (c (n "filesfinder") (v "0.4.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16biyk1q688r2812nzbf07mk0xw2lf7f0b2dwaic13qafp0zgyqz") (r "1.63.0")))

(define-public crate-filesfinder-0.4.4 (c (n "filesfinder") (v "0.4.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d0h5al1ww98b0b2qqyfvq9xyb99di930dai26gsg760w9l71xyn") (r "1.63.0")))

(define-public crate-filesfinder-0.4.5 (c (n "filesfinder") (v "0.4.5") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11q53q39wlbkblkjvp05iq4n8rdczy7wl322m8skrl2awqy3gd5a") (r "1.63.0")))

(define-public crate-filesfinder-0.4.6 (c (n "filesfinder") (v "0.4.6") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qx5vcpz1sdgi5kg3p0y92az9m66ahbpjksys9bc17q9gchh9h1x") (r "1.63.0")))

(define-public crate-filesfinder-0.4.7 (c (n "filesfinder") (v "0.4.7") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ydnl8v9y5apglvy575q5hnfbd4gfhycnj0awizxh6k225q13fhr") (r "1.63.0")))

(define-public crate-filesfinder-0.4.8 (c (n "filesfinder") (v "0.4.8") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1f8fn6ng3sdx69ja0bg89m50abiqq3x5r0dggg4drqaz5p17g6a0") (r "1.63.0")))

(define-public crate-filesfinder-0.4.9 (c (n "filesfinder") (v "0.4.9") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "094fxjb6q3306j3qgpz3r6lp1pfqg6aws6rb88vhgmvzhvr922wl") (r "1.63.0")))

(define-public crate-filesfinder-0.4.10 (c (n "filesfinder") (v "0.4.10") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "176gagc79xk58m8sh6zbazxxpj14bm6xcyqrb5z9mwkwfk6m2344") (r "1.63.0")))

(define-public crate-filesfinder-0.4.11 (c (n "filesfinder") (v "0.4.11") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14ckb3npqfr0py8nqbnssfdhrvbary1i2sp07vlj4348za09vr1p") (r "1.63.0")))

(define-public crate-filesfinder-0.4.12 (c (n "filesfinder") (v "0.4.12") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02hmfhzkmxp71aydj1ns5pnhvj88m9gq6xmbddsjfq4ali81vslk") (r "1.63.0")))

(define-public crate-filesfinder-0.4.13 (c (n "filesfinder") (v "0.4.13") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11sfdbakhx565d1b2jm205zgaz0xarj11jf2n5ik8lawh3bc0875") (r "1.63.0")))

(define-public crate-filesfinder-0.5.1 (c (n "filesfinder") (v "0.5.1") (d (list (d (n "bstr") (r "^1.9.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)) (d (n "globset") (r "^0.4.14") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0lb2i2jl8dz5p18a6w8m9h3nvrj0a453wgw41m7mxg30m1j8ygxb") (r "1.74.0")))

