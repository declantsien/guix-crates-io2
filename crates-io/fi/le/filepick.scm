(define-module (crates-io fi le filepick) #:use-module (crates-io))

(define-public crate-filepick-0.1.0 (c (n "filepick") (v "0.1.0") (h "1pmjandgq0yga23wbyh8j1ql5nkric50lvmhin3qr8nsgh3wfakl")))

(define-public crate-filepick-0.1.1 (c (n "filepick") (v "0.1.1") (h "16ga8xsxf78yrcwdx74a2g37w6lcwmgwpsqzkz022yw1wr6pnnp9")))

