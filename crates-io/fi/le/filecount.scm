(define-module (crates-io fi le filecount) #:use-module (crates-io))

(define-public crate-filecount-0.1.0 (c (n "filecount") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 0)))) (h "0y3w6g1bc56akjv5yg5adl7lxvr83jx89lqh4fdwlmwdkhy2wgkx")))

