(define-module (crates-io fi le filesets) #:use-module (crates-io))

(define-public crate-filesets-0.1.0 (c (n "filesets") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0fwswjyxvn47d6r0885gv745mw0hs4r6mgd1k4q8z6rx90v928yc")))

(define-public crate-filesets-0.1.1 (c (n "filesets") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "03fldn34pbyxffs9aq8j1fp0vhmqjxh3pm0n2qd0pfidimy04fns")))

