(define-module (crates-io fi le filedescriptor) #:use-module (crates-io))

(define-public crate-filedescriptor-0.1.0 (c (n "filedescriptor") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bw9xy94j270jpkavb8il40fmaifyry2qzb26vmnrmbsd1kw50pj")))

(define-public crate-filedescriptor-0.1.1 (c (n "filedescriptor") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hyfjzy598k6wd4filzxdyxhzmjxnnwi5pcfxzvpciwca8ysmwsz")))

(define-public crate-filedescriptor-0.2.0 (c (n "filedescriptor") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j3c04a21fg200sam86ayvph6275skw8sldjdjbsci7xs14wkv17")))

(define-public crate-filedescriptor-0.3.0 (c (n "filedescriptor") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rxsq3ljcsc6bc82ys8qz89f2zjsk4y5d250kxd2lcmnmxh5k8ly")))

(define-public crate-filedescriptor-0.4.0 (c (n "filedescriptor") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07s6fmjcgyiyph0d79ixfl9dy22hvb128hhvw77zwnwbwiy1vxcy")))

(define-public crate-filedescriptor-0.5.0 (c (n "filedescriptor") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07nc9yavpbpyndrl0pqcvs53r09d2k6h38b876gjf214509cdb5s")))

(define-public crate-filedescriptor-0.5.1 (c (n "filedescriptor") (v "0.5.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k9fy4g99majrwfwxsf50wa77bzr40va3bajhyvq2n9nasm5zmgd")))

(define-public crate-filedescriptor-0.5.2 (c (n "filedescriptor") (v "0.5.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mm2rnmg9w0kqsm85m3i4zynf686kilrpcbrglfhp4hlbvw739h8")))

(define-public crate-filedescriptor-0.6.0 (c (n "filedescriptor") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dzr1z5niqchis8aqgx3v9kwd56hzgbrmpw42bvb7nppa0aw5w08")))

(define-public crate-filedescriptor-0.7.0 (c (n "filedescriptor") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yrybgb2lb2a2dwxpl0ym16qglmd55jkirxhwsjm25fcj4xf6d96")))

(define-public crate-filedescriptor-0.7.1 (c (n "filedescriptor") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f0f5n04mzdcghbsjdhgxrx56v7b1fpbg9nghm8px4lsrgjsnvf4")))

(define-public crate-filedescriptor-0.7.3 (c (n "filedescriptor") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01jc33j9jsjx5s1918dddh9h19q4kmakcrcam6vz6hzklbfv8377")))

(define-public crate-filedescriptor-0.8.0 (c (n "filedescriptor") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jzjajskzdmvmmscghvrgn79wy436wbqc6ag0fwm5ajibrsq67m7")))

(define-public crate-filedescriptor-0.8.1 (c (n "filedescriptor") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gvvsiabpydd1n92ki5m0ijydfj943ca0lcy8q0gyd84wajxilwy")))

(define-public crate-filedescriptor-0.8.2 (c (n "filedescriptor") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "handleapi" "fileapi" "namedpipeapi" "processthreadsapi" "winsock2" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vplyh0cw35kzq7smmp2ablq0zsknk5rkvvrywqsqfrchmjxk6bi")))

