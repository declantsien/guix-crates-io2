(define-module (crates-io fi le file-matcher-rs) #:use-module (crates-io))

(define-public crate-file-matcher-rs-0.1.0 (c (n "file-matcher-rs") (v "0.1.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "049gl894ll0ms5ix1065iz5v8aacjk3dby03zd7mjnmaf3zyrhm8") (f (quote (("all" "regex" "wildmatch")))) (y #t)))

(define-public crate-file-matcher-rs-0.2.0 (c (n "file-matcher-rs") (v "0.2.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1zk1adwiigwy6vw8spkyv1rab05zqicd0zmq7i6346fbg9rlv3dj") (f (quote (("all" "regex" "wildmatch")))) (y #t)))

(define-public crate-file-matcher-rs-0.2.1 (c (n "file-matcher-rs") (v "0.2.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "0b0mmscz0d02rrbin62gs3bn4c6mfh9wanw2alnhhci66zwcsy80") (f (quote (("all" "regex" "wildmatch")))) (y #t)))

(define-public crate-file-matcher-rs-0.2.2 (c (n "file-matcher-rs") (v "0.2.2") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "1cdxy8lakzh4h369mpgkal7a8c0i8avn8di8ih7j4d00bhsqayac") (f (quote (("default" "all") ("all" "regex" "wildmatch")))) (y #t)))

(define-public crate-file-matcher-rs-0.2.3 (c (n "file-matcher-rs") (v "0.2.3") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "wildmatch") (r "^2") (o #t) (d #t) (k 0)))) (h "0q2gixf4w5rraabxisx8argln0pc5cw8vl3xqy7czbi27g81rrps") (f (quote (("default" "all") ("all" "regex" "wildmatch")))) (y #t)))

