(define-module (crates-io fi le file_cat) #:use-module (crates-io))

(define-public crate-file_cat-0.1.0 (c (n "file_cat") (v "0.1.0") (h "1i4br3wskmx58lnlbvbn97p9fm05qa1vly618bh49mxsnnalbz72")))

(define-public crate-file_cat-0.1.1 (c (n "file_cat") (v "0.1.1") (h "0x62w2zlimrrl70lxdg50z06b0kjgnfg368ssaxrmjyl4r30a2ns")))

(define-public crate-file_cat-0.2.0 (c (n "file_cat") (v "0.2.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "127m14l1a08rax1ql523ih9p1c034l1dk4r8a5sg33mcc1wj54ll")))

(define-public crate-file_cat-0.3.0 (c (n "file_cat") (v "0.3.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)))) (h "00l38lzc2c6vbzqpx8wp0da7l7izmj8889a1n8pibh2ifchjzp3a")))

