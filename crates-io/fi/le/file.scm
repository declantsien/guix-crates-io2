(define-module (crates-io fi le file) #:use-module (crates-io))

(define-public crate-file-0.1.0 (c (n "file") (v "0.1.0") (h "1njxvslnrs0gwlny28dna405wxb6xd3v626s8yx9m7wv79lzbwqn") (y #t)))

(define-public crate-file-1.0.0 (c (n "file") (v "1.0.0") (h "0aklg373m0v5jk59j9h1w054a9lfx59bsqgpwifjd4mapzkq0p8h") (y #t)))

(define-public crate-file-1.1.0 (c (n "file") (v "1.1.0") (h "16vl3xdjkr0hdvi197llqy9bwr78r44dsdj9y2krm2ggd1xiq6dn") (y #t)))

(define-public crate-file-1.1.1 (c (n "file") (v "1.1.1") (h "12yqiyq8g45yk0syhsw3d9gcc4zhphda2xddw01v3ml87m76kvi2") (y #t)))

(define-public crate-file-1.1.2 (c (n "file") (v "1.1.2") (h "0yn3cpnc1m83g7vs2b7in2m9j48x6dyb8vqi46h9a7m443lj0907")))

