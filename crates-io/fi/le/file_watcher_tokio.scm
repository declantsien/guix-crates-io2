(define-module (crates-io fi le file_watcher_tokio) #:use-module (crates-io))

(define-public crate-file_watcher_tokio-0.1.0 (c (n "file_watcher_tokio") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0widz83q6qnz8046hi5w4k8flg5hyi4xnvc27arq4s3b0sgx9s3i")))

(define-public crate-file_watcher_tokio-0.1.1 (c (n "file_watcher_tokio") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "061yhggbvjfccqdgln9g29xg3b5z8m7xmi51jbn04llqr42fhg66") (y #t)))

(define-public crate-file_watcher_tokio-0.2.0 (c (n "file_watcher_tokio") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1kraxvisvz3ckyqbplfz48z3kiqhf70wbmg8mnj82hfl2ikm9i30")))

(define-public crate-file_watcher_tokio-0.3.0 (c (n "file_watcher_tokio") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "05d3yfh7kb8c4fl0dnhkr34k5px080jw354zvfzn4vjblfazq828")))

