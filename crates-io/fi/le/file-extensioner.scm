(define-module (crates-io fi le file-extensioner) #:use-module (crates-io))

(define-public crate-file-extensioner-0.1.0 (c (n "file-extensioner") (v "0.1.0") (d (list (d (n "cargo-release") (r "^0.23.1") (d #t) (k 2)) (d (n "clap") (r "^4.0.26") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "infer") (r "^0.11.0") (d #t) (k 0)))) (h "1ap8mfv1hrybpqpldjziiga1b1719azgjpsjzqwk0k69pnddf4f6")))

