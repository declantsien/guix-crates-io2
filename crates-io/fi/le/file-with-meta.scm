(define-module (crates-io fi le file-with-meta) #:use-module (crates-io))

(define-public crate-file-with-meta-0.1.0 (c (n "file-with-meta") (v "0.1.0") (d (list (d (n "expect-exit") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "ureq") (r "^2") (o #t) (d #t) (k 0)))) (h "0bw0k4s7jxrqwas89g9zh1h80pkq1bn2qf2f46flfpa50daw64jl") (f (quote (("default" "ureq"))))))

(define-public crate-file-with-meta-0.1.1 (c (n "file-with-meta") (v "0.1.1") (d (list (d (n "expect-exit") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "ureq") (r "^2") (o #t) (d #t) (k 0)))) (h "0wp91qmshi72f39gc17vpwmqwrflb6k6bv8nj4vb7g1bpx4r6kc2") (f (quote (("default" "ureq"))))))

(define-public crate-file-with-meta-0.2.0 (c (n "file-with-meta") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2") (o #t) (d #t) (k 0)))) (h "196kpj52ji1n05syjv9axscrvrw4rl7czcazckj23rc6rjxrpnik") (f (quote (("default" "ureq"))))))

