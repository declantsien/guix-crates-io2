(define-module (crates-io fi le file-watcher) #:use-module (crates-io))

(define-public crate-file-watcher-0.0.1 (c (n "file-watcher") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dont_disappear") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 2)))) (h "0x2027y7q0g4mc7pjzkmk5k6wlbwmcpfmxarfvsvygh6m0zvdimr")))

(define-public crate-file-watcher-0.0.2 (c (n "file-watcher") (v "0.0.2") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1brgh9cwnwc6qnc9zghsh38z1y0mrnk9y932pakrvj5cskbxnwfs")))

(define-public crate-file-watcher-0.0.3 (c (n "file-watcher") (v "0.0.3") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0w2ml8xzg8h8czh9ask05a86mx6dz4m06kdf57n9799cgrs9hfd7")))

(define-public crate-file-watcher-0.0.4 (c (n "file-watcher") (v "0.0.4") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1rcf6yhwspq845x1za4n01q486kpwals5gacpysqja6x26v00rkd")))

(define-public crate-file-watcher-0.0.5 (c (n "file-watcher") (v "0.0.5") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0j5y3a4i8mj87682x8wlhkw06djjcgj9hk9brsmnh3ypr5lxfyhz")))

(define-public crate-file-watcher-0.0.6 (c (n "file-watcher") (v "0.0.6") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1fl9hkzkyk0gaph5p69lxaq9f9ydj8kcyijf25qw7x4sjyc7xkkq")))

(define-public crate-file-watcher-0.0.7 (c (n "file-watcher") (v "0.0.7") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0vabqnm3p37x9vjd4aq224bvsa4s6lkwadg3rmbx7f0rb56rgwpb")))

(define-public crate-file-watcher-0.0.8 (c (n "file-watcher") (v "0.0.8") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0jx0hc3yf8cxc5pdzzh3qfk5d2ijzxkn8gzd1c19ch7xdcpqcv6x")))

(define-public crate-file-watcher-0.0.9 (c (n "file-watcher") (v "0.0.9") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0n7r2x8scjyidy7lx405fy76jfr4jdydl58qm2llhdzm7xm8swkd")))

(define-public crate-file-watcher-0.0.10 (c (n "file-watcher") (v "0.0.10") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1pzl3vlrkaqqcq8m2dyv28qjzimirwmwp970cj7mz6llwyzji4kq")))

(define-public crate-file-watcher-0.0.11 (c (n "file-watcher") (v "0.0.11") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0qswkjai6ijv26ixaj81yvwpdvp7345835qmnrh07wv6xnanypnz")))

(define-public crate-file-watcher-0.0.12 (c (n "file-watcher") (v "0.0.12") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1031w0vas0nkn4w8f1kq721fdc6ggd3djz5i8xn2g39px1r2y5f3")))

(define-public crate-file-watcher-0.0.13 (c (n "file-watcher") (v "0.0.13") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "04np2xkp633iqa3vj1gbhl1rgkn244hvfrl75zdyx0kn53fpq9l5")))

(define-public crate-file-watcher-0.0.14 (c (n "file-watcher") (v "0.0.14") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "0w3g8a7kaw10mmcsz8w0ph5k9jgsl7gma9y0wswgi1y97p4ln2gq")))

(define-public crate-file-watcher-0.0.15 (c (n "file-watcher") (v "0.0.15") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "1rr0b21shkbsbshpp2n7877j2bz32pclxyx4vxqff17p18554fm8")))

(define-public crate-file-watcher-0.0.16 (c (n "file-watcher") (v "0.0.16") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "18bklwygnmarprvfsn2qvmgalsi7a7mgaj3vm28h6maq3fc2rv4j")))

(define-public crate-file-watcher-0.0.17 (c (n "file-watcher") (v "0.0.17") (d (list (d (n "set-error") (r "^0.0.1") (d #t) (k 0)))) (h "158x6xhgv82n3zikgbavqvkqhk6zlfg5i8czzsji1bwcy4crammr")))

(define-public crate-file-watcher-0.0.18 (c (n "file-watcher") (v "0.0.18") (d (list (d (n "set-error") (r "^1") (d #t) (k 0)))) (h "13r8cp0rjn11rydqwzvgc08g1b5v1q8gd06zffsgc7wd73bfhxx3")))

