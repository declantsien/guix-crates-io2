(define-module (crates-io fi le filetime) #:use-module (crates-io))

(define-public crate-filetime-0.1.0 (c (n "filetime") (v "0.1.0") (h "0gaai8sfpj6qwx5445dg2sm56wqb022ffl9ivm9rv4p8piizpv35")))

(define-public crate-filetime-0.1.1 (c (n "filetime") (v "0.1.1") (h "0kqm6lfp9bbb67309qc1hj37zq9v27d4ych2fw1mxgpzw8pwwzsg")))

(define-public crate-filetime-0.1.2 (c (n "filetime") (v "0.1.2") (h "0cxyvywm316s6akyypsygl2m9k7abwrmnm6hhj4hg0rc4pv4cspq")))

(define-public crate-filetime-0.1.3 (c (n "filetime") (v "0.1.3") (h "0k2rcjld8z08ym1smcs362cbd9l5q1524x6b50idwrk8v7whxpjj")))

(define-public crate-filetime-0.1.4 (c (n "filetime") (v "0.1.4") (h "1ad2cl0fy0np803xcz2xsjxd0kkhdack0g078dwaj9cwr2rcc0jv")))

(define-public crate-filetime-0.1.5 (c (n "filetime") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "140r740z7vcn2qkcaihic7rky9xgdsn6vqknn00mc6khvnw1c0c9")))

(define-public crate-filetime-0.1.6 (c (n "filetime") (v "0.1.6") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "01qr36ah6rrd1lq3c2i0fa3d9adlz1z15zswl8jhkdy539xyfg3x")))

(define-public crate-filetime-0.1.7 (c (n "filetime") (v "0.1.7") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "18sxmrvx1f4dyi9ch2qamzgmm1iq1zp9xlq2vc4dy0ag86a1rwcf")))

(define-public crate-filetime-0.1.8 (c (n "filetime") (v "0.1.8") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0hff1k8qdqsk2kwsz876yxzg0gflc8r25cs0i7b1cf9rmyssw4yw")))

(define-public crate-filetime-0.1.9 (c (n "filetime") (v "0.1.9") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1w957i942wnpvapy9jp08k6facq6h8gc2fakd3azlv361a8fx4hd")))

(define-public crate-filetime-0.1.10 (c (n "filetime") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08p9scgv30i1141cnp5xi4pqlnkfci455nrpca55df1r867anqsk")))

(define-public crate-filetime-0.1.11 (c (n "filetime") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0xdk15mxlzsby3zsjyn079jlk2yqrjncndpkbwbpr8pb8kfyzsv7")))

(define-public crate-filetime-0.1.12 (c (n "filetime") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "03icffziapaf5rja5fq5fjdqn9xvw20y0s96g4w6yz2k72zrkcba")))

(define-public crate-filetime-0.1.13 (c (n "filetime") (v "0.1.13") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "05bwlm9js0gh16ijva10pmsi5xwvqnpz1jcahwp60jl54silkgmk") (y #t)))

(define-public crate-filetime-0.1.14 (c (n "filetime") (v "0.1.14") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13hcfq6zf3scnl0xqrppij4bn401gfl7ygjqm4sk61i7g67yqxda")))

(define-public crate-filetime-0.1.15 (c (n "filetime") (v "0.1.15") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "03xishfxzpr4nfz4g3r218d6b6g94rxsqw9pw96m6wa8wgrm6iki")))

(define-public crate-filetime-0.2.0 (c (n "filetime") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ww54gn2v65yizaixsrz8393pa22yiafifdb1b245m0bmwwhllq8")))

(define-public crate-filetime-0.2.1 (c (n "filetime") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0pswp7pxl00hlqhw493b4fdyqgm7bd5k4x61099ij4vvwx4rhjys")))

(define-public crate-filetime-0.2.2 (c (n "filetime") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08grd9ailbjlajx2nlhzlq9504d7h6p0bl7y9vyjxrpaym5ycmvg")))

(define-public crate-filetime-0.2.3 (c (n "filetime") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1knwdycvsfxfiac3q1dddh83a43y1lb8pqp5fcb1nq9s32bhrsmk")))

(define-public crate-filetime-0.2.4 (c (n "filetime") (v "0.2.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ip66dc93aqr2hyjkzaa745ycxjrwqmc97bq0xvpxqjbihd5rpx2")))

(define-public crate-filetime-0.2.5 (c (n "filetime") (v "0.2.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17a213hfrmvslspn5h04p4r08la2mly5nl24ywggb8fb7w1n731g")))

(define-public crate-filetime-0.2.6 (c (n "filetime") (v "0.2.6") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kvrncy72gfhl16ssnhl2ayx998xva8ffcfsimrls33g6kf3f1a5")))

(define-public crate-filetime-0.2.7 (c (n "filetime") (v "0.2.7") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sflihq2l77xjrza7yjalnxsc7dxzg25rhzcfbd9vmyfah5kimvb")))

(define-public crate-filetime-0.2.8 (c (n "filetime") (v "0.2.8") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zfc90802dbw11bx6kmm8zw6r88k7glm4q6l8riqw35an3dd9xhz")))

(define-public crate-filetime-0.2.9 (c (n "filetime") (v "0.2.9") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pjaa1qz4crlfxqvpgf0gqp9mx3ih4xjc7fi0518x62c00wgr7pm")))

(define-public crate-filetime-0.2.10 (c (n "filetime") (v "0.2.10") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15f6gli6b29q25a1ziajrbmk9cg8qrf5fz6z393c4bqkkdbigz5g")))

(define-public crate-filetime-0.2.11 (c (n "filetime") (v "0.2.11") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nrln0mxcp0xy14blsn524iwfcbz4853a2qfiwzw9gbhmcpxl075")))

(define-public crate-filetime-0.2.12 (c (n "filetime") (v "0.2.12") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17nginckcwgjm98l2ilhjfyd6s1pnai0db2hr6sl91n6vismgn1y")))

(define-public crate-filetime-0.2.13 (c (n "filetime") (v "0.2.13") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zlyqwl6l1vv95x5mzxrhqp9jy136wyvsvzhbc0lhxm57qwjl4hc")))

(define-public crate-filetime-0.2.14 (c (n "filetime") (v "0.2.14") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s71cb7hzkk4ahc5j53w8933gnxv2giyj7v0za5hbbk37ahwyd0x")))

(define-public crate-filetime-0.2.15 (c (n "filetime") (v "0.2.15") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "165w8nrc4f7jqhf9f1c86s1vw9wh2v43h2l5h93dil6rv21wyp4p")))

(define-public crate-filetime-0.2.16 (c (n "filetime") (v "0.2.16") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03192qwgcyyyalvi377i54r10ixw4mdc1zzplyk7hl824qk8wh60")))

(define-public crate-filetime-0.2.17 (c (n "filetime") (v "0.2.17") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0v6893j7jp0hzc1q54nnx6848yvrw8ipzdkmvlhbqm4klnx7njp9")))

(define-public crate-filetime-0.2.18 (c (n "filetime") (v "0.2.18") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ly9bwmlapixr356ncr5kcxaiyjqyiyz5gcdr1fy4ynhh79n75jb")))

(define-public crate-filetime-0.2.19 (c (n "filetime") (v "0.2.19") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y9p1ixlwp0b66vy6ri6ghpkhfyw9lbm3s9k89880x0crml4d22f")))

(define-public crate-filetime-0.2.20 (c (n "filetime") (v "0.2.20") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04k4imip9hjm56pirnmjz1h7sn80124nz4bdvjggy8hvs7lfcgca")))

(define-public crate-filetime-0.2.21 (c (n "filetime") (v "0.2.21") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2.9") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lsii3b9w4ya0g5nf9ycnif4izy8i492x5ri752d9sdfxi689g2w")))

(define-public crate-filetime-0.2.22 (c (n "filetime") (v "0.2.22") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.3.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w1a4zb4ciqjl1chvp9dplbacq07jv97pkdn0pzackbk7vfrw0nl")))

(define-public crate-filetime-0.2.23 (c (n "filetime") (v "0.2.23") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.4.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1za0sbq7fqidk8aaq9v7m9ms0sv8mmi49g6p5cphpan819q4gr0y")))

