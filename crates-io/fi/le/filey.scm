(define-module (crates-io fi le filey) #:use-module (crates-io))

(define-public crate-filey-0.1.0 (c (n "filey") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1bv1cd18skr6w6iwwjggkbw9iyp8f7xwjisinibnp7dz3xng1gmm")))

(define-public crate-filey-0.2.0 (c (n "filey") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1r0j9rh4vr05hcq9gy6h6l41ycs9r9rf8wi530iphr0n1rd228ic")))

(define-public crate-filey-0.3.0 (c (n "filey") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1nnic1aippg8izs2d2wwlgs4s6032y2d56fdzvwknfb4zvlvrjsh")))

(define-public crate-filey-0.3.1 (c (n "filey") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0zyzxfpldbd03gz587crgbx1p9ssf0aid0g9g6mj790pw6a8nwqd")))

(define-public crate-filey-0.3.2 (c (n "filey") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0y7brimws0gnz34dhh9ysfwlaww5lck6zsq3dhzlc1dghkjbp8rc")))

(define-public crate-filey-0.3.3 (c (n "filey") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1kfgp12qqc201pax5pbp477l3qhkds20gmmj0rgpay9nani7k8lh")))

(define-public crate-filey-0.3.4 (c (n "filey") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "01s76pjszmk95c9bsfq7g5yfc103flp7m04a79s8vnr1n1gn4c00")))

(define-public crate-filey-0.3.5 (c (n "filey") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0zr8xanf4amz4igr8sc4459w33ibp64slw22fx7c651q10cq13c3")))

(define-public crate-filey-0.3.6 (c (n "filey") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1532js43s2xv2403j3dmixazs2qjazn3chzmcmqvakrpyrkkgnq6")))

(define-public crate-filey-0.3.7 (c (n "filey") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0gni3x21rzcin57ny3gd2r63jmn1aq5h1x5p4vfm0ahvclvadj1c")))

(define-public crate-filey-0.3.8 (c (n "filey") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08xwwv6x7sbyf8pm6ly5zc763l7s9ca7r7d0p3qxch8006bvjyb9")))

(define-public crate-filey-0.3.9 (c (n "filey") (v "0.3.9") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1chi8ik46j48dlk0ysi0cqpdcz01m4y4mycawi7lz67aza37hbw8")))

(define-public crate-filey-1.0.0 (c (n "filey") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0111gz7bazl6fim7iakqz2llzk4cngwg265v9gr7xddf9gd1xi0s")))

(define-public crate-filey-1.1.0 (c (n "filey") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0j40308h895qll3ci2n79qla5qadg94s26canvwpb2hq0av6jcdw")))

(define-public crate-filey-1.1.1 (c (n "filey") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0m3mzfpiq5cydj2rzqq12azfxbb409ma7rkabz6xv97h0m0pbkis")))

(define-public crate-filey-1.2.0 (c (n "filey") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "12vsdn7hmb4xswdbnwi2ym9gjlzdlrswy6swkzh5gndxrldf5fvi")))

(define-public crate-filey-1.3.0 (c (n "filey") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1gbs8q1f3k04xxnssmmnwm311zgrc2ac7pasw9j4msigsvpwfvsl")))

(define-public crate-filey-1.4.0 (c (n "filey") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0gsdfrpag5k2w1bv1nwfjj5k85s7fj1i4q42qzj2s63rbv9gjq9k")))

