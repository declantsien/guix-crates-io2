(define-module (crates-io fi le filewatcher) #:use-module (crates-io))

(define-public crate-filewatcher-0.1.0 (c (n "filewatcher") (v "0.1.0") (h "1qpyd9gxbg90jc8zm5iywwrj55d84dar7501v0ng4rhq7i8rmccx")))

(define-public crate-filewatcher-0.2.0 (c (n "filewatcher") (v "0.2.0") (h "0282zhnzypbfnflfmaz8xl5g2n6lffg8p7k3lp5snsivachycymm")))

(define-public crate-filewatcher-0.2.1 (c (n "filewatcher") (v "0.2.1") (h "0k6r5vs9wnwyz9hdbya77bdnih81sbp3lcx6z5b7156njqrgc2pb")))

(define-public crate-filewatcher-0.2.2 (c (n "filewatcher") (v "0.2.2") (h "0is1wsmhyy1ardcd0d5502izd7hyg0zrrifd3j12mw6ylmfjc22a")))

