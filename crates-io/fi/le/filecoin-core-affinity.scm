(define-module (crates-io fi le filecoin-core-affinity) #:use-module (crates-io))

(define-public crate-filecoin-core-affinity-0.1.0 (c (n "filecoin-core-affinity") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "143d8jb9rpjm8za5v83p44f58xh4vhmsh8dcc4ihz7y8bvw0hi4w") (f (quote (("default" "hwloc2"))))))

(define-public crate-filecoin-core-affinity-0.1.1 (c (n "filecoin-core-affinity") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0mhsh4flg9ardj4rr8vwji2p3vhqa7nd14s5jb62rb2878sdp49p") (f (quote (("default" "hwloc2"))))))

(define-public crate-filecoin-core-affinity-0.2.1 (c (n "filecoin-core-affinity") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "02q2vwq251drp6z7qb9d01pvd5acdb5qyp8zd1lc6knpw8alvhxn") (f (quote (("default" "hwloc2"))))))

