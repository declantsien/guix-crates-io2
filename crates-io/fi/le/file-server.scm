(define-module (crates-io fi le file-server) #:use-module (crates-io))

(define-public crate-file-server-1.0.0 (c (n "file-server") (v "1.0.0") (d (list (d (n "actix-files") (r "^0.1") (d #t) (k 0)) (d (n "actix-web") (r "^1") (d #t) (k 0)))) (h "0sih01b1rzslvppddf8xdrh05d2d8gdc3mdzy0jmlj6412bbivx0")))

(define-public crate-file-server-1.0.1 (c (n "file-server") (v "1.0.1") (d (list (d (n "actix-files") (r "^0.1") (d #t) (k 0)) (d (n "actix-web") (r "^1") (d #t) (k 0)))) (h "0g9w8bwnz0c391wqlh3cr74abvypkp0n9ycxhgdgfynvpfrwcxpi")))

(define-public crate-file-server-1.1.0 (c (n "file-server") (v "1.1.0") (d (list (d (n "actix-files") (r "^0.1.7") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ldwhax0s4vr5pwckpfx3xy958yhcsq736z4hig5i6jly2c9i2c2")))

(define-public crate-file-server-1.2.0 (c (n "file-server") (v "1.2.0") (d (list (d (n "actix-files") (r "^0.1.7") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ihdzfmsl8icqxb538jrq083h8lld9d59bqnblv0prcmybahjhzl")))

(define-public crate-file-server-1.3.0 (c (n "file-server") (v "1.3.0") (d (list (d (n "actix-files") (r "^0.1.7") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "17xjjgckjh37nmqkhjcsaprqaf14al7bijjxr7wlcxzp6rqar5c1")))

