(define-module (crates-io fi le file-expert) #:use-module (crates-io))

(define-public crate-file-expert-1.0.0-alpha.0 (c (n "file-expert") (v "1.0.0-alpha.0") (d (list (d (n "clap") (r "=3.0.0-beta.4") (f (quote ("std" "cargo"))) (k 0)) (d (n "fancy-regex") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06zyxdmw26psfy5k0i3frpp2yp64sq7b44kzqwcr4aqn0wz35xqg")))

(define-public crate-file-expert-1.0.0 (c (n "file-expert") (v "1.0.0") (d (list (d (n "clap") (r "=3.0.0-beta.4") (f (quote ("std" "cargo"))) (k 0)) (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jrk67g79s18zwp6hndfg5gzdz34d8q3783yxwsx2ffjljkzcv4a")))

(define-public crate-file-expert-1.1.0 (c (n "file-expert") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("std" "cargo"))) (k 0)) (d (n "fancy-regex") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "update-informer") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "15bhln0z2sv3gzzhwqlvs38rwqd1n1s70jr3r2qkairrc1653wnw") (f (quote (("default" "update-informer")))) (s 2) (e (quote (("update-informer" "dep:update-informer"))))))

