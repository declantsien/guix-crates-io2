(define-module (crates-io fi le file-mode) #:use-module (crates-io))

(define-public crate-file-mode-0.1.0 (c (n "file-mode") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.86") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1aydflb4iwhgnsx1614gq64c9ivv6xkp28ckwgzl8m6xcx0r415a")))

(define-public crate-file-mode-0.1.1 (c (n "file-mode") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.86") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0bxxnhjzvsz0v4nlwqrpx8wzlrmii6vjxhkjfa01mgchx88p3kgw")))

(define-public crate-file-mode-0.1.2 (c (n "file-mode") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.86") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0p41x5ak8kpl7sqcfj1y7lspdrr0psnk4jv2ak9vhwjp912s2gkp")))

