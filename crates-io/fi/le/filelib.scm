(define-module (crates-io fi le filelib) #:use-module (crates-io))

(define-public crate-filelib-0.1.0 (c (n "filelib") (v "0.1.0") (h "1fmnd1iy06njg127mm65s5a3a2xp5426ykimhc6wz83yd3i3dvl5") (y #t)))

(define-public crate-filelib-0.1.1 (c (n "filelib") (v "0.1.1") (h "1qlbjdzwzdcbk1lwg9smmijgikwxzv4740dhvl4qpaaxgvdlf9dm") (y #t)))

(define-public crate-filelib-0.1.2 (c (n "filelib") (v "0.1.2") (h "0ssp2jqm6l8cnd91sqa1k61lfmay1d06lf63h9n56ax7sm723qmx")))

(define-public crate-filelib-0.1.3 (c (n "filelib") (v "0.1.3") (h "0l7vr1pvm0pr8p6mg20kw4yhkws0pwp5vcf26r6ppvm2rj446sq5")))

