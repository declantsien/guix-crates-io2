(define-module (crates-io fi le file-crypto) #:use-module (crates-io))

(define-public crate-file-crypto-0.3.0 (c (n "file-crypto") (v "0.3.0") (d (list (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "05pffar1jya23jm8lychb7am5x3wl234br751wy8jd2zkd265ah2")))

