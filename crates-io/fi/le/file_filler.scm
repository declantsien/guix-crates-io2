(define-module (crates-io fi le file_filler) #:use-module (crates-io))

(define-public crate-file_filler-0.1.0 (c (n "file_filler") (v "0.1.0") (d (list (d (n "cinner") (r "^0.1.2") (d #t) (k 0)))) (h "00a3cwf2z8rkalrz7hy9km113lan937nx9kzk4zabb0m6hwg1r1f")))

(define-public crate-file_filler-1.0.0 (c (n "file_filler") (v "1.0.0") (d (list (d (n "cinner") (r "^0.1.2") (d #t) (k 0)))) (h "0icrjn9ck3r02qcyp1v36m5dw8l4jbjkxkn5b9ds664la5n06qm6")))

