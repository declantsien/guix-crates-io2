(define-module (crates-io fi le fileorama) #:use-module (crates-io))

(define-public crate-fileorama-0.0.1 (c (n "fileorama") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0akh16f1ya9qc6j133w4ymp1xwjawwhplh1g4szk900x7plh7a9r")))

(define-public crate-fileorama-0.0.2 (c (n "fileorama") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ftp") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1ib0y7v75gxr37ahkmswrgj22zf56sxw5b86k2mdg2vkia2hq2lr")))

