(define-module (crates-io fi le file_diff) #:use-module (crates-io))

(define-public crate-file_diff-0.1.0 (c (n "file_diff") (v "0.1.0") (h "142fb60q5zlq39nlym9pail1mkxmy2351cb5mrccnad28r3yiaxa")))

(define-public crate-file_diff-0.2.0 (c (n "file_diff") (v "0.2.0") (h "1za0da59j23fk3rr2nna82s2zn9r3qv87av6r37r221yc8bzxg7x")))

(define-public crate-file_diff-1.0.0 (c (n "file_diff") (v "1.0.0") (h "19a34rvbqg3b2my6ykax5n1qi2ahwbjacn9y2ji3h9gkp04ak9ri")))

