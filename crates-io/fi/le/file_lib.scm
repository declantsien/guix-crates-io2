(define-module (crates-io fi le file_lib) #:use-module (crates-io))

(define-public crate-file_lib-0.1.0 (c (n "file_lib") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0x1pink2xqxawygi1sbpyph8svkj7qwsjc1jwwpdxpjap4lxk5cj")))

(define-public crate-file_lib-0.1.1 (c (n "file_lib") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1k6vqa7aslmxb15xzyccad015birw0cvc8qmf4hfziy34q83qk19")))

