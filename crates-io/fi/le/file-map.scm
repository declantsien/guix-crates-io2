(define-module (crates-io fi le file-map) #:use-module (crates-io))

(define-public crate-file-map-0.1.0 (c (n "file-map") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18jk5pygwl0sj3mb7jcgfmf1l2hw3ji2qc0fqgis8sqhzy48v7x4")))

(define-public crate-file-map-0.1.1 (c (n "file-map") (v "0.1.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hb3086dncp5wdidrnx4yrldpkz1c6jiwsbbynqakbg95gbzk143")))

