(define-module (crates-io fi le file2png) #:use-module (crates-io))

(define-public crate-file2png-0.1.1 (c (n "file2png") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "regress") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "0vp2bj7pffz9zcs6ixrdq5hs3ld2lm8m5clsxk3abxc89cv1ddfp")))

