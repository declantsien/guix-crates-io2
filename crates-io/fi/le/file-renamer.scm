(define-module (crates-io fi le file-renamer) #:use-module (crates-io))

(define-public crate-file-renamer-0.1.0 (c (n "file-renamer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "02fvpfq6vmqwj8796kyxgwpmdh35m8akzyp94f15majb0z3lqzyl")))

(define-public crate-file-renamer-2.0.0 (c (n "file-renamer") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0snvpz4jwfw5cl1j9lzc5qryiwdmzipm8pa48jdnny7wff5cfgga")))

(define-public crate-file-renamer-3.0.0 (c (n "file-renamer") (v "3.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "06whlaja738a8009318qyjiy8nd7d24i422qgq73k7x0bxgjg8q1")))

