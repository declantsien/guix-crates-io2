(define-module (crates-io fi le file_checker) #:use-module (crates-io))

(define-public crate-file_checker-0.1.0 (c (n "file_checker") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "003arqyqxgkpj68zsd489cp1izx95rfpbba4qkmxarfw7ckav76l")))

(define-public crate-file_checker-0.1.1 (c (n "file_checker") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rqd2nf70gl8dx7jzgk3723wc0vk56jdlins4hzjsn65hvri080y")))

(define-public crate-file_checker-0.2.0 (c (n "file_checker") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0mf9yfdqgm1b5syzf9vi3w0cymyq2hwjbvf9zc3wnyj6qj0vhkas")))

(define-public crate-file_checker-0.2.1 (c (n "file_checker") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1wvzwjzzwm5sfdv00lif1dxwz3pf5jzvnyvj4n27y1c5kv8p3dr2")))

(define-public crate-file_checker-0.3.0 (c (n "file_checker") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0say8g88dafncsx7bk84j8f30aqja3s1f6m29v5hl89nx5bnk0cr")))

(define-public crate-file_checker-0.3.1 (c (n "file_checker") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "16h20c24x6l7c50shzhac5cilzw4hyw5yvzkllcn9xjpz9wpr8s0")))

