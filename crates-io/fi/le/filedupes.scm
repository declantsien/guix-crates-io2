(define-module (crates-io fi le filedupes) #:use-module (crates-io))

(define-public crate-filedupes-0.1.0 (c (n "filedupes") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "file") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1cq5l34l7j16c4z7gzl32za6x3wvrixcnjlpvi0mwbnc393nbv9q")))

(define-public crate-filedupes-0.1.1 (c (n "filedupes") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "file") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1pyc6ibmpwar5l0c4k1mxl16scdxyp6d9yxzkpy59j602gqbk5n1")))

