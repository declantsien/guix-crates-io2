(define-module (crates-io fi le filetest) #:use-module (crates-io))

(define-public crate-filetest-0.1.0 (c (n "filetest") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1kxcmbdv4lrgsf865xnam7w696ccf5xmy4ilchpqifdabpy3c3vd")))

(define-public crate-filetest-0.1.1 (c (n "filetest") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "132z835bbymzhcw0dd7jf4rc9mvmg23hc7w88qvd24c9caxad2vl")))

(define-public crate-filetest-0.1.2 (c (n "filetest") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "13kabdrqqdzha48wfaf132xy5df7drg01fwh4hzrgqhdcrf1i07z")))

