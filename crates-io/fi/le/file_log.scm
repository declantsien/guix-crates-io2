(define-module (crates-io fi le file_log) #:use-module (crates-io))

(define-public crate-file_log-0.1.0 (c (n "file_log") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0hf8zp22s6vaz50anwha23mmrfp0gx8qal7g5vfz47xg9ss9n6wn")))

(define-public crate-file_log-0.1.1 (c (n "file_log") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "138r26q2ln4c6vzif6yiamc18bpfp0f95v1h5w10zg5m0fhiddsy")))

