(define-module (crates-io fi n- fin-part-ord) #:use-module (crates-io))

(define-public crate-fin-part-ord-0.1.0 (c (n "fin-part-ord") (v "0.1.0") (d (list (d (n "daggy") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1kjfdln49j40lz6b29nk0rvzvg4svqyg63l3p85rvh5djm255n64") (f (quote (("pairs") ("default" "dag" "pairs")))) (s 2) (e (quote (("dag" "dep:daggy" "dep:petgraph"))))))

