(define-module (crates-io fi n- fin-calc) #:use-module (crates-io))

(define-public crate-fin-calc-0.1.0 (c (n "fin-calc") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "time_series_generator") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0ydjcklwk8vpwq8pnzhayzr6dq4rqbhkscmmymznxysj4fcsak7w")))

