(define-module (crates-io fi n- fin-stat) #:use-module (crates-io))

(define-public crate-fin-stat-0.1.0 (c (n "fin-stat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thousands") (r "^0.2") (d #t) (k 0)))) (h "101piqk012mw90pskbwvcdgmj76c62i15qg24g0ibc0mskh0r632")))

