(define-module (crates-io fi xn fixnum-approx-eq) #:use-module (crates-io))

(define-public crate-fixnum-approx-eq-0.1.0 (c (n "fixnum-approx-eq") (v "0.1.0") (d (list (d (n "fixnum") (r "^0.9.2") (d #t) (k 0)) (d (n "fixnum") (r "^0.9.2") (f (quote ("default" "i128" "i32" "i64" "i16"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1laib9sxj38rnqhb095bg9p10bvgyk2i3p54wgdmcxbwhsxaibxi") (f (quote (("i64" "fixnum/i64") ("i32" "fixnum/i32") ("i16" "fixnum/i16") ("i128" "fixnum/i128") ("default" "i128"))))))

