(define-module (crates-io fi -n fi-night) #:use-module (crates-io))

(define-public crate-fi-night-0.1.0 (c (n "fi-night") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18k53gbkbmpz56ns3mvl031mxf2qhnfxhkk19xl8axswblz07a9j") (f (quote (("meta_iter"))))))

(define-public crate-fi-night-0.1.1 (c (n "fi-night") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02vdc8l14pxrfrd3n9fsiqprl9dg6dw6vlbfa8gz9g6cgyx4r1az") (f (quote (("meta_iter"))))))

(define-public crate-fi-night-0.1.2 (c (n "fi-night") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gvl3wj5b6j7ljgd6agg140nmb62kmah6ljdhmj8plhwaam92l2c") (f (quote (("meta_iter"))))))

(define-public crate-fi-night-0.1.3 (c (n "fi-night") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lp4l023vvd17ygpd9wv2yr7zzydh0xa2sylsw7k1gl6v12a0mdl") (f (quote (("meta_iter") ("fsm_gen_code"))))))

(define-public crate-fi-night-0.1.4 (c (n "fi-night") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b15zlslbnki4fmkllqbpv1y7mp5d1207h7133kqbajh6h8aqlv9") (f (quote (("meta_iter") ("fsm_gen_code"))))))

(define-public crate-fi-night-0.1.5 (c (n "fi-night") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0690a38l1kdyiak3325dp8cr2rj4lkn705q363q8r4a9xwp5mdhz") (f (quote (("meta_iter") ("fsm_gen_code"))))))

(define-public crate-fi-night-0.1.6 (c (n "fi-night") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zmjnzhi8hzhki6aynhl33zykrsv2jh0gxf01ahkr2sf2r9klhj3") (f (quote (("meta_iter") ("fsm_gen_code"))))))

