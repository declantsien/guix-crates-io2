(define-module (crates-io fi md fimdoc) #:use-module (crates-io))

(define-public crate-fimdoc-0.1.0 (c (n "fimdoc") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)))) (h "19mxkvi8v1lpqgnf53dgq3dbjabskjqjacw9vp53kd8zwsap78yr")))

(define-public crate-fimdoc-0.1.1 (c (n "fimdoc") (v "0.1.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)))) (h "0waim4izgflwvp4488zkyhgypkznl9dpjqc9c7prnivawj5gxw52")))

(define-public crate-fimdoc-0.1.2 (c (n "fimdoc") (v "0.1.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0djqvy062dbz73xwjxwnmhmjf0i7k13lfax9r3n3vipfc43hwlw1")))

(define-public crate-fimdoc-0.1.3 (c (n "fimdoc") (v "0.1.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0w0v7w98x1zzqrsym4kgg4bmmxrzmipcg6ph72drpjiskfskp25b")))

(define-public crate-fimdoc-0.1.4 (c (n "fimdoc") (v "0.1.4") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0a12bjnx5319zv1ni0m2sp5qg22x9kgl5raq8rapq7alxp94ljr7")))

(define-public crate-fimdoc-0.1.5 (c (n "fimdoc") (v "0.1.5") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "14npgpd5brf8nwpacwy49rxgwzxhdbd6cx74kzqikn4pknaz1y49")))

(define-public crate-fimdoc-0.1.6 (c (n "fimdoc") (v "0.1.6") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "11rj4a5cvwki56mv3fnr03qk1l16k2k8bsvks52d7pcyf5i8ngsr")))

(define-public crate-fimdoc-0.1.7 (c (n "fimdoc") (v "0.1.7") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "1wsl86v6km4wjyahzdzd4ci3d71sanzyx415g3bdrl5anvngvz1a")))

(define-public crate-fimdoc-0.1.8 (c (n "fimdoc") (v "0.1.8") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0fsqbrya0wdw6kq6spxp6vnlr0iqjhkw1jzk9kgvwrh64i61dvfl")))

(define-public crate-fimdoc-0.1.9 (c (n "fimdoc") (v "0.1.9") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0fyxnk7k390yhg5rwyxbh6y7q9zxnm2zf6dl692ajvraanbk23ki")))

(define-public crate-fimdoc-0.2.0 (c (n "fimdoc") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0f01qgcrlhhlb4xm638lh76q2c4qmd56zaakcai6kfdw5wbb4s71")))

(define-public crate-fimdoc-0.2.1 (c (n "fimdoc") (v "0.2.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.1") (d #t) (k 0)))) (h "12k2pdzc5scrcbrp29phczv6h095k2wbcmdp0bbsabcqj8nlj4ph")))

(define-public crate-fimdoc-0.2.2 (c (n "fimdoc") (v "0.2.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.4") (d #t) (k 0)))) (h "19vgrppiahpgym2046xjw369yg4wfzqy2gqickcpga5krsx538fq")))

(define-public crate-fimdoc-0.3.0 (c (n "fimdoc") (v "0.3.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.4") (d #t) (k 0)))) (h "0i75sm0jkdpraxaad0rvih5jfgn99bp6pxbc635m533kdgasy0x1")))

(define-public crate-fimdoc-0.3.1 (c (n "fimdoc") (v "0.3.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.5") (d #t) (k 0)))) (h "1ji871y6531j8l3vddcc19ngybby9ncbaj6fvg2fq338xcx8xkn8")))

(define-public crate-fimdoc-0.3.2 (c (n "fimdoc") (v "0.3.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.5") (d #t) (k 0)))) (h "1ldc2iwv346b62x2h39r34la3g80q5ddw1vq0wwlsixy32szjq8y")))

(define-public crate-fimdoc-0.3.3 (c (n "fimdoc") (v "0.3.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "rarity") (r "^0.1.5") (d #t) (k 0)))) (h "0dmdqcqf18fw0d06kyrn7jh2jv7ai29h4l73alp1vnhpivm4n5kp")))

(define-public crate-fimdoc-0.3.4 (c (n "fimdoc") (v "0.3.4") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "golden-oak-library") (r "^0.1.0") (d #t) (k 0)))) (h "1r0h6y9f5b7pswrf61j4c9zj84sscbprwrs6qw8rdhxj3bl2pz81")))

(define-public crate-fimdoc-0.4.0 (c (n "fimdoc") (v "0.4.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "golden-oak-library") (r "^0.1.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)))) (h "1hcscan8vmd4jcd72zxag1c3g6fnk14s8z6isg5w5nhd3m4m0yq1")))

(define-public crate-fimdoc-0.5.0 (c (n "fimdoc") (v "0.5.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "golden-oak-library") (r "^0.1.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)))) (h "0f8y5z6bx6s55lna0hbbrn8cs4g9ajizgqgbdyhbx92ld2kffx2n")))

(define-public crate-fimdoc-0.6.0 (c (n "fimdoc") (v "0.6.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "pinkie-pie") (r "^0.1.0") (d #t) (k 0)))) (h "0qdcay1lzqcc6m62paqay4b23fi46dlcpn4xbsbs8y7vhxndhzi7")))

(define-public crate-fimdoc-0.6.1 (c (n "fimdoc") (v "0.6.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "golden-oak-library") (r "^0.1.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)))) (h "0bm10i5lqvr6bx8s6qv37nbwzn1b09prgymgq50qx7kqs2bb0ivl")))

