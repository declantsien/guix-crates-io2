(define-module (crates-io fi ga figa-proc) #:use-module (crates-io))

(define-public crate-figa-proc-0.1.0 (c (n "figa-proc") (v "0.1.0") (d (list (d (n "proc-easy") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sivsfchx8d788hvzvml4sd940xhnkbbqvjd11vmrhiplc6ycg8f")))

