(define-module (crates-io fi co ficon) #:use-module (crates-io))

(define-public crate-ficon-0.1.0 (c (n "ficon") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1gq5x678i663s11ri1df4j8i9462cxsimif3fyi77ivp5snm0dax")))

