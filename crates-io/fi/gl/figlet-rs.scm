(define-module (crates-io fi gl figlet-rs) #:use-module (crates-io))

(define-public crate-figlet-rs-0.1.0 (c (n "figlet-rs") (v "0.1.0") (h "080yfcn76v2m3w2l8c2p8q597p1cv55sh4zv9q6lw8pdmnax4mcy")))

(define-public crate-figlet-rs-0.1.1 (c (n "figlet-rs") (v "0.1.1") (h "0cjszjmb8ihq5jl81fwp37ki3pwv1kfhb89pc6qn3jr87ppyxhfg")))

(define-public crate-figlet-rs-0.1.2 (c (n "figlet-rs") (v "0.1.2") (h "0q2gbnbdmwn7gvd8xfhzaskilxfyyvhp0csk4lsdnd3cagzhs1fq")))

(define-public crate-figlet-rs-0.1.3 (c (n "figlet-rs") (v "0.1.3") (h "1ani0adih1v153zv0ly5vpgcjfsppv90q4d36mmp5s93li1ki2ar")))

(define-public crate-figlet-rs-0.1.4 (c (n "figlet-rs") (v "0.1.4") (h "1vb3mqhhvwxfkg05l63wb17fah8vgpayj28553908qjhr17mrbq1")))

(define-public crate-figlet-rs-0.1.5 (c (n "figlet-rs") (v "0.1.5") (h "1igv75b0hbfsjli57vnpk740rm2k7vx0h6psz63gr54nrmqs0hj7")))

