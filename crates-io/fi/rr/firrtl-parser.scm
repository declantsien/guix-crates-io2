(define-module (crates-io fi rr firrtl-parser) #:use-module (crates-io))

(define-public crate-firrtl-parser-0.1.0 (c (n "firrtl-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0jjwnrgr8y5z6vky7p9n590wizdac6ip2y5mqym7w1dlhv2v1nsh")))

