(define-module (crates-io fi bb fibbox) #:use-module (crates-io))

(define-public crate-fibbox-0.1.0 (c (n "fibbox") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "=1.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rug") (r "=1.6.0") (d #t) (k 0)))) (h "1ky41wjdbiq3vk0dx4jn1g95nxaid73gkih966f28f8d507izavj")))

