(define-module (crates-io fi gf figfont) #:use-module (crates-io))

(define-public crate-figfont-0.1.0 (c (n "figfont") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (o #t) (d #t) (k 0)))) (h "0rrbjdh6ln306cs81i3q9c3p4220l7mgvh2bn48nrxfgcq71x4k5") (f (quote (("default" "zip"))))))

(define-public crate-figfont-0.1.1 (c (n "figfont") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "zip") (r "^0.5.12") (o #t) (d #t) (k 0)))) (h "1a77vkh8i2w11c9djy70b6g56308kp4rf11qz0myl52n458np6dr") (f (quote (("default" "zip"))))))

