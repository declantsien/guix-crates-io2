(define-module (crates-io fi ro firo-parser) #:use-module (crates-io))

(define-public crate-firo-parser-0.1.0 (c (n "firo-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "pest") (r "^2.7.6") (f (quote ("pretty-print"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)))) (h "0lmng5m0i39mapmfw2ixclkg70zyhjkw0pnng4aprmxwsz1vbadx")))

