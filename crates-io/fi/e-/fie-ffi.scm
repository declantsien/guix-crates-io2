(define-module (crates-io fi e- fie-ffi) #:use-module (crates-io))

(define-public crate-fie-ffi-0.1.0 (c (n "fie-ffi") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "fie") (r "^0.12") (d #t) (k 0)))) (h "1gvgpsiwx2nqnywgnzwpcad3y4dl48jr3cr239s2kvf8yq17xh62") (f (quote (("header" "cbindgen") ("default" "c") ("c"))))))

(define-public crate-fie-ffi-0.2.0 (c (n "fie-ffi") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "fie") (r "^0.13") (d #t) (k 0)))) (h "1avzw1c4h95mgclp1h6nd7vy3zhmq9y1yvzk7am8rs6wp49mn3f0") (f (quote (("header" "cbindgen") ("default" "c") ("c"))))))

(define-public crate-fie-ffi-0.3.0 (c (n "fie-ffi") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "fie") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-global") (r "^0.4") (d #t) (k 0)))) (h "00mqc4gc03xjx6i4rjlqybqjqk5aff7gw3f033fv7wdp99wj93gs") (f (quote (("header" "cbindgen") ("default" "c") ("c"))))))

