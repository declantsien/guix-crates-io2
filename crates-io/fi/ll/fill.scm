(define-module (crates-io fi ll fill) #:use-module (crates-io))

(define-public crate-fill-0.1.0 (c (n "fill") (v "0.1.0") (h "1aq82lxn6shcapnm2j3l2sagrj4frdp2r5q40fxmfm9fwq2r4gl5") (f (quote (("default") ("alloc"))))))

(define-public crate-fill-0.1.1 (c (n "fill") (v "0.1.1") (h "1jgsngwv1xgm4yq8kdxrrrdka6sg31zjwknl2xqbj15i5ydkiz0w") (f (quote (("default") ("alloc"))))))

