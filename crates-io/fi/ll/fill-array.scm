(define-module (crates-io fi ll fill-array) #:use-module (crates-io))

(define-public crate-fill-array-0.1.0 (c (n "fill-array") (v "0.1.0") (d (list (d (n "macrotest") (r "~1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~2.0") (d #t) (k 0)))) (h "0f7wsq37xwgxiwalh5pimcy8dzcnx0xswa16wsfp0rfccypc2d92")))

(define-public crate-fill-array-0.2.0 (c (n "fill-array") (v "0.2.0") (d (list (d (n "macrotest") (r "~1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~2.0") (d #t) (k 0)))) (h "036iqyyrbpsc4qkzkk2qms31fqj0alpswg3ckz2xyhbbqh3pz6xm")))

(define-public crate-fill-array-0.2.1 (c (n "fill-array") (v "0.2.1") (d (list (d (n "macrotest") (r "~1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~2.0") (d #t) (k 0)))) (h "1g7fp3qvlrls3nawkx2l99j75zdqj5gz7yjmmwi77iwk6iiwynqq")))

