(define-module (crates-io fi ll fill_canvas) #:use-module (crates-io))

(define-public crate-fill_canvas-0.1.0 (c (n "fill_canvas") (v "0.1.0") (h "0z4cyhq065p82agp2rwci2cyrl0p6l544hbm5jx479mpqk9ycq7z")))

(define-public crate-fill_canvas-0.1.1 (c (n "fill_canvas") (v "0.1.1") (h "1dkyzr6mmlz71v6zvk6fdf6yyrwbfd37rdbi4dh8dpn66p3dr8sz")))

