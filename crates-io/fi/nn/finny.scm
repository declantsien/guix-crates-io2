(define-module (crates-io fi nn finny) #:use-module (crates-io))

(define-public crate-finny-0.1.0 (c (n "finny") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4") (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "finny_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1r33vg83gknmllk91vdx8bxggb2n3wx7mvz5mnnr1rl5psr7rs6p") (f (quote (("std" "arraydeque/std") ("default" "std"))))))

(define-public crate-finny-0.1.1 (c (n "finny") (v "0.1.1") (d (list (d (n "arraydeque") (r "^0.4") (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "finny_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0bs5a33w1wsa2s0ryqlmhwm1hbb2pipsxa30600m3vrjqssfsb99") (f (quote (("std" "arraydeque/std") ("default" "std"))))))

(define-public crate-finny-0.2.0 (c (n "finny") (v "0.2.0") (d (list (d (n "arraydeque") (r "^0.4") (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "finny_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (o #t) (k 0)))) (h "1jhvfqdffj64j9r5lh2csi35ls4b6zx1555lirfb160c8pn4r5d6") (f (quote (("timers_std") ("std" "arraydeque/std" "timers_std" "slog/std" "finny_derive/std") ("inspect_slog" "slog") ("generate_plantuml" "finny_derive/generate_plantuml") ("default" "std" "inspect_slog" "timers_std"))))))

