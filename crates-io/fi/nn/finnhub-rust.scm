(define-module (crates-io fi nn finnhub-rust) #:use-module (crates-io))

(define-public crate-finnhub-rust-0.1.0 (c (n "finnhub-rust") (v "0.1.0") (h "0cksj064illrrblqq0a9pjsdppa2pm3gpac8r0ssz22vr061f1qg")))

(define-public crate-finnhub-rust-0.1.1 (c (n "finnhub-rust") (v "0.1.1") (h "1czfawbfgrhw2df8d591rgbrhfkil4vpmw9g7nfq2ykd4k97q37n")))

(define-public crate-finnhub-rust-0.1.2 (c (n "finnhub-rust") (v "0.1.2") (h "05f7mn2wf6m49dky38jncb10rhcqw5y9aknwaf74px3iyqbjjfvz")))

(define-public crate-finnhub-rust-0.1.3 (c (n "finnhub-rust") (v "0.1.3") (h "0lr9pb5jjqzqj7s78xnpx33ixfqawa4q0nx1fzxxi83kfnimmdj3")))

(define-public crate-finnhub-rust-0.1.4 (c (n "finnhub-rust") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "09l4zh43ldimmp1gkxbnfvmcibgz4ds6772mgfdj1mfmhxh5fm3w")))

(define-public crate-finnhub-rust-0.1.5 (c (n "finnhub-rust") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j4zvyqs47skv6v7c35lpg3r8rndvh7jvkji9hc5w3q0jdh5bpbq")))

(define-public crate-finnhub-rust-0.1.6 (c (n "finnhub-rust") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0927l2x0426z47yql9mpzgmqhxdzpzank6ghlb7rig6q1qcjifx7")))

(define-public crate-finnhub-rust-0.1.7 (c (n "finnhub-rust") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1i6jpxi2bz83h49flbfkbh04psxccwnzsf514z80j0gkfb7568yi")))

(define-public crate-finnhub-rust-0.1.8 (c (n "finnhub-rust") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0wy6ffdipp6ny3k0w9396nigcxmlq1xxhrv2l41djz8dizmq9bc0")))

(define-public crate-finnhub-rust-0.1.9 (c (n "finnhub-rust") (v "0.1.9") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ibwhwbmxz28spfqfap0cw1baf35kb5hqyzq20g5faaiqn1dfwsy")))

