(define-module (crates-io fi nn finny_derive) #:use-module (crates-io))

(define-public crate-finny_derive-0.1.0 (c (n "finny_derive") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0ygqfclcisaqnmclxamvi6bbr4x2hc55bgyw2rkd09c4n3knqv3d")))

(define-public crate-finny_derive-0.2.0 (c (n "finny_derive") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "17ypdvb17hzdflpyr55v3xr0im55aak4ziz3wjcibsv3cxf8pyfv") (f (quote (("std") ("generate_plantuml") ("default" "std"))))))

