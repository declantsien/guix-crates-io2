(define-module (crates-io fi nn finn-assembler) #:use-module (crates-io))

(define-public crate-finn-assembler-0.1.0 (c (n "finn-assembler") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0yfr3h9gj4l5i1g4kbv30c87pyls1ggn3ha5r4749adp87p256qr")))

(define-public crate-finn-assembler-0.1.1 (c (n "finn-assembler") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0lvjhj43hx1l5dfr45rwfrxbp8vwzjcri1s9kwcviwqsy6hd27cq")))

(define-public crate-finn-assembler-0.1.2 (c (n "finn-assembler") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "125mwdbcrsrhqx08gnv4bryfbnk1rgvc3fz1n794nabzzi785js5")))

(define-public crate-finn-assembler-0.1.3 (c (n "finn-assembler") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0m18q058l7axn0q1c97sa32y1cwyy1h7w3nxhjr1hdl5h3ccc9s6")))

(define-public crate-finn-assembler-0.1.4 (c (n "finn-assembler") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "1x5hwc7bw2wajkazv85jmyz2d17p7fgp8gzc3q5i1c20h0srj6w1")))

(define-public crate-finn-assembler-0.1.5 (c (n "finn-assembler") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0wpkf9dckymssv6ywl0fdxmlr20npn9yrrplhpzfnblwy2621ld6")))

(define-public crate-finn-assembler-0.2.0 (c (n "finn-assembler") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "1byn3dx7a95l0qfy12ny6cakhfvnq5j3fib70bkvpxd5ci9x5fhd")))

(define-public crate-finn-assembler-0.2.1 (c (n "finn-assembler") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "0q0rq9pzzprs2hzyl439564zjpy1fzlrq85fxh3y65drfpkdj1m5")))

(define-public crate-finn-assembler-0.2.2 (c (n "finn-assembler") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 0)))) (h "13hmmvwp2v2wlz2s01yfkv0h069cv5jml7024az5ji2brqdjwk8x")))

