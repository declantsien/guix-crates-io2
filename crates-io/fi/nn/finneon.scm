(define-module (crates-io fi nn finneon) #:use-module (crates-io))

(define-public crate-finneon-0.1.0 (c (n "finneon") (v "0.1.0") (d (list (d (n "glam") (r "^0.25.0") (f (quote ("core-simd"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "0m1mxlr1cvvhqgdxqx23ccbmlxiirb0w1s28r6dp0a3p5dphzmzd")))

(define-public crate-finneon-0.1.1 (c (n "finneon") (v "0.1.1") (d (list (d (n "glam") (r "^0.25.0") (f (quote ("core-simd"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1j576rx6n8brrmlwqxxkykbh58zxswxi23ss0zbz2hc30dkqf3pm")))

(define-public crate-finneon-0.1.2 (c (n "finneon") (v "0.1.2") (d (list (d (n "glam") (r "^0.25.0") (f (quote ("core-simd"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "01bwijk344smdk4z61r6nfzv8wrbjb9znsw13caf8rn6nhq48bql")))

(define-public crate-finneon-0.1.3 (c (n "finneon") (v "0.1.3") (d (list (d (n "glam") (r "^0.25.0") (f (quote ("core-simd"))) (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1kmyp8ap3m4b33a2nbqm650ky024wgi5rdizk1bvbsik72s8jgrh")))

