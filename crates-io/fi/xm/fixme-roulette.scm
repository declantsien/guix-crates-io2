(define-module (crates-io fi xm fixme-roulette) #:use-module (crates-io))

(define-public crate-fixme-roulette-0.1.0 (c (n "fixme-roulette") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0941mbhs6yx75vxgpnfarv4hxg0sqsdc254rgx3bihhliyxjd6i9")))

(define-public crate-fixme-roulette-0.1.1 (c (n "fixme-roulette") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1zz82plx1gp9l03bj4sglggpqgxypxff5qcv06iwc3h0qby2gb5n")))

(define-public crate-fixme-roulette-0.1.2 (c (n "fixme-roulette") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1zasgvygwyjhpvg1crwsj1m1h1j3sbg7cxma5i9rzbfp9kkc1z10")))

(define-public crate-fixme-roulette-0.1.3 (c (n "fixme-roulette") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1a0l01i1py8zr0zjnib76awxrmxkw05r1g77pzb247ybixwikihx")))

(define-public crate-fixme-roulette-0.1.4 (c (n "fixme-roulette") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0a1gkw3rdhbqqx37izyyy4lvrqh5c3abynj4h7c1f5xjfvmdwbyy")))

(define-public crate-fixme-roulette-0.1.5 (c (n "fixme-roulette") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1v5b3msng8dqig734g7dcxg113y0alcxvxvdpz5vpnrysqja8laq")))

(define-public crate-fixme-roulette-0.1.6 (c (n "fixme-roulette") (v "0.1.6") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1wgls2rw1vq9wxsm8nq79bnjsi2qck2wwbk5sspjfyyxwmb06d78")))

(define-public crate-fixme-roulette-0.1.65 (c (n "fixme-roulette") (v "0.1.65") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0kwmpjycz8xmrq46dcl1jn6js1r0dfd5lmsf9cdbqqkfaa82070f")))

(define-public crate-fixme-roulette-0.1.66 (c (n "fixme-roulette") (v "0.1.66") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0nld6gh455a9a5pll9nn9mzpdmbd62zs4w5p1jpdhzxp1dn002h5")))

(define-public crate-fixme-roulette-0.1.67 (c (n "fixme-roulette") (v "0.1.67") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0v94j0h50mf0ihxc43z5g0p0lwhlq252vnngyrsi8zlk89cfy0lq")))

(define-public crate-fixme-roulette-0.1.7 (c (n "fixme-roulette") (v "0.1.7") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1xi4391x33h30dcdg4kljsz2w1ars6wjp9ca98k76pnsg9fg555i")))

(define-public crate-fixme-roulette-0.1.71 (c (n "fixme-roulette") (v "0.1.71") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "144dviappg643czazi99pzgcwvjfsx3cznyq42vjrmkqzq6886v5")))

(define-public crate-fixme-roulette-0.1.72 (c (n "fixme-roulette") (v "0.1.72") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0mcrbrmv38cz6drpc0ll84vy5id6lyf6zlx2q7nnna4lic91ca4z")))

(define-public crate-fixme-roulette-0.1.8 (c (n "fixme-roulette") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "198zl1rawzy6yk8smi2p18qq4cc6lc0bc8qfwpr8y03xlkgggj1c")))

(define-public crate-fixme-roulette-0.1.81 (c (n "fixme-roulette") (v "0.1.81") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "182xn38wpz2zspvlf8gx8l92k42gb360ik376m4rzn6y1gy05gi8")))

(define-public crate-fixme-roulette-0.1.9 (c (n "fixme-roulette") (v "0.1.9") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "154nzqhndiq92cbc7gsasgchidhfc3k596g9ayq6h0vgq5n2l060")))

(define-public crate-fixme-roulette-0.2.0 (c (n "fixme-roulette") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0f733ynzxf4yj4zsp5n52j3fplp8fy7frj2c3jcxphc8dfql4wdp")))

(define-public crate-fixme-roulette-0.2.1 (c (n "fixme-roulette") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1sw2cn0iaqil5w8a4bk59sv6lmbvd486r8h269ihc4cf7mab9zd8")))

