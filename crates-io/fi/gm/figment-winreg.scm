(define-module (crates-io fi gm figment-winreg) #:use-module (crates-io))

(define-public crate-figment-winreg-0.1.1 (c (n "figment-winreg") (v "0.1.1") (d (list (d (n "figment") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "windows") (r "^0.24") (f (quote ("Win32_Foundation" "Win32_System_Environment"))) (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (k 0)))) (h "0p3y3jir3z3irw6754wwrg2h7jhg9f7wiqmnp3hjxsff10inwj31")))

(define-public crate-figment-winreg-0.2.1 (c (n "figment-winreg") (v "0.2.1") (d (list (d (n "figment") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "windows") (r "^0.26") (f (quote ("std" "Win32_Foundation" "Win32_System_Environment"))) (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (k 0)))) (h "1mrkajqx61f79v14r0bw374rfjg6z8vjm8wabqxk8na2znpfa4zs")))

(define-public crate-figment-winreg-0.2.2 (c (n "figment-winreg") (v "0.2.2") (d (list (d (n "figment") (r "^0.10.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Environment"))) (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (k 0)))) (h "0cs0xlcwlhrrs87mdsrg0ljv6b118cjr99bpr45sg21104z0gplx")))

