(define-module (crates-io fi gm figment-directory) #:use-module (crates-io))

(define-public crate-figment-directory-0.1.0 (c (n "figment-directory") (v "0.1.0") (d (list (d (n "figment") (r "^0.10.19") (d #t) (k 0)) (d (n "figment") (r "^0.10.19") (f (quote ("toml" "test"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "13bfg0ggkwrh6hwyf4ijymp0hm4zpgzkb89cf258mnxasin0b513")))

