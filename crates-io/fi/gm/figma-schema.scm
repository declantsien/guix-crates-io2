(define-module (crates-io fi gm figma-schema) #:use-module (crates-io))

(define-public crate-figma-schema-0.2.0 (c (n "figma-schema") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typeshare") (r "^1.0.1") (d #t) (k 0)))) (h "07yzqf4mgccsk9d8ibxlpns8m598n1i8scpj02206ppd1bfrq369")))

(define-public crate-figma-schema-0.3.0 (c (n "figma-schema") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typeshare") (r "^1.0.1") (d #t) (k 0)))) (h "1fvf426fbmpddwv973l4pqcj6id9gscb2si0xcql81rj1jkmaywl")))

