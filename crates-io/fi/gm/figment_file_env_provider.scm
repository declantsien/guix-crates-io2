(define-module (crates-io fi gm figment_file_env_provider) #:use-module (crates-io))

(define-public crate-figment_file_env_provider-0.1.0 (c (n "figment_file_env_provider") (v "0.1.0") (d (list (d (n "figment") (r "^0.10") (f (quote ("env" "test"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "10kgy6vrpq51b5xd9c93zdfzr5gr59cicx16a3hn28296zgwzzfg")))

(define-public crate-figment_file_env_provider-0.2.0 (c (n "figment_file_env_provider") (v "0.2.0") (d (list (d (n "figment") (r "^0.10") (f (quote ("env" "test"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0af1cwz0b7yjzv74cmd8a41dhmnhhv7sm3sb3kn1n0vpyp1y95sn")))

