(define-module (crates-io fi gm figment_string) #:use-module (crates-io))

(define-public crate-figment_string-0.1.0 (c (n "figment_string") (v "0.1.0") (d (list (d (n "figment") (r "^0.10") (f (quote ("env"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "1i9s3qcrbylw1l5g714i64yrz6859ccq2xpbrrmajz9wsjxil1cn")))

