(define-module (crates-io fi gm figma-html) #:use-module (crates-io))

(define-public crate-figma-html-0.1.0 (c (n "figma-html") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "figma-schema") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lightningcss") (r "^1.0.0-alpha.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nmmbnja9lh4a3mxib10p5crfpigw17l1nk952h4ka1ymnk3lkjx")))

