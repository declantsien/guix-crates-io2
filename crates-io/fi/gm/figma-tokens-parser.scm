(define-module (crates-io fi gm figma-tokens-parser) #:use-module (crates-io))

(define-public crate-figma-tokens-parser-0.2.3 (c (n "figma-tokens-parser") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0lasc5iy7dghpl3x5j19amb18sgpc91d4cz61lkw329b5nmjykh4")))

