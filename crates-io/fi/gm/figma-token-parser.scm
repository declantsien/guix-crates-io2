(define-module (crates-io fi gm figma-token-parser) #:use-module (crates-io))

(define-public crate-figma-token-parser-0.2.4 (c (n "figma-token-parser") (v "0.2.4") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "181bjy1g5j4q32mf6z5dvq3hl5spyyhgr11jjs3ichknk7nrfrsp")))

