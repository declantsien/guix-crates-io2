(define-module (crates-io fi gm figment_file_provider_adapter) #:use-module (crates-io))

(define-public crate-figment_file_provider_adapter-0.1.0 (c (n "figment_file_provider_adapter") (v "0.1.0") (d (list (d (n "figment") (r "^0.10") (f (quote ("parse-value"))) (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("env" "test" "toml"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0045k03f98rjp08qj5v8ljl9cr36jjx75061i7yv3ffv9x10ccf3")))

(define-public crate-figment_file_provider_adapter-0.1.1 (c (n "figment_file_provider_adapter") (v "0.1.1") (d (list (d (n "figment") (r "^0.10, >=0.10.12") (f (quote ("parse-value"))) (d #t) (k 0)) (d (n "figment") (r "^0.10, >=0.10.12") (f (quote ("env" "test" "toml"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0553h2yf5nlyhnhkywscmzaq8wih1njhi51h50vzlzkrjq41wgim")))

