(define-module (crates-io fi el fieldx_derive_support) #:use-module (crates-io))

(define-public crate-fieldx_derive_support-0.1.0 (c (n "fieldx_derive_support") (v "0.1.0") (d (list (d (n "darling") (r ">=0.20.7") (f (quote ("suggestions"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0yqhxjalkzkvqc1yadlhicpxkmg1rndj0wclw11dgy1id299s1yf")))

