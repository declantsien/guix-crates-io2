(define-module (crates-io fi el fields-converter-derive) #:use-module (crates-io))

(define-public crate-fields-converter-derive-0.1.0 (c (n "fields-converter-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "037nsfq95xw13rypixbz11c4lplzb118007r07xv2fr662g13vhy")))

(define-public crate-fields-converter-derive-0.1.1 (c (n "fields-converter-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0w8mk0cyfl19yz6g2x34k7v76g2hbl48an5v51jysxdj1nf5r3yi") (y #t)))

(define-public crate-fields-converter-derive-0.1.2 (c (n "fields-converter-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0xw829007bq06q4qyjf3n10w4spjvnx3lv49hxkll186ggxsack6")))

(define-public crate-fields-converter-derive-0.1.3 (c (n "fields-converter-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1lz158rfpz9l6zwrgy4rvwcq74qbs5rmb9azmhqb692jpdg2nii0")))

(define-public crate-fields-converter-derive-0.1.4 (c (n "fields-converter-derive") (v "0.1.4") (d (list (d (n "clone-fields") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0fnaxdxr2709fkb0xcs61w4v5k0ligkbvf7qh651v3chglw3gcmm")))

