(define-module (crates-io fi el fieldmask_derive) #:use-module (crates-io))

(define-public crate-fieldmask_derive-0.0.1 (c (n "fieldmask_derive") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (d #t) (k 0)))) (h "0xsfp0ij6r2cwfvyh9gjysdfzsibjh6hhb8ff41ds21bp5xpx9n9") (f (quote (("prost"))))))

(define-public crate-fieldmask_derive-0.0.2 (c (n "fieldmask_derive") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1z5xzb2q485mb5x9f9cpy2zfmpanb27yr218flwniz65cx4ww994") (f (quote (("prost"))))))

(define-public crate-fieldmask_derive-0.0.3 (c (n "fieldmask_derive") (v "0.0.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06bz886z8k6csp5h3gskv1ckzfgafwi31m96k5rdzzcfb6a7zk2r") (f (quote (("prost"))))))

