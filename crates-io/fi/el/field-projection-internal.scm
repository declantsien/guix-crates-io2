(define-module (crates-io fi el field-projection-internal) #:use-module (crates-io))

(define-public crate-field-projection-internal-0.1.0 (c (n "field-projection-internal") (v "0.1.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0y3a1p1qv724pnlgi2mifnp20iclvqqqfq3n677r18s4b4fgflg6")))

(define-public crate-field-projection-internal-0.2.0 (c (n "field-projection-internal") (v "0.2.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0na1bgybs58apf2fs7bmahgg89640x4glzivqibh6d44x5j3v68f")))

