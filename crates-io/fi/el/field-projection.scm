(define-module (crates-io fi el field-projection) #:use-module (crates-io))

(define-public crate-field-projection-0.1.0 (c (n "field-projection") (v "0.1.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "field-projection-internal") (r "=0.1.0") (d #t) (k 0)))) (h "0vqs00hqhvgklk36qlv6yshgzcxr2dmh8gnyhbqls63bl2cvnrzl")))

(define-public crate-field-projection-0.2.0 (c (n "field-projection") (v "0.2.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "field-projection-internal") (r "=0.2.0") (d #t) (k 0)))) (h "1m6mgiy3vf61yhaqzql9rrfilzgf49ll217dhgp0mn8cni3b5wy6")))

