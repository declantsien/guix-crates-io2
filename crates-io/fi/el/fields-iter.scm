(define-module (crates-io fi el fields-iter) #:use-module (crates-io))

(define-public crate-fields-iter-0.0.1 (c (n "fields-iter") (v "0.0.1") (d (list (d (n "fields-iter-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0qcxslrhd3c9ndciylkii0ci7lmhbis294kfgbfyvcxm6xa2bz31")))

(define-public crate-fields-iter-0.0.2 (c (n "fields-iter") (v "0.0.2") (d (list (d (n "fields-iter-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "080dlnk52hyz6dg66dkjjb8wijw7pbqx2hm89bsfaf1pwfpl8cjv")))

(define-public crate-fields-iter-0.0.3 (c (n "fields-iter") (v "0.0.3") (d (list (d (n "fields-iter-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0jw8zccx9szw9mvawja869fwjqlrk71sk6wkrvg7kx22yz0bvpry")))

