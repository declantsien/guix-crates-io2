(define-module (crates-io fi el field_block) #:use-module (crates-io))

(define-public crate-field_block-0.1.0 (c (n "field_block") (v "0.1.0") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "0l601kj2sxv7k9mfq55586px725vr96lkfjlxjgpvym3wwc8mv1w") (y #t)))

(define-public crate-field_block-0.1.1 (c (n "field_block") (v "0.1.1") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "1x932kiqs5x9wj5k2w571114shmrcxw0qgqmbfygv0xmss6wwwds") (y #t)))

(define-public crate-field_block-0.1.2 (c (n "field_block") (v "0.1.2") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "0mzarf4vpknba73am7hsbnahjh1yza25xijan9phgn1pp0scbd6b") (y #t)))

(define-public crate-field_block-0.1.3 (c (n "field_block") (v "0.1.3") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "0xf5pk36rnwgpwjq48px1x7dj76rv644qlq45grfrgdr87q7h8rp") (y #t)))

(define-public crate-field_block-0.1.4 (c (n "field_block") (v "0.1.4") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "175jz3y26j3ai06w8x87zcn7fmic717wiy7hhmx1nkcjzhziy4a5")))

(define-public crate-field_block-0.2.0 (c (n "field_block") (v "0.2.0") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "1jnznvyi3285674rq5lpiykzmljb7lcx1mjmmj8f96183ixhgf3k")))

(define-public crate-field_block-0.3.0 (c (n "field_block") (v "0.3.0") (d (list (d (n "octets") (r "^0.2.0") (d #t) (k 0)))) (h "1mv916ndrcn2cp45ygizfpv5w19axwaphqj3j4hq0kj3fvldhpv5")))

