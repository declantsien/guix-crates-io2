(define-module (crates-io fi el fieldless_enum_tools_impl) #:use-module (crates-io))

(define-public crate-fieldless_enum_tools_impl-0.1.0 (c (n "fieldless_enum_tools_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bzc87bmlmvanfy6nrx9q6ji5dvcdh9ayv7471ymayscqargc5hy") (r "1.37")))

(define-public crate-fieldless_enum_tools_impl-0.2.0 (c (n "fieldless_enum_tools_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wqm5acybbdqkmbsqigv57fc5ik6k38qm004pi6jzd19n9ymikh1") (r "1.56")))

