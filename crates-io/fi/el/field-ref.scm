(define-module (crates-io fi el field-ref) #:use-module (crates-io))

(define-public crate-field-ref-0.1.0 (c (n "field-ref") (v "0.1.0") (h "0g2y59cbglpf3a37n9fnf9j9iya21l8k3n70s71mnm4r11vpk9rm")))

(define-public crate-field-ref-0.1.1 (c (n "field-ref") (v "0.1.1") (h "09appyayikqnv6jsbdvrs72d0ckj7np8qpixghrxp9b73fasvfgs")))

(define-public crate-field-ref-0.2.0 (c (n "field-ref") (v "0.2.0") (h "13kmdigib6dx7p7agscjf0zkgifni9fblhdwvqppmkv35dql6gqa")))

(define-public crate-field-ref-0.2.1 (c (n "field-ref") (v "0.2.1") (h "08l8fkgi319a2kiygyqz9s5lv2ixq79p8ms9cc2m7hyaa9spbal1")))

