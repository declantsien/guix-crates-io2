(define-module (crates-io fi el field_types) #:use-module (crates-io))

(define-public crate-field_types-1.0.1 (c (n "field_types") (v "1.0.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0hbl669rbr9zjaiysbri15gl7a3fag0wmjb6lvlf9fhbbgkrf1hm")))

(define-public crate-field_types-1.0.2 (c (n "field_types") (v "1.0.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07ff5sl3fjnmzpsmdicf9mqpngf5z72kjgmn4k1vy5pv1sf4q4fx")))

(define-public crate-field_types-1.0.3 (c (n "field_types") (v "1.0.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "variant_count") (r "^1.0") (d #t) (k 2)))) (h "0s0id30m9fg8pg0ax1f8nr7aa2adaph4yxwzq99c4v543paw56hy")))

(define-public crate-field_types-1.1.0 (c (n "field_types") (v "1.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "variant_count") (r "^1.0") (d #t) (k 2)))) (h "16hwcpj3hs7fxgyj00v1rcj2gsc4rnhj0r376wbvj0cci94z8bqm")))

