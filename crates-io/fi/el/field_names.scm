(define-module (crates-io fi el field_names) #:use-module (crates-io))

(define-public crate-field_names-0.1.0 (c (n "field_names") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1hchyw0q6sq77m9bnybapb73kdssfxqnxh3m0cafws03py038899")))

(define-public crate-field_names-0.1.1 (c (n "field_names") (v "0.1.1") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "0l34aplpmq8fcli4al782a72j4rb31h4a8fg2zazgrwzlwrxn9v3")))

(define-public crate-field_names-0.2.0 (c (n "field_names") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "01s8ddr7mmqpp04qszy750mm9ylwgzih44p5gm72fzlv3fmzv96c")))

