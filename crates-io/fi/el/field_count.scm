(define-module (crates-io fi el field_count) #:use-module (crates-io))

(define-public crate-field_count-0.1.0 (c (n "field_count") (v "0.1.0") (d (list (d (n "field_count_derive") (r "^0.1") (d #t) (k 0)))) (h "0z2xvq21wqajl3268h1jhnvxmpyiwadrcrsks3agyraslmm1c677")))

(define-public crate-field_count-0.1.1 (c (n "field_count") (v "0.1.1") (d (list (d (n "field_count_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1ixdvar41m9qy0k16ay7pyfm6hwslvplm8mwjh8g0k2pvn2myk98")))

