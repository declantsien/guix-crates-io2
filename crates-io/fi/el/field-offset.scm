(define-module (crates-io fi el field-offset) #:use-module (crates-io))

(define-public crate-field-offset-0.1.0 (c (n "field-offset") (v "0.1.0") (h "1v3mpw7nqbihb3gj5sjbm6i1f5rfxxwwfdnp1cdfnq05a0ik7n2x")))

(define-public crate-field-offset-0.1.1 (c (n "field-offset") (v "0.1.1") (h "0s4ybhxadhkyd40vqn0vwyp94sp9078nkyh14vh3jqa2kqrvrsb4")))

(define-public crate-field-offset-0.2.0 (c (n "field-offset") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0cy8i9v0l70xym6wvwckznip22vb7m89rvk932vdmv0w4mi1zrm6")))

(define-public crate-field-offset-0.3.0 (c (n "field-offset") (v "0.3.0") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "02sglifadw5wl6q1d5b3qsg1phxnm35265cvbpdvcnxkjp0yp5pc")))

(define-public crate-field-offset-0.3.1 (c (n "field-offset") (v "0.3.1") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0hnsbd2rwb7vzklqkiwc520bv2m85ifliws7219q06pcgbvvb96r")))

(define-public crate-field-offset-0.3.2 (c (n "field-offset") (v "0.3.2") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1m10rd9vgal4y1dni6yqw7qaj22xvn3s4cipg1jcs8hx9is7l3n4")))

(define-public crate-field-offset-0.3.3 (c (n "field-offset") (v "0.3.3") (d (list (d (n "memoffset") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "1ryxns61afx6b1w80dccw4pa264mccqdmq121bs50sq5f2x9ylyg")))

(define-public crate-field-offset-0.3.4 (c (n "field-offset") (v "0.3.4") (d (list (d (n "memoffset") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "14pvxz7yr0r3mlhaf65zva3r9d0kqid01wdw7ngx7jsh2jam870y")))

(define-public crate-field-offset-0.3.5 (c (n "field-offset") (v "0.3.5") (d (list (d (n "memoffset") (r "^0.8") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0dc58qzbfis0i031d741aavvi7crzlb4nvfacg461s7n1y03mkx3")))

(define-public crate-field-offset-0.3.6 (c (n "field-offset") (v "0.3.6") (d (list (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0zq5sssaa2ckmcmxxbly8qgz3sxpb8g1lwv90sdh1z74qif2gqiq")))

