(define-module (crates-io fi el field_accessor) #:use-module (crates-io))

(define-public crate-field_accessor-0.1.0 (c (n "field_accessor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01c384378akdx8fwvypi6i5w71fgrb76mksqirfwixhbnibvv7nj")))

(define-public crate-field_accessor-0.2.0 (c (n "field_accessor") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iih2sld5y0ddfnaxrqj08jz9iqrgz2shnlsn6fjssyn9v1il8lw")))

(define-public crate-field_accessor-0.3.0 (c (n "field_accessor") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "174xfm1z81d584j37jsr7aki77a60pbskg7rc61i70qgbsf2f55w")))

(define-public crate-field_accessor-0.4.0 (c (n "field_accessor") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04ny63m2rwf579xy6ydihs06b24j1j78pz4wmip24dkb6p3df4gf")))

(define-public crate-field_accessor-0.4.1 (c (n "field_accessor") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g53jlfbamcvzw5vfyb2q3a7rmdks4j1ys1lx5xirz155qfwwwpj")))

(define-public crate-field_accessor-0.4.2 (c (n "field_accessor") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pz1bp1pzm7kczya1ryrn8kyijig0iqss2bdzhdlz45k59cnna24")))

(define-public crate-field_accessor-0.4.3 (c (n "field_accessor") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nqpg6n7dbf319b39bqfpikjrnsiay51yn8c9mw1h1a1vlmvcpfm")))

(define-public crate-field_accessor-0.5.0 (c (n "field_accessor") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cjgv7blxafa3jrzx5f48dvzf2vbn2q3lzmabjxfp17lnj7kzf0b")))

(define-public crate-field_accessor-0.5.1 (c (n "field_accessor") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sc80cyg6vcdjhf3qd4p2ak6ibdxyrhp131n66w758qqpvki8y0k")))

(define-public crate-field_accessor-0.5.2 (c (n "field_accessor") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y6p3k68g1xh50byxqy0vzkpifnil3n9x3vi2wj46z9vmliwb59i")))

