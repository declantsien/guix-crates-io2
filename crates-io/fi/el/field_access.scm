(define-module (crates-io fi el field_access) #:use-module (crates-io))

(define-public crate-field_access-0.0.1 (c (n "field_access") (v "0.0.1") (d (list (d (n "field_access_derive") (r "=0.0.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "16xhg0walyyldkha5lkzj6yi6pcw4kiy422g0n07yahs0dac6rwb") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.0 (c (n "field_access") (v "0.1.0") (d (list (d (n "field_access_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qz2scs0qhk8559lamrkdj8h5d962vvrrlqzglc8hpwlm0nyyqyn") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.1 (c (n "field_access") (v "0.1.1") (d (list (d (n "field_access_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "12ah3jx24jqwrq74ijs0navl7ncc1yigq8nb0ndc16lvaw94bmk6") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.2 (c (n "field_access") (v "0.1.2") (d (list (d (n "field_access_derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "15xfjxcgl1q31klj2q6l0gkyyazfk4c4879rgshl8f7nfyj12p09") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.3 (c (n "field_access") (v "0.1.3") (d (list (d (n "field_access_derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.87") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hjvifg3ff8w1a7xfgqx5m4663rpzih15k3y8bn0n97nrlg4b2lv") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.4 (c (n "field_access") (v "0.1.4") (d (list (d (n "field_access_derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "15gm3b08cxmyrajnxa16q6n4k4j9hm72135dlpbgq920wlmxjr49") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.5 (c (n "field_access") (v "0.1.5") (d (list (d (n "field_access_derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0n9x99f9zlll573jdyp8i85n06jzv77z34jxla9n7nc9583z7qmi") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.6 (c (n "field_access") (v "0.1.6") (d (list (d (n "field_access_derive") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (f (quote ("diff"))) (d #t) (k 2)))) (h "143wnmqzh3kk5g797jyapv9j1rv4kcvhx4z7c8pm77nmp04xlkrc") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

(define-public crate-field_access-0.1.7 (c (n "field_access") (v "0.1.7") (d (list (d (n "field_access_derive") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (f (quote ("diff"))) (d #t) (k 2)))) (h "04v000p4qjvmf1y14j9r4hmkml0bwd9svisi9x7my7mldwpv15xi") (f (quote (("derive" "field_access_derive") ("default" "alloc" "derive") ("alloc")))) (r "1.65.0")))

