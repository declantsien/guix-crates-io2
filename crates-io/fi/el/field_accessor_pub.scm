(define-module (crates-io fi el field_accessor_pub) #:use-module (crates-io))

(define-public crate-field_accessor_pub-0.5.2 (c (n "field_accessor_pub") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qx67vjsaf2wcc9lmgw6nlksnx0nr08mxdmzcpzss3rbv2fzqk13")))

(define-public crate-field_accessor_pub-0.5.3 (c (n "field_accessor_pub") (v "0.5.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06n8x3ijgl80pxa3x4krdf8an77aw4a95cfm7mynwb56bir8ykn9")))

