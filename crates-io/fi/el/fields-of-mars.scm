(define-module (crates-io fi el fields-of-mars) #:use-module (crates-io))

(define-public crate-fields-of-mars-1.0.0 (c (n "fields-of-mars") (v "1.0.0") (d (list (d (n "astroport") (r "^1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16") (d #t) (k 0)) (d (n "cw-asset") (r "^1.0") (f (quote ("legacy"))) (d #t) (k 0)) (d (n "cw20") (r "^0.9") (d #t) (k 0)) (d (n "mars-core") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1hi6d6qnjkvh5a3swfgs6zkb9mll29havckadas6psmpbnp4hrdb")))

