(define-module (crates-io fi el field_access_derive) #:use-module (crates-io))

(define-public crate-field_access_derive-0.0.1 (c (n "field_access_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "11mzlp4ivd5hjwlb0n2man31xjj054kbv6jd994315swk47479ps") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.0 (c (n "field_access_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1fl8wik771dxzh4f69fkg4q8nhbf2503j7nbd87i6kmrh7hy42zb") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.1 (c (n "field_access_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "11r5kbckggb3dqjisdl90fx87cdav8m4wh5aaiwr5ai5kw9m0rhp") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.2 (c (n "field_access_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0ycg4bb91bmmgyb6s09micjzvspz0x8wdqnrwcwzac1sjb0pc2xh") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.3 (c (n "field_access_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0wvwhli03k4bmzsrcqs2vr2swv7hw97arlyjhl7fx8xk20gznpa2") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.4 (c (n "field_access_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0mh7wkar2ny4lyd2klrln7sbpbdvcmly95v0rw1ghqmmg22dax74") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.5 (c (n "field_access_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0j6nrfdj8d8a8kv6a0avixb4zyfgq1zrccs2xalx0wnkfazman5f") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.6 (c (n "field_access_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "0gbldg6f5as2j9bqr3jb2ki3jqp5sg6ndlqkwc2h7m6xg4wm30ig") (r "1.65.0")))

(define-public crate-field_access_derive-0.1.7 (c (n "field_access_derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0sp45d3qflix1918mrdsbfabfxwy3xf7y7q29kvw2jshib0a8v6a") (r "1.65.0")))

