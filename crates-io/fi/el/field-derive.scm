(define-module (crates-io fi el field-derive) #:use-module (crates-io))

(define-public crate-field-derive-0.6.0 (c (n "field-derive") (v "0.6.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0i8wggkffxjzmxxs871jniwf13whcc48k90d41kr0rm29wdyzxyk")))

(define-public crate-field-derive-0.6.1 (c (n "field-derive") (v "0.6.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1i4wpx3b1qp7319i86hkd5k47y0n7j24j76ih1j16qw90ndv5gms")))

