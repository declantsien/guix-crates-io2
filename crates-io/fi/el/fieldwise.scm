(define-module (crates-io fi el fieldwise) #:use-module (crates-io))

(define-public crate-fieldwise-0.1.0 (c (n "fieldwise") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r9c0bwmwcspkwnsqyhbd6z2m639zk9wim820dbcbfix3gxpssh0")))

