(define-module (crates-io fi el field_count_derive) #:use-module (crates-io))

(define-public crate-field_count_derive-0.1.0 (c (n "field_count_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r47sy0kjzqyh8hz1a8l0c5bcb931k7d17v798v6icq06qva20gj")))

(define-public crate-field_count_derive-0.1.1 (c (n "field_count_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a26nqpdipn5jbcpqs97jvr5bnff3bdqr3m3qsn1q71vzxq0jcn1")))

