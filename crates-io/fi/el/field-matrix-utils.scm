(define-module (crates-io fi el field-matrix-utils) #:use-module (crates-io))

(define-public crate-field-matrix-utils-0.1.0 (c (n "field-matrix-utils") (v "0.1.0") (d (list (d (n "ark-ff") (r "^0.4.1") (d #t) (k 0)))) (h "1qqrifwzc5fwj34j4n33khjjzzi2jhmb69nmyaq4sxd5k9mfp7kx")))

(define-public crate-field-matrix-utils-0.1.1 (c (n "field-matrix-utils") (v "0.1.1") (d (list (d (n "ark-ff") (r "^0.4.1") (d #t) (k 0)))) (h "03l473rm0hag90xqpscn94ik6y656d1lmdj8by4ifkq93mminz69")))

