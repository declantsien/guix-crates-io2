(define-module (crates-io fi el fieldfilter-derive) #:use-module (crates-io))

(define-public crate-fieldfilter-derive-0.1.0 (c (n "fieldfilter-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0zklgrasacbm9wfcbyy3k0l9mcrdw4n7x8mmsv7qg7p7yayjp4fr")))

