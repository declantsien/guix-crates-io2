(define-module (crates-io fi el fieldmask) #:use-module (crates-io))

(define-public crate-fieldmask-0.0.1 (c (n "fieldmask") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fieldmask_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0akwyfi9xwlaw3i0rhhyzm36s7inlic50sc674cir48wkk4ld7gv") (f (quote (("prost-integration" "prost" "fieldmask_derive/prost"))))))

(define-public crate-fieldmask-0.0.2 (c (n "fieldmask") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fieldmask_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0x6ha6bm5rr0wnphxsb385jwvlmw5nlzgkxa86rp97aws1nschpx") (f (quote (("prost-integration" "prost" "fieldmask_derive/prost"))))))

(define-public crate-fieldmask-0.0.3 (c (n "fieldmask") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fieldmask_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1xxsiaif2ld38bhjvdnfys91sdndlw438m2p30lqgbya4vd22ibr") (f (quote (("prost-integration" "prost" "fieldmask_derive/prost"))))))

(define-public crate-fieldmask-0.0.4 (c (n "fieldmask") (v "0.0.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fieldmask_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "prost") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "15icvgi0rg2wq1nwdzwvflf7srlvqj76b6fa1iw8iqlrqp4nn6k3") (f (quote (("prost-integration" "prost" "fieldmask_derive/prost"))))))

