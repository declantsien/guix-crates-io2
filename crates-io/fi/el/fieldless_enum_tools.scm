(define-module (crates-io fi el fieldless_enum_tools) #:use-module (crates-io))

(define-public crate-fieldless_enum_tools-0.1.0 (c (n "fieldless_enum_tools") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fieldless_enum_tools_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pdr6jm7wdhqlvqbichvn4xgmvcdnkfjjihk98r0n3pxzhzhx0i8") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.37")))

(define-public crate-fieldless_enum_tools-0.2.0 (c (n "fieldless_enum_tools") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fieldless_enum_tools_impl") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fxvamq1s8mi17fcimcavpjg3ciy3akvvlrpqm1i7cdd7jzkpavf") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.56")))

