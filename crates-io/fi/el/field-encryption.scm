(define-module (crates-io fi el field-encryption) #:use-module (crates-io))

(define-public crate-field-encryption-0.0.0 (c (n "field-encryption") (v "0.0.0") (d (list (d (n "generic-array") (r "^0.14.6") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "regex-automata") (r "^0.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r5hzmxn4nzbsj8dik5y3v245rcsy5f5h7gfq8sil1r96l5j8a4w")))

(define-public crate-field-encryption-0.0.1 (c (n "field-encryption") (v "0.0.1") (d (list (d (n "generic-array") (r "^0.14.6") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "regex-automata") (r "^0.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "074racimkyq1pfrvqkn8d3q7zkn7pwq5wqq1af56xj45zggxnfll")))

