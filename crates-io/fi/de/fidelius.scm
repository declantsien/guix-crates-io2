(define-module (crates-io fi de fidelius) #:use-module (crates-io))

(define-public crate-fidelius-0.1.0 (c (n "fidelius") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "154gal9p8qnixf3anyyq72xykjrf57s06dp95m16nwkcyb291p67")))

(define-public crate-fidelius-0.1.1 (c (n "fidelius") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "14ac9gp4vbf9cmq8pqw7lgk3fh6ikwxvspm528pzzbx1gdgiwf7d")))

(define-public crate-fidelius-0.1.2 (c (n "fidelius") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "1w75jjmhm7idj0jw7v35qykzn2x96zralj6smj5s97x0r9jgsl9n")))

(define-public crate-fidelius-0.1.4 (c (n "fidelius") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "1h1kgx2fz8mji1pj3v27nnbn0x4bkj30caxiaw8ay88nl322xmp5")))

(define-public crate-fidelius-0.1.6 (c (n "fidelius") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0z3za73q49pyr85m7gbzdckg9rwi3kajgcj4m9n4pmh5aqb8cdjl")))

(define-public crate-fidelius-0.1.7 (c (n "fidelius") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "197bk9wwl3p6z4sj7b89v3na5dyyjc56rmzgy5j80w97zcsqyzj8")))

(define-public crate-fidelius-0.1.9 (c (n "fidelius") (v "0.1.9") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "preferences") (r "^1.1.0") (d #t) (k 0)))) (h "0hc5bh9lgg1r59qdx9w5ka2s11c8khvlp868n31wc3a6fndv3z31")))

