(define-module (crates-io fi lo filon) #:use-module (crates-io))

(define-public crate-filon-1.0.0 (c (n "filon") (v "1.0.0") (h "0nwigf6gax306vwj84pzvjyfyh0yf6zxsa4n20a96ma20lr3l104")))

(define-public crate-filon-1.1.0 (c (n "filon") (v "1.1.0") (d (list (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)))) (h "0k8bbvd4j9sr8dsxcim0hr5fmh215nj0vja2xiyvd4pp0cs16qjj")))

(define-public crate-filon-1.2.0 (c (n "filon") (v "1.2.0") (d (list (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)))) (h "1wlx77hbm4n2d3dp2sbvq3ma97d5d85cfr13g2k58yc9wrwb6zbc")))

