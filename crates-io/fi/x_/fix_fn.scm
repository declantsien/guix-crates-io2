(define-module (crates-io fi x_ fix_fn) #:use-module (crates-io))

(define-public crate-fix_fn-0.1.0 (c (n "fix_fn") (v "0.1.0") (h "1m4dcd1dgijk1x1rgyji71g2g2q4y2f0rf13dh004w843c3bcsaw")))

(define-public crate-fix_fn-1.0.0 (c (n "fix_fn") (v "1.0.0") (h "0fvqjsxfymma66qbls83n33whr2kc8andhn237m7ls0s2qc6dj3w")))

(define-public crate-fix_fn-1.0.1 (c (n "fix_fn") (v "1.0.1") (h "0ksgqbr2ssha8ipw2lzq34n18hfcflr3ckkdbafdzp99p3rg3z4y")))

(define-public crate-fix_fn-1.0.2 (c (n "fix_fn") (v "1.0.2") (h "1wgphq05iglhcvyqajc7fsjgm6s4pshm1xznw375hwj12cz1j3rq")))

