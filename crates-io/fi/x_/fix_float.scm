(define-module (crates-io fi x_ fix_float) #:use-module (crates-io))

(define-public crate-fix_float-0.1.0 (c (n "fix_float") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "0ccba03xcpw861726hni0absz0q8n437c7yka2aqlyaij1qcfirc")))

(define-public crate-fix_float-0.1.1 (c (n "fix_float") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "14091jzd264xl2qbdsf01imjx4px91gf00jbjap89z9w3l7y6h3b")))

(define-public crate-fix_float-0.1.2 (c (n "fix_float") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1sf2nii91klxwdanxng8wz8b08jkmr36g9q4k9bw1rhw4wlspnax")))

(define-public crate-fix_float-0.1.3 (c (n "fix_float") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k7an2ldbbj9sxs0x80k3jrbp18p255w4awv100j7kq5rca21nb1")))

(define-public crate-fix_float-0.1.4 (c (n "fix_float") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1afanf0za2bg0z3p6f0hkqdywi4b88x6j20ma5pqg72rp3w04zir")))

