(define-module (crates-io fi x_ fix_me) #:use-module (crates-io))

(define-public crate-fix_me-0.1.0 (c (n "fix_me") (v "0.1.0") (h "0xy6g3c4w4gfh1rwjrggxzv6fssry6mrc5v0hz0f8pvq1y736m43") (f (quote (("unfixed_code") ("default") ("build_tests"))))))

(define-public crate-fix_me-0.1.1 (c (n "fix_me") (v "0.1.1") (h "19h45klac3yhp94s1dbbpq4ff2bwm3aaw4gk350pcx8i2bdzh8mg") (f (quote (("unfixed_code") ("build_tests"))))))

(define-public crate-fix_me-0.1.2 (c (n "fix_me") (v "0.1.2") (h "05wwafimriir2jain3jwjrfslzbdgar7xawllk09q6gajybck12g") (f (quote (("unfixed_code"))))))

