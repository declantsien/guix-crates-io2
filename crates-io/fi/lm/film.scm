(define-module (crates-io fi lm film) #:use-module (crates-io))

(define-public crate-film-0.0.1 (c (n "film") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "14czz6pdrb1mxca2y72xxb9x9g4jb0bhm8wc4x47w4brypkqwlyp")))

