(define-module (crates-io fi lx filx) #:use-module (crates-io))

(define-public crate-filx-0.1.0 (c (n "filx") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1lzdnmaqa8hymxr6hjjqb141r5gf60mmj1lm6zf74ji3wmx1hd6r")))

(define-public crate-filx-0.1.1 (c (n "filx") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s994bfb63kdq8jvha26jn2w5j43zic2n8pd7xjfjdfn37mrnjg3")))

(define-public crate-filx-0.1.2 (c (n "filx") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07kn1q79c42bx1g2h9nkgl30zn73c8qgas8rgflh0mhsgylv88mi")))

