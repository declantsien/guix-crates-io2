(define-module (crates-io fi z- fiz-math) #:use-module (crates-io))

(define-public crate-fiz-math-0.0.1 (c (n "fiz-math") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1y3spbc72yzlpp5yb20ihvqpk5izlzgclqbllq9n0zk1d8v7vgvp")))

(define-public crate-fiz-math-0.0.11 (c (n "fiz-math") (v "0.0.11") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1pckhc5gp1nj52x04pbnrp6966a8vm4lnxg6m15aiy91mp521ci4") (y #t)))

(define-public crate-fiz-math-0.0.12 (c (n "fiz-math") (v "0.0.12") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "08ya1m8q2j1vxng6k6hfxg4irg0aqjrkaqyyqa8grhkv1v2g7y67")))

(define-public crate-fiz-math-0.0.13 (c (n "fiz-math") (v "0.0.13") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0mkxfhzmb7yfi8q7s267f10p92igfhcn4hjx1mp3qz0nhirxzl6y")))

(define-public crate-fiz-math-0.0.14 (c (n "fiz-math") (v "0.0.14") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "00pv81na9zhlhqbrf10vkrz4cl2im12ly9ngkahp88yd6c0x83m1")))

