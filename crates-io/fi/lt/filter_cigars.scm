(define-module (crates-io fi lt filter_cigars) #:use-module (crates-io))

(define-public crate-filter_cigars-0.0.1 (c (n "filter_cigars") (v "0.0.1") (d (list (d (n "bio") (r "^0.31") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.31") (d #t) (k 0)))) (h "1r90mij2w69012ag9da0m73llm8cjx2p99h3k244iwjimnwdv9zw")))

