(define-module (crates-io fi lt filter_ast) #:use-module (crates-io))

(define-public crate-filter_ast-0.1.0 (c (n "filter_ast") (v "0.1.0") (d (list (d (n "from_variants") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ib3zlqf4jibsk0ciiwif96lmx36da4lvp7sb2gzxm74xzqmqk3b")))

(define-public crate-filter_ast-0.1.1 (c (n "filter_ast") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hwbc5cyr92rba99bp6xyd65m1wn2w2z3p0dzxl6zx6qk86fg39r")))

(define-public crate-filter_ast-0.1.2 (c (n "filter_ast") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17jl4f73mzyjs2w9k6rl1cr8gjk6a4r398xa2w6ra1dpa6wbq2jx")))

(define-public crate-filter_ast-0.2.0 (c (n "filter_ast") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dpl4hf5gflipp5pap4wac45114sxn371cbgcp9y2p74w7psrvbs")))

(define-public crate-filter_ast-0.2.1 (c (n "filter_ast") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dy43dqwwpy0mvbyfww5rd7sxlmy3hyq48vv66vq39gq5xmazjp8")))

