(define-module (crates-io fi lt filterway) #:use-module (crates-io))

(define-public crate-filterway-0.1.0 (c (n "filterway") (v "0.1.0") (d (list (d (n "aargvark") (r "^0.2") (d #t) (k 0)) (d (n "defer") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("fs"))) (d #t) (k 0)))) (h "1477ay6l2nhmwhl50lcy5lz4m1wwa1ygw2wh2i869v0k2vfbyvrk")))

