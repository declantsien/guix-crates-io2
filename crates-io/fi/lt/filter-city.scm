(define-module (crates-io fi lt filter-city) #:use-module (crates-io))

(define-public crate-filter-city-0.1.0 (c (n "filter-city") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0z42sr1swq2kv42xm2kljm3pgpf43gkwf12d9wfm3yjkj5b23jzx") (y #t)))

(define-public crate-filter-city-0.1.1 (c (n "filter-city") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1z6hb3kfh1x8zwq0r2y0dyw9gzb0lkvbvsvvbbmh7bar5abcmw55") (y #t)))

(define-public crate-filter-city-0.1.2 (c (n "filter-city") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1xis3plnj9daynr507wphdnbvs8r117wzdqhrgb7rabwk1nzbr2j") (y #t)))

(define-public crate-filter-city-0.1.3 (c (n "filter-city") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0anz7268zh9wagdghhm3d2z2z520snhsawrlqzn6y0vx2qgf1rf0") (y #t)))

(define-public crate-filter-city-0.1.4 (c (n "filter-city") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "10pq44k2vd03x2dw6yg9ickpmj9gg079pkzfznqz3yp5aaiknzgr") (y #t)))

(define-public crate-filter-city-0.1.5 (c (n "filter-city") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "06nds59shi4pfpk1mvlj908q2d6c42vlfnb93aln3jlxw9pjzapb")))

(define-public crate-filter-city-0.1.6 (c (n "filter-city") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1cw5w34269hk9ibv4phq7dx266xbkvb0yd98x97lxx1pi49cna3i")))

(define-public crate-filter-city-0.1.7 (c (n "filter-city") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0zb9jsamsn5ncpa7isfb5g0r4kkwk0hpr614s185ylnfyiccp78z")))

(define-public crate-filter-city-0.1.8 (c (n "filter-city") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0jg96k6vq7z082wvr5s8dzzfp8jm3nxpj2cy9rdszdd1h2vr0219")))

