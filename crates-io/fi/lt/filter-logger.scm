(define-module (crates-io fi lt filter-logger) #:use-module (crates-io))

(define-public crate-filter-logger-0.1.0 (c (n "filter-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3") (d #t) (k 0)))) (h "1857aaz70qrbjf516gknyqwpldxwnjk409xzmhwgc58rbgs8v95d")))

(define-public crate-filter-logger-0.2.0 (c (n "filter-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3") (d #t) (k 0)))) (h "0cf3lahp83xdrngrviiym024r7xm63b1716i7qnfdm4vslfcg7y0")))

(define-public crate-filter-logger-0.3.0 (c (n "filter-logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "054qflxh7593js9vjqk0167aialsmnblrqiyi8qnh6xybnjxwfyb")))

