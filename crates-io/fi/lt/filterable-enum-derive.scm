(define-module (crates-io fi lt filterable-enum-derive) #:use-module (crates-io))

(define-public crate-filterable-enum-derive-0.1.0 (c (n "filterable-enum-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0ysrv0gq5yl9cc7avlhnssy7i5xqi5qhzh38jixpfyxqh5pa2np2")))

