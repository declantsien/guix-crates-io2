(define-module (crates-io fi lt filter) #:use-module (crates-io))

(define-public crate-filter-0.1.0 (c (n "filter") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "045nc8kkp7cqalby7wwr7sgpaz2pyysm35ssimic73nwymhq2xy5")))

(define-public crate-filter-0.2.0 (c (n "filter") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.21.0") (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)))) (h "0clcrymlb66ag07qbghcm2fx8wqnmm95iagwdaz7aayd4rnglh5i") (f (quote (("default" "alloc") ("alloc"))))))

