(define-module (crates-io fi lt filters) #:use-module (crates-io))

(define-public crate-filters-0.1.0 (c (n "filters") (v "0.1.0") (h "14y3qjlliwldm2x453d1kwdxrs23iql78nb7hbq3mhgfyl7dn2rp")))

(define-public crate-filters-0.1.1 (c (n "filters") (v "0.1.1") (h "0hkyj6kr0qknx3cahjk38h04hh41hp8hajslx0nbs79hbnihhlbd") (f (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.2.0 (c (n "filters") (v "0.2.0") (h "04yc7by54i0ksgaq4ad06ybh795vmhklz5kwbxbmw8pnq8afca2x") (f (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.3.0 (c (n "filters") (v "0.3.0") (h "0jr77s8nsr02hlsqcr49rizik95ag8bdhcxpncq7qwq626r2x3k1") (f (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.4.0 (c (n "filters") (v "0.4.0") (h "1qxk235404f473pvsk9mv37a3qpkidhbq9j1alwl3f1v59ca78hf") (f (quote (("unstable-filter-as-fn"))))))

