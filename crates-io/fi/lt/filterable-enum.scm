(define-module (crates-io fi lt filterable-enum) #:use-module (crates-io))

(define-public crate-filterable-enum-0.1.0 (c (n "filterable-enum") (v "0.1.0") (d (list (d (n "enumflags2") (r "^0.7.9") (d #t) (k 0)) (d (n "filterable-enum-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ix49l9c46rxm0rn6ykmrwqynvql7f4v7w1ma6hdd8i3hb73rh9q")))

