(define-module (crates-io fi lt filter-clipped) #:use-module (crates-io))

(define-public crate-filter-clipped-0.1.0 (c (n "filter-clipped") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (d #t) (k 0)))) (h "1xzichdmb10kvpldndzmgwl3rjr8jrdfllz1j8xy9bzv60y00hzx")))

(define-public crate-filter-clipped-0.2.0 (c (n "filter-clipped") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (d #t) (k 0)))) (h "1b2x9q89h2r73xy1r8gq8gm55d1qm88rzxqz3yp7j2zz5ly2bafg")))

(define-public crate-filter-clipped-0.3.0 (c (n "filter-clipped") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.39.5") (d #t) (k 0)))) (h "0fjppwypsn2kswkzhimljqfhhkdrkrkr6bcvjj846zd2r8sl88c7")))

