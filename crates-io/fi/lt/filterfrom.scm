(define-module (crates-io fi lt filterfrom) #:use-module (crates-io))

(define-public crate-filterfrom-0.1.0 (c (n "filterfrom") (v "0.1.0") (d (list (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)))) (h "0jb2k6xwdxl1csqpxd1acprhslabbxdj1yz6q8c04znvskiy0kg3")))

(define-public crate-filterfrom-0.2.0 (c (n "filterfrom") (v "0.2.0") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)))) (h "1vklpmcavpq6ghyarxadhyf4f8fs5hrg3nyyslp3avl29sixa540")))

(define-public crate-filterfrom-0.3.0 (c (n "filterfrom") (v "0.3.0") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)))) (h "0l2k6pcnd71ljhqvryqxqz1msz9n4rdcl152d5mg376v7ifv2ayf")))

(define-public crate-filterfrom-0.4.0 (c (n "filterfrom") (v "0.4.0") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0ldrii9n213j3whzadsg87mnpd4jrdwsq02m2z51kqswg5ad4vwg")))

(define-public crate-filterfrom-0.4.1 (c (n "filterfrom") (v "0.4.1") (d (list (d (n "oops") (r "^0.1.0") (d #t) (k 0)) (d (n "stdinix") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "15dszj2mldx9pipn0air1smxa91dq9h233icx6fykdhcn5rp6v0l")))

