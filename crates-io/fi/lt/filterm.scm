(define-module (crates-io fi lt filterm) #:use-module (crates-io))

(define-public crate-filterm-0.1.0 (c (n "filterm") (v "0.1.0") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0kcgp2rkchp7g6p2hc7158p684asnljdvc866ipxhfinsg25b0ki") (y #t)))

(define-public crate-filterm-0.2.0 (c (n "filterm") (v "0.2.0") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "06n6l8i71l32qiyzq72ai216l42fbgxzc816mzw408lgr24n5jbr") (y #t)))

(define-public crate-filterm-0.2.1 (c (n "filterm") (v "0.2.1") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "0j7czl3frbkin9p8g3nghwmyb7bn3k82gja03v7bzz71svjhlr3q") (y #t)))

(define-public crate-filterm-0.1.1 (c (n "filterm") (v "0.1.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1siqrpk4lzhia6bkmqzz9bd5d87lc85dsaaadlvbgxalrrz952yd")))

(define-public crate-filterm-0.2.2 (c (n "filterm") (v "0.2.2") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "12j7pffb4wkvyczkd51z5nwrx2mr2k45aikbhpc89afspkp7m6sr")))

(define-public crate-filterm-0.1.2 (c (n "filterm") (v "0.1.2") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1frv4y3sknl4dsz8jvb67gjf94bzfi6dr84r9dr4al7mwlyxs4iv")))

(define-public crate-filterm-0.2.3 (c (n "filterm") (v "0.2.3") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "1kjpiy2l139ypim2chrh5dq9b3p2lvpzblih5wvzjg6cal4bcmjc")))

(define-public crate-filterm-0.2.4 (c (n "filterm") (v "0.2.4") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "0cs7gjfxw5vmj787xyp0242cfxv743d81sdcifc7qmzfcymnmy52")))

(define-public crate-filterm-0.1.3 (c (n "filterm") (v "0.1.3") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1j1ifzrqdk500z605wsy7iwl0f4fxyjb6y65i2qvpsbprpf1idn3")))

(define-public crate-filterm-0.2.5 (c (n "filterm") (v "0.2.5") (d (list (d (n "nix") (r "^0.24") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "0zkjj2kly95fjpnqrwyy4455a1d5isdnjysmyh1cz76737mws2vb")))

(define-public crate-filterm-0.3.0 (c (n "filterm") (v "0.3.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "0dqvysm721qs916av2dpal1y55yhwb2l0s5aa53lq05a2sbavg8g")))

(define-public crate-filterm-0.4.0 (c (n "filterm") (v "0.4.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "1q6bx0xhqy44gzas4l9br12aiqq2ggbgs8rckcpyg2ahjjqywy37") (r "1.63")))

(define-public crate-filterm-0.4.1 (c (n "filterm") (v "0.4.1") (d (list (d (n "atomic-int") (r "^0.1") (f (quote ("c_int" "signal"))) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "1654rrazz6vwblxx30phzn0nf40awdnw6vxf3l4hxifz85xig5c6") (r "1.63")))

(define-public crate-filterm-0.4.2 (c (n "filterm") (v "0.4.2") (d (list (d (n "atomic-int") (r "^0.1") (f (quote ("c_int" "signal"))) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (k 0)))) (h "1fphsnxygxyz52wnfzzm1lcjl07j74bj3xanw1fqbrsf340xy32g") (r "1.63")))

