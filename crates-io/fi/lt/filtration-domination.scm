(define-module (crates-io fi lt filtration-domination) #:use-module (crates-io))

(define-public crate-filtration-domination-0.0.1 (c (n "filtration-domination") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 2)) (d (n "litemap") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "sorted-iter") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dl65mmihr3wasi8nwq6x8yizmhw073kv3wzrzz7pi53bpr8ficp")))

