(define-module (crates-io fi gg figgy) #:use-module (crates-io))

(define-public crate-figgy-0.1.0 (c (n "figgy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "14z141mfi0jiwsgm7avcad0fgvkz16qmvxlyn9nzhs0spmcr9zx4")))

(define-public crate-figgy-0.1.1 (c (n "figgy") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1jhjn7iyqnqn5r4hffvphmwpp0dlf3h7fb52gax5fm9rwdislffx")))

(define-public crate-figgy-0.1.2 (c (n "figgy") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1bndvygrl6m622qkxrz98q69p7k52hif58kwwzcirap03yfd9gz2")))

