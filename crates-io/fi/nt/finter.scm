(define-module (crates-io fi nt finter) #:use-module (crates-io))

(define-public crate-finter-0.1.0 (c (n "finter") (v "0.1.0") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "05ayq86kanbhqri6l2xap6qa5gzssw5bw960djwr4axkq01iig7c")))

(define-public crate-finter-0.1.1 (c (n "finter") (v "0.1.1") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0s3l0i5wnlfk9hpspkrcp13snhjnjrcbl7fbvdq95vd3cmipsp35")))

(define-public crate-finter-0.1.2 (c (n "finter") (v "0.1.2") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0012ffxwb19i190d5axspkv6kq5sn899zs0kxqbij3fgfb3ahym3")))

(define-public crate-finter-0.1.3 (c (n "finter") (v "0.1.3") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0zqq8ssj1kdqq46y4lcd7rkcq95qzyh4mjyl09cz9ng9w3gm9q4j")))

(define-public crate-finter-0.1.4 (c (n "finter") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "10lsj6gry778fyr04scydc3z8rvqzzmydkvrwz4fqmgaqnv62adx")))

(define-public crate-finter-0.1.5 (c (n "finter") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0smcr8cqsc3y7cbkh4zhnpknv7c6cz7iszyqklrcq98rk24khb5v")))

(define-public crate-finter-0.1.6 (c (n "finter") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1iws9713l544msgwsrnkl7cws5jsjzbdn643pr5301jvfa1h0zz0")))

(define-public crate-finter-0.1.7 (c (n "finter") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0if258jk8r5mbc7w23jhqizsjzqyz3mbb4z6kdmr0vrdshv3ig50")))

(define-public crate-finter-0.1.8 (c (n "finter") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0fi25mx6wz7wgw20a8p22a1nz1a0dn82a1khx8l0pgbdd0imn7ay")))

(define-public crate-finter-0.1.9 (c (n "finter") (v "0.1.9") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1wzk1wf62q8gcz9kvrcd3g81578x547gxhd030gv74nibi51p6cm")))

