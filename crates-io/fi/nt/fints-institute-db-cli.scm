(define-module (crates-io fi nt fints-institute-db-cli) #:use-module (crates-io))

(define-public crate-fints-institute-db-cli-1.3.0 (c (n "fints-institute-db-cli") (v "1.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo" "wrap_help" "deprecated"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2") (d #t) (k 0)) (d (n "fints-institute-db") (r "^1") (d #t) (k 0)) (d (n "iban_validate") (r "^4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00mnxbpjkvlj1792dmmfxbq9l34ml9hlpydysg3s4y38irlpcmad")))

