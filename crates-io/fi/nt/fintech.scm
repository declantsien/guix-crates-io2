(define-module (crates-io fi nt fintech) #:use-module (crates-io))

(define-public crate-fintech-0.1.0 (c (n "fintech") (v "0.1.0") (h "0hpgnw12038aqpjsazylg8j5vl38nxsnqzp9y7ark2b3hrjjr40a")))

(define-public crate-fintech-0.1.1 (c (n "fintech") (v "0.1.1") (h "190bqmz1ksf0r5ih4qndqy664y1xhk49x31jqv155sab5ygpi114")))

