(define-module (crates-io fi nt fints) #:use-module (crates-io))

(define-public crate-fints-0.1.0 (c (n "fints") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.13") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fints-institute-db") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12d73cjby59ya3ksqlfa6hihrw277xla495jy9824x7614q6kvr7")))

