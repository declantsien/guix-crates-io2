(define-module (crates-io fi nt finte-derive) #:use-module (crates-io))

(define-public crate-finte-derive-0.1.0 (c (n "finte-derive") (v "0.1.0") (h "0qba72i5qh4xd0vf7bba5b9ax5ak9r31vpl1db77rfgy01q0fnxj")))

(define-public crate-finte-derive-0.1.1 (c (n "finte-derive") (v "0.1.1") (h "1qg3crf8d1f920a13wwz1jmbaymjagip2a3cv5iihjs7i3h522sw")))

(define-public crate-finte-derive-0.1.2 (c (n "finte-derive") (v "0.1.2") (h "00j9x82s7xvcppxkzkwn8cwh4d4ic2139shhpiiwf263n8iynajd")))

(define-public crate-finte-derive-0.1.3 (c (n "finte-derive") (v "0.1.3") (d (list (d (n "finte") (r "^0.1") (d #t) (k 2)))) (h "0fcf2p2fpd6ad9rirhsviw9d6ph25am26la9r2g96q72c5lxzwqk")))

(define-public crate-finte-derive-0.2.0 (c (n "finte-derive") (v "0.2.0") (h "17imvlhb72sgj19qwzfzfmixgdpx2x64xqs0gv6wajfsimn5xazd")))

