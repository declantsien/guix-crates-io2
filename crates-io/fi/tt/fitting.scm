(define-module (crates-io fi tt fitting) #:use-module (crates-io))

(define-public crate-fitting-0.1.0 (c (n "fitting") (v "0.1.0") (h "0lq4hyjwamamnccjbqhzbp7zabamia0qv457gl88iagvkaypd8ig")))

(define-public crate-fitting-0.2.0 (c (n "fitting") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("approx"))) (d #t) (k 0)))) (h "00np6d2x1pkzcf7xzm3rnzqvak34zcd7fsj8dpki96s28z46xi9p")))

(define-public crate-fitting-0.2.1 (c (n "fitting") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("approx"))) (d #t) (k 0)))) (h "0hbcxbyp239xwl8w9r83lk2x9wnjwwpdyxirafwqjv43jpwzns3w")))

(define-public crate-fitting-0.3.0 (c (n "fitting") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (f (quote ("approx"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "06cpn8sj8jjlvg0a3g6v3vzcs1dhbyygz506f1z1j01f80ipsw95")))

(define-public crate-fitting-0.4.0 (c (n "fitting") (v "0.4.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1mn2s3a10pfr07xb871b8h0pd1xy9g30wsfmmqy7rkb6w696c0l9")))

(define-public crate-fitting-0.4.1 (c (n "fitting") (v "0.4.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1arwv56x4s9sfzzrdyqxx0hpfbcsfmhr9xmy8hyx729x00pvks2y")))

(define-public crate-fitting-0.4.2 (c (n "fitting") (v "0.4.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0hm5agl9n5qc0hkybgk8pmi0z9s0x31xnyf25a0sm60df6lqb1br")))

(define-public crate-fitting-0.4.3 (c (n "fitting") (v "0.4.3") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1qyq76qqzcnyjhprb694phpkk4m7zwp87l9mixwgzdazz3ifhh4v")))

(define-public crate-fitting-0.4.4 (c (n "fitting") (v "0.4.4") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1n0g2gbyrrvlms9gfiybhbk9xcfdqrrlf4vn2r09q9zzmdbc9nkp")))

(define-public crate-fitting-0.5.0 (c (n "fitting") (v "0.5.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pzmzkrhrvnbkp35p729dd0ajg1cb8rv5br56nyf7j0hbk05y4rs")))

(define-public crate-fitting-0.5.1 (c (n "fitting") (v "0.5.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("serde" "approx" "approx-0_5"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1h3mq56jldprrcrl5w0kmaf0kswrkmbbfwk16y6lhccp5war7kpp")))

