(define-module (crates-io fi bs fibs) #:use-module (crates-io))

(define-public crate-fibs-0.1.0 (c (n "fibs") (v "0.1.0") (d (list (d (n "num") (r "^0") (d #t) (k 0)))) (h "0b19r59yn9849l5068m2d3pbbqlfb005kfjc3ndym9g7pcpmmah2") (f (quote (("std") ("default" "std"))))))

(define-public crate-fibs-0.2.0 (c (n "fibs") (v "0.2.0") (d (list (d (n "num") (r "^0") (k 0)))) (h "1z9rygkylg2k55fv9xs7phxs7nhf7i1nh7qb758p9zxv51prs3r7") (f (quote (("std" "num/std") ("default" "std"))))))

(define-public crate-fibs-0.2.1 (c (n "fibs") (v "0.2.1") (d (list (d (n "num") (r "^0") (k 0)))) (h "1jwnmcyhbiskzzaww6122adhdf6n5fdb9nybc4pgcjfj18k02h85") (f (quote (("std" "num/std") ("default" "std"))))))

(define-public crate-fibs-0.2.2 (c (n "fibs") (v "0.2.2") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "00c8w5q3wa2vy9lks9ib3hvvld36rm4c3hzx31r5p1xw44fpxp6p") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.3 (c (n "fibs") (v "0.2.3") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0lbpml9xpgalrawaijqps3dz0j3cfp7840jr6a4l6b7yiz3fw9c5") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.4 (c (n "fibs") (v "0.2.4") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0nzsp3npnmckgnwfl0kldlkavh6bbvl84a510g7sp5qffd96xpm3") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.5 (c (n "fibs") (v "0.2.5") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0dj1wps6cwz2aqzin3p256d0j3mnyzr47k4i81424fldl8lixv21") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.6 (c (n "fibs") (v "0.2.6") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0n9g6wr4y4wlv8l5mvac8w4l9lz8fvimdfwk99a08ci30473s2fq") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.7 (c (n "fibs") (v "0.2.7") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1zla2y2rihq8lckn53xg68xnpn0a8vpwzdb7nic918m40y35mlzq") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2.8 (c (n "fibs") (v "0.2.8") (d (list (d (n "num") (r "^0") (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "07rf9h4adjwngfhs2f0j6m77gnm7mkxwpmr686p6sl4zsap0vmi0") (f (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

