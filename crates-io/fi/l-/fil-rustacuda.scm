(define-module (crates-io fi l- fil-rustacuda) #:use-module (crates-io))

(define-public crate-fil-rustacuda-0.1.3 (c (n "fil-rustacuda") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-driver-sys") (r "^0.3") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0kf3ha749gczp5gvl619v1749rs3dxmxjq5va50myw593f3y44v3")))

(define-public crate-fil-rustacuda-0.1.4 (c (n "fil-rustacuda") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-driver-sys") (r "^0.3") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 0)))) (h "070rws56h5wvmry4d30f8k33hp12vd6nx8iszp93ydfmf906srj0")))

