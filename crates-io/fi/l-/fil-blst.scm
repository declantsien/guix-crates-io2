(define-module (crates-io fi l- fil-blst) #:use-module (crates-io))

(define-public crate-fil-blst-0.1.0 (c (n "fil-blst") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fff") (r "^0.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "groupy") (r "^0.3.1") (d #t) (k 0)) (d (n "paired") (r "^0.20.0") (d #t) (k 0)))) (h "1kigvc7fpdkwbw70vc095bbmai6kjiki9hy73xrqqys17dsgynb9")))

(define-public crate-fil-blst-0.1.1 (c (n "fil-blst") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fff") (r "^0.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "groupy") (r "^0.3.1") (d #t) (k 0)) (d (n "paired") (r "^0.20.0") (d #t) (k 0)))) (h "0k8ph2jkzj07jcwr5fjw7yk2r7xayx16dw8h1lfrhdd2mq0xbhgs")))

(define-public crate-fil-blst-0.1.2 (c (n "fil-blst") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fff") (r "^0.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "groupy") (r "^0.3.1") (d #t) (k 0)) (d (n "paired") (r "^0.20.0") (d #t) (k 0)))) (h "1r6b4hxrg07rf9wv5vm6q2iblp7d3mjj3cysdijy1dnwfikh1qbg")))

