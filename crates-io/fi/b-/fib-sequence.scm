(define-module (crates-io fi b- fib-sequence) #:use-module (crates-io))

(define-public crate-fib-sequence-0.1.0 (c (n "fib-sequence") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0lbiz4p1kzhrh7anbv51jcb9ksgk46bdginzvs5a7w9jkrallrj1")))

