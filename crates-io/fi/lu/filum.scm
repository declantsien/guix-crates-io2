(define-module (crates-io fi lu filum) #:use-module (crates-io))

(define-public crate-filum-0.1.0 (c (n "filum") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0s3hldrvl5n56nf3k30hkxln04hb7libfah9crxfrzhh9y405pqn") (f (quote (("verbose"))))))

(define-public crate-filum-0.1.1 (c (n "filum") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1xwwakf0a599phnvx8pnkjmwyqv4f9rqpid4r5g9wqn1z6i8sbl7") (f (quote (("verbose"))))))

(define-public crate-filum-0.1.2 (c (n "filum") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0w4vzg1bwwbahinyph843ba3g44pvr6syl2lz2gvscc797lay2xf") (f (quote (("verbose"))))))

