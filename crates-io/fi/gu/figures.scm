(define-module (crates-io fi gu figures) #:use-module (crates-io))

(define-public crate-figures-0.0.0-reserve.0 (c (n "figures") (v "0.0.0-reserve.0") (h "024vmjbi68ycya5ni7srhndwpccy6yyjv09j4969z2j71059ajph")))

(define-public crate-figures-0.1.0 (c (n "figures") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18k9l2wcfqd7y8d44bia3xd52l938642y6p6y17z2g1wjpm10bkb") (f (quote (("default"))))))

(define-public crate-figures-0.1.1 (c (n "figures") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b0rz59mgd15zamzlwf6hykx3zalxqyd9fn0f2xq7r8lz19cra0g") (f (quote (("default"))))))

(define-public crate-figures-0.1.2 (c (n "figures") (v "0.1.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05mblla9f3h32vnxr1ln65g57hi6n0p71hiqk1nj63xn86f77lnw") (f (quote (("default"))))))

(define-public crate-figures-0.2.0 (c (n "figures") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (o #t) (k 0)) (d (n "intentional") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (o #t) (k 0)) (d (n "winit") (r "^0.29.4") (o #t) (k 0)))) (h "0j69vlbfl6cxc5q972nm29a5fmz7a6wyhz103sq8l0dlsbpjq5v3") (f (quote (("x11" "winit/x11"))))))

(define-public crate-figures-0.2.1 (c (n "figures") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (o #t) (k 0)) (d (n "intentional") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (o #t) (k 0)) (d (n "winit") (r "^0.29.4") (o #t) (k 0)))) (h "11pa0yjfngl1jb8kvakvq3xshjvjvljdylfdwal7ilx01fg0ygw1") (f (quote (("x11" "winit/x11"))))))

(define-public crate-figures-0.2.2 (c (n "figures") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (o #t) (k 0)) (d (n "intentional") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (o #t) (k 0)) (d (n "winit") (r "^0.29.4") (o #t) (k 0)))) (h "0ph7mqh0ir7h8p73spv5v0kfgrzigz7m7rr87sr2vfndk3ncwn9c") (f (quote (("x11" "winit/x11"))))))

(define-public crate-figures-0.3.0 (c (n "figures") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.9") (o #t) (k 0)) (d (n "intentional") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wgpu") (r "^0.20.0") (o #t) (k 0)) (d (n "winit") (r "^0.30.0") (o #t) (k 0)))) (h "1af5l40i0fw3n4nxdm8aglwq83zk9i1i0m9irwc7gn0kf7asshra") (f (quote (("x11" "winit/x11")))) (r "1.70.0")))

