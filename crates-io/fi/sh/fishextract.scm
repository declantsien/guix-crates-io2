(define-module (crates-io fi sh fishextract) #:use-module (crates-io))

(define-public crate-fishextract-0.0.0 (c (n "fishextract") (v "0.0.0") (d (list (d (n "log") (r "^0.4.20") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k39dwx2xa1sgrfsh94mxd30m350a5rf4y2rmffdjr0dyz5vkk6r")))

