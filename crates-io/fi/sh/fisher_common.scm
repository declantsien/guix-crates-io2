(define-module (crates-io fi sh fisher_common) #:use-module (crates-io))

(define-public crate-fisher_common-0.1.0 (c (n "fisher_common") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ki50lb0hssdiw0wr1zrff11p0mc9l6g30pn323hwh1wx3r337n6")))

(define-public crate-fisher_common-0.2.0 (c (n "fisher_common") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rfva42mvbyxzivhxxqmgww0h26s4iggcs4aw9pqgnb271j7xakn")))

