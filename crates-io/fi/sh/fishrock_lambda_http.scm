(define-module (crates-io fi sh fishrock_lambda_http) #:use-module (crates-io))

(define-public crate-fishrock_lambda_http-0.3.0-patched.1 (c (n "fishrock_lambda_http") (v "0.3.0-patched.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.3.0-patched.1") (d #t) (k 0) (p "fishrock_lambda_runtime")) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1wpz0fhxmp5l8phlzqmq2zcw10jgf2c72iszmyk0dl7hhplc8vqj")))

