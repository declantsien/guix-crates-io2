(define-module (crates-io fi sh fishers_exact) #:use-module (crates-io))

(define-public crate-fishers_exact-0.1.0 (c (n "fishers_exact") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "027djm26f207bdgp2cs1zqs6k8q1jv7cl003jv11vqf7acbndpv7") (y #t)))

(define-public crate-fishers_exact-0.1.1 (c (n "fishers_exact") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)))) (h "0jxqrbjm1hm8sqyqh6v86na8l07z7v9zxmk9cmdavnbv9xl5jkf4") (y #t)))

(define-public crate-fishers_exact-1.0.0 (c (n "fishers_exact") (v "1.0.0") (h "1ql00jrr2ca3ymslj220vk8zn5mpzp3dqyazyzxk3b9wqqd901hq")))

(define-public crate-fishers_exact-1.0.1 (c (n "fishers_exact") (v "1.0.1") (h "07kvbhrzma5vxb33jw02x2h8llscgcrkibhd2v7bzp3ywxkk96b4")))

