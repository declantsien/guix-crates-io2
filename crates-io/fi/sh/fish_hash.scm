(define-module (crates-io fi sh fish_hash) #:use-module (crates-io))

(define-public crate-fish_hash-0.1.0 (c (n "fish_hash") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1p1842vck8n89fzgglbrnxn3z2wxsbm6vf6lpwn5dgf82x528xwm")))

(define-public crate-fish_hash-0.2.0 (c (n "fish_hash") (v "0.2.0") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0fyiak31vn1pv0lcw2jqypqpkr56j76j98fpwrik0mfanzn99hzr")))

(define-public crate-fish_hash-0.3.0 (c (n "fish_hash") (v "0.3.0") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "04s30gmfbh9sx9vk0q1wxv7nmll0817n01vxwljsh7j9g642dskc")))

