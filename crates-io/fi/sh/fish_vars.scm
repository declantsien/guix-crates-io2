(define-module (crates-io fi sh fish_vars) #:use-module (crates-io))

(define-public crate-fish_vars-0.1.0 (c (n "fish_vars") (v "0.1.0") (h "1gprm1vqifzqv3pmq96844b2fq8jlmykfr0z9mkqsj55074k479w")))

(define-public crate-fish_vars-0.1.1 (c (n "fish_vars") (v "0.1.1") (h "1js6xp0pkzv3a6dbj0sgibm9wg6n4yf3fsmwdl3vrninp3m67cw9")))

(define-public crate-fish_vars-0.1.2 (c (n "fish_vars") (v "0.1.2") (h "0bplfdm3vdimv7clb9hvx45y9p10bh0v3zavlj2kg4cpqpp4qgbd")))

(define-public crate-fish_vars-0.1.3 (c (n "fish_vars") (v "0.1.3") (h "0vjlciam9cpb7shyjjv4k1wppc5jvj8x0d1jm99v2fkxybacsqx9")))

