(define-module (crates-io fi nq finql-postgres) #:use-module (crates-io))

(define-public crate-finql-postgres-0.1.0 (c (n "finql-postgres") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)))) (h "00walysimxpjycz026d5rn7s8id5iy4x6504p38xwy9dabk3b0fa")))

(define-public crate-finql-postgres-0.2.0 (c (n "finql-postgres") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "chrono"))) (d #t) (k 0)))) (h "0c2c1z5q34lffabp770ywgl0ypn5wfkvz9vnd5psmwdmrsrah67j")))

(define-public crate-finql-postgres-0.2.2 (c (n "finql-postgres") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "chrono" "offline"))) (d #t) (k 0)))) (h "10ph9wz3hrh51ay71css0676c9awxhphpxng9s7r86xdkngx60iw")))

(define-public crate-finql-postgres-0.2.3 (c (n "finql-postgres") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "chrono" "offline"))) (d #t) (k 0)))) (h "1iryxln06yl0nqk43yxdjdh2k9cp5qwww1yh5xhs04vgzpbgw0r0")))

(define-public crate-finql-postgres-0.3.0 (c (n "finql-postgres") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.2") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres" "macros" "chrono" "offline"))) (d #t) (k 0)))) (h "1gk6b4rbbkdq3bins6l9lwys4dky10xhwl7wflcxwjfp3mdqxky2")))

