(define-module (crates-io fi nq finql-data) #:use-module (crates-io))

(define-public crate-finql-data-0.1.0 (c (n "finql-data") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jdsl4fr44q3zhsnpdpxsxncvjgn46b46xygxlyjamxdlv3y81ks")))

(define-public crate-finql-data-0.1.1 (c (n "finql-data") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1prggk7cjnbgkjnb8mq5ir5999xwwl1aymrk2jdfbnck0k37c464")))

(define-public crate-finql-data-0.1.2 (c (n "finql-data") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1npkb52f1dsfbq9j4dlzdkylsmd41440b7bsq3qb6sn7rrg564fp")))

(define-public crate-finql-data-0.1.3 (c (n "finql-data") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b6nnyli872mv53ky46b1xww812pvziair88rj2qs07wfd4rqkkc")))

(define-public crate-finql-data-0.1.4 (c (n "finql-data") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kqqka59ad9h47h9l0khrnc7zhvympq61n95823zhakh95rdccy1")))

(define-public crate-finql-data-0.1.5 (c (n "finql-data") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hlsqiksygrhnp30lwd1nx4gj1c8l7954f1rp8m0pdz4h7rvlx6p")))

(define-public crate-finql-data-0.2.0 (c (n "finql-data") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04y8c7l55n06rfw8rw11w03bzyv3nbf7dhzxanbarb7s9rp7280g")))

