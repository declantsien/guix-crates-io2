(define-module (crates-io fi nq finql-sqlite) #:use-module (crates-io))

(define-public crate-finql-sqlite-0.1.0 (c (n "finql-sqlite") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)))) (h "1s3pxw1322xs73xidifs6w5pndkb6kl58sjry3i7cpwd21yqppd0")))

(define-public crate-finql-sqlite-0.2.0 (c (n "finql-sqlite") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono"))) (d #t) (k 0)))) (h "1wdvf2lc1p23mlzjkmgzmqsrsjqzrncrszh1w7lhv3cbahrlplcj")))

(define-public crate-finql-sqlite-0.2.1 (c (n "finql-sqlite") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono"))) (d #t) (k 0)))) (h "06n1lcchjs73gx5y309v74mkc4f09vcrf91mj3pjvhpm7v7fswj0")))

(define-public crate-finql-sqlite-0.2.2 (c (n "finql-sqlite") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono"))) (d #t) (k 0)))) (h "1s8ydxc2nakkac9izqc8ncwxk7a1m99iw7mfjb0vq03118752w4p")))

(define-public crate-finql-sqlite-0.2.3 (c (n "finql-sqlite") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono"))) (d #t) (k 0)))) (h "0vh3j5rh5ga36hhh8j51rvf8k1n30c55x6543a8vp6y0051kq2z4")))

(define-public crate-finql-sqlite-0.2.4 (c (n "finql-sqlite") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono"))) (d #t) (k 0)))) (h "0bxkv1h5rqpxxsrz9bihmys0w3kbxmdfy7mq8d6vx3brywsf33il")))

(define-public crate-finql-sqlite-0.2.5 (c (n "finql-sqlite") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono" "offline"))) (d #t) (k 0)))) (h "0c2yh9xmzwr8a9hqlbmcidfdsgkcfxycvc977pjm4hql92bq059r")))

(define-public crate-finql-sqlite-0.3.0 (c (n "finql-sqlite") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "finql-data") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite" "macros" "chrono" "offline"))) (d #t) (k 0)))) (h "0yx74d9q0bwqznwxja98crhmb9w82xpzvqnnxcbxl2nf43wbqxk2")))

