(define-module (crates-io fi ni finito) #:use-module (crates-io))

(define-public crate-finito-0.1.0 (c (n "finito") (v "0.1.0") (d (list (d (n "futures-timer") (r "^3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "15dz1l7pijyjsfsiyn518vkmm3srwfg7lmj39fhmh8hnhmfj9113") (f (quote (("wasm-bindgen" "futures-timer/wasm-bindgen") ("default")))) (r "1.70.0")))

