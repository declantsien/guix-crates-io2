(define-module (crates-io fi ni finiteelement) #:use-module (crates-io))

(define-public crate-finiteelement-0.1.0 (c (n "finiteelement") (v "0.1.0") (d (list (d (n "finiteelement_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s4qinsq6rbdccnbnpgrck633clkpz0fgrix4i2bq1wf6bhygbbs")))

(define-public crate-finiteelement-0.3.0 (c (n "finiteelement") (v "0.3.0") (d (list (d (n "codenano") (r "^0.3.0") (k 0)) (d (n "finiteelement_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bb35w0iihcxgn0r6xqwigs0613067q2k4w21zfw2cgc0x6960zr")))

