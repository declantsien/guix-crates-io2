(define-module (crates-io fi ni finish-it) #:use-module (crates-io))

(define-public crate-finish-it-0.1.0 (c (n "finish-it") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("std"))) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled" "chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm" "serde"))) (k 0)) (d (n "tui-textarea") (r "^0.2.0") (d #t) (k 0)))) (h "1vhr4fbbfvi76yrk3aibisni34wsvzcgd1s64wj41z8l1xchmib0")))

