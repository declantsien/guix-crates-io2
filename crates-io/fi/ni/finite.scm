(define-module (crates-io fi ni finite) #:use-module (crates-io))

(define-public crate-finite-0.0.0 (c (n "finite") (v "0.0.0") (h "0hlysklnnznxf8wkpzsyj9qjrnswqi5q1rs8f7awgafgzccwfiam")))

(define-public crate-finite-0.0.1 (c (n "finite") (v "0.0.1") (h "191gx97hb674qbi45babkv3fjnkxx6lx81vpd2yvf0xrqc7xvn6r")))

