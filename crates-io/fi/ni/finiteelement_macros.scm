(define-module (crates-io fi ni finiteelement_macros) #:use-module (crates-io))

(define-public crate-finiteelement_macros-0.1.0 (c (n "finiteelement_macros") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "11m05h3s86cbr3k2rnckh9adjysxpl9ibx6rz1fgydapsgc484rr")))

