(define-module (crates-io fi ni finite-automata) #:use-module (crates-io))

(define-public crate-finite-automata-0.1.0 (c (n "finite-automata") (v "0.1.0") (d (list (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)))) (h "01dqz2xfn35y5mg9fdrjcj3p7wj108dn7fdj7wy15xgjipli1amr")))

(define-public crate-finite-automata-0.1.1 (c (n "finite-automata") (v "0.1.1") (d (list (d (n "segment-map") (r "^0.1.0") (d #t) (k 0)))) (h "0h6dld3zk4b6jc1412vy5cabkb1h0ghwy01853q4d92kgqhp42s2")))

