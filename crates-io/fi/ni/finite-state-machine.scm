(define-module (crates-io fi ni finite-state-machine) #:use-module (crates-io))

(define-public crate-finite-state-machine-0.1.0 (c (n "finite-state-machine") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "15fqsa621nb0s0f1nh7hlngn0hvdn3wkmd0ajrpz6d3k3b2ni36n") (f (quote (("verbose"))))))

(define-public crate-finite-state-machine-0.1.1 (c (n "finite-state-machine") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0mz2n98z71z4pwlgk255hfg96ys68y42ignwsgg3hjp53iy4i4c5") (f (quote (("verbose"))))))

(define-public crate-finite-state-machine-0.2.0 (c (n "finite-state-machine") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0k4pqyz0h1s83kxvjf1yrqrxf2p9ij1qvk76hh86jzf0k09k4na5") (f (quote (("verbose") ("derive_default") ("default" "derive_default"))))))

