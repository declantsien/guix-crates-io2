(define-module (crates-io fi ni finitio) #:use-module (crates-io))

(define-public crate-finitio-0.1.0 (c (n "finitio") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.4") (d #t) (k 0)) (d (n "resolver") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-hashkey") (r "^0.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1d1487hsyllxq4cqpwqrw067srxzg3z1wjbph7z9hf5gcvrj256i")))

