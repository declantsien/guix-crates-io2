(define-module (crates-io fi ni finite_repr) #:use-module (crates-io))

(define-public crate-finite_repr-0.1.0 (c (n "finite_repr") (v "0.1.0") (d (list (d (n "finite_repr_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "finite_repr_derive") (r "^0.1.0") (d #t) (k 2)))) (h "0adrisirc5snf9jgnmsxcgx5mnxrai5rvy01wyjcq2r2if0d0pn7") (f (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1.1 (c (n "finite_repr") (v "0.1.1") (d (list (d (n "finite_repr_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "finite_repr_derive") (r "^0.1") (d #t) (k 2)))) (h "059dksmsmavxi9jbnhrzb57j7161j73a6id1kvh5hpl4cg199hp5") (f (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1.3 (c (n "finite_repr") (v "0.1.3") (d (list (d (n "finite_repr_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "finite_repr_derive") (r "^0.1") (d #t) (k 2)))) (h "0mqbxj6dggawd0ng2by4hb45wd7ip0xgwmddgdbpwm4n51v0kifv") (f (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1.4 (c (n "finite_repr") (v "0.1.4") (d (list (d (n "finite_repr_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "finite_repr_derive") (r "^0.1") (d #t) (k 2)))) (h "0mnirlka7ghj13r2502z6vycfbnj2prvr8yaz5fymrw8kq0gkvcj") (f (quote (("strict") ("derive" "finite_repr_derive"))))))

