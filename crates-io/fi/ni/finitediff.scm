(define-module (crates-io fi ni finitediff) #:use-module (crates-io))

(define-public crate-finitediff-0.1.0 (c (n "finitediff") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0aw3z2f1kwirrkr4g2x562llp0ps2239p1kshdp9lx6xx8ybx065")))

(define-public crate-finitediff-0.1.1 (c (n "finitediff") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "09zz64wcpbwpr3apf4skqqw0iljf3n8qgdk9i8px15xxppm4adwp")))

(define-public crate-finitediff-0.1.2 (c (n "finitediff") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0haqz2wv511bns8mqll96py1a7ifmmrq89cdw46masn9khhykgs3")))

(define-public crate-finitediff-0.1.3 (c (n "finitediff") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1mssk15fm1bd63ris4pvmasxj67g1yf66jx72fxajvapxw7dzmhd")))

(define-public crate-finitediff-0.1.4 (c (n "finitediff") (v "0.1.4") (d (list (d (n "ndarray") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "0nknp8n40q1b0kbiypn24bh0bl8z19p4crjs95gri65k0wmkz6z2")))

