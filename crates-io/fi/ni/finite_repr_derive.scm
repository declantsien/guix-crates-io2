(define-module (crates-io fi ni finite_repr_derive) #:use-module (crates-io))

(define-public crate-finite_repr_derive-0.1.0 (c (n "finite_repr_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0x8hdbk24xhp4ml77adq1zl7ckdlkpnipdzac7la1hsr311pnljq") (f (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1.1 (c (n "finite_repr_derive") (v "0.1.1") (d (list (d (n "finite_repr") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0z3vvyczsjw1bd2ribbbf62b3p3ia002j5prbw9q6qhcfq1hb66p") (f (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1.3 (c (n "finite_repr_derive") (v "0.1.3") (d (list (d (n "finite_repr") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1an1hihzpz00lay8xzzkkkx4nr9jsi2ks007hwd1phsxqwz45a95") (f (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1.4 (c (n "finite_repr_derive") (v "0.1.4") (d (list (d (n "finite_repr") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "108lsfhfmdc2rrxz1f48hiflsc0zhid5mrksmnwnzkax1gc9ymmf") (f (quote (("strict"))))))

