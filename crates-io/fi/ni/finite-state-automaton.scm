(define-module (crates-io fi ni finite-state-automaton) #:use-module (crates-io))

(define-public crate-finite-state-automaton-0.1.0 (c (n "finite-state-automaton") (v "0.1.0") (h "0v6bqmff2c8pfsvcffk6jlz10yayngaq4wy6imbj7dprkw6gm530")))

(define-public crate-finite-state-automaton-0.1.1 (c (n "finite-state-automaton") (v "0.1.1") (h "00dm6azbsb8hqa7hydhdcnrq1zk1asfp8grdsws3riaq4n4w6a7i")))

(define-public crate-finite-state-automaton-0.1.2 (c (n "finite-state-automaton") (v "0.1.2") (h "0vh5yhkb42r791s5s54b63mnqgi87za0brnmh5j36hwyi7qj6lj3")))

