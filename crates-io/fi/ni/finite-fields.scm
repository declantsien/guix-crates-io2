(define-module (crates-io fi ni finite-fields) #:use-module (crates-io))

(define-public crate-finite-fields-0.1.0 (c (n "finite-fields") (v "0.1.0") (h "10kgg3ai4y0q1bjkhqhgc3kh04zizmn4hmz9zkg8q8iwy99n1ikv")))

(define-public crate-finite-fields-0.2.0 (c (n "finite-fields") (v "0.2.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1k9y5499ffbzy8ygzy50im6c8rklk2775mm7m5w7qcw11sx6wvm5")))

(define-public crate-finite-fields-0.3.0 (c (n "finite-fields") (v "0.3.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0xxjmk7q7gvli5sf5gclf2b3gmm7jc7yqqrms05rqmibq0di34j6")))

(define-public crate-finite-fields-0.4.0 (c (n "finite-fields") (v "0.4.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "03zkiq14v982niv25ksp57kg90yc87msgzj4gg1ixflq6v0qhk86")))

(define-public crate-finite-fields-0.4.1 (c (n "finite-fields") (v "0.4.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "13fpss0ryxbkp37pbyfcnn7vxwm1nhkm30x3q1ac1q6wh2l5spkq")))

(define-public crate-finite-fields-0.5.0 (c (n "finite-fields") (v "0.5.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1chc8iw81crql7ryjl7dvrxwsfl0mj7nard0lgqf8bkgx6pl47fx")))

(define-public crate-finite-fields-0.5.1 (c (n "finite-fields") (v "0.5.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "03601qg9lgc5p4dqjs6klh1hhm0gi2pz5w8lqmbvlys76mahfp5d")))

(define-public crate-finite-fields-0.5.2 (c (n "finite-fields") (v "0.5.2") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "08c1s98qcygyplmlgbdnl9glf4mszpkbcwzkd4lpdzpxbsiqkh0n")))

(define-public crate-finite-fields-0.6.0 (c (n "finite-fields") (v "0.6.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0pxha1giii25khbbnnnf7sf3zf9lsyg11z5sywifzg72r7p9aaj8")))

(define-public crate-finite-fields-0.6.1 (c (n "finite-fields") (v "0.6.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "12wwhsimil2229jgarf6k11jvbc84nl4an0x74p57jjg5azpmn1x")))

(define-public crate-finite-fields-0.6.2 (c (n "finite-fields") (v "0.6.2") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1cfwcy4dqqdr9dsghx44gl87gf2dr5778s654c6dh0qgh11f5khy")))

(define-public crate-finite-fields-0.7.0 (c (n "finite-fields") (v "0.7.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0nha5671nrxnzjb5ar3pqvyp1jybd23ddjk7scrzf6xy59ikfnmr")))

(define-public crate-finite-fields-0.7.1 (c (n "finite-fields") (v "0.7.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0wbk53mca63nhqkzddw1caaqjjp1k8vcmnzjnm8q4zib2bgx8gxm")))

(define-public crate-finite-fields-0.7.2 (c (n "finite-fields") (v "0.7.2") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0285q1j7c214g2cmqmk4qjqi64cgxigjpkhr339rj8qbnrnw7xxf")))

(define-public crate-finite-fields-0.7.3 (c (n "finite-fields") (v "0.7.3") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0na50hd55ba3vn58ms127zk8g3rqaf6ihha8db75dbf83b9q57lr")))

(define-public crate-finite-fields-0.7.4 (c (n "finite-fields") (v "0.7.4") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "04c34mmn2f7l6zhyb4jwnn00flmj7icfzfmmp5xjixl1alwfhw7b")))

(define-public crate-finite-fields-0.7.5 (c (n "finite-fields") (v "0.7.5") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1345achbip0kkd17vd3k8gmpxmqmnn0928jk2655mi11vw41dm3y")))

(define-public crate-finite-fields-0.7.6 (c (n "finite-fields") (v "0.7.6") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0qwd1xg3mwk1fdrr21jp55jj73sw6slb4vaya1avbkw70k6kxjrv")))

(define-public crate-finite-fields-0.7.7 (c (n "finite-fields") (v "0.7.7") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0bl6iab46gz43f96hfn2bbxvpxr18wd7jah4h971m1il74irjkps")))

(define-public crate-finite-fields-0.7.8 (c (n "finite-fields") (v "0.7.8") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "19gwiwhwlp5mfdy2yf6qy3r1lf1831b0ldhvgakqkiarjq96hikv")))

(define-public crate-finite-fields-0.8.0 (c (n "finite-fields") (v "0.8.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0j8x5mbkr19kjqpj41vmjr0hlvvpf9my0kv8r7a4hs60b7fbmr99")))

(define-public crate-finite-fields-0.9.0 (c (n "finite-fields") (v "0.9.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "18fph57qnc4wrrb67nh15ay7yvrrzjkv3v65azpilhna43abj68g")))

(define-public crate-finite-fields-0.9.1 (c (n "finite-fields") (v "0.9.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0vzvna144ipch8b61pk97yj2m8x8jhhfvl1kwh0j4qmfs755yh78") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.9.2 (c (n "finite-fields") (v "0.9.2") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "166l0hbn0gq4pcsj15rscnr2n3n7qz5qmqc99ly7ccvdkgibj4rc") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.9.3 (c (n "finite-fields") (v "0.9.3") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1lwqyji1lalv071fq1q2s6imrf54rdv6d5yj05qff74qcmq7gf7z") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.9.4 (c (n "finite-fields") (v "0.9.4") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1ck9mb6n6kxgghw1sgzjpss5x4gkqcz2rw7yqw0sp9gx3qavp6r9") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.9.5 (c (n "finite-fields") (v "0.9.5") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "020njd99nxpn5sp5mkk2wj2zcl58ip0sy7jhrlf8kysd9gahndg6") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.10.0 (c (n "finite-fields") (v "0.10.0") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1sxj2lzdbjvxfysklw0qj1i5q2kqdyr0s9i03vs9yd4dvw811jrq") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.10.1 (c (n "finite-fields") (v "0.10.1") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1nfl3kqkiyskc9d4w4g72ak9j41b69q358lq7msk41ynnk58msqr") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.10.2 (c (n "finite-fields") (v "0.10.2") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "13f320vjjm4588npvmamcaxlw9sfvkd8zni3xp7fmy03m3dq2lw3") (f (quote (("nightly"))))))

(define-public crate-finite-fields-0.10.3 (c (n "finite-fields") (v "0.10.3") (d (list (d (n "error_def") (r "^0.3.12") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0lrlqkh4wfjzxpwzac41gnbv4fs1bjd6ssrl0gc63adjq9b5nnid") (f (quote (("nightly"))))))

