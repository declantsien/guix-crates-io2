(define-module (crates-io fi ni finitefields) #:use-module (crates-io))

(define-public crate-finitefields-0.1.0 (c (n "finitefields") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.91") (d #t) (k 0)))) (h "1gcyxzngz5zkhf1zrmcfwgspsip7wzxw4hrp4x0f9ycq9pp0pc4q")))

(define-public crate-finitefields-0.1.1 (c (n "finitefields") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.91") (d #t) (k 0)))) (h "1scgcb656c2lx6szs2rkr0b8ympsrb4fkp4am1l7iwbz7w3vynsx")))

