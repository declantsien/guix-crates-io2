(define-module (crates-io fi ni finitelib) #:use-module (crates-io))

(define-public crate-finitelib-0.1.0 (c (n "finitelib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g51b4kfcw143m1fxdlm1f3lggrbmzb7q2f8i6wh1w6cjf3qhi1d")))

(define-public crate-finitelib-0.1.1 (c (n "finitelib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sl16yc1z6jjzcgv23sgh80icv1skm4cbjcn71xjab52lrvzh0hn")))

