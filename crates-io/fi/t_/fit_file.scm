(define-module (crates-io fi t_ fit_file) #:use-module (crates-io))

(define-public crate-fit_file-0.1.0 (c (n "fit_file") (v "0.1.0") (h "08axr5cqycvx25hg2j87awrvkkxcz6pifxrj40y2hxmz7gfaz28z")))

(define-public crate-fit_file-0.1.1 (c (n "fit_file") (v "0.1.1") (h "1a08h1ij8a0rr4hyvrl9wpkvf8292w7zq9rqnrm9qmx5r3w976gp")))

(define-public crate-fit_file-0.1.2 (c (n "fit_file") (v "0.1.2") (h "1fzcbkvwmj3c066rf213bwk6m0cwd8zj62341qp0n8rdb5j95dw5")))

(define-public crate-fit_file-0.1.3 (c (n "fit_file") (v "0.1.3") (h "0m7zhnvqq59k7vrrvpnw337x92v7l99qjkhcgpyny7ymdymcjl3v")))

(define-public crate-fit_file-0.1.4 (c (n "fit_file") (v "0.1.4") (h "07yiilp4v2m0zf3qm5digs1bssmirldphjrqzhzw2kvgjn6rcp48")))

(define-public crate-fit_file-0.2.0 (c (n "fit_file") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "03j3rgzmdrvgvdgr3csjiqv1i852fiz38mwjl1ml7ms2px48l0rh")))

(define-public crate-fit_file-0.2.1 (c (n "fit_file") (v "0.2.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "07d847cs6iwj46bbay8bng973ry24ngm3kag4w43zh9ls4y356bl")))

(define-public crate-fit_file-0.3.0 (c (n "fit_file") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1vng8w8x8f7qssq0clhyb8ygvp17ma8kg51ji1dc8vhkypngfkvk")))

(define-public crate-fit_file-0.3.1 (c (n "fit_file") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "015rplr4q4m2svmqp2hcihyja9dfivvfzpidyd7n1yzqgvilks4s")))

(define-public crate-fit_file-0.3.2 (c (n "fit_file") (v "0.3.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "16pzzvjch2fv7l1a6vr6haryhlqlfp65aq4adi75zy93km6ybcqy")))

(define-public crate-fit_file-0.3.3 (c (n "fit_file") (v "0.3.3") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "03mzg13syca2x3gjiplrn0zczrkrkw6kv32dzdl6a91nblaixdr1")))

(define-public crate-fit_file-0.3.4 (c (n "fit_file") (v "0.3.4") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1d9y7i4l7ifwik3b0ylg2c7dbyjs0qhfm4dw9fwgw6h2knnc13sb")))

(define-public crate-fit_file-0.3.5 (c (n "fit_file") (v "0.3.5") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0brx0lv39x0a7axmxskfdsryyy2cp7fx5y45vpk1sd9zl09fp5r2")))

(define-public crate-fit_file-0.4.0 (c (n "fit_file") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1x22j7wlbzgxh5s2r5gf084lajk1hc3s8kgcdhmjc5iv8v9c7hq5")))

(define-public crate-fit_file-0.5.0 (c (n "fit_file") (v "0.5.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0gblvs4m5ffdi9af42nd594sqm3sjn4l9zr48z467nivp8jlbi7z")))

(define-public crate-fit_file-0.6.0 (c (n "fit_file") (v "0.6.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0jp3mddznx2vraqyx8bkvypkw9yq09s06pn11wcp2mh53hlr5hrn")))

