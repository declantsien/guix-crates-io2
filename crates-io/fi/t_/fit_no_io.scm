(define-module (crates-io fi t_ fit_no_io) #:use-module (crates-io))

(define-public crate-fit_no_io-0.1.0 (c (n "fit_no_io") (v "0.1.0") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "mmap") (r "^0.1.0") (d #t) (k 2)))) (h "1m3s47j012scvar27y2c4j2g7rqzylyyxlf5j7k2ziw6m3cy25fj")))

