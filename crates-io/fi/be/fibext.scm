(define-module (crates-io fi be fibext) #:use-module (crates-io))

(define-public crate-fibext-0.1.0 (c (n "fibext") (v "0.1.0") (h "0qqzdyc8xa8185sj413lvjxwmm3w6y5hnzk3jw27jkhfwmaihzd8")))

(define-public crate-fibext-0.2.0 (c (n "fibext") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "19ak3i35wzgqf9vv4i6wx1ryzc9dyfnbaw2zd4ch1qnjkl5d6c8g") (f (quote (("std") ("iterator") ("default" "std" "checked-overflow" "iterator") ("checked-overflow")))) (s 2) (e (quote (("large-numbers" "dep:num-bigint"))))))

(define-public crate-fibext-0.2.1 (c (n "fibext") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00n1blxi5vs6sq5hgzl6svs6jxzl2pr32y0vj8wk35042mpvwpbg") (f (quote (("std") ("iterator") ("default" "std" "checked-overflow" "iterator") ("checked-overflow")))) (s 2) (e (quote (("large-numbers" "dep:num-bigint"))))))

