(define-module (crates-io fi be fibers_timeout_queue) #:use-module (crates-io))

(define-public crate-fibers_timeout_queue-0.1.0 (c (n "fibers_timeout_queue") (v "0.1.0") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0b2rb6hwvj3jyrh0d4zfrzjay0cbx6kwhx98064aix2nq1pm118c")))

