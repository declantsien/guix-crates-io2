(define-module (crates-io fi be fibers_tasque) #:use-module (crates-io))

(define-public crate-fibers_tasque-0.1.0 (c (n "fibers_tasque") (v "0.1.0") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tasque") (r "^0.1") (d #t) (k 0)))) (h "1z75hwn8zdhc05p50nfq60mfw5ybw3ldrg0k6z8j0qsmkzx6n9kv")))

(define-public crate-fibers_tasque-0.1.1 (c (n "fibers_tasque") (v "0.1.1") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tasque") (r "^0.1") (d #t) (k 0)))) (h "1y9jpw7lp6xl306lxg4idnzfi05iaqjwihx6glrfjasy10592sgc")))

(define-public crate-fibers_tasque-0.1.2 (c (n "fibers_tasque") (v "0.1.2") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tasque") (r "^0.1") (d #t) (k 0)))) (h "1b0wy8bb2q6mz078lz8zwz4b0v71nk48bkvdrw5cgq2ss7y7hhjh")))

