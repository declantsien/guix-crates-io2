(define-module (crates-io fi be fibers_inotify) #:use-module (crates-io))

(define-public crate-fibers_inotify-0.1.0 (c (n "fibers_inotify") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "inotify") (r "^0.5") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0mpir7lxdd9q42xnx0mv9jdpcybls4bkv4zbhwvr9q4r3ra1kr6s")))

(define-public crate-fibers_inotify-0.1.1 (c (n "fibers_inotify") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "inotify") (r "^0.5") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "19kds3x5iqi9nlqh4p07fvwg2ja3v5ibpsb02v2dqq37p1jh4b3p")))

(define-public crate-fibers_inotify-0.1.2 (c (n "fibers_inotify") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "inotify") (r "^0.5") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^1") (d #t) (k 0)))) (h "1hkpg74k6g8p3399f31w9nb16pl79ar4zwggadsgqx7zc4cdwsfs")))

