(define-module (crates-io fi be fibers_global) #:use-module (crates-io))

(define-public crate-fibers_global-0.1.0 (c (n "fibers_global") (v "0.1.0") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0asdsjq61ayimfw1w1rq9p4hxx3ah7hxfy41l3bgpcmwynkgcail")))

(define-public crate-fibers_global-0.1.1 (c (n "fibers_global") (v "0.1.1") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1z6ixh6afjxzsgg38h98934qsh7gb9dzbi9yb5lip7fd4rbk6zjr")))

(define-public crate-fibers_global-0.1.2 (c (n "fibers_global") (v "0.1.2") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)))) (h "1mcc1447bp624vp2h1j8k7vgi4d96dj7fkcnbjyhikssximf8xay")))

