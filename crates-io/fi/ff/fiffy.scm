(define-module (crates-io fi ff fiffy) #:use-module (crates-io))

(define-public crate-fiffy-0.1.1 (c (n "fiffy") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "147ym59k8ig7nw7iwla9d1ln3h0ql6rcj8291lxlp6v7pvbqzivf") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.2 (c (n "fiffy") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0xbzrizwmank1qdm4x3h3w2jkb11jpn1q1j1mid22sixkh2r032z") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.3 (c (n "fiffy") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "09x89a36405mr2bqrc5iscs2j1ibdp1s9z3vyq6i87l6myzclh3q") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.4 (c (n "fiffy") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0m96y2yx48rkapdl1rs0z2v0bclvmdwdw79h8hwwbwa7mqclwdb8") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.5 (c (n "fiffy") (v "0.1.5") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0dc49a11929nz12n46asw7y63jiywallx0ahs8fc8i4l8z6fs982") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.6 (c (n "fiffy") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "08vjzkbz7gykh0ll4km6xqahw290z9fksp5845gs76i81wiyynq9") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.7 (c (n "fiffy") (v "0.1.7") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1vk1byri3wlw0qrn6hgl2qyaz4hw28782dfkkc5c7i7hzry06rrr") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.8 (c (n "fiffy") (v "0.1.8") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0kn9y96rpl4vv900ccw92wss2gs84m0gc7wr5nlsrzv2a95x4mi0") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.9 (c (n "fiffy") (v "0.1.9") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0ap2l55j7qiqk6ga4iwpqsghla96ksnhhinmn1a1pn52p7cbc4y8") (f (quote (("default"))))))

(define-public crate-fiffy-0.1.10 (c (n "fiffy") (v "0.1.10") (d (list (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1rlixn6ymiv484b4j70311gc2sa4f98vnb604hb71gcby9dgmqx1") (f (quote (("default"))))))

