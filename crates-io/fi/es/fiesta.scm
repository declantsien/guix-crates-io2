(define-module (crates-io fi es fiesta) #:use-module (crates-io))

(define-public crate-fiesta-0.1.0 (c (n "fiesta") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11drs3ggl9fvy895l8awf7c70v2n8jxqgp2vqd98a7hliif220xf")))

