(define-module (crates-io fi tp fitparse) #:use-module (crates-io))

(define-public crate-fitparse-0.1.0 (c (n "fitparse") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "042c4piy4z0i17bbv4a03yzsgq5ga4z5jly6kg2knv45nq6ryyzk")))

