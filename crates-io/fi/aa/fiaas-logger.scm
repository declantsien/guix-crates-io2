(define-module (crates-io fi aa fiaas-logger) #:use-module (crates-io))

(define-public crate-fiaas-logger-0.1.0 (c (n "fiaas-logger") (v "0.1.0") (d (list (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ilh55irjc4ybvjy802rdggyaf7d2jb87zgjh6imh22gv59kz2s3") (y #t)))

(define-public crate-fiaas-logger-0.1.1 (c (n "fiaas-logger") (v "0.1.1") (d (list (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j73f6r61nwdx9qwrjcysnyy0xw1a70b32kmhbrbgqrcisvsjdl5")))

