(define-module (crates-io fi en fiender) #:use-module (crates-io))

(define-public crate-fiender-0.1.0 (c (n "fiender") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvqxx5a67jvafkwmwjpzpzip502z5gi1hz4m09gqfs86rxv91cj")))

(define-public crate-fiender-0.2.0 (c (n "fiender") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1phyxlzdwd1iqzm2nhm9ki5l6iyln1i3lqb0rfd1p66f4cy4p3f2")))

