(define-module (crates-io fi d- fid-rs) #:use-module (crates-io))

(define-public crate-fid-rs-0.1.0 (c (n "fid-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "035aif0lw28pn4iy273il6qxaxbjz74c7kylaapxm79zzz128b95")))

(define-public crate-fid-rs-0.1.1 (c (n "fid-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1kg25bxm6xbh4mgkl0bxa03l4q9d1lx87pmd0mbk081l1j66aa3c")))

(define-public crate-fid-rs-0.2.0 (c (n "fid-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "mem_dbg") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0iw4wh2w24n9j00f3lw3hn7m3bixl129s5ml8hml3lg2c0g6m5d6") (f (quote (("default" "rayon"))))))

