(define-module (crates-io fi si fisica) #:use-module (crates-io))

(define-public crate-fisica-0.1.0 (c (n "fisica") (v "0.1.0") (d (list (d (n "float_eq") (r "^0.7") (d #t) (k 2)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "03v0djw8sdcbnjkzdva7j499a0fzdga28f98l5c9g36a1bmpbk1w") (r "1.57")))

(define-public crate-fisica-0.1.1 (c (n "fisica") (v "0.1.1") (d (list (d (n "float_eq") (r "^0.7") (d #t) (k 2)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0p1857vzpwsvjarac3589rs1yz21ahv4zzq5zaw8d8xkvy895bwa") (f (quote (("std") ("safe") ("nightly") ("default" "std" "safe")))) (r "1.57")))

(define-public crate-fisica-0.2.0 (c (n "fisica") (v "0.2.0") (d (list (d (n "devela") (r "^0.7.1") (d #t) (k 0)) (d (n "float_eq") (r "^0.7") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "02dq7p1dijj7i2w7l98nkv6bcp2h2mqx77n51r8s2yvq89bbz0bg") (f (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("no-std") ("nightly_docs" "nightly" "std" "unsafe") ("nightly") ("default" "std" "safe") ("alloc")))) (r "1.71.1")))

