(define-module (crates-io fi ft fift-proc) #:use-module (crates-io))

(define-public crate-fift-proc-0.1.0 (c (n "fift-proc") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "006z3jdsq1c4njj4bhc9p783r1r45l2ya9pflpr4zi9vy5ahriay")))

(define-public crate-fift-proc-0.1.7 (c (n "fift-proc") (v "0.1.7") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16dkkf9rcq5af71ldb0791z5payxr5v3f3nj28ppy0mcdg946qmj")))

(define-public crate-fift-proc-0.1.15 (c (n "fift-proc") (v "0.1.15") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xmxwm8rnf9b3mndawd6qxaxlpfmyj2x3bapvhb4d6yg6xjbx4wc")))

