(define-module (crates-io fi ft fift-libs) #:use-module (crates-io))

(define-public crate-fift-libs-0.1.16 (c (n "fift-libs") (v "0.1.16") (h "06wfp82drn49h3m61a7q0nfpfr28jwyxijczj6djw869rl647q47") (r "1.70")))

(define-public crate-fift-libs-0.1.19 (c (n "fift-libs") (v "0.1.19") (h "1vwsfl5l4v80hz88wdn1dzbvny2v8gd9lpsc8wkfkiqf0rxyzyj1") (r "1.70")))

(define-public crate-fift-libs-0.1.22 (c (n "fift-libs") (v "0.1.22") (h "0h5lky1bncwg6vjdbv40lglkmxdqdxqj9gx62zrfa7xjiwy2ll47") (r "1.70")))

(define-public crate-fift-libs-0.1.23 (c (n "fift-libs") (v "0.1.23") (h "1hgzgrjmyaa968yyppb0dicjhs5vxr9qzad46m2ccgf1nq7v1ff3") (r "1.70")))

