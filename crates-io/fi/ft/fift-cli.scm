(define-module (crates-io fi ft fift-cli) #:use-module (crates-io))

(define-public crate-fift-cli-0.1.1 (c (n "fift-cli") (v "0.1.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fift") (r "=0.1.1") (d #t) (k 0)))) (h "09kn8pm4rs42w0ynnl71dsjjql5da9izmfyli2y4kj77c4xc2f30") (r "1.70")))

(define-public crate-fift-cli-0.1.3 (c (n "fift-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fift") (r "^0.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "1i2b1hiz1vbp5bv3xa9kykan228li5fx9xkn8gyj7pfnrd3vvhlv") (r "1.70")))

(define-public crate-fift-cli-0.1.4 (c (n "fift-cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "fift") (r "^0.1.4") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "16a6zpsfp5g71ivp7a6ddgf8ahdcmzy5pp7dxr7saj3nbv5sgxfm") (r "1.70")))

(define-public crate-fift-cli-0.1.5 (c (n "fift-cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "^0.1.5") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "152hdqiys5ayfm8mv9gid34nacd6fwimvqslhmljiz7d2rl2hwmn") (r "1.70")))

(define-public crate-fift-cli-0.1.6 (c (n "fift-cli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "^0.1.5") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "1dn696jzl27jkzgfg3mh0qvfh9zvhp347419qxvziq8fciik0p6a") (r "1.70")))

(define-public crate-fift-cli-0.1.7 (c (n "fift-cli") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "^0.1.7") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "0yvvjp26s7a8jjh0jb1i8pk4z5dk2ych516dx0m5z0cxzaygvm9d") (r "1.70")))

(define-public crate-fift-cli-0.1.8 (c (n "fift-cli") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "^0.1.8") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "0j021bmhg0i57nvf1h32mh3x0aj1a4nkzpmrvk90758flymv59dz") (r "1.70")))

(define-public crate-fift-cli-0.1.9 (c (n "fift-cli") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.9") (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (k 0)))) (h "0hsrxn19175fa5gw40wpf508iz61fxrnjd5rm38icnyhnjs7r538") (r "1.70")))

(define-public crate-fift-cli-0.1.10 (c (n "fift-cli") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.10") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "0v7g9y1i3ww3a9kqkd34r9pvbb1av3fkkjj0iry980533pl7qlhl") (r "1.70")))

(define-public crate-fift-cli-0.1.11 (c (n "fift-cli") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.11") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "196641i4vqw8bcf5vg5r63kq65iixgsr3m8mhmrkg2d3rhvb4br0") (r "1.70")))

(define-public crate-fift-cli-0.1.12 (c (n "fift-cli") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.12") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "0ymwgzdsqswlplg9nsh00zn124naz9s20y2hgrzlp02crl6incaj") (r "1.70")))

(define-public crate-fift-cli-0.1.13 (c (n "fift-cli") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.13") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "0gyz21wky8ily7fs0w6bh3x2y67nz9ahc42x6qkx1c1dm42xgh1f") (r "1.70")))

(define-public crate-fift-cli-0.1.14 (c (n "fift-cli") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.14") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "00z6j5hfx5jb3fjmxb8viannca1d4q6lj8qj7ykmiwahqi30v3s0") (r "1.70")))

(define-public crate-fift-cli-0.1.15 (c (n "fift-cli") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.15") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "1rr4sisgdc03v2z32gph1w65489lx990lw5dp690ba8blk70ifcp") (r "1.70")))

(define-public crate-fift-cli-0.1.16 (c (n "fift-cli") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.16") (d #t) (k 0)) (d (n "fift-libs") (r "=0.1.16") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)))) (h "1bpavyj2q8v61rc8i9c0dwpr733v1kz2d45phk5zqf4yzv46wl9a") (r "1.70")))

(define-public crate-fift-cli-0.1.17 (c (n "fift-cli") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.17") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.16") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1hbi841qvigakv588hdjjrxms2asawpnsq73sap7sh0lb6i44wfw") (r "1.70")))

(define-public crate-fift-cli-0.1.18 (c (n "fift-cli") (v "0.1.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.18") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.16") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1vj9libqzmfl53902d9p0z1ysmpgp2gr8db1idsw2xihhdam5yn6") (r "1.70")))

(define-public crate-fift-cli-0.1.19 (c (n "fift-cli") (v "0.1.19") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.19") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.19") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0h9xg61dvb7x2ipyh1xgbcvnxvs4x2cbardgki0yfsspsdd9x44j") (r "1.70")))

(define-public crate-fift-cli-0.1.20 (c (n "fift-cli") (v "0.1.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.20") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.19") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0vb1w64kmq6xwa078avksvdfvrrw2k9adhysprlvbggh4zb6n091") (r "1.70")))

(define-public crate-fift-cli-0.1.21 (c (n "fift-cli") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.21") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.19") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "16sb7slnldwzbnw4pv933cpny4699qb86hga96djh0mqdwb3gn15") (r "1.70")))

(define-public crate-fift-cli-0.1.22 (c (n "fift-cli") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.21") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.22") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "10rbpi3sm3rqpw69ki49f5sw2dq0anxckv77ji4lwkllfji283xl") (r "1.70")))

(define-public crate-fift-cli-0.1.23 (c (n "fift-cli") (v "0.1.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "fift") (r "=0.1.23") (d #t) (k 0)) (d (n "fift-libs") (r "^0.1.23") (d #t) (k 0)) (d (n "rustyline") (r "^12.0") (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "00r1ms9p2fh2ab1z8drmm9rzr7xpkz2win1wbd0i6s5mb8q7yzm1") (r "1.70")))

