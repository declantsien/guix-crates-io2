(define-module (crates-io fi ft fiftyonedegrees) #:use-module (crates-io))

(define-public crate-fiftyonedegrees-0.1.0 (c (n "fiftyonedegrees") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "libc") (r "^0.2.64") (d #t) (k 0)))) (h "0k5sivrxgx6h4nim0xpbg9xvybr5c08k84zqm90ccsf36i1d261j") (f (quote (("trie") ("platform-name-enum") ("pattern") ("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.1.1 (c (n "fiftyonedegrees") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "libc") (r "^0.2.64") (d #t) (k 0)))) (h "1h8jfn7rik3crqjkgmgz93srw2bc0zicinjanf1d2nk856z0i15a") (f (quote (("trie") ("platform-name-enum") ("pattern") ("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.0 (c (n "fiftyonedegrees") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0z4x7c4knd92pvd8xsic2k7cvpz7ab717h3gzv5iik6qm15qqr1w") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.1 (c (n "fiftyonedegrees") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0mxx71s30gaikpzglffss4cv7s5x140f5dm76pwlhxzkzh32bmpx") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.2 (c (n "fiftyonedegrees") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "07r7m9p0m2dixddsc0si3kvi9i76bw7rlq1z482wac0410wwmqzh") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.3 (c (n "fiftyonedegrees") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1i2lfpb26dcphc5sw6kdjb8rzn40wxvvlvc11m2nc6h9485yidjj") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.4 (c (n "fiftyonedegrees") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "02y6ysg8yd2jcsqsnpfr7gqw9wdg6108v81522gs9650jjp03cla") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.5 (c (n "fiftyonedegrees") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)))) (h "1kwdns2khwc61ks3dxyqknwc8qjk37b0apl4md1m55y7vf8vqzfg") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.6 (c (n "fiftyonedegrees") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)))) (h "1hbbjmycnpw3frp5dw03gw7hihz52cdfngndwccxdvwfsybp26zm") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.7 (c (n "fiftyonedegrees") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)))) (h "1mfgzs36lfqfmz5nvh7lb2gq1lq6cp577a0rxkyph0xi82jwiy0k") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.9 (c (n "fiftyonedegrees") (v "0.2.9") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0z5kjzjpmd0j83nsjk806880dksv5jzl87fjkn0zhkxjbbngqppd") (f (quote (("browser-name-enum-mode") ("browser-name-enum"))))))

(define-public crate-fiftyonedegrees-0.2.10 (c (n "fiftyonedegrees") (v "0.2.10") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1zbg3qipvyjxygz8v5yzd9q2ap8y6x69j1y6m15izl4svrsdqr64") (f (quote (("static"))))))

(define-public crate-fiftyonedegrees-0.2.11 (c (n "fiftyonedegrees") (v "0.2.11") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0az2sdl6qii93q6hsl60nnskk71dvnm5r0z2x7mjr6vhkyllddch")))

(define-public crate-fiftyonedegrees-0.2.12 (c (n "fiftyonedegrees") (v "0.2.12") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0ln5fnmhpcpkc6q2j0hx22fnbnzlv8vrj0s9vqpn7fqdwjlaz636")))

(define-public crate-fiftyonedegrees-0.2.13 (c (n "fiftyonedegrees") (v "0.2.13") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0hicfggkaxnlwkz6xcd9v37nq7wng01lwwr4wz0vyhd4nf9nn3aj")))

(define-public crate-fiftyonedegrees-0.2.14 (c (n "fiftyonedegrees") (v "0.2.14") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1v5rb88x8y2xlg90qq99s14xy94xfcf4b4wx0xiyh9ik7af08rwd")))

(define-public crate-fiftyonedegrees-0.2.15 (c (n "fiftyonedegrees") (v "0.2.15") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1svw98w1bzcmknqj8cjpq9dd4pbxkgmkmxw5nn1zryd5xjgalkp1")))

(define-public crate-fiftyonedegrees-0.2.16 (c (n "fiftyonedegrees") (v "0.2.16") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "05yi68aaa9pqlvqfx63c9dih01wjn3h2phqimyj4hvarn1yshld0")))

(define-public crate-fiftyonedegrees-0.2.17 (c (n "fiftyonedegrees") (v "0.2.17") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0ybx738580nqcm4zzy5h2g80ym9bs1zgm47j3w18xl7ysdahl1v7")))

(define-public crate-fiftyonedegrees-0.2.18 (c (n "fiftyonedegrees") (v "0.2.18") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1p54g11iw1n0b0cnchnscdja353h9wc1xzybzs0ffkdgpixjmvqk")))

(define-public crate-fiftyonedegrees-0.2.19 (c (n "fiftyonedegrees") (v "0.2.19") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)))) (h "0xnfckbqx4d6sfxs9ibqv9dlabhssssgnl4chihp4w6lzhadfi3v")))

(define-public crate-fiftyonedegrees-0.2.20 (c (n "fiftyonedegrees") (v "0.2.20") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 1)))) (h "1ml8i5ck19s1ifhkrqf08ivpjjzfcniwrk9hi7nq4hryydbal5ql")))

