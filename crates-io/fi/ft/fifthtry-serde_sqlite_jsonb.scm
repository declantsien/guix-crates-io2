(define-module (crates-io fi ft fifthtry-serde_sqlite_jsonb) #:use-module (crates-io))

(define-public crate-fifthtry-serde_sqlite_jsonb-0.1.0 (c (n "fifthtry-serde_sqlite_jsonb") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled" "blob"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json5") (r "^0.1") (o #t) (d #t) (k 0)))) (h "03xhxlxlgb25wdfn4qrsjai60jpi03sz5vfqmh1s2m5yd85f0sfs") (f (quote (("default" "serde_json")))) (r "1.63")))

