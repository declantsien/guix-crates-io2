(define-module (crates-io fi em fiemap) #:use-module (crates-io))

(define-public crate-fiemap-0.1.0 (c (n "fiemap") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "036gd3gjnbinv7ag6nj89p209hbpq095v8ymxwmnydwc9vrrd19p")))

(define-public crate-fiemap-0.1.1 (c (n "fiemap") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1l95pyzx8hd4w0xynp20b7bb7536b9a3d14gw8rmc5jr59134ih8")))

(define-public crate-fiemap-0.1.2 (c (n "fiemap") (v "0.1.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "08ki52zzrcnccsi100c5wnr156h10pz7cykq11xnb0zk39xmbjl9")))

