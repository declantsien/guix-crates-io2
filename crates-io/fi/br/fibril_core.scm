(define-module (crates-io fi br fibril_core) #:use-module (crates-io))

(define-public crate-fibril_core-0.0.0 (c (n "fibril_core") (v "0.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c9ip9nzj477d1ndld9x857aiby27r2vm33b4gdp5y6kbqxcsm54")))

(define-public crate-fibril_core-0.0.1 (c (n "fibril_core") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xfcb7d9jhgdv3hpqr9mm3vpwg7w08svw2lrgyyknhwaf6gvz8q9")))

(define-public crate-fibril_core-0.0.2 (c (n "fibril_core") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rfnp63flsj8d944bkbnlapzghzi5v5gd2ir7ih1wy7379fh8sip") (s 2) (e (quote (("serde_json" "dep:serde_json" "serde") ("serde" "dep:serde") ("rt" "dep:futures" "dep:tokio" "dep:tracing"))))))

(define-public crate-fibril_core-0.0.3 (c (n "fibril_core") (v "0.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qk6glppiavmir373hw7nq615vd6757h1h76318lb9df2kc2y9bb") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fibril_core-0.0.4 (c (n "fibril_core") (v "0.0.4") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l4b0f5kjq2pm5fqqlldm2n1sph2bdin47b9cdcwqlgby56hk1hn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fibril_core-0.0.5 (c (n "fibril_core") (v "0.0.5") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10d6lr4j4hp7z91b1x28i9q508l8ycc9m3vhx2dp86b11vxvbbmc") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fibril_core-0.0.6 (c (n "fibril_core") (v "0.0.6") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i6mpr6ba87infkarhp5kv6a910ig1fd1j0rp99qnj3p6xi2rzrr") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fibril_core-0.0.7 (c (n "fibril_core") (v "0.0.7") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sjsrrx28i2yyg1njq4fzbfsjpw5i5yxjrxhhxicfm5mmy3ww941") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

