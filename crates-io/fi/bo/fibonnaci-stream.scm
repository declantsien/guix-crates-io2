(define-module (crates-io fi bo fibonnaci-stream) #:use-module (crates-io))

(define-public crate-fibonnaci-stream-0.1.0 (c (n "fibonnaci-stream") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)))) (h "1820b0yshwm3grk5psi0jszm1r3kv001q83bs6qz7pa4c2flw5a2")))

