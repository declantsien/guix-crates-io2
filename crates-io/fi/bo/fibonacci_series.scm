(define-module (crates-io fi bo fibonacci_series) #:use-module (crates-io))

(define-public crate-fibonacci_series-0.1.0 (c (n "fibonacci_series") (v "0.1.0") (h "1wmk6r5niwg3sy0z9hiz18sd8ahb63m159w7606zfsdr8b52sjyx")))

(define-public crate-fibonacci_series-0.2.0 (c (n "fibonacci_series") (v "0.2.0") (h "1hrlpgmj8q92g3nxg14zfks4rpsw9qia5jbhzszwb24fhgj0bls2")))

