(define-module (crates-io fi bo fibonacci_codec) #:use-module (crates-io))

(define-public crate-fibonacci_codec-0.1.0 (c (n "fibonacci_codec") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "1mpgqn5b1a3rlq2wg6v8l5p9p2hwbfvjwxckaz40gn7b8l4iq5c5")))

(define-public crate-fibonacci_codec-0.1.1 (c (n "fibonacci_codec") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "0r13yb23rd5n9ijgwaf2csk6i29xkpwphg427wf2vxghzvns3af4")))

(define-public crate-fibonacci_codec-0.2.0 (c (n "fibonacci_codec") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 1)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1aiisgw116i9vs69xnydw55ipmxrhqqmrsnjl9mdg8q65b38r3l3")))

