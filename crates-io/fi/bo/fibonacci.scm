(define-module (crates-io fi bo fibonacci) #:use-module (crates-io))

(define-public crate-fibonacci-0.1.0 (c (n "fibonacci") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "09zlf3awljc7xh11rs3izddcxrksgizi5kajz1lj649i81ca01p2")))

(define-public crate-fibonacci-0.1.1 (c (n "fibonacci") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "12in929rj6ka87qn2qhr63g1a130fbivvhb4hxk2mpp3xza0mls2")))

