(define-module (crates-io fi bo fibonacci-like) #:use-module (crates-io))

(define-public crate-fibonacci-like-0.1.0 (c (n "fibonacci-like") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "017algir6g3s7dmksxsgk9klhk83ai1lqqgz06wpy10kmrjrmcwn") (f (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std")))) (y #t)))

(define-public crate-fibonacci-like-0.1.1 (c (n "fibonacci-like") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1sv572c8sdqp8bp2fsmk32js2wkcvhwlrpbbzh3winrj2k7csnbn") (f (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

(define-public crate-fibonacci-like-0.1.2 (c (n "fibonacci-like") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00ayzg82agrgf46max1y9s3lggws73pyigkz50nlfnn3d51i0dwb") (f (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

(define-public crate-fibonacci-like-0.1.3 (c (n "fibonacci-like") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1nv4yjdw8vnw1mgf97rwpfbdnwl4f1vix29y5q15ngv2cgacyi7j") (f (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

