(define-module (crates-io fi bo fibo) #:use-module (crates-io))

(define-public crate-fibo-0.0.1 (c (n "fibo") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ipsn403gk05zvkarm5hrgi8sqvmp0dbjfz4dxk9r7wymg90bl22")))

(define-public crate-fibo-0.0.2 (c (n "fibo") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lnbibmsfbc972vk0q34chri0b5x369azc2dsl76pfcz8hf7zvqj")))

