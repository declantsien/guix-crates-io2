(define-module (crates-io fi bo fibonacii-heap) #:use-module (crates-io))

(define-public crate-fibonacii-heap-0.1.0 (c (n "fibonacii-heap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "19wrwwcqrx04jrph62hmyimikwmvcd116bp6v14k8drhxkk6kiqv")))

(define-public crate-fibonacii-heap-0.1.1 (c (n "fibonacii-heap") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1svgvnbrxzqynkwawdk9s8i90gp7n2z7i3qd8ghb6ya13631d086")))

(define-public crate-fibonacii-heap-0.1.2 (c (n "fibonacii-heap") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1p9vh3yvmg15i2kakfzpbw9yw9h3vjf18f29j1ciinns11pzpa5g")))

