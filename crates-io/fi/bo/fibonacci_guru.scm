(define-module (crates-io fi bo fibonacci_guru) #:use-module (crates-io))

(define-public crate-fibonacci_guru-1.0.0 (c (n "fibonacci_guru") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b7p4i9f4hjfgy6pqz9y4xbcws07xp4j98bx2ym2ddjgszh2i0mp")))

(define-public crate-fibonacci_guru-1.0.1 (c (n "fibonacci_guru") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ciy3lg0dwplif0274k0j417vk5x8835a4qwwcij9c4sgzapamm5")))

