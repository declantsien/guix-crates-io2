(define-module (crates-io fi bo fibora) #:use-module (crates-io))

(define-public crate-fibora-0.1.0 (c (n "fibora") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "1vkqkzq878p3s4m7003ssf2nwlmnkphk9p6vbn2zyqnfsf7889w6")))

(define-public crate-fibora-1.0.0 (c (n "fibora") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "1cwagfayzf6acd8bwpkd3w052mvp5fcgv46ygg408jfyxhg6pap5")))

(define-public crate-fibora-1.1.0 (c (n "fibora") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (o #t) (d #t) (k 0)))) (h "1fhkvm7bgpsawphd9viz8pyp2nfki5810zrf4ycs2dfh92dl9lvz") (f (quote (("binaries" "clap"))))))

(define-public crate-fibora-1.2.0 (c (n "fibora") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m65s1yvc5sm9if5vvx95wyh48afqzvpmhqlimkrx52xgjwiz57y")))

