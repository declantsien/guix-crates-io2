(define-module (crates-io fi ve fiveo) #:use-module (crates-io))

(define-public crate-fiveo-0.1.0 (c (n "fiveo") (v "0.1.0") (d (list (d (n "strsim") (r "^0.7.0") (d #t) (k 0)))) (h "0nk5y3zcig3ma2cf7417kiiwrf2gqq4i559m7n0xck1f0lq3w72d")))

(define-public crate-fiveo-0.2.0 (c (n "fiveo") (v "0.2.0") (d (list (d (n "strsim") (r "^0.7.0") (d #t) (k 0)))) (h "0268zbsvh0rahaswd1j5n59qr4bqwbp38r2klaqqli68m516pk55")))

(define-public crate-fiveo-0.3.0 (c (n "fiveo") (v "0.3.0") (h "1xmnbg9a967a3sjkii0d4c0p4vi5l2y1lihpq6b63njjwdrd5ada") (f (quote (("webassembly"))))))

(define-public crate-fiveo-0.3.1 (c (n "fiveo") (v "0.3.1") (h "0wkagjqwy3n61l0f23r32ncr6lbjqb9mapi8iqfrdsg831awp1x6") (f (quote (("webassembly"))))))

(define-public crate-fiveo-0.3.2 (c (n "fiveo") (v "0.3.2") (h "0j1gm5sg84gl8kb75p1241jppbl65cxq9sjq281l4gyxjbf0d5q7") (f (quote (("webassembly"))))))

