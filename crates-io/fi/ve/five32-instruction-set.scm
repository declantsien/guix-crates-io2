(define-module (crates-io fi ve five32-instruction-set) #:use-module (crates-io))

(define-public crate-five32-instruction-set-0.1.0 (c (n "five32-instruction-set") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0jja9z3mcf7xlc9m2w8m653a9zhqkb66vcg09synv13jqf2hiysz")))

