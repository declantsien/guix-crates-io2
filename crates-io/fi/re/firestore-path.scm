(define-module (crates-io fi re firestore-path) #:use-module (crates-io))

(define-public crate-firestore-path-0.0.0 (c (n "firestore-path") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1j905rfgyvh36j0vrnpylzy5m4nz03ilkjwc8213dhvh5h4wayws")))

(define-public crate-firestore-path-0.1.0 (c (n "firestore-path") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0xgc78sccrm0ks692qxag92hzjw6rpwws6j5az7gvpvrm38y94mb")))

(define-public crate-firestore-path-0.2.0 (c (n "firestore-path") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1shwc2i65626pilp0s9n3wib7gn42741h5hknih55nv39cg9dizx")))

(define-public crate-firestore-path-0.3.0 (c (n "firestore-path") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "19ggvisa9xmxadb4z9vbn7vrfnii76z6h94wvk7zlgs9jaagjki7")))

(define-public crate-firestore-path-0.4.0 (c (n "firestore-path") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1i3b2rmi22y018ahnv992ykyrxbdqkpwcnq26m1kw58iacv3bpws")))

(define-public crate-firestore-path-0.5.0 (c (n "firestore-path") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.449.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0m97k1ww6ix7jvj4507rxrdd9sbj81830kjxgdhvgs18d0m3ds05")))

(define-public crate-firestore-path-0.5.1 (c (n "firestore-path") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.449.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vcqbhml81zjf19i9ssnlgq7zzq0qwvci1srlxqi486abmaz14sx")))

(define-public crate-firestore-path-0.6.0 (c (n "firestore-path") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.449.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kcldb476dhbzi3g8li3fln4j7a9bbw9w5niy3hz9dhgs3ix0mky")))

(define-public crate-firestore-path-0.7.0 (c (n "firestore-path") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.473.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19va0sysjpvgd7sljb2rpsmpb86mi61458xn3fnx0kdysy5rgbnm")))

(define-public crate-firestore-path-0.8.0 (c (n "firestore-path") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yfdg5x2giq95izh4r6v45l2yfy6cj14wy6m1hdxb8798pvlgfc1")))

(define-public crate-firestore-path-0.8.1 (c (n "firestore-path") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17say9hlpll13a73msbbyk6i26dn6b0xqpz15r0ffrbwchpcs73c")))

(define-public crate-firestore-path-0.8.2 (c (n "firestore-path") (v "0.8.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pczydd6kdrn8wyz1sazd9xdwl4flx6kp3jdr0gpwm6mrq4qygxv")))

(define-public crate-firestore-path-0.9.0 (c (n "firestore-path") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yawigw26wr35vvh2bhc3m640927fdzgxlp3x349gfs53685l0sc")))

(define-public crate-firestore-path-0.9.1 (c (n "firestore-path") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n38gy89j68gbfkwp7kxyk4hsq4qc07hpyf1jg69nx04w8y6kq89")))

(define-public crate-firestore-path-0.9.2 (c (n "firestore-path") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.476.0") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "165yvsnmfl2qj1lcn9r2760z65v83fsc9dy2d4ljh7h4905g534y")))

(define-public crate-firestore-path-0.9.3 (c (n "firestore-path") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cvmisk8n0cc4b8na5q3cq30kdfwpkwh8gyg9zvm0zqvqriqqnal")))

(define-public crate-firestore-path-0.9.4 (c (n "firestore-path") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19wrmx3kgbmbb7hhq9l9dqp76dxp4sgnra1v1r831iqmi1nl371j")))

(define-public crate-firestore-path-0.9.5 (c (n "firestore-path") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01xk5hvb1njddypg035gm7dpa0hac2k9dkfyy8g02wcszcm7k4fs")))

(define-public crate-firestore-path-0.9.6 (c (n "firestore-path") (v "0.9.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bin20y79gizfxrgyq458rcx95mizlkq6wk9lw0sq1nvvxc6spnp")))

(define-public crate-firestore-path-0.9.7 (c (n "firestore-path") (v "0.9.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09p9k94h2p1b86nbd1vrlny2f6b7hhvxv6w08q8b7mvpqivxjgmn")))

(define-public crate-firestore-path-0.9.9 (c (n "firestore-path") (v "0.9.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xa3wm8xlbkgwdnhb86d91a9142az19hbc2s8baw1wwvjhn2z5nk")))

(define-public crate-firestore-path-0.9.10 (c (n "firestore-path") (v "0.9.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sa92vwnkypgsx1669pnkikc9m171xaf7fl0m3nrp4lh1ikwh2jb")))

(define-public crate-firestore-path-0.9.11 (c (n "firestore-path") (v "0.9.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hiahv0nwmj9cikqkk52zz58z7xvwzfls02qs319ank7n382sfx7")))

(define-public crate-firestore-path-0.9.12 (c (n "firestore-path") (v "0.9.12") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1axdyyc569ws1bw6mbg7xklhr8fb13s368kr14ga51df1qfi9kf2")))

(define-public crate-firestore-path-0.9.13 (c (n "firestore-path") (v "0.9.13") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ajwf79igiip45lhzz35r6a43dh9763li8r569ix5xhwiyb2p689")))

(define-public crate-firestore-path-0.9.14 (c (n "firestore-path") (v "0.9.14") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1350vdhijpkc99ijh7hjghih8q85klinfyczisvj3mpsqvslq2cs")))

(define-public crate-firestore-path-0.9.15 (c (n "firestore-path") (v "0.9.15") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "google-api-proto") (r "^1.516") (f (quote ("google-firestore-v1"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f4i1cxdk5g15qbq5375ss0k2sixbjippn4ixmnjfb1mfd1avsls")))

