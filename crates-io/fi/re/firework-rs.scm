(define-module (crates-io fi re firework-rs) #:use-module (crates-io))

(define-public crate-firework-rs-0.1.0 (c (n "firework-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "111d18i3i3l79d5sl9lwjjydqpsc6g8pnv7mrma39almvxjbs0q2")))

(define-public crate-firework-rs-0.2.0 (c (n "firework-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0k4l71kwxcj94vhip1gwbm9lw79md0lgwya3fqjkc2qh99883kc4")))

(define-public crate-firework-rs-0.3.0 (c (n "firework-rs") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1dw9rf6fh80rq1cx018m3pl0zc79j5jd5pq2rv9sncbvqgim2wi3")))

(define-public crate-firework-rs-0.3.1 (c (n "firework-rs") (v "0.3.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1cz8qrrp6n2c6qxc8g3k8450a5b29lpxrj8in1cl4yhvs1r7pwb5")))

