(define-module (crates-io fi re firebase-app-sdk) #:use-module (crates-io))

(define-public crate-firebase-app-sdk-0.1.0 (c (n "firebase-app-sdk") (v "0.1.0") (h "1sdmfkpg9y17zvbxlvsspkj66qz826vfaair7xc7r9f24c80f3mr") (y #t)))

(define-public crate-firebase-app-sdk-0.1.1 (c (n "firebase-app-sdk") (v "0.1.1") (h "0wmj007lbv11j7vs1nyd1dc4ly5c0dhcryicpp03v9jcy98flr1p") (y #t)))

(define-public crate-firebase-app-sdk-0.1.2 (c (n "firebase-app-sdk") (v "0.1.2") (d (list (d (n "api-request-utils-rs") (r "^0.1.3") (d #t) (k 0)))) (h "0q0j4yxccvcjcf1ikbncrddivy451cazb7951c27n60pm8kif2dn") (y #t)))

(define-public crate-firebase-app-sdk-0.1.3-preview1 (c (n "firebase-app-sdk") (v "0.1.3-preview1") (h "1ghxydmgv4ga0217d67y60lb12p1yznys8s8fp28hsyj3vmx9wj8") (y #t)))

(define-public crate-firebase-app-sdk-0.1.3 (c (n "firebase-app-sdk") (v "0.1.3") (h "1wf7sdiipxapm5qw2cxmygiqdwv7sd9hg6czcmh1fdkfsf4rakdv")))

(define-public crate-firebase-app-sdk-0.1.4 (c (n "firebase-app-sdk") (v "0.1.4") (h "137yiamnjphllwh4z5rik3lq1nhd24i8pc03aq4ilcyw6jdwlh0d")))

