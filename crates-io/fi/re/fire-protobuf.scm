(define-module (crates-io fi re fire-protobuf) #:use-module (crates-io))

(define-public crate-fire-protobuf-0.1.0 (c (n "fire-protobuf") (v "0.1.0") (d (list (d (n "bytes") (r "^0.2") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "00f1qnq9nvzf8s4ddc9ndfb9f947fsksnqv1dbxhz0514bg6631j") (y #t) (r "1.58")))

(define-public crate-fire-protobuf-0.1.1 (c (n "fire-protobuf") (v "0.1.1") (d (list (d (n "bytes") (r "^0.2") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "num_enum") (r "^0.5") (d #t) (k 2)))) (h "0a3z90jpl7gfha4a6c9ss7kfa28fgvac79yn6bi6wf8b6dzaz9mf") (r "1.58")))

(define-public crate-fire-protobuf-0.1.2 (c (n "fire-protobuf") (v "0.1.2") (d (list (d (n "bytes") (r "^0.2") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1.2") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0vjldrd7v1i3n0i2nsq3ra0p62hnghq1f7w73zgdz6djsw4li554") (r "1.58")))

(define-public crate-fire-protobuf-0.1.3 (c (n "fire-protobuf") (v "0.1.3") (d (list (d (n "bytes") (r "^0.2.14") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1.2") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0kz5pr9mm1fzjwr2zjcjwhgzml5r22ibaybbbfjs2h4din6z1krn") (r "1.58")))

(define-public crate-fire-protobuf-0.1.4 (c (n "fire-protobuf") (v "0.1.4") (d (list (d (n "bytes") (r "^0.2.14") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1.2") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1zqk6a42z8zcfvr01xx6rc12bim2w2i03yx1vgqqh6ig8kxpfqh4") (r "1.58")))

(define-public crate-fire-protobuf-0.1.5 (c (n "fire-protobuf") (v "0.1.5") (d (list (d (n "bytes") (r "^0.2.14") (d #t) (k 0) (p "simple-bytes")) (d (n "codegen") (r "^0.1.3") (d #t) (k 0) (p "fire-protobuf-codegen")) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1ml9yannp4zc368lfg94mwd6qnra0iqbfdjd6sj3r05pq572l8ig") (r "1.64")))

