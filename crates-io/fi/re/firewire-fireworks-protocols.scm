(define-module (crates-io fi re firewire-fireworks-protocols) #:use-module (crates-io))

(define-public crate-firewire-fireworks-protocols-0.1.0 (c (n "firewire-fireworks-protocols") (v "0.1.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.2") (d #t) (k 0)))) (h "1b4i213975yvb4nmcv65i77vx8lm4fa531k3fl8whjw8fpc1vi96") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox")))) (y #t)))

(define-public crate-firewire-fireworks-protocols-0.1.1 (c (n "firewire-fireworks-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.2") (d #t) (k 0)))) (h "1frrzgwcx6lfjpswpdjhzvdbnr28zh143m8qz7a6fplgx3ya8kg2") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

(define-public crate-firewire-fireworks-protocols-0.2.0 (c (n "firewire-fireworks-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.3") (d #t) (k 0)))) (h "1cndfs2qprnjjlbribmgnrfq34206q3zyk0g8r60nhhg93gii8ba") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

