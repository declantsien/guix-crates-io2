(define-module (crates-io fi re firepilot_models) #:use-module (crates-io))

(define-public crate-firepilot_models-1.3.0 (c (n "firepilot_models") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0n16zqwsb5gfpc9xvcr5i19p3f6yg6skkvqgc3qn6wlriizq6c9x")))

