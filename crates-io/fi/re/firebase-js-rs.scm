(define-module (crates-io fi re firebase-js-rs) #:use-module (crates-io))

(define-public crate-firebase-js-rs-0.1.0 (c (n "firebase-js-rs") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)))) (h "0g30ksm32f9srz434si8li9j59pcdi6fsknhnv1x5g1n60h5h13j")))

(define-public crate-firebase-js-rs-0.1.1 (c (n "firebase-js-rs") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)))) (h "1zjpnz9w83ykkh6vfp9xsqkmm18vhzwv3wvqpxmdcqnfxfp1hc8b")))

