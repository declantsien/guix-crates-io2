(define-module (crates-io fi re firecore-battle-gui) #:use-module (crates-io))

(define-public crate-firecore-battle-gui-0.0.1-alpha.1 (c (n "firecore-battle-gui") (v "0.0.1-alpha.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "firecore-battle") (r "^0.0.5") (f (quote ("default_endpoint"))) (k 0)) (d (n "firecore-pokedex-engine") (r "^0.0.1") (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)))) (h "1ggacz8q2b3rz4j2yrj09ysbrvgj2zz0r3bndpydq1jjfi5zpzzq") (f (quote (("default" "audio") ("audio" "firecore-pokedex-engine/audio"))))))

