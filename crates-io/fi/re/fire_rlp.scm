(define-module (crates-io fi re fire_rlp) #:use-module (crates-io))

(define-public crate-fire_rlp-0.1.0 (c (n "fire_rlp") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16g3df4n4xs59qrb53jn736far1p9v2drk5zqjxr4z9l82qck99n")))

