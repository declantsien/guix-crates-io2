(define-module (crates-io fi re firework) #:use-module (crates-io))

(define-public crate-firework-0.1.0 (c (n "firework") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ash") (r "^0.31") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "enum-map") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "glam") (r "^0.12") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "palette") (r "^0.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hp5dxldk4acv4ril3hsrajp4ly7i0cl4d5zc0l0kpk30s5fk2ig") (y #t)))

