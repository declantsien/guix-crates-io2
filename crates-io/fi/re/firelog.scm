(define-module (crates-io fi re firelog) #:use-module (crates-io))

(define-public crate-firelog-0.1.0 (c (n "firelog") (v "0.1.0") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0db36kgsj37n9qhpjbzm2181bcp9c2530927faism5yvkiyph8zx")))

(define-public crate-firelog-0.1.1 (c (n "firelog") (v "0.1.1") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0sh81qd7zqqnhcfw35xrp9342mkgkrh8knkvjv8ydrdcjbhz1hzm")))

(define-public crate-firelog-0.1.2 (c (n "firelog") (v "0.1.2") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "1dpkzbqjgx92arlfni6pqsrwznijfkzpzb87n5cwzpvcshz8nyh0")))

(define-public crate-firelog-0.1.3 (c (n "firelog") (v "0.1.3") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0i1c90w0wmfzxbg9b29jgmzmai1yg4r3z5n7y13ficwjrrhjm32j")))

(define-public crate-firelog-0.1.4 (c (n "firelog") (v "0.1.4") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "14s5r2cydsx7vki22bhdbfx71qhv3sm2b7n7r5p602brnyv7i782")))

(define-public crate-firelog-0.1.5 (c (n "firelog") (v "0.1.5") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "1zqa4rhqvspvzbqm3j4xv40nfly20d5wjqh2zd5g5jmw6g5jc8vq")))

(define-public crate-firelog-0.1.6 (c (n "firelog") (v "0.1.6") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0dj2draprzjn31130q8ml962pj3vvs4h8316an46gdw56lkp4684")))

(define-public crate-firelog-0.1.7 (c (n "firelog") (v "0.1.7") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "1cxvj3acs723wl1lrm73r0xkqxini8gaawsmrb36r75lxqh9l356")))

(define-public crate-firelog-0.1.8 (c (n "firelog") (v "0.1.8") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0fgi5xsnrzmfr8iw8znirssfhi0gzal4c7br1vl3wrv6fd55xkb6")))

