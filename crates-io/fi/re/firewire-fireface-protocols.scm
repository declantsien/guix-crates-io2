(define-module (crates-io fi re firewire-fireface-protocols) #:use-module (crates-io))

(define-public crate-firewire-fireface-protocols-0.1.1 (c (n "firewire-fireface-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "118136z443w7qynca8i9khq8ihdgpzank1xh7ssmgznlqxip09vs") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-fireface-protocols-0.2.0 (c (n "firewire-fireface-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "0p1y6dqphnv8p4xv3iq5mhsp50bb78vsg39m3s658sy874lkiwwp") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

