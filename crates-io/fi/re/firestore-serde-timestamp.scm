(define-module (crates-io fi re firestore-serde-timestamp) #:use-module (crates-io))

(define-public crate-firestore-serde-timestamp-0.1.0 (c (n "firestore-serde-timestamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "firestore-serde") (r "^0.1.0") (d #t) (k 2)) (d (n "googapis") (r "^0.5.0") (d #t) (k 2)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "01277kyqqhb810jhfqfnakbk0bmssbxb7brgpvv603c26wrics01")))

(define-public crate-firestore-serde-timestamp-0.1.2 (c (n "firestore-serde-timestamp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "firestore-serde") (r "^0.1.2") (d #t) (k 2)) (d (n "googapis") (r "^0.6.0") (d #t) (k 2)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "0gbdym42d1jz3fvvdccy9yvyfnpkp7mbg5n2dj31m7p5322mgg69")))

