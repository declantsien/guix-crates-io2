(define-module (crates-io fi re firewire-motu-protocols) #:use-module (crates-io))

(define-public crate-firewire-motu-protocols-0.1.1 (c (n "firewire-motu-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.2") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "1fki6dn1sp9kf70qvvxmgqk6wsv22bps52vmzhxsgphd577f4zqz") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

(define-public crate-firewire-motu-protocols-0.2.0 (c (n "firewire-motu-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.3") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "055xr3zm8kzraaxskzfxyfjb9smc9734k4r0py9kcrrhimzz2c9z") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

(define-public crate-firewire-motu-protocols-0.3.0 (c (n "firewire-motu-protocols") (v "0.3.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.3") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "1s65qyx088x85hf6y2lyrcsi9wpd5z3b7rg1qzdd6ijrx5vyb7g6") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

