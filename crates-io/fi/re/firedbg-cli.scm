(define-module (crates-io fi re firedbg-cli) #:use-module (crates-io))

(define-public crate-firedbg-cli-1.74.0 (c (n "firedbg-cli") (v "1.74.0") (d (list (d (n "anstyle") (r "^1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "firedbg-rust-parser") (r "^1.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0mpnfqsnrwc3xybgfw946z8pj0p2r5sb7y9rxmq7ymr6il0csvky")))

