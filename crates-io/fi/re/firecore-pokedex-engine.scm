(define-module (crates-io fi re firecore-pokedex-engine) #:use-module (crates-io))

(define-public crate-firecore-pokedex-engine-0.0.1 (c (n "firecore-pokedex-engine") (v "0.0.1") (d (list (d (n "firecore-engine") (r "^0.0.1") (k 0)) (d (n "firecore-pokedex") (r "^0.0.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinystr") (r "^0.4") (f (quote ("serde"))) (k 0)))) (h "1xbxy04xl9nbirwxfalzzc432gww0k945qqrgbp3xvip5phjij9n") (f (quote (("default" "audio") ("audio" "firecore-engine/audio"))))))

