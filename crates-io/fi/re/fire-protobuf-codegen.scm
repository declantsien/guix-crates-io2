(define-module (crates-io fi re fire-protobuf-codegen) #:use-module (crates-io))

(define-public crate-fire-protobuf-codegen-0.1.0 (c (n "fire-protobuf-codegen") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xfnpqlh6zqqqyyl3vk13xc4q52p7vw5kfn8n04jzfj5l6l9j59i") (y #t) (r "1.58")))

(define-public crate-fire-protobuf-codegen-0.1.1 (c (n "fire-protobuf-codegen") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09fak3a4ahg6ps9lg4fm3i5njx38shqg5r9fb168sv5spr1x3i5c") (r "1.58")))

(define-public crate-fire-protobuf-codegen-0.1.2 (c (n "fire-protobuf-codegen") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n4fif2fnkc37i69kr8c3g4rrbfg3a2677x60x5mia0v02azghm6") (r "1.58")))

(define-public crate-fire-protobuf-codegen-0.1.3 (c (n "fire-protobuf-codegen") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h5vh5g8cc6vkawdi728zrpn4msbzq94n9ylq6ksn1gkfnmqp3iw") (r "1.64")))

(define-public crate-fire-protobuf-codegen-0.1.4 (c (n "fire-protobuf-codegen") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyqdvdppn3f2algfppqlmskdncd3mjzb8fn9z4007pk1cs813r6") (r "1.67")))

(define-public crate-fire-protobuf-codegen-0.1.5 (c (n "fire-protobuf-codegen") (v "0.1.5") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hkypnncgq9wq4rpcqhmfic0jy7yd3mz02mi20lzy8npy9gm0f1c") (r "1.67")))

(define-public crate-fire-protobuf-codegen-0.1.6 (c (n "fire-protobuf-codegen") (v "0.1.6") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07piwhwjx2syqm9waryj4b4m3mxbznimxmhabawv8kjhwq9vw9d4") (r "1.67")))

