(define-module (crates-io fi re firefox-native-manifest) #:use-module (crates-io))

(define-public crate-firefox-native-manifest-0.1.0 (c (n "firefox-native-manifest") (v "0.1.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "types") (r "^0.1") (d #t) (k 0) (p "firefox-native-manifest-types")) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 0)))) (h "02d4qhvcbfyyw8xb87rbvw2xcwl82rxy4cpv5976a6c1igxb1hpb")))

