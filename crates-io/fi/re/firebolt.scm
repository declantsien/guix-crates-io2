(define-module (crates-io fi re firebolt) #:use-module (crates-io))

(define-public crate-firebolt-0.1.0 (c (n "firebolt") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17qsnwqbrdlgkc4s9k971fszkdibljbci7fhmjamrmfgkaa1f78m")))

(define-public crate-firebolt-0.1.1 (c (n "firebolt") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lvpr8dzvvqjcrgzx0nnxpc42fzb0nqmnx0xichm24xgkm395k69")))

(define-public crate-firebolt-0.1.2 (c (n "firebolt") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iry6i23szix2vaiavyr0plkjfi949j0sic0ca2ap4mxk0b0hlbi")))

