(define-module (crates-io fi re firedancer-sys) #:use-module (crates-io))

(define-public crate-firedancer-sys-0.0.1 (c (n "firedancer-sys") (v "0.0.1") (h "0ib8ici3qm673f31pp5s1q1pmb5qhf20nicv16a6dxxzlkb9n2h7") (y #t)))

(define-public crate-firedancer-sys-0.1.0 (c (n "firedancer-sys") (v "0.1.0") (h "0vihp1rf0ikl0hji26qnph7wvw3kxdid1p26whnpc8bimd3r7x9k")))

(define-public crate-firedancer-sys-0.2.0 (c (n "firedancer-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0fp53iql4wkqq63y1pl7fm8cdmak74rq5gilpxid3bkh1x2almfs") (f (quote (("fuzz-asan") ("default"))))))

(define-public crate-firedancer-sys-0.3.0 (c (n "firedancer-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1gayrnbpcggp9crn5gvg83arnbrvpackshisj0kbaj6y785iq30n") (f (quote (("fuzz-asan") ("default"))))))

