(define-module (crates-io fi re firestore_grpc_cloudrun) #:use-module (crates-io))

(define-public crate-firestore_grpc_cloudrun-0.1.0 (c (n "firestore_grpc_cloudrun") (v "0.1.0") (d (list (d (n "firestore_grpc") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1shh80i41inb34sa88mv22m3aywx01pvw3lpsk268i7aksa0n8jz")))

(define-public crate-firestore_grpc_cloudrun-0.1.1 (c (n "firestore_grpc_cloudrun") (v "0.1.1") (d (list (d (n "firestore_grpc") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q75w2nwzlq89ma7s80jxkyad80hiaf1a43pxyjglh98kfjsj877")))

