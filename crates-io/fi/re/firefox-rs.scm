(define-module (crates-io fi re firefox-rs) #:use-module (crates-io))

(define-public crate-firefox-rs-0.1.0 (c (n "firefox-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10yip81zwh3ydrbqlh9bdv63w708fj4038z0isy01cr714rbmfa3")))

