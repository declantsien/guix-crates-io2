(define-module (crates-io fi re firewire-oxfw-protocols) #:use-module (crates-io))

(define-public crate-firewire-oxfw-protocols-0.1.1 (c (n "firewire-oxfw-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ta1394-avc-audio") (r "^0.1") (d #t) (k 0)) (d (n "ta1394-avc-ccm") (r "^0.1") (d #t) (k 0)) (d (n "ta1394-avc-general") (r "^0.1") (d #t) (k 0)))) (h "1gca5l9qb23yzzzmj697b3cl94wn5bdsz39vpgv1wzc4h7r460gl") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-oxfw-protocols-0.2.0 (c (n "firewire-oxfw-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ta1394-avc-audio") (r "^0.2") (d #t) (k 0)) (d (n "ta1394-avc-ccm") (r "^0.2") (d #t) (k 0)) (d (n "ta1394-avc-general") (r "^0.2") (d #t) (k 0)) (d (n "ta1394-avc-stream-format") (r "^0.2") (d #t) (k 0)))) (h "1xdp0sdwfq19hzimkspm4yy2kxv3w533iwhg0ln3ciy16pc5km0k") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

