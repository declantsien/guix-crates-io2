(define-module (crates-io fi re firebase-admin-rust-auth) #:use-module (crates-io))

(define-public crate-firebase-admin-rust-auth-0.0.1 (c (n "firebase-admin-rust-auth") (v "0.0.1") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sx1jiwdnz6rl7zn7s4769vwn1l4gp8m0jrywhsks5bzjshj0xrw")))

(define-public crate-firebase-admin-rust-auth-0.0.2 (c (n "firebase-admin-rust-auth") (v "0.0.2") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fp94wwza9jaaj60g2pxpvk0msp23f69irfwq3jgkwpcnjpk6lqw")))

(define-public crate-firebase-admin-rust-auth-0.0.3 (c (n "firebase-admin-rust-auth") (v "0.0.3") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s74bfizr5bf994p1q1mvmbxsw8h6s238z4djazd7pqzkcs9imdq")))

(define-public crate-firebase-admin-rust-auth-0.0.4 (c (n "firebase-admin-rust-auth") (v "0.0.4") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dkxc6ckvi59mim92lj847cxf2ccxqjl0l39nn4r9hrhncf3784q")))

(define-public crate-firebase-admin-rust-auth-0.0.5 (c (n "firebase-admin-rust-auth") (v "0.0.5") (d (list (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18c81zkpyjp0yqqcv905r1s7jfk5lwsf6ys70zppsrp6m5chb4b3")))

