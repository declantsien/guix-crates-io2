(define-module (crates-io fi re firewatcher) #:use-module (crates-io))

(define-public crate-firewatcher-0.1.0 (c (n "firewatcher") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "diesel") (r "^2.0.0") (f (quote ("postgres" "chrono" "serde_json" "r2d2"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "101j0qpz56a0dnfyqjyn7fdf0vk6nwzk0jk5rhzxqnbbcw9s7x4v")))

