(define-module (crates-io fi re firebase-types) #:use-module (crates-io))

(define-public crate-firebase-types-0.0.1 (c (n "firebase-types") (v "0.0.1") (d (list (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.9") (f (quote ("js"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)))) (h "1hpah2bw1darwba3z82kssjzmd3v6bqngx9g2dgm00p4b3l98na5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("expose-jsvalue" "serde" "dep:wasm-bindgen" "dep:serde-wasm-bindgen"))))))

