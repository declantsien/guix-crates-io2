(define-module (crates-io fi re firebase_realtime_database) #:use-module (crates-io))

(define-public crate-firebase_realtime_database-0.1.0 (c (n "firebase_realtime_database") (v "0.1.0") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "055qjig171ivsl8i3njmqa5p4lk7jpqqwk6r8370cp883afz4kdl")))

(define-public crate-firebase_realtime_database-0.1.1 (c (n "firebase_realtime_database") (v "0.1.1") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0yrbg5cgjc26x8vrqr6plzxwpqil9h4w80hjkd1mrrz0wr79s3pb")))

(define-public crate-firebase_realtime_database-0.1.2 (c (n "firebase_realtime_database") (v "0.1.2") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0vh8yx21rym9ww1haqwlxr19wa6g78ky2864dygk65kpxsh8k95q")))

(define-public crate-firebase_realtime_database-0.1.3 (c (n "firebase_realtime_database") (v "0.1.3") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1k3kacgfx49bl1p79bciz8ppqml6308j4cb9405mxr6ixhz0abvx")))

(define-public crate-firebase_realtime_database-0.1.4 (c (n "firebase_realtime_database") (v "0.1.4") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1bmx95890c4n0raaamx5a7qs5agarmh65acrk42z9p6118ffy78s")))

(define-public crate-firebase_realtime_database-0.1.5 (c (n "firebase_realtime_database") (v "0.1.5") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0rzgw8kvsrg622vkf7mi9v4vscp813f372rfi5n009mggaf39896")))

(define-public crate-firebase_realtime_database-0.1.6 (c (n "firebase_realtime_database") (v "0.1.6") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1q51jblvh4k2wax77g5jxyzpwj97npk9ywb3vzxp3417kb39chri")))

(define-public crate-firebase_realtime_database-0.1.7 (c (n "firebase_realtime_database") (v "0.1.7") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0b02rp63xf8y6zy2pna5q1rf126sdimhfj1pvb1hvzgz1fiqm4bz")))

(define-public crate-firebase_realtime_database-0.1.8 (c (n "firebase_realtime_database") (v "0.1.8") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1524vx9b6aw0gw95k04r3wcbrx3n3mygh2772ywkvar7x8rkj9h6")))

(define-public crate-firebase_realtime_database-0.1.9 (c (n "firebase_realtime_database") (v "0.1.9") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0aslskyvlixdlbk12rj1my0hba4vb1xamhram8r2wsbj471i8fw3")))

(define-public crate-firebase_realtime_database-0.2.0 (c (n "firebase_realtime_database") (v "0.2.0") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0f13kfa5gx3km1gmpw8wbjy2wjzanflrlvf90s9n6mcyblvlba1i")))

(define-public crate-firebase_realtime_database-0.2.1 (c (n "firebase_realtime_database") (v "0.2.1") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "00hkqv372qy5n027rjvkx3jxgnjhqrjixd841n0slc7a1dbcafr8")))

(define-public crate-firebase_realtime_database-0.2.11 (c (n "firebase_realtime_database") (v "0.2.11") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1dv3vnwqwpr756h6r8yil4rgf2shii7mqn997b2g9j027djymz9a")))

(define-public crate-firebase_realtime_database-0.2.12 (c (n "firebase_realtime_database") (v "0.2.12") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "00yxi91fgsqb73lqivflxd131i4pdl0h2v7cii3p7q009h3bggxp")))

(define-public crate-firebase_realtime_database-0.3.0 (c (n "firebase_realtime_database") (v "0.3.0") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1akcw528y023iwwys2wmpzixc80md8dq2617p2c4f4jgsrc22k0z")))

(define-public crate-firebase_realtime_database-0.3.1 (c (n "firebase_realtime_database") (v "0.3.1") (d (list (d (n "gcp_auth") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "13whcp2wi2xhdg9x028c3a744rvfrfl9z167127f0pc8398d0rgr")))

