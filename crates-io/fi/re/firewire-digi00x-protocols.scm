(define-module (crates-io fi re firewire-digi00x-protocols) #:use-module (crates-io))

(define-public crate-firewire-digi00x-protocols-0.1.1 (c (n "firewire-digi00x-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)))) (h "1gd2c5n4r6nksrcqqq2nq8scwfk308mcf7p5v7h34as1d26h7s57") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-digi00x-protocols-0.2.0 (c (n "firewire-digi00x-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)))) (h "0vriba3la9ya6ljzy0qrp7wms3fj4g5qylvfnpwh7x7fn6dhmq67") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

