(define-module (crates-io fi re fireworks) #:use-module (crates-io))

(define-public crate-fireworks-1.0.0 (c (n "fireworks") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "08q98v574xfdj8h9rdmmq4bhnlrxfb9ab43g97a8anaqdvgfwifb")))

(define-public crate-fireworks-1.0.1 (c (n "fireworks") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0yhfd5ya89hjhj55l52knhia9wdq5f3qkbgkn93fcg485q3qbg01")))

(define-public crate-fireworks-1.0.2 (c (n "fireworks") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0900a13yl191wa9a8yg97n73vqr9q23mggxdpzsiimn8kj30qmh5")))

(define-public crate-fireworks-1.0.3 (c (n "fireworks") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1phjy1pwdbsha5934zaxwajadx6kaj9lkxi9l9b5b2yh4nsfp81q")))

(define-public crate-fireworks-1.0.4 (c (n "fireworks") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "10yl2s50r7knpgbg3fwp19z89hp6j5d9l8rwa8vv88xcv1dyfav3")))

