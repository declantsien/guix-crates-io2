(define-module (crates-io fi re firerust) #:use-module (crates-io))

(define-public crate-firerust-1.0.0 (c (n "firerust") (v "1.0.0") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0lwwn4zxivr5sqz0vlbr1kj5nzww18bgvd6nggy48yy2qzakfrhb")))

(define-public crate-firerust-1.0.1 (c (n "firerust") (v "1.0.1") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0v55ng6jxkbbj0xh0gq6wymay067fr1kb4sm51n560fcwhhmiy4g")))

