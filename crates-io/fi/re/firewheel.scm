(define-module (crates-io fi re firewheel) #:use-module (crates-io))

(define-public crate-firewheel-0.0.0 (c (n "firewheel") (v "0.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glow") (r "^0.11") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.6.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanovg") (r "^1.0") (f (quote ("gl3"))) (k 0)) (d (n "raw-window-handle") (r "^0.4") (d #t) (k 0)))) (h "0r850i3f494n2086bjm9qbhya2xd0fq6j9ds3c2nakbvf5na9k30")))

(define-public crate-firewheel-0.0.1 (c (n "firewheel") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glow") (r "^0.11") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.6.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanovg") (r "^1.0") (f (quote ("gl3"))) (k 0)) (d (n "raw-window-handle") (r "^0.4") (d #t) (k 0)))) (h "1h3nawmkpsllb81aj7kbsr8gzng0mg0z8drp884z8a9jdpbi5xzp")))

