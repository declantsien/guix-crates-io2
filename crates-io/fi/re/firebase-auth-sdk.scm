(define-module (crates-io fi re firebase-auth-sdk) #:use-module (crates-io))

(define-public crate-firebase-auth-sdk-0.1.0 (c (n "firebase-auth-sdk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (f (quote ("use_pem"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0jq8jcc12x9ar8s99m6fjqpgkgyaffvcz8pa4pxpvz7zc8wjmm2k")))

