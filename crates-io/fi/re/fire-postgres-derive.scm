(define-module (crates-io fi re fire-postgres-derive) #:use-module (crates-io))

(define-public crate-fire-postgres-derive-0.1.0 (c (n "fire-postgres-derive") (v "0.1.0") (h "16kx5589jjf7zvj7in3gyxmr1hzxpwxk5fdws6ki81j1jgznh0gp")))

(define-public crate-fire-postgres-derive-0.1.2 (c (n "fire-postgres-derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jk1bshh74s63ax2hcwqpi00ssmvr55nba9rj0d4jvd9dp96x220")))

(define-public crate-fire-postgres-derive-0.2.0 (c (n "fire-postgres-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wa5hala6d27xgawmbmq6dspgr90h4k6aj70r3mlzhp5wa78h2ig") (r "1.66")))

(define-public crate-fire-postgres-derive-0.3.0-alpha.1 (c (n "fire-postgres-derive") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro-crate") (r "^3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rk7igk3mgzpfsi1krcw3zw0g4zl38xgqrany2rpxv3xd70yh215") (r "1.75")))

(define-public crate-fire-postgres-derive-0.3.0-alpha.2 (c (n "fire-postgres-derive") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro-crate") (r "^3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12b8xa514aqlmxj03m924hxj94lgd4c9sc06i779hnp6x5jn9723") (r "1.75")))

