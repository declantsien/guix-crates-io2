(define-module (crates-io fi re firebase_jwt_rs) #:use-module (crates-io))

(define-public crate-firebase_jwt_rs-0.1.0 (c (n "firebase_jwt_rs") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v9aql7pbgkqyq12pqr9f70m4vc3wsbk70982mn3i5jdcjihxj93")))

(define-public crate-firebase_jwt_rs-0.1.1 (c (n "firebase_jwt_rs") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q7h9m7xly3ps97bfgl62rpc03m9s7i47i0hpqd8qkmsn33zi6ym")))

(define-public crate-firebase_jwt_rs-0.1.2 (c (n "firebase_jwt_rs") (v "0.1.2") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1scwc8d525p9249xbpb598l2rshhy9hnrq6qwrwdncm72rgn1pa2")))

