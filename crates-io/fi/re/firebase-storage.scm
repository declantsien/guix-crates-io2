(define-module (crates-io fi re firebase-storage) #:use-module (crates-io))

(define-public crate-firebase-storage-0.1.0 (c (n "firebase-storage") (v "0.1.0") (d (list (d (n "api-request-utils-rs") (r "^0.1.5-preview3") (d #t) (k 0)) (d (n "firebase-app-sdk") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l7ahqccw190x8wn7n3ysssii1x0bhijp6w70vrg55kxksablmld")))

