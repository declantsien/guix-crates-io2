(define-module (crates-io fi re fire-stream-api-codegen) #:use-module (crates-io))

(define-public crate-fire-stream-api-codegen-0.1.0 (c (n "fire-stream-api-codegen") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hgiv45y85anxs63r3h7ya2nagz17riph84bx6ak35g5rds2ficy") (y #t) (r "1.58")))

(define-public crate-fire-stream-api-codegen-0.1.1 (c (n "fire-stream-api-codegen") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "143fbim78jh04cbhf67vrhgq5jphgqdrgmjwpxyf55hcfkqmv77j") (r "1.58")))

(define-public crate-fire-stream-api-codegen-0.1.2 (c (n "fire-stream-api-codegen") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bnws0a71xfxwa5k5n76g1jiylf1bi5i2hzp9qkp1lxd7jfy8jk6") (r "1.58")))

(define-public crate-fire-stream-api-codegen-0.1.3 (c (n "fire-stream-api-codegen") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16916dgrqx67ixm2rkp1i4zvdp8v5qgvfq1qg2pmqc2qpqcbrxdd") (r "1.58")))

(define-public crate-fire-stream-api-codegen-0.1.4 (c (n "fire-stream-api-codegen") (v "0.1.4") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0148lrgmlsinhnqdyncl8dsn696k2xwpi0xsp0ph75ckcsr1ch6w") (r "1.67")))

(define-public crate-fire-stream-api-codegen-0.1.5 (c (n "fire-stream-api-codegen") (v "0.1.5") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pnv3gaydx05pf5ndf5qkfmf8g4zng1q4prlxqi4p4dph03yhvi6") (r "1.67")))

(define-public crate-fire-stream-api-codegen-0.1.6 (c (n "fire-stream-api-codegen") (v "0.1.6") (d (list (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lfcg233dk4zjfpdpjr9cmnn1pka5wvygg96gcfa4sm2zzp457ds") (r "1.67")))

