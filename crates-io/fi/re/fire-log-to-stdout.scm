(define-module (crates-io fi re fire-log-to-stdout) #:use-module (crates-io))

(define-public crate-fire-log-to-stdout-0.1.0 (c (n "fire-log-to-stdout") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (k 0)))) (h "1d9axgx4z9yhx9vm097byhpqw1dd3bw3flr31i17xz9qbim4x8v3") (f (quote (("log_time" "chrono") ("default" "log_time"))))))

(define-public crate-fire-log-to-stdout-0.1.1 (c (n "fire-log-to-stdout") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (k 0)))) (h "09n9fgk32lwiv91l15sjagrq9wljfach1n9yqhnidh9hr13951ng") (f (quote (("log_time" "chrono") ("default" "log_time"))))))

(define-public crate-fire-log-to-stdout-0.1.2 (c (n "fire-log-to-stdout") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (o #t) (k 0)))) (h "1n0ffpwb6hs0fqvkyk92mdcbrv2j0x49i9s4qg4rmwgdjvkxlbb4") (f (quote (("log_time" "chrono"))))))

(define-public crate-fire-log-to-stdout-0.1.3 (c (n "fire-log-to-stdout") (v "0.1.3") (h "1xpsrw8pb9rq2cqxgmfignp2w21qdnh1rw7pm6lgwfdqbbik2j2j") (f (quote (("log_crate"))))))

