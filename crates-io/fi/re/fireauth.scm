(define-module (crates-io fi re fireauth) #:use-module (crates-io))

(define-public crate-fireauth-0.1.0 (c (n "fireauth") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0n8qbhk6xl081psfvbpsgfz0yfsf6fiwbg81qwx6wm7mbwl9w00v")))

(define-public crate-fireauth-0.1.1 (c (n "fireauth") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1iv8lk2g4brks2g355pj3yjnavjf6hz5dizw1fbjmjsfbi2qjnf3")))

(define-public crate-fireauth-0.1.2 (c (n "fireauth") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1naqc8p08a7pw0gil59brw6fqy0lbi2svqxh9nas9bdabds83akg")))

(define-public crate-fireauth-0.1.3 (c (n "fireauth") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ivvkw0f0zg9k3znnwl48cgb5434y1d2i1pvk1hm962dgvjgbdpb")))

(define-public crate-fireauth-0.1.4 (c (n "fireauth") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1br6jd3kc2z90dq150w85i8rmqb7hlryv3jl2fy86pwc3k4c8bbd")))

(define-public crate-fireauth-0.1.5 (c (n "fireauth") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1d4jc4925snisd89hxmwxh25pxixslvy94g7azy1fn7nfm5xw9fc")))

