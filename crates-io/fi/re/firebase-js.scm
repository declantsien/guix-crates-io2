(define-module (crates-io fi re firebase-js) #:use-module (crates-io))

(define-public crate-firebase-js-0.0.1 (c (n "firebase-js") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "console_log") (r "^1.0.0") (d #t) (k 0)) (d (n "firebase-js-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (f (quote ("serde"))) (d #t) (k 0)))) (h "0l5g9r0nfxvg9fd8d510g608hag31rhfy2jmh9m5wjqfrl8kv54z")))

