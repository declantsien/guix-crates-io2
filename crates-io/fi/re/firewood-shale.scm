(define-module (crates-io fi re firewood-shale) #:use-module (crates-io))

(define-public crate-firewood-shale-0.0.1 (c (n "firewood-shale") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0gkfwjlg27mdifdb57dx8qf538mx28vkf28g4qwy1rvdc0mw40ms") (y #t)))

(define-public crate-firewood-shale-0.0.2 (c (n "firewood-shale") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lru") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fbm888r02f73hl5b9qswxrm926pbym7xbvsmlibc45lz273ybw0") (y #t)))

