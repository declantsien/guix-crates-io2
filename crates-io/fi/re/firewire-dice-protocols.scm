(define-module (crates-io fi re firewire-dice-protocols) #:use-module (crates-io))

(define-public crate-firewire-dice-protocols-0.1.1 (c (n "firewire-dice-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "0aavw0w2dpbkbr9mpmhd2d7whpwwn8md1jb6hnd74wkm81frv5vv") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-dice-protocols-0.1.2 (c (n "firewire-dice-protocols") (v "0.1.2") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "14aiwzj5zz03l6xjdnca157q3gbacwwd5wb4flns2hhdvp9b3ir3") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-dice-protocols-0.2.0 (c (n "firewire-dice-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "0ks9b4mgc4xczalf2vmf1cgsylgq9326v84flabz06zgfskgl4lv") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-dice-protocols-0.2.1 (c (n "firewire-dice-protocols") (v "0.2.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "0lik8zb33szzvpnqddrkabpxnx850fls7rvpmym939pypbphwcm3") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-dice-protocols-0.3.0 (c (n "firewire-dice-protocols") (v "0.3.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)) (d (n "ta1394-avc-general") (r "^0.2") (d #t) (k 0)))) (h "0z3fpdr695rfavdqfi3v3qdci1fxamx6l7dwjq4zp7wlb5cm9ajf") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

(define-public crate-firewire-dice-protocols-0.3.1 (c (n "firewire-dice-protocols") (v "0.3.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)) (d (n "ta1394-avc-general") (r "^0.2") (d #t) (k 0)))) (h "0rd7zpn2f4izvm5v2xxpyfp1zzwzrhpg605v156kidwnk1bn02nh") (f (quote (("dox" "glib/dox" "hinawa/dox"))))))

