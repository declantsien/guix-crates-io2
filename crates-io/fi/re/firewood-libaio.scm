(define-module (crates-io fi re firewood-libaio) #:use-module (crates-io))

(define-public crate-firewood-libaio-0.0.1 (c (n "firewood-libaio") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "14skfq432d0jhi5jvpc33w50dd2gcbv8f8m44d0fdsy4vi76kjzr") (f (quote (("emulated-failure")))) (y #t)))

(define-public crate-firewood-libaio-0.0.2 (c (n "firewood-libaio") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0bmppdr33zd5xfvhpz9iy3k39s7l731lr6vzq3m0k35xx6l85l82") (f (quote (("emulated-failure")))) (y #t)))

