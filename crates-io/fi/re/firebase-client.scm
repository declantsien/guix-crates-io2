(define-module (crates-io fi re firebase-client) #:use-module (crates-io))

(define-public crate-firebase-client-0.0.1 (c (n "firebase-client") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "google-authz") (r "^0.0.2") (f (quote ("client"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14.15") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (d #t) (k 0)))) (h "0yg3m88npfdxyks83shmidrf3ml5h5xphr4h6r73l75v3qfwmdr5")))

