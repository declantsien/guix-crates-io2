(define-module (crates-io fi re firebase-js-sys) #:use-module (crates-io))

(define-public crate-firebase-js-sys-0.1.0 (c (n "firebase-js-sys") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "console_log") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "01rfb4x2igyjxfm36dq7nn6vfmbalbr8vncg5hvj68as7zndkkkk")))

