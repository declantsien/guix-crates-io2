(define-module (crates-io fi re firebae-derive) #:use-module (crates-io))

(define-public crate-firebae-derive-0.1.0 (c (n "firebae-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.5") (d #t) (k 0)))) (h "0hrk4mnqflnllh33x9kaf4ylaa2y4bc6j5w9fiyrzbrndkyp1wn4")))

