(define-module (crates-io fi re firestorm-enabled) #:use-module (crates-io))

(define-public crate-firestorm-enabled-0.3.1 (c (n "firestorm-enabled") (v "0.3.1") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "1qhmys2j0x25pxgixdd7pmkk0ibwvqwxcz7csy23v934vn0zzb4n") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.4.0 (c (n "firestorm-enabled") (v "0.4.0") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "17f3xc899sjp7bcxl30rd1j21yqs9hrhby7cdi80bmg1gdg9irq9") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.4.1 (c (n "firestorm-enabled") (v "0.4.1") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "16jd7jxxla4xc1nmnyan5wq8ldv5f0khydf0mq89x1yj8wk4b8s2") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.4.2 (c (n "firestorm-enabled") (v "0.4.2") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "05hypb0gx4rnghvbxa16xmgjhsq60asd3c4dcwd4ir50kczhl8dr") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.4.3 (c (n "firestorm-enabled") (v "0.4.3") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "1mnxldrs2fk0a0n1q9mvp5z92r945mzzg14m50jdckw1gm6xsqwi") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.5.0 (c (n "firestorm-enabled") (v "0.5.0") (d (list (d (n "firestorm-core") (r "^0.1") (d #t) (k 0)) (d (n "inferno") (r "^0.10") (d #t) (k 0)))) (h "09ib781np8n0x18zjq52pp7nm6zhb4361mbyx8c5ixr8z67r7bc6") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

(define-public crate-firestorm-enabled-0.5.1 (c (n "firestorm-enabled") (v "0.5.1") (d (list (d (n "firestorm-core") (r "^0.1.2") (d #t) (k 0)) (d (n "inferno") (r "^0.11.3") (d #t) (k 0)))) (h "0cxg3llgcglsqbpp3liqmfm0j4q507k4qbzykszwxgm6s0vmam94") (f (quote (("system_time" "firestorm-core/system_time") ("cpu_time" "firestorm-core/cpu_time"))))))

