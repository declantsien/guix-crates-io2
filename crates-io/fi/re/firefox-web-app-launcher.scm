(define-module (crates-io fi re firefox-web-app-launcher) #:use-module (crates-io))

(define-public crate-firefox-web-app-launcher-1.0.0 (c (n "firefox-web-app-launcher") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "1c59bdc3i1yw49lbcdj3137yy7aprpm6xpc3b3ywf9ld1chynidc")))

