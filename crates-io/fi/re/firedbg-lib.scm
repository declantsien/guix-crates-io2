(define-module (crates-io fi re firedbg-lib) #:use-module (crates-io))

(define-public crate-firedbg-lib-0.1.0 (c (n "firedbg-lib") (v "0.1.0") (h "0slk1gxymfwq3jrz6v378ax2g0knic2cdrj37has8v3kg5g6ca08") (r "1.72")))

(define-public crate-firedbg-lib-0.1.1 (c (n "firedbg-lib") (v "0.1.1") (h "0c95108spbxd23xf2savb0cms7b2sfcfq6gq0i7yda6y7hcrs92z") (r "1.72")))

(define-public crate-firedbg-lib-0.1.2 (c (n "firedbg-lib") (v "0.1.2") (h "0zzh9ndg6h2r9y5dsv8caamhics6zi7zqqqwvzib3vmac41kmv9c") (r "1.72")))

