(define-module (crates-io fi re fire) #:use-module (crates-io))

(define-public crate-fire-0.1.0 (c (n "fire") (v "0.1.0") (h "0rf21i7mgq7zcgmspmsizgzlpynl42dm1x6h3abflygibhalh6zc")))

(define-public crate-fire-0.2.0 (c (n "fire") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1a3nzl98ml62fpx3pq2pv047ga109qzmpmxs8x54jx7y9a5x0aag")))

(define-public crate-fire-0.3.0 (c (n "fire") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1zdwih60616imhphj59z3p9jwhag3158wi6zcap076khdyv6gyjd")))

(define-public crate-fire-0.4.0 (c (n "fire") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvh5z4qlvq89y9fb36293lna7f8wkq5ji8gpx2vvz99l81snry8")))

