(define-module (crates-io fi re firestore-sdk) #:use-module (crates-io))

(define-public crate-firestore-sdk-0.1.0 (c (n "firestore-sdk") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "firestore_grpc") (r "^0.31") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxp1zzay5klxhff1vhnybr5j9880ilrg0qpp3nazqzac8nsnv6c")))

