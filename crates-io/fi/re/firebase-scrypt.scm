(define-module (crates-io fi re firebase-scrypt) #:use-module (crates-io))

(define-public crate-firebase-scrypt-0.1.0 (c (n "firebase-scrypt") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "scrypt") (r "^0.10") (k 0)))) (h "0c8ia9bwwbscfpw705d5g9h0ccj9kkk1brqy5w2rqp5icca7wdhi") (f (quote (("simple") ("default" "simple"))))))

(define-public crate-firebase-scrypt-0.1.1 (c (n "firebase-scrypt") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "scrypt") (r "^0.10") (k 0)))) (h "1x6hdjg0jc16hidpprxsmk8nbxirxnq8qgdr3c108jvvjg39cpnk") (f (quote (("simple") ("default" "simple")))) (r "1.59")))

(define-public crate-firebase-scrypt-0.2.0 (c (n "firebase-scrypt") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "scrypt") (r "^0.10") (k 0)))) (h "1pvj97rklh87bk5dz8dahgwdxwwk2lwccgnv7xfwaxizc00s5jf1") (f (quote (("simple") ("default" "simple")))) (r "1.59")))

(define-public crate-firebase-scrypt-0.2.1 (c (n "firebase-scrypt") (v "0.2.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2") (d #t) (k 0)) (d (n "ctr") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "scrypt") (r "^0.10") (k 0)))) (h "0fmywrd6i9nl5cbvbx72n7mci2bvkym4f3jx2lsj53hhzxm0nh5x") (f (quote (("simple") ("default" "simple")))) (r "1.59")))

