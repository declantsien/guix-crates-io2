(define-module (crates-io fi re fireworks-erikh) #:use-module (crates-io))

(define-public crate-fireworks-erikh-0.1.0 (c (n "fireworks-erikh") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "114b2vfksyp36rxf0c1fvkvy2yg8xz80m9zygyz6z1psd455i480")))

(define-public crate-fireworks-erikh-0.1.1 (c (n "fireworks-erikh") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "1rsw6fqfpkv5g3ayyhifjni34v7f749f214z166pm0af45i1pxg9")))

