(define-module (crates-io fi re firedbg-protocol) #:use-module (crates-io))

(define-public crate-firedbg-protocol-1.0.0 (c (n "firedbg-protocol") (v "1.0.0") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jky3pqs7b17q8dhq72pyq562cc25sx8npvs3i6d72n5janh9dqj")))

