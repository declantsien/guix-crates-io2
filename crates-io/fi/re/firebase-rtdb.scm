(define-module (crates-io fi re firebase-rtdb) #:use-module (crates-io))

(define-public crate-firebase-rtdb-0.3.0 (c (n "firebase-rtdb") (v "0.3.0") (d (list (d (n "citadel_logging") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("native-tls-vendored" "json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 2)))) (h "08wgw22dfgz13rfch0vd7xb4z3f629flraz3rzdzhykf6qgi4d3d")))

(define-public crate-firebase-rtdb-0.4.0 (c (n "firebase-rtdb") (v "0.4.0") (d (list (d (n "citadel_logging") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("native-tls-vendored" "json" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 2)))) (h "0wpiwgfr7l0xq5q8yqn1w0vqipksw1zff3p2rl3mx3wx3v54sppk")))

(define-public crate-firebase-rtdb-0.5.0 (c (n "firebase-rtdb") (v "0.5.0") (d (list (d (n "citadel_logging") (r "^0.5.0") (k 2)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("rustls" "rustls-native-certs" "rustls-tls" "json" "serde_json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (k 2)))) (h "0w43bwli9lhh7zxrn96v3dxsxw4csrmq18bvazqlbzmminfgiswj")))

(define-public crate-firebase-rtdb-0.7.0 (c (n "firebase-rtdb") (v "0.7.0") (d (list (d (n "citadel_logging") (r "^0.7.0") (k 2)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("rustls" "rustls-native-certs" "rustls-tls" "json" "serde_json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (k 2)))) (h "13pc07m453fhbfs9z1vli4s7fwxa4i26n0smk48v64ym360vg9np")))

(define-public crate-firebase-rtdb-0.8.0 (c (n "firebase-rtdb") (v "0.8.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("rustls" "rustls-native-certs" "rustls-tls" "json" "serde_json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "0nwpkp1wxxas8l1dh0v4cbhddah42j8r92xrjvw1s4p6bcgvbdp0")))

