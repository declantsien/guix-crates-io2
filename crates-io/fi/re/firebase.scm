(define-module (crates-io fi re firebase) #:use-module (crates-io))

(define-public crate-firebase-0.1.0 (c (n "firebase") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jx5pi5zlggx3r0v82rg9bkww1m7lbar9j7rg9pzybzp1qcksfbj")))

(define-public crate-firebase-0.2.0 (c (n "firebase") (v "0.2.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0za5rq4hpydmlircmr8hmgjqp80rxizrfxq3jj1phq2pcn3ilfjd")))

(define-public crate-firebase-0.8.0 (c (n "firebase") (v "0.8.0") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)))) (h "1ppckcb31p986grdfcb53ldzqi46d1h5z53ffn1iw1zi2hgwhfi6")))

(define-public crate-firebase-0.9.1 (c (n "firebase") (v "0.9.1") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "url") (r "^0.2.35") (d #t) (k 0)))) (h "1s5y4ni2msvxp274x3dnqgjqry1y6v575igr4120zfm6qwf6ggg8")))

