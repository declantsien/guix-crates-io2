(define-module (crates-io fi re firebase-db-access) #:use-module (crates-io))

(define-public crate-firebase-db-access-0.1.1 (c (n "firebase-db-access") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "knock") (r "^0.1.8") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0z2fl4v6cav4y09d3mcj70pjjq067ckfsmnpr2ddh8wb7rj0l1qh") (y #t)))

(define-public crate-firebase-db-access-0.1.2 (c (n "firebase-db-access") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "knock") (r "^0.1.8") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "106hd3m87ncc0bqab23v93rwhilprc1vw32rfgaswa6s5ad98gb1") (y #t)))

