(define-module (crates-io fi re fireball) #:use-module (crates-io))

(define-public crate-fireball-0.0.1 (c (n "fireball") (v "0.0.1") (d (list (d (n "capstone") (r "^0.11.0") (d #t) (k 0)) (d (n "cpp_demangle") (r "^0.3.5") (d #t) (k 0)) (d (n "goblin") (r "^0.5.4") (d #t) (k 0)) (d (n "pdb") (r "^0.8.0") (d #t) (k 0)))) (h "1xnf0bnbfpjm4nsy8i5yf4qcz1alalshqz3q9nwkbw75vbng05jr")))

