(define-module (crates-io fi re firecore-audio) #:use-module (crates-io))

(define-public crate-firecore-audio-0.0.1 (c (n "firecore-audio") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinystr") (r "^0.4") (f (quote ("serde"))) (k 0)))) (h "1szzl9mq0ivap3vazbd2wpa444z3ybfcbpzjynbd6p4xng9rgsi4") (f (quote (("file") ("default"))))))

