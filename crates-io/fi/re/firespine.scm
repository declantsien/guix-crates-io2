(define-module (crates-io fi re firespine) #:use-module (crates-io))

(define-public crate-firespine-0.1.0 (c (n "firespine") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vaq9vwxagvbr6wni1lp0hqjx2k2f34zr0py6z12xhlaxl43dnl2")))

(define-public crate-firespine-0.1.1 (c (n "firespine") (v "0.1.1") (d (list (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ddh4cpwgk0x9s6nvkm6ahd0m1anrkkm828pgdl41ifm045r0ap4") (y #t)))

(define-public crate-firespine-0.1.2 (c (n "firespine") (v "0.1.2") (d (list (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l3q2lm9wnnh0k5b856gyy8fncdkzl3rf8vk4b3r55aljv52d4nx")))

(define-public crate-firespine-0.1.3 (c (n "firespine") (v "0.1.3") (d (list (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lgnkla7kz5vp4jhaf95vns87qwix43sr44l0p44h67ig5lyq0vw")))

