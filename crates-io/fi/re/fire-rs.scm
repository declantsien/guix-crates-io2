(define-module (crates-io fi re fire-rs) #:use-module (crates-io))

(define-public crate-fire-rs-0.1.0 (c (n "fire-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "084kh4m5sk96v5x03p5fd821azy610asn1ll60z9l1vsnzqvz047")))

(define-public crate-fire-rs-0.1.1 (c (n "fire-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iqn8bh66qddbzgn7w6a479sba66f4fq4llvfql7gcg3gv0dksqz")))

(define-public crate-fire-rs-0.2.0 (c (n "fire-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fire-rs-core") (r "^0.2.0") (d #t) (k 0)))) (h "03v7xkciywsh67mvwcqgmc7kkw212yyxyjid8rphm8xa80khdsw1")))

(define-public crate-fire-rs-0.2.2-alpha.0 (c (n "fire-rs") (v "0.2.2-alpha.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fire-rs-core") (r "^0.2.0") (d #t) (k 0)))) (h "137zfacdh660gspnvpq0d5bwiz7xanmm3hcnns6bpda9755ji6yn")))

(define-public crate-fire-rs-0.2.3 (c (n "fire-rs") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fire-rs-core") (r "^0.2") (d #t) (k 0)))) (h "115nvjm3i6gllbwsivkialfh5fv1h507n1hp65k7zlywsxmi7rjn")))

