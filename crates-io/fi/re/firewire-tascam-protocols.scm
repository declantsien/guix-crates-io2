(define-module (crates-io fi re firewire-tascam-protocols) #:use-module (crates-io))

(define-public crate-firewire-tascam-protocols-0.1.1 (c (n "firewire-tascam-protocols") (v "0.1.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.2") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "1564p4kn658i8mikb54bgglqj4nbz2ydh35abasmk6gxhh3bwvac") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

(define-public crate-firewire-tascam-protocols-0.2.0 (c (n "firewire-tascam-protocols") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "hinawa") (r "^0.7") (d #t) (k 0)) (d (n "hitaki") (r "^0.3") (d #t) (k 0)) (d (n "ieee1212-config-rom") (r "^0.1") (d #t) (k 0)))) (h "003ikmmwjkavhr06r8v2smcxp4624izdg18x0jrqzcc5fh2wd33n") (f (quote (("dox" "glib/dox" "hinawa/dox" "hitaki/dox"))))))

