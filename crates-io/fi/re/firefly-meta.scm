(define-module (crates-io fi re firefly-meta) #:use-module (crates-io))

(define-public crate-firefly-meta-0.1.0 (c (n "firefly-meta") (v "0.1.0") (d (list (d (n "postcard") (r "^1.0.8") (k 0)) (d (n "serde") (r "^1.0.197") (k 0)))) (h "1ncyfdd6s0zccjgqw6324zgmbj8ivqa1djb496dx98xq7wysllvh")))

(define-public crate-firefly-meta-0.1.1 (c (n "firefly-meta") (v "0.1.1") (d (list (d (n "postcard") (r "^1.0.8") (k 0)) (d (n "serde") (r "^1.0.197") (k 0)))) (h "1vrlnhrk173dfx7b4yscgxlkbr931fk3hizzkw042gc0kcrf6imx")))

(define-public crate-firefly-meta-0.1.2 (c (n "firefly-meta") (v "0.1.2") (d (list (d (n "postcard") (r "^1.0.8") (k 0)) (d (n "serde") (r "^1.0.197") (k 0)))) (h "1h7p0339ijpwl2999bs0lfaika3zgxbc565d6yj89mpcpghzpm8s")))

