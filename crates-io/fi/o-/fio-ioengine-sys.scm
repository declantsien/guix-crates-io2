(define-module (crates-io fi o- fio-ioengine-sys) #:use-module (crates-io))

(define-public crate-fio-ioengine-sys-0.1.0 (c (n "fio-ioengine-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1g945vsvii7vdjs5mp2azqam84gvhmg14f3l624akp4qvy4kx47z") (f (quote (("vendor") ("default" "vendor"))))))

