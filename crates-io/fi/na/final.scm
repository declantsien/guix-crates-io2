(define-module (crates-io fi na final) #:use-module (crates-io))

(define-public crate-final-0.1.0 (c (n "final") (v "0.1.0") (h "16df8ls86x1zfy9pq5sdg5p74n5zdf8dc2xjm81w8wslkgspmm87")))

(define-public crate-final-0.1.1 (c (n "final") (v "0.1.1") (h "0yahfrc56ilfm49nmmq0ml4jpqr2v5akhcl03ycj51rqmn5qc4hr")))

