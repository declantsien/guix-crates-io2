(define-module (crates-io fi na finances) #:use-module (crates-io))

(define-public crate-finances-0.0.0 (c (n "finances") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18q5lyav51varn427w4ma0frmynydp08cf8x636g8a1ixzhrv81s")))

