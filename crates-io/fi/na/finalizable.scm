(define-module (crates-io fi na finalizable) #:use-module (crates-io))

(define-public crate-finalizable-0.1.0 (c (n "finalizable") (v "0.1.0") (h "05ii4f7q9dxf7zl5s9ifck2jpkcryy4q602dyp0nykm8nc6ny3sk")))

(define-public crate-finalizable-0.1.1 (c (n "finalizable") (v "0.1.1") (h "19gnsz3b8s2hic1p8aww67j1aff56jnr6n9jz5h6dfqakmjyfwvm")))

(define-public crate-finalizable-0.1.2 (c (n "finalizable") (v "0.1.2") (h "1fiawgvksajxd6bs66lx8mxnxwzcbd68j85npr4ldwsm1fbhcmyz")))

(define-public crate-finalizable-0.1.3 (c (n "finalizable") (v "0.1.3") (h "0ar76j3jjw9lhb52x0cgagnsmw4pjygx5w81k6y6wm8gxlhg4kiz") (f (quote (("try"))))))

(define-public crate-finalizable-0.1.4 (c (n "finalizable") (v "0.1.4") (h "0wrajhxi7r6w70n4ln2z6zy7f8nis1k3q3if5bq258liqyay3zh9") (f (quote (("try"))))))

