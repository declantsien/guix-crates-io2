(define-module (crates-io fi na finally-block) #:use-module (crates-io))

(define-public crate-finally-block-0.1.0 (c (n "finally-block") (v "0.1.0") (h "0a91sgr8d1dnnivi0x58lbil9d64w81h27qkd5p82psaf641z27v")))

(define-public crate-finally-block-0.1.1 (c (n "finally-block") (v "0.1.1") (h "0zk4c69xmpayvnvls8famyz2cjvgww7963qzhjwaar5qv7qvsr2k")))

(define-public crate-finally-block-0.2.0 (c (n "finally-block") (v "0.2.0") (h "1j91cscfr3n330sdsqmmkifmsccnn0l8zinkcsw8zc9fy922fvq8")))

