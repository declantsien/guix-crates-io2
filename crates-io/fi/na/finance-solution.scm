(define-module (crates-io fi na finance-solution) #:use-module (crates-io))

(define-public crate-finance-solution-0.0.0 (c (n "finance-solution") (v "0.0.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)))) (h "00l0b6cr9m3hgbcyzfwg7brv0955q73hgl8drmlq2cdqg4a1jsfs")))

