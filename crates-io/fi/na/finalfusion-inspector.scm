(define-module (crates-io fi na finalfusion-inspector) #:use-module (crates-io))

(define-public crate-finalfusion-inspector-0.1.0 (c (n "finalfusion-inspector") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "finalfusion") (r "^0.12") (d #t) (k 0)) (d (n "gio") (r "^0.8") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.8") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "stdinout") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0c74c8pwxh83hpmbxyv648d9xh7r5zhb16lxdas4df4mmmmz90rs")))

