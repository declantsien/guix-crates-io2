(define-module (crates-io fi na financial-primitives) #:use-module (crates-io))

(define-public crate-financial-primitives-0.1.0 (c (n "financial-primitives") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-rc6") (k 0)) (d (n "sp-std") (r "^2.0.0-rc6") (k 0)))) (h "1yfn8q1fm1wlnbgrk7xjv75p7vkg9fnbgrk0lkjarfhc5saxdh49") (f (quote (("std" "frame-support/std" "sp-std/std" "codec/std") ("default" "std"))))))

(define-public crate-financial-primitives-0.1.1 (c (n "financial-primitives") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-rc6") (k 0)) (d (n "sp-std") (r "^2.0.0-rc6") (k 0)))) (h "1smm1n88jz6b37z05hwn45g7b76dfb3hnrrf8ksjd46l8hg4152n") (f (quote (("std" "frame-support/std" "sp-std/std" "codec/std") ("default" "std"))))))

