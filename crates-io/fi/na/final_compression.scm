(define-module (crates-io fi na final_compression) #:use-module (crates-io))

(define-public crate-final_compression-1.0.0 (c (n "final_compression") (v "1.0.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lz4") (r "^1.24") (d #t) (k 0)) (d (n "rust-lzo") (r "^0.6.2") (d #t) (k 0)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 0)))) (h "1mvq8953y0zc01yy6jyjy8hhkvl08cd22g5jkd5q2kl6iq1vgw7b")))

