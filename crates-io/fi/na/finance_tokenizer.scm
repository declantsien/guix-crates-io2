(define-module (crates-io fi na finance_tokenizer) #:use-module (crates-io))

(define-public crate-finance_tokenizer-0.1.0 (c (n "finance_tokenizer") (v "0.1.0") (d (list (d (n "blake3") (r "^0.3.5") (d #t) (k 0)) (d (n "credit_card") (r "^0.1.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qhkv1ixbhy9k3ykma9zg4ngp5249qir9bhks592qq8z4914f71s")))

