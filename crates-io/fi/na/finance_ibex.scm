(define-module (crates-io fi na finance_ibex) #:use-module (crates-io))

(define-public crate-finance_ibex-0.1.0-beta (c (n "finance_ibex") (v "0.1.0-beta") (d (list (d (n "finance_api") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "1zrk02zsybln76nymnknlv7nln602kjwn1kclmmmix3y7yqkajbd")))

