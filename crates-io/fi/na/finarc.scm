(define-module (crates-io fi na finarc) #:use-module (crates-io))

(define-public crate-finarc-0.1.0 (c (n "finarc") (v "0.1.0") (h "16qw9mm77s2l5q645c77v983a70n6h67rppwrwjcl8whwvpcl7g1")))

(define-public crate-finarc-0.2.0 (c (n "finarc") (v "0.2.0") (h "06160cp7idlysx0pg40vbpglqs8w3ll8r0isiirsji1lfxlmys3l")))

