(define-module (crates-io fi na financeapi) #:use-module (crates-io))

(define-public crate-financeapi-0.1.1 (c (n "financeapi") (v "0.1.1") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1nnigvagijclkasz3nsih91bk8pkzqa5sa36ya005rbjqdmpf3a9")))

