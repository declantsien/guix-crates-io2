(define-module (crates-io fi na financial) #:use-module (crates-io))

(define-public crate-financial-1.0.0 (c (n "financial") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)))) (h "0sd0cs484y7mg6xg5h75znhndn3paz4amjijr36gcjsb859mwplm")))

(define-public crate-financial-1.0.1 (c (n "financial") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)))) (h "1m78fbl40vmzw6v0dkfjz9qn342w7gc76b2294cjiziqkbnpsbcd")))

(define-public crate-financial-1.1.0 (c (n "financial") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)))) (h "1fgxyglpyws2bl809z6b2g02inq2n5ikcbkpn5g8wjgxk4q32chh")))

(define-public crate-financial-1.1.1 (c (n "financial") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0vb1sjqpqals6fj9fjcqnvpk65xhpzwgavaqi82330ii5ws4nk2m")))

(define-public crate-financial-1.1.2 (c (n "financial") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jx5hwqqwjw9aff95b90bmg9sflggcwngpkmr2azvjli89m7qq0l")))

(define-public crate-financial-1.1.3 (c (n "financial") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xigk2kh24divxjqrsjs9qj302rnl0xjb62mp5f52sadpfvhqxyc")))

(define-public crate-financial-1.1.4 (c (n "financial") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "01sx7q1qpklyybgkzdpxcp88p491x7lyysir1rv9x84bi7jjbipy")))

(define-public crate-financial-1.1.5 (c (n "financial") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0q8gwhqcpcyh598i5dsfbk7f2w2rqw8m2qlqg7kjl0pp1lxwbni1")))

