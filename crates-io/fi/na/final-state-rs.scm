(define-module (crates-io fi na final-state-rs) #:use-module (crates-io))

(define-public crate-final-state-rs-0.1.0 (c (n "final-state-rs") (v "0.1.0") (d (list (d (n "tiny-bitstream") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1kv22fhifg46vn2ljqkhafsha7ks5gavygid8x7ls84z10k8prhj")))

