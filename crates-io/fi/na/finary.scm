(define-module (crates-io fi na finary) #:use-module (crates-io))

(define-public crate-finary-0.1.0 (c (n "finary") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1la57mxml17vwrdbprq3cc53l1iydpnj42by3078fhihwhiwggn4")))

