(define-module (crates-io fi ne fine_grained) #:use-module (crates-io))

(define-public crate-fine_grained-0.1.0 (c (n "fine_grained") (v "0.1.0") (d (list (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "16fkxd6qs8b7qdhrwz6y27by86bazy5h427yy8xrmyplsv63zmra")))

(define-public crate-fine_grained-0.1.1 (c (n "fine_grained") (v "0.1.1") (d (list (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0xypc9vffqj6xamj1n5j4krdpljqb6d7q2dc92czabs8jpp4n324")))

(define-public crate-fine_grained-0.1.2 (c (n "fine_grained") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0zqmg5my4shk1q2qg9y336qffsyzfk0q26binjaclks54gd5f053")))

