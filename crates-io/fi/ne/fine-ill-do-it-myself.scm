(define-module (crates-io fi ne fine-ill-do-it-myself) #:use-module (crates-io))

(define-public crate-fine-ill-do-it-myself-0.1.0 (c (n "fine-ill-do-it-myself") (v "0.1.0") (d (list (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "notify") (r "^6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5") (d #t) (k 2)))) (h "04md1rlja0vq99bijrihbncsj5dbvjryxg5c7i3k1dapkqbfdpn0")))

(define-public crate-fine-ill-do-it-myself-0.1.1 (c (n "fine-ill-do-it-myself") (v "0.1.1") (d (list (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "notify") (r "^6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.5") (d #t) (k 2)))) (h "1i52b7qhcg619f6nh4y3rrfbr8wn0f8hsnk081z581fdf5rhkfcs")))

