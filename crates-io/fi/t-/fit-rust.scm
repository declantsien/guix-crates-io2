(define-module (crates-io fi t- fit-rust) #:use-module (crates-io))

(define-public crate-fit-rust-0.1.0 (c (n "fit-rust") (v "0.1.0") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0zm435mk5ivlqc5fagxq9c8qk4ml23m1jv7z8d6ddwz2p58ybbyn")))

(define-public crate-fit-rust-0.1.1 (c (n "fit-rust") (v "0.1.1") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "19kfkbdigq6w852cszly24v2q5k4rxsc4q65wqf4axysi9pv3l6q")))

(define-public crate-fit-rust-0.1.2 (c (n "fit-rust") (v "0.1.2") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1i6bchw6qli3svdbjhdsmrwapg6638r3kp5gcgia5wqqqfmb5bd1")))

(define-public crate-fit-rust-0.1.3 (c (n "fit-rust") (v "0.1.3") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "04x55kksmaqkcx39dcypja0afr211f77956fplzbax3d3gh2p88k")))

(define-public crate-fit-rust-0.1.4 (c (n "fit-rust") (v "0.1.4") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0jv6mh37ssqjgcbr129hc8w1hn0wvps7hw11364b046dl56k55c9")))

(define-public crate-fit-rust-0.1.5 (c (n "fit-rust") (v "0.1.5") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1gh3ql42nhl4m6af6fh47w1244kmk1nsqppwnkf6m8pn8dgqi0ch")))

(define-public crate-fit-rust-0.1.6 (c (n "fit-rust") (v "0.1.6") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0534w5vqwigxzf2xsf5vxmcvjzz1xws51lr54mr12wmq0cc8x3am")))

(define-public crate-fit-rust-0.1.7 (c (n "fit-rust") (v "0.1.7") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1g9lfs5ixgi3f6f0fn7j6v47qc19sk8fgibpryh6n1dcqddpj23m")))

(define-public crate-fit-rust-0.1.8 (c (n "fit-rust") (v "0.1.8") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "copyless") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "022b454ipiv44xgigk7ksqkdjz0ljp75p0yzh7xxfd0k538vshn5")))

