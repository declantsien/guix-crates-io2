(define-module (crates-io fi n_ fin_model) #:use-module (crates-io))

(define-public crate-fin_model-0.1.0 (c (n "fin_model") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "steel-cent") (r "^0.2.2") (d #t) (k 0)))) (h "1c7ir1zff51ay99hwdq8698lf7br2nk9glrh5gjb5c0q0ma5zfdv")))

(define-public crate-fin_model-0.1.1 (c (n "fin_model") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "steel-cent") (r "^0.2.2") (d #t) (k 0)))) (h "1r9vzrbp31iq5bc5zik2zqzwvswvzhpd93yw6190d1gipkyi144f")))

(define-public crate-fin_model-0.1.2 (c (n "fin_model") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "steel-cent") (r "^0.2.2") (d #t) (k 0)))) (h "0ny2jqbmhzsxmq7gqqxr0688dr3457dfx8nznxfmq3dy8db5fmgd")))

