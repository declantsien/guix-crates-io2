(define-module (crates-io fi n_ fin_data) #:use-module (crates-io))

(define-public crate-fin_data-0.1.0 (c (n "fin_data") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "fin_model") (r "^0.1.0") (d #t) (k 0)))) (h "0vqkwhb1d2r4lwzjgfa5wi342h3n6mpnkbg86fdfv07zyrppgk7r")))

(define-public crate-fin_data-0.1.1 (c (n "fin_data") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "fin_model") (r "^0.1.1") (d #t) (k 0)))) (h "1hsf14qbb1m97xjcy8swm8zsg6z8msgiv9iva06rrm4ic6xzshzm")))

(define-public crate-fin_data-0.1.2 (c (n "fin_data") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "fin_model") (r "^0.1.2") (d #t) (k 0)))) (h "1rz4l4yiw5zv72cgcswa07x28hvn95rlwbdrwd03gj633fmr4na4")))

