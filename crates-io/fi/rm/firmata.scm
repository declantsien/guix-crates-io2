(define-module (crates-io fi rm firmata) #:use-module (crates-io))

(define-public crate-firmata-0.0.1 (c (n "firmata") (v "0.0.1") (d (list (d (n "serial") (r "^0.2.0") (d #t) (k 0)))) (h "0q23jss9c72r96basaj08zzl74394ykbx2h3p61p9a68riaa1jj7")))

(define-public crate-firmata-0.1.0 (c (n "firmata") (v "0.1.0") (d (list (d (n "serial") (r "^0.2.0") (d #t) (k 0)))) (h "0jjdnlnbj7a9218lxpjn6h2aar1nni1n4m2qdmzbw9r2sgy6ffdg")))

(define-public crate-firmata-0.2.0 (c (n "firmata") (v "0.2.0") (d (list (d (n "serial") (r "^0.2.0") (d #t) (k 0)))) (h "0ml7xyx7g1716alfsjiix16rz84bhfgz97fzydid2h2v25w3pzw9")))

