(define-module (crates-io fi x- fix-getters-utils) #:use-module (crates-io))

(define-public crate-fix-getters-utils-0.2.1 (c (n "fix-getters-utils") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rules") (r "^0.2.1") (d #t) (k 0) (p "fix-getters-rules")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits" "visit"))) (k 0)))) (h "0klm42xfi9whw7kjqh4ps5mbylyvk95xr3wk9ajhgyp1mif4fspl") (f (quote (("default" "log"))))))

(define-public crate-fix-getters-utils-0.3.0 (c (n "fix-getters-utils") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rules") (r "^0.3.0") (d #t) (k 0) (p "fix-getters-rules")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits" "visit"))) (k 0)))) (h "1imiykha1hi5wv8jlz9ic0rk9hvgql34kkiydnl7f9bwb97slxqw") (f (quote (("default" "log"))))))

(define-public crate-fix-getters-utils-0.3.2 (c (n "fix-getters-utils") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rules") (r "^0.3.2") (d #t) (k 0) (p "fix-getters-rules")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits" "visit"))) (k 0)))) (h "0sj846v5dq085w9iqrb6fmgs2p7b1iz2r2q4ypqr466a9zw40wg9") (f (quote (("default" "log"))))))

