(define-module (crates-io fi x- fix-rs-macros) #:use-module (crates-io))

(define-public crate-fix-rs-macros-0.1.0 (c (n "fix-rs-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1xwg32dvd9lq3h4wzilla36mb4f70cnn4vdxia0rp21c5l9706s7")))

(define-public crate-fix-rs-macros-0.2.0 (c (n "fix-rs-macros") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1cvh78lkymg6hj97vlzlsjrwwlzcz94sk16ncmvdrm9c4c8fdwiz")))

(define-public crate-fix-rs-macros-0.2.1 (c (n "fix-rs-macros") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "15j35y23j61qyn2z57pk8q8n16m02shsak2j2lybl9n4pcqh2ic3")))

