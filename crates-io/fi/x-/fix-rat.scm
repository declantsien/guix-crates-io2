(define-module (crates-io fi x- fix-rat) #:use-module (crates-io))

(define-public crate-fix-rat-0.1.0 (c (n "fix-rat") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d2rsknalhhz17xcjcj9nnsg0kdqi6wazvrfd8f45mznxyrsxl4v") (f (quote (("serde1" "serde") ("nightly") ("default" "nightly" "serde1"))))))

(define-public crate-fix-rat-0.1.1 (c (n "fix-rat") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a5ji4n54d49mlgz9nlwbnhb5wci88h5vsz8m4rkmskc4aj8140i") (f (quote (("serde1" "serde") ("nightly") ("default" "nightly" "serde1"))))))

