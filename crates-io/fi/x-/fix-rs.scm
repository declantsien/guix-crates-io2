(define-module (crates-io fi x- fix-rs) #:use-module (crates-io))

(define-public crate-fix-rs-0.1.0 (c (n "fix-rs") (v "0.1.0") (h "12kzaf80clg6qqpln7mv925pb1rbd8vci4b4cxbf4pzqa3zbc7l7")))

(define-public crate-fix-rs-0.2.0 (c (n "fix-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (o #t) (d #t) (k 0)) (d (n "fix-rs-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0flr2ad4g34kv73qd0nl26ardyz3zidvv7c3i1pf2i4lksi5gzdg") (f (quote (("load-testing" "clap") ("default"))))))

(define-public crate-fix-rs-0.2.1 (c (n "fix-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (o #t) (d #t) (k 0)) (d (n "fix-rs-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mp891b1lyl9j5qw8z4kbg7viabg5xsrr39ip0vnhs1kjr0psmkq") (f (quote (("load-testing" "clap") ("default"))))))

