(define-module (crates-io fi x- fix-whitespace) #:use-module (crates-io))

(define-public crate-fix-whitespace-1.0.0 (c (n "fix-whitespace") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0fycrv53hhmip9q1r35khllwyii90gsalircpmvq5lz91khi99id")))

(define-public crate-fix-whitespace-1.0.1 (c (n "fix-whitespace") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1i8azzbbdp2q3mfd1y6gnw1wpjhdwi9dczza7m9gyka4adkzrvvw")))

(define-public crate-fix-whitespace-1.0.2 (c (n "fix-whitespace") (v "1.0.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1in3134cgqjmg7lgba1rb7avdxshn0fdjqg2xpa2z0yrmxi7hv5g")))

