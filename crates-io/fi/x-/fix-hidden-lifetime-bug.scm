(define-module (crates-io fi x- fix-hidden-lifetime-bug) #:use-module (crates-io))

(define-public crate-fix-hidden-lifetime-bug-0.1.0 (c (n "fix-hidden-lifetime-bug") (v "0.1.0") (d (list (d (n "proc_macros") (r "^0.1.0") (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "1ql2nrw9d14dv938kfz8c71bx0xy531iinnp3qrqs43p00qpkkv2")))

(define-public crate-fix-hidden-lifetime-bug-0.1.1 (c (n "fix-hidden-lifetime-bug") (v "0.1.1") (d (list (d (n "proc_macros") (r "^0.1.1") (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "0rsnybiknjpdh2ggs6brkjsqd1x7vi5zxlwdwymqark1aq85j8n2")))

(define-public crate-fix-hidden-lifetime-bug-0.2.0 (c (n "fix-hidden-lifetime-bug") (v "0.2.0") (d (list (d (n "proc-macros") (r "^0.2.0") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "1mbjkr5wz6al2cz5lmj6v5qa6cjmhl9nhb8idl882vkjfvhvmw65") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.1-rc1 (c (n "fix-hidden-lifetime-bug") (v "0.2.1-rc1") (d (list (d (n "proc-macros") (r "^0.2.1-rc1") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "1zi8brl165bwb5gs2b42y48mhcl2mpwm6ygi7zbffic332vjd4s4") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.1-rc2 (c (n "fix-hidden-lifetime-bug") (v "0.2.1-rc2") (d (list (d (n "proc-macros") (r "^0.2.1-rc2") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "05pfi2d65p7a0jpmm4lwqymm441mkiff8hr28jfycizfda16433a") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.1 (c (n "fix-hidden-lifetime-bug") (v "0.2.1") (d (list (d (n "proc-macros") (r "^0.2.1") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "0x01aw4cwgi2zjn4nhff29grzyfw0xlq6vd9kpiryl2mzwny4lrk") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.2-rc1 (c (n "fix-hidden-lifetime-bug") (v "0.2.2-rc1") (d (list (d (n "proc-macros") (r "^0.2.2-rc1") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "021sgljci6z9fyipv18hn1cqr3rq7bz7h9a14k5ksv80kamzh695") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.2 (c (n "fix-hidden-lifetime-bug") (v "0.2.2") (d (list (d (n "proc-macros") (r "^0.2.2") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "044cg03i6337sdj67a0hmq82g5ai8abanws8fsp0lbfm901qsxpq") (f (quote (("showme" "proc-macros/showme") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.3-rc1 (c (n "fix-hidden-lifetime-bug") (v "0.2.3-rc1") (d (list (d (n "proc-macros") (r "^0.2.3-rc1") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "1583lhnf0jpg267rrs7c35zhj5x6gf8kiyji2j0xgq2rd5s7d1pb") (f (quote (("showme" "proc-macros/showme") ("nightly") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.3-rc2 (c (n "fix-hidden-lifetime-bug") (v "0.2.3-rc2") (d (list (d (n "proc-macros") (r "^0.2.3-rc2") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "0kpn8qh48v9hvnplvv0095gxpydfpy2fsalyp33m9654sadv33kd") (f (quote (("showme" "proc-macros/showme") ("nightly") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.3 (c (n "fix-hidden-lifetime-bug") (v "0.2.3") (d (list (d (n "proc-macros") (r "^0.2.3") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "11si40cms125yaw8ncpb7b3bf41rdfhz5hwgyvz73c0xm1n2352k") (f (quote (("showme" "proc-macros/showme") ("nightly") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.4 (c (n "fix-hidden-lifetime-bug") (v "0.2.4") (d (list (d (n "automod") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macros") (r "^0.2.4") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "0f1rj813a60rqvq0nz9jl8zrbggh1ql0n7rrhm8vqyyly4wygdxl") (f (quote (("showme" "proc-macros/showme") ("nightly") ("default" "proc-macros"))))))

(define-public crate-fix-hidden-lifetime-bug-0.2.5 (c (n "fix-hidden-lifetime-bug") (v "0.2.5") (d (list (d (n "automod") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macros") (r "^0.2.5") (o #t) (d #t) (k 0) (p "fix-hidden-lifetime-bug-proc_macros")))) (h "1j1kkklp8xdiy93hnbv736ccbjkdgnbgzaa09qyrhqx62qh9rbnl") (f (quote (("showme" "proc-macros/showme") ("nightly") ("default" "proc-macros"))))))

