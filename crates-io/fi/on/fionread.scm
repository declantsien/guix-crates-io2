(define-module (crates-io fi on fionread) #:use-module (crates-io))

(define-public crate-fionread-0.1.0 (c (n "fionread") (v "0.1.0") (d (list (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.32") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kllrq15dl3dl87p2pvm8hqr379nafqwnl9ahkvfq8nmc3rqrq35")))

(define-public crate-fionread-0.1.1 (c (n "fionread") (v "0.1.1") (d (list (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.32") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0chxd2k7ha3vznfll59dwv3qs330n6z855bvcr08vs67zhchnczz")))

