(define-module (crates-io fi gy figyel_grrs_0001) #:use-module (crates-io))

(define-public crate-figyel_grrs_0001-0.1.0 (c (n "figyel_grrs_0001") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05wxbvv9z5g40xi3wq7arfc47gckvylpzy4x0sgimmgkfscbxyc5")))

