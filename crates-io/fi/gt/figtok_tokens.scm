(define-module (crates-io fi gt figtok_tokens) #:use-module (crates-io))

(define-public crate-figtok_tokens-0.1.0 (c (n "figtok_tokens") (v "0.1.0") (d (list (d (n "colors-transform") (r "^0.2.4") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "css_math") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "112bfp5fjssq7wqmchilccs81wf5fxhhmr89dy661py6cnv90d64")))

