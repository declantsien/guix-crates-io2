(define-module (crates-io fi ji fiji) #:use-module (crates-io))

(define-public crate-fiji-0.1.0 (c (n "fiji") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive" "extern_crate_std" "min_const_generics"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)) (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "vulkano") (r "^0.30.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.30.0") (d #t) (k 0)) (d (n "vulkano-util") (r "^0.30.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.30.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "0rk3j76z8i4v8qmmivfn9hwwg756kxhwhg3nlv4b7cadl1khmwiz")))

