(define-module (crates-io fi ps fips203-ffi) #:use-module (crates-io))

(define-public crate-fips203-ffi-0.2.1 (c (n "fips203-ffi") (v "0.2.1") (d (list (d (n "fips203") (r "^0.2.1") (d #t) (k 0)))) (h "02h5yhpc97dbqkncrfabjxd1dvvwlb3bq5fhqhcmnmvp5h4i9rh7") (r "1.70")))

