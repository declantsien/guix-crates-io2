(define-module (crates-io fi ps fips_plugin_registry) #:use-module (crates-io))

(define-public crate-fips_plugin_registry-0.1.0 (c (n "fips_plugin_registry") (v "0.1.0") (d (list (d (n "json_dotpath") (r "^1.0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1pbib8myxdqhhzc0hi2gldj1czzxfr33x3lk991rqgw9sllam2vh") (f (quote (("logging"))))))

