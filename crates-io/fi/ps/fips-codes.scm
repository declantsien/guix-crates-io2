(define-module (crates-io fi ps fips-codes) #:use-module (crates-io))

(define-public crate-fips-codes-0.1.0 (c (n "fips-codes") (v "0.1.0") (h "05wpvf50ywzdkly9rbh9l838nxw8vafm9qgkhf28z8gz67qzx65j")))

(define-public crate-fips-codes-0.1.1 (c (n "fips-codes") (v "0.1.1") (h "0ci0sd7zhpw4ldslj9ich9fn4acr2rfx9q0ja1zy2pi3illl8163")))

