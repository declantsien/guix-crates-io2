(define-module (crates-io fi ps fips_utility) #:use-module (crates-io))

(define-public crate-fips_utility-0.1.0 (c (n "fips_utility") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1q98ibphyb90bihssspf4wyfp42vc4hdvw6vvm2138xjw6ab4958")))

