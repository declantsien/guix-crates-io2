(define-module (crates-io fi ps fips_terminal_ui) #:use-module (crates-io))

(define-public crate-fips_terminal_ui-0.1.0 (c (n "fips_terminal_ui") (v "0.1.0") (d (list (d (n "colorgrad") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "fips_configuration") (r "^0.1.0") (d #t) (k 0)) (d (n "fips_plugin_registry") (r "^0.1.0") (d #t) (k 0)) (d (n "fips_utility") (r "^0.1.0") (d #t) (k 0)) (d (n "gradient_tui_fork") (r "^0.19") (f (quote ("crossterm"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "17ys7hxfrbf00bfgpnq0rniqa7jjj61np0ys0v6bk6ragn8q24zs")))

