(define-module (crates-io fi dd fiddler-macros) #:use-module (crates-io))

(define-public crate-fiddler-macros-0.1.0 (c (n "fiddler-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0srqcxbirw299qmgj2xv50s0kywvi0560hsrf24ri2i1wla0c8a7")))

(define-public crate-fiddler-macros-0.1.1 (c (n "fiddler-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1b947n3mygpp7rnxmrwvwc8ixbb2ddxsd3aghf1p36cq4rkn2zsh")))

(define-public crate-fiddler-macros-0.2.1 (c (n "fiddler-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1a6klpinbvzz1inrnj4wgrm697rlfqdk3s03d7ilx3lsj3ssz665")))

(define-public crate-fiddler-macros-0.3.0 (c (n "fiddler-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1jy06v4b6i852wyf1a450fk5374bnbflkpq00m3wj67l077mwg38")))

(define-public crate-fiddler-macros-0.3.1 (c (n "fiddler-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1rh7hakxaram4m9s3hyc5h439wj33iwzy3s00acbpk1bvw4jiygi")))

