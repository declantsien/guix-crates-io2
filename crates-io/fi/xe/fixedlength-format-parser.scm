(define-module (crates-io fi xe fixedlength-format-parser) #:use-module (crates-io))

(define-public crate-fixedlength-format-parser-0.1.0 (c (n "fixedlength-format-parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "166rvsvpkqxciamnhn28n5wc86b954kl277nrs9v957ch7p28cmj")))

(define-public crate-fixedlength-format-parser-0.1.1 (c (n "fixedlength-format-parser") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0mqbfj8zn2li3rqwqf166iwp587b08b8fkp3ydi7c61ginbac8yc")))

