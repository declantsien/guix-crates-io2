(define-module (crates-io fi xe fixed-polynomial) #:use-module (crates-io))

(define-public crate-fixed-polynomial-0.1.0 (c (n "fixed-polynomial") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "12pc0p69jd5p3hkbx4ppvjdw6449mwk31ijazyhix7xz3avjim9b")))

