(define-module (crates-io fi xe fixed-codec-derive) #:use-module (crates-io))

(define-public crate-fixed-codec-derive-0.1.0 (c (n "fixed-codec-derive") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "muta-protocol") (r "^0.1.0-alpha.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rlp") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p3yd27qdg0wc7ap93w59gcv1k3m0y6h7q4xx4acckn3bxrankkn")))

