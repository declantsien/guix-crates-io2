(define-module (crates-io fi xe fixedstep) #:use-module (crates-io))

(define-public crate-fixedstep-0.1.0 (c (n "fixedstep") (v "0.1.0") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)))) (h "0s0z0rb1wfzk1nvfbzmis7xfqil99kjravrklxswfxz7zyw66sqq")))

(define-public crate-fixedstep-0.1.1 (c (n "fixedstep") (v "0.1.1") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)))) (h "1j3ma8j470dqf5n16ybcjm74ssxn209730gnxbg2wvwkdggaxa4x")))

(define-public crate-fixedstep-0.2.0 (c (n "fixedstep") (v "0.2.0") (h "1hlqzna02hqpkhm0r23dwg0pi3nrcwcssz0cx3ad61paq2mfw2y7")))

(define-public crate-fixedstep-0.3.0 (c (n "fixedstep") (v "0.3.0") (h "1s30jm4s95k9i95569mw7z4hl9v35bgqcvicswl94myx3jn1avf0")))

