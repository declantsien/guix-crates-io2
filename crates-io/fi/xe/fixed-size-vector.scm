(define-module (crates-io fi xe fixed-size-vector) #:use-module (crates-io))

(define-public crate-fixed-size-vector-0.1.0 (c (n "fixed-size-vector") (v "0.1.0") (h "1sy3xav3n5lz0424yzvwc0yk7165avx4hfy36vchc0kclggnwb9c")))

(define-public crate-fixed-size-vector-0.1.1 (c (n "fixed-size-vector") (v "0.1.1") (h "0rwqkaxcpw8d15d4l0i04r6rbh7aav9rmpg5g9pwl1h3xvq8gysz")))

(define-public crate-fixed-size-vector-0.2.0 (c (n "fixed-size-vector") (v "0.2.0") (h "15yl9cr29vf9idlc3ia7cbs0cggwy8xmgr7ignrk9mpmszckbj3v")))

(define-public crate-fixed-size-vector-0.2.1 (c (n "fixed-size-vector") (v "0.2.1") (h "0zcg4xrfmkagpj6r7gw4wxk8gh8zchchqgh8l5p2bwwwxq4wr0xr")))

(define-public crate-fixed-size-vector-0.2.2 (c (n "fixed-size-vector") (v "0.2.2") (h "0sz61kx7hx8l2cpfi35xa4vxqzkn09cl3y758d6l92ij8gl36vnv")))

