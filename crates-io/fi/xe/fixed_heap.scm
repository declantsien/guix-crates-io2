(define-module (crates-io fi xe fixed_heap) #:use-module (crates-io))

(define-public crate-fixed_heap-0.1.0 (c (n "fixed_heap") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0wcjgpk3gmmx72xkvkzwmaxcp3j4zv7fdx6ckybjr15yic100322") (f (quote (("unstable") ("default"))))))

(define-public crate-fixed_heap-0.2.0 (c (n "fixed_heap") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "120l2q7v1crv7j7lvhassf9km3b1c22nsclpmx8nvqa8wwlsg3a9") (f (quote (("unstable") ("default"))))))

