(define-module (crates-io fi xe fixed_free_list) #:use-module (crates-io))

(define-public crate-fixed_free_list-0.1.0 (c (n "fixed_free_list") (v "0.1.0") (h "017gifhqx2v3g29y739brxijp16dc08gaq80kz5r0crhsbavqnf7") (f (quote (("unstable") ("strict") ("default"))))))

(define-public crate-fixed_free_list-0.2.0 (c (n "fixed_free_list") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1mdk0qjn6fjkgppls9cdzrp8cq4ysy29lpgp8a7lyjmsxvmw1w76") (f (quote (("unstable") ("strict") ("default"))))))

