(define-module (crates-io fi xe fixed-macro-impl) #:use-module (crates-io))

(define-public crate-fixed-macro-impl-1.0.0 (c (n "fixed-macro-impl") (v "1.0.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "077jhjzw9n5nj0wbg3wmiz7fh9fanl6chi3qamryq35nwhp8y5c5")))

(define-public crate-fixed-macro-impl-1.1.0 (c (n "fixed-macro-impl") (v "1.1.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "138zj4wp3d213jkgfga32n6y8rizhy9fbk30a1fh5s8g28zpaj0x")))

(define-public crate-fixed-macro-impl-1.1.1 (c (n "fixed-macro-impl") (v "1.1.1") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0gi8lhh9xfnmd32sp01aap25h6dx2l548jwzg629wi48pzhahzvq")))

(define-public crate-fixed-macro-impl-1.2.0 (c (n "fixed-macro-impl") (v "1.2.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0jrd82k8yjv2xwb7dwpdz39wc2lrrpsr5sh0wg2vk8f0f7s8cc69")))

