(define-module (crates-io fi xe fixedstr) #:use-module (crates-io))

(define-public crate-fixedstr-0.1.0 (c (n "fixedstr") (v "0.1.0") (h "0z1cmgvyvii5awqw8rz9qsgcj0zcmqh7s0phpy47irpfy80a6yqv")))

(define-public crate-fixedstr-0.1.1 (c (n "fixedstr") (v "0.1.1") (h "1yz7285z2v16gq0kjjfyca6lakif8vp755gxmgyww7dfgg6ss5ig")))

(define-public crate-fixedstr-0.1.2 (c (n "fixedstr") (v "0.1.2") (h "1nbls6m0ps9n2sx1x0q66z3kx6xr45kmq8lfwsajw86j2mfk10mn")))

(define-public crate-fixedstr-0.2.0 (c (n "fixedstr") (v "0.2.0") (h "1hbqw5yjhdlzjsxql4yrbfr2przphsaqpibmrvnam2fwdfk7rl9r")))

(define-public crate-fixedstr-0.2.1 (c (n "fixedstr") (v "0.2.1") (h "1b72ls16w1b2xq5p7bdd15x81ggglppwg0pg74j2swwrgffv4r75")))

(define-public crate-fixedstr-0.2.2 (c (n "fixedstr") (v "0.2.2") (h "1w530w0wk7zn23p27iwhyfz476nrrwa0ycfg2k2cx0mh1l11sl1p")))

(define-public crate-fixedstr-0.2.3 (c (n "fixedstr") (v "0.2.3") (h "1kb0mv2shl8lq4hy9dwsr91vr9y46biyq9l07nnjj3nynlzv2a7m")))

(define-public crate-fixedstr-0.2.4 (c (n "fixedstr") (v "0.2.4") (h "0njyqh15hchapfdc5mfh3fhi8yg9zjrdzbjsphjjxqp3dj4glh67")))

(define-public crate-fixedstr-0.2.5 (c (n "fixedstr") (v "0.2.5") (h "082v6840asi5fjxypdig3913m7s21jjf0r76kq9ym8csdm9jvxi6")))

(define-public crate-fixedstr-0.2.6 (c (n "fixedstr") (v "0.2.6") (h "0ijcp8ws0ss36r100clbzim707sn4wjy71s1arpx6m2g4khiakkd")))

(define-public crate-fixedstr-0.2.7 (c (n "fixedstr") (v "0.2.7") (h "0zgj4vhxq5ildbpsgn7f5sjsyw5sinpiiaa23dcn4439vm6gnjm1")))

(define-public crate-fixedstr-0.2.8 (c (n "fixedstr") (v "0.2.8") (h "0h8wpq7sbzdfxyyc5z8h91g9306mjza156q6vszbmvbac8fxa7mj")))

(define-public crate-fixedstr-0.2.9 (c (n "fixedstr") (v "0.2.9") (h "1yg8r5mkvm5valcsgwc70s4b21y546s3aqnx3d1f5li6pnw8fhni")))

(define-public crate-fixedstr-0.2.10 (c (n "fixedstr") (v "0.2.10") (h "1aaawi9azplcz98n3afm36kg61iqb0ll0zqs157cr8qdg17g7d1d")))

(define-public crate-fixedstr-0.2.11 (c (n "fixedstr") (v "0.2.11") (h "1wgfsd5574657gyaqn3c6mzprm10dwsp94anf0zfyrs6d0nyax9x")))

(define-public crate-fixedstr-0.2.12 (c (n "fixedstr") (v "0.2.12") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1w6hlp6hrd99krqx841rxcacx117cjpnvizv4wnlxfczkhdc6c7q") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.3.0 (c (n "fixedstr") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0wlwm2xr3gf1hc715wama97lbqp6a271i1q4bpqsj6akkfklfzqi") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.3.1 (c (n "fixedstr") (v "0.3.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0gb85549yy8rl4v5zrqp1d72j0vvh177h57r0r2r371h01qlc452") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.3.2 (c (n "fixedstr") (v "0.3.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09gvj5f92qwkpsgq720r8fakcixb1rrdi291hyxwl999xf5pl7pc") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.3.3 (c (n "fixedstr") (v "0.3.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1d1n6zk4cq06gsnz0638gqg38zdlbfq6f74iml4i2axwrxa1i4yp") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.3.4 (c (n "fixedstr") (v "0.3.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1r0dll5b3cjmbzq5vgfm2pn658ppzf3jl1dr5nr07vcxs6db7c7h") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.0 (c (n "fixedstr") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "163c1lnzj7p6skjc6fhb97mcbdd3n115s32d2ss0g0zdxmrkr9za") (f (quote (("std") ("pub_tstr") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.1 (c (n "fixedstr") (v "0.4.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "124zn5hsqz23zgia9xsjq4fbv9jp2vvk0bqs80phyf5nmhc0xdvx") (f (quote (("std") ("pub_tstr") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.2 (c (n "fixedstr") (v "0.4.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "05jc8jmlkq89iz269v9j3nkl09idq7gl6611mbcgmdnk3amsvapd") (f (quote (("std") ("pub_tstr") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.3 (c (n "fixedstr") (v "0.4.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0a1cd65hihp3pkfqysl44zxsf1w9gn7bcjk3czszk82bb8fbj4hq") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("flex-str") ("default" "std" "flex-str" "shared-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.4 (c (n "fixedstr") (v "0.4.4") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0wjym8n8pn3rdpr654inj4vnsvpiwqvakjh6jk10l1yfpdab60mx") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("flex-str") ("default" "std" "flex-str" "shared-str") ("circular-str")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.5 (c (n "fixedstr") (v "0.4.5") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1n75n580cnk1vrlyjsyp22nyn5ynm309b9dv9gq1c28v32d1jja8") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("flex-str") ("default" "std" "flex-str" "shared-str") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.4.6 (c (n "fixedstr") (v "0.4.6") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0r5bcpziymd6g9kbswhqkl5aif1lkclsxdmf37f6f7lxpnjsdcdf") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("flex-str") ("default" "std" "flex-str" "shared-str") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.0 (c (n "fixedstr") (v "0.5.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1fwrsv1qggj6jzm83r45snid1nxscn1azdwr973lixv52rhacscm") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("fstr" "std") ("flex-str") ("experimental") ("default") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.1 (c (n "fixedstr") (v "0.5.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "17wbvcb5yvs3scmipgbravcw0gd61jnc3cyi6nyb4d5kpdwlakgq") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.2 (c (n "fixedstr") (v "0.5.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1kwchqv1zgip2w9803lk674iq1bj8xfw9vhj5zcgmlswjygcyl3v") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.3 (c (n "fixedstr") (v "0.5.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0bxh2ip3677j56nr7qv446sl8rc7259mva2bj3hcgkqdvh57bd0z") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.4 (c (n "fixedstr") (v "0.5.4") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "03i0qwri1z3f8h6zl3287vr71zzxzkmq3hdpsk5hpisy5i72gya9") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.5 (c (n "fixedstr") (v "0.5.5") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1a59212bqawh50adn94rn09k8spvzl1dccg525ysp45myzz4skjz") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("compressed-str") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-fixedstr-0.5.6 (c (n "fixedstr") (v "0.5.6") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0c56id0y3rcylnhww639fj5ci0558m9rfizvi504wcd5mdakk47k") (f (quote (("std") ("shared-str") ("pub_tstr") ("pub-tstr" "pub_tstr") ("no-alloc") ("fstr" "std") ("flex-str") ("experimental") ("default") ("compressed-str") ("circular-str")))) (s 2) (e (quote (("serde" "dep:serde"))))))

