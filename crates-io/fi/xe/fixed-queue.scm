(define-module (crates-io fi xe fixed-queue) #:use-module (crates-io))

(define-public crate-fixed-queue-0.0.1 (c (n "fixed-queue") (v "0.0.1") (h "1ninswp8rdira2abhcnaf2x8cdwdwsa9gi0rls2sn5kxbkacyfcp")))

(define-public crate-fixed-queue-0.1.1 (c (n "fixed-queue") (v "0.1.1") (h "0ikh488ckk8x4ki8947y0y5xi8ay7zyrzxzm56l3kamnwgzcylqn")))

(define-public crate-fixed-queue-0.1.2 (c (n "fixed-queue") (v "0.1.2") (h "01w1y1xd794yd3hz7p930ckwsarbyxcm804xqvg12f5q2d8mf3gy")))

(define-public crate-fixed-queue-0.2.0 (c (n "fixed-queue") (v "0.2.0") (h "1vm48awpswhq06ghcjq0mgp5qq3jsj0jm0y6gw4cyj0m3nzni85a")))

(define-public crate-fixed-queue-0.3.0 (c (n "fixed-queue") (v "0.3.0") (h "19rp4nxd4060vbmwwm1zaa60p3zdxvlfabibvi149zfx0fwmhbj5")))

(define-public crate-fixed-queue-0.3.2 (c (n "fixed-queue") (v "0.3.2") (h "0s5gj5wcx2pv4mxyzzi65fqq6975xsv14q8516skp2fv0iy3438b")))

(define-public crate-fixed-queue-0.3.3 (c (n "fixed-queue") (v "0.3.3") (h "15li78zw206s3g6ivavm50rgpc20gjp6kqlasqdq84la9bxz9b1f")))

(define-public crate-fixed-queue-0.3.5 (c (n "fixed-queue") (v "0.3.5") (h "0i8368if0l5lzngqw4c1kc9jjyhcph66izqi16z2gzcckaydhkb3")))

(define-public crate-fixed-queue-0.3.6 (c (n "fixed-queue") (v "0.3.6") (h "0nm5hs6abkamrnnkaziphaf1vmq31kn1hn81ksalamxqkrpg9czp")))

(define-public crate-fixed-queue-0.3.7 (c (n "fixed-queue") (v "0.3.7") (h "0sncjs8c5yjsjnj0mkaa4r601qqkb026i6ggqypfpwbmb6npsyfg")))

(define-public crate-fixed-queue-0.3.8 (c (n "fixed-queue") (v "0.3.8") (h "06bhv8nfgzm0zgdh2692w7bvj9xx9xrsvsb4lb7sxkcgbcc2mdph")))

(define-public crate-fixed-queue-0.3.9 (c (n "fixed-queue") (v "0.3.9") (h "0xqfm41blx88mrsw2jlxscxz9rshyv893pkc6cxi53c2yi4h4klr")))

(define-public crate-fixed-queue-0.3.10 (c (n "fixed-queue") (v "0.3.10") (h "16mcvl0in3ffbbh0769615qg0r5l64xfzf7d0hg3p8f6jpd9l4cy")))

(define-public crate-fixed-queue-0.3.11 (c (n "fixed-queue") (v "0.3.11") (h "1cal8aqv140p9rkj3fhdm7wda9xsigcyiyl7hh2m0d1yw12wispj")))

(define-public crate-fixed-queue-0.4.0 (c (n "fixed-queue") (v "0.4.0") (d (list (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "0jlw51ghsqdn3gi83bqsrx9p9cxlldnszbd4x8l5ll9vl6idl0if")))

(define-public crate-fixed-queue-0.4.1 (c (n "fixed-queue") (v "0.4.1") (d (list (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "1skr22779mk6zg2zv3d1dbpcmdsc79jpq0n90a1fv7j755gs4qh7")))

(define-public crate-fixed-queue-0.4.2 (c (n "fixed-queue") (v "0.4.2") (d (list (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "0vbxiwig5kg195ygs5a1vb4jc2i6rrr4df9rx5hnyj6xl23gyii6")))

(define-public crate-fixed-queue-0.4.3 (c (n "fixed-queue") (v "0.4.3") (d (list (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "0biqdajznbg040ybyh9pljpwnvac5ivhixksfh9hrwnhpxyldgam")))

(define-public crate-fixed-queue-0.4.5 (c (n "fixed-queue") (v "0.4.5") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "1hk3pvn4q88f9lhfqcvl11d0ysqixl9la120gnr5m9cll87pnrcs")))

(define-public crate-fixed-queue-0.4.6 (c (n "fixed-queue") (v "0.4.6") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "0q87mqcbxc2lmgxr3n5im5fnyvhl498cs84wyy9a4cb4pyng5vig")))

(define-public crate-fixed-queue-0.4.7 (c (n "fixed-queue") (v "0.4.7") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "0vqfqav268759vapm2nylwic28l0hfy070j7fz1073gqgyjz79zp")))

(define-public crate-fixed-queue-0.4.8 (c (n "fixed-queue") (v "0.4.8") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "13byx8v6q41q8zndlshqjca5lfji7qq36iqvgsqfwhkzhmv6qgv2")))

(define-public crate-fixed-queue-0.4.9 (c (n "fixed-queue") (v "0.4.9") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "etime") (r "^0.1") (d #t) (k 2)))) (h "1h1666yyargzaba2s160sqzx8qikp0zckcyv545hr5aqr9qr86va")))

(define-public crate-fixed-queue-0.4.10 (c (n "fixed-queue") (v "0.4.10") (d (list (d (n "ach") (r "^0.1") (d #t) (k 0)))) (h "0r9r5nv2rgas1q1is8x6zvmvvrdyk4hij9gbrklg2mz0fmbn5ipy")))

(define-public crate-fixed-queue-0.5.0 (c (n "fixed-queue") (v "0.5.0") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "19kffpnz4a12a32ciyslk1wdn77136ik2clm2bw7gxfd29dxbqf4")))

(define-public crate-fixed-queue-0.5.1 (c (n "fixed-queue") (v "0.5.1") (d (list (d (n "on_drop") (r "^0.1") (d #t) (k 2)))) (h "1gm1pwigf98dyspqaaxsza3iklab8rkyxb5lsbk2njv67r7sv992")))

