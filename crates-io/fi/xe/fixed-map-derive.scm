(define-module (crates-io fi xe fixed-map-derive) #:use-module (crates-io))

(define-public crate-fixed-map-derive-0.1.0 (c (n "fixed-map-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0vxpnq24q49qg5kr77y44k1bxr7ygnbyz8yda73255b8p6rqyvmn")))

(define-public crate-fixed-map-derive-0.1.1 (c (n "fixed-map-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "12vhn8fdgc0ariy79vrhaavs5i5qwsna2praaqpl3gida1wdybf6")))

(define-public crate-fixed-map-derive-0.1.2 (c (n "fixed-map-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "11jsvi425xwrm1pj50zqvvkrv9wmhnbwns4wq56wks0ji60myahf")))

(define-public crate-fixed-map-derive-0.1.3 (c (n "fixed-map-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "05a35bd3dl89hwqb3vai69r29x171zx2yrcfzzzhc85riflnwmk2")))

(define-public crate-fixed-map-derive-0.2.0 (c (n "fixed-map-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04j4gv54r1hg180vgkpkvyrah8gv77lbvmwzkxfv0819ryzbchgx")))

(define-public crate-fixed-map-derive-0.3.0 (c (n "fixed-map-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "133x1wjb4804vp23cy3wfz0wyzyw85fgzp9chsrbm5r47hd080ci")))

(define-public crate-fixed-map-derive-0.3.1 (c (n "fixed-map-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02lsi0zf9ckknsa4j8bmzl4dnwzjzh33d78mlcy030fs0xdhaafq")))

(define-public crate-fixed-map-derive-0.3.2 (c (n "fixed-map-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "16aw61af5d62js91h2hhhipplwlwcg66f902hp9cjq9r2gwainjg")))

(define-public crate-fixed-map-derive-0.3.3 (c (n "fixed-map-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1xk1m9ir0ycjnkxrrnwldp63b90pa6h2dm13bm1l0l5ra5yj30c1")))

(define-public crate-fixed-map-derive-0.4.0 (c (n "fixed-map-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1j8li9m5p9bv5szfzci2s7aggwxs1ia5n6pc1vlm05apik0zajqp")))

(define-public crate-fixed-map-derive-0.5.0 (c (n "fixed-map-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0pnpp6smfp2ixmdphiw2cmb7ichsvdqk1zqip8ifv0wdxdkfqpn8")))

(define-public crate-fixed-map-derive-0.6.1 (c (n "fixed-map-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1r80iy3p4khqzs4mwfwrynn94pq7jdrlqrlsln2z7ggm0dq66aff")))

(define-public crate-fixed-map-derive-0.7.0 (c (n "fixed-map-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0spj1hknshk8krrmsdw4ps9md8vxmd3cimngz9qrs2jssr7b3lvs")))

(define-public crate-fixed-map-derive-0.8.0-alpha.1 (c (n "fixed-map-derive") (v "0.8.0-alpha.1") (d (list (d (n "fixed-map") (r "^0.8.0-alpha.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0m3spqy58s6pdns0mvc88lvkjzc0iwqlrrg4wk700l2208g9nbvb")))

(define-public crate-fixed-map-derive-0.8.0-alpha.2 (c (n "fixed-map-derive") (v "0.8.0-alpha.2") (d (list (d (n "fixed-map") (r "^0.8.0-alpha.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0l8qr18hx8j0b4xd5z8h4iyad0f9ai9gdkwg6q26kcfsgjncyv4f")))

(define-public crate-fixed-map-derive-0.8.0 (c (n "fixed-map-derive") (v "0.8.0") (d (list (d (n "fixed-map") (r "^0.8.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "06h86z3wj7bm8cil10169gf17xjvjk9wkdwf1zwbl5yy3r76cc53")))

(define-public crate-fixed-map-derive-0.8.1 (c (n "fixed-map-derive") (v "0.8.1") (d (list (d (n "fixed-map") (r "^0.8.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0aawk20ydc6hrlvk0npgycy44by0hk2zmf061wq0d2ka85s8grv7")))

(define-public crate-fixed-map-derive-0.8.2 (c (n "fixed-map-derive") (v "0.8.2") (d (list (d (n "fixed-map") (r "^0.8.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1w8szn8cz76yyrv6lma9vb6c88wjihgv5hdrv2sl7f7w0y73zrkv") (r "1.65")))

(define-public crate-fixed-map-derive-0.9.0 (c (n "fixed-map-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0h5m2kp67ay7y8z6i946iap869lpyl60j0wcyv0kcnbn1b0x8w6b") (r "1.65")))

(define-public crate-fixed-map-derive-0.9.1 (c (n "fixed-map-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hpn08fv4h42x6v6wnwdd6hglq41kq9fy32l5dyxq3cya46j16yz") (r "1.65")))

(define-public crate-fixed-map-derive-0.9.2 (c (n "fixed-map-derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1b00692bj121v9n2i2mxmf8ldlcdakm8c1apgx2mpskj1k0fv58v") (r "1.65")))

(define-public crate-fixed-map-derive-0.9.3 (c (n "fixed-map-derive") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "118wxm4z44iabjmkx04pll2amflh2kxb30x8mhbpcdxpvap2js31") (r "1.65")))

(define-public crate-fixed-map-derive-0.9.4 (c (n "fixed-map-derive") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "02pjfb54pbn3a2ql4fz5y94a5gv7xjkndhjd5b5zhf5xxazj9995") (r "1.72")))

(define-public crate-fixed-map-derive-0.9.5 (c (n "fixed-map-derive") (v "0.9.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "01r4prn1lybkwyiyi3ps9cnp1h5276dyjp1ccj0gpfi66g5skivd") (r "1.72")))

