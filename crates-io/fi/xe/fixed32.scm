(define-module (crates-io fi xe fixed32) #:use-module (crates-io))

(define-public crate-fixed32-0.0.1 (c (n "fixed32") (v "0.0.1") (h "11aykvzigp1s7947k3zvkalkqfj4nsfkf2wc8rhznv1m9apjnm64")))

(define-public crate-fixed32-0.0.2 (c (n "fixed32") (v "0.0.2") (h "05x1rpj8kiph4zhr7pfslf3b7vfggdy15farnr7krdlm8aw3mxs4")))

(define-public crate-fixed32-0.0.3 (c (n "fixed32") (v "0.0.3") (h "12g52g36y0j4ngs7lj889qlr2ka9bfsidbbldgs9k1lj4v60378s")))

(define-public crate-fixed32-0.0.4 (c (n "fixed32") (v "0.0.4") (h "0k5z4l8a20zchsbp1xncn9zz7h7rbwc3w4cgw1pxy38fqirnqv71")))

(define-public crate-fixed32-0.0.5 (c (n "fixed32") (v "0.0.5") (h "0h91b5bnz4q88iyf54m6l2bm8qi1zsagzjhdr0jbzj5i475a9rs1")))

