(define-module (crates-io fi xe fixed2csv) #:use-module (crates-io))

(define-public crate-fixed2csv-1.0.0 (c (n "fixed2csv") (v "1.0.0") (d (list (d (n "csv") (r "^1.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "humansize") (r "^1") (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "161z81az8qlalhinv8cy7g5n45mpix581nrm1wsqvkil23hv4ywm")))

