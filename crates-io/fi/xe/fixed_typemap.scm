(define-module (crates-io fi xe fixed_typemap) #:use-module (crates-io))

(define-public crate-fixed_typemap-0.1.0 (c (n "fixed_typemap") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fixed_typemap_internals") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed_typemap_macros") (r "^0.1.0") (d #t) (k 0)))) (h "02kir0pccf706dl41m46xqq8xi783n0l05jzlnav5jdl983vyp6s")))

