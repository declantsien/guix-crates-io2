(define-module (crates-io fi xe fixed-filters) #:use-module (crates-io))

(define-public crate-fixed-filters-0.1.0 (c (n "fixed-filters") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "fixed") (r "^2.0.0-alpha.12") (d #t) (k 0)) (d (n "micromath") (r "^2.1.0") (d #t) (k 0)))) (h "1y44jrj3kgiv0y2pazwgzy6ji4pkglfnxibrvnqn6vbys3s3pcv5")))

