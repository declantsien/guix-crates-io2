(define-module (crates-io fi xe fixed-macro-types) #:use-module (crates-io))

(define-public crate-fixed-macro-types-1.0.0 (c (n "fixed-macro-types") (v "1.0.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.0.0") (d #t) (k 0)))) (h "1akwishhx1cns0n2587b2qmsjhq3iybkk6vgz1vy967m8wfq236j")))

(define-public crate-fixed-macro-types-1.1.0 (c (n "fixed-macro-types") (v "1.1.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.1.0") (d #t) (k 0)))) (h "0c9m0qs4njzr1f5m1c3ycvrndwgw8yd6x1yvazxd0zw9h1c6rn6s")))

(define-public crate-fixed-macro-types-1.1.1 (c (n "fixed-macro-types") (v "1.1.1") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.1.1") (d #t) (k 0)))) (h "006cszhvpd5940pgajshw3kqkiqiaqfv4g8mc2s42xh378gj9iqk")))

(define-public crate-fixed-macro-types-1.2.0 (c (n "fixed-macro-types") (v "1.2.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.2.0") (d #t) (k 0)))) (h "1r67jfwj2gxdzr8wxalbwm74s6x02b8kq35acmzll9m26jq62jh4")))

