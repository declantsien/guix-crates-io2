(define-module (crates-io fi xe fixed_circular_buffer) #:use-module (crates-io))

(define-public crate-fixed_circular_buffer-0.1.0 (c (n "fixed_circular_buffer") (v "0.1.0") (h "0xr1ffg23k1cgjxvhw9pgnvqaqmhimipa1xvnfhgsh63r4gyjrbj")))

(define-public crate-fixed_circular_buffer-0.2.0 (c (n "fixed_circular_buffer") (v "0.2.0") (h "09c1rlxinq5gd8v3hjz48rx8pddxsdffg4n5b0qkdygzkiv91mr1")))

(define-public crate-fixed_circular_buffer-0.2.2 (c (n "fixed_circular_buffer") (v "0.2.2") (h "0vw8ir36xnsc6njs6da2r0pvav45c866c8hzj47lsh4wq8610mgi")))

