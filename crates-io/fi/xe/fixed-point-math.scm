(define-module (crates-io fi xe fixed-point-math) #:use-module (crates-io))

(define-public crate-fixed-point-math-0.0.1 (c (n "fixed-point-math") (v "0.0.1") (h "1fkifa80bcdkzr9380z2i5dmy0770x100bxlyi3bjw52fahri51r") (y #t) (r "1.66")))

(define-public crate-fixed-point-math-0.0.2 (c (n "fixed-point-math") (v "0.0.2") (h "0h56anxgxxh9whl06vy2i6j1i79vf3gg1maivjbhs9qbrm64k33x") (r "1.66")))

(define-public crate-fixed-point-math-0.1.0 (c (n "fixed-point-math") (v "0.1.0") (h "1waq474ij1adf7c06phn8c5lqszrjkma8nlfph4nlm9qsrjw6a8c") (r "1.66")))

