(define-module (crates-io fi xe fixed-slice-vec) #:use-module (crates-io))

(define-public crate-fixed-slice-vec-0.2.0 (c (n "fixed-slice-vec") (v "0.2.0") (h "12ryykn290ib4hiddhym7f8kjicrsf7qi71590v66jzw332rkrhd") (y #t)))

(define-public crate-fixed-slice-vec-0.3.0 (c (n "fixed-slice-vec") (v "0.3.0") (h "02b6zijyy95iaqbr1h03rfr5r0h7lyb79gg38kj6mabkklwdqpd1") (y #t)))

(define-public crate-fixed-slice-vec-0.4.0 (c (n "fixed-slice-vec") (v "0.4.0") (h "0nqz01j5m0grxk5gvh1qjzq5p1685bf0hd3m1sk7pha3sddl1q0x") (y #t)))

(define-public crate-fixed-slice-vec-0.5.0 (c (n "fixed-slice-vec") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "1d1j91ncqzva83ylv8n03lgsb0lj15qdsyqsp22d9p7bnzrd2bxg") (y #t)))

(define-public crate-fixed-slice-vec-0.6.0 (c (n "fixed-slice-vec") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "135n54r2cknfa7z5d91pr98invw7sszvkni4fmix963h8104cqxx") (y #t)))

(define-public crate-fixed-slice-vec-0.7.0 (c (n "fixed-slice-vec") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "12dzkiba2hqmgdrj73vvkqzhn9jzg7dbhmprs62mmswbzspg37gf")))

(define-public crate-fixed-slice-vec-0.7.1 (c (n "fixed-slice-vec") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "1i52ja00psi7aya4md6v3kg1najqac6ld6r28mwl0xir4cr3515s")))

(define-public crate-fixed-slice-vec-0.8.0 (c (n "fixed-slice-vec") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "0i2bh5vgnl217bcgfriwwwy275nx6bq3fl3fxrq62ba0rvgwvyzs")))

(define-public crate-fixed-slice-vec-0.9.0 (c (n "fixed-slice-vec") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "0d8f4gjsqzxpjiqdh62k5bv98ii4gk8f08pzyzkqk2920zywk7xa")))

(define-public crate-fixed-slice-vec-0.10.0 (c (n "fixed-slice-vec") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "0vijclnqsyx15ma53x61r3n5b3fywlpr16chaa0vkxczk9ckrchv")))

