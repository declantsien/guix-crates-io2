(define-module (crates-io fi xe fixed2float) #:use-module (crates-io))

(define-public crate-fixed2float-0.1.0 (c (n "fixed2float") (v "0.1.0") (h "0xqkkx8354gj9hlhjkkga8p2kjfr983bvryy7y8vhvijdv10a2dr")))

(define-public crate-fixed2float-0.1.2 (c (n "fixed2float") (v "0.1.2") (h "0a2i1hwb1vqnr5cmwj6m48wr6910jnnm91h0x4bnxvhv2ywqp8rm")))

(define-public crate-fixed2float-1.0.0-rc.1 (c (n "fixed2float") (v "1.0.0-rc.1") (h "1z7b64789a1wybs5ricp2apnn6ndj2nm37jll3znwjvwy148ana2")))

(define-public crate-fixed2float-1.0.0 (c (n "fixed2float") (v "1.0.0") (h "1wg3b2kwz0dlg64c7ccg4nly23js1742mm85hdqhzzn7y2whigpr")))

(define-public crate-fixed2float-2.0.0 (c (n "fixed2float") (v "2.0.0") (h "0l99g5mf6mm5pv008kg1ly969fp5j6fn1x8yzhgzwhibip45n3a7")))

(define-public crate-fixed2float-2.0.1 (c (n "fixed2float") (v "2.0.1") (h "1ihqz3q3z9npghyynjvx0avvnp2kf2hwxnwvzk5xg4yj72r1bwx4")))

(define-public crate-fixed2float-2.0.2 (c (n "fixed2float") (v "2.0.2") (h "069nyhcfmsczakggfggifhjy5634miqskyq1bbq7myglqwf5ia37")))

(define-public crate-fixed2float-2.0.3 (c (n "fixed2float") (v "2.0.3") (h "1y11jcfxhdc4vhmkw9m0358xlynm6l0vpk6iqzyb5bl0q8xgmia5")))

(define-public crate-fixed2float-2.0.4 (c (n "fixed2float") (v "2.0.4") (h "1vvna54qs4iwz6gwpwnbg6rbvasnsy2z315wkmxxv92sj82zc7kw")))

(define-public crate-fixed2float-3.0.0-rc1 (c (n "fixed2float") (v "3.0.0-rc1") (h "0sbkmls5g0a39mwfi6d3f0xrn8vrmshd4zs20vg4dk5w8j7kzkyb")))

(define-public crate-fixed2float-3.0.0 (c (n "fixed2float") (v "3.0.0") (h "0jpsc0y21lrmphmxl95rlnfc9qn9nh8lm5z4s5dql8gvy9frmjvp")))

(define-public crate-fixed2float-4.0.0-rc1 (c (n "fixed2float") (v "4.0.0-rc1") (h "0j9shf4v5nbl8yd52p0nddvl2zzy4frynx3ck1nfwzb6dq6im6fg")))

(define-public crate-fixed2float-4.0.1 (c (n "fixed2float") (v "4.0.1") (h "14ihx1jn028q3ax7al6bxwidwrzi194gxi6syp5xc8zi89ib2lna")))

