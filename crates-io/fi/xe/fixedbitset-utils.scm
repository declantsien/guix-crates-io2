(define-module (crates-io fi xe fixedbitset-utils) #:use-module (crates-io))

(define-public crate-fixedbitset-utils-0.2.0 (c (n "fixedbitset-utils") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1.0") (d #t) (k 0)))) (h "1k11b331bxxz1x1p40gf2chl6misc58hhdssaia00m9kqwjzm9mb")))

(define-public crate-fixedbitset-utils-0.2.1 (c (n "fixedbitset-utils") (v "0.2.1") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)))) (h "0ry72zpq7snv8v30894ajhgdxx7n6bg08ikm70l4sx7ygjbfdgad")))

