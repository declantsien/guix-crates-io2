(define-module (crates-io fi xe fixed-macro) #:use-module (crates-io))

(define-public crate-fixed-macro-1.0.0 (c (n "fixed-macro") (v "1.0.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.0.0") (d #t) (k 0)) (d (n "fixed-macro-types") (r "^1.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fa48c2a4fvrf5bl3mji6sgfd84igcmz9p75mxjrdwnp679zjd0k")))

(define-public crate-fixed-macro-1.1.0 (c (n "fixed-macro") (v "1.1.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.1.0") (d #t) (k 0)) (d (n "fixed-macro-types") (r "^1.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02qvwwy56xpbgkf49gl70vchby32wijlgm31ks8k37hp2w269dka")))

(define-public crate-fixed-macro-1.1.1 (c (n "fixed-macro") (v "1.1.1") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.1.1") (d #t) (k 0)) (d (n "fixed-macro-types") (r "^1.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ghzhcs7awm8bqgc3iq16bz0m0z12adaaf2a6s9rz674csp2qsrz")))

(define-public crate-fixed-macro-1.2.0 (c (n "fixed-macro") (v "1.2.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "fixed-macro-impl") (r "^1.2.0") (d #t) (k 0)) (d (n "fixed-macro-types") (r "^1.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g3m2v5qwlcdqzyh38ldl3vqmmvv32ighjgld0lf056bz25c9l0z")))

