(define-module (crates-io fi xe fixed_trigonometry) #:use-module (crates-io))

(define-public crate-fixed_trigonometry-0.1.0 (c (n "fixed_trigonometry") (v "0.1.0") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)))) (h "1hp1zpswq70kq31y0l2ym563iswdv15jc9ahbw8rdq82crzwjn1v")))

(define-public crate-fixed_trigonometry-0.1.1 (c (n "fixed_trigonometry") (v "0.1.1") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)))) (h "088h95ngsz1d015fgdibnw0r0gf7kcrhzfhpxxkvvgb94mjrhxi1")))

(define-public crate-fixed_trigonometry-0.1.2 (c (n "fixed_trigonometry") (v "0.1.2") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1xpx6x0lzlvfcl47g2qlaj20wbjyj9m5imak0p7ig0dqa48cpny6")))

(define-public crate-fixed_trigonometry-0.2.0 (c (n "fixed_trigonometry") (v "0.2.0") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "14czhnl9q4ykhy9as70fkf089i8plnamdsznxy8bpfzkgcy6bmfy")))

(define-public crate-fixed_trigonometry-0.2.1 (c (n "fixed_trigonometry") (v "0.2.1") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1pkj5i93kwn1sbdnsbz2h4frg9shb2wjsynszxzb9l43ngk8bxjc")))

(define-public crate-fixed_trigonometry-0.2.2 (c (n "fixed_trigonometry") (v "0.2.2") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "15yxj4q4spq93yaf4m4pxqjknsfy08dbv86b8p18b2pbysq99kv0")))

(define-public crate-fixed_trigonometry-0.2.3 (c (n "fixed_trigonometry") (v "0.2.3") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1y0myl78rx5jswhhrp8v69c9smcfzw6gflaa6f28plvspcsyw4rw")))

(define-public crate-fixed_trigonometry-0.2.4 (c (n "fixed_trigonometry") (v "0.2.4") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "022ap9p8px37xpdxjdl2lxka6x8a7cvn2b7v2b95pivdf6g3qy3b")))

(define-public crate-fixed_trigonometry-0.2.5 (c (n "fixed_trigonometry") (v "0.2.5") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0bjkixqlgzqy3x7gmbxf53n7n5gxlqql15b7f8y8fslj5qlidksb")))

(define-public crate-fixed_trigonometry-0.2.6 (c (n "fixed_trigonometry") (v "0.2.6") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0ga406vgpql2l7w3q871gcplgq5gc122sdwmgkqr55x86znwk8dw")))

(define-public crate-fixed_trigonometry-0.3.0 (c (n "fixed_trigonometry") (v "0.3.0") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 2)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0dq3hw9lifvm813dad7hv83bgjlw5qyf2s7rvkvybd196pzfagbk")))

(define-public crate-fixed_trigonometry-0.3.1 (c (n "fixed_trigonometry") (v "0.3.1") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0nzlm7ssfz4yiblnjknyp55xfd7n5w8b4x5qkqhrsml8d2gs7p19")))

(define-public crate-fixed_trigonometry-0.3.2 (c (n "fixed_trigonometry") (v "0.3.2") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1lnkma29vw2kkyz9iksmj7by9a02yf6zq2zjcii48k8inan5icrk")))

(define-public crate-fixed_trigonometry-0.3.3 (c (n "fixed_trigonometry") (v "0.3.3") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "045a599yrzsqi2rwbibd51i3y2xr5ba2j54c0496hv3qzvyqgdjy")))

(define-public crate-fixed_trigonometry-0.3.4 (c (n "fixed_trigonometry") (v "0.3.4") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "092il483znn65siafmy1n76j1bg8s8lv57fnyzh1nvxqdx1hg8l1")))

(define-public crate-fixed_trigonometry-0.3.5 (c (n "fixed_trigonometry") (v "0.3.5") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0h2wmklc8cn24pj8msfxc9x18gb4fv81k27ap7ahjxq4r2jpqj5h")))

(define-public crate-fixed_trigonometry-0.3.6 (c (n "fixed_trigonometry") (v "0.3.6") (d (list (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "17mbr1rd6v89s2xbwvvys650jgq5i5vm6jcxfiw1hnvn4rb4gnbl")))

(define-public crate-fixed_trigonometry-0.4.0 (c (n "fixed_trigonometry") (v "0.4.0") (d (list (d (n "cordic") (r "^0.1") (d #t) (k 0)) (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "159mdmx0kbacl3k3hl101a730j5b8djhw9b7nxvvi4zsfzn664qj")))

(define-public crate-fixed_trigonometry-0.4.1 (c (n "fixed_trigonometry") (v "0.4.1") (d (list (d (n "cordic") (r "^0.1") (d #t) (k 0)) (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1i0qh2j3f3m53v3dasfrxk9i2ps408cq64f08xjgfwcz9k4faisg")))

(define-public crate-fixed_trigonometry-0.4.2 (c (n "fixed_trigonometry") (v "0.4.2") (d (list (d (n "cordic") (r "^0.1") (d #t) (k 0)) (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0ajbpxa9fgwfi1ahry29inay0kskyqk3bm1sbvzji8wyp690pa5x")))

(define-public crate-fixed_trigonometry-0.4.3 (c (n "fixed_trigonometry") (v "0.4.3") (d (list (d (n "cordic") (r "^0.1") (d #t) (k 0)) (d (n "fixed") (r "^1.13") (d #t) (k 0)) (d (n "mixed-num") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1n8dp9h03qk9bikw3115il89dxwdik2wgjqw36b2q13cpjgsq4s4")))

