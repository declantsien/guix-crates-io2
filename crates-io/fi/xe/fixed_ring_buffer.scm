(define-module (crates-io fi xe fixed_ring_buffer) #:use-module (crates-io))

(define-public crate-fixed_ring_buffer-0.1.0 (c (n "fixed_ring_buffer") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)))) (h "18x9w98csg9kah85pkdyd19sxhnhcajiliyfz8phmnxn6apgwgaj")))

