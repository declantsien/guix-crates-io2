(define-module (crates-io fi xe fixed_vec) #:use-module (crates-io))

(define-public crate-fixed_vec-0.1.0 (c (n "fixed_vec") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derivative") (r "^2.1.1") (d #t) (k 0)) (d (n "type_name_value") (r "^0.1.0") (d #t) (k 0)))) (h "0vg6g3i9iiw3s9222vw0k47nwkkcz8x5alqq9s8lbbfa52a5841c") (y #t)))

