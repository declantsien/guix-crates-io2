(define-module (crates-io fi xe fixed-buffer-tokio) #:use-module (crates-io))

(define-public crate-fixed-buffer-tokio-0.1.0 (c (n "fixed-buffer-tokio") (v "0.1.0") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "0xr4svi8s66jg6xnh07y1nkrzinsxn1mzhj0gb5svvd49afxf2wr")))

(define-public crate-fixed-buffer-tokio-0.1.1 (c (n "fixed-buffer-tokio") (v "0.1.1") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "0vdqk42hq2axbq1b7wbnc36qmcfin2aan45kb5a8iwq5sl3x86iv")))

(define-public crate-fixed-buffer-tokio-0.3.0 (c (n "fixed-buffer-tokio") (v "0.3.0") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "0i3198b4ha0m9wnmj14viaff3833q1ld6052xa0ymk8w69g6kvnd")))

(define-public crate-fixed-buffer-tokio-0.3.1 (c (n "fixed-buffer-tokio") (v "0.3.1") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "0ls66pllsn2yhixz0s9k4vk1giq81r6jf6mgrrq6wlhm67absjw3")))

(define-public crate-fixed-buffer-tokio-0.3.2 (c (n "fixed-buffer-tokio") (v "0.3.2") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "1nkhh0damdf0hk36sl5r90bcz4pyh9a8s6l8pnxird20q021g8zc")))

(define-public crate-fixed-buffer-tokio-0.3.3 (c (n "fixed-buffer-tokio") (v "0.3.3") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "09p5wx5nkhgl7cs4wqlfgz8hlsv9i4g3azd1jan840c0m61zk33r")))

(define-public crate-fixed-buffer-tokio-0.3.4 (c (n "fixed-buffer-tokio") (v "0.3.4") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "fixed-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "time"))) (d #t) (k 2)))) (h "0sccinw05c7adgdi964vnkx3i8sd9qji39z77z6bn4ikv7j2209x")))

