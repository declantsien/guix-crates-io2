(define-module (crates-io fi xe fixed-unsigned) #:use-module (crates-io))

(define-public crate-fixed-unsigned-0.0.0 (c (n "fixed-unsigned") (v "0.0.0") (h "08cn45pcppmx3r6f3q848ara0kar4sjy1qjnzjq3fcag4dy9mxfc")))

(define-public crate-fixed-unsigned-0.1.0 (c (n "fixed-unsigned") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12az9b6xzbj8qa5ic2kjldz2fdi3wsp3kcb2798512slrmfs09m1")))

(define-public crate-fixed-unsigned-0.2.0 (c (n "fixed-unsigned") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13gakn742kw8gd2akyqmp6jvz01v656c2p4y9n98hg7g3lz5gzjr")))

