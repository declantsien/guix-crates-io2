(define-module (crates-io fi xe fixedvec) #:use-module (crates-io))

(define-public crate-fixedvec-0.1.0 (c (n "fixedvec") (v "0.1.0") (h "10larbjzpjyl7h9vnh6m9r884vsdg6jrkg5c06xv1xlcwj90j15p")))

(define-public crate-fixedvec-0.1.1 (c (n "fixedvec") (v "0.1.1") (h "0gx96fi3vpxqrdsbrwkznar0k6rjng5nb58yl77l06wcjiflmnl8")))

(define-public crate-fixedvec-0.2.0 (c (n "fixedvec") (v "0.2.0") (h "0y1ir1lgzh1165dsmx5d0vqdcvwiyg8rnm8k17y19v8kvnw4cip5")))

(define-public crate-fixedvec-0.2.1 (c (n "fixedvec") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1z2kkjyzq9h2rxib3q6arq780zxnnmw0gplij5wfxzf7zqfgwx3n")))

(define-public crate-fixedvec-0.2.2 (c (n "fixedvec") (v "0.2.2") (h "1mav5915rhml9knbka7i1xvkj487f7lw4ghq74b1v2kqqjc4pax4")))

(define-public crate-fixedvec-0.2.3 (c (n "fixedvec") (v "0.2.3") (d (list (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "1l0rfk0ajmq8gl9wm0vf66pvpb3gwwaf6j6n9ldc5nnc2v9icv3w")))

(define-public crate-fixedvec-0.2.4 (c (n "fixedvec") (v "0.2.4") (h "1sjis135qa65kaknwrbrxhrw7ii54bfx16mms7yfxgb2vwmfz5dk") (f (quote (("unstable"))))))

