(define-module (crates-io fi xe fixed-bump) #:use-module (crates-io))

(define-public crate-fixed-bump-0.1.0 (c (n "fixed-bump") (v "0.1.0") (h "0f7g78hbnx7qc0dbz8i5srad7j2pv5rmnpmay0mhy8yj5pjz3s1q") (f (quote (("unstable" "allocator_api") ("allocator_api"))))))

(define-public crate-fixed-bump-0.1.1 (c (n "fixed-bump") (v "0.1.1") (h "0l0qzly7daip37gwqzgqshdl406hq0cw85qmix33x2bvc0b61v2m") (f (quote (("unstable" "allocator_api") ("allocator_api"))))))

(define-public crate-fixed-bump-0.1.2 (c (n "fixed-bump") (v "0.1.2") (h "0rvyx7h4q3nm4m6fgacxn1sq5n633v9vap6pin706b4m0r8pjfdx") (f (quote (("unstable" "allocator_api") ("allocator_api"))))))

(define-public crate-fixed-bump-0.1.3 (c (n "fixed-bump") (v "0.1.3") (h "075y9viv4r2pcp7ybbzz4vw1lyqf1s7izx1ygdy5bb7zc9k8zy29") (f (quote (("unstable" "allocator_api") ("allocator_api"))))))

(define-public crate-fixed-bump-0.1.4 (c (n "fixed-bump") (v "0.1.4") (h "1afbl85xg9w0q6q5yf52idvw0i0kzfsvni295ycgv7fbc2xwr18v") (f (quote (("unstable" "allocator_api") ("allocator_api"))))))

(define-public crate-fixed-bump-0.2.0 (c (n "fixed-bump") (v "0.2.0") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1ir69z3k467q6fcw08br0nb2m7h12bnydbx8vf7hc65fvm6hakb6") (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.2.1 (c (n "fixed-bump") (v "0.2.1") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)))) (h "0bmv31mwsvcgv2vsbajwb1mkns9qd7kpv7liim967xbmbfrd8b73") (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.2.2 (c (n "fixed-bump") (v "0.2.2") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)))) (h "120pk1j79k8cyrcrkx57yj1d4pf7qgga0zhlj5gf0rzc5ph6v9sq") (f (quote (("doc_cfg")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.3.0 (c (n "fixed-bump") (v "0.3.0") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)))) (h "0gnimddjqw872gbh23x71ga147w230n6s1zp7bzyknbqw0nx2pbn") (f (quote (("doc_cfg")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.3.1 (c (n "fixed-bump") (v "0.3.1") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)))) (h "1998ww9mvxlw1d5an5j0b7z6k2fzppry591dn3xbdcq4ig1b4ji1") (f (quote (("doc_cfg")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.3.2 (c (n "fixed-bump") (v "0.3.2") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)))) (h "1yw961gl729b3yvqvm6j5ni989pl8lang1rzc9mw6mc97kis3v15") (f (quote (("doc_cfg")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

(define-public crate-fixed-bump-0.3.3 (c (n "fixed-bump") (v "0.3.3") (d (list (d (n "allocator-fallback") (r "^0.1.1") (o #t) (k 0)) (d (n "allocator-fallback") (r "^0.1.7") (o #t) (k 1)))) (h "0r0m4m8vr6x6i65mw7ky8z1x34lxn9y80dfd67ah1bfgr9nw7gzr") (f (quote (("doc_cfg")))) (s 2) (e (quote (("allocator_api" "allocator-fallback?/allocator_api")))) (r "1.60")))

