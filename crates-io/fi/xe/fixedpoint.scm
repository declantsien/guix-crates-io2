(define-module (crates-io fi xe fixedpoint) #:use-module (crates-io))

(define-public crate-fixedpoint-0.1.0 (c (n "fixedpoint") (v "0.1.0") (h "1rsrd258aif4yiaqlgwb34780f5p4mfqrxq9bwic7k9xzirbplrv")))

(define-public crate-fixedpoint-0.1.1 (c (n "fixedpoint") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "04vwiy297f6fhbfcqkks3xxd5984sm04l63smhbk7mqxplas87vn")))

(define-public crate-fixedpoint-0.1.2 (c (n "fixedpoint") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0h82shyc9h8bfabvkmdnqb55gwqw6spp9q9jzkhd5mli60z2nczd")))

