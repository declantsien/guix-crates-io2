(define-module (crates-io fi xe fixerio) #:use-module (crates-io))

(define-public crate-fixerio-0.1.0 (c (n "fixerio") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "19y5k3phpvbdfpwb9fi9w6gyf1d5w937g0xjzzxnydkgyygnm0i6")))

(define-public crate-fixerio-0.1.1 (c (n "fixerio") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "131hnh9vh62xrn4pqazc3c6jkmnlkbf6ipabqd8i3c3ak3cf50wc")))

(define-public crate-fixerio-0.1.2 (c (n "fixerio") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "169n5qb3b91zxb5iakm51g5yby7lrap7vj28p0pigldmxhs8ldwh")))

(define-public crate-fixerio-0.1.3 (c (n "fixerio") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0sj5glqsq9syqp033lp8rg15x7f83qfw328chfnhlyxrmz6r69w0")))

