(define-module (crates-io fi xe fixedhash) #:use-module (crates-io))

(define-public crate-fixedhash-0.2.0 (c (n "fixedhash") (v "0.2.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (t "cfg(not(target_os = \"unknown\"))") (k 0)) (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07y8r61pi9hkwidi9767qsarxf61p1yspjnkrps334lfpqkmdxmj") (f (quote (("std" "rustc-hex" "rand") ("impl_quickcheck_arbitrary" "quickcheck") ("heapsizeof" "heapsize") ("default" "libc"))))))

