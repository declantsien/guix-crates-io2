(define-module (crates-io fi xe fixed-bitset) #:use-module (crates-io))

(define-public crate-fixed-bitset-0.0.1 (c (n "fixed-bitset") (v "0.0.1") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "17hpmg8va8bh0l9mhwc4bpx94kicygrq1f17faraklj02d4c7ns3")))

