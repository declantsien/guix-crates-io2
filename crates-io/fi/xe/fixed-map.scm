(define-module (crates-io fi xe fixed-map) #:use-module (crates-io))

(define-public crate-fixed-map-0.1.0 (c (n "fixed-map") (v "0.1.0") (d (list (d (n "fixed-map-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0y3bdvm0aqwc3vd2ykv5ijk9qgvs3dn3p17b21li3iwmhv9318l2")))

(define-public crate-fixed-map-0.1.1 (c (n "fixed-map") (v "0.1.1") (d (list (d (n "fixed-map-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0ibywy63b9gf2mik4yn3g4s742hyqqnnbjadrkflgp090jb6knda")))

(define-public crate-fixed-map-0.1.2 (c (n "fixed-map") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "10baahcbjymf3z7acd7c0x0xahqskkq88a0bsxfzcwwhigjfqrv6")))

(define-public crate-fixed-map-0.1.3 (c (n "fixed-map") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "17ilvz057m6b6idp3mayfwayw7kgdvv9jqqb88safxc07q1528py")))

(define-public crate-fixed-map-0.2.0 (c (n "fixed-map") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "1qh5ay32d5rf7114rmnwfh7z7lrwmhmjnxhw97i4il5jhml4kkf4")))

(define-public crate-fixed-map-0.3.0 (c (n "fixed-map") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "0k04dc2x828czm8byhaib3jdndc07dmf4174xb55vn2a239qhakl")))

(define-public crate-fixed-map-0.3.1 (c (n "fixed-map") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "1jq1jdiwnn77mkdg69rbbk1461n248ssrzizgrahqx7d9kyqj5cr")))

(define-public crate-fixed-map-0.3.2 (c (n "fixed-map") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "0vdc85rhzhxnjllskk0xqghig7xx8zf27mz3f3q38nf1sq2f98fs")))

(define-public crate-fixed-map-0.3.3 (c (n "fixed-map") (v "0.3.3") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "173jwqffmvrn93f8rnmjx2mv95y4y8gm57l4jv70w4hjljhxnnaf")))

(define-public crate-fixed-map-0.4.0 (c (n "fixed-map") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "1d70f54m9bvj2np30rhpifwyp41xz040bfy7hff2kbjpgq8iwmh7")))

(define-public crate-fixed-map-0.5.0 (c (n "fixed-map") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "0d02p4zgb6s8d9blvbiwz1sy4z6v3rqv050z7gdqz0m183ym0x24")))

(define-public crate-fixed-map-0.6.0 (c (n "fixed-map") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "02wbqn9hb8p1943q5jz3bzsqnmw8w8iqkajw9zd26rxk669izlhm") (y #t)))

(define-public crate-fixed-map-0.6.1 (c (n "fixed-map") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "fixed-map-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 2)))) (h "0n0n2lhq0zkc80yp6v25fkzdk7avwjaxdx3g6fkvbbnajsqhmxy3")))

(define-public crate-fixed-map-0.7.0 (c (n "fixed-map") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fixed-map-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "1nxvd77lzh1iswgl3k097x5qidc2d00xir7qbszdwd890513va1x")))

(define-public crate-fixed-map-0.7.1 (c (n "fixed-map") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fixed-map-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "1vh9dxbmap1ajn6sgbhla5liis9swvqqg857vb7h0hpfx8ina54q")))

(define-public crate-fixed-map-0.7.2 (c (n "fixed-map") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "fixed-map-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)))) (h "1vrbpymga6z30jszbl763abyf64av39jikr3arglya4g1falvx52")))

(define-public crate-fixed-map-0.8.0-alpha.1 (c (n "fixed-map") (v "0.8.0-alpha.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0yqlxdjqy3nnxjzm8r2z2fjkv53pafg6cjfk9d3cma4hns9qnwzr") (f (quote (("map" "hashbrown") ("default" "map"))))))

(define-public crate-fixed-map-0.8.0-alpha.2 (c (n "fixed-map") (v "0.8.0-alpha.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0mfd8qsd3wh5493w93fi433hrv2x8qdwghmg15qjn99ss03n4ggc") (f (quote (("map" "hashbrown") ("default" "map"))))))

(define-public crate-fixed-map-0.8.0 (c (n "fixed-map") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0f131yirfsxbn2hc8b08r30p6vc1wj4j97bjkw3jhi1psqh1jcvk") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65.0")))

(define-public crate-fixed-map-0.8.1 (c (n "fixed-map") (v "0.8.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0ddxllg9b5xlk9pv4zb2sjlkd23bk1c1dxvplsdqcb17gqrvzkrz") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65.0")))

(define-public crate-fixed-map-0.8.2 (c (n "fixed-map") (v "0.8.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0sfz9wbi3zkp961n9p2v31svqq6m5l97ilvyvhjxxsi4cr9464rp") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65")))

(define-public crate-fixed-map-0.9.0 (c (n "fixed-map") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0cfr5vnypmw7xdxzfcc7pi11nainkza8lcd800vlaj9g1xw3vxzi") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65")))

(define-public crate-fixed-map-0.9.1 (c (n "fixed-map") (v "0.9.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0j11d62rs53zb5a7dm5975zgbr041d3lryzzmqsx9p041a8zahi2") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65")))

(define-public crate-fixed-map-0.9.2 (c (n "fixed-map") (v "0.9.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "1dzfwhl1sh1wx5a1zcwz5iang3q0zg328zcmnpqxfxzx0lxyn9ra") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65")))

(define-public crate-fixed-map-0.9.3 (c (n "fixed-map") (v "0.9.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "0p3x88fg4rmrjhrprk5pgsxqhgnbj0yax87m795kskil4pkb693k") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.65")))

(define-public crate-fixed-map-0.9.4 (c (n "fixed-map") (v "0.9.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "01gr3zjxriw32j3irrg203zdfkjwmnxz6nq01gx34pxnfw8psx0y") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.72")))

(define-public crate-fixed-map-0.9.5 (c (n "fixed-map") (v "0.9.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fixed-map-derive") (r "=0.9.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (k 0)))) (h "16r21k2yvc2z44g8a2zgzx4p29140gg78w2zrklbk32fv2nikvc6") (f (quote (("default" "hashbrown" "std")))) (s 2) (e (quote (("std" "serde?/std")))) (r "1.72")))

