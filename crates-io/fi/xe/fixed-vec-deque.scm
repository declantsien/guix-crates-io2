(define-module (crates-io fi xe fixed-vec-deque) #:use-module (crates-io))

(define-public crate-fixed-vec-deque-0.1.0 (c (n "fixed-vec-deque") (v "0.1.0") (h "0dr6qyf4shyccnh4vidxnvy5ck6a2sz4b6acbq03q0vn6pi6c2d9")))

(define-public crate-fixed-vec-deque-0.1.1 (c (n "fixed-vec-deque") (v "0.1.1") (h "0hmvzp1hz27fr1iyv7izhy0gr7r96kljdx95nzcbiapg0p4dwv99")))

(define-public crate-fixed-vec-deque-0.1.2 (c (n "fixed-vec-deque") (v "0.1.2") (h "04bwivssgh0b4ch8hl4gawwvixn8a1n4k775ahlq72y4ghg4cjl9")))

(define-public crate-fixed-vec-deque-0.1.3 (c (n "fixed-vec-deque") (v "0.1.3") (h "1gywshh7w4gpybmhbj4sm41v21gsl41vagkrsqx7d414z61ris74") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.4 (c (n "fixed-vec-deque") (v "0.1.4") (h "1dzmh6l3xrb4l3ndk0hlmwazfwrgyzi4lhx5sixg4w70b08n06wp") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.5 (c (n "fixed-vec-deque") (v "0.1.5") (h "10xhrzkalyiniz076f6qdv8vyin1jk08p21pz1hrg92bx0qjxfal") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.6 (c (n "fixed-vec-deque") (v "0.1.6") (h "0jf0qddvf5pkv8hfvbih55ckb0d4ywqb6hkv5fhia7il02bvb96z") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.7 (c (n "fixed-vec-deque") (v "0.1.7") (h "03w8hi8h903hdz8489a4daslqzsscdcbmxfw0ygjjq4hmcc8pncb") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.8 (c (n "fixed-vec-deque") (v "0.1.8") (h "0hrpfb9j15j3fdm36g5hcraks441mgydmqbysr1y9yy7xd16iqq2") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.9 (c (n "fixed-vec-deque") (v "0.1.9") (h "0wfs7mccy3k2zs9whvgxp07xz87cwc5r29s1dfkqslqlb85brh0l") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.10 (c (n "fixed-vec-deque") (v "0.1.10") (h "001rfmmcpljg9yad3c3r06gn40gmpyvj7yxh5svdxix9qj2r5ym9") (f (quote (("unstable"))))))

(define-public crate-fixed-vec-deque-0.1.11 (c (n "fixed-vec-deque") (v "0.1.11") (h "19dzisjf02kapf59ij254cr8niy69vpxy1i1lw94y5cqlgf21vdi") (f (quote (("unstable")))) (r "1.56")))

