(define-module (crates-io fi xe fixed32-math) #:use-module (crates-io))

(define-public crate-fixed32-math-0.0.1 (c (n "fixed32-math") (v "0.0.1") (d (list (d (n "fixed32") (r "^0.0.2") (d #t) (k 0)))) (h "04zn01i1f8jrs5n5d31pj8lgj4yqbr3fwppdidrxaqyp88j3g73s")))

(define-public crate-fixed32-math-0.0.2 (c (n "fixed32-math") (v "0.0.2") (d (list (d (n "fixed32") (r "^0.0.3") (d #t) (k 0)))) (h "1088vi2i8wffbfh8ywn8jwndr08g6d1i83ir91w4pl2caxza2iwa")))

