(define-module (crates-io fi xe fixed-bigint) #:use-module (crates-io))

(define-public crate-fixed-bigint-0.1.0 (c (n "fixed-bigint") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "0i0d7qjx7y4x91gwsvx66j1li9616vdbgp98dwhqllayj7ii529s")))

(define-public crate-fixed-bigint-0.1.1 (c (n "fixed-bigint") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1h9233g3y9yyd60hln5qj53lkqgyk47kxy7098rmqag214rd17dr")))

(define-public crate-fixed-bigint-0.1.2 (c (n "fixed-bigint") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "0dw96gami9vvvmndfp3j2y7wja90kpkkyp0ppvf9hz8r46in8vkn")))

(define-public crate-fixed-bigint-0.1.3 (c (n "fixed-bigint") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1a4pla1zq7gvzs619nfaqsdw6l4abxh71gmaagngb4rj700yivnm")))

(define-public crate-fixed-bigint-0.1.4 (c (n "fixed-bigint") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "025l1w9pzqrllz8j1yknm8kc0rapj68djazbhlfn12rppnb3v8kj")))

(define-public crate-fixed-bigint-0.1.5 (c (n "fixed-bigint") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "12v26hwiy9w333qich9c80141qp9f6pr9kl4vrfh93wcip2a2c52")))

(define-public crate-fixed-bigint-0.1.6 (c (n "fixed-bigint") (v "0.1.6") (d (list (d (n "num-integer") (r "^0.1.44") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "088sizpygppxy2q6jacra6gipzkjm7dkxk93zg4zq6wmgvqkq9rp")))

(define-public crate-fixed-bigint-0.1.7 (c (n "fixed-bigint") (v "0.1.7") (d (list (d (n "num-integer") (r "^0.1.44") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "0ii13ic1y017z61j6zsnbg1czw3dzh7pjcrngig18yb7vqqvsn80")))

(define-public crate-fixed-bigint-0.1.8 (c (n "fixed-bigint") (v "0.1.8") (d (list (d (n "num-integer") (r "^0.1.44") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "056xjvmnbkxlassdwyxg1lpx27ix93b807cpwz5paxyvfgzqwq2i")))

(define-public crate-fixed-bigint-0.1.9 (c (n "fixed-bigint") (v "0.1.9") (d (list (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1awqbcsw93dn5rl635xgkcnag09jjn5my1kpl1mzaglzq6psgpv6")))

