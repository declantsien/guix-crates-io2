(define-module (crates-io fi xe fixed-slice-deque) #:use-module (crates-io))

(define-public crate-fixed-slice-deque-0.1.0-beta1 (c (n "fixed-slice-deque") (v "0.1.0-beta1") (d (list (d (n "slice-deque") (r "^0.3") (d #t) (k 0)))) (h "1gybi2gjwk3fkfsl0cr5jkqn4h73by45fddfkmcb08r5l1sk45ip")))

(define-public crate-fixed-slice-deque-0.1.0-alpha1 (c (n "fixed-slice-deque") (v "0.1.0-alpha1") (d (list (d (n "slice-deque") (r "^0.3") (d #t) (k 0)))) (h "1qgkg68fjx8302cwd1wh1ydrxj2s5w1h0b4n6lmyzma908q7a4fn") (y #t)))

(define-public crate-fixed-slice-deque-0.1.0-beta2 (c (n "fixed-slice-deque") (v "0.1.0-beta2") (d (list (d (n "slice-deque") (r "^0.3") (d #t) (k 0)))) (h "0anz5bp21qz1wsp9qy6xx7kz7ybzp6c7fpz94cbkli5nbp7s1qpa")))

