(define-module (crates-io fi xe fixed_math) #:use-module (crates-io))

(define-public crate-fixed_math-0.1.0 (c (n "fixed_math") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (o #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "14g5140a91d9g6ibd3pp616l6gvwck1dv9303p20kj82vjdjijrm") (f (quote (("std" "fixed/std") ("default" "std")))) (s 2) (e (quote (("num_traits" "dep:num-traits" "fixed/num-traits"))))))

(define-public crate-fixed_math-0.1.1 (c (n "fixed_math") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (o #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1wz08irmyzyn8fb3khpipm3g4sn5qppmb56ihrnsqq8lp55dg6x2") (f (quote (("std" "fixed/std") ("default" "std")))) (s 2) (e (quote (("num_traits" "dep:num-traits" "fixed/num-traits"))))))

(define-public crate-fixed_math-0.1.2 (c (n "fixed_math") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("i128"))) (o #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1qidk8mjnhyhzza36ci9pvkgv6wy8z598a4v8vjv344g7g0ng7sv") (f (quote (("std" "fixed/std") ("default" "std")))) (s 2) (e (quote (("num_traits" "dep:num-traits" "fixed/num-traits"))))))

(define-public crate-fixed_math-0.2.0 (c (n "fixed_math") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "04j0l43lx904h3bgfj348fv6gn0rgwgi71hp18j32yrs98z010wv") (f (quote (("std" "fixed/std") ("default" "std"))))))

(define-public crate-fixed_math-0.2.1 (c (n "fixed_math") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1cp5p0psr7b3zdzb2qhzsy5kwlxrxfxni5q1ixbjfjkxx7cd5na1") (f (quote (("std" "fixed/std") ("default" "std"))))))

(define-public crate-fixed_math-0.2.2 (c (n "fixed_math") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "0ns9ibmf50w9krw90prvirw2j2bv7ai7yda67qrsdgandqx316r8") (f (quote (("std" "fixed/std") ("default" "std"))))))

(define-public crate-fixed_math-0.3.0 (c (n "fixed_math") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "04bb736p22ryy3srnkyl8i5wldfc9m7pjgjrjsm4wky8glcm547w") (f (quote (("std" "fixed/std") ("right_angles") ("default" "std" "right_angles"))))))

(define-public crate-fixed_math-0.4.0 (c (n "fixed_math") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "0jm0z4fx9485pvnqlh0dfbvb8rg21m5sw5g0n0pj9kw4ijxgsddl") (f (quote (("std" "fixed/std") ("right_angles") ("default" "std" "right_angles"))))))

(define-public crate-fixed_math-0.4.1 (c (n "fixed_math") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fixed") (r "^1.23") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1kpcp6gchg0hd8h0nn8r0hdpmwxq7gf82rjm9xixnpaacg1bvis1") (f (quote (("std" "fixed/std") ("right_angles") ("default" "std" "right_angles"))))))

