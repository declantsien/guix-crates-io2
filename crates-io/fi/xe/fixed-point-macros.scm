(define-module (crates-io fi xe fixed-point-macros) #:use-module (crates-io))

(define-public crate-fixed-point-macros-1.0.0 (c (n "fixed-point-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h5nl3zvd6m0pf6j6iy1llzz4yh69awarf6s2839id8phsx6mrld")))

