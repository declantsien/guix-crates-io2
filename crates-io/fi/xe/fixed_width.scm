(define-module (crates-io fi xe fixed_width) #:use-module (crates-io))

(define-public crate-fixed_width-0.1.0 (c (n "fixed_width") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0cvnsvkg065d1vvbwqz5yh4w6nbdahw6ymc6q1z4jca5ab1xbk2s")))

(define-public crate-fixed_width-0.2.0 (c (n "fixed_width") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1sjr54mfq0q59qrx9g4xalgwgpr09kk4bblyl294ac8wh21jqs3b")))

(define-public crate-fixed_width-0.3.0 (c (n "fixed_width") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0h5mn55379359y0pvlhy85jmpwgsjy86iyvz4j948gk1z0n35nwd")))

(define-public crate-fixed_width-0.4.0 (c (n "fixed_width") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1lnvv5y1809y8jyfllp4pvlazrl3lvxkm3m2cwf7ql3avxnkrgkm")))

(define-public crate-fixed_width-0.4.1 (c (n "fixed_width") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "13qwf3bdibpcsyhcya7sg14waa7jb336536vrsn0zin3j9qqc8dw")))

(define-public crate-fixed_width-0.4.2 (c (n "fixed_width") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "07adbb0hmfbkpy6aadphmlppvjlziqs71a721q4yff9wxdrmgax1")))

(define-public crate-fixed_width-0.5.0 (c (n "fixed_width") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1x5k2f1x5hbgfvaxj2qqgg9cx5cmrq746yvgnvgkadrb2fd28n9z")))

(define-public crate-fixed_width-0.5.1 (c (n "fixed_width") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "023n0v0ksqz42fl1k2mfkv9svgn902r7xyn8p0f7a9ddphrpi7rl")))

(define-public crate-fixed_width-0.6.0 (c (n "fixed_width") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.198") (d #t) (k 2)))) (h "1w35h1f6ykcv8in86q8k823lnnhqgnxjd1zv6dnxgn24zdajzbfv")))

