(define-module (crates-io fi xe fixed_len_str) #:use-module (crates-io))

(define-public crate-fixed_len_str-0.1.0 (c (n "fixed_len_str") (v "0.1.0") (h "0j00zalrmyv30xx15llcpy2ic806kjz38jf2gz4xqcs9qf9g2rai")))

(define-public crate-fixed_len_str-0.1.2 (c (n "fixed_len_str") (v "0.1.2") (h "0p6f03whspm0xvblq11l2fyzhhlqj456y4z297nbp3g20fmpsdgp")))

(define-public crate-fixed_len_str-0.1.3 (c (n "fixed_len_str") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0ayla8c4dv5n1vh303jaxb8idbci1gk3rrbkx52lg796jaa2v029") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.4 (c (n "fixed_len_str") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0wbqr8yyi0fqdj09jfn4ahfxif9pxwq9pfxaglsn443rwv9qyld1") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.5 (c (n "fixed_len_str") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1vvfm5nz9l3gm3y37hvm06fwq2kipi0znk0fn41h1chb1qk3blsa") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.6 (c (n "fixed_len_str") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0nf81pd2cm6wc2y28p11z1p399zi3pck36bp6c1rrhrnymlp3lmc") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.7 (c (n "fixed_len_str") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1g0651bdnhdd8hxgi3kcy07mnqlra4psyrwg03fgcs3iiakaxjrc") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.8 (c (n "fixed_len_str") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1dzcg9aj0169nkw5597p2rad81gjy8jq3wviam5cq1rax0xhd63i") (f (quote (("serde_support" "serde"))))))

(define-public crate-fixed_len_str-0.1.9 (c (n "fixed_len_str") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1lygr7pbd10xz2azfxvaqxc0rqpspsivm7ipzlqq9pkg7iqk6m0c") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.0 (c (n "fixed_len_str") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1nbimpajfvbkrwqf5zn9w7ghfa2fridhfyk8n74h7d999pjbv4di") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden")))) (y #t)))

(define-public crate-fixed_len_str-0.2.1 (c (n "fixed_len_str") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1myqr68hwr6kns1b51mh9p480hfxabjn2wcnnifdw5jg6pp14h1w") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.2 (c (n "fixed_len_str") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "136lfa6vix9rcya2z3ap0qr6x61grmjrjpl3clb9qrh97jgnkzqv") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.3 (c (n "fixed_len_str") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "19biypn9xnb1gj7ajh6nwfswjkwv3fkr7c9h1z1rdprsmcsxdh6j") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.4 (c (n "fixed_len_str") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1wjiclbvaqq5qb96pd81c5vwfmy053mm9qda2x2wrzwcijnbrmin") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.5 (c (n "fixed_len_str") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1cpk7bknxb88gvq3vs5hnswahw7j62ycbl011n3hwa0dri9jzzml") (f (quote (("serde_support" "serde") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.6 (c (n "fixed_len_str") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1kp1pyll0z532xi2k9wjcbzqskx2hgr6yxkq61zw1ff4p30wb424") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.7 (c (n "fixed_len_str") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "14b8v7fi33jad38yv8p3dmnjkd1l9pja2i96kwwmancr8kqn895n") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.8 (c (n "fixed_len_str") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1n4v8pvvkwsair1kv7y76cz52zanslzl9clzv63163xfaw20p208") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.2.9 (c (n "fixed_len_str") (v "0.2.9") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "18ibngq9amkmcrnrrif3pmkph7lnfwrfhi8yk6f57wb7d6dgkmbb") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.3.0 (c (n "fixed_len_str") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0i7zypzx3gi8vslf8r5wyxyd7vda5ry4wkx223w7mqz907khrw7d") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.3.1 (c (n "fixed_len_str") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "1i75izm6alqkyqv9dglz2sl0yx98v8rj3in7jkn0783yri9jwwvk") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.3.2 (c (n "fixed_len_str") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "01mqdqhlbhyb9ah88rzik5sb9scrm21q2bpycrgw4s9c7v9qridw") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

(define-public crate-fixed_len_str-0.3.3 (c (n "fixed_len_str") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "013p7hx70dgw76yrznk8hykh1mqqkhldqifv55l6q0x8c82zkicw") (f (quote (("serde_support" "serde") ("pattern_pred_support") ("docs_hidden") ("default" "docs_hidden"))))))

