(define-module (crates-io fi xe fixed-point) #:use-module (crates-io))

(define-public crate-fixed-point-1.0.0 (c (n "fixed-point") (v "1.0.0") (d (list (d (n "fixed-point-macros") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1xmlz0wp6awai4m40kgmhdvizkcq148nrq9b1129h9xjxsgmwjbq") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixed-point-1.0.1 (c (n "fixed-point") (v "1.0.1") (d (list (d (n "fixed-point-macros") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1kwm1b5rfm70rlhrdssm3zmd7jflvji11056kjrbqirqdsvjiy1j") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixed-point-1.0.2 (c (n "fixed-point") (v "1.0.2") (d (list (d (n "fixed-point-macros") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "16jyadm3fwn86ap4shpm3xgg0gfgijrw4kcb8bf89sgpabdxb20h") (f (quote (("std") ("default" "std"))))))

