(define-module (crates-io fi xe fixed-array) #:use-module (crates-io))

(define-public crate-fixed-array-0.1.0 (c (n "fixed-array") (v "0.1.0") (h "0mq8akaan8fa2kisdgw7hljby0d9gaqxw0pqdmqdfr1y192dfdiy")))

(define-public crate-fixed-array-0.1.1 (c (n "fixed-array") (v "0.1.1") (h "0la2wcr6cn8qd2hivgw6hnwnhmhf9i30i1y5q4iggsg2csgshkva")))

(define-public crate-fixed-array-0.2.0 (c (n "fixed-array") (v "0.2.0") (h "17lc2b7c5fp6na4a29dsa2xrfx3xrk9ggbiyrw1b9k0m2zll88zf")))

(define-public crate-fixed-array-0.3.0 (c (n "fixed-array") (v "0.3.0") (h "0d616i240yrab2746v5hr0rpry61j6hw63s15w00k7lzkjhg7xyf")))

(define-public crate-fixed-array-0.4.0 (c (n "fixed-array") (v "0.4.0") (h "1an16jp8rrhd8jwzfpnqcpk75m89kgbkf0jdw61abli6413ayi87")))

(define-public crate-fixed-array-0.4.1 (c (n "fixed-array") (v "0.4.1") (h "02sldyp0hbsiy8ky5sihalpnw50igr8gps4h2iickr9rdvznh5i4")))

