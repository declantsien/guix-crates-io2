(define-module (crates-io fi xe fixed-size) #:use-module (crates-io))

(define-public crate-fixed-size-1.0.0 (c (n "fixed-size") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.38") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1l54pdwhh812ihwyxgd1cb83ia4vh0f2m90ak7mgsqfnnvkp1brw")))

