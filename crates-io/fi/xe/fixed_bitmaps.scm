(define-module (crates-io fi xe fixed_bitmaps) #:use-module (crates-io))

(define-public crate-fixed_bitmaps-0.1.0 (c (n "fixed_bitmaps") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hh59xc2l1g1vhc1wq7jrf2wiaj8hm5ykr3v3r4yd14w3sm0j1pi")))

(define-public crate-fixed_bitmaps-0.1.1 (c (n "fixed_bitmaps") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rq3siqfsi883m6krw0p841zyb2bw03azjg2kmakxq9jflj5fh6c")))

(define-public crate-fixed_bitmaps-0.2.0 (c (n "fixed_bitmaps") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k93y0z9gxg8ibpgf20g8a2l18mq0w6mpi0ccpzc59d2ayfs1cvk")))

(define-public crate-fixed_bitmaps-0.3.0 (c (n "fixed_bitmaps") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ksahlzq2hym8p9nalira6sqzdsa9315x8fcbcay0vd6k3dda9v8")))

(define-public crate-fixed_bitmaps-0.4.0 (c (n "fixed_bitmaps") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xlcdb8fz6k0ii66qp4xfdk57i8ia7fpkdq14faxqm0kk70y09bb")))

(define-public crate-fixed_bitmaps-0.5.0 (c (n "fixed_bitmaps") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05449wxqi5xm08ggykw1zz8pzy3b032wqccc1ibxfai8kaxagnnp")))

(define-public crate-fixed_bitmaps-0.5.1 (c (n "fixed_bitmaps") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0szc7q2sj7j3sdlkv68238w6k2pm9cgb4ig17mc0g8khr8f69k50")))

(define-public crate-fixed_bitmaps-0.6.0 (c (n "fixed_bitmaps") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rd5qx32nby3lis3q27xdjg0dkw1c6vvph6b3whii6sg5s5i4yj5")))

(define-public crate-fixed_bitmaps-0.7.0 (c (n "fixed_bitmaps") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i251x7am5x67baf44jjjvqkdlc220qjwswbgcl4yw9dnm80qpcn")))

(define-public crate-fixed_bitmaps-0.7.1 (c (n "fixed_bitmaps") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lbfmy618j26g95jpn63ni0s0micgj0m3l1zq020bwcpkdwc1qdv")))

(define-public crate-fixed_bitmaps-0.7.2 (c (n "fixed_bitmaps") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ipkzj8rfdsjvagplcs2dgqshr6v13ggr6x57269hgfakcy5pw3p")))

(define-public crate-fixed_bitmaps-0.8.0 (c (n "fixed_bitmaps") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bw1yg9smamq8mclb5g9n01y87sxijavbkv31c5ci2vgcrkh425y")))

(define-public crate-fixed_bitmaps-0.9.0 (c (n "fixed_bitmaps") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14a3hrp2x8jy8mwbwjr4xn2l9y2kxn5gbb4shf28dhw443397r2v")))

(define-public crate-fixed_bitmaps-0.9.1 (c (n "fixed_bitmaps") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sfmkafysi54a2vfin3sih13095g97507k79ka5r0zb7scw491n5")))

(define-public crate-fixed_bitmaps-0.9.2 (c (n "fixed_bitmaps") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13qv3gmydhvhnscgycyqc2l2jsrhpggx53vbsd9r05152vp3i00c")))

(define-public crate-fixed_bitmaps-0.9.3 (c (n "fixed_bitmaps") (v "0.9.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xpgxigrryivqix6fgqss9yqh9i9l4irxz556vl59i5lcgcdbgwf")))

(define-public crate-fixed_bitmaps-0.10.0 (c (n "fixed_bitmaps") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02wph65f8x4ph87cic0i8kyn9g69i7m1cbdvcpbdpcq5c5li1j5d")))

(define-public crate-fixed_bitmaps-0.10.1 (c (n "fixed_bitmaps") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lmkdals8xi78g8jjlv1kpl1laxx2dx6x5am94pj3jc40b4rs5cl")))

(define-public crate-fixed_bitmaps-0.10.2 (c (n "fixed_bitmaps") (v "0.10.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x99sdab66nq908r6x29g56rapihw7i24hg9pvj09cx8whkak5mb")))

