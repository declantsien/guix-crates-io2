(define-module (crates-io fi xe fixed-byterepr) #:use-module (crates-io))

(define-public crate-fixed-byterepr-1.0.0 (c (n "fixed-byterepr") (v "1.0.0") (h "1vr5zmdqk65kdpscfyjwfjfs3940asa33bmx4rg7jqjvm208mn3m") (y #t)))

(define-public crate-fixed-byterepr-1.0.1 (c (n "fixed-byterepr") (v "1.0.1") (h "1ckia7dv8j1v2jkx85zphs8z1zlmk1i0ms1iikdbwi31nmda4k95")))

