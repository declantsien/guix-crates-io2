(define-module (crates-io fi xe fixed-vectors) #:use-module (crates-io))

(define-public crate-fixed-vectors-1.0.0 (c (n "fixed-vectors") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0n9mxcn0llvp4l9539ipp6rhnw99n1vg7ax7w5056ilbf8i5y5x5")))

(define-public crate-fixed-vectors-1.1.0 (c (n "fixed-vectors") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0pi5gqxk76d7kn1l72n409x1f2sayps5bjh6q20lmhi3pqqpr02w")))

(define-public crate-fixed-vectors-1.2.0 (c (n "fixed-vectors") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0169a6xn25rpn8sx9xc6jf2x7w42nfhahvv9haxc3nwly8c4jdam")))

(define-public crate-fixed-vectors-1.2.1 (c (n "fixed-vectors") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1qz6ciqf4cp41fwddf22f4j18g55xybzgc468r0vsgy6f7wrn12l")))

(define-public crate-fixed-vectors-1.2.2 (c (n "fixed-vectors") (v "1.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0qkwf4bl77bp98a29shwwkz6s28xmg34mdd131zii7l322rgjndw")))

(define-public crate-fixed-vectors-1.2.3 (c (n "fixed-vectors") (v "1.2.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "18h7r89348j6p4dv8wwgc9rk42cd5jm1rqcml2n5mh7nzsqzqr9h")))

(define-public crate-fixed-vectors-1.2.4 (c (n "fixed-vectors") (v "1.2.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0niw2g90pspn98ldr3w47vkbbl423s7hyr0hb5w0gy2vpiab4w1r")))

(define-public crate-fixed-vectors-1.3.0 (c (n "fixed-vectors") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0gjr72amf2zm5vhzx9kl0qmva14lqpfjflli0v2bqw1az5hr4nqk")))

(define-public crate-fixed-vectors-1.4.0 (c (n "fixed-vectors") (v "1.4.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0v425kgsrfjvv0y7aalnis5li5y9sqz5d7qc5c0cwzq4kz166ax7")))

(define-public crate-fixed-vectors-1.4.1 (c (n "fixed-vectors") (v "1.4.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0nliwcbknkp6ssa2lcx73g974gqd1fxkz82mys8x1xhc0rhj6ynm")))

(define-public crate-fixed-vectors-1.4.2 (c (n "fixed-vectors") (v "1.4.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "09j4anmi7y975hklvflppadh8w9a3pl1flq7aywgbi3zzhmpq629")))

(define-public crate-fixed-vectors-1.4.3 (c (n "fixed-vectors") (v "1.4.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1xgnwrh0pf30az80a5flb5xq05s2yr71cim7vm08vyra1rrsn2gc")))

(define-public crate-fixed-vectors-1.5.0 (c (n "fixed-vectors") (v "1.5.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1bvv6i9wzr41i7zixvm15frn1dqc1a8s1l6gmavwz0f97mk0gbdd")))

(define-public crate-fixed-vectors-1.5.1 (c (n "fixed-vectors") (v "1.5.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "141lbzjj4hkabdlfkilj8lmr200icmf9cv39k7irxm0l7g0q7apr")))

(define-public crate-fixed-vectors-1.5.2 (c (n "fixed-vectors") (v "1.5.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0xs44ss3i9a0ckw3xcx0q0czsr842i7b35f0ihyipqk87yr1sx1r")))

(define-public crate-fixed-vectors-1.5.3 (c (n "fixed-vectors") (v "1.5.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1x3z7rv3ql18v33g659ygk5h9rxpn6s32m7mdnabq56wdcmbq9sw")))

(define-public crate-fixed-vectors-1.5.4 (c (n "fixed-vectors") (v "1.5.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "00zxck5zsmpxq1nkag0w4g56ghsxg60vnn3nqc5sh8wp6s4rx05x")))

(define-public crate-fixed-vectors-1.6.0 (c (n "fixed-vectors") (v "1.6.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1xayx4yghnh8a5mb2xlr77dxkqgspb4a1ii8ddf0ma8zpyfyb9k0")))

(define-public crate-fixed-vectors-1.6.1 (c (n "fixed-vectors") (v "1.6.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "02zi7b0dbbrk7kscana3p94bavnjdkq00mqd0sh2hx4z6q8wwqxq")))

(define-public crate-fixed-vectors-1.7.0 (c (n "fixed-vectors") (v "1.7.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "095a6alyrpa49wksmn83cqiy0fkblamjz47agh5z2bra2b2dgg9i")))

(define-public crate-fixed-vectors-1.8.0 (c (n "fixed-vectors") (v "1.8.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1v7v29jpr6z01z67ai7mjv03nmlzb9jnh5id9k5f2w3a7n25wxch")))

(define-public crate-fixed-vectors-1.8.2 (c (n "fixed-vectors") (v "1.8.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0236i5nyipfq68gvrqnm5dgf3y6x4qlv2s7d0b4dfv91ynqfqnpw")))

(define-public crate-fixed-vectors-1.8.3 (c (n "fixed-vectors") (v "1.8.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1xpp5iygrj7r7ysv6i9i6x73iqidr2skjz0wbmhr7kx5i077gvwm")))

(define-public crate-fixed-vectors-1.8.4 (c (n "fixed-vectors") (v "1.8.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1z0jxsarglyrnh34p7s99zb986dhzs4s6134ffyqrhynz2s2aj2w")))

(define-public crate-fixed-vectors-2.0.0 (c (n "fixed-vectors") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1k86jb822d2hnig2cjdkcvqv2dk4hd4lp83sfyhj71az54h0znk6") (f (quote (("macros"))))))

(define-public crate-fixed-vectors-2.0.1 (c (n "fixed-vectors") (v "2.0.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0212lsdffrqdzxwjjcgvwfdli8g7ir2w6ca1jc345z5nwp42kp4c") (f (quote (("macros"))))))

(define-public crate-fixed-vectors-2.0.2 (c (n "fixed-vectors") (v "2.0.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1h5f064lg3wk712n9hnajxv37zr0hyd1cv9g1ggcikcnblb02nlh")))

(define-public crate-fixed-vectors-2.0.3 (c (n "fixed-vectors") (v "2.0.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "08qcicajg720b53l3lm0xchk5ia4k1ba1wkp68sjs6492dyvykrv") (f (quote (("macros"))))))

(define-public crate-fixed-vectors-2.0.4 (c (n "fixed-vectors") (v "2.0.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "00cycn7k488xfjjvjcq7xxx702cdhqh7pvrzxpfdsv8iqh1hr4hq")))

(define-public crate-fixed-vectors-2.0.5 (c (n "fixed-vectors") (v "2.0.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "04vx0idjg0vspkxvz9zczb5b012jcmv7ha9pb10m0zcqcbx2mcis")))

(define-public crate-fixed-vectors-3.0.0 (c (n "fixed-vectors") (v "3.0.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0hsgl6kagf1glhs7jgfc55r5gbx7ab6iyc64li7lh15pfa6cll37")))

(define-public crate-fixed-vectors-3.1.0 (c (n "fixed-vectors") (v "3.1.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1ysl48jivrgkik5afx0rlppm8744ax3ssbdxm2invv8jsixlx14a")))

(define-public crate-fixed-vectors-3.2.0 (c (n "fixed-vectors") (v "3.2.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "18frrhrc6l3r88mnxm7vnh3w7rkd0sx9lbp66bxgyq7gbcmx5ffb")))

(define-public crate-fixed-vectors-3.2.1 (c (n "fixed-vectors") (v "3.2.1") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1vx9za7wrlrsn058xwbk4q3zbik71ykxfb12fzy3sm00hjy81jli")))

(define-public crate-fixed-vectors-3.2.2 (c (n "fixed-vectors") (v "3.2.2") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0ns4xbhkfk3ffpkyqq96mdixpwdic1xq6zk2gkw554vf0n196nrl")))

