(define-module (crates-io fi xe fixed-typed-arena) #:use-module (crates-io))

(define-public crate-fixed-typed-arena-0.1.0 (c (n "fixed-typed-arena") (v "0.1.0") (d (list (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "0hwp87mh3dj4zm2i5a96gw9zj9g2w04460f8xjn2klmjjqqdkg9q") (f (quote (("unstable" "dropck_eyepatch") ("dropck_eyepatch")))) (y #t)))

(define-public crate-fixed-typed-arena-0.1.1 (c (n "fixed-typed-arena") (v "0.1.1") (d (list (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "142yqxp4pfkhzd1qxdx4nb3hzjvbnrnzi1n4jm83nxwgbr9nfsq6") (f (quote (("unstable" "dropck_eyepatch") ("dropck_eyepatch")))) (y #t)))

(define-public crate-fixed-typed-arena-0.1.2 (c (n "fixed-typed-arena") (v "0.1.2") (d (list (d (n "typenum") (r "^1.12") (d #t) (k 0)))) (h "1lrvxscwil0r4r9m7lxqihwyd5ybsfah76swrv281ys7rlp9my65") (f (quote (("unstable" "dropck_eyepatch") ("dropck_eyepatch"))))))

(define-public crate-fixed-typed-arena-0.2.0 (c (n "fixed-typed-arena") (v "0.2.0") (h "0hv6x1lmyd35vzn68gwnxh8qfaf8izaw56hd6l8d6sy500rwaf9l") (f (quote (("dropck_eyepatch"))))))

(define-public crate-fixed-typed-arena-0.2.1 (c (n "fixed-typed-arena") (v "0.2.1") (h "0rlj6sbzjrgjkfn43iflnb8nzsn5dhxsifdfg1mah90ihixdsdhy") (f (quote (("dropck_eyepatch"))))))

(define-public crate-fixed-typed-arena-0.2.2 (c (n "fixed-typed-arena") (v "0.2.2") (h "07h3cizbmc0sim7732xz7w2xij99p22ghj9df6cyv8r8s8j7v1fr") (f (quote (("dropck_eyepatch"))))))

(define-public crate-fixed-typed-arena-0.2.3 (c (n "fixed-typed-arena") (v "0.2.3") (h "028rnb2206jzlwcj1hz2i50y9hscgfn0lg889daqvqllj0ig1py4") (f (quote (("dropck_eyepatch")))) (r "1.52")))

(define-public crate-fixed-typed-arena-0.3.0 (c (n "fixed-typed-arena") (v "0.3.0") (h "1qb2hdgq141m0c0kvhmrnmrv3shpv2vqqhgwwnayhxbv6m1vm854") (f (quote (("dropck_eyepatch")))) (y #t) (r "1.59")))

(define-public crate-fixed-typed-arena-0.3.1 (c (n "fixed-typed-arena") (v "0.3.1") (h "1mhjsafsbzqcdl54kkqbqx46hbdslpbjd31awri9r9bwdw8zfp8f") (f (quote (("dropck_eyepatch")))) (r "1.59")))

(define-public crate-fixed-typed-arena-0.3.2 (c (n "fixed-typed-arena") (v "0.3.2") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wv1kxfs97cc29xqq9s8jkf3w18a7hkrh98grd3miznkh24g3vgd") (f (quote (("dropck_eyepatch" "add-syntax")))) (r "1.59")))

(define-public crate-fixed-typed-arena-0.3.3 (c (n "fixed-typed-arena") (v "0.3.3") (d (list (d (n "add-syntax") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "integral_constant") (r "^0.1") (d #t) (k 0)))) (h "1lmywd2489lbb2salgrrq6wynig4nviqa5yiqd3j39g7dfz673c3") (f (quote (("dropck_eyepatch" "add-syntax")))) (r "1.59")))

