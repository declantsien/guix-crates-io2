(define-module (crates-io fi xe fixedbitset) #:use-module (crates-io))

(define-public crate-fixedbitset-0.0.1 (c (n "fixedbitset") (v "0.0.1") (h "06271j11qw496cw2hq7hvwlw75vxdw43mfc50lsaqszkc6lixkn2")))

(define-public crate-fixedbitset-0.0.2 (c (n "fixedbitset") (v "0.0.2") (h "1wrg2181kqqqqvjk8jsvvs7y2a5znhaznzzwklv7bj04x4604wk1")))

(define-public crate-fixedbitset-0.0.3 (c (n "fixedbitset") (v "0.0.3") (h "0w2m8cjlxv83hh0nrj9hbcmcg86j3702d93x7krcacfjiki88h99")))

(define-public crate-fixedbitset-0.0.4 (c (n "fixedbitset") (v "0.0.4") (h "13abf6ymv0hbzpi872mld3r5fd0xzzxrp4kwsn1pq9jjz53xs1qd")))

(define-public crate-fixedbitset-0.0.5 (c (n "fixedbitset") (v "0.0.5") (h "1xs663jb7aia6sakpxk92y93z4g84y1r9q6ilpzlrng4wg10lfcv")))

(define-public crate-fixedbitset-0.1.0 (c (n "fixedbitset") (v "0.1.0") (h "1d2qrvnx1z7gcky2ymf0ad8lisl1ainl23djj54dn2ky5kkjb7wh")))

(define-public crate-fixedbitset-0.1.1 (c (n "fixedbitset") (v "0.1.1") (h "0vycj3v5myvzbdwcpyjyz97ycd73mdfy9z7hnqnwvpr2bhi85665")))

(define-public crate-fixedbitset-0.1.2 (c (n "fixedbitset") (v "0.1.2") (h "0j2k1l0lh17z3cvfhy3i3rg5mwrrv7rwk55d34c2cqkx5dvwvq5p")))

(define-public crate-fixedbitset-0.1.3 (c (n "fixedbitset") (v "0.1.3") (h "07b5r6i24zp2iskjrmk55m9g6dyyf9mzza0gxdsgrd2mdy8wk2hs")))

(define-public crate-fixedbitset-0.1.4 (c (n "fixedbitset") (v "0.1.4") (h "1lmzb93b73xijpc4y9qfnhpnr5dvmgvx427nmf6ccgvph00vpqb3")))

(define-public crate-fixedbitset-0.1.5 (c (n "fixedbitset") (v "0.1.5") (h "0kdnld70jzhah4fkaw8bvc3w1i09m2p9isvla4zv63f0qhzw7hw8")))

(define-public crate-fixedbitset-0.1.6 (c (n "fixedbitset") (v "0.1.6") (h "14w7bh64wpzwf6kpljak4p4y1382m2mvsbqwv1g5q48i5lp43x7w")))

(define-public crate-fixedbitset-0.1.7 (c (n "fixedbitset") (v "0.1.7") (h "1wl6m2azp1id052pz5ik1fka5zvag7qgqpff7pnwb83gf9skvjxh")))

(define-public crate-fixedbitset-0.1.8 (c (n "fixedbitset") (v "0.1.8") (h "0y347akfzvayvli5y9vk31b1sd1kr77sfg68qxnxhs3l8gn8zjw5")))

(define-public crate-fixedbitset-0.1.9 (c (n "fixedbitset") (v "0.1.9") (h "0czam11mi80dbyhf4rd4lz0ihcf7vkfchrdcrn45wbs0h40dxm46")))

(define-public crate-fixedbitset-0.2.0 (c (n "fixedbitset") (v "0.2.0") (h "0kg03p777wc0dajd9pvlcnsyrwa8dhqwf0sd9r4dw0p82rs39arp") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.3.0 (c (n "fixedbitset") (v "0.3.0") (h "06wi2papxqxrpbd40v4wfcvjj4w3jcayl935iyb82dndynngri1g") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.3.1 (c (n "fixedbitset") (v "0.3.1") (h "05fhdbb5fnia7gq9baadlvk4naifs8xrc1h7zs8y1mvmfnych22f") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.3.2 (c (n "fixedbitset") (v "0.3.2") (h "17a7pcslq6m3n76rgp0df6jv8pi63mbn9m1gs4aw9aypgrb81rs5") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.4.0 (c (n "fixedbitset") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17mnwa48dy11dnbasxa0c92sdj243acjl2ilhpcb1fa0pvxa93ir") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.4.1 (c (n "fixedbitset") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gh5qp6h5ivgbn80klx10lgp1jg0qmvmp58p0cr4qg0bw8lb17r7") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.4.2 (c (n "fixedbitset") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "101v41amgv5n9h4hcghvrbfk5vrncx1jwm35rn5szv4rk55i7rqc") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.5.0 (c (n "fixedbitset") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cbic4nzx53kakns5xahqidsr0skfysb03l7k1wxy5pzslqf87mm") (f (quote (("std") ("default" "std"))))))

(define-public crate-fixedbitset-0.5.1 (c (n "fixedbitset") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jiklkj9dv0cw269bp2njd18rvv9nshvdbgby17w9qff8c9snkqh") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-fixedbitset-0.5.2 (c (n "fixedbitset") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z4briy6d1x6aj715zih9drsrdhjyw1f5bxmqfx1a4d8p9dzzc38") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-fixedbitset-0.5.3 (c (n "fixedbitset") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18azz0ppgz7f5bhs672pcs757865ljcyww0g9bgrjzyx6k4x1qdm") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-fixedbitset-0.5.4 (c (n "fixedbitset") (v "0.5.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03gfl69s6k7p3gyd13cgkmb248r0jkiqq60cd81ncr5fcybkv7s3") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-fixedbitset-0.5.5 (c (n "fixedbitset") (v "0.5.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r4874ars7hhw9ra3fs58rrd9xdmqcarlv201c33dslmcr7lhdli") (f (quote (("std") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-fixedbitset-0.5.6 (c (n "fixedbitset") (v "0.5.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b1rvz1ags28s9m06cb783xvl86hmlg8ilm23np2nz1vqzhkr9x1") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-fixedbitset-0.5.7 (c (n "fixedbitset") (v "0.5.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16fd3v9d2cms2vddf9xhlm56sz4j0zgrk3d2h6v1l7hx760lwrqx") (f (quote (("std") ("default" "std")))) (r "1.56")))

