(define-module (crates-io fi xe fixed-exp) #:use-module (crates-io))

(define-public crate-fixed-exp-0.1.0 (c (n "fixed-exp") (v "0.1.0") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1m9h36wivgkd9sqyxr60p4qmyhvv54flmr9z5l78lwq9hz3svx6j")))

