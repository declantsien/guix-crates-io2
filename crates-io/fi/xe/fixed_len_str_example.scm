(define-module (crates-io fi xe fixed_len_str_example) #:use-module (crates-io))

(define-public crate-fixed_len_str_example-0.1.0 (c (n "fixed_len_str_example") (v "0.1.0") (d (list (d (n "fixed_len_str") (r "^0.1.5") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "119sdnab6j1cp0pbin2x7r7zsriim5scyq0fhaxfqlic6w2vxcjx") (y #t)))

(define-public crate-fixed_len_str_example-0.1.1 (c (n "fixed_len_str_example") (v "0.1.1") (d (list (d (n "fixed_len_str") (r "^0.1.5") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0pzfcwmp09bj5473na7x5zxh46hf0wnpxdbb9gl8vzpj3kgchaav")))

(define-public crate-fixed_len_str_example-0.1.2 (c (n "fixed_len_str_example") (v "0.1.2") (d (list (d (n "fixed_len_str") (r "^0.1.6") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0pwz8cb6p6zcdcpwzryqhh82n1snfi5zrq9zl7jg4caazcnkrp9m")))

(define-public crate-fixed_len_str_example-0.1.3 (c (n "fixed_len_str_example") (v "0.1.3") (d (list (d (n "fixed_len_str") (r "^0.1.7") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "136lb1dpm2yy3rhf8hc8b3d8rmka6jrixs254f221rjrg00xqqii")))

(define-public crate-fixed_len_str_example-0.1.4 (c (n "fixed_len_str_example") (v "0.1.4") (d (list (d (n "fixed_len_str") (r "^0.1.8") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0r4pccgxrfqx2p4mpgm8cfbzgdil5kpii0dz0aisa74zv1kpnffy") (y #t)))

(define-public crate-fixed_len_str_example-0.1.5 (c (n "fixed_len_str_example") (v "0.1.5") (d (list (d (n "fixed_len_str") (r "^0.1.8") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "15qzjzi2cmhyvw6x2ms74b8j5g6qp3828k794kdw1kbi4chqvb0x")))

(define-public crate-fixed_len_str_example-0.1.6 (c (n "fixed_len_str_example") (v "0.1.6") (d (list (d (n "fixed_len_str") (r "^0.2.0") (f (quote ("serde_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0xksnyj217cr409jpm3z59lrm2f7f0pm3miglbp28249x9cxn0hr") (y #t)))

(define-public crate-fixed_len_str_example-0.1.7 (c (n "fixed_len_str_example") (v "0.1.7") (d (list (d (n "fixed_len_str") (r "^0.2.1") (f (quote ("serde_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1qcanfav5hs5xrsm4sidk2lp6p36h7kg576machmghykksvd4myp")))

(define-public crate-fixed_len_str_example-0.1.8 (c (n "fixed_len_str_example") (v "0.1.8") (d (list (d (n "fixed_len_str") (r "^0.2.4") (f (quote ("serde_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0gdnnrac0qmgshjaik04xqd6y78ijr1w7imnimss78z8mvkx46vh")))

(define-public crate-fixed_len_str_example-0.1.9 (c (n "fixed_len_str_example") (v "0.1.9") (d (list (d (n "fixed_len_str") (r "^0.2.5") (f (quote ("serde_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1bkf9zy37mbnb0dzmapax4hgbpq5ishkf78p08g7z5ml7k5l63cy")))

(define-public crate-fixed_len_str_example-0.2.0 (c (n "fixed_len_str_example") (v "0.2.0") (d (list (d (n "fixed_len_str") (r "^0.2.7") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0csw3mddvfihxjqs88ln8pnqqp2a7lr47kmqjxvv4hmkjz99x4qq")))

(define-public crate-fixed_len_str_example-0.2.1 (c (n "fixed_len_str_example") (v "0.2.1") (d (list (d (n "fixed_len_str") (r "^0.2.7") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1wp9n4a4di2l6phw6i4bgl4fa0vgbmfxnccvkqpmnr2vjniqjy7h")))

(define-public crate-fixed_len_str_example-0.2.2 (c (n "fixed_len_str_example") (v "0.2.2") (d (list (d (n "fixed_len_str") (r "^0.2.8") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1i1nhd4sr866bxys4mn28pvwlnmm7ikvh7wkwmjvsz72avar8wcj")))

(define-public crate-fixed_len_str_example-0.2.3 (c (n "fixed_len_str_example") (v "0.2.3") (d (list (d (n "fixed_len_str") (r "^0.2.9") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "17gpb8msnd1qbvwcka5jsvygdhfiady72j9vcj0qj8kccxiwbqqk")))

(define-public crate-fixed_len_str_example-0.2.4 (c (n "fixed_len_str_example") (v "0.2.4") (d (list (d (n "fixed_len_str") (r "^0.3.0") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0dvn200r7330ar26hs5wvh44ljqkl0rpz4cjwh50fkm6ks7favqm")))

(define-public crate-fixed_len_str_example-0.2.5 (c (n "fixed_len_str_example") (v "0.2.5") (d (list (d (n "fixed_len_str") (r "^0.3.1") (f (quote ("serde_support" "pattern_pred_support"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0rzfi0k5f1kybcjvy42hyfsvv2595ncqfbc6clnmb0l4xjl20cp0")))

