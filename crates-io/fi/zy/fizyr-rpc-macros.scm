(define-module (crates-io fi zy fizyr-rpc-macros) #:use-module (crates-io))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha1 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "06jisicki7xzv13xhfnv2ilbw6jqfz0z5a2zbf1y35rhvjzdxhgf")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha2 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0pq07ki76hk1dxlmzbpgj9clmhnmyy76pz8dc2xpgbs3bqj9cfas")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha3 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha3") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1bds7x79iy34s9rhca0x7bn587fah438gqhcsp6q2gmsf9wgs9rp")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha4 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1k4h0sm9ppasbrgvva26s53fmx6frd6jg0z9jdr2il0jj778rqfv")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha5 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha5") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0f0k865zpphqqlbg6aam8sihrc9xmp3v6r1w9dgpv78fcghx23sy")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha6 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha6") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1dgw7x816qzrr24pg9h0lrnkhr7baiyadhhdd5dvv7h5c8p41y3f")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha7 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha7") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1vah290f07rvbq455fx6j8adb95vb1yilg4c4vyi3knkbz15sfvb")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha8 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha8") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1kpgd4zc5yn9kn1gnlhqiyll7cqrwykxjfw9cpcm3p4579kn02wn")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha9 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha9") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0rpxnak2lnra2f3kgmy9f73lz36r8v3z8ycs1d6sgm52hyxdd8ki")))

(define-public crate-fizyr-rpc-macros-0.5.0-alpha10 (c (n "fizyr-rpc-macros") (v "0.5.0-alpha10") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0kip8whjiy9gky43200nz7mpfb24rdjjvajjdxah6sidyq6hiyaj")))

(define-public crate-fizyr-rpc-macros-0.5.0 (c (n "fizyr-rpc-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0d3a7s9i0lmpbqnkw9wvkyn4nimlgfqbc52makixgsz1y990aqlw")))

(define-public crate-fizyr-rpc-macros-0.5.1 (c (n "fizyr-rpc-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1hrd7ynk10irgri2461w4wjmd8pkzcwrgk5c8c3sjsiyd91n4yn3")))

(define-public crate-fizyr-rpc-macros-0.6.0 (c (n "fizyr-rpc-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1xrp1hyyp8lj2cmsnzc6dbakkn8ss7g90ac7x5lqzww0iw35fwya")))

(define-public crate-fizyr-rpc-macros-0.7.0 (c (n "fizyr-rpc-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11jvpyzaz0yzdcign791s4ivk5bag4098s9jxgj8hdcdmcp6whpf")))

(define-public crate-fizyr-rpc-macros-0.7.1 (c (n "fizyr-rpc-macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0qk1c606c7w7m2lmbmjzdnbjrlfs79a9fcxa7lzdmvrpf561xzhn")))

(define-public crate-fizyr-rpc-macros-0.8.0 (c (n "fizyr-rpc-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1543i505irkskp1k98hbq2xv121mza0m9n467qwb7ljm9yvs62k8")))

