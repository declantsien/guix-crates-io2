(define-module (crates-io zo te zotero) #:use-module (crates-io))

(define-public crate-zotero-0.0.1-alpha (c (n "zotero") (v "0.0.1-alpha") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0c4xz74gqf543hxl0z9z81wgsy6wss26g53sa09pps2v9j455b43")))

(define-public crate-zotero-0.0.2-alpha (c (n "zotero") (v "0.0.2-alpha") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "08161d1karvlbrx0zs9r3l20dgznddlcy1f58vdqmmdv22xzr64i")))

(define-public crate-zotero-0.0.3-alpha (c (n "zotero") (v "0.0.3-alpha") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1ydn10wi6mjq46hipwl97b2dqax76y4aq0i1f87x7ksv7s4xglf0")))

(define-public crate-zotero-0.0.1 (c (n "zotero") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0qj8y3xdh0zwv3482b8afkiv86qq8mk9248a99k51a0sk60ndil8")))

(define-public crate-zotero-0.0.2 (c (n "zotero") (v "0.0.2") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "18jwakgig6qrxi6yzbfpaz9ykj0dzcx8hmy5y995f9wdyny1dzvz")))

