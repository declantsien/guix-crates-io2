(define-module (crates-io zo om zoom-sdk-windows-sys) #:use-module (crates-io))

(define-public crate-zoom-sdk-windows-sys-0.1.0 (c (n "zoom-sdk-windows-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lij7pc3w7iaampvxwcvrvcq2q5d5028r6cmqb9f8dpgms21c4jg")))

(define-public crate-zoom-sdk-windows-sys-0.2.0 (c (n "zoom-sdk-windows-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clang") (r "^1.0.3") (f (quote ("clang_10_0" "runtime"))) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "0xh00dbwpm1s47vkmyalzqp59b2xh4azwx57y6lhr26vzp9dkr1z")))

