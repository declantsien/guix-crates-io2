(define-module (crates-io zo om zoom-transcript-edit) #:use-module (crates-io))

(define-public crate-zoom-transcript-edit-0.1.0 (c (n "zoom-transcript-edit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "contracts") (r "^0.6.1") (d #t) (k 0)))) (h "1n97zalfna6gwi2fjdwm7kkwz6k42c9qcdlxc01vyfv1s1zkz75f")))

