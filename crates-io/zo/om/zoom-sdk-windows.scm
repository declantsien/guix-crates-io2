(define-module (crates-io zo om zoom-sdk-windows) #:use-module (crates-io))

(define-public crate-zoom-sdk-windows-0.2.0 (c (n "zoom-sdk-windows") (v "0.2.0") (d (list (d (n "lazycell") (r "^1.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "zoom-sdk-windows-sys") (r "^0.2.0") (d #t) (k 0)))) (h "11m3j7i3gsb9nk07qninhiam1qgpcam3dj1qya7ph3l2d5nnb8mp")))

