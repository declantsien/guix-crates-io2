(define-module (crates-io zo om zoomer) #:use-module (crates-io))

(define-public crate-zoomer-0.1.0 (c (n "zoomer") (v "0.1.0") (h "07vvjsniik4r59r3bgr1p0f74xqcz2y1a27qy7qvzpcdyllzkvb5")))

(define-public crate-zoomer-0.1.1 (c (n "zoomer") (v "0.1.1") (h "1nhfvcy73d534klgvs6r49p3y623zr44rk5qla1w6rxfgy7mvf3f")))

(define-public crate-zoomer-0.1.2 (c (n "zoomer") (v "0.1.2") (h "16hg508c5r960grvcxng8r7vky0ihxs8zlrlg2cah65ajjk07y57")))

