(define-module (crates-io zo om zoom) #:use-module (crates-io))

(define-public crate-zoom-1.0.0 (c (n "zoom") (v "1.0.0") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0mi1dj3vp09wdq453crd2c2z9m939v96mfyccpg8d5vwdpwzb1g7")))

(define-public crate-zoom-1.0.1 (c (n "zoom") (v "1.0.1") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0xxbvifkwh5d6ksqbzqhgmqdqfg8sgkqdfx7w00l2j1wmgx2z4sq")))

(define-public crate-zoom-1.0.2 (c (n "zoom") (v "1.0.2") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0sld8i1r00k94i07l5k776w3409lbal0h15x84zqi6ddsqrq31m1")))

(define-public crate-zoom-1.0.3 (c (n "zoom") (v "1.0.3") (d (list (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "1wbicc8g0xgh58kkyxs44bhhggd9in7f3jcdz2rnf4ix5l31szcl")))

(define-public crate-zoom-1.1.0 (c (n "zoom") (v "1.1.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "19jvjxc13i1k0qasmqxhcx2ryh4k08i5lzydn3z91h8b5ll57m2b")))

(define-public crate-zoom-1.2.0 (c (n "zoom") (v "1.2.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0nx2n38wlnvvwb07qp0yadlwpgk2sdnlgpz22kxfbnr5cmr2c279")))

(define-public crate-zoom-1.2.1 (c (n "zoom") (v "1.2.1") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "1awfgd8lh5ihvz3jcgnagkka6b37ygng0hc2yvr2rkrcfd8cnc82")))

(define-public crate-zoom-1.3.0 (c (n "zoom") (v "1.3.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0a698z0gp6n2fncrj10hp63fz36zmp1pm9gmccrbc0508xmg1kpm") (y #t)))

(define-public crate-zoom-1.4.0 (c (n "zoom") (v "1.4.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "12nlf9hjg7zhh5mplc6g9q308rnqp05jgz6p8xpfvkdrj0dhxkv2")))

(define-public crate-zoom-1.5.0 (c (n "zoom") (v "1.5.0") (d (list (d (n "nalgebra") (r "^0.10.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)))) (h "0lf8n8qf0b2m0w2idk5i0pqc87hh5zxp3gpn3xdr4137b3m29b70")))

