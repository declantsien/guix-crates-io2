(define-module (crates-io zo om zoomtiler) #:use-module (crates-io))

(define-public crate-zoomtiler-1.0.0 (c (n "zoomtiler") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imagesize") (r "^0.9.0") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.2") (d #t) (k 0)))) (h "04br7932hv3zzcn5638nr64zwn8nz6fix068xc8vz5gqskg9vdgn")))

