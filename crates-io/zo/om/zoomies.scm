(define-module (crates-io zo om zoomies) #:use-module (crates-io))

(define-public crate-zoomies-0.1.0 (c (n "zoomies") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.43") (d #t) (k 0)))) (h "0hjwliv0rwb9p39jham5m7ffgd5955b7i53ak9dc3b8nr67xqx5w")))

