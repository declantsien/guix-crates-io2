(define-module (crates-io zo d- zod-derive) #:use-module (crates-io))

(define-public crate-zod-derive-0.1.0 (c (n "zod-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "025hpxdgx1zinwx46n8z29icy00j8jq06wlwi0mab7lgxmp1lrqr") (f (quote (("rpc") ("default" "rpc"))))))

