(define-module (crates-io zo rq zorq-acl) #:use-module (crates-io))

(define-public crate-zorq-acl-0.1.0 (c (n "zorq-acl") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-env-log") (r "^0.2") (d #t) (k 2)))) (h "1fwc6xv84j4jg53zmb776ri086lp0p103cxqw1vqh0lhcr0vr1m2")))

