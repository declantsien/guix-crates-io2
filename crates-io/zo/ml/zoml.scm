(define-module (crates-io zo ml zoml) #:use-module (crates-io))

(define-public crate-zoml-0.1.0 (c (n "zoml") (v "0.1.0") (h "19vnvcrig5h4y8wqxbf62m4saxv53m0bihpj0dwaj6m02m21avg8") (y #t)))

(define-public crate-zoml-0.1.1 (c (n "zoml") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.71") (d #t) (k 0)))) (h "0mkfhdgq9f36vb09mh2jb2kd3x9zdvlikz96x4ndi3cbrd0crw0y") (y #t)))

(define-public crate-zoml-0.1.2 (c (n "zoml") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.71") (d #t) (k 0)))) (h "12ldskpja8k2bgfzd1aydc2inm8v70jw9khbgmidkxlhlq0i5prh") (y #t)))

