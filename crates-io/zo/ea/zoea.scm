(define-module (crates-io zo ea zoea) #:use-module (crates-io))

(define-public crate-zoea-0.0.1 (c (n "zoea") (v "0.0.1") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)))) (h "0rphp4i014wy5cbf9rjlg4jnjmld4pqr8f27cxh652cgjjg5xq6r")))

(define-public crate-zoea-0.0.2 (c (n "zoea") (v "0.0.2") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)))) (h "0szn7m5d83i4rqshrafqcjx6y7ffij0h1gl7bq9w2qxjrp630lsc")))

(define-public crate-zoea-0.0.3 (c (n "zoea") (v "0.0.3") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)))) (h "09lzysjfvamrxgmwy8q7gici1irllp48sb24672p62kj7dhj5qis")))

(define-public crate-zoea-0.0.4 (c (n "zoea") (v "0.0.4") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "0mxagwfj9yji6539kjg4crz294dcjbsl0ka9jpln3gf6d8iwsaqn")))

(define-public crate-zoea-0.0.5 (c (n "zoea") (v "0.0.5") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "1d3ycp83xg2ix7svl3xi2psi61v39nrfffvpnwjpzdqmv0fpa6h3")))

(define-public crate-zoea-0.0.6 (c (n "zoea") (v "0.0.6") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "0hyfx0ngrx3v2kjv66xnxqj5884qvlpswi1nwppjmgbzs7m9cqs2")))

(define-public crate-zoea-0.0.7 (c (n "zoea") (v "0.0.7") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "1icbp41sp4bq3nx456k7q6gf0na5f2fq2drsw3mk4m1bvr1wc19f")))

(define-public crate-zoea-0.0.8 (c (n "zoea") (v "0.0.8") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "porter-stemmer") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "1pwzdxkp8q3w8q7brdpszjp4h58ak866gfz0rzslksbck6k8dwpc")))

(define-public crate-zoea-0.0.9 (c (n "zoea") (v "0.0.9") (d (list (d (n "curl") (r "^0.4.29") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "porter-stemmer") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)))) (h "0qzd8sywni9f4ikfnnfnpjgf67nbfn4kwisj8ri3q0k7pcz9ajxc")))

(define-public crate-zoea-0.1.0 (c (n "zoea") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "porter-stemmer") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)))) (h "04nhdhrwzcvji58daydg61qnlm2q21cklqkc7bkcr7mhq0k8x4dm")))

