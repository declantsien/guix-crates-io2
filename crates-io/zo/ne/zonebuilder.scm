(define-module (crates-io zo ne zonebuilder) #:use-module (crates-io))

(define-public crate-zonebuilder-0.1.0 (c (n "zonebuilder") (v "0.1.0") (d (list (d (n "geo") (r "^0.17.1") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "geographiclib-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "geojson") (r "^0.22.2") (f (quote ("geo-types"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (o #t) (d #t) (k 0)))) (h "1gcwxj5r87rr5ia3vdbshhfpcb1l037k1crg5xhx467afar37xrg") (f (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-zonebuilder-0.3.0 (c (n "zonebuilder") (v "0.3.0") (d (list (d (n "geo") (r "^0.18.0") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "geographiclib-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "geojson") (r "^0.22.2") (f (quote ("geo-types"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (o #t) (d #t) (k 0)))) (h "19wqqk1ihrgvd6qi8z99gsfhxylcwb3xscabb8ljlja6y3w1flgi") (f (quote (("wasm" "wasm-bindgen"))))))

