(define-module (crates-io zo ne zone-detect) #:use-module (crates-io))

(define-public crate-zone-detect-0.1.0 (c (n "zone-detect") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kyvh0qs8cqck9z50psymdk8svx0r4aqvakq918vrxqv7phwa7qi")))

(define-public crate-zone-detect-0.2.0 (c (n "zone-detect") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hbilpykqbbm6n7h21gp6fxk5gz1vhv4cgkf49c7vag9f2naksa4")))

(define-public crate-zone-detect-1.0.0 (c (n "zone-detect") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (k 2)) (d (n "structopt") (r "^0.3.23") (k 2)) (d (n "thiserror") (r "^1.0.29") (k 0)))) (h "0mhs6d8gx8kijwm9n2720cgadmc550iybbawig7cn9735hmslx0f")))

(define-public crate-zone-detect-1.0.1-alpha.0 (c (n "zone-detect") (v "1.0.1-alpha.0") (d (list (d (n "clap") (r "^2.33.3") (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (k 2)) (d (n "structopt") (r "^0.3.26") (k 2)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "0cjf57m1rcfdczfzpcw2ilghhyjh5xrzvg5p07kvgnng705r4asm") (y #t)))

(define-public crate-zone-detect-1.0.1 (c (n "zone-detect") (v "1.0.1") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive" "help" "std" "usage"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (k 2)) (d (n "thiserror") (r "^1.0.30") (k 0)))) (h "0hz5lf4hj8c7xbr218is8x553m10gdx8axgfv0f9pw2rr3dlvn07")))

