(define-module (crates-io zo ne zone) #:use-module (crates-io))

(define-public crate-zone-0.1.0 (c (n "zone") (v "0.1.0") (h "09c3yasldm47qx44s7vd5hakzxykag8hayzjf6z81swhpsj22c18")))

(define-public crate-zone-0.1.1 (c (n "zone") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1lpai7c6fxpxdvzdxjq2fncsg99mxbzf0gzddldp5bikc19y8f63")))

(define-public crate-zone-0.1.2 (c (n "zone") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0a4gb4fpqxv22n6da3pcj2wfdfx9xjj7lshjl6lsy3s20r00zgak")))

(define-public crate-zone-0.1.3 (c (n "zone") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1yp7drfnag94nw5n6xakinq68sy118hnc0bmvyixp8d7g4zxzngj")))

(define-public crate-zone-0.1.4 (c (n "zone") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0asvvcji65la2ygsb4410kriipqzkzlwmh5wirx9xxif71rv5hd9")))

(define-public crate-zone-0.1.5 (c (n "zone") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1q9kgzsrcd8c1y8m4zq98nxix9lxhrqjzi47000zwd1c1xm3l6j7")))

(define-public crate-zone-0.1.6 (c (n "zone") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1zr56h83j5bzhl8kgh8c06hfa3wnbnqbi2nz1kschqc4xqgkr9yp")))

(define-public crate-zone-0.1.7 (c (n "zone") (v "0.1.7") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "14s6rng2slm7whc8l46xmhy3x94s2skbd4ba6qk99b423m0hiws1")))

(define-public crate-zone-0.1.8 (c (n "zone") (v "0.1.8") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.1.2") (d #t) (k 0)))) (h "01r7f8pxpkm2a10sc6r13p25c7069nplkqq2kfkbm7fdcg4vp5im")))

(define-public crate-zone-0.2.0 (c (n "zone") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("process"))) (o #t) (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.2.0") (d #t) (k 0)))) (h "167ijiry7wmcc06savdv5s497zdldhaa0m3df92i5a6pzd15lm5h") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-zone-0.3.0 (c (n "zone") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("process"))) (o #t) (d #t) (k 0)) (d (n "zone_cfg_derive") (r "^0.3.0") (d #t) (k 0)))) (h "08pgf9paaxqna4q15g4diavb9mxx4656spdhib7288pag6544am6") (f (quote (("sync") ("default" "sync")))) (s 2) (e (quote (("async" "dep:tokio"))))))

