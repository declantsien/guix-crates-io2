(define-module (crates-io zo ne zoneinfo_parse) #:use-module (crates-io))

(define-public crate-zoneinfo_parse-0.1.2 (c (n "zoneinfo_parse") (v "0.1.2") (d (list (d (n "datetime") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "10gmdkdx4chj32gy4jz3r5wipv49sff77npxs418r05jjl4c6rjk")))

(define-public crate-zoneinfo_parse-0.1.3 (c (n "zoneinfo_parse") (v "0.1.3") (d (list (d (n "datetime") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "0q8mnc0xpnfidf2lb6cn20ajviac3f0i2x7dyjhqk25y92xxa5ph")))

(define-public crate-zoneinfo_parse-0.1.4 (c (n "zoneinfo_parse") (v "0.1.4") (d (list (d (n "datetime") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "1pk5zr5rr06szdb07fb969wsraysdp0as2awzv5iifzaj0cfpv6m")))

