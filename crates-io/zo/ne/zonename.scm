(define-module (crates-io zo ne zonename) #:use-module (crates-io))

(define-public crate-zonename-0.1.0 (c (n "zonename") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1blgrhx1h7b4v5hlpqf8r9dijvfkqm73q45s19qvwxn2a8ipm6pa")))

(define-public crate-zonename-0.1.1 (c (n "zonename") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "077vgrkbvpy4kwxd9p13p4r9wsmkziagnk8njjjqb3jlsvs95bpv")))

