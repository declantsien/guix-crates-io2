(define-module (crates-io zo ne zone_cfg_derive) #:use-module (crates-io))

(define-public crate-zone_cfg_derive-0.1.1 (c (n "zone_cfg_derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "07c88msp1wf5b43yd4fmr8i4jmabq7nmy8q0kx538h2jnhxnymad")))

(define-public crate-zone_cfg_derive-0.1.2 (c (n "zone_cfg_derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "09wms5vahzyxgnqsqng514028xabbqcs99zglw9ndn13h24jmb31")))

(define-public crate-zone_cfg_derive-0.2.0 (c (n "zone_cfg_derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "157gj4zlxvy0an3vyfrc86599wf2iv7mydwyvcd3n387kl04n8pg")))

(define-public crate-zone_cfg_derive-0.3.0 (c (n "zone_cfg_derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1j3cca1qcqlvkk4md1r5zsmnjsy4x49rh751mkaj5ql56wfz1i6m")))

