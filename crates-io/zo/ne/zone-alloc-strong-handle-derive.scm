(define-module (crates-io zo ne zone-alloc-strong-handle-derive) #:use-module (crates-io))

(define-public crate-zone-alloc-strong-handle-derive-0.1.0 (c (n "zone-alloc-strong-handle-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "zone-alloc") (r "^0.1") (d #t) (k 2)))) (h "01vsvqy2690d4dislwzi25z6v49pjm7gq8d6yl02mygzvkgj1as1")))

(define-public crate-zone-alloc-strong-handle-derive-0.1.1 (c (n "zone-alloc-strong-handle-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "zone-alloc") (r "^0.1") (d #t) (k 2)))) (h "1sici58629x3shrng51c631wrpds6l3spy2sv9rrqpsxk053cpbj")))

(define-public crate-zone-alloc-strong-handle-derive-0.1.2 (c (n "zone-alloc-strong-handle-derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "zone-alloc") (r "^0.1") (d #t) (k 2)))) (h "1hmwvkblp97zj0qzglcrlmvzmlgy7qjb5kypb11fm4q5yqnjmw5l")))

