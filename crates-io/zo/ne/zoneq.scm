(define-module (crates-io zo ne zoneq) #:use-module (crates-io))

(define-public crate-zoneq-0.1.0 (c (n "zoneq") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)))) (h "0dg64vqpnd4cgdqlnys9xrhf0fzx7la7j9g1qj90wl23ky9s1ysm")))

(define-public crate-zoneq-0.1.1 (c (n "zoneq") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta3") (d #t) (k 0)))) (h "11jmm3h646061850dlmyn6kx45y2cz84m92jdl5qshxcivg39ijh")))

