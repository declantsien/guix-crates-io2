(define-module (crates-io zo ne zoneinfo_compiled) #:use-module (crates-io))

(define-public crate-zoneinfo_compiled-0.4.5 (c (n "zoneinfo_compiled") (v "0.4.5") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "datetime") (r "^0.4.5") (d #t) (k 0)))) (h "1ypaxr01s9604nkdmxgcykxddnkl97yv7ggbpxcj6bwhix09xn4p")))

(define-public crate-zoneinfo_compiled-0.4.7 (c (n "zoneinfo_compiled") (v "0.4.7") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "datetime") (r "^0.4.7") (d #t) (k 0)))) (h "0q05hd49nhjj16hz3g3sc6z0jyg9rmrp654n8kz0qni2nfdlvkkc")))

(define-public crate-zoneinfo_compiled-0.4.8 (c (n "zoneinfo_compiled") (v "0.4.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "datetime") (r "^0.4.7") (d #t) (k 0)))) (h "0bnm19w791q6kp79s0zl1cj9w51bw5xrifrxfy3g1p05i676y4vf")))

(define-public crate-zoneinfo_compiled-0.5.0 (c (n "zoneinfo_compiled") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "datetime") (r "^0.5") (d #t) (k 0)))) (h "0j3nd9jyn0xdxyb72cd5fcb6y0a1b8j5iygbwd4zm2r8gkwywcvh")))

(define-public crate-zoneinfo_compiled-0.5.1 (c (n "zoneinfo_compiled") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "datetime") (r "^0.5.2") (k 0)))) (h "1pm50w4vv34r08mrajfvyhc1254gv8zv4q6p7gs315c9bvkfpyv4")))

