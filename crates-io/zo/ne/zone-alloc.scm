(define-module (crates-io zo ne zone-alloc) #:use-module (crates-io))

(define-public crate-zone-alloc-0.1.0 (c (n "zone-alloc") (v "0.1.0") (h "0sba98kkzc97p96p2lkcpkm9h451mrc3fspi3gv7ai5w8s7rcm9z") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.1.1 (c (n "zone-alloc") (v "0.1.1") (h "1rj4zv3d26apc7dh1ii8xb969ir3cfky4mc2xq67mvm1hxb88cc9") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.1.2 (c (n "zone-alloc") (v "0.1.2") (h "01r7b1f36m71gjqr7z2bzjkc08yj9xp0ngiqm7mc5ihb4gi43422") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.2.0 (c (n "zone-alloc") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.6") (o #t) (d #t) (k 0)))) (h "0g2agp5z4p07wwdl8qv3y0h9xdcffb1hsb7wbdnmrwknw139shmd") (f (quote (("std") ("default" "std" "ahash")))) (s 2) (e (quote (("ahash" "dep:ahash" "std"))))))

(define-public crate-zone-alloc-0.2.1 (c (n "zone-alloc") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.6") (o #t) (d #t) (k 0)))) (h "0vbwyg5cv7crsvggcigwcij4adnm6rqkpkj8hbsa1cjrqfvq2pls") (f (quote (("std") ("default" "std" "ahash")))) (s 2) (e (quote (("ahash" "dep:ahash" "std"))))))

(define-public crate-zone-alloc-0.2.2 (c (n "zone-alloc") (v "0.2.2") (d (list (d (n "ahash") (r "^0.8.6") (o #t) (d #t) (k 0)))) (h "16h4297jbqcdmnx83xb7p8zb8pdlznwaf4gcabifry2zp64dvmj8") (f (quote (("std") ("default" "std" "ahash")))) (s 2) (e (quote (("ahash" "dep:ahash" "std"))))))

(define-public crate-zone-alloc-0.3.0 (c (n "zone-alloc") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "1vg1vfhs01ssgwyzmjlk5v0ilx9fm97wsp2jfgk6wwsc60lz12cz") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.3.1 (c (n "zone-alloc") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "0m680lpc0i2r4hfyak2sbsiw79rn848mc5chmv3w79n74gcq05mr") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.3.2 (c (n "zone-alloc") (v "0.3.2") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "1im6qkhk37gc4fg79558zl7qrbjjaqvq38k7765jjfpsb6nhcdqi") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.3.3 (c (n "zone-alloc") (v "0.3.3") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "0sv7gcsi7km260zsv6k9r4mighgj4bg7vr266h0zvqxy1rjwqivd") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.3.4 (c (n "zone-alloc") (v "0.3.4") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "13agq84d3ccmr71723zgkfr7900s8af29nvi8pdi883xb04ag616") (f (quote (("std") ("default" "std"))))))

(define-public crate-zone-alloc-0.3.5 (c (n "zone-alloc") (v "0.3.5") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "1gvllpw16ngzbr94cqsc519n9m1c3ljkg7qx8v49ds8r7nlwnlh0") (f (quote (("std") ("may-dangle") ("default" "std"))))))

(define-public crate-zone-alloc-0.4.0 (c (n "zone-alloc") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "0ygz5nfj52jgkry4nn07bfwr7a0dz92kmsk8qim2fnpsz5wxqs02") (f (quote (("std") ("may-dangle") ("default" "std"))))))

