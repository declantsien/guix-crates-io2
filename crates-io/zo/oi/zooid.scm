(define-module (crates-io zo oi zooid) #:use-module (crates-io))

(define-public crate-zooid-0.0.0 (c (n "zooid") (v "0.0.0") (h "064j78vshg1sc1ar9k1bg2k4g0zj3rnsszvc373daqni8vy3ssb4")))

(define-public crate-zooid-0.0.1 (c (n "zooid") (v "0.0.1") (d (list (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zc333yqbmf50c7crlq83l93mhf4wpp7pgwz17ayf0yin7mbnp65")))

