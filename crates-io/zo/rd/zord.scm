(define-module (crates-io zo rd zord) #:use-module (crates-io))

(define-public crate-zord-0.1.3 (c (n "zord") (v "0.1.3") (h "09wy2ysf70f5k1267dx5ps1y79bj6gxr9xdpl0lkfwvah0gyy9va") (y #t)))

(define-public crate-zord-0.1.4 (c (n "zord") (v "0.1.4") (h "1y9z881y0nvkvqx12h8qsc5831kjv88ylznn2m3ccvg5s89qlyz2") (y #t)))

(define-public crate-zord-0.1.77 (c (n "zord") (v "0.1.77") (h "0p9z4nyx9l55nh16i6z0mgkn52mjr2810ab0gbwqb73xw5rss3i8") (y #t)))

(define-public crate-zord-0.77.0 (c (n "zord") (v "0.77.0") (h "1qh69lzjbahvdgqp9bmwxmahlzzp7z3h4b4hik00p2w8hr4ikm6q") (y #t)))

(define-public crate-zord-7.7.18 (c (n "zord") (v "7.7.18") (h "0q7g0b7zyy8naiizimg27xzz1h92vm8ia2amkc0v7d6f0iafr2qi") (y #t)))

(define-public crate-zord-2018.7.7 (c (n "zord") (v "2018.7.7") (h "1qph8snifb36wly6ck4jfj0z0slrcj23r1lxv8458lqn5016vk6s") (y #t)))

(define-public crate-zord-2019.12.13 (c (n "zord") (v "2019.12.13") (h "15m3410f0wy55a3glzlix9llzslvg1qvivzf1r8zm51rva1z4a7w") (y #t)))

(define-public crate-zord-9999.999.99 (c (n "zord") (v "9999.999.99") (h "00jx4m8d4jn385vi3p5n801jg4j1shv9dp5s5ag6sjmpc2l1cjyz") (y #t)))

(define-public crate-zord-9.9.9 (c (n "zord") (v "9.9.9") (h "03nf0h30g7m9g6wl5aszvz6sdmp981lwqylwq1n0lwpbiyg5ib24") (y #t)))

(define-public crate-zord-99999.99999.99999 (c (n "zord") (v "99999.99999.99999") (h "0fjhx0d6sz0qmywdgwah12hwmpmicx2b9jiph50r1xs7n985w12g") (y #t)))

(define-public crate-zord-9999999.9999999.9999999 (c (n "zord") (v "9999999.9999999.9999999") (h "1i2wh91dwaq1yk5sfcizdy8h9xwsd8wnz2msb35i7wv7i3a60hda") (y #t)))

(define-public crate-zord-999999999.999999999.999999999 (c (n "zord") (v "999999999.999999999.999999999") (h "18paq73pq7v08w4gf80hh8rg6q4rajyjh2ccahc947m4458v33cc")))

