(define-module (crates-io zo rd zorder) #:use-module (crates-io))

(define-public crate-zorder-0.1.0 (c (n "zorder") (v "0.1.0") (h "0wddqya70m8flhsb514m0zxjf4i9h6fia9358ini1z1rzsmcyrgk")))

(define-public crate-zorder-0.2.0 (c (n "zorder") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qkzvh2gkdssczgffbbfmjw36c40wcm5dyqwv117vmicnnd603ch") (f (quote (("std") ("default" "std"))))))

(define-public crate-zorder-0.2.1 (c (n "zorder") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07la2xpmfkx9gfi1ds64prkhbyn3v8kgiv59vzw2rnwavw0v849g") (f (quote (("std") ("default" "std"))))))

(define-public crate-zorder-0.2.2 (c (n "zorder") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "00gv4v0nqq048m93z6fw1bvajxp2k2nmkm2icf0q7pnrhx577a0y") (f (quote (("std") ("default" "std"))))))

