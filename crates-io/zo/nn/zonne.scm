(define-module (crates-io zo nn zonne) #:use-module (crates-io))

(define-public crate-zonne-0.1.0 (c (n "zonne") (v "0.1.0") (h "1sbdwj9rm5x1v78mx023in0sfizy2j49ggmypn8lmjfhjd74zl1x")))

(define-public crate-zonne-0.2.0 (c (n "zonne") (v "0.2.0") (h "1ia2wdhy1qm51802wnm0g4yxqglmp59gwczd9idc0ckjgz3v5k7a")))

(define-public crate-zonne-0.2.1 (c (n "zonne") (v "0.2.1") (h "124frqd54gc977krri9ndpx6lb5zz00j3s60985ilr8889pfr8nd")))

(define-public crate-zonne-0.2.2 (c (n "zonne") (v "0.2.2") (h "18h9zb67hnqnbjcaj78is1w4kz9g8pnvsz42vxx0apdmq8wic77n")))

(define-public crate-zonne-0.2.3 (c (n "zonne") (v "0.2.3") (h "1w3f14nfavqpqqq4f11s3506yr9lx1izsxp4b3qgx0061pgjl2cn")))

