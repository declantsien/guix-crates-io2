(define-module (crates-io zo ho zoho-crm) #:use-module (crates-io))

(define-public crate-zoho-crm-0.1.0 (c (n "zoho-crm") (v "0.1.0") (d (list (d (n "mockito") (r "^0.21.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ac32k2z1yik5wdi0y65pp47cfq86hjm55imvkg9ifsqns7f2bqj")))

(define-public crate-zoho-crm-0.2.0 (c (n "zoho-crm") (v "0.2.0") (d (list (d (n "mockito") (r "^0.21.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "0c6bh413lv7vnrq6j80zacjcnlachxyrswk2afkx5bn6yk8w2f3f")))

(define-public crate-zoho-crm-0.3.0 (c (n "zoho-crm") (v "0.3.0") (d (list (d (n "mockito") (r "^0.21.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "06gv120lwb8sd8r3ma2f2294vzm55n0kz1h2gvdghg8njl8xfpdy")))

(define-public crate-zoho-crm-0.3.1 (c (n "zoho-crm") (v "0.3.1") (d (list (d (n "mockito") (r "^0.21.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "1p71dj52xq4pnln566rs12r0wx4242qjg1jm9gbv2j60r1s33pnl")))

(define-public crate-zoho-crm-0.3.2 (c (n "zoho-crm") (v "0.3.2") (d (list (d (n "mockito") (r "^0.21.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "1q8vla1jb6piil7qkb0anpsszf4f91wap12dd73hgab3xlvia2g2")))

