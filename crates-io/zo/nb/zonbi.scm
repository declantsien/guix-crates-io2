(define-module (crates-io zo nb zonbi) #:use-module (crates-io))

(define-public crate-zonbi-0.1.0 (c (n "zonbi") (v "0.1.0") (d (list (d (n "zonbi_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1gs89cqibabbs7j5wb0scdjxxdzvki231c6775k1k355nr431bzn")))

(define-public crate-zonbi-0.2.0 (c (n "zonbi") (v "0.2.0") (d (list (d (n "zonbi_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0fc9jynvwxqkkwaw80yszssibhm0vnck2bs8jsd2w4cf5hkv5gi0")))

(define-public crate-zonbi-0.3.0 (c (n "zonbi") (v "0.3.0") (d (list (d (n "zonbi_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0r31n15q357yv2h0mhgysb5lw5x6f7qkqgnfskmb8xa7fksz79qi")))

(define-public crate-zonbi-0.3.1 (c (n "zonbi") (v "0.3.1") (d (list (d (n "zonbi_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0z8yycv3hfnr0bxs42pk7k43q386c839lbn5qlp55b9b4vmz68c6")))

(define-public crate-zonbi-0.3.2 (c (n "zonbi") (v "0.3.2") (d (list (d (n "zonbi_macros") (r "^0.3.0") (d #t) (k 0)))) (h "12q4v9gxcmc6kvag6g39vj02wyi0z8c4wyggxj8xqakhwb0yb0nw")))

