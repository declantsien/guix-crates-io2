(define-module (crates-io zo nb zonbi_macros) #:use-module (crates-io))

(define-public crate-zonbi_macros-0.1.0 (c (n "zonbi_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "15ba3vmsqa86yv2plx6hp7ndxjk5677vdjn7r4gqlw03jn0hils6")))

(define-public crate-zonbi_macros-0.2.0 (c (n "zonbi_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0d4yrg27aw6nv5saklgwvdz4dxikfg6a2q9xcbn5i7vkz5w8ilsk")))

(define-public crate-zonbi_macros-0.3.0 (c (n "zonbi_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1yrcmnrr54sf5jx5m0ww43hjf4bhbi02i0gmwjw9lsji1vmh7fs4")))

