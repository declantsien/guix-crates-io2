(define-module (crates-io zo pf zopfli-rs) #:use-module (crates-io))

(define-public crate-zopfli-rs-0.1.0 (c (n "zopfli-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.7") (d #t) (k 1)) (d (n "flate2") (r "^1.0.1") (d #t) (k 2)))) (h "18c0abn0y1gz7k9v1acrq6i3n090l2jywjymw8mknr9ys7l7axsw")))

(define-public crate-zopfli-rs-0.1.1 (c (n "zopfli-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.7") (d #t) (k 1)) (d (n "flate2") (r "^1.0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "zopfli") (r "^0.3.7") (d #t) (k 2)))) (h "0yp3ayjfaa61c63cv485vmm99ibbc4wbkzsdh37w8n1whgqyw0g2")))

