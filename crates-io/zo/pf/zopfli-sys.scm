(define-module (crates-io zo pf zopfli-sys) #:use-module (crates-io))

(define-public crate-zopfli-sys-0.1.0 (c (n "zopfli-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1yhflr2khk9zk5xmgr3cgsgqwwj4z076fdkxfdfslj38yn1n6s6x") (l "zopfli")))

