(define-module (crates-io zo pf zopfli) #:use-module (crates-io))

(define-public crate-zopfli-0.1.0 (c (n "zopfli") (v "0.1.0") (h "15kr7zkpcxs6kskvj93mkjrv1pil75vkyr0p1031g6sm0c1rh6nf")))

(define-public crate-zopfli-0.2.0 (c (n "zopfli") (v "0.2.0") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wybmi044y6qalnhwbk07mmm3pqf829ifayw7j81wvd89l3bsmsp")))

(define-public crate-zopfli-0.3.0 (c (n "zopfli") (v "0.3.0") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "16ap2j3g1gsjjz7p3gw44w0ryxszmg0vx9x8lkf43xibg627zbw1")))

(define-public crate-zopfli-0.3.1 (c (n "zopfli") (v "0.3.1") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "1s0rvmh90jlnznf9f1pc6rhfknvk918pb06rzxjr1phxf32v0z9n")))

(define-public crate-zopfli-0.3.2 (c (n "zopfli") (v "0.3.2") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "1gcnpjylm80hl9pi6xnk3n0sz9yxwhs1m3p59v5qnzmp39a8p7dl")))

(define-public crate-zopfli-0.3.3 (c (n "zopfli") (v "0.3.3") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "1c7yg17fjbw9ivdwf9f2pw89y9khwl1zazpz4a0vp9x8p1xdqdcp")))

(define-public crate-zopfli-0.3.4 (c (n "zopfli") (v "0.3.4") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "0yxi6r5sxgy0d5nfw294g0v99lc511y2j7lqvcjjk2x0lw8x54zz")))

(define-public crate-zopfli-0.3.5 (c (n "zopfli") (v "0.3.5") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)))) (h "066zczdxpfyffy70wbz3vvc3barssnjwgbcflhymb447p5phrkq8")))

(define-public crate-zopfli-0.3.6 (c (n "zopfli") (v "0.3.6") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "0hq0ppzryhwg1v44cna8fafrrijkyky2liq68wakl1pbbi6yx4pa")))

(define-public crate-zopfli-0.3.7 (c (n "zopfli") (v "0.3.7") (d (list (d (n "adler32") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 0)))) (h "0kp8fckf0s7pz2qss9mwv6i88jgc5gmskqmb680431waqxqgpllz")))

(define-public crate-zopfli-0.4.0 (c (n "zopfli") (v "0.4.0") (d (list (d (n "adler32") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "typed-arena") (r "^1.4.1") (d #t) (k 0)))) (h "0r1r0xkccwrm6dz40rs212sacsrrx06gnxq13cmdwsj2cjabfya0")))

(define-public crate-zopfli-0.5.0 (c (n "zopfli") (v "0.5.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "1ssbrll2mrsv9j6hjlp621fn0i2z4qf8x6vb1d3swmy62bgg850y")))

(define-public crate-zopfli-0.6.0 (c (n "zopfli") (v "0.6.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "iter-read") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "0ly40cw71pkkyxdi9nak0g6a89hzz2nsm9572mzvpjw77bfpjk0n")))

(define-public crate-zopfli-0.7.0 (c (n "zopfli") (v "0.7.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "iter-read") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "0bpq0idjhg3c7cdj5iha27pa6frp1ykzvzwiy3vjr5dpkblcmwxl") (r "1.56")))

(define-public crate-zopfli-0.7.1 (c (n "zopfli") (v "0.7.1") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "iter-read") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1dpajpbyr8grs9kn74d8fl3grhjd71mx60wgd9l60s1361nd3q7i") (r "1.56")))

(define-public crate-zopfli-0.7.2 (c (n "zopfli") (v "0.7.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simd-adler32") (r "^0.3.4") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "1n5ap8g1dgkr0xbbr3m76chvm3m4qgl8wcx2563z42izkpabxcm5") (r "1.56")))

(define-public crate-zopfli-0.7.3 (c (n "zopfli") (v "0.7.3") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.5") (o #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (k 0)))) (h "1cpx4x0gpdwn26m506qff9cxs3c0saxrg4imm8lgg5lsclbyngyj") (f (quote (("default" "gzip" "zlib")))) (y #t) (s 2) (e (quote (("zlib" "std" "dep:simd-adler32") ("std" "crc32fast?/std" "simd-adler32?/std") ("nightly" "crc32fast?/nightly" "simd-adler32?/nightly") ("gzip" "std" "dep:crc32fast")))) (r "1.60")))

(define-public crate-zopfli-0.7.4 (c (n "zopfli") (v "0.7.4") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.5") (o #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (k 0)))) (h "1mc4418pj1zv5z44xvi57lr0vyqs5xirn2gbk3bjc4q5dap501jf") (f (quote (("default" "gzip" "zlib")))) (s 2) (e (quote (("zlib" "std" "dep:simd-adler32") ("std" "crc32fast?/std" "simd-adler32?/std") ("nightly" "crc32fast?/nightly" "simd-adler32?/nightly") ("gzip" "std" "dep:crc32fast")))) (r "1.60")))

(define-public crate-zopfli-0.8.0 (c (n "zopfli") (v "0.8.0") (d (list (d (n "crc32fast") (r "^1.3.2") (o #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.7") (o #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (k 0)))) (h "0dh7xykalfkas1ba05321wk15g008ib2j5p0mqpkyflaa3rlh7sw") (f (quote (("default" "std" "gzip" "zlib")))) (s 2) (e (quote (("zlib" "dep:simd-adler32") ("std" "crc32fast?/std" "simd-adler32?/std") ("nightly" "crc32fast?/nightly" "simd-adler32?/nightly") ("gzip" "dep:crc32fast")))) (r "1.66")))

(define-public crate-zopfli-0.8.1 (c (n "zopfli") (v "0.8.1") (d (list (d (n "bumpalo") (r "^3.16.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (o #t) (k 0)) (d (n "lockfree-object-pool") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.7") (o #t) (k 0)))) (h "0ip9azz9ldk19m0m1hdppz3n5zcz0cywbg1vx59g4p5c3cwry0g5") (f (quote (("default" "std" "gzip" "zlib")))) (s 2) (e (quote (("zlib" "dep:simd-adler32") ("std" "dep:log" "dep:lockfree-object-pool" "dep:once_cell" "crc32fast?/std" "simd-adler32?/std") ("nightly" "crc32fast?/nightly") ("gzip" "dep:crc32fast")))) (r "1.73.0")))

