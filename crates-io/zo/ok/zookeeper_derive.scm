(define-module (crates-io zo ok zookeeper_derive) #:use-module (crates-io))

(define-public crate-zookeeper_derive-0.3.0 (c (n "zookeeper_derive") (v "0.3.0") (d (list (d (n "quote") (r "~0.3.15") (d #t) (k 0)) (d (n "syn") (r "~0.11.11") (d #t) (k 0)))) (h "0h1s58jpzfcy3zalhn8grz5v89nfv7qwc64f7qg438l5bsdx7sps")))

(define-public crate-zookeeper_derive-0.4.0 (c (n "zookeeper_derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1nmrjpisvyxmm4h4h7h0zaly67b721sgzqx1plzr5y242px37c6y")))

(define-public crate-zookeeper_derive-0.4.1 (c (n "zookeeper_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08wpaihs7l2ci9hv3v6h242lgw33m0nmfisn5q4f9cn8wf8p4c22")))

