(define-module (crates-io zo et zoet) #:use-module (crates-io))

(define-public crate-zoet-0.1.0 (c (n "zoet") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (f (quote ("macros"))) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (k 0)))) (h "19sdc66d9apfhxay73ql25pnxsmz9aqg9ljlq4kq7nzvx6kni20a") (f (quote (("default") ("clippy-insane"))))))

(define-public crate-zoet-0.1.1 (c (n "zoet") (v "0.1.1") (d (list (d (n "phf") (r "^0.7") (f (quote ("macros"))) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (k 0)))) (h "19n9cx8lbxm8dndz33kbi2hfmqcpazap6mbag0mx3p5aclqigbvd") (f (quote (("default") ("clippy-insane"))))))

(define-public crate-zoet-0.1.2 (c (n "zoet") (v "0.1.2") (d (list (d (n "phf") (r "^0.7") (f (quote ("macros"))) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (k 0)))) (h "05bcv0hxyykszj793id34pbwraswmr7b2x2qhfb8nlh16my0gxzz") (f (quote (("default") ("clippy-insane"))))))

(define-public crate-zoet-0.1.3 (c (n "zoet") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.2") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("clone-impls" "extra-traits" "fold" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0x7p671dib24288a1l1yg8x6rqx8jwl3kadxikvnf6wimifi87j1") (f (quote (("default") ("clippy-insane"))))))

(define-public crate-zoet-0.1.4 (c (n "zoet") (v "0.1.4") (d (list (d (n "zoet-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0lp89wv6vbgycyihw84mi0az1x82qkachd94myg7f9vjiqlaqqgw") (f (quote (("default") ("clippy-insane" "zoet-macro/clippy-insane"))))))

(define-public crate-zoet-0.1.5 (c (n "zoet") (v "0.1.5") (d (list (d (n "zoet-macro") (r "^0.1.5") (k 0)))) (h "1rb5cch3zygq15zrc1q7pk1fd3bj460n872bagcc5g8i3s0zaqvb") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc"))))))

(define-public crate-zoet-0.1.6 (c (n "zoet") (v "0.1.6") (d (list (d (n "zoet-macro") (r "^0.1.6") (k 0)))) (h "1673k7s1z4ipyhw8vd21x799y00bx9j6lznqqjdxab8y2c5wh6dw") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc"))))))

(define-public crate-zoet-0.1.7 (c (n "zoet") (v "0.1.7") (d (list (d (n "zoet-macro") (r "^0.1.7") (k 0)))) (h "10z8xygqpwcdk4zss0n52pn39ri5g9xnx7p0p6qspsahacdhq97n") (f (quote (("nightly" "zoet-macro/nightly") ("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

(define-public crate-zoet-0.1.8 (c (n "zoet") (v "0.1.8") (d (list (d (n "zoet-macro") (r "^0.1.8") (k 0)))) (h "0asryh4dm48ibl5hrkx3hfx9f16vasn53dh1gi3brhqr1r3dvkrn") (f (quote (("nightly" "zoet-macro/nightly") ("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

(define-public crate-zoet-0.1.9 (c (n "zoet") (v "0.1.9") (d (list (d (n "zoet-macro") (r "^0.1.8") (k 0)))) (h "09wgypdv9az1gfqf447znjxh167y710pyqdargg54jybfba0hky5") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

(define-public crate-zoet-0.1.10 (c (n "zoet") (v "0.1.10") (d (list (d (n "zoet-macro") (r "^0.1.10") (k 0)))) (h "19rvvpxknrcwr39i8zvq7fqbi65zbkq48zlf5x4rcaa1hlvn5v6y") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

(define-public crate-zoet-0.1.11 (c (n "zoet") (v "0.1.11") (d (list (d (n "zoet-macro") (r "^0.1.11") (k 0)))) (h "0z2darmy4ajdazcg6smnsqsjpqkcn11ap3h00p9qdvqmqi5nf7wx") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

(define-public crate-zoet-0.1.12 (c (n "zoet") (v "0.1.12") (d (list (d (n "zoet-macro") (r "^0.1.11") (k 0)))) (h "07ghf8xgixfg4h3lrhvn4vp14sz3cn0pxayggysh29xl4nkfya4a") (f (quote (("default" "alloc") ("clippy-insane" "zoet-macro/clippy-insane") ("alloc" "zoet-macro/alloc"))))))

