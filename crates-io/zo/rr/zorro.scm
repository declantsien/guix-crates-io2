(define-module (crates-io zo rr zorro) #:use-module (crates-io))

(define-public crate-zorro-0.0.1 (c (n "zorro") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)))) (h "1ijxcb765pya366jr9jw6ai6nfa10zmjc4x8zjsq29pqfgc5dbl2")))

