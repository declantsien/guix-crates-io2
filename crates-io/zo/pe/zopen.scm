(define-module (crates-io zo pe zopen) #:use-module (crates-io))

(define-public crate-zopen-0.3.0 (c (n "zopen") (v "0.3.0") (d (list (d (n "bzip2") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0r6mc5ps3g0klkrxbk9g5mbvi3jq51717j2ag078pqf5vghrh1n0") (f (quote (("defaults"))))))

(define-public crate-zopen-0.3.1 (c (n "zopen") (v "0.3.1") (d (list (d (n "bzip2") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0cqkbs2224nfbl3qi451jrn2cq078a34fyf5pig1jg6sdgjmyhf0") (f (quote (("defaults"))))))

(define-public crate-zopen-0.3.2 (c (n "zopen") (v "0.3.2") (d (list (d (n "bzip2") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "19v2y3msfiizl1fwdfdiybl7jf8vz5mw3pzcz3rscny6y0b24xis") (f (quote (("defaults"))))))

(define-public crate-zopen-0.3.3 (c (n "zopen") (v "0.3.3") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1srj693j0imyvv1dm1nlc3jwqcm0nciirjah8dalk2cgwaxs3m7v") (f (quote (("defaults"))))))

(define-public crate-zopen-0.3.4 (c (n "zopen") (v "0.3.4") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1kl7mr54hq7yamzspnfd37cmi84cby79g4qkr52pgr357dl83dh1") (f (quote (("defaults"))))))

(define-public crate-zopen-0.3.5 (c (n "zopen") (v "0.3.5") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "060l7nmfzzg4fw5w74xwrpaxbhxh84w5wnhq56b9xl4mhxk1cxpk") (f (quote (("defaults"))))))

(define-public crate-zopen-0.4.0 (c (n "zopen") (v "0.4.0") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "17dd21xv1i14xzkwx5id2iqha90a9yhbnql7gv2yr2xjmzq6vkfy") (f (quote (("defaults"))))))

(define-public crate-zopen-0.5.0 (c (n "zopen") (v "0.5.0") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1f4zwd1lz22ndzb0f0p7bp2zhawzd2gjwnsi3ykckp1c4s7hwjvv") (f (quote (("defaults"))))))

(define-public crate-zopen-0.6.0 (c (n "zopen") (v "0.6.0") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "zstd") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0m5z2wnwka0s1hq7h8makj72s5km6i4b6ibq56px43c1g8gj9wa8") (f (quote (("defaults"))))))

(define-public crate-zopen-1.0.0 (c (n "zopen") (v "1.0.0") (d (list (d (n "bzip2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "rust-lzma") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "zstd") (r "^0.13") (o #t) (d #t) (k 0)))) (h "150kd29xdrzbc4nab6g7yih5706pc4qf26ki56vyazw50s21xc47") (f (quote (("defaults") ("all" "gzip" "bzip2" "xz" "zstd")))) (s 2) (e (quote (("xz" "dep:rust-lzma") ("gzip" "dep:flate2"))))))

