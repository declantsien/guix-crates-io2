(define-module (crates-io zo mb zombie) #:use-module (crates-io))

(define-public crate-zombie-0.0.1 (c (n "zombie") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "04y7f44xbjwxl74wx9g26hcycz2dgfpl3ahc9hgrcr09fj4sgl80")))

(define-public crate-zombie-0.0.2 (c (n "zombie") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0k7l7wlj91zjvpp1wkwnkjyf6asls6ng7811h20bdllvpdbriqrv")))

(define-public crate-zombie-0.0.3 (c (n "zombie") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "11wmv5grwy2k8wcpq756yj04dq7lmksbkw3k143alkffamfwix9l")))

(define-public crate-zombie-0.0.4 (c (n "zombie") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1rn6b44105xc0l0q55x3hr1xv4w4x0rjyvn3gm1hvdirkz927ad2")))

