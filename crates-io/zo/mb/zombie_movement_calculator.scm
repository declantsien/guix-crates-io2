(define-module (crates-io zo mb zombie_movement_calculator) #:use-module (crates-io))

(define-public crate-zombie_movement_calculator-0.1.0 (c (n "zombie_movement_calculator") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "089crp3pjm7mlpfnpcpmqk2ylj82vyz2m1mwhnif3qxmpcv92giv")))

