(define-module (crates-io zo mb zombienet-prom-metrics-parser) #:use-module (crates-io))

(define-public crate-zombienet-prom-metrics-parser-0.1.0-alpha.0 (c (n "zombienet-prom-metrics-parser") (v "0.1.0-alpha.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0phm3qsmn2a8mll3axxbq6lsgnqv28qgzyvd3x9c1s430jymqa7x") (r "1.70.0")))

(define-public crate-zombienet-prom-metrics-parser-0.1.0-alpha.1 (c (n "zombienet-prom-metrics-parser") (v "0.1.0-alpha.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11mr9azbagarplw0nz4zzv1ybc7b8bw62cwp39ciqwrq5gbxi4p1") (r "1.70.0")))

(define-public crate-zombienet-prom-metrics-parser-0.2.0 (c (n "zombienet-prom-metrics-parser") (v "0.2.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r04c6zilhyzdkhvgza9hqly79j7hjvaqfxv518wbwznx9h5xlgm") (r "1.70.0")))

(define-public crate-zombienet-prom-metrics-parser-0.2.1 (c (n "zombienet-prom-metrics-parser") (v "0.2.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "005zvc7q8d5syyr6rgygd900g45bhayp1crpgkap8bizzb78xjal") (r "1.70.0")))

(define-public crate-zombienet-prom-metrics-parser-0.2.2 (c (n "zombienet-prom-metrics-parser") (v "0.2.2") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l44p7dd3dmpk4a47p79x6hkfcwcw5hyj0whbnv2zq1a1p7fq44b") (r "1.70.0")))

(define-public crate-zombienet-prom-metrics-parser-0.2.3 (c (n "zombienet-prom-metrics-parser") (v "0.2.3") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iiq197ggk2bn82f8n2l7nqisf3ias6ipzj4vy4cww7sgv1ppbhs") (r "1.70.0")))

