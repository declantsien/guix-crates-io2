(define-module (crates-io zo lt zolt-macros) #:use-module (crates-io))

(define-public crate-zolt-macros-0.1.0 (c (n "zolt-macros") (v "0.1.0") (h "0nq9kby7gpppxfalj5hmlbvng5kjiif1hs1f5lnyqg49czhyywk6")))

(define-public crate-zolt-macros-0.7.0 (c (n "zolt-macros") (v "0.7.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-proc") (r "=0.7.0") (d #t) (k 0)))) (h "106v3w2vcyg457n62ip9jrypjl2ly93bxd8biaj46abw98j28phf")))

(define-public crate-zolt-macros-0.7.1 (c (n "zolt-macros") (v "0.7.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-proc") (r "=0.7.1") (d #t) (k 0)))) (h "0swdynqc6c6g752443mazmdh53y252vvd2l9x59ika0l9ghvcj7f")))

(define-public crate-zolt-macros-0.7.2 (c (n "zolt-macros") (v "0.7.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-proc") (r "=0.7.2") (d #t) (k 0)))) (h "15sj1s92qc05pkmax89kimfxkk7kxnb28xkd888ki6fydazcczlb")))

