(define-module (crates-io zo lt zolt) #:use-module (crates-io))

(define-public crate-zolt-0.5.0 (c (n "zolt") (v "0.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0yyfw3qynyprpmffgxsbyv45z1wv0i5nislnmpqd1srr8hdkv75z")))

(define-public crate-zolt-0.6.0 (c (n "zolt") (v "0.6.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "02vs5z31ma2v2gr7cbv96hy107ryxq9cc092nqxpg2sqkqrninh9")))

(define-public crate-zolt-0.7.0 (c (n "zolt") (v "0.7.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-macros") (r "=0.7.0") (d #t) (k 0)))) (h "1j1w3bpzz3xb5sdsskndal54kx3f6c1470g6jh116qqs8qpr21i5")))

(define-public crate-zolt-0.7.1 (c (n "zolt") (v "0.7.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-macros") (r "=0.7.1") (d #t) (k 0)))) (h "1hmc87zsz7iaf6c2s6gnl2gpvyiqxwkmk4rj65x2sbvzp0hs9ps5")))

(define-public crate-zolt-0.7.2 (c (n "zolt") (v "0.7.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "zolt-macros") (r "=0.7.2") (d #t) (k 0)))) (h "0jlzij9shw5xrhqihmjr8q62593z2qpr6zkkvdyhhqrx4r8kdj7q")))

