(define-module (crates-io i5 #{4_}# i54_) #:use-module (crates-io))

(define-public crate-i54_-0.0.1 (c (n "i54_") (v "0.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "juniper") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ux_serde") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "014dprb2x8f26iws19qk6id6nx3sawz1p0ndig3rlc0w6209j3g1")))

(define-public crate-i54_-0.1.0 (c (n "i54_") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "juniper") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ux_serde") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ldyq8s52dg4cqawfpaj0m3ycsmvzza6b4a32d67vi7l05win0rj")))

(define-public crate-i54_-0.2.0 (c (n "i54_") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "juniper") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ux_serde") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0j4kw74x0cd48ygsv7y5fcrkzgjpsirh0wpbspj63j9w80qysq5c")))

