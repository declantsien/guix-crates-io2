(define-module (crates-io gy #{-2}# gy-21) #:use-module (crates-io))

(define-public crate-gy-21-0.2.0 (c (n "gy-21") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)))) (h "1457jvdv6b0a7x4zaj5vn5gi958ixivmgwklkprcm0wzdgnc1hra")))

