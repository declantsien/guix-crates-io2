(define-module (crates-io gy m- gym-core) #:use-module (crates-io))

(define-public crate-gym-core-0.1.0 (c (n "gym-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0588rpa6jx6l69wgv90vcs1xq1an0vd372f4cd1fqx4xnlab1r68")))

