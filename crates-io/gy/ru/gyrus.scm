(define-module (crates-io gy ru gyrus) #:use-module (crates-io))

(define-public crate-gyrus-0.0.1 (c (n "gyrus") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0vf9snz2xc7gy49yjn2fxsz6k4yigdvli4b72rmnbklgdcq38gn1") (y #t)))

(define-public crate-gyrus-0.0.3 (c (n "gyrus") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "0my4zgbjxmyzwzw1n8hbf86r7ncws4d26r07hn60ha0sxpqhnm6f") (y #t)))

(define-public crate-gyrus-0.0.4 (c (n "gyrus") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dont_disappear") (r "^2") (d #t) (k 2)) (d (n "noughts_and_crosses_lib") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)))) (h "1kjvqbnvwpjm7lfn5lyabj6r9x2mzhfk80m57cs4llcr0vpv2a3r") (y #t)))

