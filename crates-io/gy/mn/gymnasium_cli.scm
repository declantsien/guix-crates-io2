(define-module (crates-io gy mn gymnasium_cli) #:use-module (crates-io))

(define-public crate-gymnasium_cli-0.0.1 (c (n "gymnasium_cli") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gymnasium") (r "^0.0.1") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)))) (h "1hzmib39z4q5j208nsh692nhxmfd94x5fadykgfhkfm0jmdkf2pg") (r "1.70")))

