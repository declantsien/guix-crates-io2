(define-module (crates-io gy mn gymnasium_sys) #:use-module (crates-io))

(define-public crate-gymnasium_sys-0.0.1 (c (n "gymnasium_sys") (v "0.0.1") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py310" "auto-initialize"))) (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.1") (d #t) (k 1)))) (h "17jaf9j5hqhmn63lmj1c933mad0nsbjldcm8v31w740pp7cgd90p") (r "1.70")))

