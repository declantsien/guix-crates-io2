(define-module (crates-io gy mn gymnasium) #:use-module (crates-io))

(define-public crate-gymnasium-0.0.0 (c (n "gymnasium") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.19") (f (quote ("abi3-py37"))) (d #t) (k 0)))) (h "05il8ppx46rd9x1i2xdplsi1q3fp2xxdzax4dha258x0imcv6cqp") (r "1.56")))

(define-public crate-gymnasium-0.0.1 (c (n "gymnasium") (v "0.0.1") (d (list (d (n "gymnasium_sys") (r "^0.0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py310" "auto-initialize"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19q7wzxcc012aslvv8c0wdqa7dvcirz3ncdki0sf5sh25x7874ma") (r "1.70")))

