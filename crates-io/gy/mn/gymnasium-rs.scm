(define-module (crates-io gy mn gymnasium-rs) #:use-module (crates-io))

(define-public crate-gymnasium-rs-0.1.0 (c (n "gymnasium-rs") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1cg44k1m41rp09ay5cv9s58x43bvqfarvbn6iakb7rllpznm15sn")))

(define-public crate-gymnasium-rs-0.1.1 (c (n "gymnasium-rs") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0rsg8i3v9ciigiqyqbm8gp8yhjbjdzdbqwdlx28vspjbp3pw2dk4")))

