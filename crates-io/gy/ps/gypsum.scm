(define-module (crates-io gy ps gypsum) #:use-module (crates-io))

(define-public crate-gypsum-0.1.0 (c (n "gypsum") (v "0.1.0") (d (list (d (n "image") (r "^0.19.0") (f (quote ("png_codec"))) (k 0)) (d (n "uni-app") (r "^0.1.0") (d #t) (k 0)) (d (n "uni-gl") (r "^0.1.0") (d #t) (k 0)))) (h "1p0iwn2ll4ljwmvb5q1dkdk89jqawjnnzzwpn9kjadyinwrk1x2m")))

