(define-module (crates-io gy ms gyms) #:use-module (crates-io))

(define-public crate-gyms-0.1.0 (c (n "gyms") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "atari-env") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "gym-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 2)) (d (n "pixels") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "1mmhd39046isfcfs14bfhrbn9q597092br9mshgh84z6si1vnjqa") (f (quote (("default" "atari") ("atari" "atari-env"))))))

