(define-module (crates-io bn f_ bnf_rules) #:use-module (crates-io))

(define-public crate-bnf_rules-0.1.0 (c (n "bnf_rules") (v "0.1.0") (d (list (d (n "bnf_rules_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "bnf_rules_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1w5qnpzkrcamglbfkpik219qx9ywjd7rwgdjlz107dxllrq4qmdk")))

(define-public crate-bnf_rules-0.1.1 (c (n "bnf_rules") (v "0.1.1") (d (list (d (n "bnf_rules_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "bnf_rules_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1gw84cgbzshxkr6iib9b0rcgsaxb0xqpd8rg9rf872wj9679mh5j")))

(define-public crate-bnf_rules-0.1.2 (c (n "bnf_rules") (v "0.1.2") (d (list (d (n "bnf_rules_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "bnf_rules_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "05dcs3rgn7aayj8dj6zkk0z9zqqjfqg01vwd06fcn8aq28i5kmxa")))

