(define-module (crates-io bn f_ bnf_rules_parser) #:use-module (crates-io))

(define-public crate-bnf_rules_parser-0.1.1 (c (n "bnf_rules_parser") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08iclpcnyz0ib4hnh4lxw496y5jgfw2xvflg0nnggx7bqy82q00w")))

(define-public crate-bnf_rules_parser-0.1.2 (c (n "bnf_rules_parser") (v "0.1.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rnn4y2kg30ngs49mv8635ahxgav3s0si9y1lvncdpb527i7qcws")))

