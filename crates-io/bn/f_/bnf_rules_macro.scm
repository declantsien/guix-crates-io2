(define-module (crates-io bn f_ bnf_rules_macro) #:use-module (crates-io))

(define-public crate-bnf_rules_macro-0.1.1 (c (n "bnf_rules_macro") (v "0.1.1") (d (list (d (n "bnf_rules_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c6qbxr8bsnx0rmcsb6jv1qabcs7b5zr8ixvslckf05wnf6lc8rn")))

(define-public crate-bnf_rules_macro-0.1.2 (c (n "bnf_rules_macro") (v "0.1.2") (d (list (d (n "bnf_rules_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17p615z9cjqfd356qiyjls9lngymsp4ddx8hgsdawy0lqfa2wal8")))

