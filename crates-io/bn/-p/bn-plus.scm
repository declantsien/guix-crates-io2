(define-module (crates-io bn -p bn-plus) #:use-module (crates-io))

(define-public crate-bn-plus-0.4.4 (c (n "bn-plus") (v "0.4.4") (d (list (d (n "bincode") (r "^0.6") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jinrwrk2a35sw4j2mf6myzfv8rmj2f0nzz7gqk8f3sspka3f07r")))

