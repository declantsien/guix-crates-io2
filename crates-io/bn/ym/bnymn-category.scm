(define-module (crates-io bn ym bnymn-category) #:use-module (crates-io))

(define-public crate-bnymn-category-0.1.0 (c (n "bnymn-category") (v "0.1.0") (h "1iwgdc82lkhgxnhww2pabwizvgkfdnjm1jddfnqzcf4lfbsj2yzw")))

(define-public crate-bnymn-category-0.1.1 (c (n "bnymn-category") (v "0.1.1") (h "0ls7sj04cg1fi7yrxmyxzsfmwc5705lrlhdick8apqhn8idfwhqb")))

