(define-module (crates-io bn o0 bno080) #:use-module (crates-io))

(define-public crate-bno080-0.1.0 (c (n "bno080") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-log") (r "^0.6.1") (f (quote ("log-integration" "itm" "semihosting"))) (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0x9f8bqnipiwnfwxzw4p5dvdyvfdwxkc6jak1a7j2390g6rdg57k")))

(define-public crate-bno080-0.1.1 (c (n "bno080") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0bbamyiafmz19h4wf2jp7lific0q84i7xi46c61h1s88s9hzylwn")))

(define-public crate-bno080-0.1.2 (c (n "bno080") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1h64xphsc8l0zj9m52zn1szgszvxdg6l2ffzcw0bylm7sq6gdklg") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-bno080-0.1.3 (c (n "bno080") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-rtt-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0pqkbgghp14skxdyap0zz9v2dwjm3ci2yfbiqsa6yfi8jkj7my3s") (f (quote (("rttdebug" "panic-rtt-core") ("default"))))))

