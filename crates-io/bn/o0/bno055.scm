(define-module (crates-io bn o0 bno055) #:use-module (crates-io))

(define-public crate-bno055-0.1.0 (c (n "bno055") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "1hzsk97fbqr7x4qyph7anh3kgzxdvh0lx12whfnsbd91j2p0d9q0")))

(define-public crate-bno055-0.1.1 (c (n "bno055") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "0vfi8q80iann0bdf1vp05vxzc0m40x2gggsmvm073b9dnmmp6br8")))

(define-public crate-bno055-0.1.2 (c (n "bno055") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "144aiw616smxyzj1k00spq24nlqzbbcdga9pzjbclraa21vp9j4g")))

(define-public crate-bno055-0.1.3 (c (n "bno055") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "0y1aa3q5985kwc804l2xq9705wd399fd51g1g497b53xv5f19ax1")))

(define-public crate-bno055-0.1.4 (c (n "bno055") (v "0.1.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "00f3xzvxayhy334x73nxrzgp81zi9flpfhsbfz6sa2i2skm71899")))

(define-public crate-bno055-0.1.5 (c (n "bno055") (v "0.1.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "0hdxx218vqwvdj4z74s3c5i2b9kf89zf2r15njgifg4f4ak6h85w")))

(define-public crate-bno055-0.2.0 (c (n "bno055") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.0") (k 0)))) (h "0pybq2gymka13vk2967rz2qpzsbv0rsncb27inm5p53khhz88ss3")))

(define-public crate-bno055-0.3.0 (c (n "bno055") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1nb1lwlc54apn2zip63mq37fild5n56xzg12y5rp6yz8l0b5pjl9")))

(define-public crate-bno055-0.3.1 (c (n "bno055") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mint") (r "^0.5.4") (d #t) (k 0)))) (h "0a0w19iyhjzianwcbsmiwvpwb43vqrmbgzni9dp6v74jqn7vvrd4")))

(define-public crate-bno055-0.3.2 (c (n "bno055") (v "0.3.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mint") (r "^0.5.4") (d #t) (k 0)))) (h "1ryav7ssw6m8rsa4v0j46dfx8c0wzfgsrnfxjp4lysn2kxy0hx9z")))

(define-public crate-bno055-0.3.3 (c (n "bno055") (v "0.3.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0834hy2yzmg8i9is02w0l9qmypcjbwn4374wrvk53b62s9n6p20b") (f (quote (("std") ("default"))))))

(define-public crate-bno055-0.3.4 (c (n "bno055") (v "0.3.4") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "10rilqabmvn7qax5721x1zxpdab6vnx65rr7zwihsvnxm8gkbcjc") (f (quote (("std") ("default")))) (y #t) (s 2) (e (quote (("serde" "dep:serde" "mint/serde") ("defmt-03" "dep:defmt" "embedded-hal/defmt-03")))) (r "1.65")))

(define-public crate-bno055-0.4.0 (c (n "bno055") (v "0.4.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0fl5bg7kvjhf707ybc1zz2sr9mgacjzvq5k1wkgal3g118g3gbi0") (f (quote (("std") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "mint/serde") ("defmt-03" "dep:defmt" "embedded-hal/defmt-03")))) (r "1.65")))

