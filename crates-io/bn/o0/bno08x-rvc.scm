(define-module (crates-io bn o0 bno08x-rvc) #:use-module (crates-io))

(define-public crate-bno08x-rvc-0.1.0 (c (n "bno08x-rvc") (v "0.1.0") (d (list (d (n "bbqueue") (r "^0.5.0") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)))) (h "17i2nvrmmczmdllrd8qbj7j5i4j2cvgxm9n4a8wi3ng175vz314s") (y #t)))

(define-public crate-bno08x-rvc-0.1.1 (c (n "bno08x-rvc") (v "0.1.1") (d (list (d (n "bbqueue") (r "^0.5.0") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)))) (h "1p4zv1y8pmxhq13922x6angb83pxvda7inyim1ilan6dmqcpd34q")))

(define-public crate-bno08x-rvc-0.1.2 (c (n "bno08x-rvc") (v "0.1.2") (d (list (d (n "bbqueue") (r "^0.5.0") (d #t) (k 0)) (d (n "postcard") (r "^0.7.2") (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)))) (h "1w50i2xsh69crpx8zbqijpw1d8rl5lsir9z090y48f2p2a5grlvc")))

