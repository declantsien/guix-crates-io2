(define-module (crates-io bn om bnomial-cli) #:use-module (crates-io))

(define-public crate-bnomial-cli-0.1.0 (c (n "bnomial-cli") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20") (d #t) (k 0)))) (h "0il129888nwxcwgd92i8ixsipwfssip9g3amplm4ggfx6df8rm32")))

(define-public crate-bnomial-cli-0.1.1 (c (n "bnomial-cli") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20") (d #t) (k 0)))) (h "0fdx40qqcsfrqv0aj666si3msq7x3z8z8dmljf98h4yxhggxlvxy")))

