(define-module (crates-io bn #{25}# bn254) #:use-module (crates-io))

(define-public crate-bn254-0.0.1 (c (n "bn254") (v "0.0.1") (d (list (d (n "bn") (r "^0.6") (d #t) (k 0) (p "substrate-bn")) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "0wsfg4cdplz0gp6hmhzk087bighs51yrich3alw2wpzwbzgs553n")))

