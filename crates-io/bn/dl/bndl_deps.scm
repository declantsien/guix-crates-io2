(define-module (crates-io bn dl bndl_deps) #:use-module (crates-io))

(define-public crate-bndl_deps-1.0.0 (c (n "bndl_deps") (v "1.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0vfx45z9lmxvmbq3arhk60lrbglg41nn37zad3z0r2za7m5hr6r1")))

(define-public crate-bndl_deps-1.0.1 (c (n "bndl_deps") (v "1.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1qm8qyf4rxbydl574d8nd0m60lni9y8i4jcx7sm2yq826za524p0")))

(define-public crate-bndl_deps-1.0.2 (c (n "bndl_deps") (v "1.0.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0dnka5l6vnsgcafph9mrfn4bkcmgnpafms95cbvmc7vn6351nmfg")))

(define-public crate-bndl_deps-1.0.3 (c (n "bndl_deps") (v "1.0.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1hk4z4ydqnxl36dzss6qqri13ayi1fp6vmg1w9qwjdwlwbrirsp9")))

(define-public crate-bndl_deps-1.0.4 (c (n "bndl_deps") (v "1.0.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0a9nqigraxjn5pv89472kksp89yhvkrwvpid66by59fdx5l9lx79")))

(define-public crate-bndl_deps-1.1.0 (c (n "bndl_deps") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "04lvapinxlc83c5wryksv2w05rr2fwffpfngyvd66f80q3xgi3hc")))

(define-public crate-bndl_deps-1.1.1 (c (n "bndl_deps") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0a21zyj4c2wv8aw68vkn210qvg42n1l2xwi5pbn3krl8ygf4s6ik")))

