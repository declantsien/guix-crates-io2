(define-module (crates-io c- ce c-certitude) #:use-module (crates-io))

(define-public crate-c-certitude-1.0.0 (c (n "c-certitude") (v "1.0.0") (d (list (d (n "certitude") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "020a18yj5jxsbglr1g01s5ivxzg1wic9d6bk1va5jhcrxbkx4q9m")))

(define-public crate-c-certitude-1.1.0 (c (n "c-certitude") (v "1.1.0") (d (list (d (n "certitude") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pg5cd4sqi0b0364cqam0qzb4yyw7f5zjvw7br3bq8fph691rd2w")))

