(define-module (crates-io c- ty c-types) #:use-module (crates-io))

(define-public crate-c-types-0.1.0 (c (n "c-types") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0fq2qsy1vlnrxq25r47wkgfryinvxw6ch4r6r475gk5088xybvcg")))

(define-public crate-c-types-0.1.1 (c (n "c-types") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0l1nf2qanj6d7lh329wdyjkwgyzrfq4ab52ll5b0syxvx9in7ig3")))

(define-public crate-c-types-0.1.2 (c (n "c-types") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "05qimh07yav8fhwpf3p5gj40h36riv570nlwjx8jrd3hqdklx5hd")))

(define-public crate-c-types-0.1.3 (c (n "c-types") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "1plmvlz1q4ip0f0xlzhm0vfq8jfm4brz3m32ljf2x2z45v4nfb6g")))

(define-public crate-c-types-0.1.4 (c (n "c-types") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.10") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0wi6d56zfaj0p78hh6q45w8lgv78nb3js4qc1iqmw3246a3xr5iz")))

(define-public crate-c-types-1.0.0 (c (n "c-types") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1dyq9rkjsx9mxfinmas2ygffisxi6wkvzaw32zrzvz5wmrdd2nq4")))

(define-public crate-c-types-1.1.0 (c (n "c-types") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "11i0vqdkd1bywwll8kpg4v1hv8npmknk4q8bvcx4gjbpi46ixlf0") (y #t)))

(define-public crate-c-types-1.1.1 (c (n "c-types") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1lw8n8kzbylll1isr9vxgi707wcyppc0f7xg90r6cbsin5rpqrrx")))

(define-public crate-c-types-1.2.0 (c (n "c-types") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0qi1v39qqv4p84xgiz2jlz0npxz6ljsxis5f96wqbyihzzb17y6g")))

(define-public crate-c-types-2.0.0 (c (n "c-types") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13bf0p39dkdjq7gwf9xwf1sri7drgdcq109cdxl39mb24p7xv3s8")))

(define-public crate-c-types-2.0.1 (c (n "c-types") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "063a51gzxga99vswd48fs1ma65m7gkp0ybl3zv2m6qcmzw5nws5y")))

(define-public crate-c-types-2.0.2 (c (n "c-types") (v "2.0.2") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q84g3zj1kgy8n5mkfkppjm842j3q6175j3s1rvwsgi6ivp4hgmr")))

(define-public crate-c-types-3.0.0 (c (n "c-types") (v "3.0.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bxixriahrj1sz5hklvsfm2s1aafbh5i85l6k8z1cs2p2aislji3")))

(define-public crate-c-types-3.0.1 (c (n "c-types") (v "3.0.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z719mzg9wvfcd7ll1sqlpfpivlc38vxj7w7a2b0qahzcff38ysm")))

