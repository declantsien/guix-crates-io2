(define-module (crates-io c- la c-lang) #:use-module (crates-io))

(define-public crate-c-lang-0.1.0 (c (n "c-lang") (v "0.1.0") (d (list (d (n "pyu_rust_util") (r "^0.1.66") (d #t) (k 0)))) (h "0dzcycn4sj092am9hqml4hjs8plzg1b0kj40h7h1d58m3l633cmr")))

(define-public crate-c-lang-0.1.1 (c (n "c-lang") (v "0.1.1") (d (list (d (n "pyu_rust_util") (r "^0.1.66") (d #t) (k 0)))) (h "0v53401xy44rwihr35w86ym4qbfcjl60y6p762vc8q0g3816rk9j")))

(define-public crate-c-lang-0.1.2 (c (n "c-lang") (v "0.1.2") (d (list (d (n "pyu_rust_util") (r "^0.1.66") (d #t) (k 0)))) (h "02v1xfx63827mb7dzmwy7zxqjb0xpfiijkfzkq1nxf1q8lvnszzq")))

(define-public crate-c-lang-0.1.3 (c (n "c-lang") (v "0.1.3") (d (list (d (n "pyu_rust_util") (r "^0.1.66") (d #t) (k 0)))) (h "0n7x77y9bx9xbsxygjsi9yvnjzkha5h2h6npxfq6a4vy7mdwr9r8")))

(define-public crate-c-lang-0.1.4 (c (n "c-lang") (v "0.1.4") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0d7amqk4k1wli7ah2z76n98j0prj6k8wr3jm246qh270z9j5j491")))

(define-public crate-c-lang-0.1.5 (c (n "c-lang") (v "0.1.5") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16icrmjdqpr4mkcd9k3javdxza1cbjcgf5ys8snidmshglwg9q7x")))

(define-public crate-c-lang-0.1.6 (c (n "c-lang") (v "0.1.6") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xm79swy8yq8iy4a54jh605zqqrbdgdpvhsdi22zvc50wqihnfap")))

(define-public crate-c-lang-0.1.7 (c (n "c-lang") (v "0.1.7") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cxbp0s2k85yxda9sygs8m1jgv5g04d7ap2bx9sjzy6qhp825fyk")))

