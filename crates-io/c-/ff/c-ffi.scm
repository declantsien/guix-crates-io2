(define-module (crates-io c- ff c-ffi) #:use-module (crates-io))

(define-public crate-c-ffi-0.1.0 (c (n "c-ffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0ah2x8pcilrha29ikl42c340f0hps0bs6221l4jqd7hilybz64vq") (f (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.2.0 (c (n "c-ffi") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "14lxj73ridc5hv5pihxcg5i7zgc7qfwvd6zfimlm06h6i2najwbg") (f (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.3.1 (c (n "c-ffi") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.3") (d #t) (k 0)))) (h "1sf3z6laffx1spkn5fb2gfqri5wfpvzik52wm8j61kck0yalwkh4") (f (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.3.2 (c (n "c-ffi") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (d #t) (k 0)))) (h "0247bypdc0qpiwxpk8kj10p3fy1pphpznp8gv21x3p6sh767n603") (f (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.4.0 (c (n "c-ffi") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "184vg6svl9r2kpand524kmsld362s8jnskvfhbwcw7g10lwppsyx") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4.1 (c (n "c-ffi") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0ljwjbhq9yzhcx3x4s6yx9y3va8lzjlci3nw5irls3ln0svq9k6i") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4.2 (c (n "c-ffi") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0bbsfsy7l0rcnls68dj39n4lwd48lsb4scc61637wmgqnffba94j") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4.3 (c (n "c-ffi") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0rgmv39qx38jzm6zr9lll5mqxlkvf23l00lnxwf8cpclb42w8qis") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4.4 (c (n "c-ffi") (v "0.4.4") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0czyl1d8f8hy7c8flgzfjd3jxv2w04n09f2ngmzvlbfh8cas98jx") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4.5 (c (n "c-ffi") (v "0.4.5") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1k97dc2n5y4q66x0a8yx2ylw562aafkgdb0ykcl6zp8d2xpqxxbq") (f (quote (("memory" "smart-ptr" "libc") ("default")))) (y #t)))

(define-public crate-c-ffi-0.4.6 (c (n "c-ffi") (v "0.4.6") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0cx07xfypwd00f771mb0qv2kchix4inzkm6v9lrxgvmk7i79jabw") (f (quote (("memory" "smart-ptr" "libc") ("default"))))))

