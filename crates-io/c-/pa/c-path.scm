(define-module (crates-io c- pa c-path) #:use-module (crates-io))

(define-public crate-c-path-0.1.0 (c (n "c-path") (v "0.1.0") (h "0ar3a4brcb22jpmbxjgbfb9iqwpzh5r1s1id7mhg8k32gb42fh42")))

(define-public crate-c-path-0.1.1 (c (n "c-path") (v "0.1.1") (h "0xpx1k5w1wp7d680isqd41r6i17xj47wkd3fdsfszk7w35j8d3wg")))

