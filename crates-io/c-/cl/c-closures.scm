(define-module (crates-io c- cl c-closures) #:use-module (crates-io))

(define-public crate-c-closures-0.1.0 (c (n "c-closures") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g4s1ha8c0n3hs0m3ghijn02yxlfa94p12721kgsq9d20n2fa533")))

(define-public crate-c-closures-0.1.1 (c (n "c-closures") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0saabi4n5g53z53x5lkbgwn2mjgssc0b7a7sfhnzjf7ai7nzy8ah")))

(define-public crate-c-closures-0.1.2 (c (n "c-closures") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xqsahmv8hvyyjz0lrpxly2anvnq1sm3z9nmv5jzf4l9qzi8klcg")))

(define-public crate-c-closures-0.1.3 (c (n "c-closures") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qj764g7f7pp3l8fjirrkkhx5hp6ziizgymxwhcv4n4yh87h72yy")))

(define-public crate-c-closures-0.1.4 (c (n "c-closures") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0agq79pi03ppshmnd0xl5p3jq5z90hp0n7lvg5fbid7v3n97j6y2")))

(define-public crate-c-closures-0.1.5 (c (n "c-closures") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07yq119bk5haw1qwzv6hj9qnarca90b1xk9hr2iyn7rg3fmcw2qs")))

(define-public crate-c-closures-0.2.0 (c (n "c-closures") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ksrashql5zfr94xk51wir5jd4ch9xpk2xj0ccq5crj3371jmcqc")))

(define-public crate-c-closures-0.3.0 (c (n "c-closures") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1p026y7djfhrll7jqkfrw553dr73i1idx0z3ml8z38zaji63gkr3")))

(define-public crate-c-closures-0.3.1 (c (n "c-closures") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nzmig17m4h49qlqbw3vziiy2mb91silv7ma5ciinkqw5rrg1s4y")))

