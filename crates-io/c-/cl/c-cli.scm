(define-module (crates-io c- cl c-cli) #:use-module (crates-io))

(define-public crate-c-cli-0.1.0 (c (n "c-cli") (v "0.1.0") (h "1b29bp14nanb63ajcyw3rpsbyb6dlsifpw1n9k50zs6y69i3xmh0")))

(define-public crate-c-cli-0.2.0 (c (n "c-cli") (v "0.2.0") (h "1p92p3wdhxap5b5dn76xwr0xvdycbrpvnshzvimhdq4szcm4f1hp")))

(define-public crate-c-cli-0.2.1 (c (n "c-cli") (v "0.2.1") (h "1gqhbbjkgs8xwbpy6j0rdy1lkkgkjqdv76apfqsa0lvhlqb7jr84")))

(define-public crate-c-cli-0.2.2 (c (n "c-cli") (v "0.2.2") (h "0873nn2kp2y1kp009bg9r2n3l3p1y4y7rb6ghykc9lwc84wygwa3")))

