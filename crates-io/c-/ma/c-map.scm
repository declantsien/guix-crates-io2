(define-module (crates-io c- ma c-map) #:use-module (crates-io))

(define-public crate-c-map-0.1.0 (c (n "c-map") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1415i7cvf7k6ndxs8krwia4l46r934m931l9mjg0mc338fsgadj5")))

(define-public crate-c-map-0.2.0 (c (n "c-map") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1vhqqplc0rbwzv779fg36p6n4slvdx9bbi8y1dhp3hd4iiaw7d85")))

(define-public crate-c-map-0.2.1 (c (n "c-map") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1wbdppcjy2xh3h5215291dsvcqr6q2w3sl1bj5zpb592kyp69mql") (f (quote (("parking-lot" "parking_lot"))))))

