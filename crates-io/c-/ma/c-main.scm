(define-module (crates-io c- ma c-main) #:use-module (crates-io))

(define-public crate-c-main-1.0.0 (c (n "c-main") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "047ld2srgz08585cqh9p9fmk56f16qhwky3n1b9wpdcw8mnqp78m")))

(define-public crate-c-main-1.0.1 (c (n "c-main") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0xcz458hzz2zva39ls3la70xlsk8k19vaf761rzxpk9bppwbyyvr")))

