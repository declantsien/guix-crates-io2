(define-module (crates-io c- li c-like-concat) #:use-module (crates-io))

(define-public crate-c-like-concat-0.0.0 (c (n "c-like-concat") (v "0.0.0") (h "14vxxid3ghnsw5arhk6gwq4lxcydksszmf2shawj9yx9p4ny9xjs") (y #t)))

(define-public crate-c-like-concat-0.0.1 (c (n "c-like-concat") (v "0.0.1") (h "1dns0hspwfav4pqs7cwl2bvb8bzgdvb4rikd683yrzlnx8z44qhp") (y #t)))

(define-public crate-c-like-concat-0.0.2 (c (n "c-like-concat") (v "0.0.2") (h "12y7zjbr6jdhyms9fn4yah09yws1w7kh5442zrh0ic44885m91zi")))

(define-public crate-c-like-concat-0.0.3 (c (n "c-like-concat") (v "0.0.3") (h "15dy6qa0n4l0g6s9nlvi6cvjjg591nd4fj1b1jp4vsq9rx3a6hqa")))

(define-public crate-c-like-concat-0.0.4 (c (n "c-like-concat") (v "0.0.4") (h "1yr32b88zx97r4qp7k3y7kx66bv8pnqxp2ikhsrjxpjpyjjf10jc")))

