(define-module (crates-io c- le c-lexer-stable) #:use-module (crates-io))

(define-public crate-c-lexer-stable-0.1.1 (c (n "c-lexer-stable") (v "0.1.1") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "1y9v9r4swg78i1hldgxfmhx7zprmaiv3sik5lz090ag5ym488pvy")))

(define-public crate-c-lexer-stable-0.1.2 (c (n "c-lexer-stable") (v "0.1.2") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "1k6g2pq5zvbf3hd2yvp8lv9dxgfql1zpc5v28l6nlws1zafpm0zi")))

(define-public crate-c-lexer-stable-0.1.3 (c (n "c-lexer-stable") (v "0.1.3") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "1l5b3g662wap5vcb89m0nbqx8qvcjcj6jzjk0grv9lfld2f0zhfn")))

(define-public crate-c-lexer-stable-0.1.4 (c (n "c-lexer-stable") (v "0.1.4") (d (list (d (n "internship") (r "^0.6") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)))) (h "11mlkhffx1m45ggsz9a79iahjz9jbyg1hkcvfxrkvjaalcsdp9gk")))

