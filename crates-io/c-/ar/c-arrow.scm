(define-module (crates-io c- ar c-arrow) #:use-module (crates-io))

(define-public crate-c-arrow-0.1.0 (c (n "c-arrow") (v "0.1.0") (h "1ixd33fw6qjrhrm77n3zs75qqk379vffr0l8fnzsar8mmqqr3mfs") (y #t)))

(define-public crate-c-arrow-0.1.1 (c (n "c-arrow") (v "0.1.1") (h "0wwsvcjs24k81x3pmxbpkczi12x0cg53z5p32sk63rdw7dwy2vw8") (y #t)))

(define-public crate-c-arrow-0.1.2 (c (n "c-arrow") (v "0.1.2") (h "0adr1hl1r06yb3r2m9yqgbb7cf6dswkxhcpzgzfsnwh6k71iy1a2") (y #t)))

(define-public crate-c-arrow-0.1.3 (c (n "c-arrow") (v "0.1.3") (h "1j748mh9b7znv3p6gcljad0mwsm5fj8gr0wzd11grjq6g6vp6hlg")))

(define-public crate-c-arrow-0.1.4 (c (n "c-arrow") (v "0.1.4") (h "1hkq9sm3r7ykiqzyq2gz0hs00zrjc624zgwhm9zy5snn3m3lvxcx")))

