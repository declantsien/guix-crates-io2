(define-module (crates-io c- ex c-expr) #:use-module (crates-io))

(define-public crate-c-expr-0.1.0 (c (n "c-expr") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "15g9fz95h1d6j76542scaq2jsxi1ha4gsrdrgdsw4yp28sm0zsqb")))

(define-public crate-c-expr-0.2.0 (c (n "c-expr") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0kahh72r1s5dxbxh8sh3mshq830w2xkxay9qcqyia4sj6hr3680b")))

(define-public crate-c-expr-0.3.0 (c (n "c-expr") (v "0.3.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "03hhx68dpyjfyhap53r4fcysmibcaw2nxb5icgyd6jbyl611hi8h")))

(define-public crate-c-expr-0.3.1 (c (n "c-expr") (v "0.3.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1f5n7f5kfjmjjml9v2wc84ba6ms8p4m3a3bar7wp5rk96xz1ipk9")))

