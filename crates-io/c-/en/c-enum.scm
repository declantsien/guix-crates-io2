(define-module (crates-io c- en c-enum) #:use-module (crates-io))

(define-public crate-c-enum-0.1.0 (c (n "c-enum") (v "0.1.0") (h "14cdghs6r9ffkksis7wq9wdpg2yr1gck5plidjlrjrdlfv9q5bjc") (r "1.56")))

(define-public crate-c-enum-0.1.1 (c (n "c-enum") (v "0.1.1") (h "1hsm82bb9lx21m9pzi7n2x828nndi0w37ysskj0vkgizyrla5z75") (r "1.56")))

(define-public crate-c-enum-0.1.2 (c (n "c-enum") (v "0.1.2") (h "1ra71xpm0bj184xl55ml8j55hcaw22nbdlgfx5b7falwmwqikdx4") (r "1.56")))

(define-public crate-c-enum-0.2.0 (c (n "c-enum") (v "0.2.0") (h "0qya51jz8ps2hz121s69pazj18pm4ijnbbzvs5saa0slps513vla") (r "1.56")))

(define-public crate-c-enum-0.2.1 (c (n "c-enum") (v "0.2.1") (h "0djbi5hl8jm2zfi8571bc74bhk4zi5vsnv8x8p4w14rk2gnrijh8") (r "1.56")))

(define-public crate-c-enum-0.2.2 (c (n "c-enum") (v "0.2.2") (h "17mzs7f5wb5b05mjzqhaqsjlzmlz1iang5yl4drkaf0yvvkxx0w8") (r "1.56")))

(define-public crate-c-enum-0.2.3 (c (n "c-enum") (v "0.2.3") (h "1s5ckcr9xcp82hgpplspb8pp63mvlh039k5z4r4qjslcka8fn5yd") (r "1.56")))

