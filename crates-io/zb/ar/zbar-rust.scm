(define-module (crates-io zb ar zbar-rust) #:use-module (crates-io))

(define-public crate-zbar-rust-0.0.1 (c (n "zbar-rust") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "png") (r "^0.13.1") (d #t) (k 2)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "0w1y0a4xgn09phay7a7zy2bhsrddp9bn91saf7as3ydw91f5akii")))

(define-public crate-zbar-rust-0.0.2 (c (n "zbar-rust") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "01ck38n36i2m8w865bi5jlq4flaprp0p9wjd5bqdh6f1fkipb8i5")))

(define-public crate-zbar-rust-0.0.3 (c (n "zbar-rust") (v "0.0.3") (d (list (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "1809gy2fkiwd0jz7pl4h70i2vm0b373y5rbl54zcabjdnwfrihjp")))

(define-public crate-zbar-rust-0.0.4 (c (n "zbar-rust") (v "0.0.4") (d (list (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "05azgcsfvmqnzyj5flsj29kisvvqhldsjcpr7db3lknnw27wv5nk")))

(define-public crate-zbar-rust-0.0.5 (c (n "zbar-rust") (v "0.0.5") (d (list (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "1n6hd3xy9x1mg50p4kgg355a368p9zr967rcxckkjncp389amwfh")))

(define-public crate-zbar-rust-0.0.6 (c (n "zbar-rust") (v "0.0.6") (d (list (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "0hf83d3f7js8h2k8zywzjdxybss74h70dam9b11g9ap2z76ya365")))

(define-public crate-zbar-rust-0.0.7 (c (n "zbar-rust") (v "0.0.7") (d (list (d (n "enum-ordinalize") (r "^2.0.5") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.2") (d #t) (k 2)))) (h "03hqd6yq3l98jiric00gm4pyx84c1k5aqj8g53f0wqkpzpgf51a2")))

(define-public crate-zbar-rust-0.0.9 (c (n "zbar-rust") (v "0.0.9") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.6") (d #t) (k 2)))) (h "15w8l6l1lgwcdywps3inbljhy3zxkq62ygdr4b59nch0hj622lnr")))

(define-public crate-zbar-rust-0.0.10 (c (n "zbar-rust") (v "0.0.10") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.6") (d #t) (k 2)))) (h "02hbkh019qkwhahy2n7l5f8iwcfq6vy0nbpr4jwbfw677fzr11q3")))

(define-public crate-zbar-rust-0.0.11 (c (n "zbar-rust") (v "0.0.11") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.6") (d #t) (k 2)))) (h "1jynbdz403iklhyf8i68jk0qvkczibfmqdyjhbsnr31lb8vdwfml") (y #t)))

(define-public crate-zbar-rust-0.0.12 (c (n "zbar-rust") (v "0.0.12") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^1.0.6") (d #t) (k 2)))) (h "1sdsbcbsibvykg6ax7nxn43idq65l3jc53bvjiazmxlgpizwarkl")))

(define-public crate-zbar-rust-0.0.13 (c (n "zbar-rust") (v "0.0.13") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4") (d #t) (k 2)))) (h "05910r394ifxcqambc8zjwpkmrfn70fk29771pqrcm11d8z47dh6")))

(define-public crate-zbar-rust-0.0.14 (c (n "zbar-rust") (v "0.0.14") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4") (d #t) (k 2)))) (h "0k59yhcm0i42ng63nic5pq26y5afbv71d4acnx4498dfr3xj4rv5")))

(define-public crate-zbar-rust-0.0.15 (c (n "zbar-rust") (v "0.0.15") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4") (d #t) (k 2)))) (h "151i4fczx0wrlakgkpcv67zrshazj0nfd26bjxv3fdf6zri0m4jy")))

(define-public crate-zbar-rust-0.0.16 (c (n "zbar-rust") (v "0.0.16") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4") (d #t) (k 2)))) (h "0g71h3z254qc04v8ihk9ainjyvhfd3ms33pap6126c57mdd7y63f")))

(define-public crate-zbar-rust-0.0.17 (c (n "zbar-rust") (v "0.0.17") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "02yambq4vq9ca2dbbvsz6qrlmwbfl032d4bl8bgh4nz36c5f5k3s") (y #t)))

(define-public crate-zbar-rust-0.0.18 (c (n "zbar-rust") (v "0.0.18") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "05f7xlybdrb0x0g0xfc1yh0kz8j8ji0dsns5s0cjfis1g4fpsvhv")))

(define-public crate-zbar-rust-0.0.19 (c (n "zbar-rust") (v "0.0.19") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "01x0jv9n6zm187x41p3jpdjp5ps1hwfzb1r7w5bd9jcm4kysx50i")))

(define-public crate-zbar-rust-0.0.20 (c (n "zbar-rust") (v "0.0.20") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "12bysksipbx7i3hwsrj1hnvsw1xd32az42aj02kpw00pggl1qqx4")))

(define-public crate-zbar-rust-0.0.21 (c (n "zbar-rust") (v "0.0.21") (d (list (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "1sv5136lkianpbi658lk6gwqnl4fz9mddk3mwilsn80h5syi51xh")))

(define-public crate-zbar-rust-0.0.22 (c (n "zbar-rust") (v "0.0.22") (d (list (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "10zzpsk91lq71fgqlfjiasnbjkb1hnl5dm3xlixk222r1ikrjav9") (r "1.62")))

(define-public crate-zbar-rust-0.0.23 (c (n "zbar-rust") (v "0.0.23") (d (list (d (n "enum-ordinalize") (r "^4.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "qrcode-generator") (r "^4.1") (d #t) (k 2)))) (h "0m6c9393bvw2sngp36m0db1gzqxyqa9ms5biibz659md68di7k59") (r "1.63")))

