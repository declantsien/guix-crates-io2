(define-module (crates-io zb ar zbar) #:use-module (crates-io))

(define-public crate-zbar-0.1.0 (c (n "zbar") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "png") (r "^0.11.0") (d #t) (k 0)))) (h "1mp2iag2l333zgah211brrs03mhqkysfkimmdkfz1a2ldx8861i3")))

