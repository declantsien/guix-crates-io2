(define-module (crates-io zb ar zbar-rs) #:use-module (crates-io))

(define-public crate-zbar-rs-0.1.0 (c (n "zbar-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)))) (h "022nfnxbfbxybyx2ggzv4ksmp03rhnhh105w8zniz5gwk0qp87by") (f (quote (("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.0 (c (n "zbar-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1c41zv18dlc8rygicngww84pswr1pngbj0nm1pf1l39l2i43gjgs") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.1 (c (n "zbar-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0qz0rcq98623d7hjmqjx7bhqhcr46vjyrdb0vggpd63ggavazidg") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.2 (c (n "zbar-rs") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1ncncr0r1fgza0hjndjdb1lp836ksbz9m92j3aj5a1c9kir0pdbp") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.3 (c (n "zbar-rs") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1pjbb9znlwg7gni7q1hrw3q9avh41db858gwj916ibybx8lg15bf") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.4 (c (n "zbar-rs") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1bzvig8dl85ajssbriygw7144n9gispzai6z1wvf88cik9f75hsf") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.5 (c (n "zbar-rs") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1hkbmc70v42z97rfmfdn98f7lp3j5yxqqxh978acj6nba5s3rbyr") (f (quote (("zbar_fork") ("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.6 (c (n "zbar-rs") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1k72hnxj9yr37l88453mhydwsbk8073q4h2g2ydxhbf3wf557sx4") (f (quote (("from_image" "image") ("default" "from_image")))) (y #t)))

(define-public crate-zbar-rs-0.2.7 (c (n "zbar-rs") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0gnwiqgigdg2y9w0yih6agzng7g624b1mc7b89k4wqz46z7h0jxg") (f (quote (("zbar_fork_if_available") ("from_image" "image") ("default" "from_image" "zbar_fork_if_available"))))))

