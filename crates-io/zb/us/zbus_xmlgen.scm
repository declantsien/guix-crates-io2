(define-module (crates-io zb us zbus_xmlgen) #:use-module (crates-io))

(define-public crate-zbus_xmlgen-1.0.0 (c (n "zbus_xmlgen") (v "1.0.0") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^1") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "1z5b6fma45zc5fd9cwam9c3q45c6w5xi5cwd8nxid606h2w5ab5r")))

(define-public crate-zbus_xmlgen-2.0.0-beta.3 (c (n "zbus_xmlgen") (v "2.0.0-beta.3") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.3") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "1pyhsb088v36g4vmavz07xc89mjkzd8265sbnam8gp531v0dy34z")))

(define-public crate-zbus_xmlgen-2.0.0-beta.4 (c (n "zbus_xmlgen") (v "2.0.0-beta.4") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.4") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "0v7bvmd7xqmcbgp3q8nwkrs8ihnbsfb3b6i6v7q17sr8grra1mar")))

(define-public crate-zbus_xmlgen-2.0.0-beta.5 (c (n "zbus_xmlgen") (v "2.0.0-beta.5") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.5") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "0g57j13ag5vy9s6a0lfgpfcfsf8mliyl6hlfpdpj3qlzsqp3qpbw")))

(define-public crate-zbus_xmlgen-2.0.0-beta.6 (c (n "zbus_xmlgen") (v "2.0.0-beta.6") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.6") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "0di93pjnqk4ilz01k4adyfrq3fn075r4kvhy3hj0p6xr0jhjzcjk")))

(define-public crate-zbus_xmlgen-2.0.0-beta.7 (c (n "zbus_xmlgen") (v "2.0.0-beta.7") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.7") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^2") (d #t) (k 0)))) (h "09rih5malja2qjvp2fxg32cqn4rinxp3b78avrvb1zn4bypaw3bk")))

(define-public crate-zbus_xmlgen-2.0.0-beta.8 (c (n "zbus_xmlgen") (v "2.0.0-beta.8") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0-beta.8") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "0py854h2v56i17k5nv6qqdbqpmmxqrpikrwm6mdahrw0sxls9bjz")))

(define-public crate-zbus_xmlgen-2.0.0 (c (n "zbus_xmlgen") (v "2.0.0") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2.0.0") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "0676vyyfz1qsddk71k22ficnblhv7iq72ws2bkd35k5hmxdgkdyy")))

(define-public crate-zbus_xmlgen-2.0.1 (c (n "zbus_xmlgen") (v "2.0.1") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^2") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "19xbkkvqbrqbsz2j7xjmjfgiwwhl3p8zr116xp0kgsdifnr3wpzj")))

(define-public crate-zbus_xmlgen-3.0.0 (c (n "zbus_xmlgen") (v "3.0.0") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "1zhy5r0cifpi6b2vxsjnwf799q9gjgfc6irppxsbh3kqf2zrs2zi") (r "1.60")))

(define-public crate-zbus_xmlgen-3.1.0 (c (n "zbus_xmlgen") (v "3.1.0") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6.1") (f (quote ("quick-xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "1s0avfgmj344csdkmy6l89wik9ihfimxd1dihb8n6x7wpyzxw8x2") (r "1.60")))

(define-public crate-zbus_xmlgen-3.1.1 (c (n "zbus_xmlgen") (v "3.1.1") (d (list (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.6.1") (f (quote ("quick-xml"))) (d #t) (k 0)) (d (n "zvariant") (r "^3") (d #t) (k 0)))) (h "0pkaax4gls4y0619ar0c9v38j3363f0b2l77vhamagjvj1d8hlm3") (r "1.60")))

(define-public crate-zbus_xmlgen-4.0.1 (c (n "zbus_xmlgen") (v "4.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^4.0.0") (d #t) (k 0)) (d (n "zbus_xml") (r "^4.0.0") (d #t) (k 0)) (d (n "zvariant") (r "^4") (d #t) (k 0)))) (h "0jdkdrdrr0qs99sb777kx89c14g8xqxhjhzv0vx65vh8rzhywh1l") (r "1.75")))

(define-public crate-zbus_xmlgen-4.1.0 (c (n "zbus_xmlgen") (v "4.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "snakecase") (r "^0.1.0") (d #t) (k 0)) (d (n "zbus") (r "^4.0.0") (d #t) (k 0)) (d (n "zbus_xml") (r "^4.0.0") (d #t) (k 0)) (d (n "zvariant") (r "^4") (d #t) (k 0)))) (h "07rs39b6s30v1hfl1gmwl2xgdmjj82qrxir220r485svk58ys605") (r "1.75")))

