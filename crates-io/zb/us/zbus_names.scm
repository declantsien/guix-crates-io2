(define-module (crates-io zb us zbus_names) #:use-module (crates-io))

(define-public crate-zbus_names-1.0.0 (c (n "zbus_names") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^2") (f (quote ("enumflags2"))) (k 0)))) (h "12vwksv8q72z3ahq60q08qzx0247zj91c0y2wnganm8sdvhzd0j8")))

(define-public crate-zbus_names-1.1.0 (c (n "zbus_names") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^2") (f (quote ("enumflags2"))) (k 0)))) (h "0cda7vxf52rdw7sbckwzbycggyg9iqykvmklg70mz3pj69568nx4")))

(define-public crate-zbus_names-2.0.0 (c (n "zbus_names") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3") (f (quote ("enumflags2"))) (k 0)))) (h "0rpfpw3lpvr1gh9hlzbmymr8yl6rhgm2lp4cba0m8s1d4hni87xf")))

(define-public crate-zbus_names-2.1.0 (c (n "zbus_names") (v "2.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3") (f (quote ("enumflags2"))) (k 0)))) (h "1qdpqkyw3nzr927n8df4s6v692mqnyqjgk1hbm8as7dphz7wvps5")))

(define-public crate-zbus_names-2.2.0 (c (n "zbus_names") (v "2.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.5") (f (quote ("enumflags2"))) (k 0)))) (h "1x8axn2jdx3gcq2mxsjixhjfjsq3zp3nv42k1xlra9imibyhi921") (r "1.60")))

(define-public crate-zbus_names-2.3.0 (c (n "zbus_names") (v "2.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.8") (f (quote ("enumflags2"))) (k 0)))) (h "0rxfqwmi8ps10g5ni6s7fk2sx48ry388br0pnbc1x4718jdvg6yn") (r "1.60")))

(define-public crate-zbus_names-2.4.0 (c (n "zbus_names") (v "2.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.9") (f (quote ("enumflags2"))) (k 0)))) (h "1j239pvdg3x2vwdhjdjmc1ibn30f2rs8j0qdcs5789w62127cwvc") (r "1.60")))

(define-public crate-zbus_names-2.5.0 (c (n "zbus_names") (v "2.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.10") (f (quote ("enumflags2"))) (k 0)))) (h "1qz79whin6389iy3x86xmhl4ybqmmdzn4m21jfwvv2dx2r4k2kzk") (r "1.60")))

(define-public crate-zbus_names-2.5.1 (c (n "zbus_names") (v "2.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.10") (f (quote ("enumflags2"))) (k 0)))) (h "0amcfyxcacf1ki1j9a7kk2b52zcmwjim2ad7aw8p82my6dh1wi42") (r "1.60")))

(define-public crate-zbus_names-2.6.0 (c (n "zbus_names") (v "2.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.10") (f (quote ("enumflags2"))) (k 0)))) (h "1adl0acl39flk4jnrv7axg29kpwm7cy15w2wf0yy59mxdmvvp07v") (r "1.64")))

(define-public crate-zbus_names-3.0.0 (c (n "zbus_names") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0.0") (f (quote ("enumflags2"))) (k 0)))) (h "0v1f0ajwafj47bf11yp0xdgp26r93lslr9nb2v6624h2gppiz6sb") (r "1.75")))

(define-public crate-zbus_names-2.6.1 (c (n "zbus_names") (v "2.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zvariant") (r "^3.15") (f (quote ("enumflags2"))) (k 0)))) (h "13achs6jbrp4l0jy5m6nn7v89clfgb63qhldkg5ddgjh6y6p6za3") (r "1.64")))

