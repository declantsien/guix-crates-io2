(define-module (crates-io zb us zbus-lockstep) #:use-module (crates-io))

(define-public crate-zbus-lockstep-0.1.0 (c (n "zbus-lockstep") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "1p6593c8szpy0wpfxg0x2qsh8s706r1zgkmgwk564r1yj54vmfqb")))

(define-public crate-zbus-lockstep-0.2.0 (c (n "zbus-lockstep") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "1pvh8yz75d64xvqn9v406fqkbjc3jjwsdhvjw19sjwbmsgmgrf57")))

(define-public crate-zbus-lockstep-0.2.1 (c (n "zbus-lockstep") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "1ggsrrf9lyp81465d8xy7l4swyldwra04yp3m7lghcjhiavw2y66")))

(define-public crate-zbus-lockstep-0.2.2 (c (n "zbus-lockstep") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "1jaz2yr7l7shz7z36s35zzbwlamsw8m4a5lqy8vg5s7v27m1c36q")))

(define-public crate-zbus-lockstep-0.2.3 (c (n "zbus-lockstep") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "1p9l3fk8nmnciad4nb11w6jlpnnky2x9pakgq4drm8b1af8s04cm")))

(define-public crate-zbus-lockstep-0.3.0 (c (n "zbus-lockstep") (v "0.3.0") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "0c64rjl5h5zg9h0r1ycc2kimpcs6whch0qv0qhpnb7qc4cq75whf")))

(define-public crate-zbus-lockstep-0.3.1 (c (n "zbus-lockstep") (v "0.3.1") (d (list (d (n "tempfile") (r "^3.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus") (r "^3") (f (quote ("xml"))) (d #t) (k 0)))) (h "0yar4jybdw0pidrxxvpn6lqb0g2bpciamkf61b6d4v849r5qixmw")))

(define-public crate-zbus-lockstep-0.4.0 (c (n "zbus-lockstep") (v "0.4.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus_xml") (r "^4.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0") (d #t) (k 0)))) (h "0s8w5s9902a0f2vc4gvqdbg810msjbhqazpc4lkfv1ajdkwipr81")))

(define-public crate-zbus-lockstep-0.4.1 (c (n "zbus-lockstep") (v "0.4.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus_xml") (r "^4.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0") (d #t) (k 0)))) (h "0izkckjwsxaxwg4had4ffshxjv597jfa2zp90w9c5g9d02n69aad")))

(define-public crate-zbus-lockstep-0.4.2 (c (n "zbus-lockstep") (v "0.4.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus_xml") (r "^4.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0") (d #t) (k 0)))) (h "03lz98g8qki05iwr88pirygdnfx4si1wcj36kqylf40y3ma2z8bp")))

(define-public crate-zbus-lockstep-0.4.3 (c (n "zbus-lockstep") (v "0.4.3") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus_xml") (r "^4.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0") (d #t) (k 0)))) (h "0bdvldz91wxjc2anr714dnzj9pq1gnqikigsmcd86fiz3ldmmsqw")))

(define-public crate-zbus-lockstep-0.4.4 (c (n "zbus-lockstep") (v "0.4.4") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zbus_xml") (r "^4.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0") (d #t) (k 0)))) (h "07gdv18ppzb3i4fvlml7l43yb2mv4v4mah0mvsmdv6q9xgfcb8jc")))

