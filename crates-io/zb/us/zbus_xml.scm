(define-module (crates-io zb us zbus_xml) #:use-module (crates-io))

(define-public crate-zbus_xml-4.0.0 (c (n "zbus_xml") (v "4.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize" "overlapped-lists"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zbus_names") (r "^3.0") (d #t) (k 0)) (d (n "zvariant") (r "^4.0.0") (k 0)))) (h "0cx2yg4p0vkrgn5ccz6ds2wqzcy9wrwyjv5xnjmzcm5ra92kfgxb") (r "1.75")))

