(define-module (crates-io zb ik zbiko_art) #:use-module (crates-io))

(define-public crate-zbiko_art-0.1.0 (c (n "zbiko_art") (v "0.1.0") (h "1zlh2fi0ygmwl3qi4i4vl7nx7pa6jy7vz4a68psinv28iwmig9s5") (y #t)))

(define-public crate-zbiko_art-0.2.0 (c (n "zbiko_art") (v "0.2.0") (h "0i7z74h57m0paqgv8gf86jsvil864f1f67d717ym6l6gwyw6xmzd") (y #t)))

