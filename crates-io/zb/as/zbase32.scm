(define-module (crates-io zb as zbase32) #:use-module (crates-io))

(define-public crate-zbase32-0.0.1 (c (n "zbase32") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1qf242x9ccsllb74l42kdmpf9h8r9k2sfi84hmmwlinxh3h1x7m0")))

(define-public crate-zbase32-0.0.2 (c (n "zbase32") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "14n3j8sbnwzh40d752dk8dh1v7lghqq8qf0mc1hfiwn4qs005z1g")))

(define-public crate-zbase32-0.0.3 (c (n "zbase32") (v "0.0.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "16f4hvvz78krbxmrkllbbcjwj7dw1yb95fvl0mkx0jmd059px02z")))

(define-public crate-zbase32-0.0.4 (c (n "zbase32") (v "0.0.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1yvzpb3ydg7m1rjglia6xhwbkfrkvydpj7bi4dz36xvjib7ak3h7")))

(define-public crate-zbase32-0.0.5 (c (n "zbase32") (v "0.0.5") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1d316xjwz73wr2mf3yiimdw3jsnvwpwfavmcbv6h06b5w6jbq6d8") (f (quote (("unstable"))))))

(define-public crate-zbase32-0.1.0 (c (n "zbase32") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "cpython") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0vh8ndda5iisgw6w5mcgkaqxb82ayiab73pzwpxhnjb3lk6wgc93") (f (quote (("unstable") ("python_tests"))))))

(define-public crate-zbase32-0.1.1 (c (n "zbase32") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "cpython") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "086mmlxci8k308q7gmh8hps6j3ma8g6akyvvas4wwfmlfkrl82w5") (f (quote (("unstable") ("python_tests"))))))

(define-public crate-zbase32-0.1.2 (c (n "zbase32") (v "0.1.2") (d (list (d (n "cpython") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0gz3nmiaidscb5c85rh3qxi8i584gz5xm3amlxqminl8jq27k40g") (f (quote (("unstable") ("python_tests"))))))

