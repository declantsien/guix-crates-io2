(define-module (crates-io zb uf zbuf) #:use-module (crates-io))

(define-public crate-zbuf-0.1.0 (c (n "zbuf") (v "0.1.0") (h "0lm5z3ccijiji8i166zymq9grsniijalzx22dmxhxg821v9plqfm")))

(define-public crate-zbuf-0.1.1 (c (n "zbuf") (v "0.1.1") (d (list (d (n "utf-8") (r "^0.7.1") (d #t) (k 0)))) (h "0sigcrbb22i6l7z6ga5jrsmp1gmhn9nz8qz0ihgxx0s0x44axbg7")))

(define-public crate-zbuf-0.1.2 (c (n "zbuf") (v "0.1.2") (d (list (d (n "utf-8") (r "^0.7.1") (d #t) (k 0)))) (h "0lz3haqg65wj1j44k8vgvz0sxvf9m1i4xm3c9acs7nd42mc76jh1")))

