(define-module (crates-io vd ir vdir) #:use-module (crates-io))

(define-public crate-vdir-0.1.0 (c (n "vdir") (v "0.1.0") (d (list (d (n "vfile") (r "^0.1") (d #t) (k 0)))) (h "1xang8fv650kpcxpaa1fpcdjgqig7drxp106wddzfz1j777xh339") (y #t)))

(define-public crate-vdir-0.8.0 (c (n "vdir") (v "0.8.0") (d (list (d (n "vfile") (r "^0.8") (d #t) (k 0)))) (h "1mynhsssz19xysga7wdsh6249yw2prarlry6vlv4cvnsfpikcqkq") (y #t)))

