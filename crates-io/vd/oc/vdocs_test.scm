(define-module (crates-io vd oc vdocs_test) #:use-module (crates-io))

(define-public crate-vdocs_test-0.1.0 (c (n "vdocs_test") (v "0.1.0") (h "0kpq3p02004hhqfqrw0d4g8347zqzvrbspdyklild38sg72cmf9z")))

(define-public crate-vdocs_test-0.1.1 (c (n "vdocs_test") (v "0.1.1") (h "1c9516432578rpf4ylp34zrl0lg0yd7vybixczzxa280i3lyxf2l")))

