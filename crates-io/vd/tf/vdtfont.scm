(define-module (crates-io vd tf vdtfont) #:use-module (crates-io))

(define-public crate-vdtfont-0.3.0 (c (n "vdtfont") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arena_system") (r "^0.0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "owned_ttf_parser") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "12c34b05c0ya6dc2656rclq50pl9n1z2drazhaabs0l2qn8ix923")))

(define-public crate-vdtfont-0.3.1 (c (n "vdtfont") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arena_system") (r "^0.0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "owned_ttf_parser") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1m19fi4ad9y4q8gdnd8brxfacwr3ylpq9j0krcw8zrlshbibjjpx")))

(define-public crate-vdtfont-0.3.2 (c (n "vdtfont") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arena_system") (r "^0.0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (d #t) (k 0)) (d (n "owned_ttf_parser") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0l3ww30g69zdfzxbgpnzavmihlsspdg4izbdy61i6jyrn6y22p69")))

