(define-module (crates-io vd so vdso) #:use-module (crates-io))

(define-public crate-vdso-0.1.0 (c (n "vdso") (v "0.1.0") (d (list (d (n "crt0stack") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 2)))) (h "1ffb17bhnwq33q2iry786a6vla9y0bs5xdgqxhdl4xrnacxbck47")))

(define-public crate-vdso-0.2.0 (c (n "vdso") (v "0.2.0") (d (list (d (n "crt0stack") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 2)))) (h "1931xqbvcl71p7h534fhv0j3f68k7d4cfkv7dw754hd52jr9kcwc")))

(define-public crate-vdso-0.2.1 (c (n "vdso") (v "0.2.1") (d (list (d (n "crt0stack") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 2)))) (h "1ny9rf96ay402lccv8hcnsd58ra1i4jk9xyhs6hxnaygi6k84qkv")))

(define-public crate-vdso-0.2.2 (c (n "vdso") (v "0.2.2") (d (list (d (n "crt0stack") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 2)))) (h "0w9jd4x9172pfyj7smhxq0ga4xla32yqaxa6xp3jiz6rl8nry47i") (r "1.56")))

