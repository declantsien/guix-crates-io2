(define-module (crates-io vd j_ vdj_types) #:use-module (crates-io))

(define-public crate-vdj_types-0.1.0 (c (n "vdj_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "strum") (r ">=0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.17.1") (d #t) (k 0)))) (h "0wldbcm2zwhhr4m8gkixyjd7azlw3qwxjyps7gl17399jq8kiv71")))

(define-public crate-vdj_types-0.1.1 (c (n "vdj_types") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "strum") (r ">=0.18.0, <0.21") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.21") (d #t) (k 0)))) (h "00za4wa8jnwnlylz2sikk32v2yw6ijrz87pc8kaasj30231bw65n")))

(define-public crate-vdj_types-0.1.2 (c (n "vdj_types") (v "0.1.2") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "strum") (r ">=0.18.0, <0.21") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.21") (d #t) (k 0)))) (h "1dx4r6v1dlsz6cvkg9b94pflpnxgz0395v62a11dd72bhg9vivyx")))

(define-public crate-vdj_types-0.1.3 (c (n "vdj_types") (v "0.1.3") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "strum") (r ">=0.18.0, <0.22") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.22") (d #t) (k 0)))) (h "1c8r0aw65picyjzhzc8y72hsr5qrx67ryi2bvwbvgfnid36mr5x0")))

(define-public crate-vdj_types-0.1.4 (c (n "vdj_types") (v "0.1.4") (d (list (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "strum") (r ">=0.18.0, <0.22") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.22") (d #t) (k 0)))) (h "150n2q734d4gb1y1ipgzlkdb9hpsb5z2cg67cl574sm6lxblmyjl")))

(define-public crate-vdj_types-0.1.5 (c (n "vdj_types") (v "0.1.5") (d (list (d (n "enum-iterator") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 2)) (d (n "strum") (r ">=0.18.0, <0.22") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.22") (d #t) (k 0)))) (h "0z5d5bwsmjl3aki7bgbqi7188f7xx1a8c92scl2ggavndlbfaxjs")))

(define-public crate-vdj_types-0.1.7 (c (n "vdj_types") (v "0.1.7") (d (list (d (n "enum-iterator") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "strum") (r ">=0.18.0, <0.22") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.18.0, <0.22") (d #t) (k 0)))) (h "15nb1jipvq2f3xs7vij58k50r33fgykqr8xaf5hhxd849zh0xh7a")))

(define-public crate-vdj_types-0.2.0 (c (n "vdj_types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "14dy8mvr1wmi7bqkp3jwvzbpwr0smdy8ypyzxhx6jaf5xhmbx782")))

