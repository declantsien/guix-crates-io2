(define-module (crates-io vd pa vdpau-sys) #:use-module (crates-io))

(define-public crate-vdpau-sys-0.0.1 (c (n "vdpau-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xlib"))) (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"netbsd\"))") (k 0)))) (h "0s3lkf8p3p7g0xb3hvgrnmvkm36ri8cp2dff2s0hzffwyszsb7gb")))

