(define-module (crates-io vd f- vdf-parser) #:use-module (crates-io))

(define-public crate-vdf-parser-0.1.0 (c (n "vdf-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "02qb7mm2f1d14ybi73gg8272gm94mv6mz0fib7q5ssflnhbp85dc")))

(define-public crate-vdf-parser-0.1.1 (c (n "vdf-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "18kg4l7ali9qjii4vfm0sbdyzgjwk6d7zn14k409v15knb326hxb")))

(define-public crate-vdf-parser-0.1.2 (c (n "vdf-parser") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0crdyjnwssqs326kb8ryqbxfsxz6mz60pxj7bcdnhyhwkcfr3pa2")))

(define-public crate-vdf-parser-0.1.3 (c (n "vdf-parser") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0d5ks66xdx81yd1h0rqc906x7vcjfpbgh5b0s1m6iamla7q3vzlr")))

(define-public crate-vdf-parser-0.1.4 (c (n "vdf-parser") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "12h17rr398f07q0ky2fs7cy1lx3lkxb6qgzx4cq7503a3bds1siw")))

(define-public crate-vdf-parser-0.1.5 (c (n "vdf-parser") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0hv26ndpp52pgx4zj743z0js748z9304hp9wxr1gy7f5qa2947z6")))

(define-public crate-vdf-parser-0.1.6 (c (n "vdf-parser") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "16xsysx9249j4a7gibkzps3yhfy6ax0q8xxpfm4rapvyxbzzik1y")))

(define-public crate-vdf-parser-0.1.7 (c (n "vdf-parser") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1fkcm8hj5rfhnqy0s68dxkcwjfn9zqwdyfjc6ijvkpizan9zd0d3")))

(define-public crate-vdf-parser-0.1.8 (c (n "vdf-parser") (v "0.1.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1vkmfhv6giljn1nr9hnhxa16bj109hiqbqr30m40a54n41qcgmg6")))

