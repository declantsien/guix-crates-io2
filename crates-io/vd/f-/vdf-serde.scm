(define-module (crates-io vd f- vdf-serde) #:use-module (crates-io))

(define-public crate-vdf-serde-0.1.0 (c (n "vdf-serde") (v "0.1.0") (d (list (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "steamy-vdf") (r "^0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0y317jcrc48msimpsh502nr251sab2g8v80bj17iclfh1m2n2xr8")))

(define-public crate-vdf-serde-0.2.0 (c (n "vdf-serde") (v "0.2.0") (d (list (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "steamy-vdf") (r "^0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1q2aqhz6kzwmc2acnqc1xnfsf282nni7xvwripjky627b1dhn0vv")))

(define-public crate-vdf-serde-0.3.0 (c (n "vdf-serde") (v "0.3.0") (d (list (d (n "nom") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "steamy-vdf") (r "^0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1fvdqpwbrrvppv1dzpcqsm1ff78687jm9qlxpz14l3p6i223zvxp")))

