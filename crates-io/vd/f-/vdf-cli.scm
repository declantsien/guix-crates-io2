(define-module (crates-io vd f- vdf-cli) #:use-module (crates-io))

(define-public crate-vdf-cli-0.1.0 (c (n "vdf-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "classgroup") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "vdf") (r "^0.1.0") (d #t) (k 0)))) (h "1v538csg5igi99xis062rnf4jcvgpygx2vlc29fdla96kwq1jn25")))

