(define-module (crates-io f6 #{4a}# f64ad_core_derive) #:use-module (crates-io))

(define-public crate-f64ad_core_derive-0.0.1 (c (n "f64ad_core_derive") (v "0.0.1") (d (list (d (n "f64ad_core") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sspy4pq59f1wd00csns8d7n5dc2nq4f2baarkx13y5d9fmpmbh5")))

(define-public crate-f64ad_core_derive-0.0.2 (c (n "f64ad_core_derive") (v "0.0.2") (d (list (d (n "f64ad_core") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sskajjn8ndw0g7r63vrhrwxhd1a48227103hz15zfjjsyb914hs")))

(define-public crate-f64ad_core_derive-0.0.3 (c (n "f64ad_core_derive") (v "0.0.3") (d (list (d (n "f64ad_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hw6khxxaibqmyps9rpgf9c8jycz0jyv81wyjxywf6jgg15al0a4")))

(define-public crate-f64ad_core_derive-0.0.4 (c (n "f64ad_core_derive") (v "0.0.4") (d (list (d (n "f64ad_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g1zw6djsk879f35mjza9w6wydkis2y3myj917l8lvmqhymnhnm7")))

(define-public crate-f64ad_core_derive-0.0.5 (c (n "f64ad_core_derive") (v "0.0.5") (d (list (d (n "f64ad_core") (r "^0.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wnvlk5syhlkn36jmjfgz25cfd0pwxab3qd9r49dk1g9bsyxkkdp")))

