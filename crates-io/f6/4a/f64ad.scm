(define-module (crates-io f6 #{4a}# f64ad) #:use-module (crates-io))

(define-public crate-f64ad-0.0.1 (c (n "f64ad") (v "0.0.1") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)) (d (n "f64ad_core") (r "^0.0.1") (d #t) (k 0)) (d (n "f64ad_core_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "142jnhh5hkv7a7ddx2c999cr2xyyxfnp32wrjqgayb94l8ywmk1m")))

(define-public crate-f64ad-0.0.2 (c (n "f64ad") (v "0.0.2") (d (list (d (n "f64ad_core") (r "^0.0.2") (d #t) (k 0)) (d (n "f64ad_core_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "1knww34qa3j2f9gj2q7x6wdkkwj9l4s3422f6px2sxjhdkzjxfdi")))

(define-public crate-f64ad-0.0.3 (c (n "f64ad") (v "0.0.3") (d (list (d (n "f64ad_core") (r "^0.0.2") (d #t) (k 0)) (d (n "f64ad_core_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "0i69mz245cw8cmms5x7q8qx55qm23swdcjp71jr9bnk727g5xqmr")))

(define-public crate-f64ad-0.0.4 (c (n "f64ad") (v "0.0.4") (d (list (d (n "f64ad_core") (r "^0.0.3") (d #t) (k 0)) (d (n "f64ad_core_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "1xkxqyqgnqmmlrrzibvlkm5id5kv1kh0q2054d24086r2k1lw86h")))

(define-public crate-f64ad-0.0.5 (c (n "f64ad") (v "0.0.5") (d (list (d (n "eval") (r "^0.1.1") (d #t) (k 0)) (d (n "f64ad_core") (r "^0.0.5") (d #t) (k 0)) (d (n "f64ad_core_derive") (r "^0.0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)))) (h "1f9y3gldqai6z9gdy9lvv8h7rm5x3l9i3lyfyg446zx0bkyb7p4y")))

