(define-module (crates-io jq ue jqueuers) #:use-module (crates-io))

(define-public crate-jqueuers-0.1.0 (c (n "jqueuers") (v "0.1.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1phq7al6jz5wvqzaxq17mdsv1rhjsn40a8fvmv0385pksclfnb99")))

(define-public crate-jqueuers-0.1.1 (c (n "jqueuers") (v "0.1.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cn63pn36vmnxr4950f52x5hkfmpm4rr34lrp6nja37qvbclql7m")))

(define-public crate-jqueuers-0.1.2 (c (n "jqueuers") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (f (quote ("connection-manager" "tokio-comp" "aio"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "107bscdgfbwi0mskzmv2sy247jjs22y7yk655mh92lsq4li6dc0s")))

