(define-module (crates-io jq l- jql-parser) #:use-module (crates-io))

(define-public crate-jql-parser-6.0.0 (c (n "jql-parser") (v "6.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "17krvkhvvgx8k7aq7xkmb5v68jp58b7hcr5waip9b2qia16vkl28")))

(define-public crate-jql-parser-6.0.1 (c (n "jql-parser") (v "6.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jyy90m57hsszd0p3abqzb0xg1gjzzpdmmps2jijzaz06if0lxgi")))

(define-public crate-jql-parser-6.0.2 (c (n "jql-parser") (v "6.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "115r4x09ss2wvnwijwap4y2rncv38n02jphjfzlqbic76yf6a28d")))

(define-public crate-jql-parser-6.0.3 (c (n "jql-parser") (v "6.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "003c37gya77xm6a37fjl050wk4g08vmjlx8y42w8qpm4w70gdxh4")))

(define-public crate-jql-parser-6.0.4 (c (n "jql-parser") (v "6.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07zmd5ls62ywhl4fafbw15zr85cbpyrblb8wicq0rri7h3qq1rws")))

(define-public crate-jql-parser-6.0.5 (c (n "jql-parser") (v "6.0.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0i93n795961rpfhxp046wkksd200x7a3gf23qvd9w13ka0a5dcav")))

(define-public crate-jql-parser-6.0.6 (c (n "jql-parser") (v "6.0.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "117wf84vj8sagg1b4ls2wy10gcsm0p69lpbjzlxds17j5gdbfzvn")))

(define-public crate-jql-parser-6.0.7 (c (n "jql-parser") (v "6.0.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0zac1758df657drvmhhw275asjh77k2y66vjrqnfnyczn8nibxwg")))

(define-public crate-jql-parser-6.0.8 (c (n "jql-parser") (v "6.0.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1vs8305apzk0qs32x6d8ky01qsffbrw7gm7j7aay9fnsw8a9hk7w")))

(define-public crate-jql-parser-6.0.9 (c (n "jql-parser") (v "6.0.9") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jaskk03dvqw5kxdh87kv408w4a8qw5s3rl0kbf8767061plf0l7")))

(define-public crate-jql-parser-7.0.0 (c (n "jql-parser") (v "7.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "111gad10b8vzap68x2ndymj65m3fr9aljch0v5bsaz18m7q5gkd6")))

(define-public crate-jql-parser-7.0.1 (c (n "jql-parser") (v "7.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "08mjggcvbqy4znyhawa1i6ns3i5hq0d1jgf3428sqafk8b8xzg5n")))

(define-public crate-jql-parser-7.0.2 (c (n "jql-parser") (v "7.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "10mf21z336x2484rl3j3xf2gliglyj27rgc3fp5nlpj17bkac0dk")))

(define-public crate-jql-parser-7.0.3 (c (n "jql-parser") (v "7.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1j3plqbflfs99bnvmh3lnb5v133llnirqm3l0dnhvz78q30gsg3k")))

(define-public crate-jql-parser-7.0.4 (c (n "jql-parser") (v "7.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0ahm6nfs0d8g7g0w5q2i9yi3z1g65lmj4hcvyn8ns39mwdvss96h")))

(define-public crate-jql-parser-7.0.5 (c (n "jql-parser") (v "7.0.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1fnsfmffccggvfdc8f536wgsdyhndslwkny4vj34p5naxq1kb8qm")))

(define-public crate-jql-parser-7.0.6 (c (n "jql-parser") (v "7.0.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1y10rr6c5rzy32qzng9bq45hca8ws8i9gnhsjl9jck6n101m4xgj")))

(define-public crate-jql-parser-7.0.7 (c (n "jql-parser") (v "7.0.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0w4d1yghw7saq2kqf35d79z33vsjhm30cm06wbi8wmq7dmaf9i6q")))

(define-public crate-jql-parser-7.1.0 (c (n "jql-parser") (v "7.1.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.19") (d #t) (k 0)))) (h "0dcnmwrmj33hx7apjsa51grhqrbaw40vjwj8h0nyjh4diyax9ms6")))

(define-public crate-jql-parser-7.1.1 (c (n "jql-parser") (v "7.1.1") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.25") (f (quote ("simd"))) (d #t) (k 0)))) (h "11yn5jr1zv1sv5miad8k1z5shsrapj899d22ppa352wqb61fpvby")))

(define-public crate-jql-parser-7.1.2 (c (n "jql-parser") (v "7.1.2") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.30") (f (quote ("simd"))) (d #t) (k 0)))) (h "04rs81lggdibnxmq0d4qsy5i9j11k9qxjl92jknih4kncnyi43pg")))

(define-public crate-jql-parser-7.1.3 (c (n "jql-parser") (v "7.1.3") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.34") (f (quote ("simd"))) (d #t) (k 0)))) (h "063aw3935jfq28z109lfg4ryiwsnzvzbydgaia9i9dwjsm4yazg8")))

(define-public crate-jql-parser-7.1.4 (c (n "jql-parser") (v "7.1.4") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (f (quote ("simd"))) (d #t) (k 0)))) (h "0kqn07dib13iqyykfgdvszmdlfq561x7xx6bnrx5qd2kgbjcdzvf")))

(define-public crate-jql-parser-7.1.5 (c (n "jql-parser") (v "7.1.5") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (f (quote ("simd"))) (d #t) (k 0)))) (h "0bz845czclhhh3g24f3cgia007i6rs23blq7q9pb96yfakxafvvv")))

(define-public crate-jql-parser-7.1.6 (c (n "jql-parser") (v "7.1.6") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (f (quote ("simd"))) (d #t) (k 0)))) (h "0bvkakagkx06m19lxjv3qyl8gklspfyf1b7ckg2b71p9i21fpxja")))

(define-public crate-jql-parser-7.1.7 (c (n "jql-parser") (v "7.1.7") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (f (quote ("simd"))) (d #t) (k 0)))) (h "0fjnb25h8c8qhiqvxyq3sszikwq64qgkn4r9r8jk7jvv7sg5ir9h")))

(define-public crate-jql-parser-7.1.8 (c (n "jql-parser") (v "7.1.8") (d (list (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "winnow") (r "^0.6.6") (f (quote ("simd"))) (d #t) (k 0)))) (h "1phk0psklkxjvlxnisi735hqg2fxbzsqhk17g4imi018k3ny38vs")))

(define-public crate-jql-parser-7.1.9 (c (n "jql-parser") (v "7.1.9") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "winnow") (r "^0.6.8") (f (quote ("simd"))) (d #t) (k 0)))) (h "1nfm300lfgzgp6bs2p004yp5cyldv9g2bb388v2m3kdhghla8syp")))

(define-public crate-jql-parser-7.1.10 (c (n "jql-parser") (v "7.1.10") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)) (d (n "winnow") (r "^0.6.8") (f (quote ("simd"))) (d #t) (k 0)))) (h "1l98hgsgpzsm2sy12p9bdpf0f26gfmy238q36fw0620ykdkwvd4j")))

