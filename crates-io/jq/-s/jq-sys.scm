(define-module (crates-io jq -s jq-sys) #:use-module (crates-io))

(define-public crate-jq-sys-0.1.0 (c (n "jq-sys") (v "0.1.0") (d (list (d (n "jq-src") (r "^0.1") (d #t) (k 0)))) (h "0d3aqs3v2w9xrd6dxyd07z4nraxkfz7sgyqkrmfrwf9qbl4h4jy8")))

(define-public crate-jq-sys-0.1.1 (c (n "jq-sys") (v "0.1.1") (d (list (d (n "jq-src") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0gxn49567nq4vlbw6hw83vmx396hffa40j101cgxgb6iqr1w9709") (f (quote (("default" "bundled") ("bundled" "jq-src"))))))

(define-public crate-jq-sys-0.2.0 (c (n "jq-sys") (v "0.2.0") (d (list (d (n "jq-src") (r "^0.2.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (o #t) (d #t) (k 1)))) (h "1gv1ab1rx78psw778l05lylra7wr9qkqm81kzlr6xa8pw1vansrk") (f (quote (("default" "pkg-config") ("bundled" "jq-src")))) (l "jq")))

(define-public crate-jq-sys-0.2.1 (c (n "jq-sys") (v "0.2.1") (d (list (d (n "jq-src") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (o #t) (d #t) (k 1)))) (h "1ppzd0hzwwbliv46zvlxpw686wgngqqz0v4qcyqi222nk8j4xi86") (f (quote (("default" "pkg-config") ("bundled" "jq-src")))) (l "jq")))

(define-public crate-jq-sys-0.2.2 (c (n "jq-sys") (v "0.2.2") (d (list (d (n "jq-src") (r "^0.4") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (o #t) (d #t) (k 1)))) (h "0j2cp9gb7yb53zdmlnygnpd5hc8dyhcwzjr0rg4gybxxaiz5pdl1") (f (quote (("default" "pkg-config") ("bundled" "jq-src")))) (l "jq")))

