(define-module (crates-io jq -s jq-src) #:use-module (crates-io))

(define-public crate-jq-src-0.1.0 (c (n "jq-src") (v "0.1.0") (d (list (d (n "autotools") (r "^0.1.3") (d #t) (k 1)))) (h "1pzic9qggdfcb8yyxrvwrxaqcsv5d7hkw0sk24az3absnrxk2v6r") (l "jq")))

(define-public crate-jq-src-0.2.0 (c (n "jq-src") (v "0.2.0") (d (list (d (n "autotools") (r "^0.1.3") (d #t) (k 0)))) (h "0s2vy41003ybzv387d73nnh4rzpj9j3q8xg6gmxnxqjsxg2z67x5")))

(define-public crate-jq-src-0.3.0 (c (n "jq-src") (v "0.3.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 0)))) (h "1wcb4vxr1nnp6kqpzam7pn1qq7lrrk8rgbdh9cgg7rfk3hpdiqm7") (y #t)))

(define-public crate-jq-src-0.3.1 (c (n "jq-src") (v "0.3.1") (d (list (d (n "autotools") (r "^0.1") (d #t) (k 0)))) (h "0h344fvha165m4k2gli3yzwvyw1c44mzgrpp47v9k68yn896jsmw")))

(define-public crate-jq-src-0.4.0 (c (n "jq-src") (v "0.4.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 0)))) (h "1xah35bv59jgll15yvm1vac5nkqczssw3xx8l3rgbawhwsp7k1fl")))

(define-public crate-jq-src-0.4.1 (c (n "jq-src") (v "0.4.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 0)))) (h "1ddlcqn62g3x3iy5idk0f0pf7g0lscba0x1m46b5p9nxc70s08xf")))

