(define-module (crates-io jq un jqunit) #:use-module (crates-io))

(define-public crate-jqunit-0.1.0-rc (c (n "jqunit") (v "0.1.0-rc") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1c66zx0jljaf1m2gvz0m5jys3nindkcyqym76m4vivsdi2d9x0bk")))

