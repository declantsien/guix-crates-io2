(define-module (crates-io jq da jqdata-model) #:use-module (crates-io))

(define-public crate-jqdata-model-0.2.0 (c (n "jqdata-model") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yhxni6j94k0rp9qxxrlr8ll0lbdybzi5ml25abyl8mwv4ka4li3")))

(define-public crate-jqdata-model-0.3.0 (c (n "jqdata-model") (v "0.3.0") (d (list (d (n "bigdecimal") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ywl80vyfffysd9k2ly8wm17lzxgm2mv0dik7r5af2vi7ryx15va")))

(define-public crate-jqdata-model-0.3.1 (c (n "jqdata-model") (v "0.3.1") (d (list (d (n "bigdecimal") (r "^0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1072il4y8448qz4nai940nbalqp734afnkjanqnx3m0as1cjrrz3")))

(define-public crate-jqdata-model-0.3.2 (c (n "jqdata-model") (v "0.3.2") (d (list (d (n "bigdecimal") (r "^0.0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0irgaid8dx5h2pf76gqcmhjhn91gvbjhmps5lhz3bbm7wh77726l")))

(define-public crate-jqdata-model-0.3.3 (c (n "jqdata-model") (v "0.3.3") (d (list (d (n "bigdecimal") (r "= 0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "084pmb30vbp4mrdi7mh2whc1cdj3rs8gf7fg9zskih3qb3d8md8w")))

