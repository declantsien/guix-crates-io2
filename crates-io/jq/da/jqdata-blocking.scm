(define-module (crates-io jq da jqdata-blocking) #:use-module (crates-io))

(define-public crate-jqdata-blocking-0.1.0 (c (n "jqdata-blocking") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16zgx0v66g8mprs0q7h4hksj8k2927gjixmfarwhn4cd472m5by0")))

(define-public crate-jqdata-blocking-0.1.1 (c (n "jqdata-blocking") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "jqdata-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "177lncdkwpw1h5njfkp63zliyqcy8pm6iyajr3nmc5y9yvyi5gy2")))

