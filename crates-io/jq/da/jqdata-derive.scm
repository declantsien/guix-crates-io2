(define-module (crates-io jq da jqdata-derive) #:use-module (crates-io))

(define-public crate-jqdata-derive-0.1.0 (c (n "jqdata-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "035311br1g959y3gwsvkkszizic9db9v5jb6dyxjgys6zg9lqn8b")))

(define-public crate-jqdata-derive-0.2.0 (c (n "jqdata-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pbwybfhgnwllp0wvrrgsxbywadx8hl5kwq816c1yqml7p4alp59")))

