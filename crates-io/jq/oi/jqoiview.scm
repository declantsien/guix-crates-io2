(define-module (crates-io jq oi jqoiview) #:use-module (crates-io))

(define-public crate-jqoiview-0.1.0 (c (n "jqoiview") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "100n3pkj4yfq84n531wscymbj23psc8x3wxmdry4kdpvikvz9kv3")))

(define-public crate-jqoiview-0.2.0 (c (n "jqoiview") (v "0.2.0") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0cw4ry13rj3789qwzy70mny8c3lzqqm4vwzafhzc2il7xplpmimn")))

(define-public crate-jqoiview-0.3.0 (c (n "jqoiview") (v "0.3.0") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "1cqh5wm34kswsz0r34wwaa420qi6lp53xdy65zqsqhpf66ys43pn")))

(define-public crate-jqoiview-0.3.1 (c (n "jqoiview") (v "0.3.1") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0q81fdcidwlsgpdqqg3b02j3571bbknmyxd8ch7nhb0a6kf868f6")))

(define-public crate-jqoiview-0.3.2 (c (n "jqoiview") (v "0.3.2") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "1bran4495alvinxl70ky7d40w3sicxfr8j2ypvq567bv36hi3izm")))

(define-public crate-jqoiview-0.4.0 (c (n "jqoiview") (v "0.4.0") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "17dwaj3vxz0s9ifpfcq29svpalyz6j09bj7qacjka38yvpjdkgdj")))

(define-public crate-jqoiview-0.5.0-alpha (c (n "jqoiview") (v "0.5.0-alpha") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0lcv1vn9af3jqgvd45c9ya876wk39ih3cnd9vk4l26kf3awq3mb5")))

(define-public crate-jqoiview-0.5.0 (c (n "jqoiview") (v "0.5.0") (d (list (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "1yjjr9npdhxbg3lnpa0bfba9w1hh63wkkinxpqx0nmb3hj068ysk")))

(define-public crate-jqoiview-0.5.1 (c (n "jqoiview") (v "0.5.1") (d (list (d (n "sdl2") (r "^0.35") (f (quote ("bundled"))) (k 0)))) (h "160sqhc1bxyk70m7kz2mxzybjyws320qqy6rh7bfmwkld41c1qmf") (y #t)))

(define-public crate-jqoiview-0.5.2 (c (n "jqoiview") (v "0.5.2") (d (list (d (n "sdl2") (r "^0.35") (f (quote ("bundled"))) (k 0)))) (h "1wv6azbkgck2qrihbp35ghna54na7yv8sn9wi87mib4pkcqvdh57")))

