(define-module (crates-io om p- omp-codegen) #:use-module (crates-io))

(define-public crate-omp-codegen-0.1.0 (c (n "omp-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15zmyn4d2f2fbb7fhdzj1xdjv4fr9pazzh2jmdfk0c14pcba8k9v")))

(define-public crate-omp-codegen-0.2.0 (c (n "omp-codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yfjzd1w99sc05iv5ciwjnva42k3f4cbhk9jzfh9smmx3q8hkir6")))

