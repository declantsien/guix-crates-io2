(define-module (crates-io om p- omp-gdk) #:use-module (crates-io))

(define-public crate-omp-gdk-0.1.0 (c (n "omp-gdk") (v "0.1.0") (d (list (d (n "omp-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b893bknkh4bjzkxrmmbwbckd315grw52wyl0ajpdf0c7xvslrlp")))

(define-public crate-omp-gdk-0.2.0 (c (n "omp-gdk") (v "0.2.0") (d (list (d (n "omp-codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gdafk1lkllw6jysyc2zynaagvsqwgpkdw91fca411fx5srb2n3y")))

