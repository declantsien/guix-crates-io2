(define-module (crates-io om ag omage) #:use-module (crates-io))

(define-public crate-omage-0.1.0 (c (n "omage") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1jkdxda18ml0d7imaivq322iphn27a32fn1dlrhgznb9acfqnjvm") (y #t)))

(define-public crate-omage-0.2.0 (c (n "omage") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0bsxdj8bmc9nl8s342r9rgdbf9ydfnvhdv0mg96g6c9kajf8hkag") (y #t)))

(define-public crate-omage-0.2.1 (c (n "omage") (v "0.2.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0bbs3ii06byx5wk3cvdxyr53mhkciaq5iiwnnx4mrhyx16yj1w1s") (y #t)))

(define-public crate-omage-0.2.2 (c (n "omage") (v "0.2.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1m6x7mdf1if7w4z4lnyr089lz55cfbybp2xbb69av185s1kx4ghb") (y #t)))

(define-public crate-omage-0.2.3 (c (n "omage") (v "0.2.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1lazxvz1jjv6gdmq4yksa6jbdvrrky748dgx50vkk3b6kkrczxn5") (y #t)))

(define-public crate-omage-0.2.4 (c (n "omage") (v "0.2.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1car2yzpws8r371bfks9pghiwqznqxpjb95yvi842gz0snhv15wy")))

(define-public crate-omage-0.2.5 (c (n "omage") (v "0.2.5") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "14iy014gsikbv0qr9zzdqmhkl10qwm4b5982c80haydn9hfk2w40")))

(define-public crate-omage-0.2.6 (c (n "omage") (v "0.2.6") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0v7f7k403qy9qv5hpl3zk5hg6y17d0p07kifrdg9w4n2fcnygda4")))

(define-public crate-omage-0.3.0 (c (n "omage") (v "0.3.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "19zr65h0bz2wphl14a5y0jxim12ibkwf7malarr81pw83h2xbpsn")))

(define-public crate-omage-0.3.1 (c (n "omage") (v "0.3.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1chw6yn0yk2qwy157c8dp7xr9liim0v8c9s3hbk0lwz2nqjlzflk")))

(define-public crate-omage-0.3.11 (c (n "omage") (v "0.3.11") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0bx6bp3avbh2scpia89cqxla23vvlafchc260s7if6fwiscvv2r5")))

