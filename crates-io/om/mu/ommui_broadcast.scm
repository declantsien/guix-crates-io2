(define-module (crates-io om mu ommui_broadcast) #:use-module (crates-io))

(define-public crate-ommui_broadcast-0.1.0 (c (n "ommui_broadcast") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1jd79s4y15xr8m0kpzgn8zdvlkg5095w3icd4hvj8ywzd4cxpgkp")))

(define-public crate-ommui_broadcast-0.2.0 (c (n "ommui_broadcast") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1nb3djgbs862z2ryc3r0f668z4ll0ycy6bnhz3kjyfy3pyw03kzr")))

(define-public crate-ommui_broadcast-0.2.1 (c (n "ommui_broadcast") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "04shw3b8vx2ypi6nqfpvr82r2czc3ccli7wiadn4sgbrckxchci8")))

(define-public crate-ommui_broadcast-0.2.2 (c (n "ommui_broadcast") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1a03rg1sb4scbbp7nl8b5sz6y5hkr7fasdjsfa8pv62vw48pdlng")))

(define-public crate-ommui_broadcast-0.2.3 (c (n "ommui_broadcast") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "14d941nqzkybzagjjhyshx4h38qnsjlj3jvyz94jcjz9f27xklc6")))

(define-public crate-ommui_broadcast-0.3.0 (c (n "ommui_broadcast") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)))) (h "0slajf96f52ykzr2l3zyx0x2r3l55kpb0l7ixxp5id4ngxj2zv2h")))

(define-public crate-ommui_broadcast-0.4.0 (c (n "ommui_broadcast") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)))) (h "1vs7vw192xq5xn08m2vchw24srx84za0y2ybiygd2v1wl7lky17m")))

