(define-module (crates-io om mu ommui_string_patterns) #:use-module (crates-io))

(define-public crate-ommui_string_patterns-0.1.0 (c (n "ommui_string_patterns") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "18g0m03i6vm6vnc46n0i3952lpmsxv4azj2zziy9p0gjqqavdbz9")))

(define-public crate-ommui_string_patterns-0.1.1 (c (n "ommui_string_patterns") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "162l22p1mbrkiywfqq3z4z90gv25jmlmxd83zpb4g94w4302jbw1")))

(define-public crate-ommui_string_patterns-0.1.2 (c (n "ommui_string_patterns") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f20w0pd5sk83c9zxd4pz9zly780vw84v5nqjj5p7cwgdn0shscz")))

