(define-module (crates-io om uc omuchat-tauri-plugin-server) #:use-module (crates-io))

(define-public crate-omuchat-tauri-plugin-server-0.1.0 (c (n "omuchat-tauri-plugin-server") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "06rvayjjyci8k7l4hi6w49r487hi25kfmr86s8f0fqwg7fyrx1za") (r "1.64")))

