(define-module (crates-io om db omdbrs) #:use-module (crates-io))

(define-public crate-omdbrs-1.0.0 (c (n "omdbrs") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0zd1i8g83nwm89izq5fm1bw3kkiwrga701rby3ni4havl3abvk69")))

(define-public crate-omdbrs-2.0.0 (c (n "omdbrs") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1qkdjqfp3k17fl5fs0dw3yz7p8m3wqw89ch91l8v8dc262r4q63g")))

(define-public crate-omdbrs-2.1.0 (c (n "omdbrs") (v "2.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "099c63dk8747bxdx8njjps17vikzi0pqy167w65ydpfv1abggmw8")))

(define-public crate-omdbrs-2.2.0 (c (n "omdbrs") (v "2.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "02v43z9j4ivjrnm19csc9qkk8iw2hai1gmf8lh3a154n13h3sb8x")))

(define-public crate-omdbrs-2.3.0 (c (n "omdbrs") (v "2.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "11xa184p6s309zlq0kijhr1lwv3z9hhqj7xl8gisx80vgfr842ib")))

(define-public crate-omdbrs-2.3.1 (c (n "omdbrs") (v "2.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1cvws4svdasf3zhqi82x3zzq60h4nf6vn1bqfk6km1fprxlfa872")))

(define-public crate-omdbrs-2.4.0 (c (n "omdbrs") (v "2.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1mqw513ixwxyx99jqfsxpjwxdk4sq7wqfpaaj9gh6xlwfx706c01")))

(define-public crate-omdbrs-2.5.0 (c (n "omdbrs") (v "2.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0z2kpnaz72xs35q7xf6zxaxis2zf83lbq7hpflqj7ia8jns71jah")))

