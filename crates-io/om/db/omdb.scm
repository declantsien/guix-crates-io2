(define-module (crates-io om db omdb) #:use-module (crates-io))

(define-public crate-omdb-0.1.0 (c (n "omdb") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.10") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)))) (h "1qhfwpyps8nm7m5n28330a6i3gjq9iz23h5p6p7q5w959yp0rczh")))

(define-public crate-omdb-0.1.1 (c (n "omdb") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.10") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)))) (h "0gipyl65prcxpfzilgzbfjk6ph63lpc5ljk19q8ivd72w9k0isz5")))

(define-public crate-omdb-0.1.2 (c (n "omdb") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "06xs6xaamfdm5j3kzqj99v72rbni1madhgs13z30d1xc4ak18bfy")))

(define-public crate-omdb-0.2.1 (c (n "omdb") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lyxz61lqrc7n0kwib6dl7hcnyshfv1m752xscx937a6dn01z48f")))

(define-public crate-omdb-0.3.1 (c (n "omdb") (v "0.3.1") (d (list (d (n "reqwest") (r "~0.10") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "08lf1krb13vdxc32w49gv1l0hvmay2pc0fvala8sm0s0hrs8xh15")))

(define-public crate-omdb-0.3.2 (c (n "omdb") (v "0.3.2") (d (list (d (n "reqwest") (r "~0.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "~1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "11kh8l2d71nz129jp00aa9hymrnw9aljvm8k1bsisj51dla98nq8")))

