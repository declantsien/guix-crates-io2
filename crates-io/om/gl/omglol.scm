(define-module (crates-io om gl omglol) #:use-module (crates-io))

(define-public crate-omglol-0.0.1 (c (n "omglol") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "email_address") (r "^0.2.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmbq4vajpfw89gylj4fd3av81vysbd1z12qm046ncjdykkzy8vp")))

