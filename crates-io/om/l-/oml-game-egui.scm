(define-module (crates-io om l- oml-game-egui) #:use-module (crates-io))

(define-public crate-oml-game-egui-0.37.0-dev (c (n "oml-game-egui") (v "0.37.0-dev") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "egui") (r "^0.20.1") (f (quote ("tracing"))) (d #t) (k 0)) (d (n "oml-game") (r "^0.37.0-alpha") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1s4mmb2kgjl9327x5al6glwgipy73kaphjlb2y2hv7fhay2xs5ka")))

(define-public crate-oml-game-egui-0.37.0-alpha (c (n "oml-game-egui") (v "0.37.0-alpha") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "egui") (r "^0.20.1") (f (quote ("tracing"))) (d #t) (k 0)) (d (n "oml-game") (r "^0.37.0-alpha") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1gss24imsghkx36br3h4k678gs9728f4prmqyx70wxqjgpgaiz97")))

