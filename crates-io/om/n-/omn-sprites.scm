(define-module (crates-io om n- omn-sprites) #:use-module (crates-io))

(define-public crate-omn-sprites-0.1.0 (c (n "omn-sprites") (v "0.1.0") (d (list (d (n "ggez") (r "^0.3") (f (quote ("cargo-resource-root"))) (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1l7jlr6g69cxiqi9qjlqhr7qny6y24r6smx98a42znhg43qws116") (f (quote (("default"))))))

