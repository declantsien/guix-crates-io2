(define-module (crates-io om a- oma-pm-operation-type) #:use-module (crates-io))

(define-public crate-oma-pm-operation-type-0.1.0 (c (n "oma-pm-operation-type") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "oma-utils") (r "^0.5.0") (f (quote ("human-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18sadf323x2g819ninjsbviq2i9ls9yyykdqpa16izi0fgj14vy8")))

(define-public crate-oma-pm-operation-type-0.1.1 (c (n "oma-pm-operation-type") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "oma-utils") (r "^0.6.0") (f (quote ("human-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q170plh87yydjzyaraxqf2sgwak3s1npxpmnnlzp9lmg0dfrh6k")))

(define-public crate-oma-pm-operation-type-0.1.2 (c (n "oma-pm-operation-type") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "oma-utils") (r "^0.7.0") (f (quote ("human-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qkrnh0kwjrxyryx2l8vmhf4rpr12rk90pm38sis9vbifhk6xhp1")))

(define-public crate-oma-pm-operation-type-0.1.3 (c (n "oma-pm-operation-type") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "oma-utils") (r "^0.7.0") (f (quote ("human-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11s1g5kmi8kkh7rzk1m7vx66inl3ci90050blb4dvbhn7cgfh0wz")))

(define-public crate-oma-pm-operation-type-0.1.4 (c (n "oma-pm-operation-type") (v "0.1.4") (d (list (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "oma-utils") (r "^0.6.0") (f (quote ("human-bytes"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ihm2h8aw8f40pdi1jiw1x7qn1a07kvi2pbfgv0xdcrm3kb3fjx")))

