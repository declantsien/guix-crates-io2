(define-module (crates-io om a- oma-debcontrol) #:use-module (crates-io))

(define-public crate-oma-debcontrol-0.3.0 (c (n "oma-debcontrol") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.3") (d #t) (k 2)) (d (n "json") (r "^0.12.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (k 0)))) (h "16ykwg9xfdsi8m9l3j9ri8flqcsj7z8ib8ajxc1nwaycb3psdj71") (f (quote (("verbose-errors") ("std" "nom/std") ("default" "std" "verbose-errors"))))))

(define-public crate-oma-debcontrol-0.3.1 (c (n "oma-debcontrol") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 2)) (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.3") (d #t) (k 2)) (d (n "json") (r "^0.12.1") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (k 0)))) (h "01n1smg0v0pvshznwrrhq14r88k6rjjxj0jg3dlpkrp71i21rrxx") (f (quote (("verbose-errors") ("std" "nom/std") ("default" "std" "verbose-errors"))))))

