(define-module (crates-io om a- oma-apt-sources-lists) #:use-module (crates-io))

(define-public crate-oma-apt-sources-lists-0.1.0 (c (n "oma-apt-sources-lists") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.3") (d #t) (k 0)))) (h "0wls16pw9qxq4z7kjp5xf5sd9xf7v5i10nnz487dllwnz8k7izpj")))

(define-public crate-oma-apt-sources-lists-0.2.0 (c (n "oma-apt-sources-lists") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.3") (d #t) (k 0)))) (h "1mzzab8crsvisldmpf8zzi6xbqrssr7q794w72iym5kdn8h7n09q")))

(define-public crate-oma-apt-sources-lists-0.3.0 (c (n "oma-apt-sources-lists") (v "0.3.0") (d (list (d (n "err-derive") (r "^0.3") (d #t) (k 0)) (d (n "oma-debcontrol") (r "^0.3") (d #t) (k 0)))) (h "1n9dnf7ymbgjhzxcx0j10awpxvbm6piy8798ih9y977qdc86l42c")))

(define-public crate-oma-apt-sources-lists-0.3.1 (c (n "oma-apt-sources-lists") (v "0.3.1") (d (list (d (n "err-derive") (r "^0.3") (d #t) (k 0)) (d (n "oma-debcontrol") (r "^0.3") (d #t) (k 0)))) (h "0r40qh5ki68hrppwp1n5zgad6wvkq3bxg1aa8ndfsp6yg80mdi88")))

