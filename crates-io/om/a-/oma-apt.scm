(define-module (crates-io om a- oma-apt) #:use-module (crates-io))

(define-public crate-oma-apt-0.1.1 (c (n "oma-apt") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.5") (d #t) (k 0)))) (h "0a125l1gdw63l70q283987gkbbxpjpx7fdpxw5clz2zkasc8mkjh")))

(define-public crate-oma-apt-0.1.2 (c (n "oma-apt") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.5") (d #t) (k 0)))) (h "1yk7ajzjkfbv6h1994hzab4acv6bz7yib2q9wihabvvdg2yzm531")))

(define-public crate-oma-apt-0.2.0 (c (n "oma-apt") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "12av9zjsj6cla7qnlvrk715v77phdymxk6jps8s8s377v6a373bc")))

(define-public crate-oma-apt-0.2.1 (c (n "oma-apt") (v "0.2.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1nl60py6sz95qnxy7jvpp2igxgbm7cm3glwmxj3l80gqfq1zgnxy")))

(define-public crate-oma-apt-0.3.0 (c (n "oma-apt") (v "0.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "101fdb83rrcp0ccfk9jddlxlzqp4zg1kjxp1ggy63r6w2543152x")))

(define-public crate-oma-apt-0.3.1 (c (n "oma-apt") (v "0.3.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "04bfs6pz1dlmzl5hg8z35civb83cwp0gln82r7c0vy6fybkm2rc3")))

(define-public crate-oma-apt-0.3.2 (c (n "oma-apt") (v "0.3.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "17kd39xp558xsq3yhbmnfb0wrrk4rgr8yavllsdl4gj3hvl00k1r")))

(define-public crate-oma-apt-0.4.0 (c (n "oma-apt") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "04vymllfa5qqa351n908zhz79srnkkgh2d9zvy4fhlsnk99lzz8n")))

(define-public crate-oma-apt-0.4.1 (c (n "oma-apt") (v "0.4.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0wg7ar94cc0kawb625dirwm9w72f6phkza31kngnn8kkmv0f45xq")))

(define-public crate-oma-apt-0.5.0 (c (n "oma-apt") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "0r6jx1ahrq6ynn6yihsdhmsgcnyn9ib1f9lh5wqmi3ba8bzl3lhq")))

