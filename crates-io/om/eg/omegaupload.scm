(define-module (crates-io om eg omegaupload) #:use-module (crates-io))

(define-public crate-omegaupload-0.1.0 (c (n "omegaupload") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "omegaupload-common") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)) (d (n "rpassword") (r "^5") (d #t) (k 0)))) (h "0c7fz2mj0ard9qbpq2i6m390lr1iskngcvv82yaywlhw7vlm5vgd")))

(define-public crate-omegaupload-0.1.1 (c (n "omegaupload") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "omegaupload-common") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)) (d (n "rpassword") (r "^5") (d #t) (k 0)))) (h "0slpzsxcxj2ggkn55m20ksc34z2wa95x1g6i7y3aljlxi7m3577c")))

