(define-module (crates-io om eg omegalul) #:use-module (crates-io))

(define-public crate-omegalul-0.1.0 (c (n "omegalul") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12kp4wi592zq25adg6zn1944ahgh0ywczzdp3ykk4vj787fkpidc")))

(define-public crate-omegalul-0.1.1 (c (n "omegalul") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18j964x5v5mb6gxcbya9xpcijbsqsxkxjrbbj5pbl2bfmydy38n5")))

(define-public crate-omegalul-0.1.2 (c (n "omegalul") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1skf6is2k3bylijppv9907clr1vlmmj3qgiz9f8vl6hx9rin3i6x")))

(define-public crate-omegalul-0.1.3 (c (n "omegalul") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pqjklrfjz3izlza5kwf7jpcpx9ilg2gjzi4vjngbvflpbdlkwz4")))

(define-public crate-omegalul-0.1.4 (c (n "omegalul") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19c5jrhqldms5xlgpvvviy6lygvhdcr0lv79msqfp42c03l8fnpy")))

