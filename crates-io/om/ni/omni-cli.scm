(define-module (crates-io om ni omni-cli) #:use-module (crates-io))

(define-public crate-omni-cli-0.1.0 (c (n "omni-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "erased-serde") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1a5rmdf7bniw9x6r9ili3m078b8x68vynx6m6ssk88rk3fg1wn37")))

