(define-module (crates-io om ni omni) #:use-module (crates-io))

(define-public crate-omni-0.0.0 (c (n "omni") (v "0.0.0") (h "1ax3q0lh74lshfjc3hk99hgk92g6m2w2nh000845hq7qnpp4inlb")))

(define-public crate-omni-0.0.1 (c (n "omni") (v "0.0.1") (h "1x2qm9wmna5p1fspa8w43wh5my4jhi68jnv75k396wmgss4p3qd4")))

