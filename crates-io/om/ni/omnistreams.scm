(define-module (crates-io om ni omnistreams) #:use-module (crates-io))

(define-public crate-omnistreams-0.1.0 (c (n "omnistreams") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "websocket") (r "^0.22") (d #t) (k 0)))) (h "01xxp6jp0iz1hqahayipdy0jjcji5lkgy9yc3iw39aq93j8b763w")))

