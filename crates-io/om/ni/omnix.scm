(define-module (crates-io om ni omnix) #:use-module (crates-io))

(define-public crate-omnix-0.0.0 (c (n "omnix") (v "0.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "parsing" "serde"))) (d #t) (k 0)))) (h "0xys62xrlap6i3s80fbdp57xzlsklqw4qsz54gv49hqywma86g9w") (y #t)))

(define-public crate-omnix-0.0.1 (c (n "omnix") (v "0.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "parsing" "serde"))) (d #t) (k 0)))) (h "0xdb0j7ybyl5m229cfls53qy9iy01sxlrnxhradcakxn87jwg63d") (y #t)))

