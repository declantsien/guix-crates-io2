(define-module (crates-io om ni omnipaxos_ui) #:use-module (crates-io))

(define-public crate-omnipaxos_ui-0.1.0 (c (n "omnipaxos_ui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "omnipaxos") (r "^0.2.2") (f (quote ("logging"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (d #t) (k 0)) (d (n "tui-logger") (r "^0.9.6") (f (quote ("slog-support" "ratatui-support"))) (k 0)))) (h "0izn6lbljnnfw9hx8qnsw783dbpx9h0gydgm2xb5q3jkh15xnr21")))

