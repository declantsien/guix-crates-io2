(define-module (crates-io om ni omnipaxos_macros) #:use-module (crates-io))

(define-public crate-omnipaxos_macros-0.1.0 (c (n "omnipaxos_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1imlprafi50xpz4w22d6s6i66d069ahx5gcyh753pw93fm9g9ryg")))

(define-public crate-omnipaxos_macros-0.1.1 (c (n "omnipaxos_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k74mbc78a2jcg96813djqkjaqyah5858qq9nz720ywqydc6i9x9")))

(define-public crate-omnipaxos_macros-0.1.2 (c (n "omnipaxos_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19f2hgqd7m2x9qxdgabchc8ivja3yzy4xhrwp5krhvad8xkc92cq")))

(define-public crate-omnipaxos_macros-0.1.3 (c (n "omnipaxos_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04rhvzzsp8pxsagvlp2yw14bzgazj110h1qnbbqrrdp56cp2129s")))

