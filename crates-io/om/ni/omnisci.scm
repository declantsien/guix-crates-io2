(define-module (crates-io om ni omnisci) #:use-module (crates-io))

(define-public crate-omnisci-0.1.0 (c (n "omnisci") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "thrift") (r "^0.13.0") (d #t) (k 0)) (d (n "try_from") (r "^0.3.2") (d #t) (k 0)))) (h "0vsndasb2kzn8c6sdcsq77pqp9ajj7zc6kpp4d3vmgixg3kafz5p")))

