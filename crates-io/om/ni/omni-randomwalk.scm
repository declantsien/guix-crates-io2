(define-module (crates-io om ni omni-randomwalk) #:use-module (crates-io))

(define-public crate-omni-randomwalk-0.0.1 (c (n "omni-randomwalk") (v "0.0.1") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01zh88li9pb30scn6493jkq2pyvnig36jswh34iq0x4rpx98k6b4")))

(define-public crate-omni-randomwalk-0.0.2 (c (n "omni-randomwalk") (v "0.0.2") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rwd11s5f2m2jp0rjfyb79izqhyf4biwkmsxq7dnqvla6zqzjq86")))

(define-public crate-omni-randomwalk-0.0.3 (c (n "omni-randomwalk") (v "0.0.3") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xarh26bl8r316pipga0bahdv1himlb6ym1ni5683m35cm8h9ip7")))

