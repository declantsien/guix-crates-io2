(define-module (crates-io om ni omniverse) #:use-module (crates-io))

(define-public crate-omniverse-0.1.0 (c (n "omniverse") (v "0.1.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "omniverse_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d58fa8y3kl6yxgxpn2ws0ymv8ygnz1k02370pqhn8cbnk70dgdw") (f (quote (("vendored" "omniverse_sys/vendored") ("default" "vendored")))) (r "1.70")))

(define-public crate-omniverse-0.1.1 (c (n "omniverse") (v "0.1.1") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "omniverse_sys") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s720virqrbr0f1d3022x651lw51r00p3qpm50z847w9p1qj0hax") (f (quote (("vendored" "omniverse_sys/vendored") ("default" "vendored")))) (r "1.70")))

(define-public crate-omniverse-0.2.0 (c (n "omniverse") (v "0.2.0") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "omniverse_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0agm8w9gbbrh6s9fs455l0gwq8kvsljprc767d3vv248cvw46cs8") (f (quote (("vendored" "omniverse_sys/vendored") ("default" "vendored")))) (r "1.70")))

