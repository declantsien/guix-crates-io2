(define-module (crates-io om ni omnis-vanitas-web) #:use-module (crates-io))

(define-public crate-omnis-vanitas-web-0.0.1 (c (n "omnis-vanitas-web") (v "0.0.1") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ximidd6yvvb8pglyjxcbj17ddbchqwrj6bz6cal51fk11r3h89s") (y #t)))

