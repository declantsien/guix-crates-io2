(define-module (crates-io om ni omnicopy_to_output) #:use-module (crates-io))

(define-public crate-omnicopy_to_output-0.1.0 (c (n "omnicopy_to_output") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "build-target") (r "^0.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "0fb5nvgi8ark3b00nrnsza483r1w2mh4ldqrasbqck2i094rsssq")))

(define-public crate-omnicopy_to_output-0.1.1 (c (n "omnicopy_to_output") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "build-target") (r "^0.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "14r3gwhk2j81dgw23d2nnb5n5gl3xm8isc4pm4bc8mingk8g9bqh")))

(define-public crate-omnicopy_to_output-0.2.0 (c (n "omnicopy_to_output") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "build-target") (r "^0.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "05w7pmnx2zp1wf69nvjhzj8zkdhz6g9kp6ghjkdjynhdzf85dqrx")))

