(define-module (crates-io om ni omnipath) #:use-module (crates-io))

(define-public crate-omnipath-0.0.0 (c (n "omnipath") (v "0.0.0") (h "13dxdndx0wyf4dwxg8j197wmml5sdr76kgw0jhy5krbchr603pzv")))

(define-public crate-omnipath-0.1.0 (c (n "omnipath") (v "0.1.0") (h "109ymrx8w23hvswqq2hqilm59mjfzv6hnxjg7hszpdsqba1ac71f") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.1 (c (n "omnipath") (v "0.1.1") (h "0v2pxyqc0cgwd5ipnwrd9z3rwinhivsigp7frlzxx6xcqmc1hip7") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.2 (c (n "omnipath") (v "0.1.2") (h "0qykrqvsq12rd27yj4ld0xnqbkmv9xhb96jb7qlg5sq7mi3b1xsr") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.3 (c (n "omnipath") (v "0.1.3") (h "0rdaqacifhb95wbpb563b5ili1l0f0csc6z2xa5gwxpfgl0avvpq") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.4 (c (n "omnipath") (v "0.1.4") (h "0misvbmi5f8jlw0w0drfb703gc2ha3p0x5hhkmw581smjyjid26d") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.5 (c (n "omnipath") (v "0.1.5") (h "1dkillrpd1md45kx2c89zqkjiyahvf2j8igx3sjp9fg9j8b2whv8") (f (quote (("std") ("default" "std"))))))

(define-public crate-omnipath-0.1.6 (c (n "omnipath") (v "0.1.6") (h "0xd5a4xwsfmhzk59v6wz65f59rk16d7gvkg90w1qhb0jg08b7bc0") (f (quote (("std") ("default" "std"))))))

