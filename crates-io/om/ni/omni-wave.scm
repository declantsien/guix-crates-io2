(define-module (crates-io om ni omni-wave) #:use-module (crates-io))

(define-public crate-omni-wave-0.1.0 (c (n "omni-wave") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "1a2i88rk1a2za8hnqp618vmyz5j8ybp7b7gayp07dm44fp7pc63q")))

(define-public crate-omni-wave-0.2.1 (c (n "omni-wave") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0ib7vxij5ari5fgsgn7glznalk64pqcf03hg6h1vipaf71hk2a7b") (f (quote (("f64"))))))

