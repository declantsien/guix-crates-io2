(define-module (crates-io om ni omnical) #:use-module (crates-io))

(define-public crate-omnical-0.1.0 (c (n "omnical") (v "0.1.0") (h "1ssdwldcyicfxn47jf1bkqjpn5ds4lf8wffybjpy59hzmdnsxvbl")))

(define-public crate-omnical-0.2.0 (c (n "omnical") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "08c9lmwssvx2xa5pjmg7132dnqgqbflx2gbf0wcvry6a95ikpk3h")))

(define-public crate-omnical-0.9.0 (c (n "omnical") (v "0.9.0") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "19vflw01sf2q1azb6p81m7sh92a6grcxswh3kakf4w35vgrkxx4n")))

(define-public crate-omnical-0.9.9 (c (n "omnical") (v "0.9.9") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "08vgfjwx9mrdp7zhm248mrkbi6ljb9m51q960ykr2218fvqs0jr6")))

(define-public crate-omnical-0.9.10 (c (n "omnical") (v "0.9.10") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1cxjgd7vcgzj7603w4nbw66l5h5s037liiki17wq9ix7yjpba062")))

(define-public crate-omnical-0.9.11 (c (n "omnical") (v "0.9.11") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1q48npg7hwc3nh6acm5ncpbnx35j7d2cr4b7xm2iy51sz8mifvlj")))

(define-public crate-omnical-0.10.0 (c (n "omnical") (v "0.10.0") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1ps22fj5i5f32wvyr1i9haggssgqhid161148zdc02gpjsyphkkc")))

(define-public crate-omnical-0.10.1 (c (n "omnical") (v "0.10.1") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1l8bkj55sdhbadbvcx43zadz4mxww3in6f57r2n6s0cv94z2kjyn")))

(define-public crate-omnical-0.10.2 (c (n "omnical") (v "0.10.2") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "141sa3rvar4k5rnxynm22vp5218gbn9mpx2g89ln795i3d76ya0i")))

(define-public crate-omnical-0.10.3 (c (n "omnical") (v "0.10.3") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1jx4fpfq12snzmmyyi1dj6amd6cyv8fprv2vfv2x23nvymizli4v")))

(define-public crate-omnical-0.10.4 (c (n "omnical") (v "0.10.4") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0czkg891dqf2k261g5nrfdjyl72a4yc3h5hic4p0i5wg7c8jl621")))

(define-public crate-omnical-0.10.5 (c (n "omnical") (v "0.10.5") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chinese-lunisolar-calendar") (r "^0.2.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "nongli") (r "^0.1.1") (d #t) (k 2)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0j6jsqp1xdj61zb8i7nv2ysnjzamf72qy0cg7wmshbfm2l9bhisc")))

(define-public crate-omnical-0.10.6 (c (n "omnical") (v "0.10.6") (d (list (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chinese-lunisolar-calendar") (r "^0.2.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "nongli") (r "^0.1.1") (d #t) (k 2)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1hvnsdsa02zzifmfz8sp49irbj792j91fg9x2cma25nxdcxjycfj")))

