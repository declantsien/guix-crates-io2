(define-module (crates-io om ni omnibor-cli) #:use-module (crates-io))

(define-public crate-omnibor-cli-0.6.0 (c (n "omnibor-cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "async-walkdir") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "omnibor") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "io-std" "io-util" "macros" "rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1pkc17052za24jvg4xaws1581vybdd01yv5pwajlff9a5xwq6vvd")))

