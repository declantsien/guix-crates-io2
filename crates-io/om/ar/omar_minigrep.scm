(define-module (crates-io om ar omar_minigrep) #:use-module (crates-io))

(define-public crate-omar_minigrep-0.1.0 (c (n "omar_minigrep") (v "0.1.0") (h "0mrqkpif2c3sc44xbyyrhja0mcsd7igjxm4vimn7ddjqkb03c0pm") (y #t)))

(define-public crate-omar_minigrep-0.1.1 (c (n "omar_minigrep") (v "0.1.1") (h "1glvysp8sph7n9fz36ml60jxvf4hxk4j5la87gb5cbyphmsmsyig")))

