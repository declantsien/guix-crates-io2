(define-module (crates-io om g- omg-html) #:use-module (crates-io))

(define-public crate-omg-html-0.1.0 (c (n "omg-html") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "1sy18ga7fjpvv3crfigj9ccv0l5b9hwj9c6wr4z0g59k3g93ljbh")))

(define-public crate-omg-html-0.1.1 (c (n "omg-html") (v "0.1.1") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "0g6hmaijv6xm2wxljkw9jmxjcshxfyfndg1an6pjc9wy6wpcp0wa")))

