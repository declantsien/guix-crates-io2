(define-module (crates-io om g- omg-cool) #:use-module (crates-io))

(define-public crate-omg-cool-0.1.0 (c (n "omg-cool") (v "0.1.0") (d (list (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0qwvpmp3ph0icsla73kmwja6ijlb1xhaf505w7xj2x142ps57wql")))

(define-public crate-omg-cool-0.1.2 (c (n "omg-cool") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "11s5az5i1f0zbix40l3cl6zzjz8afdn262sw1c923d8ijyabhz1p")))

(define-public crate-omg-cool-0.1.3 (c (n "omg-cool") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0xlhkhb650dvd292x02yxbzxc0jb9mc9dqyihw62kk26v9978hp9")))

(define-public crate-omg-cool-0.1.4 (c (n "omg-cool") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "1prj407wmbjrasd9vra449gxm97lkvvxzw3jlg2w9bqxj96xbww8")))

