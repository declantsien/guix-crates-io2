(define-module (crates-io om g- omg-api) #:use-module (crates-io))

(define-public crate-omg-api-0.0.0 (c (n "omg-api") (v "0.0.0") (h "1izfmapj8x7gypaxdnr8hi41b4fhsaa1k74ai471x0lzixvdfbb0")))

(define-public crate-omg-api-0.1.0 (c (n "omg-api") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n0apfm8y579bfr1h5clh34p92yfd8lcy766m23g0drdmvg2jyjj")))

(define-public crate-omg-api-0.2.0 (c (n "omg-api") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "structstruck") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0vnw9b805qbg631p9b8vij7waha7rm3ls3y9r9ws8l9bb40nir9n")))

