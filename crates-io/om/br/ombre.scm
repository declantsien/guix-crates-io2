(define-module (crates-io om br ombre) #:use-module (crates-io))

(define-public crate-ombre-0.1.0 (c (n "ombre") (v "0.1.0") (d (list (d (n "glfw") (r "^0.54.0") (o #t) (d #t) (k 0)) (d (n "glow") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1di1y3z3qcagh6nq79z52q7pgmipxah8nif8z7g0hppfskfb4rpz") (f (quote (("default" "glfw" "color-logging") ("color-logging" "termcolor")))) (y #t) (r "1.74.0")))

(define-public crate-ombre-0.6.6 (c (n "ombre") (v "0.6.6") (d (list (d (n "glfw") (r "^0.54.0") (o #t) (d #t) (k 0)) (d (n "glow") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0gfzci9f2x31ls8j7g3rhwvqhfmc85hj1r54dvi1vwmbvd4cijp2") (f (quote (("default" "glfw" "color-logging") ("color-logging" "termcolor")))) (r "1.74.0")))

(define-public crate-ombre-0.6.7 (c (n "ombre") (v "0.6.7") (d (list (d (n "glfw") (r "^0.54.0") (o #t) (d #t) (k 0)) (d (n "glow") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "18sagcziqxrgy8zgaaynwlbwfdj9id1nk920k6q576vjk0p74j70") (f (quote (("default" "glfw" "color-logging") ("color-logging" "termcolor")))) (r "1.74.0")))

