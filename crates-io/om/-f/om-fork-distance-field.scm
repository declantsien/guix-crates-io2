(define-module (crates-io om -f om-fork-distance-field) #:use-module (crates-io))

(define-public crate-om-fork-distance-field-0.1.8 (c (n "om-fork-distance-field") (v "0.1.8") (d (list (d (n "docopt") (r "^1.1.1") (d #t) (k 2)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "spiral") (r "^0.1.9") (d #t) (k 0)))) (h "0l0nrqn9dkb9capcxid0ziqkdwpklqqnd1dywry7q0klk5yqf3k8")))

