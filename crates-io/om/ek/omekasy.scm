(define-module (crates-io om ek omekasy) #:use-module (crates-io))

(define-public crate-omekasy-0.1.0 (c (n "omekasy") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "11ihvz9ya9nccsmw5hlr0pm3pk3z8wbd22v9d292f05q2r6hz2s2")))

(define-public crate-omekasy-0.2.0 (c (n "omekasy") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "0rjfvqk8c3svv9rawv5kk7vwvagj4n989si5c5znri51kdg0ilsk")))

(define-public crate-omekasy-1.0.0 (c (n "omekasy") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "1phw4m77s68z6xgza5m9y4rmxmhv5vyzldb91199kvqaczbmn4fn")))

(define-public crate-omekasy-1.1.0 (c (n "omekasy") (v "1.1.0") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)))) (h "02964mmwhzxjbddc292gycbpchszxf5b82mkg7g329zjlzqvs6a9")))

(define-public crate-omekasy-1.1.1 (c (n "omekasy") (v "1.1.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "07246xs91z10bp5nkxkzr4hbid68gl52r1igpz654bmyacmpccb3") (f (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1.2.1 (c (n "omekasy") (v "1.2.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (o #t) (d #t) (k 0)))) (h "0px5f0cyy3lv2ndv5z3vpgq9nm9idsnz24vg2zbnxl8adhzny8jw") (f (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1.2.2 (c (n "omekasy") (v "1.2.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (o #t) (d #t) (k 0)))) (h "19ixn9s3fs4vgj65cpbf4sci8f940wp47zx6ir44cdcd33wcv9cg") (f (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1.2.3 (c (n "omekasy") (v "1.2.3") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (o #t) (d #t) (k 0)))) (h "0a1gim8qfcjgyz73z0c7ccqxd2w4fgfbz8vqlyji7wdw4xaapqyz") (f (quote (("default" "crossterm"))))))

