(define-module (crates-io om an omango-wyhash) #:use-module (crates-io))

(define-public crate-omango-wyhash-0.1.0 (c (n "omango-wyhash") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.0") (d #t) (k 0)))) (h "0vfcy7iifgbdbqrr65ag9r680f95j4hkc7bgz92q32agqgg86y47")))

(define-public crate-omango-wyhash-0.1.1 (c (n "omango-wyhash") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.0") (d #t) (k 0)))) (h "13zd8ww4501j73mn3imvj5vb3dnhwngrm2wz7yf2cdh7h218478z")))

(define-public crate-omango-wyhash-0.1.2 (c (n "omango-wyhash") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.0") (d #t) (k 0)))) (h "0r1pck58ba2hsnib22s88linfzh5f7v7y5rwwa6v3nwm0qs6xsmf")))

(define-public crate-omango-wyhash-0.1.3 (c (n "omango-wyhash") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.5") (d #t) (k 0)))) (h "0sldgiwswsfmk4f49cggcv8xqvr47jivbmfh133sl96iwk60qqyq")))

