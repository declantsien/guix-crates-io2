(define-module (crates-io om an omango-util) #:use-module (crates-io))

(define-public crate-omango-util-0.1.0 (c (n "omango-util") (v "0.1.0") (h "1g9h4bhk4ncqi5s08fmac44a0ak4jq6f72avs4sk8gxmma6spil1")))

(define-public crate-omango-util-0.1.1 (c (n "omango-util") (v "0.1.1") (h "14zsyq47bg9x2wpcbmh6vg81lmgiz4dafyajljkl9fn86fklhc5q")))

(define-public crate-omango-util-0.1.2 (c (n "omango-util") (v "0.1.2") (h "1jpg9pc2g2i8j57zvh3qmiv3j5gf3ln020x3nb4s9z18fi8q8xqk") (y #t)))

(define-public crate-omango-util-0.1.3 (c (n "omango-util") (v "0.1.3") (h "02n1wp9wij85n872vn1avch3djylp37advhs0bg88m31a71gsrpf")))

(define-public crate-omango-util-0.1.4 (c (n "omango-util") (v "0.1.4") (h "09x9i6d4zd7xixvk25w8z1908c3hcngc9sqi6ahzjf1nc7ryxinr")))

(define-public crate-omango-util-0.1.5 (c (n "omango-util") (v "0.1.5") (h "034c99az9zz2cds10is0v3daj314s63ia12lll9ma07pxh3pwxaf")))

