(define-module (crates-io om an omango-util-rs) #:use-module (crates-io))

(define-public crate-omango-util-rs-0.1.0 (c (n "omango-util-rs") (v "0.1.0") (h "0aqzgq6rydddg6q63s3q3qkla8q1cbm21gvhjhd8d776k2bdi5hl") (y #t)))

(define-public crate-omango-util-rs-0.1.1 (c (n "omango-util-rs") (v "0.1.1") (h "1gwm94fij1kjcvk3i0fm119j0a0af5svql3agj96dg7gv7xbx61h") (y #t)))

