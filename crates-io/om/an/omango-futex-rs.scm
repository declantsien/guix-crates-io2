(define-module (crates-io om an omango-futex-rs) #:use-module (crates-io))

(define-public crate-omango-futex-rs-0.1.0 (c (n "omango-futex-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dxwybna19l6m45b25ciawl1scn5z55jaxhdw9dm2i2axl7g1xzx") (y #t)))

(define-public crate-omango-futex-rs-0.1.1 (c (n "omango-futex-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10ilbr7mkf613qw6kjfvha2724w4rvpyrf1r51qq7r47bgcqyacs") (y #t)))

(define-public crate-omango-futex-rs-0.1.2 (c (n "omango-futex-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wmrjy7v6cv4q05aayvam1h05ls3gvjq9qv7wxxqbn68haxpd7v1") (y #t)))

