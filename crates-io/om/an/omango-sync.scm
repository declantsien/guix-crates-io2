(define-module (crates-io om an omango-sync) #:use-module (crates-io))

(define-public crate-omango-sync-0.1.0 (c (n "omango-sync") (v "0.1.0") (d (list (d (n "omango-futex") (r "^0.1.2") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.5") (d #t) (k 0)))) (h "02srdhccxfkqdmvz8mi5z7ig1wj5i97sc7scmdmfsrz22vpq1zk0") (y #t)))

(define-public crate-omango-sync-0.1.1 (c (n "omango-sync") (v "0.1.1") (d (list (d (n "omango-futex") (r "^0.1.2") (d #t) (k 0)) (d (n "omango-util") (r "^0.1.5") (d #t) (k 0)))) (h "1av28fwi0nwm6hhi7hg9sq8h7s1qfm5bv8gmibpzhzjk8wygwz7f") (y #t)))

