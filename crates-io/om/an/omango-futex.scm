(define-module (crates-io om an omango-futex) #:use-module (crates-io))

(define-public crate-omango-futex-0.1.0 (c (n "omango-futex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qlnlrh1gpa71jc7ziv3knhxvn885pzc2pnk07px9qg5c79hd0qz")))

(define-public crate-omango-futex-0.1.1 (c (n "omango-futex") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jmxrbqy2wf1nxl5s9dnq85lq246j20bi4r7jbfpwnpiylqgkdzs")))

(define-public crate-omango-futex-0.1.2 (c (n "omango-futex") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"freebsd\"))") (k 0)) (d (n "omango-util") (r "^0.1.5") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n32yl6bb5sm3fl1w28vc7lgfxynl5xlzaq646q4n722c23p37n1")))

