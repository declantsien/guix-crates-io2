(define-module (crates-io om g_ omg_serde) #:use-module (crates-io))

(define-public crate-omg_serde-0.1.0 (c (n "omg_serde") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "omg_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1rmxj0m99p3xa82j2awbvfv1vh9zsl0cqh57zqyy7yk5bsrqxcds")))

