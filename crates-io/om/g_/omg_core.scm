(define-module (crates-io om g_ omg_core) #:use-module (crates-io))

(define-public crate-omg_core-0.1.0 (c (n "omg_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noise") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "svgbobdoc") (r "^0.2") (d #t) (k 0)) (d (n "unwrap") (r "^1.2") (d #t) (k 0)))) (h "0fqrhj02g5zkwka007mcg6l647gx1rpbjhrygd1fxzp3m5a36w32")))

