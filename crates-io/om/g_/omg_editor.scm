(define-module (crates-io om g_ omg_editor) #:use-module (crates-io))

(define-public crate-omg_editor-0.1.0 (c (n "omg_editor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "omg_core") (r "^0.1") (d #t) (k 0)) (d (n "omg_serde") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.2") (f (quote ("tera"))) (d #t) (k 0)))) (h "0cnl205kd902s3f8dmadjc0lwk5l27ssmsqplnady08gq3ix9c2j")))

