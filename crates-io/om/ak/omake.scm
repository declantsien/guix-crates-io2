(define-module (crates-io om ak omake) #:use-module (crates-io))

(define-public crate-omake-0.1.0 (c (n "omake") (v "0.1.0") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r ">=0.2.26") (d #t) (k 0)))) (h "1ddajsv8yj4c8z195dkxcdisljcdbgdd90vaxgsdsh8qf03np032") (y #t)))

(define-public crate-omake-0.0.0 (c (n "omake") (v "0.0.0") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r ">=0.2.26") (d #t) (k 0)))) (h "15ij3zk39wr1aijac9jf1r8k86srrwkgv4h1zrvhfiwnqapq52qw") (f (quote (("omake") ("make") ("default" "make")))) (y #t)))

(define-public crate-omake-0.1.1 (c (n "omake") (v "0.1.1") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r ">=0.2.26") (d #t) (k 0)))) (h "1cnbyp818ypdqpk2v4k7vk99rmln9n6sf1jfxj0khglkwsr0s7mn")))

(define-public crate-omake-0.1.2 (c (n "omake") (v "0.1.2") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r ">=0.2.26") (d #t) (k 0)))) (h "0b4sg72b6ii6yia2isk828abrc4y25xn214jzk923bzd2c8s7si2")))

(define-public crate-omake-0.1.3 (c (n "omake") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)))) (h "0pz66abp9wllz6g9bvr10fngixn28ly4mzakj8by2ickz6ac2rrq")))

(define-public crate-omake-0.1.4 (c (n "omake") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "const_format") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ckxplajw48ly9fbhr0gh9f2cj4nchq5ghgiigjpqwc475absk6p") (f (quote (("default" "bin") ("bin" "clap" "const_format"))))))

(define-public crate-omake-0.1.5 (c (n "omake") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)))) (h "0krxadavm9ylx0pkm28wm8ap66ffaqw89wxr9bms491a0f51x899")))

(define-public crate-omake-0.1.6 (c (n "omake") (v "0.1.6") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1sr4xmpvncsdfawmlmbp1p1dhcqg8z2mxd3ka0264y7i910mwfqy")))

