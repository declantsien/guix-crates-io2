(define-module (crates-io cz l- czl-hello) #:use-module (crates-io))

(define-public crate-czl-hello-0.1.0 (c (n "czl-hello") (v "0.1.0") (h "0ywapg157nv2vfj3gc7vlsvh06b490vqsbv4h8m00dhh615yknpl")))

(define-public crate-czl-hello-0.1.1 (c (n "czl-hello") (v "0.1.1") (h "09x3629fc1cx0qwhb68fd7hpkgmr99c9iag1f1wg2lppvqzy3jjc")))

