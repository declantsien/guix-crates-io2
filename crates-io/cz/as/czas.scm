(define-module (crates-io cz as czas) #:use-module (crates-io))

(define-public crate-czas-0.0.1 (c (n "czas") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "122ccn5qd48xrwl8l64qb9gk8pybaczirbf4c7sbi2x9fawa4xbq")))

(define-public crate-czas-0.0.2 (c (n "czas") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0g7knn47bkcz17pf0bhbj9n103asbgkkkdh15hldb7j9mavh4kdk")))

(define-public crate-czas-0.0.3 (c (n "czas") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lga4fkcqv4k12kz9bk3ilmgivdaqhmy1hl4a35vs4zxj7f2d9n5")))

