(define-module (crates-io cz mq czmq) #:use-module (crates-io))

(define-public crate-czmq-0.1.0 (c (n "czmq") (v "0.1.0") (d (list (d (n "bitflags") (r "0.5.*") (d #t) (k 0)) (d (n "czmq-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "2.1.*") (d #t) (k 2)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "0iks84x1j78zqx46lmhdklyhvqa4vg012c539l0r6npjkvsca8lr") (f (quote (("draft"))))))

