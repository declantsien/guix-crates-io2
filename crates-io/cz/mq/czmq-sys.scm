(define-module (crates-io cz mq czmq-sys) #:use-module (crates-io))

(define-public crate-czmq-sys-0.0.1 (c (n "czmq-sys") (v "0.0.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1rxbqyn2jf2pbiszrlb27bxwyynww1vhy07jzdrr4dy5dd8mnw0l")))

(define-public crate-czmq-sys-0.0.2 (c (n "czmq-sys") (v "0.0.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1d6791lh47rxjvqpm5hgsc6ww3r8q7lvv759wzfbm1c1y8zfhx7j")))

(define-public crate-czmq-sys-0.0.3 (c (n "czmq-sys") (v "0.0.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0p8y79pwf1gw6ajg4wq30kdfclbbx95h077pisi71n9l48k3www6")))

(define-public crate-czmq-sys-0.1.0 (c (n "czmq-sys") (v "0.1.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "16badc5210146iydhd1lcw2rw24vy89lzxy2vnazh8bxby1jix8k")))

