(define-module (crates-io cz ka czkawka_gui_orbtk) #:use-module (crates-io))

(define-public crate-czkawka_gui_orbtk-1.0.1 (c (n "czkawka_gui_orbtk") (v "1.0.1") (d (list (d (n "czkawka_core") (r "=1.0.1") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "0g8bdg2a4q4x2ljzz4lg9pq03sr2is9dl8wjv473nrvf072h2d57")))

(define-public crate-czkawka_gui_orbtk-1.1.0 (c (n "czkawka_gui_orbtk") (v "1.1.0") (d (list (d (n "czkawka_core") (r "=1.1.0") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "143zy76i7kc1wj78fpycwcajiq22fgksb30nas4zbblf7rgzbxrb")))

(define-public crate-czkawka_gui_orbtk-1.2.0 (c (n "czkawka_gui_orbtk") (v "1.2.0") (d (list (d (n "czkawka_core") (r "=1.2.0") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "1wp86sd85lgq169mp7r95m9i3assq1bjdgls8r98lsx9dkiqq4w3")))

(define-public crate-czkawka_gui_orbtk-1.2.1 (c (n "czkawka_gui_orbtk") (v "1.2.1") (d (list (d (n "czkawka_core") (r "=1.2.1") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "1sa93a9snhjlm6b2k5n4n89vnvs2n7rz5rmbh0bx3yzddb1nq9x0")))

(define-public crate-czkawka_gui_orbtk-1.3.0 (c (n "czkawka_gui_orbtk") (v "1.3.0") (d (list (d (n "czkawka_core") (r "=1.3.0") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "09a4w31z283q36r05i23iib600nkhq3bfq4vl7q8lxrl811r6qig")))

(define-public crate-czkawka_gui_orbtk-1.4.0 (c (n "czkawka_gui_orbtk") (v "1.4.0") (d (list (d (n "czkawka_core") (r "=1.4.0") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "0fdbdsz1xh5iliz96008d42wps8frq060ffdfvfnhrgkjns1fjw2")))

(define-public crate-czkawka_gui_orbtk-1.5.0 (c (n "czkawka_gui_orbtk") (v "1.5.0") (d (list (d (n "czkawka_core") (r "=1.5.0") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "1nz8z2fhq6240grih1dh0dbvqj2s0hbyhnkhf7q7xnn6mg1xldc2")))

(define-public crate-czkawka_gui_orbtk-1.5.1 (c (n "czkawka_gui_orbtk") (v "1.5.1") (d (list (d (n "czkawka_core") (r "=1.5.1") (d #t) (k 0)) (d (n "orbtk") (r "=0.3.1-alpha3") (d #t) (k 0)))) (h "0pwxn6sssrp2v6pk8by23k4lw7wzl4spr7nl6rpgw837mzfdr7wy")))

