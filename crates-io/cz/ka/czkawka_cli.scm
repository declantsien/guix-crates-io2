(define-module (crates-io cz ka czkawka_cli) #:use-module (crates-io))

(define-public crate-czkawka_cli-1.0.1 (c (n "czkawka_cli") (v "1.0.1") (d (list (d (n "czkawka_core") (r "=1.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "14ngww9vnkjzgihc9bczybi76gli82wdvzi4hkm99mix600b1pfk")))

(define-public crate-czkawka_cli-1.1.0 (c (n "czkawka_cli") (v "1.1.0") (d (list (d (n "czkawka_core") (r "=1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1knlfda1cfgjb5lpycnpqh17s8rv7sfynvm7r9r3al9bgg1nwm0z")))

(define-public crate-czkawka_cli-1.2.0 (c (n "czkawka_cli") (v "1.2.0") (d (list (d (n "czkawka_core") (r "=1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0dhhyyr5x0rvirv04fffc64h2210qfrb1xs4irpgcja1rzkwf4n6")))

(define-public crate-czkawka_cli-1.2.1 (c (n "czkawka_cli") (v "1.2.1") (d (list (d (n "czkawka_core") (r "=1.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "12yxl1cmixp239ym2rgz0syhjgxfw15477r6j6mdsaybskzj6l8q")))

(define-public crate-czkawka_cli-1.3.0 (c (n "czkawka_cli") (v "1.3.0") (d (list (d (n "czkawka_core") (r "=1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "17srmrisvb2904bzq478i2s5fp422y173r16g7l5iwvwh313b47d")))

(define-public crate-czkawka_cli-1.4.0 (c (n "czkawka_cli") (v "1.4.0") (d (list (d (n "czkawka_core") (r "=1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "104r1vc66spcjkvyvlsxbkchcdwnaffi6032nk0xc5h858cwmc4r")))

(define-public crate-czkawka_cli-1.5.0 (c (n "czkawka_cli") (v "1.5.0") (d (list (d (n "czkawka_core") (r "=1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "17na6g9qrn4h867h9fq3b1jd14swd8xx0nw55ff01x97ljjprn2a")))

(define-public crate-czkawka_cli-1.5.1 (c (n "czkawka_cli") (v "1.5.1") (d (list (d (n "czkawka_core") (r "=1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1gnf6lv9hsnw5gk6hkpr5wwkij1bgbh4qdma57fyv8nfg2nsypr2")))

(define-public crate-czkawka_cli-2.0.0 (c (n "czkawka_cli") (v "2.0.0") (d (list (d (n "czkawka_core") (r "=2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0p9ppwn467ksxmaapazk58x5kfmjyinf94czlp7bm9hfpb8dd53k")))

(define-public crate-czkawka_cli-2.1.0 (c (n "czkawka_cli") (v "2.1.0") (d (list (d (n "czkawka_core") (r "=2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0fff7hhv72gbiyz7cy981rlknvllifz9vsxmsrfkxhr266lsq924")))

(define-public crate-czkawka_cli-2.2.0 (c (n "czkawka_cli") (v "2.2.0") (d (list (d (n "czkawka_core") (r "=2.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0c7n6q9qhpdxq0csy3m1i5sn54r44qa3ksqgrbx39abp88hf8njz")))

(define-public crate-czkawka_cli-2.3.0 (c (n "czkawka_cli") (v "2.3.0") (d (list (d (n "czkawka_core") (r "=2.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1wsd09a9z9mgdfgqv7xww8zvz5mmh9v72zna2a905cx20l7cqzrm")))

(define-public crate-czkawka_cli-2.4.0 (c (n "czkawka_cli") (v "2.4.0") (d (list (d (n "czkawka_core") (r "=2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1vp10mlf33hljwrdy8yajdl6xlfzgkmfgm75n2z931d5avv8ivmf")))

(define-public crate-czkawka_cli-3.0.0 (c (n "czkawka_cli") (v "3.0.0") (d (list (d (n "czkawka_core") (r "=3.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "0mg561qn8cdsyyy9szywq8x2987cmbyvb9jxbxbrqz8xif1n9v9g")))

(define-public crate-czkawka_cli-3.1.0 (c (n "czkawka_cli") (v "3.1.0") (d (list (d (n "czkawka_core") (r "=3.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1gq5d1hzx4x3qflzw70yy2xn9ljfv50j34s2rq0jpxgrz6g9czzq")))

(define-public crate-czkawka_cli-3.2.0 (c (n "czkawka_cli") (v "3.2.0") (d (list (d (n "czkawka_core") (r "=3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0g0zzzyq21swhw8zb3zhmjc8nzxv9pxwmlcn0bg13flmhkssxnhx")))

(define-public crate-czkawka_cli-3.3.0 (c (n "czkawka_cli") (v "3.3.0") (d (list (d (n "czkawka_core") (r "=3.3.0") (d #t) (k 0)) (d (n "img_hash") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1ahskr9ipcbssa3idbzlwggf73208nphfs9bmc1xm0ibny2zh6hc")))

(define-public crate-czkawka_cli-3.3.1 (c (n "czkawka_cli") (v "3.3.1") (d (list (d (n "czkawka_core") (r "=3.3.1") (d #t) (k 0)) (d (n "img_hash") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0ij0266ysafs41vf1014r20cgmmi6ga02bqw04rr9f05pd1rj98y")))

(define-public crate-czkawka_cli-4.0.0 (c (n "czkawka_cli") (v "4.0.0") (d (list (d (n "czkawka_core") (r "^4.0.0") (d #t) (k 0)) (d (n "img_hash") (r "^3.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "14y9hi173k3zxh6s90d8kxakz99wr9f9b0f80hwmbr8n2ayls8dn")))

(define-public crate-czkawka_cli-4.1.0 (c (n "czkawka_cli") (v "4.1.0") (d (list (d (n "czkawka_core") (r "^4.1.0") (d #t) (k 0)) (d (n "image_hasher") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0cslq2glj08f268ksyiahq6plik9b32rlr7lr9rkc4zvyjh9hglv")))

(define-public crate-czkawka_cli-5.0.0 (c (n "czkawka_cli") (v "5.0.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^5.0.0") (d #t) (k 0)) (d (n "image_hasher") (r "^1.0.0") (d #t) (k 0)))) (h "04azrnj76qvgc5l63jpypw6nlpqmyk6l6i3i1xgkmzziaswcvl95") (f (quote (("heif" "czkawka_core/heif") ("default")))) (r "1.60")))

(define-public crate-czkawka_cli-5.0.2 (c (n "czkawka_cli") (v "5.0.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^5.0.2") (d #t) (k 0)) (d (n "image_hasher") (r "^1.0.0") (d #t) (k 0)))) (h "0n0p7hkm38y2dzn6r41rx3sscd4vs6wpdhx0s6qrdygrr514dl5d") (f (quote (("heif" "czkawka_core/heif") ("default")))) (r "1.60")))

(define-public crate-czkawka_cli-5.1.0 (c (n "czkawka_cli") (v "5.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^5.1.0") (d #t) (k 0)) (d (n "image_hasher") (r "^1.1.2") (d #t) (k 0)))) (h "0gihn2ncynk15bp4wl2yhgcq396kql536n0dnaja4qdn84d9rcw0") (f (quote (("heif" "czkawka_core/heif") ("default")))) (r "1.65.0")))

(define-public crate-czkawka_cli-6.0.0 (c (n "czkawka_cli") (v "6.0.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^6.0.0") (d #t) (k 0)) (d (n "image_hasher") (r "^1.2") (d #t) (k 0)))) (h "0dlviz2sfxbc39x4qs0bid8mdlrcsgh0dqsl485nkym9mjaqnrv8") (f (quote (("heif" "czkawka_core/heif") ("default")))) (r "1.67.1")))

(define-public crate-czkawka_cli-6.1.0 (c (n "czkawka_cli") (v "6.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^6.1.0") (d #t) (k 0)) (d (n "fun_time") (r "^0.3.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "handsome_logger") (r "^0.8") (d #t) (k 0)) (d (n "image_hasher") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "07jpk7cni4padw1ngfiqcy1zzq2j6hpdvq2jq0s60582xskyri4w") (f (quote (("heif" "czkawka_core/heif") ("default")))) (r "1.70.0")))

(define-public crate-czkawka_cli-7.0.0 (c (n "czkawka_cli") (v "7.0.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (f (quote ("termination"))) (d #t) (k 0)) (d (n "czkawka_core") (r "^7.0.0") (d #t) (k 0)) (d (n "fun_time") (r "^0.3") (f (quote ("log"))) (d #t) (k 0)) (d (n "handsome_logger") (r "^0.8") (d #t) (k 0)) (d (n "image_hasher") (r "^1.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0fycj4i04i7n0380cz4643pb0l93ka6bkpk36v2058br1f9zkmhm") (f (quote (("libraw" "czkawka_core/libraw") ("heif" "czkawka_core/heif") ("default")))) (r "1.74.0")))

