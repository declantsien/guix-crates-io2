(define-module (crates-io wv -s wv-sys) #:use-module (crates-io))

(define-public crate-wv-sys-0.1.0 (c (n "wv-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gnl1yyrr4ch9vc2pbkd9fch19l6c2rnaabsrjliaa0zay6by4hm")))

(define-public crate-wv-sys-0.1.1 (c (n "wv-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lhc5n3ri4dz4lbqky6z8fnmv85d9ijpz2qh1zc8xnhk108w8xk1")))

(define-public crate-wv-sys-0.1.2 (c (n "wv-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jvpzkdj3sp383v98ljmpppi6wjpnc8aiwg9q3j9xk62hfm21h3y")))

(define-public crate-wv-sys-0.1.3 (c (n "wv-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m574dkrp88f9im3nipp7mc1a8ifs326dly6nd5vxcky00m97k8c")))

