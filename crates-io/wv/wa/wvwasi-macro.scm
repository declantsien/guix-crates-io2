(define-module (crates-io wv wa wvwasi-macro) #:use-module (crates-io))

(define-public crate-wvwasi-macro-0.1.0 (c (n "wvwasi-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("implement" "Win32_Foundation" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0gjwg84yfx0flmbkz89239zbbqgh047j2fkf7rc7hn8x3vklbkca")))

(define-public crate-wvwasi-macro-0.1.1 (c (n "wvwasi-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("implement" "Win32_Foundation" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "15bj5b3nj0d7m2pjv08njak6namcfmjngg2a991frg6rjy0snj0v")))

(define-public crate-wvwasi-macro-0.1.2 (c (n "wvwasi-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("implement" "Win32_Foundation" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "027d4x7l185fc5sblwshispgg5n55nmd71w66bldx7nnwhz3yrgk")))

(define-public crate-wvwasi-macro-0.1.3 (c (n "wvwasi-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("implement" "Win32_Foundation" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0ywh5n3p136ai128pxps6jz52q0fvd7fdxr3i5vz36via3qky6p2")))

