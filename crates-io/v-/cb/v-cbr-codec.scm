(define-module (crates-io v- cb v-cbr-codec) #:use-module (crates-io))

(define-public crate-v-cbr-codec-0.7.1 (c (n "v-cbr-codec") (v "0.7.1") (d (list (d (n "byteorder") (r ">=0.5.0") (d #t) (k 0)) (d (n "json-codec") (r ">=0.3") (d #t) (k 2)) (d (n "libc") (r ">0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r ">=0.2.21") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r ">=0.2.21") (d #t) (k 2)) (d (n "quickcheck_macros") (r ">=0.2.21") (d #t) (k 2)) (d (n "rand") (r ">=0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r ">=0.3") (d #t) (k 2)))) (h "1xcrck9hwf3l9mr0554a18nb1d907i2ap3ddqvq5y2gn8svqy3sc") (f (quote (("random" "quickcheck"))))))

