(define-module (crates-io v- tr v-trie) #:use-module (crates-io))

(define-public crate-v-trie-0.1.0 (c (n "v-trie") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0389hqbrb8l2ka1hniihlqipgf4hy6lhx41nnw7168y7b8yrsdmq")))

(define-public crate-v-trie-0.1.1 (c (n "v-trie") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1pmqaa8zxfm39wbvdzp29cyl2sr38mhln607jb939c8ibjs7d3b6")))

(define-public crate-v-trie-0.1.2 (c (n "v-trie") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0chlqgd653kihi9flgb5ywh90bh22xzsni04pmimlv60l2bxxgbk")))

