(define-module (crates-io v- co v-common-az-lmdb) #:use-module (crates-io))

(define-public crate-v-common-az-lmdb-0.1.1 (c (n "v-common-az-lmdb") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lmdb-rs-m") (r "^0.7.7") (d #t) (k 0)) (d (n "v_authorization") (r "^0.1.0") (d #t) (k 0)))) (h "009wmkr9xkmzq3k2q06dbg796681dwg8zqkjj2cz7g0gdr3nhm00")))

(define-public crate-v-common-az-lmdb-0.1.2 (c (n "v-common-az-lmdb") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lmdb-rs-m") (r "^0.7.7") (d #t) (k 0)) (d (n "v_authorization") (r "^0.1.1") (d #t) (k 0)))) (h "1laiwzj7271g5kb5in2p16gk4kjx9b2xvd78c6dip14q7vinagzq")))

