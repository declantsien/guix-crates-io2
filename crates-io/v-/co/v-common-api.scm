(define-module (crates-io v- co v-common-api) #:use-module (crates-io))

(define-public crate-v-common-api-0.1.2 (c (n "v-common-api") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.1") (d #t) (k 0) (p "v-common-onto")))) (h "1dmkb1v1d94zz2mcv9x2lyr55bh7qsa4kdccjwwmhjn6y2clgdy4")))

(define-public crate-v-common-api-0.1.3 (c (n "v-common-api") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.2") (d #t) (k 0) (p "v-common-onto")))) (h "17p6q37mgvsw19x1zl873x3zyk2kh65sskv1y9m8llj2hqd7axsi")))

(define-public crate-v-common-api-0.1.4 (c (n "v-common-api") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.3") (d #t) (k 0) (p "v-common-onto")))) (h "0wydyqr49j49bdwxingn9cs56dm3g9ppcwkvzhdcxkik7i10jw0m")))

(define-public crate-v-common-api-0.1.5 (c (n "v-common-api") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.3") (d #t) (k 0) (p "v-common-onto")))) (h "13sz9bn59dwph0yq2400jalxvzxjkqx5x8xl616iazn0p08q10qg")))

(define-public crate-v-common-api-0.1.6 (c (n "v-common-api") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.5") (d #t) (k 0) (p "v-common-onto")))) (h "0dn6z4mzy67sj3m8p3blcdb25jdjj6y2s5da37ngpqdxqd21d0y1")))

(define-public crate-v-common-api-0.1.7 (c (n "v-common-api") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.5") (d #t) (k 0) (p "v-common-onto")))) (h "18cjlb1xq5fiy6l78j8g034ac3cf9ka5j2qr6wn8p76kvr6w9h81")))

(define-public crate-v-common-api-0.1.8 (c (n "v-common-api") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.5") (d #t) (k 0) (p "v-common-onto")))) (h "0pjrv864mcvh2rkkkxs28b2ik7kykkkvbsayxcq2qa9jg8433r82")))

(define-public crate-v-common-api-0.1.9 (c (n "v-common-api") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "^0.1.6") (d #t) (k 0) (p "v-common-onto")))) (h "1ikhfps5f9g5jx52h7nwg99ga31aw54vw5mkz2hl285v0j0xbgg3")))

(define-public crate-v-common-api-0.1.10 (c (n "v-common-api") (v "0.1.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.8") (d #t) (k 0) (p "v-common-onto")))) (h "09bkvfysa6d02q384sifmvbrxa467i02d2abh5xf9d09srg4qhp8")))

(define-public crate-v-common-api-0.1.11 (c (n "v-common-api") (v "0.1.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.9") (d #t) (k 0) (p "v-common-onto")))) (h "164kd4sdaqsnrwkbll5pf9bqibna10bn3pqp1yr8r8ip2qrzxklx")))

(define-public crate-v-common-api-0.1.12 (c (n "v-common-api") (v "0.1.12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.10") (d #t) (k 0) (p "v-common-onto")))) (h "1x9f89yn21jid90nm96gpnf0grxiqsbr82fw6jd0gxxbgn8dl3mi")))

(define-public crate-v-common-api-0.1.13 (c (n "v-common-api") (v "0.1.13") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.11") (d #t) (k 0) (p "v-common-onto")))) (h "0ic60x8ag344r3l54vjxfidwvcw8cr1airflghd9wmgl8ap0lxs1")))

(define-public crate-v-common-api-0.1.14 (c (n "v-common-api") (v "0.1.14") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.12") (d #t) (k 0) (p "v-common-onto")))) (h "018rvks7v5s51icdahqhmsp91ymyq6a23m407igbg1blp43bra3m")))

(define-public crate-v-common-api-0.1.15 (c (n "v-common-api") (v "0.1.15") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.12") (d #t) (k 0) (p "v-common-onto")))) (h "1x388axib02milx09zgjp8dmza28lzn17v2w8zkf74s106011zj0")))

(define-public crate-v-common-api-0.1.16 (c (n "v-common-api") (v "0.1.16") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.3.0") (d #t) (k 0)) (d (n "nng") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "v_onto") (r "=0.1.13") (d #t) (k 0) (p "v-common-onto")))) (h "1287vmmyaxr41dvcchiahc0qhjzplsgrb0dp251na0hxq6v22fgz")))

