(define-module (crates-io v- co v-collections) #:use-module (crates-io))

(define-public crate-v-collections-0.1.0 (c (n "v-collections") (v "0.1.0") (h "125cfyaxnjrcsd61zz3x0fch0l3mz1k2miqngxw1xj5f9ac8v085") (y #t)))

(define-public crate-v-collections-0.5.0 (c (n "v-collections") (v "0.5.0") (h "1havryk2pbq1ga18ymwnasrgk01c95nnqbqs9wipqdlpqiv7mzwz") (y #t)))

