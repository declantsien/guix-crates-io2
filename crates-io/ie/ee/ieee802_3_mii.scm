(define-module (crates-io ie ee ieee802_3_mii) #:use-module (crates-io))

(define-public crate-ieee802_3_mii-0.1.0 (c (n "ieee802_3_mii") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1ahcz90rz5rlcfk2cvzi06kwlb54rbf6v42v25a32acbp3axavas") (y #t)))

(define-public crate-ieee802_3_mii-0.2.0-ALPHA1 (c (n "ieee802_3_mii") (v "0.2.0-ALPHA1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "0igdy00hcbwyify2y6qz55h9mkk62fma2pgzy6ix7s3fc9r468xp") (y #t)))

(define-public crate-ieee802_3_mii-0.2.0 (c (n "ieee802_3_mii") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1ij6k0nxr1f4h0b6p0sh8xq0y7gc34i9c2n88lvvlpnay2f3nrd6") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r")))) (y #t)))

(define-public crate-ieee802_3_mii-0.3.0 (c (n "ieee802_3_mii") (v "0.3.0") (h "0vv416c7q9y7904bwwbzi47vqqf53m8rrn3jcxvcd719vk8cc0xj") (y #t)))

(define-public crate-ieee802_3_mii-0.3.1 (c (n "ieee802_3_mii") (v "0.3.1") (h "0xwf0xyfsmarr0mphpn6yv7wiy6x0n8rbp4ag3i9wcpvhppnxs7g")))

