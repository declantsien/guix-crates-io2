(define-module (crates-io ie ee ieee802154) #:use-module (crates-io))

(define-public crate-ieee802154-0.1.0 (c (n "ieee802154") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)))) (h "12yx5agvnsrd2cl3lrixnfdhva7crx5pnbmgmwhbs77kgwxgl6r3")))

(define-public crate-ieee802154-0.1.1 (c (n "ieee802154") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)))) (h "0jmyxpgn7bydpryf752cdsn7l8jz7xh3fdniv3n12ylzsvzh1a1c")))

(define-public crate-ieee802154-0.2.0 (c (n "ieee802154") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)))) (h "19yns9y8j5cbj03wmdlqnly50bjl0pgy1rvjz7v86imrdc9m6cdf")))

(define-public crate-ieee802154-0.3.0 (c (n "ieee802154") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)))) (h "10fjf035wlxqfw41zgld2s85d927k2dm4hrdc79r74540102nqxg")))

(define-public crate-ieee802154-0.4.0 (c (n "ieee802154") (v "0.4.0") (d (list (d (n "bytes") (r "^0.5") (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)) (d (n "static-bytes") (r "^0.1") (d #t) (k 2)))) (h "1zfypm0v4ykn67hb2mawbpn87sxj1b74whw2xkrdif6zl127bgi0")))

(define-public crate-ieee802154-0.5.0 (c (n "ieee802154") (v "0.5.0") (d (list (d (n "byte") (r "^0.2.4") (d #t) (k 0)) (d (n "hash32") (r "^0.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)))) (h "0fmyyjh3l9gbw8v7m9f81xc03x843l77paz70b7a3fwsr43r3bqg")))

(define-public crate-ieee802154-0.5.1 (c (n "ieee802154") (v "0.5.1") (d (list (d (n "aes") (r "^0.7.0") (k 2)) (d (n "byte") (r "^0.2.4") (d #t) (k 0)) (d (n "ccm") (r "^0.4.0") (k 0)) (d (n "cipher") (r "^0.3.0") (k 0)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "101j32f2h34rff2hfr8xawk7dv41dh84y36xclmql3igh6ancrwp") (y #t)))

(define-public crate-ieee802154-0.6.0 (c (n "ieee802154") (v "0.6.0") (d (list (d (n "aes") (r "^0.7.0") (k 2)) (d (n "byte") (r "^0.2.4") (d #t) (k 0)) (d (n "ccm") (r "^0.4.0") (k 0)) (d (n "cipher") (r "^0.3.0") (k 0)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0m6riv06x865zrslkys0jgvpccy0034073c4blg7y2rkm5lnvxxb")))

(define-public crate-ieee802154-0.6.1 (c (n "ieee802154") (v "0.6.1") (d (list (d (n "aes") (r "^0.7.0") (k 2)) (d (n "byte") (r "^0.2.4") (d #t) (k 0)) (d (n "ccm") (r "^0.4.0") (k 0)) (d (n "cipher") (r "^0.3.0") (k 0)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "hash32-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "19ahnhvy8c57m17mb0b5azw6cal572rjmbhrvfahf6105zk6vjsg")))

