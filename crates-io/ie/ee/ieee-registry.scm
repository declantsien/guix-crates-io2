(define-module (crates-io ie ee ieee-registry) #:use-module (crates-io))

(define-public crate-ieee-registry-0.1.0 (c (n "ieee-registry") (v "0.1.0") (d (list (d (n "expanduser") (r "^1.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)))) (h "1yrn0pbbp3fns0f72wwhy1zl65fk2h3v72i36ha4z2sjdzk5vid1")))

(define-public crate-ieee-registry-0.1.1 (c (n "ieee-registry") (v "0.1.1") (d (list (d (n "expanduser") (r "^1.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "0rmqk04bnf8f7y6vbl80iwy9bdsv70mjgv935xsdwfj6wi6xa60w")))

(define-public crate-ieee-registry-0.2.0 (c (n "ieee-registry") (v "0.2.0") (d (list (d (n "expanduser") (r "^1.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1lnwcg8hr72i4wd16zwn68fs3bzv75i15sqg24q9clw661819m8v")))

