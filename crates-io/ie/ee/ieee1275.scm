(define-module (crates-io ie ee ieee1275) #:use-module (crates-io))

(define-public crate-ieee1275-0.1.0 (c (n "ieee1275") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (t "powerpc-unknown-linux-gnu") (k 0)))) (h "0j0ccbnvjmvwb603dx14k3w7gjxban5vh4h63235q7sc6305xq3d") (f (quote (("no_panic_handler") ("no_global_allocator"))))))

(define-public crate-ieee1275-0.1.1 (c (n "ieee1275") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (t "powerpc-unknown-linux-gnu") (k 0)))) (h "0z34mpkgr6hz4q30q4lgqdhi6s3fam943bc4797ck8m9vzgniniy") (f (quote (("no_panic_handler") ("no_global_allocator"))))))

