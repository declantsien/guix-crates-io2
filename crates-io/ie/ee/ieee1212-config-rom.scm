(define-module (crates-io ie ee ieee1212-config-rom) #:use-module (crates-io))

(define-public crate-ieee1212-config-rom-0.0.90 (c (n "ieee1212-config-rom") (v "0.0.90") (h "1aqja7i9k5ihzfgyrwxy5dgh7cp8x9i01nrq5z5hr7rz8d1smn88") (y #t)))

(define-public crate-ieee1212-config-rom-0.1.0 (c (n "ieee1212-config-rom") (v "0.1.0") (h "08z1dhmvrflx9la3fshfzn0sdkryzhajbd8x84r5fh9f24w8zx3i") (y #t)))

(define-public crate-ieee1212-config-rom-0.1.1 (c (n "ieee1212-config-rom") (v "0.1.1") (h "0vhfbbnnxlx7rw8cc7r6maj34i0gql5qxyjmfgg1wz1qqx4wxb50") (y #t)))

(define-public crate-ieee1212-config-rom-0.1.2 (c (n "ieee1212-config-rom") (v "0.1.2") (h "12m9ag2w2kn4a5d072f5sy8abjq4fjyiw5cd44c6657i4pc2l1bl")))

