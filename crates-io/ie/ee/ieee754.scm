(define-module (crates-io ie ee ieee754) #:use-module (crates-io))

(define-public crate-ieee754-0.1.0 (c (n "ieee754") (v "0.1.0") (h "02hp891vzxwbrk1849l24kc06sjq23gc3159zx1qh99k2mz7xvmq") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.1.1 (c (n "ieee754") (v "0.1.1") (h "14xzm6k74mni1r8cckk9wq1fcjb6l1sjykv01q9xp55i3zg1c6bz") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.0 (c (n "ieee754") (v "0.2.0") (h "1pvgsf0g4rk5phs6jpwkv7h5w9spla3rzk7ng5kkazjc8hkcqmws") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.1 (c (n "ieee754") (v "0.2.1") (h "14sc4cr4b8czjjwz6ycmilzh6fbsfcn4zpn9c3jqcxf8y4ly62ab") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.2 (c (n "ieee754") (v "0.2.2") (h "1bafcqrms8a2frsjm3w3hjgiq3lwkmqrp6lcwa6zr5nx5vv7lir4") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.3 (c (n "ieee754") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0mj74359zf46hh3i60qfjslmdpni0hqm536d6d5nzb9xh9lzh5yz") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.4 (c (n "ieee754") (v "0.2.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0g27wvry1plsia1f325s1f07p01v1vim8h36y7b5bn5rr2iq57y0") (f (quote (("unstable")))) (y #t)))

(define-public crate-ieee754-0.2.5 (c (n "ieee754") (v "0.2.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1zrfkck70lvdi40qfp7zwvf533fdih0j5hqmp5pcg4mzjsqqjqxg") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.2.6 (c (n "ieee754") (v "0.2.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1771d2kvw1wga65yrg9m7maky0fzsaq9hvhkv91n6gmxmjfdl1wh") (f (quote (("unstable"))))))

(define-public crate-ieee754-0.3.0-alpha.2 (c (n "ieee754") (v "0.3.0-alpha.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "127ncx8wknxjs126jrpfyqzasx9iygg6vhh35kab68vh9sww1sq3") (f (quote (("unstable"))))))

