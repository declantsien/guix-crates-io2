(define-module (crates-io ie ee ieee-apsqrt) #:use-module (crates-io))

(define-public crate-ieee-apsqrt-0.1.0 (c (n "ieee-apsqrt") (v "0.1.0") (d (list (d (n "rustc_apfloat") (r "^0.2.0") (d #t) (k 0)))) (h "0fhhy5z4bqifkp3mplbw73nlf3as0cm581z02qmx9y1d5j7b4drs")))

(define-public crate-ieee-apsqrt-0.1.1 (c (n "ieee-apsqrt") (v "0.1.1") (d (list (d (n "rustc_apfloat") (r "^0.2.0") (d #t) (k 0)))) (h "1v3cbi6cd16i7rgcgy7sfj4a6bwk1vqx2322r8lfmbjaalf98a23")))

