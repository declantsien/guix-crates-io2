(define-module (crates-io ie ee ieee802_3_miim) #:use-module (crates-io))

(define-public crate-ieee802_3_miim-0.3.0-ALPHA1 (c (n "ieee802_3_miim") (v "0.3.0-ALPHA1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1jhssinzbxq2hrkwqxkl5nv6a53kyiz3zh9hp69psr1l7hzszm4k") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3.0-ALPHA2 (c (n "ieee802_3_miim") (v "0.3.0-ALPHA2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "0cp7z7czwilj6bjzp3yh8rn4j1kmrivcnj20czjnjlnlfl8isa9q") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3.0-ALPHA3 (c (n "ieee802_3_miim") (v "0.3.0-ALPHA3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1ry4xr71b7ll96hwbzg4kkx0bn8kgwl77634vd370k2w20s7gyp1") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3.0-ALPHA4 (c (n "ieee802_3_miim") (v "0.3.0-ALPHA4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1f6wpc3sgfz7chxq45zrk5w4dxz2awbvfm57ar0mdnspbq6mahgp") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.4.0 (c (n "ieee802_3_miim") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "16ipm4mk4wx46l6f94ihwi0azyv1w9h1zqj989rpkcfh14xhi199") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.4.1 (c (n "ieee802_3_miim") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1sndvb0cjha931b6k6766nvsdsfah2kidpws9rcdhpk54md1call") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.5.0 (c (n "ieee802_3_miim") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (d #t) (k 0)))) (h "0xdd7mz98p8mjb0g717n49f196m11w1ybn2nlsargjy87a6cfrp7") (f (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.6.0 (c (n "ieee802_3_miim") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0y5gnl7670637pq2y8738zbdk34m6admqhisl4vfw1wylmf4isyd") (f (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.7.0 (c (n "ieee802_3_miim") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1wqps449icja962sw8mi693yjrk34lfakknfxqzx2h9nnwf2np8l") (f (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.7.1 (c (n "ieee802_3_miim") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "12j32sgjvbk45rzvb68gxf2fhhfxw13s2vr1nfqfikrfm050q777") (f (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("ksz8081r" "phy") ("default" "lan8720a" "lan8742a" "ksz8081r")))) (y #t)))

(define-public crate-ieee802_3_miim-0.7.2 (c (n "ieee802_3_miim") (v "0.7.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0gj4wdfklyizb0jc3dz21fb0lwbz3lkskbd854qzzk5gzslyjvzs") (f (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.8.0 (c (n "ieee802_3_miim") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0a6lzz7mbr8zm1dg8jni12qb6c2gsydf2yqa1pfr8avspkl23ppv") (f (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("ksz8081r" "phy") ("default" "lan8720a" "lan8742a" "ksz8081r"))))))

