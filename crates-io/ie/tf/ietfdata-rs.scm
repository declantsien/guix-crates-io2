(define-module (crates-io ie tf ietfdata-rs) #:use-module (crates-io))

(define-public crate-ietfdata-rs-0.1.1 (c (n "ietfdata-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pab4wxszkq1y086675gd3sl7xag8719fsqpwgj6dvyg04zh732s")))

(define-public crate-ietfdata-rs-0.2.0 (c (n "ietfdata-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x5qjg3vrqs219rnr0ssh0hlzj3vfrqa792xxi9sqs8b4hyzr7q6")))

(define-public crate-ietfdata-rs-0.2.1 (c (n "ietfdata-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z3frv5jmdghpl1007jj95vjls0hidhjc0ddp06c7k3fs288skyl")))

(define-public crate-ietfdata-rs-0.2.2 (c (n "ietfdata-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n3abk5qw7mym1kyr7bkzn2k3nn9q73vjsk8cj0b2n8wx9y8glr1")))

(define-public crate-ietfdata-rs-0.2.3 (c (n "ietfdata-rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zlyx435p37z0rcrdmc8zwqar13xmbwcsy6jnahjpiawmv6j2wc5")))

(define-public crate-ietfdata-rs-0.2.4 (c (n "ietfdata-rs") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xdfbzqiayp3ccjmdyn60873j0p6jn1z2xb2p644q9qmdal01ihc")))

(define-public crate-ietfdata-rs-0.3.0 (c (n "ietfdata-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "087gfl1z34br22rvv4rl1if1wpj0n0yi87p4hy3w4rsdzyn1b5lf")))

