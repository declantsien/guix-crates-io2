(define-module (crates-io ie tf ietf) #:use-module (crates-io))

(define-public crate-ietf-0.1.0 (c (n "ietf") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.2.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "pager") (r "^0.16.0") (d #t) (k 0)))) (h "0c7lma4jy1w5angywn2fcm72gz64ia8rpyxa1s3fr425z92v3kf3")))

(define-public crate-ietf-0.2.0 (c (n "ietf") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cursive") (r "^0.15") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.2.1") (f (quote ("https"))) (d #t) (k 0)))) (h "0pldfw14iwlnvnq32a9ynigndzki0n4407ldyr0gm0qrc0qawa2v") (y #t)))

(define-public crate-ietf-0.2.1 (c (n "ietf") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.3.0") (f (quote ("https"))) (d #t) (k 0)))) (h "06jd7drp6046ssdv2xwx3qwg1ppsm38iaywkmasqmv67zmvyf0si")))

(define-public crate-ietf-0.2.2 (c (n "ietf") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "minreq") (r "^2.3.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "pager") (r "^0.16.0") (d #t) (k 0)))) (h "13p75v62y6nfywcrzy8y4bfdgbkd3h46a6h6x5q50i7qcijrrpab")))

