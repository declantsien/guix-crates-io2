(define-module (crates-io ie lr ielr) #:use-module (crates-io))

(define-public crate-ielr-0.1.0 (c (n "ielr") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "1rhrgy12z897f2n1gr3xd65ld427bv3qrdca0p5dgrvh8hlcdaky") (r "1.58")))

(define-public crate-ielr-0.1.1 (c (n "ielr") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)))) (h "17wfldppfmdyxkxad3q5v71b057n86qh3h5xkzs0vms03i5wxkpc") (r "1.58")))

