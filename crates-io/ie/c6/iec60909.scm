(define-module (crates-io ie c6 iec60909) #:use-module (crates-io))

(define-public crate-iec60909-0.1.0 (c (n "iec60909") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sparsetools") (r "^0.2") (d #t) (k 0)) (d (n "spsolve") (r "^0.1") (f (quote ("rlu"))) (d #t) (k 0)))) (h "0fakcj5fl9vkd68ny9nx29sp6cha44l928p6qm2dj6s9lr7d5hvi")))

