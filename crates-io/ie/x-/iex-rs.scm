(define-module (crates-io ie x- iex-rs) #:use-module (crates-io))

(define-public crate-iex-rs-0.1.0 (c (n "iex-rs") (v "0.1.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1cp5yy40fbnsdf7jm0n22mccdpig0gr3cylyibnc9gf6lynramn3")))

(define-public crate-iex-rs-0.1.1 (c (n "iex-rs") (v "0.1.1") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "00kngjz5liyd1s6wjsz8d4c80h9icn6663p497r1ixfb59r0hz40")))

