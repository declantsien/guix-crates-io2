(define-module (crates-io if -c if-changed) #:use-module (crates-io))

(define-public crate-if-changed-0.1.0 (c (n "if-changed") (v "0.1.0") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "01936xqn6gkc1n3mi3p7xwy7aqcjk1w4y7p081il1hirx16h1qsj")))

(define-public crate-if-changed-0.2.0 (c (n "if-changed") (v "0.2.0") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1sf461pmabljgnfcz4dviw6dlrldrxgl1305zx34f689dhdlbk2j")))

(define-public crate-if-changed-0.3.0 (c (n "if-changed") (v "0.3.0") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "insta") (r "^1") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0sspkz2q9zji0l1slhj28yp2lz37n02vmjlydqs0ax2rjh6swzhv")))

(define-public crate-if-changed-0.3.1 (c (n "if-changed") (v "0.3.1") (d (list (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "genawaiter") (r "^0.99") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "insta") (r "^1") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "000r6mvmg9ss8dr8069xmfwml87imxd159b8006nyq6lzlv5jlxf")))

