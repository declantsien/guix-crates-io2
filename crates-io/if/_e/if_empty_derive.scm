(define-module (crates-io if _e if_empty_derive) #:use-module (crates-io))

(define-public crate-if_empty_derive-0.1.0 (c (n "if_empty_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a5c2mnxjjd0q287456hfr0n7id3h231ikci9vcjj4f283b14ksd")))

