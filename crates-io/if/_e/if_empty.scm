(define-module (crates-io if _e if_empty) #:use-module (crates-io))

(define-public crate-if_empty-0.1.0 (c (n "if_empty") (v "0.1.0") (h "0jx5z86wzcpyvz2ysbhg0bk5f35lj3ajv4j2sw05363r6sdvr9db")))

(define-public crate-if_empty-0.2.0 (c (n "if_empty") (v "0.2.0") (h "1klqyah28m5abq7q6aivs9iib1rm0xirq89q4bjfkxnlrf9q0b27")))

(define-public crate-if_empty-0.3.0 (c (n "if_empty") (v "0.3.0") (d (list (d (n "if_empty_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0disyknnlazsdqkg3hxkvir8gda1a331g0p4zldimq6a75mw0b1g")))

