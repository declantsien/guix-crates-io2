(define-module (crates-io if #{97}# if97) #:use-module (crates-io))

(define-public crate-if97-0.1.0 (c (n "if97") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1xgi1mqqlv64japdkr8sk82kniadkpdbx90nj76lasn1kz68qmd2") (y #t)))

(define-public crate-if97-0.1.1 (c (n "if97") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "14b7r7yx03m6dk0r8g1vx7g2jfb9zhcrxw03yr121kbj0xyflljj") (y #t)))

(define-public crate-if97-0.1.2 (c (n "if97") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0qlhi66np4kghk2c5nk4kcpdzdfkfig47k51zlpssncqy2177329") (y #t)))

(define-public crate-if97-0.1.3 (c (n "if97") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0dijdmgbhkkvqxxgkxi47a2gk80nrwljrj3i6jcdlz9hp4x1sqvk") (y #t)))

(define-public crate-if97-0.1.4 (c (n "if97") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "08gssvwyal9cp88rgw6sks04zmgzwbb11a6ka40p9ds19lv1wpj2") (y #t)))

(define-public crate-if97-0.1.5 (c (n "if97") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "14pv0ln0mjk2arcz8sifc9dgfbrxdqwckzvpxggb22jiwd11bvy3") (y #t)))

(define-public crate-if97-0.1.6 (c (n "if97") (v "0.1.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0dk6j12y3kq5932ncyxgfpw499bsbf5j2annb9g17wss8li8aziz") (y #t)))

(define-public crate-if97-0.1.7 (c (n "if97") (v "0.1.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1cnz4r415x6rj9g1xgyppcfcy0m6zldgkrwjhr2ns5ap6gwskhig") (y #t)))

(define-public crate-if97-0.1.8 (c (n "if97") (v "0.1.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0sasn27rrn90r3as1xnlz30qiwa6y2bjii250dl917ysc5a899h7") (y #t)))

(define-public crate-if97-0.1.9 (c (n "if97") (v "0.1.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1izkxm4a4n5753km59vcwdj14ppsd6x79y6ngjxg9cyrc9k87mqj") (y #t)))

(define-public crate-if97-0.2.0 (c (n "if97") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1igsiwv6qa5379qbaahs0gnf06k93sfhx6hahhc4zmxi4s4j3cv9") (y #t)))

(define-public crate-if97-0.2.1 (c (n "if97") (v "0.2.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0600x86gcd9rp3lpiq7kg1r0c88ci0phfqdv0q40wh6j61bmzx88") (y #t)))

(define-public crate-if97-0.2.2 (c (n "if97") (v "0.2.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "02lg21ax98qfvyl9423711mkmyh1c5fqbvgkyhgqjhfpj9b02261") (y #t)))

(define-public crate-if97-0.2.3 (c (n "if97") (v "0.2.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "073lqpxsbixqvla3z82k47hw9r7n2b3a14m8fjpsmsq2j1dls0sc") (y #t)))

(define-public crate-if97-0.2.4 (c (n "if97") (v "0.2.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1xgp6pisndka81wx0ci7dzi5dlmwkwsqh1hc0xrknmv04zmhcpp0") (y #t)))

(define-public crate-if97-0.2.5 (c (n "if97") (v "0.2.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "07m8lc0ddjwmnw3v6pwi042j225dn94hds3k1pf0wf3p5flk4bk8") (y #t)))

(define-public crate-if97-0.2.6 (c (n "if97") (v "0.2.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0r7adjhfap3mz9v4gb3rpz4s3d7f1inni1a911xcwn8bbyygc3zb") (y #t)))

(define-public crate-if97-0.2.7 (c (n "if97") (v "0.2.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0bqjkcf3x4dz18qlalah2cp9x8j984yrv1mp6kvmn4y6j2yqa9ad") (y #t)))

(define-public crate-if97-0.2.8 (c (n "if97") (v "0.2.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "1yjpzpgjqzypy8n3zyqa02j1xmrgi3shqhhvq6s778fc2lcgfr1z") (y #t)))

(define-public crate-if97-0.2.9 (c (n "if97") (v "0.2.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0wwafj2sqwfx7c81bmb3qzm65dl96p2h0m3p6s7ndnkgh0n7z5gb") (y #t)))

(define-public crate-if97-0.3.0 (c (n "if97") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0amgdy5rcl0lxjz013fwa89j8dnvdw2a9cyd4rzxb8gsgdki9v1s") (y #t)))

(define-public crate-if97-0.3.1 (c (n "if97") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "10fgfjk8xm7zzmqs6nl2cjlmpq9pv2dbwjb0glfd62ha3x0hxzhn") (y #t)))

(define-public crate-if97-0.3.2 (c (n "if97") (v "0.3.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)))) (h "0qv5lqm92crdnqw1kmjhpnhzkf0bsn6gphqdmj9v46h8l3gbzl39") (y #t)))

(define-public crate-if97-0.3.3 (c (n "if97") (v "0.3.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1s2dj8x3pc07nwlfj7iy7z3nv7yydhzl792nv9gkpqshcdgrnhjy") (y #t)))

(define-public crate-if97-0.3.4 (c (n "if97") (v "0.3.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "11vc16gabssak8m643cziv1w2qw7vy5xrv3fapa3xsjyhn8chih1") (y #t)))

(define-public crate-if97-0.3.5 (c (n "if97") (v "0.3.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1l825855k4cn9nkjwf9yj601jwmx5158hjxhyhs4q4c1yls4rvq8") (y #t)))

(define-public crate-if97-0.3.6 (c (n "if97") (v "0.3.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0gzx6ii7wydqsx7wjg775bzayw1z9rj4l4bx01zc75gxpj1zfza3") (y #t)))

(define-public crate-if97-0.3.7 (c (n "if97") (v "0.3.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0iig6nrim3gs5894vyjznxxga8ycgp5fcxnzdcnbcyv4xbpz86g9") (y #t)))

(define-public crate-if97-0.3.8 (c (n "if97") (v "0.3.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1j3rbh86ss3h3f7v0gnb9q3jnrmpprfdbrjq8ldviip8vkbpka2l") (y #t)))

(define-public crate-if97-0.3.9 (c (n "if97") (v "0.3.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0k9134v9sa2dy51sgmrjn31s4nlfs1khl1j0zzbfz1027jw0xrx4") (y #t)))

(define-public crate-if97-0.4.0 (c (n "if97") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "196xcn7lyr6yas2biw4hjjb9qhfzssh9p3sy9hrs423blm668qsf") (y #t)))

(define-public crate-if97-0.4.1 (c (n "if97") (v "0.4.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "09vsng8h2n2alxfmrs6c32kjdk6pg1yrdi123fzh53lih7n8wr0x") (y #t)))

(define-public crate-if97-1.0.0 (c (n "if97") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "172nlmjjgs0h2l4hqqja5z44wpmm1d6d6d9vs6sxsiw0d4jfa6qx") (y #t)))

(define-public crate-if97-1.0.1 (c (n "if97") (v "1.0.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "16pyfs2jqk4c80f0vxwnkskax6jvzlx2i8lkyfjbcjb7iqn8512a") (y #t)))

(define-public crate-if97-1.0.2 (c (n "if97") (v "1.0.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "19j8m9rgkj92966055idphznqzp0hnazsdfk02hbg0ndbs2pmkls") (y #t)))

(define-public crate-if97-1.0.3 (c (n "if97") (v "1.0.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1q6ipghibq4kf6y3i9y3vlc55hi74afh6b6jh53vm2g374d37h95") (y #t)))

(define-public crate-if97-1.0.4 (c (n "if97") (v "1.0.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1djmrxqchm1hlxj7im55i7rd5ckxhm5b4s5hrs6ximqnx325mgmq") (y #t)))

(define-public crate-if97-1.0.5 (c (n "if97") (v "1.0.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1hdqrziqy43l7b4095ykgmv0pvqf7d9ypjph278a4h95mzbjhwaj") (y #t)))

(define-public crate-if97-1.0.6 (c (n "if97") (v "1.0.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1fiwirjii5wvlwknhjqpba1zmmq0lnf9hckxwrbmwnihinz7lz38") (y #t)))

(define-public crate-if97-1.0.7 (c (n "if97") (v "1.0.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "08jvgi5dsgs1bji7z5c59l7agmbhff3dwmdcdwjli1pjdivqv9xk") (y #t)))

(define-public crate-if97-1.0.8 (c (n "if97") (v "1.0.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0rz9djv7lk7dp7c2b0c94my93hvpzcv3s3qlk5ai7vr7w9dq4cn5") (y #t)))

(define-public crate-if97-1.0.9 (c (n "if97") (v "1.0.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1492d6gks0vbzchiplc74jr55n6ln8v2n2r1gw9wnjk535ad0rz9") (y #t)))

(define-public crate-if97-1.1.0 (c (n "if97") (v "1.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1g5hcl8d8qgrwjbpw3n1h8dd6sd8fd351k35jzza2jr1bgcxwdq3") (y #t)))

(define-public crate-if97-1.1.1 (c (n "if97") (v "1.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "007rfzrxw46zxs46l8fcpqnx1yw6jckpsnl7rsvzs9xxvl69vc7c") (y #t)))

(define-public crate-if97-1.1.2 (c (n "if97") (v "1.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ddzmyrbbsqy92sm4gzj9p624vc3cpjzgqg6qagaqpn4gvy598gv") (y #t)))

(define-public crate-if97-1.1.3 (c (n "if97") (v "1.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "013d9y5z6jyi597fdmri0hr48gvnvz83n0k51kp19aq57d4hf0ww") (y #t)))

(define-public crate-if97-1.1.4 (c (n "if97") (v "1.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0mslxd31zr4z34sgllf3hs4bcfpgd8zmwhgylkxwmh635hi2fkpd") (y #t)))

(define-public crate-if97-1.1.5 (c (n "if97") (v "1.1.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1qmpisk9cim25134krg66r4jf02s685lcw9wm1g4845rmjmldcnb") (y #t)))

(define-public crate-if97-1.1.6 (c (n "if97") (v "1.1.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "17wn0j9nhgjqc0bkgc47rp2v0ixyzgx6b3fzymda3a2n8gkdgjyr") (y #t)))

(define-public crate-if97-1.1.7 (c (n "if97") (v "1.1.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1q885jl63xmp5q1fc45hcb6nz29n1palmdcwhscbgy2zklshnv23") (y #t)))

(define-public crate-if97-1.1.8 (c (n "if97") (v "1.1.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0iy7l819c7ql1y7x9660z9rhc2hz352xk806xyz6algmhrzsv5vx") (y #t)))

(define-public crate-if97-1.1.9 (c (n "if97") (v "1.1.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1w2sjl9cgnlkhbd001g8birjxjd0zjd7gpgfr4ccirgrnfind9sj") (y #t)))

(define-public crate-if97-1.2.0 (c (n "if97") (v "1.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0zjrxslr3sv8inahvvcdhfjh0i3gymfs84ww993asnqmwmcsi53m") (y #t)))

(define-public crate-if97-1.2.1 (c (n "if97") (v "1.2.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1nxp5kz8q24d2ij8dk10g3lf736k0g15cvniain3lcyfqlbwl3lr") (y #t)))

(define-public crate-if97-1.3.0 (c (n "if97") (v "1.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1av6x9yrmr12wxs2zbh55jyq3ysq9pkg50bl8rngsdd6iykvlsfv") (f (quote (("default") ("c_api")))) (y #t)))

(define-public crate-if97-1.3.1 (c (n "if97") (v "1.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1y9b62wixqzrkdmj0mpnzs02350d0plmfslnjjfjw442lb6fnvqz") (f (quote (("default") ("c_api")))) (y #t)))

(define-public crate-if97-1.3.2 (c (n "if97") (v "1.3.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0c7xw7gpfigfkl45pbmlfihjawj2w9w9m6z6pnijdkrpj1cqq2rp") (y #t)))

(define-public crate-if97-1.3.3 (c (n "if97") (v "1.3.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0pyyfchlfp9gq1yq5slmgqzlbpflcavsrai9nq4d2cxrc4zfq8w7") (y #t)))

(define-public crate-if97-1.3.4 (c (n "if97") (v "1.3.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1jidalnxpnhgz9bjnxn0fc10v7v93mikk1p8xagvagwg6nnhxv5q") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.3.5 (c (n "if97") (v "1.3.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1gmwm1rds7zs99wpg4drc9zsap1cbmdqcdrx504nxdf2m0jn1pk9") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.3.6 (c (n "if97") (v "1.3.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0kqwp5h9xhy8vmg8d7gm12ayfar03g3hfn0imd1945bpmzp5p8c4") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.3.7 (c (n "if97") (v "1.3.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "01szxdhwb7pff2fs9967q56k5lpw3g228y9v0n8gfkbsq5vqsgf5") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.3.8 (c (n "if97") (v "1.3.8") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0f2pp8f6spg41b19qvgl3qfkhjjifhh9zj2k3xhw6g499l3g759j") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.3.9 (c (n "if97") (v "1.3.9") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1hzn555bxc1rh94r355c35y489qa83s3lh74v81ysy90hkxscc6l") (f (quote (("stdcall") ("cdecl")))) (y #t)))

(define-public crate-if97-1.4.0 (c (n "if97") (v "1.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1gjlhdhv5dy2gk5v8dzlm2lbzcplj0qjv79r6r7nsxvr5lqz6ww0") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-if97-1.4.1 (c (n "if97") (v "1.4.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "07ciiydyzp7xhms13vblg60ahxipvxka5qm85h5q1zjfdnbcwr0h") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-if97-1.4.2 (c (n "if97") (v "1.4.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0r8lzs8y2a9flsk4qw7jgryz65fy9b5xj8hnkqz89fdpgr4mxf6y") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-if97-1.4.3 (c (n "if97") (v "1.4.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "131b4zbl9i1bc08vbvlwjdfav07rpj8bi7jb40rn9w7qz50g5hha") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

(define-public crate-if97-1.4.4 (c (n "if97") (v "1.4.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "15axnzbhibgnhq144if36x8w5aznmlbdkccrlnvpnnjymvjxv5m9") (f (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (y #t)))

