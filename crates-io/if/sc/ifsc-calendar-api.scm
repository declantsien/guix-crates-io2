(define-module (crates-io if sc ifsc-calendar-api) #:use-module (crates-io))

(define-public crate-ifsc-calendar-api-0.1.0 (c (n "ifsc-calendar-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "19rq32zfz4pkmw1ifqsvwmcwmg7sqlf9ip97gbbsyxsji3ii7z0x")))

(define-public crate-ifsc-calendar-api-0.1.1 (c (n "ifsc-calendar-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0szx0xakiqx8di00fwjv7c7kw87pihmbfd2042hh5bn52cmnsgq8")))

(define-public crate-ifsc-calendar-api-0.2.0 (c (n "ifsc-calendar-api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n3jhmlxgfkck4vb9hy314nnykr0qqrwj1643x9z0216qlkq72g5")))

