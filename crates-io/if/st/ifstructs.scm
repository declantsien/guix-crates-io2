(define-module (crates-io if st ifstructs) #:use-module (crates-io))

(define-public crate-ifstructs-0.1.0 (c (n "ifstructs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 2)))) (h "02gab6pd7n0pzl080hvjx5gq02dwmmrpyv5kmp2c2pgzv82ms93z")))

(define-public crate-ifstructs-0.1.1 (c (n "ifstructs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 2)))) (h "10cmbra7bbcnyphygs3z7sw5p6117xaic6w56dm8gsm5j87pfkdj")))

