(define-module (crates-io if -d if-decompiler) #:use-module (crates-io))

(define-public crate-if-decompiler-0.1.0 (c (n "if-decompiler") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "1zapsy85mwsjlscck2zz6vsljshl7xynqym5q2m3gr77qlaz1xyk")))

