(define-module (crates-io if ac ifacialmocap) #:use-module (crates-io))

(define-public crate-ifacialmocap-0.1.0 (c (n "ifacialmocap") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1vvsxy82dvys7b7g0m9jngzc0s4n1b2ixznygj58vx0frf2drvcz")))

