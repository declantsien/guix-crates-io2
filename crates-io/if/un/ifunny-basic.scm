(define-module (crates-io if un ifunny-basic) #:use-module (crates-io))

(define-public crate-ifunny-basic-0.1.0 (c (n "ifunny-basic") (v "0.1.0") (d (list (d (n "easy_base64") (r "^0.1.7") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (k 0)))) (h "0vy6ffmljnd70b458ma31zv8hfsknv73fm3lj629d5808arc086h")))

