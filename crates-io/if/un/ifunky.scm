(define-module (crates-io if un ifunky) #:use-module (crates-io))

(define-public crate-ifunky-0.1.0 (c (n "ifunky") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1kap3m3b88anpnk8nmnbl2ckhqsppnr6vc65k9ym46ix80xj03b1")))

(define-public crate-ifunky-0.1.1 (c (n "ifunky") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0k21jncsqi2j5fdjf9548bfna234x65fkj06qpr848xvsm7jia9v")))

