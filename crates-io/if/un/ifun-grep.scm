(define-module (crates-io if un ifun-grep) #:use-module (crates-io))

(define-public crate-ifun-grep-0.1.0 (c (n "ifun-grep") (v "0.1.0") (h "0f56w5jc2wk2n1l95a2k6chd0383p6gqmmx1y6spz12i8vzldj24")))

(define-public crate-ifun-grep-0.2.0 (c (n "ifun-grep") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "037ai0cvy7vffl03n1pqn6ywwkcm54f8p101w0spvx6bz1vspi0m")))

