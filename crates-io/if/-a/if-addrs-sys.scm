(define-module (crates-io if -a if-addrs-sys) #:use-module (crates-io))

(define-public crate-if-addrs-sys-0.2.0 (c (n "if-addrs-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dj33s18lsvy8cv08kbs892v6y6rv87v4ibm389pnr5k9vvhbaky") (l "ifaddrs")))

(define-public crate-if-addrs-sys-0.3.0 (c (n "if-addrs-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b3j68jx07ysx8mci3di1vfih80i8f6xawilsysijlzw6ysjjhga") (y #t) (l "ifaddrs")))

(define-public crate-if-addrs-sys-0.3.1 (c (n "if-addrs-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pxibdmywiy5dpbklsqkddrmz0bb3ah20pdaw37jn824cpqmc9cy") (l "ifaddrs")))

(define-public crate-if-addrs-sys-0.3.2 (c (n "if-addrs-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1skrzs79rafv185064p44r0k1va9ig4bfnpbwlvyhxh4g3fvjx6y") (l "ifaddrs")))

