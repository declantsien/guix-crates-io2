(define-module (crates-io if ad ifaddrs-sys) #:use-module (crates-io))

(define-public crate-ifaddrs-sys-0.1.0 (c (n "ifaddrs-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2.28") (d #t) (k 0)))) (h "070b9lc0lp0y1dfipf4f0i5xi2jccj6hc890yi6qfq79py6fw76z") (y #t)))

