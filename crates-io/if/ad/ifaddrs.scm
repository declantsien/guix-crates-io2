(define-module (crates-io if ad ifaddrs) #:use-module (crates-io))

(define-public crate-ifaddrs-0.1.0 (c (n "ifaddrs") (v "0.1.0") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "ifaddrs-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.28") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)))) (h "1kr3vflbd7sg3vcqmcv5wgl9pm3qwk06vdkjpw106a282vrrfz4z") (y #t)))

