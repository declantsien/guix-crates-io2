(define-module (crates-io if -n if-none) #:use-module (crates-io))

(define-public crate-if-none-0.0.1 (c (n "if-none") (v "0.0.1") (h "0cnh16sr8mlg4sp3ki6jz4w61lwjzd8i3zf340yssavbmbs9qhf4")))

(define-public crate-if-none-0.0.2 (c (n "if-none") (v "0.0.2") (h "08mx4qxsjlc7w7i6f2097lnf4cbxsmjmprza5x9455x5ip2817zm") (y #t)))

(define-public crate-if-none-0.0.3 (c (n "if-none") (v "0.0.3") (h "0cb92cwyg20vx2qsrj0n3nlqdk9vpd00i7cplzpijmsiljnin8c8") (y #t)))

(define-public crate-if-none-0.0.5 (c (n "if-none") (v "0.0.5") (h "1j9hch7y4jnzmrfhd133nqpj40adbb6vvc8ff8rxy1yr56nj12nl")))

