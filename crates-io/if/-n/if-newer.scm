(define-module (crates-io if -n if-newer) #:use-module (crates-io))

(define-public crate-if-newer-0.0.0 (c (n "if-newer") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "cradle") (r "^0.2.1") (d #t) (k 2)) (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0r5gclk2a2jh0d4slljvbkv9ix5cyysfbad9y363j4kh2nqxksyn")))

