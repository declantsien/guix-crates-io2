(define-module (crates-io if -t if-to-let-chain) #:use-module (crates-io))

(define-public crate-if-to-let-chain-1.0.0 (c (n "if-to-let-chain") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1g0hhx7v9ywwyl8gym8ji0pad64lsbcbg9f9bgmvc5d2wq2ki6br")))

