(define-module (crates-io if fi iffi-macros) #:use-module (crates-io))

(define-public crate-iffi-macros-0.0.1 (c (n "iffi-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hls8ynz0anmbpqy4jkmv18zxipwqr1lc336fd390zlc0gmjxjjr")))

