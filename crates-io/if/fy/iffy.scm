(define-module (crates-io if fy iffy) #:use-module (crates-io))

(define-public crate-iffy-0.1.1 (c (n "iffy") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "191gcb3niswxppvq9k7x67p7dn452r9qv458g512w8ji8pfh4dv9")))

(define-public crate-iffy-0.1.2 (c (n "iffy") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qsibzgyfmncbv20jlm0pxbafv5fdjk44l22sq4z1j0zk7hz0fx1")))

(define-public crate-iffy-0.1.3 (c (n "iffy") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04pf9hchxsp42i2cpk8908zp53prfzg029hm7m32n83kixdp7n3v")))

