(define-module (crates-io if op ifop) #:use-module (crates-io))

(define-public crate-ifop-0.1.0 (c (n "ifop") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_System_Ole" "Win32_UI_Shell_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "1pqqgwiwp5cqshdx16mnkcvirp8nk0a7f9jx1w2klz39xziz4aqn")))

(define-public crate-ifop-0.1.1 (c (n "ifop") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_System_Ole" "Win32_UI_Shell_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "1fwz4a8gxcc9p54ss3xh1fwvvavm3dghwaylav3lr8h8dkdvz11y")))

(define-public crate-ifop-0.1.2 (c (n "ifop") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_UI_Shell_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "1hpl5gw014axyj44isdagymaws1nyqk0pc8x8mv5wlw8ccdvaj6j")))

(define-public crate-ifop-0.1.3 (c (n "ifop") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_UI_Shell_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "1dnjrgw77gnnr5kyyq63vf45av4w4lfcs97m8mwwg9j97fsajyc8")))

(define-public crate-ifop-0.1.4 (c (n "ifop") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_UI_Shell_Common"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "05j90h1kgjr6kz1j9zwb2mp824naz9p5dv3kaw4hp4l429ga1hiy")))

(define-public crate-ifop-0.1.5 (c (n "ifop") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_System_Com" "Win32_UI_Shell_Common" "Win32_Storage_FileSystem"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (k 0)))) (h "07858gp2hcswlvfnchza19lq7v5mj6p92lh5hniqjm7c3z5ln6nj")))

