(define-module (crates-io if it ifit) #:use-module (crates-io))

(define-public crate-ifit-0.1.0 (c (n "ifit") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxpcmmnamyc4zf19m5bgrb9mf26fhzsyg8q0yfnncra78njx7gc")))

(define-public crate-ifit-0.1.1 (c (n "ifit") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xlirq3ykylg9pyxva9m8f3mb0dbyzhl3r97cyd59bxyvkf2zval")))

(define-public crate-ifit-0.2.0 (c (n "ifit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1alg9qi304hs2zqicyfnppqjbpc9n2lb75fd829rc77rh2hmla4k")))

(define-public crate-ifit-0.2.1 (c (n "ifit") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sl8b71ykm6cm0946bsgbcjjcqhb11ln56lad4ba1wqzn7yb9a45")))

