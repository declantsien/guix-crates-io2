(define-module (crates-io if co ifconfig) #:use-module (crates-io))

(define-public crate-ifconfig-0.1.0 (c (n "ifconfig") (v "0.1.0") (h "1k2mnxsmsya4vh81rssryyy7dbwlpkm03yv4hl948slb57fwsznz")))

(define-public crate-ifconfig-0.1.1 (c (n "ifconfig") (v "0.1.1") (h "0p2h367n20vfhdr50n1x6pj81dpgjmxjakxcg4g2c4291qm19mdd")))

