(define-module (crates-io if ly iflytek-sys) #:use-module (crates-io))

(define-public crate-iflytek-sys-0.0.1 (c (n "iflytek-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "090s3qwf77nrx0wsmhd9ghhah8mmp9hdagpzrhs4iwrirj3p3rri") (y #t) (r "1.56")))

(define-public crate-iflytek-sys-0.0.2 (c (n "iflytek-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "10y61nvhqqcjpc25gpml9g9klpsvv7n6gzy2mycadkgjxb95g3ks") (y #t) (r "1.56")))

(define-public crate-iflytek-sys-0.0.3 (c (n "iflytek-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1z1vghqy10pi4lqldlcgr0z6x1c2nav5iq944j3wy5c87r5yvr3d") (y #t) (r "1.56")))

(define-public crate-iflytek-sys-0.0.4 (c (n "iflytek-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "023034k93km7f1wi4xx9mgjhljhagy8q8yf1adaa6awqv9adchky") (r "1.56")))

