(define-module (crates-io if wi ifwifi) #:use-module (crates-io))

(define-public crate-ifwifi-1.0.0 (c (n "ifwifi") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "wifi-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "wifiscanner") (r "0.5.*") (d #t) (k 0)))) (h "0yyr56mv5anp56df4vdg97xzg0a4sv9sskkv4id0vaahx028jd4z")))

(define-public crate-ifwifi-1.0.1 (c (n "ifwifi") (v "1.0.1") (d (list (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "wifi-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "wifiscanner") (r "0.5.*") (d #t) (k 0)))) (h "09354vlvjh0qd9aza50583la039adp00xz0r5v24q2h8i8bgvap2")))

(define-public crate-ifwifi-1.0.2 (c (n "ifwifi") (v "1.0.2") (d (list (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "wifi-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "wifiscanner") (r "0.5.*") (d #t) (k 0)))) (h "18cj0imgc1abljh56lvl5n8pfzxv6apgcw16pkj2pbfdsssaicwf") (y #t)))

(define-public crate-ifwifi-1.0.3 (c (n "ifwifi") (v "1.0.3") (d (list (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "wifi-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "wifiscanner") (r "0.5.*") (d #t) (k 0)))) (h "1aqjf9jvjk8qngla4nbx2fcs4w076jxy1dwi5zgs4986n7qq57nv")))

(define-public crate-ifwifi-1.2.0 (c (n "ifwifi") (v "1.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete_command") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "wifi-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "wifiscanner") (r "0.5.*") (d #t) (k 0)))) (h "0rz5gl2an21h4mprc8g4vip4nhx8w3b7473y8xv6mcf689m6rrl1")))

