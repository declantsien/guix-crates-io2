(define-module (crates-io if oa ifoam) #:use-module (crates-io))

(define-public crate-ifoam-0.0.0 (c (n "ifoam") (v "0.0.0") (d (list (d (n "duct") (r "^0.13.6") (d #t) (k 0)))) (h "01rv8i1da9swd9qcvmkpf12fwnhrmkbb2h2bkjd1i00wpjd00sy9")))

(define-public crate-ifoam-0.0.1 (c (n "ifoam") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "1q2hsnirai93ajwd466d7jl55xf9hwyy19753vls4mvninafx1fs")))

