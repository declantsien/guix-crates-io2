(define-module (crates-io if mt ifmt) #:use-module (crates-io))

(define-public crate-ifmt-0.1.0 (c (n "ifmt") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "15r3s4flgkz938dxv7hxb3ikn79xihjb3jzriymky8fl3i3xxf90")))

(define-public crate-ifmt-0.1.1 (c (n "ifmt") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1hfyfkgds7zc936an1lw13v7fankdgygp5v39hzafmk0g56bdyn2")))

(define-public crate-ifmt-0.1.2 (c (n "ifmt") (v "0.1.2") (d (list (d (n "ifmt-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1qbp497a3a7bkwibkp2kj9wfxi9jg3wvrxxmcsanzsk5bd3bmsap")))

(define-public crate-ifmt-0.1.3 (c (n "ifmt") (v "0.1.3") (d (list (d (n "ifmt-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0dgnd1swgfrdyrlg5qpiiqycc5ipyybicril7grbcx0n21h1fi3k")))

(define-public crate-ifmt-0.2.0 (c (n "ifmt") (v "0.2.0") (d (list (d (n "ifmt-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0r31cqkv827cvl32c3c6ffsysvvkhxmawsvsn77irvy20x823v5b")))

(define-public crate-ifmt-0.3.0 (c (n "ifmt") (v "0.3.0") (d (list (d (n "ifmt-impl") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "18nhckx6ckh89aaw750fibhdhy743f9cvg0ad75aakqf8d39ia6x")))

(define-public crate-ifmt-0.3.1 (c (n "ifmt") (v "0.3.1") (d (list (d (n "ifmt-impl") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "08swf7186yygd4x00i4wckgq0k172632q54xc08vk9blfljznldn")))

(define-public crate-ifmt-0.3.2 (c (n "ifmt") (v "0.3.2") (d (list (d (n "ifmt-impl") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1644nfz3n1jprya865d31f0frzg125y5ib15x2sfgz8qpn5xk8r3")))

(define-public crate-ifmt-0.3.3 (c (n "ifmt") (v "0.3.3") (d (list (d (n "ifmt-impl") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0l07z36vbxln98lmbrach0bsq05a88llra1ci9flf5ag83hzil3x")))

