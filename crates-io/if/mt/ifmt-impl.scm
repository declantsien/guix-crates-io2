(define-module (crates-io if mt ifmt-impl) #:use-module (crates-io))

(define-public crate-ifmt-impl-0.1.2 (c (n "ifmt-impl") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02qzxyl4r9sqclidmazfqjxd9a5jnarsgj9b3fl24wybjq67p41a")))

(define-public crate-ifmt-impl-0.1.3 (c (n "ifmt-impl") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0f5s42gkbp8grxmy947zvsf2h982hkyyds6k1066r2793s5cc216")))

(define-public crate-ifmt-impl-0.2.0 (c (n "ifmt-impl") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0wh06103xwv0s37pvfrq6ggml2s67c7glp7p451qsp9w53pr2ikl")))

(define-public crate-ifmt-impl-0.3.0 (c (n "ifmt-impl") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0frcyrpfsbap3dk7n02qi6qdqx6ysblwa0plp0b8zdx8wq2nkad3")))

(define-public crate-ifmt-impl-0.3.1 (c (n "ifmt-impl") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "131h9441sqs4pwsfha2r4gnpw8gzxkh4qwyysjjh268920ylw4np")))

(define-public crate-ifmt-impl-0.3.2 (c (n "ifmt-impl") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15va3dq5dsmwrlw7sddc8fl654mgp0xxrsfp0m24db1pwxmdnb3s")))

(define-public crate-ifmt-impl-0.3.3 (c (n "ifmt-impl") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0119a0f928syfr1skld0h6sn6c5bxqmvla5b36k32d225xk8a0z5")))

