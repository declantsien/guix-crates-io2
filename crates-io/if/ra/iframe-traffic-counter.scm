(define-module (crates-io if ra iframe-traffic-counter) #:use-module (crates-io))

(define-public crate-iframe-traffic-counter-0.1.0 (c (n "iframe-traffic-counter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq8xshd67qs3kqimpzkzrnzx74cgq9js5g2haflhj1ffszn370v")))

