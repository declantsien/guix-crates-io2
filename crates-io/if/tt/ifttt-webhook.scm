(define-module (crates-io if tt ifttt-webhook) #:use-module (crates-io))

(define-public crate-ifttt-webhook-0.1.0 (c (n "ifttt-webhook") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "051rqxirr6smx2bzn0f2a477sm2lmh52npsjh5f72m7i79vwvccw")))

(define-public crate-ifttt-webhook-0.1.1 (c (n "ifttt-webhook") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1yl756sdqcpm13xjd00q5bb72mydk7wimdxv1ykp5bvhh3cvp4wp")))

(define-public crate-ifttt-webhook-0.2.0 (c (n "ifttt-webhook") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1sqmnbrmhy6f4pj5y7px88j47qig80aaqkmhmi78wrxli3c864a6")))

(define-public crate-ifttt-webhook-0.3.0 (c (n "ifttt-webhook") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ms3y7kp9nam6vd4blmlg2i4fmmxrwx5ifmayfkqlw8f6jzjf20m")))

(define-public crate-ifttt-webhook-0.3.1 (c (n "ifttt-webhook") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1wafr3l0wpsk3hnynnfg0gz5rpflmink9nivlbapvg871dph3iwg")))

(define-public crate-ifttt-webhook-0.3.2 (c (n "ifttt-webhook") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0vnb2kll1vn3fh0213nm7n47950s14fm51y2aznq6p8sz5w5xqml")))

