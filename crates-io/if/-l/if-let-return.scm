(define-module (crates-io if -l if-let-return) #:use-module (crates-io))

(define-public crate-if-let-return-0.1.0 (c (n "if-let-return") (v "0.1.0") (h "0ckjf9w3vhgpzp2d0lyaki9mfw4b5y00m74rvh636bafmbjam00f")))

(define-public crate-if-let-return-0.1.1 (c (n "if-let-return") (v "0.1.1") (h "0cl27k73rprszv08wf3cbd09ghr8ap3my29pgvl3ybb1n8qa6c83")))

