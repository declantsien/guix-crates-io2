(define-module (crates-io if _r if_rust_version) #:use-module (crates-io))

(define-public crate-if_rust_version-0.1.0 (c (n "if_rust_version") (v "0.1.0") (h "08b49dca32g05rqll07bqmf86f1xqphzn8bdxbq6f98p0100mkgx")))

(define-public crate-if_rust_version-1.0.0 (c (n "if_rust_version") (v "1.0.0") (h "1v6mj3vqy5g0x7gpyg5kiivm42qqgras69cxb0hrg4w67qrwpns6")))

