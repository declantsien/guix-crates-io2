(define-module (crates-io ak to aktors) #:use-module (crates-io))

(define-public crate-aktors-0.2.0 (c (n "aktors") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core"))) (d #t) (k 0)))) (h "0kv51wikmldd32hqb0y06fabnjga0f45cpshq9631rbc9q4ylc0b")))

(define-public crate-aktors-0.2.1 (c (n "aktors") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core"))) (d #t) (k 0)))) (h "09hxviv6a55faw5j73f4rhaqbjr0qf2kgd767rxvhmn29v2qifaz")))

(define-public crate-aktors-0.2.2 (c (n "aktors") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core"))) (d #t) (k 0)))) (h "07qdzdcl88qw68vdas0axb7qss05g9yhwllr19sg5dpzzs609pmk")))

(define-public crate-aktors-0.2.3 (c (n "aktors") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core"))) (d #t) (k 0)))) (h "1x6z55kc71l52gzqpqg6ndwv45kf1ykcvw9nvzdg10sb3vr13nr1")))

(define-public crate-aktors-0.2.4 (c (n "aktors") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core"))) (d #t) (k 0)))) (h "03azm6h80qk189rzaaim72q4xlrslwbsnjvm6rvzppphxkbcf06d")))

(define-public crate-aktors-0.2.5 (c (n "aktors") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core" "full"))) (d #t) (k 0)))) (h "1shja6qylrm56iy80i7lidn624qchsg38x77xm3as96fbi95mfxa")))

(define-public crate-aktors-0.2.6 (c (n "aktors") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core" "full"))) (d #t) (k 0)))) (h "0s66a0gpvcyk2q7a071yqlcxzfar2k70idg0yid1rnv46gbm023c")))

(define-public crate-aktors-0.2.7 (c (n "aktors") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("sync" "rt-core" "full"))) (d #t) (k 0)))) (h "104ba3g5qmgkcq3674k5fhsnh60ya493hnvfs0x52hx6b3r2bfkb")))

