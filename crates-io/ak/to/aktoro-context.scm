(define-module (crates-io ak to aktoro-context) #:use-module (crates-io))

(define-public crate-aktoro-context-0.1.0-alpha.1 (c (n "aktoro-context") (v "0.1.0-alpha.1") (d (list (d (n "aktoro-channel") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "aktoro-raw") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1yfm2sq79zr7kfz5jrwnvgx43ngicimi51r0wq1q7njszk1zimvv")))

(define-public crate-aktoro-context-0.1.0-alpha.2 (c (n "aktoro-context") (v "0.1.0-alpha.2") (d (list (d (n "aktoro-channel") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "aktoro-raw") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1xwn2adq0vp5ljrx46354drx61ggrs0ffmfjn5cx2k7jjlwd16z5")))

(define-public crate-aktoro-context-0.1.0-alpha.3 (c (n "aktoro-context") (v "0.1.0-alpha.3") (d (list (d (n "aktoro-channel") (r "^0.1.0-alpha.6") (d #t) (k 0)) (d (n "aktoro-raw") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "06pyfndxm1j0hpimphplxqgj5jnbvlh6j5papcxcc3v10fwyaknb")))

(define-public crate-aktoro-context-0.1.0-alpha.4 (c (n "aktoro-context") (v "0.1.0-alpha.4") (d (list (d (n "aktoro-channel") (r "^0.1.0-alpha.8") (d #t) (k 0)) (d (n "aktoro-raw") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-io-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "1vrlrn2g58b5fcrg4c0lrblgnly4zj7i0dax1j26rm2fk1732ami")))

