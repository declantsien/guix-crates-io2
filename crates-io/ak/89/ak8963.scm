(define-module (crates-io ak #{89}# ak8963) #:use-module (crates-io))

(define-public crate-ak8963-1.0.0 (c (n "ak8963") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.2") (d #t) (k 0)))) (h "00mbjxj7y3jrv07c1irxmvdp3zjzbp02njf6zrlkc6k8n5vb6pi2")))

(define-public crate-ak8963-1.0.1 (c (n "ak8963") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.2") (d #t) (k 0)))) (h "0sg8nfzyypi8ylrii4f6w8alnlfxl5q0dh2g9dinj8y02nz9qvyr")))

(define-public crate-ak8963-1.1.0 (c (n "ak8963") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "1wpa6zyi036libgwp7avbzgfd1g4ipqv35ax2b3xmjrhah45nbvh")))

