(define-module (crates-io ak ai akai) #:use-module (crates-io))

(define-public crate-akai-0.1.0 (c (n "akai") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1n7hrgcrwj3prl3zrng4pcfs5pb2l64ag1bb848wvlwd4izkhj7r")))

(define-public crate-akai-0.1.1 (c (n "akai") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0n87vwy57bbcxiyy5i9n1ff86rx9bl861gxa13lcqv06xwin4nkk")))

