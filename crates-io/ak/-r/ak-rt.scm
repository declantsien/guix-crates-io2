(define-module (crates-io ak -r ak-rt) #:use-module (crates-io))

(define-public crate-ak-rt-0.1.0 (c (n "ak-rt") (v "0.1.0") (h "1nr54g5k3w92wqlslg738yzbiylqaksbb9ggmdy9gbkw46q2fbv8")))

(define-public crate-ak-rt-0.1.1 (c (n "ak-rt") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.2.0-alpha.6") (d #t) (k 0)))) (h "0qq1330m2gbyzlnbcv2wf1cg1ffhhhrl4qp60ibgm2sfvl7i0da9")))

