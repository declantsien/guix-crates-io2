(define-module (crates-io ak it akita_derive) #:use-module (crates-io))

(define-public crate-akita_derive-0.1.0 (c (n "akita_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a3isp8vlnkgl9hsir7kacnvqb2fpfmdcgf789nbbdicq3cxf8rx")))

(define-public crate-akita_derive-0.1.1 (c (n "akita_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h58xnkz8mgfw889zlw7b8zaw348irlzw0ln74kbqgi1akjz7j8a")))

(define-public crate-akita_derive-0.1.2 (c (n "akita_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "196ai9w0rygiri1b50rqwmmzswwhlllycpnijw0m9sb89hrac7ww")))

(define-public crate-akita_derive-0.1.3 (c (n "akita_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f5l8xjavbap457z4xkgv1w1ml2h8alwc5khl92nw6jlbs3kah18")))

(define-public crate-akita_derive-0.1.4 (c (n "akita_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "069w7h3ygcd51q7h280gz0j37xaf3vgzgif8q593kj014njwv7ap")))

(define-public crate-akita_derive-0.1.5 (c (n "akita_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hvcmmkjslwcfd66bk43bmr1nkgjb8khaqcljk412ns7c0drd974")))

(define-public crate-akita_derive-0.2.0 (c (n "akita_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "175sam4sspi2vf081lanrnck2dmvh7jm80v7254swy45nx446pvj")))

(define-public crate-akita_derive-0.2.1 (c (n "akita_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19mzlz5h1d0rah6fcgghyx6zbpkw7jp8fynk0k43dfznxyic9npy")))

(define-public crate-akita_derive-0.2.2 (c (n "akita_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05mx1ybs1f3mm4h9vx5f7q4kx0kk14sa8v4viblgcd77rhnk5k9w")))

(define-public crate-akita_derive-0.2.3 (c (n "akita_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09fb4fycplw07fnzp9yl141c6rhmxnz5gj79k6gw695fm3m1a4yn")))

(define-public crate-akita_derive-0.2.4 (c (n "akita_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11vbwfd4x7cb6iim6lbx736yzva1hkfsy903v7f293p7x11k01li")))

(define-public crate-akita_derive-0.2.5 (c (n "akita_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z75a76jngbn9fzplxgj46dvrlq1rd8xai5hj6kqj1gaf79v6z36")))

(define-public crate-akita_derive-0.3.0 (c (n "akita_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18f5w8ldjz0rdmyaglpy876xwxycfsdz3kls47vr6kg8b7mi2cdp")))

(define-public crate-akita_derive-0.3.1 (c (n "akita_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p16drkx0lzj7f3w4f09wkqi37ylj2wcyjrlzhgwprjzalvxp3l9")))

(define-public crate-akita_derive-0.3.2 (c (n "akita_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jfg76rw6ak3hcqyd1v34isvjv14l12lny4lzdzjlyipzhlq5vaf")))

(define-public crate-akita_derive-0.3.3 (c (n "akita_derive") (v "0.3.3") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kzxa9snbp7l0qfsrsczrsz4zk79nrhwl653hj4bdmgyw1c17b4k")))

(define-public crate-akita_derive-0.3.4 (c (n "akita_derive") (v "0.3.4") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12f0aq4hamyzhzd48jgfz934f4yrvfyphk8qwzn41vv0ww4ikcvd")))

(define-public crate-akita_derive-0.3.5 (c (n "akita_derive") (v "0.3.5") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wkd7nqgbc2k92iv5zj46c86b5klj8n133gi1siypj39njqidwch")))

(define-public crate-akita_derive-0.3.6 (c (n "akita_derive") (v "0.3.6") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s42hrli79c4kxfjf6gap4gappcwch7n27y6189li57czgcg2qv2")))

(define-public crate-akita_derive-0.3.7 (c (n "akita_derive") (v "0.3.7") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z4d55i0bf0g7fv0n9fcgdkkl26z1pw8iprdbh91bp68qd8h2xn0")))

(define-public crate-akita_derive-0.4.0 (c (n "akita_derive") (v "0.4.0") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "002b1n6mxg7f9n53jz7piw8pnhjp2x2z7pnzgv8hm2xrfj2lh5da")))

(define-public crate-akita_derive-0.4.1 (c (n "akita_derive") (v "0.4.1") (d (list (d (n "if_chain") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ls1j2h0k1w94cdk6g7pg1z236dz930z4mghbi26vahwmb3a2gxj")))

