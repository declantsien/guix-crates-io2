(define-module (crates-io ak s_ aks_primes) #:use-module (crates-io))

(define-public crate-aks_primes-0.1.0 (c (n "aks_primes") (v "0.1.0") (h "0ridcs0bi9n4vaync16saya703jdkyvawrkci2zfcibqbl65fwq0")))

(define-public crate-aks_primes-0.1.1 (c (n "aks_primes") (v "0.1.1") (h "12602h9x8z5bvshxalc2hwxmm80qk9kry6482rcahdqnwd5f4znr")))

