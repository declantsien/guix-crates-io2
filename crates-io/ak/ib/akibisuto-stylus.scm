(define-module (crates-io ak ib akibisuto-stylus) #:use-module (crates-io))

(define-public crate-akibisuto-stylus-0.1.0 (c (n "akibisuto-stylus") (v "0.1.0") (d (list (d (n "warp") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17h14rp2sr5fla2wkwfa877b3knlrrgk5cfrsjdvg5mqnqzphr81") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.1 (c (n "akibisuto-stylus") (v "0.1.1") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "0rxwfvg8jcnb7s8qi9mq3bidcg5ag9p9jz0qsannrb3j1jllblzw") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.2 (c (n "akibisuto-stylus") (v "0.1.2") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "1awjdpil47cpz9ayy0yvicqxssrx4zq0jhgn62ih86kf835ica3h") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.3 (c (n "akibisuto-stylus") (v "0.1.3") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "067ac9hb9m6kbd4hsq33zln4p9rv9nj0j0zprs0anl9hwj6xk3rn") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.4 (c (n "akibisuto-stylus") (v "0.1.4") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "1ajvn7i3v51cv0ckn676mawkb1ps37438s2xp5jz2kcad4dy3adn") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.5 (c (n "akibisuto-stylus") (v "0.1.5") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "0apw4ljn89v10119fjlyp4wjlh5g246gl7fsdlgi8hg5jfm4dp6h") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.6 (c (n "akibisuto-stylus") (v "0.1.6") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "032j1dffn6y87h46lw3lnl3ldn6g6bkg1pmlg5zxc2xiyvbsv5h5") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.7 (c (n "akibisuto-stylus") (v "0.1.7") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "1wm4fvmch1j9hj13h4fghr8gym3p4p95qnz6r8kibkj5qfb0apza") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.8 (c (n "akibisuto-stylus") (v "0.1.8") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "05a62gf2wf0vp4sjw9lkb9mwzrj7cyf5186ffdhj1ckjjz3z0slz") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.9 (c (n "akibisuto-stylus") (v "0.1.9") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "1yrjgi058b2wr77ghgf0h7sk3184f2bs5g87r9gdcfr62dmsawnq") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.10 (c (n "akibisuto-stylus") (v "0.1.10") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "01xr11vx9cwaxgir5wyymz1c0iiylywcwpfhqsddkdg4xadwk0js") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.11 (c (n "akibisuto-stylus") (v "0.1.11") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "0pcdw6fk687m8dwfxhylm6vk1adkzhzdcdvb4wbsa092yxjnl1fv") (f (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1.12 (c (n "akibisuto-stylus") (v "0.1.12") (d (list (d (n "warp") (r "^0.2") (o #t) (k 0)))) (h "0z7jspkwbhihngqpwdxk72zdcqaqsz7npib6qy12kf7x2h1y1xd3") (f (quote (("default" "warp"))))))

