(define-module (crates-io ak ia akiaki) #:use-module (crates-io))

(define-public crate-akiaki-0.0.0 (c (n "akiaki") (v "0.0.0") (d (list (d (n "akiaki-fcgi") (r "^0.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "08hxavf191hgaamihnx2gng4337db3xihjs0v7hbvmfs9296djw6")))

(define-public crate-akiaki-0.0.1 (c (n "akiaki") (v "0.0.1") (d (list (d (n "akiaki-fcgi") (r "^0.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01j8qkrf697qvc4cj4hbas0ai5w9mw75zx7p0syf0gfpnkvg7bd8")))

(define-public crate-akiaki-0.0.2 (c (n "akiaki") (v "0.0.2") (d (list (d (n "akiaki-fcgi") (r "^0.0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1izb4v51r2kb9b08l75wyd75fn1m9ri74hjnv411sv7kxsm2yb7m")))

(define-public crate-akiaki-0.0.3 (c (n "akiaki") (v "0.0.3") (d (list (d (n "akiaki-fcgi") (r "^0.0.3") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0x7wmrpzcnak5c78d4li7fc19sz3h0f3qm77wbh2wpvinkzk3xp9")))

