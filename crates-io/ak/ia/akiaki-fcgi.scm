(define-module (crates-io ak ia akiaki-fcgi) #:use-module (crates-io))

(define-public crate-akiaki-fcgi-0.0.0 (c (n "akiaki-fcgi") (v "0.0.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1ln3ljhqpgn63zmibnn8k8gmb16wbrxwg6xklzb1g4kxan3b7w4s")))

(define-public crate-akiaki-fcgi-0.0.1 (c (n "akiaki-fcgi") (v "0.0.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0wgv2f0vj4701c0144f5lx5yk2ni9330j79rkvmb77j5z7449v7w")))

(define-public crate-akiaki-fcgi-0.0.2 (c (n "akiaki-fcgi") (v "0.0.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jamj9pf1m9pq0gml5d0i0g88amz4gcbrdz1r16aacs0hs1r98sj")))

(define-public crate-akiaki-fcgi-0.0.3 (c (n "akiaki-fcgi") (v "0.0.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ydan5k0kc3v7llb3cx7v0hcf3rmslf82wl8abxfs4svgkhw4j8d")))

