(define-module (crates-io ak so akso) #:use-module (crates-io))

(define-public crate-akso-0.1.0 (c (n "akso") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10l5m6agd0m2a8r4lhfpmhj7w3hpdjgiijlz3jp6ayb39zl7k73n")))

(define-public crate-akso-0.1.1 (c (n "akso") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gwxg1gl9n6rcgib0mh3cagi5k2j7975ndvzfzd1iiyvssyc5c0q")))

(define-public crate-akso-0.1.2 (c (n "akso") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "113py6hlnyfhi9gc0f4qa14c0a03w0nlarqz4jznhpw4am01jd3s")))

(define-public crate-akso-0.1.3 (c (n "akso") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k5b1azl763200x970w7k7dabi4dwjj032l64dh41yl8wq6j0vzz")))

