(define-module (crates-io ak ko akkorders) #:use-module (crates-io))

(define-public crate-akkorders-0.1.0 (c (n "akkorders") (v "0.1.0") (h "11pzrx6v4885wsi0cfnnwr8c7w3awmqg4h40qbz87bgaql1wnnfz")))

(define-public crate-akkorders-0.1.1 (c (n "akkorders") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)))) (h "1llw28d8xlxw1alfajq941lb0qxv08qx58xsl5a7zrhas0jw5fax")))

