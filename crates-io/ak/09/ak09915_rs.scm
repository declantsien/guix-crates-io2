(define-module (crates-io ak #{09}# ak09915_rs) #:use-module (crates-io))

(define-public crate-ak09915_rs-0.1.0 (c (n "ak09915_rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0rnls4gcjrv719h3xx3ai18a0radgx2f4if84yqy7n9v51hwwjbc")))

(define-public crate-ak09915_rs-0.1.1 (c (n "ak09915_rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "077lr4zrisd0x8y6qx6fcfyg9zc6m72wxvc8559yy0rjh9xrkmvl")))

(define-public crate-ak09915_rs-0.2.0 (c (n "ak09915_rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0mykzggdgwpzhwy7lnf1cn5h9vf84jpwh770hh99xqc2y65xl3ss")))

