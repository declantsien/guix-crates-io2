(define-module (crates-io ak in akinator-py) #:use-module (crates-io))

(define-public crate-akinator-py-0.1.0 (c (n "akinator-py") (v "0.1.0") (d (list (d (n "akinator-rs") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "^0.16") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync"))) (d #t) (k 0)))) (h "09n5lllhsp94g3imapcg095xcc3x5gp90cw7xrmbg96vzzxrshqc")))

(define-public crate-akinator-py-0.2.0 (c (n "akinator-py") (v "0.2.0") (d (list (d (n "akinator-rs") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "^0.16") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0ik5lhinmwqpbrmmqf83zjjc9pqssg6chyhga361iy26pfljnscl")))

(define-public crate-akinator-py-0.2.1 (c (n "akinator-py") (v "0.2.1") (d (list (d (n "akinator-rs") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "pyo3-asyncio") (r "^0.16") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "00jp3azs42g324cgnsm51dzjrvwm2kfwpy5hg67af1y94mkj3ddl")))

