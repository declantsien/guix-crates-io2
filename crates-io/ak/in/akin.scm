(define-module (crates-io ak in akin) #:use-module (crates-io))

(define-public crate-akin-0.1.0 (c (n "akin") (v "0.1.0") (h "1g47p75c4gmambr49ainx3d99hvnz9yvyyvwgvh37r03ra3kf2s6")))

(define-public crate-akin-0.1.1 (c (n "akin") (v "0.1.1") (h "103yys33p1pdvim4afgryld2cryfq8f67kx6cgjncx2wy3z3gvxs")))

(define-public crate-akin-0.1.2 (c (n "akin") (v "0.1.2") (h "0b45ibj50vhkmyiwh2hqf2by9wf9lcgwsz93klr2riddksyngfaz")))

(define-public crate-akin-0.1.3 (c (n "akin") (v "0.1.3") (h "1gv8fhgjakhhxza9wsa1w1mdh8v6dnjk7kv8ppcl03jkchisxhmr")))

(define-public crate-akin-0.1.4 (c (n "akin") (v "0.1.4") (h "1qwr46m170qmbrpc1gs49r7naiv7xjim41sn7kcm95l35l08h293")))

(define-public crate-akin-0.2.0 (c (n "akin") (v "0.2.0") (h "09l8r41dnh9rsp74ml2h9j5dvkid2lyrwp81nbsqrak86da9yprm")))

(define-public crate-akin-0.2.1 (c (n "akin") (v "0.2.1") (h "159b6ll0qnjxm83czk8dqg3hxiz8cwzgjpgiq92wrz9ka45fy5mw")))

(define-public crate-akin-0.2.2 (c (n "akin") (v "0.2.2") (h "0fc9s001h08g96l67jy1xqrg60pkrnnlcqg4dzr8rny842l1rdcj")))

(define-public crate-akin-0.2.3 (c (n "akin") (v "0.2.3") (h "1dhsya3jmcn9qyfy68l48qqyia4x8zh3zvfc4zgc94m51nk9nfx8")))

(define-public crate-akin-0.3.0 (c (n "akin") (v "0.3.0") (h "0g9gy9vpn03jpfc4gn6hwslgxf4lq7m2jkrk95pl1ihpfbzaq3b8")))

(define-public crate-akin-0.3.1 (c (n "akin") (v "0.3.1") (h "0dzwx4i7rk3vq6cqwnrb0m6qxwgigxnhs2qq9pfagrn5lrznbfaf")))

(define-public crate-akin-0.3.2 (c (n "akin") (v "0.3.2") (h "03yg8mc916g21fd4xmxps3wd79lgyg2aj84yi0ll08fmb97i0qg8")))

(define-public crate-akin-0.3.3 (c (n "akin") (v "0.3.3") (h "0yy6pxlg3vfir271znjk5qsh4b0zd13p3irkpzx71mgixfsb2nqn")))

(define-public crate-akin-0.4.0 (c (n "akin") (v "0.4.0") (h "0bsdiggkln1bbk633mwrfajfr5amvsimdz0y0p7m8ra1q4pnjqqp")))

