(define-module (crates-io ak tr aktrs-macros) #:use-module (crates-io))

(define-public crate-aktrs-macros-0.1.0-alpha.1 (c (n "aktrs-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03vzh201lfi4ypbil8ch4jdqi9ppybmy4sxlhnbxyc00p05zhyhl")))

(define-public crate-aktrs-macros-0.1.0-alpha.2 (c (n "aktrs-macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nd4k37hf8wm699nfkvi24sjdpk7v6rhw8pcfrv2nbpcv8zvkgsv")))

