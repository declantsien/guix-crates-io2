(define-module (crates-io ak -c ak-codegen) #:use-module (crates-io))

(define-public crate-ak-codegen-0.1.0 (c (n "ak-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1aq71l3rl6ky4nzhlaxc5z2vhhkg84f56fprqd0ap6hyxrdrmn3b")))

(define-public crate-ak-codegen-0.1.1 (c (n "ak-codegen") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ki234n7d3rhy8sd38552j2dhphhrdm1ki029xchnvl20lb36cbd")))

