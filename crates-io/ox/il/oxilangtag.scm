(define-module (crates-io ox il oxilangtag) #:use-module (crates-io))

(define-public crate-oxilangtag-0.1.0 (c (n "oxilangtag") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03s0vjdzgx6qwa21anmd9badi9arqffs3ycm4lz5k5p750v6ffc9") (y #t)))

(define-public crate-oxilangtag-0.1.1 (c (n "oxilangtag") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0lfli3bybr2x9xvpr8d9bjvxb5xw4ipni8zac7krlx1bw2f1xfci") (y #t)))

(define-public crate-oxilangtag-0.1.2 (c (n "oxilangtag") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05jka0yl3jkdqqadb87cvi1fzl3hwmwgwc6y6mcf4l9wkkplz6y5")))

(define-public crate-oxilangtag-0.1.3 (c (n "oxilangtag") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "06qqikyhdrp4i1n3wfr8pc2vz4f4x2j4a4s78fa2fw5rzgsfv4cd") (f (quote (("serialize" "serde") ("default"))))))

(define-public crate-oxilangtag-0.1.4 (c (n "oxilangtag") (v "0.1.4") (d (list (d (n "criterion") (r ">=0.4, <0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.100") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1x50sm7c7j8qixy19g7sm5r3x1j9h2m0pjl9cmc30l3gjqaz04bl") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serialize" "dep:serde") ("alloc" "serde?/alloc")))) (r "1.63")))

(define-public crate-oxilangtag-0.1.5 (c (n "oxilangtag") (v "0.1.5") (d (list (d (n "criterion") (r ">=0.4, <0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.100") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1jwa1z5223hkfldjdwfrxb158w9y918667k9ldzzfsm82xvgiwr3") (f (quote (("serialize" "serde") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("alloc" "serde?/alloc")))) (r "1.63")))

