(define-module (crates-io ox il oxilib) #:use-module (crates-io))

(define-public crate-oxilib-0.1.0 (c (n "oxilib") (v "0.1.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1d2hix5khf2lmqf02db164y1581y1230l4pglb4p38b62f0h5a8c")))

(define-public crate-oxilib-0.1.1 (c (n "oxilib") (v "0.1.1") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "01swkcw3mrykqs3szcas859kdr67sj2q2zpr7i82iz79l7i92miw")))

(define-public crate-oxilib-0.1.2 (c (n "oxilib") (v "0.1.2") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1c176l6n3l4mma6zcdsg1dmiaxyvni7p6kp0gkdbwfsd75ja10b7")))

