(define-module (crates-io ox cc oxcc-stm32f767) #:use-module (crates-io))

(define-public crate-oxcc-stm32f767-0.1.0 (c (n "oxcc-stm32f767") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "05q13y7d42qjwz05a4h5wqwhn356x5nvjzp7k5cjf7q0z8qsj9d4") (f (quote (("rt" "cortex-m-rt/device"))))))

