(define-module (crates-io ox cc oxcc-stm32f767-hal) #:use-module (crates-io))

(define-public crate-oxcc-stm32f767-hal-0.1.0 (c (n "oxcc-stm32f767-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded_types") (r "^0.3.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "oxcc-stm32f767") (r "^0.1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0dplm2hyaaf9jdnyrh519nvamfvqnkbgksxskg7jw59j2g7w24dh") (f (quote (("rt" "oxcc-stm32f767/rt"))))))

(define-public crate-oxcc-stm32f767-hal-0.2.0 (c (n "oxcc-stm32f767-hal") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded_types") (r "^0.3.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "oxcc-stm32f767") (r "^0.1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0klxb33a4dncbmhacljqnyc65abpjjsc7v1iwzqws15zqsydjhgr") (f (quote (("rt" "oxcc-stm32f767/rt"))))))

