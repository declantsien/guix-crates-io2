(define-module (crates-io ox er oxerun) #:use-module (crates-io))

(define-public crate-oxerun-0.0.0-pre (c (n "oxerun") (v "0.0.0-pre") (d (list (d (n "rlibc") (r "^1") (d #t) (k 0)) (d (n "xen") (r "^0.0.0-pre") (d #t) (k 0)) (d (n "xen-sys") (r "^0.0.0-pre") (d #t) (k 0)))) (h "1jp023wvyfdbrmczg4icidp3srymzmvc3ly3q9al62mzdij3zdn1")))

(define-public crate-oxerun-0.0.0-pre1 (c (n "oxerun") (v "0.0.0-pre1") (d (list (d (n "xen") (r "^0.0.0-pre") (d #t) (k 0)) (d (n "xen-sys") (r "^0.0.0-pre") (d #t) (k 0)))) (h "04lv7ax2rk2mw7wh2gqm229yzkr8clciamq0fz9gb13nffyms5px")))

