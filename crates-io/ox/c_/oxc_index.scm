(define-module (crates-io ox c_ oxc_index) #:use-module (crates-io))

(define-public crate-oxc_index-0.0.7 (c (n "oxc_index") (v "0.0.7") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1zc4gmvin31g8xdbmjx4lbzhadv8mwhmb8vhha1kh2dh5q0ml5mc")))

(define-public crate-oxc_index-0.1.0 (c (n "oxc_index") (v "0.1.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1z9zmkr2pcbqi2g93nxnilbms2dyc9xhb7c4ha5g8hhx4by23h7d") (r "1.60")))

(define-public crate-oxc_index-0.1.1 (c (n "oxc_index") (v "0.1.1") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1h1gasrnqpjigjfbv4s6kh223wixnyzxfff0fz1ll32yziw454fq") (r "1.60")))

(define-public crate-oxc_index-0.1.2 (c (n "oxc_index") (v "0.1.2") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0591jblz923qx5yyqh1j4dg3dlqxfchw9zp31mr9rd238q0y3ihv") (r "1.60")))

(define-public crate-oxc_index-0.1.3 (c (n "oxc_index") (v "0.1.3") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "09kzqcvm28m1xlp0d3mxhs7yjffqi38ncd4jwha9qhw8i4h5x41g") (r "1.60")))

(define-public crate-oxc_index-0.2.0 (c (n "oxc_index") (v "0.2.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "16pn06qqxjrk08ymqplip4n4rbapm9blib71w465nb7n6fwa5dzc") (r "1.60")))

(define-public crate-oxc_index-0.3.0 (c (n "oxc_index") (v "0.3.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lgpajhkjis3jibzk8isiz0w6y00z5vdl79lv2hjra5ilql9yivw") (r "1.60")))

(define-public crate-oxc_index-0.4.0 (c (n "oxc_index") (v "0.4.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1cpvcl2xh8czc4lcwvrvvh80ysc030p032mf4zc3d3nmhkjifr2c") (r "1.60")))

(define-public crate-oxc_index-0.5.0 (c (n "oxc_index") (v "0.5.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "01bpzmc2i8hh1w6ccgxpc6qa5pvmkca80knb2rjb6bs9dw3j84r2") (f (quote (("serde" "index_vec/serde") ("default")))) (r "1.60")))

(define-public crate-oxc_index-0.6.0 (c (n "oxc_index") (v "0.6.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0isyswbzpyz57105q3dzs8fhzvdqs6ammqwjr0xrz3xhfpd0fvcz") (f (quote (("serde" "index_vec/serde") ("default")))) (r "1.60")))

(define-public crate-oxc_index-0.7.0 (c (n "oxc_index") (v "0.7.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1sb6d7zs6rxvpy4hb7mpgmh78w0z8qkqrmvb1fmgz0zapa3h00a0") (f (quote (("serde" "index_vec/serde") ("default")))) (r "1.60")))

(define-public crate-oxc_index-0.8.0 (c (n "oxc_index") (v "0.8.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0r9c9xj2qkvvb43rbkwjq1zj83bz1r1865s1c716b2p6pm4l2ri3") (f (quote (("serde" "index_vec/serde") ("default")))) (r "1.60")))

(define-public crate-oxc_index-0.9.0 (c (n "oxc_index") (v "0.9.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lfpw1718ayxf5jq7ap79hp8vn0r7ly3y9rzsiawyrahm12br7h9") (f (quote (("serde" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.10.0 (c (n "oxc_index") (v "0.10.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0qnqk13r7nd7skp43qk3qk004zffvb9zz579yh6l7yy996ppmz3r") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.11.0 (c (n "oxc_index") (v "0.11.0") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1g1d97bxi6nm32a87lc0wha7lkjm72i24wf79p5s3ff7nglykkah") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.11.1 (c (n "oxc_index") (v "0.11.1") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0bdbl3kj24fnlw5qz6baqfh75ir7ybkbd47d46pgsh3xd1if2l04") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.12.1 (c (n "oxc_index") (v "0.12.1") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0jxv7xrrcw7b4jvc8k25pc025lgjmgmpxqq1v2dkrgh7qsxx14qg") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.12.2 (c (n "oxc_index") (v "0.12.2") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1634v150zsq9nn0dczrcq6rakz0l3vq3jk76y2smi0na4zl8gs54") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.12.3 (c (n "oxc_index") (v "0.12.3") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "087ai3wd8ydq3rd9s2hlaasvk8iy8d8rabn5pn5p1jv49vh93aj5") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.12.4 (c (n "oxc_index") (v "0.12.4") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1drnqp9xw68vsshg6f6pa8nv3s07vahdncwbbja2y8ch1fxp2b1f") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.12.5 (c (n "oxc_index") (v "0.12.5") (d (list (d (n "index_vec") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "004bvdy1g2ggb6jgflkpmbhc3rm1lp5w4ksbcb0mc9j1yqfjzsql") (f (quote (("serialize" "index_vec/serde") ("default")))) (r "1.74")))

(define-public crate-oxc_index-0.13.0 (c (n "oxc_index") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)))) (h "1s5ac5vidw8946ahi2wnsfjy9linr9yn7ajs3rz9h66qcwswldj8") (f (quote (("example_generated")))) (s 2) (e (quote (("serialize" "dep:serde")))) (r "1.74")))

(define-public crate-oxc_index-0.13.1 (c (n "oxc_index") (v "0.13.1") (d (list (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)))) (h "15jy4lq8rppwcklcdxc262cfcva83bilyn4lnhsmmcfcjgq07gy8") (f (quote (("example_generated")))) (s 2) (e (quote (("serialize" "dep:serde")))) (r "1.74")))

