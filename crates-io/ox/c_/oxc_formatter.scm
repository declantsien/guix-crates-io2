(define-module (crates-io ox c_ oxc_formatter) #:use-module (crates-io))

(define-public crate-oxc_formatter-0.0.7 (c (n "oxc_formatter") (v "0.0.7") (d (list (d (n "miette") (r "^5.9.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 2)) (d (n "oxc_allocator") (r "^0.0.7") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.0.7") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.0.7") (d #t) (k 2)) (d (n "oxc_span") (r "^0.0.7") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.0.7") (d #t) (k 0)))) (h "1j2lwzky5snryzyb0mxgr1mjadff8wabpa13qg594qfy6v6ld3h6") (y #t)))

(define-public crate-oxc_formatter-0.1.0 (c (n "oxc_formatter") (v "0.1.0") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 2)) (d (n "oxc_allocator") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "1dy3a5nd15fiflnqxv5dbg62gmvcljnbwlpkyn43l2j09d67w3wa") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.1.1 (c (n "oxc_formatter") (v "0.1.1") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 2)) (d (n "oxc_allocator") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "1f2fbvm6n0f3szzwpbhyrj9mi3dhfdd17r2d5lnd5s3n3yws8wgz") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.1.2 (c (n "oxc_formatter") (v "0.1.2") (d (list (d (n "oxc_allocator") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.1.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.1.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "09qqcb9hjddih4dnnlxaqqbqggbzqlq0viwqrj0gv5wqhr5qw8b7") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.1.3 (c (n "oxc_formatter") (v "0.1.3") (d (list (d (n "oxc_allocator") (r "^0.1.3") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.1.3") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.1.3") (d #t) (k 2)) (d (n "oxc_span") (r "^0.1.3") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.1.3") (d #t) (k 0)))) (h "0qgq925d6a8msjswq8kqk93lrmhcsrsv9rnjajdp1pbqf0za4mvc") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.2.0 (c (n "oxc_formatter") (v "0.2.0") (d (list (d (n "oxc_allocator") (r "^0.2.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.2.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.2.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.2.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.2.0") (d #t) (k 0)))) (h "01x80gcjxv11n70phgijbrzmip5c54w73r175i3cpn0gsk1v6x5b") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.3.0 (c (n "oxc_formatter") (v "0.3.0") (d (list (d (n "oxc_allocator") (r "^0.3.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.3.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.3.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.3.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.3.0") (d #t) (k 0)))) (h "0bz5pr2wff914agypyx66fjm1rc2jylvpdr5xr6f8950cqs8g4ry") (y #t) (r "1.60")))

(define-public crate-oxc_formatter-0.4.0 (c (n "oxc_formatter") (v "0.4.0") (d (list (d (n "oxc_allocator") (r "^0.4.0") (d #t) (k 0)) (d (n "oxc_ast") (r "^0.4.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.4.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.4.0") (d #t) (k 0)) (d (n "oxc_syntax") (r "^0.4.0") (d #t) (k 0)))) (h "1zhh0gc13j6g3jl05bk3ggabll9f5sjgbhwlxfhlbkplglzr67p4") (y #t) (r "1.60")))

