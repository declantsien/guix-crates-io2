(define-module (crates-io ox c_ oxc_allocator) #:use-module (crates-io))

(define-public crate-oxc_allocator-0.0.1 (c (n "oxc_allocator") (v "0.0.1") (d (list (d (n "bumpalo") (r "^3.12.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (d #t) (k 0)))) (h "02g0cpd5dl0h77vrbn6b683rffppyrdzna71mvr6m8wn0kzw1r73")))

(define-public crate-oxc_allocator-0.0.6 (c (n "oxc_allocator") (v "0.0.6") (d (list (d (n "bumpalo") (r "^3.12.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "1lzhzq8ljcg1n5ad61xa9b0x1jnm3px0167yf6av9r72sra7mbxy")))

(define-public crate-oxc_allocator-0.0.7 (c (n "oxc_allocator") (v "0.0.7") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "124sq9w016kkahg2sm3br1y4chfjzgadps91mbwzm69r46g57gx3")))

(define-public crate-oxc_allocator-0.1.0 (c (n "oxc_allocator") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "03v2vls3mh6p2id1gpyx3198l5crsnmfmha1v731smydzax9fdkd") (r "1.60")))

(define-public crate-oxc_allocator-0.1.1 (c (n "oxc_allocator") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "069frk919nlxjpggm9g7pilnfmjmrgp5l3v8cl8w158ra36w0nq6") (r "1.60")))

(define-public crate-oxc_allocator-0.1.2 (c (n "oxc_allocator") (v "0.1.2") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "1m9ds4q1zfp9hx6v336pj27c0v3wfy27kgdvb25ij6p5pscs8ibj") (r "1.60")))

(define-public crate-oxc_allocator-0.1.3 (c (n "oxc_allocator") (v "0.1.3") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "0y253ahrkdw7rcqh5qxacrmy5pq2habpkkvrbxnd7dj7s1k4inwa") (r "1.60")))

(define-public crate-oxc_allocator-0.2.0 (c (n "oxc_allocator") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.13.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "1gg88jvg1i9w3nvpgd9a1pfx0hnlgbiivlv18i2ip1rlncnva31d") (r "1.60")))

(define-public crate-oxc_allocator-0.3.0 (c (n "oxc_allocator") (v "0.3.0") (d (list (d (n "bumpalo") (r "^3.14.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0sk6fy23rcsr39m18bipm27pnlrss7jf5lq95vz6z7f1gp96f713") (r "1.60")))

(define-public crate-oxc_allocator-0.4.0 (c (n "oxc_allocator") (v "0.4.0") (d (list (d (n "bumpalo") (r "^3.14.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "107ra3q23sip6yqfpn0bf2klw46in80qvmigrfsgqgc0yiqy3ych") (r "1.60")))

(define-public crate-oxc_allocator-0.5.0 (c (n "oxc_allocator") (v "0.5.0") (d (list (d (n "bumpalo") (r "^3.14.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "16v2rszv2dllb68wnw7nm35kgbqlah4vgvid9893604b3517gw3a") (r "1.60")))

(define-public crate-oxc_allocator-0.6.0 (c (n "oxc_allocator") (v "0.6.0") (d (list (d (n "bumpalo") (r "^3.14.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "0d9lmsvpd4bgflkw6zp03x9n5v0w6n3c2brps01623zdq96psx0k") (r "1.60")))

(define-public crate-oxc_allocator-0.7.0 (c (n "oxc_allocator") (v "0.7.0") (d (list (d (n "bumpalo") (r "^3.14.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "184yyqd2jl4r02f7jypl5d1cx41sy5fwjkrnj9ihxjz9w2r628al") (r "1.60")))

(define-public crate-oxc_allocator-0.8.0 (c (n "oxc_allocator") (v "0.8.0") (d (list (d (n "bumpalo") (r "^3.15.3") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "02hza27r8m3whc6bpjh7n595w0c5blnfi7k2pdazwqwkd1xpag5k") (r "1.60")))

(define-public crate-oxc_allocator-0.9.0 (c (n "oxc_allocator") (v "0.9.0") (d (list (d (n "bumpalo") (r "^3.15.3") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "10yfha1jfizmg61gdl3shkr1dh27z4szdzcrj5ahsmm00hc6hxsn") (r "1.74")))

(define-public crate-oxc_allocator-0.10.0 (c (n "oxc_allocator") (v "0.10.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "06y39mn35bpbqv03wlk3wwr8kl64zzrgphd1f9w15xvaa2827swy") (r "1.74")))

(define-public crate-oxc_allocator-0.11.0 (c (n "oxc_allocator") (v "0.11.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "09hksllvfx9j3k0l9skav9ssryzdimznl7nkrbkb96ac885js4qp") (r "1.74")))

(define-public crate-oxc_allocator-0.11.1 (c (n "oxc_allocator") (v "0.11.1") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "1kh097bf76xn60zl8z0d82xr1x6yxic1brywqmsxasha69kdswk5") (r "1.74")))

(define-public crate-oxc_allocator-0.12.1 (c (n "oxc_allocator") (v "0.12.1") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "0hydq1177n63k395xyjx5pibrichlkvmkd75bpddc1096mpwhfyn") (r "1.74")))

(define-public crate-oxc_allocator-0.12.2 (c (n "oxc_allocator") (v "0.12.2") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "15wyhad63620hd23wiavgcp7nqkhd0kpdzan5nhp87l9lcmp1a7p") (r "1.74")))

(define-public crate-oxc_allocator-0.12.3 (c (n "oxc_allocator") (v "0.12.3") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "0bbf0zaii9h6ha3rii17hnbpywvyk9ci005rs7j7ls5jr21424nc") (r "1.74")))

(define-public crate-oxc_allocator-0.12.4 (c (n "oxc_allocator") (v "0.12.4") (d (list (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 0)) (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections" "allocator-api2"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "1ifxyn4aanp38b1731ns913mj43ii3yp7d3pbak4jz1hpbdqrvwp") (r "1.74")))

(define-public crate-oxc_allocator-0.12.5 (c (n "oxc_allocator") (v "0.12.5") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)) (d (n "bumpalo") (r "^3.16.0") (f (quote ("collections" "allocator-api2"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "1a8w390jvl8r76qxnsbrqgzjm51z7w1rmcxmh8j1kmw3p8lbs13y") (r "1.74")))

(define-public crate-oxc_allocator-0.13.0 (c (n "oxc_allocator") (v "0.13.0") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)) (d (n "bumpalo") (r "^3.16.0") (f (quote ("collections" "allocator-api2"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "0w4h43wz1wsfs100sm0xzj1mq23lpqgqr2q1z15ajcw9m1xlgg8z") (s 2) (e (quote (("serialize" "dep:serde")))) (r "1.74")))

(define-public crate-oxc_allocator-0.13.1 (c (n "oxc_allocator") (v "0.13.1") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)) (d (n "bumpalo") (r "^3.16.0") (f (quote ("allocator-api2" "collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "0gg33gkg4x97bnkdcqwxngk7vq8ksy46bqv1y5jxyadmjvnsppah") (s 2) (e (quote (("serialize" "dep:serde")))) (r "1.74")))

