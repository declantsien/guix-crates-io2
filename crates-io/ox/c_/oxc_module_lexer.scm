(define-module (crates-io ox c_ oxc_module_lexer) #:use-module (crates-io))

(define-public crate-oxc_module_lexer-0.0.1 (c (n "oxc_module_lexer") (v "0.0.1") (d (list (d (n "oxc_allocator") (r "^0.9.0") (d #t) (k 2)) (d (n "oxc_ast") (r "^0.9.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.9.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.9.0") (d #t) (k 0)))) (h "0gs1cj48rglpnm2xp3589h3v3hsan7w15kdp1jzal0zkbxsgl3mb") (r "1.74")))

(define-public crate-oxc_module_lexer-0.13.0 (c (n "oxc_module_lexer") (v "0.13.0") (d (list (d (n "oxc_allocator") (r "^0.13.0") (d #t) (k 2)) (d (n "oxc_ast") (r "^0.13.0") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.13.0") (d #t) (k 2)) (d (n "oxc_span") (r "^0.13.0") (d #t) (k 0)))) (h "0zyiwwx0liz2w27cp6m2rq828rcyzk7w9k00x05yx8b07zd0s7l7") (r "1.74")))

(define-public crate-oxc_module_lexer-0.13.1 (c (n "oxc_module_lexer") (v "0.13.1") (d (list (d (n "oxc_allocator") (r "^0.13.1") (d #t) (k 2)) (d (n "oxc_ast") (r "^0.13.1") (d #t) (k 0)) (d (n "oxc_parser") (r "^0.13.1") (d #t) (k 2)) (d (n "oxc_span") (r "^0.13.1") (d #t) (k 0)))) (h "1pdrwzw5bscx79ybbas8hh96vihaz46wyr6rsh9zr6x93hllvwqq") (r "1.74")))

