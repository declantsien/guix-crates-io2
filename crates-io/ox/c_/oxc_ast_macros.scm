(define-module (crates-io ox c_ oxc_ast_macros) #:use-module (crates-io))

(define-public crate-oxc_ast_macros-0.13.0 (c (n "oxc_ast_macros") (v "0.13.0") (h "0ir6fxv7x5d5cs2ivjqkqqhaakk0s6ysf8gjizf9w4hav6fa7rac") (r "1.74")))

(define-public crate-oxc_ast_macros-0.13.1 (c (n "oxc_ast_macros") (v "0.13.1") (h "00vx19ffnc0xc39qz6yvw0x4nycnv9hdrcw2cgkpink72a2r1vvx") (r "1.74")))

