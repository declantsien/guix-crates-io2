(define-module (crates-io ox id oxide-auth-axum) #:use-module (crates-io))

(define-public crate-oxide-auth-axum-0.1.0 (c (n "oxide-auth-axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.4") (d #t) (k 0)) (d (n "oxide-auth") (r "^0.5") (d #t) (k 0)))) (h "0889l6838381pmwli09sfj01vkwxam5lg43ch0kffaw9rc9c58kp")))

(define-public crate-oxide-auth-axum-0.2.0 (c (n "oxide-auth-axum") (v "0.2.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "oxide-auth") (r "^0.5") (d #t) (k 0)))) (h "10vm7alzdwdlfhxvam0zpapidx3l2sl5v9hwnkbvh360a3s31ibf")))

(define-public crate-oxide-auth-axum-0.3.0 (c (n "oxide-auth-axum") (v "0.3.0") (d (list (d (n "axum") (r "^0.6") (f (quote ("form" "query"))) (k 0)) (d (n "oxide-auth") (r "^0.5") (d #t) (k 0)))) (h "1k1dwl082lnafhwmsskm1j2c0wh1rmdgig7s43pzws2jj6d59i7m")))

(define-public crate-oxide-auth-axum-0.4.0 (c (n "oxide-auth-axum") (v "0.4.0") (d (list (d (n "axum") (r "^0.7") (f (quote ("form" "query"))) (k 0)) (d (n "oxide-auth") (r "^0.5") (d #t) (k 0)))) (h "1q9lrpj1s0r3cqpqip0wrar11gy5zr3xk6rl303634bsjy8c34im")))

(define-public crate-oxide-auth-axum-0.5.0 (c (n "oxide-auth-axum") (v "0.5.0") (d (list (d (n "axum") (r "^0.7") (f (quote ("form" "query"))) (k 0)) (d (n "oxide-auth") (r "^0.6") (d #t) (k 0)))) (h "1f5wpw4mprlx11fwcpzaddcq97pnx1x4lgx39fr6qn8jw81h6px6")))

