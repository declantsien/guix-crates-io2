(define-module (crates-io ox id oxidd-rules-zbdd) #:use-module (crates-io))

(define-public crate-oxidd-rules-zbdd-0.6.0 (c (n "oxidd-rules-zbdd") (v "0.6.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "oxidd-derive") (r "^0.6") (k 0)) (d (n "oxidd-dump") (r "^0.1") (f (quote ("dddmp" "dot"))) (k 0)))) (h "07yrdsfkddcy80vnavl8nfqv5gi9hxbw9bcisfpkgzvwwwc4j1n4") (f (quote (("statistics") ("multi-threading") ("default"))))))

