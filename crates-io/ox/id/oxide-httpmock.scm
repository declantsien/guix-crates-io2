(define-module (crates-io ox id oxide-httpmock) #:use-module (crates-io))

(define-public crate-oxide-httpmock-0.2.0 (c (n "oxide-httpmock") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "httpmock") (r "^0.6.8") (d #t) (k 0)) (d (n "oxide") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rzsjjn864rda83mnbcdvbwzfzfjm9c6lqkygsm1j45gp97j6zkf")))

(define-public crate-oxide-httpmock-0.3.0+0.0.6 (c (n "oxide-httpmock") (v "0.3.0+0.0.6") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "httpmock") (r "^0.6.8") (d #t) (k 0)) (d (n "oxide") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1snii2qx66lg2fp8vix551c61927j1bwqjf8fyjb71820wjdfnyi")))

(define-public crate-oxide-httpmock-0.4.0+20240327.0 (c (n "oxide-httpmock") (v "0.4.0+20240327.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "httpmock") (r "^0.6.8") (d #t) (k 0)) (d (n "oxide") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0a6c5q8b5qzbdqx9349nkiiwiphaqlwq5yakmpz4x5ykd1ihs6cl")))

(define-public crate-oxide-httpmock-0.5.0+20240502.0 (c (n "oxide-httpmock") (v "0.5.0+20240502.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "httpmock") (r "^0.6.8") (d #t) (k 0)) (d (n "oxide") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1q71x04x4gshz85zr3r5yq8j1azynmjivxw4khz0gsnvk88wq5j7")))

