(define-module (crates-io ox id oxidy) #:use-module (crates-io))

(define-public crate-oxidy-0.0.1 (c (n "oxidy") (v "0.0.1") (h "068vfiq0pfxcgz3j9cym8mf9g05av72snsqar3jfcsxc3dc8wz2w")))

(define-public crate-oxidy-0.1.0 (c (n "oxidy") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1548zzdr5mhqf1rp764hjissda5bpl7a8sr6kvm4l6b34ildxmlg")))

(define-public crate-oxidy-0.2.0 (c (n "oxidy") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0w051yjnx03cis8r26k17b1czyrfx12g859n403wh2rq3rhb6q4j")))

(define-public crate-oxidy-0.2.1 (c (n "oxidy") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1987zds81h1ppdpigckx3va9zibvmba4vsvz02lzcaw12m52rkqk")))

(define-public crate-oxidy-0.3.0 (c (n "oxidy") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0qis5gxc18s4kikzaqmfgnciwajw3b4z7dc2m60ydc4y75244f2w")))

(define-public crate-oxidy-0.3.1 (c (n "oxidy") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1pls1r1yxwprjwrin817ql9xfhbj1p3xwq3gfxv67c86vwrpsbbl")))

(define-public crate-oxidy-0.4.0 (c (n "oxidy") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "09qskidl7amdvdqjdbbzmr94lmsiks40cmdpg3hhpdak6hh36ly0")))

(define-public crate-oxidy-0.5.0 (c (n "oxidy") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt-multi-thread" "macros" "net" "io-util"))) (d #t) (k 0)))) (h "1v3jm63ddx6psi8b28nz0cvjvlrrk9i06n39w3wf6kvkif9dy23l")))

