(define-module (crates-io ox id oxidate-macros) #:use-module (crates-io))

(define-public crate-oxidate-macros-0.1.0 (c (n "oxidate-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "macroscope-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "peekmore") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0lz0ripivg45h693mbil72yivmrdwk1nanc4j7anic7jf238kbjb")))

