(define-module (crates-io ox id oxidd-rules-bdd) #:use-module (crates-io))

(define-public crate-oxidd-rules-bdd-0.6.0 (c (n "oxidd-rules-bdd") (v "0.6.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "oxidd-derive") (r "^0.6") (k 0)) (d (n "oxidd-dump") (r "^0.1") (f (quote ("dddmp" "dot"))) (k 0)))) (h "1pnck0n903wmqb98vi4jca081531afhskzfcigy9vacgyjx8707b") (f (quote (("statistics") ("simple") ("multi-threading") ("default" "simple" "complement-edge" "multi-threading") ("complement-edge"))))))

