(define-module (crates-io ox id oxidice) #:use-module (crates-io))

(define-public crate-oxiDice-0.1.0 (c (n "oxiDice") (v "0.1.0") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "string" "derive"))) (d #t) (k 0)) (d (n "ilog") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17645qpnj3hxwmcq28b29zvlqyccza4wk3q8xj8g4kbhq2n52m12")))

(define-public crate-oxiDice-1.0.0 (c (n "oxiDice") (v "1.0.0") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "string" "derive"))) (d #t) (k 0)) (d (n "ilog") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a6qhr5f371yj919ca2kvfvjnmnvyvzwyr24lc6zwhp512095ji1")))

(define-public crate-oxiDice-1.0.1 (c (n "oxiDice") (v "1.0.1") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "string" "derive"))) (d #t) (k 0)) (d (n "ilog") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m8nyg7anwhaygwjm74j57cwp3gz3c9h3vn2brlgllz0ra4530mb")))

(define-public crate-oxiDice-1.0.2 (c (n "oxiDice") (v "1.0.2") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "string" "derive"))) (d #t) (k 0)) (d (n "ilog") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yfls71ysafbbidnxzdvwcw95jn9rimdpigwgivsxg1c670wccr7")))

