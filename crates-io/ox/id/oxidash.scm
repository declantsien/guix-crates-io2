(define-module (crates-io ox id oxidash) #:use-module (crates-io))

(define-public crate-oxidash-0.1.0 (c (n "oxidash") (v "0.1.0") (d (list (d (n "adw") (r ">=0.3.1") (f (quote ("v1_2"))) (d #t) (k 0) (p "libadwaita")) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.2") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib-build-tools") (r "^0.18") (d #t) (k 1)) (d (n "gtk") (r "^0.6.6") (d #t) (k 0) (p "gtk4")) (d (n "gtk4-layer-shell") (r "^0.0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0w06vhjgqi46mhzk9kbnl2x55wyxf31nynkcvmjx6mq9r8g49xq7")))

