(define-module (crates-io ox id oxid_roblox) #:use-module (crates-io))

(define-public crate-oxid_roblox-0.1.0 (c (n "oxid_roblox") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.18") (d #t) (k 0)))) (h "1mgcz5ih8kmfx0lfwrm6sw4nfarvkpwhbwg2g6ahs075c10kmcq3")))

