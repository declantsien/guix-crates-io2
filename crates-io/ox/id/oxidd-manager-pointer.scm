(define-module (crates-io ox id oxidd-manager-pointer) #:use-module (crates-io))

(define-public crate-oxidd-manager-pointer-0.1.0 (c (n "oxidd-manager-pointer") (v "0.1.0") (d (list (d (n "arcslab") (r "^0.1") (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "linear-hashtbl") (r "^0.1") (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1rr397fzy8axvmc4ni04rdlwh8qf4qi5bf4pgbyk5l3rdjy1ba9k") (f (quote (("static_leak_check") ("default"))))))

