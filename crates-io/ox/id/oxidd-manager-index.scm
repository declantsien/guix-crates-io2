(define-module (crates-io ox id oxidd-manager-index) #:use-module (crates-io))

(define-public crate-oxidd-manager-index-0.6.0 (c (n "oxidd-manager-index") (v "0.6.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "linear-hashtbl") (r "^0.1") (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "0z57x2izpfzp9xkwmspcsvgfsgcr6m6rhr6h4b5lgb1h5wx6bkww") (f (quote (("statistics") ("static_leak_check") ("default"))))))

