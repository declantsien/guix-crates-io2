(define-module (crates-io ox id oxidd-derive) #:use-module (crates-io))

(define-public crate-oxidd-derive-0.6.0 (c (n "oxidd-derive") (v "0.6.0") (d (list (d (n "oxidd-core") (r "^0.6") (k 2)) (d (n "oxidd-test-utils") (r "^0.1") (k 2)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1hlzkb4x5py8saqz8h3cvy59c849qmlavy6j39596am22dy5klf8") (f (quote (("nightly-ui-tests") ("default"))))))

