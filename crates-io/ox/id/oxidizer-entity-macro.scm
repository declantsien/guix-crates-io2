(define-module (crates-io ox id oxidizer-entity-macro) #:use-module (crates-io))

(define-public crate-oxidizer-entity-macro-0.1.0 (c (n "oxidizer-entity-macro") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06mnr3ibxglsv6001b3mahpxj534rcwhd2kmfab4p2i0rra96xxa")))

(define-public crate-oxidizer-entity-macro-0.1.1 (c (n "oxidizer-entity-macro") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ip2zkb7w244inkqgw5dxfsr3gpvcrif8m4mk1w5d3q0lis7707s")))

(define-public crate-oxidizer-entity-macro-0.2.0 (c (n "oxidizer-entity-macro") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.38") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04xpras9vxxncqwjmk7v8wabmikfvfipz1m702f7d2b8iqkwkdaj")))

(define-public crate-oxidizer-entity-macro-0.2.1 (c (n "oxidizer-entity-macro") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.38") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cvw3fmpwmgkwlkrqm2g1sjz4arjadqa0gcd9xifq2p4bg46qylv")))

