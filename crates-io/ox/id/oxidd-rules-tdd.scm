(define-module (crates-io ox id oxidd-rules-tdd) #:use-module (crates-io))

(define-public crate-oxidd-rules-tdd-0.1.0 (c (n "oxidd-rules-tdd") (v "0.1.0") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "oxidd-derive") (r "^0.6") (k 0)) (d (n "oxidd-dump") (r "^0.1") (f (quote ("dddmp" "dot"))) (k 0)))) (h "0i7m5k0yzlcc7r3153qdax9f1bvnfrl12lvgl4m9x4f4i34l7vds") (f (quote (("statistics") ("multi-threading") ("default"))))))

