(define-module (crates-io ox id oxidd-dump) #:use-module (crates-io))

(define-public crate-oxidd-dump-0.1.0 (c (n "oxidd-dump") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "is_sorted") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "rustc-hash") (r "^1") (o #t) (d #t) (k 0)))) (h "02b6gkrfk4lqzc30xzr5gkcvimk10qw62s7yy0vkh0irdhfh911f") (f (quote (("dot") ("default" "dddmp" "dot") ("dddmp" "bitvec" "rustc-hash" "memchr" "is_sorted"))))))

