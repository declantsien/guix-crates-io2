(define-module (crates-io ox id oxidation_bencode) #:use-module (crates-io))

(define-public crate-oxidation_bencode-0.1.0 (c (n "oxidation_bencode") (v "0.1.0") (h "1fk9bddyhx99y7f035ns3j3v4gbqscwvziah40qp5ygckhqijl38")))

(define-public crate-oxidation_bencode-0.2.0 (c (n "oxidation_bencode") (v "0.2.0") (h "0wydbhp3lp8mjd6lypml8ysxss3k09915ld5wc0ckdckgbkjclpb")))

