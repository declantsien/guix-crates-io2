(define-module (crates-io ox id oxidized_make) #:use-module (crates-io))

(define-public crate-oxidized_make-0.1.0 (c (n "oxidized_make") (v "0.1.0") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r ">=0.2.26") (d #t) (k 0)))) (h "0mvd78ac2c45vngn49mw4rn7lfc4hbyfga9ff1a19s0zc6msgygr") (f (quote (("omake") ("make") ("default" "make")))) (y #t)))

(define-public crate-oxidized_make-0.0.0 (c (n "oxidized_make") (v "0.0.0") (h "1b2wxkyqyc3dfqbch8056x0q56h91sm0bqjwiq2c2mj2s4979bh3")))

