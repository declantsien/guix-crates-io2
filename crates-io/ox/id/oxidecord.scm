(define-module (crates-io ox id oxidecord) #:use-module (crates-io))

(define-public crate-oxidecord-0.0.1 (c (n "oxidecord") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.7.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.6.1") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0dc67mka5fmqp1bhrpbkn8si4k0d1l77dykn2d0i71xzmknqd8hk")))

