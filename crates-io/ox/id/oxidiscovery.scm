(define-module (crates-io ox id oxidiscovery) #:use-module (crates-io))

(define-public crate-oxidiscovery-0.1.0 (c (n "oxidiscovery") (v "0.1.0") (h "1x9cq7dmzalfxpxh9r0l2sif2wscdm9i7hfggpb7v2w2k5zsk7j2")))

(define-public crate-oxidiscovery-0.1.1 (c (n "oxidiscovery") (v "0.1.1") (h "06219bqx5095kcnif3j2snsycv0rrpj3j40295vf2ffdblzypysf")))

(define-public crate-oxidiscovery-0.1.2 (c (n "oxidiscovery") (v "0.1.2") (h "1lhmglbr8yr2y2w3s9hwmi3g2pp9cx7gqfsjidca1rq1zhbjgp0a")))

(define-public crate-oxidiscovery-0.1.3 (c (n "oxidiscovery") (v "0.1.3") (h "1j75lwfvnz1xbxg0w5d229c2pl499528b0pm11w6f50c3b37bfd0")))

