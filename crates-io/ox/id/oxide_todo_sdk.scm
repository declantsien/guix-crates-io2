(define-module (crates-io ox id oxide_todo_sdk) #:use-module (crates-io))

(define-public crate-oxide_todo_sdk-0.1.0-pre.0 (c (n "oxide_todo_sdk") (v "0.1.0-pre.0") (d (list (d (n "reqwest") (r "=0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "=1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "=1.3.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "197bvsrl22f8q3hn7gyr07ixl10qsr9w4lvmkmywdmrhm547n24c") (f (quote (("debug")))) (r "1.64.0")))

