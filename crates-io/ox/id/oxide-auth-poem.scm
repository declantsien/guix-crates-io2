(define-module (crates-io ox id oxide-auth-poem) #:use-module (crates-io))

(define-public crate-oxide-auth-poem-0.1.0 (c (n "oxide-auth-poem") (v "0.1.0") (d (list (d (n "oxide-auth") (r "^0.5") (d #t) (k 0)) (d (n "poem") (r "^1.3") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02908d9m0fyrj7985lkcpvvqxxmkbcf1f9h9hm7v92mrmdnspq2a")))

(define-public crate-oxide-auth-poem-0.2.0 (c (n "oxide-auth-poem") (v "0.2.0") (d (list (d (n "oxide-auth") (r "^0.6") (d #t) (k 0)) (d (n "poem") (r "^1.3") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09rglxdf6z7iankzrysd17l8rc5ffas4hh09gv4b27mj1nrfhksy")))

