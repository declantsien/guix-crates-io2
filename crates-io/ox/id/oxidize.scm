(define-module (crates-io ox id oxidize) #:use-module (crates-io))

(define-public crate-oxidize-0.1.0 (c (n "oxidize") (v "0.1.0") (h "0ry1nhv32sl02ibzmq3q7n85vsjag1jb6lpk2z3prrv7n854wdb7")))

(define-public crate-oxidize-0.2.0 (c (n "oxidize") (v "0.2.0") (h "03mslhbi5frd38b34wnl3kg6fh89nbpd714994ch6fpb35djqqxs")))

(define-public crate-oxidize-0.2.1 (c (n "oxidize") (v "0.2.1") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "1zahv3q4wv4762ffk3mc06vzpxb4c0wsivqz2zbp70ggpmc87gzz")))

