(define-module (crates-io ox id oxide-auth-rouille) #:use-module (crates-io))

(define-public crate-oxide-auth-rouille-0.1.0-rc.0 (c (n "oxide-auth-rouille") (v "0.1.0-rc.0") (d (list (d (n "oxide-auth") (r "^0.5.0-preview.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "rouille") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "132h67n1ywlvync5salzh1dlr4aqbp5zdg0d4wq7n8d69wck3i1y")))

(define-public crate-oxide-auth-rouille-0.1.0 (c (n "oxide-auth-rouille") (v "0.1.0") (d (list (d (n "oxide-auth") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "rouille") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "08ijqb2pz896jg9l2qhk6h1f5fm7ka1p1kgxaqaz7djx9wb6qrim")))

(define-public crate-oxide-auth-rouille-0.2.0 (c (n "oxide-auth-rouille") (v "0.2.0") (d (list (d (n "oxide-auth") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "rouille") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "10m56iq15ig83zblqzw345j3s4d5sgr22nq8afzv98w2iyv3spnc")))

