(define-module (crates-io ox id oxidefurnace) #:use-module (crates-io))

(define-public crate-oxidefurnace-0.1.0 (c (n "oxidefurnace") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)))) (h "18fglzdf5z5jkldr1b747x3hgji7nvbnkx9qrb2bvmqak1fdgj0a")))

(define-public crate-oxidefurnace-0.1.1 (c (n "oxidefurnace") (v "0.1.1") (d (list (d (n "hidapi") (r "^2.0.2") (d #t) (k 0)))) (h "1y43ymf8s98z991xg2n5pn5shfw61j929xirgpriqfsim262gzrf")))

