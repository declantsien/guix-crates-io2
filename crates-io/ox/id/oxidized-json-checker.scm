(define-module (crates-io ox id oxidized-json-checker) #:use-module (crates-io))

(define-public crate-oxidized-json-checker-0.1.0 (c (n "oxidized-json-checker") (v "0.1.0") (d (list (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "0rzn1g3m7psch6rnhbwili6hbid4w3ca6jl6rh3yx6b8cqkgplzj")))

(define-public crate-oxidized-json-checker-0.2.0 (c (n "oxidized-json-checker") (v "0.2.0") (d (list (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "08d6gm5qmkgysgqwvb3y93mhllh7bbcl6yc8i6vd94l6pg4vp3jw")))

(define-public crate-oxidized-json-checker-0.3.0 (c (n "oxidized-json-checker") (v "0.3.0") (d (list (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "0xil9xhxa40f002kd56lvwfa8rr94h0dxz70zmgyy5j9ia18x9k0")))

(define-public crate-oxidized-json-checker-0.3.1 (c (n "oxidized-json-checker") (v "0.3.1") (d (list (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "1ahmwbwnr22j9dwq5mchwv047aw6ck14mnb79p0a3hrvy1s34969")))

(define-public crate-oxidized-json-checker-0.3.2 (c (n "oxidized-json-checker") (v "0.3.2") (d (list (d (n "snap") (r "^1.0.0") (d #t) (k 2)))) (h "1b0dbdzmxf6i867i1lqlp308aacmvzic1kyihsmlhgsnpyp6914k")))

