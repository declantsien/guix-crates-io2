(define-module (crates-io ox id oxidd-reorder) #:use-module (crates-io))

(define-public crate-oxidd-reorder-0.1.0 (c (n "oxidd-reorder") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (k 0)) (d (n "is_sorted") (r "^0.1") (d #t) (k 0)) (d (n "oxidd-core") (r "^0.6") (k 0)) (d (n "oxidd-test-utils") (r "^0.1") (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "10l00kybyzwffkdf10l8b6x5ib1a932g3cppdmixsfxm2jjy124k")))

