(define-module (crates-io ox id oxidized-cuda-kernels) #:use-module (crates-io))

(define-public crate-oxidized-cuda-kernels-0.1.1 (c (n "oxidized-cuda-kernels") (v "0.1.1") (d (list (d (n "bindgen_cuda") (r "^0.1.1") (d #t) (k 1)) (d (n "candle-core") (r "^0.4") (f (quote ("cuda"))) (d #t) (k 0)) (d (n "cudarc") (r "^0.10") (f (quote ("f16"))) (d #t) (k 0)) (d (n "half") (r "^2.4") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (d #t) (k 1)))) (h "1dh4p8iw66mi3w9iqq5abic22vzp26z90sj3msawbfrxjyv7hwra")))

