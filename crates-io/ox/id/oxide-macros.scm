(define-module (crates-io ox id oxide-macros) #:use-module (crates-io))

(define-public crate-oxide-macros-0.0.6 (c (n "oxide-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0friijjrhnijv25craviamx32kw76c2yaqymmmvvvf1i6pyw90x2")))

(define-public crate-oxide-macros-0.1.0 (c (n "oxide-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wk9hf6c63s2bkc76bdyrawp3b5n60hl555h2y1xrdk2v421dv4p")))

(define-public crate-oxide-macros-0.2.0 (c (n "oxide-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zmlp6paf425vixqbjgvz1qicda9az3n0gd0dh9911r1a00mcpsj")))

(define-public crate-oxide-macros-0.2.1 (c (n "oxide-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01axpxr87zx61cmffaxks0b5il7266sh59a5hraqpjrv7l1yl2vr")))

(define-public crate-oxide-macros-0.2.2 (c (n "oxide-macros") (v "0.2.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qw3hh1i2yb2pisdyk8pa7z9k4d0x12p2202g7h0da4bf373zwpr")))

(define-public crate-oxide-macros-0.3.0 (c (n "oxide-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ki8q2w95sc1rad8m0j7xk3080d6pk8sbzis1wsq5b0g18z2ajrs")))

(define-public crate-oxide-macros-0.3.1 (c (n "oxide-macros") (v "0.3.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10ddf5f0cg4zhs106p4xw2w261wcvwn1pskgng0kwgarafddrpyg")))

(define-public crate-oxide-macros-0.4.0 (c (n "oxide-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y5r90lkdp7ifbx60z1v9gzqrqf19v0gkpbzl9qrw0g5xpahd0b4")))

(define-public crate-oxide-macros-0.4.1 (c (n "oxide-macros") (v "0.4.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13q1fppxd9gp82jqg3jsj14jji4zhdy3xa0s2p8cj2lkiprvc5sl")))

