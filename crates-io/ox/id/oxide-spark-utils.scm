(define-module (crates-io ox id oxide-spark-utils) #:use-module (crates-io))

(define-public crate-oxide-spark-utils-0.1.0 (c (n "oxide-spark-utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "14w2azfjiiyc7nssg0liv7vfjlbgayz1w0m9jw7ln9anpnsfrc78")))

(define-public crate-oxide-spark-utils-0.2.0 (c (n "oxide-spark-utils") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1diqizcxsbz7gmds6ig5pfapv5yjsdjd2rgkfl7cy46431spmfzr")))

(define-public crate-oxide-spark-utils-0.2.1 (c (n "oxide-spark-utils") (v "0.2.1") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rj9cgpsqxj53788nkb8fqg94fj1hpjg6f3pp4jwrzrccsa4knmn")))

(define-public crate-oxide-spark-utils-0.2.2 (c (n "oxide-spark-utils") (v "0.2.2") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0900zvsvjdjspgdiw8p4pw1dmvsllk8qm10mzcwa0idd1v3ipnn5")))

(define-public crate-oxide-spark-utils-0.2.3 (c (n "oxide-spark-utils") (v "0.2.3") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xvn1dwrhx47v4lgd3j5l6ljp81891ld1l467rz6zjxb3h69b4ci")))

(define-public crate-oxide-spark-utils-0.2.4 (c (n "oxide-spark-utils") (v "0.2.4") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lqmh6wq7myq6ri3nw0cbj2sk1c88qv865m5aiddgxq9x1y0im49")))

(define-public crate-oxide-spark-utils-0.2.5 (c (n "oxide-spark-utils") (v "0.2.5") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lih2336h08v9kyyd2vrsg7hfng5q3ngqbr0185zvq8akj199s6x")))

(define-public crate-oxide-spark-utils-0.2.6 (c (n "oxide-spark-utils") (v "0.2.6") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "051590vimwmm9ip8askg1n73bgczxl7w65m2m1vlxz4vsvkdc8ir")))

(define-public crate-oxide-spark-utils-0.2.7 (c (n "oxide-spark-utils") (v "0.2.7") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v5w58h837r43p8npmyx3ahcaq873k42pb35y8ia1pp1fc5mjj8a")))

(define-public crate-oxide-spark-utils-0.2.8 (c (n "oxide-spark-utils") (v "0.2.8") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qp2sjl2nh889v5svf189clnvl4dbk57ahxziffgyrd1gnjp5dsa")))

(define-public crate-oxide-spark-utils-0.2.9 (c (n "oxide-spark-utils") (v "0.2.9") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0640d3p4qnk9sc9m17b78hn055k1k3jrwna20sn9zhlz8xzv1y5m")))

(define-public crate-oxide-spark-utils-0.2.10 (c (n "oxide-spark-utils") (v "0.2.10") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07y034l2rclh2cp4pp5n832apv04pdhvgnf3vj9vq3ayvlaxcnil")))

(define-public crate-oxide-spark-utils-0.3.0 (c (n "oxide-spark-utils") (v "0.3.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r6dj70c3jqyz7422274lni5y3avjydariq15q5swrmpwmjdff2w")))

(define-public crate-oxide-spark-utils-0.3.3 (c (n "oxide-spark-utils") (v "0.3.3") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "embedded-svc") (r "^0.25.3") (f (quote ("use_serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ibfm2gk7bpcyw62b2c0lrv8f0l35wd78y3sb6k07b7nf0290nsc")))

