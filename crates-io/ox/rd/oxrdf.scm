(define-module (crates-io ox rd oxrdf) #:use-module (crates-io))

(define-public crate-oxrdf-0.1.0-beta.2 (c (n "oxrdf") (v "0.1.0-beta.2") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sophia_api") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1fwg3piznyxn5ppaxv2m22cw2s3wqp5429dwjk48d507mmvhd63j") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.0-beta.3 (c (n "oxrdf") (v "0.1.0-beta.3") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sophia_api") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1yh2gpq74nwddqlnnj9dcr4500rdfyc72hql9nrr7zqr7d50a4fd") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.0-beta.4 (c (n "oxrdf") (v "0.1.0-beta.4") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0l80wcz6c0hpjbyqvq9mxh4pqd9grr4gyd70nmxq0zi4lzsnjrjg") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.0-rc.1 (c (n "oxrdf") (v "0.1.0-rc.1") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07i1v45sp5c1xxc9ls3s9s0w41q2my5fdfc1zxbw4z96gyw7m0nv") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.0 (c (n "oxrdf") (v "0.1.0") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09n5cgjmfv0ajmi0pqkdj1v63gk202h0b4a57192cfw62pmz74s5") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.1 (c (n "oxrdf") (v "0.1.1") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0sg6jw6plnxpbv8was0w088w4hci01dfwwsr1mx8q7kyp3lsnjpb") (f (quote (("rdf-star") ("default"))))))

(define-public crate-oxrdf-0.1.2 (c (n "oxrdf") (v "0.1.2") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rxqc7qqigfdsqqfif5dlc7cmg6j06gldrifvh8414hg6x86pdjh") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.1.3 (c (n "oxrdf") (v "0.1.3") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ach82ijvv81s98zbry0jr0lmrp02icdl22bs6cq83n5li02mnyy") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.1.4 (c (n "oxrdf") (v "0.1.4") (d (list (d (n "lasso") (r "^0.6") (f (quote ("inline-more"))) (d #t) (k 0)) (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "199n33l9q04aiyvnwjqwqqv5ncphxy7wzghxkfvf75hq33ihp3z7") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.1.5 (c (n "oxrdf") (v "0.1.5") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nm8s4ng45d7gxll2fgz8flccvqy3hxwq4fq2him11398x8z7avg") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.1.6 (c (n "oxrdf") (v "0.1.6") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "066hmi0sqi5d2n6kzgyhb0zwpfmdpfy246qra4wpx2xgcksbnqv4") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.1.7 (c (n "oxrdf") (v "0.1.7") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19cpmwn26bm4q15mngqj37iw7fkh6zbwnshmlq35xqjplz48g4ih") (f (quote (("rdf-star") ("default")))) (r "1.60")))

(define-public crate-oxrdf-0.2.0-alpha.1 (c (n "oxrdf") (v "0.2.0-alpha.1") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dwdhvf55q339cmwlki26rv7lwqfwgrvfzj5wza1gypblif84d63") (f (quote (("rdf-star") ("default")))) (r "1.70")))

(define-public crate-oxrdf-0.2.0-alpha.2 (c (n "oxrdf") (v "0.2.0-alpha.2") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fn9g171004mr6zn1bcq9ynhfpkz675in7hqlk2p4z2ndd2lcl0p") (f (quote (("rdf-star") ("default")))) (r "1.70")))

(define-public crate-oxrdf-0.2.0-alpha.3 (c (n "oxrdf") (v "0.2.0-alpha.3") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1kc35kn8k7a0qiqll6l9pm9karbvxbawz1dzxfd30sfkjhmsfh7s") (f (quote (("rdf-star") ("default")))) (r "1.70")))

(define-public crate-oxrdf-0.2.0-alpha.4 (c (n "oxrdf") (v "0.2.0-alpha.4") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "oxsdatatypes") (r "^0.2.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08649bjjndkrls3sl0wp65akyv59md2klcs8snh4pa5z7bzf82w0") (f (quote (("rdf-star") ("default")))) (r "1.70")))

(define-public crate-oxrdf-0.2.0-alpha.5 (c (n "oxrdf") (v "0.2.0-alpha.5") (d (list (d (n "oxilangtag") (r "^0.1") (d #t) (k 0)) (d (n "oxiri") (r "^0.2.3") (d #t) (k 0)) (d (n "oxsdatatypes") (r "=0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1cm1pwc9ls2r98xnipvgvhglv31lbwnqgbx14yf6x3c2jgfdvr5m") (f (quote (("rdf-star") ("default")))) (r "1.70")))

