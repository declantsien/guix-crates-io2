(define-module (crates-io ox ik oxikey) #:use-module (crates-io))

(define-public crate-oxikey-0.0.0 (c (n "oxikey") (v "0.0.0") (d (list (d (n "atsamd-hal") (r "^0.15.1") (f (quote ("samd21g" "rtic-monotonic" "usb"))) (d #t) (k 0)) (d (n "atsamd21g") (r "^0.12.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rtic") (r "^1.1.3") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)) (d (n "usbd-hid") (r "^0.6.0") (d #t) (k 0)))) (h "0n3n0hmfb1dpwnc97mjlwy3xsh95cgzmax9s5f65y18y7lk1sxcl")))

