(define-module (crates-io ox yg oxygen_wave) #:use-module (crates-io))

(define-public crate-oxygen_wave-0.0.1 (c (n "oxygen_wave") (v "0.0.1") (h "1njl65r9kcids7bbx2fv35hz6489mi6gy87l0xg5z0cfxgxzdvx2")))

(define-public crate-oxygen_wave-0.0.4 (c (n "oxygen_wave") (v "0.0.4") (h "0yr2nnnbgq1nkmj6nl4n0yc5w8kns11gp4fw61iwyvg0f5glcbhj")))

(define-public crate-oxygen_wave-0.0.5 (c (n "oxygen_wave") (v "0.0.5") (h "1i0qd33836qiny2q3xqshlfja62xdm9mav8vd77y26f0dr626pv7")))

