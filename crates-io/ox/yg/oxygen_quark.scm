(define-module (crates-io ox yg oxygen_quark) #:use-module (crates-io))

(define-public crate-oxygen_quark-0.0.1 (c (n "oxygen_quark") (v "0.0.1") (h "05fg4k12c5a0lm4gd6ijgqsniahhbybcqggagpxl9qzbiqnqpdz4")))

(define-public crate-oxygen_quark-0.0.4 (c (n "oxygen_quark") (v "0.0.4") (h "152c62wdvz31w4g2zsnqs65a3qmsilbxgm21afha5gb17rvpa904")))

(define-public crate-oxygen_quark-0.0.5 (c (n "oxygen_quark") (v "0.0.5") (h "1p9hv8hwx7fhj2lgls6x4f7d9ngdsh1r5jr1s05hhcs13p1s085f")))

(define-public crate-oxygen_quark-0.0.6 (c (n "oxygen_quark") (v "0.0.6") (h "034yg79ndn541pw11jvljgpx7icc915k67glrqffa7h06qib4668")))

(define-public crate-oxygen_quark-0.0.11 (c (n "oxygen_quark") (v "0.0.11") (d (list (d (n "oxygen_quark_derive") (r "^0.0.2") (d #t) (k 0)))) (h "0i158r255y73fr1n9dl30zyglgnq7xggfi43v1jwhgq0iwpkbjq8")))

(define-public crate-oxygen_quark-0.0.10 (c (n "oxygen_quark") (v "0.0.10") (h "0gs2x9cjv7a9j48naqlpxcz4j6mk6r5kssd4p8jribp9y1873aww")))

