(define-module (crates-io ox yg oxygen_bond) #:use-module (crates-io))

(define-public crate-oxygen_bond-0.0.1 (c (n "oxygen_bond") (v "0.0.1") (h "03cfjq592r2167wp61ldhl9b5mgmzqjba9sirchjxy6n67zkg0x9")))

(define-public crate-oxygen_bond-0.0.2 (c (n "oxygen_bond") (v "0.0.2") (h "0fqrdmcjy5zmzczv9amjjci96prr57rpdb8zrza7hg38ysf00fy1")))

(define-public crate-oxygen_bond-0.0.4 (c (n "oxygen_bond") (v "0.0.4") (h "151qb3hx2qxyb1j84146fmz4zzwbldkpm550pz5bfdj3v8wfrq8p")))

(define-public crate-oxygen_bond-0.0.5 (c (n "oxygen_bond") (v "0.0.5") (h "1j809hjb3lym6xcrd6f0v9a88hpk6jl0i2s4vfiyhwq3s1f9yfgl")))

