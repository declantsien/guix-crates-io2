(define-module (crates-io ox yg oxygengine-backend-desktop) #:use-module (crates-io))

(define-public crate-oxygengine-backend-desktop-0.40.0 (c (n "oxygengine-backend-desktop") (v "0.40.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)))) (h "1dbcdiw8ipnln1mq9p5nc3wn14g0dhybq6ykll35vf79h8jlhpj4") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.40.1 (c (n "oxygengine-backend-desktop") (v "0.40.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)))) (h "0gnamg8npg1inj1ghb1cbvslg801sw8q6lv6riydrb4xsr1y2m8n") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.41.0 (c (n "oxygengine-backend-desktop") (v "0.41.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.41") (d #t) (k 0)))) (h "1ila9m2il00x5mk0357f517www2lh47ksqmh9z9ygf7va0rlf1l5") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.42.0 (c (n "oxygengine-backend-desktop") (v "0.42.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.42") (d #t) (k 0)))) (h "17nakqmf7z5nyg129mdrkxbjbxmsjnwdx6lcpwzbzwr3h14igf1n") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.43.0 (c (n "oxygengine-backend-desktop") (v "0.43.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.43") (d #t) (k 0)))) (h "1dg0caz5ldj5cfkhci608x2s6h6qamccfwg97rrw931i5yh6l2pi") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.44.0 (c (n "oxygengine-backend-desktop") (v "0.44.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.44") (d #t) (k 0)))) (h "02hrz5sdjqjdsl8cvd3bl7l4ddizk53carn15mznc5hhailx3p7s") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.45.0 (c (n "oxygengine-backend-desktop") (v "0.45.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)))) (h "18apyi0jc077lnd3r5y88ra6hzb523qk4gzlvzby439s9jqdij84") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.45.1 (c (n "oxygengine-backend-desktop") (v "0.45.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)))) (h "089d5z22r051sgmw34czdm1k9jrlngd24f3ylyjm3b4va9sp12zi") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.46.0 (c (n "oxygengine-backend-desktop") (v "0.46.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)))) (h "0j7yvblb6vwsy5py2vsc2ga0c3wf5aa9w8wc2v3c5sgfgv913bmg") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

(define-public crate-oxygengine-backend-desktop-0.46.1 (c (n "oxygengine-backend-desktop") (v "0.46.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)))) (h "0gdd00x22dr9q8khi6fjwqw0cwggfcbvx572p315m27has6j6qpr") (f (quote (("scalar64" "oxygengine-core/scalar64"))))))

