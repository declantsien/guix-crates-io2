(define-module (crates-io ox yg oxygen_photon) #:use-module (crates-io))

(define-public crate-oxygen_photon-0.0.1 (c (n "oxygen_photon") (v "0.0.1") (h "0g2f6sjdp5zm5hzb4d6ahdnyrgi52hlpmh190hwdg7cd7h2zpfdj")))

(define-public crate-oxygen_photon-0.0.4 (c (n "oxygen_photon") (v "0.0.4") (h "1h0lwxd63j6ivv2c44jfw0v56vl89sksfcn0zh4jyv96wj52sj8h")))

(define-public crate-oxygen_photon-0.0.5 (c (n "oxygen_photon") (v "0.0.5") (h "06kbd74nk3b5pl2vj9311rlassbifafyzrsxwy5smab3n81wjhdv")))

