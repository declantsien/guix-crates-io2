(define-module (crates-io ox yg oxygengine-audio) #:use-module (crates-io))

(define-public crate-oxygengine-audio-0.4.6 (c (n "oxygengine-audio") (v "0.4.6") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1nc7n4jigxwqhxg7blaaxdw04ha6mkfjvzv3048lirwvnnvs9i9n")))

(define-public crate-oxygengine-audio-0.4.7 (c (n "oxygengine-audio") (v "0.4.7") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1smr5r4x8hff7b4hy0i8758n396bpwzv2y527hh0sv194wwsan5z")))

(define-public crate-oxygengine-audio-0.5.0 (c (n "oxygengine-audio") (v "0.5.0") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "1djja8p5jvg5xnw28fdhy7q79f3x5dkz0msmr5kah1c5ldyl2v2n")))

(define-public crate-oxygengine-audio-0.5.3 (c (n "oxygengine-audio") (v "0.5.3") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "06pw4hb385br6hdl60dmaqycky3j0a128qypj6k2vbb0k1n6iqns")))

(define-public crate-oxygengine-audio-0.5.5 (c (n "oxygengine-audio") (v "0.5.5") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14i130dwx8k6yswx6p7hx7hhh6dcvxcl5mcb3zll72bl06xlbfc2")))

(define-public crate-oxygengine-audio-0.5.6 (c (n "oxygengine-audio") (v "0.5.6") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ibsg8cl8ykysqv63d0hgclv3808r7srz0q96hcda0ym9q23bdcz")))

(define-public crate-oxygengine-audio-0.6.0 (c (n "oxygengine-audio") (v "0.6.0") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sspy896yyl0y2xhl7wwa1c2arqhkdm9n8k3i4c1kp8kwbngcjmj")))

(define-public crate-oxygengine-audio-0.6.1 (c (n "oxygengine-audio") (v "0.6.1") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i9z2ajmj093cms31vdnd296ykmmysqfcw58ixzvim1pn96k5b45")))

(define-public crate-oxygengine-audio-0.6.2 (c (n "oxygengine-audio") (v "0.6.2") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19y10ym3ashkdp4xplai9lb5xvka9wlwkh4cxcw7l1qpv2i982c8")))

(define-public crate-oxygengine-audio-0.6.3 (c (n "oxygengine-audio") (v "0.6.3") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hbp3c8vp393izw03mdxi9qhghn10slirzgsvdbdcycccfxn7xvk")))

(define-public crate-oxygengine-audio-0.6.5 (c (n "oxygengine-audio") (v "0.6.5") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jjg7rl5sjzl7ic6hp3nnqr9aaz6amr1qdmpplcijs1bdlid64kd")))

(define-public crate-oxygengine-audio-0.7.0 (c (n "oxygengine-audio") (v "0.7.0") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13r513cdn7vsjng7i6qiiigvi3vmfzcjkqxirsfqpyjanambg24k") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.7.1 (c (n "oxygengine-audio") (v "0.7.1") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "120sywq957aa3fnlvb89g5r1svgy5mvjjbvlb6wa9mhb0zizlazj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.7.2 (c (n "oxygengine-audio") (v "0.7.2") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zam2vj0ayvjmgh5lnayag8lqvn644zv959hpwb6hsr1y6ivwzps") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.8.0 (c (n "oxygengine-audio") (v "0.8.0") (d (list (d (n "oxygengine-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ps6kd5ihfsvkq2wrz7002fjaaqnrz68vi17w8r5wf6z24vd7c68") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.9.0 (c (n "oxygengine-audio") (v "0.9.0") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ans6iwhr85iicjwpvqp4rhc056az9pnx8vkjlhp2d8qmcyg0ynw") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.9.1 (c (n "oxygengine-audio") (v "0.9.1") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zblh0l31pkmyyb09bpllgwvd8mb4ihgyc4ydfh80am8pgyl6i8w") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.10.0 (c (n "oxygengine-audio") (v "0.10.0") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5hn867w1578ix5211gmkdx13cqw6mg82hqjdcdc2da8kshwdmj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.10.1 (c (n "oxygengine-audio") (v "0.10.1") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vhp57ivrbbcr35c28fiq1nycf0d3hhns7zr7h2264p7f1qzs459") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.10.2 (c (n "oxygengine-audio") (v "0.10.2") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10pbq94binwh1ni1w0xsvnfv1cpil2v29nlyv9rm3nxl2vs63d0s") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.11.0 (c (n "oxygengine-audio") (v "0.11.0") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mghr8crnimkdrkjkx91saq30mv4crzjmzxpz8mk76qjdg1f2cq") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.11.1 (c (n "oxygengine-audio") (v "0.11.1") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11w9x79ndsmsylc6wyqbama581f8mj5qshaz2fcv8riqb9s3gcb1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.11.2 (c (n "oxygengine-audio") (v "0.11.2") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fbzxg9djryxmdxkniagrijr480ydcavhbwzs4i88ppbm8rsrl7c") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.0 (c (n "oxygengine-audio") (v "0.12.0") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f749psaqqn2iamzz3pfd50binh4q5vghrvys6wrbwgklvjh9asq") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.1 (c (n "oxygengine-audio") (v "0.12.1") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lw42fd11zhrzfp6igaspz51w201vrs3959i8w4hq8wfspn7sf6i") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.2 (c (n "oxygengine-audio") (v "0.12.2") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "081fn7acgn1ybv3cawswa08pb4pfd1ygb8nrjyygrv716nqpa5kk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.3 (c (n "oxygengine-audio") (v "0.12.3") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1psj3f5v6vs7z0jzvjf3fg4rfmm4lwx0qmcn1sa74igvpp9q78vh") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.4 (c (n "oxygengine-audio") (v "0.12.4") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i24vk5s8agyh0y4z5pads62c65mx19n32ydbwwqrfi1nfi8njsy") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.12.5 (c (n "oxygengine-audio") (v "0.12.5") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0krk9s2ilv500p9almifv20jvf1hp59i64r208nb42jhsg9xb8wz") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.13.0 (c (n "oxygengine-audio") (v "0.13.0") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10c0ywizyhgvfgss6bbb3v8i4m869l923hqvlphx6dgi4vgs6jpd") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.13.1 (c (n "oxygengine-audio") (v "0.13.1") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qvb6yczy9jdhmqvmnknl6gpd7hg0gyacs1h61zlbcvn70yp4pl0") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.13.2 (c (n "oxygengine-audio") (v "0.13.2") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nzxszaq9wdlfwjzgd0smw64g7hhkci9lvr435ff7b77wfvag1m1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.14.0 (c (n "oxygengine-audio") (v "0.14.0") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m1j5smv6zap2d3bh1bhmlb7psl3rsr2xm6afmk8v799zxvfmaq1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.14.1 (c (n "oxygengine-audio") (v "0.14.1") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "052gs2n2sa6pfsm76bd6328c3afdkby07h5wbvp0q2mfhkk71jjj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.14.2 (c (n "oxygengine-audio") (v "0.14.2") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04b8d38ll0wx1v7zlhq1prz9klknhhj64mjdbxld35ppwwq3v1mr") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.15.0 (c (n "oxygengine-audio") (v "0.15.0") (d (list (d (n "oxygengine-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0smmqif1pajfk1ww6hbrlars4pwn06d6y7midh1qv1ds07m50lfm") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.16.0 (c (n "oxygengine-audio") (v "0.16.0") (d (list (d (n "oxygengine-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09jsj4xv3wcv4yw7fdk47af9b6yscd9h0rmyl3nh0iv3fqd0sip3") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.17.0 (c (n "oxygengine-audio") (v "0.17.0") (d (list (d (n "oxygengine-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08bwflz9nvwmjg8yk2w8jan6l7z7xhx83cfxfgna5qvshdfzn37g") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.18.0 (c (n "oxygengine-audio") (v "0.18.0") (d (list (d (n "oxygengine-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mjbwjqb34q8jgd7an13878dixlv504zy94hywjd6n134ydf8296") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.19.0 (c (n "oxygengine-audio") (v "0.19.0") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18ww49nxcks2by74ybpv2714kwl4jdy8bqrg45vxzdnwfriq84dy") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.19.1 (c (n "oxygengine-audio") (v "0.19.1") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i1krinzyhbn3df38sh22kxbf1pq6qyyap5b2k2m3l2k8g8b38wc") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.19.2 (c (n "oxygengine-audio") (v "0.19.2") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06q5pb31r3i8w957w5gggfycyb5d4zkrwzivik2qcg15i7k7df0r") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.20.0 (c (n "oxygengine-audio") (v "0.20.0") (d (list (d (n "oxygengine-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xr5ip32jacq8c3v4dsgpakf29bfqallpvy39mqgxj2fp13d7sd7") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.21.0 (c (n "oxygengine-audio") (v "0.21.0") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07q34k88jcmbzbfyawhnjzylhm9y2639dx4s9q45m5ca9cb7r7fx") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.21.1 (c (n "oxygengine-audio") (v "0.21.1") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sasiyiwiimws0s694ni780jb1vazxdbj1ns8nxfi7lyjs59986p") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.22.0 (c (n "oxygengine-audio") (v "0.22.0") (d (list (d (n "oxygengine-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gfi3ssgqr87q0cwfv8phxa0ax3p3p8nncq76my3jilph1qpgh7v") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.24.0 (c (n "oxygengine-audio") (v "0.24.0") (d (list (d (n "oxygengine-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fy05pgn0br5mlhisjpyc08xllvsj4vws87gfl4ba5gz392rp1jk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.25.0 (c (n "oxygengine-audio") (v "0.25.0") (d (list (d (n "oxygengine-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yb96z5ga3q71vf0586ckx5q5gddr91md2galk6wirazs4j3q5n4") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.26.0 (c (n "oxygengine-audio") (v "0.26.0") (d (list (d (n "oxygengine-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07wijp28hdwcq91fckp99jdp2rq9jisw38fzb6fj5lyk0rbs2gw9") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.27.0 (c (n "oxygengine-audio") (v "0.27.0") (d (list (d (n "oxygengine-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzz5mrba8mfxr22xfhpi00g1n6k04zw86d91649zrw5xlqkbq4q") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.28.0 (c (n "oxygengine-audio") (v "0.28.0") (d (list (d (n "oxygengine-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hzjqd5w1n7irprgfp7fhj9iqrf890ab0vc7zcjfri2lcd03i4f5") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.30.0 (c (n "oxygengine-audio") (v "0.30.0") (d (list (d (n "oxygengine-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ml82an9x94ck7sggmdrq5lr1dvjgmfkmbbm5v0qf9ic0hqva1mn") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.40.0 (c (n "oxygengine-audio") (v "0.40.0") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wfb4qkn4ffcwkx4b860hg1n35yvapi1w2cnii18nbjlvdqmdwil") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.40.1 (c (n "oxygengine-audio") (v "0.40.1") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hdn2hr9rk3g7vmhh48rvzaiih58qk6ixb7rkk71742saripz78i") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.41.0 (c (n "oxygengine-audio") (v "0.41.0") (d (list (d (n "oxygengine-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pqxd56lir65ydalvvdglfhpjzybm1ws8nl8zcd5944cj8yh4xlm") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.42.0 (c (n "oxygengine-audio") (v "0.42.0") (d (list (d (n "oxygengine-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1212psyrc7mwz1x3dskw4i9jknfqg1z411zxccf7kfzdk8zsy11f") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.43.0 (c (n "oxygengine-audio") (v "0.43.0") (d (list (d (n "oxygengine-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08k7rkvf27l50d2mydkiqdjshq24skliim1m4rb9n3mk536mkz8h") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.44.0 (c (n "oxygengine-audio") (v "0.44.0") (d (list (d (n "oxygengine-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bf4mzdiqxds4bdangyynwg1kdyxdj6l5a63fja5xa5d7sk4jn6y") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.45.0 (c (n "oxygengine-audio") (v "0.45.0") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rmkrpr9c4xfvf6jhxwhpgc364hxnfw1bnj2zg8blz4ymm53nk2s") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.45.1 (c (n "oxygengine-audio") (v "0.45.1") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yk1d1g0l5f8f9mhhyqid4hlrnijjm7d6vwan7vgjxx6mrgpwqzp") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.46.0 (c (n "oxygengine-audio") (v "0.46.0") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x50p4jy28ic7qh6w2shg9fk5z3y5vqcwg85q869h9v0b1wiqrps") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-audio-0.46.1 (c (n "oxygengine-audio") (v "0.46.1") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bckc36558zbx21k00i979r8a0s7fzzm2iaiszciv9pi9lwdicll") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

