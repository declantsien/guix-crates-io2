(define-module (crates-io ox yg oxygraphis) #:use-module (crates-io))

(define-public crate-oxygraphis-0.1.6 (c (n "oxygraphis") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "oxygraph") (r "^0.1.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0s91g62bnrmyispxx16ry7pawjnid7fqnrn6nw6rhbmldnlpd89c")))

(define-public crate-oxygraphis-0.1.62 (c (n "oxygraphis") (v "0.1.62") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "oxygraph") (r "^0.1.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "09mqc8n1dygc98xi47ip34038kb0rs80a5wq1ajxjgxxh0hfkz59")))

