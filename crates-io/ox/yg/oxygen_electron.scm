(define-module (crates-io ox yg oxygen_electron) #:use-module (crates-io))

(define-public crate-oxygen_electron-0.0.1 (c (n "oxygen_electron") (v "0.0.1") (h "0fzsmqsd1nm8977yv6wvsl33mq3dybvdryyawn7sybj92aggngy2")))

(define-public crate-oxygen_electron-0.0.4 (c (n "oxygen_electron") (v "0.0.4") (h "0ad348g7gfy6jk82cfga18qfs3xjkq3zpcfgvdwfp6n9nwawj3mf")))

(define-public crate-oxygen_electron-0.0.5 (c (n "oxygen_electron") (v "0.0.5") (h "0pr2amrq1sdf3vy56wldffipsc48d1n47av6q19fhc34a6p3zlax")))

