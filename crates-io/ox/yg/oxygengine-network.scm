(define-module (crates-io ox yg oxygengine-network) #:use-module (crates-io))

(define-public crate-oxygengine-network-0.3.2 (c (n "oxygengine-network") (v "0.3.2") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "12nqkqlhr9c2kvsh7jxgq5q06iw3abg6j3szqlcqclr83k01q9dw")))

(define-public crate-oxygengine-network-0.3.4 (c (n "oxygengine-network") (v "0.3.4") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1mp29dcddai0aq348cg336j31ydixhx9q1hgypckgj88bv9sk5kq")))

(define-public crate-oxygengine-network-0.3.5 (c (n "oxygengine-network") (v "0.3.5") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0zxjbl4wsjp5ba8mi25vdzz84sy6fpgwlk6bpcv08lxj2dfjfjsz")))

(define-public crate-oxygengine-network-0.3.6 (c (n "oxygengine-network") (v "0.3.6") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "06k1l721lpfnzbnihl8g344ajkizj80f5l4hg3lx7y0hlbgps2dz")))

(define-public crate-oxygengine-network-0.3.7 (c (n "oxygengine-network") (v "0.3.7") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "12qg3kaf78xhw2xa30q7ymwlpnb2k3gvf432dz5lv9mhda8yzvg5")))

(define-public crate-oxygengine-network-0.3.8 (c (n "oxygengine-network") (v "0.3.8") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "10yvbgbahifqn09mmgv261f8qjvsm204w9n8bxzrfxv4alm0l902")))

(define-public crate-oxygengine-network-0.3.9 (c (n "oxygengine-network") (v "0.3.9") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1l1w7xi6sx0pb6k0zsh8l8imlz5b5fq12qs7flas1c5a4dg4nljq")))

(define-public crate-oxygengine-network-0.3.12 (c (n "oxygengine-network") (v "0.3.12") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1gm9n10gzcgapj40jccfd4ymbxbqjfngdhxzvmhzs6vgjhaqypqm")))

(define-public crate-oxygengine-network-0.3.13 (c (n "oxygengine-network") (v "0.3.13") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "06c6mqwqf3shsy6qb73lixbdc7v84wyc86z5p9f7wx2d0xnrj1ql")))

(define-public crate-oxygengine-network-0.3.14 (c (n "oxygengine-network") (v "0.3.14") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0cz8sbfkva0qr0c2331h39d61jla2s6ya3gyr3jpaw1b5cw5zqv1")))

(define-public crate-oxygengine-network-0.3.15 (c (n "oxygengine-network") (v "0.3.15") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1cyykzlm2ia6ny4gmmy6ln46l7sqqx0hb5ld61n662nkfaw90szn")))

(define-public crate-oxygengine-network-0.3.16 (c (n "oxygengine-network") (v "0.3.16") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1bnhnwz880wnz8swnqylc2l8gkzn675x2p010nkb0clsvl1k2pyx")))

(define-public crate-oxygengine-network-0.3.17 (c (n "oxygengine-network") (v "0.3.17") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "129rizqrajpmaqygcwm6bwkhkqjh881jsp6dlla29ldi5pxzri29")))

(define-public crate-oxygengine-network-0.4.0 (c (n "oxygengine-network") (v "0.4.0") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "07d4yfkl3yk0cyb9f2pzqmacll39nmi59nxk5w10f9wdgdp1xk1k") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.2 (c (n "oxygengine-network") (v "0.4.2") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1k4c1xchhnajqjqbk96knx5jf6xyz0yc28rwv2rkqif3nrlcg4wm") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.3 (c (n "oxygengine-network") (v "0.4.3") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "0284ypscq4y89j3m9mghp41mn5six1r55mr0z9shjpfn6q8jj0fn") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.4 (c (n "oxygengine-network") (v "0.4.4") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1hnahw10xsikgjkgm684fy31ni2rqmry7lfnlp45dm7p3xs7dspc") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.5 (c (n "oxygengine-network") (v "0.4.5") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "043wrjkbc9gmnqldksifsprq1scd5pa1dq6pydi8qw22hg03z87j") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.6 (c (n "oxygengine-network") (v "0.4.6") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1l9ap5pcvff52pdar4win6bjqm10yv1a1dpilpcmymip9ajblc2l") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.4.7 (c (n "oxygengine-network") (v "0.4.7") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "0yxyam0jmzz07sbn9fpc2fg9ns5jl0xbx4sk82k5i2af2f1y1f9p") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.5.0 (c (n "oxygengine-network") (v "0.5.0") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "09x4rprwwf1hknb7f9dgkszp0fr382gs7bn1ka7wxv9s5pigdkj6") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.5.3 (c (n "oxygengine-network") (v "0.5.3") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "14xqc78dpd7z8va390gxmxjvffbcrjvkm7pcvmfjq0cn6fpv2m4g") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.5.5 (c (n "oxygengine-network") (v "0.5.5") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "1dxjqx0j4wjxyfapprnav4yjajhms6azq2bl1q2i0bj5806jakr8") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.5.6 (c (n "oxygengine-network") (v "0.5.6") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "0bckqsl979ssq7mcn6727nkqcfwxg7grgd15hk5mm7rim5llbgfj") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.6.0 (c (n "oxygengine-network") (v "0.6.0") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)))) (h "11x0a8hkyvpzrc1kgvkc63xmxv4xrq4l2zj3qc4aj55x0lr6a8hd") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.6.1 (c (n "oxygengine-network") (v "0.6.1") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)))) (h "1a7v0rvml1k1gqvmy63v1qdykx5r41dqgnjy97y3iv428ij13zyv") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.6.3 (c (n "oxygengine-network") (v "0.6.3") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)))) (h "03fi6if764bgz2wg3693wimw04vqdrn503py4m9vf5hbwrqabnk5") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.6.5 (c (n "oxygengine-network") (v "0.6.5") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)))) (h "0hlgm56657pn9w22ffv4r2390x4qs7pf027d3hm5qgs74jsiiv4x") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.7.0 (c (n "oxygengine-network") (v "0.7.0") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)))) (h "0s8kf7i3zz00nl334vrzakvwhqsiayk87wqgfqdr5563paj5py1b") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.7.1 (c (n "oxygengine-network") (v "0.7.1") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)))) (h "1bxy48531j3b30pykd1fxllifc0rdxmp5cff369cs9lmr3r4192z") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.7.2 (c (n "oxygengine-network") (v "0.7.2") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)))) (h "1jga1nljx3kmlqk3273dlfazcbc1gjp8wbxfr472xmvgkrjw1is9") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.8.0 (c (n "oxygengine-network") (v "0.8.0") (d (list (d (n "oxygengine-core") (r "^0.8") (d #t) (k 0)))) (h "1njk5024196lydzbqml5jqsvxf2371yz9v1d1qyam587j0wapp55") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.9.0 (c (n "oxygengine-network") (v "0.9.0") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)))) (h "1nkh6fkji0l90c5c08cm1y3yrsmpjwgbqvz0xms5y2d35vnd1hik") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.9.1 (c (n "oxygengine-network") (v "0.9.1") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)))) (h "1b9c6z14j8x2wpkwna8l3438ar9nws2i1j3010xp6izg9m069cmi") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.10.0 (c (n "oxygengine-network") (v "0.10.0") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)))) (h "1sn8ss5bwi8j5jmflj1x1s691nhbpbvai8a57vnhpczq0mgci0fa") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.10.1 (c (n "oxygengine-network") (v "0.10.1") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)))) (h "1k0wnp112ksp8c23w5366mfryrjjr7v2hjvcpd68jxh2rh5lbvjx") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.10.2 (c (n "oxygengine-network") (v "0.10.2") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)))) (h "1q6zqydxrhp2nk4g7ssr2n7alqi5gf7mmg918bij8wpvc1fy7ikx") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.11.0 (c (n "oxygengine-network") (v "0.11.0") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)))) (h "1m10rmk8bx52gjzjz2gxybnj48yrr9b6dkgfcmgbhv1i1f25l8rk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.11.1 (c (n "oxygengine-network") (v "0.11.1") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)))) (h "0b196x1vaacgdakj4pkc5d8p1s55068jm52h6v1vx1wxzqyc9cz1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.11.2 (c (n "oxygengine-network") (v "0.11.2") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)))) (h "0w2ynvwyj8bm5gsdx6kbp90w3pp0jds5mzdqk39ppqna9yd1ds7m") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.0 (c (n "oxygengine-network") (v "0.12.0") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "1kjl89gq7lqc8v58mavmc5s3i9vww2abf4b35bihw8x917z9cggm") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.1 (c (n "oxygengine-network") (v "0.12.1") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "1d08vsrx4c19nik6h0zmx88ny3hbs7dcq6238njbhv7lrgr9v707") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.2 (c (n "oxygengine-network") (v "0.12.2") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "0yhy4wgws18cqx63ch38rvfbmvv41xvyzfh4xl3kddrk3w05kyvj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.3 (c (n "oxygengine-network") (v "0.12.3") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "1n8hmay94s22v3gljll6vfv2zjgfxvwgs5dnd1qsbygmvbw0vrqg") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.4 (c (n "oxygengine-network") (v "0.12.4") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "0nqscnm58p6igzijwlrva2dammnplr11hmipqfwcj3vcqj1s3p1d") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.12.5 (c (n "oxygengine-network") (v "0.12.5") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)))) (h "1rdrki8914fy2a90b62sbz0lmrhwkbgrdn129jwvbzx99ryr4jg7") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.13.0 (c (n "oxygengine-network") (v "0.13.0") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)))) (h "0h620kyvdcm4xpd9y87ljln4y4aj3wbk1mngcmisfd88laqkh6fw") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.13.1 (c (n "oxygengine-network") (v "0.13.1") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)))) (h "0q1fg24l35s88gc10f2b01n34xds8mzm679zhpkk02bm8cplxs24") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.13.2 (c (n "oxygengine-network") (v "0.13.2") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)))) (h "0ykp6bhfi3qaq6867g0vyhi97v1rpbc168dw3grsayy8pijk9jz8") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.14.0 (c (n "oxygengine-network") (v "0.14.0") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)))) (h "124gqiv8s4jy1jslg9c6hv32h0pzp0ivd9y7vyljn8p7sll8fiyj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.14.1 (c (n "oxygengine-network") (v "0.14.1") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)))) (h "0sd7w71g2mj7ryf2fid0v4mis6wj6ncmf0zxwwk0vrzzgq5i5pr7") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.14.2 (c (n "oxygengine-network") (v "0.14.2") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)))) (h "1xfmlp7s922hiyxa15g3pr2kkggjzqzvqggz8f3sfbbcwbvih1cn") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.15.0 (c (n "oxygengine-network") (v "0.15.0") (d (list (d (n "oxygengine-core") (r "^0.15") (d #t) (k 0)))) (h "0mwac36j5gx3ksfppqmcqv5m5kh791znkan03cii48yng6vqy34x") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.16.0 (c (n "oxygengine-network") (v "0.16.0") (d (list (d (n "oxygengine-core") (r "^0.16") (d #t) (k 0)))) (h "19zz308bxkvga6ras37q5anycjiw32kw3phwrj1wiqb8fygpskf5") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.17.0 (c (n "oxygengine-network") (v "0.17.0") (d (list (d (n "oxygengine-core") (r "^0.17") (d #t) (k 0)))) (h "0jvlj200m39bxd1xvkcdgl0byil4p46ls90f2x5gagkb7gsxzaaw") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.18.0 (c (n "oxygengine-network") (v "0.18.0") (d (list (d (n "oxygengine-core") (r "^0.18") (d #t) (k 0)))) (h "03r7kcyn6pn5ybll0nnknf1jvkhg4hq4h94kx2c08hbilgcyryr0") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.19.0 (c (n "oxygengine-network") (v "0.19.0") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)))) (h "16mkyq80jcnvkx8ajiwsiybpgydfrl69yfwphyxqk3klycbnrp4s") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.19.1 (c (n "oxygengine-network") (v "0.19.1") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)))) (h "00bm9p9zf79v8i24vj8wafsxp6gb7jpdq7bnk31qdm0pmvn5m91h") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.19.2 (c (n "oxygengine-network") (v "0.19.2") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)))) (h "1067wbfk01jzhwsqvpqlr5zvrbxf56wfw2pz9sg2ipwh45rdwa8a") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.20.0 (c (n "oxygengine-network") (v "0.20.0") (d (list (d (n "oxygengine-core") (r "^0.20") (d #t) (k 0)))) (h "0mw9x3wab6b7qw0vwp0wqhfmah1ajxb3ck7zlza8clxm91259521") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.21.0 (c (n "oxygengine-network") (v "0.21.0") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)))) (h "1zasrl5jr83sbd9b1lx750liz12xvnnyqk8dcdrn9fqhb1gkmisy") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.21.1 (c (n "oxygengine-network") (v "0.21.1") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)))) (h "1yrhbw5mmrsq9nr0vkwh5yvrs265i0mhi34a5ydp1lcqpc0f5dmb") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.22.0 (c (n "oxygengine-network") (v "0.22.0") (d (list (d (n "oxygengine-core") (r "^0.22") (d #t) (k 0)))) (h "1ahjrw4lhlqkszmbzkzs0w36lq90ml5h8jnragjszxp0n1cfxyjk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.24.0 (c (n "oxygengine-network") (v "0.24.0") (d (list (d (n "oxygengine-core") (r "^0.24") (d #t) (k 0)))) (h "00k1989svv8s9vzbcx4s7wpyppx8svg8pjkal4ixaav46jm82l0w") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.25.0 (c (n "oxygengine-network") (v "0.25.0") (d (list (d (n "oxygengine-core") (r "^0.25") (d #t) (k 0)))) (h "0gnsr7v5analg7xmgl77r73mixszl2v7mw0d2s9fihjnxdafyaqm") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.26.0 (c (n "oxygengine-network") (v "0.26.0") (d (list (d (n "oxygengine-core") (r "^0.26") (d #t) (k 0)))) (h "0h6vqsiz97jd0nzsw0gfrdy2mx5wi14ia76xhxk32w30zz7vbzav") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.27.0 (c (n "oxygengine-network") (v "0.27.0") (d (list (d (n "oxygengine-core") (r "^0.27") (d #t) (k 0)))) (h "0hlzac7mj4sin0m59np5p18iwbdjsi4a5cb6wvjcx15kn7dh6f49") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.28.0 (c (n "oxygengine-network") (v "0.28.0") (d (list (d (n "oxygengine-core") (r "^0.28") (d #t) (k 0)))) (h "0lb9kx7jnzmkx75prqbfyfwgaiy3zli77g3654j3ks4d09hyf4zn") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.30.0 (c (n "oxygengine-network") (v "0.30.0") (d (list (d (n "oxygengine-core") (r "^0.30") (d #t) (k 0)))) (h "076bagk713y1syz1p0xv0yn1gxh5iz709whf09mr9r5kkll0jrxh") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.40.0 (c (n "oxygengine-network") (v "0.40.0") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)))) (h "1c7h60krsda2r960j8lvhkciq4jmss9xhpbj0s5ppqhggm8pgfza") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.40.1 (c (n "oxygengine-network") (v "0.40.1") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)))) (h "07qq204wx3jk72r39cbs8d2ybzn95vnjsvjbgz4qk3wn1kn69692") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.41.0 (c (n "oxygengine-network") (v "0.41.0") (d (list (d (n "oxygengine-core") (r "^0.41") (d #t) (k 0)))) (h "152137bk21kspp031vf4d4qgxaaspznckzgfm7n6qq1xnb105xdy") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.42.0 (c (n "oxygengine-network") (v "0.42.0") (d (list (d (n "oxygengine-core") (r "^0.42") (d #t) (k 0)))) (h "1p0d6b9yd77z07iix9h8mv6qnj4j0zrxx84alsa7ji3b9p8ghmbh") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.43.0 (c (n "oxygengine-network") (v "0.43.0") (d (list (d (n "oxygengine-core") (r "^0.43") (d #t) (k 0)))) (h "1cyxsbligidcwv7ai3l5rkdkadysnvcmpi3q9dg1f0041w41j6bw") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.44.0 (c (n "oxygengine-network") (v "0.44.0") (d (list (d (n "oxygengine-core") (r "^0.44") (d #t) (k 0)))) (h "0dgbsgz4g141cvjmd7bmsyac35831nfkf3d1cmxyln8dms02miis") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.45.0 (c (n "oxygengine-network") (v "0.45.0") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)))) (h "1lm9wwa64phi0b4gzkb5c049mfpzlh2p9qb22rcwi2g89v425pcd") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.45.1 (c (n "oxygengine-network") (v "0.45.1") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)))) (h "1z8by74bjdk74gprvbh03qgzjp6ha6m303j389jjc0b81n8kn0r3") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.46.0 (c (n "oxygengine-network") (v "0.46.0") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)))) (h "1m4h0l8975n08gani278nsmk3qfb3da8f2wzkypsdz1v548ys4xd") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-network-0.46.1 (c (n "oxygengine-network") (v "0.46.1") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)))) (h "0q0waw76wiggbx80xlc4szbb7b0hxvji18zdvxhs8n9jyr7yzvf6") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

