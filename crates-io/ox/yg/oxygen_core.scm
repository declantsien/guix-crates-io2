(define-module (crates-io ox yg oxygen_core) #:use-module (crates-io))

(define-public crate-oxygen_core-0.0.1 (c (n "oxygen_core") (v "0.0.1") (d (list (d (n "oxygen_bond") (r "^0.0.4") (d #t) (k 0)) (d (n "oxygen_electron") (r "^0.0.4") (d #t) (k 0)) (d (n "oxygen_emission") (r "^0.0.4") (d #t) (k 0)) (d (n "oxygen_orbit") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_photon") (r "^0.0.4") (d #t) (k 0)) (d (n "oxygen_quantum") (r "^0.0.4") (d #t) (k 0)) (d (n "oxygen_quark") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_wave") (r "^0.0.4") (d #t) (k 0)))) (h "0nr25zmkvjl75bwqylia0pn7gr0dcphjx3bql36p4640wac1ic0y")))

(define-public crate-oxygen_core-0.0.2 (c (n "oxygen_core") (v "0.0.2") (d (list (d (n "oxygen_bond") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_electron") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_emission") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_orbit") (r "^0.0.6") (d #t) (k 0)) (d (n "oxygen_photon") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_quantum") (r "^0.0.5") (d #t) (k 0)) (d (n "oxygen_quark") (r "^0.0.6") (d #t) (k 0)) (d (n "oxygen_wave") (r "^0.0.5") (d #t) (k 0)))) (h "08cphpmyjrj4k63hy9rlfadbmb9w9cdcj6dj417nk7ziddyniv17")))

