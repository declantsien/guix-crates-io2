(define-module (crates-io ox yg oxygengine-ignite-types) #:use-module (crates-io))

(define-public crate-oxygengine-ignite-types-0.7.2 (c (n "oxygengine-ignite-types") (v "0.7.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "167wl9lqncflhx3ndp87dp46hcyv4yvwg3yl96lvln2rl0kkxb1i")))

(define-public crate-oxygengine-ignite-types-0.8.0 (c (n "oxygengine-ignite-types") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09f25ah3hgms3yqrz1kn1ndb91kqrnhw6j2sncdspl4q6w64kqvc")))

(define-public crate-oxygengine-ignite-types-0.9.0 (c (n "oxygengine-ignite-types") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f483zjgrrhcs3jcpnijzz7kk805r34i55fhna4cmwl6nbxnwfjk")))

(define-public crate-oxygengine-ignite-types-0.9.1 (c (n "oxygengine-ignite-types") (v "0.9.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11ypgc8w9sqcq9xhk7sd905k900cpv5ls6kidgxvjvga5vq2wkj8")))

(define-public crate-oxygengine-ignite-types-0.10.0 (c (n "oxygengine-ignite-types") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d2b1rs39y04053gri6i0l4ndwkiy5i3jyi04hryhr9fr65s1j8p")))

(define-public crate-oxygengine-ignite-types-0.10.1 (c (n "oxygengine-ignite-types") (v "0.10.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05mc6nxy9sjxfrnapf13zvmrls0j1vb4yvcdppdsfyvgcfgq63fr")))

(define-public crate-oxygengine-ignite-types-0.10.2 (c (n "oxygengine-ignite-types") (v "0.10.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yc317ypa8vp14g9s5g3r11gcd63zkr7lx4nm8345vqiwxgyxpf0")))

(define-public crate-oxygengine-ignite-types-0.11.0 (c (n "oxygengine-ignite-types") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "091hsb0zhlz3r8qv1jxxypy7342ckrms8sb95dxf9bmkip6wpkzj")))

(define-public crate-oxygengine-ignite-types-0.11.1 (c (n "oxygengine-ignite-types") (v "0.11.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17vb9sggqdxjhf04fbm33la7g8kf8an4wfx4k98aj2dk107wcg78")))

(define-public crate-oxygengine-ignite-types-0.11.2 (c (n "oxygengine-ignite-types") (v "0.11.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iwbchxnwkyyfl6hhz8c4xkp5bxlckyy0nrh2zpgv4j7sm8pyy9k")))

(define-public crate-oxygengine-ignite-types-0.12.0 (c (n "oxygengine-ignite-types") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z4anprgj7fn4x20n9c4g73bwvc7kma3hfs0cm305bpmfmq7jl55")))

(define-public crate-oxygengine-ignite-types-0.12.1 (c (n "oxygengine-ignite-types") (v "0.12.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l8n2kkkrwk7wqmh6krnprd1dr7pcnaqj95293g6kqj250qik7pz")))

(define-public crate-oxygengine-ignite-types-0.12.2 (c (n "oxygengine-ignite-types") (v "0.12.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r0sgw0wvn5hhawlkwcyqc46yd7agxby2nz63mzzsfy81h9spsni")))

(define-public crate-oxygengine-ignite-types-0.12.3 (c (n "oxygengine-ignite-types") (v "0.12.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "164hvsklcvqskw38bgz4hr6f04s1mwikh0mwj6sy3vbw5ip22ij1")))

(define-public crate-oxygengine-ignite-types-0.12.4 (c (n "oxygengine-ignite-types") (v "0.12.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lw314r30wlrki15w57jd24m1njsnpgyyxxh03y9rqg6bygwi5d7")))

(define-public crate-oxygengine-ignite-types-0.12.5 (c (n "oxygengine-ignite-types") (v "0.12.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w6vgpi73bqh1q215xr8x6b3jq4v1njz8wq30333q55ci1i9fbk2")))

(define-public crate-oxygengine-ignite-types-0.13.0 (c (n "oxygengine-ignite-types") (v "0.13.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a6dprnbp0qlrpfjjmpc1m9l0imn86hmm8mz0bz0fc8ph11xcg88")))

(define-public crate-oxygengine-ignite-types-0.13.1 (c (n "oxygengine-ignite-types") (v "0.13.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09hn1qv5iyl3rj7wfia5xndj2zqks8nqdjrsy1nd0fs70pmfcm3b")))

(define-public crate-oxygengine-ignite-types-0.13.2 (c (n "oxygengine-ignite-types") (v "0.13.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06imbsy2d5w1xip09zrn3k39biawasppqrs9sjwiza21d3r0hha0")))

(define-public crate-oxygengine-ignite-types-0.14.0 (c (n "oxygengine-ignite-types") (v "0.14.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dlp7p2gxfyahlc0mwxz6mf8msp9rf44xnqrafjn3lgq826pf7bf")))

(define-public crate-oxygengine-ignite-types-0.14.1 (c (n "oxygengine-ignite-types") (v "0.14.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "016yd5nwrnzwnaxh0igxz8hz76s8wphj5m88dvxjvlqqb91kicia")))

(define-public crate-oxygengine-ignite-types-0.14.2 (c (n "oxygengine-ignite-types") (v "0.14.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b9nwnbzyjjx83xknm3wb29wr394kkgnbhr89g9y3xx5fd0b0icf")))

(define-public crate-oxygengine-ignite-types-0.15.0 (c (n "oxygengine-ignite-types") (v "0.15.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ih725zjbnc300glpl0fbjgwn78jn2a56ss7cp4bydpi9ad88lzj")))

(define-public crate-oxygengine-ignite-types-0.16.0 (c (n "oxygengine-ignite-types") (v "0.16.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a6w91avwwfcjavfbibjshg1mfsijwcyialp1caynj3r793i7pk1")))

(define-public crate-oxygengine-ignite-types-0.17.0 (c (n "oxygengine-ignite-types") (v "0.17.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f2qhgvcj9651p95dibby7hzm85bsdgp6mky6id1w6d44ngsrs2y")))

(define-public crate-oxygengine-ignite-types-0.18.0 (c (n "oxygengine-ignite-types") (v "0.18.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wcw5wyrh7h2d6nrb3bnzri8d3ll5i9ivia3xd9j3c9lh346y7rr")))

(define-public crate-oxygengine-ignite-types-0.19.0 (c (n "oxygengine-ignite-types") (v "0.19.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bnbgf6pm3qa7jh1fqkqw6gdh20zly2sl374vv8fblb5288nl424")))

(define-public crate-oxygengine-ignite-types-0.19.1 (c (n "oxygengine-ignite-types") (v "0.19.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ldwbjhvjmxfnh9rwxa8m7d17v12x58ykv3aj6nyjdlr5imlnyfx")))

(define-public crate-oxygengine-ignite-types-0.19.2 (c (n "oxygengine-ignite-types") (v "0.19.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z8scrdh4vaqyd9l5m8igmy97xgva208glvfdyr92215m8ml8mfw")))

(define-public crate-oxygengine-ignite-types-0.20.0 (c (n "oxygengine-ignite-types") (v "0.20.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w450cpp1x2qbywab6k7ivxpnjdfv0fj2q6k7fi4k2kfcaghfmks")))

(define-public crate-oxygengine-ignite-types-0.21.0 (c (n "oxygengine-ignite-types") (v "0.21.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hcmwgba3g6m8h3qyp96wd7b36sc84c7x57dj099is62y18zmxsk")))

(define-public crate-oxygengine-ignite-types-0.21.1 (c (n "oxygengine-ignite-types") (v "0.21.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cv5jic4995yn4l2r8xz25lfhfbhf1jv5gnqgkc22p287jd944gs")))

(define-public crate-oxygengine-ignite-types-0.22.0 (c (n "oxygengine-ignite-types") (v "0.22.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a00w4jwmzvi6zpm028wrkwr0r088xnxf4vl6q73vkcajvdd25li")))

(define-public crate-oxygengine-ignite-types-0.24.0 (c (n "oxygengine-ignite-types") (v "0.24.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rvlk0ljxpfrksgc4hh2qxg2xh0diq9py3icbw4lzg99cjmcvhkd")))

(define-public crate-oxygengine-ignite-types-0.25.0 (c (n "oxygengine-ignite-types") (v "0.25.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xz76dcc0lfl61z1ivhz7d7k06fpgmh63dypnqz49i3ljn7llfhk")))

(define-public crate-oxygengine-ignite-types-0.26.0 (c (n "oxygengine-ignite-types") (v "0.26.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x9zzpr8vkdlklq1nyr0fl4c6gay9jrfd8bsh31gclhysmqqifh9")))

(define-public crate-oxygengine-ignite-types-0.27.0 (c (n "oxygengine-ignite-types") (v "0.27.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "191w7mbcmydm8lsy94gagr98f82z4x5yd7f97xps09hzaf7w0gbk")))

(define-public crate-oxygengine-ignite-types-0.28.0 (c (n "oxygengine-ignite-types") (v "0.28.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j9qw9cvj2aq8q205riybrmw2f3j5lmzscsznihzcfpsf0i8zmp6")))

(define-public crate-oxygengine-ignite-types-0.30.0 (c (n "oxygengine-ignite-types") (v "0.30.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "063wfa85vz472xvz9ih0z8a04n4nvj98dvb2cw4ma84lwapfljq0")))

