(define-module (crates-io ox yg oxygen_orbit) #:use-module (crates-io))

(define-public crate-oxygen_orbit-0.0.1 (c (n "oxygen_orbit") (v "0.0.1") (h "1jpaidia8h0bmlkgavdv0w484arv6wxv39547vmrgk2y6cldl0id")))

(define-public crate-oxygen_orbit-0.0.4 (c (n "oxygen_orbit") (v "0.0.4") (h "0d7m17i1hk8d802z10y8igsyaxvs88y8xhgj8dnwhanzx5bi5pyn")))

(define-public crate-oxygen_orbit-0.0.5 (c (n "oxygen_orbit") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)))) (h "1qawlzpvm3l7ivpnsz574mfs4azqil7izhrrqkfhppqg83aysv68")))

(define-public crate-oxygen_orbit-0.0.6 (c (n "oxygen_orbit") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)))) (h "1am8mxcilpzrg9i9d5vakcisa3dw84ch1dvbgiq4z8n1myrd3ndk")))

