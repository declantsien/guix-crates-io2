(define-module (crates-io ox yg oxygen_quark_derive) #:use-module (crates-io))

(define-public crate-oxygen_quark_derive-0.0.2 (c (n "oxygen_quark_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (d #t) (k 0)))) (h "19lmpsx2bg6s1c73lxp496p7w13na0ddjwmhhsyajnilakv4ma7k")))

