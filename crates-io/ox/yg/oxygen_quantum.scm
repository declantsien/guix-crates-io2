(define-module (crates-io ox yg oxygen_quantum) #:use-module (crates-io))

(define-public crate-oxygen_quantum-0.0.1 (c (n "oxygen_quantum") (v "0.0.1") (h "0wqn33k67kwnab9kkf07f5fihxbk72d60dpvmf9sbvnid6kn3zir")))

(define-public crate-oxygen_quantum-0.0.4 (c (n "oxygen_quantum") (v "0.0.4") (h "1wjy1gx1ihyvxkndl10682rghbvxpsyx3r4b73i22gwvl35lxh7j")))

(define-public crate-oxygen_quantum-0.0.5 (c (n "oxygen_quantum") (v "0.0.5") (h "0asbaj77j9abvhl4lrb4m8b2fbmqsks3h0gpys2qmy0zlm2n0avh")))

