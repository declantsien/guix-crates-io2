(define-module (crates-io ox yg oxygengine-utils) #:use-module (crates-io))

(define-public crate-oxygengine-utils-0.3.12 (c (n "oxygengine-utils") (v "0.3.12") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0khrqkyhsv20z90jhxb0hwnjjmw09aya9zainmbi5kigg2vy3j4g") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.3.13 (c (n "oxygengine-utils") (v "0.3.13") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bvadi95qy5ji29v2jn1001b1wpdwfq6764q6z5ilxya37mdrvn") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.3.14 (c (n "oxygengine-utils") (v "0.3.14") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n373fkwfkjajajz9qjs847kyjzps97r1x5axf04cspxbiz4kvnn") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.3.15 (c (n "oxygengine-utils") (v "0.3.15") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nsmkqy1q2zgayadhkviizh43hsa5q8adck7dzc8q50qijcy6cmc") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.3.16 (c (n "oxygengine-utils") (v "0.3.16") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g1pqnmccqd3mqflxgf1chk839m2cvxkadn07qzq2vsp2pwxqak6") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.3.17 (c (n "oxygengine-utils") (v "0.3.17") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ngic1qmpdq9023v93bsvsmym4zlp0wi36vn74q33w4rn4s611qg") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.0 (c (n "oxygengine-utils") (v "0.4.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d68a2xf8ralagav98cz8sdkycwcy3qnhrgih4cj18lmzb00wh6k") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.2 (c (n "oxygengine-utils") (v "0.4.2") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n3l4flk1wp9yl29p61mi7ahwj2fwj350vwsh28vs70vvyz90l36") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.3 (c (n "oxygengine-utils") (v "0.4.3") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14r05ick5zkhg8y9g9h1dj91q6wl5i0n6d00hz0a8lzynvma0yi5") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.4 (c (n "oxygengine-utils") (v "0.4.4") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xy3wpn7lm7mbrpkl2fy2nrq4i90lw0rml78y6qgabzw2cfkki41") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.5 (c (n "oxygengine-utils") (v "0.4.5") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16cm94ra7j5z72h06y8gqnxic6d8cm6zpb2fp32ixq0l493lmjwm") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.6 (c (n "oxygengine-utils") (v "0.4.6") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15alsqkxih8r29k00rjwfrzic7r3h8bvb6cik2a9gsr260hwzpsy") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.4.7 (c (n "oxygengine-utils") (v "0.4.7") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0713nqckqi0kwx3mw15ncz7vm21i8dwl7s4674r5ndd2rnm1jxqj") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.5.0 (c (n "oxygengine-utils") (v "0.5.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g17wdgn869bi11v6d3s26np56qjgrx776y4k1gsnmg9g1m2spsy") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.5.3 (c (n "oxygengine-utils") (v "0.5.3") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n0vkc5y2ky564gqbyk0rb7nn5bzld1rwjxjgrjgcw14i1gfxjpb") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.5.5 (c (n "oxygengine-utils") (v "0.5.5") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08gib4cpm3gsbmvsv4lmvxhm40f2rgzywwv8gxbllgaa8a4faxdx") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.5.6 (c (n "oxygengine-utils") (v "0.5.6") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gk1gii2lxak4ij91m569wsfiv1kr3r5h4bxc8lw8qsd722pr2ac") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.6.0 (c (n "oxygengine-utils") (v "0.6.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bbcn4x688xj9d0alipp110l0qsdk3lz9yf05gy97b9jqddb0118") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.6.1 (c (n "oxygengine-utils") (v "0.6.1") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qlrnmgnbmy2xrrws7blvrs3s72j3m8ffq44hcgjsx99kmv2000h") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.6.2 (c (n "oxygengine-utils") (v "0.6.2") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jwrvnz98rbmaznybp8i0ynzlkri1974pzw9s6iq11pg14k2jnnm") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.6.3 (c (n "oxygengine-utils") (v "0.6.3") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "041giw8mc5ypn4nwdkq90xdsi92grygv4mp7hk6fsny6bz90875d") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.6.5 (c (n "oxygengine-utils") (v "0.6.5") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0frg019bpzb1dhlhcgv05l3pqw1mcssfqj3pgyw1bbxav16p7z7w") (f (quote (("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.7.0 (c (n "oxygengine-utils") (v "0.7.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16pqcjxkgwqayxprz366qdvf2z4q7zmpbbfa7spn1cfy6fpahig7") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.7.1 (c (n "oxygengine-utils") (v "0.7.1") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gyjryds8cnrimsw8kbr6xdd7xb2cldabjxfa5k63whks2q1lmjq") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.7.2 (c (n "oxygengine-utils") (v "0.7.2") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gy1s3i2bvw7mky0cqj6hibdk4rpf0vr0r6ix33xdlz8v67afdci") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.8.0 (c (n "oxygengine-utils") (v "0.8.0") (d (list (d (n "noise") (r "^0.5") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l9aps02qw3f6yifd7m1sxnr5qbbl0bm8vx6i8rf0qsrp158rg6p") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.9.0 (c (n "oxygengine-utils") (v "0.9.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lsbl2prlcm2r6w9qwzvs2ylc89fc5sy9q6wk09nxz5gskiwc3h8") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.9.1 (c (n "oxygengine-utils") (v "0.9.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lp4p5zhz0xfk6a539g2l0yfibnky26cpbsqkwkzcx2ha5xki44y") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.10.0 (c (n "oxygengine-utils") (v "0.10.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a0gjnakclpwzlkznx9hh2i27yqgnxd239r2bwk6y0li5z70qb1n") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.10.1 (c (n "oxygengine-utils") (v "0.10.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vx3zd9ywx5l95wc19r0q65g1any36nnhdishhqnyj3i2c02c2vs") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.10.2 (c (n "oxygengine-utils") (v "0.10.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v9r0pimbksqf90hps0jx8b5hv7j4bzyifjw4hd4afgq4sl6kb8j") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.11.0 (c (n "oxygengine-utils") (v "0.11.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09h29gfkckay5p2pn4d2zin4ds6xn9xghgpzvrzxrj5dgc62y87j") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.11.1 (c (n "oxygengine-utils") (v "0.11.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "034a7nvzj1cxikrq8pawb7qj20yv4lpfamqr8ag7n5dj9ka0802g") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.11.2 (c (n "oxygengine-utils") (v "0.11.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pcg66p7anmmf55fp5cgx185v14kw966ndpgy7zdlyyfblif6gng") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.0 (c (n "oxygengine-utils") (v "0.12.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cj6xzvs4syhqlmz9gk28y7sb38z3gmwlh7lv2432j8pn4h8b216") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.1 (c (n "oxygengine-utils") (v "0.12.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k277m0q1sy34675szd3qfzvygnp4irxndc2cq5422x89pgqnnbj") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.2 (c (n "oxygengine-utils") (v "0.12.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k5pyaabnrxdpxjs8gahiyz5h55l352223x23ypw4ycmmdfi4mb6") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.3 (c (n "oxygengine-utils") (v "0.12.3") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b06xcwl6dxfh9q6xqdyz69mgswjmyn6c250qc4xaj8izca3yraz") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.4 (c (n "oxygengine-utils") (v "0.12.4") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ck92qn72f8072yycavrbw95h5raacdkgyy76s2hhxnm0rm3hz70") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.12.5 (c (n "oxygengine-utils") (v "0.12.5") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19hl5zkxjk3j92n4g17qnffhq3v5xhr3zj8jvzq4l422ynn5m5qa") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.13.0 (c (n "oxygengine-utils") (v "0.13.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w19svmrn1lc7n2xqwjd534f2shb10c9sdxfjslx6v6dnjl7ig3j") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.13.1 (c (n "oxygengine-utils") (v "0.13.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fl6qvhl0cac646rcdr36b267rxpmd7whvw3cbn887j5p2pl1104") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.13.2 (c (n "oxygengine-utils") (v "0.13.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xc253fj02wak2dwas7xfva4mijbv6501ylgwvncvdvw4p2wj7j3") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.14.0 (c (n "oxygengine-utils") (v "0.14.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bid8cx5v44c63kgwn5422xk8514jrb3ys23cr1k2l2gsnb251fn") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.14.1 (c (n "oxygengine-utils") (v "0.14.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cpma289akxlcak7xv57hy1q55pfwwcp0i97q9r475pqymrf9pnk") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.14.2 (c (n "oxygengine-utils") (v "0.14.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v8zxvd1h870pfxfgb1x6194gz3k9crhlmyl2nmvpwl7v79y6d5x") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.15.0 (c (n "oxygengine-utils") (v "0.15.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "190l8kym4dwm5ynx559zaljxvnws4gvrar9qhwiym580fly99bd2") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.16.0 (c (n "oxygengine-utils") (v "0.16.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ax8xwlkb13lckn919g3457qmzjdqyyjnxnvc47j2hh7db2bb9dl") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.17.0 (c (n "oxygengine-utils") (v "0.17.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0giilwmpxywybbbpzsvgj9rjqr64bslqxkh6bpqd3fi0jzsl1i0a") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.18.0 (c (n "oxygengine-utils") (v "0.18.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vp7jgqsxcpqnqbs1kgfny9af4nfd2cqlngzhz4gq4qh7gvksda9") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.19.0 (c (n "oxygengine-utils") (v "0.19.0") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11ncvczpaqrlxg0nx1nyjlaadbqlclr1jbx7q44xdylmr8ibvqij") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.19.1 (c (n "oxygengine-utils") (v "0.19.1") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00z1z0wpdgfv1al2a5yv5fqp1gf794sfzpr4caysca6ygkn04c8m") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.19.2 (c (n "oxygengine-utils") (v "0.19.2") (d (list (d (n "noise") (r "^0.6") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cpza9bf37axfl15b6pb8g8apzvskyd6r5i1a0h6f39fnz9aw7hx") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.20.0 (c (n "oxygengine-utils") (v "0.20.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10h08vxcnifcq0dbgm9pb2ymhj8fivz1ab3i00fw5angfp469m9c") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.21.0 (c (n "oxygengine-utils") (v "0.21.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jp5a00iy6fl02dfpg0k6n8qp7nyryr5vcm6xafq7r5584jh3ixj") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.21.1 (c (n "oxygengine-utils") (v "0.21.1") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fgnbdzds4l7rch6wjflscwqvnvk84ariis39vhli7gxk91vl1bn") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.22.0 (c (n "oxygengine-utils") (v "0.22.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b60yi4zx4xjd537r7wl3h37wrhsg57vb1d4j9xns2ryfn4nb7ph") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.24.0 (c (n "oxygengine-utils") (v "0.24.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15dmnb43bdkb2rm25q6rppr3x0p94rvg8f35rilqc4n18w4pmapn") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.25.0 (c (n "oxygengine-utils") (v "0.25.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1llzbkixl9swx9g5grmflxmm9b5fjm3vsrqy3nqir97cv87n3gak") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.26.0 (c (n "oxygengine-utils") (v "0.26.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y0n6k2acvkybgpnngbvr2p0pcs7y0hcfpl3r621dlvxr1wbz9yh") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.27.0 (c (n "oxygengine-utils") (v "0.27.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03i5j6rbny78i0bylwx7n7s2fmn6fd2yzkwlisx7q3m55560kzx7") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.28.0 (c (n "oxygengine-utils") (v "0.28.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v2xlaz62vzncwnsdvq297a6nf0c7hsj42fy1vrnpwdmyav027ay") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.30.0 (c (n "oxygengine-utils") (v "0.30.0") (d (list (d (n "noise") (r "^0.7") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04kq6xp3zig9b597wgzvw232rjiy91g1sc3plxx0kfzksqmhd1b7") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.40.0 (c (n "oxygengine-utils") (v "0.40.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "094r8jpa9w5lmcxirsb4z6vppyx63ikhwx3vwhwlkz1vvk947vg1") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.40.1 (c (n "oxygengine-utils") (v "0.40.1") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bha62hjsgnwkh5rgdn4nngzd4dfl28qh4cshcq0fh6dr7bhg16n") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.41.0 (c (n "oxygengine-utils") (v "0.41.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xgw5xxvr2cn3ijqm4s780l89dlq7agh01qx9fccpivm5npg89xg") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.42.0 (c (n "oxygengine-utils") (v "0.42.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dbsqcil5f7mdy9z80rynmr2lwnbl3qvysaycddbx6hs3qw8yha2") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.43.0 (c (n "oxygengine-utils") (v "0.43.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lx0y4s5ck1139ccbhbzd48ipc820gvz25f3n1p0jn97x28lhjsz") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.44.0 (c (n "oxygengine-utils") (v "0.44.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v4rx0xychgdas6wspf2hglh5hsb4wdzzgldlvn4gv0sdgn9czv8") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.45.0 (c (n "oxygengine-utils") (v "0.45.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ynvrmsv3nc2hhpfarmlcz27la2w8badm1zzq6j0kcw2mzi6sn5v") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.45.1 (c (n "oxygengine-utils") (v "0.45.1") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04sid18cza6yhwaj3brlg9zxkv9g7iw3djibk7qz46m4r13wickh") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.46.0 (c (n "oxygengine-utils") (v "0.46.0") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n3nf2zfbgx0ffv1ik7nli8ml2b7pj1pmq5gccbbvxbi0wsxlbfd") (f (quote (("scalar64") ("parallel" "rayon"))))))

(define-public crate-oxygengine-utils-0.46.1 (c (n "oxygengine-utils") (v "0.46.1") (d (list (d (n "noise") (r "^0.8") (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rk3n3mrfngqv465dcpn8xbw0958smhl52zz4iam6zxqzpxsa1si") (f (quote (("scalar64") ("parallel" "rayon"))))))

