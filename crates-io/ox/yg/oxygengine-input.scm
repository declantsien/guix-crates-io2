(define-module (crates-io ox yg oxygengine-input) #:use-module (crates-io))

(define-public crate-oxygengine-input-0.3.0 (c (n "oxygengine-input") (v "0.3.0") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "00mdi5kgnvalvj7bw469a8rdh87y3djb5yldni9z8g484ircja2h")))

(define-public crate-oxygengine-input-0.3.1 (c (n "oxygengine-input") (v "0.3.1") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1ws8w0jl3c94b10bn819q6ckaigd06ghzwmbbap422hj5a32lnjp")))

(define-public crate-oxygengine-input-0.3.2 (c (n "oxygengine-input") (v "0.3.2") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "06n3kbz0ampx7zfnmww78g7nvjygd1bgfnlmx144kml2av6xxgpg")))

(define-public crate-oxygengine-input-0.3.4 (c (n "oxygengine-input") (v "0.3.4") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0mvjxv6dqk86caz107a80cm0zb3j5xk3g0awy1w31f43hgihq7cl")))

(define-public crate-oxygengine-input-0.3.5 (c (n "oxygengine-input") (v "0.3.5") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0v76y303i1mhdynkr777wan3qffsq210ayfzi7x2i8ljacjqc3sm")))

(define-public crate-oxygengine-input-0.3.6 (c (n "oxygengine-input") (v "0.3.6") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1fql89jiyyz0pc1crnc757f0xcv1dvv7k4k0294h1rpv54zaah1x")))

(define-public crate-oxygengine-input-0.3.7 (c (n "oxygengine-input") (v "0.3.7") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "048kkqjp707j6382wspvikyynbz5szzp13pf3svdss56619lqdqq")))

(define-public crate-oxygengine-input-0.3.8 (c (n "oxygengine-input") (v "0.3.8") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1npa4jq73zkv296djai97cgdn2zjg6xzn0m4iahivhyshcr9s7qy")))

(define-public crate-oxygengine-input-0.3.9 (c (n "oxygengine-input") (v "0.3.9") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1qsbqfjk4mq833z2rhx81jazrrhq8dd2d7wrjfxl6p5wynrcj467")))

(define-public crate-oxygengine-input-0.3.12 (c (n "oxygengine-input") (v "0.3.12") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "1jl73j5d2x8hjqfjy40p22bx7vnacgzxzaj0qvrx45y1scl1ph3g")))

(define-public crate-oxygengine-input-0.3.13 (c (n "oxygengine-input") (v "0.3.13") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0nzccizwiwhlz2mbplsj6pjjf2yxm9dfkryyfkjixajy0xwlbi4y")))

(define-public crate-oxygengine-input-0.3.14 (c (n "oxygengine-input") (v "0.3.14") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "0kayk7pwgykz6zh2hkdhdaw61bbyis4wmm0p40kzhab7hrck40qd")))

(define-public crate-oxygengine-input-0.3.15 (c (n "oxygengine-input") (v "0.3.15") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "08dzmhyg9y89fvcv29jnnh4lfxqxydn52n4zkjk7hzpzvhlx9slf")))

(define-public crate-oxygengine-input-0.3.16 (c (n "oxygengine-input") (v "0.3.16") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "11linhhr8r5dx4i7sn8nn0vfv281amh02gd4l8l0nh7kzjv1kn65")))

(define-public crate-oxygengine-input-0.3.17 (c (n "oxygengine-input") (v "0.3.17") (d (list (d (n "oxygengine-core") (r "^0.3") (d #t) (k 0)))) (h "14cmpp5qiqmk6lj7cap3n0w552imxpjnxfn1hm6a8y28q2dbaq1p")))

(define-public crate-oxygengine-input-0.4.0 (c (n "oxygengine-input") (v "0.4.0") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1fhgaybrsm4frpdx1nimgv8h620di7823myh2r8rilcy7ryyqax6") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.2 (c (n "oxygengine-input") (v "0.4.2") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "15liv8rwbarj3n2r87f80bylagyqdm65abvlbkihqlzh9z529g7p") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.3 (c (n "oxygengine-input") (v "0.4.3") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "0lp3dblhmqyqygqr583avlq2hl0p0m55x0m54j0nxnjphkf1kzik") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.4 (c (n "oxygengine-input") (v "0.4.4") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1r8p8xv4b209i7y8914klpiacm5znxf98r3iix3ig3lvrc5gazlq") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.5 (c (n "oxygengine-input") (v "0.4.5") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "00l21kv46hisjgyyz16l8hxqmpkxb907q60cjgkj2q4lbhdnrdks") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.6 (c (n "oxygengine-input") (v "0.4.6") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "0ycww6xchm0i0g8qlvw2cvib447lh1afhh5f2kqdb6g698ka1aiw") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.4.7 (c (n "oxygengine-input") (v "0.4.7") (d (list (d (n "oxygengine-core") (r "^0.4") (d #t) (k 0)))) (h "1rv7gq7zkr4asgfy37kjsnvfymyfbcrp0x6gah2hg38v862n5zly") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.5.0 (c (n "oxygengine-input") (v "0.5.0") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "0j8cfzawykgpqx761pvm4shgh3973kyxvanpk89qrc16yrz3lfk2") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.5.3 (c (n "oxygengine-input") (v "0.5.3") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)))) (h "16jcbpskwwivjzm2ghm4s5aqgafyzbbjn322i83cn84xdm9q52a9") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.5.5 (c (n "oxygengine-input") (v "0.5.5") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rk1znhpi5y198qrxpa19gvmy8qc3viwkspbl4cnbairhgd9yfyy") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.5.6 (c (n "oxygengine-input") (v "0.5.6") (d (list (d (n "oxygengine-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0irrhdayh12vmjs1nd0p65344w9cr4d0v6xs7misfzpd4kbj78am") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.6.0 (c (n "oxygengine-input") (v "0.6.0") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yi87d2h0dsdpj8fp75xa2xylq1ii55iad2pvak50yy7s7pl8qpc") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.6.1 (c (n "oxygengine-input") (v "0.6.1") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f49kndssggcsiicvhxdy1fj93ip4h58r73iypn103hifyr6yl0a") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.6.2 (c (n "oxygengine-input") (v "0.6.2") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xajc6b089xcryf408sl0kfnm3d8l6aj3swj2y91av4059a9nf4h") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.6.3 (c (n "oxygengine-input") (v "0.6.3") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r74x3qv4nm5k7jp9fv95s2zfgj6a97k7smb7n89klkaz0azdy86") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.6.5 (c (n "oxygengine-input") (v "0.6.5") (d (list (d (n "oxygengine-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1znwlxbjsfz9l81md14a4p7sc2xvqjlbhldwdfi21x44j2j5xlk2") (f (quote (("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.7.0 (c (n "oxygengine-input") (v "0.7.0") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02ga8grjiay9p57k4zspkr6wiasiqbc6prpdy638sbmii6mr7rsq") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.7.1 (c (n "oxygengine-input") (v "0.7.1") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1130rqwif6dqmsm20dgl22cnllvynn3b1a0ainbfjy0rpnf94jix") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.7.2 (c (n "oxygengine-input") (v "0.7.2") (d (list (d (n "oxygengine-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0apggbm6crahdbb2dk33494nf8v17cqmxmxka8cqv4a08b54xr9r") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.8.0 (c (n "oxygengine-input") (v "0.8.0") (d (list (d (n "oxygengine-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xamswra9dfyrmv3m85n7sbna2ddn3f5xc6pl1kw1ppbz590k7q1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.9.0 (c (n "oxygengine-input") (v "0.9.0") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07z0xm3dkljw56m4f3i030r9bbhz0vxsi22b6qpx0810xnssk9r2") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.9.1 (c (n "oxygengine-input") (v "0.9.1") (d (list (d (n "oxygengine-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r0ngyws19a5kiv0zy8s3zfar9lx1iavaq0pkspf3lm867div3x8") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.10.0 (c (n "oxygengine-input") (v "0.10.0") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nlzfw856hpx21cbqapcjb0w0c4qsc0ibq4i03ib0xvlmwwsgwxn") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.10.1 (c (n "oxygengine-input") (v "0.10.1") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ljria3jgk1dmcsf9gmzjsvsnsqxn9rn1m8jn9lsx8wp6iwb56k3") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.10.2 (c (n "oxygengine-input") (v "0.10.2") (d (list (d (n "oxygengine-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wnk5r2iz91pmkjrf0n9yf1sc174j33sr03wsgkwrddg6g1g6bh3") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.11.0 (c (n "oxygengine-input") (v "0.11.0") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yh3zc6vn3w0mdnjzn56i5jh9sqlayq2nbf9h2li10zzdm37dd1i") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.11.1 (c (n "oxygengine-input") (v "0.11.1") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c0ygjbd98b833l9x94hdgrwvh8bi54pf9xnsd5n69jja48i2x9x") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.11.2 (c (n "oxygengine-input") (v "0.11.2") (d (list (d (n "oxygengine-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00pz07d9a4vsvwiv9xv5x7pjs710g1sx4vk4bzzlff01xjqf2db0") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.0 (c (n "oxygengine-input") (v "0.12.0") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dxk4y1k584gqdz1jzlq9ry82m8ha9ny8g051237imlmgxi5zyvj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.1 (c (n "oxygengine-input") (v "0.12.1") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "151vm6pnm71yphjl4m9x8myw1mzbg8hxq03m5zwxpzrsa0fhps43") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.2 (c (n "oxygengine-input") (v "0.12.2") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07gb342ijj0pnldzp7b2kbcri69qy2w5bq1zryjyvgxqn4w0yx85") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.3 (c (n "oxygengine-input") (v "0.12.3") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kv8iwlp1c2w3vb3gaj9l5b1r2lsk5jbv9g034qlw58hvwqrc8z") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.4 (c (n "oxygengine-input") (v "0.12.4") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ky00g107dn0diq47gbv2c9jyz41iabn5a8nzg7cf6yxkc1cljwr") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.12.5 (c (n "oxygengine-input") (v "0.12.5") (d (list (d (n "oxygengine-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p59a1py423d71d69kgw8ccms7blrfr2k9cv7fb1is69vl5zkgrg") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.13.0 (c (n "oxygengine-input") (v "0.13.0") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q47ysp0yfvrpncr0l1wpl06a48kai2zkmqgbj07xhmlzg1mm8xr") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.13.1 (c (n "oxygengine-input") (v "0.13.1") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "041rm8qg0ivb3n0k894q72fcndw28i3l36j7h2nj8z0cvxdw9ddw") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.13.2 (c (n "oxygengine-input") (v "0.13.2") (d (list (d (n "oxygengine-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "173c62vpd19zsr8j2sl39skky21b4lrw0p5yi5fq1vaw6phj5rhk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.14.0 (c (n "oxygengine-input") (v "0.14.0") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1shbn5nbawzxb6cqzh9g73lq3yk9s00z5xvm6cxx2cwwhzhlfv48") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.14.1 (c (n "oxygengine-input") (v "0.14.1") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p7fqf0588594g6g1nhvxicag11l8ik8wb2wjzpix068mw0h3vn7") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.14.2 (c (n "oxygengine-input") (v "0.14.2") (d (list (d (n "oxygengine-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15yyf8j2mxkzx6pdm951gp9nyi9czx4h2n5f5ix5r02zzkmj4rrh") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.15.0 (c (n "oxygengine-input") (v "0.15.0") (d (list (d (n "oxygengine-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l61lwr8gplc8zfh854vp5dl8w2vpyp5wjr57j281l2ym7d68rpl") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.16.0 (c (n "oxygengine-input") (v "0.16.0") (d (list (d (n "oxygengine-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06kfv6kgisrwv9yqn8a2szxhq73f50j1hllk59skl5xq6dp339cd") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.17.0 (c (n "oxygengine-input") (v "0.17.0") (d (list (d (n "oxygengine-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kjin17ikc16s001zj41c446d06y2rpazqmmfa8a3ylb4zxhxsf5") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.18.0 (c (n "oxygengine-input") (v "0.18.0") (d (list (d (n "oxygengine-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h9bcijy23l4d99rp49n0mcww9c95wgmpl348xs17483h3qlan54") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.19.0 (c (n "oxygengine-input") (v "0.19.0") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "199wpxdbgghkcwg8kdbvhamnkcmwavq5kd8annvd0ankz6ya1yzh") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.19.1 (c (n "oxygengine-input") (v "0.19.1") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l84s1hl8619viffgxgq3sc89as6n57p4pnd95vw8i530n9a6y02") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.19.2 (c (n "oxygengine-input") (v "0.19.2") (d (list (d (n "oxygengine-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hrmsylzxfby9bpkr1vazq3lzxwr3cid36x8gy1j8nprqhz8da3v") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.20.0 (c (n "oxygengine-input") (v "0.20.0") (d (list (d (n "oxygengine-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "075wlsg4mz2n50dyll836kc8xvwq6y4n5009z58z68bw6003ha78") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.21.0 (c (n "oxygengine-input") (v "0.21.0") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xm8ibs7xazqf4xs2skigdg29260wzqvq3h92r7ydwcn2jbgc7nr") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.21.1 (c (n "oxygengine-input") (v "0.21.1") (d (list (d (n "oxygengine-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00k4113av92n877fhpimy4mn5x5r7br7y9f2x8wni0zb8qch7213") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.22.0 (c (n "oxygengine-input") (v "0.22.0") (d (list (d (n "oxygengine-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z5fz52jpvqilkkb5c6sfs1ih8niwiypa1d6ig3va4yx10fdpm6m") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.24.0 (c (n "oxygengine-input") (v "0.24.0") (d (list (d (n "oxygengine-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i6f5x6485j16j1lw6r69h9k7fqayhpc0s5vja9p4blxhng3mhkj") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.25.0 (c (n "oxygengine-input") (v "0.25.0") (d (list (d (n "oxygengine-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17dvdpjjg2kncn06p141iqhsylf15g96xgandljsncf9mqfgwsw1") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.26.0 (c (n "oxygengine-input") (v "0.26.0") (d (list (d (n "oxygengine-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1glxchylf02kcx4hpdfm0daskmgsvmfg06y90cygkfc2jxmk71mq") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.27.0 (c (n "oxygengine-input") (v "0.27.0") (d (list (d (n "oxygengine-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s24av1rx2kdqdvbdd731iwdd6pdq40pgamr7yrzxvqq3ah9b1bl") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.28.0 (c (n "oxygengine-input") (v "0.28.0") (d (list (d (n "oxygengine-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v3mh2va5kfmag3k1aaky3i8kva8laklnlgssvi75gzm9b9ylpm8") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.30.0 (c (n "oxygengine-input") (v "0.30.0") (d (list (d (n "oxygengine-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04b9qiz0s2kms87kjp0xdjg06mhsbcypyzsxwv8i02jdmqh5wb5j") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.40.0 (c (n "oxygengine-input") (v "0.40.0") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1gndzh4dlcwrb25h4v53hcvd3w9j5nf39ls0jnpb5cpb8mygm60m") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.40.1 (c (n "oxygengine-input") (v "0.40.1") (d (list (d (n "oxygengine-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1ph8mj7hj0hf2ahaql4xzmy26qifppbsl02afc6rlrhp7mj3pn1x") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.41.0 (c (n "oxygengine-input") (v "0.41.0") (d (list (d (n "oxygengine-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0gj7hfs1wljn84dwmxqa2x5ds1g2jrl65sd90d1qd2qbdsm4fhqc") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.42.0 (c (n "oxygengine-input") (v "0.42.0") (d (list (d (n "oxygengine-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "18bdfc1f62yhcjpzqf5x8x5g44yyy62z8xirlycw3iv4fx0hf736") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.43.0 (c (n "oxygengine-input") (v "0.43.0") (d (list (d (n "oxygengine-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0r468r0dhmbzxh0h4vrb61rn5rkrm7dw7w206180fmzgys7iazwv") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.44.0 (c (n "oxygengine-input") (v "0.44.0") (d (list (d (n "oxygengine-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0qihq4isjllr9vbr9dc6r42v0yh07bnmn3j5q24f6jsqfyf9vq0c") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.45.0 (c (n "oxygengine-input") (v "0.45.0") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1s9lwpinb30ijqglsqk6npl9xj8cay3i7j5fmix7hw3i855wc7gl") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.45.1 (c (n "oxygengine-input") (v "0.45.1") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0h1wxvz8fyx9k33ygaiacrrzb3nx6qr1ix1rvajq6pk13kb1gm7i") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.46.0 (c (n "oxygengine-input") (v "0.46.0") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0zxca9r404hq3myfw2kcdrzj25msx3l0syfr46bvq9s9xcd6c5ci") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-input-0.46.1 (c (n "oxygengine-input") (v "0.46.1") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "03z1rxmhy40ib6v0dgdzpi98nacdv7lv4k52fmw479lnc1ng93zx") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

