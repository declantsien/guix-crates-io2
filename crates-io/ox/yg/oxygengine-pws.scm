(define-module (crates-io ox yg oxygengine-pws) #:use-module (crates-io))

(define-public crate-oxygengine-pws-0.15.0 (c (n "oxygengine-pws") (v "0.15.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.15") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pc6sp0z4dkzhyrmqxglsx6h9famh67rl15r37mx18pcg1vky640")))

(define-public crate-oxygengine-pws-0.16.0 (c (n "oxygengine-pws") (v "0.16.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.16") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0069ppyjf2l41xgkyl5iwdvhqmzj2vylmb44xz2mkyvljhxp95nn")))

(define-public crate-oxygengine-pws-0.17.0 (c (n "oxygengine-pws") (v "0.17.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.17") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ranh47hmqqqh5kn9lk422i39gxnifvlha7dfkdh9j7yxfyk8xqb")))

(define-public crate-oxygengine-pws-0.18.0 (c (n "oxygengine-pws") (v "0.18.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.18") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1c6s5whmkj9c4rz1kvykldsp41vf9wfy4fz90igb28ppwci00xjl")))

(define-public crate-oxygengine-pws-0.19.0 (c (n "oxygengine-pws") (v "0.19.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.19") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0sc0mn8vc6j7ws5484ai92fjn4fv2wsg8v5hqqs4h74cvayf381k")))

(define-public crate-oxygengine-pws-0.19.1 (c (n "oxygengine-pws") (v "0.19.1") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.19") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nb2h6i78fvl9ly5wkz3hl0g4fdhhgy81kcfm56rarfnrlvipxfv")))

(define-public crate-oxygengine-pws-0.19.2 (c (n "oxygengine-pws") (v "0.19.2") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.19") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1cirrm0ncib2wz1i2zzjnvnvfrz4iqpb2m4wdid0ybx0gf1dzz49")))

(define-public crate-oxygengine-pws-0.20.0 (c (n "oxygengine-pws") (v "0.20.0") (d (list (d (n "minifb") (r "^0.19") (d #t) (k 0)) (d (n "oxygengine-procedural") (r "^0.20") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07fqh8nx8ql1y8bf9pfwng0lid9rnsz5gkq3b5c5cfrhv06j4ix7")))

