(define-module (crates-io ox yg oxygenate) #:use-module (crates-io))

(define-public crate-oxygenate-0.1.0 (c (n "oxygenate") (v "0.1.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "notify") (r "= 5.0.0-pre.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0gqr32xvfqy7v0wswn5261zr04jdyywgrnygbc4hgjdfsmhys88x") (y #t)))

