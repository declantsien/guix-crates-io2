(define-module (crates-io ox yg oxygen_emission) #:use-module (crates-io))

(define-public crate-oxygen_emission-0.0.1 (c (n "oxygen_emission") (v "0.0.1") (h "1qixv2f6ajb46p23z2k5vh8nipywn7i2rizqmy5s1x8px7iyajps")))

(define-public crate-oxygen_emission-0.0.4 (c (n "oxygen_emission") (v "0.0.4") (h "1hq8y6gv142gi01ddb1sdsfwz5bmyd31b3jkpm11gjg897ljq3gb")))

(define-public crate-oxygen_emission-0.0.5 (c (n "oxygen_emission") (v "0.0.5") (h "11cwykxmvm725cx5l8wns29w9zwiglmx7621fn7ki9yxm97ggc23")))

