(define-module (crates-io ox yg oxygengine-nodes) #:use-module (crates-io))

(define-public crate-oxygengine-nodes-0.42.0 (c (n "oxygengine-nodes") (v "0.42.0") (d (list (d (n "oxygengine-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jgqbyaaq71iim9x9siq52sk14dka6d2s6dmnb725a26zvkh883d") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.43.0 (c (n "oxygengine-nodes") (v "0.43.0") (d (list (d (n "oxygengine-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gbq2ics5aqizxcqpg005iqad2cvrlqa7j5k692gkvm1w6f33f9i") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.44.0 (c (n "oxygengine-nodes") (v "0.44.0") (d (list (d (n "oxygengine-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fs3i07bhk694rn0qc21fw12m6s78rjzx82qd9wpw1yrrj60r1vk") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.45.0 (c (n "oxygengine-nodes") (v "0.45.0") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c1ckdwh1vwfpsjvd88lcc5qzyij6hq99pbdm5xhkwfw83cmvs0n") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.45.1 (c (n "oxygengine-nodes") (v "0.45.1") (d (list (d (n "oxygengine-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s3vbj08wm24qpfbwh94as2c8ckm6r7w0kf5l4vska2aqmkval2r") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.46.0 (c (n "oxygengine-nodes") (v "0.46.0") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pvixz0sqnhy39476plm24nawc0f3kczrqh499kjivq5nacrf8zs") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

(define-public crate-oxygengine-nodes-0.46.1 (c (n "oxygengine-nodes") (v "0.46.1") (d (list (d (n "oxygengine-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mid6clr7gc3h1ybcr4riqzqg996d7dn5n974ll8gdmcizg9lnv5") (f (quote (("web" "oxygengine-core/web") ("scalar64" "oxygengine-core/scalar64") ("parallel" "oxygengine-core/parallel"))))))

