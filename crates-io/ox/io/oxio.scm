(define-module (crates-io ox io oxio) #:use-module (crates-io))

(define-public crate-oxio-0.1.0 (c (n "oxio") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)))) (h "11dxmvj7b4wn2fx9d42bdsi4dj6gy0m7hmpbgmrf57bcfrlz43g3") (y #t)))

(define-public crate-oxio-0.1.1 (c (n "oxio") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)))) (h "008mz4iwh2q17chnrcfzvpxlcrxxd3n5l9bf163j3zcn9qqlqdna")))

(define-public crate-oxio-0.1.2 (c (n "oxio") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "19fp0x3qdnhz61d1dc2war0ibdc5q9y56cqxabhh0f1575hlh0fr")))

(define-public crate-oxio-0.1.3 (c (n "oxio") (v "0.1.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)))) (h "1cbpmzjblsv7scd84bqvmscxx19gy4sc5mp169ra31xppv9iyvbn")))

