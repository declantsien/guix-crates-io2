(define-module (crates-io ox ro oxrocksdb-sys) #:use-module (crates-io))

(define-public crate-oxrocksdb-sys-0.3.0-beta.2 (c (n "oxrocksdb-sys") (v "0.3.0-beta.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17m6rsl4a99zvj62p9lll69nlhkqndc3sk8fgbg9wjw8yq1s9nsb") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.0-beta.3 (c (n "oxrocksdb-sys") (v "0.3.0-beta.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hsslzy8v19ldq3x37gwqn4m31qv41v1sz6yaga1l8w18gn0zppp") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.0-beta.4 (c (n "oxrocksdb-sys") (v "0.3.0-beta.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x3sqyznbgzrbfbjar7q2mzi0gbcmxp3icrr316zxjkw6dllq2x4") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.0-rc.1 (c (n "oxrocksdb-sys") (v "0.3.0-rc.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jvyx9kwz0vxyschvxq8ymzd0qj4j1flg27ahp9my9acn450a6ci") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.0 (c (n "oxrocksdb-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14xhp56nyhlyq97amkp9ngdmdsndrrdpy6a8a3l6xzdyr8md5jhl") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.1 (c (n "oxrocksdb-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xk4dsjq0pz81ra4vy99k0ihwpxw4h610ygrb4xnpxq32l4a6zd8") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.2 (c (n "oxrocksdb-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wk8ly9r9zgqdzj9gc32k7p7pxwi0qmqfkr63b2bl8s2dlv1ck4v") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.3 (c (n "oxrocksdb-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bqjsg86rwl813lsavqpx3ci2vv6yplx3qqx3l35badq5qnmvcd9") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.7 (c (n "oxrocksdb-sys") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ywwvxgs744symph5hw4lbzi97pmb7j5ljhqzxh8jdsy8pp7k8wd") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.10 (c (n "oxrocksdb-sys") (v "0.3.10") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "046mgdd27q8nr57cqqydi73fsh9drf7fn30ila0c086ja4bl09sn") (l "rocksdb")))

(define-public crate-oxrocksdb-sys-0.3.11 (c (n "oxrocksdb-sys") (v "0.3.11") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18vb3l6d9wb9764nsbp9prswqvnnx1lzjsizday1sf8hrazcxjx5") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.14 (c (n "oxrocksdb-sys") (v "0.3.14") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bq3p4w591k6z98g1v1zlvg632nzj9csy6g7mwnbixbwqddc80sj") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.15 (c (n "oxrocksdb-sys") (v "0.3.15") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v7rq0vgw3mm3dnabqhdbmqj2dvdz1civbwmf1jqq6cgm8yy20ar") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.16 (c (n "oxrocksdb-sys") (v "0.3.16") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b6hh54si9ancr4fhgfw0s70akd8i0ncipwbs0bma6pz81s4kk67") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.17 (c (n "oxrocksdb-sys") (v "0.3.17") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vawl0vwxi6iapss9j37rzs5z62bmhvls4kvhzx9hnahbdbrdnfl") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.18 (c (n "oxrocksdb-sys") (v "0.3.18") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pd2jyk0csrwdfmc4lly7ys8c56dh83y5znjkpdrh75bpfbc5cir") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.19 (c (n "oxrocksdb-sys") (v "0.3.19") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "010ix9ppc6y44i4697xa1hirhhlf6frf1zwnb57icxqa88s1nqny") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.20 (c (n "oxrocksdb-sys") (v "0.3.20") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f3vnf59krjpaj0pgbar6csmhfij0lqp7sbhdnc71b0zz338qpqa") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.21 (c (n "oxrocksdb-sys") (v "0.3.21") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vlx427rvzs4hyk5ca5jysnfq957fxgk2vd2xbdpm7lm7gvdffyc") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.3.22 (c (n "oxrocksdb-sys") (v "0.3.22") (d (list (d (n "bindgen") (r ">=0.66, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1431ndygfxlsjz99bwksdr54vbby26y4myxn2jljjl566ggxxscs") (l "rocksdb") (r "1.60")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.1 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.1") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0ans18y4van8fpppyg53nv8s0j9ix88hhy5w5xzwx6hnarnyrlif") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.2 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.2") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1apm0jw1z5f3nkxcfc4jzdkvrrxnmsnnbv9byj53h0glnyiz7342") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.3 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.3") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "10zazk18338sh2qyswj5idjmfq33z8j987gmgaf9hks5lygn38cm") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.4 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.4") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1qp1ldbbr10aj16dfb229glfblwkl2c5kvad8qnymc8xd1zjngwz") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.5 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.5") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "11h4kcwi92xdv2gl4k5c3nrns7pbsc9xmvw66fs66c9nqmnmjg00") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.6 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.6") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1g59rqlds7dpfi880fdi1cn7prizfry02hqmkpmgcgqwg94y0wjz") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

(define-public crate-oxrocksdb-sys-0.4.0-alpha.7 (c (n "oxrocksdb-sys") (v "0.4.0-alpha.7") (d (list (d (n "bindgen") (r ">=0.60, <0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0qc95mlqv5kh9mpi1lpg646j4593zh6nnmvacgphfm5m6xc520fv") (l "rocksdb") (s 2) (e (quote (("pkg-config" "dep:pkg-config")))) (r "1.70")))

