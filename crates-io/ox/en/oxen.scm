(define-module (crates-io ox en oxen) #:use-module (crates-io))

(define-public crate-oxen-0.0.1 (c (n "oxen") (v "0.0.1") (d (list (d (n "glium") (r "^0.4.0") (d #t) (k 0)) (d (n "glutin") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "10532vkl0rilsxd9g0ls31hz9gg6jzmxx1fx3qpsspvhjk4ly98b")))

(define-public crate-oxen-0.0.2 (c (n "oxen") (v "0.0.2") (d (list (d (n "glium") (r "*") (d #t) (k 0)) (d (n "glutin") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0rm9r7nr46xxpnwbpc45rj7y8pjq4hbm2z2aj2ml5m2qfi2pmpx5")))

