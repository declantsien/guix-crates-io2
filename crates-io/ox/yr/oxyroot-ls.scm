(define-module (crates-io ox yr oxyroot-ls) #:use-module (crates-io))

(define-public crate-oxyroot-ls-0.1.20 (c (n "oxyroot-ls") (v "0.1.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)))) (h "1zimgs3kcgwdjm3ljafc870djb02jxkk62rkb8zaah9f4x7pynsj")))

(define-public crate-oxyroot-ls-0.1.21 (c (n "oxyroot-ls") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)))) (h "0l5q9jjnsj7xb5ibrxw0srvaga8737ph2n9zw336vsf6qwvbwfx7")))

(define-public crate-oxyroot-ls-0.1.22 (c (n "oxyroot-ls") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)))) (h "1f2a2wmsgrb5fl24153j3y5fyqkhvz55k5drvak4y30xk3g0kxgc")))

(define-public crate-oxyroot-ls-0.1.23 (c (n "oxyroot-ls") (v "0.1.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)))) (h "1j4q689iwi6qrasxwrv2f6i0d9npkkyhx3dbrz2ib7bkashbfjy6")))

(define-public crate-oxyroot-ls-0.1.24 (c (n "oxyroot-ls") (v "0.1.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)))) (h "1i9l4gpffha2pa1lw83zhzbx43p5j43qhwgdgkazg01k4b861rsn")))

