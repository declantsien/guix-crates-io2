(define-module (crates-io ox yr oxyroot_derive) #:use-module (crates-io))

(define-public crate-oxyroot_derive-0.1.14 (c (n "oxyroot_derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ar6v0pw2a9qsbpqxrqyicbb3h39b6d2qm527kzcz864f309v0lp")))

(define-public crate-oxyroot_derive-0.1.17 (c (n "oxyroot_derive") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m3s8hfj2sqs5wlaqlrxinrwqsvwc0yqljv4md899srp3ahp05ak")))

(define-public crate-oxyroot_derive-0.1.18 (c (n "oxyroot_derive") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r1rmq8cjkg3lih2l2g3hkl3vbhqg11f3brjchvk0m6fknxgwfr7")))

(define-public crate-oxyroot_derive-0.1.19 (c (n "oxyroot_derive") (v "0.1.19") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kyxvasysx7xqmxgryaq49y97rjdx6zsmm5v4idfhi3i44b3wgbp")))

(define-public crate-oxyroot_derive-0.1.20 (c (n "oxyroot_derive") (v "0.1.20") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n051y2d5rxp26yl8r2wkrd3y2amxgg81fdhpwdf8513pxcj6lpn")))

(define-public crate-oxyroot_derive-0.1.21 (c (n "oxyroot_derive") (v "0.1.21") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k67dp1na1hjwmgbvh2mxa2ay2qwx3wpmm8hpx2jyfmfm2r4ndih")))

(define-public crate-oxyroot_derive-0.1.22 (c (n "oxyroot_derive") (v "0.1.22") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01ndbavpzbmf45m5di7i5h7za9nyki60lyzzq3cph282gpfxchzh")))

(define-public crate-oxyroot_derive-0.1.23 (c (n "oxyroot_derive") (v "0.1.23") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ga6m12pv59j6xb26i7v55jc5f74fiwdq8mz3m6mkla0hnc9zzc7")))

(define-public crate-oxyroot_derive-0.1.24 (c (n "oxyroot_derive") (v "0.1.24") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1g0n79irr3jrk59fik21nf2xd1cbm4piqc1x8i62qwxsivr55f94")))

