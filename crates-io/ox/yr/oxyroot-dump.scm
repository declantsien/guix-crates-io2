(define-module (crates-io ox yr oxyroot-dump) #:use-module (crates-io))

(define-public crate-oxyroot-dump-0.1.20 (c (n "oxyroot-dump") (v "0.1.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nj5yld11f4ic898bfq0kb8y51rd9yglh3cpn5llla843sc8jalx")))

(define-public crate-oxyroot-dump-0.1.21 (c (n "oxyroot-dump") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09ws3hw0n5ixp3dfbw06hmmvkp8018vscg346yfv8kb7999lxf1w")))

(define-public crate-oxyroot-dump-0.1.22 (c (n "oxyroot-dump") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vpqmx6h24vzv78vn3k7drnvc6g9l341lxqmzwjrl3qp5r113mi4")))

(define-public crate-oxyroot-dump-0.1.23 (c (n "oxyroot-dump") (v "0.1.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13c6nfvg3am1h0jvkzjlqq0xf50j0a9ryb5kxcn4wwbfd84gj6vs")))

(define-public crate-oxyroot-dump-0.1.24 (c (n "oxyroot-dump") (v "0.1.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "oxyroot") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08vzc607pbpv70m37h8373927323q5j8wld0l8004p9q9izzd09f")))

