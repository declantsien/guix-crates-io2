(define-module (crates-io ox iu oxiunify) #:use-module (crates-io))

(define-public crate-oxiunify-0.1.0 (c (n "oxiunify") (v "0.1.0") (h "1s98l0xsjsswb48mrgzrvrpsjk2r8mhg5zzgmjgmz20zzcm9lfq7")))

(define-public crate-oxiunify-0.1.1 (c (n "oxiunify") (v "0.1.1") (h "1225bp39ki690bl99d617864s223wfmdlpdn63hpbrfb2pscfliw")))

