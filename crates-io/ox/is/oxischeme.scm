(define-module (crates-io ox is oxischeme) #:use-module (crates-io))

(define-public crate-oxischeme-0.0.1 (c (n "oxischeme") (v "0.0.1") (h "1shms562z650awksfjvpppkm7jdzf3jpjinqbiarwm6151r46775")))

(define-public crate-oxischeme-0.0.2 (c (n "oxischeme") (v "0.0.2") (h "1q08afakwj9m5x9pl0vra7zswdvhi1z269yi26fsfc75xcgma0ib")))

(define-public crate-oxischeme-0.0.3 (c (n "oxischeme") (v "0.0.3") (h "1f2f4xjz7pv8ymby78zlh28gysf7sy22sfvzy3l91j68ri2jjwz0")))

