(define-module (crates-io ox is oxishut) #:use-module (crates-io))

(define-public crate-oxishut-0.1.0 (c (n "oxishut") (v "0.1.0") (d (list (d (n "adw") (r ">=0.3.1") (f (quote ("v1_2"))) (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib-build-tools") (r "^0.18") (d #t) (k 1)) (d (n "gtk") (r "^0.6.6") (d #t) (k 0) (p "gtk4")) (d (n "gtk4-layer-shell") (r "^0.0.3") (d #t) (k 0)))) (h "01lx2pdhlhnl45p5b09ag0rsr8syhgyaxdwg9b4p3bgxk08khnfn")))

