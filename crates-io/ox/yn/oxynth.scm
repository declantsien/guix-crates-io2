(define-module (crates-io ox yn oxynth) #:use-module (crates-io))

(define-public crate-oxynth-0.1.0 (c (n "oxynth") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "piston_window") (r "0.*") (d #t) (k 0)))) (h "0fjd2mxn5x1q2ci3x6cfdjmrqrkxwq89rdv83v3rb3srd4gvpjl5")))

(define-public crate-oxynth-0.1.1 (c (n "oxynth") (v "0.1.1") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "piston_window") (r "0.*") (d #t) (k 0)))) (h "0h5s95x2hjmirjz3ghyzzk50p75hy9xdvwsya03ywk63zrd535cb")))

