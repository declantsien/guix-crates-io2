(define-module (crates-io ox bo oxbow) #:use-module (crates-io))

(define-public crate-oxbow-0.1.0 (c (n "oxbow") (v "0.1.0") (d (list (d (n "arrow") (r "^37.0.0") (d #t) (k 0)) (d (n "noodles") (r "^0.35.0") (f (quote ("bam" "bgzf" "core" "sam" "csi"))) (d #t) (k 0)))) (h "00r9pfj2q4sfxz4r64qgqvd646n729z20jckcrb1z3m4gbdvn2ps")))

(define-public crate-oxbow-0.2.0 (c (n "oxbow") (v "0.2.0") (d (list (d (n "arrow") (r "^37.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "noodles") (r "^0.45.0") (f (quote ("bam" "bcf" "bgzf" "core" "cram" "fasta" "fastq" "sam" "csi" "vcf" "tabix"))) (d #t) (k 0)))) (h "0fwszaacbxy5q8r05qyfs6qdzp1p7b8jcgl5f8xdb5mbrs9r9dv8")))

(define-public crate-oxbow-0.3.0 (c (n "oxbow") (v "0.3.0") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "bigtools") (r "^0.3.0") (f (quote ("read"))) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "noodles") (r "^0.55.0") (f (quote ("bam" "bcf" "bgzf" "core" "cram" "fasta" "fastq" "gff" "gtf" "sam" "csi" "vcf" "tabix"))) (d #t) (k 0)))) (h "0d2jl5cv3walzdg85226a3pqmd0xpvylwxv0lxjrwjnhj84v5jk3")))

(define-public crate-oxbow-0.3.1 (c (n "oxbow") (v "0.3.1") (d (list (d (n "arrow") (r "^48.0.0") (d #t) (k 0)) (d (n "bigtools") (r "^0.3.0") (f (quote ("read"))) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "noodles") (r "^0.55.0") (f (quote ("bam" "bcf" "bgzf" "core" "cram" "fasta" "fastq" "gff" "gtf" "sam" "csi" "vcf" "tabix"))) (d #t) (k 0)))) (h "0iqq7p2r11nhkxz2v1k7zl55bc0ylyxx1s8azmqhv4sfl1kpv63y")))

