(define-module (crates-io ox _p ox_parser) #:use-module (crates-io))

(define-public crate-ox_parser-0.2.0 (c (n "ox_parser") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "08q4w82lm2njhqzpbz3gis6x668fqim3hmdkgb52ln0s5yfihncm") (y #t)))

(define-public crate-ox_parser-0.2.5 (c (n "ox_parser") (v "0.2.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "05qi721ki9r2fjhcawjwxw366y6brnfjvh3yrq5nq2573v94h988") (y #t)))

(define-public crate-ox_parser-0.2.1 (c (n "ox_parser") (v "0.2.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0iglz04dxknis41walvykv4qzzip1c2h5vk29900p3hv6r3apkni") (y #t)))

(define-public crate-ox_parser-0.2.2 (c (n "ox_parser") (v "0.2.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0x2n6l3ps60whpj4k2hcz2n3wjfkmmzv447d9bb3s2513g8p8ygm") (y #t)))

(define-public crate-ox_parser-0.2.6 (c (n "ox_parser") (v "0.2.6") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0g6c0xc9znp4db9bwmgrsqgsxzwhc1gn2q0y16lchp21da2jj5yy") (y #t)))

(define-public crate-ox_parser-0.3.0 (c (n "ox_parser") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1h74h8bhf9jws9zffxini01am2ljabvcnzdr178dvdafwr80797b") (y #t)))

(define-public crate-ox_parser-0.3.1 (c (n "ox_parser") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vilsrznxgr2129slc2zplxbc8l051p891pg1kzmm0i1c2vh4jkq") (y #t)))

(define-public crate-ox_parser-0.3.2 (c (n "ox_parser") (v "0.3.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1gd2m8pqa29apvm0rssc136wkwjm8zc2wwkapwmxvr8cs5df6spa") (y #t)))

(define-public crate-ox_parser-0.3.3 (c (n "ox_parser") (v "0.3.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1zhl893b428syw6s5wirqxrqv4nf91zn917hndcb03bhcayjf1yw") (y #t)))

(define-public crate-ox_parser-0.3.4 (c (n "ox_parser") (v "0.3.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0yq3pkssasnw6rxzpsm13m5y7mimkyyfbrcj1yvgmgj9pndc018l") (y #t)))

(define-public crate-ox_parser-0.3.5 (c (n "ox_parser") (v "0.3.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "01z39wggpb4pj2hbfpk9cbcr9y74xsbvgj1dqff9x5wf5h60jb6p") (y #t)))

(define-public crate-ox_parser-0.3.6 (c (n "ox_parser") (v "0.3.6") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0vgka0qx7bh1jp41j4pq2s6d4x44iqh2fancn1m6qbzdlw1ywx13") (y #t)))

(define-public crate-ox_parser-0.3.7 (c (n "ox_parser") (v "0.3.7") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1i3m3pkvkd12sg8qcqg9mhqr7h9rjfawcnh09l39darxbl2hisjl") (y #t)))

(define-public crate-ox_parser-0.3.8 (c (n "ox_parser") (v "0.3.8") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1dxrbqszpi5s0sdqdfjzi0qqqmjmd9gdsxgmfbmjkwhmzwjygi3j") (y #t)))

(define-public crate-ox_parser-0.3.9 (c (n "ox_parser") (v "0.3.9") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0szakhh1jnf5d4kc4q8ypq0cc6kr1s2wpgwcziw4x9vm1j8f4kzf") (y #t)))

