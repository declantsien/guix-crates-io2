(define-module (crates-io ox fo oxford_join) #:use-module (crates-io))

(define-public crate-oxford_join-0.1.0 (c (n "oxford_join") (v "0.1.0") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "0hifb651vljvkp7y5bhqrj2i3fcqsw81ng1z952ihxmvmma88s5y")))

(define-public crate-oxford_join-0.1.1 (c (n "oxford_join") (v "0.1.1") (d (list (d (n "brunch") (r "0.1.*") (d #t) (k 2)))) (h "1y7ab6mj53s9qv7sfbv9akxim736clvzd8g3w8zwb97rkcsax26l")))

(define-public crate-oxford_join-0.2.0 (c (n "oxford_join") (v "0.2.0") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "0dnamrm2yv8c71kvmbcp87n0bb80c2qza41l908cnhij59ir4dxb") (r "1.56")))

(define-public crate-oxford_join-0.2.1 (c (n "oxford_join") (v "0.2.1") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "14bq9y0h6i2njvzbvdgancdjab2x3far7i29g085s1qb4qrz9zcg") (r "1.56")))

(define-public crate-oxford_join-0.2.2 (c (n "oxford_join") (v "0.2.2") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "07rzzgbjiv99xk1hd4qm8wkxxgh8x824r2s0yx8xi98s7m6dcsad") (r "1.56")))

(define-public crate-oxford_join-0.2.3 (c (n "oxford_join") (v "0.2.3") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "0lr94c5djnb09rl8lnwlj2cir5cv7pl5fr8jkrkyhj8ccxp8wfiv") (r "1.56")))

(define-public crate-oxford_join-0.2.4 (c (n "oxford_join") (v "0.2.4") (d (list (d (n "brunch") (r "0.2.*") (d #t) (k 2)))) (h "1l5bsaww1174fg16hi55yarmpg2g186ynp0h7xnsshhsldkj3m43") (r "1.62")))

(define-public crate-oxford_join-0.2.5 (c (n "oxford_join") (v "0.2.5") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "1dxvhn6mdddzjyr0dbpqdl1dyskg5hdjqchpbky12axsd9bima2q") (r "1.62")))

(define-public crate-oxford_join-0.2.6 (c (n "oxford_join") (v "0.2.6") (d (list (d (n "brunch") (r "0.3.*") (d #t) (k 2)))) (h "07q37fcvq46xdbqk7g1qnach41m8r16xybhh8a482l1mhcfxi1w1") (r "1.62")))

(define-public crate-oxford_join-0.2.7 (c (n "oxford_join") (v "0.2.7") (d (list (d (n "brunch") (r "0.4.*") (d #t) (k 2)))) (h "1p4xkzz1pj7w4d16q6izvg9lyp8jsgkpjhcwgsnbyk94rdi4zmcn") (r "1.62")))

(define-public crate-oxford_join-0.2.8 (c (n "oxford_join") (v "0.2.8") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "1dgwyr3c56lvkx5451wgc3ixpg9chjwqnk0sk84v12ifl29k0p3l") (r "1.62")))

(define-public crate-oxford_join-0.2.9 (c (n "oxford_join") (v "0.2.9") (d (list (d (n "brunch") (r "0.5.*") (d #t) (k 2)))) (h "1c3kfz5hn4nn33swpsdll3piszgsn0mfkyry99d7spd3iymmlxcn") (r "1.62")))

