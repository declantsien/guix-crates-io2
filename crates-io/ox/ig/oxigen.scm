(define-module (crates-io ox ig oxigen) #:use-module (crates-io))

(define-public crate-oxigen-1.0.0 (c (n "oxigen") (v "1.0.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1v5wxmi0w1mmg3r61cma9xb3zgj5qnscmn187q71g9bhrncw4rbh") (y #t)))

(define-public crate-oxigen-1.1.0 (c (n "oxigen") (v "1.1.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1qsi48fmxp3mnr4kpdjkc7731azgyznx1hj8474571bvqm8ikm0l") (y #t)))

(define-public crate-oxigen-1.2.0 (c (n "oxigen") (v "1.2.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "05hwg81m2b7wbq49dgmsysns71skh8nir9i5q38ii5nvqnvhkagx") (y #t)))

(define-public crate-oxigen-1.2.1 (c (n "oxigen") (v "1.2.1") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0xdq6r5f92dp866fllcx05cxbnwr47k9fdr830xmq4dqh0jigfpl") (y #t)))

(define-public crate-oxigen-1.3.0 (c (n "oxigen") (v "1.3.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "172x6f7jkvv8f6hl9bd3qwpgiv6b30mr4n3sark06frnf6dar9qd") (y #t)))

(define-public crate-oxigen-1.4.0 (c (n "oxigen") (v "1.4.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1azn0n21ih0kkwx397q89yb7rvwdmx2p7yyb8ljvk8j6mbh82i0n") (y #t)))

(define-public crate-oxigen-1.4.1 (c (n "oxigen") (v "1.4.1") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "03plpi3832ms77rgagxvcpz3faqqg864ia1j53gczwbm709821c7") (y #t)))

(define-public crate-oxigen-1.4.2 (c (n "oxigen") (v "1.4.2") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1s1g2di5r3iwysfzzyvnkzgifb3dk6hxfym2ia7ybq47v5nzfvm6") (y #t)))

(define-public crate-oxigen-1.4.3 (c (n "oxigen") (v "1.4.3") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0r8m9nvha3v9ssv0cn7j59qglqbf0l5j5vbqpmid4x4jf0mg4viq") (y #t)))

(define-public crate-oxigen-1.5.0 (c (n "oxigen") (v "1.5.0") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1n0j40yngjfpcs094sj9dd76v67cddry2rw17vhcyvi594iwgy76") (y #t)))

(define-public crate-oxigen-2.0.0 (c (n "oxigen") (v "2.0.0") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0zyr720h9c5gnf66xz4yl4f2wx8fkvv71ldq0q6fr9jhz2xlzcv8") (y #t)))

(define-public crate-oxigen-2.0.1 (c (n "oxigen") (v "2.0.1") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "02n9ssq40i8jqhc1fkzhyyf8k99bl4qcv8djapb5mfhs2zqn1n61") (y #t)))

(define-public crate-oxigen-2.0.2 (c (n "oxigen") (v "2.0.2") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "01d559kg467n3kfxzal3j8g98y5zymq3aw3m4jd82fbd3f2nbgln") (y #t)))

(define-public crate-oxigen-2.1.0 (c (n "oxigen") (v "2.1.0") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0mxdjjvdxjrmfbrjcazpk2cad5l74iylds5d3kd1j622ib88ffiv") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-2.0.3 (c (n "oxigen") (v "2.0.3") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0af2gxz2af93r1bhkw9p9sak5vixwfhlvqdp47lxnrm14bhqnfzf") (y #t)))

(define-public crate-oxigen-2.1.1 (c (n "oxigen") (v "2.1.1") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0q5wjvd445x2w0d4mf5xmhqidiw9jcg46skjvw7rqifwjy8x0qzc") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-2.2.0 (c (n "oxigen") (v "2.2.0") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "19gmlxq9qmda0mh1b2nqj8bg2syp2nb0kjpkh9whzrprirrzsdzh") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-2.0.4 (c (n "oxigen") (v "2.0.4") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "139hi564izi9x8dl3gyl7dxaf6vcynhfh7r5f4ql1va1gn3772pb") (y #t)))

(define-public crate-oxigen-2.1.2 (c (n "oxigen") (v "2.1.2") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1rh7f199g8scbf6rg34sk7i82p6qvbqjfbmz35fgd9bqkmmgg3b2") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-1.5.1 (c (n "oxigen") (v "1.5.1") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1vvn3hpmc556gxldgmi9f0rnmc9rj0qc4y0xihkw09r6q1h4g5xm") (y #t)))

(define-public crate-oxigen-2.0.5 (c (n "oxigen") (v "2.0.5") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0ysfyjgmigzmilk33q9wma57v3804b6h032p8q9sg2mhf82vqj2z") (y #t)))

(define-public crate-oxigen-2.1.3 (c (n "oxigen") (v "2.1.3") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0h4nfgcqv80r69klvcwcc3dyh22nj6ar9n3pgr7a4zxy2pjrykl6") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-2.2.1 (c (n "oxigen") (v "2.2.1") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0dw8infk33q2b785is88ln7vbpzx0mwjdhj3xnzknbrdm3zz1xvb") (f (quote (("global_cache") ("default_features")))) (y #t)))

(define-public crate-oxigen-1.5.2 (c (n "oxigen") (v "1.5.2") (d (list (d (n "historian") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "100q18yh1mp0zpc1kw943s3g0gwlg3lf2mgvfl6kky26xpm5djj0")))

(define-public crate-oxigen-2.0.6 (c (n "oxigen") (v "2.0.6") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0625dyymy7bvyr24gphw22l28h15gj5sghfpq13v0xjf8n87ssxz")))

(define-public crate-oxigen-2.1.4 (c (n "oxigen") (v "2.1.4") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1x0dmsscdjjb139yafh4dhajvhzvxf66zn7w7irn7wic9hg76bzv") (f (quote (("global_cache") ("default_features"))))))

(define-public crate-oxigen-2.2.2 (c (n "oxigen") (v "2.2.2") (d (list (d (n "historian") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1g6mag7cqzx12lm11qrb38a2yx7ivihkwiylrh7gnixa8v8rdhsv") (f (quote (("global_cache") ("default_features"))))))

