(define-module (crates-io ox pa oxparse) #:use-module (crates-io))

(define-public crate-OxParse-0.1.0 (c (n "OxParse") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "181szxl6lskzz3y8ngd0xp3rqd1hih7zdpky37lmlmdfpia38hvb") (y #t)))

(define-public crate-OxParse-0.1.1 (c (n "OxParse") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wq7gwf0zmzvp2ykj6bcn1g3klba8942z15wphkwljpdz749il6c") (y #t)))

(define-public crate-OxParse-0.2.0 (c (n "OxParse") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1f43p1kghjrb0icrspzra8y99d714x9wscvh1br5lhkfri8a6m1r") (y #t)))

