(define-module (crates-io ox sd oxsdatatypes) #:use-module (crates-io))

(define-public crate-oxsdatatypes-0.1.0 (c (n "oxsdatatypes") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "184ka5h78q2dll9bam9xj0z1rsvmc1vv76p5zq0wbhqwk40074f0") (r "1.60")))

(define-public crate-oxsdatatypes-0.1.1 (c (n "oxsdatatypes") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1n8gns3jxza6p593pxa2wajx99rxpy4qdjazqd6bfvwzrnaxw28c") (r "1.60")))

(define-public crate-oxsdatatypes-0.1.2 (c (n "oxsdatatypes") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "0q6f0i9kgb50pfcy9hanzqr9hhwkyqih06z9skid9rh394kd2b0v") (r "1.60")))

(define-public crate-oxsdatatypes-0.1.3 (c (n "oxsdatatypes") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "1jam5ba308b0pdxq39a8c36c5rsxw6c8kqqjgr16dl5gnmq0gszd") (r "1.60")))

(define-public crate-oxsdatatypes-0.2.0-alpha.1 (c (n "oxsdatatypes") (v "0.2.0-alpha.1") (d (list (d (n "js-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "00al4gyh8z29rf0l8v7d5y3d79xy55mz0bdvl01v76mka0gkqvdm") (f (quote (("js" "js-sys") ("custom-now")))) (r "1.70")))

(define-public crate-oxsdatatypes-0.2.0-alpha.2 (c (n "oxsdatatypes") (v "0.2.0-alpha.2") (d (list (d (n "js-sys") (r "^0.3.60") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1f0fa0byygsshwqlwcmfji7578646ajrskc27rhvzx20x9145chm") (f (quote (("js" "js-sys") ("custom-now")))) (r "1.70")))

