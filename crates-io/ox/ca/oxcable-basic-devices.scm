(define-module (crates-io ox ca oxcable-basic-devices) #:use-module (crates-io))

(define-public crate-oxcable-basic-devices-0.5.0 (c (n "oxcable-basic-devices") (v "0.5.0") (d (list (d (n "num") (r "^0.1.27") (k 0)) (d (n "oxcable") (r "^0.5.0") (d #t) (k 0)))) (h "0lahxg7cvbbkri5gk0ggj0znvibnfsg6za73w1blwlfvnzplfs7w")))

(define-public crate-oxcable-basic-devices-0.5.1 (c (n "oxcable-basic-devices") (v "0.5.1") (d (list (d (n "num") (r "^0.1.27") (k 0)) (d (n "oxcable") (r "^0.5.1") (d #t) (k 0)))) (h "0jjwf68v5rk8ghw9c3d5ii6wqlli5475cjimrn2h0lxdqg6i8qhl")))

