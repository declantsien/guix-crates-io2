(define-module (crates-io ox ca oxcable) #:use-module (crates-io))

(define-public crate-oxcable-0.3.0 (c (n "oxcable") (v "0.3.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "portmidi") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "08wk3fyjvyb5h1w2v51nxcsjwls4fy7ykwbq8g3nv6jh6a1pvp7g")))

(define-public crate-oxcable-0.3.1 (c (n "oxcable") (v "0.3.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "portmidi") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0p222l051hpcnriq412fi6ap18baxlnl145yid0nh481lkimk4ba")))

(define-public crate-oxcable-0.3.2 (c (n "oxcable") (v "0.3.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "portmidi") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "07vngmmqkklvmki063lly76wlybqf8mklk27c7dvnrzr4yfi6md9")))

(define-public crate-oxcable-0.4.0 (c (n "oxcable") (v "0.4.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "*") (d #t) (k 0)) (d (n "portmidi") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1y5qz7zm1inzjki5y1hh1qpxz8d4acv2z1z9l9a23ck3rrli2wrd")))

(define-public crate-oxcable-0.4.1 (c (n "oxcable") (v "0.4.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (k 0)) (d (n "portaudio") (r "0.4.*") (d #t) (k 0)) (d (n "portmidi") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1c1jxqrfc0plr8a4z0b2v06i97wvml2y1zp342lwr9gmjghsv4q0")))

(define-public crate-oxcable-0.4.2 (c (n "oxcable") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "portaudio") (r "^0.5.1") (d #t) (k 0)) (d (n "portmidi") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "0qrjs6kx3fzidpa55y1hqwk8qwbmfmxcfzdjsg460kwlgc1n2ln9")))

(define-public crate-oxcable-0.5.0 (c (n "oxcable") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (k 0)) (d (n "portaudio") (r "^0.5.1") (d #t) (k 0)) (d (n "portmidi") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "0v7lh2k0ankjqlx7iya973ra99awv7m2qkwd7c9gdfid46x3sk90")))

(define-public crate-oxcable-0.5.1 (c (n "oxcable") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (f (quote ("complex"))) (k 0)) (d (n "portaudio") (r "^0.5.1") (d #t) (k 0)) (d (n "portmidi") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "180p2l51iiipr83ghm0v0201vkkw481b5mwndkxgp26l6awy1vyb")))

