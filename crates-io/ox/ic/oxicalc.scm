(define-module (crates-io ox ic oxicalc) #:use-module (crates-io))

(define-public crate-oxicalc-0.1.0 (c (n "oxicalc") (v "0.1.0") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib") (r "^0.17.5") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0bglwyycc6pflikiv5isifgjr8ildjiyja3f210kks8qzci3d0js")))

(define-public crate-oxicalc-0.1.1 (c (n "oxicalc") (v "0.1.1") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib") (r "^0.17.5") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "06z1kizbrxi0m1rqh9mjk5sk7njwcidzpwnqscpmm191bxfi0234")))

(define-public crate-oxicalc-0.1.2 (c (n "oxicalc") (v "0.1.2") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib") (r "^0.17.5") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1isjad4baan06j1cmlljg8ak3bz56dqz4bzq77n03gxyp3amrdfb")))

(define-public crate-oxicalc-0.2.0 (c (n "oxicalc") (v "0.2.0") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib") (r "^0.17.5") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1vkgdz8jpx23ig8mji0h90ibzp8njcwpvh1a8qhff2gii9vqjdkd")))

(define-public crate-oxicalc-0.2.1 (c (n "oxicalc") (v "0.2.1") (d (list (d (n "adw") (r "^0.3") (d #t) (k 0) (p "libadwaita")) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "glib") (r "^0.17.5") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0iirvzv78vmgi6ldc6msi1fxs641hv13ys06psl0svgxgmp1r2rw")))

