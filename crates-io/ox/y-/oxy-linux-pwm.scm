(define-module (crates-io ox y- oxy-linux-pwm) #:use-module (crates-io))

(define-public crate-oxy-linux-pwm-0.1.0 (c (n "oxy-linux-pwm") (v "0.1.0") (h "03wksmifg76dfjvrkzlckrcp7ianjcga8w7jf6gaxkiwr3x53h3g")))

(define-public crate-oxy-linux-pwm-0.1.1 (c (n "oxy-linux-pwm") (v "0.1.1") (h "0x30fjc1jvys8p4hw5666amn9dmppvwzmg6h9lq82a019sfb8ks6")))

