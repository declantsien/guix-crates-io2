(define-module (crates-io ox i- oxi-luajit) #:use-module (crates-io))

(define-public crate-oxi-luajit-0.3.0 (c (n "oxi-luajit") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g1rjlac1nq3wjqcnma2g25gcsv3zdwxcn3r52giz7n9by2my71d")))

(define-public crate-oxi-luajit-0.4.0 (c (n "oxi-luajit") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "064gvx5dc42akd4sff0gh37wr3978k1k6a6zbkbgvv9ilcvdzh3y")))

(define-public crate-oxi-luajit-0.4.1 (c (n "oxi-luajit") (v "0.4.1") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02dm75m9i83wkz6wxyw5h81rbk3vyg9sxqbp04q96icrrjncnv96")))

(define-public crate-oxi-luajit-0.4.2 (c (n "oxi-luajit") (v "0.4.2") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ar5xf5gv1017hjiyp44idxh9ik8m5agqw252zmsc5jqqg1w8i56")))

