(define-module (crates-io ox i- oxi-libuv) #:use-module (crates-io))

(define-public crate-oxi-libuv-0.3.0 (c (n "oxi-libuv") (v "0.3.0") (d (list (d (n "libuv-sys2") (r "^1.44") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17f9wakgs6a50g70iadw61rm66dfq5wkavskbkbrwmwr5b5azk5y")))

(define-public crate-oxi-libuv-0.4.0 (c (n "oxi-libuv") (v "0.4.0") (d (list (d (n "libuv-sys2-1-44") (r "^1.44.2") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "libuv-sys2-1-46") (r "^1.46.0") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mscbfyzhg1w28myh39ssp3rapa67mn4wjyxk0dnvj45nrf6k7hh") (f (quote (("use-1-46" "libuv-sys2-1-46") ("use-1-44" "libuv-sys2-1-44"))))))

(define-public crate-oxi-libuv-0.4.1 (c (n "oxi-libuv") (v "0.4.1") (d (list (d (n "libuv-sys2-1-44") (r "^1.44.2") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "libuv-sys2-1-46") (r "^1.46.0") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v6xx6r5qyv89dcavq4nkfqs6i1z33jj36g9z14lsazc073yhj6b") (f (quote (("use-1-46" "libuv-sys2-1-46") ("use-1-44" "libuv-sys2-1-44"))))))

(define-public crate-oxi-libuv-0.4.2 (c (n "oxi-libuv") (v "0.4.2") (d (list (d (n "libuv-sys2-1-44") (r "^1.44.2") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "libuv-sys2-1-46") (r "^1.46.0") (o #t) (d #t) (k 0) (p "libuv-sys2")) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xjvg28g5wrlwcsq17151fv0xj7knblprmzlp49pbjfy1s2wg83x") (f (quote (("use-1-46" "libuv-sys2-1-46") ("use-1-44" "libuv-sys2-1-44"))))))

