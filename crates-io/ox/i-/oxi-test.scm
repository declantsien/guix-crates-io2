(define-module (crates-io ox i- oxi-test) #:use-module (crates-io))

(define-public crate-oxi-test-0.1.0 (c (n "oxi-test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b2m3wjb8xax6wjx91h87rjlpdrm5a7k1irm3wqprgl1wgd125md")))

(define-public crate-oxi-test-0.2.0 (c (n "oxi-test") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n9gx6yz60xbcjc9wqd6x7llf0p3gvn54izlfy7sgdwghhwm3dyh")))

