(define-module (crates-io ox i- oxi-types) #:use-module (crates-io))

(define-public crate-oxi-types-0.3.0 (c (n "oxi-types") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0na6n3pfikvjxg5abkbsjxj8rqm3576j53ayrqz9q8ny54f0dx75") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-oxi-types-0.4.0 (c (n "oxi-types") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g4ndc8z5k0njk4mr10jvc7g65d3w0lrjlf2pwmwwcgkmllgb0g9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-oxi-types-0.4.1 (c (n "oxi-types") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10lfm4lfzjpwskfbjzj0mh5dxi8sxic0r47j7mjlw5sxadyhs96d") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-oxi-types-0.4.2 (c (n "oxi-types") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oxi-luajit") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nr5bjp6m74qf8w62h0i6awbc3g8gii6c7c132bfzf9kfyykm085") (s 2) (e (quote (("serde" "dep:serde"))))))

