(define-module (crates-io ox i- oxi-macros) #:use-module (crates-io))

(define-public crate-oxi-macros-0.3.0 (c (n "oxi-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01x00dgd9jcqxx1l4gj336yvp8k63swssz9hmcdz81qjg7jpv9vz") (f (quote (("test") ("module"))))))

(define-public crate-oxi-macros-0.4.0 (c (n "oxi-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h2mydn2s4sz2a42g6z5l8if8k72fmjrpg1prfqx0q7h2ynwa11l") (f (quote (("test") ("module"))))))

(define-public crate-oxi-macros-0.4.1 (c (n "oxi-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05kmcvf38kd9i7f4ws9yq0hcjh530k2l6ypcpkpg8ywr57v6ybq5") (f (quote (("test") ("module"))))))

(define-public crate-oxi-macros-0.4.2 (c (n "oxi-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "128dp8jj8v13gxn86p0jk0cr5flrq5hf1jlfg6zi3xnc33q2fxdf") (f (quote (("test") ("module"))))))

