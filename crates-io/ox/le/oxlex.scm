(define-module (crates-io ox le oxlex) #:use-module (crates-io))

(define-public crate-oxlex-0.1.0 (c (n "oxlex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "oxlex-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "18wkhzzznsw255pmrh13xn8yck4mdlp0xg4xki9j53bmcdmjh8zn") (f (quote (("derive" "oxlex-derive") ("default" "derive"))))))

