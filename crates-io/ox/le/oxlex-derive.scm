(define-module (crates-io ox le oxlex-derive) #:use-module (crates-io))

(define-public crate-oxlex-derive-0.1.0 (c (n "oxlex-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "02l5q4ihx1dy5ay98wxhfmp623hai1f4nf7ah92zx19z54pp3zva") (f (quote (("default" "syn/full"))))))

