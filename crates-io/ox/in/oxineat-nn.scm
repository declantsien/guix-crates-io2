(define-module (crates-io ox in oxineat-nn) #:use-module (crates-io))

(define-public crate-oxineat-nn-0.1.0 (c (n "oxineat-nn") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "oxineat") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "03d1p1hdyvdqwhnxgg2jkaxsc6a31wykpibf2yrk7da2gqkbfj8i")))

(define-public crate-oxineat-nn-0.1.1 (c (n "oxineat-nn") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "oxineat") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xbrd6rrvy8mpzdlvvyca0dl7vdbly7h012g3yda1wp701vbfafc")))

