(define-module (crates-io ox in oxinat) #:use-module (crates-io))

(define-public crate-oxinat-0.3.0 (c (n "oxinat") (v "0.3.0") (d (list (d (n "oxinat_core") (r "^0.5.0") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.5.0") (d #t) (k 0)))) (h "00mqvsnqbz1m77bgdq8q125nd01syrhhdq9pgmpw2x2y56z7z2py") (f (quote (("full" "core" "derive") ("derive") ("core"))))))

(define-public crate-oxinat-0.4.0 (c (n "oxinat") (v "0.4.0") (d (list (d (n "oxinat_core") (r "^0.7.0") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.6.1") (d #t) (k 0)))) (h "0a42pkdk04ncqdl0y02wskf5n1ss42rcyhmhvqw3qfmhg2nvmadn") (f (quote (("full" "core" "derive") ("derive") ("core"))))))

(define-public crate-oxinat-0.4.1 (c (n "oxinat") (v "0.4.1") (d (list (d (n "oxinat_core") (r "^0.7.1") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.6.1") (d #t) (k 0)))) (h "0c0mr240iqybzwfv0j4592lxfwsjhzwcc38fq14rpsfq3x18b76g") (f (quote (("full" "core" "derive") ("derive") ("core"))))))

(define-public crate-oxinat-0.4.2 (c (n "oxinat") (v "0.4.2") (d (list (d (n "oxinat_core") (r "^0.8.0") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.7.0") (d #t) (k 0)))) (h "04pwqflssdvnv2hh6ivxwvimx9xzvkjmx779ivq42wryx4sjn5dw") (f (quote (("full" "core" "derive") ("derive") ("core"))))))

