(define-module (crates-io ox in oxinoti) #:use-module (crates-io))

(define-public crate-oxinoti-0.1.0 (c (n "oxinoti") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.2") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "gtk") (r "^0.17.1") (d #t) (k 0) (p "gtk")) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0nbxl4l7aqflsva4gc35xay9rl1ixcsadqyxs72w2rr8crsv5k5p")))

(define-public crate-oxinoti-0.1.1 (c (n "oxinoti") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.2") (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "gtk") (r "^0.17.1") (d #t) (k 0) (p "gtk")) (d (n "gtk-layer-shell") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0qvz338qwrq6j4msvsaamskr52dzjkkxrkkdc72ggwqp2s5xz8bi")))

