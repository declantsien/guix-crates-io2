(define-module (crates-io ox in oxinscribe) #:use-module (crates-io))

(define-public crate-oxinscribe-0.1.0 (c (n "oxinscribe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ropey") (r "^1.5") (d #t) (k 0)))) (h "0rv5q1970y4jnkbm9yphld0wag1q11l1swvpjqpk0ay7lbxw486w") (y #t)))

(define-public crate-oxinscribe-0.0.1 (c (n "oxinscribe") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ropey") (r "^1.5") (d #t) (k 0)))) (h "1ns8cn3ng4rg282kazswl56i4gflzw73g1pjqbc9q2c3cmj5p1k2")))

