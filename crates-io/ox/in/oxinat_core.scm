(define-module (crates-io ox in oxinat_core) #:use-module (crates-io))

(define-public crate-oxinat_core-0.5.0 (c (n "oxinat_core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "12xj6dbqdkqz2rd0387yl8apqqygkqjg7cvdwvl1qqgbzsrzk538")))

(define-public crate-oxinat_core-0.5.1 (c (n "oxinat_core") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0nbql36s2418ji659g5m4i36bpvhdr7xlddkbjhhill291ji3gp6")))

(define-public crate-oxinat_core-0.7.0 (c (n "oxinat_core") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "05bfqbzdkmilmj7vqw8qd046199a1w76dnad37k2al3yjq35z8qd")))

(define-public crate-oxinat_core-0.7.1 (c (n "oxinat_core") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1i2d19hvy7i4qxlqndy9dxrnpnnfjzfzx2pcmg57i60bc3dz0k83")))

(define-public crate-oxinat_core-0.8.0 (c (n "oxinat_core") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "oxinat_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "09hm0h6wp1aj5psl9yhryyngi0kynrkgm4y2d6c1yx0gp1cynb2l")))

