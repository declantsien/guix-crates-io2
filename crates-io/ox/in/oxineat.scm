(define-module (crates-io ox in oxineat) #:use-module (crates-io))

(define-public crate-oxineat-0.1.0 (c (n "oxineat") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cpwzyhxfbfmblix1snkrr8wy5b1a6bklry7jahcwi4wfv63i0jf")))

(define-public crate-oxineat-0.1.1 (c (n "oxineat") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "13s6g9pw3s0j2zcpq44jcv99xj9zm5w2bvx3pazg5xhmkkxml378")))

(define-public crate-oxineat-0.2.0 (c (n "oxineat") (v "0.2.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sapfp2br1if5khrs2l233izmpzhjih946ygk8ri7migfnp1d65l")))

(define-public crate-oxineat-0.2.1 (c (n "oxineat") (v "0.2.1") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b1cqgjhfr1zs2dxkqd56vigqsa94bnh017izrrm208rd5nzi110")))

(define-public crate-oxineat-0.3.0 (c (n "oxineat") (v "0.3.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)))) (h "0if90b2kf5j4xphlg9nxfyyi9wxi4amal97kym94diln1aggq0hw")))

(define-public crate-oxineat-0.3.1 (c (n "oxineat") (v "0.3.1") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "oxineat-nn") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "16jig4rnkfzkzcgsyri3miaiikr1x63scagkr0kn6cbmy8x50k5a")))

(define-public crate-oxineat-0.3.2 (c (n "oxineat") (v "0.3.2") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "oxineat-nn") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n7mn3zqxqqv459rfpsxd975g3rig7gz8vy69r7xp3xnm4pcnv98")))

