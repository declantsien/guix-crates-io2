(define-module (crates-io ox t- oxt-confidence) #:use-module (crates-io))

(define-public crate-oxt-confidence-0.1.0 (c (n "oxt-confidence") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nc6vs1fr8h30cq4bk1d04ycy5hm6z0bxacr6s4k04gldv6shs8w") (y #t)))

(define-public crate-oxt-confidence-0.2.0 (c (n "oxt-confidence") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0absjalhwfhpbm5wbbjidfi3p3pid9hz60df6xjja5jzf36339pc") (y #t)))

(define-public crate-oxt-confidence-0.2.1 (c (n "oxt-confidence") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g2pjrq09hxnxj61xzxjgmbzzzjhjv9w8v9jq9m2cwc7j2ikhmc7")))

(define-public crate-oxt-confidence-0.2.2 (c (n "oxt-confidence") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ndd0cqh70ji8y0wivghcia1bqwywv59463j4pbkfzldnk6x2rqv")))

(define-public crate-oxt-confidence-0.2.3 (c (n "oxt-confidence") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iww9wpr5vpsd90rjq7bafgy1pf1d0lvs0iiizwgzz56hzczf6f3")))

(define-public crate-oxt-confidence-0.2.4 (c (n "oxt-confidence") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g937094981zsg81mlyb00xl1g8n3xz3ip49frmr70yjyrysrvln")))

(define-public crate-oxt-confidence-0.2.5 (c (n "oxt-confidence") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)))) (h "0km56l5r6jk9lzmj13kcjlvqhqcr8halcwgs2ws1xv5zjfs380zi")))

