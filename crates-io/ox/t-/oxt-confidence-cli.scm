(define-module (crates-io ox t- oxt-confidence-cli) #:use-module (crates-io))

(define-public crate-oxt-confidence-cli-0.1.0 (c (n "oxt-confidence-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "oxt-confidence") (r "^0.2.0") (d #t) (k 0)))) (h "13fp7am3ds7ij3ywimp1pdnvsngl14dwn74imv2aqy9015lwss5z")))

