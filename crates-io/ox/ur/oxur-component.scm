(define-module (crates-io ox ur oxur-component) #:use-module (crates-io))

(define-public crate-oxur-component-0.1.0 (c (n "oxur-component") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)))) (h "0glv6jmzpn0j1qs0n3j55c4rij227pyp460qx76qqkbv9awmbrkg")))

(define-public crate-oxur-component-0.1.1 (c (n "oxur-component") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)))) (h "13gl5k75q84j7givd9sp037550ddij4k9xgqx449zn5r12g4gc8z")))

(define-public crate-oxur-component-0.1.2 (c (n "oxur-component") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)))) (h "0wgjh6pn4jirbpli9kxna9l09kigy9mhr61x1q5q3dyb3bc49yd4")))

