(define-module (crates-io ox yd oxydian) #:use-module (crates-io))

(define-public crate-oxydian-0.1.0 (c (n "oxydian") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)))) (h "07ixp8561xhz5k4rv1gfigdambc5npl6xwvn9vjmgnk4fda9lv48")))

(define-public crate-oxydian-0.1.1 (c (n "oxydian") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "05qib50mhr0ix1wp0c4jcc9k2rdkwzrapx81b90zfpdzir31ynsf")))

(define-public crate-oxydian-0.1.2 (c (n "oxydian") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "183hkkr1fpp3l1l260ahi8p7dddnyrfqwiyqnlc9v4wvclzva1kv")))

