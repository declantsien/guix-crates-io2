(define-module (crates-io ox yd oxydized-money-macros) #:use-module (crates-io))

(define-public crate-oxydized-money-macros-0.1.0 (c (n "oxydized-money-macros") (v "0.1.0") (d (list (d (n "iso_currency") (r "^0.4.4") (f (quote ("iterator"))) (d #t) (k 1)) (d (n "rust_decimal_macros") (r "^1.34.2") (f (quote ("reexportable"))) (d #t) (k 0)))) (h "1fcfgxiywa364dh1ypq31lh0bj5jslkn0a4i2qnywj7wjzs324lc")))

(define-public crate-oxydized-money-macros-0.2.0 (c (n "oxydized-money-macros") (v "0.2.0") (d (list (d (n "iso_currency") (r "^0.4.4") (f (quote ("iterator"))) (d #t) (k 1)) (d (n "rust_decimal_macros") (r "^1.34.2") (f (quote ("reexportable"))) (d #t) (k 0)))) (h "04y57iy57k58kzw4zsy3n0j8qlf6bp7gpinmf62p431ylsb4axwa")))

