(define-module (crates-io ox yd oxydized-money) #:use-module (crates-io))

(define-public crate-oxydized-money-0.1.0 (c (n "oxydized-money") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "iso_currency") (r "^0.4.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34.3") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.34.2") (d #t) (k 2)))) (h "13pkfdwam3b318qwj6hn5sb2gd9k3qimhcv3s722y2p4a5vnl0jj")))

(define-public crate-oxydized-money-0.2.0 (c (n "oxydized-money") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "iso_currency") (r "^0.4.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34.3") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.34.2") (d #t) (k 2)))) (h "10kwg1iif9iwlzfdyzn812v3sxakp5k6ii5m8m3hq7igrjc0ndys")))

(define-public crate-oxydized-money-0.3.0 (c (n "oxydized-money") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "iso_currency") (r "^0.4.4") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.34.3") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.34.2") (d #t) (k 2)))) (h "1rjr1l71m373f0w8dmps6njxkhhjs4f4z7a9gh7lh310vivf4shn")))

