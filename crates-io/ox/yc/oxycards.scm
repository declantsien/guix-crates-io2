(define-module (crates-io ox yc oxycards) #:use-module (crates-io))

(define-public crate-oxycards-1.0.0 (c (n "oxycards") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "09mpgrchwki5yzj4mlmi1a9jvzcfq1h1g3jkxaskrl0dqnwpylb4")))

(define-public crate-oxycards-1.0.1 (c (n "oxycards") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1p83d84ksqm38nmh5456kkamyf8lksdg5dy48gkpbs7l608mlga4")))

(define-public crate-oxycards-1.0.2 (c (n "oxycards") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0b6c5jd1k57dwq7n6mvkkvmrzhw539dq0chyk5r0sbmbdmcxpfip")))

