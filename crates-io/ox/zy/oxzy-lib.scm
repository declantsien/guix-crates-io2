(define-module (crates-io ox zy oxzy-lib) #:use-module (crates-io))

(define-public crate-oxzy-lib-0.1.0 (c (n "oxzy-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "152djgaag3iwjwmypvjdf43v00rc21z26p9j4hh0fr1l9a394czz")))

(define-public crate-oxzy-lib-0.2.0 (c (n "oxzy-lib") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ki0nk812qdy1ranynr1g8b85cpwn6dh2vbjxbrhcr5dx76h0mr5")))

(define-public crate-oxzy-lib-1.0.0 (c (n "oxzy-lib") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01cpgwh2nxa4b214v9m1mps5d7ja3s5mqy29npgaiklrdyacrlrw")))

(define-public crate-oxzy-lib-1.0.1 (c (n "oxzy-lib") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ilwav8kjvngb51lyyycpdgvjw4zi60n8a3kr92g7gqny2jrcs9")))

(define-public crate-oxzy-lib-2.0.0 (c (n "oxzy-lib") (v "2.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04vag661zc49a0afw9cn8xvhy50i5y848rassjzydwd1pvcfkcpr")))

(define-public crate-oxzy-lib-3.0.0 (c (n "oxzy-lib") (v "3.0.0") (d (list (d (n "serde") (r "^1.0.103, <1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dwxjy95cv8cpk3p7rlsa8vwnswn2kvrgp91fpsr23dlkibqxn07")))

(define-public crate-oxzy-lib-3.0.1 (c (n "oxzy-lib") (v "3.0.1") (d (list (d (n "serde") (r "^1.0.103, <1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "17x11mzv6nfg583rx6ghy9ndgpgy976h62ssf0x247zybzk8qkrd")))

