(define-module (crates-io ox yt oxytail-theme-dark) #:use-module (crates-io))

(define-public crate-oxytail-theme-dark-0.1.0 (c (n "oxytail-theme-dark") (v "0.1.0") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.0") (d #t) (k 0)) (d (n "oxytail-theme-defaults") (r "^0.1.0") (d #t) (k 0)))) (h "17avpy3b7apa5fh8kg5mv554qzclnrhl4dhmwgad4hfirxjfn8qa") (r "1.75")))

(define-public crate-oxytail-theme-dark-0.1.1 (c (n "oxytail-theme-dark") (v "0.1.1") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-theme-defaults") (r "^0.1.1") (d #t) (k 0)))) (h "099q550h0w52d1iaa6f5yppsidl4rn9xa6dx6dgf7r4kl98l3bw3") (r "1.75")))

(define-public crate-oxytail-theme-dark-0.1.2 (c (n "oxytail-theme-dark") (v "0.1.2") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.2") (d #t) (k 0)) (d (n "oxytail-theme-defaults") (r "^0.1.2") (d #t) (k 0)))) (h "168wnf02pd08791sarrmmmmqmyyy5y6ls93k6a4z18g23zpb3c76") (r "1.75")))

