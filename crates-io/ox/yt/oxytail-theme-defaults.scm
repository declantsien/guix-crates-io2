(define-module (crates-io ox yt oxytail-theme-defaults) #:use-module (crates-io))

(define-public crate-oxytail-theme-defaults-0.1.0 (c (n "oxytail-theme-defaults") (v "0.1.0") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.0") (d #t) (k 0)))) (h "1b83zkdlik9gsx1wbakd1kinlj5gvbm61shmsl14hf5f5lj7ri9q") (r "1.75")))

(define-public crate-oxytail-theme-defaults-0.1.1 (c (n "oxytail-theme-defaults") (v "0.1.1") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.1") (d #t) (k 0)))) (h "0r7jspghvqf1br9xr1pwrfs1mw1jicpv866rvhwyvpl67rzbh8q5") (r "1.75")))

(define-public crate-oxytail-theme-defaults-0.1.2 (c (n "oxytail-theme-defaults") (v "0.1.2") (d (list (d (n "floem") (r "^0.1.1") (d #t) (k 0)) (d (n "oxytail-base") (r "^0.1.2") (d #t) (k 0)))) (h "1m1kcpl30dd74a6kn056wmzc3r6y3yfv7q9vzcddrr8qig5aq66h") (r "1.75")))

