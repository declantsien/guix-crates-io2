(define-module (crates-io sb nf sbnfc) #:use-module (crates-io))

(define-public crate-sbnfc-0.3.1 (c (n "sbnfc") (v "0.3.1") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.3.1") (d #t) (k 0)))) (h "09qi7hrgnm3vj649s6kbvmx509sbcsm4icmvsn9qgp381nx6sk8j")))

(define-public crate-sbnfc-0.4.0 (c (n "sbnfc") (v "0.4.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.4.0") (d #t) (k 0)))) (h "0fvlsnskh463qk6dz09b409iz89cfz1j5gzwmy3nlnwzylrbiv76")))

(define-public crate-sbnfc-0.5.0 (c (n "sbnfc") (v "0.5.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.5.0") (d #t) (k 0)))) (h "0ips2h8is466bk42cnl52v5nxqnh0cajr24mvllp22iz0lrrigqs")))

(define-public crate-sbnfc-0.5.1 (c (n "sbnfc") (v "0.5.1") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.5.1") (d #t) (k 0)))) (h "0hrgp2fdx06ckq0zyrzn6dl2jnfq4vvlhfixwlr9c0abd5y0w8yj")))

(define-public crate-sbnfc-0.6.0 (c (n "sbnfc") (v "0.6.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.6.0") (d #t) (k 0)))) (h "06qjzk38x36pcjz7apd2p01rs2rjz48fl0iq9y61w3r4fifp521l")))

(define-public crate-sbnfc-0.6.1 (c (n "sbnfc") (v "0.6.1") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.6.0") (d #t) (k 0)))) (h "1g5qf9mj1cw6vkk69m8pj5kmg60vm291jmmmfammczgilsg86ccq")))

(define-public crate-sbnfc-0.6.2 (c (n "sbnfc") (v "0.6.2") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.6.2") (d #t) (k 0)))) (h "012q9a2zynma0qq2n0haq273zinxxqcpivc7kvpgjglsb73bkcd7")))

(define-public crate-sbnfc-0.6.3 (c (n "sbnfc") (v "0.6.3") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.6.3") (d #t) (k 0)))) (h "176xcdrz1may15s553yv655smz8f6c60hh04wnn6bfv656g710py")))

(define-public crate-sbnfc-0.6.4 (c (n "sbnfc") (v "0.6.4") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "sbnf") (r "^0.6.4") (d #t) (k 0)))) (h "1w5z362rdmsv4c7kjhava0pj8lkk14hv25ad2wlfl2kk6j8vj01g")))

