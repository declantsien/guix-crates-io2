(define-module (crates-io sb nf sbnf) #:use-module (crates-io))

(define-public crate-sbnf-0.1.0 (c (n "sbnf") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("wrap_help"))) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "14903bm1y5x2vp88vqdnkp5fhwk77nkifyq383z8161ss1h0k1ym")))

(define-public crate-sbnf-0.3.0 (c (n "sbnf") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0xpd5rkaxzvpsdq7y1npr8bbd6cqf98x7q70wmfq6zjvs01z2jbs")))

(define-public crate-sbnf-0.3.1 (c (n "sbnf") (v "0.3.1") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "045v9lq7vn19kxdnyzlfxj9mj5aj0kafb3x0zaj8n5bvvm9v1x1k")))

(define-public crate-sbnf-0.4.0 (c (n "sbnf") (v "0.4.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1szywn7di9h0plrww85c8lw1ygcc02q5h7ybb5326xmar91iil2l")))

(define-public crate-sbnf-0.5.0 (c (n "sbnf") (v "0.5.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0cfwq2a51yi6r838yj9g8cglp4adiyvjj42jn7vj0kziw80n0id0")))

(define-public crate-sbnf-0.5.1 (c (n "sbnf") (v "0.5.1") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1bwhxhwrgyprh000llky5mspl765d322insb400jf1pxkz0bvkj5")))

(define-public crate-sbnf-0.6.0 (c (n "sbnf") (v "0.6.0") (d (list (d (n "base64") (r "^0.21.1") (d #t) (k 0)) (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "symbol_table") (r "^0.3.0") (d #t) (k 0)))) (h "0h2s5lz7hxpir4mkdhxp0nalf0p9p9ilgxlh0fymmdxn30mja5p7")))

(define-public crate-sbnf-0.6.1 (c (n "sbnf") (v "0.6.1") (d (list (d (n "base64") (r "^0.21.1") (d #t) (k 0)) (d (n "bumpalo") (r "^3.13.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "symbol_table") (r "^0.3.0") (d #t) (k 0)))) (h "04s01mzjfg0i7if1xgmr7ihvpi3dpay818drg9a2l4yxg3igcg7c")))

(define-public crate-sbnf-0.6.2 (c (n "sbnf") (v "0.6.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1k2nr56s264yyv7kpsj48f4gidhxwy47gb9k3dbdzq4724nr7041")))

(define-public crate-sbnf-0.6.3 (c (n "sbnf") (v "0.6.3") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0nnzd3b0nh6xvhrf2njm7rzfd6c3zsq369z1rwlwz44wqjz7q1l0")))

(define-public crate-sbnf-0.6.4 (c (n "sbnf") (v "0.6.4") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "1329rdpyrmsh73clgq2y8qr8an1l5xf553wgi7j3f8k4qxqpimcw")))

