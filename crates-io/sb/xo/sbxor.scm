(define-module (crates-io sb xo sbxor) #:use-module (crates-io))

(define-public crate-sbxor-0.1.0 (c (n "sbxor") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "words") (r "^0.1.2") (d #t) (k 0)))) (h "1d21bskaj6347k49zz72mncm5x2mw27kfvbv9xdqwgx5293q8cbm")))

(define-public crate-sbxor-0.1.1 (c (n "sbxor") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "words") (r "^0.1.2") (d #t) (k 0)))) (h "0i5jpzdk67pjwlbbpgwqgbri1zk0jyaiffmi9hvx1kf7i2fqpl9a")))

(define-public crate-sbxor-0.1.2 (c (n "sbxor") (v "0.1.2") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "words") (r "^0.1.2") (d #t) (k 0)))) (h "0ys0n5p0kmq66s4xa3gzfcqgm61bnzpj17pyxykvymnf0fip1zwz")))

