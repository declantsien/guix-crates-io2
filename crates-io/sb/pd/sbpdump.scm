(define-module (crates-io sb pd sbpdump) #:use-module (crates-io))

(define-public crate-sbpdump-0.1.0 (c (n "sbpdump") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yw81z2nraflyya3m3dbnn13y3kvn62af4hzrz3blhb0fhcnvkqd")))

(define-public crate-sbpdump-0.1.1 (c (n "sbpdump") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n6qbpdh67hsg8rjr7avbmjv43lfb07fk30m9j2kqpw0mdmwlkf5")))

