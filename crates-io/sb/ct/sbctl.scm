(define-module (crates-io sb ct sbctl) #:use-module (crates-io))

(define-public crate-sbctl-0.0.1 (c (n "sbctl") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0b6g0qba6ijj8wyjgxq9g21gb53igrpp8g52rh3qxq6l1794aj71")))

