(define-module (crates-io sb d- sbd-cli) #:use-module (crates-io))

(define-public crate-sbd-cli-0.1.0 (c (n "sbd-cli") (v "0.1.0") (h "12ayv3fgqwn48w3w1ck8lp09rsksq0vlf86xdkqhqrf0v9mx8aks")))

(define-public crate-sbd-cli-0.1.1 (c (n "sbd-cli") (v "0.1.1") (h "18jmvn0mmaijmsq2m3c73xc24chrbs179gnl17b3wf37x55lxqzx")))

(define-public crate-sbd-cli-0.1.2 (c (n "sbd-cli") (v "0.1.2") (h "1ccpbsx3v4q32vk912mj7qf7d73hrq6hhn052xwrd8r9si7rxlx6")))

(define-public crate-sbd-cli-0.1.3 (c (n "sbd-cli") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0syb9nf0p6shdq6j06mqsp0p7zpbcssilawiacwnb0w732bx7dqc")))

(define-public crate-sbd-cli-0.1.4 (c (n "sbd-cli") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "11dlzxn5a50bas3hkqadsxsvwrwz41qax56lq9vq6iyb343gibm2")))

(define-public crate-sbd-cli-0.1.5 (c (n "sbd-cli") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0drbzrmjz290g6d4103c54gsbbllijk94pyn1aym15qllmnxyn30")))

(define-public crate-sbd-cli-0.1.6 (c (n "sbd-cli") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1l27fl89qw57sarhhyp15m8llhl98wp2qyc7s1gjiri1wwfdzqg7")))

(define-public crate-sbd-cli-0.1.7 (c (n "sbd-cli") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0krnsiyn6856615mhsz2xw6bkb2wjcd5b09gynaa9b6jrcqipwkq")))

(define-public crate-sbd-cli-0.1.8 (c (n "sbd-cli") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "018w0p8zrx3v8680rf45976n7zxrk5h31hzmfslvwz8wzjhm01rz")))

