(define-module (crates-io sb d- sbd-o-bahn-client-tester) #:use-module (crates-io))

(define-public crate-sbd-o-bahn-client-tester-0.0.1-alpha (c (n "sbd-o-bahn-client-tester") (v "0.0.1-alpha") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "1psqsij3r5zni95mgc652hfysagivb0mrzv8wpdwq5dcqa9kgp1z")))

(define-public crate-sbd-o-bahn-client-tester-0.0.2-alpha (c (n "sbd-o-bahn-client-tester") (v "0.0.2-alpha") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "1kj7sn49bg36q504z0awfgs12q5fsl5gbm0v7i1mrarfq52xj1z2")))

(define-public crate-sbd-o-bahn-client-tester-0.0.3-alpha (c (n "sbd-o-bahn-client-tester") (v "0.0.3-alpha") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "08rcqh7h0b8ssc17l89hk8nypafg9500z3wimrpr5g8hj21gzrnb")))

(define-public crate-sbd-o-bahn-client-tester-0.0.4-alpha (c (n "sbd-o-bahn-client-tester") (v "0.0.4-alpha") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "16rssp9bsn5d6l6k73k1g1cl1p11vr9nk1pamasl60qajlai075j")))

