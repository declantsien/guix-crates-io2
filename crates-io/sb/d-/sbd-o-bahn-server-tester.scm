(define-module (crates-io sb d- sbd-o-bahn-server-tester) #:use-module (crates-io))

(define-public crate-sbd-o-bahn-server-tester-0.0.1-alpha (c (n "sbd-o-bahn-server-tester") (v "0.0.1-alpha") (d (list (d (n "sbd-client") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "1c3qr7hdhhqwpj7gdwrk2r01h5sfvj5wmjr61mi0ix7f0bcb0yz9")))

(define-public crate-sbd-o-bahn-server-tester-0.0.2-alpha (c (n "sbd-o-bahn-server-tester") (v "0.0.2-alpha") (d (list (d (n "sbd-client") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "12m0h4ng407j4z4hx2hm8g8mmhrms41mz81h1vj8fyqvb2vfa65q")))

(define-public crate-sbd-o-bahn-server-tester-0.0.3-alpha (c (n "sbd-o-bahn-server-tester") (v "0.0.3-alpha") (d (list (d (n "sbd-client") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "13da99zpg1psrzv88rqbsa154l4zg1r54g4iysbkdhv3dqin7caw")))

(define-public crate-sbd-o-bahn-server-tester-0.0.4-alpha (c (n "sbd-o-bahn-server-tester") (v "0.0.4-alpha") (d (list (d (n "sbd-client") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 0)))) (h "0wrifp3ic57bkyqpqy7fp532r263cvawl00v0k5yfzf2v50dlvpd")))

