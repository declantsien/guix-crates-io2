(define-module (crates-io sb d- sbd-e2e-crypto-client) #:use-module (crates-io))

(define-public crate-sbd-e2e-crypto-client-0.0.1-alpha (c (n "sbd-e2e-crypto-client") (v "0.0.1-alpha") (d (list (d (n "sbd-client") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.1-alpha") (d #t) (k 2)) (d (n "sodoken") (r "^0.0.901-alpha") (k 0)) (d (n "tokio") (r "^1.37.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 2)))) (h "10cpiz63f64pz807xv1kciy9dyalh3zx5rlmqmgk4zav1qfdrp61")))

(define-public crate-sbd-e2e-crypto-client-0.0.2-alpha (c (n "sbd-e2e-crypto-client") (v "0.0.2-alpha") (d (list (d (n "sbd-client") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.2-alpha") (d #t) (k 2)) (d (n "sodoken") (r "^0.0.901-alpha") (k 0)) (d (n "tokio") (r "^1.37.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 2)))) (h "0g491d1mpjh504xdfqjads5nwz4ccrpc5gsgx19a3cnpfqvvwchr")))

(define-public crate-sbd-e2e-crypto-client-0.0.3-alpha (c (n "sbd-e2e-crypto-client") (v "0.0.3-alpha") (d (list (d (n "sbd-client") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.3-alpha") (d #t) (k 2)) (d (n "sodoken") (r "^0.0.901-alpha") (k 0)) (d (n "tokio") (r "^1.37.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 2)))) (h "0cyaqbaayb5h9hvzi8haxhdmk56q0d719skjj7pwa356z8cqz2fp")))

(define-public crate-sbd-e2e-crypto-client-0.0.4-alpha (c (n "sbd-e2e-crypto-client") (v "0.0.4-alpha") (d (list (d (n "sbd-client") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "sbd-server") (r "^0.0.4-alpha") (d #t) (k 2)) (d (n "sodoken") (r "^0.0.901-alpha") (k 0)) (d (n "tokio") (r "^1.37.0") (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (k 2)))) (h "1pald0v0cmvgdnqy6dzq4rmzbg7nin2h7fx32swprn7i3ppildrz")))

