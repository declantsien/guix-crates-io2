(define-module (crates-io sb ox sbox) #:use-module (crates-io))

(define-public crate-sbox-0.1.0 (c (n "sbox") (v "0.1.0") (d (list (d (n "clone3") (r "^0.2.3") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user"))) (d #t) (k 0)))) (h "1p9wnr34mzsfh0h4la5gwk4zg8jpzj3904qwmzdcnni80y0hl9wv")))

(define-public crate-sbox-0.1.1 (c (n "sbox") (v "0.1.1") (d (list (d (n "clone3") (r "^0.2.3") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user"))) (d #t) (k 0)))) (h "01w9i83cz87lbnnryzv2cq5z9y2dfhnp031l391z5lvvmi7g9hns")))

(define-public crate-sbox-0.1.2 (c (n "sbox") (v "0.1.2") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount"))) (d #t) (k 0)))) (h "1hw1slhh9cygfcfhapipncv9zx28x710mr61kp560yb24yswpkry")))

(define-public crate-sbox-0.1.3 (c (n "sbox") (v "0.1.3") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount"))) (d #t) (k 0)))) (h "132lmjx9s5pc6az5xnybywxz5mmsg43blzzgxn95wixn2di912sh")))

(define-public crate-sbox-0.1.4 (c (n "sbox") (v "0.1.4") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount"))) (d #t) (k 0)))) (h "0hwqfqq60x4pax044fipnm2m1ms1d4650c08bnx5y755bjk64rqi")))

(define-public crate-sbox-0.1.5 (c (n "sbox") (v "0.1.5") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18ykdx1vw8261s5rjrcc87b6w1bnqk6zjyqg41x83b7lipwkcjl8")))

(define-public crate-sbox-0.1.6 (c (n "sbox") (v "0.1.6") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10z5f13ms532ybq5qyk19y5r4vf3lxsfqgfj50r0hzgbszx4q446")))

(define-public crate-sbox-0.1.7 (c (n "sbox") (v "0.1.7") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "13yrh2ia4kwp35cw50xfm23w6kpf8lmga5g753bx0jrh10djs5wb")))

(define-public crate-sbox-0.1.8 (c (n "sbox") (v "0.1.8") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1i1z54j49phj6mlrsw6zsi5c34kmq2lpn7da23v01rs0wza5lklf")))

