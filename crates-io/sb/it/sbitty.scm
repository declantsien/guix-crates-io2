(define-module (crates-io sb it sbitty) #:use-module (crates-io))

(define-public crate-sbitty-0.5.0 (c (n "sbitty") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lm8g79x6pfgdgfby0116kfkdyrgqj1c8c880rgxggaqa6syq3wj")))

(define-public crate-sbitty-0.5.1 (c (n "sbitty") (v "0.5.1") (h "1zs2lsyjjpj0s5ms1lr1saw10rafqx0akskv1qcf5x7vgzmw7nfg")))

(define-public crate-sbitty-0.5.2 (c (n "sbitty") (v "0.5.2") (h "160c9r6fj36q6c2hd2s41jg35h6w8xqqzyi7xhlxzl4gnhffmiik")))

(define-public crate-sbitty-0.5.3 (c (n "sbitty") (v "0.5.3") (h "0cw70p1cjb88lbiv9angcll94y6d71233aah6sikz8vsb3j3kvy0")))

(define-public crate-sbitty-0.5.4 (c (n "sbitty") (v "0.5.4") (h "0rx28nd2v1aplqg7c066iivpqk9ab8gls16mv4h9qpzvmcjgllvv")))

(define-public crate-sbitty-0.5.5 (c (n "sbitty") (v "0.5.5") (h "17cpdvlrj315jkirhl943dwqg3gxvsvbsq0sa65y3618ycbzgrdy")))

(define-public crate-sbitty-0.1.0 (c (n "sbitty") (v "0.1.0") (h "17s1jlfcqwn0cl3752lvmnldkax3d2awn5sq30z3xrlid8zirmlv")))

(define-public crate-sbitty-0.1.1 (c (n "sbitty") (v "0.1.1") (h "15blssyxnd9a72n1c8x7lpzfgcks3npnrggxd48ynpa1zi62v4vq")))

(define-public crate-sbitty-1.0.0 (c (n "sbitty") (v "1.0.0") (h "0cmxicjqdj1jsr97y1qdpqc14s6h6406d05jx8ysr61f9mfm975v")))

(define-public crate-sbitty-1.0.1 (c (n "sbitty") (v "1.0.1") (h "1z5y963x3ssqfb7h6a08bynkiswqhnkqimn32r4iyswfcypjpqma")))

(define-public crate-sbitty-1.1.1 (c (n "sbitty") (v "1.1.1") (h "1g9ii7zlrihy7018ap3r26yg18w0v13sfzrwm7wyd9p5m0dwvc4d")))

