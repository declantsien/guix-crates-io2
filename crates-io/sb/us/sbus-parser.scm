(define-module (crates-io sb us sbus-parser) #:use-module (crates-io))

(define-public crate-sbus-parser-0.0.1 (c (n "sbus-parser") (v "0.0.1") (h "04x8gjml8yi11h9wac6pfq9hgf4n5xncg4dv68jqjy22wwfxdp14")))

(define-public crate-sbus-parser-0.0.2 (c (n "sbus-parser") (v "0.0.2") (h "1c13n9b6k2mglvy5jbabv84xrn30m4sa29kd29qs2268rj12pcq4")))

(define-public crate-sbus-parser-0.1.0 (c (n "sbus-parser") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0vddn1fz271snsxz78jgn49qgnnfir3m106vxl3x7kp2sin7a06m")))

