(define-module (crates-io sb us sbus) #:use-module (crates-io))

(define-public crate-sbus-0.1.0 (c (n "sbus") (v "0.1.0") (d (list (d (n "arraydeque") (r "~0.4") (k 0)))) (h "0vn1hqwy0n95x529938d41pi9y3szpzjp76883rd10j0cbjpi2py")))

(define-public crate-sbus-0.2.0 (c (n "sbus") (v "0.2.0") (d (list (d (n "arraydeque") (r "~0.4") (k 0)))) (h "1wmpqarpqc6m5kd28l9brmdwfcgwqq3zvvqqq5vsw99y04xv162x")))

(define-public crate-sbus-0.2.1 (c (n "sbus") (v "0.2.1") (d (list (d (n "arraydeque") (r "~0.4") (k 0)))) (h "1hyqc0gnhvqcxdkb0m48pa90i5jjp76jn5z2hflsj1clw6sw1qyb")))

(define-public crate-sbus-0.2.2 (c (n "sbus") (v "0.2.2") (d (list (d (n "arraydeque") (r "~0.4") (k 0)))) (h "1mzpczxzzmny96ry4wmrpbpyr1xcx1228rffmkjpls92rhcff42j")))

