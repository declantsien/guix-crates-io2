(define-module (crates-io sb f- sbf-blake3) #:use-module (crates-io))

(define-public crate-sbf-blake3-0.2.0 (c (n "sbf-blake3") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "md4") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kdv73qz3yv582dw3gcs809dixvx9ayglrzswskpqqv63sd0hzi0") (f (quote (("serde_support" "serde") ("metrics") ("md5_hash" "md5") ("md4_hash" "md4") ("default" "md5_hash") ("blake3_hash" "blake3"))))))

