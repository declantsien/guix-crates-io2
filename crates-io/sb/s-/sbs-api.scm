(define-module (crates-io sb s- sbs-api) #:use-module (crates-io))

(define-public crate-sbs-api-0.1.0 (c (n "sbs-api") (v "0.1.0") (d (list (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "sbs-api-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1k4m7sv6gb5jgrvzl0f01z8l0l2vdmq5qh8d09bi9kw6sby1k7wf")))

(define-public crate-sbs-api-0.1.12 (c (n "sbs-api") (v "0.1.12") (d (list (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "sbs-api-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0fk9w3cjr7lx2h60a3ggw9bqa725f3z974s0wglc2m4jkr772r3d")))

(define-public crate-sbs-api-0.1.13 (c (n "sbs-api") (v "0.1.13") (d (list (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "sbs-api-macro") (r "^0.1.12") (d #t) (k 0)))) (h "1gymzi0m5ns2fj5bwzpm4hr6hmapswv1w8ygr3iqxcgzmyfzv9fv")))

(define-public crate-sbs-api-0.1.14 (c (n "sbs-api") (v "0.1.14") (d (list (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "sbs-api-macro") (r "^0.1.13") (d #t) (k 0)))) (h "0qhf4q6b6yrnxd4kaf6ifymikn1z9azz68y1lqjqc92vi5ii46gi")))

(define-public crate-sbs-api-0.1.15 (c (n "sbs-api") (v "0.1.15") (d (list (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "sbs-api-macro") (r "^0.1.14") (d #t) (k 0)))) (h "0d4can5mjyy47zrxnrm5kri378iq4j5aqyaysgy1mbz2caqq4ihj")))

