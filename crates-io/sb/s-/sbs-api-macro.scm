(define-module (crates-io sb s- sbs-api-macro) #:use-module (crates-io))

(define-public crate-sbs-api-macro-0.1.0 (c (n "sbs-api-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "159y69fxlxanjc0iwlp33sv6y249scbdbcwyn0wsd3vpk5bvbayn")))

(define-public crate-sbs-api-macro-0.1.1 (c (n "sbs-api-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "17sgliyjhg04wk4ww1cqy8fg215jbhn5rb2xmxnhrm2fbqdwakmr")))

(define-public crate-sbs-api-macro-0.1.11 (c (n "sbs-api-macro") (v "0.1.11") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "1qc783i6wm9vvyxqfwm1qmzndh2y69d9qf3wdzlnfv76awv4cczp")))

(define-public crate-sbs-api-macro-0.1.12 (c (n "sbs-api-macro") (v "0.1.12") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "0ppg8my2a26cas06jb57h024dilyznk2hrxw0hxrjd0gaai8h92s")))

(define-public crate-sbs-api-macro-0.1.13 (c (n "sbs-api-macro") (v "0.1.13") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "0mnvsk5jdbprnyhfiy87fhzv2n26dizn2pj3bg0qrjvb6yfrr26s")))

(define-public crate-sbs-api-macro-0.1.14 (c (n "sbs-api-macro") (v "0.1.14") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "sbs-api-internal") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (d #t) (k 0)))) (h "18n3f89xm27w72a75qk37j61y42g27agxr392lna58j9s1xyirza")))

