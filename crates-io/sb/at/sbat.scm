(define-module (crates-io sb at sbat) #:use-module (crates-io))

(define-public crate-sbat-0.1.0 (c (n "sbat") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "1inbhci1ddcm4wpx70nsabd2y3lvy4c15adjh77vf7mnmwv3y3x8") (f (quote (("alloc"))))))

(define-public crate-sbat-0.2.0 (c (n "sbat") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "0w6c658lb98j2j8h3f4nwppkilfwf7hycj3g5m8jswbgfbz74da5") (f (quote (("alloc"))))))

(define-public crate-sbat-0.3.0 (c (n "sbat") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "0q7vkf7njx9dl5j6j09x731zv97gm65gknnk0ac10b74v2a6lk9g") (f (quote (("alloc"))))))

(define-public crate-sbat-0.3.1 (c (n "sbat") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "0n2wp3sbj3lldw4vsk0wbq9bh1skq41ri3w8m1k43n2dix906jss") (f (quote (("alloc")))) (r "1.64")))

(define-public crate-sbat-0.3.2 (c (n "sbat") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "1dk32984s3di0c3rzhbxfidfzfdb26y9qc4g4dbbpj7dqmrwbsry") (f (quote (("std" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-sbat-0.4.0 (c (n "sbat") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "0jyycsv6cx92dflzwig7827rqm6fr8w1afhcrgc07w33x9mms8im") (f (quote (("std" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-sbat-0.5.0 (c (n "sbat") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "0rcvpv14311a25g2biwcyijrsq0bfzdpdfnq7bmaykgyj7x4fbid") (f (quote (("std" "alloc") ("alloc" "ascii/alloc")))) (r "1.64")))

(define-public crate-sbat-0.5.1 (c (n "sbat") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "1nvi7fhk0mvwdmqsdbvvpc3i9l7v449k8jlfmnzfx211jqjzh34w") (f (quote (("std" "alloc") ("alloc" "ascii/alloc")))) (r "1.74")))

(define-public crate-sbat-0.5.2 (c (n "sbat") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "ascii") (r "^1.0.0") (k 0)) (d (n "log") (r "^0.4.0") (k 0)))) (h "1xn26qk2qd3glq8c3l9s89lqqc0maymhqv9wvzrqcq8a2kfxkxb3") (f (quote (("std" "alloc") ("alloc" "ascii/alloc")))) (r "1.74")))

