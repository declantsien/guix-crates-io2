(define-module (crates-io sb x_ sbx_cloud) #:use-module (crates-io))

(define-public crate-sbx_cloud-0.1.0 (c (n "sbx_cloud") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "14hprg0wb7gw601khm6yn7fjgqa0jd84ihalwm3bqcishk5m2lgi")))

(define-public crate-sbx_cloud-0.1.10 (c (n "sbx_cloud") (v "0.1.10") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0wfl7n0m9pr4bblzky9wqwgzdrn6z031jbrz56rkwndkkvz1fgkn")))

(define-public crate-sbx_cloud-0.1.11 (c (n "sbx_cloud") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0gcq2qmqnjzfjx4m3df1c6cqpy2ilw71x2jviz54lk6cq09w7256")))

(define-public crate-sbx_cloud-0.1.12 (c (n "sbx_cloud") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1hbap1fbgpnwgzazmp3ndwrqnvxkkdvjq07z7z5cfb2n2vq6j7mp")))

(define-public crate-sbx_cloud-0.1.13 (c (n "sbx_cloud") (v "0.1.13") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1w954zs4mfah6zfn9a3izxhvf1s7cmz25fjm9lpik93dar654rzl")))

(define-public crate-sbx_cloud-0.1.15 (c (n "sbx_cloud") (v "0.1.15") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1cgbg7q1lmrv25gipz8q2y9vc3sxi2hkf2gy3g31j21r2dah2mxm")))

(define-public crate-sbx_cloud-0.1.16 (c (n "sbx_cloud") (v "0.1.16") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "18wni1sqp8sjjl6vgr750pjbyay21jx4il34ldg5nkzblzaisikz")))

