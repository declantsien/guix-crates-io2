(define-module (crates-io sb bf sbbf-rs-safe) #:use-module (crates-io))

(define-public crate-sbbf-rs-safe-0.0.1 (c (n "sbbf-rs-safe") (v "0.0.1") (d (list (d (n "sbbf-rs") (r "^0.0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "08m591pccirb80icdll4mhfkhw6l20m5v5spf7g5ah3vbw5zyz4i") (y #t)))

(define-public crate-sbbf-rs-safe-0.0.2 (c (n "sbbf-rs-safe") (v "0.0.2") (d (list (d (n "sbbf-rs") (r "^0.0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0m3phlh7964wixjn3zj20dn38pc338y9ifs40pa0ayfm1w4n3rx1") (y #t)))

(define-public crate-sbbf-rs-safe-0.0.3 (c (n "sbbf-rs-safe") (v "0.0.3") (d (list (d (n "sbbf-rs") (r "^0.0.2") (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "09wpm59yhphkc612fw331ajrjk7y4i2qjcbnkdarqz82zwk9nszf") (y #t)))

(define-public crate-sbbf-rs-safe-0.0.4 (c (n "sbbf-rs-safe") (v "0.0.4") (d (list (d (n "sbbf-rs") (r "^0.0.2") (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "1pali2pckyfczh2p5yclqcgzlk80pcm5dyxr46si0vllah1zmjfv")))

(define-public crate-sbbf-rs-safe-0.1.0 (c (n "sbbf-rs-safe") (v "0.1.0") (d (list (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)))) (h "07s2km7yxb53k55nighrzapbxxwlzl0kyi7ldwfh9l4i61imqacf")))

(define-public crate-sbbf-rs-safe-0.1.1 (c (n "sbbf-rs-safe") (v "0.1.1") (d (list (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)))) (h "170k1jqrqmf40qhrqsm7kn2zalcymx70rn8glflbimvd8zh18r86")))

(define-public crate-sbbf-rs-safe-0.1.2 (c (n "sbbf-rs-safe") (v "0.1.2") (d (list (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0i3mqpfqhw9ksprkgs49f1x945vwfyaqfgpw826764npbj6a8nvv")))

(define-public crate-sbbf-rs-safe-0.2.0 (c (n "sbbf-rs-safe") (v "0.2.0") (d (list (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0v4r8x8yppmxvmlp6s4905naq4yzr8z5z0jdwmiih4ky77cic2ck")))

(define-public crate-sbbf-rs-safe-0.2.1 (c (n "sbbf-rs-safe") (v "0.2.1") (d (list (d (n "bloom") (r "^0.3.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5.3") (d #t) (k 2)) (d (n "probabilistic-collections") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-bloom") (r "^1.16.1") (d #t) (k 2)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "16pmqshjd74r5c1wfhwvfhz8x6salizh71808pbyiy48a55brs7h")))

(define-public crate-sbbf-rs-safe-0.2.2 (c (n "sbbf-rs-safe") (v "0.2.2") (d (list (d (n "bloom") (r "^0.3.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5.3") (d #t) (k 2)) (d (n "probabilistic-collections") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sbbf-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-bloom") (r "^1.16.1") (d #t) (k 2)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "0iqq312f5m64kh629fzaiji2zj942wzcy5nk7yxkdc75fif1aw3d")))

(define-public crate-sbbf-rs-safe-0.3.0 (c (n "sbbf-rs-safe") (v "0.3.0") (d (list (d (n "bloom") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5") (d #t) (k 2)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sbbf-rs") (r "^0.2") (d #t) (k 0)) (d (n "solana-bloom") (r "^1.16") (d #t) (k 2)) (d (n "wyhash") (r "^0.5") (d #t) (k 2)))) (h "0xp62ssxw049abk1gbcd0dpdqaxak71v7s10jq2mbnzn9pg6lvb4")))

(define-public crate-sbbf-rs-safe-0.3.1 (c (n "sbbf-rs-safe") (v "0.3.1") (d (list (d (n "bloom") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5") (d #t) (k 2)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sbbf-rs") (r "^0.2") (d #t) (k 0)) (d (n "solana-bloom") (r "^1.16") (d #t) (k 2)) (d (n "wyhash") (r "^0.5") (d #t) (k 2)))) (h "02ffrfpj2l6lfwxjmbyhajrhcm6pdgwnshmra3fjnvhszfk4yhip")))

(define-public crate-sbbf-rs-safe-0.3.2 (c (n "sbbf-rs-safe") (v "0.3.2") (d (list (d (n "bloom") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5") (d #t) (k 2)) (d (n "probabilistic-collections") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sbbf-rs") (r "^0.2") (d #t) (k 0)) (d (n "solana-bloom") (r "^1.16") (d #t) (k 2)) (d (n "wyhash") (r "^0.5") (d #t) (k 2)))) (h "0g5sa4hmxfhf4hygq7gycsdgqq4mmhkdbiv05h3qqgzz5kmzy0lr")))

