(define-module (crates-io sb pl sbplug) #:use-module (crates-io))

(define-public crate-sbplug-0.0.1 (c (n "sbplug") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzhz1x18xkygmck4gzrr8ckxhzja8nz683gxvx3g6dgjbif1516")))

(define-public crate-sbplug-0.0.2 (c (n "sbplug") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1km0zkl0g2d555hkkvri9w8mgf6lmpaiykv08x6z0h0rrdf67i9k")))

(define-public crate-sbplug-0.0.3 (c (n "sbplug") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sxrwq7lk6mp5pn88bf4sy3yxlm75z6pyv1c4ccrjwrxrgcvi8yy") (y #t)))

(define-public crate-sbplug-0.0.4 (c (n "sbplug") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i54f8gbs4km0llsqcy781ryqk7dp72v3fn1ila8r1f4c0bk627m")))

