(define-module (crates-io sb ka sbkafka) #:use-module (crates-io))

(define-public crate-sbkafka-0.1.0 (c (n "sbkafka") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.1") (f (quote ("env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rdkafka") (r "^0.28.0") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.0") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "18mpnjxjpd7xg10hjlcwz3pzp5jz858xm15ggryvdrmxmrqk0dn7")))

