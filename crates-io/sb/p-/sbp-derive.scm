(define-module (crates-io sb p- sbp-derive) #:use-module (crates-io))

(define-public crate-sbp-derive-0.1.0 (c (n "sbp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bmprcwhc9r2dgk494bmnyihhxjr6snr0z3silysigcfq7zv8gdj") (y #t)))

(define-public crate-sbp-derive-0.1.1 (c (n "sbp-derive") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "00phhl946y4r00ypnisd0700fzwm119h43vsxjkgc4yn2ch30gbl") (y #t)))

