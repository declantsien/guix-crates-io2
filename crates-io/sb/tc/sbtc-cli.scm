(define-module (crates-io sb tc sbtc-cli) #:use-module (crates-io))

(define-public crate-sbtc-cli-0.1.0 (c (n "sbtc-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bdk") (r "^0.28.1") (f (quote ("keys-bip39" "rpc"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "~1.8.4") (d #t) (k 0)) (d (n "sbtc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stacks-core") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0bmsngqva2lhhvvdk7gr3zsrwg6fnh7jznmjb76w8skkbhkvsxqw")))

