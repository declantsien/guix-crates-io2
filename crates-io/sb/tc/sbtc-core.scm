(define-module (crates-io sb tc sbtc-core) #:use-module (crates-io))

(define-public crate-sbtc-core-0.1.0 (c (n "sbtc-core") (v "0.1.0") (d (list (d (n "bdk") (r "^0.28.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "p256k1") (r "^5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "regex") (r "~1.8.4") (d #t) (k 0)) (d (n "stacks-core") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "wsts") (r "^1.2") (d #t) (k 0)))) (h "03lar7fr8il8kb9kkly8lyi52ycqlji9k52iqkgyyyb4dzshalqk")))

