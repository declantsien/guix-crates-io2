(define-module (crates-io sb or sbor-derive) #:use-module (crates-io))

(define-public crate-sbor-derive-1.2.0-dev (c (n "sbor-derive") (v "1.2.0-dev") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "sbor-derive-common") (r "^1.2.0-dev") (k 0)))) (h "00d26h8987dcp9v5c5c1wqxgfs467bld0jjp92wbdzwlpwbag23v") (f (quote (("trace" "sbor-derive-common/trace"))))))

(define-public crate-sbor-derive-1.2.0 (c (n "sbor-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "sbor-derive-common") (r "^1.2.0") (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "0hkw1lzyjb76i7par4hqy8d035d9m61kwivahala4jbq5sysclaa") (f (quote (("trace" "sbor-derive-common/trace"))))))

