(define-module (crates-io sb or sbor-derive-common) #:use-module (crates-io))

(define-public crate-sbor-derive-common-1.2.0-dev (c (n "sbor-derive-common") (v "1.2.0-dev") (d (list (d (n "const-sha1") (r "^0.3.0") (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "129xmbb4qpysbx9p62nbr1pbiqvk03mdqqksibr6xrs21f39a1cv") (f (quote (("trace"))))))

(define-public crate-sbor-derive-common-1.2.0 (c (n "sbor-derive-common") (v "1.2.0") (d (list (d (n "const-sha1") (r "^0.3.0") (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "080n3jnip9ixwpncvs49ncc4mrm43bj0dz2sqhjdg0cc3nd21w82") (f (quote (("trace"))))))

