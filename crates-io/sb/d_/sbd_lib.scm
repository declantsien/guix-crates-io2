(define-module (crates-io sb d_ sbd_lib) #:use-module (crates-io))

(define-public crate-sbd_lib-0.2.0 (c (n "sbd_lib") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1abpy8cx2sq5p0yia45xlrgia92np3j1c186ig0i0p8my2v3804v")))

(define-public crate-sbd_lib-0.2.1 (c (n "sbd_lib") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ji7nwnqb4mpdc5524pcdk7la2g527jpxv2gs1l5ps0pg38hym8a") (f (quote (("serde-derive" "chrono/serde" "serde"))))))

(define-public crate-sbd_lib-0.2.2 (c (n "sbd_lib") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "020h121rvfbwar2x4m30js7zrpn4qlsjylwdg2yq3yg3q62cbj87") (f (quote (("serde-derive" "chrono/serde" "serde"))))))

