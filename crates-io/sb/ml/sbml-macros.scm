(define-module (crates-io sb ml sbml-macros) #:use-module (crates-io))

(define-public crate-sbml-macros-0.1.1-alpha.1 (c (n "sbml-macros") (v "0.1.1-alpha.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dmvvirsf322kg9av8isxzi8iac90q2c8krnmhri88nw1x3pa0ia")))

(define-public crate-sbml-macros-0.1.1-alpha.2 (c (n "sbml-macros") (v "0.1.1-alpha.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "15fipa4qjj9h8996ih9xbjqcq4cn6yb80r6wrs0d7bhcxdmny990")))

(define-public crate-sbml-macros-0.1.1 (c (n "sbml-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17rcq0s3wdilkff306y6lrgwv78mbc9pn0jszdndnpdlgvwas97m")))

