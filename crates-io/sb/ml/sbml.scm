(define-module (crates-io sb ml sbml) #:use-module (crates-io))

(define-public crate-sbml-0.1.0 (c (n "sbml") (v "0.1.0") (d (list (d (n "mathml") (r "^0.2.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "146aspkadw91fb7si9w2krfnzvcj7813jxd2sxqrj2d1zhg6vzv9")))

(define-public crate-sbml-0.2.1 (c (n "sbml") (v "0.2.1") (d (list (d (n "mathml") (r "^0.4.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1mxlbqggp2sbz51xj7xfh2ywm42fapyz97dr1fa2cvid1a9lzhn7")))

(define-public crate-sbml-0.2.2 (c (n "sbml") (v "0.2.2") (d (list (d (n "mathml") (r "^0.4.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)))) (h "1h84ligawbq689185999zwcpyk9rnn2zwlg0faw5hjygf762ilq0")))

