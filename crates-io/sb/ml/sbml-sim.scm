(define-module (crates-io sb ml sbml-sim) #:use-module (crates-io))

(define-public crate-sbml-sim-0.1.1-alpha.2 (c (n "sbml-sim") (v "0.1.1-alpha.2") (d (list (d (n "sbml-rs") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "0sgslpp6dyn8m7vx3nsx2idawhn8ipl3qld79hs6dih4nih5vcq8")))

(define-public crate-sbml-sim-0.1.1 (c (n "sbml-sim") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mathml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "sbml-rs") (r "^0.1.1") (d #t) (k 0)))) (h "01l3yazsk7h6pzk2r2p779xgci1nrdwb5941ksq7k2p608i9i3hq")))

(define-public crate-sbml-sim-0.1.2 (c (n "sbml-sim") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "mathml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "sbml-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1jaw492kk66k4g88bwk2ha7p97h1kpbm0n592w2k0fpq4r9mr826")))

