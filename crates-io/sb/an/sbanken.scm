(define-module (crates-io sb an sbanken) #:use-module (crates-io))

(define-public crate-sbanken-0.0.1-alpha.1 (c (n "sbanken") (v "0.0.1-alpha.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "urlencoding") (r "^1") (d #t) (k 0)))) (h "1vqzp34p14y1012pqxxa0g04ll1dw13xmm4vdqr0f0z911ix1l9c")))

(define-public crate-sbanken-0.0.1-alpha.2 (c (n "sbanken") (v "0.0.1-alpha.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "urlencoding") (r "^1") (d #t) (k 0)))) (h "1dsrlxqqlzwvay7x31spvlaadhpx92jcqgqw6rjsx5pvr8hs835a")))

(define-public crate-sbanken-0.0.1-alpha.3 (c (n "sbanken") (v "0.0.1-alpha.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qi8cx8c034lrh0m52jgaq93ggi5fhda979rsnlv1261mp941b1n")))

