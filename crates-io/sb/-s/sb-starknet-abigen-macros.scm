(define-module (crates-io sb -s sb-starknet-abigen-macros) #:use-module (crates-io))

(define-public crate-sb-starknet-abigen-macros-0.1.0 (c (n "sb-starknet-abigen-macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sb-starknet-abigen-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "starknet") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0168ay66rz9s533n48k1rafclpdnag99c1cabf8habxqh6p3sslq")))

(define-public crate-sb-starknet-abigen-macros-0.1.1 (c (n "sb-starknet-abigen-macros") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sb-starknet-abigen-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "starknet") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1l14afb406l11ivi8nyw9w9kwgrh2xqb26cgn455r6zhx0bgy9xf")))

(define-public crate-sb-starknet-abigen-macros-0.1.2 (c (n "sb-starknet-abigen-macros") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sb-starknet-abigen-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "starknet") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0bdnjj8fmq20b8fkb1sc67fyv69x5cwwvswpnl7dzxixmj9735qd")))

