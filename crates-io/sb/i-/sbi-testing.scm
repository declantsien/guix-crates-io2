(define-module (crates-io sb i- sbi-testing) #:use-module (crates-io))

(define-public crate-sbi-testing-0.0.0 (c (n "sbi-testing") (v "0.0.0") (d (list (d (n "log_crate") (r "^0.4.17") (o #t) (d #t) (k 0) (p "log")) (d (n "riscv") (r "^0.8") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.1") (d #t) (k 0)))) (h "1wc48ghy0bl5nbhjhav9nmchj7b7i354nz667chlw4f068h0hjpy") (f (quote (("log" "log_crate"))))))

(define-public crate-sbi-testing-0.0.1 (c (n "sbi-testing") (v "0.0.1") (d (list (d (n "log_crate") (r "^0.4.17") (o #t) (d #t) (k 0) (p "log")) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.2") (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "0hvlspqxhfzy5p29pymi4y0dc2wh8irj4ir53hc2s7q1ykf26msb") (f (quote (("log" "log_crate"))))))

(define-public crate-sbi-testing-0.0.3-alpha.1 (c (n "sbi-testing") (v "0.0.3-alpha.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "riscv") (r "^0.11.0") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.3") (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "1khm1g6g2mgw1g0k7fhy6lgkg3iv8a16a7h23fxwcnm1mnps5ypd") (s 2) (e (quote (("log" "dep:log"))))))

(define-public crate-sbi-testing-0.0.3-alpha.2 (c (n "sbi-testing") (v "0.0.3-alpha.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "riscv") (r "^0.11.0") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.3") (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "07jk30aj6p2rp4mc0yh3s57vadqds8szznn3w6kpgpkyw0f0yp0k") (s 2) (e (quote (("log" "dep:log"))))))

