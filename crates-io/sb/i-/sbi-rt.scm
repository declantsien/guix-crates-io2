(define-module (crates-io sb i- sbi-rt) #:use-module (crates-io))

(define-public crate-sbi-rt-0.0.0 (c (n "sbi-rt") (v "0.0.0") (d (list (d (n "sbi-spec") (r "^0.0.2") (d #t) (k 0)))) (h "07vf47k0f8712hjj219as1rbrrxm1dzrxqbi2aj16gn5xy3hi2sq")))

(define-public crate-sbi-rt-0.0.1 (c (n "sbi-rt") (v "0.0.1") (d (list (d (n "sbi-spec") (r "^0.0.3") (d #t) (k 0)))) (h "137yb98r88sj8x4xl1l00l576kvnv6p3nw6d4y03yxsd7c2pzwh7")))

(define-public crate-sbi-rt-0.0.2 (c (n "sbi-rt") (v "0.0.2") (d (list (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "1ds27aj9aip0h9xp6rmb05h8v2s8xlj35x013qaarf0x559kq4cc") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3-rc.1 (c (n "sbi-rt") (v "0.0.3-rc.1") (d (list (d (n "sbi-spec") (r "^0.0.5") (d #t) (k 0)))) (h "10s6q1anf2ar29g6sqjri28p9ivaxyk2qmd9j8r2vr59wx5gjd24") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3-rc.2 (c (n "sbi-rt") (v "0.0.3-rc.2") (d (list (d (n "sbi-spec") (r "^0.0.6") (d #t) (k 0)))) (h "0yp3xn8vi91wfgf5pcbjibjdxm4d33g8ynqzi3mqs6aghzq146ym") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3-rc.3 (c (n "sbi-rt") (v "0.0.3-rc.3") (d (list (d (n "sbi-spec") (r "^0.0.7-alpha.2") (d #t) (k 0)))) (h "0fp39wrrg540mj3bg3d1iv5bzl572k5r3f345v9il260w9m70qfn") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3-rc.4 (c (n "sbi-rt") (v "0.0.3-rc.4") (d (list (d (n "sbi-spec") (r "^0.0.7-alpha.3") (d #t) (k 0)))) (h "0ws91y1z1x6gkam0nr3czqxwbx4vyw2r9i9hidzh501kc09ifzw5") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3-rc.5 (c (n "sbi-rt") (v "0.0.3-rc.5") (d (list (d (n "sbi-spec") (r "^0.0.7-alpha.3") (d #t) (k 0)))) (h "1dnbvkamxxwb2r5nhsk58nwimbnv6bd35pid9w4wr8amja6irz8n") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (c (n "sbi-rt") (v "0.0.3") (d (list (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "1l4blnbd8lhdj1v0adk5nhlfk0h44sr8km764v263p7fw6dsdfkz") (f (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

