(define-module (crates-io sb i- sbi-spec) #:use-module (crates-io))

(define-public crate-sbi-spec-0.0.1 (c (n "sbi-spec") (v "0.0.1") (h "1vlw504hg1m1nwq841jf152iqj3mg67jqpj49q4ph31nwpsanllh")))

(define-public crate-sbi-spec-0.0.2 (c (n "sbi-spec") (v "0.0.2") (h "1kpwcwpkmk6bp69mk5zwxk2zn15hc2aflkqdb6qdxg6p0wxpsqbv")))

(define-public crate-sbi-spec-0.0.3 (c (n "sbi-spec") (v "0.0.3") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0z2wp80j1g2wshs18ydafrqgqz8djpdbsx7wm5z5i9jjk1f1lcxb")))

(define-public crate-sbi-spec-0.0.4 (c (n "sbi-spec") (v "0.0.4") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0xsk89zj533k4h79ygmpdklspib5c6z87qn01zysk4dmkg7jfh3d") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5-rc.1 (c (n "sbi-spec") (v "0.0.5-rc.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1kb829s790jzcls8bikbrii7dwsna50v1kzjsxm3p83y12i90fpd") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5-rc.2 (c (n "sbi-spec") (v "0.0.5-rc.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0bfplrmvhjsa58zacf5396vg6lawf7v82i2y15z4wlf8blmzx5f3") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5 (c (n "sbi-spec") (v "0.0.5") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0wnwybi08cq7sygjydfms1gd7yk1gvizfscmacha05smdpqv2dd5") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.6 (c (n "sbi-spec") (v "0.0.6") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0952zkflhlx0w1sfy70i38nb8nb2s3p2pjcarxh6na4ivk1jdgry") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7-alpha.1 (c (n "sbi-spec") (v "0.0.7-alpha.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1p4fwzgnrm9yfzsy3fq51ahis2cz1mf1f2c2m5qk47fnc51bps18") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7-alpha.2 (c (n "sbi-spec") (v "0.0.7-alpha.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0arr8834aci6h2qldj1awr1fpwb467fx3axm1gga90ba3z1fnhc0") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7-alpha.3 (c (n "sbi-spec") (v "0.0.7-alpha.3") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lg9pv3i9lyx1hif5jm2h4icvyk9s4h7z8mhghxva8x590693468") (f (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7 (c (n "sbi-spec") (v "0.0.7") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "146qb0isw8l5nz8vjp2q6jna9arb823m3indiv811p2xzc967qz6") (f (quote (("legacy") ("default"))))))

