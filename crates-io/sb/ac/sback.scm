(define-module (crates-io sb ac sback) #:use-module (crates-io))

(define-public crate-sback-0.1.0 (c (n "sback") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "1g41ws63cqw5gx8w5diinna8nz1ydqd5y4dw6bcpym6gws0q9il6")))

(define-public crate-sback-0.2.0 (c (n "sback") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "0s4x5apymwdi16czq5mxgy59pd6yxinx3g10mizyscfnjzv68xrb")))

(define-public crate-sback-0.3.0 (c (n "sback") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "10ry757sv6czx9v5hxaj8lnmih9ghkna9nz5qk78n4ybv13yyvpz")))

