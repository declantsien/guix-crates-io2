(define-module (crates-io sb yt sbyte) #:use-module (crates-io))

(define-public crate-sbyte-0.1.0 (c (n "sbyte") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.0") (d #t) (k 0)))) (h "18nhp4bdz03n845c7mhibiki2hlzvbsdrfd0wz8vvxahzs5c83s7") (y #t)))

(define-public crate-sbyte-0.1.1 (c (n "sbyte") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.9") (d #t) (k 0)))) (h "1kc10py0v7k66j3ya6a9v8wdy6i3fdlxx16l8nmwrjvls1zxn3z8") (y #t)))

(define-public crate-sbyte-0.1.2 (c (n "sbyte") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.10") (d #t) (k 0)))) (h "0q9g9k4l0vk4q1m8skxmwfpa1xbjy6l9xqkkbs0a0s9wa5zxnq6l") (y #t)))

(define-public crate-sbyte-0.1.3 (c (n "sbyte") (v "0.1.3") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.10") (d #t) (k 0)))) (h "0zvqv59d1sqb0z5ma2xk0ci6zkpk972cshydcwbn6pifx5vmx7b0") (y #t)))

(define-public crate-sbyte-0.1.4 (c (n "sbyte") (v "0.1.4") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.10") (d #t) (k 0)))) (h "16b0476k0f0m9553v87v4qxc9szpsa4p95lv61bx59jh9gdch16p") (y #t)))

(define-public crate-sbyte-0.2.0 (c (n "sbyte") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.13") (d #t) (k 0)))) (h "0fdsmgmbhz44pz3digbnriim41rbwfv76mhasywqbpxxl1ancgp1") (y #t)))

(define-public crate-sbyte-0.2.4 (c (n "sbyte") (v "0.2.4") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.13") (d #t) (k 0)))) (h "09igx6s7bi958dc6p22ij5mf1ghzhg4yicd0h9cj09ibk0pb43f5") (y #t)))

(define-public crate-sbyte-0.2.5 (c (n "sbyte") (v "0.2.5") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.13") (d #t) (k 0)))) (h "14ijs8wii6rdjzcnz98jdp8z0z1z78glz2sj005szxn07p9hjxvq") (y #t)))

(define-public crate-sbyte-0.2.6 (c (n "sbyte") (v "0.2.6") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.13") (d #t) (k 0)))) (h "0sdzv79jxzl55l15y0wgq3fbma9xa5l5cyhfhapqjyil0fzlnfc6") (y #t)))

(define-public crate-sbyte-0.2.7 (c (n "sbyte") (v "0.2.7") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.18") (d #t) (k 0)))) (h "14a7zxxh9s0566zw2jznssp39681lmfxzvcrq0agnnkzvbra2s2p") (y #t)))

(define-public crate-sbyte-0.2.8 (c (n "sbyte") (v "0.2.8") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.18") (d #t) (k 0)))) (h "0zcza7ldpc26j3r2vkf1n6kq71gdb4wl6fd9k8vpg89h4ga3ilrp") (y #t)))

(define-public crate-sbyte-0.2.10 (c (n "sbyte") (v "0.2.10") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.18") (d #t) (k 0)))) (h "1cd2aa4rkk836rhhipacxm1hxvfyr2pqwisxikxdkhl0gpmh9d67") (y #t)))

(define-public crate-sbyte-0.2.12 (c (n "sbyte") (v "0.2.12") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.18") (d #t) (k 0)))) (h "1sg3v5ibl3h0z8f0gawzfh7zgh6g07gqsb7bbmhi2f3bi2fqg7cp") (y #t)))

(define-public crate-sbyte-0.2.13 (c (n "sbyte") (v "0.2.13") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.18") (d #t) (k 0)))) (h "0f7j14z81jlz78fr7a2lb03d9rxl0q6hgl8s841d4xgld1hz0607") (y #t)))

(define-public crate-sbyte-0.2.14 (c (n "sbyte") (v "0.2.14") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.20") (d #t) (k 0)))) (h "0q8rnv9mhsg746ayrdgxr4sidwg5k509l9abcdi91pwlqsicw02i") (y #t)))

(define-public crate-sbyte-0.2.15 (c (n "sbyte") (v "0.2.15") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.21") (d #t) (k 0)))) (h "1akrp8k42jxafsfi64cnjp2lhi506vqavbs9rggfvqjg4rk94kxl") (y #t)))

(define-public crate-sbyte-0.2.16 (c (n "sbyte") (v "0.2.16") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^0.1.24") (d #t) (k 0)))) (h "0csssr7h3cvqs1jpz6cl9bg2y2p6xr5k79m5zdrjaw4v7xvfzllf") (y #t)))

(define-public crate-sbyte-0.2.17 (c (n "sbyte") (v "0.2.17") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.0") (d #t) (k 0)))) (h "1pgzw17zbwmr2iidkjs65ys7waz1jnczjyw40vmfhk6ckvvq4nkv") (y #t)))

(define-public crate-sbyte-0.2.18 (c (n "sbyte") (v "0.2.18") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.0") (d #t) (k 0)))) (h "1vrk1757ahay3hxmi9zf5r3qh0cmhra7x42z5m921fs4xfp6k76y") (y #t)))

(define-public crate-sbyte-0.2.19 (c (n "sbyte") (v "0.2.19") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.4") (d #t) (k 0)))) (h "0dvmp6v2195x23dir6m5a7hh7khpzjq7py3f554cya5ig8iryh05") (y #t)))

(define-public crate-sbyte-0.2.20 (c (n "sbyte") (v "0.2.20") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.5") (d #t) (k 0)))) (h "07f4fn0vbhhjaygh7k4n04jbz4j6ybn4zgm81ldps503jfv63cvj") (y #t)))

(define-public crate-sbyte-0.2.21 (c (n "sbyte") (v "0.2.21") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "08y38q3cg9yb2dj9xihmnahsxwcrng7g9ayhdfskkvbml61i3b98") (y #t)))

(define-public crate-sbyte-0.2.22 (c (n "sbyte") (v "0.2.22") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1syrd2hdzf1q62sj7031xgq8hsgdkc3kdr3hz63iz4v2m94kika6") (y #t)))

(define-public crate-sbyte-0.2.23 (c (n "sbyte") (v "0.2.23") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1wxfk1cn39ik3fsrk3i9mh3bl70k2x0yd3vbs8mc0kj7r4y6jm8z") (y #t)))

(define-public crate-sbyte-0.2.24 (c (n "sbyte") (v "0.2.24") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1gv5yfzv5q053rsz25cfm29smjr0hf5vkacaxhyh39hm09ndmyjs") (y #t)))

(define-public crate-sbyte-0.2.25 (c (n "sbyte") (v "0.2.25") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "0fhxwby1xk7574wqyq9pvk64qjwxa9w8ab8hf1b65rfq52vg4r1d") (y #t)))

(define-public crate-sbyte-0.2.26 (c (n "sbyte") (v "0.2.26") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "0j012dq5gkb7lj12m01g53ds0wil84hjkkgmg1n9jrfj091lh3zj") (y #t)))

(define-public crate-sbyte-0.2.27 (c (n "sbyte") (v "0.2.27") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "122x0vs492v3v0d780i6z223csa9pfcxcbcapw68n9lzq1s4jhn0") (y #t)))

(define-public crate-sbyte-0.2.28 (c (n "sbyte") (v "0.2.28") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "11jwbxy7kswa9x29s3dlas1wmkww5465nx1lc6grivlcq8v4hay4") (y #t)))

(define-public crate-sbyte-0.2.29 (c (n "sbyte") (v "0.2.29") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "18c8lzqjpaifgq344zqw112fhbraddb8vd0sdq5a7m1ly26zy5bb") (y #t)))

(define-public crate-sbyte-0.2.30 (c (n "sbyte") (v "0.2.30") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1pr2d2qvb67wm795xnbdk89wvmabwkchdmnfv9caqwagcpqscixl") (y #t)))

(define-public crate-sbyte-0.2.31 (c (n "sbyte") (v "0.2.31") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1dyd9d281dng5bkn54akah3ivkz8gkl2pmgsw9137nxmfnmysn32") (y #t)))

(define-public crate-sbyte-0.2.32 (c (n "sbyte") (v "0.2.32") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.0.7") (d #t) (k 0)))) (h "1fswm678ymkf5gqkpmj51asc2qqzyaamxn00naidby51kkqgync4")))

(define-public crate-sbyte-0.3.0 (c (n "sbyte") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "wrecked") (r "^1.1.0") (d #t) (k 0)))) (h "11x7bxm8fy27aavaisvilifb6rpl9zw7nips5qrwc0f7yasd3lm8")))

(define-public crate-sbyte-0.3.1 (c (n "sbyte") (v "0.3.1") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wrecked") (r "^1.1.0") (d #t) (k 0)))) (h "0qw6sh226acpa2sgd80vg9sqi444ha9z1x0kjk2j4f27lgrx79k6")))

(define-public crate-sbyte-0.3.2 (c (n "sbyte") (v "0.3.2") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wrecked") (r "^1.1.0") (d #t) (k 0)))) (h "0xfjs81n024gphqi9y2ra95phnpnmskh9ncsfmq7xl3cn8ryhsx0")))

(define-public crate-sbyte-0.3.3 (c (n "sbyte") (v "0.3.3") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wrecked") (r "^1.1.0") (d #t) (k 0)))) (h "1l4bnnpwbljl9rc2by071cfbyfmayr00hbf6rvc0mdzdj5h7h35n")))

