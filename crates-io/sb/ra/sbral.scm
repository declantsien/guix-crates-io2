(define-module (crates-io sb ra sbral) #:use-module (crates-io))

(define-public crate-sbral-0.1.0 (c (n "sbral") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dogged") (r "^0.2") (d #t) (k 2)) (d (n "im-rc") (r "^13") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.1") (d #t) (k 2)) (d (n "rpds") (r "^0.6") (d #t) (k 2)))) (h "1k6vap1k6yi7mh1wr4b2dgwg6skgri6gw5l1hl88w34s27wixfza") (f (quote (("threadsafe"))))))

