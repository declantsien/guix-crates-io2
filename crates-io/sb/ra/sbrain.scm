(define-module (crates-io sb ra sbrain) #:use-module (crates-io))

(define-public crate-sbrain-0.2.1 (c (n "sbrain") (v "0.2.1") (h "0qhydqqpis3z7qi7qnp9lpyq5cjbcnsvjz4xfyxv2i5c8lzd203a")))

(define-public crate-sbrain-0.3.0 (c (n "sbrain") (v "0.3.0") (h "0jx3zvffralibix9fgvpwkf7dh2ypk7xafvjpnf7rf63rs4mn0aa")))

(define-public crate-sbrain-0.3.1 (c (n "sbrain") (v "0.3.1") (h "197pr836sy8lw33fsbqpj9rw2q3w1rknxypjm8z2dafhqqx60jnk") (y #t)))

(define-public crate-sbrain-0.3.2 (c (n "sbrain") (v "0.3.2") (h "0r28716va0azp8zvlx9d156xigwgmgcvmpmkhindyyvgsa4cj4dr")))

(define-public crate-sbrain-0.4.1 (c (n "sbrain") (v "0.4.1") (h "01ypi1gwygmk0lrrmpdz5kmic35bz3jmlzps6s49whql7m4w1krv")))

(define-public crate-sbrain-0.4.2 (c (n "sbrain") (v "0.4.2") (h "1ssidzil03hacp5p2lm6cc77yr0hryzd7vjbwnxl90r54apj14x7")))

(define-public crate-sbrain-0.5.0 (c (n "sbrain") (v "0.5.0") (h "065pkih9b10n3mlwcs0f7fyrhdfkic6i7yq71xn4ccjh17w3mq7i")))

(define-public crate-sbrain-0.99.0 (c (n "sbrain") (v "0.99.0") (h "1jb48vp7454whhvz6f3shil0asx45q6cdfyp94pgv5rmrfjcjrs2")))

