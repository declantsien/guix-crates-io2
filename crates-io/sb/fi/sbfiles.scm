(define-module (crates-io sb fi sbfiles) #:use-module (crates-io))

(define-public crate-sbfiles-0.1.0 (c (n "sbfiles") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "gut") (r "^0.4.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1z51n34z5f6lrmd107hvhbvx5lz8ia6qgdm7nd0cb1y42054490c")))

