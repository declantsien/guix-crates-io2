(define-module (crates-io sb v2 sbv2-near) #:use-module (crates-io))

(define-public crate-sbv2-near-0.1.0 (c (n "sbv2-near") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "near-contract-standards") (r "^4.0.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.0.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "subtle") (r "^2.2.1") (k 0)) (d (n "superslice") (r "^1") (d #t) (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1xbswn5jpvb0jgrv4gzs2s8hk2rns4pwmfqa24ql00v3rvpwp5js") (f (quote (("testnet") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

