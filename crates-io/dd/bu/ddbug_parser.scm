(define-module (crates-io dd bu ddbug_parser) #:use-module (crates-io))

(define-public crate-ddbug_parser-0.3.0 (c (n "ddbug_parser") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.23") (d #t) (k 0)) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "0ahddnzkcqnyx89rfg30yxb4rpkniq5zszhc29pv4qhj9v0shwgm") (f (quote (("default"))))))

