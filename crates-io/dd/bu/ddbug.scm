(define-module (crates-io dd bu ddbug) #:use-module (crates-io))

(define-public crate-ddbug-0.1.0 (c (n "ddbug") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "gimli") (r "^0.14.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.10") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "memmap") (r "^0.4.0") (d #t) (k 0)) (d (n "pdb") (r "^0.1.5") (d #t) (k 0)))) (h "04gas2zs3v2kjrwc3sz5g81wm48nn0bqg4i5wan9cb6lb5gmpr6g")))

(define-public crate-ddbug-0.2.0 (c (n "ddbug") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "gimli") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "object") (r "^0.6") (d #t) (k 0)) (d (n "pdb") (r "^0.2") (d #t) (k 0)))) (h "0gnmdrzhwxvbh4ikp5hcc0fkc0wdzqs14n07pr9a2cc3nc9f58w6")))

(define-public crate-ddbug-0.3.0 (c (n "ddbug") (v "0.3.0") (d (list (d (n "capstone") (r "^0.7") (d #t) (k 0)) (d (n "capstone-sys") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "parser") (r "^0.3.0") (d #t) (k 0) (p "ddbug_parser")) (d (n "typed-arena") (r "^2") (d #t) (k 0)))) (h "10b074c04w1mhc3hxmbi17s825gdq17w336czjmcc923cpxccrhn") (f (quote (("system_alloc") ("default"))))))

