(define-module (crates-io dd f- ddf-blocking-http-client) #:use-module (crates-io))

(define-public crate-ddf-blocking-http-client-0.0.1 (c (n "ddf-blocking-http-client") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1gwip4krzp2xrf15awk43wrpzchqgz4lx5cvsfza6d047iw4hxy5")))

(define-public crate-ddf-blocking-http-client-0.0.2 (c (n "ddf-blocking-http-client") (v "0.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1jri5b9if17zr2ymkpzwvz4x5hmbq0x8435i5k0bqarkjdi86bvm")))

(define-public crate-ddf-blocking-http-client-0.0.3 (c (n "ddf-blocking-http-client") (v "0.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1b03cz890vjqn4ch0wavz3361hxd9h59y9szhr78h20my7rij8kx")))

