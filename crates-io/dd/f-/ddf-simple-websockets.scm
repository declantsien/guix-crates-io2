(define-module (crates-io dd f- ddf-simple-websockets) #:use-module (crates-io))

(define-public crate-ddf-simple-websockets-0.1.4 (c (n "ddf-simple-websockets") (v "0.1.4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.17") (d #t) (k 0)))) (h "1frd4q15yv15kkxlmny7xkj9v1cxgpicdpnkayrcir5r46579cbp")))

