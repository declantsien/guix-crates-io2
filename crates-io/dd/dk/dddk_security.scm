(define-module (crates-io dd dk dddk_security) #:use-module (crates-io))

(define-public crate-dddk_security-0.1.0 (c (n "dddk_security") (v "0.1.0") (d (list (d (n "dddk_core") (r "^0.1.0") (d #t) (k 0)))) (h "11dh1p5x871zfisq6vhmbhchqga9g32fpj5mxphnb90bn3lrrv5h") (y #t)))

(define-public crate-dddk_security-0.1.1 (c (n "dddk_security") (v "0.1.1") (d (list (d (n "dddk_core") (r "^0.1.0") (d #t) (k 0)))) (h "0520j06ljvqv82m7mr60x9gbirbd2snn32fg5fgi7gflhrl3aqrf")))

(define-public crate-dddk_security-0.2.0 (c (n "dddk_security") (v "0.2.0") (d (list (d (n "dddk_core") (r "^0.2.0") (d #t) (k 0)) (d (n "dddk_macro") (r "^0.2.0") (d #t) (k 2)))) (h "180pg26593rr9if9wigkabklhy6zf0yln8qpwrbq6sgaf01f26k8")))

(define-public crate-dddk_security-0.3.0 (c (n "dddk_security") (v "0.3.0") (d (list (d (n "dddk_core") (r "^0.3.0") (d #t) (k 0)) (d (n "dddk_macro") (r "^0.3.0") (d #t) (k 2)))) (h "00pdj4s0pph44v6aqqlr5fb3sbinxkkhcjmrvzgknkrfld0xj3z4")))

(define-public crate-dddk_security-0.4.0 (c (n "dddk_security") (v "0.4.0") (d (list (d (n "dddk_core") (r "^0.4.0") (d #t) (k 0)) (d (n "dddk_macro") (r "^0.4.0") (d #t) (k 2)))) (h "0sjir3zbfr3x5ya9vssivzmk6fc26fm6vzw07wjra7pkiznvn3cy")))

(define-public crate-dddk_security-0.6.0 (c (n "dddk_security") (v "0.6.0") (d (list (d (n "dddk_core") (r "^0.6.0") (d #t) (k 0)) (d (n "dddk_macro") (r "^0.6.0") (d #t) (k 2)))) (h "0bf0draij9d96dbwkj2d8ggxywji26as0nszvxqd83dq5qx2n4k7")))

