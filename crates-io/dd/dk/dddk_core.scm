(define-module (crates-io dd dk dddk_core) #:use-module (crates-io))

(define-public crate-dddk_core-0.1.0 (c (n "dddk_core") (v "0.1.0") (h "03dbqmkbffgcqvgsg0044rpbw6mqfjcrf3z1cqw6g8si76l380fk")))

(define-public crate-dddk_core-0.2.0 (c (n "dddk_core") (v "0.2.0") (d (list (d (n "dddk_macro") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bghz482lbrj0ska359sc5r6ck4z5xp8ph827mwxcvq8hqhimkdb")))

(define-public crate-dddk_core-0.3.0 (c (n "dddk_core") (v "0.3.0") (d (list (d (n "dddk_macro") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l0v643y21w374kx2jdzwj4yf10n4y622s7yvwb1g7lacf3isvli")))

(define-public crate-dddk_core-0.4.0 (c (n "dddk_core") (v "0.4.0") (d (list (d (n "dddk_macro") (r "^0.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x9v3gy83xcx877sr2nwb4mj5n7qvfpigj6kyn4fgpya7iladc7r")))

(define-public crate-dddk_core-0.6.0 (c (n "dddk_core") (v "0.6.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "12a0ix17gizlgfqabdlkw4xhicj9swnkvrmrbpbfg0f3wfmbh5in")))

