(define-module (crates-io dd dk dddk_macro) #:use-module (crates-io))

(define-public crate-dddk_macro-0.1.0 (c (n "dddk_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13wjk159r9vkpl2y2n86f49g4qbaq96k1ahml5jgrlijrqv0hwzm")))

(define-public crate-dddk_macro-0.2.0 (c (n "dddk_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14y6knp1fq7qx981168nc99glv6hv39y384gahf1kqfm8h28bm8n")))

(define-public crate-dddk_macro-0.3.0 (c (n "dddk_macro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y2rnv9138zn95pv1m9330jirhb5x35smn471jjd2kvhls3gczfk")))

(define-public crate-dddk_macro-0.4.0 (c (n "dddk_macro") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xnqlpn4b4hzxwv4dzlad1ha1ivx2aha045pd0f1iqgmp00k6xx8")))

(define-public crate-dddk_macro-0.5.0 (c (n "dddk_macro") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13pmb18gsyf4xbhq451v8wqkzbnwlbk2brbln5hmmvps3jg70gfs")))

(define-public crate-dddk_macro-0.6.0 (c (n "dddk_macro") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "063ik8hm132rp9brx1y4mjbm185jbzlrn75qhv4ypw40q43dyfvy")))

