(define-module (crates-io dd _m dd_maths_traits_macros) #:use-module (crates-io))

(define-public crate-dd_maths_traits_macros-0.1.0 (c (n "dd_maths_traits_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "1gzkmihxdzibsb3qninxhfpcqzbb0sh2i93r3ff36068qyfhhgci")))

