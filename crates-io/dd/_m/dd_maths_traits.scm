(define-module (crates-io dd _m dd_maths_traits) #:use-module (crates-io))

(define-public crate-dd_maths_traits-0.1.0 (c (n "dd_maths_traits") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "15jlfsr0r2v7gi3f4q66p2dmwk7012xlvvfcz66gz57s9mlgypqj") (f (quote (("std") ("default" "num-bigint"))))))

(define-public crate-dd_maths_traits-0.1.1 (c (n "dd_maths_traits") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1m95paki47gnaz5wmmlna9pn4x7x004rxhclcinwynkc92i85a0i") (f (quote (("std") ("default" "num-bigint"))))))

(define-public crate-dd_maths_traits-0.1.2 (c (n "dd_maths_traits") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0d2bk1f1wd1bc8905l67x7dr8895rcvzrknad50xlj1qxjk6rsmd") (f (quote (("std") ("default" "num-bigint"))))))

(define-public crate-dd_maths_traits-0.1.3 (c (n "dd_maths_traits") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0wm6cxmpdw91xzd98dak5vz36qhxsm1gn6y0fg4qy8i5nx6i0ras") (f (quote (("std") ("default" "num-bigint"))))))

(define-public crate-dd_maths_traits-0.1.4 (c (n "dd_maths_traits") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "022kwm7qwls09353sf76xcn7xwg7v8k8czii4pz8v3apvg99jl0l") (f (quote (("std") ("default" "num-bigint"))))))

(define-public crate-dd_maths_traits-0.1.5 (c (n "dd_maths_traits") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "043hwpabhaqp2v2j0ycq5hckm4rrqymvmg64ldagzq0rcrs33ywz") (f (quote (("std") ("nightly" "adt_const_params" "negative_impls" "const_trait_impl" "decl_macro") ("negative_impls") ("decl_macro") ("const_trait_impl") ("bigint" "num-bigint") ("all" "std" "bigint" "nightly") ("adt_const_params"))))))

(define-public crate-dd_maths_traits-0.1.6 (c (n "dd_maths_traits") (v "0.1.6") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0whcm8nhc5r379rp2vknim0dcwff036hz1a67kvb5gzk7k1fszxg") (f (quote (("std") ("nightly" "adt_const_params" "negative_impls" "const_trait_impl" "decl_macro") ("negative_impls") ("decl_macro") ("const_trait_impl") ("bigint" "num-bigint") ("all" "std" "bigint" "nightly") ("adt_const_params"))))))

(define-public crate-dd_maths_traits-0.1.7 (c (n "dd_maths_traits") (v "0.1.7") (d (list (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0y5dcfpi8zhc0p22a6yjcr8v204g7c4fc1ix9bd7xjgslmhv9dgg") (f (quote (("std") ("nightly" "adt_const_params" "negative_impls" "const_trait_impl" "decl_macro") ("negative_impls") ("decl_macro") ("const_trait_impl") ("bigint" "num-bigint") ("all" "std" "bigint" "nightly") ("adt_const_params"))))))

(define-public crate-dd_maths_traits-0.1.8 (c (n "dd_maths_traits") (v "0.1.8") (d (list (d (n "dd_maths_traits_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0p89zjfkxldsasa866pvid41h9y75wx3zyjh9m76cwmzk2mgin4b") (f (quote (("std") ("nightly" "adt_const_params" "negative_impls" "const_trait_impl" "decl_macro" "const_ops") ("negative_impls") ("decl_macro") ("const_trait_impl") ("const_ops") ("bigint" "num-bigint") ("all" "std" "bigint" "nightly") ("adt_const_params"))))))

