(define-module (crates-io dd aa ddaa_protocol) #:use-module (crates-io))

(define-public crate-ddaa_protocol-0.1.0 (c (n "ddaa_protocol") (v "0.1.0") (h "1r6c0fzhri42z777v9bamwsi0wycqqnzd1k6fp9w5r3xfhvji0sp")))

(define-public crate-ddaa_protocol-0.2.0 (c (n "ddaa_protocol") (v "0.2.0") (h "0ja9nv66wrmnscaf40py4xyvxm989671i80771qjwcpxqcwxx8l6")))

