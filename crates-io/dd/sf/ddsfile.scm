(define-module (crates-io dd sf ddsfile) #:use-module (crates-io))

(define-public crate-ddsfile-0.2.0 (c (n "ddsfile") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "10ymkijxvwbcxjkkylil1blhgpzdmzpig1rzdbjj4blfw8aj9cq3")))

(define-public crate-ddsfile-0.2.1 (c (n "ddsfile") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1x8j92iwkfawmqdi6kgwr1hqvsmy6a9mq7p3l9xm4aw3y3mry2m9")))

(define-public crate-ddsfile-0.2.2 (c (n "ddsfile") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "19wmyhzf5mjyyix4xfvpdf2ybdfm8kr3wdsmhvpgr3avhhyv2ryy")))

(define-public crate-ddsfile-0.2.3 (c (n "ddsfile") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "139a398ka1f0l3893zama7yiqai4qvwpsd1bgwip9ziql8ivig1y")))

(define-public crate-ddsfile-0.4.0 (c (n "ddsfile") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)))) (h "1ffalziivn52hn5xc0czz0317b2nm6sz5x5n32227d25cklmgwrl")))

(define-public crate-ddsfile-0.5.0 (c (n "ddsfile") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)))) (h "1w1cz3iciss77bg7la2lx9wxypx4lb2bw7z0v455mf6mfdwz1hws")))

(define-public crate-ddsfile-0.5.1 (c (n "ddsfile") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)))) (h "11rf0j02xp7nc8wpq74m1q1zxhazjxbc794dvrfxnh1ggjbcskjr")))

(define-public crate-ddsfile-0.5.2 (c (n "ddsfile") (v "0.5.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1i36igx6mmrr8xs14b1z72l2vhy4kml6jyxcqsb9xaipcwggx7a7")))

