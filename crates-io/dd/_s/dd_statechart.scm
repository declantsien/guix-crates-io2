(define-module (crates-io dd _s dd_statechart) #:use-module (crates-io))

(define-public crate-dd_statechart-0.3.2 (c (n "dd_statechart") (v "0.3.2") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mdh4p5fp5kfhkc12wwzgcvr3441gzrfp4271m0bpqyl69fzv90c")))

(define-public crate-dd_statechart-0.4.0 (c (n "dd_statechart") (v "0.4.0") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0j0ylwsxyx2qihpc82ysx9y9zskxkqa79clgvhpsamlmrq8wyi54")))

(define-public crate-dd_statechart-0.4.1 (c (n "dd_statechart") (v "0.4.1") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0y0crck50f3aragy059z3vvbsyyns784msriv7hy9zzm40slcpbw")))

(define-public crate-dd_statechart-0.7.0 (c (n "dd_statechart") (v "0.7.0") (d (list (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1r0kjsawswwswj2sfd7np40bw1cxflpmc2x8l5gwjvnpmnyj6r9i")))

