(define-module (crates-io dd mw ddmw-types) #:use-module (crates-io))

(define-public crate-ddmw-types-0.1.0 (c (n "ddmw-types") (v "0.1.0") (h "1k7izv66wkyb5hcd1dbscj25m9fiyifvp3jmyjnd8x3kjjfbyxp2")))

(define-public crate-ddmw-types-0.1.1 (c (n "ddmw-types") (v "0.1.1") (h "0pfbf1xca8788qz2kw8waam5vcszy7f16gg7bacar4fwycbia29a")))

