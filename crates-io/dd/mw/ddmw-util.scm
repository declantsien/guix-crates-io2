(define-module (crates-io dd mw ddmw-util) #:use-module (crates-io))

(define-public crate-ddmw-util-0.1.0 (c (n "ddmw-util") (v "0.1.0") (h "10yvz612854fqqn1d69h6ac6p0c993jlms01lbfrxg7fb7c79grx")))

(define-public crate-ddmw-util-0.1.1 (c (n "ddmw-util") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "17xsf7zvymcny6z95klqp5sajrvw9i2xrgprfcv2pfr4p0by2gxw")))

(define-public crate-ddmw-util-0.1.2 (c (n "ddmw-util") (v "0.1.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "043syhpyrczrql8r3wmq3y2sr6dq1dshlps0zzxpcvqcbnll0wqp")))

(define-public crate-ddmw-util-0.1.3 (c (n "ddmw-util") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0mfz1yzcpnhfr8g6vr1mhxl77463jd89kwwi2022c8y4q4dh27xv")))

(define-public crate-ddmw-util-0.1.4 (c (n "ddmw-util") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1d1jmdjndisa1s3sj4j1kgk99z2yh8gca418pbb0isayjs0l7gl4")))

(define-public crate-ddmw-util-0.2.0 (c (n "ddmw-util") (v "0.2.0") (d (list (d (n "figment") (r "^0.10") (f (quote ("toml"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vassk2fd8j1n9wcfd67bwiv9ffq228y9f5dx3hm6lk75fng2fh7")))

