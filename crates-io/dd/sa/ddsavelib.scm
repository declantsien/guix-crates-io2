(define-module (crates-io dd sa ddsavelib) #:use-module (crates-io))

(define-public crate-ddsavelib-0.1.0 (c (n "ddsavelib") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "itoa") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.0") (o #t) (k 0)))) (h "06p1hpn3wvw1zjppphz4rl3mi0wdbi6mr5z4fjbyp85phhhp4fd9") (f (quote (("default" "string_cache"))))))

(define-public crate-ddsavelib-0.1.1 (c (n "ddsavelib") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "dtoa") (r "^0.4.5") (d #t) (k 0)) (d (n "itoa") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.0") (o #t) (k 0)))) (h "0236cwc35bcyfxnsdyhg3m0q2vnmnx3vdmclqhwh343hnznadrbk") (f (quote (("default" "string_cache"))))))

