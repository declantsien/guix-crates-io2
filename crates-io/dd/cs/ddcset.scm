(define-module (crates-io dd cs ddcset) #:use-module (crates-io))

(define-public crate-ddcset-0.0.1 (c (n "ddcset") (v "0.0.1") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "ddc-hi") (r "~0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1.1") (d #t) (k 0)) (d (n "result") (r "^1.0.0") (d #t) (k 0)))) (h "17mm6md0sil7mr5m6z2gfbc11frqll4850windr2mg9dy7iwlaca")))

(define-public crate-ddcset-0.0.2 (c (n "ddcset") (v "0.0.2") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "ddc-hi") (r "~0.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1.1") (d #t) (k 0)) (d (n "result") (r "^1.0.0") (d #t) (k 0)))) (h "038w74byq75dqpr4fg84zd7vjih85g7b9vrxmjaxgv8fs2jh866s")))

(define-public crate-ddcset-0.0.3 (c (n "ddcset") (v "0.0.3") (d (list (d (n "clap") (r "^2.31.2") (k 0)) (d (n "ddc-hi") (r "~0.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1.1") (d #t) (k 0)) (d (n "result") (r "^1.0.0") (d #t) (k 0)))) (h "0wmg6znkqq0m1ssy12zad01kzrhkgw43rkavixjqipaf9klcns37")))

(define-public crate-ddcset-0.0.4 (c (n "ddcset") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (k 0)) (d (n "ddc-hi") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1.1") (d #t) (k 0)))) (h "1hb4kn0iksjmxgwgm8pff1cyaw7kphy3qq11lz5rxr0fyna6h9yr")))

(define-public crate-ddcset-0.0.5 (c (n "ddcset") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "ddc-hi") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1") (d #t) (k 0)))) (h "1isyw13c730k4zy8nk0kbzxdv2hd9skr9hr7nchw8f34yzakaq07")))

