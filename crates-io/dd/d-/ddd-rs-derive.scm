(define-module (crates-io dd d- ddd-rs-derive) #:use-module (crates-io))

(define-public crate-ddd-rs-derive-0.1.0 (c (n "ddd-rs-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i3d765p4r2p9181h5lbx3040q07l4ql8h27qdn9rl5s75fwrzzp")))

(define-public crate-ddd-rs-derive-0.1.1 (c (n "ddd-rs-derive") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1wpgaazy4cg96s8pyng0c46aiv7r4m0ay8jzx14f8kw5l5s6ig31") (y #t)))

(define-public crate-ddd-rs-derive-0.1.2 (c (n "ddd-rs-derive") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "15z4f0s2p2b6f3ggcq2x19ysny714w15p8cdvc02pw6gslnlngpv")))

(define-public crate-ddd-rs-derive-0.1.3 (c (n "ddd-rs-derive") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0c383icmqi6byw2k82ah92n1ygvrdb758mbrycgbg7834dfivl4b")))

(define-public crate-ddd-rs-derive-0.1.4 (c (n "ddd-rs-derive") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1g60r0akfpcaccw8rj4hc56l545bbc42dlvdxkfg4yp9zfhg28kp")))

(define-public crate-ddd-rs-derive-0.1.5 (c (n "ddd-rs-derive") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "159yxmq9pf0b1ag0nh0j30xq43xwlvfrgfrl376c8vqb9n5f74sa")))

(define-public crate-ddd-rs-derive-0.1.6 (c (n "ddd-rs-derive") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0yrmn0pzphhp03mj7fka17y4k0dcykx70ga8vh1p2amd7158h7sm")))

(define-public crate-ddd-rs-derive-0.1.7 (c (n "ddd-rs-derive") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0ay3wjkbxl3h4lkbnid57zblys1305sny48hsgs9j50gcq6j0mbz")))

(define-public crate-ddd-rs-derive-0.1.8 (c (n "ddd-rs-derive") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1c52qzlnmaw7lfjqsx6s9cfx9sykhxhlmxldk8mnshcljm7f2ncp")))

(define-public crate-ddd-rs-derive-0.1.9 (c (n "ddd-rs-derive") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0rwv7vlknyslrrnszj4sgd9h41g89wq4xi2lbb8xls1s9g2pzv8k")))

(define-public crate-ddd-rs-derive-0.1.10 (c (n "ddd-rs-derive") (v "0.1.10") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0cih8irgy8ssdklknz9ycbpx3p0g46wsln6w2fdzv08czk0q7kvx")))

(define-public crate-ddd-rs-derive-0.2.0 (c (n "ddd-rs-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1c2kp0cf3i58gjwzx6df1p8cq2d78668f2s2mmzb0xiby7j91b7k")))

(define-public crate-ddd-rs-derive-0.2.1 (c (n "ddd-rs-derive") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "15d6hfavvxhy6csxvw7rrd8cvywlm8hhrr23r20cmgfjb4jf3jlb")))

(define-public crate-ddd-rs-derive-0.2.2 (c (n "ddd-rs-derive") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "178ccgvc0v7a79asavjxj5gf9vnrmzijamfyg7imd0hi3zhjbm3h")))

(define-public crate-ddd-rs-derive-0.3.0 (c (n "ddd-rs-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rm7xp4cp0s2c6il58sbya20w7n1gfbap3vy2xblz5fp6qwsdyma")))

(define-public crate-ddd-rs-derive-0.4.0 (c (n "ddd-rs-derive") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "00v33hfl6iqv24ic0h9kha1xmp5yfcbr3ln8xvcp9gx4nmmhimv9") (y #t)))

(define-public crate-ddd-rs-derive-0.4.1 (c (n "ddd-rs-derive") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "17shi5ca37zn1m8v3lpigqbng3s9dpa9fdxnh3r7dqax2z7kq0hz") (y #t)))

(define-public crate-ddd-rs-derive-0.4.2 (c (n "ddd-rs-derive") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "17sxpf6jcy64abbdiss7mvf2y1s1bv2vfxsplwrcrirdlw96bjkb") (y #t)))

(define-public crate-ddd-rs-derive-0.4.3 (c (n "ddd-rs-derive") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "ddd-rs") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1j8f1csgpvqhxlf8xz4ac1q70z3qr5c2h0g381ndqbcrizsclcpz")))

(define-public crate-ddd-rs-derive-1.0.0 (c (n "ddd-rs-derive") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0ib5ivxrqd5xr0vbz1w2x05k6hcxsyb8fi4dx6nnxk5aza4sq0ff")))

(define-public crate-ddd-rs-derive-1.1.0 (c (n "ddd-rs-derive") (v "1.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0rvza9rsc0c83lma6rgzz49p6ldyvmkdbd08y80lac44czlyf5h0")))

