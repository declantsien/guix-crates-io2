(define-module (crates-io dd tr ddtri) #:use-module (crates-io))

(define-public crate-ddtri-0.1.0 (c (n "ddtri") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "022chq5wqcc3w5640ghvzi89riq5r76vr0giz4v29bi3xxm8q3bc") (y #t)))

(define-public crate-ddtri-0.2.0 (c (n "ddtri") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1gn944k014rw05anmkv6f7il4prmyxd3jp9573nv2h6wij9my0w4") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ddtri-0.2.1 (c (n "ddtri") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1576qrbhrg98gi0p6a9c1nidgys4yhyvnrqb4nr9wh2jw5ddf480") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ddtri-0.3.0 (c (n "ddtri") (v "0.3.0") (h "11sbrp8d21kg9z2lyln8lcq4mf77hwsq6nd7qgw8iq9d31nf3q07")))

(define-public crate-ddtri-0.3.1 (c (n "ddtri") (v "0.3.1") (h "1a9jz4fwbkrp2yrsj92vfzyir95vgc5cp5a1hbqbn95f4kdz39kb")))

