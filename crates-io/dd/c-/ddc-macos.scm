(define-module (crates-io dd c- ddc-macos) #:use-module (crates-io))

(define-public crate-ddc-macos-0.1.0 (c (n "ddc-macos") (v "0.1.0") (d (list (d (n "IOKit-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.1") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "ddc") (r "^0.2.0") (d #t) (k 0)) (d (n "mach") (r "^0.3.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1my8yl8r3yjb9dp8vkf939z15yflcvx441ss66alvslv18ihxslw")))

(define-public crate-ddc-macos-0.2.0 (c (n "ddc-macos") (v "0.2.0") (d (list (d (n "core-foundation") (r "^0.9.1") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "ddc") (r "^0.2.0") (d #t) (k 0)) (d (n "io-kit-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "mach") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1slx57l8yd9fak8n51fpzmrqn5mlvw282v45m06w7kqkq4bg7fjw")))

(define-public crate-ddc-macos-0.2.1 (c (n "ddc-macos") (v "0.2.1") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "core-graphics") (r "^0.23") (d #t) (k 0)) (d (n "ddc") (r "^0.2") (d #t) (k 0)) (d (n "edid-rs") (r "^0.1") (d #t) (k 2)) (d (n "io-kit-sys") (r "^0.4") (d #t) (k 0)) (d (n "mach") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jxksak59l4y1nll76v6r6xz61s462948rxlb5qx7jvnl0hwa11x")))

