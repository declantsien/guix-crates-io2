(define-module (crates-io dd c- ddc-winapi) #:use-module (crates-io))

(define-public crate-ddc-winapi-0.1.0 (c (n "ddc-winapi") (v "0.1.0") (d (list (d (n "ddc") (r "^0.1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winuser"))) (d #t) (k 0)))) (h "1pjw3na0gchapz04p4gmk9s3rcg054kvhzsha6vq1dc1lln68921")))

(define-public crate-ddc-winapi-0.2.0 (c (n "ddc-winapi") (v "0.2.0") (d (list (d (n "ddc") (r "^0.2.0") (d #t) (k 0)) (d (n "widestring") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winuser"))) (d #t) (k 0)))) (h "17pwswh6k3yjjlww9vpal9jclbr0i6k4d5ajvqvf4w68cldyff1j")))

(define-public crate-ddc-winapi-0.2.1 (c (n "ddc-winapi") (v "0.2.1") (d (list (d (n "ddc") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "minwindef" "winuser" "physicalmonitorenumerationapi" "lowlevelmonitorconfigurationapi"))) (d #t) (k 0)))) (h "0gwlzjsa7nm6flf9fz6nr0z10vfs4h1pcxr906799shlv3bg0p81")))

