(define-module (crates-io dd c- ddc-node-rs) #:use-module (crates-io))

(define-public crate-ddc-node-rs-2.0.0 (c (n "ddc-node-rs") (v "2.0.0") (d (list (d (n "ddc") (r "^0.2.2") (d #t) (k 0)) (d (n "ddc-hi") (r "^0.4.1") (d #t) (k 0)) (d (n "mccs-db") (r "^0.1.2") (d #t) (k 0)) (d (n "neon") (r "^0.10.1") (f (quote ("napi-latest"))) (k 0)))) (h "1w5bib2akh9m2g77q2zhfn8g498acbn3p3dy205z7qcd1pg37dyp")))

