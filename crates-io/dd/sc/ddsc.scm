(define-module (crates-io dd sc ddsc) #:use-module (crates-io))

(define-public crate-ddsc-0.1.1 (c (n "ddsc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "libddsc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1r0hvq8q1cn70szwnz3p96hqpndydnjmg8jyinwq035swlryppnv")))

(define-public crate-ddsc-0.1.2 (c (n "ddsc") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "libddsc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0rs9lip466n3anyswkm9wrmmj4flj213wkxm0qi1fvsh9c12x9rr")))

