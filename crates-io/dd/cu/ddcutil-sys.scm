(define-module (crates-io dd cu ddcutil-sys) #:use-module (crates-io))

(define-public crate-ddcutil-sys-0.0.1 (c (n "ddcutil-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0kw2fpv6nfnnfvrwh6gsl8ny4p57xvygiraialg2pkgs7my8zbqj")))

(define-public crate-ddcutil-sys-0.0.2 (c (n "ddcutil-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1zmska5bm57fwyr40j0niilmx5c51ik7m8j13mf4dfdk3zv2a1aa")))

(define-public crate-ddcutil-sys-0.0.3 (c (n "ddcutil-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1rlqh3rrflzdrrc0sgdfbaf51586j1ln13x1jbg1a5d3rnp55nx7")))

