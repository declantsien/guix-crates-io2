(define-module (crates-io dd cu ddcutil) #:use-module (crates-io))

(define-public crate-ddcutil-0.0.1 (c (n "ddcutil") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "ddcutil-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "1i2ahcbdvbsf5xdhs57q9j7crw5a4lr7zapc5qbx75g9k5m06a4d")))

(define-public crate-ddcutil-0.0.2 (c (n "ddcutil") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "ddcutil-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "00q1isgs610zirc46sfw5bmcjvvcxczmn092xqq2pwj2vdxyj05c")))

(define-public crate-ddcutil-0.0.3 (c (n "ddcutil") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "ddcutil-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0zynvbrf0kw4zlmgjvr5f0sgakshrnv61vyx2cjpaa2f002r2vzm")))

