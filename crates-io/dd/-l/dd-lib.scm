(define-module (crates-io dd -l dd-lib) #:use-module (crates-io))

(define-public crate-dd-lib-0.1.0 (c (n "dd-lib") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "12gpsd7kziwdmvvakk3pw9rai4bh10i4kh7lrwka5mnnwhl9av57")))

(define-public crate-dd-lib-0.1.1 (c (n "dd-lib") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "08yh210d9yg2j0845sb3cndxgr5nglw7zxza0fshabl56qbmj5jw")))

(define-public crate-dd-lib-0.2.0 (c (n "dd-lib") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0a5lmdimw6qlv5h10k0f8dhyw9nxpny4sy3942p1f2v968bh5sqp")))

(define-public crate-dd-lib-0.2.1 (c (n "dd-lib") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "encoding8") (r "^0.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0mk058pliv0xdyjahh824gsv3xk39gk6mj6dl1kgiqnbkj0wlrm8")))

