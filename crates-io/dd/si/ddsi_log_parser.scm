(define-module (crates-io dd si ddsi_log_parser) #:use-module (crates-io))

(define-public crate-ddsi_log_parser-0.1.0 (c (n "ddsi_log_parser") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01z63inmw9b197dss27mdi54n488d8v8sfyfisiq4r8nwp26p3gn")))

(define-public crate-ddsi_log_parser-0.1.1 (c (n "ddsi_log_parser") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pjyfh8077v5dgblyjym5xa54r1drv7x9mdb07ck24y8fmbv08k9")))

(define-public crate-ddsi_log_parser-0.1.2 (c (n "ddsi_log_parser") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gmwj7lrnaz277fv5kb7zis81rnpvs7dy2gx6z83jd16ggsjala3")))

