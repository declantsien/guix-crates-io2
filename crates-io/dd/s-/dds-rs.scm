(define-module (crates-io dd s- dds-rs) #:use-module (crates-io))

(define-public crate-dds-rs-0.1.0 (c (n "dds-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)))) (h "1dfv720819x5akgbry6v2firj163gdwpx55mlxdx0vkdyi2axkkh")))

(define-public crate-dds-rs-0.1.1 (c (n "dds-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)))) (h "13mcixfv0dm0xs3lc1nyb8kxn2wngxc3xqq9k2i87fryvcbb2kaj")))

(define-public crate-dds-rs-0.2.0 (c (n "dds-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)))) (h "1pxvlqa8qvlna2mdd1r197vn41dnvws355bpz7kf183y9aci6qq6")))

(define-public crate-dds-rs-0.3.0 (c (n "dds-rs") (v "0.3.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)))) (h "1bcz267ya6fc8c766s0a30na1mc5w1ci9yfm4ci1s9r9mvlrgfam")))

(define-public crate-dds-rs-0.4.0 (c (n "dds-rs") (v "0.4.0") (d (list (d (n "clap") (r "^2.20.1") (d #t) (k 2)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 2)))) (h "035bags58cvdsnabzp9ik2akcj5qfnf3xwmbqfq3kmb82zrajdy2")))

(define-public crate-dds-rs-0.5.0 (c (n "dds-rs") (v "0.5.0") (d (list (d (n "bincode") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "clap") (r "^2.20.1") (d #t) (k 2)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 0)))) (h "0wylyz9x6afgdh21z8qdslz6siz3q5izy520j6b7zwx7lq7jjp4b")))

(define-public crate-dds-rs-0.5.1 (c (n "dds-rs") (v "0.5.1") (d (list (d (n "bincode") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "clap") (r "^2.20.1") (d #t) (k 2)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 0)))) (h "09p836ip1iidwmw4r1i7a3xgp77riqgf36ij3iw6w6yyz4y02d1b")))

(define-public crate-dds-rs-0.5.2 (c (n "dds-rs") (v "0.5.2") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.20.1") (d #t) (k 2)) (d (n "image") (r "^0.12.2") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1n6h1g6db3k2glskj2dz4dq562rbhlr0jrg15gpdvs4zb4q2cnys")))

(define-public crate-dds-rs-0.6.0 (c (n "dds-rs") (v "0.6.0") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1bzdan6ygvpdlq6gfpil5cj72ljqkv8ia5w2gyq41bzffcjmw1kw")))

(define-public crate-dds-rs-0.7.0 (c (n "dds-rs") (v "0.7.0") (d (list (d (n "bincode") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "18amz957gj0p6a2qfgvnalbzzlk3bk0g3ymmn50hcxp998p3yaac")))

