(define-module (crates-io dd s- dds-bridge) #:use-module (crates-io))

(define-public crate-dds-bridge-0.1.0 (c (n "dds-bridge") (v "0.1.0") (d (list (d (n "dds-bridge-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1f1zdc29sjql0g219xc7lmmr1dcpg2pb6769yzsy1kpgs5kwcg0s")))

(define-public crate-dds-bridge-0.1.1 (c (n "dds-bridge") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "dds-bridge-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xgcb45ingh5asixk9nnfnrgsw7kvyfdkf45br5j813l8v32k1za")))

(define-public crate-dds-bridge-0.2.0 (c (n "dds-bridge") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "dds-bridge-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vai91kwsrsz470mlvaz03qhm18yi8i64b6jzvmp1qpfs5by9h4l")))

(define-public crate-dds-bridge-0.3.0 (c (n "dds-bridge") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "dds-bridge-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1h746wrp4pz2ahwmnamvg79yy8xmbnz2hga9cr6w5m6gdp6a152p")))

