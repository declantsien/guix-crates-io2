(define-module (crates-io dd s- dds-bridge-sys) #:use-module (crates-io))

(define-public crate-dds-bridge-sys-0.1.0 (c (n "dds-bridge-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1abbacwrd83dn8sql5y3a9yydvqs7hxda6g2zha1pl0ri6i31620")))

(define-public crate-dds-bridge-sys-0.1.1 (c (n "dds-bridge-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "1d6y2q671ahc682aprllnvmjm21lsqrndcprp7laim0rzxq4n25z")))

(define-public crate-dds-bridge-sys-0.1.2 (c (n "dds-bridge-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "openmp-sys") (r "^1.2.3") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "1pzbi0m99mmgaznc6kcivg7jk7zd1lqsvd8xi5rc9sbk8jqfj3ww")))

(define-public crate-dds-bridge-sys-0.1.3 (c (n "dds-bridge-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "13nbm6ngpg3bclygirza78bkib54v1970wxca0kyiwzbz5p5vzib") (f (quote (("openmp") ("default" "openmp"))))))

(define-public crate-dds-bridge-sys-1.0.0 (c (n "dds-bridge-sys") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "16jq28d6iwrszd7rnxh4fk50qyj8ybnpjwiq9cf53gakpp7czf8y") (f (quote (("openmp") ("default" "openmp"))))))

(define-public crate-dds-bridge-sys-1.0.1 (c (n "dds-bridge-sys") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "1ci9mamj24x8sq0y2giz8krk1d7hpc8kfdi9bg4h1rznhnbkpadc") (f (quote (("openmp") ("default" "openmp"))))))

(define-public crate-dds-bridge-sys-1.1.0 (c (n "dds-bridge-sys") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.98") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "0ldv3fl40jgg1kgqybn8r284kxkngsnadlzh9x65rcafkislldhf") (f (quote (("openmp") ("default" "openmp"))))))

