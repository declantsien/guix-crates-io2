(define-module (crates-io dd lc ddlc_helper) #:use-module (crates-io))

(define-public crate-ddlc_helper-1.0.0 (c (n "ddlc_helper") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "09rsmv3kqjw37ci9s8hvj9vszg9h2z7sd1fb4djsbm7ywa8ss7nc") (y #t)))

(define-public crate-ddlc_helper-1.0.1 (c (n "ddlc_helper") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "18s2nbh991iy5s94py9bx81z5bla26qisyg950lxmdd4kpasf424") (y #t)))

(define-public crate-ddlc_helper-1.0.2 (c (n "ddlc_helper") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "10bpmkwwbs1rlvzh720p0jsn9a0qvyapc9xjfv7y0kyhfiaw07qs") (y #t)))

(define-public crate-ddlc_helper-1.0.3 (c (n "ddlc_helper") (v "1.0.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0q05q0hbm5wp3ah36a83gfxgnbx72qhvlzxcrd7vqb1404vwndd2") (y #t)))

(define-public crate-ddlc_helper-1.1.0 (c (n "ddlc_helper") (v "1.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "00bky4n4x2fz9lhgfvph18mimla4l9ki2k3syx7hb6zm920mysnq") (y #t)))

(define-public crate-ddlc_helper-1.1.1 (c (n "ddlc_helper") (v "1.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1bq2vznn4kscd9ijwwg75yhpyyfjp28rlnpnig77q215g9x8xp0a") (y #t)))

(define-public crate-ddlc_helper-1.2.0 (c (n "ddlc_helper") (v "1.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1ijyvfn9z34zrrv6mlvfxaglnsxjhs9rqa2gqsz1pxa4q9ma0qcb") (y #t)))

(define-public crate-ddlc_helper-1.2.1 (c (n "ddlc_helper") (v "1.2.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "01zd4w41sn6crc63bjh5cxg8bfk4ciw6ilq0mz5ljdzm4p5x8bm4")))

(define-public crate-ddlc_helper-1.2.2 (c (n "ddlc_helper") (v "1.2.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0swrmmh4vkjivhsaa1q7wlp0qjppm0v4piwjidgkbl2wz0sz3ngc")))

(define-public crate-ddlc_helper-1.2.3 (c (n "ddlc_helper") (v "1.2.3") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1nl0ilh84664rh6qcyhq5mznizybximip6lidy45hbppzdh72l1z")))

(define-public crate-ddlc_helper-1.2.4 (c (n "ddlc_helper") (v "1.2.4") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0f50pb3p9ibv46zz0dmhybgg8gfbwxnfff5h9rpp4nlm5p0ksj4a")))

(define-public crate-ddlc_helper-1.2.5 (c (n "ddlc_helper") (v "1.2.5") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0wxyhbac6nxi7jg56bs0zf5lmrjas7xg8a2xj9pgpf6jrfjyfh08")))

(define-public crate-ddlc_helper-1.2.6 (c (n "ddlc_helper") (v "1.2.6") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0xa9m4f7fs3syqql2j0cmssy7j5jqblcf9lq9axf2ifwiiw5ivkg")))

(define-public crate-ddlc_helper-1.3.0 (c (n "ddlc_helper") (v "1.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "14y30wdjl3774ibl7mibiv15nspb1xb7kanycv6di7vg3x8brgz9") (y #t)))

(define-public crate-ddlc_helper-2.0.0 (c (n "ddlc_helper") (v "2.0.0") (h "09y27ly8cdzz7r5wf09b272nanc8vs9x0vd7bsibjdf6g7piy8hx") (y #t)))

(define-public crate-ddlc_helper-2.0.1 (c (n "ddlc_helper") (v "2.0.1") (h "1vc7inc41h9nbk8nxn48rwzq03gk8x693h2kasdrqz8czp4vp30k")))

(define-public crate-ddlc_helper-2.0.2 (c (n "ddlc_helper") (v "2.0.2") (h "1qg5idg45p5halsw26rmmzyqjxiqarjjncggn542v824rzy47kqa")))

(define-public crate-ddlc_helper-2.0.3 (c (n "ddlc_helper") (v "2.0.3") (h "1w02wz0085bdkjvbvjc77s1cbfqcw0wic1gqjw0qjp9ggi0zfr16")))

(define-public crate-ddlc_helper-2.0.4 (c (n "ddlc_helper") (v "2.0.4") (h "0agdv4jmiqn7ax4m8iy93gm83ifnv5bsycdwffvm2757nn24kv62")))

