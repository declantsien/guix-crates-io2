(define-module (crates-io dd ir ddir) #:use-module (crates-io))

(define-public crate-ddir-0.0.1 (c (n "ddir") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1n6cypdipy2zps50qma5fvjglw281nz92jw3q1vava2b6sdfn27x")))

(define-public crate-ddir-0.0.2 (c (n "ddir") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1k030mzl85xg3pp88agrj9n1m7lzrkvc64wymw70cqmam2z5xxfg")))

