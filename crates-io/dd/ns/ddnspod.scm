(define-module (crates-io dd ns ddnspod) #:use-module (crates-io))

(define-public crate-ddnspod-0.1.2 (c (n "ddnspod") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "1h110ij5pkvdgy1apcvfncjck1ya52ngrymp9xs6p9n7qany8ggf")))

(define-public crate-ddnspod-0.1.3 (c (n "ddnspod") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "1x6hbwbq53dk0h3mgc560kpr4k5r1grnsdfq3ly3d7fmkz96pykd")))

