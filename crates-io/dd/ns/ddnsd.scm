(define-module (crates-io dd ns ddnsd) #:use-module (crates-io))

(define-public crate-ddnsd-0.1.0 (c (n "ddnsd") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hgrz3p0ri9ngj5l1q828r1sxhh6xq2p4k80kz0g2nbs1ih2md06")))

