(define-module (crates-io dd if ddiff) #:use-module (crates-io))

(define-public crate-ddiff-0.1.0 (c (n "ddiff") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tabwriter") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14zs4ygkxs000x9gmzxdr7s7qhi7jw9xfdgjjpc17wkkdgkj4l7p")))

