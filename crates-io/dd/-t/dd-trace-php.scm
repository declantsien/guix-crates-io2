(define-module (crates-io dd -t dd-trace-php) #:use-module (crates-io))

(define-public crate-dd-trace-php-0.1.0 (c (n "dd-trace-php") (v "0.1.0") (h "0md3la612r4m7ka6dajns9kygzchn79rr1lar7h9h5i00nv1rmxa") (y #t)))

(define-public crate-dd-trace-php-0.0.0 (c (n "dd-trace-php") (v "0.0.0") (h "148zfb5s4fna2l0mm5mfrxygsxr0mrrkzcp00z4m8kl4zd6wwv47")))

