(define-module (crates-io dd w_ ddw_utils) #:use-module (crates-io))

(define-public crate-ddw_utils-0.1.0 (c (n "ddw_utils") (v "0.1.0") (h "0j2qf3lyy3inalyaql16yh00y56f0j6v3q3ivs0ln4xjcj94jsh5")))

(define-public crate-ddw_utils-0.1.1 (c (n "ddw_utils") (v "0.1.1") (h "0s7859v36a1nvqcc6wi8mykbk8ssn26zd2906ac6qjwhs5sx79pi")))

(define-public crate-ddw_utils-0.1.2 (c (n "ddw_utils") (v "0.1.2") (h "145siw4c6kjripxxr9qdknm9ayllqhawh4j9gchj2ln7n6ifw03q")))

