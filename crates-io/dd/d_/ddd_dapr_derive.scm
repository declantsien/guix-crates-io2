(define-module (crates-io dd d_ ddd_dapr_derive) #:use-module (crates-io))

(define-public crate-ddd_dapr_derive-0.1.0 (c (n "ddd_dapr_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1pw9c2qsal70ivnsmds7db152iaamxi94hyjdnj2m0k3lmnwxpbk")))

(define-public crate-ddd_dapr_derive-0.1.1 (c (n "ddd_dapr_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0i82zz4fy3jn9jdq44fabszfwzqd27z07lcg6rkvsr8kdgp1s6vr")))

(define-public crate-ddd_dapr_derive-0.1.2 (c (n "ddd_dapr_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "02040bd438wrrxmpahs8gh4zl1kvvdhid2n42m79q8xmjc4yb0bw")))

(define-public crate-ddd_dapr_derive-0.1.4 (c (n "ddd_dapr_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1aqbq29zgzw99gjyhwry7habrhsr13v57ydyh7ydmcnx7awfy15z")))

(define-public crate-ddd_dapr_derive-0.1.5 (c (n "ddd_dapr_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "00nihi6j7ddvmb56ryb2wjra55fl8a4hib84bhx9ixvybzvd5isv") (f (quote (("debug_mode"))))))

(define-public crate-ddd_dapr_derive-0.1.6 (c (n "ddd_dapr_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "17faiidd3m2wd3c5i18lkl7mmql784adjdcxwv2dp630p0xppkqi") (f (quote (("debug_mode"))))))

(define-public crate-ddd_dapr_derive-1.0.0 (c (n "ddd_dapr_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1g463vy0y9xsm2ns0is554rsg0ijcnlcpvv11kpa4yvylhxkn58b") (f (quote (("debug_mode"))))))

(define-public crate-ddd_dapr_derive-1.0.1 (c (n "ddd_dapr_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0fpz0bzjj9qb2m3y5aa3skbg3pzk81m5hkyyxpam1abiak7w47gw") (f (quote (("debug_mode"))))))

