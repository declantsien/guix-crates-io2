(define-module (crates-io dd p- ddp-connection) #:use-module (crates-io))

(define-public crate-ddp-connection-0.1.0 (c (n "ddp-connection") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0j901z6kd09sxa9s50pr431sc79cjajah64ayz6c2pggfpxwwkm0")))

