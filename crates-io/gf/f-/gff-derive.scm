(define-module (crates-io gf f- gff-derive) #:use-module (crates-io))

(define-public crate-gff-derive-0.1.0 (c (n "gff-derive") (v "0.1.0") (d (list (d (n "gff") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "1y62kk3vra9vhs5wdh2vfhppilfmifb04091ar6nhpxnxych3k19")))

