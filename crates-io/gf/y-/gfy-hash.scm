(define-module (crates-io gf y- gfy-hash) #:use-module (crates-io))

(define-public crate-gfy-hash-0.1.0 (c (n "gfy-hash") (v "0.1.0") (d (list (d (n "crc") (r "^1.8") (d #t) (k 0)))) (h "0rssh2kjn6342dc0ffmgzxyhnvf22vn47kwv1k6881basf3rmkkr")))

(define-public crate-gfy-hash-0.3.0 (c (n "gfy-hash") (v "0.3.0") (d (list (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "1ppxagvxjfwn4067jqqv3j28h7vvv1xdb6jsp7d202my7bvqrknc") (f (quote (("list_builtin") ("default" "list_builtin"))))))

