(define-module (crates-io gf x_ gfx_core) #:use-module (crates-io))

(define-public crate-gfx_core-0.1.0 (c (n "gfx_core") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "draw_state") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0qrpr495zgjd2w9n0a2p8jfc4ivf45h3g3vjlaj98pvhzz4ajzdh") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.1.1 (c (n "gfx_core") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "draw_state") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "12w7rp7nb4ici87akmfg4mbw508yz47igdwr8agy7g10d1szvshm") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.1.2 (c (n "gfx_core") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "draw_state") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0nn44caf58sjwa3hgmjkj5s13iwcfd1rwxwlj56rv9a94l5dvn86") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.2.0 (c (n "gfx_core") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "draw_state") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0jl18453aqwvw148f5ymp31rcw2d3syv7ikxf6qslcm58h76kqqm") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.2.1 (c (n "gfx_core") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.4") (d #t) (k 0)) (d (n "draw_state") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1qv3l3vb09vdz8dnw77rwzqlgn18mbh1p2n45kmwk9253dn1pj8d") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.3.0 (c (n "gfx_core") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "19n5kfls149a8y1y76pyg1v3q9h9bpmdf8bcglgqr0wyx14q89fj") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.3.1 (c (n "gfx_core") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "11hax140d5p07iyxzz2dfyfcr6f9w51v29rmg1yq3kn1yl84h7zx") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.4.0 (c (n "gfx_core") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "14zwhvass01l7z4cak3x9ivpaqp89m0iafpibblnbvxf9pzaxraq") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.5.0 (c (n "gfx_core") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "17cf94cd6gqaswmpdylpafb1smni5pkbfaxjn626r3vpr0nihash") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.5.1 (c (n "gfx_core") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "184nmfss4znvs6d2077cv78wvhdjha2xziz5flszkdnkx9y54jh2") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.6.0 (c (n "gfx_core") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0scddnn8jicivv0am369ipv6n56wvva74nm4mdmy07zg7x0pgqr6") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.6.1 (c (n "gfx_core") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "10ysd2wf95a36zvxvpl8k49jvm9v6m6l87n4af40imkw7zvp24kh") (f (quote (("unstable"))))))

(define-public crate-gfx_core-0.7.0 (c (n "gfx_core") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gdxzgpviiq18jcpfzvkmq5ixd8hnignd3a6y477indrxxpqliz6") (f (quote (("unstable") ("serialize" "serde" "serde_derive" "draw_state/serialize"))))))

(define-public crate-gfx_core-0.7.1 (c (n "gfx_core") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0220as6y4a45x8yjjbhskfy2vlgs5y4v96ap81ikx837y8v2fzi5") (f (quote (("unstable") ("serialize" "serde" "serde_derive" "draw_state/serialize") ("cgmath-types" "cgmath"))))))

(define-public crate-gfx_core-0.7.2 (c (n "gfx_core") (v "0.7.2") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "16rch83b1hvjfcdpkw19xf7prk8vqa1k5csvbmc30p1v3ir9pfff") (f (quote (("unstable") ("serialize" "serde" "serde_derive" "draw_state/serialize") ("cgmath-types" "cgmath"))))))

(define-public crate-gfx_core-0.8.0 (c (n "gfx_core") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "12ydmdnkpw2y0fw4svv3lh7z1q7n6mpni0d2b50by9hr9k97c18k") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

(define-public crate-gfx_core-0.8.1 (c (n "gfx_core") (v "0.8.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1y2qndpx514pc8agmcsw6lazwkx7l6pg40mnpm8scnsap3kqpala") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

(define-public crate-gfx_core-0.8.2 (c (n "gfx_core") (v "0.8.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0rv3zicfxrxfcnmjj0b5d7nrr0rqc63qfrwffbp8yd50pnvkjl6q") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

(define-public crate-gfx_core-0.8.3 (c (n "gfx_core") (v "0.8.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1bih32skldwlp8mq2x0263g7sw0k2m3lf6nsqdb0kwk1gs1k4jf7") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

(define-public crate-gfx_core-0.9.0 (c (n "gfx_core") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1b34rynn7g8gr5l4scm1rrb7gr4swddhc68mf9spcw28h91pp6jm") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde")))) (y #t)))

(define-public crate-gfx_core-0.9.1 (c (n "gfx_core") (v "0.9.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0vqpj9bnxiq5s3f9wr7iwj6mkzcy87sa1n4i024cqkwx5aq2f4af") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

(define-public crate-gfx_core-0.9.2 (c (n "gfx_core") (v "0.9.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0haldr99n12d90vqgvl77n59hywlklhdff85j2aljaz1yapdvyvm") (f (quote (("unstable") ("serialize" "serde" "draw_state/serde"))))))

