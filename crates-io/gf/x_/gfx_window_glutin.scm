(define-module (crates-io gf x_ gfx_window_glutin) #:use-module (crates-io))

(define-public crate-gfx_window_glutin-0.0.1 (c (n "gfx_window_glutin") (v "0.0.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glutin") (r "*") (d #t) (k 0)))) (h "0q2wm7xlz5p61xibc3azhpxb62b1br2xfryyvlwicy3l90vc8rf4")))

(define-public crate-gfx_window_glutin-0.1.0 (c (n "gfx_window_glutin") (v "0.1.0") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glutin") (r "*") (d #t) (k 0)))) (h "1pl0glixyi40f92gcibjzj8p0cjzgvs8jy0ikfi1lzxz24jq9c3n")))

(define-public crate-gfx_window_glutin-0.1.1 (c (n "gfx_window_glutin") (v "0.1.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glutin") (r "*") (d #t) (k 0)))) (h "0g0pkim2bjp9j0iqydy5kp74cxc5lfkvrbdpwkr5kji68cvjdmfd")))

(define-public crate-gfx_window_glutin-0.2.0 (c (n "gfx_window_glutin") (v "0.2.0") (d (list (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glutin") (r "0.1.*") (d #t) (k 0)))) (h "0lsrri02wqad62hadr0rdadm2vr89qqfsvz67bn2vwksk501wk9b")))

(define-public crate-gfx_window_glutin-0.3.0 (c (n "gfx_window_glutin") (v "0.3.0") (d (list (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glutin") (r "^0.2") (d #t) (k 0)))) (h "0fxzp87f6r8fhrs74f7qmkd9w5qjkflfggn0d8i2nhsgbxjd2hyp")))

(define-public crate-gfx_window_glutin-0.4.0 (c (n "gfx_window_glutin") (v "0.4.0") (d (list (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glutin") (r "^0.3") (d #t) (k 0)))) (h "1dwa8li6rjc8z65vf2jmxh5ar0h9y0gdmjq0jp9706gx36j3dsj2")))

(define-public crate-gfx_window_glutin-0.5.0 (c (n "gfx_window_glutin") (v "0.5.0") (d (list (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glutin") (r "^0.3") (d #t) (k 0)))) (h "072l3vw4mhyhisilq4p04hxajznf91nxk9fmcc9zw9crl2l1z7ky")))

(define-public crate-gfx_window_glutin-0.6.0 (c (n "gfx_window_glutin") (v "0.6.0") (d (list (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glutin") (r "^0.4") (d #t) (k 0)))) (h "0fr001987hghw02wscznb91x67xq0bnxyy9wl2dzap5gjm9mclz5")))

(define-public crate-gfx_window_glutin-0.7.0 (c (n "gfx_window_glutin") (v "0.7.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.6") (d #t) (k 0)) (d (n "glutin") (r "^0.4") (d #t) (k 0)))) (h "0ixix443zqiyvibngf0y2dp5vxqbmzamw30pm84ya39nykdhbwg5")))

(define-public crate-gfx_window_glutin-0.8.0 (c (n "gfx_window_glutin") (v "0.8.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.7") (d #t) (k 0)) (d (n "glutin") (r ">= 0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cbk62gcxhwwhnj2sxl3p6q09mlc180p5d4q0dk03s50y7g60wv9")))

(define-public crate-gfx_window_glutin-0.9.0 (c (n "gfx_window_glutin") (v "0.9.0") (d (list (d (n "gfx") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.8") (d #t) (k 0)) (d (n "glutin") (r ">= 0.4.2") (d #t) (k 0)))) (h "119hrcma8rr98b7xlpmkm4rv1lgqh7f19jz3k1wvafp0ykz8791x")))

(define-public crate-gfx_window_glutin-0.9.1 (c (n "gfx_window_glutin") (v "0.9.1") (d (list (d (n "gfx_core") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.8.1") (d #t) (k 0)) (d (n "glutin") (r "^0.4.8") (d #t) (k 0)))) (h "035c1li84gpxps0nzl881dbpapfd32hb498cfgxxzqhzqx07jfyv")))

(define-public crate-gfx_window_glutin-0.10.0 (c (n "gfx_window_glutin") (v "0.10.0") (d (list (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.9") (d #t) (k 0)) (d (n "glutin") (r "^0.4.8") (d #t) (k 0)))) (h "0x6x72i341r05f8cjcb80bx8sf34704wwid68xg076yni39z1lqx")))

(define-public crate-gfx_window_glutin-0.11.0 (c (n "gfx_window_glutin") (v "0.11.0") (d (list (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.10") (d #t) (k 0)) (d (n "glutin") (r "^0.5") (d #t) (k 0)))) (h "07bx45jvlpp1djia784zcc22918l8ny0h9spjgsapf9q5g52fm12")))

(define-public crate-gfx_window_glutin-0.12.0 (c (n "gfx_window_glutin") (v "0.12.0") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.11") (d #t) (k 0)) (d (n "glutin") (r "^0.6") (d #t) (k 0)))) (h "0mj4pgwdbpd7sw4fpzwb3khn32mx8ajggqbsbr0vv8xg4gf6mq3f")))

(define-public crate-gfx_window_glutin-0.13.1 (c (n "gfx_window_glutin") (v "0.13.1") (d (list (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.12") (d #t) (k 0)) (d (n "glutin") (r "^0.7") (d #t) (k 0)))) (h "18bmpg33yfkhvd4gi2j1dwj615rc575n5ys29ryxwans1hp1wrfv")))

(define-public crate-gfx_window_glutin-0.14.0 (c (n "gfx_window_glutin") (v "0.14.0") (d (list (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.13") (d #t) (k 0)) (d (n "glutin") (r "^0.7") (d #t) (k 0)))) (h "1wikql0qga7fwl17fw5h4xabhrdnp7jh0sxm66n76qbbh0l4md77")))

(define-public crate-gfx_window_glutin-0.15.0 (c (n "gfx_window_glutin") (v "0.15.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.7") (d #t) (k 0)))) (h "0qrkwkqsjsa41n7b8pi0yr19qsqcdzy0kb9zsy0lb4k5l5nsk8p1")))

(define-public crate-gfx_window_glutin-0.16.0 (c (n "gfx_window_glutin") (v "0.16.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.8") (d #t) (k 0)))) (h "1v1qmbcqg83g235gp5mfn7adn48msfckvhq83ppmj86q8is434h6")))

(define-public crate-gfx_window_glutin-0.17.0 (c (n "gfx_window_glutin") (v "0.17.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.9") (d #t) (k 0)))) (h "0n8lq1pkrayq5lzpcjd2cb1qiygf8d0r958yrpx2dj7m61p8v2fv")))

(define-public crate-gfx_window_glutin-0.18.0 (c (n "gfx_window_glutin") (v "0.18.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.10") (d #t) (k 0)))) (h "1xlnqc27g48b5yczv9i8m8k0c3xy42x5k3m6a9y3zkd4bfk8s1i3")))

(define-public crate-gfx_window_glutin-0.19.0 (c (n "gfx_window_glutin") (v "0.19.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.11") (d #t) (k 0)))) (h "0a9x68s1gd9hp2w67d934nyw7dgpg6pph0044sagqza023b6mm37")))

(define-public crate-gfx_window_glutin-0.20.0 (c (n "gfx_window_glutin") (v "0.20.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.12") (d #t) (k 0)))) (h "0ayd191sd3msncmcwhwgk2giw39iwhra13wdwlfmzkj9b2lwgbkk") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.21.0 (c (n "gfx_window_glutin") (v "0.21.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.13") (d #t) (k 0)))) (h "1qfr8c0jxris6mr1ki2gkihq8qplr86jxc63pdmdc3p6np5isb97") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.22.0 (c (n "gfx_window_glutin") (v "0.22.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.14") (d #t) (k 0)))) (h "0r9zf49mdycx3hqhcmmsrhl2ivf0flzfpl2nibxmxcnm8bq3zi93") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.23.0 (c (n "gfx_window_glutin") (v "0.23.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.15") (d #t) (k 0)))) (h "08c94cn6wkxg8kvrygzj4kqf8gwz1s3qpmzy1xq2a1h2jm7b6yjy") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.24.0 (c (n "gfx_window_glutin") (v "0.24.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.16") (d #t) (k 0)))) (h "1zza0q4i7fdpbiqqgabqgvv65p5z6xfnq9z98vfxfkw4466fybn9") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.25.0 (c (n "gfx_window_glutin") (v "0.25.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.17") (d #t) (k 0)))) (h "1f59fd712gcnlazfb63sniyv7cbfp8f8wwnqnmfaxw0y641npkpp") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.26.0 (c (n "gfx_window_glutin") (v "0.26.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.18") (d #t) (k 0)))) (h "12k74vfj1qkw2dj3980ldzl5pk7azy9h8qlh0fwahw8f01krji3i") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.27.0 (c (n "gfx_window_glutin") (v "0.27.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)))) (h "00vhm133cd23a19v5b1ca3szj9apqdjci91arpvldn5i8zammi8c") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.28.0 (c (n "gfx_window_glutin") (v "0.28.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)))) (h "0zyvfpbrgpg201690zhdv636bhp4f513m8pwc7fc1lm478578qlp") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.29.0 (c (n "gfx_window_glutin") (v "0.29.0") (d (list (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "glutin") (r "^0.19") (d #t) (k 0)))) (h "19q9dhl0yqwcbs8kpzm0gsl7xh1l0mrg3ixj609mmn92ygh05lwl") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.30.0 (c (n "gfx_window_glutin") (v "0.30.0") (d (list (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "glutin") (r "^0.20") (d #t) (k 0)))) (h "1chy46d721xm9cj363h5cwp6x9wj0phfk0qnxlyjzabf9r9x65gl") (f (quote (("headless"))))))

(define-public crate-gfx_window_glutin-0.31.0 (c (n "gfx_window_glutin") (v "0.31.0") (d (list (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16.1") (d #t) (k 0)) (d (n "glutin") (r "^0.21") (d #t) (k 0)))) (h "0im6jy7fffvjr5saayr0b12cjxdvpm42zslgn5a5i9dm11pzc3ri") (f (quote (("headless"))))))

