(define-module (crates-io gf x_ gfx_window_glfw) #:use-module (crates-io))

(define-public crate-gfx_window_glfw-0.0.1 (c (n "gfx_window_glfw") (v "0.0.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 0)))) (h "0xfyabxpiw4vbv67wkdv06snwm0l9ql7wg8a476ghd1gxv0frmsn")))

(define-public crate-gfx_window_glfw-0.1.0 (c (n "gfx_window_glfw") (v "0.1.0") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 0)))) (h "00wmda4k6q8gncch9sblmlvzw8qni52qjv1xynfcmf65mc7k3pr4")))

(define-public crate-gfx_window_glfw-0.1.1 (c (n "gfx_window_glfw") (v "0.1.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 0)))) (h "0r34znwa55p0d12zf03ihmbg8fxxr082nvyk5cb0k2a7llsf6f72")))

(define-public crate-gfx_window_glfw-0.2.0 (c (n "gfx_window_glfw") (v "0.2.0") (d (list (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glfw") (r ">= 0.0.8") (d #t) (k 0)))) (h "1kwl9z2zrs4n0145xjilnbd7x0yhcmwz9pdw0898cg8k8rbh793h")))

(define-public crate-gfx_window_glfw-0.3.0 (c (n "gfx_window_glfw") (v "0.3.0") (d (list (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "glfw") (r ">= 0.0.8") (d #t) (k 0)))) (h "13sxf267v2mkhfwa5axhmmsfjgpjgnzpc9jsxsh6nrnpbljcis2w")))

(define-public crate-gfx_window_glfw-0.4.0 (c (n "gfx_window_glfw") (v "0.4.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r ">= 0.0.8") (d #t) (k 0)))) (h "10m6a75x6ddpw9h87z30pcrhpi62z5z7lff7qcf7xgphqqxl73x3")))

(define-public crate-gfx_window_glfw-0.4.1 (c (n "gfx_window_glfw") (v "0.4.1") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.6") (d #t) (k 0)) (d (n "glfw") (r "^0.1") (d #t) (k 0)))) (h "1fxdkdfwpfmsmd609a7xxcj9jhj3vnsc3i9h38bwfb2xsg23m5a4")))

(define-public crate-gfx_window_glfw-0.5.0 (c (n "gfx_window_glfw") (v "0.5.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.7") (d #t) (k 0)) (d (n "glfw") (r "^0.2") (d #t) (k 0)))) (h "0iddjhq7qhancx6j9jpbvrgg92h7fdakbr1pwyjzfdcsxgv9k2dj")))

(define-public crate-gfx_window_glfw-0.6.0 (c (n "gfx_window_glfw") (v "0.6.0") (d (list (d (n "gfx") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.8") (d #t) (k 0)) (d (n "glfw") (r "^0.2") (d #t) (k 0)))) (h "08qx37q0cxzwgm58w37df6afmz0ab5g47ji0kpbqz50dh4mhd9dj")))

(define-public crate-gfx_window_glfw-0.7.0 (c (n "gfx_window_glfw") (v "0.7.0") (d (list (d (n "gfx") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.8") (d #t) (k 0)) (d (n "glfw") (r "^0.3") (d #t) (k 0)))) (h "0158cwmy46a1nc76yv31ydgf92qiqbyhcmyx7qfv1b91990wx07b")))

(define-public crate-gfx_window_glfw-0.7.1 (c (n "gfx_window_glfw") (v "0.7.1") (d (list (d (n "gfx_core") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.8.1") (d #t) (k 0)) (d (n "glfw") (r "^0.3") (d #t) (k 0)))) (h "1lkv8xcv7672brn37amjx8acjjivs9jk4qdkahlx19vnq7w1hcva")))

(define-public crate-gfx_window_glfw-0.8.0 (c (n "gfx_window_glfw") (v "0.8.0") (d (list (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.9") (d #t) (k 0)) (d (n "glfw") (r "^0.3") (d #t) (k 0)))) (h "11j04lkqqj05asyqvd1zgfrc75jkkd1jy22jprdpy57d7s08awsp")))

(define-public crate-gfx_window_glfw-0.9.0 (c (n "gfx_window_glfw") (v "0.9.0") (d (list (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.9") (d #t) (k 0)) (d (n "glfw") (r "^0.4") (d #t) (k 0)))) (h "1bj8crlzbjsn6adp3w7pmqx2xw76cm45b75m231nk011lkkp1m16")))

(define-public crate-gfx_window_glfw-0.10.0 (c (n "gfx_window_glfw") (v "0.10.0") (d (list (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.10") (d #t) (k 0)) (d (n "glfw") (r "^0.5") (d #t) (k 0)))) (h "0qh1ad5n0spi2wp0nx37wf3z6immld6sclkqwzar9wq3q8kimr1i")))

(define-public crate-gfx_window_glfw-0.11.0 (c (n "gfx_window_glfw") (v "0.11.0") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.11") (d #t) (k 0)) (d (n "glfw") (r "^0.5") (d #t) (k 0)))) (h "0mgnjkccn6g88vrkmh85wp6csz7y9pfd5crfra25q2501p0z6gkc")))

(define-public crate-gfx_window_glfw-0.12.0 (c (n "gfx_window_glfw") (v "0.12.0") (d (list (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.12") (d #t) (k 0)) (d (n "glfw") (r "^0.11") (d #t) (k 0)))) (h "1sl4cwpwfgqhhk14glbcivs4bbwp0nzld1qd2bkcg35panmkcxxk")))

(define-public crate-gfx_window_glfw-0.13.0 (c (n "gfx_window_glfw") (v "0.13.0") (d (list (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.13") (d #t) (k 0)) (d (n "glfw") (r "^0.12") (d #t) (k 0)))) (h "0zgnhb0019pnxlai08hxbmzwsh6g6fzb84knx6n6fz30xsjng94l")))

(define-public crate-gfx_window_glfw-0.14.0 (c (n "gfx_window_glfw") (v "0.14.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.13") (d #t) (k 0)))) (h "0pk7lb2b45kaigi84wv43cw2kk0va9r8b910gakxnannl41qn9qs")))

(define-public crate-gfx_window_glfw-0.15.0 (c (n "gfx_window_glfw") (v "0.15.0") (d (list (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.15") (d #t) (k 0)))) (h "03548ijnyv6pmbdgqjwsa5fiqyrwginihzcziavrmfwbbcyahkdp")))

(define-public crate-gfx_window_glfw-0.16.0 (c (n "gfx_window_glfw") (v "0.16.0") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "glfw") (r "^0.20") (d #t) (k 0)))) (h "1byid54jpjwn0zpmw023lzdfzxrk8jc579yckyp50rf157psppv4")))

(define-public crate-gfx_window_glfw-0.17.0 (c (n "gfx_window_glfw") (v "0.17.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "glfw") (r "^0.20") (d #t) (k 0)))) (h "1xqxw6w441nl5hf0h0l3c4mb5b1z6hz72057q5vd4zd3prkp4x1x")))

(define-public crate-gfx_window_glfw-0.17.1 (c (n "gfx_window_glfw") (v "0.17.1") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16.1") (d #t) (k 0)) (d (n "glfw") (r "^0.20") (d #t) (k 0)))) (h "15jq941rxs5shs138pbq643j90r3z4dxw7bgg7r6yq7kl348akks")))

