(define-module (crates-io gf x_ gfx_text) #:use-module (crates-io))

(define-public crate-gfx_text-0.0.0 (c (n "gfx_text") (v "0.0.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.0.10") (d #t) (k 0)) (d (n "gfx") (r "^0.5.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.1.1") (d #t) (k 2)) (d (n "glutin") (r "^0.1.6") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1dg54igfvq77vs0acnhf6yv3rdf70nxscmlfafnnwfv2srsh612l")))

(define-public crate-gfx_text-0.1.0 (c (n "gfx_text") (v "0.1.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "~0.0.10") (d #t) (k 0)) (d (n "gfx") (r "^0.5.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "1vg9dz8cl6iapdz60ziwg18krr9jxgrnn5d5sma8iilcn2h0ppln")))

(define-public crate-gfx_text-0.2.0 (c (n "gfx_text") (v "0.2.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "~0.0.10") (d #t) (k 0)) (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "0b8vfdvl5nhm3prkv3mw00vna1iyf599ss7ryy895xqdpqfpc4g8")))

(define-public crate-gfx_text-0.3.0 (c (n "gfx_text") (v "0.3.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "~0.0.10") (d #t) (k 0)) (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "0nn138qrfclcz7wi832j2kkvry2ikg6xcavxycmjsgfdlygmi5r7") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.3.1 (c (n "gfx_text") (v "0.3.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "00dyqri0xzja53fwpjkczdg2s2mcsxg8ibp4lh1f1rn175abjxg8") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.4.0 (c (n "gfx_text") (v "0.4.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "04lc0s4wimiyx5xyisy8xsqnavpwjd7y0kcp6b78zp48an4lbc9k") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.4.1 (c (n "gfx_text") (v "0.4.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "gfx") (r "^0.6.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "03mh02y9lfh0ir9i9apg76s8rppyjbflzm6l9mhm439506drqw05") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.5.0 (c (n "gfx_text") (v "0.5.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "0jlzcd84nzgyg48j18x4288pby8xzypifyii6gmivy2hbhi8kfb8") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.5.1 (c (n "gfx_text") (v "0.5.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx") (r "^0.7.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.5.0") (d #t) (k 2)) (d (n "glutin") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "0rbqm33z7yysqs942xndnmi37dsqab6jh3x42gzhc5y0my07syly") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.6.0 (c (n "gfx_text") (v "0.6.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "freetype-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.7") (d #t) (k 2)) (d (n "glutin") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 2)))) (h "0w2nwy5k81am2wbh28r742zg1ib1dwrdf01n609pmyxfhcvnmg4s") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.7.0 (c (n "gfx_text") (v "0.7.0") (d (list (d (n "freetype-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.8.0") (d #t) (k 2)) (d (n "glutin") (r "^0.4") (d #t) (k 2)))) (h "1kq3lkf0kjfpl99c3q9pscv7940nf5vn4cr1khzy9mqnx5dzrnps") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.8.0 (c (n "gfx_text") (v "0.8.0") (d (list (d (n "freetype-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.8.0") (d #t) (k 2)) (d (n "glutin") (r "^0.4") (d #t) (k 2)))) (h "0wvhg60y02f6bc00zjgfj6kx8bqwfipzlr3a199nzr13gx5xgs9y") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.9.0 (c (n "gfx_text") (v "0.9.0") (d (list (d (n "freetype-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "gfx") (r "^0.10.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.4.8") (d #t) (k 2)))) (h "1f7yp0r7hb2nxbdrcrbkmb1q4wjdsly18hdawj90gn3ddv5sapq6") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.10.0 (c (n "gfx_text") (v "0.10.0") (d (list (d (n "freetype-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "gfx") (r "^0.11.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.11.0") (d #t) (k 2)) (d (n "glutin") (r "^0.5.0") (d #t) (k 2)))) (h "0awq3vcg0nydqnqbk3mj1d1kd27i0a6wxan6i2mmp8z2agzhjfsj") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.11.0 (c (n "gfx_text") (v "0.11.0") (d (list (d (n "freetype-rs") (r "^0.8") (d #t) (k 0)) (d (n "gfx") (r "^0.12") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "0mz0cpdqpf7f9j4495lxk1x3f5zylbisp9xx9ks3gwz50mgw1z95") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.12.0 (c (n "gfx_text") (v "0.12.0") (d (list (d (n "freetype-rs") (r "^0.10") (d #t) (k 0)) (d (n "gfx") (r "^0.12") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "1vk9131k4bl08hinbmsxvqm3zlq903gxnczd9kbmi3n3svcq2yzj") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.13.0 (c (n "gfx_text") (v "0.13.0") (d (list (d (n "freetype-rs") (r "^0.11") (d #t) (k 0)) (d (n "gfx") (r "^0.12") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "0vkwyr88lna645jib6md015bbrgs2km54b1ngivvadp9gvvr4mx9") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.13.2 (c (n "gfx_text") (v "0.13.2") (d (list (d (n "freetype-rs") (r "^0.11") (d #t) (k 0)) (d (n "gfx") (r "^0.12.2") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "0b9z30l2023r6szq7zkab0yzzlrxv3kx7pc5jvv4dri76q729cfh") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.13.1 (c (n "gfx_text") (v "0.13.1") (d (list (d (n "freetype-rs") (r "^0.11") (d #t) (k 0)) (d (n "gfx") (r "^0.12") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.12") (d #t) (k 2)) (d (n "glutin") (r "^0.6") (d #t) (k 2)))) (h "0jzj6zjrmy3ziaf1bd38mngyi3285pprn2mvyw63la62xlxq1k93") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.14.0 (c (n "gfx_text") (v "0.14.0") (d (list (d (n "freetype-rs") (r "^0.11") (d #t) (k 0)) (d (n "gfx") (r "^0.13.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.13.1") (d #t) (k 2)) (d (n "glutin") (r "^0.7.1") (d #t) (k 2)))) (h "09sgx967cy2bjvqvssj3197r3hpq5xc8kpgcq9i5mbiiamby09x9") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.15.0 (c (n "gfx_text") (v "0.15.0") (d (list (d (n "freetype-rs") (r "^0.11") (d #t) (k 0)) (d (n "gfx") (r "^0.14.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.14.0") (d #t) (k 2)) (d (n "glutin") (r "^0.7.1") (d #t) (k 2)))) (h "1m8zqp4dw3kahazy98p4dg9bslr9ar1zx6af13zph8avp5h80mf7") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.16.0 (c (n "gfx_text") (v "0.16.0") (d (list (d (n "freetype-rs") (r "^0.12") (d #t) (k 0)) (d (n "gfx") (r "^0.15.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.15.0") (d #t) (k 2)) (d (n "glutin") (r "^0.7.1") (d #t) (k 2)))) (h "0vyzv55l9zv38vsvivbs2kn3g1f020cn5zsciwlf195jd4ygica8") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.17.0 (c (n "gfx_text") (v "0.17.0") (d (list (d (n "freetype-rs") (r "^0.13") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.15.0") (d #t) (k 2)) (d (n "glutin") (r "^0.7.1") (d #t) (k 2)))) (h "17h4xjk556w9da6z1iw4b6yxk65b4d3y5ialq65whjpdl465z244") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.18.0 (c (n "gfx_text") (v "0.18.0") (d (list (d (n "freetype-rs") (r "^0.14") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.17.0") (d #t) (k 2)) (d (n "glutin") (r "^0.9.2") (d #t) (k 2)))) (h "1l0p4p4sy1l85rqqb9l754gfhpz6jv1gdkma3vkpz5ra8pgzfa1i") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.19.0 (c (n "gfx_text") (v "0.19.0") (d (list (d (n "freetype-rs") (r "^0.15") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.17.0") (d #t) (k 2)) (d (n "glutin") (r "^0.9.2") (d #t) (k 2)))) (h "1jq75mk2zym0a61ggzqacjhl1nw6gid50hxm2y6yvz0jbdy397fi") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.20.0 (c (n "gfx_text") (v "0.20.0") (d (list (d (n "freetype-rs") (r "^0.16") (d #t) (k 0)) (d (n "gfx") (r "^0.16.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.17.0") (d #t) (k 2)) (d (n "glutin") (r "^0.9.2") (d #t) (k 2)))) (h "06i0wvwqp6cf2p3y9f3x78gd3ik3n2bqm0l8p5zhk0cr1yjmff1f") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.21.0 (c (n "gfx_text") (v "0.21.0") (d (list (d (n "freetype-rs") (r "^0.17") (d #t) (k 0)) (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.20.0") (d #t) (k 2)) (d (n "glutin") (r "^0.12.0") (d #t) (k 2)))) (h "0d6k27y3k0789wpgc549yr6dbxqnzay1381zsqcbgi831w235dhz") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.22.0 (c (n "gfx_text") (v "0.22.0") (d (list (d (n "freetype-rs") (r "^0.18") (d #t) (k 0)) (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.20.0") (d #t) (k 2)) (d (n "glutin") (r "^0.12.0") (d #t) (k 2)))) (h "19gia5mhyragma7xmcs4liq1mnis9a5lyjpf8v55bm9kpmyliwzn") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.23.0 (c (n "gfx_text") (v "0.23.0") (d (list (d (n "freetype-rs") (r "^0.19") (d #t) (k 0)) (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26.0") (d #t) (k 2)) (d (n "glutin") (r "^0.18.0") (d #t) (k 2)))) (h "0vr5cjn7dkh8ahw1q2v1md8jpcx89p1nchmidyqi64r3q3sg16rn") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.24.0 (c (n "gfx_text") (v "0.24.0") (d (list (d (n "freetype-rs") (r "^0.20") (d #t) (k 0)) (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26.0") (d #t) (k 2)) (d (n "glutin") (r "^0.18.0") (d #t) (k 2)))) (h "1lq1xnidnfh7195f9bgyrxr2cg0q3sq8jcn81bc1r79x7a3xck7n") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.25.0 (c (n "gfx_text") (v "0.25.0") (d (list (d (n "freetype-rs") (r "^0.22.0") (d #t) (k 0)) (d (n "gfx") (r "^0.17.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26.0") (d #t) (k 2)) (d (n "glutin") (r "^0.18.0") (d #t) (k 2)))) (h "0i1wxznhqj8zm0gpd39mnxwp9sfviy464gwjgmrypnv3c73blfxl") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.26.0 (c (n "gfx_text") (v "0.26.0") (d (list (d (n "freetype-rs") (r "^0.23.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.31.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)))) (h "07f33y77bkcjx4078qkrgmk7mhd2akb5hydc3rq86f77rdmc6l9f") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.27.0 (c (n "gfx_text") (v "0.27.0") (d (list (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.31.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)))) (h "0bgjb5zn9k3iz8jjhh948xlcmrif9ihbnbrpzk4bxli0djvvxxdr") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.28.0 (c (n "gfx_text") (v "0.28.0") (d (list (d (n "freetype-rs") (r "^0.27.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.31.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)))) (h "0xrvf8118fir240bn0xmx68v3s4ibrbgxwnzz994f0ygfxml7vi5") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.29.0 (c (n "gfx_text") (v "0.29.0") (d (list (d (n "freetype-rs") (r "^0.29.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.31.0") (d #t) (k 2)) (d (n "glutin") (r "^0.21.0") (d #t) (k 2)))) (h "1b5cbn6c91k2220s1qrc4z9kda0c2n6abcry1xrpdjnqgjb15qm9") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.30.0 (c (n "gfx_text") (v "0.30.0") (d (list (d (n "freetype-rs") (r "^0.31.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.123.0") (d #t) (k 2)))) (h "16pnmjkd44mh167n7s2xsx3w84zwbwlkh741w79hlah5833hpv52") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.31.0 (c (n "gfx_text") (v "0.31.0") (d (list (d (n "freetype-rs") (r "^0.32.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.125.0") (d #t) (k 2)))) (h "0bbnmawkgvmjlm80a4in14gnni6afz1qyyax35ypn6cmp3z8dv9f") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.32.0 (c (n "gfx_text") (v "0.32.0") (d (list (d (n "freetype-rs") (r "^0.33.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.130.0") (d #t) (k 2)))) (h "05vfq1fqlnj5rg8yh49419im3k51j7qb0z6mj8dc4p0s355ahrp9") (f (quote (("include-font") ("default" "include-font"))))))

(define-public crate-gfx_text-0.33.0 (c (n "gfx_text") (v "0.33.0") (d (list (d (n "freetype-rs") (r "^0.34.0") (d #t) (k 0)) (d (n "gfx") (r "^0.18.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)))) (h "1xvk7p3hz45l8whbz9gf825nv17qzjxq9gv1ccim080aljkkqvik") (f (quote (("include-font") ("default" "include-font"))))))

