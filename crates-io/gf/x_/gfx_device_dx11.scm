(define-module (crates-io gf x_ gfx_device_dx11) #:use-module (crates-io))

(define-public crate-gfx_device_dx11-0.1.0 (c (n "gfx_device_dx11") (v "0.1.0") (d (list (d (n "d3d11-sys") (r "^0.1") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1q32fmxvd9j0rp2nyy57vazczjhb14613kbc7rk07c82ihrjwy82")))

(define-public crate-gfx_device_dx11-0.2.0 (c (n "gfx_device_dx11") (v "0.2.0") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "09hj24p9i8jqpsrgh2vfmi4r7qn1k7lws4nai8kz5cf1q0fgin2h")))

(define-public crate-gfx_device_dx11-0.2.1 (c (n "gfx_device_dx11") (v "0.2.1") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.7") (d #t) (k 0)))) (h "1gya29yqkmxx3ca7h7r115grim4af6dkqlsdjawqi05rc33r77mp")))

(define-public crate-gfx_device_dx11-0.3.0 (c (n "gfx_device_dx11") (v "0.3.0") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.7") (d #t) (k 0)))) (h "1fjcy5rksg5749fivpnqi5v3zzq2xdvvilmzi3zfk5m877jvji2n")))

(define-public crate-gfx_device_dx11-0.4.0 (c (n "gfx_device_dx11") (v "0.4.0") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.7") (d #t) (k 0)))) (h "0iqf6q3qh39wnlwj93nv6bzzywswjjvsv631rhp1rkjjrgcpfzk5")))

(define-public crate-gfx_device_dx11-0.5.0 (c (n "gfx_device_dx11") (v "0.5.0") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.7") (d #t) (k 0)))) (h "0siy2dy018aaf56rbr5cjbblg4p58lm6qsjgdk1h5walvh07hfy7")))

(define-public crate-gfx_device_dx11-0.6.0 (c (n "gfx_device_dx11") (v "0.6.0") (d (list (d (n "d3d11-sys") (r "^0.2") (d #t) (k 0)) (d (n "d3dcompiler-sys") (r "^0.2") (d #t) (k 0)) (d (n "dxguid-sys") (r "^0.2") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0c2ma9rw8a4rxrxvnqmrpzvcdr5rsxmzya2xzgz3q9skz7hp5yi5")))

(define-public crate-gfx_device_dx11-0.7.0 (c (n "gfx_device_dx11") (v "0.7.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "d3dcommon" "d3dcompiler" "winerror"))) (d #t) (k 0)))) (h "1dc2dalyg2xac929kqfyj9hic95640dk5m8aicvfg1072a9py582")))

(define-public crate-gfx_device_dx11-0.7.1 (c (n "gfx_device_dx11") (v "0.7.1") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "d3dcommon" "d3dcompiler" "winerror"))) (d #t) (k 0)))) (h "1zw3ccir9xyi4wsgkakb1d53mdibgyy08plw8sxflzblqlnf465r")))

(define-public crate-gfx_device_dx11-0.8.0 (c (n "gfx_device_dx11") (v "0.8.0") (d (list (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "d3dcommon" "d3dcompiler" "winerror"))) (d #t) (k 0)))) (h "1svb9zzxbyyjv1w4yx7swj43w8s18p61mzs9n78vxbygfa844q6c")))

(define-public crate-gfx_device_dx11-0.8.2 (c (n "gfx_device_dx11") (v "0.8.2") (d (list (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "d3dcommon" "d3dcompiler" "winerror"))) (d #t) (k 0)))) (h "1cwpi88ppklzq12lcwnwvblkkwg7n2hkax99qyy6pa4sbhrcagvz")))

