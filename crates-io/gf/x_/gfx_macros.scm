(define-module (crates-io gf x_ gfx_macros) #:use-module (crates-io))

(define-public crate-gfx_macros-0.1.0 (c (n "gfx_macros") (v "0.1.0") (h "1ixn8851wwb97chdm07y296apxwc9fnvvfr0iypq7gwh8xil69ac")))

(define-public crate-gfx_macros-0.1.1 (c (n "gfx_macros") (v "0.1.1") (h "041b7sr60jdcvqg9awriy13xrn7qi775i66gmfbp4dgmsgfwba25")))

(define-public crate-gfx_macros-0.1.3 (c (n "gfx_macros") (v "0.1.3") (h "0zcr57wvy11zizh1xbz4sra4ys4h90ba4k34s0ijwr9000hx1kxk")))

(define-public crate-gfx_macros-0.1.4 (c (n "gfx_macros") (v "0.1.4") (h "0n0a23nm1c8dfdr65492w4ln2wxffgwbjnx7716frzh46gvr3445")))

(define-public crate-gfx_macros-0.1.5 (c (n "gfx_macros") (v "0.1.5") (h "1bg9rbdv24jd1sz3m9xml97yjysm4504aqys6qfcb11lgwv74qja")))

(define-public crate-gfx_macros-0.1.6 (c (n "gfx_macros") (v "0.1.6") (h "0p9lq4l16as9glxxksqjss7fcivsw2gqnsnhhpif77nnil6l4fda")))

(define-public crate-gfx_macros-0.1.7 (c (n "gfx_macros") (v "0.1.7") (d (list (d (n "gfx") (r "*") (d #t) (k 2)))) (h "1j6knmhb3rdh1qkrz9sm6xgaq432qjmy00cv2f04sl67fya14k3i")))

(define-public crate-gfx_macros-0.1.8 (c (n "gfx_macros") (v "0.1.8") (d (list (d (n "gfx") (r "*") (d #t) (k 2)))) (h "1wf63z58xg5w6jar9gb2j8nzfcs486yj3dml0sk439xr2cvkxn74")))

(define-public crate-gfx_macros-0.1.10 (c (n "gfx_macros") (v "0.1.10") (d (list (d (n "gfx") (r "*") (d #t) (k 2)))) (h "0bwlc0784wllyb26vxkl0ifa8ghkqz349r550f16l0yvgdh5iqx5")))

(define-public crate-gfx_macros-0.2.0 (c (n "gfx_macros") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0bqvmsmlk113v2yy0dp1wcbgkbjkgi8knx3g7nbslw1jlb9zl2yj")))

(define-public crate-gfx_macros-0.2.1 (c (n "gfx_macros") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "07rh9lh6d89p3lwvasqzg2jsnmf9f7ykazj3fdpb41a77wfrx3yk")))

