(define-module (crates-io gf x_ gfx_window_sdl) #:use-module (crates-io))

(define-public crate-gfx_window_sdl-0.1.0 (c (n "gfx_window_sdl") (v "0.1.0") (d (list (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.9") (d #t) (k 0)) (d (n "sdl2") (r "^0.16") (d #t) (k 0)))) (h "120hcmkifizrw9wn7jxzy1mqbqzmxqcf7p6870fb7h54zj25z5yx")))

(define-public crate-gfx_window_sdl-0.2.0 (c (n "gfx_window_sdl") (v "0.2.0") (d (list (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.10") (d #t) (k 0)) (d (n "sdl2") (r "^0.18") (d #t) (k 0)))) (h "1hhcfxp1ia3p6p3xl6q01fwf6wgr5947q8gj9wls6958490kb7x6")))

(define-public crate-gfx_window_sdl-0.3.0 (c (n "gfx_window_sdl") (v "0.3.0") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.11") (d #t) (k 0)) (d (n "sdl2") (r "^0.18") (d #t) (k 0)))) (h "0jmiz7whfix4z29w9zx9hshcs4s181bsnl7rlw59s9nyd99ilqw5")))

(define-public crate-gfx_window_sdl-0.4.0 (c (n "gfx_window_sdl") (v "0.4.0") (d (list (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.12") (d #t) (k 0)) (d (n "sdl2") (r "^0.27") (d #t) (k 0)))) (h "0a0psw2ff145d61xqn12l66acbg98x3mi5pq33c73jqnv8bdikwf")))

(define-public crate-gfx_window_sdl-0.5.0 (c (n "gfx_window_sdl") (v "0.5.0") (d (list (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.28") (d #t) (k 0)))) (h "1f4drl4s0ciq3c7f5f552yvbffspm3iqpwv64k723n775x3bhka8")))

(define-public crate-gfx_window_sdl-0.6.0 (c (n "gfx_window_sdl") (v "0.6.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "0nsl64n3bl960h9m0xh0wjc5jcjpwqjvy2myh09cql9cp2lx17zw")))

(define-public crate-gfx_window_sdl-0.7.0 (c (n "gfx_window_sdl") (v "0.7.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0njmhg8zk6s0jfyqmlm7jdq3wd2r29hifivzrc7yxqkz09rpsby1")))

(define-public crate-gfx_window_sdl-0.8.0 (c (n "gfx_window_sdl") (v "0.8.0") (d (list (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.31") (d #t) (k 0)))) (h "1wa48d0aj5jh2n5wn36f1rp1qkvbh5gxj7rxc73h8232wr86xlzh")))

(define-public crate-gfx_window_sdl-0.9.0 (c (n "gfx_window_sdl") (v "0.9.0") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1gy8i742y0wv81m8wdi2m9nkvvjlfgvh1z7mpzgk3zzwii72jgyn")))

(define-public crate-gfx_window_sdl-0.9.1 (c (n "gfx_window_sdl") (v "0.9.1") (d (list (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.16.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "19d97r1pqc3im3nwc1az6rxiz8xlxs3q1mrs2pja32qqslq1jdr6")))

