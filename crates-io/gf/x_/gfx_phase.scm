(define-module (crates-io gf x_ gfx_phase) #:use-module (crates-io))

(define-public crate-gfx_phase-0.1.0 (c (n "gfx_phase") (v "0.1.0") (d (list (d (n "draw_queue") (r "*") (d #t) (k 0)) (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1kvppbcv5n4qlxn1vhznmxpra53z4ms7xppjxm03dp44bigx485n")))

(define-public crate-gfx_phase-0.1.1 (c (n "gfx_phase") (v "0.1.1") (d (list (d (n "draw_queue") (r "*") (d #t) (k 0)) (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "03lxni3dcb8li27ia75zjrzqkcvrwwxnrdyzq01ni0ijj19p8116")))

(define-public crate-gfx_phase-0.2.0 (c (n "gfx_phase") (v "0.2.0") (d (list (d (n "draw_queue") (r "0.1.*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0y45pyxplvyqicp8zf6gs9s6js5a0bfg7b40aa701cpwyi2nlc3g")))

(define-public crate-gfx_phase-0.2.1 (c (n "gfx_phase") (v "0.2.1") (d (list (d (n "draw_queue") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "15pzihf0jmg7jf5ygfbijkcsxf9ndy04ailbn2i2mzh5nlanim5n")))

(define-public crate-gfx_phase-0.3.0 (c (n "gfx_phase") (v "0.3.0") (d (list (d (n "draw_queue") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1sszj9fr508cz4akdqf22m9ayyp92n23d7hk5b1xvii3jhr8a5k2")))

(define-public crate-gfx_phase-0.4.0 (c (n "gfx_phase") (v "0.4.0") (d (list (d (n "draw_queue") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1cqlrbgldj805yfqjawwbd65sy2ygz6dybl4mdczxkn98n4iknyf")))

(define-public crate-gfx_phase-0.5.0 (c (n "gfx_phase") (v "0.5.0") (d (list (d (n "draw_queue") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "19cycnays73d7ph6p35df0li15h3a3gsvvb5v63lahyklycv2g5w")))

(define-public crate-gfx_phase-0.6.0 (c (n "gfx_phase") (v "0.6.0") (d (list (d (n "draw_queue") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1kdg481q2dhfjqbiryh15r8ccxqi1qh0jyravpb243w76bd7fp8x")))

