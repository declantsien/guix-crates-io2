(define-module (crates-io gf x_ gfx_shader_watch) #:use-module (crates-io))

(define-public crate-gfx_shader_watch-0.1.0 (c (n "gfx_shader_watch") (v "0.1.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "gfx") (r "^0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0") (d #t) (k 2)) (d (n "glutin") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "16cil08rb2q0z8lb3vbqv9860sccm6j8j9v5i6marg1xfd96pll2")))

(define-public crate-gfx_shader_watch-0.2.0 (c (n "gfx_shader_watch") (v "0.2.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "gfx") (r "^0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0") (d #t) (k 2)) (d (n "glutin") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "0xnpv4n9dk8ji9sscjgh09w4d3qgx79cbky4pm2z8lq7lkmgvgn3")))

(define-public crate-gfx_shader_watch-0.3.0 (c (n "gfx_shader_watch") (v "0.3.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "gfx") (r "^0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0") (d #t) (k 2)) (d (n "glutin") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "0xbwhf6gbg38cqbmiy399qwcajxpf41d7a1akzn353pg52shzlai")))

(define-public crate-gfx_shader_watch-0.3.1 (c (n "gfx_shader_watch") (v "0.3.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "gfx") (r "^0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0") (d #t) (k 2)) (d (n "glutin") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "1qqxwrf4iyq7zd28795ry3s64hlqvqx493gvpq555fwhb4vivhbn")))

(define-public crate-gfx_shader_watch-0.3.2 (c (n "gfx_shader_watch") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.16") (d #t) (k 2)) (d (n "glutin") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "128zjbyr2blji0npf6hsqcy8rkd0g4n6mxby6n7ppyhdcgdz0y4q")))

(define-public crate-gfx_shader_watch-0.3.3 (c (n "gfx_shader_watch") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.16") (d #t) (k 2)) (d (n "glutin") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "1nv3v1bp8cz9rskv002zqcxwsk1s9g9xmyp2kzrik5vmm1qgznv0")))

(define-public crate-gfx_shader_watch-0.3.4 (c (n "gfx_shader_watch") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "gfx") (r "^0.16") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.19") (d #t) (k 2)) (d (n "glutin") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "1xdll0rw5zs9hcn0n3mc19i4nj109017nwz5n0gninvw2frlkmzs")))

(define-public crate-gfx_shader_watch-0.4.0 (c (n "gfx_shader_watch") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.5.0-rc.1") (d #t) (k 2)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.20") (d #t) (k 2)) (d (n "glutin") (r "^0.12") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "10y1pjmlm2qam96i5h0042y6i5mrbbq3p9ckabl42wwavarfrbmw")))

(define-public crate-gfx_shader_watch-0.5.0 (c (n "gfx_shader_watch") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.26") (d #t) (k 2)) (d (n "glutin") (r "^0.18") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "05xsnzwy0qjbc6ab4jlqznz7fn908jj9bwy53dlppx4fn1i2vj1d")))

(define-public crate-gfx_shader_watch-0.6.0 (c (n "gfx_shader_watch") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.29") (d #t) (k 2)) (d (n "glutin") (r "^0.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)))) (h "1kmdfrjgn1ppbycgacrnkivq471rcvyymv2v7cjnrnd1lba16bqz")))

(define-public crate-gfx_shader_watch-0.6.1 (c (n "gfx_shader_watch") (v "0.6.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "glutin") (r "^0.29") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^5") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.29") (d #t) (k 2)))) (h "1kvpmawzq3cznkf9r1rkdfmzaphb2c91z9xi1c09p0xz6p5swfbf")))

(define-public crate-gfx_shader_watch-0.6.2 (c (n "gfx_shader_watch") (v "0.6.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "gfx") (r "^0.18") (d #t) (k 0)) (d (n "glutin") (r "^0.30.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^6") (d #t) (k 0)) (d (n "old_school_gfx_glutin_ext") (r "^0.31") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "0a1wc50kabl8iwqq5ar2maqksrv29gcg1spp60jwhd87mf7zq1m0")))

