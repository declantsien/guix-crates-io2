(define-module (crates-io gf x_ gfx_64) #:use-module (crates-io))

(define-public crate-gfx_64-0.1.0 (c (n "gfx_64") (v "0.1.0") (d (list (d (n "log_64") (r "^0.1.0") (d #t) (k 0)) (d (n "math_64") (r "^0.1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.60") (k 1)))) (h "0sxy14z3yvf657a3s5wah503bknrrrjmw94ph88nx86qdcw4h984")))

