(define-module (crates-io gf x_ gfx_smaa) #:use-module (crates-io))

(define-public crate-gfx_smaa-0.1.0 (c (n "gfx_smaa") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "piston_window") (r "^0.75") (d #t) (k 2)))) (h "0cxyjqlmh5wfrwfnpv858xd4yr8sq23zwk5zb02biwl9msdbb5sd")))

(define-public crate-gfx_smaa-0.2.0 (c (n "gfx_smaa") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "piston_window") (r "^0.77") (d #t) (k 2)))) (h "056fbv82sm3xbg7kd01xn6b4s5ppap0pi5bcsxyn8n8n53fmpy27")))

(define-public crate-gfx_smaa-0.2.1 (c (n "gfx_smaa") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gfx") (r "^0.17") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "piston_window") (r "^0.77") (d #t) (k 2)))) (h "1pzfyrzh6jnamd66dq9h0mk2k3kk32myp9rg6rsha63lmfpi02zc")))

