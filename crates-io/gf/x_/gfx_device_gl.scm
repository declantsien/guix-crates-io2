(define-module (crates-io gf x_ gfx_device_gl) #:use-module (crates-io))

(define-public crate-gfx_device_gl-0.0.0-test (c (n "gfx_device_gl") (v "0.0.0-test") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1q0sjmns6bzbsy7xg24fzw56fb0zv183i6kqxn10bxh2w6vv6cdf")))

(define-public crate-gfx_device_gl-0.1.0 (c (n "gfx_device_gl") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0s8shhf4p4jii7yc9sf6kpdi0p5yc1wrnflb5bvwydm5xpalmgb9")))

(define-public crate-gfx_device_gl-0.1.1 (c (n "gfx_device_gl") (v "0.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0gxbsq83ddcxwzlnra06sy6c0b9d9vmvsbdxadcnmpipbpglc79d")))

(define-public crate-gfx_device_gl-0.1.2 (c (n "gfx_device_gl") (v "0.1.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0vyc7ycrbd7pxwf5ab68wip71lh9m6bvcxn1x3i71gr5j24hpjf3")))

(define-public crate-gfx_device_gl-0.1.3 (c (n "gfx_device_gl") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1gmzdd9mvf09h170l9bbyfzcbn3sw0ipkmry7y17772llz2j3yx8")))

(define-public crate-gfx_device_gl-0.2.1 (c (n "gfx_device_gl") (v "0.2.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1xchcidrq9zr9dizhxcwq26dzixpgcdjifcbb74k655zp813zmcj")))

(define-public crate-gfx_device_gl-0.2.2 (c (n "gfx_device_gl") (v "0.2.2") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "15fix5bp3qv3d1y48lirbwwsskpz46s673rni3mprzx2phbspj3w")))

(define-public crate-gfx_device_gl-0.2.3 (c (n "gfx_device_gl") (v "0.2.3") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1sjha2y4hbv1khdrjy03xp938zq31w64h772gvgc743j0zs8d4vh")))

(define-public crate-gfx_device_gl-0.2.5 (c (n "gfx_device_gl") (v "0.2.5") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0lz1wr0xsx57ysry53gs5l8nyh5pfq1362pdmr4hrxbksqf6bblj")))

(define-public crate-gfx_device_gl-0.2.6 (c (n "gfx_device_gl") (v "0.2.6") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1daziyf176028krqxayd9wz9jvzf5fx6zhf987y0s20kjgvrkwyb")))

(define-public crate-gfx_device_gl-0.3.0 (c (n "gfx_device_gl") (v "0.3.0") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0vl0cfd7r2c32xx423s0i8s153vb0fqzd1li92qyl55nhg0m4dv4")))

(define-public crate-gfx_device_gl-0.3.1 (c (n "gfx_device_gl") (v "0.3.1") (d (list (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0cpcn4f8y17030wixndsr5rqybbq3502mbnwn199wm92nmwigh74")))

(define-public crate-gfx_device_gl-0.4.0 (c (n "gfx_device_gl") (v "0.4.0") (d (list (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_gl") (r "~0.1.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "07jq0bn9ncfhzl5vn71r1irmal37ffxah4d5xliklvadqznvj588")))

(define-public crate-gfx_device_gl-0.4.1 (c (n "gfx_device_gl") (v "0.4.1") (d (list (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "02l6spz1jjx62rqx7bmx5c46gr72v3vnx2zpywmvxzjmf383a6vx")))

(define-public crate-gfx_device_gl-0.5.0 (c (n "gfx_device_gl") (v "0.5.0") (d (list (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "04n7a77b77yim37gmh39wi7127ww9ajcvlq2ba4hlychwsx1vdm6")))

(define-public crate-gfx_device_gl-0.6.0 (c (n "gfx_device_gl") (v "0.6.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0zlbd0wx8b4y3xki6vq9j46s15ymi7iyyvzpphzhvbgzyg5218m9")))

(define-public crate-gfx_device_gl-0.6.1 (c (n "gfx_device_gl") (v "0.6.1") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1p5dmlq729dl6vibjzq3yl2rnk7wsds1n0xv04jiqkfxmap8wxwi")))

(define-public crate-gfx_device_gl-0.7.0 (c (n "gfx_device_gl") (v "0.7.0") (d (list (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1xns665h9cra9gzx29adz52z478yiakn5v5s67mljdkzps5r95f9")))

(define-public crate-gfx_device_gl-0.8.0 (c (n "gfx_device_gl") (v "0.8.0") (d (list (d (n "gfx_core") (r "^0.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0cksv0fm50zqx3v4f7chimwwb7z64mv9axk2aqrs54xx1r8byn9p")))

(define-public crate-gfx_device_gl-0.8.1 (c (n "gfx_device_gl") (v "0.8.1") (d (list (d (n "gfx_core") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0fzspnmyf9bbrm3z4y3ca0b0mcfn5mmvd1ydmxx9f08pvsf9yzcg")))

(define-public crate-gfx_device_gl-0.8.2 (c (n "gfx_device_gl") (v "0.8.2") (d (list (d (n "gfx_core") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "08vsrf4wgk6v64jxq6pd4ljsa9n9ad1c9c1x6ljs1bj7z9azzkxs")))

(define-public crate-gfx_device_gl-0.9.0 (c (n "gfx_device_gl") (v "0.9.0") (d (list (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1dx8nm18nsizxzb0yk2i1pk6sv1vp92vqgrs3wygshw40pfhn4j5")))

(define-public crate-gfx_device_gl-0.10.0 (c (n "gfx_device_gl") (v "0.10.0") (d (list (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1yfyfv3y9wh6wngqh29fgcpbhbc7p1gbyh09hbj2jr75m0lr2xip")))

(define-public crate-gfx_device_gl-0.11.0 (c (n "gfx_device_gl") (v "0.11.0") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "189y69r39lq6y34lc29z23a6pncvmdw7iij45jkyq44ddvhr8582")))

(define-public crate-gfx_device_gl-0.11.1 (c (n "gfx_device_gl") (v "0.11.1") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "17jjnmirdpzbzc9501p3b3jqnknwd14ls3gg934l33bj1aqj1av4")))

(define-public crate-gfx_device_gl-0.11.2 (c (n "gfx_device_gl") (v "0.11.2") (d (list (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1f21d38axk8chsz49zx4bgd12fznmdcnwxhrs7sgs7ksx2cfq00n")))

(define-public crate-gfx_device_gl-0.12.0 (c (n "gfx_device_gl") (v "0.12.0") (d (list (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1jn2dnbakrwyiwk4wiz58c5lsbcg328s55qiall6dyraaws76qih")))

(define-public crate-gfx_device_gl-0.13.0 (c (n "gfx_device_gl") (v "0.13.0") (d (list (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0k8nx64i7qhw839hncscmkidxzxdrh8nfh3hckad4alf72f5qavw")))

(define-public crate-gfx_device_gl-0.13.1 (c (n "gfx_device_gl") (v "0.13.1") (d (list (d (n "gfx_core") (r "^0.6.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1kphdaifddlh01ksamfwvcd7lv7fd87pczfipyr3z9yyxmx24l9s")))

(define-public crate-gfx_device_gl-0.14.0 (c (n "gfx_device_gl") (v "0.14.0") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "02s1w74qgnanw4yqzyngvm3g8pl11a9qg8bmdf19j345grd876fh")))

(define-public crate-gfx_device_gl-0.14.1 (c (n "gfx_device_gl") (v "0.14.1") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1a7786g1p4xskrxv33ffgpf5wll3k292mxrv41wyijpm494g4snr")))

(define-public crate-gfx_device_gl-0.14.2 (c (n "gfx_device_gl") (v "0.14.2") (d (list (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "03yv92d8rij6j3698chdf9wmph8lf6v4ivvcs82h5m4n6c4x89ph")))

(define-public crate-gfx_device_gl-0.14.3 (c (n "gfx_device_gl") (v "0.14.3") (d (list (d (n "gfx_core") (r "^0.7.2") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "055w6iv4ikackk68w3ajzjjsqxx5q5gswyd3akw4jgy2nk0a93rs")))

(define-public crate-gfx_device_gl-0.14.4 (c (n "gfx_device_gl") (v "0.14.4") (d (list (d (n "gfx_core") (r "^0.7.2") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1705kisc86qaxf46z926lhmv23459lx1xlr4yfjrff4j5yq0x7zl")))

(define-public crate-gfx_device_gl-0.14.5 (c (n "gfx_device_gl") (v "0.14.5") (d (list (d (n "gfx_core") (r "^0.7.2") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "03fhs1avab44sig6rhx8ij5cwsxb4101pgn5xlb25bs0535mxxvm")))

(define-public crate-gfx_device_gl-0.14.6 (c (n "gfx_device_gl") (v "0.14.6") (d (list (d (n "gfx_core") (r "^0.7.2") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1hfqff0d2d5dayp2pz74mpbhxyv5d8w6l2i1waqx0f9lyqay6iaz")))

(define-public crate-gfx_device_gl-0.15.0 (c (n "gfx_device_gl") (v "0.15.0") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0m1hga4255sjbj383nsy3rq36h4z3f82q6qsfxk4km840yhkqqmx")))

(define-public crate-gfx_device_gl-0.15.1 (c (n "gfx_device_gl") (v "0.15.1") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09fhi57kizrq2v6s2py348dvm7r5mk7ycnbb6ss42d7w2swmbqsf")))

(define-public crate-gfx_device_gl-0.15.2 (c (n "gfx_device_gl") (v "0.15.2") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pcjfx1ndzclmlrlb48cx3nhpif8injm39zpn1xaa88pjwhrpnr8")))

(define-public crate-gfx_device_gl-0.15.3 (c (n "gfx_device_gl") (v "0.15.3") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01c335w6xvyhz7dakzb84n7pnvdi3rc74jc199680abj0733pyss")))

(define-public crate-gfx_device_gl-0.15.4 (c (n "gfx_device_gl") (v "0.15.4") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03qdx0q72a7h9zzjxzwpmlgg0gnrghb8qq3ghli70hhldbzz037b")))

(define-public crate-gfx_device_gl-0.15.5 (c (n "gfx_device_gl") (v "0.15.5") (d (list (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10m9h603978cw131vylz649xg9rz4rnb3vgm3rx1iqwsmdvcryfy")))

(define-public crate-gfx_device_gl-0.16.0 (c (n "gfx_device_gl") (v "0.16.0") (d (list (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gymbsmlwld6yp1c2553943v7zwl8lvn219kxfd7ww9harnb8a92")))

(define-public crate-gfx_device_gl-0.16.1 (c (n "gfx_device_gl") (v "0.16.1") (d (list (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00r0zp7j9d13jw90lph9d80976p051x77f67kxd2vwsps0hrrnrz")))

(define-public crate-gfx_device_gl-0.16.2 (c (n "gfx_device_gl") (v "0.16.2") (d (list (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "gfx_gl") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g5yg19jvxdmviljyakhd6253bnb2qg7v8iscf48ihc0ldgki70h")))

