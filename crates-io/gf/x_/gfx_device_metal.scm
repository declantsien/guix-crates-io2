(define-module (crates-io gf x_ gfx_device_metal) #:use-module (crates-io))

(define-public crate-gfx_device_metal-0.1.0 (c (n "gfx_device_metal") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "cocoa") (r "^0.2.5") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "metal-rs") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1.8") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (k 0)))) (h "1j443v06y5nh99p8l20n8gf6w5bfyhsxij8rgx9g8a0fhscr2vxv")))

(define-public crate-gfx_device_metal-0.2.0 (c (n "gfx_device_metal") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "cocoa") (r "^0.5") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "metal-rs") (r "^0.3") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (k 0)))) (h "0626ckw951cdgz8347ry3vh3669xqx17rswilz5ramv0kw3bi46p")))

(define-public crate-gfx_device_metal-0.3.0 (c (n "gfx_device_metal") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "cocoa") (r "^0.9") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metal-rs") (r "^0.4") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (k 0)))) (h "147nap58khr6r3n51yavfs8wk0mq38xazwgfd0mp6j2lsrqjqys0")))

