(define-module (crates-io gf x_ gfx_pipeline) #:use-module (crates-io))

(define-public crate-gfx_pipeline-0.1.0 (c (n "gfx_pipeline") (v "0.1.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_phase") (r "*") (d #t) (k 0)) (d (n "gfx_scene") (r "*") (d #t) (k 0)))) (h "190d2gjzc3nb0qg9wf8qdwpzdb1anl7z4hyf6wscbpcn6a2c9fdz")))

(define-public crate-gfx_pipeline-0.2.0 (c (n "gfx_pipeline") (v "0.2.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "0.2.*") (d #t) (k 0)) (d (n "gfx_scene") (r "0.2.*") (d #t) (k 0)))) (h "0av3ghf5i2sb1n0ddrcmws78y92ipkmg3v14m6vcw6vqd86s2kgg")))

(define-public crate-gfx_pipeline-0.2.1 (c (n "gfx_pipeline") (v "0.2.1") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.2") (d #t) (k 0)) (d (n "gfx_scene") (r "^0.3") (d #t) (k 0)))) (h "1b1fg3pik6h3jm7rzvfp3883cl23a1w1zdr02s4331vnw1i9jbx3")))

(define-public crate-gfx_pipeline-0.3.0 (c (n "gfx_pipeline") (v "0.3.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.4") (d #t) (k 0)) (d (n "gfx_scene") (r "^0.4") (d #t) (k 0)))) (h "1n32x2chr4mi8732igrx4z811i64k145csq13dm1gshpljwsagd9")))

