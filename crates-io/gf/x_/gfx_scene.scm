(define-module (crates-io gf x_ gfx_scene) #:use-module (crates-io))

(define-public crate-gfx_scene-0.1.0 (c (n "gfx_scene") (v "0.1.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "*") (d #t) (k 0)) (d (n "gfx_phase") (r "*") (d #t) (k 0)))) (h "1hdc803jgqz0gj4z49g8fhc3m999bkrdc68491w3iq9cdazqm6s9")))

(define-public crate-gfx_scene-0.2.0 (c (n "gfx_scene") (v "0.2.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "0.2.*") (d #t) (k 0)))) (h "1mypsjapivqn0b5rkwl9pdnilpp7qwai44bp5wgskd43lzrqzdh1")))

(define-public crate-gfx_scene-0.3.0 (c (n "gfx_scene") (v "0.3.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "*") (d #t) (k 0)))) (h "11lw1y3a91rv5w8g5jx6my5x0zadp6ikxf3ab4ysz0rb6xs7v2i4")))

(define-public crate-gfx_scene-0.3.1 (c (n "gfx_scene") (v "0.3.1") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "0.6.*") (d #t) (k 0)) (d (n "gfx_phase") (r "*") (d #t) (k 0)))) (h "0xrhss3lzxrbzcc41hrzvj3kgr65gghf6b779zcd20hnwlh0mndl")))

(define-public crate-gfx_scene-0.4.0 (c (n "gfx_scene") (v "0.4.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.4") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)))) (h "02h68c1pih93x65mm38j0l0mma5wlld0qd28szvanc85ylnmkbz6")))

(define-public crate-gfx_scene-0.4.1 (c (n "gfx_scene") (v "0.4.1") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.4") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)))) (h "0i4if6s75kn31x17i4gxaffan1gwnwifnj05aj8pa9mqx65avxv6")))

(define-public crate-gfx_scene-0.5.0 (c (n "gfx_scene") (v "0.5.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "gfx") (r "^0.7") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.5") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)))) (h "0yx37vywinq2nwa7wbz04a8zsgan5zwr06bp4ii4sklhkjg25735")))

(define-public crate-gfx_scene-0.8.0 (c (n "gfx_scene") (v "0.8.0") (d (list (d (n "cgmath") (r "^0.4") (d #t) (k 0)) (d (n "collision") (r "^0.4") (d #t) (k 0)) (d (n "gfx") (r "^0.8") (d #t) (k 0)) (d (n "gfx_phase") (r "^0.6") (d #t) (k 0)) (d (n "hprof") (r "^0.1") (d #t) (k 0)))) (h "0j5ycqmn4xldn0wgrnyp0vcxljn8f0v024vrgf35mkf5mqz7py3j")))

