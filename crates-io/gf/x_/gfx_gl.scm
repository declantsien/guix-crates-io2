(define-module (crates-io gf x_ gfx_gl) #:use-module (crates-io))

(define-public crate-gfx_gl-0.1.0 (c (n "gfx_gl") (v "0.1.0") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)))) (h "0wfwvp0wrjjsd1jhk8mc5x8w3ilrv4kmhpby9hwp6fcs8yd6xblg")))

(define-public crate-gfx_gl-0.1.1 (c (n "gfx_gl") (v "0.1.1") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1nig70sijrbbd8afnbp8p2ksr3i23fic3w8nq16gh1a65c2ydz2a")))

(define-public crate-gfx_gl-0.1.2 (c (n "gfx_gl") (v "0.1.2") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0z7shbs8qj6xgsqpnl8j80il59a9dnbqbsirxmf2kjskdz0l6y66")))

(define-public crate-gfx_gl-0.1.3 (c (n "gfx_gl") (v "0.1.3") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ria54f68v3vp3wz1ps0pd0nx40kf59gwrvlp4zpm5bzy7ky5s4f")))

(define-public crate-gfx_gl-0.1.4 (c (n "gfx_gl") (v "0.1.4") (d (list (d (n "gl_common") (r "*") (d #t) (k 0)) (d (n "gl_generator") (r "*") (d #t) (k 1)) (d (n "khronos_api") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "09qcw51ximcz70425jkcdliarz1hxwhyycz6ysjsq3b2bcigldjg")))

(define-public crate-gfx_gl-0.1.5 (c (n "gfx_gl") (v "0.1.5") (d (list (d (n "gl_common") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.1") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0c3avbmgfazllq5wz4ha7n9ddz2xxx1kz97xsz2c3qhxn6fg60c2")))

(define-public crate-gfx_gl-0.2.0 (c (n "gfx_gl") (v "0.2.0") (d (list (d (n "gl_common") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.2") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14jr6vxnjmbajbii2ddmjwdklnx6mkazgrjl37qh2xwy9gp4846i")))

(define-public crate-gfx_gl-0.3.0 (c (n "gfx_gl") (v "0.3.0") (d (list (d (n "gl_common") (r "^0.1") (d #t) (k 0)) (d (n "gl_generator") (r "^0.4") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0") (d #t) (k 1)))) (h "0gv4cg7f4sqddjxivxssq1kh4cjkn06ivd6zhlym4pvlr32nmgds")))

(define-public crate-gfx_gl-0.3.1 (c (n "gfx_gl") (v "0.3.1") (d (list (d (n "gl_generator") (r "^0.5") (d #t) (k 1)))) (h "0pc4cv1wp5nbps7hpnb48n07wq3jh4fmwkdwpy91pfcs69k3hp7j")))

(define-public crate-gfx_gl-0.4.0 (c (n "gfx_gl") (v "0.4.0") (d (list (d (n "gl_generator") (r "^0.7") (d #t) (k 1)))) (h "0h0m4n8dpzfag5qs5mygg7x7hghjxyilrw230n7cfjsxbky0386s")))

(define-public crate-gfx_gl-0.5.0 (c (n "gfx_gl") (v "0.5.0") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)))) (h "1jzfgrmvb86vx3xmsxndsdpm0ngh5d82bpgrvnkja43ciw7r52iy")))

(define-public crate-gfx_gl-0.6.0 (c (n "gfx_gl") (v "0.6.0") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)))) (h "06c3dhr5iqlp72kw2ma5f3rw2px80xprfdli9yq28r4sj26qh9c5")))

(define-public crate-gfx_gl-0.6.1 (c (n "gfx_gl") (v "0.6.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "0ppzj4bgjawdqz3fvnscqk8lnmgh95pwzh0v96vwy809cxj83lzj")))

