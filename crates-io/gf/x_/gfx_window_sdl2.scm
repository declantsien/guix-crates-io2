(define-module (crates-io gf x_ gfx_window_sdl2) #:use-module (crates-io))

(define-public crate-gfx_window_sdl2-0.1.0 (c (n "gfx_window_sdl2") (v "0.1.0") (d (list (d (n "gfx") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_gl") (r ">= 0.3.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.8") (d #t) (k 0)))) (h "10v197mfcg6a6rxaxdx0pb5rxp7a2d4vfivjb7xnkr0jyyfcyr2s")))

(define-public crate-gfx_window_sdl2-0.2.0 (c (n "gfx_window_sdl2") (v "0.2.0") (d (list (d (n "gfx") (r "^0.8.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.7.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.12.1") (d #t) (k 0)))) (h "04gdqix2kc55r1kjw2gbfmmqkgz80hl13bi65rmn9yxa9r61776f")))

