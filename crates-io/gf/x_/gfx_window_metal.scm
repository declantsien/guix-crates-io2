(define-module (crates-io gf x_ gfx_window_metal) #:use-module (crates-io))

(define-public crate-gfx_window_metal-0.1.0 (c (n "gfx_window_metal") (v "0.1.0") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_metal") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "metal-rs") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1.8") (d #t) (k 0)) (d (n "winit") (r "^0.5") (d #t) (k 0)))) (h "010n0aka8xmlml5qlnxzgxa7313qf9l3iif5dhfqkpbw0xcqi8f6")))

(define-public crate-gfx_window_metal-0.2.0 (c (n "gfx_window_metal") (v "0.2.0") (d (list (d (n "cocoa") (r "^0.5") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_metal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "metal-rs") (r "^0.3") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.5") (d #t) (k 0)))) (h "1my4khrwjshbqg69gshlvznab4wlqzh6cf854b66ix811975xidh")))

(define-public crate-gfx_window_metal-0.3.0 (c (n "gfx_window_metal") (v "0.3.0") (d (list (d (n "cocoa") (r "^0.5") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "gfx_device_metal") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "metal-rs") (r "^0.3") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.6") (d #t) (k 0)))) (h "1ih10ybqlscj2v2gv5s0k1zii7niygyl1z0s58f0p8xvy3nq2yd1")))

(define-public crate-gfx_window_metal-0.4.0 (c (n "gfx_window_metal") (v "0.4.0") (d (list (d (n "cocoa") (r "^0.9") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_metal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metal-rs") (r "^0.4") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.10") (d #t) (k 0)))) (h "0chq78521l7j5f89dknkn0vjqrx9qwrgyr8bkaq6y7fscvigys8w")))

(define-public crate-gfx_window_metal-0.6.0 (c (n "gfx_window_metal") (v "0.6.0") (d (list (d (n "cocoa") (r "^0.9") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "gfx_device_metal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metal-rs") (r "^0.4") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.12") (d #t) (k 0)))) (h "0wyx425vyyl0n04rh84dvlzg7x493wq851i5v4x03cjqqxvg4jd3")))

