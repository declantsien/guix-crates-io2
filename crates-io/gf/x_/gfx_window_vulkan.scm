(define-module (crates-io gf x_ gfx_window_vulkan) #:use-module (crates-io))

(define-public crate-gfx_window_vulkan-0.1.0 (c (n "gfx_window_vulkan") (v "0.1.0") (d (list (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "gfx_device_vulkan") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "vk-sys") (r "^0.2") (d #t) (k 0)) (d (n "winit") (r "^0.5") (d #t) (k 0)))) (h "11a6hh04s2dsdww31979srs75ad7wqv3gl35gk3xxs450jkb02d0")))

