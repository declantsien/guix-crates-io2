(define-module (crates-io gf a- gfa-reader) #:use-module (crates-io))

(define-public crate-gfa-reader-0.1.4 (c (n "gfa-reader") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "0mri9bbv7hv84w6fn3h5xn7b2vm2rfbwwqxvrvinj5y73cmbqy6s")))

