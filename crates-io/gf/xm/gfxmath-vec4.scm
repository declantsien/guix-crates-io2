(define-module (crates-io gf xm gfxmath-vec4) #:use-module (crates-io))

(define-public crate-gfxmath-vec4-0.0.0 (c (n "gfxmath-vec4") (v "0.0.0") (h "15133bgna9sykmxi5n4sw620zh89c417fdh6b86s1a5h93221cjb") (y #t)))

(define-public crate-gfxmath-vec4-0.1.0 (c (n "gfxmath-vec4") (v "0.1.0") (d (list (d (n "opimps") (r "^0.1.4") (d #t) (k 0)))) (h "0r0pqqzcbcr6xhr5f6p7arhsf57fnisvdl6ig7wycpmqlly021kx")))

(define-public crate-gfxmath-vec4-0.1.1 (c (n "gfxmath-vec4") (v "0.1.1") (d (list (d (n "opimps") (r "^0.1.4") (d #t) (k 0)))) (h "0r3vr4ccc0hzngqq0d1isd1bnf1ain22vby607mx1jc4my1w83wp")))

