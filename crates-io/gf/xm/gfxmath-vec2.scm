(define-module (crates-io gf xm gfxmath-vec2) #:use-module (crates-io))

(define-public crate-gfxmath-vec2-0.0.0 (c (n "gfxmath-vec2") (v "0.0.0") (h "12q0bbdlhrs5zpbw9h3h1n3rjphv3w50pvaj009d0dia3p54r1fi") (y #t)))

(define-public crate-gfxmath-vec2-0.1.0 (c (n "gfxmath-vec2") (v "0.1.0") (d (list (d (n "opimps") (r "^0.1.3") (d #t) (k 0)))) (h "1zbqf2q5jhkw1i65n6a4ifmz3yqxblj1d547hv68h88wkb3d7xxd")))

(define-public crate-gfxmath-vec2-0.1.1 (c (n "gfxmath-vec2") (v "0.1.1") (d (list (d (n "opimps") (r "^0.1.3") (d #t) (k 0)))) (h "0b2b0j8n33w2n99lmly0xkjwriy8yl07yp514lg0qbrv67vjdxmb")))

(define-public crate-gfxmath-vec2-0.1.2 (c (n "gfxmath-vec2") (v "0.1.2") (d (list (d (n "opimps") (r "^0.1.3") (d #t) (k 0)))) (h "1kq083wzg3miy638430rj7hnfdh5aigmw05vbvzzdmvr9ljqqdpy")))

(define-public crate-gfxmath-vec2-0.1.3 (c (n "gfxmath-vec2") (v "0.1.3") (d (list (d (n "opimps") (r "^0.1.4") (d #t) (k 0)))) (h "15lvqlmb0pdykflv1ba0w6s460ky9h41q0629g2m0jh5gkwfl5nr")))

(define-public crate-gfxmath-vec2-0.1.4 (c (n "gfxmath-vec2") (v "0.1.4") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 2)) (d (n "khronos_api") (r "^3.1.0") (d #t) (k 2)) (d (n "opimps") (r "^0.1.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 2)))) (h "1wx62p1xzssx670qrvw667l1rqkyxyga32xvgamg3kfcz25l92i4")))

(define-public crate-gfxmath-vec2-0.1.5 (c (n "gfxmath-vec2") (v "0.1.5") (d (list (d (n "opimps") (r "^0.1.4") (d #t) (k 0)))) (h "04729ypf086az7hql0hyg8iidrfqiixb0xf87adkfrqzc9i7cvzj")))

(define-public crate-gfxmath-vec2-0.1.6 (c (n "gfxmath-vec2") (v "0.1.6") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "gl_generator") (r "^0.14.0") (d #t) (k 2)) (d (n "khronos_api") (r "^3.1.0") (d #t) (k 2)) (d (n "opimps") (r "^0.2.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 2)))) (h "0sia79nsjyqj0l0in01ak1816nj54a4yg8b9ylaz6xq5gvjk6hdx")))

