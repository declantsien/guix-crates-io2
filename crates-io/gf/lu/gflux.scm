(define-module (crates-io gf lu gflux) #:use-module (crates-io))

(define-public crate-gflux-0.1.0 (c (n "gflux") (v "0.1.0") (d (list (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "gtk") (r "^0.6") (d #t) (k 2) (p "gtk4")))) (h "1m7b0z3g7s7z45zjfkdkwjnkqi60m22jz8pm3g8gsmgcdhnh1f2g") (y #t)))

(define-public crate-gflux-0.1.1 (c (n "gflux") (v "0.1.1") (d (list (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "gtk") (r "^0.6") (d #t) (k 2) (p "gtk4")))) (h "1ddxi11xrmxkbwskrmpdal6d9qh8ppvbq51adxldx7m9vfqkfdzx")))

(define-public crate-gflux-0.1.2 (c (n "gflux") (v "0.1.2") (d (list (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "gtk") (r "^0.6") (d #t) (k 2) (p "gtk4")))) (h "15hzh6d7hd6wjsw66lsfq0xblc8vmf4idigj4z1ghr8gd1ym7xcx")))

(define-public crate-gflux-0.1.3 (c (n "gflux") (v "0.1.3") (d (list (d (n "glib") (r "^0.17") (d #t) (k 2)) (d (n "gtk") (r "^0.6") (d #t) (k 2) (p "gtk4")))) (h "10zxya83dia98l0r7j9xkqifhfj94km44bhg5rxy064c0sv6ajxr")))

