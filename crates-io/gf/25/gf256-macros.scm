(define-module (crates-io gf #{25}# gf256-macros) #:use-module (crates-io))

(define-public crate-gf256-macros-0.1.0 (c (n "gf256-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "evalexpr") (r "^6.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "19x8i8alkllkj8k0fsd8mphqiqd15whbb0dk2k0aszvg0ipiz1q7") (f (quote (("small-tables") ("shamir") ("rs") ("raid") ("no-xmul") ("no-tables") ("lfsr") ("crc"))))))

(define-public crate-gf256-macros-0.2.0 (c (n "gf256-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "evalexpr") (r "^6.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5243b1zpvji491l8wpigki4v2wn91iw9gsg8h3y39rw0kd42v9") (f (quote (("small-tables") ("shamir") ("rs") ("raid") ("no-xmul") ("no-tables") ("lfsr") ("crc"))))))

(define-public crate-gf256-macros-0.3.0 (c (n "gf256-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "evalexpr") (r "^6.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8rkr7j133pxn30lcn5630nhpmq163456i9zy26s8cpx8ilj21v") (f (quote (("small-tables") ("shamir") ("rs") ("raid") ("no-xmul") ("no-tables") ("lfsr") ("crc"))))))

