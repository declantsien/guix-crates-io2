(define-module (crates-io gf la gflags-derive) #:use-module (crates-io))

(define-public crate-gflags-derive-0.1.0 (c (n "gflags-derive") (v "0.1.0") (d (list (d (n "gflags") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "1f8rhbig1wy8kbps7c84izi39ampavmgxw9v98i4mxjfcjsv3w55")))

